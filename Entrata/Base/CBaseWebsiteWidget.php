<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteWidget extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.website_widgets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultTemplateWidgetId;
	protected $m_intWebsiteTemplateId;
	protected $m_intWebsiteId;
	protected $m_intCustomWidgetParentId;
	protected $m_strWidgetName;
	protected $m_strTitle;
	protected $m_strText;
	protected $m_intCompanyMediaFileId;
	protected $m_strAlt;
	protected $m_strLink;
	protected $m_strLinkText;
	protected $m_strTarget;
	protected $m_strWidgetParameters;
	protected $m_boolIsActive;
	protected $m_boolIsPreview;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strWidgetLabel;
	protected $m_strButtonLabel;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;
		$this->m_boolIsPreview = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_template_widget_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultTemplateWidgetId', trim( $arrValues['default_template_widget_id'] ) ); elseif( isset( $arrValues['default_template_widget_id'] ) ) $this->setDefaultTemplateWidgetId( $arrValues['default_template_widget_id'] );
		if( isset( $arrValues['website_template_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateId', trim( $arrValues['website_template_id'] ) ); elseif( isset( $arrValues['website_template_id'] ) ) $this->setWebsiteTemplateId( $arrValues['website_template_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['custom_widget_parent_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomWidgetParentId', trim( $arrValues['custom_widget_parent_id'] ) ); elseif( isset( $arrValues['custom_widget_parent_id'] ) ) $this->setCustomWidgetParentId( $arrValues['custom_widget_parent_id'] );
		if( isset( $arrValues['widget_name'] ) && $boolDirectSet ) $this->set( 'm_strWidgetName', trim( $arrValues['widget_name'] ) ); elseif( isset( $arrValues['widget_name'] ) ) $this->setWidgetName( $arrValues['widget_name'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strText', trim( $arrValues['text'] ) ); elseif( isset( $arrValues['text'] ) ) $this->setText( $arrValues['text'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['alt'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAlt', trim( $arrValues['alt'] ) ); elseif( isset( $arrValues['alt'] ) ) $this->setAlt( $arrValues['alt'] );
		if( isset( $arrValues['link'] ) && $boolDirectSet ) $this->set( 'm_strLink', trim( $arrValues['link'] ) ); elseif( isset( $arrValues['link'] ) ) $this->setLink( $arrValues['link'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( $arrValues['link_text'] ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( $arrValues['link_text'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( $arrValues['target'] ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( $arrValues['target'] );
		if( isset( $arrValues['widget_parameters'] ) && $boolDirectSet ) $this->set( 'm_strWidgetParameters', trim( $arrValues['widget_parameters'] ) ); elseif( isset( $arrValues['widget_parameters'] ) ) $this->setWidgetParameters( $arrValues['widget_parameters'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_preview'] ) && $boolDirectSet ) $this->set( 'm_boolIsPreview', trim( stripcslashes( $arrValues['is_preview'] ) ) ); elseif( isset( $arrValues['is_preview'] ) ) $this->setIsPreview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_preview'] ) : $arrValues['is_preview'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['widget_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strWidgetLabel', trim( $arrValues['widget_label'] ) ); elseif( isset( $arrValues['widget_label'] ) ) $this->setWidgetLabel( $arrValues['widget_label'] );
		if( isset( $arrValues['button_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strButtonLabel', trim( $arrValues['button_label'] ) ); elseif( isset( $arrValues['button_label'] ) ) $this->setButtonLabel( $arrValues['button_label'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultTemplateWidgetId( $intDefaultTemplateWidgetId ) {
		$this->set( 'm_intDefaultTemplateWidgetId', CStrings::strToIntDef( $intDefaultTemplateWidgetId, NULL, false ) );
	}

	public function getDefaultTemplateWidgetId() {
		return $this->m_intDefaultTemplateWidgetId;
	}

	public function sqlDefaultTemplateWidgetId() {
		return ( true == isset( $this->m_intDefaultTemplateWidgetId ) ) ? ( string ) $this->m_intDefaultTemplateWidgetId : 'NULL';
	}

	public function setWebsiteTemplateId( $intWebsiteTemplateId ) {
		$this->set( 'm_intWebsiteTemplateId', CStrings::strToIntDef( $intWebsiteTemplateId, NULL, false ) );
	}

	public function getWebsiteTemplateId() {
		return $this->m_intWebsiteTemplateId;
	}

	public function sqlWebsiteTemplateId() {
		return ( true == isset( $this->m_intWebsiteTemplateId ) ) ? ( string ) $this->m_intWebsiteTemplateId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setCustomWidgetParentId( $intCustomWidgetParentId ) {
		$this->set( 'm_intCustomWidgetParentId', CStrings::strToIntDef( $intCustomWidgetParentId, NULL, false ) );
	}

	public function getCustomWidgetParentId() {
		return $this->m_intCustomWidgetParentId;
	}

	public function sqlCustomWidgetParentId() {
		return ( true == isset( $this->m_intCustomWidgetParentId ) ) ? ( string ) $this->m_intCustomWidgetParentId : 'NULL';
	}

	public function setWidgetName( $strWidgetName ) {
		$this->set( 'm_strWidgetName', CStrings::strTrimDef( $strWidgetName, 250, NULL, true ) );
	}

	public function getWidgetName() {
		return $this->m_strWidgetName;
	}

	public function sqlWidgetName() {
		return ( true == isset( $this->m_strWidgetName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWidgetName ) : '\'' . addslashes( $this->m_strWidgetName ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 250, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setText( $strText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strText', CStrings::strTrimDef( $strText, -1, NULL, true ), $strLocaleCode );
	}

	public function getText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strText', $strLocaleCode );
	}

	public function sqlText() {
		return ( true == isset( $this->m_strText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strText ) : '\'' . addslashes( $this->m_strText ) . '\'' ) : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setAlt( $strAlt, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAlt', CStrings::strTrimDef( $strAlt, 250, NULL, true ), $strLocaleCode );
	}

	public function getAlt( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAlt', $strLocaleCode );
	}

	public function sqlAlt() {
		return ( true == isset( $this->m_strAlt ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAlt ) : '\'' . addslashes( $this->m_strAlt ) . '\'' ) : 'NULL';
	}

	public function setLink( $strLink ) {
		$this->set( 'm_strLink', CStrings::strTrimDef( $strLink, -1, NULL, true ) );
	}

	public function getLink() {
		return $this->m_strLink;
	}

	public function sqlLink() {
		return ( true == isset( $this->m_strLink ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLink ) : '\'' . addslashes( $this->m_strLink ) . '\'' ) : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, 100, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkText ) : '\'' . addslashes( $this->m_strLinkText ) . '\'' ) : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, 30, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTarget ) : '\'' . addslashes( $this->m_strTarget ) . '\'' ) : 'NULL';
	}

	public function setWidgetParameters( $strWidgetParameters ) {
		$this->set( 'm_strWidgetParameters', CStrings::strTrimDef( $strWidgetParameters, -1, NULL, true ) );
	}

	public function getWidgetParameters() {
		return $this->m_strWidgetParameters;
	}

	public function sqlWidgetParameters() {
		return ( true == isset( $this->m_strWidgetParameters ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWidgetParameters ) : '\'' . addslashes( $this->m_strWidgetParameters ) . '\'' ) : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPreview( $boolIsPreview ) {
		$this->set( 'm_boolIsPreview', CStrings::strToBool( $boolIsPreview ) );
	}

	public function getIsPreview() {
		return $this->m_boolIsPreview;
	}

	public function sqlIsPreview() {
		return ( true == isset( $this->m_boolIsPreview ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPreview ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setWidgetLabel( $strWidgetLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strWidgetLabel', CStrings::strTrimDef( $strWidgetLabel, 250, NULL, true ), $strLocaleCode );
	}

	public function getWidgetLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strWidgetLabel', $strLocaleCode );
	}

	public function sqlWidgetLabel() {
		return ( true == isset( $this->m_strWidgetLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWidgetLabel ) : '\'' . addslashes( $this->m_strWidgetLabel ) . '\'' ) : 'NULL';
	}

	public function setButtonLabel( $strButtonLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strButtonLabel', CStrings::strTrimDef( $strButtonLabel, -1, NULL, true ), $strLocaleCode );
	}

	public function getButtonLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strButtonLabel', $strLocaleCode );
	}

	public function sqlButtonLabel() {
		return ( true == isset( $this->m_strButtonLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strButtonLabel ) : '\'' . addslashes( $this->m_strButtonLabel ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_template_widget_id, website_template_id, website_id, custom_widget_parent_id, widget_name, title, text, company_media_file_id, alt, link, link_text, target, widget_parameters, is_active, is_preview, updated_by, updated_on, created_by, created_on, details, widget_label, button_label )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultTemplateWidgetId() . ', ' .
						$this->sqlWebsiteTemplateId() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlCustomWidgetParentId() . ', ' .
						$this->sqlWidgetName() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlText() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlAlt() . ', ' .
						$this->sqlLink() . ', ' .
						$this->sqlLinkText() . ', ' .
						$this->sqlTarget() . ', ' .
						$this->sqlWidgetParameters() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlIsPreview() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlWidgetLabel() . ', ' .
						$this->sqlButtonLabel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_template_widget_id = ' . $this->sqlDefaultTemplateWidgetId(). ',' ; } elseif( true == array_key_exists( 'DefaultTemplateWidgetId', $this->getChangedColumns() ) ) { $strSql .= ' default_template_widget_id = ' . $this->sqlDefaultTemplateWidgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_widget_parent_id = ' . $this->sqlCustomWidgetParentId(). ',' ; } elseif( true == array_key_exists( 'CustomWidgetParentId', $this->getChangedColumns() ) ) { $strSql .= ' custom_widget_parent_id = ' . $this->sqlCustomWidgetParentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_name = ' . $this->sqlWidgetName(). ',' ; } elseif( true == array_key_exists( 'WidgetName', $this->getChangedColumns() ) ) { $strSql .= ' widget_name = ' . $this->sqlWidgetName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' text = ' . $this->sqlText(). ',' ; } elseif( true == array_key_exists( 'Text', $this->getChangedColumns() ) ) { $strSql .= ' text = ' . $this->sqlText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt = ' . $this->sqlAlt(). ',' ; } elseif( true == array_key_exists( 'Alt', $this->getChangedColumns() ) ) { $strSql .= ' alt = ' . $this->sqlAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link = ' . $this->sqlLink(). ',' ; } elseif( true == array_key_exists( 'Link', $this->getChangedColumns() ) ) { $strSql .= ' link = ' . $this->sqlLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget(). ',' ; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_parameters = ' . $this->sqlWidgetParameters(). ',' ; } elseif( true == array_key_exists( 'WidgetParameters', $this->getChangedColumns() ) ) { $strSql .= ' widget_parameters = ' . $this->sqlWidgetParameters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_preview = ' . $this->sqlIsPreview(). ',' ; } elseif( true == array_key_exists( 'IsPreview', $this->getChangedColumns() ) ) { $strSql .= ' is_preview = ' . $this->sqlIsPreview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_label = ' . $this->sqlWidgetLabel(). ',' ; } elseif( true == array_key_exists( 'WidgetLabel', $this->getChangedColumns() ) ) { $strSql .= ' widget_label = ' . $this->sqlWidgetLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' button_label = ' . $this->sqlButtonLabel(). ',' ; } elseif( true == array_key_exists( 'ButtonLabel', $this->getChangedColumns() ) ) { $strSql .= ' button_label = ' . $this->sqlButtonLabel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_template_widget_id' => $this->getDefaultTemplateWidgetId(),
			'website_template_id' => $this->getWebsiteTemplateId(),
			'website_id' => $this->getWebsiteId(),
			'custom_widget_parent_id' => $this->getCustomWidgetParentId(),
			'widget_name' => $this->getWidgetName(),
			'title' => $this->getTitle(),
			'text' => $this->getText(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'alt' => $this->getAlt(),
			'link' => $this->getLink(),
			'link_text' => $this->getLinkText(),
			'target' => $this->getTarget(),
			'widget_parameters' => $this->getWidgetParameters(),
			'is_active' => $this->getIsActive(),
			'is_preview' => $this->getIsPreview(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'widget_label' => $this->getWidgetLabel(),
			'button_label' => $this->getButtonLabel()
		);
	}

}
?>