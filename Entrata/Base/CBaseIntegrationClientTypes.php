<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientTypes
 * Do not add any new functions to this class.
 */

class CBaseIntegrationClientTypes extends CEosPluralBase {

	/**
	 * @return CIntegrationClientType[]
	 */
	public static function fetchIntegrationClientTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationClientType::class, $objDatabase );
	}

	/**
	 * @return CIntegrationClientType
	 */
	public static function fetchIntegrationClientType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationClientType::class, $objDatabase );
	}

	public static function fetchIntegrationClientTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_client_types', $objDatabase );
	}

	public static function fetchIntegrationClientTypeById( $intId, $objDatabase ) {
		return self::fetchIntegrationClientType( sprintf( 'SELECT * FROM integration_client_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>