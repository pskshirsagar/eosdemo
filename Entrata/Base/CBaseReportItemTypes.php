<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportItemTypes
 * Do not add any new functions to this class.
 */

class CBaseReportItemTypes extends CEosPluralBase {

	/**
	 * @return CReportItemType[]
	 */
	public static function fetchReportItemTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportItemType::class, $objDatabase );
	}

	/**
	 * @return CReportItemType
	 */
	public static function fetchReportItemType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportItemType::class, $objDatabase );
	}

	public static function fetchReportItemTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_item_types', $objDatabase );
	}

	public static function fetchReportItemTypeById( $intId, $objDatabase ) {
		return self::fetchReportItemType( sprintf( 'SELECT * FROM report_item_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>