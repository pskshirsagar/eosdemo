<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayments extends CEosPluralBase {

	/**
	 * @return CApPayment[]
	 */
	public static function fetchApPayments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayment
	 */
	public static function fetchApPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayment', $objDatabase );
	}

	public static function fetchApPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payments', $objDatabase );
	}

	public static function fetchApPaymentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayment( sprintf( 'SELECT * FROM ap_payments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE merchant_account_id = %d AND cid = %d', ( int ) $intMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByMerchantGatewayIdByCid( $intMerchantGatewayId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE merchant_gateway_id = %d AND cid = %d', ( int ) $intMerchantGatewayId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByPaymentMediumIdByCid( $intPaymentMediumId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE payment_medium_id = %d AND cid = %d', ( int ) $intPaymentMediumId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByApPaymentTypeIdByCid( $intApPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE ap_payment_type_id = %d AND cid = %d', ( int ) $intApPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByPaymentStatusTypeIdByCid( $intPaymentStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE payment_status_type_id = %d AND cid = %d', ( int ) $intPaymentStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByReturnTypeIdByCid( $intReturnTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE return_type_id = %d AND cid = %d', ( int ) $intReturnTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE check_account_type_id = %d AND cid = %d', ( int ) $intCheckAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByApPaymentBatchIdByCid( $intApPaymentBatchId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE ap_payment_batch_id = %d AND cid = %d', ( int ) $intApPaymentBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsBySourceCheckAccountTypeIdByCid( $intSourceCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE source_check_account_type_id = %d AND cid = %d', ( int ) $intSourceCheckAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentsByApRemittanceIdByCid( $intApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApPayments( sprintf( 'SELECT * FROM ap_payments WHERE ap_remittance_id = %d AND cid = %d', ( int ) $intApRemittanceId, ( int ) $intCid ), $objDatabase );
	}

}
?>