<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReturnTypeOptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReturnTypeOptions extends CEosPluralBase {

	/**
	 * @return CReturnTypeOption[]
	 */
	public static function fetchReturnTypeOptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CReturnTypeOption', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReturnTypeOption
	 */
	public static function fetchReturnTypeOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReturnTypeOption', $objDatabase );
	}

	public static function fetchReturnTypeOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'return_type_options', $objDatabase );
	}

	public static function fetchReturnTypeOptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReturnTypeOption( sprintf( 'SELECT * FROM return_type_options WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReturnTypeOptionsByCid( $intCid, $objDatabase ) {
		return self::fetchReturnTypeOptions( sprintf( 'SELECT * FROM return_type_options WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReturnTypeOptionsByReturnTypeIdByCid( $intReturnTypeId, $intCid, $objDatabase ) {
		return self::fetchReturnTypeOptions( sprintf( 'SELECT * FROM return_type_options WHERE return_type_id = %d AND cid = %d', ( int ) $intReturnTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>