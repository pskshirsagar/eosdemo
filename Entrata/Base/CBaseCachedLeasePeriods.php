<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedLeasePeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedLeasePeriods extends CEosPluralBase {

	/**
	 * @return CCachedLeasePeriod[]
	 */
	public static function fetchCachedLeasePeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCachedLeasePeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedLeasePeriod
	 */
	public static function fetchCachedLeasePeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCachedLeasePeriod', $objDatabase );
	}

	public static function fetchCachedLeasePeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_lease_periods', $objDatabase );
	}

	public static function fetchCachedLeasePeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriod( sprintf( 'SELECT * FROM cached_lease_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByCachedLeaseLogIdByCid( $intCachedLeaseLogId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE cached_lease_log_id = %d AND cid = %d', ( int ) $intCachedLeaseLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedLeasePeriodsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedLeasePeriods( sprintf( 'SELECT * FROM cached_lease_periods WHERE lease_status_type_id = %d AND cid = %d', ( int ) $intLeaseStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>