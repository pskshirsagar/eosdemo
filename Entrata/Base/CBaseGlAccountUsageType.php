<?php

class CBaseGlAccountUsageType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.gl_account_usage_types';

	protected $m_intId;
	protected $m_intGlGroupTypeId;
	protected $m_arrintDebitArCodeTypes;
	protected $m_arrintCreditArCodeTypes;
	protected $m_arrintCreditArTriggerIds;
	protected $m_arrintCreditArOrigins;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolAllowNullTriggers;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intGlAccountTypeId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowNullTriggers = true;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['debit_ar_code_types'] ) && $boolDirectSet ) $this->set( 'm_arrintDebitArCodeTypes', trim( $arrValues['debit_ar_code_types'] ) ); elseif( isset( $arrValues['debit_ar_code_types'] ) ) $this->setDebitArCodeTypes( $arrValues['debit_ar_code_types'] );
		if( isset( $arrValues['credit_ar_code_types'] ) && $boolDirectSet ) $this->set( 'm_arrintCreditArCodeTypes', trim( $arrValues['credit_ar_code_types'] ) ); elseif( isset( $arrValues['credit_ar_code_types'] ) ) $this->setCreditArCodeTypes( $arrValues['credit_ar_code_types'] );
		if( isset( $arrValues['credit_ar_trigger_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintCreditArTriggerIds', trim( $arrValues['credit_ar_trigger_ids'] ) ); elseif( isset( $arrValues['credit_ar_trigger_ids'] ) ) $this->setCreditArTriggerIds( $arrValues['credit_ar_trigger_ids'] );
		if( isset( $arrValues['credit_ar_origins'] ) && $boolDirectSet ) $this->set( 'm_arrintCreditArOrigins', trim( $arrValues['credit_ar_origins'] ) ); elseif( isset( $arrValues['credit_ar_origins'] ) ) $this->setCreditArOrigins( $arrValues['credit_ar_origins'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['allow_null_triggers'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNullTriggers', trim( stripcslashes( $arrValues['allow_null_triggers'] ) ) ); elseif( isset( $arrValues['allow_null_triggers'] ) ) $this->setAllowNullTriggers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_null_triggers'] ) : $arrValues['allow_null_triggers'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setDebitArCodeTypes( $arrintDebitArCodeTypes ) {
		$this->set( 'm_arrintDebitArCodeTypes', CStrings::strToArrIntDef( $arrintDebitArCodeTypes, NULL ) );
	}

	public function getDebitArCodeTypes() {
		return $this->m_arrintDebitArCodeTypes;
	}

	public function sqlDebitArCodeTypes() {
		return ( true == isset( $this->m_arrintDebitArCodeTypes ) && true == valArr( $this->m_arrintDebitArCodeTypes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDebitArCodeTypes, NULL ) . '\'' : 'NULL';
	}

	public function setCreditArCodeTypes( $arrintCreditArCodeTypes ) {
		$this->set( 'm_arrintCreditArCodeTypes', CStrings::strToArrIntDef( $arrintCreditArCodeTypes, NULL ) );
	}

	public function getCreditArCodeTypes() {
		return $this->m_arrintCreditArCodeTypes;
	}

	public function sqlCreditArCodeTypes() {
		return ( true == isset( $this->m_arrintCreditArCodeTypes ) && true == valArr( $this->m_arrintCreditArCodeTypes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCreditArCodeTypes, NULL ) . '\'' : 'NULL';
	}

	public function setCreditArTriggerIds( $arrintCreditArTriggerIds ) {
		$this->set( 'm_arrintCreditArTriggerIds', CStrings::strToArrIntDef( $arrintCreditArTriggerIds, NULL ) );
	}

	public function getCreditArTriggerIds() {
		return $this->m_arrintCreditArTriggerIds;
	}

	public function sqlCreditArTriggerIds() {
		return ( true == isset( $this->m_arrintCreditArTriggerIds ) && true == valArr( $this->m_arrintCreditArTriggerIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCreditArTriggerIds, NULL ) . '\'' : 'NULL';
	}

	public function setCreditArOrigins( $arrintCreditArOrigins ) {
		$this->set( 'm_arrintCreditArOrigins', CStrings::strToArrIntDef( $arrintCreditArOrigins, NULL ) );
	}

	public function getCreditArOrigins() {
		return $this->m_arrintCreditArOrigins;
	}

	public function sqlCreditArOrigins() {
		return ( true == isset( $this->m_arrintCreditArOrigins ) && true == valArr( $this->m_arrintCreditArOrigins ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCreditArOrigins, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setAllowNullTriggers( $boolAllowNullTriggers ) {
		$this->set( 'm_boolAllowNullTriggers', CStrings::strToBool( $boolAllowNullTriggers ) );
	}

	public function getAllowNullTriggers() {
		return $this->m_boolAllowNullTriggers;
	}

	public function sqlAllowNullTriggers() {
		return ( true == isset( $this->m_boolAllowNullTriggers ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNullTriggers ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'debit_ar_code_types' => $this->getDebitArCodeTypes(),
			'credit_ar_code_types' => $this->getCreditArCodeTypes(),
			'credit_ar_trigger_ids' => $this->getCreditArTriggerIds(),
			'credit_ar_origins' => $this->getCreditArOrigins(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'allow_null_triggers' => $this->getAllowNullTriggers(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>