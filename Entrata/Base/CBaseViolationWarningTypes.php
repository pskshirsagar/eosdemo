<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationWarningTypes
 * Do not add any new functions to this class.
 */

class CBaseViolationWarningTypes extends CEosPluralBase {

	/**
	 * @return CViolationWarningType[]
	 */
	public static function fetchViolationWarningTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CViolationWarningType::class, $objDatabase );
	}

	/**
	 * @return CViolationWarningType
	 */
	public static function fetchViolationWarningType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationWarningType::class, $objDatabase );
	}

	public static function fetchViolationWarningTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_warning_types', $objDatabase );
	}

	public static function fetchViolationWarningTypeById( $intId, $objDatabase ) {
		return self::fetchViolationWarningType( sprintf( 'SELECT * FROM violation_warning_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>