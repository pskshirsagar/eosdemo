<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserCalendars
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserCalendars extends CEosPluralBase {

	/**
	 * @return CCompanyUserCalendar[]
	 */
	public static function fetchCompanyUserCalendars( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserCalendar', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserCalendar
	 */
	public static function fetchCompanyUserCalendar( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserCalendar', $objDatabase );
	}

	public static function fetchCompanyUserCalendarCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_calendars', $objDatabase );
	}

	public static function fetchCompanyUserCalendarByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserCalendar( sprintf( 'SELECT * FROM company_user_calendars WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserCalendarsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserCalendars( sprintf( 'SELECT * FROM company_user_calendars WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserCalendarsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserCalendars( sprintf( 'SELECT * FROM company_user_calendars WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserCalendarsByCalendarPropertyIdByCid( $intCalendarPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserCalendars( sprintf( 'SELECT * FROM company_user_calendars WHERE calendar_property_id = %d AND cid = %d', ( int ) $intCalendarPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserCalendarsByCalendarCompanyUserIdByCid( $intCalendarCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserCalendars( sprintf( 'SELECT * FROM company_user_calendars WHERE calendar_company_user_id = %d AND cid = %d', ( int ) $intCalendarCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>