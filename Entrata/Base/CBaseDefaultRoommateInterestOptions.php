<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultRoommateInterestOptions
 * Do not add any new functions to this class.
 */

class CBaseDefaultRoommateInterestOptions extends CEosPluralBase {

	/**
	 * @return CDefaultRoommateInterestOption[]
	 */
	public static function fetchDefaultRoommateInterestOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultRoommateInterestOption::class, $objDatabase );
	}

	/**
	 * @return CDefaultRoommateInterestOption
	 */
	public static function fetchDefaultRoommateInterestOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultRoommateInterestOption::class, $objDatabase );
	}

	public static function fetchDefaultRoommateInterestOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_roommate_interest_options', $objDatabase );
	}

	public static function fetchDefaultRoommateInterestOptionById( $intId, $objDatabase ) {
		return self::fetchDefaultRoommateInterestOption( sprintf( 'SELECT * FROM default_roommate_interest_options WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultRoommateInterestOptionsByDefaultRoommateInterestId( $intDefaultRoommateInterestId, $objDatabase ) {
		return self::fetchDefaultRoommateInterestOptions( sprintf( 'SELECT * FROM default_roommate_interest_options WHERE default_roommate_interest_id = %d', $intDefaultRoommateInterestId ), $objDatabase );
	}

}
?>