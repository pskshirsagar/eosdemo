<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFile extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.files';

	protected $m_intId;
	protected $m_intFileExtensionId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyUserId;
	protected $m_intDocumentId;
	protected $m_intDocumentAddendaId;
	protected $m_intFileId;
	protected $m_intFileTypeId;
	protected $m_intFileSize;
	protected $m_strFileUploadDate;
	protected $m_strRemoteIp;
	protected $m_strRemotePrimaryKey;
	protected $m_strTitle;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strVersion;
	protected $m_intShowInPortal;
	protected $m_intRequireCountersign;
	protected $m_intCountersignedBy;
	protected $m_strCountersignedOn;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intShowInPortal = '0';
		$this->m_intRequireCountersign = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['file_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExtensionId', trim( $arrValues['file_extension_id'] ) ); elseif( isset( $arrValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrValues['file_extension_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['document_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentAddendaId', trim( $arrValues['document_addenda_id'] ) ); elseif( isset( $arrValues['document_addenda_id'] ) ) $this->setDocumentAddendaId( $arrValues['document_addenda_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_intFileSize', trim( $arrValues['file_size'] ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( $arrValues['file_size'] );
		if( isset( $arrValues['file_upload_date'] ) && $boolDirectSet ) $this->set( 'm_strFileUploadDate', trim( $arrValues['file_upload_date'] ) ); elseif( isset( $arrValues['file_upload_date'] ) ) $this->setFileUploadDate( $arrValues['file_upload_date'] );
		if( isset( $arrValues['remote_ip'] ) && $boolDirectSet ) $this->set( 'm_strRemoteIp', trim( stripcslashes( $arrValues['remote_ip'] ) ) ); elseif( isset( $arrValues['remote_ip'] ) ) $this->setRemoteIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_ip'] ) : $arrValues['remote_ip'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['version'] ) && $boolDirectSet ) $this->set( 'm_strVersion', trim( stripcslashes( $arrValues['version'] ) ) ); elseif( isset( $arrValues['version'] ) ) $this->setVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['version'] ) : $arrValues['version'] );
		if( isset( $arrValues['show_in_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowInPortal', trim( $arrValues['show_in_portal'] ) ); elseif( isset( $arrValues['show_in_portal'] ) ) $this->setShowInPortal( $arrValues['show_in_portal'] );
		if( isset( $arrValues['require_countersign'] ) && $boolDirectSet ) $this->set( 'm_intRequireCountersign', trim( $arrValues['require_countersign'] ) ); elseif( isset( $arrValues['require_countersign'] ) ) $this->setRequireCountersign( $arrValues['require_countersign'] );
		if( isset( $arrValues['countersigned_by'] ) && $boolDirectSet ) $this->set( 'm_intCountersignedBy', trim( $arrValues['countersigned_by'] ) ); elseif( isset( $arrValues['countersigned_by'] ) ) $this->setCountersignedBy( $arrValues['countersigned_by'] );
		if( isset( $arrValues['countersigned_on'] ) && $boolDirectSet ) $this->set( 'm_strCountersignedOn', trim( $arrValues['countersigned_on'] ) ); elseif( isset( $arrValues['countersigned_on'] ) ) $this->setCountersignedOn( $arrValues['countersigned_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->set( 'm_intFileExtensionId', CStrings::strToIntDef( $intFileExtensionId, NULL, false ) );
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function sqlFileExtensionId() {
		return ( true == isset( $this->m_intFileExtensionId ) ) ? ( string ) $this->m_intFileExtensionId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDocumentAddendaId( $intDocumentAddendaId ) {
		$this->set( 'm_intDocumentAddendaId', CStrings::strToIntDef( $intDocumentAddendaId, NULL, false ) );
	}

	public function getDocumentAddendaId() {
		return $this->m_intDocumentAddendaId;
	}

	public function sqlDocumentAddendaId() {
		return ( true == isset( $this->m_intDocumentAddendaId ) ) ? ( string ) $this->m_intDocumentAddendaId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setFileSize( $intFileSize ) {
		$this->set( 'm_intFileSize', CStrings::strToIntDef( $intFileSize, NULL, false ) );
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_intFileSize ) ) ? ( string ) $this->m_intFileSize : 'NULL';
	}

	public function setFileUploadDate( $strFileUploadDate ) {
		$this->set( 'm_strFileUploadDate', CStrings::strTrimDef( $strFileUploadDate, -1, NULL, true ) );
	}

	public function getFileUploadDate() {
		return $this->m_strFileUploadDate;
	}

	public function sqlFileUploadDate() {
		return ( true == isset( $this->m_strFileUploadDate ) ) ? '\'' . $this->m_strFileUploadDate . '\'' : 'NULL';
	}

	public function setRemoteIp( $strRemoteIp ) {
		$this->set( 'm_strRemoteIp', CStrings::strTrimDef( $strRemoteIp, 30, NULL, true ) );
	}

	public function getRemoteIp() {
		return $this->m_strRemoteIp;
	}

	public function sqlRemoteIp() {
		return ( true == isset( $this->m_strRemoteIp ) ) ? '\'' . addslashes( $this->m_strRemoteIp ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setVersion( $strVersion ) {
		$this->set( 'm_strVersion', CStrings::strTrimDef( $strVersion, 50, NULL, true ) );
	}

	public function getVersion() {
		return $this->m_strVersion;
	}

	public function sqlVersion() {
		return ( true == isset( $this->m_strVersion ) ) ? '\'' . addslashes( $this->m_strVersion ) . '\'' : 'NULL';
	}

	public function setShowInPortal( $intShowInPortal ) {
		$this->set( 'm_intShowInPortal', CStrings::strToIntDef( $intShowInPortal, NULL, false ) );
	}

	public function getShowInPortal() {
		return $this->m_intShowInPortal;
	}

	public function sqlShowInPortal() {
		return ( true == isset( $this->m_intShowInPortal ) ) ? ( string ) $this->m_intShowInPortal : '0';
	}

	public function setRequireCountersign( $intRequireCountersign ) {
		$this->set( 'm_intRequireCountersign', CStrings::strToIntDef( $intRequireCountersign, NULL, false ) );
	}

	public function getRequireCountersign() {
		return $this->m_intRequireCountersign;
	}

	public function sqlRequireCountersign() {
		return ( true == isset( $this->m_intRequireCountersign ) ) ? ( string ) $this->m_intRequireCountersign : '0';
	}

	public function setCountersignedBy( $intCountersignedBy ) {
		$this->set( 'm_intCountersignedBy', CStrings::strToIntDef( $intCountersignedBy, NULL, false ) );
	}

	public function getCountersignedBy() {
		return $this->m_intCountersignedBy;
	}

	public function sqlCountersignedBy() {
		return ( true == isset( $this->m_intCountersignedBy ) ) ? ( string ) $this->m_intCountersignedBy : 'NULL';
	}

	public function setCountersignedOn( $strCountersignedOn ) {
		$this->set( 'm_strCountersignedOn', CStrings::strTrimDef( $strCountersignedOn, -1, NULL, true ) );
	}

	public function getCountersignedOn() {
		return $this->m_strCountersignedOn;
	}

	public function sqlCountersignedOn() {
		return ( true == isset( $this->m_strCountersignedOn ) ) ? '\'' . $this->m_strCountersignedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, file_extension_id, cid, property_id, company_user_id, document_id, document_addenda_id, file_id, file_type_id, file_size, file_upload_date, remote_ip, remote_primary_key, title, file_name, file_path, details, version, show_in_portal, require_countersign, countersigned_by, countersigned_on, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlFileExtensionId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlDocumentAddendaId() . ', ' .
						$this->sqlFileId() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlFileSize() . ', ' .
						$this->sqlFileUploadDate() . ', ' .
						$this->sqlRemoteIp() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlVersion() . ', ' .
						$this->sqlShowInPortal() . ', ' .
						$this->sqlRequireCountersign() . ', ' .
						$this->sqlCountersignedBy() . ', ' .
						$this->sqlCountersignedOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId(). ',' ; } elseif( true == array_key_exists( 'FileExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId(). ',' ; } elseif( true == array_key_exists( 'DocumentAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId(). ',' ; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize(). ',' ; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_upload_date = ' . $this->sqlFileUploadDate(). ',' ; } elseif( true == array_key_exists( 'FileUploadDate', $this->getChangedColumns() ) ) { $strSql .= ' file_upload_date = ' . $this->sqlFileUploadDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_ip = ' . $this->sqlRemoteIp(). ',' ; } elseif( true == array_key_exists( 'RemoteIp', $this->getChangedColumns() ) ) { $strSql .= ' remote_ip = ' . $this->sqlRemoteIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version = ' . $this->sqlVersion(). ',' ; } elseif( true == array_key_exists( 'Version', $this->getChangedColumns() ) ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_portal = ' . $this->sqlShowInPortal(). ',' ; } elseif( true == array_key_exists( 'ShowInPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_in_portal = ' . $this->sqlShowInPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_countersign = ' . $this->sqlRequireCountersign(). ',' ; } elseif( true == array_key_exists( 'RequireCountersign', $this->getChangedColumns() ) ) { $strSql .= ' require_countersign = ' . $this->sqlRequireCountersign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' countersigned_by = ' . $this->sqlCountersignedBy(). ',' ; } elseif( true == array_key_exists( 'CountersignedBy', $this->getChangedColumns() ) ) { $strSql .= ' countersigned_by = ' . $this->sqlCountersignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' countersigned_on = ' . $this->sqlCountersignedOn(). ',' ; } elseif( true == array_key_exists( 'CountersignedOn', $this->getChangedColumns() ) ) { $strSql .= ' countersigned_on = ' . $this->sqlCountersignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'file_extension_id' => $this->getFileExtensionId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_user_id' => $this->getCompanyUserId(),
			'document_id' => $this->getDocumentId(),
			'document_addenda_id' => $this->getDocumentAddendaId(),
			'file_id' => $this->getFileId(),
			'file_type_id' => $this->getFileTypeId(),
			'file_size' => $this->getFileSize(),
			'file_upload_date' => $this->getFileUploadDate(),
			'remote_ip' => $this->getRemoteIp(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'title' => $this->getTitle(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'details' => $this->getDetails(),
			'version' => $this->getVersion(),
			'show_in_portal' => $this->getShowInPortal(),
			'require_countersign' => $this->getRequireCountersign(),
			'countersigned_by' => $this->getCountersignedBy(),
			'countersigned_on' => $this->getCountersignedOn(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>