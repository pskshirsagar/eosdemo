<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwner extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.owners';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intOwnerTypeId;
	protected $m_intApPayeeId;
	protected $m_intBusinessCompanyUserId;
	protected $m_intAccountingCompanyUserId;
	protected $m_intDistributionGlAccountId;
	protected $m_intContributionGlAccountId;
	protected $m_strOwnerName;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_boolIsAddressVerified;
	protected $m_strPhoneNumber;
	protected $m_strFaxNumber;
	protected $m_strWebsiteUrl;
	protected $m_strTaxNumberEncrypted;
	protected $m_intSecurityDepositsReducesDraw;
	protected $m_intShouldForwardDistributions;
	protected $m_intHasOwnership;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAddressVerified = false;
		$this->m_intSecurityDepositsReducesDraw = '0';
		$this->m_intShouldForwardDistributions = '1';
		$this->m_intHasOwnership = '0';
		$this->m_arrstrPostalAddressFields = [
			'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['owner_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerTypeId', trim( $arrValues['owner_type_id'] ) ); elseif( isset( $arrValues['owner_type_id'] ) ) $this->setOwnerTypeId( $arrValues['owner_type_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['business_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBusinessCompanyUserId', trim( $arrValues['business_company_user_id'] ) ); elseif( isset( $arrValues['business_company_user_id'] ) ) $this->setBusinessCompanyUserId( $arrValues['business_company_user_id'] );
		if( isset( $arrValues['accounting_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingCompanyUserId', trim( $arrValues['accounting_company_user_id'] ) ); elseif( isset( $arrValues['accounting_company_user_id'] ) ) $this->setAccountingCompanyUserId( $arrValues['accounting_company_user_id'] );
		if( isset( $arrValues['distribution_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDistributionGlAccountId', trim( $arrValues['distribution_gl_account_id'] ) ); elseif( isset( $arrValues['distribution_gl_account_id'] ) ) $this->setDistributionGlAccountId( $arrValues['distribution_gl_account_id'] );
		if( isset( $arrValues['contribution_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intContributionGlAccountId', trim( $arrValues['contribution_gl_account_id'] ) ); elseif( isset( $arrValues['contribution_gl_account_id'] ) ) $this->setContributionGlAccountId( $arrValues['contribution_gl_account_id'] );
		if( isset( $arrValues['owner_name'] ) && $boolDirectSet ) $this->set( 'm_strOwnerName', trim( $arrValues['owner_name'] ) ); elseif( isset( $arrValues['owner_name'] ) ) $this->setOwnerName( $arrValues['owner_name'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( $arrValues['street_line1'] ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( $arrValues['street_line2'] ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( $arrValues['street_line3'] ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( $arrValues['province'] ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['is_address_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsAddressVerified', trim( stripcslashes( $arrValues['is_address_verified'] ) ) ); elseif( isset( $arrValues['is_address_verified'] ) ) $this->setIsAddressVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_address_verified'] ) : $arrValues['is_address_verified'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( $arrValues['website_url'] ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( $arrValues['website_url'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['security_deposits_reduces_draw'] ) && $boolDirectSet ) $this->set( 'm_intSecurityDepositsReducesDraw', trim( $arrValues['security_deposits_reduces_draw'] ) ); elseif( isset( $arrValues['security_deposits_reduces_draw'] ) ) $this->setSecurityDepositsReducesDraw( $arrValues['security_deposits_reduces_draw'] );
		if( isset( $arrValues['should_forward_distributions'] ) && $boolDirectSet ) $this->set( 'm_intShouldForwardDistributions', trim( $arrValues['should_forward_distributions'] ) ); elseif( isset( $arrValues['should_forward_distributions'] ) ) $this->setShouldForwardDistributions( $arrValues['should_forward_distributions'] );
		if( isset( $arrValues['has_ownership'] ) && $boolDirectSet ) $this->set( 'm_intHasOwnership', trim( $arrValues['has_ownership'] ) ); elseif( isset( $arrValues['has_ownership'] ) ) $this->setHasOwnership( $arrValues['has_ownership'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setOwnerTypeId( $intOwnerTypeId ) {
		$this->set( 'm_intOwnerTypeId', CStrings::strToIntDef( $intOwnerTypeId, NULL, false ) );
	}

	public function getOwnerTypeId() {
		return $this->m_intOwnerTypeId;
	}

	public function sqlOwnerTypeId() {
		return ( true == isset( $this->m_intOwnerTypeId ) ) ? ( string ) $this->m_intOwnerTypeId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setBusinessCompanyUserId( $intBusinessCompanyUserId ) {
		$this->set( 'm_intBusinessCompanyUserId', CStrings::strToIntDef( $intBusinessCompanyUserId, NULL, false ) );
	}

	public function getBusinessCompanyUserId() {
		return $this->m_intBusinessCompanyUserId;
	}

	public function sqlBusinessCompanyUserId() {
		return ( true == isset( $this->m_intBusinessCompanyUserId ) ) ? ( string ) $this->m_intBusinessCompanyUserId : 'NULL';
	}

	public function setAccountingCompanyUserId( $intAccountingCompanyUserId ) {
		$this->set( 'm_intAccountingCompanyUserId', CStrings::strToIntDef( $intAccountingCompanyUserId, NULL, false ) );
	}

	public function getAccountingCompanyUserId() {
		return $this->m_intAccountingCompanyUserId;
	}

	public function sqlAccountingCompanyUserId() {
		return ( true == isset( $this->m_intAccountingCompanyUserId ) ) ? ( string ) $this->m_intAccountingCompanyUserId : 'NULL';
	}

	public function setDistributionGlAccountId( $intDistributionGlAccountId ) {
		$this->set( 'm_intDistributionGlAccountId', CStrings::strToIntDef( $intDistributionGlAccountId, NULL, false ) );
	}

	public function getDistributionGlAccountId() {
		return $this->m_intDistributionGlAccountId;
	}

	public function sqlDistributionGlAccountId() {
		return ( true == isset( $this->m_intDistributionGlAccountId ) ) ? ( string ) $this->m_intDistributionGlAccountId : 'NULL';
	}

	public function setContributionGlAccountId( $intContributionGlAccountId ) {
		$this->set( 'm_intContributionGlAccountId', CStrings::strToIntDef( $intContributionGlAccountId, NULL, false ) );
	}

	public function getContributionGlAccountId() {
		return $this->m_intContributionGlAccountId;
	}

	public function sqlContributionGlAccountId() {
		return ( true == isset( $this->m_intContributionGlAccountId ) ) ? ( string ) $this->m_intContributionGlAccountId : 'NULL';
	}

	public function setOwnerName( $strOwnerName ) {
		$this->set( 'm_strOwnerName', CStrings::strTrimDef( $strOwnerName, 100, NULL, true ) );
	}

	public function getOwnerName() {
		return $this->m_strOwnerName;
	}

	public function sqlOwnerName() {
		return ( true == isset( $this->m_strOwnerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerName ) : '\'' . addslashes( $this->m_strOwnerName ) . '\'' ) : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine1 ) : '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine2 ) : '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine3 ) : '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProvince ) : '\'' . addslashes( $this->m_strProvince ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setIsAddressVerified( $boolIsAddressVerified ) {
		$this->set( 'm_boolIsAddressVerified', CStrings::strToBool( $boolIsAddressVerified ) );
	}

	public function getIsAddressVerified() {
		return $this->m_boolIsAddressVerified;
	}

	public function sqlIsAddressVerified() {
		return ( true == isset( $this->m_boolIsAddressVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAddressVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 255, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebsiteUrl ) : '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setSecurityDepositsReducesDraw( $intSecurityDepositsReducesDraw ) {
		$this->set( 'm_intSecurityDepositsReducesDraw', CStrings::strToIntDef( $intSecurityDepositsReducesDraw, NULL, false ) );
	}

	public function getSecurityDepositsReducesDraw() {
		return $this->m_intSecurityDepositsReducesDraw;
	}

	public function sqlSecurityDepositsReducesDraw() {
		return ( true == isset( $this->m_intSecurityDepositsReducesDraw ) ) ? ( string ) $this->m_intSecurityDepositsReducesDraw : '0';
	}

	public function setShouldForwardDistributions( $intShouldForwardDistributions ) {
		$this->set( 'm_intShouldForwardDistributions', CStrings::strToIntDef( $intShouldForwardDistributions, NULL, false ) );
	}

	public function getShouldForwardDistributions() {
		return $this->m_intShouldForwardDistributions;
	}

	public function sqlShouldForwardDistributions() {
		return ( true == isset( $this->m_intShouldForwardDistributions ) ) ? ( string ) $this->m_intShouldForwardDistributions : '1';
	}

	public function setHasOwnership( $intHasOwnership ) {
		$this->set( 'm_intHasOwnership', CStrings::strToIntDef( $intHasOwnership, NULL, false ) );
	}

	public function getHasOwnership() {
		return $this->m_intHasOwnership;
	}

	public function sqlHasOwnership() {
		return ( true == isset( $this->m_intHasOwnership ) ) ? ( string ) $this->m_intHasOwnership : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, owner_type_id, ap_payee_id, business_company_user_id, accounting_company_user_id, distribution_gl_account_id, contribution_gl_account_id, owner_name, street_line1, street_line2, street_line3, city, state_code, province, postal_code, country_code, is_address_verified, phone_number, fax_number, website_url, tax_number_encrypted, security_deposits_reduces_draw, should_forward_distributions, has_ownership, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlOwnerTypeId() . ', ' .
		          $this->sqlApPayeeId() . ', ' .
		          $this->sqlBusinessCompanyUserId() . ', ' .
		          $this->sqlAccountingCompanyUserId() . ', ' .
		          $this->sqlDistributionGlAccountId() . ', ' .
		          $this->sqlContributionGlAccountId() . ', ' .
		          $this->sqlOwnerName() . ', ' .
		          $this->sqlStreetLine1() . ', ' .
		          $this->sqlStreetLine2() . ', ' .
		          $this->sqlStreetLine3() . ', ' .
		          $this->sqlCity() . ', ' .
		          $this->sqlStateCode() . ', ' .
		          $this->sqlProvince() . ', ' .
		          $this->sqlPostalCode() . ', ' .
		          $this->sqlCountryCode() . ', ' .
		          $this->sqlIsAddressVerified() . ', ' .
		          $this->sqlPhoneNumber() . ', ' .
		          $this->sqlFaxNumber() . ', ' .
		          $this->sqlWebsiteUrl() . ', ' .
		          $this->sqlTaxNumberEncrypted() . ', ' .
		          $this->sqlSecurityDepositsReducesDraw() . ', ' .
		          $this->sqlShouldForwardDistributions() . ', ' .
		          $this->sqlHasOwnership() . ', ' .
		          $this->sqlDisabledBy() . ', ' .
		          $this->sqlDisabledOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_type_id = ' . $this->sqlOwnerTypeId(). ',' ; } elseif( true == array_key_exists( 'OwnerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' owner_type_id = ' . $this->sqlOwnerTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_company_user_id = ' . $this->sqlBusinessCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'BusinessCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' business_company_user_id = ' . $this->sqlBusinessCompanyUserId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_company_user_id = ' . $this->sqlAccountingCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'AccountingCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_company_user_id = ' . $this->sqlAccountingCompanyUserId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_gl_account_id = ' . $this->sqlDistributionGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DistributionGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' distribution_gl_account_id = ' . $this->sqlDistributionGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contribution_gl_account_id = ' . $this->sqlContributionGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ContributionGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' contribution_gl_account_id = ' . $this->sqlContributionGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName(). ',' ; } elseif( true == array_key_exists( 'OwnerName', $this->getChangedColumns() ) ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2(). ',' ; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3(). ',' ; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_address_verified = ' . $this->sqlIsAddressVerified(). ',' ; } elseif( true == array_key_exists( 'IsAddressVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_address_verified = ' . $this->sqlIsAddressVerified() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl(). ',' ; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_deposits_reduces_draw = ' . $this->sqlSecurityDepositsReducesDraw(). ',' ; } elseif( true == array_key_exists( 'SecurityDepositsReducesDraw', $this->getChangedColumns() ) ) { $strSql .= ' security_deposits_reduces_draw = ' . $this->sqlSecurityDepositsReducesDraw() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' should_forward_distributions = ' . $this->sqlShouldForwardDistributions(). ',' ; } elseif( true == array_key_exists( 'ShouldForwardDistributions', $this->getChangedColumns() ) ) { $strSql .= ' should_forward_distributions = ' . $this->sqlShouldForwardDistributions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_ownership = ' . $this->sqlHasOwnership(). ',' ; } elseif( true == array_key_exists( 'HasOwnership', $this->getChangedColumns() ) ) { $strSql .= ' has_ownership = ' . $this->sqlHasOwnership() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'owner_type_id' => $this->getOwnerTypeId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'business_company_user_id' => $this->getBusinessCompanyUserId(),
			'accounting_company_user_id' => $this->getAccountingCompanyUserId(),
			'distribution_gl_account_id' => $this->getDistributionGlAccountId(),
			'contribution_gl_account_id' => $this->getContributionGlAccountId(),
			'owner_name' => $this->getOwnerName(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'is_address_verified' => $this->getIsAddressVerified(),
			'phone_number' => $this->getPhoneNumber(),
			'fax_number' => $this->getFaxNumber(),
			'website_url' => $this->getWebsiteUrl(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'security_deposits_reduces_draw' => $this->getSecurityDepositsReducesDraw(),
			'should_forward_distributions' => $this->getShouldForwardDistributions(),
			'has_ownership' => $this->getHasOwnership(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>