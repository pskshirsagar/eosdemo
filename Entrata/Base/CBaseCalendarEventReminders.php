<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventReminders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEventReminders extends CEosPluralBase {

	/**
	 * @return CCalendarEventReminder[]
	 */
	public static function fetchCalendarEventReminders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCalendarEventReminder', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCalendarEventReminder
	 */
	public static function fetchCalendarEventReminder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCalendarEventReminder', $objDatabase );
	}

	public static function fetchCalendarEventReminderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calendar_event_reminders', $objDatabase );
	}

	public static function fetchCalendarEventReminderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminder( sprintf( 'SELECT * FROM calendar_event_reminders WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventRemindersByCid( $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminders( sprintf( 'SELECT * FROM calendar_event_reminders WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventRemindersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminders( sprintf( 'SELECT * FROM calendar_event_reminders WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventRemindersByCalendarEventIdByCid( $intCalendarEventId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminders( sprintf( 'SELECT * FROM calendar_event_reminders WHERE calendar_event_id = %d AND cid = %d', ( int ) $intCalendarEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventRemindersByReminderTypeIdByCid( $intReminderTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminders( sprintf( 'SELECT * FROM calendar_event_reminders WHERE reminder_type_id = %d AND cid = %d', ( int ) $intReminderTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventRemindersByIntervalTypeIdByCid( $intIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventReminders( sprintf( 'SELECT * FROM calendar_event_reminders WHERE interval_type_id = %d AND cid = %d', ( int ) $intIntervalTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>