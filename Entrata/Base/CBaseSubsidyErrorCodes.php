<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyErrorCodes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyErrorCodes extends CEosPluralBase {

	/**
	 * @return CSubsidyErrorCode[]
	 */
	public static function fetchSubsidyErrorCodes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyErrorCode', $objDatabase );
	}

	/**
	 * @return CSubsidyErrorCode
	 */
	public static function fetchSubsidyErrorCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyErrorCode', $objDatabase );
	}

	public static function fetchSubsidyErrorCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_error_codes', $objDatabase );
	}

	public static function fetchSubsidyErrorCodeById( $intId, $objDatabase ) {
		return self::fetchSubsidyErrorCode( sprintf( 'SELECT * FROM subsidy_error_codes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>