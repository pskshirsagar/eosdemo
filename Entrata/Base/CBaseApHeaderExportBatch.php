<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApHeaderExportBatch extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ap_header_export_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledApHeaderExportBatchId;
	protected $m_intApHeaderExportFileFormatTypeId;
	protected $m_strBatchDatetime;
	protected $m_strMessage;
	protected $m_strFileNamePrefix;
	protected $m_strFileNameSuffix;
	protected $m_boolIsSuccess;
	protected $m_intReattemptCount;
	protected $m_strLastAttemptedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSuccess = true;
		$this->m_intReattemptCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_ap_header_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledApHeaderExportBatchId', trim( $arrValues['scheduled_ap_header_export_batch_id'] ) ); elseif( isset( $arrValues['scheduled_ap_header_export_batch_id'] ) ) $this->setScheduledApHeaderExportBatchId( $arrValues['scheduled_ap_header_export_batch_id'] );
		if( isset( $arrValues['ap_header_export_file_format_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderExportFileFormatTypeId', trim( $arrValues['ap_header_export_file_format_type_id'] ) ); elseif( isset( $arrValues['ap_header_export_file_format_type_id'] ) ) $this->setApHeaderExportFileFormatTypeId( $arrValues['ap_header_export_file_format_type_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['message'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['file_name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strFileNamePrefix', trim( stripcslashes( $arrValues['file_name_prefix'] ) ) ); elseif( isset( $arrValues['file_name_prefix'] ) ) $this->setFileNamePrefix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name_prefix'] ) : $arrValues['file_name_prefix'] );
		if( isset( $arrValues['file_name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strFileNameSuffix', trim( stripcslashes( $arrValues['file_name_suffix'] ) ) ); elseif( isset( $arrValues['file_name_suffix'] ) ) $this->setFileNameSuffix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name_suffix'] ) : $arrValues['file_name_suffix'] );
		if( isset( $arrValues['is_success'] ) && $boolDirectSet ) $this->set( 'm_boolIsSuccess', trim( stripcslashes( $arrValues['is_success'] ) ) ); elseif( isset( $arrValues['is_success'] ) ) $this->setIsSuccess( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_success'] ) : $arrValues['is_success'] );
		if( isset( $arrValues['reattempt_count'] ) && $boolDirectSet ) $this->set( 'm_intReattemptCount', trim( $arrValues['reattempt_count'] ) ); elseif( isset( $arrValues['reattempt_count'] ) ) $this->setReattemptCount( $arrValues['reattempt_count'] );
		if( isset( $arrValues['last_attempted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastAttemptedOn', trim( $arrValues['last_attempted_on'] ) ); elseif( isset( $arrValues['last_attempted_on'] ) ) $this->setLastAttemptedOn( $arrValues['last_attempted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledApHeaderExportBatchId( $intScheduledApHeaderExportBatchId ) {
		$this->set( 'm_intScheduledApHeaderExportBatchId', CStrings::strToIntDef( $intScheduledApHeaderExportBatchId, NULL, false ) );
	}

	public function getScheduledApHeaderExportBatchId() {
		return $this->m_intScheduledApHeaderExportBatchId;
	}

	public function sqlScheduledApHeaderExportBatchId() {
		return ( true == isset( $this->m_intScheduledApHeaderExportBatchId ) ) ? ( string ) $this->m_intScheduledApHeaderExportBatchId : 'NULL';
	}

	public function setApHeaderExportFileFormatTypeId( $intApHeaderExportFileFormatTypeId ) {
		$this->set( 'm_intApHeaderExportFileFormatTypeId', CStrings::strToIntDef( $intApHeaderExportFileFormatTypeId, NULL, false ) );
	}

	public function getApHeaderExportFileFormatTypeId() {
		return $this->m_intApHeaderExportFileFormatTypeId;
	}

	public function sqlApHeaderExportFileFormatTypeId() {
		return ( true == isset( $this->m_intApHeaderExportFileFormatTypeId ) ) ? ( string ) $this->m_intApHeaderExportFileFormatTypeId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NULL';
	}

	public function setMessage( $strMessage, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ), $strLocaleCode );
	}

	public function getMessage( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMessage', $strLocaleCode );
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setFileNamePrefix( $strFileNamePrefix ) {
		$this->set( 'm_strFileNamePrefix', CStrings::strTrimDef( $strFileNamePrefix, 100, NULL, true ) );
	}

	public function getFileNamePrefix() {
		return $this->m_strFileNamePrefix;
	}

	public function sqlFileNamePrefix() {
		return ( true == isset( $this->m_strFileNamePrefix ) ) ? '\'' . addslashes( $this->m_strFileNamePrefix ) . '\'' : 'NULL';
	}

	public function setFileNameSuffix( $strFileNameSuffix ) {
		$this->set( 'm_strFileNameSuffix', CStrings::strTrimDef( $strFileNameSuffix, 100, NULL, true ) );
	}

	public function getFileNameSuffix() {
		return $this->m_strFileNameSuffix;
	}

	public function sqlFileNameSuffix() {
		return ( true == isset( $this->m_strFileNameSuffix ) ) ? '\'' . addslashes( $this->m_strFileNameSuffix ) . '\'' : 'NULL';
	}

	public function setIsSuccess( $boolIsSuccess ) {
		$this->set( 'm_boolIsSuccess', CStrings::strToBool( $boolIsSuccess ) );
	}

	public function getIsSuccess() {
		return $this->m_boolIsSuccess;
	}

	public function sqlIsSuccess() {
		return ( true == isset( $this->m_boolIsSuccess ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSuccess ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReattemptCount( $intReattemptCount ) {
		$this->set( 'm_intReattemptCount', CStrings::strToIntDef( $intReattemptCount, NULL, false ) );
	}

	public function getReattemptCount() {
		return $this->m_intReattemptCount;
	}

	public function sqlReattemptCount() {
		return ( true == isset( $this->m_intReattemptCount ) ) ? ( string ) $this->m_intReattemptCount : '0';
	}

	public function setLastAttemptedOn( $strLastAttemptedOn ) {
		$this->set( 'm_strLastAttemptedOn', CStrings::strTrimDef( $strLastAttemptedOn, -1, NULL, true ) );
	}

	public function getLastAttemptedOn() {
		return $this->m_strLastAttemptedOn;
	}

	public function sqlLastAttemptedOn() {
		return ( true == isset( $this->m_strLastAttemptedOn ) ) ? '\'' . $this->m_strLastAttemptedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_ap_header_export_batch_id, ap_header_export_file_format_type_id, batch_datetime, message, file_name_prefix, file_name_suffix, is_success, reattempt_count, last_attempted_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScheduledApHeaderExportBatchId() . ', ' .
						$this->sqlApHeaderExportFileFormatTypeId() . ', ' .
						$this->sqlBatchDatetime() . ', ' .
						$this->sqlMessage() . ', ' .
						$this->sqlFileNamePrefix() . ', ' .
						$this->sqlFileNameSuffix() . ', ' .
						$this->sqlIsSuccess() . ', ' .
						$this->sqlReattemptCount() . ', ' .
						$this->sqlLastAttemptedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_ap_header_export_batch_id = ' . $this->sqlScheduledApHeaderExportBatchId(). ',' ; } elseif( true == array_key_exists( 'ScheduledApHeaderExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_ap_header_export_batch_id = ' . $this->sqlScheduledApHeaderExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_export_file_format_type_id = ' . $this->sqlApHeaderExportFileFormatTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderExportFileFormatTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_export_file_format_type_id = ' . $this->sqlApHeaderExportFileFormatTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime(). ',' ; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage(). ',' ; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix(). ',' ; } elseif( true == array_key_exists( 'FileNamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix(). ',' ; } elseif( true == array_key_exists( 'FileNameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess(). ',' ; } elseif( true == array_key_exists( 'IsSuccess', $this->getChangedColumns() ) ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount(). ',' ; } elseif( true == array_key_exists( 'ReattemptCount', $this->getChangedColumns() ) ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_attempted_on = ' . $this->sqlLastAttemptedOn(). ',' ; } elseif( true == array_key_exists( 'LastAttemptedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_attempted_on = ' . $this->sqlLastAttemptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_ap_header_export_batch_id' => $this->getScheduledApHeaderExportBatchId(),
			'ap_header_export_file_format_type_id' => $this->getApHeaderExportFileFormatTypeId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'message' => $this->getMessage(),
			'file_name_prefix' => $this->getFileNamePrefix(),
			'file_name_suffix' => $this->getFileNameSuffix(),
			'is_success' => $this->getIsSuccess(),
			'reattempt_count' => $this->getReattemptCount(),
			'last_attempted_on' => $this->getLastAttemptedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>