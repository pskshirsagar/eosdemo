<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportMapping extends CEosSingularBase {

	const TABLE_NAME = 'public.import_mappings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intImportSourceId;
	protected $m_intImportTemplateId;
	protected $m_intImportDataTypeId;
	protected $m_intMigrationTokenId;
	protected $m_strSourceKey;
	protected $m_strLocalDataKey;
	protected $m_strChildSourceKey;
	protected $m_strLocalAdditionalKey;
	protected $m_strCustomName;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSourceDetails;
	protected $m_jsonSourceDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['import_source_id'] ) && $boolDirectSet ) $this->set( 'm_intImportSourceId', trim( $arrValues['import_source_id'] ) ); elseif( isset( $arrValues['import_source_id'] ) ) $this->setImportSourceId( $arrValues['import_source_id'] );
		if( isset( $arrValues['import_template_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTemplateId', trim( $arrValues['import_template_id'] ) ); elseif( isset( $arrValues['import_template_id'] ) ) $this->setImportTemplateId( $arrValues['import_template_id'] );
		if( isset( $arrValues['import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportDataTypeId', trim( $arrValues['import_data_type_id'] ) ); elseif( isset( $arrValues['import_data_type_id'] ) ) $this->setImportDataTypeId( $arrValues['import_data_type_id'] );
		if( isset( $arrValues['migration_token_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationTokenId', trim( $arrValues['migration_token_id'] ) ); elseif( isset( $arrValues['migration_token_id'] ) ) $this->setMigrationTokenId( $arrValues['migration_token_id'] );
		if( isset( $arrValues['source_key'] ) && $boolDirectSet ) $this->set( 'm_strSourceKey', trim( stripcslashes( $arrValues['source_key'] ) ) ); elseif( isset( $arrValues['source_key'] ) ) $this->setSourceKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source_key'] ) : $arrValues['source_key'] );
		if( isset( $arrValues['local_data_key'] ) && $boolDirectSet ) $this->set( 'm_strLocalDataKey', trim( stripcslashes( $arrValues['local_data_key'] ) ) ); elseif( isset( $arrValues['local_data_key'] ) ) $this->setLocalDataKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['local_data_key'] ) : $arrValues['local_data_key'] );
		if( isset( $arrValues['child_source_key'] ) && $boolDirectSet ) $this->set( 'm_strChildSourceKey', trim( stripcslashes( $arrValues['child_source_key'] ) ) ); elseif( isset( $arrValues['child_source_key'] ) ) $this->setChildSourceKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['child_source_key'] ) : $arrValues['child_source_key'] );
		if( isset( $arrValues['local_additional_key'] ) && $boolDirectSet ) $this->set( 'm_strLocalAdditionalKey', trim( stripcslashes( $arrValues['local_additional_key'] ) ) ); elseif( isset( $arrValues['local_additional_key'] ) ) $this->setLocalAdditionalKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['local_additional_key'] ) : $arrValues['local_additional_key'] );
		if( isset( $arrValues['custom_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomName', trim( stripcslashes( $arrValues['custom_name'] ) ) ); elseif( isset( $arrValues['custom_name'] ) ) $this->setCustomName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_name'] ) : $arrValues['custom_name'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['source_details'] ) ) $this->set( 'm_strSourceDetails', trim( $arrValues['source_details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setImportSourceId( $intImportSourceId ) {
		$this->set( 'm_intImportSourceId', CStrings::strToIntDef( $intImportSourceId, NULL, false ) );
	}

	public function getImportSourceId() {
		return $this->m_intImportSourceId;
	}

	public function sqlImportSourceId() {
		return ( true == isset( $this->m_intImportSourceId ) ) ? ( string ) $this->m_intImportSourceId : 'NULL';
	}

	public function setImportTemplateId( $intImportTemplateId ) {
		$this->set( 'm_intImportTemplateId', CStrings::strToIntDef( $intImportTemplateId, NULL, false ) );
	}

	public function getImportTemplateId() {
		return $this->m_intImportTemplateId;
	}

	public function sqlImportTemplateId() {
		return ( true == isset( $this->m_intImportTemplateId ) ) ? ( string ) $this->m_intImportTemplateId : 'NULL';
	}

	public function setImportDataTypeId( $intImportDataTypeId ) {
		$this->set( 'm_intImportDataTypeId', CStrings::strToIntDef( $intImportDataTypeId, NULL, false ) );
	}

	public function getImportDataTypeId() {
		return $this->m_intImportDataTypeId;
	}

	public function sqlImportDataTypeId() {
		return ( true == isset( $this->m_intImportDataTypeId ) ) ? ( string ) $this->m_intImportDataTypeId : 'NULL';
	}

	public function setMigrationTokenId( $intMigrationTokenId ) {
		$this->set( 'm_intMigrationTokenId', CStrings::strToIntDef( $intMigrationTokenId, NULL, false ) );
	}

	public function getMigrationTokenId() {
		return $this->m_intMigrationTokenId;
	}

	public function sqlMigrationTokenId() {
		return ( true == isset( $this->m_intMigrationTokenId ) ) ? ( string ) $this->m_intMigrationTokenId : 'NULL';
	}

	public function setSourceKey( $strSourceKey ) {
		$this->set( 'm_strSourceKey', CStrings::strTrimDef( $strSourceKey, 50, NULL, true ) );
	}

	public function getSourceKey() {
		return $this->m_strSourceKey;
	}

	public function sqlSourceKey() {
		return ( true == isset( $this->m_strSourceKey ) ) ? '\'' . addslashes( $this->m_strSourceKey ) . '\'' : 'NULL';
	}

	public function setLocalDataKey( $strLocalDataKey ) {
		$this->set( 'm_strLocalDataKey', CStrings::strTrimDef( $strLocalDataKey, 50, NULL, true ) );
	}

	public function getLocalDataKey() {
		return $this->m_strLocalDataKey;
	}

	public function sqlLocalDataKey() {
		return ( true == isset( $this->m_strLocalDataKey ) ) ? '\'' . addslashes( $this->m_strLocalDataKey ) . '\'' : 'NULL';
	}

	public function setChildSourceKey( $strChildSourceKey ) {
		$this->set( 'm_strChildSourceKey', CStrings::strTrimDef( $strChildSourceKey, 50, NULL, true ) );
	}

	public function getChildSourceKey() {
		return $this->m_strChildSourceKey;
	}

	public function sqlChildSourceKey() {
		return ( true == isset( $this->m_strChildSourceKey ) ) ? '\'' . addslashes( $this->m_strChildSourceKey ) . '\'' : 'NULL';
	}

	public function setLocalAdditionalKey( $strLocalAdditionalKey ) {
		$this->set( 'm_strLocalAdditionalKey', CStrings::strTrimDef( $strLocalAdditionalKey, 1000, NULL, true ) );
	}

	public function getLocalAdditionalKey() {
		return $this->m_strLocalAdditionalKey;
	}

	public function sqlLocalAdditionalKey() {
		return ( true == isset( $this->m_strLocalAdditionalKey ) ) ? '\'' . addslashes( $this->m_strLocalAdditionalKey ) . '\'' : 'NULL';
	}

	public function setCustomName( $strCustomName ) {
		$this->set( 'm_strCustomName', CStrings::strTrimDef( $strCustomName, 50, NULL, true ) );
	}

	public function getCustomName() {
		return $this->m_strCustomName;
	}

	public function sqlCustomName() {
		return ( true == isset( $this->m_strCustomName ) ) ? '\'' . addslashes( $this->m_strCustomName ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSourceDetails( $jsonSourceDetails ) {
		if( true == valObj( $jsonSourceDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonSourceDetails', $jsonSourceDetails );
		} elseif( true == valJsonString( $jsonSourceDetails ) ) {
			$this->set( 'm_jsonSourceDetails', CStrings::strToJson( $jsonSourceDetails ) );
		} else {
			$this->set( 'm_jsonSourceDetails', NULL ); 
		}
		unset( $this->m_strSourceDetails );
	}

	public function getSourceDetails() {
		if( true == isset( $this->m_strSourceDetails ) ) {
			$this->m_jsonSourceDetails = CStrings::strToJson( $this->m_strSourceDetails );
			unset( $this->m_strSourceDetails );
		}
		return $this->m_jsonSourceDetails;
	}

	public function sqlSourceDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSourceDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getSourceDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, import_source_id, import_template_id, import_data_type_id, migration_token_id, source_key, local_data_key, child_source_key, local_additional_key, custom_name, created_by, created_on, source_details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlImportSourceId() . ', ' .
 						$this->sqlImportTemplateId() . ', ' .
 						$this->sqlImportDataTypeId() . ', ' .
 						$this->sqlMigrationTokenId() . ', ' .
 						$this->sqlSourceKey() . ', ' .
 						$this->sqlLocalDataKey() . ', ' .
 						$this->sqlChildSourceKey() . ', ' .
 						$this->sqlLocalAdditionalKey() . ', ' .
 						$this->sqlCustomName() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlSourceDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_source_id = ' . $this->sqlImportSourceId() . ','; } elseif( true == array_key_exists( 'ImportSourceId', $this->getChangedColumns() ) ) { $strSql .= ' import_source_id = ' . $this->sqlImportSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_template_id = ' . $this->sqlImportTemplateId() . ','; } elseif( true == array_key_exists( 'ImportTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' import_template_id = ' . $this->sqlImportTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; } elseif( true == array_key_exists( 'ImportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_token_id = ' . $this->sqlMigrationTokenId() . ','; } elseif( true == array_key_exists( 'MigrationTokenId', $this->getChangedColumns() ) ) { $strSql .= ' migration_token_id = ' . $this->sqlMigrationTokenId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_key = ' . $this->sqlSourceKey() . ','; } elseif( true == array_key_exists( 'SourceKey', $this->getChangedColumns() ) ) { $strSql .= ' source_key = ' . $this->sqlSourceKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' local_data_key = ' . $this->sqlLocalDataKey() . ','; } elseif( true == array_key_exists( 'LocalDataKey', $this->getChangedColumns() ) ) { $strSql .= ' local_data_key = ' . $this->sqlLocalDataKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' child_source_key = ' . $this->sqlChildSourceKey() . ','; } elseif( true == array_key_exists( 'ChildSourceKey', $this->getChangedColumns() ) ) { $strSql .= ' child_source_key = ' . $this->sqlChildSourceKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' local_additional_key = ' . $this->sqlLocalAdditionalKey() . ','; } elseif( true == array_key_exists( 'LocalAdditionalKey', $this->getChangedColumns() ) ) { $strSql .= ' local_additional_key = ' . $this->sqlLocalAdditionalKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_name = ' . $this->sqlCustomName() . ','; } elseif( true == array_key_exists( 'CustomName', $this->getChangedColumns() ) ) { $strSql .= ' custom_name = ' . $this->sqlCustomName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_details = ' . $this->sqlSourceDetails() . ','; } elseif( true == array_key_exists( 'SourceDetails', $this->getChangedColumns() ) ) { $strSql .= ' source_details = ' . $this->sqlSourceDetails() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'import_source_id' => $this->getImportSourceId(),
			'import_template_id' => $this->getImportTemplateId(),
			'import_data_type_id' => $this->getImportDataTypeId(),
			'migration_token_id' => $this->getMigrationTokenId(),
			'source_key' => $this->getSourceKey(),
			'local_data_key' => $this->getLocalDataKey(),
			'child_source_key' => $this->getChildSourceKey(),
			'local_additional_key' => $this->getLocalAdditionalKey(),
			'custom_name' => $this->getCustomName(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'source_details' => $this->getSourceDetails()
		);
	}

}
?>