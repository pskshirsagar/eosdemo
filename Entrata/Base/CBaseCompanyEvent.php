<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEvent extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyEventTypeId;
	protected $m_intFrequencyId;
	protected $m_intClubId;
	protected $m_intCustomerId;
	protected $m_intCompanyMediaFileId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDrivingDirections;
	protected $m_strDateStart;
	protected $m_strDateEnd;
	protected $m_strTimeStart;
	protected $m_strTimeEnd;
	protected $m_strLocation;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strLatitude;
	protected $m_strLongitude;
	protected $m_intIsPublished;
	protected $m_intIsArchived;
	protected $m_intIsGlobal;
	protected $m_intAllowResponses;
	protected $m_intOrderNum;
	protected $m_strRejectionComment;
	protected $m_strCompanyEventTypeOther;
	protected $m_strEventApprovalData;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intIsArchived = '0';
		$this->m_intIsGlobal = '0';
		$this->m_intOrderNum = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEventTypeId', trim( $arrValues['company_event_type_id'] ) ); elseif( isset( $arrValues['company_event_type_id'] ) ) $this->setCompanyEventTypeId( $arrValues['company_event_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['club_id'] ) && $boolDirectSet ) $this->set( 'm_intClubId', trim( $arrValues['club_id'] ) ); elseif( isset( $arrValues['club_id'] ) ) $this->setClubId( $arrValues['club_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['driving_directions'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDrivingDirections', trim( $arrValues['driving_directions'] ) ); elseif( isset( $arrValues['driving_directions'] ) ) $this->setDrivingDirections( $arrValues['driving_directions'] );
		if( isset( $arrValues['date_start'] ) && $boolDirectSet ) $this->set( 'm_strDateStart', trim( $arrValues['date_start'] ) ); elseif( isset( $arrValues['date_start'] ) ) $this->setDateStart( $arrValues['date_start'] );
		if( isset( $arrValues['date_end'] ) && $boolDirectSet ) $this->set( 'm_strDateEnd', trim( $arrValues['date_end'] ) ); elseif( isset( $arrValues['date_end'] ) ) $this->setDateEnd( $arrValues['date_end'] );
		if( isset( $arrValues['time_start'] ) && $boolDirectSet ) $this->set( 'm_strTimeStart', trim( $arrValues['time_start'] ) ); elseif( isset( $arrValues['time_start'] ) ) $this->setTimeStart( $arrValues['time_start'] );
		if( isset( $arrValues['time_end'] ) && $boolDirectSet ) $this->set( 'm_strTimeEnd', trim( $arrValues['time_end'] ) ); elseif( isset( $arrValues['time_end'] ) ) $this->setTimeEnd( $arrValues['time_end'] );
		if( isset( $arrValues['location'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLocation', trim( $arrValues['location'] ) ); elseif( isset( $arrValues['location'] ) ) $this->setLocation( $arrValues['location'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( $arrValues['street_line1'] ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( $arrValues['street_line2'] ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( $arrValues['street_line3'] ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( $arrValues['province'] ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( $arrValues['latitude'] ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( $arrValues['latitude'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( $arrValues['longitude'] ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( $arrValues['longitude'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['is_global'] ) && $boolDirectSet ) $this->set( 'm_intIsGlobal', trim( $arrValues['is_global'] ) ); elseif( isset( $arrValues['is_global'] ) ) $this->setIsGlobal( $arrValues['is_global'] );
		if( isset( $arrValues['allow_responses'] ) && $boolDirectSet ) $this->set( 'm_intAllowResponses', trim( $arrValues['allow_responses'] ) ); elseif( isset( $arrValues['allow_responses'] ) ) $this->setAllowResponses( $arrValues['allow_responses'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['rejection_comment'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRejectionComment', trim( $arrValues['rejection_comment'] ) ); elseif( isset( $arrValues['rejection_comment'] ) ) $this->setRejectionComment( $arrValues['rejection_comment'] );
		if( isset( $arrValues['company_event_type_other'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEventTypeOther', trim( $arrValues['company_event_type_other'] ) ); elseif( isset( $arrValues['company_event_type_other'] ) ) $this->setCompanyEventTypeOther( $arrValues['company_event_type_other'] );
		if( isset( $arrValues['event_approval_data'] ) && $boolDirectSet ) $this->set( 'm_strEventApprovalData', trim( stripcslashes( $arrValues['event_approval_data'] ) ) ); elseif( isset( $arrValues['event_approval_data'] ) ) $this->setEventApprovalData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_approval_data'] ) : $arrValues['event_approval_data'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyEventTypeId( $intCompanyEventTypeId ) {
		$this->set( 'm_intCompanyEventTypeId', CStrings::strToIntDef( $intCompanyEventTypeId, NULL, false ) );
	}

	public function getCompanyEventTypeId() {
		return $this->m_intCompanyEventTypeId;
	}

	public function sqlCompanyEventTypeId() {
		return ( true == isset( $this->m_intCompanyEventTypeId ) ) ? ( string ) $this->m_intCompanyEventTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setClubId( $intClubId ) {
		$this->set( 'm_intClubId', CStrings::strToIntDef( $intClubId, NULL, false ) );
	}

	public function getClubId() {
		return $this->m_intClubId;
	}

	public function sqlClubId() {
		return ( true == isset( $this->m_intClubId ) ) ? ( string ) $this->m_intClubId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDrivingDirections( $strDrivingDirections, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDrivingDirections', CStrings::strTrimDef( $strDrivingDirections, 2000, NULL, true ), $strLocaleCode );
	}

	public function getDrivingDirections( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDrivingDirections', $strLocaleCode );
	}

	public function sqlDrivingDirections() {
		return ( true == isset( $this->m_strDrivingDirections ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDrivingDirections ) : '\'' . addslashes( $this->m_strDrivingDirections ) . '\'' ) : 'NULL';
	}

	public function setDateStart( $strDateStart ) {
		$this->set( 'm_strDateStart', CStrings::strTrimDef( $strDateStart, -1, NULL, true ) );
	}

	public function getDateStart() {
		return $this->m_strDateStart;
	}

	public function sqlDateStart() {
		return ( true == isset( $this->m_strDateStart ) ) ? '\'' . $this->m_strDateStart . '\'' : 'NULL';
	}

	public function setDateEnd( $strDateEnd ) {
		$this->set( 'm_strDateEnd', CStrings::strTrimDef( $strDateEnd, -1, NULL, true ) );
	}

	public function getDateEnd() {
		return $this->m_strDateEnd;
	}

	public function sqlDateEnd() {
		return ( true == isset( $this->m_strDateEnd ) ) ? '\'' . $this->m_strDateEnd . '\'' : 'NULL';
	}

	public function setTimeStart( $strTimeStart ) {
		$this->set( 'm_strTimeStart', CStrings::strTrimDef( $strTimeStart, 50, NULL, true ) );
	}

	public function getTimeStart() {
		return $this->m_strTimeStart;
	}

	public function sqlTimeStart() {
		return ( true == isset( $this->m_strTimeStart ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTimeStart ) : '\'' . addslashes( $this->m_strTimeStart ) . '\'' ) : 'NULL';
	}

	public function setTimeEnd( $strTimeEnd ) {
		$this->set( 'm_strTimeEnd', CStrings::strTrimDef( $strTimeEnd, 50, NULL, true ) );
	}

	public function getTimeEnd() {
		return $this->m_strTimeEnd;
	}

	public function sqlTimeEnd() {
		return ( true == isset( $this->m_strTimeEnd ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTimeEnd ) : '\'' . addslashes( $this->m_strTimeEnd ) . '\'' ) : 'NULL';
	}

	public function setLocation( $strLocation, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLocation', CStrings::strTrimDef( $strLocation, 100, NULL, true ), $strLocaleCode );
	}

	public function getLocation( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLocation', $strLocaleCode );
	}

	public function sqlLocation() {
		return ( true == isset( $this->m_strLocation ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLocation ) : '\'' . addslashes( $this->m_strLocation ) . '\'' ) : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine1 ) : '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine2 ) : '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine3 ) : '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProvince ) : '\'' . addslashes( $this->m_strProvince ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 25, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLatitude ) : '\'' . addslashes( $this->m_strLatitude ) . '\'' ) : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 25, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLongitude ) : '\'' . addslashes( $this->m_strLongitude ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : '0';
	}

	public function setIsGlobal( $intIsGlobal ) {
		$this->set( 'm_intIsGlobal', CStrings::strToIntDef( $intIsGlobal, NULL, false ) );
	}

	public function getIsGlobal() {
		return $this->m_intIsGlobal;
	}

	public function sqlIsGlobal() {
		return ( true == isset( $this->m_intIsGlobal ) ) ? ( string ) $this->m_intIsGlobal : '0';
	}

	public function setAllowResponses( $intAllowResponses ) {
		$this->set( 'm_intAllowResponses', CStrings::strToIntDef( $intAllowResponses, NULL, false ) );
	}

	public function getAllowResponses() {
		return $this->m_intAllowResponses;
	}

	public function sqlAllowResponses() {
		return ( true == isset( $this->m_intAllowResponses ) ) ? ( string ) $this->m_intAllowResponses : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setRejectionComment( $strRejectionComment, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRejectionComment', CStrings::strTrimDef( $strRejectionComment, 500, NULL, true ), $strLocaleCode );
	}

	public function getRejectionComment( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRejectionComment', $strLocaleCode );
	}

	public function sqlRejectionComment() {
		return ( true == isset( $this->m_strRejectionComment ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRejectionComment ) : '\'' . addslashes( $this->m_strRejectionComment ) . '\'' ) : 'NULL';
	}

	public function setCompanyEventTypeOther( $strCompanyEventTypeOther ) {
		$this->set( 'm_strCompanyEventTypeOther', CStrings::strTrimDef( $strCompanyEventTypeOther, 50, NULL, true ) );
	}

	public function getCompanyEventTypeOther() {
		return $this->m_strCompanyEventTypeOther;
	}

	public function sqlCompanyEventTypeOther() {
		return ( true == isset( $this->m_strCompanyEventTypeOther ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyEventTypeOther ) : '\'' . addslashes( $this->m_strCompanyEventTypeOther ) . '\'' ) : 'NULL';
	}

	public function setEventApprovalData( $strEventApprovalData ) {
		$this->set( 'm_strEventApprovalData', CStrings::strTrimDef( $strEventApprovalData, -1, NULL, true ) );
	}

	public function getEventApprovalData() {
		return $this->m_strEventApprovalData;
	}

	public function sqlEventApprovalData() {
		return ( true == isset( $this->m_strEventApprovalData ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEventApprovalData ) : '\'' . addslashes( $this->m_strEventApprovalData ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_event_type_id, frequency_id, club_id, customer_id, company_media_file_id, name, description, driving_directions, date_start, date_end, time_start, time_end, location, street_line1, street_line2, street_line3, city, state_code, province, postal_code, country_code, latitude, longitude, is_published, is_archived, is_global, allow_responses, order_num, rejection_comment, company_event_type_other, event_approval_data, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyEventTypeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlClubId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDrivingDirections() . ', ' .
						$this->sqlDateStart() . ', ' .
						$this->sqlDateEnd() . ', ' .
						$this->sqlTimeStart() . ', ' .
						$this->sqlTimeEnd() . ', ' .
						$this->sqlLocation() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlStreetLine2() . ', ' .
						$this->sqlStreetLine3() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlLatitude() . ', ' .
						$this->sqlLongitude() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsArchived() . ', ' .
						$this->sqlIsGlobal() . ', ' .
						$this->sqlAllowResponses() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlRejectionComment() . ', ' .
						$this->sqlCompanyEventTypeOther() . ', ' .
						$this->sqlEventApprovalData() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_event_type_id = ' . $this->sqlCompanyEventTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_event_type_id = ' . $this->sqlCompanyEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' club_id = ' . $this->sqlClubId(). ',' ; } elseif( true == array_key_exists( 'ClubId', $this->getChangedColumns() ) ) { $strSql .= ' club_id = ' . $this->sqlClubId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections(). ',' ; } elseif( true == array_key_exists( 'DrivingDirections', $this->getChangedColumns() ) ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_start = ' . $this->sqlDateStart(). ',' ; } elseif( true == array_key_exists( 'DateStart', $this->getChangedColumns() ) ) { $strSql .= ' date_start = ' . $this->sqlDateStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_end = ' . $this->sqlDateEnd(). ',' ; } elseif( true == array_key_exists( 'DateEnd', $this->getChangedColumns() ) ) { $strSql .= ' date_end = ' . $this->sqlDateEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_start = ' . $this->sqlTimeStart(). ',' ; } elseif( true == array_key_exists( 'TimeStart', $this->getChangedColumns() ) ) { $strSql .= ' time_start = ' . $this->sqlTimeStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_end = ' . $this->sqlTimeEnd(). ',' ; } elseif( true == array_key_exists( 'TimeEnd', $this->getChangedColumns() ) ) { $strSql .= ' time_end = ' . $this->sqlTimeEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location = ' . $this->sqlLocation(). ',' ; } elseif( true == array_key_exists( 'Location', $this->getChangedColumns() ) ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2(). ',' ; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3(). ',' ; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude(). ',' ; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude(). ',' ; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived(). ',' ; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_global = ' . $this->sqlIsGlobal(). ',' ; } elseif( true == array_key_exists( 'IsGlobal', $this->getChangedColumns() ) ) { $strSql .= ' is_global = ' . $this->sqlIsGlobal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_responses = ' . $this->sqlAllowResponses(). ',' ; } elseif( true == array_key_exists( 'AllowResponses', $this->getChangedColumns() ) ) { $strSql .= ' allow_responses = ' . $this->sqlAllowResponses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_comment = ' . $this->sqlRejectionComment(). ',' ; } elseif( true == array_key_exists( 'RejectionComment', $this->getChangedColumns() ) ) { $strSql .= ' rejection_comment = ' . $this->sqlRejectionComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_event_type_other = ' . $this->sqlCompanyEventTypeOther(). ',' ; } elseif( true == array_key_exists( 'CompanyEventTypeOther', $this->getChangedColumns() ) ) { $strSql .= ' company_event_type_other = ' . $this->sqlCompanyEventTypeOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_approval_data = ' . $this->sqlEventApprovalData(). ',' ; } elseif( true == array_key_exists( 'EventApprovalData', $this->getChangedColumns() ) ) { $strSql .= ' event_approval_data = ' . $this->sqlEventApprovalData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_event_type_id' => $this->getCompanyEventTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'club_id' => $this->getClubId(),
			'customer_id' => $this->getCustomerId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'driving_directions' => $this->getDrivingDirections(),
			'date_start' => $this->getDateStart(),
			'date_end' => $this->getDateEnd(),
			'time_start' => $this->getTimeStart(),
			'time_end' => $this->getTimeEnd(),
			'location' => $this->getLocation(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'latitude' => $this->getLatitude(),
			'longitude' => $this->getLongitude(),
			'is_published' => $this->getIsPublished(),
			'is_archived' => $this->getIsArchived(),
			'is_global' => $this->getIsGlobal(),
			'allow_responses' => $this->getAllowResponses(),
			'order_num' => $this->getOrderNum(),
			'rejection_comment' => $this->getRejectionComment(),
			'company_event_type_other' => $this->getCompanyEventTypeOther(),
			'event_approval_data' => $this->getEventApprovalData(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>