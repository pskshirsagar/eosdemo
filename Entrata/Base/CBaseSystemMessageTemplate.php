<?php

class CBaseSystemMessageTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.system_message_templates';

	protected $m_intId;
	protected $m_strName;
	protected $m_intSystemMessageTypeId;
	protected $m_strPath;
	protected $m_strPreviewImageUri;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['system_message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTypeId', trim( $arrValues['system_message_type_id'] ) ); elseif( isset( $arrValues['system_message_type_id'] ) ) $this->setSystemMessageTypeId( $arrValues['system_message_type_id'] );
		if( isset( $arrValues['path'] ) && $boolDirectSet ) $this->set( 'm_strPath', trim( stripcslashes( $arrValues['path'] ) ) ); elseif( isset( $arrValues['path'] ) ) $this->setPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['path'] ) : $arrValues['path'] );
		if( isset( $arrValues['preview_image_uri'] ) && $boolDirectSet ) $this->set( 'm_strPreviewImageUri', trim( stripcslashes( $arrValues['preview_image_uri'] ) ) ); elseif( isset( $arrValues['preview_image_uri'] ) ) $this->setPreviewImageUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preview_image_uri'] ) : $arrValues['preview_image_uri'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSystemMessageTypeId( $intSystemMessageTypeId ) {
		$this->set( 'm_intSystemMessageTypeId', CStrings::strToIntDef( $intSystemMessageTypeId, NULL, false ) );
	}

	public function getSystemMessageTypeId() {
		return $this->m_intSystemMessageTypeId;
	}

	public function sqlSystemMessageTypeId() {
		return ( true == isset( $this->m_intSystemMessageTypeId ) ) ? ( string ) $this->m_intSystemMessageTypeId : 'NULL';
	}

	public function setPath( $strPath ) {
		$this->set( 'm_strPath', CStrings::strTrimDef( $strPath, 255, NULL, true ) );
	}

	public function getPath() {
		return $this->m_strPath;
	}

	public function sqlPath() {
		return ( true == isset( $this->m_strPath ) ) ? '\'' . addslashes( $this->m_strPath ) . '\'' : 'NULL';
	}

	public function setPreviewImageUri( $strPreviewImageUri ) {
		$this->set( 'm_strPreviewImageUri', CStrings::strTrimDef( $strPreviewImageUri, 255, NULL, true ) );
	}

	public function getPreviewImageUri() {
		return $this->m_strPreviewImageUri;
	}

	public function sqlPreviewImageUri() {
		return ( true == isset( $this->m_strPreviewImageUri ) ) ? '\'' . addslashes( $this->m_strPreviewImageUri ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'system_message_type_id' => $this->getSystemMessageTypeId(),
			'path' => $this->getPath(),
			'preview_image_uri' => $this->getPreviewImageUri(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>