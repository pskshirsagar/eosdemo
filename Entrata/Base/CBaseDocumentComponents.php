<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentComponents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentComponents extends CEosPluralBase {

	/**
	 * @return CDocumentComponent[]
	 */
	public static function fetchDocumentComponents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDocumentComponent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocumentComponent
	 */
	public static function fetchDocumentComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentComponent', $objDatabase );
	}

	public static function fetchDocumentComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_components', $objDatabase );
	}

	public static function fetchDocumentComponentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocumentComponent( sprintf( 'SELECT * FROM document_components WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentComponentsByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentComponents( sprintf( 'SELECT * FROM document_components WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentTemplateIdByCid( $intDocumentTemplateId, $intCid, $objDatabase ) {
		return self::fetchDocumentComponents( sprintf( 'SELECT * FROM document_components WHERE document_template_id = %d AND cid = %d', ( int ) $intDocumentTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentTemplateComponentIdByCid( $intDocumentTemplateComponentId, $intCid, $objDatabase ) {
		return self::fetchDocumentComponents( sprintf( 'SELECT * FROM document_components WHERE document_template_component_id = %d AND cid = %d', ( int ) $intDocumentTemplateComponentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentComponents( sprintf( 'SELECT * FROM document_components WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentComponentIdByCid( $intDocumentComponentId, $intCid, $objDatabase ) {
		return self::fetchDocumentComponents( sprintf( 'SELECT * FROM document_components WHERE document_component_id = %d AND cid = %d', ( int ) $intDocumentComponentId, ( int ) $intCid ), $objDatabase );
	}

}
?>