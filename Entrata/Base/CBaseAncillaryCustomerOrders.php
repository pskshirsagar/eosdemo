<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryCustomerOrders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAncillaryCustomerOrders extends CEosPluralBase {

	/**
	 * @return CAncillaryCustomerOrder[]
	 */
	public static function fetchAncillaryCustomerOrders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAncillaryCustomerOrder', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAncillaryCustomerOrder
	 */
	public static function fetchAncillaryCustomerOrder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAncillaryCustomerOrder', $objDatabase );
	}

	public static function fetchAncillaryCustomerOrderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_customer_orders', $objDatabase );
	}

	public static function fetchAncillaryCustomerOrderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAncillaryCustomerOrder( sprintf( 'SELECT * FROM ancillary_customer_orders WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAncillaryCustomerOrdersByCid( $intCid, $objDatabase ) {
		return self::fetchAncillaryCustomerOrders( sprintf( 'SELECT * FROM ancillary_customer_orders WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAncillaryCustomerOrdersByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchAncillaryCustomerOrders( sprintf( 'SELECT * FROM ancillary_customer_orders WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAncillaryCustomerOrdersByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchAncillaryCustomerOrders( sprintf( 'SELECT * FROM ancillary_customer_orders WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAncillaryCustomerOrdersByAncillaryVendorIdByCid( $intAncillaryVendorId, $intCid, $objDatabase ) {
		return self::fetchAncillaryCustomerOrders( sprintf( 'SELECT * FROM ancillary_customer_orders WHERE ancillary_vendor_id = %d AND cid = %d', ( int ) $intAncillaryVendorId, ( int ) $intCid ), $objDatabase );
	}

}
?>