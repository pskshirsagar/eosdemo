<?php

class CBaseCamPoolTypes extends CEosPluralBase {

	/**
	 * @return CCamPoolType[]
	 */
	public static function fetchCamPoolTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCamPoolType::class, $objDatabase );
	}

	/**
	 * @return CCamPoolType
	 */
	public static function fetchCamPoolType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCamPoolType::class, $objDatabase );
	}

	public static function fetchCamPoolTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cam_pool_types', $objDatabase );
	}

	public static function fetchCamPoolTypeById( $intId, $objDatabase ) {
		return self::fetchCamPoolType( sprintf( 'SELECT * FROM cam_pool_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>