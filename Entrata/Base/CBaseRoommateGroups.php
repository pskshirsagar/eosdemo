<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoommateGroups extends CEosPluralBase {

	/**
	 * @return CRoommateGroup[]
	 */
	public static function fetchRoommateGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRoommateGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRoommateGroup
	 */
	public static function fetchRoommateGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRoommateGroup', $objDatabase );
	}

	public static function fetchRoommateGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roommate_groups', $objDatabase );
	}

	public static function fetchRoommateGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRoommateGroup( sprintf( 'SELECT * FROM roommate_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommateGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchRoommateGroups( sprintf( 'SELECT * FROM roommate_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommateGroupsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchRoommateGroups( sprintf( 'SELECT * FROM roommate_groups WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommateGroupsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchRoommateGroups( sprintf( 'SELECT * FROM roommate_groups WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>