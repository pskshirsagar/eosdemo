<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForm1099BoxTypes
 * Do not add any new functions to this class.
 */

class CBaseForm1099BoxTypes extends CEosPluralBase {

	/**
	 * @return CForm1099BoxType[]
	 */
	public static function fetchForm1099BoxTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CForm1099BoxType', $objDatabase );
	}

	/**
	 * @return CForm1099BoxType
	 */
	public static function fetchForm1099BoxType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CForm1099BoxType', $objDatabase );
	}

	public static function fetchForm1099BoxTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'form_1099_box_types', $objDatabase );
	}

	public static function fetchForm1099BoxTypeById( $intId, $objDatabase ) {
		return self::fetchForm1099BoxType( sprintf( 'SELECT * FROM form_1099_box_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchForm1099BoxTypesByForm1099TypeId( $intForm1099TypeId, $objDatabase ) {
		return self::fetchForm1099BoxTypes( sprintf( 'SELECT * FROM form_1099_box_types WHERE form_1099_type_id = %d', ( int ) $intForm1099TypeId ), $objDatabase );
	}

}
?>