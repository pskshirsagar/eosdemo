<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportArchives
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportArchives extends CEosPluralBase {

	/**
	 * @return CReportArchive[]
	 */
	public static function fetchReportArchives( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CReportArchive', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportArchive
	 */
	public static function fetchReportArchive( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReportArchive', $objDatabase );
	}

	public static function fetchReportArchiveCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_archives', $objDatabase );
	}

	public static function fetchReportArchiveByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportArchive( sprintf( 'SELECT * FROM report_archives WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportArchivesByCid( $intCid, $objDatabase ) {
		return self::fetchReportArchives( sprintf( 'SELECT * FROM report_archives WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportArchivesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchReportArchives( sprintf( 'SELECT * FROM report_archives WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportArchivesByDefaultReportIdByCid( $intDefaultReportId, $intCid, $objDatabase ) {
		return self::fetchReportArchives( sprintf( 'SELECT * FROM report_archives WHERE default_report_id = %d AND cid = %d', ( int ) $intDefaultReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportArchivesByFilterIdByCid( $intFilterId, $intCid, $objDatabase ) {
		return self::fetchReportArchives( sprintf( 'SELECT * FROM report_archives WHERE filter_id = %d AND cid = %d', ( int ) $intFilterId, ( int ) $intCid ), $objDatabase );
	}

}
?>