<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryVendorsRegions
 * Do not add any new functions to this class.
 */

class CBaseAncillaryVendorsRegions extends CEosPluralBase {

	/**
	 * @return CAncillaryVendorsRegion[]
	 */
	public static function fetchAncillaryVendorsRegions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAncillaryVendorsRegion', $objDatabase );
	}

	/**
	 * @return CAncillaryVendorsRegion
	 */
	public static function fetchAncillaryVendorsRegion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAncillaryVendorsRegion', $objDatabase );
	}

	public static function fetchAncillaryVendorsRegionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_vendors_regions', $objDatabase );
	}

	public static function fetchAncillaryVendorsRegionById( $intId, $objDatabase ) {
		return self::fetchAncillaryVendorsRegion( sprintf( 'SELECT * FROM ancillary_vendors_regions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAncillaryVendorsRegionsByAncillaryVendorId( $intAncillaryVendorId, $objDatabase ) {
		return self::fetchAncillaryVendorsRegions( sprintf( 'SELECT * FROM ancillary_vendors_regions WHERE ancillary_vendor_id = %d', ( int ) $intAncillaryVendorId ), $objDatabase );
	}

}
?>