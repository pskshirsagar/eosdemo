<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMediaFile extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_media_files';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyMediaFolderId;
	protected $m_intMediaFileTypeId;
	protected $m_intMediaTypeId;
	protected $m_intMimeTypeId;
	protected $m_intParentCompanyMediaFileId;
	protected $m_intMarketingMediaId;
	protected $m_intIsHidden;
	protected $m_boolIsCompressed;
	protected $m_strMediaDetails;
	protected $m_strMediaCaption;
	protected $m_strMediaDescription;
	protected $m_strMediaData;
	protected $m_strMediaWidth;
	protected $m_strMediaHeight;
	protected $m_strMediaAlt;
	protected $m_strFullsizeUri;
	protected $m_strThumbnailUri;
	protected $m_strExternalUri;
	protected $m_strFileName;
	protected $m_intFileSize;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intIsHidden = '0';
		$this->m_boolIsCompressed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_media_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFolderId', trim( $arrValues['company_media_folder_id'] ) ); elseif( isset( $arrValues['company_media_folder_id'] ) ) $this->setCompanyMediaFolderId( $arrValues['company_media_folder_id'] );
		if( isset( $arrValues['media_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMediaFileTypeId', trim( $arrValues['media_file_type_id'] ) ); elseif( isset( $arrValues['media_file_type_id'] ) ) $this->setMediaFileTypeId( $arrValues['media_file_type_id'] );
		if( isset( $arrValues['media_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMediaTypeId', trim( $arrValues['media_type_id'] ) ); elseif( isset( $arrValues['media_type_id'] ) ) $this->setMediaTypeId( $arrValues['media_type_id'] );
		if( isset( $arrValues['mime_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMimeTypeId', trim( $arrValues['mime_type_id'] ) ); elseif( isset( $arrValues['mime_type_id'] ) ) $this->setMimeTypeId( $arrValues['mime_type_id'] );
		if( isset( $arrValues['parent_company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intParentCompanyMediaFileId', trim( $arrValues['parent_company_media_file_id'] ) ); elseif( isset( $arrValues['parent_company_media_file_id'] ) ) $this->setParentCompanyMediaFileId( $arrValues['parent_company_media_file_id'] );
		if( isset( $arrValues['marketing_media_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingMediaId', trim( $arrValues['marketing_media_id'] ) ); elseif( isset( $arrValues['marketing_media_id'] ) ) $this->setMarketingMediaId( $arrValues['marketing_media_id'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_intIsHidden', trim( $arrValues['is_hidden'] ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( $arrValues['is_hidden'] );
		if( isset( $arrValues['is_compressed'] ) && $boolDirectSet ) $this->set( 'm_boolIsCompressed', trim( stripcslashes( $arrValues['is_compressed'] ) ) ); elseif( isset( $arrValues['is_compressed'] ) ) $this->setIsCompressed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_compressed'] ) : $arrValues['is_compressed'] );
		if( isset( $arrValues['media_details'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaDetails', trim( stripcslashes( $arrValues['media_details'] ) ) ); elseif( isset( $arrValues['media_details'] ) ) $this->setMediaDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_details'] ) : $arrValues['media_details'] );
		if( isset( $arrValues['media_caption'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaCaption', trim( stripcslashes( $arrValues['media_caption'] ) ) ); elseif( isset( $arrValues['media_caption'] ) ) $this->setMediaCaption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_caption'] ) : $arrValues['media_caption'] );
		if( isset( $arrValues['media_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaDescription', trim( stripcslashes( $arrValues['media_description'] ) ) ); elseif( isset( $arrValues['media_description'] ) ) $this->setMediaDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_description'] ) : $arrValues['media_description'] );
		if( isset( $arrValues['media_data'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaData', trim( stripcslashes( $arrValues['media_data'] ) ) ); elseif( isset( $arrValues['media_data'] ) ) $this->setMediaData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_data'] ) : $arrValues['media_data'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_strMediaWidth', trim( stripcslashes( $arrValues['media_width'] ) ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_width'] ) : $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_strMediaHeight', trim( stripcslashes( $arrValues['media_height'] ) ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_height'] ) : $arrValues['media_height'] );
		if( isset( $arrValues['media_alt'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaAlt', trim( stripcslashes( $arrValues['media_alt'] ) ) ); elseif( isset( $arrValues['media_alt'] ) ) $this->setMediaAlt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_alt'] ) : $arrValues['media_alt'] );
		if( isset( $arrValues['fullsize_uri'] ) && $boolDirectSet ) $this->set( 'm_strFullsizeUri', trim( stripcslashes( $arrValues['fullsize_uri'] ) ) ); elseif( isset( $arrValues['fullsize_uri'] ) ) $this->setFullsizeUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fullsize_uri'] ) : $arrValues['fullsize_uri'] );
		if( isset( $arrValues['thumbnail_uri'] ) && $boolDirectSet ) $this->set( 'm_strThumbnailUri', trim( stripcslashes( $arrValues['thumbnail_uri'] ) ) ); elseif( isset( $arrValues['thumbnail_uri'] ) ) $this->setThumbnailUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['thumbnail_uri'] ) : $arrValues['thumbnail_uri'] );
		if( isset( $arrValues['external_uri'] ) && $boolDirectSet ) $this->set( 'm_strExternalUri', trim( stripcslashes( $arrValues['external_uri'] ) ) ); elseif( isset( $arrValues['external_uri'] ) ) $this->setExternalUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_uri'] ) : $arrValues['external_uri'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_intFileSize', trim( $arrValues['file_size'] ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( $arrValues['file_size'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setCompanyMediaFolderId( $intCompanyMediaFolderId ) {
		$this->set( 'm_intCompanyMediaFolderId', CStrings::strToIntDef( $intCompanyMediaFolderId, NULL, false ) );
	}

	public function getCompanyMediaFolderId() {
		return $this->m_intCompanyMediaFolderId;
	}

	public function sqlCompanyMediaFolderId() {
		return ( true == isset( $this->m_intCompanyMediaFolderId ) ) ? ( string ) $this->m_intCompanyMediaFolderId : 'NULL';
	}

	public function setMediaFileTypeId( $intMediaFileTypeId ) {
		$this->set( 'm_intMediaFileTypeId', CStrings::strToIntDef( $intMediaFileTypeId, NULL, false ) );
	}

	public function getMediaFileTypeId() {
		return $this->m_intMediaFileTypeId;
	}

	public function sqlMediaFileTypeId() {
		return ( true == isset( $this->m_intMediaFileTypeId ) ) ? ( string ) $this->m_intMediaFileTypeId : 'NULL';
	}

	public function setMediaTypeId( $intMediaTypeId ) {
		$this->set( 'm_intMediaTypeId', CStrings::strToIntDef( $intMediaTypeId, NULL, false ) );
	}

	public function getMediaTypeId() {
		return $this->m_intMediaTypeId;
	}

	public function sqlMediaTypeId() {
		return ( true == isset( $this->m_intMediaTypeId ) ) ? ( string ) $this->m_intMediaTypeId : 'NULL';
	}

	public function setMimeTypeId( $intMimeTypeId ) {
		$this->set( 'm_intMimeTypeId', CStrings::strToIntDef( $intMimeTypeId, NULL, false ) );
	}

	public function getMimeTypeId() {
		return $this->m_intMimeTypeId;
	}

	public function sqlMimeTypeId() {
		return ( true == isset( $this->m_intMimeTypeId ) ) ? ( string ) $this->m_intMimeTypeId : 'NULL';
	}

	public function setParentCompanyMediaFileId( $intParentCompanyMediaFileId ) {
		$this->set( 'm_intParentCompanyMediaFileId', CStrings::strToIntDef( $intParentCompanyMediaFileId, NULL, false ) );
	}

	public function getParentCompanyMediaFileId() {
		return $this->m_intParentCompanyMediaFileId;
	}

	public function sqlParentCompanyMediaFileId() {
		return ( true == isset( $this->m_intParentCompanyMediaFileId ) ) ? ( string ) $this->m_intParentCompanyMediaFileId : 'NULL';
	}

	public function setMarketingMediaId( $intMarketingMediaId ) {
		$this->set( 'm_intMarketingMediaId', CStrings::strToIntDef( $intMarketingMediaId, NULL, false ) );
	}

	public function getMarketingMediaId() {
		return $this->m_intMarketingMediaId;
	}

	public function sqlMarketingMediaId() {
		return ( true == isset( $this->m_intMarketingMediaId ) ) ? ( string ) $this->m_intMarketingMediaId : 'NULL';
	}

	public function setIsHidden( $intIsHidden ) {
		$this->set( 'm_intIsHidden', CStrings::strToIntDef( $intIsHidden, NULL, false ) );
	}

	public function getIsHidden() {
		return $this->m_intIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_intIsHidden ) ) ? ( string ) $this->m_intIsHidden : '0';
	}

	public function setIsCompressed( $boolIsCompressed ) {
		$this->set( 'm_boolIsCompressed', CStrings::strToBool( $boolIsCompressed ) );
	}

	public function getIsCompressed() {
		return $this->m_boolIsCompressed;
	}

	public function sqlIsCompressed() {
		return ( true == isset( $this->m_boolIsCompressed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCompressed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMediaDetails( $strMediaDetails, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaDetails', CStrings::strTrimDef( $strMediaDetails, -1, NULL, true ), $strLocaleCode );
	}

	public function getMediaDetails( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaDetails', $strLocaleCode );
	}

	public function sqlMediaDetails() {
		return ( true == isset( $this->m_strMediaDetails ) ) ? '\'' . addslashes( $this->m_strMediaDetails ) . '\'' : 'NULL';
	}

	public function setMediaCaption( $strMediaCaption, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaCaption', CStrings::strTrimDef( $strMediaCaption, 240, NULL, true ), $strLocaleCode );
	}

	public function getMediaCaption( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaCaption', $strLocaleCode );
	}

	public function sqlMediaCaption() {
		return ( true == isset( $this->m_strMediaCaption ) ) ? '\'' . addslashes( $this->m_strMediaCaption ) . '\'' : 'NULL';
	}

	public function setMediaDescription( $strMediaDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaDescription', CStrings::strTrimDef( $strMediaDescription, 1000, NULL, true ), $strLocaleCode );
	}

	public function getMediaDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaDescription', $strLocaleCode );
	}

	public function sqlMediaDescription() {
		return ( true == isset( $this->m_strMediaDescription ) ) ? '\'' . addslashes( $this->m_strMediaDescription ) . '\'' : 'NULL';
	}

	public function setMediaData( $strMediaData, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaData', CStrings::strTrimDef( $strMediaData, -1, NULL, true ), $strLocaleCode );
	}

	public function getMediaData( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaData', $strLocaleCode );
	}

	public function sqlMediaData() {
		return ( true == isset( $this->m_strMediaData ) ) ? '\'' . addslashes( $this->m_strMediaData ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $strMediaWidth ) {
		$this->set( 'm_strMediaWidth', CStrings::strTrimDef( $strMediaWidth, 20, NULL, true ) );
	}

	public function getMediaWidth() {
		return $this->m_strMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_strMediaWidth ) ) ? '\'' . addslashes( $this->m_strMediaWidth ) . '\'' : 'NULL';
	}

	public function setMediaHeight( $strMediaHeight ) {
		$this->set( 'm_strMediaHeight', CStrings::strTrimDef( $strMediaHeight, 20, NULL, true ) );
	}

	public function getMediaHeight() {
		return $this->m_strMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_strMediaHeight ) ) ? '\'' . addslashes( $this->m_strMediaHeight ) . '\'' : 'NULL';
	}

	public function setMediaAlt( $strMediaAlt, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaAlt', CStrings::strTrimDef( $strMediaAlt, 240, NULL, true ), $strLocaleCode );
	}

	public function getMediaAlt( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaAlt', $strLocaleCode );
	}

	public function sqlMediaAlt() {
		return ( true == isset( $this->m_strMediaAlt ) ) ? '\'' . addslashes( $this->m_strMediaAlt ) . '\'' : 'NULL';
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->set( 'm_strFullsizeUri', CStrings::strTrimDef( $strFullsizeUri, 4096, NULL, true ) );
	}

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	public function sqlFullsizeUri() {
		return ( true == isset( $this->m_strFullsizeUri ) ) ? '\'' . addslashes( $this->m_strFullsizeUri ) . '\'' : 'NULL';
	}

	public function setThumbnailUri( $strThumbnailUri ) {
		$this->set( 'm_strThumbnailUri', CStrings::strTrimDef( $strThumbnailUri, 4096, NULL, true ) );
	}

	public function getThumbnailUri() {
		return $this->m_strThumbnailUri;
	}

	public function sqlThumbnailUri() {
		return ( true == isset( $this->m_strThumbnailUri ) ) ? '\'' . addslashes( $this->m_strThumbnailUri ) . '\'' : 'NULL';
	}

	public function setExternalUri( $strExternalUri ) {
		$this->set( 'm_strExternalUri', CStrings::strTrimDef( $strExternalUri, 4096, NULL, true ) );
	}

	public function getExternalUri() {
		return $this->m_strExternalUri;
	}

	public function sqlExternalUri() {
		return ( true == isset( $this->m_strExternalUri ) ) ? '\'' . addslashes( $this->m_strExternalUri ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFileSize( $intFileSize ) {
		$this->set( 'm_intFileSize', CStrings::strToIntDef( $intFileSize, NULL, false ) );
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_intFileSize ) ) ? ( string ) $this->m_intFileSize : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_media_folder_id, media_file_type_id, media_type_id, mime_type_id, parent_company_media_file_id, marketing_media_id, is_hidden, is_compressed, media_details, media_caption, media_description, media_data, media_width, media_height, media_alt, fullsize_uri, thumbnail_uri, external_uri, file_name, file_size, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyMediaFolderId() . ', ' .
						$this->sqlMediaFileTypeId() . ', ' .
						$this->sqlMediaTypeId() . ', ' .
						$this->sqlMimeTypeId() . ', ' .
						$this->sqlParentCompanyMediaFileId() . ', ' .
						$this->sqlMarketingMediaId() . ', ' .
						$this->sqlIsHidden() . ', ' .
						$this->sqlIsCompressed() . ', ' .
						$this->sqlMediaDetails() . ', ' .
						$this->sqlMediaCaption() . ', ' .
						$this->sqlMediaDescription() . ', ' .
						$this->sqlMediaData() . ', ' .
						$this->sqlMediaWidth() . ', ' .
						$this->sqlMediaHeight() . ', ' .
						$this->sqlMediaAlt() . ', ' .
						$this->sqlFullsizeUri() . ', ' .
						$this->sqlThumbnailUri() . ', ' .
						$this->sqlExternalUri() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFileSize() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_folder_id = ' . $this->sqlCompanyMediaFolderId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFolderId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_folder_id = ' . $this->sqlCompanyMediaFolderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_file_type_id = ' . $this->sqlMediaFileTypeId(). ',' ; } elseif( true == array_key_exists( 'MediaFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' media_file_type_id = ' . $this->sqlMediaFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_type_id = ' . $this->sqlMediaTypeId(). ',' ; } elseif( true == array_key_exists( 'MediaTypeId', $this->getChangedColumns() ) ) { $strSql .= ' media_type_id = ' . $this->sqlMediaTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mime_type_id = ' . $this->sqlMimeTypeId(). ',' ; } elseif( true == array_key_exists( 'MimeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' mime_type_id = ' . $this->sqlMimeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_company_media_file_id = ' . $this->sqlParentCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'ParentCompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' parent_company_media_file_id = ' . $this->sqlParentCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_media_id = ' . $this->sqlMarketingMediaId(). ',' ; } elseif( true == array_key_exists( 'MarketingMediaId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_media_id = ' . $this->sqlMarketingMediaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden(). ',' ; } elseif( true == array_key_exists( 'IsHidden', $this->getChangedColumns() ) ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_compressed = ' . $this->sqlIsCompressed(). ',' ; } elseif( true == array_key_exists( 'IsCompressed', $this->getChangedColumns() ) ) { $strSql .= ' is_compressed = ' . $this->sqlIsCompressed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_details = ' . $this->sqlMediaDetails(). ',' ; } elseif( true == array_key_exists( 'MediaDetails', $this->getChangedColumns() ) ) { $strSql .= ' media_details = ' . $this->sqlMediaDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption(). ',' ; } elseif( true == array_key_exists( 'MediaCaption', $this->getChangedColumns() ) ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription(). ',' ; } elseif( true == array_key_exists( 'MediaDescription', $this->getChangedColumns() ) ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_data = ' . $this->sqlMediaData(). ',' ; } elseif( true == array_key_exists( 'MediaData', $this->getChangedColumns() ) ) { $strSql .= ' media_data = ' . $this->sqlMediaData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth(). ',' ; } elseif( true == array_key_exists( 'MediaWidth', $this->getChangedColumns() ) ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight(). ',' ; } elseif( true == array_key_exists( 'MediaHeight', $this->getChangedColumns() ) ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt(). ',' ; } elseif( true == array_key_exists( 'MediaAlt', $this->getChangedColumns() ) ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fullsize_uri = ' . $this->sqlFullsizeUri(). ',' ; } elseif( true == array_key_exists( 'FullsizeUri', $this->getChangedColumns() ) ) { $strSql .= ' fullsize_uri = ' . $this->sqlFullsizeUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thumbnail_uri = ' . $this->sqlThumbnailUri(). ',' ; } elseif( true == array_key_exists( 'ThumbnailUri', $this->getChangedColumns() ) ) { $strSql .= ' thumbnail_uri = ' . $this->sqlThumbnailUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_uri = ' . $this->sqlExternalUri(). ',' ; } elseif( true == array_key_exists( 'ExternalUri', $this->getChangedColumns() ) ) { $strSql .= ' external_uri = ' . $this->sqlExternalUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize(). ',' ; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_media_folder_id' => $this->getCompanyMediaFolderId(),
			'media_file_type_id' => $this->getMediaFileTypeId(),
			'media_type_id' => $this->getMediaTypeId(),
			'mime_type_id' => $this->getMimeTypeId(),
			'parent_company_media_file_id' => $this->getParentCompanyMediaFileId(),
			'marketing_media_id' => $this->getMarketingMediaId(),
			'is_hidden' => $this->getIsHidden(),
			'is_compressed' => $this->getIsCompressed(),
			'media_details' => $this->getMediaDetails(),
			'media_caption' => $this->getMediaCaption(),
			'media_description' => $this->getMediaDescription(),
			'media_data' => $this->getMediaData(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'media_alt' => $this->getMediaAlt(),
			'fullsize_uri' => $this->getFullsizeUri(),
			'thumbnail_uri' => $this->getThumbnailUri(),
			'external_uri' => $this->getExternalUri(),
			'file_name' => $this->getFileName(),
			'file_size' => $this->getFileSize(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>