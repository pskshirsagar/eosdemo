<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentTypeLibraries
 * Do not add any new functions to this class.
 */

class CBaseDocumentTypeLibraries extends CEosPluralBase {

	/**
	 * @return CDocumentTypeLibrary[]
	 */
	public static function fetchDocumentTypeLibraries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTypeLibrary', $objDatabase );
	}

	/**
	 * @return CDocumentTypeLibrary
	 */
	public static function fetchDocumentTypeLibrary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTypeLibrary', $objDatabase );
	}

	public static function fetchDocumentTypeLibraryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_type_libraries', $objDatabase );
	}

	public static function fetchDocumentTypeLibraryById( $intId, $objDatabase ) {
		return self::fetchDocumentTypeLibrary( sprintf( 'SELECT * FROM document_type_libraries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTypeLibrariesByDocumentTypeId( $intDocumentTypeId, $objDatabase ) {
		return self::fetchDocumentTypeLibraries( sprintf( 'SELECT * FROM document_type_libraries WHERE document_type_id = %d', ( int ) $intDocumentTypeId ), $objDatabase );
	}

	public static function fetchDocumentTypeLibrariesByDocumentSubTypeId( $intDocumentSubTypeId, $objDatabase ) {
		return self::fetchDocumentTypeLibraries( sprintf( 'SELECT * FROM document_type_libraries WHERE document_sub_type_id = %d', ( int ) $intDocumentSubTypeId ), $objDatabase );
	}

	public static function fetchDocumentTypeLibrariesByDocumentLibraryId( $intDocumentLibraryId, $objDatabase ) {
		return self::fetchDocumentTypeLibraries( sprintf( 'SELECT * FROM document_type_libraries WHERE document_library_id = %d', ( int ) $intDocumentLibraryId ), $objDatabase );
	}

}
?>