<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjects
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjects extends CEosPluralBase {

	/**
	 * @return CJobProject[]
	 */
	public static function fetchJobProjects( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobProject', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobProject
	 */
	public static function fetchJobProject( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobProject', $objDatabase );
	}

	public static function fetchJobProjectCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_projects', $objDatabase );
	}

	public static function fetchJobProjectByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobProject( sprintf( 'SELECT * FROM job_projects WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectsByCid( $intCid, $objDatabase ) {
		return self::fetchJobProjects( sprintf( 'SELECT * FROM job_projects WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobProjects( sprintf( 'SELECT * FROM job_projects WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchJobProjects( sprintf( 'SELECT * FROM job_projects WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectsByAmenityJobProjectTimingIdByCid( $intAmenityJobProjectTimingId, $intCid, $objDatabase ) {
		return self::fetchJobProjects( sprintf( 'SELECT * FROM job_projects WHERE amenity_job_project_timing_id = %d AND cid = %d', ( int ) $intAmenityJobProjectTimingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectsByMaintenanceJobProjectTimingIdByCid( $intMaintenanceJobProjectTimingId, $intCid, $objDatabase ) {
		return self::fetchJobProjects( sprintf( 'SELECT * FROM job_projects WHERE maintenance_job_project_timing_id = %d AND cid = %d', ( int ) $intMaintenanceJobProjectTimingId, ( int ) $intCid ), $objDatabase );
	}

}
?>