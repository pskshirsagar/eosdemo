<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseIntervalResponsibilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseIntervalResponsibilities extends CEosPluralBase {

	/**
	 * @return CLeaseIntervalResponsibility[]
	 */
	public static function fetchLeaseIntervalResponsibilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseIntervalResponsibility', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseIntervalResponsibility
	 */
	public static function fetchLeaseIntervalResponsibility( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseIntervalResponsibility', $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_interval_responsibilities', $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseIntervalResponsibility( sprintf( 'SELECT * FROM lease_interval_responsibilities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseIntervalResponsibilities( sprintf( 'SELECT * FROM lease_interval_responsibilities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilitiesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseIntervalResponsibilities( sprintf( 'SELECT * FROM lease_interval_responsibilities WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilitiesByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchLeaseIntervalResponsibilities( sprintf( 'SELECT * FROM lease_interval_responsibilities WHERE lease_interval_id = %d AND cid = %d', ( int ) $intLeaseIntervalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseIntervalResponsibilitiesByPropertyResponsibilityIdByCid( $intPropertyResponsibilityId, $intCid, $objDatabase ) {
		return self::fetchLeaseIntervalResponsibilities( sprintf( 'SELECT * FROM lease_interval_responsibilities WHERE property_responsibility_id = %d AND cid = %d', ( int ) $intPropertyResponsibilityId, ( int ) $intCid ), $objDatabase );
	}

}
?>