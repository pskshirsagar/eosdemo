<?php

class CBaseAncillaryVendor extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ancillary_vendors';

	protected $m_intId;
	protected $m_intAncillaryTypeId;
	protected $m_strName;
	protected $m_strUrl;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strPromotionalDescription;
	protected $m_strPhoneNumber;
	protected $m_strLearnMoreLink;
	protected $m_strEflLink;
	protected $m_strTerm;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intAncillaryTypeId = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ancillary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAncillaryTypeId', trim( $arrValues['ancillary_type_id'] ) ); elseif( isset( $arrValues['ancillary_type_id'] ) ) $this->setAncillaryTypeId( $arrValues['ancillary_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( stripcslashes( $arrValues['username_encrypted'] ) ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username_encrypted'] ) : $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['promotional_description'] ) && $boolDirectSet ) $this->set( 'm_strPromotionalDescription', trim( stripcslashes( $arrValues['promotional_description'] ) ) ); elseif( isset( $arrValues['promotional_description'] ) ) $this->setPromotionalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['promotional_description'] ) : $arrValues['promotional_description'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['learn_more_link'] ) && $boolDirectSet ) $this->set( 'm_strLearnMoreLink', trim( stripcslashes( $arrValues['learn_more_link'] ) ) ); elseif( isset( $arrValues['learn_more_link'] ) ) $this->setLearnMoreLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['learn_more_link'] ) : $arrValues['learn_more_link'] );
		if( isset( $arrValues['efl_link'] ) && $boolDirectSet ) $this->set( 'm_strEflLink', trim( stripcslashes( $arrValues['efl_link'] ) ) ); elseif( isset( $arrValues['efl_link'] ) ) $this->setEflLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['efl_link'] ) : $arrValues['efl_link'] );
		if( isset( $arrValues['term'] ) && $boolDirectSet ) $this->set( 'm_strTerm', trim( stripcslashes( $arrValues['term'] ) ) ); elseif( isset( $arrValues['term'] ) ) $this->setTerm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['term'] ) : $arrValues['term'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAncillaryTypeId( $intAncillaryTypeId ) {
		$this->set( 'm_intAncillaryTypeId', CStrings::strToIntDef( $intAncillaryTypeId, NULL, false ) );
	}

	public function getAncillaryTypeId() {
		return $this->m_intAncillaryTypeId;
	}

	public function sqlAncillaryTypeId() {
		return ( true == isset( $this->m_intAncillaryTypeId ) ) ? ( string ) $this->m_intAncillaryTypeId : '0';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 100, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, 240, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setPromotionalDescription( $strPromotionalDescription ) {
		$this->set( 'm_strPromotionalDescription', CStrings::strTrimDef( $strPromotionalDescription, 1000, NULL, true ) );
	}

	public function getPromotionalDescription() {
		return $this->m_strPromotionalDescription;
	}

	public function sqlPromotionalDescription() {
		return ( true == isset( $this->m_strPromotionalDescription ) ) ? '\'' . addslashes( $this->m_strPromotionalDescription ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setLearnMoreLink( $strLearnMoreLink ) {
		$this->set( 'm_strLearnMoreLink', CStrings::strTrimDef( $strLearnMoreLink, 100, NULL, true ) );
	}

	public function getLearnMoreLink() {
		return $this->m_strLearnMoreLink;
	}

	public function sqlLearnMoreLink() {
		return ( true == isset( $this->m_strLearnMoreLink ) ) ? '\'' . addslashes( $this->m_strLearnMoreLink ) . '\'' : 'NULL';
	}

	public function setEflLink( $strEflLink ) {
		$this->set( 'm_strEflLink', CStrings::strTrimDef( $strEflLink, 100, NULL, true ) );
	}

	public function getEflLink() {
		return $this->m_strEflLink;
	}

	public function sqlEflLink() {
		return ( true == isset( $this->m_strEflLink ) ) ? '\'' . addslashes( $this->m_strEflLink ) . '\'' : 'NULL';
	}

	public function setTerm( $strTerm ) {
		$this->set( 'm_strTerm', CStrings::strTrimDef( $strTerm, -1, NULL, true ) );
	}

	public function getTerm() {
		return $this->m_strTerm;
	}

	public function sqlTerm() {
		return ( true == isset( $this->m_strTerm ) ) ? '\'' . addslashes( $this->m_strTerm ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ancillary_type_id, name, url, username_encrypted, password_encrypted, promotional_description, phone_number, learn_more_link, efl_link, term, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAncillaryTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlUsernameEncrypted() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlPromotionalDescription() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlLearnMoreLink() . ', ' .
						$this->sqlEflLink() . ', ' .
						$this->sqlTerm() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ancillary_type_id = ' . $this->sqlAncillaryTypeId(). ',' ; } elseif( true == array_key_exists( 'AncillaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ancillary_type_id = ' . $this->sqlAncillaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promotional_description = ' . $this->sqlPromotionalDescription(). ',' ; } elseif( true == array_key_exists( 'PromotionalDescription', $this->getChangedColumns() ) ) { $strSql .= ' promotional_description = ' . $this->sqlPromotionalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' learn_more_link = ' . $this->sqlLearnMoreLink(). ',' ; } elseif( true == array_key_exists( 'LearnMoreLink', $this->getChangedColumns() ) ) { $strSql .= ' learn_more_link = ' . $this->sqlLearnMoreLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' efl_link = ' . $this->sqlEflLink(). ',' ; } elseif( true == array_key_exists( 'EflLink', $this->getChangedColumns() ) ) { $strSql .= ' efl_link = ' . $this->sqlEflLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' term = ' . $this->sqlTerm(). ',' ; } elseif( true == array_key_exists( 'Term', $this->getChangedColumns() ) ) { $strSql .= ' term = ' . $this->sqlTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ancillary_type_id' => $this->getAncillaryTypeId(),
			'name' => $this->getName(),
			'url' => $this->getUrl(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'promotional_description' => $this->getPromotionalDescription(),
			'phone_number' => $this->getPhoneNumber(),
			'learn_more_link' => $this->getLearnMoreLink(),
			'efl_link' => $this->getEflLink(),
			'term' => $this->getTerm(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>