<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceStartWindowSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceStartWindowSettings extends CEosPluralBase {

	/**
	 * @return CUnitSpaceStartWindowSetting[]
	 */
	public static function fetchUnitSpaceStartWindowSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitSpaceStartWindowSetting::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceStartWindowSetting
	 */
	public static function fetchUnitSpaceStartWindowSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceStartWindowSetting::class, $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_start_window_settings', $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSetting( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceStartWindowSettingsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceStartWindowSettings( sprintf( 'SELECT * FROM unit_space_start_window_settings WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>