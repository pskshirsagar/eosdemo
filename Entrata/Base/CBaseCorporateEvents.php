<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateEvents extends CEosPluralBase {

	/**
	 * @return CCorporateEvent[]
	 */
	public static function fetchCorporateEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCorporateEvent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporateEvent
	 */
	public static function fetchCorporateEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCorporateEvent', $objDatabase );
	}

	public static function fetchCorporateEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_events', $objDatabase );
	}

	public static function fetchCorporateEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporateEvent( sprintf( 'SELECT * FROM corporate_events WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateEventsByCid( $intCid, $objDatabase ) {
		return self::fetchCorporateEvents( sprintf( 'SELECT * FROM corporate_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateEventsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCorporateEvents( sprintf( 'SELECT * FROM corporate_events WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateEventsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCorporateEvents( sprintf( 'SELECT * FROM corporate_events WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateEventsByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {
		return self::fetchCorporateEvents( sprintf( 'SELECT * FROM corporate_events WHERE corporate_id = %d AND cid = %d', ( int ) $intCorporateId, ( int ) $intCid ), $objDatabase );
	}

}
?>