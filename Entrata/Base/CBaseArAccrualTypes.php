<?php

class CBaseArAccrualTypes extends CEosPluralBase {

    const TABLE_AR_ACCRUAL_TYPES = 'public.ar_accrual_types';

    public static function fetchArAccrualTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CArAccrualType', $objDatabase );
    }

    public static function fetchArAccrualType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArAccrualType', $objDatabase );
    }

    public static function fetchArAccrualTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_accrual_types', $objDatabase );
    }

    public static function fetchArAccrualTypeById( $intId, $objDatabase ) {
        return self::fetchArAccrualType( sprintf( 'SELECT * FROM ar_accrual_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>