<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteAd extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.website_ads';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteAdTypeId;
	protected $m_intSpecialId;
	protected $m_strTitle;
	protected $m_strMainText;
	protected $m_strDescription;
	protected $m_strLinkText;
	protected $m_strLinkUrl;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsTarget;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strAltText;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsTarget = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_ad_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteAdTypeId', trim( $arrValues['website_ad_type_id'] ) ); elseif( isset( $arrValues['website_ad_type_id'] ) ) $this->setWebsiteAdTypeId( $arrValues['website_ad_type_id'] );
		if( isset( $arrValues['special_id'] ) && $boolDirectSet ) $this->set( 'm_intSpecialId', trim( $arrValues['special_id'] ) ); elseif( isset( $arrValues['special_id'] ) ) $this->setSpecialId( $arrValues['special_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['main_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMainText', trim( $arrValues['main_text'] ) ); elseif( isset( $arrValues['main_text'] ) ) $this->setMainText( $arrValues['main_text'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( $arrValues['link_text'] ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( $arrValues['link_text'] );
		if( isset( $arrValues['link_url'] ) && $boolDirectSet ) $this->set( 'm_strLinkUrl', trim( $arrValues['link_url'] ) ); elseif( isset( $arrValues['link_url'] ) ) $this->setLinkUrl( $arrValues['link_url'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_target'] ) && $boolDirectSet ) $this->set( 'm_boolIsTarget', trim( stripcslashes( $arrValues['is_target'] ) ) ); elseif( isset( $arrValues['is_target'] ) ) $this->setIsTarget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_target'] ) : $arrValues['is_target'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['alt_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAltText', trim( $arrValues['alt_text'] ) ); elseif( isset( $arrValues['alt_text'] ) ) $this->setAltText( $arrValues['alt_text'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteAdTypeId( $intWebsiteAdTypeId ) {
		$this->set( 'm_intWebsiteAdTypeId', CStrings::strToIntDef( $intWebsiteAdTypeId, NULL, false ) );
	}

	public function getWebsiteAdTypeId() {
		return $this->m_intWebsiteAdTypeId;
	}

	public function sqlWebsiteAdTypeId() {
		return ( true == isset( $this->m_intWebsiteAdTypeId ) ) ? ( string ) $this->m_intWebsiteAdTypeId : 'NULL';
	}

	public function setSpecialId( $intSpecialId ) {
		$this->set( 'm_intSpecialId', CStrings::strToIntDef( $intSpecialId, NULL, false ) );
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function sqlSpecialId() {
		return ( true == isset( $this->m_intSpecialId ) ) ? ( string ) $this->m_intSpecialId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setMainText( $strMainText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMainText', CStrings::strTrimDef( $strMainText, -1, NULL, true ), $strLocaleCode );
	}

	public function getMainText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMainText', $strLocaleCode );
	}

	public function sqlMainText() {
		return ( true == isset( $this->m_strMainText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMainText ) : '\'' . addslashes( $this->m_strMainText ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, 255, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkText ) : '\'' . addslashes( $this->m_strLinkText ) . '\'' ) : 'NULL';
	}

	public function setLinkUrl( $strLinkUrl ) {
		$this->set( 'm_strLinkUrl', CStrings::strTrimDef( $strLinkUrl, 4096, NULL, true ) );
	}

	public function getLinkUrl() {
		return $this->m_strLinkUrl;
	}

	public function sqlLinkUrl() {
		return ( true == isset( $this->m_strLinkUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkUrl ) : '\'' . addslashes( $this->m_strLinkUrl ) . '\'' ) : 'NULL';
	}

	public function setIsTarget( $boolIsTarget ) {
		$this->set( 'm_boolIsTarget', CStrings::strToBool( $boolIsTarget ) );
	}

	public function getIsTarget() {
		return $this->m_boolIsTarget;
	}

	public function sqlIsTarget() {
		return ( true == isset( $this->m_boolIsTarget ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTarget ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setAltText( $strAltText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAltText', CStrings::strTrimDef( $strAltText, 255, NULL, true ), $strLocaleCode );
	}

	public function getAltText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAltText', $strLocaleCode );
	}

	public function sqlAltText() {
		return ( true == isset( $this->m_strAltText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltText ) : '\'' . addslashes( $this->m_strAltText ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_ad_type_id, special_id, title, main_text, description, link_text, link_url, details, is_target, deleted_on, deleted_by, updated_on, updated_by, created_on, created_by, alt_text )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlWebsiteAdTypeId() . ', ' .
						$this->sqlSpecialId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlMainText() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlLinkText() . ', ' .
						$this->sqlLinkUrl() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsTarget() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlAltText() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_ad_type_id = ' . $this->sqlWebsiteAdTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsiteAdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_ad_type_id = ' . $this->sqlWebsiteAdTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_id = ' . $this->sqlSpecialId(). ',' ; } elseif( true == array_key_exists( 'SpecialId', $this->getChangedColumns() ) ) { $strSql .= ' special_id = ' . $this->sqlSpecialId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_text = ' . $this->sqlMainText(). ',' ; } elseif( true == array_key_exists( 'MainText', $this->getChangedColumns() ) ) { $strSql .= ' main_text = ' . $this->sqlMainText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl(). ',' ; } elseif( true == array_key_exists( 'LinkUrl', $this->getChangedColumns() ) ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_target = ' . $this->sqlIsTarget(). ',' ; } elseif( true == array_key_exists( 'IsTarget', $this->getChangedColumns() ) ) { $strSql .= ' is_target = ' . $this->sqlIsTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_text = ' . $this->sqlAltText(). ',' ; } elseif( true == array_key_exists( 'AltText', $this->getChangedColumns() ) ) { $strSql .= ' alt_text = ' . $this->sqlAltText() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_ad_type_id' => $this->getWebsiteAdTypeId(),
			'special_id' => $this->getSpecialId(),
			'title' => $this->getTitle(),
			'main_text' => $this->getMainText(),
			'description' => $this->getDescription(),
			'link_text' => $this->getLinkText(),
			'link_url' => $this->getLinkUrl(),
			'details' => $this->getDetails(),
			'is_target' => $this->getIsTarget(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'alt_text' => $this->getAltText()
		);
	}

}
?>