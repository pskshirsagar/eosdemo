<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExceptionQueueItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApExceptionQueueItems extends CEosPluralBase {

	/**
	 * @return CApExceptionQueueItem[]
	 */
	public static function fetchApExceptionQueueItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApExceptionQueueItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApExceptionQueueItem
	 */
	public static function fetchApExceptionQueueItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApExceptionQueueItem::class, $objDatabase );
	}

	public static function fetchApExceptionQueueItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_exception_queue_items', $objDatabase );
	}

	public static function fetchApExceptionQueueItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItem( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByCid( $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByUtilityBillIdByCid( $intUtilityBillId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE utility_bill_id = %d AND cid = %d', $intUtilityBillId, $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByBillEscalationIdByCid( $intBillEscalationId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE bill_escalation_id = %d AND cid = %d', $intBillEscalationId, $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByBillEscalateTypeIdByCid( $intBillEscalateTypeId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE bill_escalate_type_id = %d AND cid = %d', $intBillEscalateTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByOrderHeaderIdByCid( $intOrderHeaderId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE order_header_id = %d AND cid = %d', $intOrderHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApExceptionQueueItems( sprintf( 'SELECT * FROM ap_exception_queue_items WHERE ap_header_id = %d AND cid = %d', $intApHeaderId, $intCid ), $objDatabase );
	}

}
?>