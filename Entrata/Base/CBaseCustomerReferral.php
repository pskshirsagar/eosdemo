<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerReferral extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_referrals';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intPropertyId;
	protected $m_intCustomerApplicationId;
	protected $m_intArTransactionId;
	protected $m_fltIncentiveAmount;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strPersonalNote;
	protected $m_strBrochureSentOn;
	protected $m_strConvertedToLeadOn;
	protected $m_strIncentiveCreditedOn;
	protected $m_intIncentiveApprovedBy;
	protected $m_strIncentiveApprovedOn;
	protected $m_intViewedBy;
	protected $m_strViewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltIncentiveAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerApplicationId', trim( $arrValues['customer_application_id'] ) ); elseif( isset( $arrValues['customer_application_id'] ) ) $this->setCustomerApplicationId( $arrValues['customer_application_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['incentive_amount'] ) && $boolDirectSet ) $this->set( 'm_fltIncentiveAmount', trim( $arrValues['incentive_amount'] ) ); elseif( isset( $arrValues['incentive_amount'] ) ) $this->setIncentiveAmount( $arrValues['incentive_amount'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['personal_note'] ) && $boolDirectSet ) $this->set( 'm_strPersonalNote', trim( $arrValues['personal_note'] ) ); elseif( isset( $arrValues['personal_note'] ) ) $this->setPersonalNote( $arrValues['personal_note'] );
		if( isset( $arrValues['brochure_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strBrochureSentOn', trim( $arrValues['brochure_sent_on'] ) ); elseif( isset( $arrValues['brochure_sent_on'] ) ) $this->setBrochureSentOn( $arrValues['brochure_sent_on'] );
		if( isset( $arrValues['converted_to_lead_on'] ) && $boolDirectSet ) $this->set( 'm_strConvertedToLeadOn', trim( $arrValues['converted_to_lead_on'] ) ); elseif( isset( $arrValues['converted_to_lead_on'] ) ) $this->setConvertedToLeadOn( $arrValues['converted_to_lead_on'] );
		if( isset( $arrValues['incentive_credited_on'] ) && $boolDirectSet ) $this->set( 'm_strIncentiveCreditedOn', trim( $arrValues['incentive_credited_on'] ) ); elseif( isset( $arrValues['incentive_credited_on'] ) ) $this->setIncentiveCreditedOn( $arrValues['incentive_credited_on'] );
		if( isset( $arrValues['incentive_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intIncentiveApprovedBy', trim( $arrValues['incentive_approved_by'] ) ); elseif( isset( $arrValues['incentive_approved_by'] ) ) $this->setIncentiveApprovedBy( $arrValues['incentive_approved_by'] );
		if( isset( $arrValues['incentive_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strIncentiveApprovedOn', trim( $arrValues['incentive_approved_on'] ) ); elseif( isset( $arrValues['incentive_approved_on'] ) ) $this->setIncentiveApprovedOn( $arrValues['incentive_approved_on'] );
		if( isset( $arrValues['viewed_by'] ) && $boolDirectSet ) $this->set( 'm_intViewedBy', trim( $arrValues['viewed_by'] ) ); elseif( isset( $arrValues['viewed_by'] ) ) $this->setViewedBy( $arrValues['viewed_by'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerApplicationId( $intCustomerApplicationId ) {
		$this->set( 'm_intCustomerApplicationId', CStrings::strToIntDef( $intCustomerApplicationId, NULL, false ) );
	}

	public function getCustomerApplicationId() {
		return $this->m_intCustomerApplicationId;
	}

	public function sqlCustomerApplicationId() {
		return ( true == isset( $this->m_intCustomerApplicationId ) ) ? ( string ) $this->m_intCustomerApplicationId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setIncentiveAmount( $fltIncentiveAmount ) {
		$this->set( 'm_fltIncentiveAmount', CStrings::strToFloatDef( $fltIncentiveAmount, NULL, false, 2 ) );
	}

	public function getIncentiveAmount() {
		return $this->m_fltIncentiveAmount;
	}

	public function sqlIncentiveAmount() {
		return ( true == isset( $this->m_fltIncentiveAmount ) ) ? ( string ) $this->m_fltIncentiveAmount : '0';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPersonalNote( $strPersonalNote ) {
		$this->set( 'm_strPersonalNote', CStrings::strTrimDef( $strPersonalNote, -1, NULL, true ) );
	}

	public function getPersonalNote() {
		return $this->m_strPersonalNote;
	}

	public function sqlPersonalNote() {
		return ( true == isset( $this->m_strPersonalNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPersonalNote ) : '\'' . addslashes( $this->m_strPersonalNote ) . '\'' ) : 'NULL';
	}

	public function setBrochureSentOn( $strBrochureSentOn ) {
		$this->set( 'm_strBrochureSentOn', CStrings::strTrimDef( $strBrochureSentOn, -1, NULL, true ) );
	}

	public function getBrochureSentOn() {
		return $this->m_strBrochureSentOn;
	}

	public function sqlBrochureSentOn() {
		return ( true == isset( $this->m_strBrochureSentOn ) ) ? '\'' . $this->m_strBrochureSentOn . '\'' : 'NULL';
	}

	public function setConvertedToLeadOn( $strConvertedToLeadOn ) {
		$this->set( 'm_strConvertedToLeadOn', CStrings::strTrimDef( $strConvertedToLeadOn, -1, NULL, true ) );
	}

	public function getConvertedToLeadOn() {
		return $this->m_strConvertedToLeadOn;
	}

	public function sqlConvertedToLeadOn() {
		return ( true == isset( $this->m_strConvertedToLeadOn ) ) ? '\'' . $this->m_strConvertedToLeadOn . '\'' : 'NULL';
	}

	public function setIncentiveCreditedOn( $strIncentiveCreditedOn ) {
		$this->set( 'm_strIncentiveCreditedOn', CStrings::strTrimDef( $strIncentiveCreditedOn, -1, NULL, true ) );
	}

	public function getIncentiveCreditedOn() {
		return $this->m_strIncentiveCreditedOn;
	}

	public function sqlIncentiveCreditedOn() {
		return ( true == isset( $this->m_strIncentiveCreditedOn ) ) ? '\'' . $this->m_strIncentiveCreditedOn . '\'' : 'NULL';
	}

	public function setIncentiveApprovedBy( $intIncentiveApprovedBy ) {
		$this->set( 'm_intIncentiveApprovedBy', CStrings::strToIntDef( $intIncentiveApprovedBy, NULL, false ) );
	}

	public function getIncentiveApprovedBy() {
		return $this->m_intIncentiveApprovedBy;
	}

	public function sqlIncentiveApprovedBy() {
		return ( true == isset( $this->m_intIncentiveApprovedBy ) ) ? ( string ) $this->m_intIncentiveApprovedBy : 'NULL';
	}

	public function setIncentiveApprovedOn( $strIncentiveApprovedOn ) {
		$this->set( 'm_strIncentiveApprovedOn', CStrings::strTrimDef( $strIncentiveApprovedOn, -1, NULL, true ) );
	}

	public function getIncentiveApprovedOn() {
		return $this->m_strIncentiveApprovedOn;
	}

	public function sqlIncentiveApprovedOn() {
		return ( true == isset( $this->m_strIncentiveApprovedOn ) ) ? '\'' . $this->m_strIncentiveApprovedOn . '\'' : 'NULL';
	}

	public function setViewedBy( $intViewedBy ) {
		$this->set( 'm_intViewedBy', CStrings::strToIntDef( $intViewedBy, NULL, false ) );
	}

	public function getViewedBy() {
		return $this->m_intViewedBy;
	}

	public function sqlViewedBy() {
		return ( true == isset( $this->m_intViewedBy ) ) ? ( string ) $this->m_intViewedBy : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, property_id, customer_application_id, ar_transaction_id, incentive_amount, name_first, name_last, email_address, phone_number, personal_note, brochure_sent_on, converted_to_lead_on, incentive_credited_on, incentive_approved_by, incentive_approved_on, viewed_by, viewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerApplicationId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlIncentiveAmount() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlPersonalNote() . ', ' .
						$this->sqlBrochureSentOn() . ', ' .
						$this->sqlConvertedToLeadOn() . ', ' .
						$this->sqlIncentiveCreditedOn() . ', ' .
						$this->sqlIncentiveApprovedBy() . ', ' .
						$this->sqlIncentiveApprovedOn() . ', ' .
						$this->sqlViewedBy() . ', ' .
						$this->sqlViewedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_application_id = ' . $this->sqlCustomerApplicationId(). ',' ; } elseif( true == array_key_exists( 'CustomerApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' customer_application_id = ' . $this->sqlCustomerApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_amount = ' . $this->sqlIncentiveAmount(). ',' ; } elseif( true == array_key_exists( 'IncentiveAmount', $this->getChangedColumns() ) ) { $strSql .= ' incentive_amount = ' . $this->sqlIncentiveAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_note = ' . $this->sqlPersonalNote(). ',' ; } elseif( true == array_key_exists( 'PersonalNote', $this->getChangedColumns() ) ) { $strSql .= ' personal_note = ' . $this->sqlPersonalNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' brochure_sent_on = ' . $this->sqlBrochureSentOn(). ',' ; } elseif( true == array_key_exists( 'BrochureSentOn', $this->getChangedColumns() ) ) { $strSql .= ' brochure_sent_on = ' . $this->sqlBrochureSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' converted_to_lead_on = ' . $this->sqlConvertedToLeadOn(). ',' ; } elseif( true == array_key_exists( 'ConvertedToLeadOn', $this->getChangedColumns() ) ) { $strSql .= ' converted_to_lead_on = ' . $this->sqlConvertedToLeadOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_credited_on = ' . $this->sqlIncentiveCreditedOn(). ',' ; } elseif( true == array_key_exists( 'IncentiveCreditedOn', $this->getChangedColumns() ) ) { $strSql .= ' incentive_credited_on = ' . $this->sqlIncentiveCreditedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_approved_by = ' . $this->sqlIncentiveApprovedBy(). ',' ; } elseif( true == array_key_exists( 'IncentiveApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' incentive_approved_by = ' . $this->sqlIncentiveApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_approved_on = ' . $this->sqlIncentiveApprovedOn(). ',' ; } elseif( true == array_key_exists( 'IncentiveApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' incentive_approved_on = ' . $this->sqlIncentiveApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy(). ',' ; } elseif( true == array_key_exists( 'ViewedBy', $this->getChangedColumns() ) ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn(). ',' ; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'property_id' => $this->getPropertyId(),
			'customer_application_id' => $this->getCustomerApplicationId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'incentive_amount' => $this->getIncentiveAmount(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'personal_note' => $this->getPersonalNote(),
			'brochure_sent_on' => $this->getBrochureSentOn(),
			'converted_to_lead_on' => $this->getConvertedToLeadOn(),
			'incentive_credited_on' => $this->getIncentiveCreditedOn(),
			'incentive_approved_by' => $this->getIncentiveApprovedBy(),
			'incentive_approved_on' => $this->getIncentiveApprovedOn(),
			'viewed_by' => $this->getViewedBy(),
			'viewed_on' => $this->getViewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>