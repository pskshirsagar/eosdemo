<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerTypes extends CEosPluralBase {

	/**
	 * @return CCustomerType[]
	 */
	public static function fetchCustomerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerType::class, $objDatabase );
	}

	/**
	 * @return CCustomerType
	 */
	public static function fetchCustomerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerType::class, $objDatabase );
	}

	public static function fetchCustomerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_types', $objDatabase );
	}

	public static function fetchCustomerTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerType( sprintf( 'SELECT * FROM customer_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>