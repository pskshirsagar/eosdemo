<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeePropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeePropertyGroups extends CEosPluralBase {

	/**
	 * @return CApPayeePropertyGroup[]
	 */
	public static function fetchApPayeePropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayeePropertyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeePropertyGroup
	 */
	public static function fetchApPayeePropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayeePropertyGroup', $objDatabase );
	}

	public static function fetchApPayeePropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_property_groups', $objDatabase );
	}

	public static function fetchApPayeePropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroup( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroups( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroups( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroups( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroups( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertyGroupsByDefaultGlAccountIdByCid( $intDefaultGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeePropertyGroups( sprintf( 'SELECT * FROM ap_payee_property_groups WHERE default_gl_account_id = %d AND cid = %d', ( int ) $intDefaultGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>