<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApPaymentTypeAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApPaymentTypeAssociations extends CEosPluralBase {

	/**
	 * @return CCompanyApPaymentTypeAssociation[]
	 */
	public static function fetchCompanyApPaymentTypeAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyApPaymentTypeAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyApPaymentTypeAssociation
	 */
	public static function fetchCompanyApPaymentTypeAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyApPaymentTypeAssociation', $objDatabase );
	}

	public static function fetchCompanyApPaymentTypeAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_ap_payment_type_associations', $objDatabase );
	}

	public static function fetchCompanyApPaymentTypeAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyApPaymentTypeAssociation( sprintf( 'SELECT * FROM company_ap_payment_type_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApPaymentTypeAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApPaymentTypeAssociations( sprintf( 'SELECT * FROM company_ap_payment_type_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApPaymentTypeAssociationsByApPaymentTypeIdByCid( $intApPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyApPaymentTypeAssociations( sprintf( 'SELECT * FROM company_ap_payment_type_associations WHERE ap_payment_type_id = %d AND cid = %d', ( int ) $intApPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>