<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDataNote extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_data_notes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intCustomerDataTypeId;
	protected $m_intCustomerDataReferenceId;
	protected $m_strNote;
	protected $m_intNoteCreatedBy;
	protected $m_strNoteCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataTypeId', trim( $arrValues['customer_data_type_id'] ) ); elseif( isset( $arrValues['customer_data_type_id'] ) ) $this->setCustomerDataTypeId( $arrValues['customer_data_type_id'] );
		if( isset( $arrValues['customer_data_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataReferenceId', trim( $arrValues['customer_data_reference_id'] ) ); elseif( isset( $arrValues['customer_data_reference_id'] ) ) $this->setCustomerDataReferenceId( $arrValues['customer_data_reference_id'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['note_created_by'] ) && $boolDirectSet ) $this->set( 'm_intNoteCreatedBy', trim( $arrValues['note_created_by'] ) ); elseif( isset( $arrValues['note_created_by'] ) ) $this->setNoteCreatedBy( $arrValues['note_created_by'] );
		if( isset( $arrValues['note_created_on'] ) && $boolDirectSet ) $this->set( 'm_strNoteCreatedOn', trim( $arrValues['note_created_on'] ) ); elseif( isset( $arrValues['note_created_on'] ) ) $this->setNoteCreatedOn( $arrValues['note_created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerDataTypeId( $intCustomerDataTypeId ) {
		$this->set( 'm_intCustomerDataTypeId', CStrings::strToIntDef( $intCustomerDataTypeId, NULL, false ) );
	}

	public function getCustomerDataTypeId() {
		return $this->m_intCustomerDataTypeId;
	}

	public function sqlCustomerDataTypeId() {
		return ( true == isset( $this->m_intCustomerDataTypeId ) ) ? ( string ) $this->m_intCustomerDataTypeId : 'NULL';
	}

	public function setCustomerDataReferenceId( $intCustomerDataReferenceId ) {
		$this->set( 'm_intCustomerDataReferenceId', CStrings::strToIntDef( $intCustomerDataReferenceId, NULL, false ) );
	}

	public function getCustomerDataReferenceId() {
		return $this->m_intCustomerDataReferenceId;
	}

	public function sqlCustomerDataReferenceId() {
		return ( true == isset( $this->m_intCustomerDataReferenceId ) ) ? ( string ) $this->m_intCustomerDataReferenceId : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setNoteCreatedBy( $intNoteCreatedBy ) {
		$this->set( 'm_intNoteCreatedBy', CStrings::strToIntDef( $intNoteCreatedBy, NULL, false ) );
	}

	public function getNoteCreatedBy() {
		return $this->m_intNoteCreatedBy;
	}

	public function sqlNoteCreatedBy() {
		return ( true == isset( $this->m_intNoteCreatedBy ) ) ? ( string ) $this->m_intNoteCreatedBy : 'NULL';
	}

	public function setNoteCreatedOn( $strNoteCreatedOn ) {
		$this->set( 'm_strNoteCreatedOn', CStrings::strTrimDef( $strNoteCreatedOn, -1, NULL, true ) );
	}

	public function getNoteCreatedOn() {
		return $this->m_strNoteCreatedOn;
	}

	public function sqlNoteCreatedOn() {
		return ( true == isset( $this->m_strNoteCreatedOn ) ) ? '\'' . $this->m_strNoteCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, customer_data_type_id, customer_data_reference_id, note, note_created_by, note_created_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlCustomerDataTypeId() . ', ' .
 						$this->sqlCustomerDataReferenceId() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlNoteCreatedBy() . ', ' .
 						$this->sqlNoteCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; } elseif( true == array_key_exists( 'CustomerDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_reference_id = ' . $this->sqlCustomerDataReferenceId() . ','; } elseif( true == array_key_exists( 'CustomerDataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_reference_id = ' . $this->sqlCustomerDataReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_created_by = ' . $this->sqlNoteCreatedBy() . ','; } elseif( true == array_key_exists( 'NoteCreatedBy', $this->getChangedColumns() ) ) { $strSql .= ' note_created_by = ' . $this->sqlNoteCreatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_created_on = ' . $this->sqlNoteCreatedOn() . ','; } elseif( true == array_key_exists( 'NoteCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' note_created_on = ' . $this->sqlNoteCreatedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'customer_data_type_id' => $this->getCustomerDataTypeId(),
			'customer_data_reference_id' => $this->getCustomerDataReferenceId(),
			'note' => $this->getNote(),
			'note_created_by' => $this->getNoteCreatedBy(),
			'note_created_on' => $this->getNoteCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>