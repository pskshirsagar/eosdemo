<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDrawRequestDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDrawRequestDetails extends CEosPluralBase {

	/**
	 * @return CDrawRequestDetail[]
	 */
	public static function fetchDrawRequestDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDrawRequestDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDrawRequestDetail
	 */
	public static function fetchDrawRequestDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDrawRequestDetail', $objDatabase );
	}

	public static function fetchDrawRequestDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'draw_request_details', $objDatabase );
	}

	public static function fetchDrawRequestDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetail( sprintf( 'SELECT * FROM draw_request_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetails( sprintf( 'SELECT * FROM draw_request_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestDetailsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetails( sprintf( 'SELECT * FROM draw_request_details WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestDetailsByDrawRequestIdByCid( $intDrawRequestId, $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetails( sprintf( 'SELECT * FROM draw_request_details WHERE draw_request_id = %d AND cid = %d', ( int ) $intDrawRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestDetailsByApDetailIdByCid( $intApDetailId, $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetails( sprintf( 'SELECT * FROM draw_request_details WHERE ap_detail_id = %d AND cid = %d', ( int ) $intApDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestDetailsByGlDetailIdByCid( $intGlDetailId, $intCid, $objDatabase ) {
		return self::fetchDrawRequestDetails( sprintf( 'SELECT * FROM draw_request_details WHERE gl_detail_id = %d AND cid = %d', ( int ) $intGlDetailId, ( int ) $intCid ), $objDatabase );
	}

}
?>