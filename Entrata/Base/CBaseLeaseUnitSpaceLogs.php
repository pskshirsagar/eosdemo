<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseUnitSpaceLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseUnitSpaceLogs extends CEosPluralBase {

	/**
	 * @return CLeaseUnitSpaceLog[]
	 */
	public static function fetchLeaseUnitSpaceLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseUnitSpaceLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseUnitSpaceLog
	 */
	public static function fetchLeaseUnitSpaceLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseUnitSpaceLog', $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_unit_space_logs', $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLog( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByPriorLeaseUnitSpaceLogIdByCid( $intPriorLeaseUnitSpaceLogId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE prior_lease_unit_space_log_id = %d AND cid = %d', ( int ) $intPriorLeaseUnitSpaceLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE reporting_period_id = %d AND cid = %d', ( int ) $intReportingPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE effective_period_id = %d AND cid = %d', ( int ) $intEffectivePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpaceLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaceLogs( sprintf( 'SELECT * FROM lease_unit_space_logs WHERE original_period_id = %d AND cid = %d', ( int ) $intOriginalPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>