<?php

class CBaseIntegrationClientTypeService extends CEosSingularBase {

	const TABLE_NAME = 'public.integration_client_type_services';

	protected $m_intId;
	protected $m_intIntegrationClientTypeId;
	protected $m_intIntegrationServiceId;
	protected $m_intIntegrationSyncTypeId;
	protected $m_intIntegrationClientStatusTypeId;
	protected $m_intIntegrationPeriodId;
	protected $m_strFinalSeriviceUri;
	protected $m_intIsDefaultOff;
	protected $m_intOrderNum;
	protected $m_boolIsPropertySpecific;
	protected $m_strInterfaceEntity;
	protected $m_strInterfaceLicense;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDefaultOff = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolIsPropertySpecific = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['integration_client_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientTypeId', trim( $arrValues['integration_client_type_id'] ) ); elseif( isset( $arrValues['integration_client_type_id'] ) ) $this->setIntegrationClientTypeId( $arrValues['integration_client_type_id'] );
		if( isset( $arrValues['integration_service_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationServiceId', trim( $arrValues['integration_service_id'] ) ); elseif( isset( $arrValues['integration_service_id'] ) ) $this->setIntegrationServiceId( $arrValues['integration_service_id'] );
		if( isset( $arrValues['integration_sync_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationSyncTypeId', trim( $arrValues['integration_sync_type_id'] ) ); elseif( isset( $arrValues['integration_sync_type_id'] ) ) $this->setIntegrationSyncTypeId( $arrValues['integration_sync_type_id'] );
		if( isset( $arrValues['integration_client_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientStatusTypeId', trim( $arrValues['integration_client_status_type_id'] ) ); elseif( isset( $arrValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrValues['integration_client_status_type_id'] );
		if( isset( $arrValues['integration_period_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationPeriodId', trim( $arrValues['integration_period_id'] ) ); elseif( isset( $arrValues['integration_period_id'] ) ) $this->setIntegrationPeriodId( $arrValues['integration_period_id'] );
		if( isset( $arrValues['final_serivice_uri'] ) && $boolDirectSet ) $this->set( 'm_strFinalSeriviceUri', trim( stripcslashes( $arrValues['final_serivice_uri'] ) ) ); elseif( isset( $arrValues['final_serivice_uri'] ) ) $this->setFinalSeriviceUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['final_serivice_uri'] ) : $arrValues['final_serivice_uri'] );
		if( isset( $arrValues['is_default_off'] ) && $boolDirectSet ) $this->set( 'm_intIsDefaultOff', trim( $arrValues['is_default_off'] ) ); elseif( isset( $arrValues['is_default_off'] ) ) $this->setIsDefaultOff( $arrValues['is_default_off'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_property_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsPropertySpecific', trim( stripcslashes( $arrValues['is_property_specific'] ) ) ); elseif( isset( $arrValues['is_property_specific'] ) ) $this->setIsPropertySpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property_specific'] ) : $arrValues['is_property_specific'] );
		if( isset( $arrValues['interface_entity'] ) && $boolDirectSet ) $this->set( 'm_strInterfaceEntity', trim( stripcslashes( $arrValues['interface_entity'] ) ) ); elseif( isset( $arrValues['interface_entity'] ) ) $this->setInterfaceEntity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['interface_entity'] ) : $arrValues['interface_entity'] );
		if( isset( $arrValues['interface_license'] ) && $boolDirectSet ) $this->set( 'm_strInterfaceLicense', trim( stripcslashes( $arrValues['interface_license'] ) ) ); elseif( isset( $arrValues['interface_license'] ) ) $this->setInterfaceLicense( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['interface_license'] ) : $arrValues['interface_license'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIntegrationClientTypeId( $intIntegrationClientTypeId ) {
		$this->set( 'm_intIntegrationClientTypeId', CStrings::strToIntDef( $intIntegrationClientTypeId, NULL, false ) );
	}

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
	}

	public function sqlIntegrationClientTypeId() {
		return ( true == isset( $this->m_intIntegrationClientTypeId ) ) ? ( string ) $this->m_intIntegrationClientTypeId : 'NULL';
	}

	public function setIntegrationServiceId( $intIntegrationServiceId ) {
		$this->set( 'm_intIntegrationServiceId', CStrings::strToIntDef( $intIntegrationServiceId, NULL, false ) );
	}

	public function getIntegrationServiceId() {
		return $this->m_intIntegrationServiceId;
	}

	public function sqlIntegrationServiceId() {
		return ( true == isset( $this->m_intIntegrationServiceId ) ) ? ( string ) $this->m_intIntegrationServiceId : 'NULL';
	}

	public function setIntegrationSyncTypeId( $intIntegrationSyncTypeId ) {
		$this->set( 'm_intIntegrationSyncTypeId', CStrings::strToIntDef( $intIntegrationSyncTypeId, NULL, false ) );
	}

	public function getIntegrationSyncTypeId() {
		return $this->m_intIntegrationSyncTypeId;
	}

	public function sqlIntegrationSyncTypeId() {
		return ( true == isset( $this->m_intIntegrationSyncTypeId ) ) ? ( string ) $this->m_intIntegrationSyncTypeId : 'NULL';
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		$this->set( 'm_intIntegrationClientStatusTypeId', CStrings::strToIntDef( $intIntegrationClientStatusTypeId, NULL, false ) );
	}

	public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

	public function sqlIntegrationClientStatusTypeId() {
		return ( true == isset( $this->m_intIntegrationClientStatusTypeId ) ) ? ( string ) $this->m_intIntegrationClientStatusTypeId : 'NULL';
	}

	public function setIntegrationPeriodId( $intIntegrationPeriodId ) {
		$this->set( 'm_intIntegrationPeriodId', CStrings::strToIntDef( $intIntegrationPeriodId, NULL, false ) );
	}

	public function getIntegrationPeriodId() {
		return $this->m_intIntegrationPeriodId;
	}

	public function sqlIntegrationPeriodId() {
		return ( true == isset( $this->m_intIntegrationPeriodId ) ) ? ( string ) $this->m_intIntegrationPeriodId : 'NULL';
	}

	public function setFinalSeriviceUri( $strFinalSeriviceUri ) {
		$this->set( 'm_strFinalSeriviceUri', CStrings::strTrimDef( $strFinalSeriviceUri, 4096, NULL, true ) );
	}

	public function getFinalSeriviceUri() {
		return $this->m_strFinalSeriviceUri;
	}

	public function sqlFinalSeriviceUri() {
		return ( true == isset( $this->m_strFinalSeriviceUri ) ) ? '\'' . addslashes( $this->m_strFinalSeriviceUri ) . '\'' : 'NULL';
	}

	public function setIsDefaultOff( $intIsDefaultOff ) {
		$this->set( 'm_intIsDefaultOff', CStrings::strToIntDef( $intIsDefaultOff, NULL, false ) );
	}

	public function getIsDefaultOff() {
		return $this->m_intIsDefaultOff;
	}

	public function sqlIsDefaultOff() {
		return ( true == isset( $this->m_intIsDefaultOff ) ) ? ( string ) $this->m_intIsDefaultOff : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsPropertySpecific( $boolIsPropertySpecific ) {
		$this->set( 'm_boolIsPropertySpecific', CStrings::strToBool( $boolIsPropertySpecific ) );
	}

	public function getIsPropertySpecific() {
		return $this->m_boolIsPropertySpecific;
	}

	public function sqlIsPropertySpecific() {
		return ( true == isset( $this->m_boolIsPropertySpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPropertySpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInterfaceEntity( $strInterfaceEntity ) {
		$this->set( 'm_strInterfaceEntity', CStrings::strTrimDef( $strInterfaceEntity, 100, NULL, true ) );
	}

	public function getInterfaceEntity() {
		return $this->m_strInterfaceEntity;
	}

	public function sqlInterfaceEntity() {
		return ( true == isset( $this->m_strInterfaceEntity ) ) ? '\'' . addslashes( $this->m_strInterfaceEntity ) . '\'' : 'NULL';
	}

	public function setInterfaceLicense( $strInterfaceLicense ) {
		$this->set( 'm_strInterfaceLicense', CStrings::strTrimDef( $strInterfaceLicense, -1, NULL, true ) );
	}

	public function getInterfaceLicense() {
		return $this->m_strInterfaceLicense;
	}

	public function sqlInterfaceLicense() {
		return ( true == isset( $this->m_strInterfaceLicense ) ) ? '\'' . addslashes( $this->m_strInterfaceLicense ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'integration_client_type_id' => $this->getIntegrationClientTypeId(),
			'integration_service_id' => $this->getIntegrationServiceId(),
			'integration_sync_type_id' => $this->getIntegrationSyncTypeId(),
			'integration_client_status_type_id' => $this->getIntegrationClientStatusTypeId(),
			'integration_period_id' => $this->getIntegrationPeriodId(),
			'final_serivice_uri' => $this->getFinalSeriviceUri(),
			'is_default_off' => $this->getIsDefaultOff(),
			'order_num' => $this->getOrderNum(),
			'is_property_specific' => $this->getIsPropertySpecific(),
			'interface_entity' => $this->getInterfaceEntity(),
			'interface_license' => $this->getInterfaceLicense()
		);
	}

}
?>