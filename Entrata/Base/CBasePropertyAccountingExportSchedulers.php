<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAccountingExportSchedulers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAccountingExportSchedulers extends CEosPluralBase {

	/**
	 * @return CPropertyAccountingExportScheduler[]
	 */
	public static function fetchPropertyAccountingExportSchedulers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyAccountingExportScheduler::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAccountingExportScheduler
	 */
	public static function fetchPropertyAccountingExportScheduler( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyAccountingExportScheduler::class, $objDatabase );
	}

	public static function fetchPropertyAccountingExportSchedulerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_accounting_export_schedulers', $objDatabase );
	}

	public static function fetchPropertyAccountingExportSchedulerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAccountingExportScheduler( sprintf( 'SELECT * FROM property_accounting_export_schedulers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAccountingExportSchedulersByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAccountingExportSchedulers( sprintf( 'SELECT * FROM property_accounting_export_schedulers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAccountingExportSchedulersByAccountingExportSchedulerIdByCid( $intAccountingExportSchedulerId, $intCid, $objDatabase ) {
		return self::fetchPropertyAccountingExportSchedulers( sprintf( 'SELECT * FROM property_accounting_export_schedulers WHERE accounting_export_scheduler_id = %d AND cid = %d', ( int ) $intAccountingExportSchedulerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAccountingExportSchedulersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAccountingExportSchedulers( sprintf( 'SELECT * FROM property_accounting_export_schedulers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>