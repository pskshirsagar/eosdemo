<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitListApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWaitListApplications extends CEosPluralBase {

	/**
	 * @return CWaitListApplication[]
	 */
	public static function fetchWaitListApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CWaitListApplication', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CWaitListApplication
	 */
	public static function fetchWaitListApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CWaitListApplication', $objDatabase );
	}

	public static function fetchWaitListApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'wait_list_applications', $objDatabase );
	}

	public static function fetchWaitListApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchWaitListApplication( sprintf( 'SELECT * FROM wait_list_applications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchWaitListApplications( sprintf( 'SELECT * FROM wait_list_applications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListApplicationsByWaitListIdByCid( $intWaitListId, $intCid, $objDatabase ) {
		return self::fetchWaitListApplications( sprintf( 'SELECT * FROM wait_list_applications WHERE wait_list_id = %d AND cid = %d', ( int ) $intWaitListId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListApplicationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchWaitListApplications( sprintf( 'SELECT * FROM wait_list_applications WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListApplicationsByWaitListPointIdByCid( $intWaitListPointId, $intCid, $objDatabase ) {
		return self::fetchWaitListApplications( sprintf( 'SELECT * FROM wait_list_applications WHERE wait_list_point_id = %d AND cid = %d', ( int ) $intWaitListPointId, ( int ) $intCid ), $objDatabase );
	}

}
?>