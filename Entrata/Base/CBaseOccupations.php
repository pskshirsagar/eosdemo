<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COccupations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOccupations extends CEosPluralBase {

	/**
	 * @return COccupation[]
	 */
	public static function fetchOccupations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COccupation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COccupation
	 */
	public static function fetchOccupation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COccupation::class, $objDatabase );
	}

	public static function fetchOccupationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'occupations', $objDatabase );
	}

	public static function fetchOccupationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOccupation( sprintf( 'SELECT * FROM occupations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchOccupationsByCid( $intCid, $objDatabase ) {
		return self::fetchOccupations( sprintf( 'SELECT * FROM occupations WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>