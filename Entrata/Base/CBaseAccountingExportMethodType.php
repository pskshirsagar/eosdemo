<?php

class CBaseAccountingExportMethodType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.accounting_export_method_types';

	protected $m_intId;
	protected $m_strMethodName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['method_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMethodName', trim( stripcslashes( $arrValues['method_name'] ) ) ); elseif( isset( $arrValues['method_name'] ) ) $this->setMethodName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['method_name'] ) : $arrValues['method_name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMethodName( $strMethodName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMethodName', CStrings::strTrimDef( $strMethodName, 50, NULL, true ), $strLocaleCode );
	}

	public function getMethodName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMethodName', $strLocaleCode );
	}

	public function sqlMethodName() {
		return ( true == isset( $this->m_strMethodName ) ) ? '\'' . addslashes( $this->m_strMethodName ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'method_name' => $this->getMethodName(),
			'details' => $this->getDetails()
		);
	}

}
?>