<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatchFiles
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileBatchFiles extends CEosPluralBase {

	/**
	 * @return CFileBatchFile[]
	 */
	public static function fetchFileBatchFiles( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFileBatchFile', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileBatchFile
	 */
	public static function fetchFileBatchFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFileBatchFile', $objDatabase );
	}

	public static function fetchFileBatchFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_batch_files', $objDatabase );
	}

	public static function fetchFileBatchFileByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileBatchFile( sprintf( 'SELECT * FROM file_batch_files WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileBatchFilesByCid( $intCid, $objDatabase ) {
		return self::fetchFileBatchFiles( sprintf( 'SELECT * FROM file_batch_files WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileBatchFilesByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		return self::fetchFileBatchFiles( sprintf( 'SELECT * FROM file_batch_files WHERE file_batch_id = %d AND cid = %d', ( int ) $intFileBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileBatchFilesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileBatchFiles( sprintf( 'SELECT * FROM file_batch_files WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileBatchFilesByFileAssociationIdByCid( $intFileAssociationId, $intCid, $objDatabase ) {
		return self::fetchFileBatchFiles( sprintf( 'SELECT * FROM file_batch_files WHERE file_association_id = %d AND cid = %d', ( int ) $intFileAssociationId, ( int ) $intCid ), $objDatabase );
	}

}
?>