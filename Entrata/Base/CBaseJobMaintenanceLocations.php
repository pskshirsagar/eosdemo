<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobMaintenanceLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobMaintenanceLocations extends CEosPluralBase {

	/**
	 * @return CJobMaintenanceLocation[]
	 */
	public static function fetchJobMaintenanceLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobMaintenanceLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobMaintenanceLocation
	 */
	public static function fetchJobMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobMaintenanceLocation::class, $objDatabase );
	}

	public static function fetchJobMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_maintenance_locations', $objDatabase );
	}

	public static function fetchJobMaintenanceLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobMaintenanceLocation( sprintf( 'SELECT * FROM job_maintenance_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobMaintenanceLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchJobMaintenanceLocations( sprintf( 'SELECT * FROM job_maintenance_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobMaintenanceLocationsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobMaintenanceLocations( sprintf( 'SELECT * FROM job_maintenance_locations WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobMaintenanceLocationsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchJobMaintenanceLocations( sprintf( 'SELECT * FROM job_maintenance_locations WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>