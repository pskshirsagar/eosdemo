<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantRoommateInterest extends CEosSingularBase {

	const TABLE_NAME = 'public.applicant_roommate_interests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intLeaseId;
	protected $m_intRoommateInterestId;
	protected $m_intRoommateInterestOptionId;
	protected $m_strPreferenceDatetime;
	protected $m_strWrittenResponse;
	protected $m_intImportanceRating;
	protected $m_intRanking;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['roommate_interest_id'] ) && $boolDirectSet ) $this->set( 'm_intRoommateInterestId', trim( $arrValues['roommate_interest_id'] ) ); elseif( isset( $arrValues['roommate_interest_id'] ) ) $this->setRoommateInterestId( $arrValues['roommate_interest_id'] );
		if( isset( $arrValues['roommate_interest_option_id'] ) && $boolDirectSet ) $this->set( 'm_intRoommateInterestOptionId', trim( $arrValues['roommate_interest_option_id'] ) ); elseif( isset( $arrValues['roommate_interest_option_id'] ) ) $this->setRoommateInterestOptionId( $arrValues['roommate_interest_option_id'] );
		if( isset( $arrValues['preference_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPreferenceDatetime', trim( $arrValues['preference_datetime'] ) ); elseif( isset( $arrValues['preference_datetime'] ) ) $this->setPreferenceDatetime( $arrValues['preference_datetime'] );
		if( isset( $arrValues['written_response'] ) && $boolDirectSet ) $this->set( 'm_strWrittenResponse', trim( stripcslashes( $arrValues['written_response'] ) ) ); elseif( isset( $arrValues['written_response'] ) ) $this->setWrittenResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['written_response'] ) : $arrValues['written_response'] );
		if( isset( $arrValues['importance_rating'] ) && $boolDirectSet ) $this->set( 'm_intImportanceRating', trim( $arrValues['importance_rating'] ) ); elseif( isset( $arrValues['importance_rating'] ) ) $this->setImportanceRating( $arrValues['importance_rating'] );
		if( isset( $arrValues['ranking'] ) && $boolDirectSet ) $this->set( 'm_intRanking', trim( $arrValues['ranking'] ) ); elseif( isset( $arrValues['ranking'] ) ) $this->setRanking( $arrValues['ranking'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setRoommateInterestId( $intRoommateInterestId ) {
		$this->set( 'm_intRoommateInterestId', CStrings::strToIntDef( $intRoommateInterestId, NULL, false ) );
	}

	public function getRoommateInterestId() {
		return $this->m_intRoommateInterestId;
	}

	public function sqlRoommateInterestId() {
		return ( true == isset( $this->m_intRoommateInterestId ) ) ? ( string ) $this->m_intRoommateInterestId : 'NULL';
	}

	public function setRoommateInterestOptionId( $intRoommateInterestOptionId ) {
		$this->set( 'm_intRoommateInterestOptionId', CStrings::strToIntDef( $intRoommateInterestOptionId, NULL, false ) );
	}

	public function getRoommateInterestOptionId() {
		return $this->m_intRoommateInterestOptionId;
	}

	public function sqlRoommateInterestOptionId() {
		return ( true == isset( $this->m_intRoommateInterestOptionId ) ) ? ( string ) $this->m_intRoommateInterestOptionId : 'NULL';
	}

	public function setPreferenceDatetime( $strPreferenceDatetime ) {
		$this->set( 'm_strPreferenceDatetime', CStrings::strTrimDef( $strPreferenceDatetime, -1, NULL, true ) );
	}

	public function getPreferenceDatetime() {
		return $this->m_strPreferenceDatetime;
	}

	public function sqlPreferenceDatetime() {
		return ( true == isset( $this->m_strPreferenceDatetime ) ) ? '\'' . $this->m_strPreferenceDatetime . '\'' : 'NOW()';
	}

	public function setWrittenResponse( $strWrittenResponse ) {
		$this->set( 'm_strWrittenResponse', CStrings::strTrimDef( $strWrittenResponse, 2000, NULL, true ) );
	}

	public function getWrittenResponse() {
		return $this->m_strWrittenResponse;
	}

	public function sqlWrittenResponse() {
		return ( true == isset( $this->m_strWrittenResponse ) ) ? '\'' . addslashes( $this->m_strWrittenResponse ) . '\'' : 'NULL';
	}

	public function setImportanceRating( $intImportanceRating ) {
		$this->set( 'm_intImportanceRating', CStrings::strToIntDef( $intImportanceRating, NULL, false ) );
	}

	public function getImportanceRating() {
		return $this->m_intImportanceRating;
	}

	public function sqlImportanceRating() {
		return ( true == isset( $this->m_intImportanceRating ) ) ? ( string ) $this->m_intImportanceRating : 'NULL';
	}

	public function setRanking( $intRanking ) {
		$this->set( 'm_intRanking', CStrings::strToIntDef( $intRanking, NULL, false ) );
	}

	public function getRanking() {
		return $this->m_intRanking;
	}

	public function sqlRanking() {
		return ( true == isset( $this->m_intRanking ) ) ? ( string ) $this->m_intRanking : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, lease_id, roommate_interest_id, roommate_interest_option_id, preference_datetime, written_response, importance_rating, ranking, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlApplicantId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlRoommateInterestId() . ', ' .
		          $this->sqlRoommateInterestOptionId() . ', ' .
		          $this->sqlPreferenceDatetime() . ', ' .
		          $this->sqlWrittenResponse() . ', ' .
		          $this->sqlImportanceRating() . ', ' .
		          $this->sqlRanking() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' roommate_interest_id = ' . $this->sqlRoommateInterestId() . ','; } elseif( true == array_key_exists( 'RoommateInterestId', $this->getChangedColumns() ) ) { $strSql .= ' roommate_interest_id = ' . $this->sqlRoommateInterestId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' roommate_interest_option_id = ' . $this->sqlRoommateInterestOptionId() . ','; } elseif( true == array_key_exists( 'RoommateInterestOptionId', $this->getChangedColumns() ) ) { $strSql .= ' roommate_interest_option_id = ' . $this->sqlRoommateInterestOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preference_datetime = ' . $this->sqlPreferenceDatetime() . ','; } elseif( true == array_key_exists( 'PreferenceDatetime', $this->getChangedColumns() ) ) { $strSql .= ' preference_datetime = ' . $this->sqlPreferenceDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' written_response = ' . $this->sqlWrittenResponse() . ','; } elseif( true == array_key_exists( 'WrittenResponse', $this->getChangedColumns() ) ) { $strSql .= ' written_response = ' . $this->sqlWrittenResponse() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' importance_rating = ' . $this->sqlImportanceRating() . ','; } elseif( true == array_key_exists( 'ImportanceRating', $this->getChangedColumns() ) ) { $strSql .= ' importance_rating = ' . $this->sqlImportanceRating() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ranking = ' . $this->sqlRanking() . ','; } elseif( true == array_key_exists( 'Ranking', $this->getChangedColumns() ) ) { $strSql .= ' ranking = ' . $this->sqlRanking() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'lease_id' => $this->getLeaseId(),
			'roommate_interest_id' => $this->getRoommateInterestId(),
			'roommate_interest_option_id' => $this->getRoommateInterestOptionId(),
			'preference_datetime' => $this->getPreferenceDatetime(),
			'written_response' => $this->getWrittenResponse(),
			'importance_rating' => $this->getImportanceRating(),
			'ranking' => $this->getRanking(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>