<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocuments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocuments extends CEosPluralBase {

	/**
	 * @return CDocument[]
	 */
	public static function fetchDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDocument::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocument
	 */
	public static function fetchDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocument::class, $objDatabase );
	}

	public static function fetchDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'documents', $objDatabase );
	}

	public static function fetchDocumentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocument( sprintf( 'SELECT * FROM documents WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByDocumentTypeIdByCid( $intDocumentTypeId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE document_type_id = %d AND cid = %d', $intDocumentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByDocumentSubTypeIdByCid( $intDocumentSubTypeId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE document_sub_type_id = %d AND cid = %d', $intDocumentSubTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE file_type_id = %d AND cid = %d', $intFileTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE ar_trigger_id = %d AND cid = %d', $intArTriggerId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByOriginalDocumentIdByCid( $intOriginalDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE original_document_id = %d AND cid = %d', $intOriginalDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByRestoredDocumentIdByCid( $intRestoredDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE restored_document_id = %d AND cid = %d', $intRestoredDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByDocumentInclusionTypeIdByCid( $intDocumentInclusionTypeId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE document_inclusion_type_id = %d AND cid = %d', $intDocumentInclusionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE transmission_vendor_id = %d AND cid = %d', $intTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentsByLocaleDocumentIdByCid( $intLocaleDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocuments( sprintf( 'SELECT * FROM documents WHERE locale_document_id = %d AND cid = %d', $intLocaleDocumentId, $intCid ), $objDatabase );
	}

}
?>