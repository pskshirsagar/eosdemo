<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueUnitTypes extends CEosPluralBase {

	/**
	 * @return CRevenueUnitType[]
	 */
	public static function fetchRevenueUnitTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenueUnitType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueUnitType
	 */
	public static function fetchRevenueUnitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueUnitType::class, $objDatabase );
	}

	public static function fetchRevenueUnitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_unit_types', $objDatabase );
	}

	public static function fetchRevenueUnitTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitType( sprintf( 'SELECT * FROM revenue_unit_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypesByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypes( sprintf( 'SELECT * FROM revenue_unit_types WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypes( sprintf( 'SELECT * FROM revenue_unit_types WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypes( sprintf( 'SELECT * FROM revenue_unit_types WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

}
?>