<?php

class CBaseLoggedAction extends CEosSingularBase {

    protected $m_strSchemaName;
    protected $m_strTableName;
    protected $m_strUserName;
    protected $m_strActionTstamp;
    protected $m_strAction;
    protected $m_strOriginalData;
    protected $m_strNewData;
    protected $m_strQuery;
    protected $m_strClientIp;

    public function __construct() {
        parent::__construct();

        $this->m_strActionTstamp = 'now()';

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['schema_name'] ) && $boolDirectSet ) $this->m_strSchemaName = trim( stripcslashes( $arrValues['schema_name'] ) ); else if( isset( $arrValues['schema_name'] ) ) $this->setSchemaName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schema_name'] ) : $arrValues['schema_name'] );
        if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->m_strTableName = trim( stripcslashes( $arrValues['table_name'] ) ); else if( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
        if( isset( $arrValues['user_name'] ) && $boolDirectSet ) $this->m_strUserName = trim( stripcslashes( $arrValues['user_name'] ) ); else if( isset( $arrValues['user_name'] ) ) $this->setUserName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_name'] ) : $arrValues['user_name'] );
        if( isset( $arrValues['action_tstamp'] ) && $boolDirectSet ) $this->m_strActionTstamp = trim( $arrValues['action_tstamp'] ); else if( isset( $arrValues['action_tstamp'] ) ) $this->setActionTstamp( $arrValues['action_tstamp'] );
        if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->m_strAction = trim( stripcslashes( $arrValues['action'] ) ); else if( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
        if( isset( $arrValues['original_data'] ) && $boolDirectSet ) $this->m_strOriginalData = trim( stripcslashes( $arrValues['original_data'] ) ); else if( isset( $arrValues['original_data'] ) ) $this->setOriginalData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['original_data'] ) : $arrValues['original_data'] );
        if( isset( $arrValues['new_data'] ) && $boolDirectSet ) $this->m_strNewData = trim( stripcslashes( $arrValues['new_data'] ) ); else if( isset( $arrValues['new_data'] ) ) $this->setNewData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_data'] ) : $arrValues['new_data'] );
        if( isset( $arrValues['query'] ) && $boolDirectSet ) $this->m_strQuery = trim( stripcslashes( $arrValues['query'] ) ); else if( isset( $arrValues['query'] ) ) $this->setQuery( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['query'] ) : $arrValues['query'] );
        if( isset( $arrValues['client_ip'] ) && $boolDirectSet ) $this->m_strClientIp = trim( stripcslashes( $arrValues['client_ip'] ) ); else if( isset( $arrValues['client_ip'] ) ) $this->setClientIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_ip'] ) : $arrValues['client_ip'] );
    }

    public function setSchemaName( $strSchemaName ) {
        $this->m_strSchemaName = CStrings::strTrimDef( $strSchemaName, -1, NULL, true );
    }

    public function getSchemaName() {
        return $this->m_strSchemaName;
    }

    public function sqlSchemaName() {
        return ( true == isset( $this->m_strSchemaName ) ) ? '\'' . addslashes( $this->m_strSchemaName ) . '\'' : 'NULL';
    }

    public function setTableName( $strTableName ) {
        $this->m_strTableName = CStrings::strTrimDef( $strTableName, -1, NULL, true );
    }

    public function getTableName() {
        return $this->m_strTableName;
    }

    public function sqlTableName() {
        return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
    }

    public function setUserName( $strUserName ) {
        $this->m_strUserName = CStrings::strTrimDef( $strUserName, -1, NULL, true );
    }

    public function getUserName() {
        return $this->m_strUserName;
    }

    public function sqlUserName() {
        return ( true == isset( $this->m_strUserName ) ) ? '\'' . addslashes( $this->m_strUserName ) . '\'' : 'NULL';
    }

    public function setActionTstamp( $strActionTstamp ) {
        $this->m_strActionTstamp = CStrings::strTrimDef( $strActionTstamp, -1, NULL, true );
    }

    public function getActionTstamp() {
        return $this->m_strActionTstamp;
    }

    public function sqlActionTstamp() {
        return ( true == isset( $this->m_strActionTstamp ) ) ? '\'' . $this->m_strActionTstamp . '\'' : 'NOW()';
    }

    public function setAction( $strAction ) {
        $this->m_strAction = CStrings::strTrimDef( $strAction, -1, NULL, true );
    }

    public function getAction() {
        return $this->m_strAction;
    }

    public function sqlAction() {
        return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
    }

    public function setOriginalData( $strOriginalData ) {
        $this->m_strOriginalData = CStrings::strTrimDef( $strOriginalData, -1, NULL, true );
    }

    public function getOriginalData() {
        return $this->m_strOriginalData;
    }

    public function sqlOriginalData() {
        return ( true == isset( $this->m_strOriginalData ) ) ? '\'' . addslashes( $this->m_strOriginalData ) . '\'' : 'NULL';
    }

    public function setNewData( $strNewData ) {
        $this->m_strNewData = CStrings::strTrimDef( $strNewData, -1, NULL, true );
    }

    public function getNewData() {
        return $this->m_strNewData;
    }

    public function sqlNewData() {
        return ( true == isset( $this->m_strNewData ) ) ? '\'' . addslashes( $this->m_strNewData ) . '\'' : 'NULL';
    }

    public function setQuery( $strQuery ) {
        $this->m_strQuery = CStrings::strTrimDef( $strQuery, -1, NULL, true );
    }

    public function getQuery() {
        return $this->m_strQuery;
    }

    public function sqlQuery() {
        return ( true == isset( $this->m_strQuery ) ) ? '\'' . addslashes( $this->m_strQuery ) . '\'' : 'NULL';
    }

    public function setClientIp( $strClientIp ) {
        $this->m_strClientIp = CStrings::strTrimDef( $strClientIp, -1, NULL, true );
    }

    public function getClientIp() {
        return $this->m_strClientIp;
    }

    public function sqlClientIp() {
        return ( true == isset( $this->m_strClientIp ) ) ? '\'' . addslashes( $this->m_strClientIp ) . '\'' : 'NULL';
    }

    public function toArray() {
        return array(
        	'schema_name' => $this->getSchemaName(),
        	'table_name' => $this->getTableName(),
        	'user_name' => $this->getUserName(),
        	'action_tstamp' => $this->getActionTstamp(),
        	'action' => $this->getAction(),
        	'original_data' => $this->getOriginalData(),
        	'new_data' => $this->getNewData(),
        	'query' => $this->getQuery(),
        	'client_ip' => $this->getClientIp()
        );
    }

}
?>