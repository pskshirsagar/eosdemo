<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COneTimeLinks
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOneTimeLinks extends CEosPluralBase {

	/**
	 * @return COneTimeLink[]
	 */
	public static function fetchOneTimeLinks( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COneTimeLink', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COneTimeLink
	 */
	public static function fetchOneTimeLink( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COneTimeLink', $objDatabase );
	}

	public static function fetchOneTimeLinkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'one_time_links', $objDatabase );
	}

	public static function fetchOneTimeLinkByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOneTimeLink( sprintf( 'SELECT * FROM one_time_links WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOneTimeLinksByCid( $intCid, $objDatabase ) {
		return self::fetchOneTimeLinks( sprintf( 'SELECT * FROM one_time_links WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOneTimeLinksByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchOneTimeLinks( sprintf( 'SELECT * FROM one_time_links WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

}
?>