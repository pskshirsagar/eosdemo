<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCitizenshipEligibilityTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCitizenshipEligibilityTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCitizenshipEligibilityType[]
	 */
	public static function fetchSubsidyCitizenshipEligibilityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCitizenshipEligibilityType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCitizenshipEligibilityType
	 */
	public static function fetchSubsidyCitizenshipEligibilityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCitizenshipEligibilityType::class, $objDatabase );
	}

	public static function fetchSubsidyCitizenshipEligibilityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_citizenship_eligibility_types', $objDatabase );
	}

	public static function fetchSubsidyCitizenshipEligibilityTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCitizenshipEligibilityType( sprintf( 'SELECT * FROM subsidy_citizenship_eligibility_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>