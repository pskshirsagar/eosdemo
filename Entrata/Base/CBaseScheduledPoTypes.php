<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPoTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduledPoTypes extends CEosPluralBase {

	/**
	 * @return CScheduledPoType[]
	 */
	public static function fetchScheduledPoTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledPoType::class, $objDatabase );
	}

	/**
	 * @return CScheduledPoType
	 */
	public static function fetchScheduledPoType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledPoType::class, $objDatabase );
	}

	public static function fetchScheduledPoTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_po_types', $objDatabase );
	}

	public static function fetchScheduledPoTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledPoType( sprintf( 'SELECT * FROM scheduled_po_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>