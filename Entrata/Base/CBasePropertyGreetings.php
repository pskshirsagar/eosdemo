<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGreetings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGreetings extends CEosPluralBase {

	/**
	 * @return CPropertyGreeting[]
	 */
	public static function fetchPropertyGreetings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyGreeting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGreeting
	 */
	public static function fetchPropertyGreeting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyGreeting', $objDatabase );
	}

	public static function fetchPropertyGreetingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_greetings', $objDatabase );
	}

	public static function fetchPropertyGreetingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGreeting( sprintf( 'SELECT * FROM property_greetings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGreetingsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGreetings( sprintf( 'SELECT * FROM property_greetings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGreetingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyGreetings( sprintf( 'SELECT * FROM property_greetings WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGreetingsByPropertyGreetingTypeIdByCid( $intPropertyGreetingTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGreetings( sprintf( 'SELECT * FROM property_greetings WHERE property_greeting_type_id = %d AND cid = %d', ( int ) $intPropertyGreetingTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGreetingsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchPropertyGreetings( sprintf( 'SELECT * FROM property_greetings WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>