<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenancePriorities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenancePriorities extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenancePriority[]
	 */
	public static function fetchPropertyMaintenancePriorities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyMaintenancePriority::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenancePriority
	 */
	public static function fetchPropertyMaintenancePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyMaintenancePriority::class, $objDatabase );
	}

	public static function fetchPropertyMaintenancePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_priorities', $objDatabase );
	}

	public static function fetchPropertyMaintenancePriorityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriority( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByMaintenancePriorityIdByCid( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE maintenance_priority_id = %d AND cid = %d', $intMaintenancePriorityId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByResponseIntervalTypeIdByCid( $intResponseIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE response_interval_type_id = %d AND cid = %d', $intResponseIntervalTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByDueIntervalTypeIdByCid( $intDueIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE due_interval_type_id = %d AND cid = %d', $intDueIntervalTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenancePrioritiesByMaintenanceHourTypeIdByCid( $intMaintenanceHourTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenancePriorities( sprintf( 'SELECT * FROM property_maintenance_priorities WHERE maintenance_hour_type_id = %d AND cid = %d', $intMaintenanceHourTypeId, $intCid ), $objDatabase );
	}

}
?>