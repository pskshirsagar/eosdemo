<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankCsvColumnMapping extends CEosSingularBase {

	const TABLE_NAME = 'public.bank_csv_column_mappings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBankAccountId;
	protected $m_strMappingName;
	protected $m_intDateColumn;
	protected $m_intTransactionTypeColumn;
	protected $m_intDescriptionColumn;
	protected $m_intAmountColumn;
	protected $m_intTransactionNumberColumn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['mapping_name'] ) && $boolDirectSet ) $this->set( 'm_strMappingName', trim( $arrValues['mapping_name'] ) ); elseif( isset( $arrValues['mapping_name'] ) ) $this->setMappingName( $arrValues['mapping_name'] );
		if( isset( $arrValues['date_column'] ) && $boolDirectSet ) $this->set( 'm_intDateColumn', trim( $arrValues['date_column'] ) ); elseif( isset( $arrValues['date_column'] ) ) $this->setDateColumn( $arrValues['date_column'] );
		if( isset( $arrValues['transaction_type_column'] ) && $boolDirectSet ) $this->set( 'm_intTransactionTypeColumn', trim( $arrValues['transaction_type_column'] ) ); elseif( isset( $arrValues['transaction_type_column'] ) ) $this->setTransactionTypeColumn( $arrValues['transaction_type_column'] );
		if( isset( $arrValues['description_column'] ) && $boolDirectSet ) $this->set( 'm_intDescriptionColumn', trim( $arrValues['description_column'] ) ); elseif( isset( $arrValues['description_column'] ) ) $this->setDescriptionColumn( $arrValues['description_column'] );
		if( isset( $arrValues['amount_column'] ) && $boolDirectSet ) $this->set( 'm_intAmountColumn', trim( $arrValues['amount_column'] ) ); elseif( isset( $arrValues['amount_column'] ) ) $this->setAmountColumn( $arrValues['amount_column'] );
		if( isset( $arrValues['transaction_number_column'] ) && $boolDirectSet ) $this->set( 'm_intTransactionNumberColumn', trim( $arrValues['transaction_number_column'] ) ); elseif( isset( $arrValues['transaction_number_column'] ) ) $this->setTransactionNumberColumn( $arrValues['transaction_number_column'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setMappingName( $strMappingName ) {
		$this->set( 'm_strMappingName', CStrings::strTrimDef( $strMappingName, 50, NULL, true ) );
	}

	public function getMappingName() {
		return $this->m_strMappingName;
	}

	public function sqlMappingName() {
		return ( true == isset( $this->m_strMappingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMappingName ) : '\'' . addslashes( $this->m_strMappingName ) . '\'' ) : 'NULL';
	}

	public function setDateColumn( $intDateColumn ) {
		$this->set( 'm_intDateColumn', CStrings::strToIntDef( $intDateColumn, NULL, false ) );
	}

	public function getDateColumn() {
		return $this->m_intDateColumn;
	}

	public function sqlDateColumn() {
		return ( true == isset( $this->m_intDateColumn ) ) ? ( string ) $this->m_intDateColumn : 'NULL';
	}

	public function setTransactionTypeColumn( $intTransactionTypeColumn ) {
		$this->set( 'm_intTransactionTypeColumn', CStrings::strToIntDef( $intTransactionTypeColumn, NULL, false ) );
	}

	public function getTransactionTypeColumn() {
		return $this->m_intTransactionTypeColumn;
	}

	public function sqlTransactionTypeColumn() {
		return ( true == isset( $this->m_intTransactionTypeColumn ) ) ? ( string ) $this->m_intTransactionTypeColumn : 'NULL';
	}

	public function setDescriptionColumn( $intDescriptionColumn ) {
		$this->set( 'm_intDescriptionColumn', CStrings::strToIntDef( $intDescriptionColumn, NULL, false ) );
	}

	public function getDescriptionColumn() {
		return $this->m_intDescriptionColumn;
	}

	public function sqlDescriptionColumn() {
		return ( true == isset( $this->m_intDescriptionColumn ) ) ? ( string ) $this->m_intDescriptionColumn : 'NULL';
	}

	public function setAmountColumn( $intAmountColumn ) {
		$this->set( 'm_intAmountColumn', CStrings::strToIntDef( $intAmountColumn, NULL, false ) );
	}

	public function getAmountColumn() {
		return $this->m_intAmountColumn;
	}

	public function sqlAmountColumn() {
		return ( true == isset( $this->m_intAmountColumn ) ) ? ( string ) $this->m_intAmountColumn : 'NULL';
	}

	public function setTransactionNumberColumn( $intTransactionNumberColumn ) {
		$this->set( 'm_intTransactionNumberColumn', CStrings::strToIntDef( $intTransactionNumberColumn, NULL, false ) );
	}

	public function getTransactionNumberColumn() {
		return $this->m_intTransactionNumberColumn;
	}

	public function sqlTransactionNumberColumn() {
		return ( true == isset( $this->m_intTransactionNumberColumn ) ) ? ( string ) $this->m_intTransactionNumberColumn : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, bank_account_id, mapping_name, date_column, transaction_type_column, description_column, amount_column, transaction_number_column, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlMappingName() . ', ' .
						$this->sqlDateColumn() . ', ' .
						$this->sqlTransactionTypeColumn() . ', ' .
						$this->sqlDescriptionColumn() . ', ' .
						$this->sqlAmountColumn() . ', ' .
						$this->sqlTransactionNumberColumn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_name = ' . $this->sqlMappingName(). ',' ; } elseif( true == array_key_exists( 'MappingName', $this->getChangedColumns() ) ) { $strSql .= ' mapping_name = ' . $this->sqlMappingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_column = ' . $this->sqlDateColumn(). ',' ; } elseif( true == array_key_exists( 'DateColumn', $this->getChangedColumns() ) ) { $strSql .= ' date_column = ' . $this->sqlDateColumn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_type_column = ' . $this->sqlTransactionTypeColumn(). ',' ; } elseif( true == array_key_exists( 'TransactionTypeColumn', $this->getChangedColumns() ) ) { $strSql .= ' transaction_type_column = ' . $this->sqlTransactionTypeColumn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description_column = ' . $this->sqlDescriptionColumn(). ',' ; } elseif( true == array_key_exists( 'DescriptionColumn', $this->getChangedColumns() ) ) { $strSql .= ' description_column = ' . $this->sqlDescriptionColumn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_column = ' . $this->sqlAmountColumn(). ',' ; } elseif( true == array_key_exists( 'AmountColumn', $this->getChangedColumns() ) ) { $strSql .= ' amount_column = ' . $this->sqlAmountColumn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_number_column = ' . $this->sqlTransactionNumberColumn(). ',' ; } elseif( true == array_key_exists( 'TransactionNumberColumn', $this->getChangedColumns() ) ) { $strSql .= ' transaction_number_column = ' . $this->sqlTransactionNumberColumn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'bank_account_id' => $this->getBankAccountId(),
			'mapping_name' => $this->getMappingName(),
			'date_column' => $this->getDateColumn(),
			'transaction_type_column' => $this->getTransactionTypeColumn(),
			'description_column' => $this->getDescriptionColumn(),
			'amount_column' => $this->getAmountColumn(),
			'transaction_number_column' => $this->getTransactionNumberColumn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>