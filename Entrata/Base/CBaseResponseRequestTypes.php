<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResponseRequestTypes
 * Do not add any new functions to this class.
 */

class CBaseResponseRequestTypes extends CEosPluralBase {

	/**
	 * @return CResponseRequestType[]
	 */
	public static function fetchResponseRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CResponseRequestType::class, $objDatabase );
	}

	/**
	 * @return CResponseRequestType
	 */
	public static function fetchResponseRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CResponseRequestType::class, $objDatabase );
	}

	public static function fetchResponseRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'response_request_types', $objDatabase );
	}

	public static function fetchResponseRequestTypeById( $intId, $objDatabase ) {
		return self::fetchResponseRequestType( sprintf( 'SELECT * FROM response_request_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>