<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBlogPost extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.blog_posts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWordpressPostId;
	protected $m_intBlogPostStatusTypeId;
	protected $m_strTitle;
	protected $m_strUrl;
	protected $m_strUrlCustomExtension;
	protected $m_strMetaDescription;
	protected $m_strMetaKeywords;
	protected $m_strArticle;
	protected $m_strAuthorName;
	protected $m_boolIsFeatured;
	protected $m_boolIsDisabled;
	protected $m_boolIsCommentAllowed;
	protected $m_strScheduledOn;
	protected $m_strPublishedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFeatured = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsCommentAllowed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['wordpress_post_id'] ) && $boolDirectSet ) $this->set( 'm_intWordpressPostId', trim( $arrValues['wordpress_post_id'] ) ); elseif( isset( $arrValues['wordpress_post_id'] ) ) $this->setWordpressPostId( $arrValues['wordpress_post_id'] );
		if( isset( $arrValues['blog_post_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBlogPostStatusTypeId', trim( $arrValues['blog_post_status_type_id'] ) ); elseif( isset( $arrValues['blog_post_status_type_id'] ) ) $this->setBlogPostStatusTypeId( $arrValues['blog_post_status_type_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( $arrValues['url'] ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( $arrValues['url'] );
		if( isset( $arrValues['url_custom_extension'] ) && $boolDirectSet ) $this->set( 'm_strUrlCustomExtension', trim( $arrValues['url_custom_extension'] ) ); elseif( isset( $arrValues['url_custom_extension'] ) ) $this->setUrlCustomExtension( $arrValues['url_custom_extension'] );
		if( isset( $arrValues['meta_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMetaDescription', trim( $arrValues['meta_description'] ) ); elseif( isset( $arrValues['meta_description'] ) ) $this->setMetaDescription( $arrValues['meta_description'] );
		if( isset( $arrValues['meta_keywords'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMetaKeywords', trim( $arrValues['meta_keywords'] ) ); elseif( isset( $arrValues['meta_keywords'] ) ) $this->setMetaKeywords( $arrValues['meta_keywords'] );
		if( isset( $arrValues['article'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strArticle', trim( $arrValues['article'] ) ); elseif( isset( $arrValues['article'] ) ) $this->setArticle( $arrValues['article'] );
		if( isset( $arrValues['author_name'] ) && $boolDirectSet ) $this->set( 'm_strAuthorName', trim( $arrValues['author_name'] ) ); elseif( isset( $arrValues['author_name'] ) ) $this->setAuthorName( $arrValues['author_name'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_comment_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommentAllowed', trim( stripcslashes( $arrValues['is_comment_allowed'] ) ) ); elseif( isset( $arrValues['is_comment_allowed'] ) ) $this->setIsCommentAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_comment_allowed'] ) : $arrValues['is_comment_allowed'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWordpressPostId( $intWordpressPostId ) {
		$this->set( 'm_intWordpressPostId', CStrings::strToIntDef( $intWordpressPostId, NULL, false ) );
	}

	public function getWordpressPostId() {
		return $this->m_intWordpressPostId;
	}

	public function sqlWordpressPostId() {
		return ( true == isset( $this->m_intWordpressPostId ) ) ? ( string ) $this->m_intWordpressPostId : 'NULL';
	}

	public function setBlogPostStatusTypeId( $intBlogPostStatusTypeId ) {
		$this->set( 'm_intBlogPostStatusTypeId', CStrings::strToIntDef( $intBlogPostStatusTypeId, NULL, false ) );
	}

	public function getBlogPostStatusTypeId() {
		return $this->m_intBlogPostStatusTypeId;
	}

	public function sqlBlogPostStatusTypeId() {
		return ( true == isset( $this->m_intBlogPostStatusTypeId ) ) ? ( string ) $this->m_intBlogPostStatusTypeId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 256, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 256, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrl ) : '\'' . addslashes( $this->m_strUrl ) . '\'' ) : 'NULL';
	}

	public function setUrlCustomExtension( $strUrlCustomExtension ) {
		$this->set( 'm_strUrlCustomExtension', CStrings::strTrimDef( $strUrlCustomExtension, 128, NULL, true ) );
	}

	public function getUrlCustomExtension() {
		return $this->m_strUrlCustomExtension;
	}

	public function sqlUrlCustomExtension() {
		return ( true == isset( $this->m_strUrlCustomExtension ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrlCustomExtension ) : '\'' . addslashes( $this->m_strUrlCustomExtension ) . '\'' ) : 'NULL';
	}

	public function setMetaDescription( $strMetaDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMetaDescription', CStrings::strTrimDef( $strMetaDescription, 320, NULL, true ), $strLocaleCode );
	}

	public function getMetaDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMetaDescription', $strLocaleCode );
	}

	public function sqlMetaDescription() {
		return ( true == isset( $this->m_strMetaDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMetaDescription ) : '\'' . addslashes( $this->m_strMetaDescription ) . '\'' ) : 'NULL';
	}

	public function setMetaKeywords( $strMetaKeywords, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMetaKeywords', CStrings::strTrimDef( $strMetaKeywords, 256, NULL, true ), $strLocaleCode );
	}

	public function getMetaKeywords( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMetaKeywords', $strLocaleCode );
	}

	public function sqlMetaKeywords() {
		return ( true == isset( $this->m_strMetaKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMetaKeywords ) : '\'' . addslashes( $this->m_strMetaKeywords ) . '\'' ) : 'NULL';
	}

	public function setArticle( $strArticle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strArticle', CStrings::strTrimDef( $strArticle, -1, NULL, true ), $strLocaleCode );
	}

	public function getArticle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strArticle', $strLocaleCode );
	}

	public function sqlArticle() {
		return ( true == isset( $this->m_strArticle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strArticle ) : '\'' . addslashes( $this->m_strArticle ) . '\'' ) : 'NULL';
	}

	public function setAuthorName( $strAuthorName ) {
		$this->set( 'm_strAuthorName', CStrings::strTrimDef( $strAuthorName, 50, NULL, true ) );
	}

	public function getAuthorName() {
		return $this->m_strAuthorName;
	}

	public function sqlAuthorName() {
		return ( true == isset( $this->m_strAuthorName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAuthorName ) : '\'' . addslashes( $this->m_strAuthorName ) . '\'' ) : 'NULL';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCommentAllowed( $boolIsCommentAllowed ) {
		$this->set( 'm_boolIsCommentAllowed', CStrings::strToBool( $boolIsCommentAllowed ) );
	}

	public function getIsCommentAllowed() {
		return $this->m_boolIsCommentAllowed;
	}

	public function sqlIsCommentAllowed() {
		return ( true == isset( $this->m_boolIsCommentAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommentAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, wordpress_post_id, blog_post_status_type_id, title, url, url_custom_extension, meta_description, meta_keywords, article, author_name, is_featured, is_disabled, is_comment_allowed, scheduled_on, published_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlWordpressPostId() . ', ' .
						$this->sqlBlogPostStatusTypeId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlUrlCustomExtension() . ', ' .
						$this->sqlMetaDescription() . ', ' .
						$this->sqlMetaKeywords() . ', ' .
						$this->sqlArticle() . ', ' .
						$this->sqlAuthorName() . ', ' .
						$this->sqlIsFeatured() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsCommentAllowed() . ', ' .
						$this->sqlScheduledOn() . ', ' .
						$this->sqlPublishedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wordpress_post_id = ' . $this->sqlWordpressPostId(). ',' ; } elseif( true == array_key_exists( 'WordpressPostId', $this->getChangedColumns() ) ) { $strSql .= ' wordpress_post_id = ' . $this->sqlWordpressPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blog_post_status_type_id = ' . $this->sqlBlogPostStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'BlogPostStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' blog_post_status_type_id = ' . $this->sqlBlogPostStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url_custom_extension = ' . $this->sqlUrlCustomExtension(). ',' ; } elseif( true == array_key_exists( 'UrlCustomExtension', $this->getChangedColumns() ) ) { $strSql .= ' url_custom_extension = ' . $this->sqlUrlCustomExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription(). ',' ; } elseif( true == array_key_exists( 'MetaDescription', $this->getChangedColumns() ) ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords(). ',' ; } elseif( true == array_key_exists( 'MetaKeywords', $this->getChangedColumns() ) ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' article = ' . $this->sqlArticle(). ',' ; } elseif( true == array_key_exists( 'Article', $this->getChangedColumns() ) ) { $strSql .= ' article = ' . $this->sqlArticle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' author_name = ' . $this->sqlAuthorName(). ',' ; } elseif( true == array_key_exists( 'AuthorName', $this->getChangedColumns() ) ) { $strSql .= ' author_name = ' . $this->sqlAuthorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured(). ',' ; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_comment_allowed = ' . $this->sqlIsCommentAllowed(). ',' ; } elseif( true == array_key_exists( 'IsCommentAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_comment_allowed = ' . $this->sqlIsCommentAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn(). ',' ; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'wordpress_post_id' => $this->getWordpressPostId(),
			'blog_post_status_type_id' => $this->getBlogPostStatusTypeId(),
			'title' => $this->getTitle(),
			'url' => $this->getUrl(),
			'url_custom_extension' => $this->getUrlCustomExtension(),
			'meta_description' => $this->getMetaDescription(),
			'meta_keywords' => $this->getMetaKeywords(),
			'article' => $this->getArticle(),
			'author_name' => $this->getAuthorName(),
			'is_featured' => $this->getIsFeatured(),
			'is_disabled' => $this->getIsDisabled(),
			'is_comment_allowed' => $this->getIsCommentAllowed(),
			'scheduled_on' => $this->getScheduledOn(),
			'published_on' => $this->getPublishedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>