<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApplicationCustomerDataType extends CEosSingularBase {

	const TABLE_NAME = 'public.property_application_customer_data_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyApplicationId;
	protected $m_intCustomerDataTypeId;
	protected $m_arrintVerificationTypeIds;
	protected $m_boolIsRequired;
	protected $m_boolIsVerificationRequired;
	protected $m_boolAllowNotes;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRequired = false;
		$this->m_boolIsVerificationRequired = false;
		$this->m_boolAllowNotes = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyApplicationId', trim( $arrValues['company_application_id'] ) ); elseif( isset( $arrValues['company_application_id'] ) ) $this->setCompanyApplicationId( $arrValues['company_application_id'] );
		if( isset( $arrValues['customer_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataTypeId', trim( $arrValues['customer_data_type_id'] ) ); elseif( isset( $arrValues['customer_data_type_id'] ) ) $this->setCustomerDataTypeId( $arrValues['customer_data_type_id'] );
		if( isset( $arrValues['verification_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintVerificationTypeIds', trim( $arrValues['verification_type_ids'] ) ); elseif( isset( $arrValues['verification_type_ids'] ) ) $this->setVerificationTypeIds( $arrValues['verification_type_ids'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequired', trim( stripcslashes( $arrValues['is_required'] ) ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required'] ) : $arrValues['is_required'] );
		if( isset( $arrValues['is_verification_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerificationRequired', trim( stripcslashes( $arrValues['is_verification_required'] ) ) ); elseif( isset( $arrValues['is_verification_required'] ) ) $this->setIsVerificationRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verification_required'] ) : $arrValues['is_verification_required'] );
		if( isset( $arrValues['allow_notes'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotes', trim( stripcslashes( $arrValues['allow_notes'] ) ) ); elseif( isset( $arrValues['allow_notes'] ) ) $this->setAllowNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_notes'] ) : $arrValues['allow_notes'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->set( 'm_intCompanyApplicationId', CStrings::strToIntDef( $intCompanyApplicationId, NULL, false ) );
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function sqlCompanyApplicationId() {
		return ( true == isset( $this->m_intCompanyApplicationId ) ) ? ( string ) $this->m_intCompanyApplicationId : 'NULL';
	}

	public function setCustomerDataTypeId( $intCustomerDataTypeId ) {
		$this->set( 'm_intCustomerDataTypeId', CStrings::strToIntDef( $intCustomerDataTypeId, NULL, false ) );
	}

	public function getCustomerDataTypeId() {
		return $this->m_intCustomerDataTypeId;
	}

	public function sqlCustomerDataTypeId() {
		return ( true == isset( $this->m_intCustomerDataTypeId ) ) ? ( string ) $this->m_intCustomerDataTypeId : 'NULL';
	}

	public function setVerificationTypeIds( $arrintVerificationTypeIds ) {
		$this->set( 'm_arrintVerificationTypeIds', CStrings::strToArrIntDef( $arrintVerificationTypeIds, NULL ) );
	}

	public function getVerificationTypeIds() {
		return $this->m_arrintVerificationTypeIds;
	}

	public function sqlVerificationTypeIds() {
		return ( true == isset( $this->m_arrintVerificationTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintVerificationTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->set( 'm_boolIsRequired', CStrings::strToBool( $boolIsRequired ) );
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_boolIsRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsVerificationRequired( $boolIsVerificationRequired ) {
		$this->set( 'm_boolIsVerificationRequired', CStrings::strToBool( $boolIsVerificationRequired ) );
	}

	public function getIsVerificationRequired() {
		return $this->m_boolIsVerificationRequired;
	}

	public function sqlIsVerificationRequired() {
		return ( true == isset( $this->m_boolIsVerificationRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerificationRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotes( $boolAllowNotes ) {
		$this->set( 'm_boolAllowNotes', CStrings::strToBool( $boolAllowNotes ) );
	}

	public function getAllowNotes() {
		return $this->m_boolAllowNotes;
	}

	public function sqlAllowNotes() {
		return ( true == isset( $this->m_boolAllowNotes ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotes ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, company_application_id, customer_data_type_id, verification_type_ids, is_required, is_verification_required, allow_notes, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompanyApplicationId() . ', ' .
 						$this->sqlCustomerDataTypeId() . ', ' .
 						$this->sqlVerificationTypeIds() . ', ' .
 						$this->sqlIsRequired() . ', ' .
 						$this->sqlIsVerificationRequired() . ', ' .
 						$this->sqlAllowNotes() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; } elseif( true == array_key_exists( 'CompanyApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; } elseif( true == array_key_exists( 'CustomerDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verification_type_ids = ' . $this->sqlVerificationTypeIds() . ','; } elseif( true == array_key_exists( 'VerificationTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' verification_type_ids = ' . $this->sqlVerificationTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verification_required = ' . $this->sqlIsVerificationRequired() . ','; } elseif( true == array_key_exists( 'IsVerificationRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_verification_required = ' . $this->sqlIsVerificationRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_notes = ' . $this->sqlAllowNotes() . ','; } elseif( true == array_key_exists( 'AllowNotes', $this->getChangedColumns() ) ) { $strSql .= ' allow_notes = ' . $this->sqlAllowNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_application_id' => $this->getCompanyApplicationId(),
			'customer_data_type_id' => $this->getCustomerDataTypeId(),
			'verification_type_ids' => $this->getVerificationTypeIds(),
			'is_required' => $this->getIsRequired(),
			'is_verification_required' => $this->getIsVerificationRequired(),
			'allow_notes' => $this->getAllowNotes(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>