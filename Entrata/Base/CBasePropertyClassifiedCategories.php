<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyClassifiedCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyClassifiedCategories extends CEosPluralBase {

	/**
	 * @return CPropertyClassifiedCategory[]
	 */
	public static function fetchPropertyClassifiedCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyClassifiedCategory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyClassifiedCategory
	 */
	public static function fetchPropertyClassifiedCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyClassifiedCategory', $objDatabase );
	}

	public static function fetchPropertyClassifiedCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_classified_categories', $objDatabase );
	}

	public static function fetchPropertyClassifiedCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyClassifiedCategory( sprintf( 'SELECT * FROM property_classified_categories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyClassifiedCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyClassifiedCategories( sprintf( 'SELECT * FROM property_classified_categories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyClassifiedCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyClassifiedCategories( sprintf( 'SELECT * FROM property_classified_categories WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyClassifiedCategoriesByCompanyClassifiedCategoryIdByCid( $intCompanyClassifiedCategoryId, $intCid, $objDatabase ) {
		return self::fetchPropertyClassifiedCategories( sprintf( 'SELECT * FROM property_classified_categories WHERE company_classified_category_id = %d AND cid = %d', ( int ) $intCompanyClassifiedCategoryId, ( int ) $intCid ), $objDatabase );
	}

}
?>