<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallCustomerTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCallCustomerTypes extends CEosPluralBase {

	/**
	 * @return CScheduledCallCustomerType[]
	 */
	public static function fetchScheduledCallCustomerTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledCallCustomerType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledCallCustomerType
	 */
	public static function fetchScheduledCallCustomerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledCallCustomerType', $objDatabase );
	}

	public static function fetchScheduledCallCustomerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_call_customer_types', $objDatabase );
	}

	public static function fetchScheduledCallCustomerTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallCustomerType( sprintf( 'SELECT * FROM scheduled_call_customer_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallCustomerTypesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledCallCustomerTypes( sprintf( 'SELECT * FROM scheduled_call_customer_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallCustomerTypesByScheduledCallIdByCid( $intScheduledCallId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallCustomerTypes( sprintf( 'SELECT * FROM scheduled_call_customer_types WHERE scheduled_call_id = %d AND cid = %d', ( int ) $intScheduledCallId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallCustomerTypesByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallCustomerTypes( sprintf( 'SELECT * FROM scheduled_call_customer_types WHERE lease_status_type_id = %d AND cid = %d', ( int ) $intLeaseStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>