<?php

class CBaseGlExportFormatTypes extends CEosPluralBase {

	/**
	 * @return CGlExportFormatType[]
	 */
	public static function fetchGlExportFormatTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlExportFormatType::class, $objDatabase );
	}

	/**
	 * @return CGlExportFormatType
	 */
	public static function fetchGlExportFormatType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlExportFormatType::class, $objDatabase );
	}

	public static function fetchGlExportFormatTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_export_format_types', $objDatabase );
	}

	public static function fetchGlExportFormatTypeById( $intId, $objDatabase ) {
		return self::fetchGlExportFormatType( sprintf( 'SELECT * FROM gl_export_format_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>