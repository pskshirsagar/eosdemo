<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.unit_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strIlsRemotePrimaryKey;
	protected $m_strVaultwareKey;
	protected $m_strName;
	protected $m_strLookupCode;
	protected $m_strDescription;
	protected $m_fltMarketRent;
	protected $m_fltInitialRent;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intNumberOfUnits;
	protected $m_intNumberOfFloors;
	protected $m_intNumberOfRooms;
	protected $m_intNumberOfBedrooms;
	protected $m_strBedroomDescription;
	protected $m_fltNumberOfBathrooms;
	protected $m_strBathroomDescription;
	protected $m_strAppliancesDescription;
	protected $m_intMaximumOccupants;
	protected $m_intDefaultLeaseTermMonths;
	protected $m_strNotes;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intBlockIntegration;
	protected $m_intBlockPricingIntegration;
	protected $m_intIsOccupiable;
	protected $m_intIsFurnished;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_strExportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intBlockIntegration = '0';
		$this->m_intBlockPricingIntegration = '0';
		$this->m_intIsOccupiable = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['ils_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strIlsRemotePrimaryKey', trim( stripcslashes( $arrValues['ils_remote_primary_key'] ) ) ); elseif( isset( $arrValues['ils_remote_primary_key'] ) ) $this->setIlsRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ils_remote_primary_key'] ) : $arrValues['ils_remote_primary_key'] );
		if( isset( $arrValues['vaultware_key'] ) && $boolDirectSet ) $this->set( 'm_strVaultwareKey', trim( stripcslashes( $arrValues['vaultware_key'] ) ) ); elseif( isset( $arrValues['vaultware_key'] ) ) $this->setVaultwareKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vaultware_key'] ) : $arrValues['vaultware_key'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( stripcslashes( $arrValues['lookup_code'] ) ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['initial_rent'] ) && $boolDirectSet ) $this->set( 'm_fltInitialRent', trim( $arrValues['initial_rent'] ) ); elseif( isset( $arrValues['initial_rent'] ) ) $this->setInitialRent( $arrValues['initial_rent'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['number_of_floors'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfFloors', trim( $arrValues['number_of_floors'] ) ); elseif( isset( $arrValues['number_of_floors'] ) ) $this->setNumberOfFloors( $arrValues['number_of_floors'] );
		if( isset( $arrValues['number_of_rooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfRooms', trim( $arrValues['number_of_rooms'] ) ); elseif( isset( $arrValues['number_of_rooms'] ) ) $this->setNumberOfRooms( $arrValues['number_of_rooms'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['bedroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBedroomDescription', trim( stripcslashes( $arrValues['bedroom_description'] ) ) ); elseif( isset( $arrValues['bedroom_description'] ) ) $this->setBedroomDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bedroom_description'] ) : $arrValues['bedroom_description'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['bathroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBathroomDescription', trim( stripcslashes( $arrValues['bathroom_description'] ) ) ); elseif( isset( $arrValues['bathroom_description'] ) ) $this->setBathroomDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bathroom_description'] ) : $arrValues['bathroom_description'] );
		if( isset( $arrValues['appliances_description'] ) && $boolDirectSet ) $this->set( 'm_strAppliancesDescription', trim( stripcslashes( $arrValues['appliances_description'] ) ) ); elseif( isset( $arrValues['appliances_description'] ) ) $this->setAppliancesDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['appliances_description'] ) : $arrValues['appliances_description'] );
		if( isset( $arrValues['maximum_occupants'] ) && $boolDirectSet ) $this->set( 'm_intMaximumOccupants', trim( $arrValues['maximum_occupants'] ) ); elseif( isset( $arrValues['maximum_occupants'] ) ) $this->setMaximumOccupants( $arrValues['maximum_occupants'] );
		if( isset( $arrValues['default_lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseTermMonths', trim( $arrValues['default_lease_term_months'] ) ); elseif( isset( $arrValues['default_lease_term_months'] ) ) $this->setDefaultLeaseTermMonths( $arrValues['default_lease_term_months'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['block_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockIntegration', trim( $arrValues['block_integration'] ) ); elseif( isset( $arrValues['block_integration'] ) ) $this->setBlockIntegration( $arrValues['block_integration'] );
		if( isset( $arrValues['block_pricing_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockPricingIntegration', trim( $arrValues['block_pricing_integration'] ) ); elseif( isset( $arrValues['block_pricing_integration'] ) ) $this->setBlockPricingIntegration( $arrValues['block_pricing_integration'] );
		if( isset( $arrValues['is_occupiable'] ) && $boolDirectSet ) $this->set( 'm_intIsOccupiable', trim( $arrValues['is_occupiable'] ) ); elseif( isset( $arrValues['is_occupiable'] ) ) $this->setIsOccupiable( $arrValues['is_occupiable'] );
		if( isset( $arrValues['is_furnished'] ) && $boolDirectSet ) $this->set( 'm_intIsFurnished', trim( $arrValues['is_furnished'] ) ); elseif( isset( $arrValues['is_furnished'] ) ) $this->setIsFurnished( $arrValues['is_furnished'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setIlsRemotePrimaryKey( $strIlsRemotePrimaryKey ) {
		$this->set( 'm_strIlsRemotePrimaryKey', CStrings::strTrimDef( $strIlsRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getIlsRemotePrimaryKey() {
		return $this->m_strIlsRemotePrimaryKey;
	}

	public function sqlIlsRemotePrimaryKey() {
		return ( true == isset( $this->m_strIlsRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strIlsRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setVaultwareKey( $strVaultwareKey ) {
		$this->set( 'm_strVaultwareKey', CStrings::strTrimDef( $strVaultwareKey, 64, NULL, true ) );
	}

	public function getVaultwareKey() {
		return $this->m_strVaultwareKey;
	}

	public function sqlVaultwareKey() {
		return ( true == isset( $this->m_strVaultwareKey ) ) ? '\'' . addslashes( $this->m_strVaultwareKey ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 240, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? '\'' . addslashes( $this->m_strLookupCode ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : 'NULL';
	}

	public function setInitialRent( $fltInitialRent ) {
		$this->set( 'm_fltInitialRent', CStrings::strToFloatDef( $fltInitialRent, NULL, false, 2 ) );
	}

	public function getInitialRent() {
		return $this->m_fltInitialRent;
	}

	public function sqlInitialRent() {
		return ( true == isset( $this->m_fltInitialRent ) ) ? ( string ) $this->m_fltInitialRent : 'NULL';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setNumberOfFloors( $intNumberOfFloors ) {
		$this->set( 'm_intNumberOfFloors', CStrings::strToIntDef( $intNumberOfFloors, NULL, false ) );
	}

	public function getNumberOfFloors() {
		return $this->m_intNumberOfFloors;
	}

	public function sqlNumberOfFloors() {
		return ( true == isset( $this->m_intNumberOfFloors ) ) ? ( string ) $this->m_intNumberOfFloors : 'NULL';
	}

	public function setNumberOfRooms( $intNumberOfRooms ) {
		$this->set( 'm_intNumberOfRooms', CStrings::strToIntDef( $intNumberOfRooms, NULL, false ) );
	}

	public function getNumberOfRooms() {
		return $this->m_intNumberOfRooms;
	}

	public function sqlNumberOfRooms() {
		return ( true == isset( $this->m_intNumberOfRooms ) ) ? ( string ) $this->m_intNumberOfRooms : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setBedroomDescription( $strBedroomDescription ) {
		$this->set( 'm_strBedroomDescription', CStrings::strTrimDef( $strBedroomDescription, 240, NULL, true ) );
	}

	public function getBedroomDescription() {
		return $this->m_strBedroomDescription;
	}

	public function sqlBedroomDescription() {
		return ( true == isset( $this->m_strBedroomDescription ) ) ? '\'' . addslashes( $this->m_strBedroomDescription ) . '\'' : 'NULL';
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->set( 'm_fltNumberOfBathrooms', CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_fltNumberOfBathrooms ) ) ? ( string ) $this->m_fltNumberOfBathrooms : 'NULL';
	}

	public function setBathroomDescription( $strBathroomDescription ) {
		$this->set( 'm_strBathroomDescription', CStrings::strTrimDef( $strBathroomDescription, 240, NULL, true ) );
	}

	public function getBathroomDescription() {
		return $this->m_strBathroomDescription;
	}

	public function sqlBathroomDescription() {
		return ( true == isset( $this->m_strBathroomDescription ) ) ? '\'' . addslashes( $this->m_strBathroomDescription ) . '\'' : 'NULL';
	}

	public function setAppliancesDescription( $strAppliancesDescription ) {
		$this->set( 'm_strAppliancesDescription', CStrings::strTrimDef( $strAppliancesDescription, 240, NULL, true ) );
	}

	public function getAppliancesDescription() {
		return $this->m_strAppliancesDescription;
	}

	public function sqlAppliancesDescription() {
		return ( true == isset( $this->m_strAppliancesDescription ) ) ? '\'' . addslashes( $this->m_strAppliancesDescription ) . '\'' : 'NULL';
	}

	public function setMaximumOccupants( $intMaximumOccupants ) {
		$this->set( 'm_intMaximumOccupants', CStrings::strToIntDef( $intMaximumOccupants, NULL, false ) );
	}

	public function getMaximumOccupants() {
		return $this->m_intMaximumOccupants;
	}

	public function sqlMaximumOccupants() {
		return ( true == isset( $this->m_intMaximumOccupants ) ) ? ( string ) $this->m_intMaximumOccupants : 'NULL';
	}

	public function setDefaultLeaseTermMonths( $intDefaultLeaseTermMonths ) {
		$this->set( 'm_intDefaultLeaseTermMonths', CStrings::strToIntDef( $intDefaultLeaseTermMonths, NULL, false ) );
	}

	public function getDefaultLeaseTermMonths() {
		return $this->m_intDefaultLeaseTermMonths;
	}

	public function sqlDefaultLeaseTermMonths() {
		return ( true == isset( $this->m_intDefaultLeaseTermMonths ) ) ? ( string ) $this->m_intDefaultLeaseTermMonths : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setBlockIntegration( $intBlockIntegration ) {
		$this->set( 'm_intBlockIntegration', CStrings::strToIntDef( $intBlockIntegration, NULL, false ) );
	}

	public function getBlockIntegration() {
		return $this->m_intBlockIntegration;
	}

	public function sqlBlockIntegration() {
		return ( true == isset( $this->m_intBlockIntegration ) ) ? ( string ) $this->m_intBlockIntegration : '0';
	}

	public function setBlockPricingIntegration( $intBlockPricingIntegration ) {
		$this->set( 'm_intBlockPricingIntegration', CStrings::strToIntDef( $intBlockPricingIntegration, NULL, false ) );
	}

	public function getBlockPricingIntegration() {
		return $this->m_intBlockPricingIntegration;
	}

	public function sqlBlockPricingIntegration() {
		return ( true == isset( $this->m_intBlockPricingIntegration ) ) ? ( string ) $this->m_intBlockPricingIntegration : '0';
	}

	public function setIsOccupiable( $intIsOccupiable ) {
		$this->set( 'm_intIsOccupiable', CStrings::strToIntDef( $intIsOccupiable, NULL, false ) );
	}

	public function getIsOccupiable() {
		return $this->m_intIsOccupiable;
	}

	public function sqlIsOccupiable() {
		return ( true == isset( $this->m_intIsOccupiable ) ) ? ( string ) $this->m_intIsOccupiable : '1';
	}

	public function setIsFurnished( $intIsFurnished ) {
		$this->set( 'm_intIsFurnished', CStrings::strToIntDef( $intIsFurnished, NULL, false ) );
	}

	public function getIsFurnished() {
		return $this->m_intIsFurnished;
	}

	public function sqlIsFurnished() {
		return ( true == isset( $this->m_intIsFurnished ) ) ? ( string ) $this->m_intIsFurnished : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, remote_primary_key, ils_remote_primary_key, vaultware_key, name, lookup_code, description, market_rent, initial_rent, min_square_feet, max_square_feet, number_of_units, number_of_floors, number_of_rooms, number_of_bedrooms, bedroom_description, number_of_bathrooms, bathroom_description, appliances_description, maximum_occupants, default_lease_term_months, notes, details, block_integration, block_pricing_integration, is_occupiable, is_furnished, is_published, order_num, imported_on, exported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlIlsRemotePrimaryKey() . ', ' .
						$this->sqlVaultwareKey() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlLookupCode() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlMarketRent() . ', ' .
						$this->sqlInitialRent() . ', ' .
						$this->sqlMinSquareFeet() . ', ' .
						$this->sqlMaxSquareFeet() . ', ' .
						$this->sqlNumberOfUnits() . ', ' .
						$this->sqlNumberOfFloors() . ', ' .
						$this->sqlNumberOfRooms() . ', ' .
						$this->sqlNumberOfBedrooms() . ', ' .
						$this->sqlBedroomDescription() . ', ' .
						$this->sqlNumberOfBathrooms() . ', ' .
						$this->sqlBathroomDescription() . ', ' .
						$this->sqlAppliancesDescription() . ', ' .
						$this->sqlMaximumOccupants() . ', ' .
						$this->sqlDefaultLeaseTermMonths() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlBlockIntegration() . ', ' .
						$this->sqlBlockPricingIntegration() . ', ' .
						$this->sqlIsOccupiable() . ', ' .
						$this->sqlIsFurnished() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_remote_primary_key = ' . $this->sqlIlsRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'IlsRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' ils_remote_primary_key = ' . $this->sqlIlsRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vaultware_key = ' . $this->sqlVaultwareKey(). ',' ; } elseif( true == array_key_exists( 'VaultwareKey', $this->getChangedColumns() ) ) { $strSql .= ' vaultware_key = ' . $this->sqlVaultwareKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode(). ',' ; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent(). ',' ; } elseif( true == array_key_exists( 'MarketRent', $this->getChangedColumns() ) ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_rent = ' . $this->sqlInitialRent(). ',' ; } elseif( true == array_key_exists( 'InitialRent', $this->getChangedColumns() ) ) { $strSql .= ' initial_rent = ' . $this->sqlInitialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors(). ',' ; } elseif( true == array_key_exists( 'NumberOfFloors', $this->getChangedColumns() ) ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfRooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription(). ',' ; } elseif( true == array_key_exists( 'BedroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription(). ',' ; } elseif( true == array_key_exists( 'BathroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription(). ',' ; } elseif( true == array_key_exists( 'AppliancesDescription', $this->getChangedColumns() ) ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_occupants = ' . $this->sqlMaximumOccupants(). ',' ; } elseif( true == array_key_exists( 'MaximumOccupants', $this->getChangedColumns() ) ) { $strSql .= ' maximum_occupants = ' . $this->sqlMaximumOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths(). ',' ; } elseif( true == array_key_exists( 'DefaultLeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration(). ',' ; } elseif( true == array_key_exists( 'BlockIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration(). ',' ; } elseif( true == array_key_exists( 'BlockPricingIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_occupiable = ' . $this->sqlIsOccupiable(). ',' ; } elseif( true == array_key_exists( 'IsOccupiable', $this->getChangedColumns() ) ) { $strSql .= ' is_occupiable = ' . $this->sqlIsOccupiable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished(). ',' ; } elseif( true == array_key_exists( 'IsFurnished', $this->getChangedColumns() ) ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'ils_remote_primary_key' => $this->getIlsRemotePrimaryKey(),
			'vaultware_key' => $this->getVaultwareKey(),
			'name' => $this->getName(),
			'lookup_code' => $this->getLookupCode(),
			'description' => $this->getDescription(),
			'market_rent' => $this->getMarketRent(),
			'initial_rent' => $this->getInitialRent(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'number_of_units' => $this->getNumberOfUnits(),
			'number_of_floors' => $this->getNumberOfFloors(),
			'number_of_rooms' => $this->getNumberOfRooms(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'bedroom_description' => $this->getBedroomDescription(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'bathroom_description' => $this->getBathroomDescription(),
			'appliances_description' => $this->getAppliancesDescription(),
			'maximum_occupants' => $this->getMaximumOccupants(),
			'default_lease_term_months' => $this->getDefaultLeaseTermMonths(),
			'notes' => $this->getNotes(),
			'details' => $this->getDetails(),
			'block_integration' => $this->getBlockIntegration(),
			'block_pricing_integration' => $this->getBlockPricingIntegration(),
			'is_occupiable' => $this->getIsOccupiable(),
			'is_furnished' => $this->getIsFurnished(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'exported_on' => $this->getExportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>