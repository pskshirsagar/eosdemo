<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileExports extends CEosPluralBase {

	/**
	 * @return CFileExport[]
	 */
	public static function fetchFileExports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFileExport', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileExport
	 */
	public static function fetchFileExport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFileExport', $objDatabase );
	}

	public static function fetchFileExportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_exports', $objDatabase );
	}

	public static function fetchFileExportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileExport( sprintf( 'SELECT * FROM file_exports WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileExportsByCid( $intCid, $objDatabase ) {
		return self::fetchFileExports( sprintf( 'SELECT * FROM file_exports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileExportsByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		return self::fetchFileExports( sprintf( 'SELECT * FROM file_exports WHERE file_batch_id = %d AND cid = %d', ( int ) $intFileBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileExportsByFileExportTypeIdByCid( $intFileExportTypeId, $intCid, $objDatabase ) {
		return self::fetchFileExports( sprintf( 'SELECT * FROM file_exports WHERE file_export_type_id = %d AND cid = %d', ( int ) $intFileExportTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileExportsByFileExportStatusIdByCid( $intFileExportStatusId, $intCid, $objDatabase ) {
		return self::fetchFileExports( sprintf( 'SELECT * FROM file_exports WHERE file_export_status_id = %d AND cid = %d', ( int ) $intFileExportStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>