<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPermissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyGroupPermissions extends CEosPluralBase {

	/**
	 * @return CCompanyGroupPermission[]
	 */
	public static function fetchCompanyGroupPermissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyGroupPermission', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyGroupPermission
	 */
	public static function fetchCompanyGroupPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyGroupPermission', $objDatabase );
	}

	public static function fetchCompanyGroupPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_group_permissions', $objDatabase );
	}

	public static function fetchCompanyGroupPermissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPermission( sprintf( 'SELECT * FROM company_group_permissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPermissionsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPermissions( sprintf( 'SELECT * FROM company_group_permissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPermissionsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPermissions( sprintf( 'SELECT * FROM company_group_permissions WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPermissionsByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPermissions( sprintf( 'SELECT * FROM company_group_permissions WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>