<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPortalSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_portal_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strPasswordQuestion;
	protected $m_strPasswordAnswerEncrypted;
	protected $m_strPasswordExpiration;
	protected $m_strFacebookUser;
	protected $m_intDontAllowLogin;
	protected $m_intDontAcceptPayments;
	protected $m_intBlockPosting;
	protected $m_intBlockNetworking;
	protected $m_intBlockOnlinePayments;
	protected $m_intIsNetworkingEnrolled;
	protected $m_strApprovalIpAddress;
	protected $m_strTermsApprovalIp;
	protected $m_strTermsApprovedOn;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_intProfilePublishedBy;
	protected $m_strProfilePublishedOn;
	protected $m_strMobileNumberAuthorizedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDontAllowLogin = '0';
		$this->m_intDontAcceptPayments = '0';
		$this->m_intBlockPosting = '0';
		$this->m_intBlockNetworking = '0';
		$this->m_intBlockOnlinePayments = '0';
		$this->m_intIsNetworkingEnrolled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['password_question'] ) && $boolDirectSet ) $this->set( 'm_strPasswordQuestion', trim( stripcslashes( $arrValues['password_question'] ) ) ); elseif( isset( $arrValues['password_question'] ) ) $this->setPasswordQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_question'] ) : $arrValues['password_question'] );
		if( isset( $arrValues['password_answer_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordAnswerEncrypted', trim( stripcslashes( $arrValues['password_answer_encrypted'] ) ) ); elseif( isset( $arrValues['password_answer_encrypted'] ) ) $this->setPasswordAnswerEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_answer_encrypted'] ) : $arrValues['password_answer_encrypted'] );
		if( isset( $arrValues['password_expiration'] ) && $boolDirectSet ) $this->set( 'm_strPasswordExpiration', trim( $arrValues['password_expiration'] ) ); elseif( isset( $arrValues['password_expiration'] ) ) $this->setPasswordExpiration( $arrValues['password_expiration'] );
		if( isset( $arrValues['facebook_user'] ) && $boolDirectSet ) $this->set( 'm_strFacebookUser', trim( stripcslashes( $arrValues['facebook_user'] ) ) ); elseif( isset( $arrValues['facebook_user'] ) ) $this->setFacebookUser( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['facebook_user'] ) : $arrValues['facebook_user'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['dont_accept_payments'] ) && $boolDirectSet ) $this->set( 'm_intDontAcceptPayments', trim( $arrValues['dont_accept_payments'] ) ); elseif( isset( $arrValues['dont_accept_payments'] ) ) $this->setDontAcceptPayments( $arrValues['dont_accept_payments'] );
		if( isset( $arrValues['block_posting'] ) && $boolDirectSet ) $this->set( 'm_intBlockPosting', trim( $arrValues['block_posting'] ) ); elseif( isset( $arrValues['block_posting'] ) ) $this->setBlockPosting( $arrValues['block_posting'] );
		if( isset( $arrValues['block_networking'] ) && $boolDirectSet ) $this->set( 'm_intBlockNetworking', trim( $arrValues['block_networking'] ) ); elseif( isset( $arrValues['block_networking'] ) ) $this->setBlockNetworking( $arrValues['block_networking'] );
		if( isset( $arrValues['block_online_payments'] ) && $boolDirectSet ) $this->set( 'm_intBlockOnlinePayments', trim( $arrValues['block_online_payments'] ) ); elseif( isset( $arrValues['block_online_payments'] ) ) $this->setBlockOnlinePayments( $arrValues['block_online_payments'] );
		if( isset( $arrValues['is_networking_enrolled'] ) && $boolDirectSet ) $this->set( 'm_intIsNetworkingEnrolled', trim( $arrValues['is_networking_enrolled'] ) ); elseif( isset( $arrValues['is_networking_enrolled'] ) ) $this->setIsNetworkingEnrolled( $arrValues['is_networking_enrolled'] );
		if( isset( $arrValues['approval_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strApprovalIpAddress', trim( stripcslashes( $arrValues['approval_ip_address'] ) ) ); elseif( isset( $arrValues['approval_ip_address'] ) ) $this->setApprovalIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approval_ip_address'] ) : $arrValues['approval_ip_address'] );
		if( isset( $arrValues['terms_approval_ip'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovalIp', trim( stripcslashes( $arrValues['terms_approval_ip'] ) ) ); elseif( isset( $arrValues['terms_approval_ip'] ) ) $this->setTermsApprovalIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['terms_approval_ip'] ) : $arrValues['terms_approval_ip'] );
		if( isset( $arrValues['terms_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovedOn', trim( $arrValues['terms_approved_on'] ) ); elseif( isset( $arrValues['terms_approved_on'] ) ) $this->setTermsApprovedOn( $arrValues['terms_approved_on'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['profile_published_by'] ) && $boolDirectSet ) $this->set( 'm_intProfilePublishedBy', trim( $arrValues['profile_published_by'] ) ); elseif( isset( $arrValues['profile_published_by'] ) ) $this->setProfilePublishedBy( $arrValues['profile_published_by'] );
		if( isset( $arrValues['profile_published_on'] ) && $boolDirectSet ) $this->set( 'm_strProfilePublishedOn', trim( $arrValues['profile_published_on'] ) ); elseif( isset( $arrValues['profile_published_on'] ) ) $this->setProfilePublishedOn( $arrValues['profile_published_on'] );
		if( isset( $arrValues['mobile_number_authorized_on'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumberAuthorizedOn', trim( $arrValues['mobile_number_authorized_on'] ) ); elseif( isset( $arrValues['mobile_number_authorized_on'] ) ) $this->setMobileNumberAuthorizedOn( $arrValues['mobile_number_authorized_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordQuestion( $strPasswordQuestion ) {
		$this->set( 'm_strPasswordQuestion', CStrings::strTrimDef( $strPasswordQuestion, 2000, NULL, true ) );
	}

	public function getPasswordQuestion() {
		return $this->m_strPasswordQuestion;
	}

	public function sqlPasswordQuestion() {
		return ( true == isset( $this->m_strPasswordQuestion ) ) ? '\'' . addslashes( $this->m_strPasswordQuestion ) . '\'' : 'NULL';
	}

	public function setPasswordAnswerEncrypted( $strPasswordAnswerEncrypted ) {
		$this->set( 'm_strPasswordAnswerEncrypted', CStrings::strTrimDef( $strPasswordAnswerEncrypted, 240, NULL, true ) );
	}

	public function getPasswordAnswerEncrypted() {
		return $this->m_strPasswordAnswerEncrypted;
	}

	public function sqlPasswordAnswerEncrypted() {
		return ( true == isset( $this->m_strPasswordAnswerEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordAnswerEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordExpiration( $strPasswordExpiration ) {
		$this->set( 'm_strPasswordExpiration', CStrings::strTrimDef( $strPasswordExpiration, -1, NULL, true ) );
	}

	public function getPasswordExpiration() {
		return $this->m_strPasswordExpiration;
	}

	public function sqlPasswordExpiration() {
		return ( true == isset( $this->m_strPasswordExpiration ) ) ? '\'' . $this->m_strPasswordExpiration . '\'' : 'NULL';
	}

	public function setFacebookUser( $strFacebookUser ) {
		$this->set( 'm_strFacebookUser', CStrings::strTrimDef( $strFacebookUser, 240, NULL, true ) );
	}

	public function getFacebookUser() {
		return $this->m_strFacebookUser;
	}

	public function sqlFacebookUser() {
		return ( true == isset( $this->m_strFacebookUser ) ) ? '\'' . addslashes( $this->m_strFacebookUser ) . '\'' : 'NULL';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setDontAcceptPayments( $intDontAcceptPayments ) {
		$this->set( 'm_intDontAcceptPayments', CStrings::strToIntDef( $intDontAcceptPayments, NULL, false ) );
	}

	public function getDontAcceptPayments() {
		return $this->m_intDontAcceptPayments;
	}

	public function sqlDontAcceptPayments() {
		return ( true == isset( $this->m_intDontAcceptPayments ) ) ? ( string ) $this->m_intDontAcceptPayments : '0';
	}

	public function setBlockPosting( $intBlockPosting ) {
		$this->set( 'm_intBlockPosting', CStrings::strToIntDef( $intBlockPosting, NULL, false ) );
	}

	public function getBlockPosting() {
		return $this->m_intBlockPosting;
	}

	public function sqlBlockPosting() {
		return ( true == isset( $this->m_intBlockPosting ) ) ? ( string ) $this->m_intBlockPosting : '0';
	}

	public function setBlockNetworking( $intBlockNetworking ) {
		$this->set( 'm_intBlockNetworking', CStrings::strToIntDef( $intBlockNetworking, NULL, false ) );
	}

	public function getBlockNetworking() {
		return $this->m_intBlockNetworking;
	}

	public function sqlBlockNetworking() {
		return ( true == isset( $this->m_intBlockNetworking ) ) ? ( string ) $this->m_intBlockNetworking : '0';
	}

	public function setBlockOnlinePayments( $intBlockOnlinePayments ) {
		$this->set( 'm_intBlockOnlinePayments', CStrings::strToIntDef( $intBlockOnlinePayments, NULL, false ) );
	}

	public function getBlockOnlinePayments() {
		return $this->m_intBlockOnlinePayments;
	}

	public function sqlBlockOnlinePayments() {
		return ( true == isset( $this->m_intBlockOnlinePayments ) ) ? ( string ) $this->m_intBlockOnlinePayments : '0';
	}

	public function setIsNetworkingEnrolled( $intIsNetworkingEnrolled ) {
		$this->set( 'm_intIsNetworkingEnrolled', CStrings::strToIntDef( $intIsNetworkingEnrolled, NULL, false ) );
	}

	public function getIsNetworkingEnrolled() {
		return $this->m_intIsNetworkingEnrolled;
	}

	public function sqlIsNetworkingEnrolled() {
		return ( true == isset( $this->m_intIsNetworkingEnrolled ) ) ? ( string ) $this->m_intIsNetworkingEnrolled : '0';
	}

	public function setApprovalIpAddress( $strApprovalIpAddress ) {
		$this->set( 'm_strApprovalIpAddress', CStrings::strTrimDef( $strApprovalIpAddress, 23, NULL, true ) );
	}

	public function getApprovalIpAddress() {
		return $this->m_strApprovalIpAddress;
	}

	public function sqlApprovalIpAddress() {
		return ( true == isset( $this->m_strApprovalIpAddress ) ) ? '\'' . addslashes( $this->m_strApprovalIpAddress ) . '\'' : 'NULL';
	}

	public function setTermsApprovalIp( $strTermsApprovalIp ) {
		$this->set( 'm_strTermsApprovalIp', CStrings::strTrimDef( $strTermsApprovalIp, 23, NULL, true ) );
	}

	public function getTermsApprovalIp() {
		return $this->m_strTermsApprovalIp;
	}

	public function sqlTermsApprovalIp() {
		return ( true == isset( $this->m_strTermsApprovalIp ) ) ? '\'' . addslashes( $this->m_strTermsApprovalIp ) . '\'' : 'NULL';
	}

	public function setTermsApprovedOn( $strTermsApprovedOn ) {
		$this->set( 'm_strTermsApprovedOn', CStrings::strTrimDef( $strTermsApprovedOn, -1, NULL, true ) );
	}

	public function getTermsApprovedOn() {
		return $this->m_strTermsApprovedOn;
	}

	public function sqlTermsApprovedOn() {
		return ( true == isset( $this->m_strTermsApprovedOn ) ) ? '\'' . $this->m_strTermsApprovedOn . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setProfilePublishedBy( $intProfilePublishedBy ) {
		$this->set( 'm_intProfilePublishedBy', CStrings::strToIntDef( $intProfilePublishedBy, NULL, false ) );
	}

	public function getProfilePublishedBy() {
		return $this->m_intProfilePublishedBy;
	}

	public function sqlProfilePublishedBy() {
		return ( true == isset( $this->m_intProfilePublishedBy ) ) ? ( string ) $this->m_intProfilePublishedBy : 'NULL';
	}

	public function setProfilePublishedOn( $strProfilePublishedOn ) {
		$this->set( 'm_strProfilePublishedOn', CStrings::strTrimDef( $strProfilePublishedOn, -1, NULL, true ) );
	}

	public function getProfilePublishedOn() {
		return $this->m_strProfilePublishedOn;
	}

	public function sqlProfilePublishedOn() {
		return ( true == isset( $this->m_strProfilePublishedOn ) ) ? '\'' . $this->m_strProfilePublishedOn . '\'' : 'NULL';
	}

	public function setMobileNumberAuthorizedOn( $strMobileNumberAuthorizedOn ) {
		$this->set( 'm_strMobileNumberAuthorizedOn', CStrings::strTrimDef( $strMobileNumberAuthorizedOn, -1, NULL, true ) );
	}

	public function getMobileNumberAuthorizedOn() {
		return $this->m_strMobileNumberAuthorizedOn;
	}

	public function sqlMobileNumberAuthorizedOn() {
		return ( true == isset( $this->m_strMobileNumberAuthorizedOn ) ) ? '\'' . $this->m_strMobileNumberAuthorizedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, username, password_encrypted, password_question, password_answer_encrypted, password_expiration, facebook_user, dont_allow_login, dont_accept_payments, block_posting, block_networking, block_online_payments, is_networking_enrolled, approval_ip_address, terms_approval_ip, terms_approved_on, login_attempt_count, last_login_attempt_on, profile_published_by, profile_published_on, mobile_number_authorized_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlPasswordQuestion() . ', ' .
 						$this->sqlPasswordAnswerEncrypted() . ', ' .
 						$this->sqlPasswordExpiration() . ', ' .
 						$this->sqlFacebookUser() . ', ' .
 						$this->sqlDontAllowLogin() . ', ' .
 						$this->sqlDontAcceptPayments() . ', ' .
 						$this->sqlBlockPosting() . ', ' .
 						$this->sqlBlockNetworking() . ', ' .
 						$this->sqlBlockOnlinePayments() . ', ' .
 						$this->sqlIsNetworkingEnrolled() . ', ' .
 						$this->sqlApprovalIpAddress() . ', ' .
 						$this->sqlTermsApprovalIp() . ', ' .
 						$this->sqlTermsApprovedOn() . ', ' .
 						$this->sqlLoginAttemptCount() . ', ' .
 						$this->sqlLastLoginAttemptOn() . ', ' .
 						$this->sqlProfilePublishedBy() . ', ' .
 						$this->sqlProfilePublishedOn() . ', ' .
 						$this->sqlMobileNumberAuthorizedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; } elseif( true == array_key_exists( 'PasswordQuestion', $this->getChangedColumns() ) ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordAnswerEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_expiration = ' . $this->sqlPasswordExpiration() . ','; } elseif( true == array_key_exists( 'PasswordExpiration', $this->getChangedColumns() ) ) { $strSql .= ' password_expiration = ' . $this->sqlPasswordExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' facebook_user = ' . $this->sqlFacebookUser() . ','; } elseif( true == array_key_exists( 'FacebookUser', $this->getChangedColumns() ) ) { $strSql .= ' facebook_user = ' . $this->sqlFacebookUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; } elseif( true == array_key_exists( 'DontAllowLogin', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; } elseif( true == array_key_exists( 'DontAcceptPayments', $this->getChangedColumns() ) ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_posting = ' . $this->sqlBlockPosting() . ','; } elseif( true == array_key_exists( 'BlockPosting', $this->getChangedColumns() ) ) { $strSql .= ' block_posting = ' . $this->sqlBlockPosting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_networking = ' . $this->sqlBlockNetworking() . ','; } elseif( true == array_key_exists( 'BlockNetworking', $this->getChangedColumns() ) ) { $strSql .= ' block_networking = ' . $this->sqlBlockNetworking() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_online_payments = ' . $this->sqlBlockOnlinePayments() . ','; } elseif( true == array_key_exists( 'BlockOnlinePayments', $this->getChangedColumns() ) ) { $strSql .= ' block_online_payments = ' . $this->sqlBlockOnlinePayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_networking_enrolled = ' . $this->sqlIsNetworkingEnrolled() . ','; } elseif( true == array_key_exists( 'IsNetworkingEnrolled', $this->getChangedColumns() ) ) { $strSql .= ' is_networking_enrolled = ' . $this->sqlIsNetworkingEnrolled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; } elseif( true == array_key_exists( 'ApprovalIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terms_approval_ip = ' . $this->sqlTermsApprovalIp() . ','; } elseif( true == array_key_exists( 'TermsApprovalIp', $this->getChangedColumns() ) ) { $strSql .= ' terms_approval_ip = ' . $this->sqlTermsApprovalIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terms_approved_on = ' . $this->sqlTermsApprovedOn() . ','; } elseif( true == array_key_exists( 'TermsApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' terms_approved_on = ' . $this->sqlTermsApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_published_by = ' . $this->sqlProfilePublishedBy() . ','; } elseif( true == array_key_exists( 'ProfilePublishedBy', $this->getChangedColumns() ) ) { $strSql .= ' profile_published_by = ' . $this->sqlProfilePublishedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_published_on = ' . $this->sqlProfilePublishedOn() . ','; } elseif( true == array_key_exists( 'ProfilePublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' profile_published_on = ' . $this->sqlProfilePublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number_authorized_on = ' . $this->sqlMobileNumberAuthorizedOn() . ','; } elseif( true == array_key_exists( 'MobileNumberAuthorizedOn', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number_authorized_on = ' . $this->sqlMobileNumberAuthorizedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'password_question' => $this->getPasswordQuestion(),
			'password_answer_encrypted' => $this->getPasswordAnswerEncrypted(),
			'password_expiration' => $this->getPasswordExpiration(),
			'facebook_user' => $this->getFacebookUser(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'dont_accept_payments' => $this->getDontAcceptPayments(),
			'block_posting' => $this->getBlockPosting(),
			'block_networking' => $this->getBlockNetworking(),
			'block_online_payments' => $this->getBlockOnlinePayments(),
			'is_networking_enrolled' => $this->getIsNetworkingEnrolled(),
			'approval_ip_address' => $this->getApprovalIpAddress(),
			'terms_approval_ip' => $this->getTermsApprovalIp(),
			'terms_approved_on' => $this->getTermsApprovedOn(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'profile_published_by' => $this->getProfilePublishedBy(),
			'profile_published_on' => $this->getProfilePublishedOn(),
			'mobile_number_authorized_on' => $this->getMobileNumberAuthorizedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>