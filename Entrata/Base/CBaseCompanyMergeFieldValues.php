<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeFieldValues extends CEosPluralBase {

	/**
	 * @return CCompanyMergeFieldValue[]
	 */
	public static function fetchCompanyMergeFieldValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyMergeFieldValue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyMergeFieldValue
	 */
	public static function fetchCompanyMergeFieldValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyMergeFieldValue', $objDatabase );
	}

	public static function fetchCompanyMergeFieldValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merge_field_values', $objDatabase );
	}

	public static function fetchCompanyMergeFieldValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldValue( sprintf( 'SELECT * FROM company_merge_field_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldValuesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldValues( sprintf( 'SELECT * FROM company_merge_field_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldValuesByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldValues( sprintf( 'SELECT * FROM company_merge_field_values WHERE company_merge_field_id = %d AND cid = %d', ( int ) $intCompanyMergeFieldId, ( int ) $intCid ), $objDatabase );
	}

}
?>