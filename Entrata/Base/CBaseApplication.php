<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplication extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.applications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intLeaseTypeId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intApplicationStepId;
	protected $m_intMaxApplicationStageId;
	protected $m_intMaxApplicationStatusId;
	protected $m_intMaxApplicationStepId;
	protected $m_intCancellationListTypeId;
	protected $m_intCancellationListItemId;
	protected $m_intPrimaryApplicantId;
	protected $m_intUnitKindId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intSpaceConfigurationId;
	protected $m_intDesiredSpaceConfigurationId;
	protected $m_intScreeningId;
	protected $m_intPsProductId;
	protected $m_intApplicationPsProductId;
	protected $m_intLeasePsProductId;
	protected $m_intLeasingAgentId;
	protected $m_intInternetListingServiceId;
	protected $m_intOriginatingLeadSourceId;
	protected $m_intConvertingLeadSourceId;
	protected $m_intCompanyApplicationId;
	protected $m_intCombinedApplicationId;
	protected $m_intQuoteId;
	protected $m_intSemAdGroupId;
	protected $m_intSemKeywordId;
	protected $m_intSemSourceId;
	protected $m_intCallId;
	protected $m_intWebsiteId;
	protected $m_intCraigslistPostId;
	protected $m_intClAdId;
	protected $m_intTrafficCookieId;
	protected $m_intFirstEventId;
	protected $m_intFirstEventMediumId;
	protected $m_intFirstLeasingCenterEventId;
	protected $m_intResponseEventId;
	protected $m_intLastEventId;
	protected $m_intReferringLeaseCustomerId;
	protected $m_intLeaseSignatureTypeId;
	protected $m_intInsurancePolicyId;
	protected $m_intInsurancePolicyStatusTypeId;
	protected $m_intOccupancyTypeId;
	protected $m_strGuestRemotePrimaryKey;
	protected $m_strAppRemotePrimaryKey;
	protected $m_fltDesiredRentMin;
	protected $m_fltDesiredRentMax;
	protected $m_intDesiredBedrooms;
	protected $m_fltDesiredBathrooms;
	protected $m_intDesiredPets;
	protected $m_intDesiredOccupants;
	protected $m_arrintDesiredAmenities;
	protected $m_strApplicationDatetime;
	protected $m_strUrlReferer;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intQuotesCount;
	protected $m_fltConversionProbability;
	protected $m_strFirstContactedOn;
	protected $m_strTimeToFirstResponse;
	protected $m_strBlueMoonApplicationSentOn;
	protected $m_strInfoApprovedOn;
	protected $m_strPreQualificationCompletedOn;
	protected $m_strPreQualificationApprovedOn;
	protected $m_strApplicationStartedOn;
	protected $m_strApplicationCompletedOn;
	protected $m_strApplicationApprovedOn;
	protected $m_strScreeningApprovedOn;
	protected $m_strLeaseGeneratedOn;
	protected $m_strLeaseCompletedOn;
	protected $m_strLeaseApprovedOn;
	protected $m_strCancelledOn;
	protected $m_strTransferredInOn;
	protected $m_strTransferredOutOn;
	protected $m_strMuteFollowupsUntil;
	protected $m_strQuotedRentLockedUntil;
	protected $m_strHoldUnitUntil;
	protected $m_boolRequiresCapture;
	protected $m_boolIsPetPolicyConflicted;
	protected $m_boolIsDeleted;
	protected $m_intLockedBy;
	protected $m_strLockedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intApplicationStageId = '1';
		$this->m_intApplicationStatusId = '1';
		$this->m_intApplicationStepId = '1';
		$this->m_intMaxApplicationStageId = '1';
		$this->m_intMaxApplicationStatusId = '1';
		$this->m_intMaxApplicationStepId = '1';
		$this->m_intOccupancyTypeId = '1';
		$this->m_strApplicationDatetime = 'now()';
		$this->m_intQuotesCount = '0';
		$this->m_fltConversionProbability = '0';
		$this->m_boolRequiresCapture = false;
		$this->m_boolIsPetPolicyConflicted = false;
		$this->m_boolIsDeleted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTypeId', trim( $arrValues['lease_type_id'] ) ); elseif( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
		if( isset( $arrValues['application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageId', trim( $arrValues['application_stage_id'] ) ); elseif( isset( $arrValues['application_stage_id'] ) ) $this->setApplicationStageId( $arrValues['application_stage_id'] );
		if( isset( $arrValues['application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStatusId', trim( $arrValues['application_status_id'] ) ); elseif( isset( $arrValues['application_status_id'] ) ) $this->setApplicationStatusId( $arrValues['application_status_id'] );
		if( isset( $arrValues['application_step_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStepId', trim( $arrValues['application_step_id'] ) ); elseif( isset( $arrValues['application_step_id'] ) ) $this->setApplicationStepId( $arrValues['application_step_id'] );
		if( isset( $arrValues['max_application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStageId', trim( $arrValues['max_application_stage_id'] ) ); elseif( isset( $arrValues['max_application_stage_id'] ) ) $this->setMaxApplicationStageId( $arrValues['max_application_stage_id'] );
		if( isset( $arrValues['max_application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStatusId', trim( $arrValues['max_application_status_id'] ) ); elseif( isset( $arrValues['max_application_status_id'] ) ) $this->setMaxApplicationStatusId( $arrValues['max_application_status_id'] );
		if( isset( $arrValues['max_application_step_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStepId', trim( $arrValues['max_application_step_id'] ) ); elseif( isset( $arrValues['max_application_step_id'] ) ) $this->setMaxApplicationStepId( $arrValues['max_application_step_id'] );
		if( isset( $arrValues['cancellation_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCancellationListTypeId', trim( $arrValues['cancellation_list_type_id'] ) ); elseif( isset( $arrValues['cancellation_list_type_id'] ) ) $this->setCancellationListTypeId( $arrValues['cancellation_list_type_id'] );
		if( isset( $arrValues['cancellation_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intCancellationListItemId', trim( $arrValues['cancellation_list_item_id'] ) ); elseif( isset( $arrValues['cancellation_list_item_id'] ) ) $this->setCancellationListItemId( $arrValues['cancellation_list_item_id'] );
		if( isset( $arrValues['primary_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryApplicantId', trim( $arrValues['primary_applicant_id'] ) ); elseif( isset( $arrValues['primary_applicant_id'] ) ) $this->setPrimaryApplicantId( $arrValues['primary_applicant_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['desired_space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intDesiredSpaceConfigurationId', trim( $arrValues['desired_space_configuration_id'] ) ); elseif( isset( $arrValues['desired_space_configuration_id'] ) ) $this->setDesiredSpaceConfigurationId( $arrValues['desired_space_configuration_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['application_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationPsProductId', trim( $arrValues['application_ps_product_id'] ) ); elseif( isset( $arrValues['application_ps_product_id'] ) ) $this->setApplicationPsProductId( $arrValues['application_ps_product_id'] );
		if( isset( $arrValues['lease_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasePsProductId', trim( $arrValues['lease_ps_product_id'] ) ); elseif( isset( $arrValues['lease_ps_product_id'] ) ) $this->setLeasePsProductId( $arrValues['lease_ps_product_id'] );
		if( isset( $arrValues['leasing_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingAgentId', trim( $arrValues['leasing_agent_id'] ) ); elseif( isset( $arrValues['leasing_agent_id'] ) ) $this->setLeasingAgentId( $arrValues['leasing_agent_id'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['originating_lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginatingLeadSourceId', trim( $arrValues['originating_lead_source_id'] ) ); elseif( isset( $arrValues['originating_lead_source_id'] ) ) $this->setOriginatingLeadSourceId( $arrValues['originating_lead_source_id'] );
		if( isset( $arrValues['converting_lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intConvertingLeadSourceId', trim( $arrValues['converting_lead_source_id'] ) ); elseif( isset( $arrValues['converting_lead_source_id'] ) ) $this->setConvertingLeadSourceId( $arrValues['converting_lead_source_id'] );
		if( isset( $arrValues['company_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyApplicationId', trim( $arrValues['company_application_id'] ) ); elseif( isset( $arrValues['company_application_id'] ) ) $this->setCompanyApplicationId( $arrValues['company_application_id'] );
		if( isset( $arrValues['combined_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCombinedApplicationId', trim( $arrValues['combined_application_id'] ) ); elseif( isset( $arrValues['combined_application_id'] ) ) $this->setCombinedApplicationId( $arrValues['combined_application_id'] );
		if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['craigslist_post_id'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistPostId', trim( $arrValues['craigslist_post_id'] ) ); elseif( isset( $arrValues['craigslist_post_id'] ) ) $this->setCraigslistPostId( $arrValues['craigslist_post_id'] );
		if( isset( $arrValues['cl_ad_id'] ) && $boolDirectSet ) $this->set( 'm_intClAdId', trim( $arrValues['cl_ad_id'] ) ); elseif( isset( $arrValues['cl_ad_id'] ) ) $this->setClAdId( $arrValues['cl_ad_id'] );
		if( isset( $arrValues['traffic_cookie_id'] ) && $boolDirectSet ) $this->set( 'm_intTrafficCookieId', trim( $arrValues['traffic_cookie_id'] ) ); elseif( isset( $arrValues['traffic_cookie_id'] ) ) $this->setTrafficCookieId( $arrValues['traffic_cookie_id'] );
		if( isset( $arrValues['first_event_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstEventId', trim( $arrValues['first_event_id'] ) ); elseif( isset( $arrValues['first_event_id'] ) ) $this->setFirstEventId( $arrValues['first_event_id'] );
		if( isset( $arrValues['first_event_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstEventMediumId', trim( $arrValues['first_event_medium_id'] ) ); elseif( isset( $arrValues['first_event_medium_id'] ) ) $this->setFirstEventMediumId( $arrValues['first_event_medium_id'] );
		if( isset( $arrValues['first_leasing_center_event_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstLeasingCenterEventId', trim( $arrValues['first_leasing_center_event_id'] ) ); elseif( isset( $arrValues['first_leasing_center_event_id'] ) ) $this->setFirstLeasingCenterEventId( $arrValues['first_leasing_center_event_id'] );
		if( isset( $arrValues['response_event_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseEventId', trim( $arrValues['response_event_id'] ) ); elseif( isset( $arrValues['response_event_id'] ) ) $this->setResponseEventId( $arrValues['response_event_id'] );
		if( isset( $arrValues['last_event_id'] ) && $boolDirectSet ) $this->set( 'm_intLastEventId', trim( $arrValues['last_event_id'] ) ); elseif( isset( $arrValues['last_event_id'] ) ) $this->setLastEventId( $arrValues['last_event_id'] );
		if( isset( $arrValues['referring_lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intReferringLeaseCustomerId', trim( $arrValues['referring_lease_customer_id'] ) ); elseif( isset( $arrValues['referring_lease_customer_id'] ) ) $this->setReferringLeaseCustomerId( $arrValues['referring_lease_customer_id'] );
		if( isset( $arrValues['lease_signature_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseSignatureTypeId', trim( $arrValues['lease_signature_type_id'] ) ); elseif( isset( $arrValues['lease_signature_type_id'] ) ) $this->setLeaseSignatureTypeId( $arrValues['lease_signature_type_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['insurance_policy_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyStatusTypeId', trim( $arrValues['insurance_policy_status_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_status_type_id'] ) ) $this->setInsurancePolicyStatusTypeId( $arrValues['insurance_policy_status_type_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['guest_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strGuestRemotePrimaryKey', trim( stripcslashes( $arrValues['guest_remote_primary_key'] ) ) ); elseif( isset( $arrValues['guest_remote_primary_key'] ) ) $this->setGuestRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['guest_remote_primary_key'] ) : $arrValues['guest_remote_primary_key'] );
		if( isset( $arrValues['app_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strAppRemotePrimaryKey', trim( stripcslashes( $arrValues['app_remote_primary_key'] ) ) ); elseif( isset( $arrValues['app_remote_primary_key'] ) ) $this->setAppRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['app_remote_primary_key'] ) : $arrValues['app_remote_primary_key'] );
		if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMin', trim( $arrValues['desired_rent_min'] ) ); elseif( isset( $arrValues['desired_rent_min'] ) ) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
		if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMax', trim( $arrValues['desired_rent_max'] ) ); elseif( isset( $arrValues['desired_rent_max'] ) ) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
		if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intDesiredBedrooms', trim( $arrValues['desired_bedrooms'] ) ); elseif( isset( $arrValues['desired_bedrooms'] ) ) $this->setDesiredBedrooms( $arrValues['desired_bedrooms'] );
		if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredBathrooms', trim( $arrValues['desired_bathrooms'] ) ); elseif( isset( $arrValues['desired_bathrooms'] ) ) $this->setDesiredBathrooms( $arrValues['desired_bathrooms'] );
		if( isset( $arrValues['desired_pets'] ) && $boolDirectSet ) $this->set( 'm_intDesiredPets', trim( $arrValues['desired_pets'] ) ); elseif( isset( $arrValues['desired_pets'] ) ) $this->setDesiredPets( $arrValues['desired_pets'] );
		if( isset( $arrValues['desired_occupants'] ) && $boolDirectSet ) $this->set( 'm_intDesiredOccupants', trim( $arrValues['desired_occupants'] ) ); elseif( isset( $arrValues['desired_occupants'] ) ) $this->setDesiredOccupants( $arrValues['desired_occupants'] );
		if( isset( $arrValues['desired_amenities'] ) && $boolDirectSet ) $this->set( 'm_arrintDesiredAmenities', trim( $arrValues['desired_amenities'] ) ); elseif( isset( $arrValues['desired_amenities'] ) ) $this->setDesiredAmenities( $arrValues['desired_amenities'] );
		if( isset( $arrValues['application_datetime'] ) && $boolDirectSet ) $this->set( 'm_strApplicationDatetime', trim( $arrValues['application_datetime'] ) ); elseif( isset( $arrValues['application_datetime'] ) ) $this->setApplicationDatetime( $arrValues['application_datetime'] );
		if( isset( $arrValues['url_referer'] ) && $boolDirectSet ) $this->set( 'm_strUrlReferer', trim( stripcslashes( $arrValues['url_referer'] ) ) ); elseif( isset( $arrValues['url_referer'] ) ) $this->setUrlReferer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url_referer'] ) : $arrValues['url_referer'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['quotes_count'] ) && $boolDirectSet ) $this->set( 'm_intQuotesCount', trim( $arrValues['quotes_count'] ) ); elseif( isset( $arrValues['quotes_count'] ) ) $this->setQuotesCount( $arrValues['quotes_count'] );
		if( isset( $arrValues['conversion_probability'] ) && $boolDirectSet ) $this->set( 'm_fltConversionProbability', trim( $arrValues['conversion_probability'] ) ); elseif( isset( $arrValues['conversion_probability'] ) ) $this->setConversionProbability( $arrValues['conversion_probability'] );
		if( isset( $arrValues['first_contacted_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstContactedOn', trim( $arrValues['first_contacted_on'] ) ); elseif( isset( $arrValues['first_contacted_on'] ) ) $this->setFirstContactedOn( $arrValues['first_contacted_on'] );
		if( isset( $arrValues['time_to_first_response'] ) && $boolDirectSet ) $this->set( 'm_strTimeToFirstResponse', trim( $arrValues['time_to_first_response'] ) ); elseif( isset( $arrValues['time_to_first_response'] ) ) $this->setTimeToFirstResponse( $arrValues['time_to_first_response'] );
		if( isset( $arrValues['blue_moon_application_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strBlueMoonApplicationSentOn', trim( $arrValues['blue_moon_application_sent_on'] ) ); elseif( isset( $arrValues['blue_moon_application_sent_on'] ) ) $this->setBlueMoonApplicationSentOn( $arrValues['blue_moon_application_sent_on'] );
		if( isset( $arrValues['info_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strInfoApprovedOn', trim( $arrValues['info_approved_on'] ) ); elseif( isset( $arrValues['info_approved_on'] ) ) $this->setInfoApprovedOn( $arrValues['info_approved_on'] );
		if( isset( $arrValues['pre_qualification_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strPreQualificationCompletedOn', trim( $arrValues['pre_qualification_completed_on'] ) ); elseif( isset( $arrValues['pre_qualification_completed_on'] ) ) $this->setPreQualificationCompletedOn( $arrValues['pre_qualification_completed_on'] );
		if( isset( $arrValues['pre_qualification_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strPreQualificationApprovedOn', trim( $arrValues['pre_qualification_approved_on'] ) ); elseif( isset( $arrValues['pre_qualification_approved_on'] ) ) $this->setPreQualificationApprovedOn( $arrValues['pre_qualification_approved_on'] );
		if( isset( $arrValues['application_started_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationStartedOn', trim( $arrValues['application_started_on'] ) ); elseif( isset( $arrValues['application_started_on'] ) ) $this->setApplicationStartedOn( $arrValues['application_started_on'] );
		if( isset( $arrValues['application_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationCompletedOn', trim( $arrValues['application_completed_on'] ) ); elseif( isset( $arrValues['application_completed_on'] ) ) $this->setApplicationCompletedOn( $arrValues['application_completed_on'] );
		if( isset( $arrValues['application_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationApprovedOn', trim( $arrValues['application_approved_on'] ) ); elseif( isset( $arrValues['application_approved_on'] ) ) $this->setApplicationApprovedOn( $arrValues['application_approved_on'] );
		if( isset( $arrValues['screening_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningApprovedOn', trim( $arrValues['screening_approved_on'] ) ); elseif( isset( $arrValues['screening_approved_on'] ) ) $this->setScreeningApprovedOn( $arrValues['screening_approved_on'] );
		if( isset( $arrValues['lease_generated_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseGeneratedOn', trim( $arrValues['lease_generated_on'] ) ); elseif( isset( $arrValues['lease_generated_on'] ) ) $this->setLeaseGeneratedOn( $arrValues['lease_generated_on'] );
		if( isset( $arrValues['lease_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseCompletedOn', trim( $arrValues['lease_completed_on'] ) ); elseif( isset( $arrValues['lease_completed_on'] ) ) $this->setLeaseCompletedOn( $arrValues['lease_completed_on'] );
		if( isset( $arrValues['lease_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseApprovedOn', trim( $arrValues['lease_approved_on'] ) ); elseif( isset( $arrValues['lease_approved_on'] ) ) $this->setLeaseApprovedOn( $arrValues['lease_approved_on'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['transferred_in_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredInOn', trim( $arrValues['transferred_in_on'] ) ); elseif( isset( $arrValues['transferred_in_on'] ) ) $this->setTransferredInOn( $arrValues['transferred_in_on'] );
		if( isset( $arrValues['transferred_out_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredOutOn', trim( $arrValues['transferred_out_on'] ) ); elseif( isset( $arrValues['transferred_out_on'] ) ) $this->setTransferredOutOn( $arrValues['transferred_out_on'] );
		if( isset( $arrValues['mute_followups_until'] ) && $boolDirectSet ) $this->set( 'm_strMuteFollowupsUntil', trim( $arrValues['mute_followups_until'] ) ); elseif( isset( $arrValues['mute_followups_until'] ) ) $this->setMuteFollowupsUntil( $arrValues['mute_followups_until'] );
		if( isset( $arrValues['quoted_rent_locked_until'] ) && $boolDirectSet ) $this->set( 'm_strQuotedRentLockedUntil', trim( $arrValues['quoted_rent_locked_until'] ) ); elseif( isset( $arrValues['quoted_rent_locked_until'] ) ) $this->setQuotedRentLockedUntil( $arrValues['quoted_rent_locked_until'] );
		if( isset( $arrValues['hold_unit_until'] ) && $boolDirectSet ) $this->set( 'm_strHoldUnitUntil', trim( $arrValues['hold_unit_until'] ) ); elseif( isset( $arrValues['hold_unit_until'] ) ) $this->setHoldUnitUntil( $arrValues['hold_unit_until'] );
		if( isset( $arrValues['requires_capture'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresCapture', trim( stripcslashes( $arrValues['requires_capture'] ) ) ); elseif( isset( $arrValues['requires_capture'] ) ) $this->setRequiresCapture( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_capture'] ) : $arrValues['requires_capture'] );
		if( isset( $arrValues['is_pet_policy_conflicted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPetPolicyConflicted', trim( stripcslashes( $arrValues['is_pet_policy_conflicted'] ) ) ); elseif( isset( $arrValues['is_pet_policy_conflicted'] ) ) $this->setIsPetPolicyConflicted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pet_policy_conflicted'] ) : $arrValues['is_pet_policy_conflicted'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setLeaseTypeId( $intLeaseTypeId ) {
		$this->set( 'm_intLeaseTypeId', CStrings::strToIntDef( $intLeaseTypeId, NULL, false ) );
	}

	public function getLeaseTypeId() {
		return $this->m_intLeaseTypeId;
	}

	public function sqlLeaseTypeId() {
		return ( true == isset( $this->m_intLeaseTypeId ) ) ? ( string ) $this->m_intLeaseTypeId : 'NULL';
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->set( 'm_intApplicationStageId', CStrings::strToIntDef( $intApplicationStageId, NULL, false ) );
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function sqlApplicationStageId() {
		return ( true == isset( $this->m_intApplicationStageId ) ) ? ( string ) $this->m_intApplicationStageId : '1';
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->set( 'm_intApplicationStatusId', CStrings::strToIntDef( $intApplicationStatusId, NULL, false ) );
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function sqlApplicationStatusId() {
		return ( true == isset( $this->m_intApplicationStatusId ) ) ? ( string ) $this->m_intApplicationStatusId : '1';
	}

	public function setApplicationStepId( $intApplicationStepId ) {
		$this->set( 'm_intApplicationStepId', CStrings::strToIntDef( $intApplicationStepId, NULL, false ) );
	}

	public function getApplicationStepId() {
		return $this->m_intApplicationStepId;
	}

	public function sqlApplicationStepId() {
		return ( true == isset( $this->m_intApplicationStepId ) ) ? ( string ) $this->m_intApplicationStepId : '1';
	}

	public function setMaxApplicationStageId( $intMaxApplicationStageId ) {
		$this->set( 'm_intMaxApplicationStageId', CStrings::strToIntDef( $intMaxApplicationStageId, NULL, false ) );
	}

	public function getMaxApplicationStageId() {
		return $this->m_intMaxApplicationStageId;
	}

	public function sqlMaxApplicationStageId() {
		return ( true == isset( $this->m_intMaxApplicationStageId ) ) ? ( string ) $this->m_intMaxApplicationStageId : '1';
	}

	public function setMaxApplicationStatusId( $intMaxApplicationStatusId ) {
		$this->set( 'm_intMaxApplicationStatusId', CStrings::strToIntDef( $intMaxApplicationStatusId, NULL, false ) );
	}

	public function getMaxApplicationStatusId() {
		return $this->m_intMaxApplicationStatusId;
	}

	public function sqlMaxApplicationStatusId() {
		return ( true == isset( $this->m_intMaxApplicationStatusId ) ) ? ( string ) $this->m_intMaxApplicationStatusId : '1';
	}

	public function setMaxApplicationStepId( $intMaxApplicationStepId ) {
		$this->set( 'm_intMaxApplicationStepId', CStrings::strToIntDef( $intMaxApplicationStepId, NULL, false ) );
	}

	public function getMaxApplicationStepId() {
		return $this->m_intMaxApplicationStepId;
	}

	public function sqlMaxApplicationStepId() {
		return ( true == isset( $this->m_intMaxApplicationStepId ) ) ? ( string ) $this->m_intMaxApplicationStepId : '1';
	}

	public function setCancellationListTypeId( $intCancellationListTypeId ) {
		$this->set( 'm_intCancellationListTypeId', CStrings::strToIntDef( $intCancellationListTypeId, NULL, false ) );
	}

	public function getCancellationListTypeId() {
		return $this->m_intCancellationListTypeId;
	}

	public function sqlCancellationListTypeId() {
		return ( true == isset( $this->m_intCancellationListTypeId ) ) ? ( string ) $this->m_intCancellationListTypeId : 'NULL';
	}

	public function setCancellationListItemId( $intCancellationListItemId ) {
		$this->set( 'm_intCancellationListItemId', CStrings::strToIntDef( $intCancellationListItemId, NULL, false ) );
	}

	public function getCancellationListItemId() {
		return $this->m_intCancellationListItemId;
	}

	public function sqlCancellationListItemId() {
		return ( true == isset( $this->m_intCancellationListItemId ) ) ? ( string ) $this->m_intCancellationListItemId : 'NULL';
	}

	public function setPrimaryApplicantId( $intPrimaryApplicantId ) {
		$this->set( 'm_intPrimaryApplicantId', CStrings::strToIntDef( $intPrimaryApplicantId, NULL, false ) );
	}

	public function getPrimaryApplicantId() {
		return $this->m_intPrimaryApplicantId;
	}

	public function sqlPrimaryApplicantId() {
		return ( true == isset( $this->m_intPrimaryApplicantId ) ) ? ( string ) $this->m_intPrimaryApplicantId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : 'NULL';
	}

	public function setDesiredSpaceConfigurationId( $intDesiredSpaceConfigurationId ) {
		$this->set( 'm_intDesiredSpaceConfigurationId', CStrings::strToIntDef( $intDesiredSpaceConfigurationId, NULL, false ) );
	}

	public function getDesiredSpaceConfigurationId() {
		return $this->m_intDesiredSpaceConfigurationId;
	}

	public function sqlDesiredSpaceConfigurationId() {
		return ( true == isset( $this->m_intDesiredSpaceConfigurationId ) ) ? ( string ) $this->m_intDesiredSpaceConfigurationId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setApplicationPsProductId( $intApplicationPsProductId ) {
		$this->set( 'm_intApplicationPsProductId', CStrings::strToIntDef( $intApplicationPsProductId, NULL, false ) );
	}

	public function getApplicationPsProductId() {
		return $this->m_intApplicationPsProductId;
	}

	public function sqlApplicationPsProductId() {
		return ( true == isset( $this->m_intApplicationPsProductId ) ) ? ( string ) $this->m_intApplicationPsProductId : 'NULL';
	}

	public function setLeasePsProductId( $intLeasePsProductId ) {
		$this->set( 'm_intLeasePsProductId', CStrings::strToIntDef( $intLeasePsProductId, NULL, false ) );
	}

	public function getLeasePsProductId() {
		return $this->m_intLeasePsProductId;
	}

	public function sqlLeasePsProductId() {
		return ( true == isset( $this->m_intLeasePsProductId ) ) ? ( string ) $this->m_intLeasePsProductId : 'NULL';
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->set( 'm_intLeasingAgentId', CStrings::strToIntDef( $intLeasingAgentId, NULL, false ) );
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function sqlLeasingAgentId() {
		return ( true == isset( $this->m_intLeasingAgentId ) ) ? ( string ) $this->m_intLeasingAgentId : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setOriginatingLeadSourceId( $intOriginatingLeadSourceId ) {
		$this->set( 'm_intOriginatingLeadSourceId', CStrings::strToIntDef( $intOriginatingLeadSourceId, NULL, false ) );
	}

	public function getOriginatingLeadSourceId() {
		return $this->m_intOriginatingLeadSourceId;
	}

	public function sqlOriginatingLeadSourceId() {
		return ( true == isset( $this->m_intOriginatingLeadSourceId ) ) ? ( string ) $this->m_intOriginatingLeadSourceId : 'NULL';
	}

	public function setConvertingLeadSourceId( $intConvertingLeadSourceId ) {
		$this->set( 'm_intConvertingLeadSourceId', CStrings::strToIntDef( $intConvertingLeadSourceId, NULL, false ) );
	}

	public function getConvertingLeadSourceId() {
		return $this->m_intConvertingLeadSourceId;
	}

	public function sqlConvertingLeadSourceId() {
		return ( true == isset( $this->m_intConvertingLeadSourceId ) ) ? ( string ) $this->m_intConvertingLeadSourceId : 'NULL';
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->set( 'm_intCompanyApplicationId', CStrings::strToIntDef( $intCompanyApplicationId, NULL, false ) );
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function sqlCompanyApplicationId() {
		return ( true == isset( $this->m_intCompanyApplicationId ) ) ? ( string ) $this->m_intCompanyApplicationId : 'NULL';
	}

	public function setCombinedApplicationId( $intCombinedApplicationId ) {
		$this->set( 'm_intCombinedApplicationId', CStrings::strToIntDef( $intCombinedApplicationId, NULL, false ) );
	}

	public function getCombinedApplicationId() {
		return $this->m_intCombinedApplicationId;
	}

	public function sqlCombinedApplicationId() {
		return ( true == isset( $this->m_intCombinedApplicationId ) ) ? ( string ) $this->m_intCombinedApplicationId : 'NULL';
	}

	public function setQuoteId( $intQuoteId ) {
		$this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function sqlQuoteId() {
		return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setCraigslistPostId( $intCraigslistPostId ) {
		$this->set( 'm_intCraigslistPostId', CStrings::strToIntDef( $intCraigslistPostId, NULL, false ) );
	}

	public function getCraigslistPostId() {
		return $this->m_intCraigslistPostId;
	}

	public function sqlCraigslistPostId() {
		return ( true == isset( $this->m_intCraigslistPostId ) ) ? ( string ) $this->m_intCraigslistPostId : 'NULL';
	}

	public function setClAdId( $intClAdId ) {
		$this->set( 'm_intClAdId', CStrings::strToIntDef( $intClAdId, NULL, false ) );
	}

	public function getClAdId() {
		return $this->m_intClAdId;
	}

	public function sqlClAdId() {
		return ( true == isset( $this->m_intClAdId ) ) ? ( string ) $this->m_intClAdId : 'NULL';
	}

	public function setTrafficCookieId( $intTrafficCookieId ) {
		$this->set( 'm_intTrafficCookieId', CStrings::strToIntDef( $intTrafficCookieId, NULL, false ) );
	}

	public function getTrafficCookieId() {
		return $this->m_intTrafficCookieId;
	}

	public function sqlTrafficCookieId() {
		return ( true == isset( $this->m_intTrafficCookieId ) ) ? ( string ) $this->m_intTrafficCookieId : 'NULL';
	}

	public function setFirstEventId( $intFirstEventId ) {
		$this->set( 'm_intFirstEventId', CStrings::strToIntDef( $intFirstEventId, NULL, false ) );
	}

	public function getFirstEventId() {
		return $this->m_intFirstEventId;
	}

	public function sqlFirstEventId() {
		return ( true == isset( $this->m_intFirstEventId ) ) ? ( string ) $this->m_intFirstEventId : 'NULL';
	}

	public function setFirstEventMediumId( $intFirstEventMediumId ) {
		$this->set( 'm_intFirstEventMediumId', CStrings::strToIntDef( $intFirstEventMediumId, NULL, false ) );
	}

	public function getFirstEventMediumId() {
		return $this->m_intFirstEventMediumId;
	}

	public function sqlFirstEventMediumId() {
		return ( true == isset( $this->m_intFirstEventMediumId ) ) ? ( string ) $this->m_intFirstEventMediumId : 'NULL';
	}

	public function setFirstLeasingCenterEventId( $intFirstLeasingCenterEventId ) {
		$this->set( 'm_intFirstLeasingCenterEventId', CStrings::strToIntDef( $intFirstLeasingCenterEventId, NULL, false ) );
	}

	public function getFirstLeasingCenterEventId() {
		return $this->m_intFirstLeasingCenterEventId;
	}

	public function sqlFirstLeasingCenterEventId() {
		return ( true == isset( $this->m_intFirstLeasingCenterEventId ) ) ? ( string ) $this->m_intFirstLeasingCenterEventId : 'NULL';
	}

	public function setResponseEventId( $intResponseEventId ) {
		$this->set( 'm_intResponseEventId', CStrings::strToIntDef( $intResponseEventId, NULL, false ) );
	}

	public function getResponseEventId() {
		return $this->m_intResponseEventId;
	}

	public function sqlResponseEventId() {
		return ( true == isset( $this->m_intResponseEventId ) ) ? ( string ) $this->m_intResponseEventId : 'NULL';
	}

	public function setLastEventId( $intLastEventId ) {
		$this->set( 'm_intLastEventId', CStrings::strToIntDef( $intLastEventId, NULL, false ) );
	}

	public function getLastEventId() {
		return $this->m_intLastEventId;
	}

	public function sqlLastEventId() {
		return ( true == isset( $this->m_intLastEventId ) ) ? ( string ) $this->m_intLastEventId : 'NULL';
	}

	public function setReferringLeaseCustomerId( $intReferringLeaseCustomerId ) {
		$this->set( 'm_intReferringLeaseCustomerId', CStrings::strToIntDef( $intReferringLeaseCustomerId, NULL, false ) );
	}

	public function getReferringLeaseCustomerId() {
		return $this->m_intReferringLeaseCustomerId;
	}

	public function sqlReferringLeaseCustomerId() {
		return ( true == isset( $this->m_intReferringLeaseCustomerId ) ) ? ( string ) $this->m_intReferringLeaseCustomerId : 'NULL';
	}

	public function setLeaseSignatureTypeId( $intLeaseSignatureTypeId ) {
		$this->set( 'm_intLeaseSignatureTypeId', CStrings::strToIntDef( $intLeaseSignatureTypeId, NULL, false ) );
	}

	public function getLeaseSignatureTypeId() {
		return $this->m_intLeaseSignatureTypeId;
	}

	public function sqlLeaseSignatureTypeId() {
		return ( true == isset( $this->m_intLeaseSignatureTypeId ) ) ? ( string ) $this->m_intLeaseSignatureTypeId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId ) {
		$this->set( 'm_intInsurancePolicyStatusTypeId', CStrings::strToIntDef( $intInsurancePolicyStatusTypeId, NULL, false ) );
	}

	public function getInsurancePolicyStatusTypeId() {
		return $this->m_intInsurancePolicyStatusTypeId;
	}

	public function sqlInsurancePolicyStatusTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyStatusTypeId ) ) ? ( string ) $this->m_intInsurancePolicyStatusTypeId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setGuestRemotePrimaryKey( $strGuestRemotePrimaryKey ) {
		$this->set( 'm_strGuestRemotePrimaryKey', CStrings::strTrimDef( $strGuestRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getGuestRemotePrimaryKey() {
		return $this->m_strGuestRemotePrimaryKey;
	}

	public function sqlGuestRemotePrimaryKey() {
		return ( true == isset( $this->m_strGuestRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strGuestRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setAppRemotePrimaryKey( $strAppRemotePrimaryKey ) {
		$this->set( 'm_strAppRemotePrimaryKey', CStrings::strTrimDef( $strAppRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getAppRemotePrimaryKey() {
		return $this->m_strAppRemotePrimaryKey;
	}

	public function sqlAppRemotePrimaryKey() {
		return ( true == isset( $this->m_strAppRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strAppRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		$this->set( 'm_fltDesiredRentMin', CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2 ) );
	}

	public function getDesiredRentMin() {
		return $this->m_fltDesiredRentMin;
	}

	public function sqlDesiredRentMin() {
		return ( true == isset( $this->m_fltDesiredRentMin ) ) ? ( string ) $this->m_fltDesiredRentMin : 'NULL';
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		$this->set( 'm_fltDesiredRentMax', CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2 ) );
	}

	public function getDesiredRentMax() {
		return $this->m_fltDesiredRentMax;
	}

	public function sqlDesiredRentMax() {
		return ( true == isset( $this->m_fltDesiredRentMax ) ) ? ( string ) $this->m_fltDesiredRentMax : 'NULL';
	}

	public function setDesiredBedrooms( $intDesiredBedrooms ) {
		$this->set( 'm_intDesiredBedrooms', CStrings::strToIntDef( $intDesiredBedrooms, NULL, false ) );
	}

	public function getDesiredBedrooms() {
		return $this->m_intDesiredBedrooms;
	}

	public function sqlDesiredBedrooms() {
		return ( true == isset( $this->m_intDesiredBedrooms ) ) ? ( string ) $this->m_intDesiredBedrooms : 'NULL';
	}

	public function setDesiredBathrooms( $fltDesiredBathrooms ) {
		$this->set( 'm_fltDesiredBathrooms', CStrings::strToFloatDef( $fltDesiredBathrooms, NULL, false, 2 ) );
	}

	public function getDesiredBathrooms() {
		return $this->m_fltDesiredBathrooms;
	}

	public function sqlDesiredBathrooms() {
		return ( true == isset( $this->m_fltDesiredBathrooms ) ) ? ( string ) $this->m_fltDesiredBathrooms : 'NULL';
	}

	public function setDesiredPets( $intDesiredPets ) {
		$this->set( 'm_intDesiredPets', CStrings::strToIntDef( $intDesiredPets, NULL, false ) );
	}

	public function getDesiredPets() {
		return $this->m_intDesiredPets;
	}

	public function sqlDesiredPets() {
		return ( true == isset( $this->m_intDesiredPets ) ) ? ( string ) $this->m_intDesiredPets : 'NULL';
	}

	public function setDesiredOccupants( $intDesiredOccupants ) {
		$this->set( 'm_intDesiredOccupants', CStrings::strToIntDef( $intDesiredOccupants, NULL, false ) );
	}

	public function getDesiredOccupants() {
		return $this->m_intDesiredOccupants;
	}

	public function sqlDesiredOccupants() {
		return ( true == isset( $this->m_intDesiredOccupants ) ) ? ( string ) $this->m_intDesiredOccupants : 'NULL';
	}

	public function setDesiredAmenities( $arrintDesiredAmenities ) {
		$this->set( 'm_arrintDesiredAmenities', CStrings::strToArrIntDef( $arrintDesiredAmenities, NULL ) );
	}

	public function getDesiredAmenities() {
		return $this->m_arrintDesiredAmenities;
	}

	public function sqlDesiredAmenities() {
		return ( true == isset( $this->m_arrintDesiredAmenities ) && true == valArr( $this->m_arrintDesiredAmenities ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDesiredAmenities, NULL ) . '\'' : 'NULL';
	}

	public function setApplicationDatetime( $strApplicationDatetime ) {
		$this->set( 'm_strApplicationDatetime', CStrings::strTrimDef( $strApplicationDatetime, -1, NULL, true ) );
	}

	public function getApplicationDatetime() {
		return $this->m_strApplicationDatetime;
	}

	public function sqlApplicationDatetime() {
		return ( true == isset( $this->m_strApplicationDatetime ) ) ? '\'' . $this->m_strApplicationDatetime . '\'' : 'NOW()';
	}

	public function setUrlReferer( $strUrlReferer ) {
		$this->set( 'm_strUrlReferer', CStrings::strTrimDef( $strUrlReferer, 2500, NULL, true ) );
	}

	public function getUrlReferer() {
		return $this->m_strUrlReferer;
	}

	public function sqlUrlReferer() {
		return ( true == isset( $this->m_strUrlReferer ) ) ? '\'' . addslashes( $this->m_strUrlReferer ) . '\'' : 'NULL';
	}

	public function setQuotesCount( $intQuotesCount ) {
		$this->set( 'm_intQuotesCount', CStrings::strToIntDef( $intQuotesCount, NULL, false ) );
	}

	public function getQuotesCount() {
		return $this->m_intQuotesCount;
	}

	public function sqlQuotesCount() {
		return ( true == isset( $this->m_intQuotesCount ) ) ? ( string ) $this->m_intQuotesCount : '0';
	}

	public function setConversionProbability( $fltConversionProbability ) {
		$this->set( 'm_fltConversionProbability', CStrings::strToFloatDef( $fltConversionProbability, NULL, false, 2 ) );
	}

	public function getConversionProbability() {
		return $this->m_fltConversionProbability;
	}

	public function sqlConversionProbability() {
		return ( true == isset( $this->m_fltConversionProbability ) ) ? ( string ) $this->m_fltConversionProbability : '0';
	}

	public function setFirstContactedOn( $strFirstContactedOn ) {
		$this->set( 'm_strFirstContactedOn', CStrings::strTrimDef( $strFirstContactedOn, -1, NULL, true ) );
	}

	public function getFirstContactedOn() {
		return $this->m_strFirstContactedOn;
	}

	public function sqlFirstContactedOn() {
		return ( true == isset( $this->m_strFirstContactedOn ) ) ? '\'' . $this->m_strFirstContactedOn . '\'' : 'NULL';
	}

	public function setTimeToFirstResponse( $strTimeToFirstResponse ) {
		$this->set( 'm_strTimeToFirstResponse', CStrings::strTrimDef( $strTimeToFirstResponse, NULL, NULL, true ) );
	}

	public function getTimeToFirstResponse() {
		return $this->m_strTimeToFirstResponse;
	}

	public function sqlTimeToFirstResponse() {
		return ( true == isset( $this->m_strTimeToFirstResponse ) ) ? '\'' . $this->m_strTimeToFirstResponse . '\'' : 'NULL';
	}

	public function setBlueMoonApplicationSentOn( $strBlueMoonApplicationSentOn ) {
		$this->set( 'm_strBlueMoonApplicationSentOn', CStrings::strTrimDef( $strBlueMoonApplicationSentOn, -1, NULL, true ) );
	}

	public function getBlueMoonApplicationSentOn() {
		return $this->m_strBlueMoonApplicationSentOn;
	}

	public function sqlBlueMoonApplicationSentOn() {
		return ( true == isset( $this->m_strBlueMoonApplicationSentOn ) ) ? '\'' . $this->m_strBlueMoonApplicationSentOn . '\'' : 'NULL';
	}

	public function setInfoApprovedOn( $strInfoApprovedOn ) {
		$this->set( 'm_strInfoApprovedOn', CStrings::strTrimDef( $strInfoApprovedOn, -1, NULL, true ) );
	}

	public function getInfoApprovedOn() {
		return $this->m_strInfoApprovedOn;
	}

	public function sqlInfoApprovedOn() {
		return ( true == isset( $this->m_strInfoApprovedOn ) ) ? '\'' . $this->m_strInfoApprovedOn . '\'' : 'NULL';
	}

	public function setPreQualificationCompletedOn( $strPreQualificationCompletedOn ) {
		$this->set( 'm_strPreQualificationCompletedOn', CStrings::strTrimDef( $strPreQualificationCompletedOn, -1, NULL, true ) );
	}

	public function getPreQualificationCompletedOn() {
		return $this->m_strPreQualificationCompletedOn;
	}

	public function sqlPreQualificationCompletedOn() {
		return ( true == isset( $this->m_strPreQualificationCompletedOn ) ) ? '\'' . $this->m_strPreQualificationCompletedOn . '\'' : 'NULL';
	}

	public function setPreQualificationApprovedOn( $strPreQualificationApprovedOn ) {
		$this->set( 'm_strPreQualificationApprovedOn', CStrings::strTrimDef( $strPreQualificationApprovedOn, -1, NULL, true ) );
	}

	public function getPreQualificationApprovedOn() {
		return $this->m_strPreQualificationApprovedOn;
	}

	public function sqlPreQualificationApprovedOn() {
		return ( true == isset( $this->m_strPreQualificationApprovedOn ) ) ? '\'' . $this->m_strPreQualificationApprovedOn . '\'' : 'NULL';
	}

	public function setApplicationStartedOn( $strApplicationStartedOn ) {
		$this->set( 'm_strApplicationStartedOn', CStrings::strTrimDef( $strApplicationStartedOn, -1, NULL, true ) );
	}

	public function getApplicationStartedOn() {
		return $this->m_strApplicationStartedOn;
	}

	public function sqlApplicationStartedOn() {
		return ( true == isset( $this->m_strApplicationStartedOn ) ) ? '\'' . $this->m_strApplicationStartedOn . '\'' : 'NULL';
	}

	public function setApplicationCompletedOn( $strApplicationCompletedOn ) {
		$this->set( 'm_strApplicationCompletedOn', CStrings::strTrimDef( $strApplicationCompletedOn, -1, NULL, true ) );
	}

	public function getApplicationCompletedOn() {
		return $this->m_strApplicationCompletedOn;
	}

	public function sqlApplicationCompletedOn() {
		return ( true == isset( $this->m_strApplicationCompletedOn ) ) ? '\'' . $this->m_strApplicationCompletedOn . '\'' : 'NULL';
	}

	public function setApplicationApprovedOn( $strApplicationApprovedOn ) {
		$this->set( 'm_strApplicationApprovedOn', CStrings::strTrimDef( $strApplicationApprovedOn, -1, NULL, true ) );
	}

	public function getApplicationApprovedOn() {
		return $this->m_strApplicationApprovedOn;
	}

	public function sqlApplicationApprovedOn() {
		return ( true == isset( $this->m_strApplicationApprovedOn ) ) ? '\'' . $this->m_strApplicationApprovedOn . '\'' : 'NULL';
	}

	public function setScreeningApprovedOn( $strScreeningApprovedOn ) {
		$this->set( 'm_strScreeningApprovedOn', CStrings::strTrimDef( $strScreeningApprovedOn, -1, NULL, true ) );
	}

	public function getScreeningApprovedOn() {
		return $this->m_strScreeningApprovedOn;
	}

	public function sqlScreeningApprovedOn() {
		return ( true == isset( $this->m_strScreeningApprovedOn ) ) ? '\'' . $this->m_strScreeningApprovedOn . '\'' : 'NULL';
	}

	public function setLeaseGeneratedOn( $strLeaseGeneratedOn ) {
		$this->set( 'm_strLeaseGeneratedOn', CStrings::strTrimDef( $strLeaseGeneratedOn, -1, NULL, true ) );
	}

	public function getLeaseGeneratedOn() {
		return $this->m_strLeaseGeneratedOn;
	}

	public function sqlLeaseGeneratedOn() {
		return ( true == isset( $this->m_strLeaseGeneratedOn ) ) ? '\'' . $this->m_strLeaseGeneratedOn . '\'' : 'NULL';
	}

	public function setLeaseCompletedOn( $strLeaseCompletedOn ) {
		$this->set( 'm_strLeaseCompletedOn', CStrings::strTrimDef( $strLeaseCompletedOn, -1, NULL, true ) );
	}

	public function getLeaseCompletedOn() {
		return $this->m_strLeaseCompletedOn;
	}

	public function sqlLeaseCompletedOn() {
		return ( true == isset( $this->m_strLeaseCompletedOn ) ) ? '\'' . $this->m_strLeaseCompletedOn . '\'' : 'NULL';
	}

	public function setLeaseApprovedOn( $strLeaseApprovedOn ) {
		$this->set( 'm_strLeaseApprovedOn', CStrings::strTrimDef( $strLeaseApprovedOn, -1, NULL, true ) );
	}

	public function getLeaseApprovedOn() {
		return $this->m_strLeaseApprovedOn;
	}

	public function sqlLeaseApprovedOn() {
		return ( true == isset( $this->m_strLeaseApprovedOn ) ) ? '\'' . $this->m_strLeaseApprovedOn . '\'' : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setTransferredInOn( $strTransferredInOn ) {
		$this->set( 'm_strTransferredInOn', CStrings::strTrimDef( $strTransferredInOn, -1, NULL, true ) );
	}

	public function getTransferredInOn() {
		return $this->m_strTransferredInOn;
	}

	public function sqlTransferredInOn() {
		return ( true == isset( $this->m_strTransferredInOn ) ) ? '\'' . $this->m_strTransferredInOn . '\'' : 'NULL';
	}

	public function setTransferredOutOn( $strTransferredOutOn ) {
		$this->set( 'm_strTransferredOutOn', CStrings::strTrimDef( $strTransferredOutOn, -1, NULL, true ) );
	}

	public function getTransferredOutOn() {
		return $this->m_strTransferredOutOn;
	}

	public function sqlTransferredOutOn() {
		return ( true == isset( $this->m_strTransferredOutOn ) ) ? '\'' . $this->m_strTransferredOutOn . '\'' : 'NULL';
	}

	public function setMuteFollowupsUntil( $strMuteFollowupsUntil ) {
		$this->set( 'm_strMuteFollowupsUntil', CStrings::strTrimDef( $strMuteFollowupsUntil, -1, NULL, true ) );
	}

	public function getMuteFollowupsUntil() {
		return $this->m_strMuteFollowupsUntil;
	}

	public function sqlMuteFollowupsUntil() {
		return ( true == isset( $this->m_strMuteFollowupsUntil ) ) ? '\'' . $this->m_strMuteFollowupsUntil . '\'' : 'NULL';
	}

	public function setQuotedRentLockedUntil( $strQuotedRentLockedUntil ) {
		$this->set( 'm_strQuotedRentLockedUntil', CStrings::strTrimDef( $strQuotedRentLockedUntil, -1, NULL, true ) );
	}

	public function getQuotedRentLockedUntil() {
		return $this->m_strQuotedRentLockedUntil;
	}

	public function sqlQuotedRentLockedUntil() {
		return ( true == isset( $this->m_strQuotedRentLockedUntil ) ) ? '\'' . $this->m_strQuotedRentLockedUntil . '\'' : 'NULL';
	}

	public function setHoldUnitUntil( $strHoldUnitUntil ) {
		$this->set( 'm_strHoldUnitUntil', CStrings::strTrimDef( $strHoldUnitUntil, -1, NULL, true ) );
	}

	public function getHoldUnitUntil() {
		return $this->m_strHoldUnitUntil;
	}

	public function sqlHoldUnitUntil() {
		return ( true == isset( $this->m_strHoldUnitUntil ) ) ? '\'' . $this->m_strHoldUnitUntil . '\'' : 'NULL';
	}

	public function setRequiresCapture( $boolRequiresCapture ) {
		$this->set( 'm_boolRequiresCapture', CStrings::strToBool( $boolRequiresCapture ) );
	}

	public function getRequiresCapture() {
		return $this->m_boolRequiresCapture;
	}

	public function sqlRequiresCapture() {
		return ( true == isset( $this->m_boolRequiresCapture ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresCapture ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPetPolicyConflicted( $boolIsPetPolicyConflicted ) {
		$this->set( 'm_boolIsPetPolicyConflicted', CStrings::strToBool( $boolIsPetPolicyConflicted ) );
	}

	public function getIsPetPolicyConflicted() {
		return $this->m_boolIsPetPolicyConflicted;
	}

	public function sqlIsPetPolicyConflicted() {
		return ( true == isset( $this->m_boolIsPetPolicyConflicted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPetPolicyConflicted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, lease_interval_id, lease_type_id, application_stage_id, application_status_id, application_step_id, max_application_stage_id, max_application_status_id, max_application_step_id, cancellation_list_type_id, cancellation_list_item_id, primary_applicant_id, unit_kind_id, unit_type_id, property_floorplan_id, property_building_id, property_floor_id, property_unit_id, unit_space_id, space_configuration_id, desired_space_configuration_id, screening_id, ps_product_id, application_ps_product_id, lease_ps_product_id, leasing_agent_id, internet_listing_service_id, originating_lead_source_id, converting_lead_source_id, company_application_id, combined_application_id, quote_id, sem_ad_group_id, sem_keyword_id, sem_source_id, call_id, website_id, craigslist_post_id, cl_ad_id, traffic_cookie_id, first_event_id, first_event_medium_id, first_leasing_center_event_id, response_event_id, last_event_id, referring_lease_customer_id, lease_signature_type_id, insurance_policy_id, insurance_policy_status_type_id, occupancy_type_id, guest_remote_primary_key, app_remote_primary_key, desired_rent_min, desired_rent_max, desired_bedrooms, desired_bathrooms, desired_pets, desired_occupants, desired_amenities, application_datetime, url_referer, details, quotes_count, conversion_probability, first_contacted_on, time_to_first_response, blue_moon_application_sent_on, info_approved_on, pre_qualification_completed_on, pre_qualification_approved_on, application_started_on, application_completed_on, application_approved_on, screening_approved_on, lease_generated_on, lease_completed_on, lease_approved_on, cancelled_on, transferred_in_on, transferred_out_on, mute_followups_until, quoted_rent_locked_until, hold_unit_until, requires_capture, is_pet_policy_conflicted, is_deleted, locked_by, locked_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlLeaseIntervalId() . ', ' .
		          $this->sqlLeaseTypeId() . ', ' .
		          $this->sqlApplicationStageId() . ', ' .
		          $this->sqlApplicationStatusId() . ', ' .
		          $this->sqlApplicationStepId() . ', ' .
		          $this->sqlMaxApplicationStageId() . ', ' .
		          $this->sqlMaxApplicationStatusId() . ', ' .
		          $this->sqlMaxApplicationStepId() . ', ' .
		          $this->sqlCancellationListTypeId() . ', ' .
		          $this->sqlCancellationListItemId() . ', ' .
		          $this->sqlPrimaryApplicantId() . ', ' .
		          $this->sqlUnitKindId() . ', ' .
		          $this->sqlUnitTypeId() . ', ' .
		          $this->sqlPropertyFloorplanId() . ', ' .
		          $this->sqlPropertyBuildingId() . ', ' .
		          $this->sqlPropertyFloorId() . ', ' .
		          $this->sqlPropertyUnitId() . ', ' .
		          $this->sqlUnitSpaceId() . ', ' .
		          $this->sqlSpaceConfigurationId() . ', ' .
		          $this->sqlDesiredSpaceConfigurationId() . ', ' .
		          $this->sqlScreeningId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlApplicationPsProductId() . ', ' .
		          $this->sqlLeasePsProductId() . ', ' .
		          $this->sqlLeasingAgentId() . ', ' .
		          $this->sqlInternetListingServiceId() . ', ' .
		          $this->sqlOriginatingLeadSourceId() . ', ' .
		          $this->sqlConvertingLeadSourceId() . ', ' .
		          $this->sqlCompanyApplicationId() . ', ' .
		          $this->sqlCombinedApplicationId() . ', ' .
		          $this->sqlQuoteId() . ', ' .
		          $this->sqlSemAdGroupId() . ', ' .
		          $this->sqlSemKeywordId() . ', ' .
		          $this->sqlSemSourceId() . ', ' .
		          $this->sqlCallId() . ', ' .
		          $this->sqlWebsiteId() . ', ' .
		          $this->sqlCraigslistPostId() . ', ' .
		          $this->sqlClAdId() . ', ' .
		          $this->sqlTrafficCookieId() . ', ' .
		          $this->sqlFirstEventId() . ', ' .
		          $this->sqlFirstEventMediumId() . ', ' .
		          $this->sqlFirstLeasingCenterEventId() . ', ' .
		          $this->sqlResponseEventId() . ', ' .
		          $this->sqlLastEventId() . ', ' .
		          $this->sqlReferringLeaseCustomerId() . ', ' .
		          $this->sqlLeaseSignatureTypeId() . ', ' .
		          $this->sqlInsurancePolicyId() . ', ' .
		          $this->sqlInsurancePolicyStatusTypeId() . ', ' .
		          $this->sqlOccupancyTypeId() . ', ' .
		          $this->sqlGuestRemotePrimaryKey() . ', ' .
		          $this->sqlAppRemotePrimaryKey() . ', ' .
		          $this->sqlDesiredRentMin() . ', ' .
		          $this->sqlDesiredRentMax() . ', ' .
		          $this->sqlDesiredBedrooms() . ', ' .
		          $this->sqlDesiredBathrooms() . ', ' .
		          $this->sqlDesiredPets() . ', ' .
		          $this->sqlDesiredOccupants() . ', ' .
		          $this->sqlDesiredAmenities() . ', ' .
		          $this->sqlApplicationDatetime() . ', ' .
		          $this->sqlUrlReferer() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlQuotesCount() . ', ' .
		          $this->sqlConversionProbability() . ', ' .
		          $this->sqlFirstContactedOn() . ', ' .
		          $this->sqlTimeToFirstResponse() . ', ' .
		          $this->sqlBlueMoonApplicationSentOn() . ', ' .
		          $this->sqlInfoApprovedOn() . ', ' .
		          $this->sqlPreQualificationCompletedOn() . ', ' .
		          $this->sqlPreQualificationApprovedOn() . ', ' .
		          $this->sqlApplicationStartedOn() . ', ' .
		          $this->sqlApplicationCompletedOn() . ', ' .
		          $this->sqlApplicationApprovedOn() . ', ' .
		          $this->sqlScreeningApprovedOn() . ', ' .
		          $this->sqlLeaseGeneratedOn() . ', ' .
		          $this->sqlLeaseCompletedOn() . ', ' .
		          $this->sqlLeaseApprovedOn() . ', ' .
		          $this->sqlCancelledOn() . ', ' .
		          $this->sqlTransferredInOn() . ', ' .
		          $this->sqlTransferredOutOn() . ', ' .
		          $this->sqlMuteFollowupsUntil() . ', ' .
		          $this->sqlQuotedRentLockedUntil() . ', ' .
		          $this->sqlHoldUnitUntil() . ', ' .
		          $this->sqlRequiresCapture() . ', ' .
		          $this->sqlIsPetPolicyConflicted() . ', ' .
		          $this->sqlIsDeleted() . ', ' .
		          $this->sqlLockedBy() . ', ' .
		          $this->sqlLockedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_id = ' . $this->sqlApplicationStageId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStageId', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_id = ' . $this->sqlApplicationStageId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_status_id = ' . $this->sqlApplicationStatusId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' application_status_id = ' . $this->sqlApplicationStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStepId', $this->getChangedColumns() ) ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_stage_id = ' . $this->sqlMaxApplicationStageId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStageId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_stage_id = ' . $this->sqlMaxApplicationStageId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_status_id = ' . $this->sqlMaxApplicationStatusId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_status_id = ' . $this->sqlMaxApplicationStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_step_id = ' . $this->sqlMaxApplicationStepId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStepId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_step_id = ' . $this->sqlMaxApplicationStepId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancellation_list_type_id = ' . $this->sqlCancellationListTypeId(). ',' ; } elseif( true == array_key_exists( 'CancellationListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cancellation_list_type_id = ' . $this->sqlCancellationListTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancellation_list_item_id = ' . $this->sqlCancellationListItemId(). ',' ; } elseif( true == array_key_exists( 'CancellationListItemId', $this->getChangedColumns() ) ) { $strSql .= ' cancellation_list_item_id = ' . $this->sqlCancellationListItemId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId(). ',' ; } elseif( true == array_key_exists( 'PrimaryApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_space_configuration_id = ' . $this->sqlDesiredSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'DesiredSpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' desired_space_configuration_id = ' . $this->sqlDesiredSpaceConfigurationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_ps_product_id = ' . $this->sqlApplicationPsProductId(). ',' ; } elseif( true == array_key_exists( 'ApplicationPsProductId', $this->getChangedColumns() ) ) { $strSql .= ' application_ps_product_id = ' . $this->sqlApplicationPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_ps_product_id = ' . $this->sqlLeasePsProductId(). ',' ; } elseif( true == array_key_exists( 'LeasePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' lease_ps_product_id = ' . $this->sqlLeasePsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId(). ',' ; } elseif( true == array_key_exists( 'LeasingAgentId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_lead_source_id = ' . $this->sqlOriginatingLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'OriginatingLeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' originating_lead_source_id = ' . $this->sqlOriginatingLeadSourceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' converting_lead_source_id = ' . $this->sqlConvertingLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'ConvertingLeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' converting_lead_source_id = ' . $this->sqlConvertingLeadSourceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId(). ',' ; } elseif( true == array_key_exists( 'CompanyApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' combined_application_id = ' . $this->sqlCombinedApplicationId(). ',' ; } elseif( true == array_key_exists( 'CombinedApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' combined_application_id = ' . $this->sqlCombinedApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId(). ',' ; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId(). ',' ; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId(). ',' ; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId(). ',' ; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId(). ',' ; } elseif( true == array_key_exists( 'CraigslistPostId', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId(). ',' ; } elseif( true == array_key_exists( 'ClAdId', $this->getChangedColumns() ) ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId(). ',' ; } elseif( true == array_key_exists( 'TrafficCookieId', $this->getChangedColumns() ) ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_event_id = ' . $this->sqlFirstEventId(). ',' ; } elseif( true == array_key_exists( 'FirstEventId', $this->getChangedColumns() ) ) { $strSql .= ' first_event_id = ' . $this->sqlFirstEventId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_event_medium_id = ' . $this->sqlFirstEventMediumId(). ',' ; } elseif( true == array_key_exists( 'FirstEventMediumId', $this->getChangedColumns() ) ) { $strSql .= ' first_event_medium_id = ' . $this->sqlFirstEventMediumId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_leasing_center_event_id = ' . $this->sqlFirstLeasingCenterEventId(). ',' ; } elseif( true == array_key_exists( 'FirstLeasingCenterEventId', $this->getChangedColumns() ) ) { $strSql .= ' first_leasing_center_event_id = ' . $this->sqlFirstLeasingCenterEventId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_event_id = ' . $this->sqlResponseEventId(). ',' ; } elseif( true == array_key_exists( 'ResponseEventId', $this->getChangedColumns() ) ) { $strSql .= ' response_event_id = ' . $this->sqlResponseEventId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_event_id = ' . $this->sqlLastEventId(). ',' ; } elseif( true == array_key_exists( 'LastEventId', $this->getChangedColumns() ) ) { $strSql .= ' last_event_id = ' . $this->sqlLastEventId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referring_lease_customer_id = ' . $this->sqlReferringLeaseCustomerId(). ',' ; } elseif( true == array_key_exists( 'ReferringLeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' referring_lease_customer_id = ' . $this->sqlReferringLeaseCustomerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_signature_type_id = ' . $this->sqlLeaseSignatureTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseSignatureTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_signature_type_id = ' . $this->sqlLeaseSignatureTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'GuestRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'AppRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMin', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMax', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets(). ',' ; } elseif( true == array_key_exists( 'DesiredPets', $this->getChangedColumns() ) ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants(). ',' ; } elseif( true == array_key_exists( 'DesiredOccupants', $this->getChangedColumns() ) ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_amenities = ' . $this->sqlDesiredAmenities(). ',' ; } elseif( true == array_key_exists( 'DesiredAmenities', $this->getChangedColumns() ) ) { $strSql .= ' desired_amenities = ' . $this->sqlDesiredAmenities() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_datetime = ' . $this->sqlApplicationDatetime(). ',' ; } elseif( true == array_key_exists( 'ApplicationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' application_datetime = ' . $this->sqlApplicationDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url_referer = ' . $this->sqlUrlReferer(). ',' ; } elseif( true == array_key_exists( 'UrlReferer', $this->getChangedColumns() ) ) { $strSql .= ' url_referer = ' . $this->sqlUrlReferer() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quotes_count = ' . $this->sqlQuotesCount(). ',' ; } elseif( true == array_key_exists( 'QuotesCount', $this->getChangedColumns() ) ) { $strSql .= ' quotes_count = ' . $this->sqlQuotesCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conversion_probability = ' . $this->sqlConversionProbability(). ',' ; } elseif( true == array_key_exists( 'ConversionProbability', $this->getChangedColumns() ) ) { $strSql .= ' conversion_probability = ' . $this->sqlConversionProbability() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_contacted_on = ' . $this->sqlFirstContactedOn(). ',' ; } elseif( true == array_key_exists( 'FirstContactedOn', $this->getChangedColumns() ) ) { $strSql .= ' first_contacted_on = ' . $this->sqlFirstContactedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_to_first_response = ' . $this->sqlTimeToFirstResponse(). ',' ; } elseif( true == array_key_exists( 'TimeToFirstResponse', $this->getChangedColumns() ) ) { $strSql .= ' time_to_first_response = ' . $this->sqlTimeToFirstResponse() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn(). ',' ; } elseif( true == array_key_exists( 'BlueMoonApplicationSentOn', $this->getChangedColumns() ) ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' info_approved_on = ' . $this->sqlInfoApprovedOn(). ',' ; } elseif( true == array_key_exists( 'InfoApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' info_approved_on = ' . $this->sqlInfoApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_qualification_completed_on = ' . $this->sqlPreQualificationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'PreQualificationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' pre_qualification_completed_on = ' . $this->sqlPreQualificationCompletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_qualification_approved_on = ' . $this->sqlPreQualificationApprovedOn(). ',' ; } elseif( true == array_key_exists( 'PreQualificationApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' pre_qualification_approved_on = ' . $this->sqlPreQualificationApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_started_on = ' . $this->sqlApplicationStartedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_started_on = ' . $this->sqlApplicationStartedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_completed_on = ' . $this->sqlApplicationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_completed_on = ' . $this->sqlApplicationCompletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_approved_on = ' . $this->sqlApplicationApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_approved_on = ' . $this->sqlApplicationApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_approved_on = ' . $this->sqlScreeningApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_approved_on = ' . $this->sqlScreeningApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseGeneratedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_completed_on = ' . $this->sqlLeaseCompletedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_completed_on = ' . $this->sqlLeaseCompletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_approved_on = ' . $this->sqlLeaseApprovedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_approved_on = ' . $this->sqlLeaseApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_in_on = ' . $this->sqlTransferredInOn(). ',' ; } elseif( true == array_key_exists( 'TransferredInOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_in_on = ' . $this->sqlTransferredInOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_out_on = ' . $this->sqlTransferredOutOn(). ',' ; } elseif( true == array_key_exists( 'TransferredOutOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_out_on = ' . $this->sqlTransferredOutOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mute_followups_until = ' . $this->sqlMuteFollowupsUntil(). ',' ; } elseif( true == array_key_exists( 'MuteFollowupsUntil', $this->getChangedColumns() ) ) { $strSql .= ' mute_followups_until = ' . $this->sqlMuteFollowupsUntil() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quoted_rent_locked_until = ' . $this->sqlQuotedRentLockedUntil(). ',' ; } elseif( true == array_key_exists( 'QuotedRentLockedUntil', $this->getChangedColumns() ) ) { $strSql .= ' quoted_rent_locked_until = ' . $this->sqlQuotedRentLockedUntil() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hold_unit_until = ' . $this->sqlHoldUnitUntil(). ',' ; } elseif( true == array_key_exists( 'HoldUnitUntil', $this->getChangedColumns() ) ) { $strSql .= ' hold_unit_until = ' . $this->sqlHoldUnitUntil() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_capture = ' . $this->sqlRequiresCapture(). ',' ; } elseif( true == array_key_exists( 'RequiresCapture', $this->getChangedColumns() ) ) { $strSql .= ' requires_capture = ' . $this->sqlRequiresCapture() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pet_policy_conflicted = ' . $this->sqlIsPetPolicyConflicted(). ',' ; } elseif( true == array_key_exists( 'IsPetPolicyConflicted', $this->getChangedColumns() ) ) { $strSql .= ' is_pet_policy_conflicted = ' . $this->sqlIsPetPolicyConflicted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy(). ',' ; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'lease_type_id' => $this->getLeaseTypeId(),
			'application_stage_id' => $this->getApplicationStageId(),
			'application_status_id' => $this->getApplicationStatusId(),
			'application_step_id' => $this->getApplicationStepId(),
			'max_application_stage_id' => $this->getMaxApplicationStageId(),
			'max_application_status_id' => $this->getMaxApplicationStatusId(),
			'max_application_step_id' => $this->getMaxApplicationStepId(),
			'cancellation_list_type_id' => $this->getCancellationListTypeId(),
			'cancellation_list_item_id' => $this->getCancellationListItemId(),
			'primary_applicant_id' => $this->getPrimaryApplicantId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'desired_space_configuration_id' => $this->getDesiredSpaceConfigurationId(),
			'screening_id' => $this->getScreeningId(),
			'ps_product_id' => $this->getPsProductId(),
			'application_ps_product_id' => $this->getApplicationPsProductId(),
			'lease_ps_product_id' => $this->getLeasePsProductId(),
			'leasing_agent_id' => $this->getLeasingAgentId(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'originating_lead_source_id' => $this->getOriginatingLeadSourceId(),
			'converting_lead_source_id' => $this->getConvertingLeadSourceId(),
			'company_application_id' => $this->getCompanyApplicationId(),
			'combined_application_id' => $this->getCombinedApplicationId(),
			'quote_id' => $this->getQuoteId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'sem_source_id' => $this->getSemSourceId(),
			'call_id' => $this->getCallId(),
			'website_id' => $this->getWebsiteId(),
			'craigslist_post_id' => $this->getCraigslistPostId(),
			'cl_ad_id' => $this->getClAdId(),
			'traffic_cookie_id' => $this->getTrafficCookieId(),
			'first_event_id' => $this->getFirstEventId(),
			'first_event_medium_id' => $this->getFirstEventMediumId(),
			'first_leasing_center_event_id' => $this->getFirstLeasingCenterEventId(),
			'response_event_id' => $this->getResponseEventId(),
			'last_event_id' => $this->getLastEventId(),
			'referring_lease_customer_id' => $this->getReferringLeaseCustomerId(),
			'lease_signature_type_id' => $this->getLeaseSignatureTypeId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'insurance_policy_status_type_id' => $this->getInsurancePolicyStatusTypeId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'guest_remote_primary_key' => $this->getGuestRemotePrimaryKey(),
			'app_remote_primary_key' => $this->getAppRemotePrimaryKey(),
			'desired_rent_min' => $this->getDesiredRentMin(),
			'desired_rent_max' => $this->getDesiredRentMax(),
			'desired_bedrooms' => $this->getDesiredBedrooms(),
			'desired_bathrooms' => $this->getDesiredBathrooms(),
			'desired_pets' => $this->getDesiredPets(),
			'desired_occupants' => $this->getDesiredOccupants(),
			'desired_amenities' => $this->getDesiredAmenities(),
			'application_datetime' => $this->getApplicationDatetime(),
			'url_referer' => $this->getUrlReferer(),
			'details' => $this->getDetails(),
			'quotes_count' => $this->getQuotesCount(),
			'conversion_probability' => $this->getConversionProbability(),
			'first_contacted_on' => $this->getFirstContactedOn(),
			'time_to_first_response' => $this->getTimeToFirstResponse(),
			'blue_moon_application_sent_on' => $this->getBlueMoonApplicationSentOn(),
			'info_approved_on' => $this->getInfoApprovedOn(),
			'pre_qualification_completed_on' => $this->getPreQualificationCompletedOn(),
			'pre_qualification_approved_on' => $this->getPreQualificationApprovedOn(),
			'application_started_on' => $this->getApplicationStartedOn(),
			'application_completed_on' => $this->getApplicationCompletedOn(),
			'application_approved_on' => $this->getApplicationApprovedOn(),
			'screening_approved_on' => $this->getScreeningApprovedOn(),
			'lease_generated_on' => $this->getLeaseGeneratedOn(),
			'lease_completed_on' => $this->getLeaseCompletedOn(),
			'lease_approved_on' => $this->getLeaseApprovedOn(),
			'cancelled_on' => $this->getCancelledOn(),
			'transferred_in_on' => $this->getTransferredInOn(),
			'transferred_out_on' => $this->getTransferredOutOn(),
			'mute_followups_until' => $this->getMuteFollowupsUntil(),
			'quoted_rent_locked_until' => $this->getQuotedRentLockedUntil(),
			'hold_unit_until' => $this->getHoldUnitUntil(),
			'requires_capture' => $this->getRequiresCapture(),
			'is_pet_policy_conflicted' => $this->getIsPetPolicyConflicted(),
			'is_deleted' => $this->getIsDeleted(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>