<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyHapRequestArTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_hap_request_ar_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyHapRequestId;
	protected $m_intLeaseId;
	protected $m_intArTransactionId;
	protected $m_intSubsidyCertificationId;
	protected $m_fltHudApprovedAmount;
	protected $m_boolIsAdjustment;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intExcludedBy;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAdjustment = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_hap_request_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyHapRequestId', trim( $arrValues['subsidy_hap_request_id'] ) ); elseif( isset( $arrValues['subsidy_hap_request_id'] ) ) $this->setSubsidyHapRequestId( $arrValues['subsidy_hap_request_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['subsidy_certification_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationId', trim( $arrValues['subsidy_certification_id'] ) ); elseif( isset( $arrValues['subsidy_certification_id'] ) ) $this->setSubsidyCertificationId( $arrValues['subsidy_certification_id'] );
		if( isset( $arrValues['hud_approved_amount'] ) && $boolDirectSet ) $this->set( 'm_fltHudApprovedAmount', trim( $arrValues['hud_approved_amount'] ) ); elseif( isset( $arrValues['hud_approved_amount'] ) ) $this->setHudApprovedAmount( $arrValues['hud_approved_amount'] );
		if( isset( $arrValues['is_adjustment'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdjustment', trim( stripcslashes( $arrValues['is_adjustment'] ) ) ); elseif( isset( $arrValues['is_adjustment'] ) ) $this->setIsAdjustment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_adjustment'] ) : $arrValues['is_adjustment'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['excluded_by'] ) && $boolDirectSet ) $this->set( 'm_intExcludedBy', trim( $arrValues['excluded_by'] ) ); elseif( isset( $arrValues['excluded_by'] ) ) $this->setExcludedBy( $arrValues['excluded_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyHapRequestId( $intSubsidyHapRequestId ) {
		$this->set( 'm_intSubsidyHapRequestId', CStrings::strToIntDef( $intSubsidyHapRequestId, NULL, false ) );
	}

	public function getSubsidyHapRequestId() {
		return $this->m_intSubsidyHapRequestId;
	}

	public function sqlSubsidyHapRequestId() {
		return ( true == isset( $this->m_intSubsidyHapRequestId ) ) ? ( string ) $this->m_intSubsidyHapRequestId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setSubsidyCertificationId( $intSubsidyCertificationId ) {
		$this->set( 'm_intSubsidyCertificationId', CStrings::strToIntDef( $intSubsidyCertificationId, NULL, false ) );
	}

	public function getSubsidyCertificationId() {
		return $this->m_intSubsidyCertificationId;
	}

	public function sqlSubsidyCertificationId() {
		return ( true == isset( $this->m_intSubsidyCertificationId ) ) ? ( string ) $this->m_intSubsidyCertificationId : 'NULL';
	}

	public function setHudApprovedAmount( $fltHudApprovedAmount ) {
		$this->set( 'm_fltHudApprovedAmount', CStrings::strToFloatDef( $fltHudApprovedAmount, NULL, false, 2 ) );
	}

	public function getHudApprovedAmount() {
		return $this->m_fltHudApprovedAmount;
	}

	public function sqlHudApprovedAmount() {
		return ( true == isset( $this->m_fltHudApprovedAmount ) ) ? ( string ) $this->m_fltHudApprovedAmount : 'NULL';
	}

	public function setIsAdjustment( $boolIsAdjustment ) {
		$this->set( 'm_boolIsAdjustment', CStrings::strToBool( $boolIsAdjustment ) );
	}

	public function getIsAdjustment() {
		return $this->m_boolIsAdjustment;
	}

	public function sqlIsAdjustment() {
		return ( true == isset( $this->m_boolIsAdjustment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdjustment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setExcludedBy( $intExcludedBy ) {
		$this->set( 'm_intExcludedBy', CStrings::strToIntDef( $intExcludedBy, NULL, false ) );
	}

	public function getExcludedBy() {
		return $this->m_intExcludedBy;
	}

	public function sqlExcludedBy() {
		return ( true == isset( $this->m_intExcludedBy ) ) ? ( string ) $this->m_intExcludedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_hap_request_id, lease_id, ar_transaction_id, subsidy_certification_id, hud_approved_amount, is_adjustment, updated_by, updated_on, created_by, created_on, excluded_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSubsidyHapRequestId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlSubsidyCertificationId() . ', ' .
						$this->sqlHudApprovedAmount() . ', ' .
						$this->sqlIsAdjustment() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlExcludedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId(). ',' ; } elseif( true == array_key_exists( 'SubsidyHapRequestId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_id = ' . $this->sqlSubsidyCertificationId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_id = ' . $this->sqlSubsidyCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_approved_amount = ' . $this->sqlHudApprovedAmount(). ',' ; } elseif( true == array_key_exists( 'HudApprovedAmount', $this->getChangedColumns() ) ) { $strSql .= ' hud_approved_amount = ' . $this->sqlHudApprovedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment(). ',' ; } elseif( true == array_key_exists( 'IsAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' excluded_by = ' . $this->sqlExcludedBy(). ',' ; } elseif( true == array_key_exists( 'ExcludedBy', $this->getChangedColumns() ) ) { $strSql .= ' excluded_by = ' . $this->sqlExcludedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_hap_request_id' => $this->getSubsidyHapRequestId(),
			'lease_id' => $this->getLeaseId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'subsidy_certification_id' => $this->getSubsidyCertificationId(),
			'hud_approved_amount' => $this->getHudApprovedAmount(),
			'is_adjustment' => $this->getIsAdjustment(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'excluded_by' => $this->getExcludedBy()
		);
	}

}
?>
