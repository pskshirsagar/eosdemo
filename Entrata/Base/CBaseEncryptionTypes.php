<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEncryptionTypes
 * Do not add any new functions to this class.
 */

class CBaseEncryptionTypes extends CEosPluralBase {

	/**
	 * @return CEncryptionType[]
	 */
	public static function fetchEncryptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEncryptionType::class, $objDatabase );
	}

	/**
	 * @return CEncryptionType
	 */
	public static function fetchEncryptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEncryptionType::class, $objDatabase );
	}

	public static function fetchEncryptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'encryption_types', $objDatabase );
	}

	public static function fetchEncryptionTypeById( $intId, $objDatabase ) {
		return self::fetchEncryptionType( sprintf( 'SELECT * FROM encryption_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>