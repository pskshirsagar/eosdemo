<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyMessages extends CEosPluralBase {

	/**
	 * @return CSubsidyMessage[]
	 */
	public static function fetchSubsidyMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyMessage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyMessage
	 */
	public static function fetchSubsidyMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyMessage', $objDatabase );
	}

	public static function fetchSubsidyMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_messages', $objDatabase );
	}

	public static function fetchSubsidyMessageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessage( sprintf( 'SELECT * FROM subsidy_messages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesBySubsidyRequestIdByCid( $intSubsidyRequestId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE subsidy_request_id = %d AND cid = %d', ( int ) $intSubsidyRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesBySubsidyMessageTypeIdByCid( $intSubsidyMessageTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE subsidy_message_type_id = %d AND cid = %d', ( int ) $intSubsidyMessageTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesBySubsidyMessageSubTypeIdByCid( $intSubsidyMessageSubTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE subsidy_message_sub_type_id = %d AND cid = %d', ( int ) $intSubsidyMessageSubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesBySubsidyMessageStatusTypeIdByCid( $intSubsidyMessageStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE subsidy_message_status_type_id = %d AND cid = %d', ( int ) $intSubsidyMessageStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessagesBySubsidyMessageIdByCid( $intSubsidyMessageId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessages( sprintf( 'SELECT * FROM subsidy_messages WHERE subsidy_message_id = %d AND cid = %d', ( int ) $intSubsidyMessageId, ( int ) $intCid ), $objDatabase );
	}

}
?>