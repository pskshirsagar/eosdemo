<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAmenityAvailabilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAmenityAvailabilities extends CEosPluralBase {

	/**
	 * @return CPropertyAmenityAvailability[]
	 */
	public static function fetchPropertyAmenityAvailabilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyAmenityAvailability::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAmenityAvailability
	 */
	public static function fetchPropertyAmenityAvailability( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyAmenityAvailability::class, $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_amenity_availabilities', $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailability( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByAmenityIdByCid( $intAmenityId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE amenity_id = %d AND cid = %d', $intAmenityId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByRateAssociationIdByCid( $intRateAssociationId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE rate_association_id = %d AND cid = %d', $intRateAssociationId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByReservationFeeOccurrenceTypeIdByCid( $intReservationFeeOccurrenceTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE reservation_fee_occurrence_type_id = %d AND cid = %d', $intReservationFeeOccurrenceTypeId, $intCid ), $objDatabase );
	}

}
?>