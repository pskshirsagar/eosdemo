<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduledCallTypes extends CEosPluralBase {

	/**
	 * @return CScheduledCallType[]
	 */
	public static function fetchScheduledCallTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledCallType::class, $objDatabase );
	}

	/**
	 * @return CScheduledCallType
	 */
	public static function fetchScheduledCallType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledCallType::class, $objDatabase );
	}

	public static function fetchScheduledCallTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_call_types', $objDatabase );
	}

	public static function fetchScheduledCallTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledCallType( sprintf( 'SELECT * FROM scheduled_call_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScheduledCallTypesByCallFrequencyId( $intCallFrequencyId, $objDatabase ) {
		return self::fetchScheduledCallTypes( sprintf( 'SELECT * FROM scheduled_call_types WHERE call_frequency_id = %d', $intCallFrequencyId ), $objDatabase );
	}

	public static function fetchScheduledCallTypesByCallPriorityId( $intCallPriorityId, $objDatabase ) {
		return self::fetchScheduledCallTypes( sprintf( 'SELECT * FROM scheduled_call_types WHERE call_priority_id = %d', $intCallPriorityId ), $objDatabase );
	}

}
?>