<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAdvanceDayExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAdvanceDayExceptions extends CEosPluralBase {

	/**
	 * @return CPropertyAdvanceDayException[]
	 */
	public static function fetchPropertyAdvanceDayExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyAdvanceDayException', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAdvanceDayException
	 */
	public static function fetchPropertyAdvanceDayException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyAdvanceDayException', $objDatabase );
	}

	public static function fetchPropertyAdvanceDayExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_advance_day_exceptions', $objDatabase );
	}

	public static function fetchPropertyAdvanceDayExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAdvanceDayException( sprintf( 'SELECT * FROM property_advance_day_exceptions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAdvanceDayExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAdvanceDayExceptions( sprintf( 'SELECT * FROM property_advance_day_exceptions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAdvanceDayExceptionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAdvanceDayExceptions( sprintf( 'SELECT * FROM property_advance_day_exceptions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>