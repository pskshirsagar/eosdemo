<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGreetingTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyGreetingTypes extends CEosPluralBase {

	/**
	 * @return CPropertyGreetingType[]
	 */
	public static function fetchPropertyGreetingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyGreetingType::class, $objDatabase );
	}

	/**
	 * @return CPropertyGreetingType
	 */
	public static function fetchPropertyGreetingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyGreetingType::class, $objDatabase );
	}

	public static function fetchPropertyGreetingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_greeting_types', $objDatabase );
	}

	public static function fetchPropertyGreetingTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyGreetingType( sprintf( 'SELECT * FROM property_greeting_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>