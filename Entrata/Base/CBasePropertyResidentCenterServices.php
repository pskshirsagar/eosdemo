<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyResidentCenterServices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyResidentCenterServices extends CEosPluralBase {

	/**
	 * @return CPropertyResidentCenterService[]
	 */
	public static function fetchPropertyResidentCenterServices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyResidentCenterService', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyResidentCenterService
	 */
	public static function fetchPropertyResidentCenterService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyResidentCenterService', $objDatabase );
	}

	public static function fetchPropertyResidentCenterServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_resident_center_services', $objDatabase );
	}

	public static function fetchPropertyResidentCenterServiceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyResidentCenterService( sprintf( 'SELECT * FROM property_resident_center_services WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyResidentCenterServicesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyResidentCenterServices( sprintf( 'SELECT * FROM property_resident_center_services WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyResidentCenterServicesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyResidentCenterServices( sprintf( 'SELECT * FROM property_resident_center_services WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyResidentCenterServicesByResidentCenterServiceIdByCid( $intResidentCenterServiceId, $intCid, $objDatabase ) {
		return self::fetchPropertyResidentCenterServices( sprintf( 'SELECT * FROM property_resident_center_services WHERE resident_center_service_id = %d AND cid = %d', ( int ) $intResidentCenterServiceId, ( int ) $intCid ), $objDatabase );
	}

}
?>