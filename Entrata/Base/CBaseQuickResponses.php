<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CQuickResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuickResponses extends CEosPluralBase {

	/**
	 * @return CQuickResponse[]
	 */
	public static function fetchQuickResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CQuickResponse', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CQuickResponse
	 */
	public static function fetchQuickResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CQuickResponse', $objDatabase );
	}

	public static function fetchQuickResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'quick_responses', $objDatabase );
	}

	public static function fetchQuickResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchQuickResponse( sprintf( 'SELECT * FROM quick_responses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuickResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchQuickResponses( sprintf( 'SELECT * FROM quick_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuickResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchQuickResponses( sprintf( 'SELECT * FROM quick_responses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuickResponsesBySettingsTemplateIdByCid( $intSettingsTemplateId, $intCid, $objDatabase ) {
		return self::fetchQuickResponses( sprintf( 'SELECT * FROM quick_responses WHERE settings_template_id = %d AND cid = %d', ( int ) $intSettingsTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuickResponsesByHeaderFooterOptionTypeIdByCid( $intHeaderFooterOptionTypeId, $intCid, $objDatabase ) {
		return self::fetchQuickResponses( sprintf( 'SELECT * FROM quick_responses WHERE header_footer_option_type_id = %d AND cid = %d', ( int ) $intHeaderFooterOptionTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>