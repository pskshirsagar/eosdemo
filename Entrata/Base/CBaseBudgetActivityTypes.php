<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetActivityTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetActivityTypes extends CEosPluralBase {

	/**
	 * @return CBudgetActivityType[]
	 */
	public static function fetchBudgetActivityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetActivityType::class, $objDatabase );
	}

	/**
	 * @return CBudgetActivityType
	 */
	public static function fetchBudgetActivityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetActivityType::class, $objDatabase );
	}

	public static function fetchBudgetActivityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_activity_types', $objDatabase );
	}

	public static function fetchBudgetActivityTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetActivityType( sprintf( 'SELECT * FROM budget_activity_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>