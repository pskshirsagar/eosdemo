<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUnitMaintenanceLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUnitMaintenanceLocations extends CEosPluralBase {

	/**
	 * @return CPropertyUnitMaintenanceLocation[]
	 */
	public static function fetchPropertyUnitMaintenanceLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyUnitMaintenanceLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyUnitMaintenanceLocation
	 */
	public static function fetchPropertyUnitMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyUnitMaintenanceLocation::class, $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_unit_maintenance_locations', $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitMaintenanceLocation( sprintf( 'SELECT * FROM property_unit_maintenance_locations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyUnitMaintenanceLocations( sprintf( 'SELECT * FROM property_unit_maintenance_locations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitMaintenanceLocations( sprintf( 'SELECT * FROM property_unit_maintenance_locations WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitMaintenanceLocations( sprintf( 'SELECT * FROM property_unit_maintenance_locations WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitMaintenanceLocationsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitMaintenanceLocations( sprintf( 'SELECT * FROM property_unit_maintenance_locations WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

}
?>