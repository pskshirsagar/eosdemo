<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArTierLevels extends CEosPluralBase {

    const TABLE_AR_TIER_LEVELS = 'public.ar_tier_levels';

    public static function fetchArTierLevels( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CArTierLevel', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchArTierLevel( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArTierLevel', $objDatabase );
    }

    public static function fetchArTierLevelCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_tier_levels', $objDatabase );
    }

    public static function fetchArTierLevelByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchArTierLevel( sprintf( 'SELECT * FROM ar_tier_levels WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArTierLevelsByCid( $intCid, $objDatabase ) {
        return self::fetchArTierLevels( sprintf( 'SELECT * FROM ar_tier_levels WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchArTierLevelsByArTierIdByCid( $intArTierId, $intCid, $objDatabase ) {
        return self::fetchArTierLevels( sprintf( 'SELECT * FROM ar_tier_levels WHERE ar_tier_id = %d AND cid = %d', (int) $intArTierId, (int) $intCid ), $objDatabase );
    }

}
?>