<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCustomDataKeyValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCustomDataKeyValues extends CEosPluralBase {

	/**
	 * @return CPropertyCustomDataKeyValue[]
	 */
	public static function fetchPropertyCustomDataKeyValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyCustomDataKeyValue::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCustomDataKeyValue
	 */
	public static function fetchPropertyCustomDataKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyCustomDataKeyValue::class, $objDatabase );
	}

	public static function fetchPropertyCustomDataKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_custom_data_key_values', $objDatabase );
	}

	public static function fetchPropertyCustomDataKeyValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomDataKeyValue( sprintf( 'SELECT * FROM property_custom_data_key_values WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomDataKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCustomDataKeyValues( sprintf( 'SELECT * FROM property_custom_data_key_values WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomDataKeyValuesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomDataKeyValues( sprintf( 'SELECT * FROM property_custom_data_key_values WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>