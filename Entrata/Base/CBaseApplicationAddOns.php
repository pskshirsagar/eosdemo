<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationAddOns
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationAddOns extends CEosPluralBase {

	/**
	 * @return CApplicationAddOn[]
	 */
	public static function fetchApplicationAddOns( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationAddOn', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationAddOn
	 */
	public static function fetchApplicationAddOn( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationAddOn', $objDatabase );
	}

	public static function fetchApplicationAddOnCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_add_ons', $objDatabase );
	}

	public static function fetchApplicationAddOnByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationAddOn( sprintf( 'SELECT * FROM application_add_ons WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAddOnsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationAddOns( sprintf( 'SELECT * FROM application_add_ons WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAddOnsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplicationAddOns( sprintf( 'SELECT * FROM application_add_ons WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAddOnsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationAddOns( sprintf( 'SELECT * FROM application_add_ons WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAddOnsByAddOnIdByCid( $intAddOnId, $intCid, $objDatabase ) {
		return self::fetchApplicationAddOns( sprintf( 'SELECT * FROM application_add_ons WHERE add_on_id = %d AND cid = %d', ( int ) $intAddOnId, ( int ) $intCid ), $objDatabase );
	}

}
?>