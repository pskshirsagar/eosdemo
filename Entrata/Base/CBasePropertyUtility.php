<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUtility extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_utilities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intArCascadeId;
	protected $m_intArCascadeReferenceId;
	protected $m_intUtilityId;
	protected $m_intUtilityFormulaId;
	protected $m_intApPayeeLocationId;
	protected $m_intAmenityId;
	protected $m_strRemotePrimaryKey;
	protected $m_fltMinimumCostPerSpace;
	protected $m_fltAverageCostPerSpace;
	protected $m_fltMaximumCostPerSpace;
	protected $m_strResponsibilityDescription;
	protected $m_boolIsResponsible;
	protected $m_boolShowResponsibilityOnPortal;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intApPayeeId;

	public function __construct() {
		parent::__construct();

		$this->m_fltMinimumCostPerSpace = '0';
		$this->m_fltAverageCostPerSpace = '0';
		$this->m_fltMaximumCostPerSpace = '0';
		$this->m_boolIsResponsible = true;
		$this->m_boolShowResponsibilityOnPortal = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeId', trim( $arrValues['ar_cascade_id'] ) ); elseif( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
		if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeReferenceId', trim( $arrValues['ar_cascade_reference_id'] ) ); elseif( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
		if( isset( $arrValues['utility_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityId', trim( $arrValues['utility_id'] ) ); elseif( isset( $arrValues['utility_id'] ) ) $this->setUtilityId( $arrValues['utility_id'] );
		if( isset( $arrValues['utility_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityFormulaId', trim( $arrValues['utility_formula_id'] ) ); elseif( isset( $arrValues['utility_formula_id'] ) ) $this->setUtilityFormulaId( $arrValues['utility_formula_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityId', trim( $arrValues['amenity_id'] ) ); elseif( isset( $arrValues['amenity_id'] ) ) $this->setAmenityId( $arrValues['amenity_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['minimum_cost_per_space'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumCostPerSpace', trim( $arrValues['minimum_cost_per_space'] ) ); elseif( isset( $arrValues['minimum_cost_per_space'] ) ) $this->setMinimumCostPerSpace( $arrValues['minimum_cost_per_space'] );
		if( isset( $arrValues['average_cost_per_space'] ) && $boolDirectSet ) $this->set( 'm_fltAverageCostPerSpace', trim( $arrValues['average_cost_per_space'] ) ); elseif( isset( $arrValues['average_cost_per_space'] ) ) $this->setAverageCostPerSpace( $arrValues['average_cost_per_space'] );
		if( isset( $arrValues['maximum_cost_per_space'] ) && $boolDirectSet ) $this->set( 'm_fltMaximumCostPerSpace', trim( $arrValues['maximum_cost_per_space'] ) ); elseif( isset( $arrValues['maximum_cost_per_space'] ) ) $this->setMaximumCostPerSpace( $arrValues['maximum_cost_per_space'] );
		if( isset( $arrValues['responsibility_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strResponsibilityDescription', trim( $arrValues['responsibility_description'] ) ); elseif( isset( $arrValues['responsibility_description'] ) ) $this->setResponsibilityDescription( $arrValues['responsibility_description'] );
		if( isset( $arrValues['is_responsible'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponsible', trim( stripcslashes( $arrValues['is_responsible'] ) ) ); elseif( isset( $arrValues['is_responsible'] ) ) $this->setIsResponsible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible'] ) : $arrValues['is_responsible'] );
		if( isset( $arrValues['show_responsibility_on_portal'] ) && $boolDirectSet ) $this->set( 'm_boolShowResponsibilityOnPortal', trim( stripcslashes( $arrValues['show_responsibility_on_portal'] ) ) ); elseif( isset( $arrValues['show_responsibility_on_portal'] ) ) $this->setShowResponsibilityOnPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_responsibility_on_portal'] ) : $arrValues['show_responsibility_on_portal'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->set( 'm_intArCascadeId', CStrings::strToIntDef( $intArCascadeId, NULL, false ) );
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function sqlArCascadeId() {
		return ( true == isset( $this->m_intArCascadeId ) ) ? ( string ) $this->m_intArCascadeId : 'NULL';
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->set( 'm_intArCascadeReferenceId', CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false ) );
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function sqlArCascadeReferenceId() {
		return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? ( string ) $this->m_intArCascadeReferenceId : 'NULL';
	}

	public function setUtilityId( $intUtilityId ) {
		$this->set( 'm_intUtilityId', CStrings::strToIntDef( $intUtilityId, NULL, false ) );
	}

	public function getUtilityId() {
		return $this->m_intUtilityId;
	}

	public function sqlUtilityId() {
		return ( true == isset( $this->m_intUtilityId ) ) ? ( string ) $this->m_intUtilityId : 'NULL';
	}

	public function setUtilityFormulaId( $intUtilityFormulaId ) {
		$this->set( 'm_intUtilityFormulaId', CStrings::strToIntDef( $intUtilityFormulaId, NULL, false ) );
	}

	public function getUtilityFormulaId() {
		return $this->m_intUtilityFormulaId;
	}

	public function sqlUtilityFormulaId() {
		return ( true == isset( $this->m_intUtilityFormulaId ) ) ? ( string ) $this->m_intUtilityFormulaId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setAmenityId( $intAmenityId ) {
		$this->set( 'm_intAmenityId', CStrings::strToIntDef( $intAmenityId, NULL, false ) );
	}

	public function getAmenityId() {
		return $this->m_intAmenityId;
	}

	public function sqlAmenityId() {
		return ( true == isset( $this->m_intAmenityId ) ) ? ( string ) $this->m_intAmenityId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setMinimumCostPerSpace( $fltMinimumCostPerSpace ) {
		$this->set( 'm_fltMinimumCostPerSpace', CStrings::strToFloatDef( $fltMinimumCostPerSpace, NULL, false, 2 ) );
	}

	public function getMinimumCostPerSpace() {
		return $this->m_fltMinimumCostPerSpace;
	}

	public function sqlMinimumCostPerSpace() {
		return ( true == isset( $this->m_fltMinimumCostPerSpace ) ) ? ( string ) $this->m_fltMinimumCostPerSpace : '0';
	}

	public function setAverageCostPerSpace( $fltAverageCostPerSpace ) {
		$this->set( 'm_fltAverageCostPerSpace', CStrings::strToFloatDef( $fltAverageCostPerSpace, NULL, false, 2 ) );
	}

	public function getAverageCostPerSpace() {
		return $this->m_fltAverageCostPerSpace;
	}

	public function sqlAverageCostPerSpace() {
		return ( true == isset( $this->m_fltAverageCostPerSpace ) ) ? ( string ) $this->m_fltAverageCostPerSpace : '0';
	}

	public function setMaximumCostPerSpace( $fltMaximumCostPerSpace ) {
		$this->set( 'm_fltMaximumCostPerSpace', CStrings::strToFloatDef( $fltMaximumCostPerSpace, NULL, false, 2 ) );
	}

	public function getMaximumCostPerSpace() {
		return $this->m_fltMaximumCostPerSpace;
	}

	public function sqlMaximumCostPerSpace() {
		return ( true == isset( $this->m_fltMaximumCostPerSpace ) ) ? ( string ) $this->m_fltMaximumCostPerSpace : '0';
	}

	public function setResponsibilityDescription( $strResponsibilityDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strResponsibilityDescription', CStrings::strTrimDef( $strResponsibilityDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getResponsibilityDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strResponsibilityDescription', $strLocaleCode );
	}

	public function sqlResponsibilityDescription() {
		return ( true == isset( $this->m_strResponsibilityDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponsibilityDescription ) : '\'' . addslashes( $this->m_strResponsibilityDescription ) . '\'' ) : 'NULL';
	}

	public function setIsResponsible( $boolIsResponsible ) {
		$this->set( 'm_boolIsResponsible', CStrings::strToBool( $boolIsResponsible ) );
	}

	public function getIsResponsible() {
		return $this->m_boolIsResponsible;
	}

	public function sqlIsResponsible() {
		return ( true == isset( $this->m_boolIsResponsible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowResponsibilityOnPortal( $boolShowResponsibilityOnPortal ) {
		$this->set( 'm_boolShowResponsibilityOnPortal', CStrings::strToBool( $boolShowResponsibilityOnPortal ) );
	}

	public function getShowResponsibilityOnPortal() {
		return $this->m_boolShowResponsibilityOnPortal;
	}

	public function sqlShowResponsibilityOnPortal() {
		return ( true == isset( $this->m_boolShowResponsibilityOnPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowResponsibilityOnPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ar_cascade_id, ar_cascade_reference_id, utility_id, utility_formula_id, ap_payee_location_id, amenity_id, remote_primary_key, minimum_cost_per_space, average_cost_per_space, maximum_cost_per_space, responsibility_description, is_responsible, show_responsibility_on_portal, updated_by, updated_on, created_by, created_on, details, ap_payee_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlArCascadeId() . ', ' .
						$this->sqlArCascadeReferenceId() . ', ' .
						$this->sqlUtilityId() . ', ' .
						$this->sqlUtilityFormulaId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlAmenityId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlMinimumCostPerSpace() . ', ' .
						$this->sqlAverageCostPerSpace() . ', ' .
						$this->sqlMaximumCostPerSpace() . ', ' .
						$this->sqlResponsibilityDescription() . ', ' .
						$this->sqlIsResponsible() . ', ' .
						$this->sqlShowResponsibilityOnPortal() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlApPayeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_id = ' . $this->sqlUtilityId(). ',' ; } elseif( true == array_key_exists( 'UtilityId', $this->getChangedColumns() ) ) { $strSql .= ' utility_id = ' . $this->sqlUtilityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_formula_id = ' . $this->sqlUtilityFormulaId(). ',' ; } elseif( true == array_key_exists( 'UtilityFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_formula_id = ' . $this->sqlUtilityFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId(). ',' ; } elseif( true == array_key_exists( 'AmenityId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_cost_per_space = ' . $this->sqlMinimumCostPerSpace(). ',' ; } elseif( true == array_key_exists( 'MinimumCostPerSpace', $this->getChangedColumns() ) ) { $strSql .= ' minimum_cost_per_space = ' . $this->sqlMinimumCostPerSpace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_cost_per_space = ' . $this->sqlAverageCostPerSpace(). ',' ; } elseif( true == array_key_exists( 'AverageCostPerSpace', $this->getChangedColumns() ) ) { $strSql .= ' average_cost_per_space = ' . $this->sqlAverageCostPerSpace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_cost_per_space = ' . $this->sqlMaximumCostPerSpace(). ',' ; } elseif( true == array_key_exists( 'MaximumCostPerSpace', $this->getChangedColumns() ) ) { $strSql .= ' maximum_cost_per_space = ' . $this->sqlMaximumCostPerSpace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responsibility_description = ' . $this->sqlResponsibilityDescription(). ',' ; } elseif( true == array_key_exists( 'ResponsibilityDescription', $this->getChangedColumns() ) ) { $strSql .= ' responsibility_description = ' . $this->sqlResponsibilityDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible = ' . $this->sqlIsResponsible(). ',' ; } elseif( true == array_key_exists( 'IsResponsible', $this->getChangedColumns() ) ) { $strSql .= ' is_responsible = ' . $this->sqlIsResponsible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_responsibility_on_portal = ' . $this->sqlShowResponsibilityOnPortal(). ',' ; } elseif( true == array_key_exists( 'ShowResponsibilityOnPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_responsibility_on_portal = ' . $this->sqlShowResponsibilityOnPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ar_cascade_id' => $this->getArCascadeId(),
			'ar_cascade_reference_id' => $this->getArCascadeReferenceId(),
			'utility_id' => $this->getUtilityId(),
			'utility_formula_id' => $this->getUtilityFormulaId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'amenity_id' => $this->getAmenityId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'minimum_cost_per_space' => $this->getMinimumCostPerSpace(),
			'average_cost_per_space' => $this->getAverageCostPerSpace(),
			'maximum_cost_per_space' => $this->getMaximumCostPerSpace(),
			'responsibility_description' => $this->getResponsibilityDescription(),
			'is_responsible' => $this->getIsResponsible(),
			'show_responsibility_on_portal' => $this->getShowResponsibilityOnPortal(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'ap_payee_id' => $this->getApPayeeId()
		);
	}

}
?>