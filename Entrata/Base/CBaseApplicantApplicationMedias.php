<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationMedias
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationMedias extends CEosPluralBase {

	/**
	 * @return CApplicantApplicationMedia[]
	 */
	public static function fetchApplicantApplicationMedias( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantApplicationMedia', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantApplicationMedia
	 */
	public static function fetchApplicantApplicationMedia( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantApplicationMedia', $objDatabase );
	}

	public static function fetchApplicantApplicationMediaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_application_medias', $objDatabase );
	}

	public static function fetchApplicantApplicationMediaByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationMedia( sprintf( 'SELECT * FROM applicant_application_medias WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationMediasByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationMedias( sprintf( 'SELECT * FROM applicant_application_medias WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationMediasByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationMedias( sprintf( 'SELECT * FROM applicant_application_medias WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationMediasByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationMedias( sprintf( 'SELECT * FROM applicant_application_medias WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>