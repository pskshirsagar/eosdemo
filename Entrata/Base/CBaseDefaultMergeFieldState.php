<?php

class CBaseDefaultMergeFieldState extends CEosSingularBase {

	const TABLE_NAME = 'public.default_merge_field_states';

	protected $m_intId;
	protected $m_intDefaultMergeFieldId;
	protected $m_intMergeFieldGroupId;
	protected $m_strStateCode;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMergeFieldId', trim( $arrValues['default_merge_field_id'] ) ); elseif( isset( $arrValues['default_merge_field_id'] ) ) $this->setDefaultMergeFieldId( $arrValues['default_merge_field_id'] );
		if( isset( $arrValues['merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldGroupId', trim( $arrValues['merge_field_group_id'] ) ); elseif( isset( $arrValues['merge_field_group_id'] ) ) $this->setMergeFieldGroupId( $arrValues['merge_field_group_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultMergeFieldId( $intDefaultMergeFieldId ) {
		$this->set( 'm_intDefaultMergeFieldId', CStrings::strToIntDef( $intDefaultMergeFieldId, NULL, false ) );
	}

	public function getDefaultMergeFieldId() {
		return $this->m_intDefaultMergeFieldId;
	}

	public function sqlDefaultMergeFieldId() {
		return ( true == isset( $this->m_intDefaultMergeFieldId ) ) ? ( string ) $this->m_intDefaultMergeFieldId : 'NULL';
	}

	public function setMergeFieldGroupId( $intMergeFieldGroupId ) {
		$this->set( 'm_intMergeFieldGroupId', CStrings::strToIntDef( $intMergeFieldGroupId, NULL, false ) );
	}

	public function getMergeFieldGroupId() {
		return $this->m_intMergeFieldGroupId;
	}

	public function sqlMergeFieldGroupId() {
		return ( true == isset( $this->m_intMergeFieldGroupId ) ) ? ( string ) $this->m_intMergeFieldGroupId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_merge_field_id' => $this->getDefaultMergeFieldId(),
			'merge_field_group_id' => $this->getMergeFieldGroupId(),
			'state_code' => $this->getStateCode()
		);
	}

}
?>