<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationNotices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationNotices extends CEosPluralBase {

	/**
	 * @return CViolationNotice[]
	 */
	public static function fetchViolationNotices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolationNotice::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolationNotice
	 */
	public static function fetchViolationNotice( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationNotice::class, $objDatabase );
	}

	public static function fetchViolationNoticeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_notices', $objDatabase );
	}

	public static function fetchViolationNoticeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolationNotice( sprintf( 'SELECT * FROM violation_notices WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByCid( $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByViolationIdByCid( $intViolationId, $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE violation_id = %d AND cid = %d', $intViolationId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNoticesByViolationTemplateNoticeIdByCid( $intViolationTemplateNoticeId, $intCid, $objDatabase ) {
		return self::fetchViolationNotices( sprintf( 'SELECT * FROM violation_notices WHERE violation_template_notice_id = %d AND cid = %d', $intViolationTemplateNoticeId, $intCid ), $objDatabase );
	}

}
?>