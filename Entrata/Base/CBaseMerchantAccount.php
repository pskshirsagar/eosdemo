<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMerchantAccount extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.merchant_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProcessingBankId;
	protected $m_intFundMovementTypeId;
	protected $m_intCommercialMerchantAccountId;
	protected $m_strCurrencyCode;
	protected $m_strCountryCode;
	protected $m_intDistributionAccountId;
	protected $m_intIntermediaryAccountId;
	protected $m_intBillingAccountId;
	protected $m_intReturnsAccountId;
	protected $m_intClearingAccountId;
	protected $m_intDonationAccountId;
	protected $m_intRebateAccountId;
	protected $m_intMerchantProcessingTypeId;
	protected $m_intDistributionGroupTypeId;
	protected $m_intAllocationTypeId;
	protected $m_strAccountName;
	protected $m_strDescription;
	protected $m_strNotificationEmail;
	protected $m_strNotes;
	protected $m_strMerchantName;
	protected $m_strTransactionDescription;
	protected $m_strPortalInputLabel;
	protected $m_strTaxLegalName;
	protected $m_strTaxNumberEncrypted;
	protected $m_strExternalLegalEntityId;
	protected $m_fltDistributionFee;
	protected $m_fltDistributionFailureFee;
	protected $m_fltFixedConvenienceFee;
	protected $m_fltAcquireProcessingFee;
	protected $m_fltTargetDiscountRate;
	protected $m_strDisableDistributionUntil;
	protected $m_intEnableFeeRestructuring;
	protected $m_intEnablePinlessDebitProcessing;
	protected $m_intEnableSeparateDebitPricing;
	protected $m_boolEnableVisaPilot;
	protected $m_intAccountVerificationType;
	protected $m_boolRequireCvv;
	protected $m_fltApPaymentFee;
	protected $m_fltApPaymentFailureFee;
	protected $m_fltApPaymentReversalFee;
	protected $m_fltApCheckFee;
	protected $m_intApMinimumDelayDays;
	protected $m_boolApAchIsDisabled;
	protected $m_fltApAchMonthlyVolumeLimit;
	protected $m_fltApAchMonthlyRunningTotal;
	protected $m_fltApAchMaxPaymentAmount;
	protected $m_fltApAchBalanceCheckAmount;
	protected $m_fltApAchBalanceCheckFee;
	protected $m_fltApAchAccountVerificationFee;
	protected $m_intApPrintServiceLevelAgreement;
	protected $m_intNumAchReturnRetries;
	protected $m_intAllowInternationalCards;
	protected $m_intBillInternationalCards;
	protected $m_intRequireSeparateSettlement;
	protected $m_intConvertCheck21ToAch;
	protected $m_intIsAggregated;
	protected $m_intIsDisabled;
	protected $m_intIsCompanyChargeIncentivesActivated;
	protected $m_strTaxVerifiedOn;
	protected $m_strUnderwritingRequestDate;
	protected $m_strUnderwritingSentOn;
	protected $m_strUnderwrittenOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_strCountryCode = 'US';
		$this->m_intMerchantProcessingTypeId = '1';
		$this->m_intAllocationTypeId = '1';
		$this->m_fltDistributionFee = '0';
		$this->m_fltDistributionFailureFee = '0';
		$this->m_fltAcquireProcessingFee = '0';
		$this->m_intEnableFeeRestructuring = '0';
		$this->m_intEnablePinlessDebitProcessing = '0';
		$this->m_intEnableSeparateDebitPricing = '0';
		$this->m_boolEnableVisaPilot = false;
		$this->m_intAccountVerificationType = '0';
		$this->m_boolRequireCvv = false;
		$this->m_fltApPaymentFee = '0';
		$this->m_fltApPaymentFailureFee = '0';
		$this->m_fltApCheckFee = '0';
		$this->m_intApMinimumDelayDays = '2';
		$this->m_boolApAchIsDisabled = false;
		$this->m_fltApAchMonthlyVolumeLimit = '100000';
		$this->m_fltApAchMonthlyRunningTotal = '0';
		$this->m_fltApAchMaxPaymentAmount = '0';
		$this->m_fltApAchBalanceCheckAmount = '0';
		$this->m_fltApAchBalanceCheckFee = '0';
		$this->m_fltApAchAccountVerificationFee = '0';
		$this->m_intApPrintServiceLevelAgreement = '1';
		$this->m_intNumAchReturnRetries = '0';
		$this->m_intAllowInternationalCards = '0';
		$this->m_intBillInternationalCards = '0';
		$this->m_intRequireSeparateSettlement = '0';
		$this->m_intConvertCheck21ToAch = '0';
		$this->m_intIsAggregated = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsCompanyChargeIncentivesActivated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['fund_movement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFundMovementTypeId', trim( $arrValues['fund_movement_type_id'] ) ); elseif( isset( $arrValues['fund_movement_type_id'] ) ) $this->setFundMovementTypeId( $arrValues['fund_movement_type_id'] );
		if( isset( $arrValues['commercial_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCommercialMerchantAccountId', trim( $arrValues['commercial_merchant_account_id'] ) ); elseif( isset( $arrValues['commercial_merchant_account_id'] ) ) $this->setCommercialMerchantAccountId( $arrValues['commercial_merchant_account_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['distribution_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDistributionAccountId', trim( $arrValues['distribution_account_id'] ) ); elseif( isset( $arrValues['distribution_account_id'] ) ) $this->setDistributionAccountId( $arrValues['distribution_account_id'] );
		if( isset( $arrValues['intermediary_account_id'] ) && $boolDirectSet ) $this->set( 'm_intIntermediaryAccountId', trim( $arrValues['intermediary_account_id'] ) ); elseif( isset( $arrValues['intermediary_account_id'] ) ) $this->setIntermediaryAccountId( $arrValues['intermediary_account_id'] );
		if( isset( $arrValues['billing_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingAccountId', trim( $arrValues['billing_account_id'] ) ); elseif( isset( $arrValues['billing_account_id'] ) ) $this->setBillingAccountId( $arrValues['billing_account_id'] );
		if( isset( $arrValues['returns_account_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnsAccountId', trim( $arrValues['returns_account_id'] ) ); elseif( isset( $arrValues['returns_account_id'] ) ) $this->setReturnsAccountId( $arrValues['returns_account_id'] );
		if( isset( $arrValues['clearing_account_id'] ) && $boolDirectSet ) $this->set( 'm_intClearingAccountId', trim( $arrValues['clearing_account_id'] ) ); elseif( isset( $arrValues['clearing_account_id'] ) ) $this->setClearingAccountId( $arrValues['clearing_account_id'] );
		if( isset( $arrValues['donation_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDonationAccountId', trim( $arrValues['donation_account_id'] ) ); elseif( isset( $arrValues['donation_account_id'] ) ) $this->setDonationAccountId( $arrValues['donation_account_id'] );
		if( isset( $arrValues['rebate_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRebateAccountId', trim( $arrValues['rebate_account_id'] ) ); elseif( isset( $arrValues['rebate_account_id'] ) ) $this->setRebateAccountId( $arrValues['rebate_account_id'] );
		if( isset( $arrValues['merchant_processing_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantProcessingTypeId', trim( $arrValues['merchant_processing_type_id'] ) ); elseif( isset( $arrValues['merchant_processing_type_id'] ) ) $this->setMerchantProcessingTypeId( $arrValues['merchant_processing_type_id'] );
		if( isset( $arrValues['distribution_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDistributionGroupTypeId', trim( $arrValues['distribution_group_type_id'] ) ); elseif( isset( $arrValues['distribution_group_type_id'] ) ) $this->setDistributionGroupTypeId( $arrValues['distribution_group_type_id'] );
		if( isset( $arrValues['allocation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAllocationTypeId', trim( $arrValues['allocation_type_id'] ) ); elseif( isset( $arrValues['allocation_type_id'] ) ) $this->setAllocationTypeId( $arrValues['allocation_type_id'] );
		if( isset( $arrValues['account_name'] ) && $boolDirectSet ) $this->set( 'm_strAccountName', trim( stripcslashes( $arrValues['account_name'] ) ) ); elseif( isset( $arrValues['account_name'] ) ) $this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_name'] ) : $arrValues['account_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['notification_email'] ) && $boolDirectSet ) $this->set( 'm_strNotificationEmail', trim( stripcslashes( $arrValues['notification_email'] ) ) ); elseif( isset( $arrValues['notification_email'] ) ) $this->setNotificationEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notification_email'] ) : $arrValues['notification_email'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['merchant_name'] ) && $boolDirectSet ) $this->set( 'm_strMerchantName', trim( stripcslashes( $arrValues['merchant_name'] ) ) ); elseif( isset( $arrValues['merchant_name'] ) ) $this->setMerchantName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['merchant_name'] ) : $arrValues['merchant_name'] );
		if( isset( $arrValues['transaction_description'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDescription', trim( stripcslashes( $arrValues['transaction_description'] ) ) ); elseif( isset( $arrValues['transaction_description'] ) ) $this->setTransactionDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_description'] ) : $arrValues['transaction_description'] );
		if( isset( $arrValues['portal_input_label'] ) && $boolDirectSet ) $this->set( 'm_strPortalInputLabel', trim( stripcslashes( $arrValues['portal_input_label'] ) ) ); elseif( isset( $arrValues['portal_input_label'] ) ) $this->setPortalInputLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['portal_input_label'] ) : $arrValues['portal_input_label'] );
		if( isset( $arrValues['tax_legal_name'] ) && $boolDirectSet ) $this->set( 'm_strTaxLegalName', trim( stripcslashes( $arrValues['tax_legal_name'] ) ) ); elseif( isset( $arrValues['tax_legal_name'] ) ) $this->setTaxLegalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_legal_name'] ) : $arrValues['tax_legal_name'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['external_legal_entity_id'] ) && $boolDirectSet ) $this->set( 'm_strExternalLegalEntityId', trim( stripcslashes( $arrValues['external_legal_entity_id'] ) ) ); elseif( isset( $arrValues['external_legal_entity_id'] ) ) $this->setExternalLegalEntityId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_legal_entity_id'] ) : $arrValues['external_legal_entity_id'] );
		if( isset( $arrValues['distribution_fee'] ) && $boolDirectSet ) $this->set( 'm_fltDistributionFee', trim( $arrValues['distribution_fee'] ) ); elseif( isset( $arrValues['distribution_fee'] ) ) $this->setDistributionFee( $arrValues['distribution_fee'] );
		if( isset( $arrValues['distribution_failure_fee'] ) && $boolDirectSet ) $this->set( 'm_fltDistributionFailureFee', trim( $arrValues['distribution_failure_fee'] ) ); elseif( isset( $arrValues['distribution_failure_fee'] ) ) $this->setDistributionFailureFee( $arrValues['distribution_failure_fee'] );
		if( isset( $arrValues['fixed_convenience_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFixedConvenienceFee', trim( $arrValues['fixed_convenience_fee'] ) ); elseif( isset( $arrValues['fixed_convenience_fee'] ) ) $this->setFixedConvenienceFee( $arrValues['fixed_convenience_fee'] );
		if( isset( $arrValues['acquire_processing_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAcquireProcessingFee', trim( $arrValues['acquire_processing_fee'] ) ); elseif( isset( $arrValues['acquire_processing_fee'] ) ) $this->setAcquireProcessingFee( $arrValues['acquire_processing_fee'] );
		if( isset( $arrValues['target_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltTargetDiscountRate', trim( $arrValues['target_discount_rate'] ) ); elseif( isset( $arrValues['target_discount_rate'] ) ) $this->setTargetDiscountRate( $arrValues['target_discount_rate'] );
		if( isset( $arrValues['disable_distribution_until'] ) && $boolDirectSet ) $this->set( 'm_strDisableDistributionUntil', trim( $arrValues['disable_distribution_until'] ) ); elseif( isset( $arrValues['disable_distribution_until'] ) ) $this->setDisableDistributionUntil( $arrValues['disable_distribution_until'] );
		if( isset( $arrValues['enable_fee_restructuring'] ) && $boolDirectSet ) $this->set( 'm_intEnableFeeRestructuring', trim( $arrValues['enable_fee_restructuring'] ) ); elseif( isset( $arrValues['enable_fee_restructuring'] ) ) $this->setEnableFeeRestructuring( $arrValues['enable_fee_restructuring'] );
		if( isset( $arrValues['enable_pinless_debit_processing'] ) && $boolDirectSet ) $this->set( 'm_intEnablePinlessDebitProcessing', trim( $arrValues['enable_pinless_debit_processing'] ) ); elseif( isset( $arrValues['enable_pinless_debit_processing'] ) ) $this->setEnablePinlessDebitProcessing( $arrValues['enable_pinless_debit_processing'] );
		if( isset( $arrValues['enable_separate_debit_pricing'] ) && $boolDirectSet ) $this->set( 'm_intEnableSeparateDebitPricing', trim( $arrValues['enable_separate_debit_pricing'] ) ); elseif( isset( $arrValues['enable_separate_debit_pricing'] ) ) $this->setEnableSeparateDebitPricing( $arrValues['enable_separate_debit_pricing'] );
		if( isset( $arrValues['enable_visa_pilot'] ) && $boolDirectSet ) $this->set( 'm_boolEnableVisaPilot', trim( stripcslashes( $arrValues['enable_visa_pilot'] ) ) ); elseif( isset( $arrValues['enable_visa_pilot'] ) ) $this->setEnableVisaPilot( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['enable_visa_pilot'] ) : $arrValues['enable_visa_pilot'] );
		if( isset( $arrValues['account_verification_type'] ) && $boolDirectSet ) $this->set( 'm_intAccountVerificationType', trim( $arrValues['account_verification_type'] ) ); elseif( isset( $arrValues['account_verification_type'] ) ) $this->setAccountVerificationType( $arrValues['account_verification_type'] );
		if( isset( $arrValues['require_cvv'] ) && $boolDirectSet ) $this->set( 'm_boolRequireCvv', trim( stripcslashes( $arrValues['require_cvv'] ) ) ); elseif( isset( $arrValues['require_cvv'] ) ) $this->setRequireCvv( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_cvv'] ) : $arrValues['require_cvv'] );
		if( isset( $arrValues['ap_payment_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApPaymentFee', trim( $arrValues['ap_payment_fee'] ) ); elseif( isset( $arrValues['ap_payment_fee'] ) ) $this->setApPaymentFee( $arrValues['ap_payment_fee'] );
		if( isset( $arrValues['ap_payment_failure_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApPaymentFailureFee', trim( $arrValues['ap_payment_failure_fee'] ) ); elseif( isset( $arrValues['ap_payment_failure_fee'] ) ) $this->setApPaymentFailureFee( $arrValues['ap_payment_failure_fee'] );
		if( isset( $arrValues['ap_payment_reversal_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApPaymentReversalFee', trim( $arrValues['ap_payment_reversal_fee'] ) ); elseif( isset( $arrValues['ap_payment_reversal_fee'] ) ) $this->setApPaymentReversalFee( $arrValues['ap_payment_reversal_fee'] );
		if( isset( $arrValues['ap_check_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApCheckFee', trim( $arrValues['ap_check_fee'] ) ); elseif( isset( $arrValues['ap_check_fee'] ) ) $this->setApCheckFee( $arrValues['ap_check_fee'] );
		if( isset( $arrValues['ap_minimum_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intApMinimumDelayDays', trim( $arrValues['ap_minimum_delay_days'] ) ); elseif( isset( $arrValues['ap_minimum_delay_days'] ) ) $this->setApMinimumDelayDays( $arrValues['ap_minimum_delay_days'] );
		if( isset( $arrValues['ap_ach_is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolApAchIsDisabled', trim( stripcslashes( $arrValues['ap_ach_is_disabled'] ) ) ); elseif( isset( $arrValues['ap_ach_is_disabled'] ) ) $this->setApAchIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ap_ach_is_disabled'] ) : $arrValues['ap_ach_is_disabled'] );
		if( isset( $arrValues['ap_ach_monthly_volume_limit'] ) && $boolDirectSet ) $this->set( 'm_fltApAchMonthlyVolumeLimit', trim( $arrValues['ap_ach_monthly_volume_limit'] ) ); elseif( isset( $arrValues['ap_ach_monthly_volume_limit'] ) ) $this->setApAchMonthlyVolumeLimit( $arrValues['ap_ach_monthly_volume_limit'] );
		if( isset( $arrValues['ap_ach_monthly_running_total'] ) && $boolDirectSet ) $this->set( 'm_fltApAchMonthlyRunningTotal', trim( $arrValues['ap_ach_monthly_running_total'] ) ); elseif( isset( $arrValues['ap_ach_monthly_running_total'] ) ) $this->setApAchMonthlyRunningTotal( $arrValues['ap_ach_monthly_running_total'] );
		if( isset( $arrValues['ap_ach_max_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltApAchMaxPaymentAmount', trim( $arrValues['ap_ach_max_payment_amount'] ) ); elseif( isset( $arrValues['ap_ach_max_payment_amount'] ) ) $this->setApAchMaxPaymentAmount( $arrValues['ap_ach_max_payment_amount'] );
		if( isset( $arrValues['ap_ach_balance_check_amount'] ) && $boolDirectSet ) $this->set( 'm_fltApAchBalanceCheckAmount', trim( $arrValues['ap_ach_balance_check_amount'] ) ); elseif( isset( $arrValues['ap_ach_balance_check_amount'] ) ) $this->setApAchBalanceCheckAmount( $arrValues['ap_ach_balance_check_amount'] );
		if( isset( $arrValues['ap_ach_balance_check_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApAchBalanceCheckFee', trim( $arrValues['ap_ach_balance_check_fee'] ) ); elseif( isset( $arrValues['ap_ach_balance_check_fee'] ) ) $this->setApAchBalanceCheckFee( $arrValues['ap_ach_balance_check_fee'] );
		if( isset( $arrValues['ap_ach_account_verification_fee'] ) && $boolDirectSet ) $this->set( 'm_fltApAchAccountVerificationFee', trim( $arrValues['ap_ach_account_verification_fee'] ) ); elseif( isset( $arrValues['ap_ach_account_verification_fee'] ) ) $this->setApAchAccountVerificationFee( $arrValues['ap_ach_account_verification_fee'] );
		if( isset( $arrValues['ap_print_service_level_agreement'] ) && $boolDirectSet ) $this->set( 'm_intApPrintServiceLevelAgreement', trim( $arrValues['ap_print_service_level_agreement'] ) ); elseif( isset( $arrValues['ap_print_service_level_agreement'] ) ) $this->setApPrintServiceLevelAgreement( $arrValues['ap_print_service_level_agreement'] );
		if( isset( $arrValues['num_ach_return_retries'] ) && $boolDirectSet ) $this->set( 'm_intNumAchReturnRetries', trim( $arrValues['num_ach_return_retries'] ) ); elseif( isset( $arrValues['num_ach_return_retries'] ) ) $this->setNumAchReturnRetries( $arrValues['num_ach_return_retries'] );
		if( isset( $arrValues['allow_international_cards'] ) && $boolDirectSet ) $this->set( 'm_intAllowInternationalCards', trim( $arrValues['allow_international_cards'] ) ); elseif( isset( $arrValues['allow_international_cards'] ) ) $this->setAllowInternationalCards( $arrValues['allow_international_cards'] );
		if( isset( $arrValues['bill_international_cards'] ) && $boolDirectSet ) $this->set( 'm_intBillInternationalCards', trim( $arrValues['bill_international_cards'] ) ); elseif( isset( $arrValues['bill_international_cards'] ) ) $this->setBillInternationalCards( $arrValues['bill_international_cards'] );
		if( isset( $arrValues['require_separate_settlement'] ) && $boolDirectSet ) $this->set( 'm_intRequireSeparateSettlement', trim( $arrValues['require_separate_settlement'] ) ); elseif( isset( $arrValues['require_separate_settlement'] ) ) $this->setRequireSeparateSettlement( $arrValues['require_separate_settlement'] );
		if( isset( $arrValues['convert_check21_to_ach'] ) && $boolDirectSet ) $this->set( 'm_intConvertCheck21ToAch', trim( $arrValues['convert_check21_to_ach'] ) ); elseif( isset( $arrValues['convert_check21_to_ach'] ) ) $this->setConvertCheck21ToAch( $arrValues['convert_check21_to_ach'] );
		if( isset( $arrValues['is_aggregated'] ) && $boolDirectSet ) $this->set( 'm_intIsAggregated', trim( $arrValues['is_aggregated'] ) ); elseif( isset( $arrValues['is_aggregated'] ) ) $this->setIsAggregated( $arrValues['is_aggregated'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_company_charge_incentives_activated'] ) && $boolDirectSet ) $this->set( 'm_intIsCompanyChargeIncentivesActivated', trim( $arrValues['is_company_charge_incentives_activated'] ) ); elseif( isset( $arrValues['is_company_charge_incentives_activated'] ) ) $this->setIsCompanyChargeIncentivesActivated( $arrValues['is_company_charge_incentives_activated'] );
		if( isset( $arrValues['tax_verified_on'] ) && $boolDirectSet ) $this->set( 'm_strTaxVerifiedOn', trim( $arrValues['tax_verified_on'] ) ); elseif( isset( $arrValues['tax_verified_on'] ) ) $this->setTaxVerifiedOn( $arrValues['tax_verified_on'] );
		if( isset( $arrValues['underwriting_request_date'] ) && $boolDirectSet ) $this->set( 'm_strUnderwritingRequestDate', trim( $arrValues['underwriting_request_date'] ) ); elseif( isset( $arrValues['underwriting_request_date'] ) ) $this->setUnderwritingRequestDate( $arrValues['underwriting_request_date'] );
		if( isset( $arrValues['underwriting_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strUnderwritingSentOn', trim( $arrValues['underwriting_sent_on'] ) ); elseif( isset( $arrValues['underwriting_sent_on'] ) ) $this->setUnderwritingSentOn( $arrValues['underwriting_sent_on'] );
		if( isset( $arrValues['underwritten_on'] ) && $boolDirectSet ) $this->set( 'm_strUnderwrittenOn', trim( $arrValues['underwritten_on'] ) ); elseif( isset( $arrValues['underwritten_on'] ) ) $this->setUnderwrittenOn( $arrValues['underwritten_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setFundMovementTypeId( $intFundMovementTypeId ) {
		$this->set( 'm_intFundMovementTypeId', CStrings::strToIntDef( $intFundMovementTypeId, NULL, false ) );
	}

	public function getFundMovementTypeId() {
		return $this->m_intFundMovementTypeId;
	}

	public function sqlFundMovementTypeId() {
		return ( true == isset( $this->m_intFundMovementTypeId ) ) ? ( string ) $this->m_intFundMovementTypeId : 'NULL';
	}

	public function setCommercialMerchantAccountId( $intCommercialMerchantAccountId ) {
		$this->set( 'm_intCommercialMerchantAccountId', CStrings::strToIntDef( $intCommercialMerchantAccountId, NULL, false ) );
	}

	public function getCommercialMerchantAccountId() {
		return $this->m_intCommercialMerchantAccountId;
	}

	public function sqlCommercialMerchantAccountId() {
		return ( true == isset( $this->m_intCommercialMerchantAccountId ) ) ? ( string ) $this->m_intCommercialMerchantAccountId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : '\'US\'';
	}

	public function setDistributionAccountId( $intDistributionAccountId ) {
		$this->set( 'm_intDistributionAccountId', CStrings::strToIntDef( $intDistributionAccountId, NULL, false ) );
	}

	public function getDistributionAccountId() {
		return $this->m_intDistributionAccountId;
	}

	public function sqlDistributionAccountId() {
		return ( true == isset( $this->m_intDistributionAccountId ) ) ? ( string ) $this->m_intDistributionAccountId : 'NULL';
	}

	public function setIntermediaryAccountId( $intIntermediaryAccountId ) {
		$this->set( 'm_intIntermediaryAccountId', CStrings::strToIntDef( $intIntermediaryAccountId, NULL, false ) );
	}

	public function getIntermediaryAccountId() {
		return $this->m_intIntermediaryAccountId;
	}

	public function sqlIntermediaryAccountId() {
		return ( true == isset( $this->m_intIntermediaryAccountId ) ) ? ( string ) $this->m_intIntermediaryAccountId : 'NULL';
	}

	public function setBillingAccountId( $intBillingAccountId ) {
		$this->set( 'm_intBillingAccountId', CStrings::strToIntDef( $intBillingAccountId, NULL, false ) );
	}

	public function getBillingAccountId() {
		return $this->m_intBillingAccountId;
	}

	public function sqlBillingAccountId() {
		return ( true == isset( $this->m_intBillingAccountId ) ) ? ( string ) $this->m_intBillingAccountId : 'NULL';
	}

	public function setReturnsAccountId( $intReturnsAccountId ) {
		$this->set( 'm_intReturnsAccountId', CStrings::strToIntDef( $intReturnsAccountId, NULL, false ) );
	}

	public function getReturnsAccountId() {
		return $this->m_intReturnsAccountId;
	}

	public function sqlReturnsAccountId() {
		return ( true == isset( $this->m_intReturnsAccountId ) ) ? ( string ) $this->m_intReturnsAccountId : 'NULL';
	}

	public function setClearingAccountId( $intClearingAccountId ) {
		$this->set( 'm_intClearingAccountId', CStrings::strToIntDef( $intClearingAccountId, NULL, false ) );
	}

	public function getClearingAccountId() {
		return $this->m_intClearingAccountId;
	}

	public function sqlClearingAccountId() {
		return ( true == isset( $this->m_intClearingAccountId ) ) ? ( string ) $this->m_intClearingAccountId : 'NULL';
	}

	public function setDonationAccountId( $intDonationAccountId ) {
		$this->set( 'm_intDonationAccountId', CStrings::strToIntDef( $intDonationAccountId, NULL, false ) );
	}

	public function getDonationAccountId() {
		return $this->m_intDonationAccountId;
	}

	public function sqlDonationAccountId() {
		return ( true == isset( $this->m_intDonationAccountId ) ) ? ( string ) $this->m_intDonationAccountId : 'NULL';
	}

	public function setRebateAccountId( $intRebateAccountId ) {
		$this->set( 'm_intRebateAccountId', CStrings::strToIntDef( $intRebateAccountId, NULL, false ) );
	}

	public function getRebateAccountId() {
		return $this->m_intRebateAccountId;
	}

	public function sqlRebateAccountId() {
		return ( true == isset( $this->m_intRebateAccountId ) ) ? ( string ) $this->m_intRebateAccountId : 'NULL';
	}

	public function setMerchantProcessingTypeId( $intMerchantProcessingTypeId ) {
		$this->set( 'm_intMerchantProcessingTypeId', CStrings::strToIntDef( $intMerchantProcessingTypeId, NULL, false ) );
	}

	public function getMerchantProcessingTypeId() {
		return $this->m_intMerchantProcessingTypeId;
	}

	public function sqlMerchantProcessingTypeId() {
		return ( true == isset( $this->m_intMerchantProcessingTypeId ) ) ? ( string ) $this->m_intMerchantProcessingTypeId : '1';
	}

	public function setDistributionGroupTypeId( $intDistributionGroupTypeId ) {
		$this->set( 'm_intDistributionGroupTypeId', CStrings::strToIntDef( $intDistributionGroupTypeId, NULL, false ) );
	}

	public function getDistributionGroupTypeId() {
		return $this->m_intDistributionGroupTypeId;
	}

	public function sqlDistributionGroupTypeId() {
		return ( true == isset( $this->m_intDistributionGroupTypeId ) ) ? ( string ) $this->m_intDistributionGroupTypeId : 'NULL';
	}

	public function setAllocationTypeId( $intAllocationTypeId ) {
		$this->set( 'm_intAllocationTypeId', CStrings::strToIntDef( $intAllocationTypeId, NULL, false ) );
	}

	public function getAllocationTypeId() {
		return $this->m_intAllocationTypeId;
	}

	public function sqlAllocationTypeId() {
		return ( true == isset( $this->m_intAllocationTypeId ) ) ? ( string ) $this->m_intAllocationTypeId : '1';
	}

	public function setAccountName( $strAccountName ) {
		$this->set( 'm_strAccountName', CStrings::strTrimDef( $strAccountName, 100, NULL, true ) );
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function sqlAccountName() {
		return ( true == isset( $this->m_strAccountName ) ) ? '\'' . addslashes( $this->m_strAccountName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNotificationEmail( $strNotificationEmail ) {
		$this->set( 'm_strNotificationEmail', CStrings::strTrimDef( $strNotificationEmail, 240, NULL, true ) );
	}

	public function getNotificationEmail() {
		return $this->m_strNotificationEmail;
	}

	public function sqlNotificationEmail() {
		return ( true == isset( $this->m_strNotificationEmail ) ) ? '\'' . addslashes( $this->m_strNotificationEmail ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setMerchantName( $strMerchantName ) {
		$this->set( 'm_strMerchantName', CStrings::strTrimDef( $strMerchantName, 100, NULL, true ) );
	}

	public function getMerchantName() {
		return $this->m_strMerchantName;
	}

	public function sqlMerchantName() {
		return ( true == isset( $this->m_strMerchantName ) ) ? '\'' . addslashes( $this->m_strMerchantName ) . '\'' : 'NULL';
	}

	public function setTransactionDescription( $strTransactionDescription ) {
		$this->set( 'm_strTransactionDescription', CStrings::strTrimDef( $strTransactionDescription, 240, NULL, true ) );
	}

	public function getTransactionDescription() {
		return $this->m_strTransactionDescription;
	}

	public function sqlTransactionDescription() {
		return ( true == isset( $this->m_strTransactionDescription ) ) ? '\'' . addslashes( $this->m_strTransactionDescription ) . '\'' : 'NULL';
	}

	public function setPortalInputLabel( $strPortalInputLabel ) {
		$this->set( 'm_strPortalInputLabel', CStrings::strTrimDef( $strPortalInputLabel, 240, NULL, true ) );
	}

	public function getPortalInputLabel() {
		return $this->m_strPortalInputLabel;
	}

	public function sqlPortalInputLabel() {
		return ( true == isset( $this->m_strPortalInputLabel ) ) ? '\'' . addslashes( $this->m_strPortalInputLabel ) . '\'' : 'NULL';
	}

	public function setTaxLegalName( $strTaxLegalName ) {
		$this->set( 'm_strTaxLegalName', CStrings::strTrimDef( $strTaxLegalName, 100, NULL, true ) );
	}

	public function getTaxLegalName() {
		return $this->m_strTaxLegalName;
	}

	public function sqlTaxLegalName() {
		return ( true == isset( $this->m_strTaxLegalName ) ) ? '\'' . addslashes( $this->m_strTaxLegalName ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setExternalLegalEntityId( $strExternalLegalEntityId ) {
		$this->set( 'm_strExternalLegalEntityId', CStrings::strTrimDef( $strExternalLegalEntityId, 100, NULL, true ) );
	}

	public function getExternalLegalEntityId() {
		return $this->m_strExternalLegalEntityId;
	}

	public function sqlExternalLegalEntityId() {
		return ( true == isset( $this->m_strExternalLegalEntityId ) ) ? '\'' . addslashes( $this->m_strExternalLegalEntityId ) . '\'' : 'NULL';
	}

	public function setDistributionFee( $fltDistributionFee ) {
		$this->set( 'm_fltDistributionFee', CStrings::strToFloatDef( $fltDistributionFee, NULL, false, 2 ) );
	}

	public function getDistributionFee() {
		return $this->m_fltDistributionFee;
	}

	public function sqlDistributionFee() {
		return ( true == isset( $this->m_fltDistributionFee ) ) ? ( string ) $this->m_fltDistributionFee : '0';
	}

	public function setDistributionFailureFee( $fltDistributionFailureFee ) {
		$this->set( 'm_fltDistributionFailureFee', CStrings::strToFloatDef( $fltDistributionFailureFee, NULL, false, 4 ) );
	}

	public function getDistributionFailureFee() {
		return $this->m_fltDistributionFailureFee;
	}

	public function sqlDistributionFailureFee() {
		return ( true == isset( $this->m_fltDistributionFailureFee ) ) ? ( string ) $this->m_fltDistributionFailureFee : '0';
	}

	public function setFixedConvenienceFee( $fltFixedConvenienceFee ) {
		$this->set( 'm_fltFixedConvenienceFee', CStrings::strToFloatDef( $fltFixedConvenienceFee, NULL, false, 4 ) );
	}

	public function getFixedConvenienceFee() {
		return $this->m_fltFixedConvenienceFee;
	}

	public function sqlFixedConvenienceFee() {
		return ( true == isset( $this->m_fltFixedConvenienceFee ) ) ? ( string ) $this->m_fltFixedConvenienceFee : 'NULL';
	}

	public function setAcquireProcessingFee( $fltAcquireProcessingFee ) {
		$this->set( 'm_fltAcquireProcessingFee', CStrings::strToFloatDef( $fltAcquireProcessingFee, NULL, false, 2 ) );
	}

	public function getAcquireProcessingFee() {
		return $this->m_fltAcquireProcessingFee;
	}

	public function sqlAcquireProcessingFee() {
		return ( true == isset( $this->m_fltAcquireProcessingFee ) ) ? ( string ) $this->m_fltAcquireProcessingFee : '0';
	}

	public function setTargetDiscountRate( $fltTargetDiscountRate ) {
		$this->set( 'm_fltTargetDiscountRate', CStrings::strToFloatDef( $fltTargetDiscountRate, NULL, false, 6 ) );
	}

	public function getTargetDiscountRate() {
		return $this->m_fltTargetDiscountRate;
	}

	public function sqlTargetDiscountRate() {
		return ( true == isset( $this->m_fltTargetDiscountRate ) ) ? ( string ) $this->m_fltTargetDiscountRate : 'NULL';
	}

	public function setDisableDistributionUntil( $strDisableDistributionUntil ) {
		$this->set( 'm_strDisableDistributionUntil', CStrings::strTrimDef( $strDisableDistributionUntil, -1, NULL, true ) );
	}

	public function getDisableDistributionUntil() {
		return $this->m_strDisableDistributionUntil;
	}

	public function sqlDisableDistributionUntil() {
		return ( true == isset( $this->m_strDisableDistributionUntil ) ) ? '\'' . $this->m_strDisableDistributionUntil . '\'' : 'NULL';
	}

	public function setEnableFeeRestructuring( $intEnableFeeRestructuring ) {
		$this->set( 'm_intEnableFeeRestructuring', CStrings::strToIntDef( $intEnableFeeRestructuring, NULL, false ) );
	}

	public function getEnableFeeRestructuring() {
		return $this->m_intEnableFeeRestructuring;
	}

	public function sqlEnableFeeRestructuring() {
		return ( true == isset( $this->m_intEnableFeeRestructuring ) ) ? ( string ) $this->m_intEnableFeeRestructuring : '0';
	}

	public function setEnablePinlessDebitProcessing( $intEnablePinlessDebitProcessing ) {
		$this->set( 'm_intEnablePinlessDebitProcessing', CStrings::strToIntDef( $intEnablePinlessDebitProcessing, NULL, false ) );
	}

	public function getEnablePinlessDebitProcessing() {
		return $this->m_intEnablePinlessDebitProcessing;
	}

	public function sqlEnablePinlessDebitProcessing() {
		return ( true == isset( $this->m_intEnablePinlessDebitProcessing ) ) ? ( string ) $this->m_intEnablePinlessDebitProcessing : '0';
	}

	public function setEnableSeparateDebitPricing( $intEnableSeparateDebitPricing ) {
		$this->set( 'm_intEnableSeparateDebitPricing', CStrings::strToIntDef( $intEnableSeparateDebitPricing, NULL, false ) );
	}

	public function getEnableSeparateDebitPricing() {
		return $this->m_intEnableSeparateDebitPricing;
	}

	public function sqlEnableSeparateDebitPricing() {
		return ( true == isset( $this->m_intEnableSeparateDebitPricing ) ) ? ( string ) $this->m_intEnableSeparateDebitPricing : '0';
	}

	public function setEnableVisaPilot( $boolEnableVisaPilot ) {
		$this->set( 'm_boolEnableVisaPilot', CStrings::strToBool( $boolEnableVisaPilot ) );
	}

	public function getEnableVisaPilot() {
		return $this->m_boolEnableVisaPilot;
	}

	public function sqlEnableVisaPilot() {
		return ( true == isset( $this->m_boolEnableVisaPilot ) ) ? '\'' . ( true == ( bool ) $this->m_boolEnableVisaPilot ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAccountVerificationType( $intAccountVerificationType ) {
		$this->set( 'm_intAccountVerificationType', CStrings::strToIntDef( $intAccountVerificationType, NULL, false ) );
	}

	public function getAccountVerificationType() {
		return $this->m_intAccountVerificationType;
	}

	public function sqlAccountVerificationType() {
		return ( true == isset( $this->m_intAccountVerificationType ) ) ? ( string ) $this->m_intAccountVerificationType : '0';
	}

	public function setRequireCvv( $boolRequireCvv ) {
		$this->set( 'm_boolRequireCvv', CStrings::strToBool( $boolRequireCvv ) );
	}

	public function getRequireCvv() {
		return $this->m_boolRequireCvv;
	}

	public function sqlRequireCvv() {
		return ( true == isset( $this->m_boolRequireCvv ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireCvv ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApPaymentFee( $fltApPaymentFee ) {
		$this->set( 'm_fltApPaymentFee', CStrings::strToFloatDef( $fltApPaymentFee, NULL, false, 2 ) );
	}

	public function getApPaymentFee() {
		return $this->m_fltApPaymentFee;
	}

	public function sqlApPaymentFee() {
		return ( true == isset( $this->m_fltApPaymentFee ) ) ? ( string ) $this->m_fltApPaymentFee : '0';
	}

	public function setApPaymentFailureFee( $fltApPaymentFailureFee ) {
		$this->set( 'm_fltApPaymentFailureFee', CStrings::strToFloatDef( $fltApPaymentFailureFee, NULL, false, 2 ) );
	}

	public function getApPaymentFailureFee() {
		return $this->m_fltApPaymentFailureFee;
	}

	public function sqlApPaymentFailureFee() {
		return ( true == isset( $this->m_fltApPaymentFailureFee ) ) ? ( string ) $this->m_fltApPaymentFailureFee : '0';
	}

	public function setApPaymentReversalFee( $fltApPaymentReversalFee ) {
		$this->set( 'm_fltApPaymentReversalFee', CStrings::strToFloatDef( $fltApPaymentReversalFee, NULL, false, 2 ) );
	}

	public function getApPaymentReversalFee() {
		return $this->m_fltApPaymentReversalFee;
	}

	public function sqlApPaymentReversalFee() {
		return ( true == isset( $this->m_fltApPaymentReversalFee ) ) ? ( string ) $this->m_fltApPaymentReversalFee : 'NULL';
	}

	public function setApCheckFee( $fltApCheckFee ) {
		$this->set( 'm_fltApCheckFee', CStrings::strToFloatDef( $fltApCheckFee, NULL, false, 2 ) );
	}

	public function getApCheckFee() {
		return $this->m_fltApCheckFee;
	}

	public function sqlApCheckFee() {
		return ( true == isset( $this->m_fltApCheckFee ) ) ? ( string ) $this->m_fltApCheckFee : '0';
	}

	public function setApMinimumDelayDays( $intApMinimumDelayDays ) {
		$this->set( 'm_intApMinimumDelayDays', CStrings::strToIntDef( $intApMinimumDelayDays, NULL, false ) );
	}

	public function getApMinimumDelayDays() {
		return $this->m_intApMinimumDelayDays;
	}

	public function sqlApMinimumDelayDays() {
		return ( true == isset( $this->m_intApMinimumDelayDays ) ) ? ( string ) $this->m_intApMinimumDelayDays : '2';
	}

	public function setApAchIsDisabled( $boolApAchIsDisabled ) {
		$this->set( 'm_boolApAchIsDisabled', CStrings::strToBool( $boolApAchIsDisabled ) );
	}

	public function getApAchIsDisabled() {
		return $this->m_boolApAchIsDisabled;
	}

	public function sqlApAchIsDisabled() {
		return ( true == isset( $this->m_boolApAchIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolApAchIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApAchMonthlyVolumeLimit( $fltApAchMonthlyVolumeLimit ) {
		$this->set( 'm_fltApAchMonthlyVolumeLimit', CStrings::strToFloatDef( $fltApAchMonthlyVolumeLimit, NULL, false, 2 ) );
	}

	public function getApAchMonthlyVolumeLimit() {
		return $this->m_fltApAchMonthlyVolumeLimit;
	}

	public function sqlApAchMonthlyVolumeLimit() {
		return ( true == isset( $this->m_fltApAchMonthlyVolumeLimit ) ) ? ( string ) $this->m_fltApAchMonthlyVolumeLimit : '100000';
	}

	public function setApAchMonthlyRunningTotal( $fltApAchMonthlyRunningTotal ) {
		$this->set( 'm_fltApAchMonthlyRunningTotal', CStrings::strToFloatDef( $fltApAchMonthlyRunningTotal, NULL, false, 2 ) );
	}

	public function getApAchMonthlyRunningTotal() {
		return $this->m_fltApAchMonthlyRunningTotal;
	}

	public function sqlApAchMonthlyRunningTotal() {
		return ( true == isset( $this->m_fltApAchMonthlyRunningTotal ) ) ? ( string ) $this->m_fltApAchMonthlyRunningTotal : '0';
	}

	public function setApAchMaxPaymentAmount( $fltApAchMaxPaymentAmount ) {
		$this->set( 'm_fltApAchMaxPaymentAmount', CStrings::strToFloatDef( $fltApAchMaxPaymentAmount, NULL, false, 2 ) );
	}

	public function getApAchMaxPaymentAmount() {
		return $this->m_fltApAchMaxPaymentAmount;
	}

	public function sqlApAchMaxPaymentAmount() {
		return ( true == isset( $this->m_fltApAchMaxPaymentAmount ) ) ? ( string ) $this->m_fltApAchMaxPaymentAmount : '0';
	}

	public function setApAchBalanceCheckAmount( $fltApAchBalanceCheckAmount ) {
		$this->set( 'm_fltApAchBalanceCheckAmount', CStrings::strToFloatDef( $fltApAchBalanceCheckAmount, NULL, false, 2 ) );
	}

	public function getApAchBalanceCheckAmount() {
		return $this->m_fltApAchBalanceCheckAmount;
	}

	public function sqlApAchBalanceCheckAmount() {
		return ( true == isset( $this->m_fltApAchBalanceCheckAmount ) ) ? ( string ) $this->m_fltApAchBalanceCheckAmount : '0';
	}

	public function setApAchBalanceCheckFee( $fltApAchBalanceCheckFee ) {
		$this->set( 'm_fltApAchBalanceCheckFee', CStrings::strToFloatDef( $fltApAchBalanceCheckFee, NULL, false, 2 ) );
	}

	public function getApAchBalanceCheckFee() {
		return $this->m_fltApAchBalanceCheckFee;
	}

	public function sqlApAchBalanceCheckFee() {
		return ( true == isset( $this->m_fltApAchBalanceCheckFee ) ) ? ( string ) $this->m_fltApAchBalanceCheckFee : '0';
	}

	public function setApAchAccountVerificationFee( $fltApAchAccountVerificationFee ) {
		$this->set( 'm_fltApAchAccountVerificationFee', CStrings::strToFloatDef( $fltApAchAccountVerificationFee, NULL, false, 2 ) );
	}

	public function getApAchAccountVerificationFee() {
		return $this->m_fltApAchAccountVerificationFee;
	}

	public function sqlApAchAccountVerificationFee() {
		return ( true == isset( $this->m_fltApAchAccountVerificationFee ) ) ? ( string ) $this->m_fltApAchAccountVerificationFee : '0';
	}

	public function setApPrintServiceLevelAgreement( $intApPrintServiceLevelAgreement ) {
		$this->set( 'm_intApPrintServiceLevelAgreement', CStrings::strToIntDef( $intApPrintServiceLevelAgreement, NULL, false ) );
	}

	public function getApPrintServiceLevelAgreement() {
		return $this->m_intApPrintServiceLevelAgreement;
	}

	public function sqlApPrintServiceLevelAgreement() {
		return ( true == isset( $this->m_intApPrintServiceLevelAgreement ) ) ? ( string ) $this->m_intApPrintServiceLevelAgreement : '1';
	}

	public function setNumAchReturnRetries( $intNumAchReturnRetries ) {
		$this->set( 'm_intNumAchReturnRetries', CStrings::strToIntDef( $intNumAchReturnRetries, NULL, false ) );
	}

	public function getNumAchReturnRetries() {
		return $this->m_intNumAchReturnRetries;
	}

	public function sqlNumAchReturnRetries() {
		return ( true == isset( $this->m_intNumAchReturnRetries ) ) ? ( string ) $this->m_intNumAchReturnRetries : '0';
	}

	public function setAllowInternationalCards( $intAllowInternationalCards ) {
		$this->set( 'm_intAllowInternationalCards', CStrings::strToIntDef( $intAllowInternationalCards, NULL, false ) );
	}

	public function getAllowInternationalCards() {
		return $this->m_intAllowInternationalCards;
	}

	public function sqlAllowInternationalCards() {
		return ( true == isset( $this->m_intAllowInternationalCards ) ) ? ( string ) $this->m_intAllowInternationalCards : '0';
	}

	public function setBillInternationalCards( $intBillInternationalCards ) {
		$this->set( 'm_intBillInternationalCards', CStrings::strToIntDef( $intBillInternationalCards, NULL, false ) );
	}

	public function getBillInternationalCards() {
		return $this->m_intBillInternationalCards;
	}

	public function sqlBillInternationalCards() {
		return ( true == isset( $this->m_intBillInternationalCards ) ) ? ( string ) $this->m_intBillInternationalCards : '0';
	}

	public function setRequireSeparateSettlement( $intRequireSeparateSettlement ) {
		$this->set( 'm_intRequireSeparateSettlement', CStrings::strToIntDef( $intRequireSeparateSettlement, NULL, false ) );
	}

	public function getRequireSeparateSettlement() {
		return $this->m_intRequireSeparateSettlement;
	}

	public function sqlRequireSeparateSettlement() {
		return ( true == isset( $this->m_intRequireSeparateSettlement ) ) ? ( string ) $this->m_intRequireSeparateSettlement : '0';
	}

	public function setConvertCheck21ToAch( $intConvertCheck21ToAch ) {
		$this->set( 'm_intConvertCheck21ToAch', CStrings::strToIntDef( $intConvertCheck21ToAch, NULL, false ) );
	}

	public function getConvertCheck21ToAch() {
		return $this->m_intConvertCheck21ToAch;
	}

	public function sqlConvertCheck21ToAch() {
		return ( true == isset( $this->m_intConvertCheck21ToAch ) ) ? ( string ) $this->m_intConvertCheck21ToAch : '0';
	}

	public function setIsAggregated( $intIsAggregated ) {
		$this->set( 'm_intIsAggregated', CStrings::strToIntDef( $intIsAggregated, NULL, false ) );
	}

	public function getIsAggregated() {
		return $this->m_intIsAggregated;
	}

	public function sqlIsAggregated() {
		return ( true == isset( $this->m_intIsAggregated ) ) ? ( string ) $this->m_intIsAggregated : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsCompanyChargeIncentivesActivated( $intIsCompanyChargeIncentivesActivated ) {
		$this->set( 'm_intIsCompanyChargeIncentivesActivated', CStrings::strToIntDef( $intIsCompanyChargeIncentivesActivated, NULL, false ) );
	}

	public function getIsCompanyChargeIncentivesActivated() {
		return $this->m_intIsCompanyChargeIncentivesActivated;
	}

	public function sqlIsCompanyChargeIncentivesActivated() {
		return ( true == isset( $this->m_intIsCompanyChargeIncentivesActivated ) ) ? ( string ) $this->m_intIsCompanyChargeIncentivesActivated : '0';
	}

	public function setTaxVerifiedOn( $strTaxVerifiedOn ) {
		$this->set( 'm_strTaxVerifiedOn', CStrings::strTrimDef( $strTaxVerifiedOn, -1, NULL, true ) );
	}

	public function getTaxVerifiedOn() {
		return $this->m_strTaxVerifiedOn;
	}

	public function sqlTaxVerifiedOn() {
		return ( true == isset( $this->m_strTaxVerifiedOn ) ) ? '\'' . $this->m_strTaxVerifiedOn . '\'' : 'NULL';
	}

	public function setUnderwritingRequestDate( $strUnderwritingRequestDate ) {
		$this->set( 'm_strUnderwritingRequestDate', CStrings::strTrimDef( $strUnderwritingRequestDate, -1, NULL, true ) );
	}

	public function getUnderwritingRequestDate() {
		return $this->m_strUnderwritingRequestDate;
	}

	public function sqlUnderwritingRequestDate() {
		return ( true == isset( $this->m_strUnderwritingRequestDate ) ) ? '\'' . $this->m_strUnderwritingRequestDate . '\'' : 'NULL';
	}

	public function setUnderwritingSentOn( $strUnderwritingSentOn ) {
		$this->set( 'm_strUnderwritingSentOn', CStrings::strTrimDef( $strUnderwritingSentOn, -1, NULL, true ) );
	}

	public function getUnderwritingSentOn() {
		return $this->m_strUnderwritingSentOn;
	}

	public function sqlUnderwritingSentOn() {
		return ( true == isset( $this->m_strUnderwritingSentOn ) ) ? '\'' . $this->m_strUnderwritingSentOn . '\'' : 'NULL';
	}

	public function setUnderwrittenOn( $strUnderwrittenOn ) {
		$this->set( 'm_strUnderwrittenOn', CStrings::strTrimDef( $strUnderwrittenOn, -1, NULL, true ) );
	}

	public function getUnderwrittenOn() {
		return $this->m_strUnderwrittenOn;
	}

	public function sqlUnderwrittenOn() {
		return ( true == isset( $this->m_strUnderwrittenOn ) ) ? '\'' . $this->m_strUnderwrittenOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, processing_bank_id, fund_movement_type_id, commercial_merchant_account_id, currency_code, country_code, distribution_account_id, intermediary_account_id, billing_account_id, returns_account_id, clearing_account_id, donation_account_id, rebate_account_id, merchant_processing_type_id, distribution_group_type_id, allocation_type_id, account_name, description, notification_email, notes, merchant_name, transaction_description, portal_input_label, tax_legal_name, tax_number_encrypted, external_legal_entity_id, distribution_fee, distribution_failure_fee, fixed_convenience_fee, acquire_processing_fee, target_discount_rate, disable_distribution_until, enable_fee_restructuring, enable_pinless_debit_processing, enable_separate_debit_pricing, enable_visa_pilot, account_verification_type, require_cvv, ap_payment_fee, ap_payment_failure_fee, ap_payment_reversal_fee, ap_check_fee, ap_minimum_delay_days, ap_ach_is_disabled, ap_ach_monthly_volume_limit, ap_ach_monthly_running_total, ap_ach_max_payment_amount, ap_ach_balance_check_amount, ap_ach_balance_check_fee, ap_ach_account_verification_fee, ap_print_service_level_agreement, num_ach_return_retries, allow_international_cards, bill_international_cards, require_separate_settlement, convert_check21_to_ach, is_aggregated, is_disabled, is_company_charge_incentives_activated, tax_verified_on, underwriting_request_date, underwriting_sent_on, underwritten_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlProcessingBankId() . ', ' .
			$this->sqlFundMovementTypeId() . ', ' .
			$this->sqlCommercialMerchantAccountId() . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlCountryCode() . ', ' .
			$this->sqlDistributionAccountId() . ', ' .
			$this->sqlIntermediaryAccountId() . ', ' .
			$this->sqlBillingAccountId() . ', ' .
			$this->sqlReturnsAccountId() . ', ' .
			$this->sqlClearingAccountId() . ', ' .
			$this->sqlDonationAccountId() . ', ' .
			$this->sqlRebateAccountId() . ', ' .
			$this->sqlMerchantProcessingTypeId() . ', ' .
			$this->sqlDistributionGroupTypeId() . ', ' .
			$this->sqlAllocationTypeId() . ', ' .
			$this->sqlAccountName() . ', ' .
			$this->sqlDescription() . ', ' .
			$this->sqlNotificationEmail() . ', ' .
			$this->sqlNotes() . ', ' .
			$this->sqlMerchantName() . ', ' .
			$this->sqlTransactionDescription() . ', ' .
			$this->sqlPortalInputLabel() . ', ' .
			$this->sqlTaxLegalName() . ', ' .
			$this->sqlTaxNumberEncrypted() . ', ' .
			$this->sqlExternalLegalEntityId() . ', ' .
			$this->sqlDistributionFee() . ', ' .
			$this->sqlDistributionFailureFee() . ', ' .
			$this->sqlFixedConvenienceFee() . ', ' .
			$this->sqlAcquireProcessingFee() . ', ' .
			$this->sqlTargetDiscountRate() . ', ' .
			$this->sqlDisableDistributionUntil() . ', ' .
			$this->sqlEnableFeeRestructuring() . ', ' .
			$this->sqlEnablePinlessDebitProcessing() . ', ' .
			$this->sqlEnableSeparateDebitPricing() . ', ' .
			$this->sqlEnableVisaPilot() . ', ' .
			$this->sqlAccountVerificationType() . ', ' .
			$this->sqlRequireCvv() . ', ' .
			$this->sqlApPaymentFee() . ', ' .
			$this->sqlApPaymentFailureFee() . ', ' .
			$this->sqlApPaymentReversalFee() . ', ' .
			$this->sqlApCheckFee() . ', ' .
			$this->sqlApMinimumDelayDays() . ', ' .
			$this->sqlApAchIsDisabled() . ', ' .
			$this->sqlApAchMonthlyVolumeLimit() . ', ' .
			$this->sqlApAchMonthlyRunningTotal() . ', ' .
			$this->sqlApAchMaxPaymentAmount() . ', ' .
			$this->sqlApAchBalanceCheckAmount() . ', ' .
			$this->sqlApAchBalanceCheckFee() . ', ' .
			$this->sqlApAchAccountVerificationFee() . ', ' .
			$this->sqlApPrintServiceLevelAgreement() . ', ' .
			$this->sqlNumAchReturnRetries() . ', ' .
			$this->sqlAllowInternationalCards() . ', ' .
			$this->sqlBillInternationalCards() . ', ' .
			$this->sqlRequireSeparateSettlement() . ', ' .
			$this->sqlConvertCheck21ToAch() . ', ' .
			$this->sqlIsAggregated() . ', ' .
			$this->sqlIsDisabled() . ', ' .
			$this->sqlIsCompanyChargeIncentivesActivated() . ', ' .
			$this->sqlTaxVerifiedOn() . ', ' .
			$this->sqlUnderwritingRequestDate() . ', ' .
			$this->sqlUnderwritingSentOn() . ', ' .
			$this->sqlUnderwrittenOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ', ' .
			$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fund_movement_type_id = ' . $this->sqlFundMovementTypeId() . ','; } elseif( true == array_key_exists( 'FundMovementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fund_movement_type_id = ' . $this->sqlFundMovementTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_merchant_account_id = ' . $this->sqlCommercialMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CommercialMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' commercial_merchant_account_id = ' . $this->sqlCommercialMerchantAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_account_id = ' . $this->sqlDistributionAccountId() . ','; } elseif( true == array_key_exists( 'DistributionAccountId', $this->getChangedColumns() ) ) { $strSql .= ' distribution_account_id = ' . $this->sqlDistributionAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' intermediary_account_id = ' . $this->sqlIntermediaryAccountId() . ','; } elseif( true == array_key_exists( 'IntermediaryAccountId', $this->getChangedColumns() ) ) { $strSql .= ' intermediary_account_id = ' . $this->sqlIntermediaryAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId() . ','; } elseif( true == array_key_exists( 'BillingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_account_id = ' . $this->sqlReturnsAccountId() . ','; } elseif( true == array_key_exists( 'ReturnsAccountId', $this->getChangedColumns() ) ) { $strSql .= ' returns_account_id = ' . $this->sqlReturnsAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_account_id = ' . $this->sqlClearingAccountId() . ','; } elseif( true == array_key_exists( 'ClearingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' clearing_account_id = ' . $this->sqlClearingAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' donation_account_id = ' . $this->sqlDonationAccountId() . ','; } elseif( true == array_key_exists( 'DonationAccountId', $this->getChangedColumns() ) ) { $strSql .= ' donation_account_id = ' . $this->sqlDonationAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rebate_account_id = ' . $this->sqlRebateAccountId() . ','; } elseif( true == array_key_exists( 'RebateAccountId', $this->getChangedColumns() ) ) { $strSql .= ' rebate_account_id = ' . $this->sqlRebateAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_processing_type_id = ' . $this->sqlMerchantProcessingTypeId() . ','; } elseif( true == array_key_exists( 'MerchantProcessingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_processing_type_id = ' . $this->sqlMerchantProcessingTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_group_type_id = ' . $this->sqlDistributionGroupTypeId() . ','; } elseif( true == array_key_exists( 'DistributionGroupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' distribution_group_type_id = ' . $this->sqlDistributionGroupTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_type_id = ' . $this->sqlAllocationTypeId() . ','; } elseif( true == array_key_exists( 'AllocationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' allocation_type_id = ' . $this->sqlAllocationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; } elseif( true == array_key_exists( 'AccountName', $this->getChangedColumns() ) ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_email = ' . $this->sqlNotificationEmail() . ','; } elseif( true == array_key_exists( 'NotificationEmail', $this->getChangedColumns() ) ) { $strSql .= ' notification_email = ' . $this->sqlNotificationEmail() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_name = ' . $this->sqlMerchantName() . ','; } elseif( true == array_key_exists( 'MerchantName', $this->getChangedColumns() ) ) { $strSql .= ' merchant_name = ' . $this->sqlMerchantName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_description = ' . $this->sqlTransactionDescription() . ','; } elseif( true == array_key_exists( 'TransactionDescription', $this->getChangedColumns() ) ) { $strSql .= ' transaction_description = ' . $this->sqlTransactionDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' portal_input_label = ' . $this->sqlPortalInputLabel() . ','; } elseif( true == array_key_exists( 'PortalInputLabel', $this->getChangedColumns() ) ) { $strSql .= ' portal_input_label = ' . $this->sqlPortalInputLabel() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_legal_name = ' . $this->sqlTaxLegalName() . ','; } elseif( true == array_key_exists( 'TaxLegalName', $this->getChangedColumns() ) ) { $strSql .= ' tax_legal_name = ' . $this->sqlTaxLegalName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_legal_entity_id = ' . $this->sqlExternalLegalEntityId() . ','; } elseif( true == array_key_exists( 'ExternalLegalEntityId', $this->getChangedColumns() ) ) { $strSql .= ' external_legal_entity_id = ' . $this->sqlExternalLegalEntityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_fee = ' . $this->sqlDistributionFee() . ','; } elseif( true == array_key_exists( 'DistributionFee', $this->getChangedColumns() ) ) { $strSql .= ' distribution_fee = ' . $this->sqlDistributionFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_failure_fee = ' . $this->sqlDistributionFailureFee() . ','; } elseif( true == array_key_exists( 'DistributionFailureFee', $this->getChangedColumns() ) ) { $strSql .= ' distribution_failure_fee = ' . $this->sqlDistributionFailureFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_convenience_fee = ' . $this->sqlFixedConvenienceFee() . ','; } elseif( true == array_key_exists( 'FixedConvenienceFee', $this->getChangedColumns() ) ) { $strSql .= ' fixed_convenience_fee = ' . $this->sqlFixedConvenienceFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acquire_processing_fee = ' . $this->sqlAcquireProcessingFee() . ','; } elseif( true == array_key_exists( 'AcquireProcessingFee', $this->getChangedColumns() ) ) { $strSql .= ' acquire_processing_fee = ' . $this->sqlAcquireProcessingFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target_discount_rate = ' . $this->sqlTargetDiscountRate() . ','; } elseif( true == array_key_exists( 'TargetDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' target_discount_rate = ' . $this->sqlTargetDiscountRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disable_distribution_until = ' . $this->sqlDisableDistributionUntil() . ','; } elseif( true == array_key_exists( 'DisableDistributionUntil', $this->getChangedColumns() ) ) { $strSql .= ' disable_distribution_until = ' . $this->sqlDisableDistributionUntil() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_fee_restructuring = ' . $this->sqlEnableFeeRestructuring() . ','; } elseif( true == array_key_exists( 'EnableFeeRestructuring', $this->getChangedColumns() ) ) { $strSql .= ' enable_fee_restructuring = ' . $this->sqlEnableFeeRestructuring() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_pinless_debit_processing = ' . $this->sqlEnablePinlessDebitProcessing() . ','; } elseif( true == array_key_exists( 'EnablePinlessDebitProcessing', $this->getChangedColumns() ) ) { $strSql .= ' enable_pinless_debit_processing = ' . $this->sqlEnablePinlessDebitProcessing() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_separate_debit_pricing = ' . $this->sqlEnableSeparateDebitPricing() . ','; } elseif( true == array_key_exists( 'EnableSeparateDebitPricing', $this->getChangedColumns() ) ) { $strSql .= ' enable_separate_debit_pricing = ' . $this->sqlEnableSeparateDebitPricing() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_visa_pilot = ' . $this->sqlEnableVisaPilot() . ','; } elseif( true == array_key_exists( 'EnableVisaPilot', $this->getChangedColumns() ) ) { $strSql .= ' enable_visa_pilot = ' . $this->sqlEnableVisaPilot() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_verification_type = ' . $this->sqlAccountVerificationType() . ','; } elseif( true == array_key_exists( 'AccountVerificationType', $this->getChangedColumns() ) ) { $strSql .= ' account_verification_type = ' . $this->sqlAccountVerificationType() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_cvv = ' . $this->sqlRequireCvv() . ','; } elseif( true == array_key_exists( 'RequireCvv', $this->getChangedColumns() ) ) { $strSql .= ' require_cvv = ' . $this->sqlRequireCvv() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_fee = ' . $this->sqlApPaymentFee() . ','; } elseif( true == array_key_exists( 'ApPaymentFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_fee = ' . $this->sqlApPaymentFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_failure_fee = ' . $this->sqlApPaymentFailureFee() . ','; } elseif( true == array_key_exists( 'ApPaymentFailureFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_failure_fee = ' . $this->sqlApPaymentFailureFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_reversal_fee = ' . $this->sqlApPaymentReversalFee() . ','; } elseif( true == array_key_exists( 'ApPaymentReversalFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_reversal_fee = ' . $this->sqlApPaymentReversalFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_check_fee = ' . $this->sqlApCheckFee() . ','; } elseif( true == array_key_exists( 'ApCheckFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_check_fee = ' . $this->sqlApCheckFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_minimum_delay_days = ' . $this->sqlApMinimumDelayDays() . ','; } elseif( true == array_key_exists( 'ApMinimumDelayDays', $this->getChangedColumns() ) ) { $strSql .= ' ap_minimum_delay_days = ' . $this->sqlApMinimumDelayDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_is_disabled = ' . $this->sqlApAchIsDisabled() . ','; } elseif( true == array_key_exists( 'ApAchIsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_is_disabled = ' . $this->sqlApAchIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_monthly_volume_limit = ' . $this->sqlApAchMonthlyVolumeLimit() . ','; } elseif( true == array_key_exists( 'ApAchMonthlyVolumeLimit', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_monthly_volume_limit = ' . $this->sqlApAchMonthlyVolumeLimit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_monthly_running_total = ' . $this->sqlApAchMonthlyRunningTotal() . ','; } elseif( true == array_key_exists( 'ApAchMonthlyRunningTotal', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_monthly_running_total = ' . $this->sqlApAchMonthlyRunningTotal() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_max_payment_amount = ' . $this->sqlApAchMaxPaymentAmount() . ','; } elseif( true == array_key_exists( 'ApAchMaxPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_max_payment_amount = ' . $this->sqlApAchMaxPaymentAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_balance_check_amount = ' . $this->sqlApAchBalanceCheckAmount() . ','; } elseif( true == array_key_exists( 'ApAchBalanceCheckAmount', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_balance_check_amount = ' . $this->sqlApAchBalanceCheckAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_balance_check_fee = ' . $this->sqlApAchBalanceCheckFee() . ','; } elseif( true == array_key_exists( 'ApAchBalanceCheckFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_balance_check_fee = ' . $this->sqlApAchBalanceCheckFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_account_verification_fee = ' . $this->sqlApAchAccountVerificationFee() . ','; } elseif( true == array_key_exists( 'ApAchAccountVerificationFee', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_account_verification_fee = ' . $this->sqlApAchAccountVerificationFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_print_service_level_agreement = ' . $this->sqlApPrintServiceLevelAgreement() . ','; } elseif( true == array_key_exists( 'ApPrintServiceLevelAgreement', $this->getChangedColumns() ) ) { $strSql .= ' ap_print_service_level_agreement = ' . $this->sqlApPrintServiceLevelAgreement() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_ach_return_retries = ' . $this->sqlNumAchReturnRetries() . ','; } elseif( true == array_key_exists( 'NumAchReturnRetries', $this->getChangedColumns() ) ) { $strSql .= ' num_ach_return_retries = ' . $this->sqlNumAchReturnRetries() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_international_cards = ' . $this->sqlAllowInternationalCards() . ','; } elseif( true == array_key_exists( 'AllowInternationalCards', $this->getChangedColumns() ) ) { $strSql .= ' allow_international_cards = ' . $this->sqlAllowInternationalCards() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_international_cards = ' . $this->sqlBillInternationalCards() . ','; } elseif( true == array_key_exists( 'BillInternationalCards', $this->getChangedColumns() ) ) { $strSql .= ' bill_international_cards = ' . $this->sqlBillInternationalCards() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_separate_settlement = ' . $this->sqlRequireSeparateSettlement() . ','; } elseif( true == array_key_exists( 'RequireSeparateSettlement', $this->getChangedColumns() ) ) { $strSql .= ' require_separate_settlement = ' . $this->sqlRequireSeparateSettlement() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' convert_check21_to_ach = ' . $this->sqlConvertCheck21ToAch() . ','; } elseif( true == array_key_exists( 'ConvertCheck21ToAch', $this->getChangedColumns() ) ) { $strSql .= ' convert_check21_to_ach = ' . $this->sqlConvertCheck21ToAch() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_aggregated = ' . $this->sqlIsAggregated() . ','; } elseif( true == array_key_exists( 'IsAggregated', $this->getChangedColumns() ) ) { $strSql .= ' is_aggregated = ' . $this->sqlIsAggregated() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_company_charge_incentives_activated = ' . $this->sqlIsCompanyChargeIncentivesActivated() . ','; } elseif( true == array_key_exists( 'IsCompanyChargeIncentivesActivated', $this->getChangedColumns() ) ) { $strSql .= ' is_company_charge_incentives_activated = ' . $this->sqlIsCompanyChargeIncentivesActivated() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_verified_on = ' . $this->sqlTaxVerifiedOn() . ','; } elseif( true == array_key_exists( 'TaxVerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' tax_verified_on = ' . $this->sqlTaxVerifiedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' underwriting_request_date = ' . $this->sqlUnderwritingRequestDate() . ','; } elseif( true == array_key_exists( 'UnderwritingRequestDate', $this->getChangedColumns() ) ) { $strSql .= ' underwriting_request_date = ' . $this->sqlUnderwritingRequestDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' underwriting_sent_on = ' . $this->sqlUnderwritingSentOn() . ','; } elseif( true == array_key_exists( 'UnderwritingSentOn', $this->getChangedColumns() ) ) { $strSql .= ' underwriting_sent_on = ' . $this->sqlUnderwritingSentOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' underwritten_on = ' . $this->sqlUnderwrittenOn() . ','; } elseif( true == array_key_exists( 'UnderwrittenOn', $this->getChangedColumns() ) ) { $strSql .= ' underwritten_on = ' . $this->sqlUnderwrittenOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'fund_movement_type_id' => $this->getFundMovementTypeId(),
			'commercial_merchant_account_id' => $this->getCommercialMerchantAccountId(),
			'currency_code' => $this->getCurrencyCode(),
			'country_code' => $this->getCountryCode(),
			'distribution_account_id' => $this->getDistributionAccountId(),
			'intermediary_account_id' => $this->getIntermediaryAccountId(),
			'billing_account_id' => $this->getBillingAccountId(),
			'returns_account_id' => $this->getReturnsAccountId(),
			'clearing_account_id' => $this->getClearingAccountId(),
			'donation_account_id' => $this->getDonationAccountId(),
			'rebate_account_id' => $this->getRebateAccountId(),
			'merchant_processing_type_id' => $this->getMerchantProcessingTypeId(),
			'distribution_group_type_id' => $this->getDistributionGroupTypeId(),
			'allocation_type_id' => $this->getAllocationTypeId(),
			'account_name' => $this->getAccountName(),
			'description' => $this->getDescription(),
			'notification_email' => $this->getNotificationEmail(),
			'notes' => $this->getNotes(),
			'merchant_name' => $this->getMerchantName(),
			'transaction_description' => $this->getTransactionDescription(),
			'portal_input_label' => $this->getPortalInputLabel(),
			'tax_legal_name' => $this->getTaxLegalName(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'external_legal_entity_id' => $this->getExternalLegalEntityId(),
			'distribution_fee' => $this->getDistributionFee(),
			'distribution_failure_fee' => $this->getDistributionFailureFee(),
			'fixed_convenience_fee' => $this->getFixedConvenienceFee(),
			'acquire_processing_fee' => $this->getAcquireProcessingFee(),
			'target_discount_rate' => $this->getTargetDiscountRate(),
			'disable_distribution_until' => $this->getDisableDistributionUntil(),
			'enable_fee_restructuring' => $this->getEnableFeeRestructuring(),
			'enable_pinless_debit_processing' => $this->getEnablePinlessDebitProcessing(),
			'enable_separate_debit_pricing' => $this->getEnableSeparateDebitPricing(),
			'enable_visa_pilot' => $this->getEnableVisaPilot(),
			'account_verification_type' => $this->getAccountVerificationType(),
			'require_cvv' => $this->getRequireCvv(),
			'ap_payment_fee' => $this->getApPaymentFee(),
			'ap_payment_failure_fee' => $this->getApPaymentFailureFee(),
			'ap_payment_reversal_fee' => $this->getApPaymentReversalFee(),
			'ap_check_fee' => $this->getApCheckFee(),
			'ap_minimum_delay_days' => $this->getApMinimumDelayDays(),
			'ap_ach_is_disabled' => $this->getApAchIsDisabled(),
			'ap_ach_monthly_volume_limit' => $this->getApAchMonthlyVolumeLimit(),
			'ap_ach_monthly_running_total' => $this->getApAchMonthlyRunningTotal(),
			'ap_ach_max_payment_amount' => $this->getApAchMaxPaymentAmount(),
			'ap_ach_balance_check_amount' => $this->getApAchBalanceCheckAmount(),
			'ap_ach_balance_check_fee' => $this->getApAchBalanceCheckFee(),
			'ap_ach_account_verification_fee' => $this->getApAchAccountVerificationFee(),
			'ap_print_service_level_agreement' => $this->getApPrintServiceLevelAgreement(),
			'num_ach_return_retries' => $this->getNumAchReturnRetries(),
			'allow_international_cards' => $this->getAllowInternationalCards(),
			'bill_international_cards' => $this->getBillInternationalCards(),
			'require_separate_settlement' => $this->getRequireSeparateSettlement(),
			'convert_check21_to_ach' => $this->getConvertCheck21ToAch(),
			'is_aggregated' => $this->getIsAggregated(),
			'is_disabled' => $this->getIsDisabled(),
			'is_company_charge_incentives_activated' => $this->getIsCompanyChargeIncentivesActivated(),
			'tax_verified_on' => $this->getTaxVerifiedOn(),
			'underwriting_request_date' => $this->getUnderwritingRequestDate(),
			'underwriting_sent_on' => $this->getUnderwritingSentOn(),
			'underwritten_on' => $this->getUnderwrittenOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>