<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTemplateSlotImage extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.template_slot_images';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteTemplateSlotId;
	protected $m_intCompanyMediaFileId;
	protected $m_strLink;
	protected $m_strLinkText;
	protected $m_strAlt;
	protected $m_strTarget;
	protected $m_strCaption;
	protected $m_intIsPublished;
	protected $m_boolIsPreview;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_boolIsPreview = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_template_slot_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateSlotId', trim( $arrValues['website_template_slot_id'] ) ); elseif( isset( $arrValues['website_template_slot_id'] ) ) $this->setWebsiteTemplateSlotId( $arrValues['website_template_slot_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['link'] ) && $boolDirectSet ) $this->set( 'm_strLink', trim( stripcslashes( $arrValues['link'] ) ) ); elseif( isset( $arrValues['link'] ) ) $this->setLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link'] ) : $arrValues['link'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( stripcslashes( $arrValues['link_text'] ) ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_text'] ) : $arrValues['link_text'] );
		if( isset( $arrValues['alt'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAlt', trim( stripcslashes( $arrValues['alt'] ) ) ); elseif( isset( $arrValues['alt'] ) ) $this->setAlt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alt'] ) : $arrValues['alt'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( stripcslashes( $arrValues['target'] ) ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['target'] ) : $arrValues['target'] );
		if( isset( $arrValues['caption'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCaption', trim( stripcslashes( $arrValues['caption'] ) ) ); elseif( isset( $arrValues['caption'] ) ) $this->setCaption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caption'] ) : $arrValues['caption'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_preview'] ) && $boolDirectSet ) $this->set( 'm_boolIsPreview', trim( stripcslashes( $arrValues['is_preview'] ) ) ); elseif( isset( $arrValues['is_preview'] ) ) $this->setIsPreview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_preview'] ) : $arrValues['is_preview'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteTemplateSlotId( $intWebsiteTemplateSlotId ) {
		$this->set( 'm_intWebsiteTemplateSlotId', CStrings::strToIntDef( $intWebsiteTemplateSlotId, NULL, false ) );
	}

	public function getWebsiteTemplateSlotId() {
		return $this->m_intWebsiteTemplateSlotId;
	}

	public function sqlWebsiteTemplateSlotId() {
		return ( true == isset( $this->m_intWebsiteTemplateSlotId ) ) ? ( string ) $this->m_intWebsiteTemplateSlotId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setLink( $strLink ) {
		$this->set( 'm_strLink', CStrings::strTrimDef( $strLink, 4096, NULL, true ) );
	}

	public function getLink() {
		return $this->m_strLink;
	}

	public function sqlLink() {
		return ( true == isset( $this->m_strLink ) ) ? '\'' . addslashes( $this->m_strLink ) . '\'' : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, 240, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? '\'' . addslashes( $this->m_strLinkText ) . '\'' : 'NULL';
	}

	public function setAlt( $strAlt, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAlt', CStrings::strTrimDef( $strAlt, 240, NULL, true ), $strLocaleCode );
	}

	public function getAlt( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAlt', $strLocaleCode );
	}

	public function sqlAlt() {
		return ( true == isset( $this->m_strAlt ) ) ? '\'' . addslashes( $this->m_strAlt ) . '\'' : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, 240, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? '\'' . addslashes( $this->m_strTarget ) . '\'' : 'NULL';
	}

	public function setCaption( $strCaption, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCaption', CStrings::strTrimDef( $strCaption, 240, NULL, true ), $strLocaleCode );
	}

	public function getCaption( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCaption', $strLocaleCode );
	}

	public function sqlCaption() {
		return ( true == isset( $this->m_strCaption ) ) ? '\'' . addslashes( $this->m_strCaption ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsPreview( $boolIsPreview ) {
		$this->set( 'm_boolIsPreview', CStrings::strToBool( $boolIsPreview ) );
	}

	public function getIsPreview() {
		return $this->m_boolIsPreview;
	}

	public function sqlIsPreview() {
		return ( true == isset( $this->m_boolIsPreview ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPreview ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_template_slot_id, company_media_file_id, link, link_text, alt, target, caption, is_published, is_preview, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlWebsiteTemplateSlotId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlLink() . ', ' .
						$this->sqlLinkText() . ', ' .
						$this->sqlAlt() . ', ' .
						$this->sqlTarget() . ', ' .
						$this->sqlCaption() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsPreview() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_slot_id = ' . $this->sqlWebsiteTemplateSlotId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateSlotId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_slot_id = ' . $this->sqlWebsiteTemplateSlotId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link = ' . $this->sqlLink(). ',' ; } elseif( true == array_key_exists( 'Link', $this->getChangedColumns() ) ) { $strSql .= ' link = ' . $this->sqlLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt = ' . $this->sqlAlt(). ',' ; } elseif( true == array_key_exists( 'Alt', $this->getChangedColumns() ) ) { $strSql .= ' alt = ' . $this->sqlAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget(). ',' ; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caption = ' . $this->sqlCaption(). ',' ; } elseif( true == array_key_exists( 'Caption', $this->getChangedColumns() ) ) { $strSql .= ' caption = ' . $this->sqlCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_preview = ' . $this->sqlIsPreview(). ',' ; } elseif( true == array_key_exists( 'IsPreview', $this->getChangedColumns() ) ) { $strSql .= ' is_preview = ' . $this->sqlIsPreview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_template_slot_id' => $this->getWebsiteTemplateSlotId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'link' => $this->getLink(),
			'link_text' => $this->getLinkText(),
			'alt' => $this->getAlt(),
			'target' => $this->getTarget(),
			'caption' => $this->getCaption(),
			'is_published' => $this->getIsPublished(),
			'is_preview' => $this->getIsPreview(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>