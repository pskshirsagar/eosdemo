<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceRequestTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestType[]
	 */
	public static function fetchMaintenanceRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceRequestType
	 */
	public static function fetchMaintenanceRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestType::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_types', $objDatabase );
	}

	public static function fetchMaintenanceRequestTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceRequestType( sprintf( 'SELECT * FROM maintenance_request_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>