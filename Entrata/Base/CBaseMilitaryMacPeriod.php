<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMilitaryMacPeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.military_mac_periods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMilitaryInstallationId;
	protected $m_intMilitaryMacPeriodStatusId;
	protected $m_strPostMonth;
	protected $m_intNumMoveIns;
	protected $m_intNumMoveOuts;
	protected $m_intNumPromotions;
	protected $m_intNumDemotions;
	protected $m_fltTotalBahAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strMimoGeneratedOn;
	protected $m_intMimoGeneratedBy;
	protected $m_strMimoAcceptedOn;
	protected $m_intMimoAcceptedBy;
	protected $m_strPromoDemoUploadedOn;
	protected $m_intPromoDemoUploadedBy;
	protected $m_strPromoDemoProcessedOn;
	protected $m_intPromoDemoProcessedBy;
	protected $m_strPaymentsUploadedOn;
	protected $m_intPaymentsUploadedBy;
	protected $m_strPaymentsProcessedOn;
	protected $m_intPaymentsProcessedBy;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['military_installation_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryInstallationId', trim( $arrValues['military_installation_id'] ) ); elseif( isset( $arrValues['military_installation_id'] ) ) $this->setMilitaryInstallationId( $arrValues['military_installation_id'] );
		if( isset( $arrValues['military_mac_period_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryMacPeriodStatusId', trim( $arrValues['military_mac_period_status_id'] ) ); elseif( isset( $arrValues['military_mac_period_status_id'] ) ) $this->setMilitaryMacPeriodStatusId( $arrValues['military_mac_period_status_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['num_move_ins'] ) && $boolDirectSet ) $this->set( 'm_intNumMoveIns', trim( $arrValues['num_move_ins'] ) ); elseif( isset( $arrValues['num_move_ins'] ) ) $this->setNumMoveIns( $arrValues['num_move_ins'] );
		if( isset( $arrValues['num_move_outs'] ) && $boolDirectSet ) $this->set( 'm_intNumMoveOuts', trim( $arrValues['num_move_outs'] ) ); elseif( isset( $arrValues['num_move_outs'] ) ) $this->setNumMoveOuts( $arrValues['num_move_outs'] );
		if( isset( $arrValues['num_promotions'] ) && $boolDirectSet ) $this->set( 'm_intNumPromotions', trim( $arrValues['num_promotions'] ) ); elseif( isset( $arrValues['num_promotions'] ) ) $this->setNumPromotions( $arrValues['num_promotions'] );
		if( isset( $arrValues['num_demotions'] ) && $boolDirectSet ) $this->set( 'm_intNumDemotions', trim( $arrValues['num_demotions'] ) ); elseif( isset( $arrValues['num_demotions'] ) ) $this->setNumDemotions( $arrValues['num_demotions'] );
		if( isset( $arrValues['total_bah_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalBahAmount', trim( $arrValues['total_bah_amount'] ) ); elseif( isset( $arrValues['total_bah_amount'] ) ) $this->setTotalBahAmount( $arrValues['total_bah_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mimo_generated_on'] ) && $boolDirectSet ) $this->set( 'm_strMimoGeneratedOn', trim( $arrValues['mimo_generated_on'] ) ); elseif( isset( $arrValues['mimo_generated_on'] ) ) $this->setMimoGeneratedOn( $arrValues['mimo_generated_on'] );
		if( isset( $arrValues['mimo_generated_by'] ) && $boolDirectSet ) $this->set( 'm_intMimoGeneratedBy', trim( $arrValues['mimo_generated_by'] ) ); elseif( isset( $arrValues['mimo_generated_by'] ) ) $this->setMimoGeneratedBy( $arrValues['mimo_generated_by'] );
		if( isset( $arrValues['mimo_accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strMimoAcceptedOn', trim( $arrValues['mimo_accepted_on'] ) ); elseif( isset( $arrValues['mimo_accepted_on'] ) ) $this->setMimoAcceptedOn( $arrValues['mimo_accepted_on'] );
		if( isset( $arrValues['mimo_accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intMimoAcceptedBy', trim( $arrValues['mimo_accepted_by'] ) ); elseif( isset( $arrValues['mimo_accepted_by'] ) ) $this->setMimoAcceptedBy( $arrValues['mimo_accepted_by'] );
		if( isset( $arrValues['promo_demo_uploaded_on'] ) && $boolDirectSet ) $this->set( 'm_strPromoDemoUploadedOn', trim( $arrValues['promo_demo_uploaded_on'] ) ); elseif( isset( $arrValues['promo_demo_uploaded_on'] ) ) $this->setPromoDemoUploadedOn( $arrValues['promo_demo_uploaded_on'] );
		if( isset( $arrValues['promo_demo_uploaded_by'] ) && $boolDirectSet ) $this->set( 'm_intPromoDemoUploadedBy', trim( $arrValues['promo_demo_uploaded_by'] ) ); elseif( isset( $arrValues['promo_demo_uploaded_by'] ) ) $this->setPromoDemoUploadedBy( $arrValues['promo_demo_uploaded_by'] );
		if( isset( $arrValues['promo_demo_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strPromoDemoProcessedOn', trim( $arrValues['promo_demo_processed_on'] ) ); elseif( isset( $arrValues['promo_demo_processed_on'] ) ) $this->setPromoDemoProcessedOn( $arrValues['promo_demo_processed_on'] );
		if( isset( $arrValues['promo_demo_processed_by'] ) && $boolDirectSet ) $this->set( 'm_intPromoDemoProcessedBy', trim( $arrValues['promo_demo_processed_by'] ) ); elseif( isset( $arrValues['promo_demo_processed_by'] ) ) $this->setPromoDemoProcessedBy( $arrValues['promo_demo_processed_by'] );
		if( isset( $arrValues['payments_uploaded_on'] ) && $boolDirectSet ) $this->set( 'm_strPaymentsUploadedOn', trim( $arrValues['payments_uploaded_on'] ) ); elseif( isset( $arrValues['payments_uploaded_on'] ) ) $this->setPaymentsUploadedOn( $arrValues['payments_uploaded_on'] );
		if( isset( $arrValues['payments_uploaded_by'] ) && $boolDirectSet ) $this->set( 'm_intPaymentsUploadedBy', trim( $arrValues['payments_uploaded_by'] ) ); elseif( isset( $arrValues['payments_uploaded_by'] ) ) $this->setPaymentsUploadedBy( $arrValues['payments_uploaded_by'] );
		if( isset( $arrValues['payments_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strPaymentsProcessedOn', trim( $arrValues['payments_processed_on'] ) ); elseif( isset( $arrValues['payments_processed_on'] ) ) $this->setPaymentsProcessedOn( $arrValues['payments_processed_on'] );
		if( isset( $arrValues['payments_processed_by'] ) && $boolDirectSet ) $this->set( 'm_intPaymentsProcessedBy', trim( $arrValues['payments_processed_by'] ) ); elseif( isset( $arrValues['payments_processed_by'] ) ) $this->setPaymentsProcessedBy( $arrValues['payments_processed_by'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMilitaryInstallationId( $intMilitaryInstallationId ) {
		$this->set( 'm_intMilitaryInstallationId', CStrings::strToIntDef( $intMilitaryInstallationId, NULL, false ) );
	}

	public function getMilitaryInstallationId() {
		return $this->m_intMilitaryInstallationId;
	}

	public function sqlMilitaryInstallationId() {
		return ( true == isset( $this->m_intMilitaryInstallationId ) ) ? ( string ) $this->m_intMilitaryInstallationId : 'NULL';
	}

	public function setMilitaryMacPeriodStatusId( $intMilitaryMacPeriodStatusId ) {
		$this->set( 'm_intMilitaryMacPeriodStatusId', CStrings::strToIntDef( $intMilitaryMacPeriodStatusId, NULL, false ) );
	}

	public function getMilitaryMacPeriodStatusId() {
		return $this->m_intMilitaryMacPeriodStatusId;
	}

	public function sqlMilitaryMacPeriodStatusId() {
		return ( true == isset( $this->m_intMilitaryMacPeriodStatusId ) ) ? ( string ) $this->m_intMilitaryMacPeriodStatusId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setNumMoveIns( $intNumMoveIns ) {
		$this->set( 'm_intNumMoveIns', CStrings::strToIntDef( $intNumMoveIns, NULL, false ) );
	}

	public function getNumMoveIns() {
		return $this->m_intNumMoveIns;
	}

	public function sqlNumMoveIns() {
		return ( true == isset( $this->m_intNumMoveIns ) ) ? ( string ) $this->m_intNumMoveIns : 'NULL';
	}

	public function setNumMoveOuts( $intNumMoveOuts ) {
		$this->set( 'm_intNumMoveOuts', CStrings::strToIntDef( $intNumMoveOuts, NULL, false ) );
	}

	public function getNumMoveOuts() {
		return $this->m_intNumMoveOuts;
	}

	public function sqlNumMoveOuts() {
		return ( true == isset( $this->m_intNumMoveOuts ) ) ? ( string ) $this->m_intNumMoveOuts : 'NULL';
	}

	public function setNumPromotions( $intNumPromotions ) {
		$this->set( 'm_intNumPromotions', CStrings::strToIntDef( $intNumPromotions, NULL, false ) );
	}

	public function getNumPromotions() {
		return $this->m_intNumPromotions;
	}

	public function sqlNumPromotions() {
		return ( true == isset( $this->m_intNumPromotions ) ) ? ( string ) $this->m_intNumPromotions : 'NULL';
	}

	public function setNumDemotions( $intNumDemotions ) {
		$this->set( 'm_intNumDemotions', CStrings::strToIntDef( $intNumDemotions, NULL, false ) );
	}

	public function getNumDemotions() {
		return $this->m_intNumDemotions;
	}

	public function sqlNumDemotions() {
		return ( true == isset( $this->m_intNumDemotions ) ) ? ( string ) $this->m_intNumDemotions : 'NULL';
	}

	public function setTotalBahAmount( $fltTotalBahAmount ) {
		$this->set( 'm_fltTotalBahAmount', CStrings::strToFloatDef( $fltTotalBahAmount, NULL, false, 2 ) );
	}

	public function getTotalBahAmount() {
		return $this->m_fltTotalBahAmount;
	}

	public function sqlTotalBahAmount() {
		return ( true == isset( $this->m_fltTotalBahAmount ) ) ? ( string ) $this->m_fltTotalBahAmount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMimoGeneratedOn( $strMimoGeneratedOn ) {
		$this->set( 'm_strMimoGeneratedOn', CStrings::strTrimDef( $strMimoGeneratedOn, -1, NULL, true ) );
	}

	public function getMimoGeneratedOn() {
		return $this->m_strMimoGeneratedOn;
	}

	public function sqlMimoGeneratedOn() {
		return ( true == isset( $this->m_strMimoGeneratedOn ) ) ? '\'' . $this->m_strMimoGeneratedOn . '\'' : 'NULL';
	}

	public function setMimoGeneratedBy( $intMimoGeneratedBy ) {
		$this->set( 'm_intMimoGeneratedBy', CStrings::strToIntDef( $intMimoGeneratedBy, NULL, false ) );
	}

	public function getMimoGeneratedBy() {
		return $this->m_intMimoGeneratedBy;
	}

	public function sqlMimoGeneratedBy() {
		return ( true == isset( $this->m_intMimoGeneratedBy ) ) ? ( string ) $this->m_intMimoGeneratedBy : 'NULL';
	}

	public function setMimoAcceptedOn( $strMimoAcceptedOn ) {
		$this->set( 'm_strMimoAcceptedOn', CStrings::strTrimDef( $strMimoAcceptedOn, -1, NULL, true ) );
	}

	public function getMimoAcceptedOn() {
		return $this->m_strMimoAcceptedOn;
	}

	public function sqlMimoAcceptedOn() {
		return ( true == isset( $this->m_strMimoAcceptedOn ) ) ? '\'' . $this->m_strMimoAcceptedOn . '\'' : 'NULL';
	}

	public function setMimoAcceptedBy( $intMimoAcceptedBy ) {
		$this->set( 'm_intMimoAcceptedBy', CStrings::strToIntDef( $intMimoAcceptedBy, NULL, false ) );
	}

	public function getMimoAcceptedBy() {
		return $this->m_intMimoAcceptedBy;
	}

	public function sqlMimoAcceptedBy() {
		return ( true == isset( $this->m_intMimoAcceptedBy ) ) ? ( string ) $this->m_intMimoAcceptedBy : 'NULL';
	}

	public function setPromoDemoUploadedOn( $strPromoDemoUploadedOn ) {
		$this->set( 'm_strPromoDemoUploadedOn', CStrings::strTrimDef( $strPromoDemoUploadedOn, -1, NULL, true ) );
	}

	public function getPromoDemoUploadedOn() {
		return $this->m_strPromoDemoUploadedOn;
	}

	public function sqlPromoDemoUploadedOn() {
		return ( true == isset( $this->m_strPromoDemoUploadedOn ) ) ? '\'' . $this->m_strPromoDemoUploadedOn . '\'' : 'NULL';
	}

	public function setPromoDemoUploadedBy( $intPromoDemoUploadedBy ) {
		$this->set( 'm_intPromoDemoUploadedBy', CStrings::strToIntDef( $intPromoDemoUploadedBy, NULL, false ) );
	}

	public function getPromoDemoUploadedBy() {
		return $this->m_intPromoDemoUploadedBy;
	}

	public function sqlPromoDemoUploadedBy() {
		return ( true == isset( $this->m_intPromoDemoUploadedBy ) ) ? ( string ) $this->m_intPromoDemoUploadedBy : 'NULL';
	}

	public function setPromoDemoProcessedOn( $strPromoDemoProcessedOn ) {
		$this->set( 'm_strPromoDemoProcessedOn', CStrings::strTrimDef( $strPromoDemoProcessedOn, -1, NULL, true ) );
	}

	public function getPromoDemoProcessedOn() {
		return $this->m_strPromoDemoProcessedOn;
	}

	public function sqlPromoDemoProcessedOn() {
		return ( true == isset( $this->m_strPromoDemoProcessedOn ) ) ? '\'' . $this->m_strPromoDemoProcessedOn . '\'' : 'NULL';
	}

	public function setPromoDemoProcessedBy( $intPromoDemoProcessedBy ) {
		$this->set( 'm_intPromoDemoProcessedBy', CStrings::strToIntDef( $intPromoDemoProcessedBy, NULL, false ) );
	}

	public function getPromoDemoProcessedBy() {
		return $this->m_intPromoDemoProcessedBy;
	}

	public function sqlPromoDemoProcessedBy() {
		return ( true == isset( $this->m_intPromoDemoProcessedBy ) ) ? ( string ) $this->m_intPromoDemoProcessedBy : 'NULL';
	}

	public function setPaymentsUploadedOn( $strPaymentsUploadedOn ) {
		$this->set( 'm_strPaymentsUploadedOn', CStrings::strTrimDef( $strPaymentsUploadedOn, -1, NULL, true ) );
	}

	public function getPaymentsUploadedOn() {
		return $this->m_strPaymentsUploadedOn;
	}

	public function sqlPaymentsUploadedOn() {
		return ( true == isset( $this->m_strPaymentsUploadedOn ) ) ? '\'' . $this->m_strPaymentsUploadedOn . '\'' : 'NULL';
	}

	public function setPaymentsUploadedBy( $intPaymentsUploadedBy ) {
		$this->set( 'm_intPaymentsUploadedBy', CStrings::strToIntDef( $intPaymentsUploadedBy, NULL, false ) );
	}

	public function getPaymentsUploadedBy() {
		return $this->m_intPaymentsUploadedBy;
	}

	public function sqlPaymentsUploadedBy() {
		return ( true == isset( $this->m_intPaymentsUploadedBy ) ) ? ( string ) $this->m_intPaymentsUploadedBy : 'NULL';
	}

	public function setPaymentsProcessedOn( $strPaymentsProcessedOn ) {
		$this->set( 'm_strPaymentsProcessedOn', CStrings::strTrimDef( $strPaymentsProcessedOn, -1, NULL, true ) );
	}

	public function getPaymentsProcessedOn() {
		return $this->m_strPaymentsProcessedOn;
	}

	public function sqlPaymentsProcessedOn() {
		return ( true == isset( $this->m_strPaymentsProcessedOn ) ) ? '\'' . $this->m_strPaymentsProcessedOn . '\'' : 'NULL';
	}

	public function setPaymentsProcessedBy( $intPaymentsProcessedBy ) {
		$this->set( 'm_intPaymentsProcessedBy', CStrings::strToIntDef( $intPaymentsProcessedBy, NULL, false ) );
	}

	public function getPaymentsProcessedBy() {
		return $this->m_intPaymentsProcessedBy;
	}

	public function sqlPaymentsProcessedBy() {
		return ( true == isset( $this->m_intPaymentsProcessedBy ) ) ? ( string ) $this->m_intPaymentsProcessedBy : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, military_installation_id, military_mac_period_status_id, post_month, num_move_ins, num_move_outs, num_promotions, num_demotions, total_bah_amount, updated_by, updated_on, created_by, created_on, mimo_generated_on, mimo_generated_by, mimo_accepted_on, mimo_accepted_by, promo_demo_uploaded_on, promo_demo_uploaded_by, promo_demo_processed_on, promo_demo_processed_by, payments_uploaded_on, payments_uploaded_by, payments_processed_on, payments_processed_by, deleted_by, deleted_on )
					VALUES ( ' .
		 $strId . ', ' .
		 $this->sqlCid() . ', ' .
		 $this->sqlMilitaryInstallationId() . ', ' .
		 $this->sqlMilitaryMacPeriodStatusId() . ', ' .
		 $this->sqlPostMonth() . ', ' .
		 $this->sqlNumMoveIns() . ', ' .
		 $this->sqlNumMoveOuts() . ', ' .
		 $this->sqlNumPromotions() . ', ' .
		 $this->sqlNumDemotions() . ', ' .
		 $this->sqlTotalBahAmount() . ', ' .
		 ( int ) $intCurrentUserId . ', ' .
		 $this->sqlUpdatedOn() . ', ' .
		 ( int ) $intCurrentUserId . ', ' .
		 $this->sqlCreatedOn() . ', ' .
		 $this->sqlMimoGeneratedOn() . ', ' .
		 $this->sqlMimoGeneratedBy() . ', ' .
		 $this->sqlMimoAcceptedOn() . ', ' .
		 $this->sqlMimoAcceptedBy() . ', ' .
		 $this->sqlPromoDemoUploadedOn() . ', ' .
		 $this->sqlPromoDemoUploadedBy() . ', ' .
		 $this->sqlPromoDemoProcessedOn() . ', ' .
		 $this->sqlPromoDemoProcessedBy() . ', ' .
		 $this->sqlPaymentsUploadedOn() . ', ' .
		 $this->sqlPaymentsUploadedBy() . ', ' .
		 $this->sqlPaymentsProcessedOn() . ', ' .
		 $this->sqlPaymentsProcessedBy() . ', ' .
		 $this->sqlDeletedBy() . ', ' .
		 $this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_installation_id = ' . $this->sqlMilitaryInstallationId(). ',' ; } elseif( true == array_key_exists( 'MilitaryInstallationId', $this->getChangedColumns() ) ) { $strSql .= ' military_installation_id = ' . $this->sqlMilitaryInstallationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_mac_period_status_id = ' . $this->sqlMilitaryMacPeriodStatusId(). ',' ; } elseif( true == array_key_exists( 'MilitaryMacPeriodStatusId', $this->getChangedColumns() ) ) { $strSql .= ' military_mac_period_status_id = ' . $this->sqlMilitaryMacPeriodStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_move_ins = ' . $this->sqlNumMoveIns(). ',' ; } elseif( true == array_key_exists( 'NumMoveIns', $this->getChangedColumns() ) ) { $strSql .= ' num_move_ins = ' . $this->sqlNumMoveIns() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_move_outs = ' . $this->sqlNumMoveOuts(). ',' ; } elseif( true == array_key_exists( 'NumMoveOuts', $this->getChangedColumns() ) ) { $strSql .= ' num_move_outs = ' . $this->sqlNumMoveOuts() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_promotions = ' . $this->sqlNumPromotions(). ',' ; } elseif( true == array_key_exists( 'NumPromotions', $this->getChangedColumns() ) ) { $strSql .= ' num_promotions = ' . $this->sqlNumPromotions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_demotions = ' . $this->sqlNumDemotions(). ',' ; } elseif( true == array_key_exists( 'NumDemotions', $this->getChangedColumns() ) ) { $strSql .= ' num_demotions = ' . $this->sqlNumDemotions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_bah_amount = ' . $this->sqlTotalBahAmount(). ',' ; } elseif( true == array_key_exists( 'TotalBahAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_bah_amount = ' . $this->sqlTotalBahAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mimo_generated_on = ' . $this->sqlMimoGeneratedOn(). ',' ; } elseif( true == array_key_exists( 'MimoGeneratedOn', $this->getChangedColumns() ) ) { $strSql .= ' mimo_generated_on = ' . $this->sqlMimoGeneratedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mimo_generated_by = ' . $this->sqlMimoGeneratedBy(). ',' ; } elseif( true == array_key_exists( 'MimoGeneratedBy', $this->getChangedColumns() ) ) { $strSql .= ' mimo_generated_by = ' . $this->sqlMimoGeneratedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mimo_accepted_on = ' . $this->sqlMimoAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'MimoAcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' mimo_accepted_on = ' . $this->sqlMimoAcceptedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mimo_accepted_by = ' . $this->sqlMimoAcceptedBy(). ',' ; } elseif( true == array_key_exists( 'MimoAcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' mimo_accepted_by = ' . $this->sqlMimoAcceptedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_demo_uploaded_on = ' . $this->sqlPromoDemoUploadedOn(). ',' ; } elseif( true == array_key_exists( 'PromoDemoUploadedOn', $this->getChangedColumns() ) ) { $strSql .= ' promo_demo_uploaded_on = ' . $this->sqlPromoDemoUploadedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_demo_uploaded_by = ' . $this->sqlPromoDemoUploadedBy(). ',' ; } elseif( true == array_key_exists( 'PromoDemoUploadedBy', $this->getChangedColumns() ) ) { $strSql .= ' promo_demo_uploaded_by = ' . $this->sqlPromoDemoUploadedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_demo_processed_on = ' . $this->sqlPromoDemoProcessedOn(). ',' ; } elseif( true == array_key_exists( 'PromoDemoProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' promo_demo_processed_on = ' . $this->sqlPromoDemoProcessedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_demo_processed_by = ' . $this->sqlPromoDemoProcessedBy(). ',' ; } elseif( true == array_key_exists( 'PromoDemoProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' promo_demo_processed_by = ' . $this->sqlPromoDemoProcessedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payments_uploaded_on = ' . $this->sqlPaymentsUploadedOn(). ',' ; } elseif( true == array_key_exists( 'PaymentsUploadedOn', $this->getChangedColumns() ) ) { $strSql .= ' payments_uploaded_on = ' . $this->sqlPaymentsUploadedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payments_uploaded_by = ' . $this->sqlPaymentsUploadedBy(). ',' ; } elseif( true == array_key_exists( 'PaymentsUploadedBy', $this->getChangedColumns() ) ) { $strSql .= ' payments_uploaded_by = ' . $this->sqlPaymentsUploadedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payments_processed_on = ' . $this->sqlPaymentsProcessedOn(). ',' ; } elseif( true == array_key_exists( 'PaymentsProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' payments_processed_on = ' . $this->sqlPaymentsProcessedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payments_processed_by = ' . $this->sqlPaymentsProcessedBy(). ',' ; } elseif( true == array_key_exists( 'PaymentsProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' payments_processed_by = ' . $this->sqlPaymentsProcessedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
		 'id' => $this->getId(),
		 'cid' => $this->getCid(),
		 'military_installation_id' => $this->getMilitaryInstallationId(),
		 'military_mac_period_status_id' => $this->getMilitaryMacPeriodStatusId(),
		 'post_month' => $this->getPostMonth(),
		 'num_move_ins' => $this->getNumMoveIns(),
		 'num_move_outs' => $this->getNumMoveOuts(),
		 'num_promotions' => $this->getNumPromotions(),
		 'num_demotions' => $this->getNumDemotions(),
		 'total_bah_amount' => $this->getTotalBahAmount(),
		 'updated_by' => $this->getUpdatedBy(),
		 'updated_on' => $this->getUpdatedOn(),
		 'created_by' => $this->getCreatedBy(),
		 'created_on' => $this->getCreatedOn(),
		 'mimo_generated_on' => $this->getMimoGeneratedOn(),
		 'mimo_generated_by' => $this->getMimoGeneratedBy(),
		 'mimo_accepted_on' => $this->getMimoAcceptedOn(),
		 'mimo_accepted_by' => $this->getMimoAcceptedBy(),
		 'promo_demo_uploaded_on' => $this->getPromoDemoUploadedOn(),
		 'promo_demo_uploaded_by' => $this->getPromoDemoUploadedBy(),
		 'promo_demo_processed_on' => $this->getPromoDemoProcessedOn(),
		 'promo_demo_processed_by' => $this->getPromoDemoProcessedBy(),
		 'payments_uploaded_on' => $this->getPaymentsUploadedOn(),
		 'payments_uploaded_by' => $this->getPaymentsUploadedBy(),
		 'payments_processed_on' => $this->getPaymentsProcessedOn(),
		 'payments_processed_by' => $this->getPaymentsProcessedBy(),
		 'deleted_by' => $this->getDeletedBy(),
		 'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>
