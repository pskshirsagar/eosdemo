<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWaitList extends CEosSingularBase {

	const TABLE_NAME = 'public.wait_lists';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyGroupId;
	protected $m_intOccupancyTypeId;
	protected $m_intApplicationStageStatusId;
	protected $m_intStrategy;
	protected $m_intExpirationDays;
	protected $m_intResponseDays;
	protected $m_intOfferLimit;
	protected $m_intRejectionLimit;
	protected $m_boolIsActive;
	protected $m_boolHasTransferApplications;
	protected $m_boolEnableAvailabilityWindowForWaitlist;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;
		$this->m_boolHasTransferApplications = false;
		$this->m_boolEnableAvailabilityWindowForWaitlist = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_group_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyGroupId', trim( $arrValues['property_group_id'] ) ); elseif( isset( $arrValues['property_group_id'] ) ) $this->setPropertyGroupId( $arrValues['property_group_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['application_stage_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageStatusId', trim( $arrValues['application_stage_status_id'] ) ); elseif( isset( $arrValues['application_stage_status_id'] ) ) $this->setApplicationStageStatusId( $arrValues['application_stage_status_id'] );
		if( isset( $arrValues['strategy'] ) && $boolDirectSet ) $this->set( 'm_intStrategy', trim( $arrValues['strategy'] ) ); elseif( isset( $arrValues['strategy'] ) ) $this->setStrategy( $arrValues['strategy'] );
		if( isset( $arrValues['expiration_days'] ) && $boolDirectSet ) $this->set( 'm_intExpirationDays', trim( $arrValues['expiration_days'] ) ); elseif( isset( $arrValues['expiration_days'] ) ) $this->setExpirationDays( $arrValues['expiration_days'] );
		if( isset( $arrValues['response_days'] ) && $boolDirectSet ) $this->set( 'm_intResponseDays', trim( $arrValues['response_days'] ) ); elseif( isset( $arrValues['response_days'] ) ) $this->setResponseDays( $arrValues['response_days'] );
		if( isset( $arrValues['offer_limit'] ) && $boolDirectSet ) $this->set( 'm_intOfferLimit', trim( $arrValues['offer_limit'] ) ); elseif( isset( $arrValues['offer_limit'] ) ) $this->setOfferLimit( $arrValues['offer_limit'] );
		if( isset( $arrValues['rejection_limit'] ) && $boolDirectSet ) $this->set( 'm_intRejectionLimit', trim( $arrValues['rejection_limit'] ) ); elseif( isset( $arrValues['rejection_limit'] ) ) $this->setRejectionLimit( $arrValues['rejection_limit'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['has_transfer_applications'] ) && $boolDirectSet ) $this->set( 'm_boolHasTransferApplications', trim( stripcslashes( $arrValues['has_transfer_applications'] ) ) ); elseif( isset( $arrValues['has_transfer_applications'] ) ) $this->setHasTransferApplications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_transfer_applications'] ) : $arrValues['has_transfer_applications'] );
		if( isset( $arrValues['enable_availability_window_for_waitlist'] ) && $boolDirectSet ) $this->set( 'm_boolEnableAvailabilityWindowForWaitlist', trim( stripcslashes( $arrValues['enable_availability_window_for_waitlist'] ) ) ); elseif( isset( $arrValues['enable_availability_window_for_waitlist'] ) ) $this->setEnableAvailabilityWindowForWaitlist( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['enable_availability_window_for_waitlist'] ) : $arrValues['enable_availability_window_for_waitlist'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->set( 'm_intPropertyGroupId', CStrings::strToIntDef( $intPropertyGroupId, NULL, false ) );
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function sqlPropertyGroupId() {
		return ( true == isset( $this->m_intPropertyGroupId ) ) ? ( string ) $this->m_intPropertyGroupId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->set( 'm_intApplicationStageStatusId', CStrings::strToIntDef( $intApplicationStageStatusId, NULL, false ) );
	}

	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	public function sqlApplicationStageStatusId() {
		return ( true == isset( $this->m_intApplicationStageStatusId ) ) ? ( string ) $this->m_intApplicationStageStatusId : 'NULL';
	}

	public function setStrategy( $intStrategy ) {
		$this->set( 'm_intStrategy', CStrings::strToIntDef( $intStrategy, NULL, false ) );
	}

	public function getStrategy() {
		return $this->m_intStrategy;
	}

	public function sqlStrategy() {
		return ( true == isset( $this->m_intStrategy ) ) ? ( string ) $this->m_intStrategy : 'NULL';
	}

	public function setExpirationDays( $intExpirationDays ) {
		$this->set( 'm_intExpirationDays', CStrings::strToIntDef( $intExpirationDays, NULL, false ) );
	}

	public function getExpirationDays() {
		return $this->m_intExpirationDays;
	}

	public function sqlExpirationDays() {
		return ( true == isset( $this->m_intExpirationDays ) ) ? ( string ) $this->m_intExpirationDays : 'NULL';
	}

	public function setResponseDays( $intResponseDays ) {
		$this->set( 'm_intResponseDays', CStrings::strToIntDef( $intResponseDays, NULL, false ) );
	}

	public function getResponseDays() {
		return $this->m_intResponseDays;
	}

	public function sqlResponseDays() {
		return ( true == isset( $this->m_intResponseDays ) ) ? ( string ) $this->m_intResponseDays : 'NULL';
	}

	public function setOfferLimit( $intOfferLimit ) {
		$this->set( 'm_intOfferLimit', CStrings::strToIntDef( $intOfferLimit, NULL, false ) );
	}

	public function getOfferLimit() {
		return $this->m_intOfferLimit;
	}

	public function sqlOfferLimit() {
		return ( true == isset( $this->m_intOfferLimit ) ) ? ( string ) $this->m_intOfferLimit : 'NULL';
	}

	public function setRejectionLimit( $intRejectionLimit ) {
		$this->set( 'm_intRejectionLimit', CStrings::strToIntDef( $intRejectionLimit, NULL, false ) );
	}

	public function getRejectionLimit() {
		return $this->m_intRejectionLimit;
	}

	public function sqlRejectionLimit() {
		return ( true == isset( $this->m_intRejectionLimit ) ) ? ( string ) $this->m_intRejectionLimit : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasTransferApplications( $boolHasTransferApplications ) {
		$this->set( 'm_boolHasTransferApplications', CStrings::strToBool( $boolHasTransferApplications ) );
	}

	public function getHasTransferApplications() {
		return $this->m_boolHasTransferApplications;
	}

	public function sqlHasTransferApplications() {
		return ( true == isset( $this->m_boolHasTransferApplications ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasTransferApplications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEnableAvailabilityWindowForWaitlist( $boolEnableAvailabilityWindowForWaitlist ) {
		$this->set( 'm_boolEnableAvailabilityWindowForWaitlist', CStrings::strToBool( $boolEnableAvailabilityWindowForWaitlist ) );
	}

	public function getEnableAvailabilityWindowForWaitlist() {
		return $this->m_boolEnableAvailabilityWindowForWaitlist;
	}

	public function sqlEnableAvailabilityWindowForWaitlist() {
		return ( true == isset( $this->m_boolEnableAvailabilityWindowForWaitlist ) ) ? '\'' . ( true == ( bool ) $this->m_boolEnableAvailabilityWindowForWaitlist ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_group_id, occupancy_type_id, application_stage_status_id, strategy, expiration_days, response_days, offer_limit, rejection_limit, is_active, has_transfer_applications, enable_availability_window_for_waitlist, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyGroupId() . ', ' .
		          $this->sqlOccupancyTypeId() . ', ' .
		          $this->sqlApplicationStageStatusId() . ', ' .
		          $this->sqlStrategy() . ', ' .
		          $this->sqlExpirationDays() . ', ' .
		          $this->sqlResponseDays() . ', ' .
		          $this->sqlOfferLimit() . ', ' .
		          $this->sqlRejectionLimit() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          $this->sqlHasTransferApplications() . ', ' .
		          $this->sqlEnableAvailabilityWindowForWaitlist() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_id = ' . $this->sqlPropertyGroupId() . ','; } elseif( true == array_key_exists( 'PropertyGroupId', $this->getChangedColumns() ) ) { $strSql .= ' property_group_id = ' . $this->sqlPropertyGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId() . ','; } elseif( true == array_key_exists( 'ApplicationStageStatusId', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' strategy = ' . $this->sqlStrategy() . ','; } elseif( true == array_key_exists( 'Strategy', $this->getChangedColumns() ) ) { $strSql .= ' strategy = ' . $this->sqlStrategy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration_days = ' . $this->sqlExpirationDays() . ','; } elseif( true == array_key_exists( 'ExpirationDays', $this->getChangedColumns() ) ) { $strSql .= ' expiration_days = ' . $this->sqlExpirationDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_days = ' . $this->sqlResponseDays() . ','; } elseif( true == array_key_exists( 'ResponseDays', $this->getChangedColumns() ) ) { $strSql .= ' response_days = ' . $this->sqlResponseDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_limit = ' . $this->sqlOfferLimit() . ','; } elseif( true == array_key_exists( 'OfferLimit', $this->getChangedColumns() ) ) { $strSql .= ' offer_limit = ' . $this->sqlOfferLimit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_limit = ' . $this->sqlRejectionLimit() . ','; } elseif( true == array_key_exists( 'RejectionLimit', $this->getChangedColumns() ) ) { $strSql .= ' rejection_limit = ' . $this->sqlRejectionLimit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_transfer_applications = ' . $this->sqlHasTransferApplications() . ','; } elseif( true == array_key_exists( 'HasTransferApplications', $this->getChangedColumns() ) ) { $strSql .= ' has_transfer_applications = ' . $this->sqlHasTransferApplications() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_availability_window_for_waitlist = ' . $this->sqlEnableAvailabilityWindowForWaitlist() . ','; } elseif( true == array_key_exists( 'EnableAvailabilityWindowForWaitlist', $this->getChangedColumns() ) ) { $strSql .= ' enable_availability_window_for_waitlist = ' . $this->sqlEnableAvailabilityWindowForWaitlist() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_group_id' => $this->getPropertyGroupId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'application_stage_status_id' => $this->getApplicationStageStatusId(),
			'strategy' => $this->getStrategy(),
			'expiration_days' => $this->getExpirationDays(),
			'response_days' => $this->getResponseDays(),
			'offer_limit' => $this->getOfferLimit(),
			'rejection_limit' => $this->getRejectionLimit(),
			'is_active' => $this->getIsActive(),
			'has_transfer_applications' => $this->getHasTransferApplications(),
			'enable_availability_window_for_waitlist' => $this->getEnableAvailabilityWindowForWaitlist(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>