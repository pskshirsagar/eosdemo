<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenuePostRules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenuePostRules extends CEosPluralBase {

	/**
	 * @return CRevenuePostRule[]
	 */
	public static function fetchRevenuePostRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenuePostRule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenuePostRule
	 */
	public static function fetchRevenuePostRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenuePostRule', $objDatabase );
	}

	public static function fetchRevenuePostRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_post_rules', $objDatabase );
	}

	public static function fetchRevenuePostRuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRule( sprintf( 'SELECT * FROM revenue_post_rules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRulesByCid( $intCid, $objDatabase ) {
		return self::fetchRevenuePostRules( sprintf( 'SELECT * FROM revenue_post_rules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRules( sprintf( 'SELECT * FROM revenue_post_rules WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>