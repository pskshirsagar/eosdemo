<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueStudentDemands
 * Do not add any new functions to this class.
 */

class CBaseRevenueStudentDemands extends CEosPluralBase {

	/**
	 * @return CRevenueStudentDemand[]
	 */
	public static function fetchRevenueStudentDemands( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueStudentDemand::class, $objDatabase );
	}

	/**
	 * @return CRevenueStudentDemand
	 */
	public static function fetchRevenueStudentDemand( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueStudentDemand::class, $objDatabase );
	}

	public static function fetchRevenueStudentDemandCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_student_demands', $objDatabase );
	}

	public static function fetchRevenueStudentDemandBy( $int, $objDatabase ) {
		return self::fetchRevenueStudentDemand( sprintf( 'SELECT * FROM revenue_student_demands WHERE	= %d', $int ), $objDatabase );
	}

}
?>