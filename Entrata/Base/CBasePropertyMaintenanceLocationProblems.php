<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceLocationProblems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceLocationProblems extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceLocationProblem[]
	 */
	public static function fetchPropertyMaintenanceLocationProblems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMaintenanceLocationProblem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceLocationProblem
	 */
	public static function fetchPropertyMaintenanceLocationProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMaintenanceLocationProblem', $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_location_problems', $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocationProblem( sprintf( 'SELECT * FROM property_maintenance_location_problems WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM property_maintenance_location_problems WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM property_maintenance_location_problems WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM property_maintenance_location_problems WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationProblemsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM property_maintenance_location_problems WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

}
?>