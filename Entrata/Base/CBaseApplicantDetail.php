<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantDetail extends CEosSingularBase {

	use TEosDetails;
	use TEosTranslated;

	const TABLE_NAME = 'public.applicant_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intSecondaryPhoneNumberTypeId;
	protected $m_intMessageOperatorId;
	protected $m_strStudentIdNumber;
	protected $m_strPetDescription;
	protected $m_strRelocationReason;
	protected $m_strBankruptcyCounty;
	protected $m_strBankruptcyStateCode;
	protected $m_strInsuranceProviderName;
	protected $m_strInsurancePolicyNumber;
	protected $m_strFelonyComments;
	protected $m_strSuedForRentComments;
	protected $m_strSuedForDamageComments;
	protected $m_strEvictedComments;
	protected $m_strBrokenLeaseComments;
	protected $m_strFelonyConvictionComments;
	protected $m_strPreviousRentOwedComments;
	protected $m_strBankruptcyComments;
	protected $m_strReturningResidentComments;
	protected $m_strUnemployedComments;
	protected $m_strReasonInUsComments;
	protected $m_strGaaScreeningInitials;
	protected $m_strRelationshipToApplicant;
	protected $m_intFelonyIsResolved;
	protected $m_intSmokes;
	protected $m_intHasPets;
	protected $m_intHasFelonyConviction;
	protected $m_intHasBeenEvicted;
	protected $m_intHasBrokenLease;
	protected $m_intHasBankruptcy;
	protected $m_intHasPublicJudgment;
	protected $m_intHasRentersInsurance;
	protected $m_intHasPreviousRentOwed;
	protected $m_intHasMiddleName;
	protected $m_intWasSuedForRent;
	protected $m_intWasSuedForDamages;
	protected $m_intIsAlien;
	protected $m_intIsReturningResident;
	protected $m_intIsUnemployed;
	protected $m_intIsLegalUsResident;
	protected $m_intFelonyYear;
	protected $m_intBankruptcyYear;
	protected $m_intPublicJudgmentYear;
	protected $m_strReferenceComments;
	protected $m_intReferencesApprovedBy;
	protected $m_strReferencesApprovedOn;
	protected $m_strSmsActivatedOn;
	protected $m_strSmsCancelledOn;
	protected $m_strSmsApplicationUpdatesActivatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intHasAltMiddleName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intHasNameLastMatronymic;
	protected $m_strCitizenshipCountryCode;

	public function __construct() {
		parent::__construct();

		$this->m_intHasMiddleName = '1';
		$this->m_intIsAlien = '0';
		$this->m_intHasAltMiddleName = '1';
		$this->m_intHasNameLastMatronymic = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['primary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryPhoneNumberTypeId', trim( $arrValues['primary_phone_number_type_id'] ) ); elseif( isset( $arrValues['primary_phone_number_type_id'] ) ) $this->setPrimaryPhoneNumberTypeId( $arrValues['primary_phone_number_type_id'] );
		if( isset( $arrValues['secondary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryPhoneNumberTypeId', trim( $arrValues['secondary_phone_number_type_id'] ) ); elseif( isset( $arrValues['secondary_phone_number_type_id'] ) ) $this->setSecondaryPhoneNumberTypeId( $arrValues['secondary_phone_number_type_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['student_id_number'] ) && $boolDirectSet ) $this->set( 'm_strStudentIdNumber', trim( stripcslashes( $arrValues['student_id_number'] ) ) ); elseif( isset( $arrValues['student_id_number'] ) ) $this->setStudentIdNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['student_id_number'] ) : $arrValues['student_id_number'] );
		if( isset( $arrValues['pet_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPetDescription', trim( stripcslashes( $arrValues['pet_description'] ) ) ); elseif( isset( $arrValues['pet_description'] ) ) $this->setPetDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pet_description'] ) : $arrValues['pet_description'] );
		if( isset( $arrValues['relocation_reason'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRelocationReason', trim( stripcslashes( $arrValues['relocation_reason'] ) ) ); elseif( isset( $arrValues['relocation_reason'] ) ) $this->setRelocationReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['relocation_reason'] ) : $arrValues['relocation_reason'] );
		if( isset( $arrValues['bankruptcy_county'] ) && $boolDirectSet ) $this->set( 'm_strBankruptcyCounty', trim( stripcslashes( $arrValues['bankruptcy_county'] ) ) ); elseif( isset( $arrValues['bankruptcy_county'] ) ) $this->setBankruptcyCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bankruptcy_county'] ) : $arrValues['bankruptcy_county'] );
		if( isset( $arrValues['bankruptcy_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBankruptcyStateCode', trim( stripcslashes( $arrValues['bankruptcy_state_code'] ) ) ); elseif( isset( $arrValues['bankruptcy_state_code'] ) ) $this->setBankruptcyStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bankruptcy_state_code'] ) : $arrValues['bankruptcy_state_code'] );
		if( isset( $arrValues['insurance_provider_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuranceProviderName', trim( stripcslashes( $arrValues['insurance_provider_name'] ) ) ); elseif( isset( $arrValues['insurance_provider_name'] ) ) $this->setInsuranceProviderName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insurance_provider_name'] ) : $arrValues['insurance_provider_name'] );
		if( isset( $arrValues['insurance_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyNumber', trim( stripcslashes( $arrValues['insurance_policy_number'] ) ) ); elseif( isset( $arrValues['insurance_policy_number'] ) ) $this->setInsurancePolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insurance_policy_number'] ) : $arrValues['insurance_policy_number'] );
		if( isset( $arrValues['felony_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFelonyComments', trim( stripcslashes( $arrValues['felony_comments'] ) ) ); elseif( isset( $arrValues['felony_comments'] ) ) $this->setFelonyComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['felony_comments'] ) : $arrValues['felony_comments'] );
		if( isset( $arrValues['sued_for_rent_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSuedForRentComments', trim( stripcslashes( $arrValues['sued_for_rent_comments'] ) ) ); elseif( isset( $arrValues['sued_for_rent_comments'] ) ) $this->setSuedForRentComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sued_for_rent_comments'] ) : $arrValues['sued_for_rent_comments'] );
		if( isset( $arrValues['sued_for_damage_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSuedForDamageComments', trim( stripcslashes( $arrValues['sued_for_damage_comments'] ) ) ); elseif( isset( $arrValues['sued_for_damage_comments'] ) ) $this->setSuedForDamageComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sued_for_damage_comments'] ) : $arrValues['sued_for_damage_comments'] );
		if( isset( $arrValues['evicted_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strEvictedComments', trim( stripcslashes( $arrValues['evicted_comments'] ) ) ); elseif( isset( $arrValues['evicted_comments'] ) ) $this->setEvictedComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['evicted_comments'] ) : $arrValues['evicted_comments'] );
		if( isset( $arrValues['broken_lease_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBrokenLeaseComments', trim( stripcslashes( $arrValues['broken_lease_comments'] ) ) ); elseif( isset( $arrValues['broken_lease_comments'] ) ) $this->setBrokenLeaseComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['broken_lease_comments'] ) : $arrValues['broken_lease_comments'] );
		if( isset( $arrValues['felony_conviction_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFelonyConvictionComments', trim( stripcslashes( $arrValues['felony_conviction_comments'] ) ) ); elseif( isset( $arrValues['felony_conviction_comments'] ) ) $this->setFelonyConvictionComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['felony_conviction_comments'] ) : $arrValues['felony_conviction_comments'] );
		if( isset( $arrValues['previous_rent_owed_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPreviousRentOwedComments', trim( stripcslashes( $arrValues['previous_rent_owed_comments'] ) ) ); elseif( isset( $arrValues['previous_rent_owed_comments'] ) ) $this->setPreviousRentOwedComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['previous_rent_owed_comments'] ) : $arrValues['previous_rent_owed_comments'] );
		if( isset( $arrValues['bankruptcy_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBankruptcyComments', trim( stripcslashes( $arrValues['bankruptcy_comments'] ) ) ); elseif( isset( $arrValues['bankruptcy_comments'] ) ) $this->setBankruptcyComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bankruptcy_comments'] ) : $arrValues['bankruptcy_comments'] );
		if( isset( $arrValues['returning_resident_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strReturningResidentComments', trim( stripcslashes( $arrValues['returning_resident_comments'] ) ) ); elseif( isset( $arrValues['returning_resident_comments'] ) ) $this->setReturningResidentComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['returning_resident_comments'] ) : $arrValues['returning_resident_comments'] );
		if( isset( $arrValues['unemployed_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnemployedComments', trim( stripcslashes( $arrValues['unemployed_comments'] ) ) ); elseif( isset( $arrValues['unemployed_comments'] ) ) $this->setUnemployedComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unemployed_comments'] ) : $arrValues['unemployed_comments'] );
		if( isset( $arrValues['reason_in_us_comments'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strReasonInUsComments', trim( stripcslashes( $arrValues['reason_in_us_comments'] ) ) ); elseif( isset( $arrValues['reason_in_us_comments'] ) ) $this->setReasonInUsComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_in_us_comments'] ) : $arrValues['reason_in_us_comments'] );
		if( isset( $arrValues['gaa_screening_initials'] ) && $boolDirectSet ) $this->set( 'm_strGaaScreeningInitials', trim( stripcslashes( $arrValues['gaa_screening_initials'] ) ) ); elseif( isset( $arrValues['gaa_screening_initials'] ) ) $this->setGaaScreeningInitials( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gaa_screening_initials'] ) : $arrValues['gaa_screening_initials'] );
		if( isset( $arrValues['relationship_to_applicant'] ) && $boolDirectSet ) $this->set( 'm_strRelationshipToApplicant', trim( stripcslashes( $arrValues['relationship_to_applicant'] ) ) ); elseif( isset( $arrValues['relationship_to_applicant'] ) ) $this->setRelationshipToApplicant( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['relationship_to_applicant'] ) : $arrValues['relationship_to_applicant'] );
		if( isset( $arrValues['felony_is_resolved'] ) && $boolDirectSet ) $this->set( 'm_intFelonyIsResolved', trim( $arrValues['felony_is_resolved'] ) ); elseif( isset( $arrValues['felony_is_resolved'] ) ) $this->setFelonyIsResolved( $arrValues['felony_is_resolved'] );
		if( isset( $arrValues['smokes'] ) && $boolDirectSet ) $this->set( 'm_intSmokes', trim( $arrValues['smokes'] ) ); elseif( isset( $arrValues['smokes'] ) ) $this->setSmokes( $arrValues['smokes'] );
		if( isset( $arrValues['has_pets'] ) && $boolDirectSet ) $this->set( 'm_intHasPets', trim( $arrValues['has_pets'] ) ); elseif( isset( $arrValues['has_pets'] ) ) $this->setHasPets( $arrValues['has_pets'] );
		if( isset( $arrValues['has_felony_conviction'] ) && $boolDirectSet ) $this->set( 'm_intHasFelonyConviction', trim( $arrValues['has_felony_conviction'] ) ); elseif( isset( $arrValues['has_felony_conviction'] ) ) $this->setHasFelonyConviction( $arrValues['has_felony_conviction'] );
		if( isset( $arrValues['has_been_evicted'] ) && $boolDirectSet ) $this->set( 'm_intHasBeenEvicted', trim( $arrValues['has_been_evicted'] ) ); elseif( isset( $arrValues['has_been_evicted'] ) ) $this->setHasBeenEvicted( $arrValues['has_been_evicted'] );
		if( isset( $arrValues['has_broken_lease'] ) && $boolDirectSet ) $this->set( 'm_intHasBrokenLease', trim( $arrValues['has_broken_lease'] ) ); elseif( isset( $arrValues['has_broken_lease'] ) ) $this->setHasBrokenLease( $arrValues['has_broken_lease'] );
		if( isset( $arrValues['has_bankruptcy'] ) && $boolDirectSet ) $this->set( 'm_intHasBankruptcy', trim( $arrValues['has_bankruptcy'] ) ); elseif( isset( $arrValues['has_bankruptcy'] ) ) $this->setHasBankruptcy( $arrValues['has_bankruptcy'] );
		if( isset( $arrValues['has_public_judgment'] ) && $boolDirectSet ) $this->set( 'm_intHasPublicJudgment', trim( $arrValues['has_public_judgment'] ) ); elseif( isset( $arrValues['has_public_judgment'] ) ) $this->setHasPublicJudgment( $arrValues['has_public_judgment'] );
		if( isset( $arrValues['has_renters_insurance'] ) && $boolDirectSet ) $this->set( 'm_intHasRentersInsurance', trim( $arrValues['has_renters_insurance'] ) ); elseif( isset( $arrValues['has_renters_insurance'] ) ) $this->setHasRentersInsurance( $arrValues['has_renters_insurance'] );
		if( isset( $arrValues['has_previous_rent_owed'] ) && $boolDirectSet ) $this->set( 'm_intHasPreviousRentOwed', trim( $arrValues['has_previous_rent_owed'] ) ); elseif( isset( $arrValues['has_previous_rent_owed'] ) ) $this->setHasPreviousRentOwed( $arrValues['has_previous_rent_owed'] );
		if( isset( $arrValues['has_middle_name'] ) && $boolDirectSet ) $this->set( 'm_intHasMiddleName', trim( $arrValues['has_middle_name'] ) ); elseif( isset( $arrValues['has_middle_name'] ) ) $this->setHasMiddleName( $arrValues['has_middle_name'] );
		if( isset( $arrValues['was_sued_for_rent'] ) && $boolDirectSet ) $this->set( 'm_intWasSuedForRent', trim( $arrValues['was_sued_for_rent'] ) ); elseif( isset( $arrValues['was_sued_for_rent'] ) ) $this->setWasSuedForRent( $arrValues['was_sued_for_rent'] );
		if( isset( $arrValues['was_sued_for_damages'] ) && $boolDirectSet ) $this->set( 'm_intWasSuedForDamages', trim( $arrValues['was_sued_for_damages'] ) ); elseif( isset( $arrValues['was_sued_for_damages'] ) ) $this->setWasSuedForDamages( $arrValues['was_sued_for_damages'] );
		if( isset( $arrValues['is_alien'] ) && $boolDirectSet ) $this->set( 'm_intIsAlien', trim( $arrValues['is_alien'] ) ); elseif( isset( $arrValues['is_alien'] ) ) $this->setIsAlien( $arrValues['is_alien'] );
		if( isset( $arrValues['is_returning_resident'] ) && $boolDirectSet ) $this->set( 'm_intIsReturningResident', trim( $arrValues['is_returning_resident'] ) ); elseif( isset( $arrValues['is_returning_resident'] ) ) $this->setIsReturningResident( $arrValues['is_returning_resident'] );
		if( isset( $arrValues['is_unemployed'] ) && $boolDirectSet ) $this->set( 'm_intIsUnemployed', trim( $arrValues['is_unemployed'] ) ); elseif( isset( $arrValues['is_unemployed'] ) ) $this->setIsUnemployed( $arrValues['is_unemployed'] );
		if( isset( $arrValues['is_legal_us_resident'] ) && $boolDirectSet ) $this->set( 'm_intIsLegalUsResident', trim( $arrValues['is_legal_us_resident'] ) ); elseif( isset( $arrValues['is_legal_us_resident'] ) ) $this->setIsLegalUsResident( $arrValues['is_legal_us_resident'] );
		if( isset( $arrValues['felony_year'] ) && $boolDirectSet ) $this->set( 'm_intFelonyYear', trim( $arrValues['felony_year'] ) ); elseif( isset( $arrValues['felony_year'] ) ) $this->setFelonyYear( $arrValues['felony_year'] );
		if( isset( $arrValues['bankruptcy_year'] ) && $boolDirectSet ) $this->set( 'm_intBankruptcyYear', trim( $arrValues['bankruptcy_year'] ) ); elseif( isset( $arrValues['bankruptcy_year'] ) ) $this->setBankruptcyYear( $arrValues['bankruptcy_year'] );
		if( isset( $arrValues['public_judgment_year'] ) && $boolDirectSet ) $this->set( 'm_intPublicJudgmentYear', trim( $arrValues['public_judgment_year'] ) ); elseif( isset( $arrValues['public_judgment_year'] ) ) $this->setPublicJudgmentYear( $arrValues['public_judgment_year'] );
		if( isset( $arrValues['reference_comments'] ) && $boolDirectSet ) $this->set( 'm_strReferenceComments', trim( $arrValues['reference_comments'] ) ); elseif( isset( $arrValues['reference_comments'] ) ) $this->setReferenceComments( $arrValues['reference_comments'] );
		if( isset( $arrValues['references_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intReferencesApprovedBy', trim( $arrValues['references_approved_by'] ) ); elseif( isset( $arrValues['references_approved_by'] ) ) $this->setReferencesApprovedBy( $arrValues['references_approved_by'] );
		if( isset( $arrValues['references_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strReferencesApprovedOn', trim( $arrValues['references_approved_on'] ) ); elseif( isset( $arrValues['references_approved_on'] ) ) $this->setReferencesApprovedOn( $arrValues['references_approved_on'] );
		if( isset( $arrValues['sms_activated_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsActivatedOn', trim( $arrValues['sms_activated_on'] ) ); elseif( isset( $arrValues['sms_activated_on'] ) ) $this->setSmsActivatedOn( $arrValues['sms_activated_on'] );
		if( isset( $arrValues['sms_cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsCancelledOn', trim( $arrValues['sms_cancelled_on'] ) ); elseif( isset( $arrValues['sms_cancelled_on'] ) ) $this->setSmsCancelledOn( $arrValues['sms_cancelled_on'] );
		if( isset( $arrValues['sms_application_updates_activated_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsApplicationUpdatesActivatedOn', trim( $arrValues['sms_application_updates_activated_on'] ) ); elseif( isset( $arrValues['sms_application_updates_activated_on'] ) ) $this->setSmsApplicationUpdatesActivatedOn( $arrValues['sms_application_updates_activated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['has_alt_middle_name'] ) && $boolDirectSet ) $this->set( 'm_intHasAltMiddleName', trim( $arrValues['has_alt_middle_name'] ) ); elseif( isset( $arrValues['has_alt_middle_name'] ) ) $this->setHasAltMiddleName( $arrValues['has_alt_middle_name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['has_name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_intHasNameLastMatronymic', trim( $arrValues['has_name_last_matronymic'] ) ); elseif( isset( $arrValues['has_name_last_matronymic'] ) ) $this->setHasNameLastMatronymic( $arrValues['has_name_last_matronymic'] );
		if( isset( $arrValues['citizenship_country_code'] ) && $boolDirectSet ) $this->set( 'm_strCitizenshipCountryCode', trim( stripcslashes( $arrValues['citizenship_country_code'] ) ) ); elseif( isset( $arrValues['citizenship_country_code'] ) ) $this->setCitizenshipCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['citizenship_country_code'] ) : $arrValues['citizenship_country_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->set( 'm_intPrimaryPhoneNumberTypeId', CStrings::strToIntDef( $intPrimaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getPrimaryPhoneNumberTypeId() {
		return $this->m_intPrimaryPhoneNumberTypeId;
	}

	public function sqlPrimaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPrimaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intPrimaryPhoneNumberTypeId : 'NULL';
	}

	public function setSecondaryPhoneNumberTypeId( $intSecondaryPhoneNumberTypeId ) {
		$this->set( 'm_intSecondaryPhoneNumberTypeId', CStrings::strToIntDef( $intSecondaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getSecondaryPhoneNumberTypeId() {
		return $this->m_intSecondaryPhoneNumberTypeId;
	}

	public function sqlSecondaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intSecondaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intSecondaryPhoneNumberTypeId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setStudentIdNumber( $strStudentIdNumber ) {
		$this->set( 'm_strStudentIdNumber', CStrings::strTrimDef( $strStudentIdNumber, 25, NULL, true ) );
	}

	public function getStudentIdNumber() {
		return $this->m_strStudentIdNumber;
	}

	public function sqlStudentIdNumber() {
		return ( true == isset( $this->m_strStudentIdNumber ) ) ? '\'' . addslashes( $this->m_strStudentIdNumber ) . '\'' : 'NULL';
	}

	public function setPetDescription( $strPetDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPetDescription', CStrings::strTrimDef( $strPetDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getPetDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPetDescription', $strLocaleCode );
	}

	public function sqlPetDescription() {
		return ( true == isset( $this->m_strPetDescription ) ) ? '\'' . addslashes( $this->m_strPetDescription ) . '\'' : 'NULL';
	}

	public function setRelocationReason( $strRelocationReason, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRelocationReason', CStrings::strTrimDef( $strRelocationReason, 2000, NULL, true ), $strLocaleCode );
	}

	public function getRelocationReason( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRelocationReason', $strLocaleCode );
	}

	public function sqlRelocationReason() {
		return ( true == isset( $this->m_strRelocationReason ) ) ? '\'' . addslashes( $this->m_strRelocationReason ) . '\'' : 'NULL';
	}

	public function setBankruptcyCounty( $strBankruptcyCounty ) {
		$this->set( 'm_strBankruptcyCounty', CStrings::strTrimDef( $strBankruptcyCounty, 50, NULL, true ) );
	}

	public function getBankruptcyCounty() {
		return $this->m_strBankruptcyCounty;
	}

	public function sqlBankruptcyCounty() {
		return ( true == isset( $this->m_strBankruptcyCounty ) ) ? '\'' . addslashes( $this->m_strBankruptcyCounty ) . '\'' : 'NULL';
	}

	public function setBankruptcyStateCode( $strBankruptcyStateCode ) {
		$this->set( 'm_strBankruptcyStateCode', CStrings::strTrimDef( $strBankruptcyStateCode, 2, NULL, true ) );
	}

	public function getBankruptcyStateCode() {
		return $this->m_strBankruptcyStateCode;
	}

	public function sqlBankruptcyStateCode() {
		return ( true == isset( $this->m_strBankruptcyStateCode ) ) ? '\'' . addslashes( $this->m_strBankruptcyStateCode ) . '\'' : 'NULL';
	}

	public function setInsuranceProviderName( $strInsuranceProviderName ) {
		$this->set( 'm_strInsuranceProviderName', CStrings::strTrimDef( $strInsuranceProviderName, 50, NULL, true ) );
	}

	public function getInsuranceProviderName() {
		return $this->m_strInsuranceProviderName;
	}

	public function sqlInsuranceProviderName() {
		return ( true == isset( $this->m_strInsuranceProviderName ) ) ? '\'' . addslashes( $this->m_strInsuranceProviderName ) . '\'' : 'NULL';
	}

	public function setInsurancePolicyNumber( $strInsurancePolicyNumber ) {
		$this->set( 'm_strInsurancePolicyNumber', CStrings::strTrimDef( $strInsurancePolicyNumber, 50, NULL, true ) );
	}

	public function getInsurancePolicyNumber() {
		return $this->m_strInsurancePolicyNumber;
	}

	public function sqlInsurancePolicyNumber() {
		return ( true == isset( $this->m_strInsurancePolicyNumber ) ) ? '\'' . addslashes( $this->m_strInsurancePolicyNumber ) . '\'' : 'NULL';
	}

	public function setFelonyComments( $strFelonyComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFelonyComments', CStrings::strTrimDef( $strFelonyComments, 2000, NULL, true ), $strLocaleCode );
	}

	public function getFelonyComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFelonyComments', $strLocaleCode );
	}

	public function sqlFelonyComments() {
		return ( true == isset( $this->m_strFelonyComments ) ) ? '\'' . addslashes( $this->m_strFelonyComments ) . '\'' : 'NULL';
	}

	public function setSuedForRentComments( $strSuedForRentComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSuedForRentComments', CStrings::strTrimDef( $strSuedForRentComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getSuedForRentComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSuedForRentComments', $strLocaleCode );
	}

	public function sqlSuedForRentComments() {
		return ( true == isset( $this->m_strSuedForRentComments ) ) ? '\'' . addslashes( $this->m_strSuedForRentComments ) . '\'' : 'NULL';
	}

	public function setSuedForDamageComments( $strSuedForDamageComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSuedForDamageComments', CStrings::strTrimDef( $strSuedForDamageComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getSuedForDamageComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSuedForDamageComments', $strLocaleCode );
	}

	public function sqlSuedForDamageComments() {
		return ( true == isset( $this->m_strSuedForDamageComments ) ) ? '\'' . addslashes( $this->m_strSuedForDamageComments ) . '\'' : 'NULL';
	}

	public function setEvictedComments( $strEvictedComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strEvictedComments', CStrings::strTrimDef( $strEvictedComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getEvictedComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strEvictedComments', $strLocaleCode );
	}

	public function sqlEvictedComments() {
		return ( true == isset( $this->m_strEvictedComments ) ) ? '\'' . addslashes( $this->m_strEvictedComments ) . '\'' : 'NULL';
	}

	public function setBrokenLeaseComments( $strBrokenLeaseComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBrokenLeaseComments', CStrings::strTrimDef( $strBrokenLeaseComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getBrokenLeaseComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBrokenLeaseComments', $strLocaleCode );
	}

	public function sqlBrokenLeaseComments() {
		return ( true == isset( $this->m_strBrokenLeaseComments ) ) ? '\'' . addslashes( $this->m_strBrokenLeaseComments ) . '\'' : 'NULL';
	}

	public function setFelonyConvictionComments( $strFelonyConvictionComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFelonyConvictionComments', CStrings::strTrimDef( $strFelonyConvictionComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getFelonyConvictionComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFelonyConvictionComments', $strLocaleCode );
	}

	public function sqlFelonyConvictionComments() {
		return ( true == isset( $this->m_strFelonyConvictionComments ) ) ? '\'' . addslashes( $this->m_strFelonyConvictionComments ) . '\'' : 'NULL';
	}

	public function setPreviousRentOwedComments( $strPreviousRentOwedComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPreviousRentOwedComments', CStrings::strTrimDef( $strPreviousRentOwedComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getPreviousRentOwedComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPreviousRentOwedComments', $strLocaleCode );
	}

	public function sqlPreviousRentOwedComments() {
		return ( true == isset( $this->m_strPreviousRentOwedComments ) ) ? '\'' . addslashes( $this->m_strPreviousRentOwedComments ) . '\'' : 'NULL';
	}

	public function setBankruptcyComments( $strBankruptcyComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBankruptcyComments', CStrings::strTrimDef( $strBankruptcyComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getBankruptcyComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBankruptcyComments', $strLocaleCode );
	}

	public function sqlBankruptcyComments() {
		return ( true == isset( $this->m_strBankruptcyComments ) ) ? '\'' . addslashes( $this->m_strBankruptcyComments ) . '\'' : 'NULL';
	}

	public function setReturningResidentComments( $strReturningResidentComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strReturningResidentComments', CStrings::strTrimDef( $strReturningResidentComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getReturningResidentComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strReturningResidentComments', $strLocaleCode );
	}

	public function sqlReturningResidentComments() {
		return ( true == isset( $this->m_strReturningResidentComments ) ) ? '\'' . addslashes( $this->m_strReturningResidentComments ) . '\'' : 'NULL';
	}

	public function setUnemployedComments( $strUnemployedComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnemployedComments', CStrings::strTrimDef( $strUnemployedComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getUnemployedComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnemployedComments', $strLocaleCode );
	}

	public function sqlUnemployedComments() {
		return ( true == isset( $this->m_strUnemployedComments ) ) ? '\'' . addslashes( $this->m_strUnemployedComments ) . '\'' : 'NULL';
	}

	public function setReasonInUsComments( $strReasonInUsComments, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strReasonInUsComments', CStrings::strTrimDef( $strReasonInUsComments, 240, NULL, true ), $strLocaleCode );
	}

	public function getReasonInUsComments( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strReasonInUsComments', $strLocaleCode );
	}

	public function sqlReasonInUsComments() {
		return ( true == isset( $this->m_strReasonInUsComments ) ) ? '\'' . addslashes( $this->m_strReasonInUsComments ) . '\'' : 'NULL';
	}

	public function setGaaScreeningInitials( $strGaaScreeningInitials ) {
		$this->set( 'm_strGaaScreeningInitials', CStrings::strTrimDef( $strGaaScreeningInitials, 240, NULL, true ) );
	}

	public function getGaaScreeningInitials() {
		return $this->m_strGaaScreeningInitials;
	}

	public function sqlGaaScreeningInitials() {
		return ( true == isset( $this->m_strGaaScreeningInitials ) ) ? '\'' . addslashes( $this->m_strGaaScreeningInitials ) . '\'' : 'NULL';
	}

	public function setRelationshipToApplicant( $strRelationshipToApplicant ) {
		$this->set( 'm_strRelationshipToApplicant', CStrings::strTrimDef( $strRelationshipToApplicant, 50, NULL, true ) );
	}

	public function getRelationshipToApplicant() {
		return $this->m_strRelationshipToApplicant;
	}

	public function sqlRelationshipToApplicant() {
		return ( true == isset( $this->m_strRelationshipToApplicant ) ) ? '\'' . addslashes( $this->m_strRelationshipToApplicant ) . '\'' : 'NULL';
	}

	public function setFelonyIsResolved( $intFelonyIsResolved ) {
		$this->set( 'm_intFelonyIsResolved', CStrings::strToIntDef( $intFelonyIsResolved, NULL, false ) );
	}

	public function getFelonyIsResolved() {
		return $this->m_intFelonyIsResolved;
	}

	public function sqlFelonyIsResolved() {
		return ( true == isset( $this->m_intFelonyIsResolved ) ) ? ( string ) $this->m_intFelonyIsResolved : 'NULL';
	}

	public function setSmokes( $intSmokes ) {
		$this->set( 'm_intSmokes', CStrings::strToIntDef( $intSmokes, NULL, false ) );
	}

	public function getSmokes() {
		return $this->m_intSmokes;
	}

	public function sqlSmokes() {
		return ( true == isset( $this->m_intSmokes ) ) ? ( string ) $this->m_intSmokes : 'NULL';
	}

	public function setHasPets( $intHasPets ) {
		$this->set( 'm_intHasPets', CStrings::strToIntDef( $intHasPets, NULL, false ) );
	}

	public function getHasPets() {
		return $this->m_intHasPets;
	}

	public function sqlHasPets() {
		return ( true == isset( $this->m_intHasPets ) ) ? ( string ) $this->m_intHasPets : 'NULL';
	}

	public function setHasFelonyConviction( $intHasFelonyConviction ) {
		$this->set( 'm_intHasFelonyConviction', CStrings::strToIntDef( $intHasFelonyConviction, NULL, false ) );
	}

	public function getHasFelonyConviction() {
		return $this->m_intHasFelonyConviction;
	}

	public function sqlHasFelonyConviction() {
		return ( true == isset( $this->m_intHasFelonyConviction ) ) ? ( string ) $this->m_intHasFelonyConviction : 'NULL';
	}

	public function setHasBeenEvicted( $intHasBeenEvicted ) {
		$this->set( 'm_intHasBeenEvicted', CStrings::strToIntDef( $intHasBeenEvicted, NULL, false ) );
	}

	public function getHasBeenEvicted() {
		return $this->m_intHasBeenEvicted;
	}

	public function sqlHasBeenEvicted() {
		return ( true == isset( $this->m_intHasBeenEvicted ) ) ? ( string ) $this->m_intHasBeenEvicted : 'NULL';
	}

	public function setHasBrokenLease( $intHasBrokenLease ) {
		$this->set( 'm_intHasBrokenLease', CStrings::strToIntDef( $intHasBrokenLease, NULL, false ) );
	}

	public function getHasBrokenLease() {
		return $this->m_intHasBrokenLease;
	}

	public function sqlHasBrokenLease() {
		return ( true == isset( $this->m_intHasBrokenLease ) ) ? ( string ) $this->m_intHasBrokenLease : 'NULL';
	}

	public function setHasBankruptcy( $intHasBankruptcy ) {
		$this->set( 'm_intHasBankruptcy', CStrings::strToIntDef( $intHasBankruptcy, NULL, false ) );
	}

	public function getHasBankruptcy() {
		return $this->m_intHasBankruptcy;
	}

	public function sqlHasBankruptcy() {
		return ( true == isset( $this->m_intHasBankruptcy ) ) ? ( string ) $this->m_intHasBankruptcy : 'NULL';
	}

	public function setHasPublicJudgment( $intHasPublicJudgment ) {
		$this->set( 'm_intHasPublicJudgment', CStrings::strToIntDef( $intHasPublicJudgment, NULL, false ) );
	}

	public function getHasPublicJudgment() {
		return $this->m_intHasPublicJudgment;
	}

	public function sqlHasPublicJudgment() {
		return ( true == isset( $this->m_intHasPublicJudgment ) ) ? ( string ) $this->m_intHasPublicJudgment : 'NULL';
	}

	public function setHasRentersInsurance( $intHasRentersInsurance ) {
		$this->set( 'm_intHasRentersInsurance', CStrings::strToIntDef( $intHasRentersInsurance, NULL, false ) );
	}

	public function getHasRentersInsurance() {
		return $this->m_intHasRentersInsurance;
	}

	public function sqlHasRentersInsurance() {
		return ( true == isset( $this->m_intHasRentersInsurance ) ) ? ( string ) $this->m_intHasRentersInsurance : 'NULL';
	}

	public function setHasPreviousRentOwed( $intHasPreviousRentOwed ) {
		$this->set( 'm_intHasPreviousRentOwed', CStrings::strToIntDef( $intHasPreviousRentOwed, NULL, false ) );
	}

	public function getHasPreviousRentOwed() {
		return $this->m_intHasPreviousRentOwed;
	}

	public function sqlHasPreviousRentOwed() {
		return ( true == isset( $this->m_intHasPreviousRentOwed ) ) ? ( string ) $this->m_intHasPreviousRentOwed : 'NULL';
	}

	public function setHasMiddleName( $intHasMiddleName ) {
		$this->set( 'm_intHasMiddleName', CStrings::strToIntDef( $intHasMiddleName, NULL, false ) );
	}

	public function getHasMiddleName() {
		return $this->m_intHasMiddleName;
	}

	public function sqlHasMiddleName() {
		return ( true == isset( $this->m_intHasMiddleName ) ) ? ( string ) $this->m_intHasMiddleName : '1';
	}

	public function setWasSuedForRent( $intWasSuedForRent ) {
		$this->set( 'm_intWasSuedForRent', CStrings::strToIntDef( $intWasSuedForRent, NULL, false ) );
	}

	public function getWasSuedForRent() {
		return $this->m_intWasSuedForRent;
	}

	public function sqlWasSuedForRent() {
		return ( true == isset( $this->m_intWasSuedForRent ) ) ? ( string ) $this->m_intWasSuedForRent : 'NULL';
	}

	public function setWasSuedForDamages( $intWasSuedForDamages ) {
		$this->set( 'm_intWasSuedForDamages', CStrings::strToIntDef( $intWasSuedForDamages, NULL, false ) );
	}

	public function getWasSuedForDamages() {
		return $this->m_intWasSuedForDamages;
	}

	public function sqlWasSuedForDamages() {
		return ( true == isset( $this->m_intWasSuedForDamages ) ) ? ( string ) $this->m_intWasSuedForDamages : 'NULL';
	}

	public function setIsAlien( $intIsAlien ) {
		$this->set( 'm_intIsAlien', CStrings::strToIntDef( $intIsAlien, NULL, false ) );
	}

	public function getIsAlien() {
		return $this->m_intIsAlien;
	}

	public function sqlIsAlien() {
		return ( true == isset( $this->m_intIsAlien ) ) ? ( string ) $this->m_intIsAlien : '0';
	}

	public function setIsReturningResident( $intIsReturningResident ) {
		$this->set( 'm_intIsReturningResident', CStrings::strToIntDef( $intIsReturningResident, NULL, false ) );
	}

	public function getIsReturningResident() {
		return $this->m_intIsReturningResident;
	}

	public function sqlIsReturningResident() {
		return ( true == isset( $this->m_intIsReturningResident ) ) ? ( string ) $this->m_intIsReturningResident : 'NULL';
	}

	public function setIsUnemployed( $intIsUnemployed ) {
		$this->set( 'm_intIsUnemployed', CStrings::strToIntDef( $intIsUnemployed, NULL, false ) );
	}

	public function getIsUnemployed() {
		return $this->m_intIsUnemployed;
	}

	public function sqlIsUnemployed() {
		return ( true == isset( $this->m_intIsUnemployed ) ) ? ( string ) $this->m_intIsUnemployed : 'NULL';
	}

	public function setIsLegalUsResident( $intIsLegalUsResident ) {
		$this->set( 'm_intIsLegalUsResident', CStrings::strToIntDef( $intIsLegalUsResident, NULL, false ) );
	}

	public function getIsLegalUsResident() {
		return $this->m_intIsLegalUsResident;
	}

	public function sqlIsLegalUsResident() {
		return ( true == isset( $this->m_intIsLegalUsResident ) ) ? ( string ) $this->m_intIsLegalUsResident : 'NULL';
	}

	public function setFelonyYear( $intFelonyYear ) {
		$this->set( 'm_intFelonyYear', CStrings::strToIntDef( $intFelonyYear, NULL, false ) );
	}

	public function getFelonyYear() {
		return $this->m_intFelonyYear;
	}

	public function sqlFelonyYear() {
		return ( true == isset( $this->m_intFelonyYear ) ) ? ( string ) $this->m_intFelonyYear : 'NULL';
	}

	public function setBankruptcyYear( $intBankruptcyYear ) {
		$this->set( 'm_intBankruptcyYear', CStrings::strToIntDef( $intBankruptcyYear, NULL, false ) );
	}

	public function getBankruptcyYear() {
		return $this->m_intBankruptcyYear;
	}

	public function sqlBankruptcyYear() {
		return ( true == isset( $this->m_intBankruptcyYear ) ) ? ( string ) $this->m_intBankruptcyYear : 'NULL';
	}

	public function setPublicJudgmentYear( $intPublicJudgmentYear ) {
		$this->set( 'm_intPublicJudgmentYear', CStrings::strToIntDef( $intPublicJudgmentYear, NULL, false ) );
	}

	public function getPublicJudgmentYear() {
		return $this->m_intPublicJudgmentYear;
	}

	public function sqlPublicJudgmentYear() {
		return ( true == isset( $this->m_intPublicJudgmentYear ) ) ? ( string ) $this->m_intPublicJudgmentYear : 'NULL';
	}

	public function setReferenceComments( $strReferenceComments ) {
		$this->set( 'm_strReferenceComments', CStrings::strTrimDef( $strReferenceComments, -1, NULL, true ) );
	}

	public function getReferenceComments() {
		return $this->m_strReferenceComments;
	}

	public function sqlReferenceComments() {
		return ( true == isset( $this->m_strReferenceComments ) ) ? '\'' . $this->m_strReferenceComments . '\'' : 'NULL';
	}

	public function setReferencesApprovedBy( $intReferencesApprovedBy ) {
		$this->set( 'm_intReferencesApprovedBy', CStrings::strToIntDef( $intReferencesApprovedBy, NULL, false ) );
	}

	public function getReferencesApprovedBy() {
		return $this->m_intReferencesApprovedBy;
	}

	public function sqlReferencesApprovedBy() {
		return ( true == isset( $this->m_intReferencesApprovedBy ) ) ? ( string ) $this->m_intReferencesApprovedBy : 'NULL';
	}

	public function setReferencesApprovedOn( $strReferencesApprovedOn ) {
		$this->set( 'm_strReferencesApprovedOn', CStrings::strTrimDef( $strReferencesApprovedOn, -1, NULL, true ) );
	}

	public function getReferencesApprovedOn() {
		return $this->m_strReferencesApprovedOn;
	}

	public function sqlReferencesApprovedOn() {
		return ( true == isset( $this->m_strReferencesApprovedOn ) ) ? '\'' . $this->m_strReferencesApprovedOn . '\'' : 'NULL';
	}

	public function setSmsActivatedOn( $strSmsActivatedOn ) {
		$this->set( 'm_strSmsActivatedOn', CStrings::strTrimDef( $strSmsActivatedOn, -1, NULL, true ) );
	}

	public function getSmsActivatedOn() {
		return $this->m_strSmsActivatedOn;
	}

	public function sqlSmsActivatedOn() {
		return ( true == isset( $this->m_strSmsActivatedOn ) ) ? '\'' . $this->m_strSmsActivatedOn . '\'' : 'NULL';
	}

	public function setSmsCancelledOn( $strSmsCancelledOn ) {
		$this->set( 'm_strSmsCancelledOn', CStrings::strTrimDef( $strSmsCancelledOn, -1, NULL, true ) );
	}

	public function getSmsCancelledOn() {
		return $this->m_strSmsCancelledOn;
	}

	public function sqlSmsCancelledOn() {
		return ( true == isset( $this->m_strSmsCancelledOn ) ) ? '\'' . $this->m_strSmsCancelledOn . '\'' : 'NULL';
	}

	public function setSmsApplicationUpdatesActivatedOn( $strSmsApplicationUpdatesActivatedOn ) {
		$this->set( 'm_strSmsApplicationUpdatesActivatedOn', CStrings::strTrimDef( $strSmsApplicationUpdatesActivatedOn, -1, NULL, true ) );
	}

	public function getSmsApplicationUpdatesActivatedOn() {
		return $this->m_strSmsApplicationUpdatesActivatedOn;
	}

	public function sqlSmsApplicationUpdatesActivatedOn() {
		return ( true == isset( $this->m_strSmsApplicationUpdatesActivatedOn ) ) ? '\'' . $this->m_strSmsApplicationUpdatesActivatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setHasAltMiddleName( $intHasAltMiddleName ) {
		$this->set( 'm_intHasAltMiddleName', CStrings::strToIntDef( $intHasAltMiddleName, NULL, false ) );
	}

	public function getHasAltMiddleName() {
		return $this->m_intHasAltMiddleName;
	}

	public function sqlHasAltMiddleName() {
		return ( true == isset( $this->m_intHasAltMiddleName ) ) ? ( string ) $this->m_intHasAltMiddleName : '1';
	}

	public function setHasNameLastMatronymic( $intHasNameLastMatronymic ) {
		$this->set( 'm_intHasNameLastMatronymic', CStrings::strToIntDef( $intHasNameLastMatronymic, NULL, false ) );
	}

	public function getHasNameLastMatronymic() {
		return $this->m_intHasNameLastMatronymic;
	}

	public function sqlHasNameLastMatronymic() {
		return ( true == isset( $this->m_intHasNameLastMatronymic ) ) ? ( string ) $this->m_intHasNameLastMatronymic : '1';
	}

	public function setCitizenshipCountryCode( $strCitizenshipCountryCode ) {
		$this->set( 'm_strCitizenshipCountryCode', CStrings::strTrimDef( $strCitizenshipCountryCode, 2, NULL, true ) );
	}

	public function getCitizenshipCountryCode() {
		return $this->m_strCitizenshipCountryCode;
	}

	public function sqlCitizenshipCountryCode() {
		return ( true == isset( $this->m_strCitizenshipCountryCode ) ) ? '\'' . addslashes( $this->m_strCitizenshipCountryCode ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, primary_phone_number_type_id, secondary_phone_number_type_id, message_operator_id, student_id_number, pet_description, relocation_reason, bankruptcy_county, bankruptcy_state_code, insurance_provider_name, insurance_policy_number, felony_comments, sued_for_rent_comments, sued_for_damage_comments, evicted_comments, broken_lease_comments, felony_conviction_comments, previous_rent_owed_comments, bankruptcy_comments, returning_resident_comments, unemployed_comments, reason_in_us_comments, gaa_screening_initials, relationship_to_applicant, felony_is_resolved, smokes, has_pets, has_felony_conviction, has_been_evicted, has_broken_lease, has_bankruptcy, has_public_judgment, has_renters_insurance, has_previous_rent_owed, has_middle_name, was_sued_for_rent, was_sued_for_damages, is_alien, is_returning_resident, is_unemployed, is_legal_us_resident, felony_year, bankruptcy_year, public_judgment_year, reference_comments, references_approved_by, references_approved_on, sms_activated_on, sms_cancelled_on, sms_application_updates_activated_on, updated_by, updated_on, created_by, created_on, has_alt_middle_name, details, has_name_last_matronymic )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlPetDescription() . ', ' .
						$this->sqlRelocationReason() . ', ' .
						$this->sqlBankruptcyCounty() . ', ' .
						$this->sqlBankruptcyStateCode() . ', ' .
						$this->sqlInsuranceProviderName() . ', ' .
						$this->sqlInsurancePolicyNumber() . ', ' .
						$this->sqlFelonyComments() . ', ' .
						$this->sqlSuedForRentComments() . ', ' .
						$this->sqlSuedForDamageComments() . ', ' .
						$this->sqlEvictedComments() . ', ' .
						$this->sqlBrokenLeaseComments() . ', ' .
						$this->sqlFelonyConvictionComments() . ', ' .
						$this->sqlPreviousRentOwedComments() . ', ' .
						$this->sqlBankruptcyComments() . ', ' .
						$this->sqlReturningResidentComments() . ', ' .
						$this->sqlUnemployedComments() . ', ' .
						$this->sqlReasonInUsComments() . ', ' .
						$this->sqlGaaScreeningInitials() . ', ' .
						$this->sqlRelationshipToApplicant() . ', ' .
						$this->sqlFelonyIsResolved() . ', ' .
						$this->sqlSmokes() . ', ' .
						$this->sqlHasPets() . ', ' .
						$this->sqlHasFelonyConviction() . ', ' .
						$this->sqlHasBeenEvicted() . ', ' .
						$this->sqlHasBrokenLease() . ', ' .
						$this->sqlHasBankruptcy() . ', ' .
						$this->sqlHasPublicJudgment() . ', ' .
						$this->sqlHasRentersInsurance() . ', ' .
						$this->sqlHasPreviousRentOwed() . ', ' .
						$this->sqlHasMiddleName() . ', ' .
						$this->sqlWasSuedForRent() . ', ' .
						$this->sqlWasSuedForDamages() . ', ' .
						$this->sqlIsAlien() . ', ' .
						$this->sqlIsReturningResident() . ', ' .
						$this->sqlIsUnemployed() . ', ' .
						$this->sqlIsLegalUsResident() . ', ' .
						$this->sqlFelonyYear() . ', ' .
						$this->sqlBankruptcyYear() . ', ' .
						$this->sqlPublicJudgmentYear() . ', ' .
						$this->sqlReferenceComments() . ', ' .
						$this->sqlReferencesApprovedBy() . ', ' .
						$this->sqlReferencesApprovedOn() . ', ' .
						$this->sqlSmsActivatedOn() . ', ' .
						$this->sqlSmsCancelledOn() . ', ' .
						$this->sqlSmsApplicationUpdatesActivatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlHasAltMiddleName() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
		                $this->sqlCitizenshipCountryCode() . ' ) ' . ' RETURNING id;';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number_type_id = ' . $this->sqlPrimaryPhoneNumberTypeId() . ','; } elseif( true == array_key_exists( 'PrimaryPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number_type_id = ' . $this->sqlPrimaryPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_phone_number_type_id = ' . $this->sqlSecondaryPhoneNumberTypeId() . ','; } elseif( true == array_key_exists( 'SecondaryPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' secondary_phone_number_type_id = ' . $this->sqlSecondaryPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; } elseif( true == array_key_exists( 'MessageOperatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_id_number = ' . $this->sqlStudentIdNumber() . ','; } elseif( true == array_key_exists( 'StudentIdNumber', $this->getChangedColumns() ) ) { $strSql .= ' student_id_number = ' . $this->sqlStudentIdNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_description = ' . $this->sqlPetDescription() . ','; } elseif( true == array_key_exists( 'PetDescription', $this->getChangedColumns() ) ) { $strSql .= ' pet_description = ' . $this->sqlPetDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relocation_reason = ' . $this->sqlRelocationReason() . ','; } elseif( true == array_key_exists( 'RelocationReason', $this->getChangedColumns() ) ) { $strSql .= ' relocation_reason = ' . $this->sqlRelocationReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bankruptcy_county = ' . $this->sqlBankruptcyCounty() . ','; } elseif( true == array_key_exists( 'BankruptcyCounty', $this->getChangedColumns() ) ) { $strSql .= ' bankruptcy_county = ' . $this->sqlBankruptcyCounty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bankruptcy_state_code = ' . $this->sqlBankruptcyStateCode() . ','; } elseif( true == array_key_exists( 'BankruptcyStateCode', $this->getChangedColumns() ) ) { $strSql .= ' bankruptcy_state_code = ' . $this->sqlBankruptcyStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_provider_name = ' . $this->sqlInsuranceProviderName() . ','; } elseif( true == array_key_exists( 'InsuranceProviderName', $this->getChangedColumns() ) ) { $strSql .= ' insurance_provider_name = ' . $this->sqlInsuranceProviderName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber() . ','; } elseif( true == array_key_exists( 'InsurancePolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_comments = ' . $this->sqlFelonyComments() . ','; } elseif( true == array_key_exists( 'FelonyComments', $this->getChangedColumns() ) ) { $strSql .= ' felony_comments = ' . $this->sqlFelonyComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sued_for_rent_comments = ' . $this->sqlSuedForRentComments() . ','; } elseif( true == array_key_exists( 'SuedForRentComments', $this->getChangedColumns() ) ) { $strSql .= ' sued_for_rent_comments = ' . $this->sqlSuedForRentComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sued_for_damage_comments = ' . $this->sqlSuedForDamageComments() . ','; } elseif( true == array_key_exists( 'SuedForDamageComments', $this->getChangedColumns() ) ) { $strSql .= ' sued_for_damage_comments = ' . $this->sqlSuedForDamageComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' evicted_comments = ' . $this->sqlEvictedComments() . ','; } elseif( true == array_key_exists( 'EvictedComments', $this->getChangedColumns() ) ) { $strSql .= ' evicted_comments = ' . $this->sqlEvictedComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' broken_lease_comments = ' . $this->sqlBrokenLeaseComments() . ','; } elseif( true == array_key_exists( 'BrokenLeaseComments', $this->getChangedColumns() ) ) { $strSql .= ' broken_lease_comments = ' . $this->sqlBrokenLeaseComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_conviction_comments = ' . $this->sqlFelonyConvictionComments() . ','; } elseif( true == array_key_exists( 'FelonyConvictionComments', $this->getChangedColumns() ) ) { $strSql .= ' felony_conviction_comments = ' . $this->sqlFelonyConvictionComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_rent_owed_comments = ' . $this->sqlPreviousRentOwedComments() . ','; } elseif( true == array_key_exists( 'PreviousRentOwedComments', $this->getChangedColumns() ) ) { $strSql .= ' previous_rent_owed_comments = ' . $this->sqlPreviousRentOwedComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bankruptcy_comments = ' . $this->sqlBankruptcyComments() . ','; } elseif( true == array_key_exists( 'BankruptcyComments', $this->getChangedColumns() ) ) { $strSql .= ' bankruptcy_comments = ' . $this->sqlBankruptcyComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returning_resident_comments = ' . $this->sqlReturningResidentComments() . ','; } elseif( true == array_key_exists( 'ReturningResidentComments', $this->getChangedColumns() ) ) { $strSql .= ' returning_resident_comments = ' . $this->sqlReturningResidentComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unemployed_comments = ' . $this->sqlUnemployedComments() . ','; } elseif( true == array_key_exists( 'UnemployedComments', $this->getChangedColumns() ) ) { $strSql .= ' unemployed_comments = ' . $this->sqlUnemployedComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_in_us_comments = ' . $this->sqlReasonInUsComments() . ','; } elseif( true == array_key_exists( 'ReasonInUsComments', $this->getChangedColumns() ) ) { $strSql .= ' reason_in_us_comments = ' . $this->sqlReasonInUsComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gaa_screening_initials = ' . $this->sqlGaaScreeningInitials() . ','; } elseif( true == array_key_exists( 'GaaScreeningInitials', $this->getChangedColumns() ) ) { $strSql .= ' gaa_screening_initials = ' . $this->sqlGaaScreeningInitials() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relationship_to_applicant = ' . $this->sqlRelationshipToApplicant() . ','; } elseif( true == array_key_exists( 'RelationshipToApplicant', $this->getChangedColumns() ) ) { $strSql .= ' relationship_to_applicant = ' . $this->sqlRelationshipToApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_is_resolved = ' . $this->sqlFelonyIsResolved() . ','; } elseif( true == array_key_exists( 'FelonyIsResolved', $this->getChangedColumns() ) ) { $strSql .= ' felony_is_resolved = ' . $this->sqlFelonyIsResolved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' smokes = ' . $this->sqlSmokes() . ','; } elseif( true == array_key_exists( 'Smokes', $this->getChangedColumns() ) ) { $strSql .= ' smokes = ' . $this->sqlSmokes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_pets = ' . $this->sqlHasPets() . ','; } elseif( true == array_key_exists( 'HasPets', $this->getChangedColumns() ) ) { $strSql .= ' has_pets = ' . $this->sqlHasPets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_felony_conviction = ' . $this->sqlHasFelonyConviction() . ','; } elseif( true == array_key_exists( 'HasFelonyConviction', $this->getChangedColumns() ) ) { $strSql .= ' has_felony_conviction = ' . $this->sqlHasFelonyConviction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_been_evicted = ' . $this->sqlHasBeenEvicted() . ','; } elseif( true == array_key_exists( 'HasBeenEvicted', $this->getChangedColumns() ) ) { $strSql .= ' has_been_evicted = ' . $this->sqlHasBeenEvicted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_broken_lease = ' . $this->sqlHasBrokenLease() . ','; } elseif( true == array_key_exists( 'HasBrokenLease', $this->getChangedColumns() ) ) { $strSql .= ' has_broken_lease = ' . $this->sqlHasBrokenLease() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_bankruptcy = ' . $this->sqlHasBankruptcy() . ','; } elseif( true == array_key_exists( 'HasBankruptcy', $this->getChangedColumns() ) ) { $strSql .= ' has_bankruptcy = ' . $this->sqlHasBankruptcy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_public_judgment = ' . $this->sqlHasPublicJudgment() . ','; } elseif( true == array_key_exists( 'HasPublicJudgment', $this->getChangedColumns() ) ) { $strSql .= ' has_public_judgment = ' . $this->sqlHasPublicJudgment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_renters_insurance = ' . $this->sqlHasRentersInsurance() . ','; } elseif( true == array_key_exists( 'HasRentersInsurance', $this->getChangedColumns() ) ) { $strSql .= ' has_renters_insurance = ' . $this->sqlHasRentersInsurance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_previous_rent_owed = ' . $this->sqlHasPreviousRentOwed() . ','; } elseif( true == array_key_exists( 'HasPreviousRentOwed', $this->getChangedColumns() ) ) { $strSql .= ' has_previous_rent_owed = ' . $this->sqlHasPreviousRentOwed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_middle_name = ' . $this->sqlHasMiddleName() . ','; } elseif( true == array_key_exists( 'HasMiddleName', $this->getChangedColumns() ) ) { $strSql .= ' has_middle_name = ' . $this->sqlHasMiddleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' was_sued_for_rent = ' . $this->sqlWasSuedForRent() . ','; } elseif( true == array_key_exists( 'WasSuedForRent', $this->getChangedColumns() ) ) { $strSql .= ' was_sued_for_rent = ' . $this->sqlWasSuedForRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' was_sued_for_damages = ' . $this->sqlWasSuedForDamages() . ','; } elseif( true == array_key_exists( 'WasSuedForDamages', $this->getChangedColumns() ) ) { $strSql .= ' was_sued_for_damages = ' . $this->sqlWasSuedForDamages() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_alien = ' . $this->sqlIsAlien() . ','; } elseif( true == array_key_exists( 'IsAlien', $this->getChangedColumns() ) ) { $strSql .= ' is_alien = ' . $this->sqlIsAlien() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_returning_resident = ' . $this->sqlIsReturningResident() . ','; } elseif( true == array_key_exists( 'IsReturningResident', $this->getChangedColumns() ) ) { $strSql .= ' is_returning_resident = ' . $this->sqlIsReturningResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unemployed = ' . $this->sqlIsUnemployed() . ','; } elseif( true == array_key_exists( 'IsUnemployed', $this->getChangedColumns() ) ) { $strSql .= ' is_unemployed = ' . $this->sqlIsUnemployed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_legal_us_resident = ' . $this->sqlIsLegalUsResident() . ','; } elseif( true == array_key_exists( 'IsLegalUsResident', $this->getChangedColumns() ) ) { $strSql .= ' is_legal_us_resident = ' . $this->sqlIsLegalUsResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_year = ' . $this->sqlFelonyYear() . ','; } elseif( true == array_key_exists( 'FelonyYear', $this->getChangedColumns() ) ) { $strSql .= ' felony_year = ' . $this->sqlFelonyYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bankruptcy_year = ' . $this->sqlBankruptcyYear() . ','; } elseif( true == array_key_exists( 'BankruptcyYear', $this->getChangedColumns() ) ) { $strSql .= ' bankruptcy_year = ' . $this->sqlBankruptcyYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' public_judgment_year = ' . $this->sqlPublicJudgmentYear() . ','; } elseif( true == array_key_exists( 'PublicJudgmentYear', $this->getChangedColumns() ) ) { $strSql .= ' public_judgment_year = ' . $this->sqlPublicJudgmentYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_comments = ' . $this->sqlReferenceComments() . ','; } elseif( true == array_key_exists( 'ReferenceComments', $this->getChangedColumns() ) ) { $strSql .= ' reference_comments = ' . $this->sqlReferenceComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' references_approved_by = ' . $this->sqlReferencesApprovedBy() . ','; } elseif( true == array_key_exists( 'ReferencesApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' references_approved_by = ' . $this->sqlReferencesApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' references_approved_on = ' . $this->sqlReferencesApprovedOn() . ','; } elseif( true == array_key_exists( 'ReferencesApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' references_approved_on = ' . $this->sqlReferencesApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_activated_on = ' . $this->sqlSmsActivatedOn() . ','; } elseif( true == array_key_exists( 'SmsActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' sms_activated_on = ' . $this->sqlSmsActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_cancelled_on = ' . $this->sqlSmsCancelledOn() . ','; } elseif( true == array_key_exists( 'SmsCancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' sms_cancelled_on = ' . $this->sqlSmsCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_application_updates_activated_on = ' . $this->sqlSmsApplicationUpdatesActivatedOn() . ','; } elseif( true == array_key_exists( 'SmsApplicationUpdatesActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' sms_application_updates_activated_on = ' . $this->sqlSmsApplicationUpdatesActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_alt_middle_name = ' . $this->sqlHasAltMiddleName() . ','; } elseif( true == array_key_exists( 'HasAltMiddleName', $this->getChangedColumns() ) ) { $strSql .= ' has_alt_middle_name = ' . $this->sqlHasAltMiddleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_name_last_matronymic = ' . $this->sqlHasNameLastMatronymic() . ','; } elseif( true == array_key_exists( 'HasNameLastMatronymic', $this->getChangedColumns() ) ) { $strSql .= ' has_name_last_matronymic = ' . $this->sqlHasNameLastMatronymic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' citizenship_country_code = ' . $this->sqlCitizenshipCountryCode() . ','; } elseif( true == array_key_exists( 'CitizenshipCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' citizenship_country_code = ' . $this->sqlCitizenshipCountryCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'primary_phone_number_type_id' => $this->getPrimaryPhoneNumberTypeId(),
			'secondary_phone_number_type_id' => $this->getSecondaryPhoneNumberTypeId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'student_id_number' => $this->getStudentIdNumber(),
			'pet_description' => $this->getPetDescription(),
			'relocation_reason' => $this->getRelocationReason(),
			'bankruptcy_county' => $this->getBankruptcyCounty(),
			'bankruptcy_state_code' => $this->getBankruptcyStateCode(),
			'insurance_provider_name' => $this->getInsuranceProviderName(),
			'insurance_policy_number' => $this->getInsurancePolicyNumber(),
			'felony_comments' => $this->getFelonyComments(),
			'sued_for_rent_comments' => $this->getSuedForRentComments(),
			'sued_for_damage_comments' => $this->getSuedForDamageComments(),
			'evicted_comments' => $this->getEvictedComments(),
			'broken_lease_comments' => $this->getBrokenLeaseComments(),
			'felony_conviction_comments' => $this->getFelonyConvictionComments(),
			'previous_rent_owed_comments' => $this->getPreviousRentOwedComments(),
			'bankruptcy_comments' => $this->getBankruptcyComments(),
			'returning_resident_comments' => $this->getReturningResidentComments(),
			'unemployed_comments' => $this->getUnemployedComments(),
			'reason_in_us_comments' => $this->getReasonInUsComments(),
			'gaa_screening_initials' => $this->getGaaScreeningInitials(),
			'relationship_to_applicant' => $this->getRelationshipToApplicant(),
			'felony_is_resolved' => $this->getFelonyIsResolved(),
			'smokes' => $this->getSmokes(),
			'has_pets' => $this->getHasPets(),
			'has_felony_conviction' => $this->getHasFelonyConviction(),
			'has_been_evicted' => $this->getHasBeenEvicted(),
			'has_broken_lease' => $this->getHasBrokenLease(),
			'has_bankruptcy' => $this->getHasBankruptcy(),
			'has_public_judgment' => $this->getHasPublicJudgment(),
			'has_renters_insurance' => $this->getHasRentersInsurance(),
			'has_previous_rent_owed' => $this->getHasPreviousRentOwed(),
			'has_middle_name' => $this->getHasMiddleName(),
			'was_sued_for_rent' => $this->getWasSuedForRent(),
			'was_sued_for_damages' => $this->getWasSuedForDamages(),
			'is_alien' => $this->getIsAlien(),
			'is_returning_resident' => $this->getIsReturningResident(),
			'is_unemployed' => $this->getIsUnemployed(),
			'is_legal_us_resident' => $this->getIsLegalUsResident(),
			'felony_year' => $this->getFelonyYear(),
			'bankruptcy_year' => $this->getBankruptcyYear(),
			'public_judgment_year' => $this->getPublicJudgmentYear(),
			'reference_comments' => $this->getReferenceComments(),
			'references_approved_by' => $this->getReferencesApprovedBy(),
			'references_approved_on' => $this->getReferencesApprovedOn(),
			'sms_activated_on' => $this->getSmsActivatedOn(),
			'sms_cancelled_on' => $this->getSmsCancelledOn(),
			'sms_application_updates_activated_on' => $this->getSmsApplicationUpdatesActivatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'has_alt_middle_name' => $this->getHasAltMiddleName(),
			'details' => $this->getDetails(),
			'has_name_last_matronymic' => $this->getHasNameLastMatronymic(),
			'citizenship_country_code' => $this->getCitizenshipCountryCode()
		);
	}

}
?>