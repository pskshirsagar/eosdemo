<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectAmenities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjectAmenities extends CEosPluralBase {

	/**
	 * @return CJobProjectAmenity[]
	 */
	public static function fetchJobProjectAmenities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobProjectAmenity', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobProjectAmenity
	 */
	public static function fetchJobProjectAmenity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobProjectAmenity', $objDatabase );
	}

	public static function fetchJobProjectAmenityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_project_amenities', $objDatabase );
	}

	public static function fetchJobProjectAmenityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobProjectAmenity( sprintf( 'SELECT * FROM job_project_amenities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectAmenitiesByCid( $intCid, $objDatabase ) {
		return self::fetchJobProjectAmenities( sprintf( 'SELECT * FROM job_project_amenities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectAmenitiesByJobProjectIdByCid( $intJobProjectId, $intCid, $objDatabase ) {
		return self::fetchJobProjectAmenities( sprintf( 'SELECT * FROM job_project_amenities WHERE job_project_id = %d AND cid = %d', ( int ) $intJobProjectId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectAmenitiesByAmenityIdByCid( $intAmenityId, $intCid, $objDatabase ) {
		return self::fetchJobProjectAmenities( sprintf( 'SELECT * FROM job_project_amenities WHERE amenity_id = %d AND cid = %d', ( int ) $intAmenityId, ( int ) $intCid ), $objDatabase );
	}

}
?>