<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIpsPropertyMigration extends CEosSingularBase {

	const TABLE_NAME = 'public.ips_property_migrations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScheduledBy;
	protected $m_strScheduledOn;
	protected $m_intInitiatedBy;
	protected $m_strInitiatedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['scheduled_by'] ) && $boolDirectSet ) $this->set( 'm_intScheduledBy', trim( $arrValues['scheduled_by'] ) ); elseif( isset( $arrValues['scheduled_by'] ) ) $this->setScheduledBy( $arrValues['scheduled_by'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['initiated_by'] ) && $boolDirectSet ) $this->set( 'm_intInitiatedBy', trim( $arrValues['initiated_by'] ) ); elseif( isset( $arrValues['initiated_by'] ) ) $this->setInitiatedBy( $arrValues['initiated_by'] );
		if( isset( $arrValues['initiated_on'] ) && $boolDirectSet ) $this->set( 'm_strInitiatedOn', trim( $arrValues['initiated_on'] ) ); elseif( isset( $arrValues['initiated_on'] ) ) $this->setInitiatedOn( $arrValues['initiated_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScheduledBy( $intScheduledBy ) {
		$this->set( 'm_intScheduledBy', CStrings::strToIntDef( $intScheduledBy, NULL, false ) );
	}

	public function getScheduledBy() {
		return $this->m_intScheduledBy;
	}

	public function sqlScheduledBy() {
		return ( true == isset( $this->m_intScheduledBy ) ) ? ( string ) $this->m_intScheduledBy : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setInitiatedBy( $intInitiatedBy ) {
		$this->set( 'm_intInitiatedBy', CStrings::strToIntDef( $intInitiatedBy, NULL, false ) );
	}

	public function getInitiatedBy() {
		return $this->m_intInitiatedBy;
	}

	public function sqlInitiatedBy() {
		return ( true == isset( $this->m_intInitiatedBy ) ) ? ( string ) $this->m_intInitiatedBy : 'NULL';
	}

	public function setInitiatedOn( $strInitiatedOn ) {
		$this->set( 'm_strInitiatedOn', CStrings::strTrimDef( $strInitiatedOn, -1, NULL, true ) );
	}

	public function getInitiatedOn() {
		return $this->m_strInitiatedOn;
	}

	public function sqlInitiatedOn() {
		return ( true == isset( $this->m_strInitiatedOn ) ) ? '\'' . $this->m_strInitiatedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, scheduled_by, scheduled_on, initiated_by, initiated_on, completed_by, completed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlScheduledBy() . ', ' .
						$this->sqlScheduledOn() . ', ' .
						$this->sqlInitiatedBy() . ', ' .
						$this->sqlInitiatedOn() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_by = ' . $this->sqlScheduledBy(). ',' ; } elseif( true == array_key_exists( 'ScheduledBy', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_by = ' . $this->sqlScheduledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy(). ',' ; } elseif( true == array_key_exists( 'InitiatedBy', $this->getChangedColumns() ) ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn(). ',' ; } elseif( true == array_key_exists( 'InitiatedOn', $this->getChangedColumns() ) ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'scheduled_by' => $this->getScheduledBy(),
			'scheduled_on' => $this->getScheduledOn(),
			'initiated_by' => $this->getInitiatedBy(),
			'initiated_on' => $this->getInitiatedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>