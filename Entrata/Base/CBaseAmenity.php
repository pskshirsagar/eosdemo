<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAmenity extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.amenities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultAmenityId;
	protected $m_intAmenityFilterId;
	protected $m_intCategorizationTypeId;
	protected $m_intAmenityTypeId;
	protected $m_intCompanyMediaFileId;
	protected $m_strExternalVideoUrl;
	protected $m_strName;
	protected $m_strInternalDescription;
	protected $m_strExternalDescription;
	protected $m_boolHideExternalDescription;
	protected $m_boolIsCategorizationQueued;
	protected $m_intOrderNum;
	protected $m_intCategorizedBy;
	protected $m_strCategorizedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCategorizationTypeId = '3';
		$this->m_boolHideExternalDescription = false;
		$this->m_boolIsCategorizationQueued = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultAmenityId', trim( $arrValues['default_amenity_id'] ) ); elseif( isset( $arrValues['default_amenity_id'] ) ) $this->setDefaultAmenityId( $arrValues['default_amenity_id'] );
		if( isset( $arrValues['amenity_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityFilterId', trim( $arrValues['amenity_filter_id'] ) ); elseif( isset( $arrValues['amenity_filter_id'] ) ) $this->setAmenityFilterId( $arrValues['amenity_filter_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['amenity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityTypeId', trim( $arrValues['amenity_type_id'] ) ); elseif( isset( $arrValues['amenity_type_id'] ) ) $this->setAmenityTypeId( $arrValues['amenity_type_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['external_video_url'] ) && $boolDirectSet ) $this->set( 'm_strExternalVideoUrl', trim( stripcslashes( $arrValues['external_video_url'] ) ) ); elseif( isset( $arrValues['external_video_url'] ) ) $this->setExternalVideoUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_video_url'] ) : $arrValues['external_video_url'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['internal_description'] ) && $boolDirectSet ) $this->set( 'm_strInternalDescription', trim( stripcslashes( $arrValues['internal_description'] ) ) ); elseif( isset( $arrValues['internal_description'] ) ) $this->setInternalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['internal_description'] ) : $arrValues['internal_description'] );
		if( isset( $arrValues['external_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strExternalDescription', trim( stripcslashes( $arrValues['external_description'] ) ) ); elseif( isset( $arrValues['external_description'] ) ) $this->setExternalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_description'] ) : $arrValues['external_description'] );
		if( isset( $arrValues['hide_external_description'] ) && $boolDirectSet ) $this->set( 'm_boolHideExternalDescription', trim( stripcslashes( $arrValues['hide_external_description'] ) ) ); elseif( isset( $arrValues['hide_external_description'] ) ) $this->setHideExternalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_external_description'] ) : $arrValues['hide_external_description'] );
		if( isset( $arrValues['is_categorization_queued'] ) && $boolDirectSet ) $this->set( 'm_boolIsCategorizationQueued', trim( stripcslashes( $arrValues['is_categorization_queued'] ) ) ); elseif( isset( $arrValues['is_categorization_queued'] ) ) $this->setIsCategorizationQueued( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_categorization_queued'] ) : $arrValues['is_categorization_queued'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['categorized_by'] ) && $boolDirectSet ) $this->set( 'm_intCategorizedBy', trim( $arrValues['categorized_by'] ) ); elseif( isset( $arrValues['categorized_by'] ) ) $this->setCategorizedBy( $arrValues['categorized_by'] );
		if( isset( $arrValues['categorized_on'] ) && $boolDirectSet ) $this->set( 'm_strCategorizedOn', trim( $arrValues['categorized_on'] ) ); elseif( isset( $arrValues['categorized_on'] ) ) $this->setCategorizedOn( $arrValues['categorized_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultAmenityId( $intDefaultAmenityId ) {
		$this->set( 'm_intDefaultAmenityId', CStrings::strToIntDef( $intDefaultAmenityId, NULL, false ) );
	}

	public function getDefaultAmenityId() {
		return $this->m_intDefaultAmenityId;
	}

	public function sqlDefaultAmenityId() {
		return ( true == isset( $this->m_intDefaultAmenityId ) ) ? ( string ) $this->m_intDefaultAmenityId : 'NULL';
	}

	public function setAmenityFilterId( $intAmenityFilterId ) {
		$this->set( 'm_intAmenityFilterId', CStrings::strToIntDef( $intAmenityFilterId, NULL, false ) );
	}

	public function getAmenityFilterId() {
		return $this->m_intAmenityFilterId;
	}

	public function sqlAmenityFilterId() {
		return ( true == isset( $this->m_intAmenityFilterId ) ) ? ( string ) $this->m_intAmenityFilterId : 'NULL';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '3';
	}

	public function setAmenityTypeId( $intAmenityTypeId ) {
		$this->set( 'm_intAmenityTypeId', CStrings::strToIntDef( $intAmenityTypeId, NULL, false ) );
	}

	public function getAmenityTypeId() {
		return $this->m_intAmenityTypeId;
	}

	public function sqlAmenityTypeId() {
		return ( true == isset( $this->m_intAmenityTypeId ) ) ? ( string ) $this->m_intAmenityTypeId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setExternalVideoUrl( $strExternalVideoUrl ) {
		$this->set( 'm_strExternalVideoUrl', CStrings::strTrimDef( $strExternalVideoUrl, 4096, NULL, true ) );
	}

	public function getExternalVideoUrl() {
		return $this->m_strExternalVideoUrl;
	}

	public function sqlExternalVideoUrl() {
		return ( true == isset( $this->m_strExternalVideoUrl ) ) ? '\'' . addslashes( $this->m_strExternalVideoUrl ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setInternalDescription( $strInternalDescription ) {
		$this->set( 'm_strInternalDescription', CStrings::strTrimDef( $strInternalDescription, 512, NULL, true ) );
	}

	public function getInternalDescription() {
		return $this->m_strInternalDescription;
	}

	public function sqlInternalDescription() {
		return ( true == isset( $this->m_strInternalDescription ) ) ? '\'' . addslashes( $this->m_strInternalDescription ) . '\'' : 'NULL';
	}

	public function setExternalDescription( $strExternalDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strExternalDescription', CStrings::strTrimDef( $strExternalDescription, 512, NULL, true ), $strLocaleCode );
	}

	public function getExternalDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strExternalDescription', $strLocaleCode );
	}

	public function sqlExternalDescription() {
		return ( true == isset( $this->m_strExternalDescription ) ) ? '\'' . addslashes( $this->m_strExternalDescription ) . '\'' : 'NULL';
	}

	public function setHideExternalDescription( $boolHideExternalDescription ) {
		$this->set( 'm_boolHideExternalDescription', CStrings::strToBool( $boolHideExternalDescription ) );
	}

	public function getHideExternalDescription() {
		return $this->m_boolHideExternalDescription;
	}

	public function sqlHideExternalDescription() {
		return ( true == isset( $this->m_boolHideExternalDescription ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideExternalDescription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCategorizationQueued( $boolIsCategorizationQueued ) {
		$this->set( 'm_boolIsCategorizationQueued', CStrings::strToBool( $boolIsCategorizationQueued ) );
	}

	public function getIsCategorizationQueued() {
		return $this->m_boolIsCategorizationQueued;
	}

	public function sqlIsCategorizationQueued() {
		return ( true == isset( $this->m_boolIsCategorizationQueued ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCategorizationQueued ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCategorizedBy( $intCategorizedBy ) {
		$this->set( 'm_intCategorizedBy', CStrings::strToIntDef( $intCategorizedBy, NULL, false ) );
	}

	public function getCategorizedBy() {
		return $this->m_intCategorizedBy;
	}

	public function sqlCategorizedBy() {
		return ( true == isset( $this->m_intCategorizedBy ) ) ? ( string ) $this->m_intCategorizedBy : 'NULL';
	}

	public function setCategorizedOn( $strCategorizedOn ) {
		$this->set( 'm_strCategorizedOn', CStrings::strTrimDef( $strCategorizedOn, -1, NULL, true ) );
	}

	public function getCategorizedOn() {
		return $this->m_strCategorizedOn;
	}

	public function sqlCategorizedOn() {
		return ( true == isset( $this->m_strCategorizedOn ) ) ? '\'' . $this->m_strCategorizedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_amenity_id, amenity_filter_id, categorization_type_id, amenity_type_id, company_media_file_id, external_video_url, name, internal_description, external_description, hide_external_description, is_categorization_queued, order_num, categorized_by, categorized_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultAmenityId() . ', ' .
						$this->sqlAmenityFilterId() . ', ' .
						$this->sqlCategorizationTypeId() . ', ' .
						$this->sqlAmenityTypeId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlExternalVideoUrl() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlInternalDescription() . ', ' .
						$this->sqlExternalDescription() . ', ' .
						$this->sqlHideExternalDescription() . ', ' .
						$this->sqlIsCategorizationQueued() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCategorizedBy() . ', ' .
						$this->sqlCategorizedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amenity_id = ' . $this->sqlDefaultAmenityId(). ',' ; } elseif( true == array_key_exists( 'DefaultAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' default_amenity_id = ' . $this->sqlDefaultAmenityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_filter_id = ' . $this->sqlAmenityFilterId(). ',' ; } elseif( true == array_key_exists( 'AmenityFilterId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_filter_id = ' . $this->sqlAmenityFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId(). ',' ; } elseif( true == array_key_exists( 'CategorizationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_type_id = ' . $this->sqlAmenityTypeId(). ',' ; } elseif( true == array_key_exists( 'AmenityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_type_id = ' . $this->sqlAmenityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl(). ',' ; } elseif( true == array_key_exists( 'ExternalVideoUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription(). ',' ; } elseif( true == array_key_exists( 'InternalDescription', $this->getChangedColumns() ) ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_description = ' . $this->sqlExternalDescription(). ',' ; } elseif( true == array_key_exists( 'ExternalDescription', $this->getChangedColumns() ) ) { $strSql .= ' external_description = ' . $this->sqlExternalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_external_description = ' . $this->sqlHideExternalDescription(). ',' ; } elseif( true == array_key_exists( 'HideExternalDescription', $this->getChangedColumns() ) ) { $strSql .= ' hide_external_description = ' . $this->sqlHideExternalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued(). ',' ; } elseif( true == array_key_exists( 'IsCategorizationQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorized_by = ' . $this->sqlCategorizedBy(). ',' ; } elseif( true == array_key_exists( 'CategorizedBy', $this->getChangedColumns() ) ) { $strSql .= ' categorized_by = ' . $this->sqlCategorizedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorized_on = ' . $this->sqlCategorizedOn(). ',' ; } elseif( true == array_key_exists( 'CategorizedOn', $this->getChangedColumns() ) ) { $strSql .= ' categorized_on = ' . $this->sqlCategorizedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_amenity_id' => $this->getDefaultAmenityId(),
			'amenity_filter_id' => $this->getAmenityFilterId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'amenity_type_id' => $this->getAmenityTypeId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'external_video_url' => $this->getExternalVideoUrl(),
			'name' => $this->getName(),
			'internal_description' => $this->getInternalDescription(),
			'external_description' => $this->getExternalDescription(),
			'hide_external_description' => $this->getHideExternalDescription(),
			'is_categorization_queued' => $this->getIsCategorizationQueued(),
			'order_num' => $this->getOrderNum(),
			'categorized_by' => $this->getCategorizedBy(),
			'categorized_on' => $this->getCategorizedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>