<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetLedger extends CEosSingularBase {

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intAssetLedgerTypeId;
	protected $m_intApCodeId;
	protected $m_intAssetId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAssetLedgerTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['asset_ledger_type_id'] ) && $boolDirectSet ) $this->m_intAssetLedgerTypeId = trim( $arrValues['asset_ledger_type_id'] ); else if( isset( $arrValues['asset_ledger_type_id'] ) ) $this->setAssetLedgerTypeId( $arrValues['asset_ledger_type_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->m_intApCodeId = trim( $arrValues['ap_code_id'] ); else if( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['asset_id'] ) && $boolDirectSet ) $this->m_intAssetId = trim( $arrValues['asset_id'] ); else if( isset( $arrValues['asset_id'] ) ) $this->setAssetId( $arrValues['asset_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
	}

	public function setAssetLedgerTypeId( $intAssetLedgerTypeId ) {
		$this->m_intAssetLedgerTypeId = CStrings::strToIntDef( $intAssetLedgerTypeId, NULL, false );
	}

	public function getAssetLedgerTypeId() {
		return $this->m_intAssetLedgerTypeId;
	}

	public function sqlAssetLedgerTypeId() {
		return ( true == isset( $this->m_intAssetLedgerTypeId ) ) ? (string) $this->m_intAssetLedgerTypeId : '1';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = CStrings::strToIntDef( $intApCodeId, NULL, false );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? (string) $this->m_intApCodeId : 'NULL';
	}

	public function setAssetId( $intAssetId ) {
		$this->m_intAssetId = CStrings::strToIntDef( $intAssetId, NULL, false );
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function sqlAssetId() {
		return ( true == isset( $this->m_intAssetId ) ) ? (string) $this->m_intAssetId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.asset_ledgers_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.asset_ledgers
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlAssetLedgerTypeId() . ', ' .
 						$this->sqlApCodeId() . ', ' .
 						$this->sqlAssetId() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.asset_ledgers
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_ledger_type_id = ' . $this->sqlAssetLedgerTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAssetLedgerTypeId() ) != $this->getOriginalValueByFieldName ( 'asset_ledger_type_id' ) ) { $arrstrOriginalValueChanges['asset_ledger_type_id'] = $this->sqlAssetLedgerTypeId(); $strSql .= ' asset_ledger_type_id = ' . $this->sqlAssetLedgerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApCodeId() ) != $this->getOriginalValueByFieldName ( 'ap_code_id' ) ) { $arrstrOriginalValueChanges['ap_code_id'] = $this->sqlApCodeId(); $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAssetId() ) != $this->getOriginalValueByFieldName ( 'asset_id' ) ) { $arrstrOriginalValueChanges['asset_id'] = $this->sqlAssetId(); $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.asset_ledgers WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.asset_ledgers_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'asset_ledger_type_id' => $this->getAssetLedgerTypeId(),
			'ap_code_id' => $this->getApCodeId(),
			'asset_id' => $this->getAssetId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>