<?php

class CBaseRouteRuleReferenceTypes extends CEosPluralBase {

	/**
	 * @return CRouteRuleReferenceType[]
	 */
	public static function fetchRouteRuleReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRouteRuleReferenceType', $objDatabase );
	}

	/**
	 * @return CRouteRuleReferenceType
	 */
	public static function fetchRouteRuleReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRouteRuleReferenceType', $objDatabase );
	}

	public static function fetchRouteRuleReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'route_rule_reference_types', $objDatabase );
	}

	public static function fetchRouteRuleReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchRouteRuleReferenceType( sprintf( 'SELECT * FROM route_rule_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRouteRuleReferenceTypesByRouteRuleReferenceTypeId( $intRouteRuleReferenceTypeId, $objDatabase ) {
		return self::fetchRouteRuleReferenceTypes( sprintf( 'SELECT * FROM route_rule_reference_types WHERE route_rule_reference_type_id = %d', ( int ) $intRouteRuleReferenceTypeId ), $objDatabase );
	}

}
?>