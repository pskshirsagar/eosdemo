<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDetailLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidyDetailLogs extends CEosPluralBase {

	/**
	 * @return CCustomerSubsidyDetailLog[]
	 */
	public static function fetchCustomerSubsidyDetailLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerSubsidyDetailLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSubsidyDetailLog
	 */
	public static function fetchCustomerSubsidyDetailLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerSubsidyDetailLog', $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_subsidy_detail_logs', $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLog( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsByCustomerSubsidyDetailIdByCid( $intCustomerSubsidyDetailId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE customer_subsidy_detail_id = %d AND cid = %d', ( int ) $intCustomerSubsidyDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsBySubsidyCitizenshipTypeIdByCid( $intSubsidyCitizenshipTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE subsidy_citizenship_type_id = %d AND cid = %d', ( int ) $intSubsidyCitizenshipTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsBySubsidyEthnicityTypeIdByCid( $intSubsidyEthnicityTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE subsidy_ethnicity_type_id = %d AND cid = %d', ( int ) $intSubsidyEthnicityTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsBySubsidyEthnicitySubTypeIdByCid( $intSubsidyEthnicitySubTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE subsidy_ethnicity_sub_type_id = %d AND cid = %d', ( int ) $intSubsidyEthnicitySubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsBySubsidySsnExceptionTypeIdByCid( $intSubsidySsnExceptionTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE subsidy_ssn_exception_type_id = %d AND cid = %d', ( int ) $intSubsidySsnExceptionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsBySubsidyDependentTypeIdByCid( $intSubsidyDependentTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE subsidy_dependent_type_id = %d AND cid = %d', ( int ) $intSubsidyDependentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailLogsByPriorCustomerSubsidyDetailLogIdByCid( $intPriorCustomerSubsidyDetailLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetailLogs( sprintf( 'SELECT * FROM customer_subsidy_detail_logs WHERE prior_customer_subsidy_detail_log_id = %d AND cid = %d', ( int ) $intPriorCustomerSubsidyDetailLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>