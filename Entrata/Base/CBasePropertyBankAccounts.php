<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBankAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBankAccounts extends CEosPluralBase {

	/**
	 * @return CPropertyBankAccount[]
	 */
	public static function fetchPropertyBankAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyBankAccount', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyBankAccount
	 */
	public static function fetchPropertyBankAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyBankAccount', $objDatabase );
	}

	public static function fetchPropertyBankAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_bank_accounts', $objDatabase );
	}

	public static function fetchPropertyBankAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccount( sprintf( 'SELECT * FROM property_bank_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccounts( sprintf( 'SELECT * FROM property_bank_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccounts( sprintf( 'SELECT * FROM property_bank_accounts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccounts( sprintf( 'SELECT * FROM property_bank_accounts WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountsByBankApCodeIdByCid( $intBankApCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccounts( sprintf( 'SELECT * FROM property_bank_accounts WHERE bank_ap_code_id = %d AND cid = %d', ( int ) $intBankApCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>