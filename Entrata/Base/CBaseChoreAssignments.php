<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreAssignments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreAssignments extends CEosPluralBase {

	/**
	 * @return CChoreAssignment[]
	 */
	public static function fetchChoreAssignments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CChoreAssignment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChoreAssignment
	 */
	public static function fetchChoreAssignment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChoreAssignment', $objDatabase );
	}

	public static function fetchChoreAssignmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_assignments', $objDatabase );
	}

	public static function fetchChoreAssignmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChoreAssignment( sprintf( 'SELECT * FROM chore_assignments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreAssignmentsByCid( $intCid, $objDatabase ) {
		return self::fetchChoreAssignments( sprintf( 'SELECT * FROM chore_assignments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreAssignmentsByChoreIdByCid( $intChoreId, $intCid, $objDatabase ) {
		return self::fetchChoreAssignments( sprintf( 'SELECT * FROM chore_assignments WHERE chore_id = %d AND cid = %d', ( int ) $intChoreId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreAssignmentsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchChoreAssignments( sprintf( 'SELECT * FROM chore_assignments WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreAssignmentsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchChoreAssignments( sprintf( 'SELECT * FROM chore_assignments WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreAssignmentsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchChoreAssignments( sprintf( 'SELECT * FROM chore_assignments WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>