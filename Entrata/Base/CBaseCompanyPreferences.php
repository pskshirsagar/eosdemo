<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyPreferences extends CEosPluralBase {

	/**
	 * @return CCompanyPreference[]
	 */
	public static function fetchCompanyPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyPreference::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyPreference
	 */
	public static function fetchCompanyPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyPreference::class, $objDatabase );
	}

	public static function fetchCompanyPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_preferences', $objDatabase );
	}

	public static function fetchCompanyPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyPreference( sprintf( 'SELECT * FROM company_preferences WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyPreferences( sprintf( 'SELECT * FROM company_preferences WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>