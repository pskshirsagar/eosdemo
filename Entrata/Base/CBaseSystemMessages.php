<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemMessages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSystemMessages extends CEosPluralBase {

	/**
	 * @return CSystemMessage[]
	 */
	public static function fetchSystemMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSystemMessage::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSystemMessage
	 */
	public static function fetchSystemMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemMessage::class, $objDatabase );
	}

	public static function fetchSystemMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_messages', $objDatabase );
	}

	public static function fetchSystemMessageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSystemMessage( sprintf( 'SELECT * FROM system_messages WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesBySystemMessageTypeIdByCid( $intSystemMessageTypeId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE system_message_type_id = %d AND cid = %d', $intSystemMessageTypeId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesBySystemMessageAudienceIdByCid( $intSystemMessageAudienceId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE system_message_audience_id = %d AND cid = %d', $intSystemMessageAudienceId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesBySystemMessageCategoryIdByCid( $intSystemMessageCategoryId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE system_message_category_id = %d AND cid = %d', $intSystemMessageCategoryId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesBySystemMessageTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE system_message_template_id = %d AND cid = %d', $intSystemMessageTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessagesByDefaultSystemMessageIdByCid( $intDefaultSystemMessageId, $intCid, $objDatabase ) {
		return self::fetchSystemMessages( sprintf( 'SELECT * FROM system_messages WHERE default_system_message_id = %d AND cid = %d', $intDefaultSystemMessageId, $intCid ), $objDatabase );
	}

}
?>