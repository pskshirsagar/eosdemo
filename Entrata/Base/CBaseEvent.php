<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEvent extends CEosSingularBase {

    use TEosDetails;

    const TABLE_NAME = 'public.events';

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intPropertyUnitId;
    protected $m_intUnitSpaceId;
    protected $m_intEventTypeId;
    protected $m_intEventSubTypeId;
    protected $m_intEventResultId;
    protected $m_intDefaultEventResultId;
    protected $m_intAssociatedEventId;
    protected $m_intPsProductId;
    protected $m_intOldStageId;
    protected $m_intNewStageId;
    protected $m_intOldStatusId;
    protected $m_intNewStatusId;
    protected $m_intCompanyEmployeeId;
    protected $m_intDataReferenceId;
    protected $m_intIntegrationResultId;
    protected $m_intLeaseId;
    protected $m_intLeaseIntervalId;
    protected $m_intCustomerId;
    protected $m_intScheduledTaskId;
    protected $m_strRemotePrimaryKey;
    protected $m_strCalendarEventKey;
    protected $m_strScheduledDatetime;
    protected $m_strScheduledEndDatetime;
    protected $m_strEventDatetime;
    protected $m_strEventHandle;
    protected $m_strTitle;
    protected $m_strNotes;
    protected $m_strDetails;
    protected $m_jsonDetails;
    protected $m_strIpAddress;
    protected $m_boolDoNotExport;
    protected $m_boolIsResident;
    protected $m_boolIsDeleted;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;
    protected $m_intOrganizationContractId;
    protected $m_arrstrTags;

    public function __construct() {
        parent::__construct();

        $this->m_boolDoNotExport = false;
        $this->m_boolIsResident = false;
        $this->m_boolIsDeleted = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
        if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
        if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
        if( isset( $arrValues['event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventSubTypeId', trim( $arrValues['event_sub_type_id'] ) ); elseif( isset( $arrValues['event_sub_type_id'] ) ) $this->setEventSubTypeId( $arrValues['event_sub_type_id'] );
        if( isset( $arrValues['event_result_id'] ) && $boolDirectSet ) $this->set( 'm_intEventResultId', trim( $arrValues['event_result_id'] ) ); elseif( isset( $arrValues['event_result_id'] ) ) $this->setEventResultId( $arrValues['event_result_id'] );
        if( isset( $arrValues['default_event_result_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultEventResultId', trim( $arrValues['default_event_result_id'] ) ); elseif( isset( $arrValues['default_event_result_id'] ) ) $this->setDefaultEventResultId( $arrValues['default_event_result_id'] );
        if( isset( $arrValues['associated_event_id'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedEventId', trim( $arrValues['associated_event_id'] ) ); elseif( isset( $arrValues['associated_event_id'] ) ) $this->setAssociatedEventId( $arrValues['associated_event_id'] );
        if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
        if( isset( $arrValues['old_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStageId', trim( $arrValues['old_stage_id'] ) ); elseif( isset( $arrValues['old_stage_id'] ) ) $this->setOldStageId( $arrValues['old_stage_id'] );
        if( isset( $arrValues['new_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStageId', trim( $arrValues['new_stage_id'] ) ); elseif( isset( $arrValues['new_stage_id'] ) ) $this->setNewStageId( $arrValues['new_stage_id'] );
        if( isset( $arrValues['old_status_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStatusId', trim( $arrValues['old_status_id'] ) ); elseif( isset( $arrValues['old_status_id'] ) ) $this->setOldStatusId( $arrValues['old_status_id'] );
        if( isset( $arrValues['new_status_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStatusId', trim( $arrValues['new_status_id'] ) ); elseif( isset( $arrValues['new_status_id'] ) ) $this->setNewStatusId( $arrValues['new_status_id'] );
        if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
        if( isset( $arrValues['data_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intDataReferenceId', trim( $arrValues['data_reference_id'] ) ); elseif( isset( $arrValues['data_reference_id'] ) ) $this->setDataReferenceId( $arrValues['data_reference_id'] );
        if( isset( $arrValues['integration_result_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationResultId', trim( $arrValues['integration_result_id'] ) ); elseif( isset( $arrValues['integration_result_id'] ) ) $this->setIntegrationResultId( $arrValues['integration_result_id'] );
        if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
        if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
        if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
        if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
        if( isset( $arrValues['calendar_event_key'] ) && $boolDirectSet ) $this->set( 'm_strCalendarEventKey', trim( $arrValues['calendar_event_key'] ) ); elseif( isset( $arrValues['calendar_event_key'] ) ) $this->setCalendarEventKey( $arrValues['calendar_event_key'] );
        if( isset( $arrValues['scheduled_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledDatetime', trim( $arrValues['scheduled_datetime'] ) ); elseif( isset( $arrValues['scheduled_datetime'] ) ) $this->setScheduledDatetime( $arrValues['scheduled_datetime'] );
        if( isset( $arrValues['scheduled_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndDatetime', trim( $arrValues['scheduled_end_datetime'] ) ); elseif( isset( $arrValues['scheduled_end_datetime'] ) ) $this->setScheduledEndDatetime( $arrValues['scheduled_end_datetime'] );
        if( isset( $arrValues['event_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEventDatetime', trim( $arrValues['event_datetime'] ) ); elseif( isset( $arrValues['event_datetime'] ) ) $this->setEventDatetime( $arrValues['event_datetime'] );
        if( isset( $arrValues['event_handle'] ) && $boolDirectSet ) $this->set( 'm_strEventHandle', trim( $arrValues['event_handle'] ) ); elseif( isset( $arrValues['event_handle'] ) ) $this->setEventHandle( $arrValues['event_handle'] );
        if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
        if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
        if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
        if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
        if( isset( $arrValues['do_not_export'] ) && $boolDirectSet ) $this->set( 'm_boolDoNotExport', trim( stripcslashes( $arrValues['do_not_export'] ) ) ); elseif( isset( $arrValues['do_not_export'] ) ) $this->setDoNotExport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['do_not_export'] ) : $arrValues['do_not_export'] );
        if( isset( $arrValues['is_resident'] ) && $boolDirectSet ) $this->set( 'm_boolIsResident', trim( stripcslashes( $arrValues['is_resident'] ) ) ); elseif( isset( $arrValues['is_resident'] ) ) $this->setIsResident( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resident'] ) : $arrValues['is_resident'] );
        if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
        if( isset( $arrValues['tags'] ) && $boolDirectSet ) $this->set( 'm_arrstrTags', trim( $arrValues['tags'] ) ); elseif( isset( $arrValues['tags'] ) ) $this->setTags( $arrValues['tags'] );
        $this->m_boolInitialized = true;
    }

    public function setId( $intId ) {
        $this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
    }

    public function setPropertyUnitId( $intPropertyUnitId ) {
        $this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
    }

    public function getPropertyUnitId() {
        return $this->m_intPropertyUnitId;
    }

    public function sqlPropertyUnitId() {
        return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
    }

    public function setUnitSpaceId( $intUnitSpaceId ) {
        $this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
    }

    public function getUnitSpaceId() {
        return $this->m_intUnitSpaceId;
    }

    public function sqlUnitSpaceId() {
        return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
    }

    public function setEventTypeId( $intEventTypeId ) {
        $this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
    }

    public function getEventTypeId() {
        return $this->m_intEventTypeId;
    }

    public function sqlEventTypeId() {
        return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
    }

    public function setEventSubTypeId( $intEventSubTypeId ) {
        $this->set( 'm_intEventSubTypeId', CStrings::strToIntDef( $intEventSubTypeId, NULL, false ) );
    }

    public function getEventSubTypeId() {
        return $this->m_intEventSubTypeId;
    }

    public function sqlEventSubTypeId() {
        return ( true == isset( $this->m_intEventSubTypeId ) ) ? ( string ) $this->m_intEventSubTypeId : 'NULL';
    }

    public function setEventResultId( $intEventResultId ) {
        $this->set( 'm_intEventResultId', CStrings::strToIntDef( $intEventResultId, NULL, false ) );
    }

    public function getEventResultId() {
        return $this->m_intEventResultId;
    }

    public function sqlEventResultId() {
        return ( true == isset( $this->m_intEventResultId ) ) ? ( string ) $this->m_intEventResultId : 'NULL';
    }

    public function setDefaultEventResultId( $intDefaultEventResultId ) {
        $this->set( 'm_intDefaultEventResultId', CStrings::strToIntDef( $intDefaultEventResultId, NULL, false ) );
    }

    public function getDefaultEventResultId() {
        return $this->m_intDefaultEventResultId;
    }

    public function sqlDefaultEventResultId() {
        return ( true == isset( $this->m_intDefaultEventResultId ) ) ? ( string ) $this->m_intDefaultEventResultId : 'NULL';
    }

    public function setAssociatedEventId( $intAssociatedEventId ) {
        $this->set( 'm_intAssociatedEventId', CStrings::strToIntDef( $intAssociatedEventId, NULL, false ) );
    }

    public function getAssociatedEventId() {
        return $this->m_intAssociatedEventId;
    }

    public function sqlAssociatedEventId() {
        return ( true == isset( $this->m_intAssociatedEventId ) ) ? ( string ) $this->m_intAssociatedEventId : 'NULL';
    }

    public function setPsProductId( $intPsProductId ) {
        $this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
    }

    public function getPsProductId() {
        return $this->m_intPsProductId;
    }

    public function sqlPsProductId() {
        return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
    }

    public function setOldStageId( $intOldStageId ) {
        $this->set( 'm_intOldStageId', CStrings::strToIntDef( $intOldStageId, NULL, false ) );
    }

    public function getOldStageId() {
        return $this->m_intOldStageId;
    }

    public function sqlOldStageId() {
        return ( true == isset( $this->m_intOldStageId ) ) ? ( string ) $this->m_intOldStageId : 'NULL';
    }

    public function setNewStageId( $intNewStageId ) {
        $this->set( 'm_intNewStageId', CStrings::strToIntDef( $intNewStageId, NULL, false ) );
    }

    public function getNewStageId() {
        return $this->m_intNewStageId;
    }

    public function sqlNewStageId() {
        return ( true == isset( $this->m_intNewStageId ) ) ? ( string ) $this->m_intNewStageId : 'NULL';
    }

    public function setOldStatusId( $intOldStatusId ) {
        $this->set( 'm_intOldStatusId', CStrings::strToIntDef( $intOldStatusId, NULL, false ) );
    }

    public function getOldStatusId() {
        return $this->m_intOldStatusId;
    }

    public function sqlOldStatusId() {
        return ( true == isset( $this->m_intOldStatusId ) ) ? ( string ) $this->m_intOldStatusId : 'NULL';
    }

    public function setNewStatusId( $intNewStatusId ) {
        $this->set( 'm_intNewStatusId', CStrings::strToIntDef( $intNewStatusId, NULL, false ) );
    }

    public function getNewStatusId() {
        return $this->m_intNewStatusId;
    }

    public function sqlNewStatusId() {
        return ( true == isset( $this->m_intNewStatusId ) ) ? ( string ) $this->m_intNewStatusId : 'NULL';
    }

    public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
        $this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
    }

    public function getCompanyEmployeeId() {
        return $this->m_intCompanyEmployeeId;
    }

    public function sqlCompanyEmployeeId() {
        return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
    }

    public function setDataReferenceId( $intDataReferenceId ) {
        $this->set( 'm_intDataReferenceId', CStrings::strToIntDef( $intDataReferenceId, NULL, false ) );
    }

    public function getDataReferenceId() {
        return $this->m_intDataReferenceId;
    }

    public function sqlDataReferenceId() {
        return ( true == isset( $this->m_intDataReferenceId ) ) ? ( string ) $this->m_intDataReferenceId : 'NULL';
    }

    public function setIntegrationResultId( $intIntegrationResultId ) {
        $this->set( 'm_intIntegrationResultId', CStrings::strToIntDef( $intIntegrationResultId, NULL, false ) );
    }

    public function getIntegrationResultId() {
        return $this->m_intIntegrationResultId;
    }

    public function sqlIntegrationResultId() {
        return ( true == isset( $this->m_intIntegrationResultId ) ) ? ( string ) $this->m_intIntegrationResultId : 'NULL';
    }

    public function setLeaseId( $intLeaseId ) {
        $this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
    }

    public function getLeaseId() {
        return $this->m_intLeaseId;
    }

    public function sqlLeaseId() {
        return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
    }

    public function setLeaseIntervalId( $intLeaseIntervalId ) {
        $this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
    }

    public function getLeaseIntervalId() {
        return $this->m_intLeaseIntervalId;
    }

    public function sqlLeaseIntervalId() {
        return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
    }

    public function setCustomerId( $intCustomerId ) {
        $this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
    }

    public function getCustomerId() {
        return $this->m_intCustomerId;
    }

    public function sqlCustomerId() {
        return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
    }

    public function setScheduledTaskId( $intScheduledTaskId ) {
        $this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
    }

    public function getScheduledTaskId() {
        return $this->m_intScheduledTaskId;
    }

    public function sqlScheduledTaskId() {
        return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 40, NULL, true ) );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
    }

    public function setCalendarEventKey( $strCalendarEventKey ) {
        $this->set( 'm_strCalendarEventKey', CStrings::strTrimDef( $strCalendarEventKey, 240, NULL, true ) );
    }

    public function getCalendarEventKey() {
        return $this->m_strCalendarEventKey;
    }

    public function sqlCalendarEventKey() {
        return ( true == isset( $this->m_strCalendarEventKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCalendarEventKey ) : '\'' . addslashes( $this->m_strCalendarEventKey ) . '\'' ) : 'NULL';
    }

    public function setScheduledDatetime( $strScheduledDatetime ) {
        $this->set( 'm_strScheduledDatetime', CStrings::strTrimDef( $strScheduledDatetime, -1, NULL, true ) );
    }

    public function getScheduledDatetime() {
        return $this->m_strScheduledDatetime;
    }

    public function sqlScheduledDatetime() {
        return ( true == isset( $this->m_strScheduledDatetime ) ) ? '\'' . $this->m_strScheduledDatetime . '\'' : 'NOW()';
    }

    public function setScheduledEndDatetime( $strScheduledEndDatetime ) {
        $this->set( 'm_strScheduledEndDatetime', CStrings::strTrimDef( $strScheduledEndDatetime, -1, NULL, true ) );
    }

    public function getScheduledEndDatetime() {
        return $this->m_strScheduledEndDatetime;
    }

    public function sqlScheduledEndDatetime() {
        return ( true == isset( $this->m_strScheduledEndDatetime ) ) ? '\'' . $this->m_strScheduledEndDatetime . '\'' : 'NULL';
    }

    public function setEventDatetime( $strEventDatetime ) {
        $this->set( 'm_strEventDatetime', CStrings::strTrimDef( $strEventDatetime, -1, NULL, true ) );
    }

    public function getEventDatetime() {
        return $this->m_strEventDatetime;
    }

    public function sqlEventDatetime() {
        return ( true == isset( $this->m_strEventDatetime ) ) ? '\'' . $this->m_strEventDatetime . '\'' : 'NOW()';
    }

    public function setEventHandle( $strEventHandle ) {
        $this->set( 'm_strEventHandle', CStrings::strTrimDef( $strEventHandle, -1, NULL, true ) );
    }

    public function getEventHandle() {
        return $this->m_strEventHandle;
    }

    public function sqlEventHandle() {
        return ( true == isset( $this->m_strEventHandle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEventHandle ) : '\'' . addslashes( $this->m_strEventHandle ) . '\'' ) : 'NULL';
    }

    public function setTitle( $strTitle ) {
        $this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
    }

    public function getTitle() {
        return $this->m_strTitle;
    }

    public function sqlTitle() {
        return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
    }

    public function setNotes( $strNotes ) {
        $this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
    }

    public function getNotes() {
        return $this->m_strNotes;
    }

    public function sqlNotes() {
        return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
    }

    public function setIpAddress( $strIpAddress ) {
        $this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 50, NULL, true ) );
    }

    public function getIpAddress() {
        return $this->m_strIpAddress;
    }

    public function sqlIpAddress() {
        return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
    }

    public function setDoNotExport( $boolDoNotExport ) {
        $this->set( 'm_boolDoNotExport', CStrings::strToBool( $boolDoNotExport ) );
    }

    public function getDoNotExport() {
        return $this->m_boolDoNotExport;
    }

    public function sqlDoNotExport() {
        return ( true == isset( $this->m_boolDoNotExport ) ) ? '\'' . ( true == ( bool ) $this->m_boolDoNotExport ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsResident( $boolIsResident ) {
        $this->set( 'm_boolIsResident', CStrings::strToBool( $boolIsResident ) );
    }

    public function getIsResident() {
        return $this->m_boolIsResident;
    }

    public function sqlIsResident() {
        return ( true == isset( $this->m_boolIsResident ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResident ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsDeleted( $boolIsDeleted ) {
        $this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
    }

    public function getIsDeleted() {
        return $this->m_boolIsDeleted;
    }

    public function sqlIsDeleted() {
        return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function setOrganizationContractId( $intOrganizationContractId ) {
        $this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
    }

    public function getOrganizationContractId() {
        return $this->m_intOrganizationContractId;
    }

    public function sqlOrganizationContractId() {
        return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
    }

    public function setTags( $arrstrTags ) {
        $this->set( 'm_arrstrTags', CStrings::strToArrIntDef( $arrstrTags, NULL ) );
    }

    public function getTags() {
        return $this->m_arrstrTags;
    }

    public function sqlTags() {
        return ( true == isset( $this->m_arrstrTags ) && true == valArr( $this->m_arrstrTags ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrTags, NULL ) . '\'' : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

        $strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, unit_space_id, event_type_id, event_sub_type_id, event_result_id, default_event_result_id, associated_event_id, ps_product_id, old_stage_id, new_stage_id, old_status_id, new_status_id, company_employee_id, data_reference_id, integration_result_id, lease_id, lease_interval_id, customer_id, scheduled_task_id, remote_primary_key, calendar_event_key, scheduled_datetime, scheduled_end_datetime, event_datetime, event_handle, title, notes, details, ip_address, do_not_export, is_resident, is_deleted, updated_by, updated_on, created_by, created_on, organization_contract_id, tags )
					VALUES ( ' .
            $strId . ', ' .
            $this->sqlCid() . ', ' .
            $this->sqlPropertyId() . ', ' .
            $this->sqlPropertyUnitId() . ', ' .
            $this->sqlUnitSpaceId() . ', ' .
            $this->sqlEventTypeId() . ', ' .
            $this->sqlEventSubTypeId() . ', ' .
            $this->sqlEventResultId() . ', ' .
            $this->sqlDefaultEventResultId() . ', ' .
            $this->sqlAssociatedEventId() . ', ' .
            $this->sqlPsProductId() . ', ' .
            $this->sqlOldStageId() . ', ' .
            $this->sqlNewStageId() . ', ' .
            $this->sqlOldStatusId() . ', ' .
            $this->sqlNewStatusId() . ', ' .
            $this->sqlCompanyEmployeeId() . ', ' .
            $this->sqlDataReferenceId() . ', ' .
            $this->sqlIntegrationResultId() . ', ' .
            $this->sqlLeaseId() . ', ' .
            $this->sqlLeaseIntervalId() . ', ' .
            $this->sqlCustomerId() . ', ' .
            $this->sqlScheduledTaskId() . ', ' .
            $this->sqlRemotePrimaryKey() . ', ' .
            $this->sqlCalendarEventKey() . ', ' .
            $this->sqlScheduledDatetime() . ', ' .
            $this->sqlScheduledEndDatetime() . ', ' .
            $this->sqlEventDatetime() . ', ' .
            $this->sqlEventHandle() . ', ' .
            $this->sqlTitle() . ', ' .
            $this->sqlNotes() . ', ' .
            $this->sqlDetails() . ', ' .
            $this->sqlIpAddress() . ', ' .
            $this->sqlDoNotExport() . ', ' .
            $this->sqlIsResident() . ', ' .
            $this->sqlIsDeleted() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlUpdatedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlCreatedOn() . ', ' .
            $this->sqlOrganizationContractId() . ', ' .
            $this->sqlTags() . ' ) ' . ' RETURNING id;';
//show($strSql);
        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        if( false == $this->getAllowDifferentialUpdate() ) {
            $boolUpdate = true;
        } else {
            $boolUpdate = false;
        }

        $strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId(). ',' ; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId(). ',' ; } elseif( true == array_key_exists( 'EventSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_result_id = ' . $this->sqlEventResultId(). ',' ; } elseif( true == array_key_exists( 'EventResultId', $this->getChangedColumns() ) ) { $strSql .= ' event_result_id = ' . $this->sqlEventResultId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_event_result_id = ' . $this->sqlDefaultEventResultId(). ',' ; } elseif( true == array_key_exists( 'DefaultEventResultId', $this->getChangedColumns() ) ) { $strSql .= ' default_event_result_id = ' . $this->sqlDefaultEventResultId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_event_id = ' . $this->sqlAssociatedEventId(). ',' ; } elseif( true == array_key_exists( 'AssociatedEventId', $this->getChangedColumns() ) ) { $strSql .= ' associated_event_id = ' . $this->sqlAssociatedEventId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_stage_id = ' . $this->sqlOldStageId(). ',' ; } elseif( true == array_key_exists( 'OldStageId', $this->getChangedColumns() ) ) { $strSql .= ' old_stage_id = ' . $this->sqlOldStageId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_stage_id = ' . $this->sqlNewStageId(). ',' ; } elseif( true == array_key_exists( 'NewStageId', $this->getChangedColumns() ) ) { $strSql .= ' new_stage_id = ' . $this->sqlNewStageId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId(). ',' ; } elseif( true == array_key_exists( 'OldStatusId', $this->getChangedColumns() ) ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId(). ',' ; } elseif( true == array_key_exists( 'NewStatusId', $this->getChangedColumns() ) ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_reference_id = ' . $this->sqlDataReferenceId(). ',' ; } elseif( true == array_key_exists( 'DataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' data_reference_id = ' . $this->sqlDataReferenceId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId(). ',' ; } elseif( true == array_key_exists( 'IntegrationResultId', $this->getChangedColumns() ) ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_event_key = ' . $this->sqlCalendarEventKey(). ',' ; } elseif( true == array_key_exists( 'CalendarEventKey', $this->getChangedColumns() ) ) { $strSql .= ' calendar_event_key = ' . $this->sqlCalendarEventKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime(). ',' ; } elseif( true == array_key_exists( 'EventDatetime', $this->getChangedColumns() ) ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_handle = ' . $this->sqlEventHandle(). ',' ; } elseif( true == array_key_exists( 'EventHandle', $this->getChangedColumns() ) ) { $strSql .= ' event_handle = ' . $this->sqlEventHandle() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_export = ' . $this->sqlDoNotExport(). ',' ; } elseif( true == array_key_exists( 'DoNotExport', $this->getChangedColumns() ) ) { $strSql .= ' do_not_export = ' . $this->sqlDoNotExport() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident = ' . $this->sqlIsResident(). ',' ; } elseif( true == array_key_exists( 'IsResident', $this->getChangedColumns() ) ) { $strSql .= ' is_resident = ' . $this->sqlIsResident() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tags = ' . $this->sqlTags(). ',' ; } elseif( true == array_key_exists( 'Tags', $this->getChangedColumns() ) ) { $strSql .= ' tags = ' . $this->sqlTags() . ','; $boolUpdate = true; }
        $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
        $strSql .= ' updated_on = \'NOW()\' ';

        $strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
            return ( true == $boolUpdate ) ? $strSql : false;
        } else {
            if( true == $boolUpdate ) {
                if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
                    if( true == $this->getAllowDifferentialUpdate() ) {
                        $this->resetChangedColumns();
                        return true;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'cid' => $this->getCid(),
            'property_id' => $this->getPropertyId(),
            'property_unit_id' => $this->getPropertyUnitId(),
            'unit_space_id' => $this->getUnitSpaceId(),
            'event_type_id' => $this->getEventTypeId(),
            'event_sub_type_id' => $this->getEventSubTypeId(),
            'event_result_id' => $this->getEventResultId(),
            'default_event_result_id' => $this->getDefaultEventResultId(),
            'associated_event_id' => $this->getAssociatedEventId(),
            'ps_product_id' => $this->getPsProductId(),
            'old_stage_id' => $this->getOldStageId(),
            'new_stage_id' => $this->getNewStageId(),
            'old_status_id' => $this->getOldStatusId(),
            'new_status_id' => $this->getNewStatusId(),
            'company_employee_id' => $this->getCompanyEmployeeId(),
            'data_reference_id' => $this->getDataReferenceId(),
            'integration_result_id' => $this->getIntegrationResultId(),
            'lease_id' => $this->getLeaseId(),
            'lease_interval_id' => $this->getLeaseIntervalId(),
            'customer_id' => $this->getCustomerId(),
            'scheduled_task_id' => $this->getScheduledTaskId(),
            'remote_primary_key' => $this->getRemotePrimaryKey(),
            'calendar_event_key' => $this->getCalendarEventKey(),
            'scheduled_datetime' => $this->getScheduledDatetime(),
            'scheduled_end_datetime' => $this->getScheduledEndDatetime(),
            'event_datetime' => $this->getEventDatetime(),
            'event_handle' => $this->getEventHandle(),
            'title' => $this->getTitle(),
            'notes' => $this->getNotes(),
            'details' => $this->getDetails(),
            'ip_address' => $this->getIpAddress(),
            'do_not_export' => $this->getDoNotExport(),
            'is_resident' => $this->getIsResident(),
            'is_deleted' => $this->getIsDeleted(),
            'updated_by' => $this->getUpdatedBy(),
            'updated_on' => $this->getUpdatedOn(),
            'created_by' => $this->getCreatedBy(),
            'created_on' => $this->getCreatedOn(),
            'organization_contract_id' => $this->getOrganizationContractId(),
            'tags' => $this->getTags()
        );
    }

}
?>