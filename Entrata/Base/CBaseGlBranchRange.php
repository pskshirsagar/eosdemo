<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlBranchRange extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_branch_ranges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlTreeId;
	protected $m_intGlBranchId;
	protected $m_strStartNumberRange;
	protected $m_strEndNumberRange;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intGlGroupId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeId', trim( $arrValues['gl_tree_id'] ) ); elseif( isset( $arrValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrValues['gl_tree_id'] );
		if( isset( $arrValues['gl_branch_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBranchId', trim( $arrValues['gl_branch_id'] ) ); elseif( isset( $arrValues['gl_branch_id'] ) ) $this->setGlBranchId( $arrValues['gl_branch_id'] );
		if( isset( $arrValues['start_number_range'] ) && $boolDirectSet ) $this->set( 'm_strStartNumberRange', trim( $arrValues['start_number_range'] ) ); elseif( isset( $arrValues['start_number_range'] ) ) $this->setStartNumberRange( $arrValues['start_number_range'] );
		if( isset( $arrValues['end_number_range'] ) && $boolDirectSet ) $this->set( 'm_strEndNumberRange', trim( $arrValues['end_number_range'] ) ); elseif( isset( $arrValues['end_number_range'] ) ) $this->setEndNumberRange( $arrValues['end_number_range'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupId', trim( $arrValues['gl_group_id'] ) ); elseif( isset( $arrValues['gl_group_id'] ) ) $this->setGlGroupId( $arrValues['gl_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlTreeId( $intGlTreeId ) {
		$this->set( 'm_intGlTreeId', CStrings::strToIntDef( $intGlTreeId, NULL, false ) );
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function sqlGlTreeId() {
		return ( true == isset( $this->m_intGlTreeId ) ) ? ( string ) $this->m_intGlTreeId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlBranchId( $intGlBranchId ) {
		$this->set( 'm_intGlBranchId', CStrings::strToIntDef( $intGlBranchId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlBranchId() {
		return $this->m_intGlBranchId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlBranchId() {
		return ( true == isset( $this->m_intGlBranchId ) ) ? ( string ) $this->m_intGlBranchId : 'NULL';
	}

	public function setStartNumberRange( $strStartNumberRange ) {
		$this->set( 'm_strStartNumberRange', CStrings::strTrimDef( $strStartNumberRange, 16, NULL, true ) );
	}

	public function getStartNumberRange() {
		return $this->m_strStartNumberRange;
	}

	public function sqlStartNumberRange() {
		return ( true == isset( $this->m_strStartNumberRange ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStartNumberRange ) : '\'' . addslashes( $this->m_strStartNumberRange ) . '\'' ) : 'NULL';
	}

	public function setEndNumberRange( $strEndNumberRange ) {
		$this->set( 'm_strEndNumberRange', CStrings::strTrimDef( $strEndNumberRange, 16, NULL, true ) );
	}

	public function getEndNumberRange() {
		return $this->m_strEndNumberRange;
	}

	public function sqlEndNumberRange() {
		return ( true == isset( $this->m_strEndNumberRange ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEndNumberRange ) : '\'' . addslashes( $this->m_strEndNumberRange ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGlGroupId( $intGlGroupId ) {
		$this->set( 'm_intGlGroupId', CStrings::strToIntDef( $intGlGroupId, NULL, false ) );
	}

	public function getGlGroupId() {
		return $this->m_intGlGroupId;
	}

	public function sqlGlGroupId() {
		return ( true == isset( $this->m_intGlGroupId ) ) ? ( string ) $this->m_intGlGroupId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_tree_id, gl_branch_id, start_number_range, end_number_range, updated_by, updated_on, created_by, created_on, gl_group_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlTreeId() . ', ' .
						$this->sqlGlBranchId() . ', ' .
						$this->sqlStartNumberRange() . ', ' .
						$this->sqlEndNumberRange() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlGlGroupId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId(). ',' ; } elseif( true == array_key_exists( 'GlBranchId', $this->getChangedColumns() ) ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_number_range = ' . $this->sqlStartNumberRange(). ',' ; } elseif( true == array_key_exists( 'StartNumberRange', $this->getChangedColumns() ) ) { $strSql .= ' start_number_range = ' . $this->sqlStartNumberRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_number_range = ' . $this->sqlEndNumberRange(). ',' ; } elseif( true == array_key_exists( 'EndNumberRange', $this->getChangedColumns() ) ) { $strSql .= ' end_number_range = ' . $this->sqlEndNumberRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId(). ',' ; } elseif( true == array_key_exists( 'GlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_tree_id' => $this->getGlTreeId(),
			'gl_branch_id' => $this->getGlBranchId(),
			'start_number_range' => $this->getStartNumberRange(),
			'end_number_range' => $this->getEndNumberRange(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'gl_group_id' => $this->getGlGroupId()
		);
	}

}
?>