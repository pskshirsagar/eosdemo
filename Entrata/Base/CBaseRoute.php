<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoute extends CEosSingularBase {

	const TABLE_NAME = 'public.routes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRouteTypeId;
	protected $m_strName;
	protected $m_boolAllowNotesOnApproval;
	protected $m_boolRequireNotesOnRejectedReturned;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOldRouteId;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowNotesOnApproval = false;
		$this->m_boolRequireNotesOnRejectedReturned = false;
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['route_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteTypeId', trim( $arrValues['route_type_id'] ) ); elseif( isset( $arrValues['route_type_id'] ) ) $this->setRouteTypeId( $arrValues['route_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['allow_notes_on_approval'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotesOnApproval', trim( stripcslashes( $arrValues['allow_notes_on_approval'] ) ) ); elseif( isset( $arrValues['allow_notes_on_approval'] ) ) $this->setAllowNotesOnApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_notes_on_approval'] ) : $arrValues['allow_notes_on_approval'] );
		if( isset( $arrValues['require_notes_on_rejected_returned'] ) && $boolDirectSet ) $this->set( 'm_boolRequireNotesOnRejectedReturned', trim( stripcslashes( $arrValues['require_notes_on_rejected_returned'] ) ) ); elseif( isset( $arrValues['require_notes_on_rejected_returned'] ) ) $this->setRequireNotesOnRejectedReturned( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_notes_on_rejected_returned'] ) : $arrValues['require_notes_on_rejected_returned'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['old_route_id'] ) && $boolDirectSet ) $this->set( 'm_intOldRouteId', trim( $arrValues['old_route_id'] ) ); elseif( isset( $arrValues['old_route_id'] ) ) $this->setOldRouteId( $arrValues['old_route_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRouteTypeId( $intRouteTypeId ) {
		$this->set( 'm_intRouteTypeId', CStrings::strToIntDef( $intRouteTypeId, NULL, false ) );
	}

	public function getRouteTypeId() {
		return $this->m_intRouteTypeId;
	}

	public function sqlRouteTypeId() {
		return ( true == isset( $this->m_intRouteTypeId ) ) ? ( string ) $this->m_intRouteTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAllowNotesOnApproval( $boolAllowNotesOnApproval ) {
		$this->set( 'm_boolAllowNotesOnApproval', CStrings::strToBool( $boolAllowNotesOnApproval ) );
	}

	public function getAllowNotesOnApproval() {
		return $this->m_boolAllowNotesOnApproval;
	}

	public function sqlAllowNotesOnApproval() {
		return ( true == isset( $this->m_boolAllowNotesOnApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotesOnApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireNotesOnRejectedReturned( $boolRequireNotesOnRejectedReturned ) {
		$this->set( 'm_boolRequireNotesOnRejectedReturned', CStrings::strToBool( $boolRequireNotesOnRejectedReturned ) );
	}

	public function getRequireNotesOnRejectedReturned() {
		return $this->m_boolRequireNotesOnRejectedReturned;
	}

	public function sqlRequireNotesOnRejectedReturned() {
		return ( true == isset( $this->m_boolRequireNotesOnRejectedReturned ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireNotesOnRejectedReturned ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOldRouteId( $intOldRouteId ) {
		$this->set( 'm_intOldRouteId', CStrings::strToIntDef( $intOldRouteId, NULL, false ) );
	}

	public function getOldRouteId() {
		return $this->m_intOldRouteId;
	}

	public function sqlOldRouteId() {
		return ( true == isset( $this->m_intOldRouteId ) ) ? ( string ) $this->m_intOldRouteId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, route_type_id, name, allow_notes_on_approval, require_notes_on_rejected_returned, is_published, updated_by, updated_on, created_by, created_on, old_route_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRouteTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlAllowNotesOnApproval() . ', ' .
 						$this->sqlRequireNotesOnRejectedReturned() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlOldRouteId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; } elseif( true == array_key_exists( 'RouteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_notes_on_approval = ' . $this->sqlAllowNotesOnApproval() . ','; } elseif( true == array_key_exists( 'AllowNotesOnApproval', $this->getChangedColumns() ) ) { $strSql .= ' allow_notes_on_approval = ' . $this->sqlAllowNotesOnApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_notes_on_rejected_returned = ' . $this->sqlRequireNotesOnRejectedReturned() . ','; } elseif( true == array_key_exists( 'RequireNotesOnRejectedReturned', $this->getChangedColumns() ) ) { $strSql .= ' require_notes_on_rejected_returned = ' . $this->sqlRequireNotesOnRejectedReturned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_route_id = ' . $this->sqlOldRouteId() . ','; } elseif( true == array_key_exists( 'OldRouteId', $this->getChangedColumns() ) ) { $strSql .= ' old_route_id = ' . $this->sqlOldRouteId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'route_type_id' => $this->getRouteTypeId(),
			'name' => $this->getName(),
			'allow_notes_on_approval' => $this->getAllowNotesOnApproval(),
			'require_notes_on_rejected_returned' => $this->getRequireNotesOnRejectedReturned(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'old_route_id' => $this->getOldRouteId()
		);
	}

}
?>