<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionFilters extends CEosPluralBase {

	/**
	 * @return CInspectionFilter[]
	 */
	public static function fetchInspectionFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInspectionFilter::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionFilter
	 */
	public static function fetchInspectionFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspectionFilter::class, $objDatabase );
	}

	public static function fetchInspectionFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_filters', $objDatabase );
	}

	public static function fetchInspectionFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionFilter( sprintf( 'SELECT * FROM inspection_filters WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionFilters( sprintf( 'SELECT * FROM inspection_filters WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInspectionFiltersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchInspectionFilters( sprintf( 'SELECT * FROM inspection_filters WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>