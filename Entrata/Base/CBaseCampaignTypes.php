<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCampaignTypes
 * Do not add any new functions to this class.
 */

class CBaseCampaignTypes extends CEosPluralBase {

	/**
	 * @return CCampaignType[]
	 */
	public static function fetchCampaignTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCampaignType::class, $objDatabase );
	}

	/**
	 * @return CCampaignType
	 */
	public static function fetchCampaignType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCampaignType::class, $objDatabase );
	}

	public static function fetchCampaignTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'campaign_types', $objDatabase );
	}

	public static function fetchCampaignTypeById( $intId, $objDatabase ) {
		return self::fetchCampaignType( sprintf( 'SELECT * FROM campaign_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>