<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyTaxCreditSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyTaxCreditSettings extends CEosPluralBase {

	/**
	 * @return CSubsidyTaxCreditSetting[]
	 */
	public static function fetchSubsidyTaxCreditSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyTaxCreditSetting::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyTaxCreditSetting
	 */
	public static function fetchSubsidyTaxCreditSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyTaxCreditSetting::class, $objDatabase );
	}

	public static function fetchSubsidyTaxCreditSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_tax_credit_settings', $objDatabase );
	}

	public static function fetchSubsidyTaxCreditSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyTaxCreditSetting( sprintf( 'SELECT * FROM subsidy_tax_credit_settings WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyTaxCreditSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyTaxCreditSettings( sprintf( 'SELECT * FROM subsidy_tax_credit_settings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSubsidyTaxCreditSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyTaxCreditSettings( sprintf( 'SELECT * FROM subsidy_tax_credit_settings WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyTaxCreditSettingsByRentAssistanceArCodeIdByCid( $intRentAssistanceArCodeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyTaxCreditSettings( sprintf( 'SELECT * FROM subsidy_tax_credit_settings WHERE rent_assistance_ar_code_id = %d AND cid = %d', $intRentAssistanceArCodeId, $intCid ), $objDatabase );
	}

}
?>