<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOptimalRents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueOptimalRents extends CEosPluralBase {

	/**
	 * @return CRevenueOptimalRent[]
	 */
	public static function fetchRevenueOptimalRents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueOptimalRent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueOptimalRent
	 */
	public static function fetchRevenueOptimalRent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueOptimalRent', $objDatabase );
	}

	public static function fetchRevenueOptimalRentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_optimal_rents', $objDatabase );
	}

	public static function fetchRevenueOptimalRentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRent( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRents( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRents( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRents( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRents( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRents( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>