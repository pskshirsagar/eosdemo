<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteSettingLog extends CEosSingularBase {

	const TABLE_NAME = 'public.website_setting_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteSettingKeyId;
	protected $m_intWebsiteId;
	protected $m_intReferenceId;
	protected $m_strAction;
	protected $m_strUpdatedValue;
	protected $m_strOptionLabel;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_setting_key_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteSettingKeyId', trim( $arrValues['website_setting_key_id'] ) ); elseif( isset( $arrValues['website_setting_key_id'] ) ) $this->setWebsiteSettingKeyId( $arrValues['website_setting_key_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['updated_value'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedValue', trim( stripcslashes( $arrValues['updated_value'] ) ) ); elseif( isset( $arrValues['updated_value'] ) ) $this->setUpdatedValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['updated_value'] ) : $arrValues['updated_value'] );
		if( isset( $arrValues['option_label'] ) && $boolDirectSet ) $this->set( 'm_strOptionLabel', trim( stripcslashes( $arrValues['option_label'] ) ) ); elseif( isset( $arrValues['option_label'] ) ) $this->setOptionLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['option_label'] ) : $arrValues['option_label'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteSettingKeyId( $intWebsiteSettingKeyId ) {
		$this->set( 'm_intWebsiteSettingKeyId', CStrings::strToIntDef( $intWebsiteSettingKeyId, NULL, false ) );
	}

	public function getWebsiteSettingKeyId() {
		return $this->m_intWebsiteSettingKeyId;
	}

	public function sqlWebsiteSettingKeyId() {
		return ( true == isset( $this->m_intWebsiteSettingKeyId ) ) ? ( string ) $this->m_intWebsiteSettingKeyId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setUpdatedValue( $strUpdatedValue ) {
		$this->set( 'm_strUpdatedValue', CStrings::strTrimDef( $strUpdatedValue, -1, NULL, true ) );
	}

	public function getUpdatedValue() {
		return $this->m_strUpdatedValue;
	}

	public function sqlUpdatedValue() {
		return ( true == isset( $this->m_strUpdatedValue ) ) ? '\'' . addslashes( $this->m_strUpdatedValue ) . '\'' : 'NULL';
	}

	public function setOptionLabel( $strOptionLabel ) {
		$this->set( 'm_strOptionLabel', CStrings::strTrimDef( $strOptionLabel, -1, NULL, true ) );
	}

	public function getOptionLabel() {
		return $this->m_strOptionLabel;
	}

	public function sqlOptionLabel() {
		return ( true == isset( $this->m_strOptionLabel ) ) ? '\'' . addslashes( $this->m_strOptionLabel ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_setting_key_id, website_id, reference_id, action, updated_value, option_label, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWebsiteSettingKeyId() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlAction() . ', ' .
 						$this->sqlUpdatedValue() . ', ' .
 						$this->sqlOptionLabel() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_setting_key_id = ' . $this->sqlWebsiteSettingKeyId() . ','; } elseif( true == array_key_exists( 'WebsiteSettingKeyId', $this->getChangedColumns() ) ) { $strSql .= ' website_setting_key_id = ' . $this->sqlWebsiteSettingKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction() . ','; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' updated_value = ' . $this->sqlUpdatedValue() . ','; } elseif( true == array_key_exists( 'UpdatedValue', $this->getChangedColumns() ) ) { $strSql .= ' updated_value = ' . $this->sqlUpdatedValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' option_label = ' . $this->sqlOptionLabel() . ','; } elseif( true == array_key_exists( 'OptionLabel', $this->getChangedColumns() ) ) { $strSql .= ' option_label = ' . $this->sqlOptionLabel() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_setting_key_id' => $this->getWebsiteSettingKeyId(),
			'website_id' => $this->getWebsiteId(),
			'reference_id' => $this->getReferenceId(),
			'action' => $this->getAction(),
			'updated_value' => $this->getUpdatedValue(),
			'option_label' => $this->getOptionLabel(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>