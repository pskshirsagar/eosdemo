<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerEvents extends CEosPluralBase {

	/**
	 * @return CCustomerEvent[]
	 */
	public static function fetchCustomerEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerEvent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerEvent
	 */
	public static function fetchCustomerEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerEvent', $objDatabase );
	}

	public static function fetchCustomerEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_events', $objDatabase );
	}

	public static function fetchCustomerEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerEvent( sprintf( 'SELECT * FROM customer_events WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerEventsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerEvents( sprintf( 'SELECT * FROM customer_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerEventsByCompanyEventIdByCid( $intCompanyEventId, $intCid, $objDatabase ) {
		return self::fetchCustomerEvents( sprintf( 'SELECT * FROM customer_events WHERE company_event_id = %d AND cid = %d', ( int ) $intCompanyEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerEventsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerEvents( sprintf( 'SELECT * FROM customer_events WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>