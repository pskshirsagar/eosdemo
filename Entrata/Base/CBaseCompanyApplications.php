<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApplications extends CEosPluralBase {

	/**
	 * @return CCompanyApplication[]
	 */
	public static function fetchCompanyApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyApplication::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyApplication
	 */
	public static function fetchCompanyApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyApplication::class, $objDatabase );
	}

	public static function fetchCompanyApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_applications', $objDatabase );
	}

	public static function fetchCompanyApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplication( sprintf( 'SELECT * FROM company_applications WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByPrimaryDocumentIdByCid( $intPrimaryDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE primary_document_id = %d AND cid = %d', $intPrimaryDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByCoApplicantDocumentIdByCid( $intCoApplicantDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE co_applicant_document_id = %d AND cid = %d', $intCoApplicantDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByCoSignerDocumentIdByCid( $intCoSignerDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE co_signer_document_id = %d AND cid = %d', $intCoSignerDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByPrimaryLeaseDocumentIdByCid( $intPrimaryLeaseDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE primary_lease_document_id = %d AND cid = %d', $intPrimaryLeaseDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByCoApplicantLeaseDocumentIdByCid( $intCoApplicantLeaseDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE co_applicant_lease_document_id = %d AND cid = %d', $intCoApplicantLeaseDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByCoSignerLeaseDocumentIdByCid( $intCoSignerLeaseDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE co_signer_lease_document_id = %d AND cid = %d', $intCoSignerLeaseDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplications( sprintf( 'SELECT * FROM company_applications WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

}
?>