<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultActivityPoints
 * Do not add any new functions to this class.
 */

class CBaseDefaultActivityPoints extends CEosPluralBase {

	/**
	 * @return CDefaultActivityPoint[]
	 */
	public static function fetchDefaultActivityPoints( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultActivityPoint::class, $objDatabase );
	}

	/**
	 * @return CDefaultActivityPoint
	 */
	public static function fetchDefaultActivityPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultActivityPoint::class, $objDatabase );
	}

	public static function fetchDefaultActivityPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_activity_points', $objDatabase );
	}

	public static function fetchDefaultActivityPointById( $intId, $objDatabase ) {
		return self::fetchDefaultActivityPoint( sprintf( 'SELECT * FROM default_activity_points WHERE id = %d', $intId ), $objDatabase );
	}

}
?>