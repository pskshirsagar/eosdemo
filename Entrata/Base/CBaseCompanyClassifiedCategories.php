<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyClassifiedCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyClassifiedCategories extends CEosPluralBase {

	/**
	 * @return CCompanyClassifiedCategory[]
	 */
	public static function fetchCompanyClassifiedCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyClassifiedCategory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyClassifiedCategory
	 */
	public static function fetchCompanyClassifiedCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyClassifiedCategory::class, $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_classified_categories', $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyClassifiedCategory( sprintf( 'SELECT * FROM company_classified_categories WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyClassifiedCategories( sprintf( 'SELECT * FROM company_classified_categories WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoriesByCompanyClassifiedCategoryIdByCid( $intCompanyClassifiedCategoryId, $intCid, $objDatabase ) {
		return self::fetchCompanyClassifiedCategories( sprintf( 'SELECT * FROM company_classified_categories WHERE company_classified_category_id = %d AND cid = %d', $intCompanyClassifiedCategoryId, $intCid ), $objDatabase );
	}

}
?>