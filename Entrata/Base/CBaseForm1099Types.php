<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForm1099Types
 * Do not add any new functions to this class.
 */

class CBaseForm1099Types extends CEosPluralBase {

	/**
	 * @return CForm1099Type[]
	 */
	public static function fetchForm1099Types( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CForm1099Type', $objDatabase );
	}

	/**
	 * @return CForm1099Type
	 */
	public static function fetchForm1099Type( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CForm1099Type', $objDatabase );
	}

	public static function fetchForm1099TypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'form_1099_types', $objDatabase );
	}

	public static function fetchForm1099TypeById( $intId, $objDatabase ) {
		return self::fetchForm1099Type( sprintf( 'SELECT * FROM form_1099_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>