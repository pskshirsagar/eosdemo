<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserReportHistories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserReportHistories extends CEosPluralBase {

	/**
	 * @return CCompanyUserReportHistory[]
	 */
	public static function fetchCompanyUserReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserReportHistory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserReportHistory
	 */
	public static function fetchCompanyUserReportHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserReportHistory', $objDatabase );
	}

	public static function fetchCompanyUserReportHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_report_histories', $objDatabase );
	}

	public static function fetchCompanyUserReportHistoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserReportHistory( sprintf( 'SELECT * FROM company_user_report_histories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserReportHistoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserReportHistories( sprintf( 'SELECT * FROM company_user_report_histories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserReportHistoriesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserReportHistories( sprintf( 'SELECT * FROM company_user_report_histories WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserReportHistoriesByReportHistoryIdByCid( $intReportHistoryId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserReportHistories( sprintf( 'SELECT * FROM company_user_report_histories WHERE report_history_id = %d AND cid = %d', ( int ) $intReportHistoryId, ( int ) $intCid ), $objDatabase );
	}

}
?>