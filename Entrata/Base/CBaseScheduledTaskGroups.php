<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskGroups
 * Do not add any new functions to this class.
 */

class CBaseScheduledTaskGroups extends CEosPluralBase {

	/**
	 * @return CScheduledTaskGroup[]
	 */
	public static function fetchScheduledTaskGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledTaskGroup::class, $objDatabase );
	}

	/**
	 * @return CScheduledTaskGroup
	 */
	public static function fetchScheduledTaskGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledTaskGroup::class, $objDatabase );
	}

	public static function fetchScheduledTaskGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_task_groups', $objDatabase );
	}

	public static function fetchScheduledTaskGroupById( $intId, $objDatabase ) {
		return self::fetchScheduledTaskGroup( sprintf( 'SELECT * FROM scheduled_task_groups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScheduledTaskGroupsByScheduledTaskTypeId( $intScheduledTaskTypeId, $objDatabase ) {
		return self::fetchScheduledTaskGroups( sprintf( 'SELECT * FROM scheduled_task_groups WHERE scheduled_task_type_id = %d', $intScheduledTaskTypeId ), $objDatabase );
	}

}
?>