<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAppLoginTokens
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAppLoginTokens extends CEosPluralBase {

	/**
	 * @return CCustomerAppLoginToken[]
	 */
	public static function fetchCustomerAppLoginTokens( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerAppLoginToken', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAppLoginToken
	 */
	public static function fetchCustomerAppLoginToken( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerAppLoginToken', $objDatabase );
	}

	public static function fetchCustomerAppLoginTokenCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_app_login_tokens', $objDatabase );
	}

	public static function fetchCustomerAppLoginTokenByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAppLoginToken( sprintf( 'SELECT * FROM customer_app_login_tokens WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAppLoginTokensByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAppLoginTokens( sprintf( 'SELECT * FROM customer_app_login_tokens WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAppLoginTokensByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAppLoginTokens( sprintf( 'SELECT * FROM customer_app_login_tokens WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>