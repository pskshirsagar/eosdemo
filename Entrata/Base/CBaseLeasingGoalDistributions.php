<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoalDistributions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeasingGoalDistributions extends CEosPluralBase {

	/**
	 * @return CLeasingGoalDistribution[]
	 */
	public static function fetchLeasingGoalDistributions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeasingGoalDistribution::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeasingGoalDistribution
	 */
	public static function fetchLeasingGoalDistribution( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeasingGoalDistribution::class, $objDatabase );
	}

	public static function fetchLeasingGoalDistributionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'leasing_goal_distributions', $objDatabase );
	}

	public static function fetchLeasingGoalDistributionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalDistribution( sprintf( 'SELECT * FROM leasing_goal_distributions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalDistributionsByCid( $intCid, $objDatabase ) {
		return self::fetchLeasingGoalDistributions( sprintf( 'SELECT * FROM leasing_goal_distributions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalDistributionsByLeasingGoalIdByCid( $intLeasingGoalId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalDistributions( sprintf( 'SELECT * FROM leasing_goal_distributions WHERE leasing_goal_id = %d AND cid = %d', $intLeasingGoalId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalDistributionsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalDistributions( sprintf( 'SELECT * FROM leasing_goal_distributions WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

}
?>