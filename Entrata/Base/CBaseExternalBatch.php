<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseExternalBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.external_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intPaymentTypeId;
	protected $m_strDistributeOn;
	protected $m_strDepositDate;
	protected $m_strExternalBatchNumber;
	protected $m_intEntrataBatchNumber;
	protected $m_strBatchCreatedOn;
	protected $m_strBatchClosedOn;
	protected $m_strBatchStatus;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['distribute_on'] ) && $boolDirectSet ) $this->set( 'm_strDistributeOn', trim( $arrValues['distribute_on'] ) ); elseif( isset( $arrValues['distribute_on'] ) ) $this->setDistributeOn( $arrValues['distribute_on'] );
		if( isset( $arrValues['deposit_date'] ) && $boolDirectSet ) $this->set( 'm_strDepositDate', trim( $arrValues['deposit_date'] ) ); elseif( isset( $arrValues['deposit_date'] ) ) $this->setDepositDate( $arrValues['deposit_date'] );
		if( isset( $arrValues['external_batch_number'] ) && $boolDirectSet ) $this->set( 'm_strExternalBatchNumber', trim( $arrValues['external_batch_number'] ) ); elseif( isset( $arrValues['external_batch_number'] ) ) $this->setExternalBatchNumber( $arrValues['external_batch_number'] );
		if( isset( $arrValues['entrata_batch_number'] ) && $boolDirectSet ) $this->set( 'm_intEntrataBatchNumber', trim( $arrValues['entrata_batch_number'] ) ); elseif( isset( $arrValues['entrata_batch_number'] ) ) $this->setEntrataBatchNumber( $arrValues['entrata_batch_number'] );
		if( isset( $arrValues['batch_created_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchCreatedOn', trim( $arrValues['batch_created_on'] ) ); elseif( isset( $arrValues['batch_created_on'] ) ) $this->setBatchCreatedOn( $arrValues['batch_created_on'] );
		if( isset( $arrValues['batch_closed_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchClosedOn', trim( $arrValues['batch_closed_on'] ) ); elseif( isset( $arrValues['batch_closed_on'] ) ) $this->setBatchClosedOn( $arrValues['batch_closed_on'] );
		if( isset( $arrValues['batch_status'] ) && $boolDirectSet ) $this->set( 'm_strBatchStatus', trim( $arrValues['batch_status'] ) ); elseif( isset( $arrValues['batch_status'] ) ) $this->setBatchStatus( $arrValues['batch_status'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setDistributeOn( $strDistributeOn ) {
		$this->set( 'm_strDistributeOn', CStrings::strTrimDef( $strDistributeOn, -1, NULL, true ) );
	}

	public function getDistributeOn() {
		return $this->m_strDistributeOn;
	}

	public function sqlDistributeOn() {
		return ( true == isset( $this->m_strDistributeOn ) ) ? '\'' . $this->m_strDistributeOn . '\'' : 'NOW()';
	}

	public function setDepositDate( $strDepositDate ) {
		$this->set( 'm_strDepositDate', CStrings::strTrimDef( $strDepositDate, -1, NULL, true ) );
	}

	public function getDepositDate() {
		return $this->m_strDepositDate;
	}

	public function sqlDepositDate() {
		return ( true == isset( $this->m_strDepositDate ) ) ? '\'' . $this->m_strDepositDate . '\'' : 'NOW()';
	}

	public function setExternalBatchNumber( $strExternalBatchNumber ) {
		$this->set( 'm_strExternalBatchNumber', CStrings::strTrimDef( $strExternalBatchNumber, -1, NULL, true ) );
	}

	public function getExternalBatchNumber() {
		return $this->m_strExternalBatchNumber;
	}

	public function sqlExternalBatchNumber() {
		return ( true == isset( $this->m_strExternalBatchNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalBatchNumber ) : '\'' . addslashes( $this->m_strExternalBatchNumber ) . '\'' ) : 'NULL';
	}

	public function setEntrataBatchNumber( $intEntrataBatchNumber ) {
		$this->set( 'm_intEntrataBatchNumber', CStrings::strToIntDef( $intEntrataBatchNumber, NULL, false ) );
	}

	public function getEntrataBatchNumber() {
		return $this->m_intEntrataBatchNumber;
	}

	public function sqlEntrataBatchNumber() {
		return ( true == isset( $this->m_intEntrataBatchNumber ) ) ? ( string ) $this->m_intEntrataBatchNumber : 'NULL';
	}

	public function setBatchCreatedOn( $strBatchCreatedOn ) {
		$this->set( 'm_strBatchCreatedOn', CStrings::strTrimDef( $strBatchCreatedOn, -1, NULL, true ) );
	}

	public function getBatchCreatedOn() {
		return $this->m_strBatchCreatedOn;
	}

	public function sqlBatchCreatedOn() {
		return ( true == isset( $this->m_strBatchCreatedOn ) ) ? '\'' . $this->m_strBatchCreatedOn . '\'' : 'NOW()';
	}

	public function setBatchClosedOn( $strBatchClosedOn ) {
		$this->set( 'm_strBatchClosedOn', CStrings::strTrimDef( $strBatchClosedOn, -1, NULL, true ) );
	}

	public function getBatchClosedOn() {
		return $this->m_strBatchClosedOn;
	}

	public function sqlBatchClosedOn() {
		return ( true == isset( $this->m_strBatchClosedOn ) ) ? '\'' . $this->m_strBatchClosedOn . '\'' : 'NULL';
	}

	public function setBatchStatus( $strBatchStatus ) {
		$this->set( 'm_strBatchStatus', CStrings::strTrimDef( $strBatchStatus, -1, NULL, true ) );
	}

	public function getBatchStatus() {
		return $this->m_strBatchStatus;
	}

	public function sqlBatchStatus() {
		return ( true == isset( $this->m_strBatchStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchStatus ) : '\'' . addslashes( $this->m_strBatchStatus ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_database_id, company_merchant_account_id, payment_type_id, distribute_on, deposit_date, external_batch_number, entrata_batch_number, batch_created_on, batch_closed_on, batch_status, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlCompanyMerchantAccountId() . ', ' .
						$this->sqlPaymentTypeId() . ', ' .
						$this->sqlDistributeOn() . ', ' .
						$this->sqlDepositDate() . ', ' .
						$this->sqlExternalBatchNumber() . ', ' .
						$this->sqlEntrataBatchNumber() . ', ' .
						$this->sqlBatchCreatedOn() . ', ' .
						$this->sqlBatchClosedOn() . ', ' .
						$this->sqlBatchStatus() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribute_on = ' . $this->sqlDistributeOn(). ',' ; } elseif( true == array_key_exists( 'DistributeOn', $this->getChangedColumns() ) ) { $strSql .= ' distribute_on = ' . $this->sqlDistributeOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_date = ' . $this->sqlDepositDate(). ',' ; } elseif( true == array_key_exists( 'DepositDate', $this->getChangedColumns() ) ) { $strSql .= ' deposit_date = ' . $this->sqlDepositDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_batch_number = ' . $this->sqlExternalBatchNumber(). ',' ; } elseif( true == array_key_exists( 'ExternalBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' external_batch_number = ' . $this->sqlExternalBatchNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_batch_number = ' . $this->sqlEntrataBatchNumber(). ',' ; } elseif( true == array_key_exists( 'EntrataBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' entrata_batch_number = ' . $this->sqlEntrataBatchNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_created_on = ' . $this->sqlBatchCreatedOn(). ',' ; } elseif( true == array_key_exists( 'BatchCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' batch_created_on = ' . $this->sqlBatchCreatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_closed_on = ' . $this->sqlBatchClosedOn(). ',' ; } elseif( true == array_key_exists( 'BatchClosedOn', $this->getChangedColumns() ) ) { $strSql .= ' batch_closed_on = ' . $this->sqlBatchClosedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_status = ' . $this->sqlBatchStatus(). ',' ; } elseif( true == array_key_exists( 'BatchStatus', $this->getChangedColumns() ) ) { $strSql .= ' batch_status = ' . $this->sqlBatchStatus() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'distribute_on' => $this->getDistributeOn(),
			'deposit_date' => $this->getDepositDate(),
			'external_batch_number' => $this->getExternalBatchNumber(),
			'entrata_batch_number' => $this->getEntrataBatchNumber(),
			'batch_created_on' => $this->getBatchCreatedOn(),
			'batch_closed_on' => $this->getBatchClosedOn(),
			'batch_status' => $this->getBatchStatus(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>