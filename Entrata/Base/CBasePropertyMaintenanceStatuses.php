<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceStatuses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceStatuses extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceStatus[]
	 */
	public static function fetchPropertyMaintenanceStatuses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMaintenanceStatus', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceStatus
	 */
	public static function fetchPropertyMaintenanceStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMaintenanceStatus', $objDatabase );
	}

	public static function fetchPropertyMaintenanceStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_statuses', $objDatabase );
	}

	public static function fetchPropertyMaintenanceStatusByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceStatus( sprintf( 'SELECT * FROM property_maintenance_statuses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceStatusesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceStatuses( sprintf( 'SELECT * FROM property_maintenance_statuses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceStatusesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceStatuses( sprintf( 'SELECT * FROM property_maintenance_statuses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceStatusesByMaintenanceStatusIdByCid( $intMaintenanceStatusId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceStatuses( sprintf( 'SELECT * FROM property_maintenance_statuses WHERE maintenance_status_id = %d AND cid = %d', ( int ) $intMaintenanceStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>