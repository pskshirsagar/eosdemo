<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDepositInterestFormulas extends CEosPluralBase {

    const TABLE_DEPOSIT_INTEREST_FORMULAS = 'public.deposit_interest_formulas';

    public static function fetchDepositInterestFormulas( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CDepositInterestFormula', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchDepositInterestFormula( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CDepositInterestFormula', $objDatabase );
    }

    public static function fetchDepositInterestFormulaCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'deposit_interest_formulas', $objDatabase );
    }

    public static function fetchDepositInterestFormulaByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchDepositInterestFormula( sprintf( 'SELECT * FROM deposit_interest_formulas WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchDepositInterestFormulasByCid( $intCid, $objDatabase ) {
        return self::fetchDepositInterestFormulas( sprintf( 'SELECT * FROM deposit_interest_formulas WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

}
?>