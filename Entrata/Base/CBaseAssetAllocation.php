<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetAllocation extends CEosSingularBase {

	const TABLE_NAME = 'public.asset_allocations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPeriodId;
	protected $m_intGlTransactionTypeId;
	protected $m_intChargeAssetTransactionId;
	protected $m_intCreditAssetTransactionId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intAssetAllocationId;
	protected $m_intQuantity;
	protected $m_fltUnitCost;
	protected $m_strAlllocationDatetime;
	protected $m_fltAllocationAmount;
	protected $m_strReportingPostDate;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_boolIsDeleted;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDeleted = false;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['charge_asset_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeAssetTransactionId', trim( $arrValues['charge_asset_transaction_id'] ) ); elseif( isset( $arrValues['charge_asset_transaction_id'] ) ) $this->setChargeAssetTransactionId( $arrValues['charge_asset_transaction_id'] );
		if( isset( $arrValues['credit_asset_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditAssetTransactionId', trim( $arrValues['credit_asset_transaction_id'] ) ); elseif( isset( $arrValues['credit_asset_transaction_id'] ) ) $this->setCreditAssetTransactionId( $arrValues['credit_asset_transaction_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['asset_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetAllocationId', trim( $arrValues['asset_allocation_id'] ) ); elseif( isset( $arrValues['asset_allocation_id'] ) ) $this->setAssetAllocationId( $arrValues['asset_allocation_id'] );
		if( isset( $arrValues['quantity'] ) && $boolDirectSet ) $this->set( 'm_intQuantity', trim( $arrValues['quantity'] ) ); elseif( isset( $arrValues['quantity'] ) ) $this->setQuantity( $arrValues['quantity'] );
		if( isset( $arrValues['unit_cost'] ) && $boolDirectSet ) $this->set( 'm_fltUnitCost', trim( $arrValues['unit_cost'] ) ); elseif( isset( $arrValues['unit_cost'] ) ) $this->setUnitCost( $arrValues['unit_cost'] );
		if( isset( $arrValues['alllocation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAlllocationDatetime', trim( $arrValues['alllocation_datetime'] ) ); elseif( isset( $arrValues['alllocation_datetime'] ) ) $this->setAlllocationDatetime( $arrValues['alllocation_datetime'] );
		if( isset( $arrValues['allocation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAllocationAmount', trim( $arrValues['allocation_amount'] ) ); elseif( isset( $arrValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrValues['allocation_amount'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setChargeAssetTransactionId( $intChargeAssetTransactionId ) {
		$this->set( 'm_intChargeAssetTransactionId', CStrings::strToIntDef( $intChargeAssetTransactionId, NULL, false ) );
	}

	public function getChargeAssetTransactionId() {
		return $this->m_intChargeAssetTransactionId;
	}

	public function sqlChargeAssetTransactionId() {
		return ( true == isset( $this->m_intChargeAssetTransactionId ) ) ? ( string ) $this->m_intChargeAssetTransactionId : 'NULL';
	}

	public function setCreditAssetTransactionId( $intCreditAssetTransactionId ) {
		$this->set( 'm_intCreditAssetTransactionId', CStrings::strToIntDef( $intCreditAssetTransactionId, NULL, false ) );
	}

	public function getCreditAssetTransactionId() {
		return $this->m_intCreditAssetTransactionId;
	}

	public function sqlCreditAssetTransactionId() {
		return ( true == isset( $this->m_intCreditAssetTransactionId ) ) ? ( string ) $this->m_intCreditAssetTransactionId : 'NULL';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setAssetAllocationId( $intAssetAllocationId ) {
		$this->set( 'm_intAssetAllocationId', CStrings::strToIntDef( $intAssetAllocationId, NULL, false ) );
	}

	public function getAssetAllocationId() {
		return $this->m_intAssetAllocationId;
	}

	public function sqlAssetAllocationId() {
		return ( true == isset( $this->m_intAssetAllocationId ) ) ? ( string ) $this->m_intAssetAllocationId : 'NULL';
	}

	public function setQuantity( $intQuantity ) {
		$this->set( 'm_intQuantity', CStrings::strToIntDef( $intQuantity, NULL, false ) );
	}

	public function getQuantity() {
		return $this->m_intQuantity;
	}

	public function sqlQuantity() {
		return ( true == isset( $this->m_intQuantity ) ) ? ( string ) $this->m_intQuantity : 'NULL';
	}

	public function setUnitCost( $fltUnitCost ) {
		$this->set( 'm_fltUnitCost', CStrings::strToFloatDef( $fltUnitCost, NULL, false, 2 ) );
	}

	public function getUnitCost() {
		return $this->m_fltUnitCost;
	}

	public function sqlUnitCost() {
		return ( true == isset( $this->m_fltUnitCost ) ) ? ( string ) $this->m_fltUnitCost : 'NULL';
	}

	public function setAlllocationDatetime( $strAlllocationDatetime ) {
		$this->set( 'm_strAlllocationDatetime', CStrings::strTrimDef( $strAlllocationDatetime, -1, NULL, true ) );
	}

	public function getAlllocationDatetime() {
		return $this->m_strAlllocationDatetime;
	}

	public function sqlAlllocationDatetime() {
		return ( true == isset( $this->m_strAlllocationDatetime ) ) ? '\'' . $this->m_strAlllocationDatetime . '\'' : 'NOW()';
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$this->set( 'm_fltAllocationAmount', CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 2 ) );
	}

	public function getAllocationAmount() {
		return $this->m_fltAllocationAmount;
	}

	public function sqlAllocationAmount() {
		return ( true == isset( $this->m_fltAllocationAmount ) ) ? ( string ) $this->m_fltAllocationAmount : 'NULL';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, period_id, gl_transaction_type_id, charge_asset_transaction_id, credit_asset_transaction_id, debit_gl_account_id, credit_gl_account_id, asset_allocation_id, quantity, unit_cost, alllocation_datetime, allocation_amount, reporting_post_date, post_date, post_month, is_deleted, is_posted, is_initial_import, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlGlTransactionTypeId() . ', ' .
 						$this->sqlChargeAssetTransactionId() . ', ' .
 						$this->sqlCreditAssetTransactionId() . ', ' .
 						$this->sqlDebitGlAccountId() . ', ' .
 						$this->sqlCreditGlAccountId() . ', ' .
 						$this->sqlAssetAllocationId() . ', ' .
 						$this->sqlQuantity() . ', ' .
 						$this->sqlUnitCost() . ', ' .
 						$this->sqlAlllocationDatetime() . ', ' .
 						$this->sqlAllocationAmount() . ', ' .
 						$this->sqlReportingPostDate() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlIsDeleted() . ', ' .
 						$this->sqlIsPosted() . ', ' .
 						$this->sqlIsInitialImport() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_asset_transaction_id = ' . $this->sqlChargeAssetTransactionId() . ','; } elseif( true == array_key_exists( 'ChargeAssetTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' charge_asset_transaction_id = ' . $this->sqlChargeAssetTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_asset_transaction_id = ' . $this->sqlCreditAssetTransactionId() . ','; } elseif( true == array_key_exists( 'CreditAssetTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' credit_asset_transaction_id = ' . $this->sqlCreditAssetTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; } elseif( true == array_key_exists( 'DebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; } elseif( true == array_key_exists( 'CreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_allocation_id = ' . $this->sqlAssetAllocationId() . ','; } elseif( true == array_key_exists( 'AssetAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' asset_allocation_id = ' . $this->sqlAssetAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; } elseif( true == array_key_exists( 'Quantity', $this->getChangedColumns() ) ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_cost = ' . $this->sqlUnitCost() . ','; } elseif( true == array_key_exists( 'UnitCost', $this->getChangedColumns() ) ) { $strSql .= ' unit_cost = ' . $this->sqlUnitCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alllocation_datetime = ' . $this->sqlAlllocationDatetime() . ','; } elseif( true == array_key_exists( 'AlllocationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' alllocation_datetime = ' . $this->sqlAlllocationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; } elseif( true == array_key_exists( 'AllocationAmount', $this->getChangedColumns() ) ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'period_id' => $this->getPeriodId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'charge_asset_transaction_id' => $this->getChargeAssetTransactionId(),
			'credit_asset_transaction_id' => $this->getCreditAssetTransactionId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'asset_allocation_id' => $this->getAssetAllocationId(),
			'quantity' => $this->getQuantity(),
			'unit_cost' => $this->getUnitCost(),
			'alllocation_datetime' => $this->getAlllocationDatetime(),
			'allocation_amount' => $this->getAllocationAmount(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'is_deleted' => $this->getIsDeleted(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>