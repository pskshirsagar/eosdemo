<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySubsidyDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySubsidyDetails extends CEosPluralBase {

	/**
	 * @return CPropertySubsidyDetail[]
	 */
	public static function fetchPropertySubsidyDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertySubsidyDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertySubsidyDetail
	 */
	public static function fetchPropertySubsidyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertySubsidyDetail::class, $objDatabase );
	}

	public static function fetchPropertySubsidyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_subsidy_details', $objDatabase );
	}

	public static function fetchPropertySubsidyDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetail( sprintf( 'SELECT * FROM property_subsidy_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetail( sprintf( 'SELECT * FROM property_subsidy_details WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByHudSubsidyRecertificationTypeIdByCid( $intHudSubsidyRecertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE hud_subsidy_recertification_type_id = %d AND cid = %d', $intHudSubsidyRecertificationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByTaxCreditSubsidyRecertificationTypeIdByCid( $intTaxCreditSubsidyRecertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE tax_credit_subsidy_recertification_type_id = %d AND cid = %d', $intTaxCreditSubsidyRecertificationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByHomeSubsidyRecertificationTypeIdByCid( $intHomeSubsidyRecertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE home_subsidy_recertification_type_id = %d AND cid = %d', $intHomeSubsidyRecertificationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsBySubsidyTracsVersionIdByCid( $intSubsidyTracsVersionId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE subsidy_tracs_version_id = %d AND cid = %d', $intSubsidyTracsVersionId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE file_type_id = %d AND cid = %d', $intFileTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByTaxCreditHhCompChangeSubsidyRecertificationTypeIdByCid( $intTaxCreditHhCompChangeSubsidyRecertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetails( sprintf( 'SELECT * FROM property_subsidy_details WHERE tax_credit_hh_comp_change_subsidy_recertification_type_id = %d AND cid = %d', $intTaxCreditHhCompChangeSubsidyRecertificationTypeId, $intCid ), $objDatabase );
	}

}
?>