<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySpecialClaims
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidySpecialClaims extends CEosPluralBase {

	/**
	 * @return CSubsidySpecialClaim[]
	 */
	public static function fetchSubsidySpecialClaims( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidySpecialClaim', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidySpecialClaim
	 */
	public static function fetchSubsidySpecialClaim( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidySpecialClaim', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_special_claims', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaim( sprintf( 'SELECT * FROM subsidy_special_claims WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaims( sprintf( 'SELECT * FROM subsidy_special_claims WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaims( sprintf( 'SELECT * FROM subsidy_special_claims WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimsBySubsidySpecialClaimTypeIdByCid( $intSubsidySpecialClaimTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaims( sprintf( 'SELECT * FROM subsidy_special_claims WHERE subsidy_special_claim_type_id = %d AND cid = %d', ( int ) $intSubsidySpecialClaimTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimsBySubsidySpecialClaimStatusTypeIdByCid( $intSubsidySpecialClaimStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaims( sprintf( 'SELECT * FROM subsidy_special_claims WHERE subsidy_special_claim_status_type_id = %d AND cid = %d', ( int ) $intSubsidySpecialClaimStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimsBySubsidyHapRequestIdByCid( $intSubsidyHapRequestId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaims( sprintf( 'SELECT * FROM subsidy_special_claims WHERE subsidy_hap_request_id = %d AND cid = %d', ( int ) $intSubsidyHapRequestId, ( int ) $intCid ), $objDatabase );
	}

}
?>