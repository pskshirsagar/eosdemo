<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentDependencyTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentDependencyTypes extends CEosPluralBase {

	/**
	 * @return CDocumentDependencyType[]
	 */
	public static function fetchDocumentDependencyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDocumentDependencyType::class, $objDatabase );
	}

	/**
	 * @return CDocumentDependencyType
	 */
	public static function fetchDocumentDependencyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentDependencyType::class, $objDatabase );
	}

	public static function fetchDocumentDependencyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_dependency_types', $objDatabase );
	}

	public static function fetchDocumentDependencyTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentDependencyType( sprintf( 'SELECT * FROM document_dependency_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDocumentDependencyTypesByDocumentAssociationTypeId( $intDocumentAssociationTypeId, $objDatabase ) {
		return self::fetchDocumentDependencyTypes( sprintf( 'SELECT * FROM document_dependency_types WHERE document_association_type_id = %d', $intDocumentAssociationTypeId ), $objDatabase );
	}

}
?>