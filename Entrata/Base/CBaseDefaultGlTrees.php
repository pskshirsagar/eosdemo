<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlTrees
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlTrees extends CEosPluralBase {

	/**
	 * @return CDefaultGlTree[]
	 */
	public static function fetchDefaultGlTrees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlTree::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlTree
	 */
	public static function fetchDefaultGlTree( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlTree::class, $objDatabase );
	}

	public static function fetchDefaultGlTreeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_trees', $objDatabase );
	}

	public static function fetchDefaultGlTreeById( $intId, $objDatabase ) {
		return self::fetchDefaultGlTree( sprintf( 'SELECT * FROM default_gl_trees WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultGlTreesByGlTreeTypeId( $intGlTreeTypeId, $objDatabase ) {
		return self::fetchDefaultGlTrees( sprintf( 'SELECT * FROM default_gl_trees WHERE gl_tree_type_id = %d', $intGlTreeTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlTreesByModeTypeId( $intModeTypeId, $objDatabase ) {
		return self::fetchDefaultGlTrees( sprintf( 'SELECT * FROM default_gl_trees WHERE mode_type_id = %d', $intModeTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlTreesByDefaultGlChartId( $intDefaultGlChartId, $objDatabase ) {
		return self::fetchDefaultGlTrees( sprintf( 'SELECT * FROM default_gl_trees WHERE default_gl_chart_id = %d', $intDefaultGlChartId ), $objDatabase );
	}

	public static function fetchDefaultGlTreesByRestrictedDefaultGlTreeId( $intRestrictedDefaultGlTreeId, $objDatabase ) {
		return self::fetchDefaultGlTrees( sprintf( 'SELECT * FROM default_gl_trees WHERE restricted_default_gl_tree_id = %d', $intRestrictedDefaultGlTreeId ), $objDatabase );
	}

}
?>