<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeField extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_merge_fields';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsMultiselect;
	protected $m_intMergeFieldTypeId;
	protected $m_strViewExpression;
	protected $m_jsonViewExpression;
	protected $m_intOriginalCompanyMergeFieldId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strTitle;
	protected $m_intMergeFieldGroupId;
	protected $m_intDefaultMergeFieldId;
	protected $m_intBlockCompanyMergeFieldId;
	protected $m_strDefaultValue;
	protected $m_intIsRequired;
	protected $m_intShowInMergeDisplay;
	protected $m_intAllowUserUpdate;
	protected $m_intIsBlock;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMergeFieldSubGroupId;
	protected $m_strDataType;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMultiselect = '0';
		$this->m_intMergeFieldGroupId = '10';
		$this->m_intIsRequired = '0';
		$this->m_intShowInMergeDisplay = '0';
		$this->m_intAllowUserUpdate = '0';
		$this->m_intIsBlock = '0';
		$this->m_strDataType = 'text';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['is_multiselect'] ) && $boolDirectSet ) $this->set( 'm_intIsMultiselect', trim( $arrValues['is_multiselect'] ) ); elseif( isset( $arrValues['is_multiselect'] ) ) $this->setIsMultiselect( $arrValues['is_multiselect'] );
		if( isset( $arrValues['merge_field_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldTypeId', trim( $arrValues['merge_field_type_id'] ) ); elseif( isset( $arrValues['merge_field_type_id'] ) ) $this->setMergeFieldTypeId( $arrValues['merge_field_type_id'] );
		if( isset( $arrValues['view_expression'] ) ) $this->set( 'm_strViewExpression', trim( $arrValues['view_expression'] ) );
		if( isset( $arrValues['original_company_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalCompanyMergeFieldId', trim( $arrValues['original_company_merge_field_id'] ) ); elseif( isset( $arrValues['original_company_merge_field_id'] ) ) $this->setOriginalCompanyMergeFieldId( $arrValues['original_company_merge_field_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldGroupId', trim( $arrValues['merge_field_group_id'] ) ); elseif( isset( $arrValues['merge_field_group_id'] ) ) $this->setMergeFieldGroupId( $arrValues['merge_field_group_id'] );
		if( isset( $arrValues['default_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMergeFieldId', trim( $arrValues['default_merge_field_id'] ) ); elseif( isset( $arrValues['default_merge_field_id'] ) ) $this->setDefaultMergeFieldId( $arrValues['default_merge_field_id'] );
		if( isset( $arrValues['block_company_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockCompanyMergeFieldId', trim( $arrValues['block_company_merge_field_id'] ) ); elseif( isset( $arrValues['block_company_merge_field_id'] ) ) $this->setBlockCompanyMergeFieldId( $arrValues['block_company_merge_field_id'] );
		if( isset( $arrValues['default_value'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDefaultValue', trim( $arrValues['default_value'] ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( $arrValues['default_value'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['show_in_merge_display'] ) && $boolDirectSet ) $this->set( 'm_intShowInMergeDisplay', trim( $arrValues['show_in_merge_display'] ) ); elseif( isset( $arrValues['show_in_merge_display'] ) ) $this->setShowInMergeDisplay( $arrValues['show_in_merge_display'] );
		if( isset( $arrValues['allow_user_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowUserUpdate', trim( $arrValues['allow_user_update'] ) ); elseif( isset( $arrValues['allow_user_update'] ) ) $this->setAllowUserUpdate( $arrValues['allow_user_update'] );
		if( isset( $arrValues['is_block'] ) && $boolDirectSet ) $this->set( 'm_intIsBlock', trim( $arrValues['is_block'] ) ); elseif( isset( $arrValues['is_block'] ) ) $this->setIsBlock( $arrValues['is_block'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['merge_field_sub_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldSubGroupId', trim( $arrValues['merge_field_sub_group_id'] ) ); elseif( isset( $arrValues['merge_field_sub_group_id'] ) ) $this->setMergeFieldSubGroupId( $arrValues['merge_field_sub_group_id'] );
		if( isset( $arrValues['data_type'] ) && $boolDirectSet ) $this->set( 'm_strDataType', trim( stripcslashes( $arrValues['data_type'] ) ) ); elseif( isset( $arrValues['data_type'] ) ) $this->setDataType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_type'] ) : $arrValues['data_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 64, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsMultiselect( $intIsMultiselect ) {
		$this->set( 'm_intIsMultiselect', CStrings::strToIntDef( $intIsMultiselect, NULL, false ) );
	}

	public function getIsMultiselect() {
		return $this->m_intIsMultiselect;
	}

	public function sqlIsMultiselect() {
		return ( true == isset( $this->m_intIsMultiselect ) ) ? ( string ) $this->m_intIsMultiselect : '0';
	}

	public function setMergeFieldTypeId( $intMergeFieldTypeId ) {
		$this->set( 'm_intMergeFieldTypeId', CStrings::strToIntDef( $intMergeFieldTypeId, NULL, false ) );
	}

	public function getMergeFieldTypeId() {
		return $this->m_intMergeFieldTypeId;
	}

	public function sqlMergeFieldTypeId() {
		return ( true == isset( $this->m_intMergeFieldTypeId ) ) ? ( string ) $this->m_intMergeFieldTypeId : 'NULL';
	}

	public function setViewExpression( $jsonViewExpression ) {
		if( true == valObj( $jsonViewExpression, 'stdClass' ) ) {
			$this->set( 'm_jsonViewExpression', $jsonViewExpression );
		} elseif( true == valJsonString( $jsonViewExpression ) ) {
			$this->set( 'm_jsonViewExpression', CStrings::strToJson( $jsonViewExpression ) );
		} else {
			$this->set( 'm_jsonViewExpression', NULL );
		}
		unset( $this->m_strViewExpression );
	}

	public function getViewExpression() {
		if( true == isset( $this->m_strViewExpression ) ) {
			$this->m_jsonViewExpression = CStrings::strToJson( $this->m_strViewExpression );
			unset( $this->m_strViewExpression );
		}
		return $this->m_jsonViewExpression;
	}

	public function sqlViewExpression() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getViewExpression() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getViewExpression() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getViewExpression() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setOriginalCompanyMergeFieldId( $intOriginalCompanyMergeFieldId ) {
		$this->set( 'm_intOriginalCompanyMergeFieldId', CStrings::strToIntDef( $intOriginalCompanyMergeFieldId, NULL, false ) );
	}

	public function getOriginalCompanyMergeFieldId() {
		return $this->m_intOriginalCompanyMergeFieldId;
	}

	public function sqlOriginalCompanyMergeFieldId() {
		return ( true == isset( $this->m_intOriginalCompanyMergeFieldId ) ) ? ( string ) $this->m_intOriginalCompanyMergeFieldId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 250, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setMergeFieldGroupId( $intMergeFieldGroupId ) {
		$this->set( 'm_intMergeFieldGroupId', CStrings::strToIntDef( $intMergeFieldGroupId, NULL, false ) );
	}

	public function getMergeFieldGroupId() {
		return $this->m_intMergeFieldGroupId;
	}

	public function sqlMergeFieldGroupId() {
		return ( true == isset( $this->m_intMergeFieldGroupId ) ) ? ( string ) $this->m_intMergeFieldGroupId : '10';
	}

	public function setDefaultMergeFieldId( $intDefaultMergeFieldId ) {
		$this->set( 'm_intDefaultMergeFieldId', CStrings::strToIntDef( $intDefaultMergeFieldId, NULL, false ) );
	}

	public function getDefaultMergeFieldId() {
		return $this->m_intDefaultMergeFieldId;
	}

	public function sqlDefaultMergeFieldId() {
		return ( true == isset( $this->m_intDefaultMergeFieldId ) ) ? ( string ) $this->m_intDefaultMergeFieldId : 'NULL';
	}

	public function setBlockCompanyMergeFieldId( $intBlockCompanyMergeFieldId ) {
		$this->set( 'm_intBlockCompanyMergeFieldId', CStrings::strToIntDef( $intBlockCompanyMergeFieldId, NULL, false ) );
	}

	public function getBlockCompanyMergeFieldId() {
		return $this->m_intBlockCompanyMergeFieldId;
	}

	public function sqlBlockCompanyMergeFieldId() {
		return ( true == isset( $this->m_intBlockCompanyMergeFieldId ) ) ? ( string ) $this->m_intBlockCompanyMergeFieldId : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ), $strLocaleCode );
	}

	public function getDefaultValue( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDefaultValue', $strLocaleCode );
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDefaultValue ) : '\'' . addslashes( $this->m_strDefaultValue ) . '\'' ) : 'NULL';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setShowInMergeDisplay( $intShowInMergeDisplay ) {
		$this->set( 'm_intShowInMergeDisplay', CStrings::strToIntDef( $intShowInMergeDisplay, NULL, false ) );
	}

	public function getShowInMergeDisplay() {
		return $this->m_intShowInMergeDisplay;
	}

	public function sqlShowInMergeDisplay() {
		return ( true == isset( $this->m_intShowInMergeDisplay ) ) ? ( string ) $this->m_intShowInMergeDisplay : '0';
	}

	public function setAllowUserUpdate( $intAllowUserUpdate ) {
		$this->set( 'm_intAllowUserUpdate', CStrings::strToIntDef( $intAllowUserUpdate, NULL, false ) );
	}

	public function getAllowUserUpdate() {
		return $this->m_intAllowUserUpdate;
	}

	public function sqlAllowUserUpdate() {
		return ( true == isset( $this->m_intAllowUserUpdate ) ) ? ( string ) $this->m_intAllowUserUpdate : '0';
	}

	public function setIsBlock( $intIsBlock ) {
		$this->set( 'm_intIsBlock', CStrings::strToIntDef( $intIsBlock, NULL, false ) );
	}

	public function getIsBlock() {
		return $this->m_intIsBlock;
	}

	public function sqlIsBlock() {
		return ( true == isset( $this->m_intIsBlock ) ) ? ( string ) $this->m_intIsBlock : '0';
	}

	public function setMergeFieldSubGroupId( $intMergeFieldSubGroupId ) {
		$this->set( 'm_intMergeFieldSubGroupId', CStrings::strToIntDef( $intMergeFieldSubGroupId, NULL, false ) );
	}

	public function getMergeFieldSubGroupId() {
		return $this->m_intMergeFieldSubGroupId;
	}

	public function sqlMergeFieldSubGroupId() {
		return ( true == isset( $this->m_intMergeFieldSubGroupId ) ) ? ( string ) $this->m_intMergeFieldSubGroupId : 'NULL';
	}

	public function setDataType( $strDataType ) {
		$this->set( 'm_strDataType', CStrings::strTrimDef( $strDataType, -1, NULL, true ) );
	}

	public function getDataType() {
		return $this->m_strDataType;
	}

	public function sqlDataType() {
		return ( true == isset( $this->m_strDataType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataType ) : '\'' . addslashes( $this->m_strDataType ) . '\'' ) : '\'text\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, name, description, is_multiselect, merge_field_type_id, view_expression, original_company_merge_field_id, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, title, merge_field_group_id, default_merge_field_id, block_company_merge_field_id, default_value, is_required, show_in_merge_display, allow_user_update, is_block, details, merge_field_sub_group_id, data_type )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlIsMultiselect() . ', ' .
		          $this->sqlMergeFieldTypeId() . ', ' .
		          $this->sqlViewExpression() . ', ' .
		          $this->sqlOriginalCompanyMergeFieldId() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlTitle() . ', ' .
		          $this->sqlMergeFieldGroupId() . ', ' .
		          $this->sqlDefaultMergeFieldId() . ', ' .
		          $this->sqlBlockCompanyMergeFieldId() . ', ' .
		          $this->sqlDefaultValue() . ', ' .
		          $this->sqlIsRequired() . ', ' .
		          $this->sqlShowInMergeDisplay() . ', ' .
		          $this->sqlAllowUserUpdate() . ', ' .
		          $this->sqlIsBlock() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlMergeFieldSubGroupId() . ', ' .
		          $this->sqlDataType() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_multiselect = ' . $this->sqlIsMultiselect(). ',' ; } elseif( true == array_key_exists( 'IsMultiselect', $this->getChangedColumns() ) ) { $strSql .= ' is_multiselect = ' . $this->sqlIsMultiselect() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_type_id = ' . $this->sqlMergeFieldTypeId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldTypeId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_type_id = ' . $this->sqlMergeFieldTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression(). ',' ; } elseif( true == array_key_exists( 'ViewExpression', $this->getChangedColumns() ) ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_company_merge_field_id = ' . $this->sqlOriginalCompanyMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'OriginalCompanyMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' original_company_merge_field_id = ' . $this->sqlOriginalCompanyMergeFieldId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_merge_field_id = ' . $this->sqlDefaultMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'DefaultMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' default_merge_field_id = ' . $this->sqlDefaultMergeFieldId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_company_merge_field_id = ' . $this->sqlBlockCompanyMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'BlockCompanyMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' block_company_merge_field_id = ' . $this->sqlBlockCompanyMergeFieldId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue(). ',' ; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay(). ',' ; } elseif( true == array_key_exists( 'ShowInMergeDisplay', $this->getChangedColumns() ) ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowUserUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_block = ' . $this->sqlIsBlock(). ',' ; } elseif( true == array_key_exists( 'IsBlock', $this->getChangedColumns() ) ) { $strSql .= ' is_block = ' . $this->sqlIsBlock() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldSubGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_type = ' . $this->sqlDataType(). ',' ; } elseif( true == array_key_exists( 'DataType', $this->getChangedColumns() ) ) { $strSql .= ' data_type = ' . $this->sqlDataType() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_multiselect' => $this->getIsMultiselect(),
			'merge_field_type_id' => $this->getMergeFieldTypeId(),
			'view_expression' => $this->getViewExpression(),
			'original_company_merge_field_id' => $this->getOriginalCompanyMergeFieldId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'title' => $this->getTitle(),
			'merge_field_group_id' => $this->getMergeFieldGroupId(),
			'default_merge_field_id' => $this->getDefaultMergeFieldId(),
			'block_company_merge_field_id' => $this->getBlockCompanyMergeFieldId(),
			'default_value' => $this->getDefaultValue(),
			'is_required' => $this->getIsRequired(),
			'show_in_merge_display' => $this->getShowInMergeDisplay(),
			'allow_user_update' => $this->getAllowUserUpdate(),
			'is_block' => $this->getIsBlock(),
			'details' => $this->getDetails(),
			'merge_field_sub_group_id' => $this->getMergeFieldSubGroupId(),
			'data_type' => $this->getDataType()
		);
	}

}
?>