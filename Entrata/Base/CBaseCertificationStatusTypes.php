<?php

class CBaseCertificationStatusTypes extends CEosPluralBase {

	public static function fetchCertificationStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCertificationStatusType', $objDatabase );
	}

	public static function fetchCertificationStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCertificationStatusType', $objDatabase );
	}

	public static function fetchCertificationStatusTypeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'certification_status_types', $objDatabase );
	}

	public static function fetchCertificationStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCertificationStatusType( sprintf( 'SELECT * FROM certification_status_types WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>