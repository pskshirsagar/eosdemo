<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportGroups
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportGroups extends CEosPluralBase {

	/**
	 * @return CDefaultReportGroup[]
	 */
	public static function fetchDefaultReportGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReportGroup::class, $objDatabase );
	}

	/**
	 * @return CDefaultReportGroup
	 */
	public static function fetchDefaultReportGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReportGroup::class, $objDatabase );
	}

	public static function fetchDefaultReportGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_groups', $objDatabase );
	}

	public static function fetchDefaultReportGroupById( $intId, $objDatabase ) {
		return self::fetchDefaultReportGroup( sprintf( 'SELECT * FROM default_report_groups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultReportGroupsByReportGroupTypeId( $intReportGroupTypeId, $objDatabase ) {
		return self::fetchDefaultReportGroups( sprintf( 'SELECT * FROM default_report_groups WHERE report_group_type_id = %d', $intReportGroupTypeId ), $objDatabase );
	}

	public static function fetchDefaultReportGroupsByParentModuleId( $intParentModuleId, $objDatabase ) {
		return self::fetchDefaultReportGroups( sprintf( 'SELECT * FROM default_report_groups WHERE parent_module_id = %d', $intParentModuleId ), $objDatabase );
	}

}
?>