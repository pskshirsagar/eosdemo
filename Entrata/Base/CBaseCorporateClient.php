<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateClient extends CEosSingularBase {

	const TABLE_NAME = 'public.corporate_clients';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strBilltoCompanyName;
	protected $m_strUsername;
	protected $m_strPassword;
	protected $m_strPasswordQuestion;
	protected $m_strPasswordAnswerEncrypted;
	protected $m_intDontAllowLogin;
	protected $m_intDontAcceptPayments;
	protected $m_intInPoorStanding;
	protected $m_intIsResponsibleForPayment;
	protected $m_strLastLogin;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDontAllowLogin = '0';
		$this->m_intDontAcceptPayments = '0';
		$this->m_intInPoorStanding = '0';
		$this->m_intIsResponsibleForPayment = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( stripcslashes( $arrValues['billto_company_name'] ) ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['password_question'] ) && $boolDirectSet ) $this->set( 'm_strPasswordQuestion', trim( stripcslashes( $arrValues['password_question'] ) ) ); elseif( isset( $arrValues['password_question'] ) ) $this->setPasswordQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_question'] ) : $arrValues['password_question'] );
		if( isset( $arrValues['password_answer_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordAnswerEncrypted', trim( stripcslashes( $arrValues['password_answer_encrypted'] ) ) ); elseif( isset( $arrValues['password_answer_encrypted'] ) ) $this->setPasswordAnswerEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_answer_encrypted'] ) : $arrValues['password_answer_encrypted'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['dont_accept_payments'] ) && $boolDirectSet ) $this->set( 'm_intDontAcceptPayments', trim( $arrValues['dont_accept_payments'] ) ); elseif( isset( $arrValues['dont_accept_payments'] ) ) $this->setDontAcceptPayments( $arrValues['dont_accept_payments'] );
		if( isset( $arrValues['in_poor_standing'] ) && $boolDirectSet ) $this->set( 'm_intInPoorStanding', trim( $arrValues['in_poor_standing'] ) ); elseif( isset( $arrValues['in_poor_standing'] ) ) $this->setInPoorStanding( $arrValues['in_poor_standing'] );
		if( isset( $arrValues['is_responsible_for_payment'] ) && $boolDirectSet ) $this->set( 'm_intIsResponsibleForPayment', trim( $arrValues['is_responsible_for_payment'] ) ); elseif( isset( $arrValues['is_responsible_for_payment'] ) ) $this->setIsResponsibleForPayment( $arrValues['is_responsible_for_payment'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 240, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setPasswordQuestion( $strPasswordQuestion ) {
		$this->set( 'm_strPasswordQuestion', CStrings::strTrimDef( $strPasswordQuestion, 240, NULL, true ) );
	}

	public function getPasswordQuestion() {
		return $this->m_strPasswordQuestion;
	}

	public function sqlPasswordQuestion() {
		return ( true == isset( $this->m_strPasswordQuestion ) ) ? '\'' . addslashes( $this->m_strPasswordQuestion ) . '\'' : 'NULL';
	}

	public function setPasswordAnswerEncrypted( $strPasswordAnswerEncrypted ) {
		$this->set( 'm_strPasswordAnswerEncrypted', CStrings::strTrimDef( $strPasswordAnswerEncrypted, 240, NULL, true ) );
	}

	public function getPasswordAnswerEncrypted() {
		return $this->m_strPasswordAnswerEncrypted;
	}

	public function sqlPasswordAnswerEncrypted() {
		return ( true == isset( $this->m_strPasswordAnswerEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordAnswerEncrypted ) . '\'' : 'NULL';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setDontAcceptPayments( $intDontAcceptPayments ) {
		$this->set( 'm_intDontAcceptPayments', CStrings::strToIntDef( $intDontAcceptPayments, NULL, false ) );
	}

	public function getDontAcceptPayments() {
		return $this->m_intDontAcceptPayments;
	}

	public function sqlDontAcceptPayments() {
		return ( true == isset( $this->m_intDontAcceptPayments ) ) ? ( string ) $this->m_intDontAcceptPayments : '0';
	}

	public function setInPoorStanding( $intInPoorStanding ) {
		$this->set( 'm_intInPoorStanding', CStrings::strToIntDef( $intInPoorStanding, NULL, false ) );
	}

	public function getInPoorStanding() {
		return $this->m_intInPoorStanding;
	}

	public function sqlInPoorStanding() {
		return ( true == isset( $this->m_intInPoorStanding ) ) ? ( string ) $this->m_intInPoorStanding : '0';
	}

	public function setIsResponsibleForPayment( $intIsResponsibleForPayment ) {
		$this->set( 'm_intIsResponsibleForPayment', CStrings::strToIntDef( $intIsResponsibleForPayment, NULL, false ) );
	}

	public function getIsResponsibleForPayment() {
		return $this->m_intIsResponsibleForPayment;
	}

	public function sqlIsResponsibleForPayment() {
		return ( true == isset( $this->m_intIsResponsibleForPayment ) ) ? ( string ) $this->m_intIsResponsibleForPayment : '1';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, billto_company_name, username, password, password_question, password_answer_encrypted, dont_allow_login, dont_accept_payments, in_poor_standing, is_responsible_for_payment, last_login, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlBilltoCompanyName() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPassword() . ', ' .
 						$this->sqlPasswordQuestion() . ', ' .
 						$this->sqlPasswordAnswerEncrypted() . ', ' .
 						$this->sqlDontAllowLogin() . ', ' .
 						$this->sqlDontAcceptPayments() . ', ' .
 						$this->sqlInPoorStanding() . ', ' .
 						$this->sqlIsResponsibleForPayment() . ', ' .
 						$this->sqlLastLogin() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; } elseif( true == array_key_exists( 'PasswordQuestion', $this->getChangedColumns() ) ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordAnswerEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; } elseif( true == array_key_exists( 'DontAllowLogin', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; } elseif( true == array_key_exists( 'DontAcceptPayments', $this->getChangedColumns() ) ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' in_poor_standing = ' . $this->sqlInPoorStanding() . ','; } elseif( true == array_key_exists( 'InPoorStanding', $this->getChangedColumns() ) ) { $strSql .= ' in_poor_standing = ' . $this->sqlInPoorStanding() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_for_payment = ' . $this->sqlIsResponsibleForPayment() . ','; } elseif( true == array_key_exists( 'IsResponsibleForPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_responsible_for_payment = ' . $this->sqlIsResponsibleForPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'password_question' => $this->getPasswordQuestion(),
			'password_answer_encrypted' => $this->getPasswordAnswerEncrypted(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'dont_accept_payments' => $this->getDontAcceptPayments(),
			'in_poor_standing' => $this->getInPoorStanding(),
			'is_responsible_for_payment' => $this->getIsResponsibleForPayment(),
			'last_login' => $this->getLastLogin(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>