<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporates extends CEosPluralBase {

	/**
	 * @return CCorporate[]
	 */
	public static function fetchCorporates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCorporate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporate
	 */
	public static function fetchCorporate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCorporate', $objDatabase );
	}

	public static function fetchCorporateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporates', $objDatabase );
	}

	public static function fetchCorporateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporate( sprintf( 'SELECT * FROM corporates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporatesByCid( $intCid, $objDatabase ) {
		return self::fetchCorporates( sprintf( 'SELECT * FROM corporates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporatesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCorporates( sprintf( 'SELECT * FROM corporates WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>