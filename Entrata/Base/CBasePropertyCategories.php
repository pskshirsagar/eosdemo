<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCategories extends CEosPluralBase {

	/**
	 * @return CPropertyCategory[]
	 */
	public static function fetchPropertyCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCategory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCategory
	 */
	public static function fetchPropertyCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCategory', $objDatabase );
	}

	public static function fetchPropertyCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_categories', $objDatabase );
	}

	public static function fetchPropertyCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCategory( sprintf( 'SELECT * FROM property_categories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCategories( sprintf( 'SELECT * FROM property_categories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCategories( sprintf( 'SELECT * FROM property_categories WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCategoriesByCompanyCategoryIdByCid( $intCompanyCategoryId, $intCid, $objDatabase ) {
		return self::fetchPropertyCategories( sprintf( 'SELECT * FROM property_categories WHERE company_category_id = %d AND cid = %d', ( int ) $intCompanyCategoryId, ( int ) $intCid ), $objDatabase );
	}

}
?>