<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceGroups extends CEosPluralBase {

	/**
	 * @return CMaintenanceGroup[]
	 */
	public static function fetchMaintenanceGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceGroup
	 */
	public static function fetchMaintenanceGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceGroup::class, $objDatabase );
	}

	public static function fetchMaintenanceGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_groups', $objDatabase );
	}

	public static function fetchMaintenanceGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroup( sprintf( 'SELECT * FROM maintenance_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroups( sprintf( 'SELECT * FROM maintenance_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceGroupsByParentMaintenanceGroupIdByCid( $intParentMaintenanceGroupId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroups( sprintf( 'SELECT * FROM maintenance_groups WHERE parent_maintenance_group_id = %d AND cid = %d', $intParentMaintenanceGroupId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceGroupsByMaintenanceTemplateIdByCid( $intMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroups( sprintf( 'SELECT * FROM maintenance_groups WHERE maintenance_template_id = %d AND cid = %d', $intMaintenanceTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceGroupsByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroups( sprintf( 'SELECT * FROM maintenance_groups WHERE scheduled_task_id = %d AND cid = %d', $intScheduledTaskId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceGroupsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceGroups( sprintf( 'SELECT * FROM maintenance_groups WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>