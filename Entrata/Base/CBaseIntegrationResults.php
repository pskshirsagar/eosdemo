<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationResults
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationResults extends CEosPluralBase {

	/**
	 * @return CIntegrationResult[]
	 */
	public static function fetchIntegrationResults( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationResult', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationResult
	 */
	public static function fetchIntegrationResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationResult', $objDatabase );
	}

	public static function fetchIntegrationResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_results', $objDatabase );
	}

	public static function fetchIntegrationResultByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResult( sprintf( 'SELECT * FROM integration_results WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByWebMethodIdByCid( $intWebMethodId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE web_method_id = %d AND cid = %d', ( int ) $intWebMethodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByServiceIdByCid( $intServiceId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE service_id = %d AND cid = %d', ( int ) $intServiceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByIntegrationClientIdByCid( $intIntegrationClientId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE integration_client_id = %d AND cid = %d', ( int ) $intIntegrationClientId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByIntegrationServiceIdByCid( $intIntegrationServiceId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE integration_service_id = %d AND cid = %d', ( int ) $intIntegrationServiceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultsByIntegrationQueueIdByCid( $intIntegrationQueueId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResults( sprintf( 'SELECT * FROM integration_results WHERE integration_queue_id = %d AND cid = %d', ( int ) $intIntegrationQueueId, ( int ) $intCid ), $objDatabase );
	}

}
?>