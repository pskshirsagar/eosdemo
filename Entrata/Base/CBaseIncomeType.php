<?php

class CBaseIncomeType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.income_types';

	protected $m_intId;
	protected $m_intIncomeTypeGroupId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strPreferenceKey;
	protected $m_strHudCode;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['income_type_group_id'] ) && $boolDirectSet ) $this->set( 'm_intIncomeTypeGroupId', trim( $arrValues['income_type_group_id'] ) ); elseif( isset( $arrValues['income_type_group_id'] ) ) $this->setIncomeTypeGroupId( $arrValues['income_type_group_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['preference_key'] ) && $boolDirectSet ) $this->set( 'm_strPreferenceKey', trim( stripcslashes( $arrValues['preference_key'] ) ) ); elseif( isset( $arrValues['preference_key'] ) ) $this->setPreferenceKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preference_key'] ) : $arrValues['preference_key'] );
		if( isset( $arrValues['hud_code'] ) && $boolDirectSet ) $this->set( 'm_strHudCode', trim( stripcslashes( $arrValues['hud_code'] ) ) ); elseif( isset( $arrValues['hud_code'] ) ) $this->setHudCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_code'] ) : $arrValues['hud_code'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIncomeTypeGroupId( $intIncomeTypeGroupId ) {
		$this->set( 'm_intIncomeTypeGroupId', CStrings::strToIntDef( $intIncomeTypeGroupId, NULL, false ) );
	}

	public function getIncomeTypeGroupId() {
		return $this->m_intIncomeTypeGroupId;
	}

	public function sqlIncomeTypeGroupId() {
		return ( true == isset( $this->m_intIncomeTypeGroupId ) ) ? ( string ) $this->m_intIncomeTypeGroupId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setPreferenceKey( $strPreferenceKey ) {
		$this->set( 'm_strPreferenceKey', CStrings::strTrimDef( $strPreferenceKey, 24, NULL, true ) );
	}

	public function getPreferenceKey() {
		return $this->m_strPreferenceKey;
	}

	public function sqlPreferenceKey() {
		return ( true == isset( $this->m_strPreferenceKey ) ) ? '\'' . addslashes( $this->m_strPreferenceKey ) . '\'' : 'NULL';
	}

	public function setHudCode( $strHudCode ) {
		$this->set( 'm_strHudCode', CStrings::strTrimDef( $strHudCode, 4, NULL, true ) );
	}

	public function getHudCode() {
		return $this->m_strHudCode;
	}

	public function sqlHudCode() {
		return ( true == isset( $this->m_strHudCode ) ) ? '\'' . addslashes( $this->m_strHudCode ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'income_type_group_id' => $this->getIncomeTypeGroupId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'preference_key' => $this->getPreferenceKey(),
			'hud_code' => $this->getHudCode(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>