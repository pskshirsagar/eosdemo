<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultResidenceTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultResidenceTypes extends CEosPluralBase {

	/**
	 * @return CDefaultResidenceType[]
	 */
	public static function fetchDefaultResidenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultResidenceType::class, $objDatabase );
	}

	/**
	 * @return CDefaultResidenceType
	 */
	public static function fetchDefaultResidenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultResidenceType::class, $objDatabase );
	}

	public static function fetchDefaultResidenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_residence_types', $objDatabase );
	}

	public static function fetchDefaultResidenceTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultResidenceType( sprintf( 'SELECT * FROM default_residence_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>