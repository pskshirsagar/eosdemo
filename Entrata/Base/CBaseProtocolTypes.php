<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProtocolTypes
 * Do not add any new functions to this class.
 */

class CBaseProtocolTypes extends CEosPluralBase {

	/**
	 * @return CProtocolType[]
	 */
	public static function fetchProtocolTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CProtocolType::class, $objDatabase );
	}

	/**
	 * @return CProtocolType
	 */
	public static function fetchProtocolType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProtocolType::class, $objDatabase );
	}

	public static function fetchProtocolTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'protocol_types', $objDatabase );
	}

	public static function fetchProtocolTypeById( $intId, $objDatabase ) {
		return self::fetchProtocolType( sprintf( 'SELECT * FROM protocol_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>