<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyTransmissionVendor extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.company_transmission_vendors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransmissionTypeId;
	protected $m_intTransmissionVendorId;
	protected $m_intTransmissionConnectionTypeId;
	protected $m_intEncryptionTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strEmailAddress;
	protected $m_strClientNumber;
	protected $m_strVendorCode;
	protected $m_strRequestUrl;
	protected $m_strRetrievalUrl;
	protected $m_intPort;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strPublicKey;
	protected $m_strPassphraseEncrypted;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAdditionalNodes;
	protected $m_jsonAdditionalNodes;
	protected $m_strDestinationPath;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transmission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionTypeId', trim( $arrValues['transmission_type_id'] ) ); elseif( isset( $arrValues['transmission_type_id'] ) ) $this->setTransmissionTypeId( $arrValues['transmission_type_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['transmission_connection_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionConnectionTypeId', trim( $arrValues['transmission_connection_type_id'] ) ); elseif( isset( $arrValues['transmission_connection_type_id'] ) ) $this->setTransmissionConnectionTypeId( $arrValues['transmission_connection_type_id'] );
		if( isset( $arrValues['encryption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEncryptionTypeId', trim( $arrValues['encryption_type_id'] ) ); elseif( isset( $arrValues['encryption_type_id'] ) ) $this->setEncryptionTypeId( $arrValues['encryption_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['client_number'] ) && $boolDirectSet ) $this->set( 'm_strClientNumber', trim( $arrValues['client_number'] ) ); elseif( isset( $arrValues['client_number'] ) ) $this->setClientNumber( $arrValues['client_number'] );
		if( isset( $arrValues['vendor_code'] ) && $boolDirectSet ) $this->set( 'm_strVendorCode', trim( $arrValues['vendor_code'] ) ); elseif( isset( $arrValues['vendor_code'] ) ) $this->setVendorCode( $arrValues['vendor_code'] );
		if( isset( $arrValues['request_url'] ) && $boolDirectSet ) $this->set( 'm_strRequestUrl', trim( $arrValues['request_url'] ) ); elseif( isset( $arrValues['request_url'] ) ) $this->setRequestUrl( $arrValues['request_url'] );
		if( isset( $arrValues['retrieval_url'] ) && $boolDirectSet ) $this->set( 'm_strRetrievalUrl', trim( $arrValues['retrieval_url'] ) ); elseif( isset( $arrValues['retrieval_url'] ) ) $this->setRetrievalUrl( $arrValues['retrieval_url'] );
		if( isset( $arrValues['port'] ) && $boolDirectSet ) $this->set( 'm_intPort', trim( $arrValues['port'] ) ); elseif( isset( $arrValues['port'] ) ) $this->setPort( $arrValues['port'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( $arrValues['username_encrypted'] ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['public_key'] ) && $boolDirectSet ) $this->set( 'm_strPublicKey', trim( $arrValues['public_key'] ) ); elseif( isset( $arrValues['public_key'] ) ) $this->setPublicKey( $arrValues['public_key'] );
		if( isset( $arrValues['passphrase_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPassphraseEncrypted', trim( $arrValues['passphrase_encrypted'] ) ); elseif( isset( $arrValues['passphrase_encrypted'] ) ) $this->setPassphraseEncrypted( $arrValues['passphrase_encrypted'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['additional_nodes'] ) ) $this->set( 'm_strAdditionalNodes', trim( $arrValues['additional_nodes'] ) );
		if( isset( $arrValues['destination_path'] ) && $boolDirectSet ) $this->set( 'm_strDestinationPath', trim( $arrValues['destination_path'] ) ); elseif( isset( $arrValues['destination_path'] ) ) $this->setDestinationPath( $arrValues['destination_path'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransmissionTypeId( $intTransmissionTypeId ) {
		$this->set( 'm_intTransmissionTypeId', CStrings::strToIntDef( $intTransmissionTypeId, NULL, false ) );
	}

	public function getTransmissionTypeId() {
		return $this->m_intTransmissionTypeId;
	}

	public function sqlTransmissionTypeId() {
		return ( true == isset( $this->m_intTransmissionTypeId ) ) ? ( string ) $this->m_intTransmissionTypeId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setTransmissionConnectionTypeId( $intTransmissionConnectionTypeId ) {
		$this->set( 'm_intTransmissionConnectionTypeId', CStrings::strToIntDef( $intTransmissionConnectionTypeId, NULL, false ) );
	}

	public function getTransmissionConnectionTypeId() {
		return $this->m_intTransmissionConnectionTypeId;
	}

	public function sqlTransmissionConnectionTypeId() {
		return ( true == isset( $this->m_intTransmissionConnectionTypeId ) ) ? ( string ) $this->m_intTransmissionConnectionTypeId : 'NULL';
	}

	public function setEncryptionTypeId( $intEncryptionTypeId ) {
		$this->set( 'm_intEncryptionTypeId', CStrings::strToIntDef( $intEncryptionTypeId, NULL, false ) );
	}

	public function getEncryptionTypeId() {
		return $this->m_intEncryptionTypeId;
	}

	public function sqlEncryptionTypeId() {
		return ( true == isset( $this->m_intEncryptionTypeId ) ) ? ( string ) $this->m_intEncryptionTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 150, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setClientNumber( $strClientNumber ) {
		$this->set( 'm_strClientNumber', CStrings::strTrimDef( $strClientNumber, 64, NULL, true ) );
	}

	public function getClientNumber() {
		return $this->m_strClientNumber;
	}

	public function sqlClientNumber() {
		return ( true == isset( $this->m_strClientNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strClientNumber ) : '\'' . addslashes( $this->m_strClientNumber ) . '\'' ) : 'NULL';
	}

	public function setVendorCode( $strVendorCode ) {
		$this->set( 'm_strVendorCode', CStrings::strTrimDef( $strVendorCode, 64, NULL, true ) );
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function sqlVendorCode() {
		return ( true == isset( $this->m_strVendorCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorCode ) : '\'' . addslashes( $this->m_strVendorCode ) . '\'' ) : 'NULL';
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->set( 'm_strRequestUrl', CStrings::strTrimDef( $strRequestUrl, 4096, NULL, true ) );
	}

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function sqlRequestUrl() {
		return ( true == isset( $this->m_strRequestUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestUrl ) : '\'' . addslashes( $this->m_strRequestUrl ) . '\'' ) : 'NULL';
	}

	public function setRetrievalUrl( $strRetrievalUrl ) {
		$this->set( 'm_strRetrievalUrl', CStrings::strTrimDef( $strRetrievalUrl, 4096, NULL, true ) );
	}

	public function getRetrievalUrl() {
		return $this->m_strRetrievalUrl;
	}

	public function sqlRetrievalUrl() {
		return ( true == isset( $this->m_strRetrievalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRetrievalUrl ) : '\'' . addslashes( $this->m_strRetrievalUrl ) . '\'' ) : 'NULL';
	}

	public function setPort( $intPort ) {
		$this->set( 'm_intPort', CStrings::strToIntDef( $intPort, NULL, false ) );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function sqlPort() {
		return ( true == isset( $this->m_intPort ) ) ? ( string ) $this->m_intPort : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, 240, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsernameEncrypted ) : '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPublicKey( $strPublicKey ) {
		$this->set( 'm_strPublicKey', CStrings::strTrimDef( $strPublicKey, 240, NULL, true ) );
	}

	public function getPublicKey() {
		return $this->m_strPublicKey;
	}

	public function sqlPublicKey() {
		return ( true == isset( $this->m_strPublicKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPublicKey ) : '\'' . addslashes( $this->m_strPublicKey ) . '\'' ) : 'NULL';
	}

	public function setPassphraseEncrypted( $strPassphraseEncrypted ) {
		$this->set( 'm_strPassphraseEncrypted', CStrings::strTrimDef( $strPassphraseEncrypted, 240, NULL, true ) );
	}

	public function getPassphraseEncrypted() {
		return $this->m_strPassphraseEncrypted;
	}

	public function sqlPassphraseEncrypted() {
		return ( true == isset( $this->m_strPassphraseEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPassphraseEncrypted ) : '\'' . addslashes( $this->m_strPassphraseEncrypted ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAdditionalNodes( $jsonAdditionalNodes ) {
		if( true == valObj( $jsonAdditionalNodes, 'stdClass' ) ) {
			$this->set( 'm_jsonAdditionalNodes', $jsonAdditionalNodes );
		} elseif( true == valJsonString( $jsonAdditionalNodes ) ) {
			$this->set( 'm_jsonAdditionalNodes', CStrings::strToJson( $jsonAdditionalNodes ) );
		} else {
			$this->set( 'm_jsonAdditionalNodes', NULL ); 
		}
		unset( $this->m_strAdditionalNodes );
	}

	public function getAdditionalNodes() {
		if( true == isset( $this->m_strAdditionalNodes ) ) {
			$this->m_jsonAdditionalNodes = CStrings::strToJson( $this->m_strAdditionalNodes );
			unset( $this->m_strAdditionalNodes );
		}
		return $this->m_jsonAdditionalNodes;
	}

	public function sqlAdditionalNodes() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAdditionalNodes() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAdditionalNodes() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAdditionalNodes() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setDestinationPath( $strDestinationPath ) {
		$this->set( 'm_strDestinationPath', CStrings::strTrimDef( $strDestinationPath, 64, NULL, true ) );
	}

	public function getDestinationPath() {
		return $this->m_strDestinationPath;
	}

	public function sqlDestinationPath() {
		return ( true == isset( $this->m_strDestinationPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDestinationPath ) : '\'' . addslashes( $this->m_strDestinationPath ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transmission_type_id, transmission_vendor_id, transmission_connection_type_id, encryption_type_id, name, description, email_address, client_number, vendor_code, request_url, retrieval_url, port, username_encrypted, password_encrypted, public_key, passphrase_encrypted, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, additional_nodes, destination_path, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTransmissionTypeId() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlTransmissionConnectionTypeId() . ', ' .
						$this->sqlEncryptionTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlClientNumber() . ', ' .
						$this->sqlVendorCode() . ', ' .
						$this->sqlRequestUrl() . ', ' .
						$this->sqlRetrievalUrl() . ', ' .
						$this->sqlPort() . ', ' .
						$this->sqlUsernameEncrypted() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlPublicKey() . ', ' .
						$this->sqlPassphraseEncrypted() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAdditionalNodes() . ', ' .
						$this->sqlDestinationPath() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_type_id = ' . $this->sqlTransmissionTypeId(). ',' ; } elseif( true == array_key_exists( 'TransmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_type_id = ' . $this->sqlTransmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_connection_type_id = ' . $this->sqlTransmissionConnectionTypeId(). ',' ; } elseif( true == array_key_exists( 'TransmissionConnectionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_connection_type_id = ' . $this->sqlTransmissionConnectionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encryption_type_id = ' . $this->sqlEncryptionTypeId(). ',' ; } elseif( true == array_key_exists( 'EncryptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' encryption_type_id = ' . $this->sqlEncryptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_number = ' . $this->sqlClientNumber(). ',' ; } elseif( true == array_key_exists( 'ClientNumber', $this->getChangedColumns() ) ) { $strSql .= ' client_number = ' . $this->sqlClientNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode(). ',' ; } elseif( true == array_key_exists( 'VendorCode', $this->getChangedColumns() ) ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl(). ',' ; } elseif( true == array_key_exists( 'RequestUrl', $this->getChangedColumns() ) ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retrieval_url = ' . $this->sqlRetrievalUrl(). ',' ; } elseif( true == array_key_exists( 'RetrievalUrl', $this->getChangedColumns() ) ) { $strSql .= ' retrieval_url = ' . $this->sqlRetrievalUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port = ' . $this->sqlPort(). ',' ; } elseif( true == array_key_exists( 'Port', $this->getChangedColumns() ) ) { $strSql .= ' port = ' . $this->sqlPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' public_key = ' . $this->sqlPublicKey(). ',' ; } elseif( true == array_key_exists( 'PublicKey', $this->getChangedColumns() ) ) { $strSql .= ' public_key = ' . $this->sqlPublicKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' passphrase_encrypted = ' . $this->sqlPassphraseEncrypted(). ',' ; } elseif( true == array_key_exists( 'PassphraseEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' passphrase_encrypted = ' . $this->sqlPassphraseEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_nodes = ' . $this->sqlAdditionalNodes(). ',' ; } elseif( true == array_key_exists( 'AdditionalNodes', $this->getChangedColumns() ) ) { $strSql .= ' additional_nodes = ' . $this->sqlAdditionalNodes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_path = ' . $this->sqlDestinationPath(). ',' ; } elseif( true == array_key_exists( 'DestinationPath', $this->getChangedColumns() ) ) { $strSql .= ' destination_path = ' . $this->sqlDestinationPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transmission_type_id' => $this->getTransmissionTypeId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'transmission_connection_type_id' => $this->getTransmissionConnectionTypeId(),
			'encryption_type_id' => $this->getEncryptionTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'email_address' => $this->getEmailAddress(),
			'client_number' => $this->getClientNumber(),
			'vendor_code' => $this->getVendorCode(),
			'request_url' => $this->getRequestUrl(),
			'retrieval_url' => $this->getRetrievalUrl(),
			'port' => $this->getPort(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'public_key' => $this->getPublicKey(),
			'passphrase_encrypted' => $this->getPassphraseEncrypted(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'additional_nodes' => $this->getAdditionalNodes(),
			'destination_path' => $this->getDestinationPath(),
			'details' => $this->getDetails()
		);
	}

}
?>