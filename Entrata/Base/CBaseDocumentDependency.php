<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentDependency extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.document_dependencies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDocumentId;
	protected $m_intDocumentAddendaId;
	protected $m_intDocumentDependencyTypeId;
	protected $m_intIncludeDocumentAddendaId;
	protected $m_arrintReferenceIds;
	protected $m_intMinLimit;
	protected $m_intMaxLimit;
	protected $m_intOrderNum;
	protected $m_boolIsInclude;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsInclude = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['document_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentAddendaId', trim( $arrValues['document_addenda_id'] ) ); elseif( isset( $arrValues['document_addenda_id'] ) ) $this->setDocumentAddendaId( $arrValues['document_addenda_id'] );
		if( isset( $arrValues['document_dependency_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentDependencyTypeId', trim( $arrValues['document_dependency_type_id'] ) ); elseif( isset( $arrValues['document_dependency_type_id'] ) ) $this->setDocumentDependencyTypeId( $arrValues['document_dependency_type_id'] );
		if( isset( $arrValues['include_document_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intIncludeDocumentAddendaId', trim( $arrValues['include_document_addenda_id'] ) ); elseif( isset( $arrValues['include_document_addenda_id'] ) ) $this->setIncludeDocumentAddendaId( $arrValues['include_document_addenda_id'] );
		if( isset( $arrValues['reference_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintReferenceIds', trim( $arrValues['reference_ids'] ) ); elseif( isset( $arrValues['reference_ids'] ) ) $this->setReferenceIds( $arrValues['reference_ids'] );
		if( isset( $arrValues['min_limit'] ) && $boolDirectSet ) $this->set( 'm_intMinLimit', trim( $arrValues['min_limit'] ) ); elseif( isset( $arrValues['min_limit'] ) ) $this->setMinLimit( $arrValues['min_limit'] );
		if( isset( $arrValues['max_limit'] ) && $boolDirectSet ) $this->set( 'm_intMaxLimit', trim( $arrValues['max_limit'] ) ); elseif( isset( $arrValues['max_limit'] ) ) $this->setMaxLimit( $arrValues['max_limit'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_include'] ) && $boolDirectSet ) $this->set( 'm_boolIsInclude', trim( stripcslashes( $arrValues['is_include'] ) ) ); elseif( isset( $arrValues['is_include'] ) ) $this->setIsInclude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include'] ) : $arrValues['is_include'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDocumentAddendaId( $intDocumentAddendaId ) {
		$this->set( 'm_intDocumentAddendaId', CStrings::strToIntDef( $intDocumentAddendaId, NULL, false ) );
	}

	public function getDocumentAddendaId() {
		return $this->m_intDocumentAddendaId;
	}

	public function sqlDocumentAddendaId() {
		return ( true == isset( $this->m_intDocumentAddendaId ) ) ? ( string ) $this->m_intDocumentAddendaId : 'NULL';
	}

	public function setDocumentDependencyTypeId( $intDocumentDependencyTypeId ) {
		$this->set( 'm_intDocumentDependencyTypeId', CStrings::strToIntDef( $intDocumentDependencyTypeId, NULL, false ) );
	}

	public function getDocumentDependencyTypeId() {
		return $this->m_intDocumentDependencyTypeId;
	}

	public function sqlDocumentDependencyTypeId() {
		return ( true == isset( $this->m_intDocumentDependencyTypeId ) ) ? ( string ) $this->m_intDocumentDependencyTypeId : 'NULL';
	}

	public function setIncludeDocumentAddendaId( $intIncludeDocumentAddendaId ) {
		$this->set( 'm_intIncludeDocumentAddendaId', CStrings::strToIntDef( $intIncludeDocumentAddendaId, NULL, false ) );
	}

	public function getIncludeDocumentAddendaId() {
		return $this->m_intIncludeDocumentAddendaId;
	}

	public function sqlIncludeDocumentAddendaId() {
		return ( true == isset( $this->m_intIncludeDocumentAddendaId ) ) ? ( string ) $this->m_intIncludeDocumentAddendaId : 'NULL';
	}

	public function setReferenceIds( $arrintReferenceIds ) {
		$this->set( 'm_arrintReferenceIds', CStrings::strToArrIntDef( $arrintReferenceIds, NULL ) );
	}

	public function getReferenceIds() {
		return $this->m_arrintReferenceIds;
	}

	public function sqlReferenceIds() {
		return ( true == isset( $this->m_arrintReferenceIds ) && true == valArr( $this->m_arrintReferenceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintReferenceIds, NULL ) . '\'' : 'NULL';
	}

	public function setMinLimit( $intMinLimit ) {
		$this->set( 'm_intMinLimit', CStrings::strToIntDef( $intMinLimit, NULL, false ) );
	}

	public function getMinLimit() {
		return $this->m_intMinLimit;
	}

	public function sqlMinLimit() {
		return ( true == isset( $this->m_intMinLimit ) ) ? ( string ) $this->m_intMinLimit : 'NULL';
	}

	public function setMaxLimit( $intMaxLimit ) {
		$this->set( 'm_intMaxLimit', CStrings::strToIntDef( $intMaxLimit, NULL, false ) );
	}

	public function getMaxLimit() {
		return $this->m_intMaxLimit;
	}

	public function sqlMaxLimit() {
		return ( true == isset( $this->m_intMaxLimit ) ) ? ( string ) $this->m_intMaxLimit : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsInclude( $boolIsInclude ) {
		$this->set( 'm_boolIsInclude', CStrings::strToBool( $boolIsInclude ) );
	}

	public function getIsInclude() {
		return $this->m_boolIsInclude;
	}

	public function sqlIsInclude() {
		return ( true == isset( $this->m_boolIsInclude ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInclude ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, document_id, document_addenda_id, document_dependency_type_id, include_document_addenda_id, reference_ids, min_limit, max_limit, order_num, is_include, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlDocumentAddendaId() . ', ' .
						$this->sqlDocumentDependencyTypeId() . ', ' .
						$this->sqlIncludeDocumentAddendaId() . ', ' .
						$this->sqlReferenceIds() . ', ' .
						$this->sqlMinLimit() . ', ' .
						$this->sqlMaxLimit() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlIsInclude() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId(). ',' ; } elseif( true == array_key_exists( 'DocumentAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_dependency_type_id = ' . $this->sqlDocumentDependencyTypeId(). ',' ; } elseif( true == array_key_exists( 'DocumentDependencyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_dependency_type_id = ' . $this->sqlDocumentDependencyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_document_addenda_id = ' . $this->sqlIncludeDocumentAddendaId(). ',' ; } elseif( true == array_key_exists( 'IncludeDocumentAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' include_document_addenda_id = ' . $this->sqlIncludeDocumentAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_ids = ' . $this->sqlReferenceIds(). ',' ; } elseif( true == array_key_exists( 'ReferenceIds', $this->getChangedColumns() ) ) { $strSql .= ' reference_ids = ' . $this->sqlReferenceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_limit = ' . $this->sqlMinLimit(). ',' ; } elseif( true == array_key_exists( 'MinLimit', $this->getChangedColumns() ) ) { $strSql .= ' min_limit = ' . $this->sqlMinLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_limit = ' . $this->sqlMaxLimit(). ',' ; } elseif( true == array_key_exists( 'MaxLimit', $this->getChangedColumns() ) ) { $strSql .= ' max_limit = ' . $this->sqlMaxLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include = ' . $this->sqlIsInclude(). ',' ; } elseif( true == array_key_exists( 'IsInclude', $this->getChangedColumns() ) ) { $strSql .= ' is_include = ' . $this->sqlIsInclude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'document_id' => $this->getDocumentId(),
			'document_addenda_id' => $this->getDocumentAddendaId(),
			'document_dependency_type_id' => $this->getDocumentDependencyTypeId(),
			'include_document_addenda_id' => $this->getIncludeDocumentAddendaId(),
			'reference_ids' => $this->getReferenceIds(),
			'min_limit' => $this->getMinLimit(),
			'max_limit' => $this->getMaxLimit(),
			'order_num' => $this->getOrderNum(),
			'is_include' => $this->getIsInclude(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>