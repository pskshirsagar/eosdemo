<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExportStatuses
 * Do not add any new functions to this class.
 */

class CBaseFileExportStatuses extends CEosPluralBase {

	/**
	 * @return CFileExportStatus[]
	 */
	public static function fetchFileExportStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFileExportStatus::class, $objDatabase );
	}

	/**
	 * @return CFileExportStatus
	 */
	public static function fetchFileExportStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileExportStatus::class, $objDatabase );
	}

	public static function fetchFileExportStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_export_statuses', $objDatabase );
	}

	public static function fetchFileExportStatusById( $intId, $objDatabase ) {
		return self::fetchFileExportStatus( sprintf( 'SELECT * FROM file_export_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>