<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningNotifications extends CEosPluralBase {

	/**
	 * @return CScreeningNotification[]
	 */
	public static function fetchScreeningNotifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningNotification', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningNotification
	 */
	public static function fetchScreeningNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningNotification', $objDatabase );
	}

	public static function fetchScreeningNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_notifications', $objDatabase );
	}

	public static function fetchScreeningNotificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotification( sprintf( 'SELECT * FROM screening_notifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningNotifications( sprintf( 'SELECT * FROM screening_notifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotifications( sprintf( 'SELECT * FROM screening_notifications WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationsByScreeningNotificationTypeIdByCid( $intScreeningNotificationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotifications( sprintf( 'SELECT * FROM screening_notifications WHERE screening_notification_type_id = %d AND cid = %d', ( int ) $intScreeningNotificationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationsByQuickResponseIdByCid( $intQuickResponseId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotifications( sprintf( 'SELECT * FROM screening_notifications WHERE quick_response_id = %d AND cid = %d', ( int ) $intQuickResponseId, ( int ) $intCid ), $objDatabase );
	}

}
?>