<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSplitBillingTypes
 * Do not add any new functions to this class.
 */

class CBaseSplitBillingTypes extends CEosPluralBase {

	/**
	 * @return CSplitBillingType[]
	 */
	public static function fetchSplitBillingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSplitBillingType::class, $objDatabase );
	}

	/**
	 * @return CSplitBillingType
	 */
	public static function fetchSplitBillingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSplitBillingType::class, $objDatabase );
	}

	public static function fetchSplitBillingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'split_billing_types', $objDatabase );
	}

	public static function fetchSplitBillingTypeById( $intId, $objDatabase ) {
		return self::fetchSplitBillingType( sprintf( 'SELECT * FROM split_billing_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>