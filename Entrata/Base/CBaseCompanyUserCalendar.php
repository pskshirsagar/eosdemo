<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserCalendar extends CEosSingularBase {

	const TABLE_NAME = 'public.company_user_calendars';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intCalendarPropertyId;
	protected $m_intCalendarCompanyUserId;
	protected $m_strCalendarColor;
	protected $m_boolIsVisible;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVisible = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['calendar_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCalendarPropertyId', trim( $arrValues['calendar_property_id'] ) ); elseif( isset( $arrValues['calendar_property_id'] ) ) $this->setCalendarPropertyId( $arrValues['calendar_property_id'] );
		if( isset( $arrValues['calendar_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCalendarCompanyUserId', trim( $arrValues['calendar_company_user_id'] ) ); elseif( isset( $arrValues['calendar_company_user_id'] ) ) $this->setCalendarCompanyUserId( $arrValues['calendar_company_user_id'] );
		if( isset( $arrValues['calendar_color'] ) && $boolDirectSet ) $this->set( 'm_strCalendarColor', trim( stripcslashes( $arrValues['calendar_color'] ) ) ); elseif( isset( $arrValues['calendar_color'] ) ) $this->setCalendarColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['calendar_color'] ) : $arrValues['calendar_color'] );
		if( isset( $arrValues['is_visible'] ) && $boolDirectSet ) $this->set( 'm_boolIsVisible', trim( stripcslashes( $arrValues['is_visible'] ) ) ); elseif( isset( $arrValues['is_visible'] ) ) $this->setIsVisible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_visible'] ) : $arrValues['is_visible'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setCalendarPropertyId( $intCalendarPropertyId ) {
		$this->set( 'm_intCalendarPropertyId', CStrings::strToIntDef( $intCalendarPropertyId, NULL, false ) );
	}

	public function getCalendarPropertyId() {
		return $this->m_intCalendarPropertyId;
	}

	public function sqlCalendarPropertyId() {
		return ( true == isset( $this->m_intCalendarPropertyId ) ) ? ( string ) $this->m_intCalendarPropertyId : 'NULL';
	}

	public function setCalendarCompanyUserId( $intCalendarCompanyUserId ) {
		$this->set( 'm_intCalendarCompanyUserId', CStrings::strToIntDef( $intCalendarCompanyUserId, NULL, false ) );
	}

	public function getCalendarCompanyUserId() {
		return $this->m_intCalendarCompanyUserId;
	}

	public function sqlCalendarCompanyUserId() {
		return ( true == isset( $this->m_intCalendarCompanyUserId ) ) ? ( string ) $this->m_intCalendarCompanyUserId : 'NULL';
	}

	public function setCalendarColor( $strCalendarColor ) {
		$this->set( 'm_strCalendarColor', CStrings::strTrimDef( $strCalendarColor, 6, NULL, true ) );
	}

	public function getCalendarColor() {
		return $this->m_strCalendarColor;
	}

	public function sqlCalendarColor() {
		return ( true == isset( $this->m_strCalendarColor ) ) ? '\'' . addslashes( $this->m_strCalendarColor ) . '\'' : 'NULL';
	}

	public function setIsVisible( $boolIsVisible ) {
		$this->set( 'm_boolIsVisible', CStrings::strToBool( $boolIsVisible ) );
	}

	public function getIsVisible() {
		return $this->m_boolIsVisible;
	}

	public function sqlIsVisible() {
		return ( true == isset( $this->m_boolIsVisible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVisible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, calendar_property_id, calendar_company_user_id, calendar_color, is_visible, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlCalendarPropertyId() . ', ' .
 						$this->sqlCalendarCompanyUserId() . ', ' .
 						$this->sqlCalendarColor() . ', ' .
 						$this->sqlIsVisible() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_property_id = ' . $this->sqlCalendarPropertyId() . ','; } elseif( true == array_key_exists( 'CalendarPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' calendar_property_id = ' . $this->sqlCalendarPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_company_user_id = ' . $this->sqlCalendarCompanyUserId() . ','; } elseif( true == array_key_exists( 'CalendarCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' calendar_company_user_id = ' . $this->sqlCalendarCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_color = ' . $this->sqlCalendarColor() . ','; } elseif( true == array_key_exists( 'CalendarColor', $this->getChangedColumns() ) ) { $strSql .= ' calendar_color = ' . $this->sqlCalendarColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible() . ','; } elseif( true == array_key_exists( 'IsVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'calendar_property_id' => $this->getCalendarPropertyId(),
			'calendar_company_user_id' => $this->getCalendarCompanyUserId(),
			'calendar_color' => $this->getCalendarColor(),
			'is_visible' => $this->getIsVisible(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>