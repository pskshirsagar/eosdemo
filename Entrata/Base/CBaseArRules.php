<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArRules extends CEosPluralBase {

    const TABLE_AR_RULES = 'public.ar_rules';

    public static function fetchArRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CArRule', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchArRule( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArRule', $objDatabase );
    }

    public static function fetchArRuleCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_rules', $objDatabase );
    }

    public static function fetchArRuleByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchArRule( sprintf( 'SELECT * FROM ar_rules WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArRulesByCid( $intCid, $objDatabase ) {
        return self::fetchArRules( sprintf( 'SELECT * FROM ar_rules WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchArRulesByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
        return self::fetchArRules( sprintf( 'SELECT * FROM ar_rules WHERE ar_origin_id = %d AND cid = %d', (int) $intArOriginId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArRulesByArCodeTypeIdByCid( $intArCodeTypeId, $intCid, $objDatabase ) {
        return self::fetchArRules( sprintf( 'SELECT * FROM ar_rules WHERE ar_code_type_id = %d AND cid = %d', (int) $intArCodeTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArRulesByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
        return self::fetchArRules( sprintf( 'SELECT * FROM ar_rules WHERE ar_trigger_id = %d AND cid = %d', (int) $intArTriggerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArRulesByArActionIdByCid( $intArActionId, $intCid, $objDatabase ) {
        return self::fetchArRules( sprintf( 'SELECT * FROM ar_rules WHERE ar_action_id = %d AND cid = %d', (int) $intArActionId, (int) $intCid ), $objDatabase );
    }

}
?>