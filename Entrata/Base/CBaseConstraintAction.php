<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseConstraintAction extends CEosSingularBase {

	const TABLE_NAME = 'public.constraint_actions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRevenueConstraintId;
	protected $m_strConditionResult;
	protected $m_strType;
	protected $m_strOutput;
	protected $m_strMessage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['revenue_constraint_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueConstraintId', trim( $arrValues['revenue_constraint_id'] ) ); elseif( isset( $arrValues['revenue_constraint_id'] ) ) $this->setRevenueConstraintId( $arrValues['revenue_constraint_id'] );
		if( isset( $arrValues['condition_result'] ) && $boolDirectSet ) $this->set( 'm_strConditionResult', trim( stripcslashes( $arrValues['condition_result'] ) ) ); elseif( isset( $arrValues['condition_result'] ) ) $this->setConditionResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_result'] ) : $arrValues['condition_result'] );
		if( isset( $arrValues['type'] ) && $boolDirectSet ) $this->set( 'm_strType', trim( stripcslashes( $arrValues['type'] ) ) ); elseif( isset( $arrValues['type'] ) ) $this->setType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type'] ) : $arrValues['type'] );
		if( isset( $arrValues['output'] ) && $boolDirectSet ) $this->set( 'm_strOutput', trim( stripcslashes( $arrValues['output'] ) ) ); elseif( isset( $arrValues['output'] ) ) $this->setOutput( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['output'] ) : $arrValues['output'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRevenueConstraintId( $intRevenueConstraintId ) {
		$this->set( 'm_intRevenueConstraintId', CStrings::strToIntDef( $intRevenueConstraintId, NULL, false ) );
	}

	public function getRevenueConstraintId() {
		return $this->m_intRevenueConstraintId;
	}

	public function sqlRevenueConstraintId() {
		return ( true == isset( $this->m_intRevenueConstraintId ) ) ? ( string ) $this->m_intRevenueConstraintId : 'NULL';
	}

	public function setConditionResult( $strConditionResult ) {
		$this->set( 'm_strConditionResult', CStrings::strTrimDef( $strConditionResult, 50, NULL, true ) );
	}

	public function getConditionResult() {
		return $this->m_strConditionResult;
	}

	public function sqlConditionResult() {
		return ( true == isset( $this->m_strConditionResult ) ) ? '\'' . addslashes( $this->m_strConditionResult ) . '\'' : 'NULL';
	}

	public function setType( $strType ) {
		$this->set( 'm_strType', CStrings::strTrimDef( $strType, 50, NULL, true ) );
	}

	public function getType() {
		return $this->m_strType;
	}

	public function sqlType() {
		return ( true == isset( $this->m_strType ) ) ? '\'' . addslashes( $this->m_strType ) . '\'' : 'NULL';
	}

	public function setOutput( $strOutput ) {
		$this->set( 'm_strOutput', CStrings::strTrimDef( $strOutput, 50, NULL, true ) );
	}

	public function getOutput() {
		return $this->m_strOutput;
	}

	public function sqlOutput() {
		return ( true == isset( $this->m_strOutput ) ) ? '\'' . addslashes( $this->m_strOutput ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, 50, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, revenue_constraint_id, condition_result, type, output, message, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRevenueConstraintId() . ', ' .
 						$this->sqlConditionResult() . ', ' .
 						$this->sqlType() . ', ' .
 						$this->sqlOutput() . ', ' .
 						$this->sqlMessage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_constraint_id = ' . $this->sqlRevenueConstraintId() . ','; } elseif( true == array_key_exists( 'RevenueConstraintId', $this->getChangedColumns() ) ) { $strSql .= ' revenue_constraint_id = ' . $this->sqlRevenueConstraintId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_result = ' . $this->sqlConditionResult() . ','; } elseif( true == array_key_exists( 'ConditionResult', $this->getChangedColumns() ) ) { $strSql .= ' condition_result = ' . $this->sqlConditionResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type = ' . $this->sqlType() . ','; } elseif( true == array_key_exists( 'Type', $this->getChangedColumns() ) ) { $strSql .= ' type = ' . $this->sqlType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' output = ' . $this->sqlOutput() . ','; } elseif( true == array_key_exists( 'Output', $this->getChangedColumns() ) ) { $strSql .= ' output = ' . $this->sqlOutput() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'revenue_constraint_id' => $this->getRevenueConstraintId(),
			'condition_result' => $this->getConditionResult(),
			'type' => $this->getType(),
			'output' => $this->getOutput(),
			'message' => $this->getMessage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>