<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobPhases
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobPhases extends CEosPluralBase {

	/**
	 * @return CJobPhase[]
	 */
	public static function fetchJobPhases( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobPhase', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobPhase
	 */
	public static function fetchJobPhase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPhase', $objDatabase );
	}

	public static function fetchJobPhaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_phases', $objDatabase );
	}

	public static function fetchJobPhaseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobPhase( sprintf( 'SELECT * FROM job_phases WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobPhasesByCid( $intCid, $objDatabase ) {
		return self::fetchJobPhases( sprintf( 'SELECT * FROM job_phases WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobPhasesByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobPhases( sprintf( 'SELECT * FROM job_phases WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobPhasesByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchJobPhases( sprintf( 'SELECT * FROM job_phases WHERE ap_header_id = %d AND cid = %d', ( int ) $intApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobPhasesByReClassGlHeaderIdByCid( $intReClassGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchJobPhases( sprintf( 'SELECT * FROM job_phases WHERE re_class_gl_header_id = %d AND cid = %d', ( int ) $intReClassGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

}
?>