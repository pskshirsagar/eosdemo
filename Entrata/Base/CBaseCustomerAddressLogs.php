<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAddressLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAddressLogs extends CEosPluralBase {

	/**
	 * @return CCustomerAddressLog[]
	 */
	public static function fetchCustomerAddressLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerAddressLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAddressLog
	 */
	public static function fetchCustomerAddressLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAddressLog::class, $objDatabase );
	}

	public static function fetchCustomerAddressLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_address_logs', $objDatabase );
	}

	public static function fetchCustomerAddressLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLog( sprintf( 'SELECT * FROM customer_address_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByCustomerAddressIdByCid( $intCustomerAddressId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE customer_address_id = %d AND cid = %d', $intCustomerAddressId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByPriorCustomerAddressLogIdByCid( $intPriorCustomerAddressLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE prior_customer_address_log_id = %d AND cid = %d', $intPriorCustomerAddressLogId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE address_type_id = %d AND cid = %d', $intAddressTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByCompanyResidenceTypeIdByCid( $intCompanyResidenceTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE company_residence_type_id = %d AND cid = %d', $intCompanyResidenceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByTimeZoneIdByCid( $intTimeZoneId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE time_zone_id = %d AND cid = %d', $intTimeZoneId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAddressLogsByMoveOutReasonListItemIdByCid( $intMoveOutReasonListItemId, $intCid, $objDatabase ) {
		return self::fetchCustomerAddressLogs( sprintf( 'SELECT * FROM customer_address_logs WHERE move_out_reason_list_item_id = %d AND cid = %d', $intMoveOutReasonListItemId, $intCid ), $objDatabase );
	}

}
?>