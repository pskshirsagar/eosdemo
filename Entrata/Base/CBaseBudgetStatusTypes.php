<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetStatusTypes extends CEosPluralBase {

	/**
	 * @return CBudgetStatusType[]
	 */
	public static function fetchBudgetStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetStatusType::class, $objDatabase );
	}

	/**
	 * @return CBudgetStatusType
	 */
	public static function fetchBudgetStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetStatusType::class, $objDatabase );
	}

	public static function fetchBudgetStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_status_types', $objDatabase );
	}

	public static function fetchBudgetStatusTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetStatusType( sprintf( 'SELECT * FROM budget_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>