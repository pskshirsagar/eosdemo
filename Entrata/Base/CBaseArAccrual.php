<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArAccrual extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_accruals';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intPropertyInterestFormulaId;
	protected $m_intArAccrualBatchId;
	protected $m_intArTransactionId;
	protected $m_intAccruedArTransactionId;
	protected $m_intAccruedArAllocationId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strAccrualDatetime;
	protected $m_fltAccrualAmount;
	protected $m_boolIsForfeited;
	protected $m_boolIsDeleted;
	protected $m_intWaivedBy;
	protected $m_strWaivedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAccrualAmount = '0';
		$this->m_boolIsForfeited = false;
		$this->m_boolIsDeleted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['property_interest_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyInterestFormulaId', trim( $arrValues['property_interest_formula_id'] ) ); elseif( isset( $arrValues['property_interest_formula_id'] ) ) $this->setPropertyInterestFormulaId( $arrValues['property_interest_formula_id'] );
		if( isset( $arrValues['ar_accrual_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intArAccrualBatchId', trim( $arrValues['ar_accrual_batch_id'] ) ); elseif( isset( $arrValues['ar_accrual_batch_id'] ) ) $this->setArAccrualBatchId( $arrValues['ar_accrual_batch_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['accrued_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intAccruedArTransactionId', trim( $arrValues['accrued_ar_transaction_id'] ) ); elseif( isset( $arrValues['accrued_ar_transaction_id'] ) ) $this->setAccruedArTransactionId( $arrValues['accrued_ar_transaction_id'] );
		if( isset( $arrValues['accrued_ar_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intAccruedArAllocationId', trim( $arrValues['accrued_ar_allocation_id'] ) ); elseif( isset( $arrValues['accrued_ar_allocation_id'] ) ) $this->setAccruedArAllocationId( $arrValues['accrued_ar_allocation_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['accrual_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAccrualDatetime', trim( $arrValues['accrual_datetime'] ) ); elseif( isset( $arrValues['accrual_datetime'] ) ) $this->setAccrualDatetime( $arrValues['accrual_datetime'] );
		if( isset( $arrValues['accrual_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAccrualAmount', trim( $arrValues['accrual_amount'] ) ); elseif( isset( $arrValues['accrual_amount'] ) ) $this->setAccrualAmount( $arrValues['accrual_amount'] );
		if( isset( $arrValues['is_forfeited'] ) && $boolDirectSet ) $this->set( 'm_boolIsForfeited', trim( stripcslashes( $arrValues['is_forfeited'] ) ) ); elseif( isset( $arrValues['is_forfeited'] ) ) $this->setIsForfeited( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_forfeited'] ) : $arrValues['is_forfeited'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['waived_by'] ) && $boolDirectSet ) $this->set( 'm_intWaivedBy', trim( $arrValues['waived_by'] ) ); elseif( isset( $arrValues['waived_by'] ) ) $this->setWaivedBy( $arrValues['waived_by'] );
		if( isset( $arrValues['waived_on'] ) && $boolDirectSet ) $this->set( 'm_strWaivedOn', trim( $arrValues['waived_on'] ) ); elseif( isset( $arrValues['waived_on'] ) ) $this->setWaivedOn( $arrValues['waived_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPropertyInterestFormulaId( $intPropertyInterestFormulaId ) {
		$this->set( 'm_intPropertyInterestFormulaId', CStrings::strToIntDef( $intPropertyInterestFormulaId, NULL, false ) );
	}

	public function getPropertyInterestFormulaId() {
		return $this->m_intPropertyInterestFormulaId;
	}

	public function sqlPropertyInterestFormulaId() {
		return ( true == isset( $this->m_intPropertyInterestFormulaId ) ) ? ( string ) $this->m_intPropertyInterestFormulaId : 'NULL';
	}

	public function setArAccrualBatchId( $intArAccrualBatchId ) {
		$this->set( 'm_intArAccrualBatchId', CStrings::strToIntDef( $intArAccrualBatchId, NULL, false ) );
	}

	public function getArAccrualBatchId() {
		return $this->m_intArAccrualBatchId;
	}

	public function sqlArAccrualBatchId() {
		return ( true == isset( $this->m_intArAccrualBatchId ) ) ? ( string ) $this->m_intArAccrualBatchId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setAccruedArTransactionId( $intAccruedArTransactionId ) {
		$this->set( 'm_intAccruedArTransactionId', CStrings::strToIntDef( $intAccruedArTransactionId, NULL, false ) );
	}

	public function getAccruedArTransactionId() {
		return $this->m_intAccruedArTransactionId;
	}

	public function sqlAccruedArTransactionId() {
		return ( true == isset( $this->m_intAccruedArTransactionId ) ) ? ( string ) $this->m_intAccruedArTransactionId : 'NULL';
	}

	public function setAccruedArAllocationId( $intAccruedArAllocationId ) {
		$this->set( 'm_intAccruedArAllocationId', CStrings::strToIntDef( $intAccruedArAllocationId, NULL, false ) );
	}

	public function getAccruedArAllocationId() {
		return $this->m_intAccruedArAllocationId;
	}

	public function sqlAccruedArAllocationId() {
		return ( true == isset( $this->m_intAccruedArAllocationId ) ) ? ( string ) $this->m_intAccruedArAllocationId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function setAccrualDatetime( $strAccrualDatetime ) {
		$this->set( 'm_strAccrualDatetime', CStrings::strTrimDef( $strAccrualDatetime, -1, NULL, true ) );
	}

	public function getAccrualDatetime() {
		return $this->m_strAccrualDatetime;
	}

	public function sqlAccrualDatetime() {
		return ( true == isset( $this->m_strAccrualDatetime ) ) ? '\'' . $this->m_strAccrualDatetime . '\'' : 'NOW()';
	}

	public function setAccrualAmount( $fltAccrualAmount ) {
		$this->set( 'm_fltAccrualAmount', CStrings::strToFloatDef( $fltAccrualAmount, NULL, false, 2 ) );
	}

	public function getAccrualAmount() {
		return $this->m_fltAccrualAmount;
	}

	public function sqlAccrualAmount() {
		return ( true == isset( $this->m_fltAccrualAmount ) ) ? ( string ) $this->m_fltAccrualAmount : '0';
	}

	public function setIsForfeited( $boolIsForfeited ) {
		$this->set( 'm_boolIsForfeited', CStrings::strToBool( $boolIsForfeited ) );
	}

	public function getIsForfeited() {
		return $this->m_boolIsForfeited;
	}

	public function sqlIsForfeited() {
		return ( true == isset( $this->m_boolIsForfeited ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsForfeited ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWaivedBy( $intWaivedBy ) {
		$this->set( 'm_intWaivedBy', CStrings::strToIntDef( $intWaivedBy, NULL, false ) );
	}

	public function getWaivedBy() {
		return $this->m_intWaivedBy;
	}

	public function sqlWaivedBy() {
		return ( true == isset( $this->m_intWaivedBy ) ) ? ( string ) $this->m_intWaivedBy : 'NULL';
	}

	public function setWaivedOn( $strWaivedOn ) {
		$this->set( 'm_strWaivedOn', CStrings::strTrimDef( $strWaivedOn, -1, NULL, true ) );
	}

	public function getWaivedOn() {
		return $this->m_strWaivedOn;
	}

	public function sqlWaivedOn() {
		return ( true == isset( $this->m_strWaivedOn ) ) ? '\'' . $this->m_strWaivedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, property_interest_formula_id, ar_accrual_batch_id, ar_transaction_id, accrued_ar_transaction_id, accrued_ar_allocation_id, start_date, end_date, accrual_datetime, accrual_amount, is_forfeited, is_deleted, waived_by, waived_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlPropertyInterestFormulaId() . ', ' .
 						$this->sqlArAccrualBatchId() . ', ' .
 						$this->sqlArTransactionId() . ', ' .
 						$this->sqlAccruedArTransactionId() . ', ' .
 						$this->sqlAccruedArAllocationId() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlAccrualDatetime() . ', ' .
 						$this->sqlAccrualAmount() . ', ' .
 						$this->sqlIsForfeited() . ', ' .
 						$this->sqlIsDeleted() . ', ' .
 						$this->sqlWaivedBy() . ', ' .
 						$this->sqlWaivedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_interest_formula_id = ' . $this->sqlPropertyInterestFormulaId() . ','; } elseif( true == array_key_exists( 'PropertyInterestFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' property_interest_formula_id = ' . $this->sqlPropertyInterestFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_accrual_batch_id = ' . $this->sqlArAccrualBatchId() . ','; } elseif( true == array_key_exists( 'ArAccrualBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ar_accrual_batch_id = ' . $this->sqlArAccrualBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrued_ar_transaction_id = ' . $this->sqlAccruedArTransactionId() . ','; } elseif( true == array_key_exists( 'AccruedArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' accrued_ar_transaction_id = ' . $this->sqlAccruedArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrued_ar_allocation_id = ' . $this->sqlAccruedArAllocationId() . ','; } elseif( true == array_key_exists( 'AccruedArAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' accrued_ar_allocation_id = ' . $this->sqlAccruedArAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_datetime = ' . $this->sqlAccrualDatetime() . ','; } elseif( true == array_key_exists( 'AccrualDatetime', $this->getChangedColumns() ) ) { $strSql .= ' accrual_datetime = ' . $this->sqlAccrualDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_amount = ' . $this->sqlAccrualAmount() . ','; } elseif( true == array_key_exists( 'AccrualAmount', $this->getChangedColumns() ) ) { $strSql .= ' accrual_amount = ' . $this->sqlAccrualAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_forfeited = ' . $this->sqlIsForfeited() . ','; } elseif( true == array_key_exists( 'IsForfeited', $this->getChangedColumns() ) ) { $strSql .= ' is_forfeited = ' . $this->sqlIsForfeited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_by = ' . $this->sqlWaivedBy() . ','; } elseif( true == array_key_exists( 'WaivedBy', $this->getChangedColumns() ) ) { $strSql .= ' waived_by = ' . $this->sqlWaivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_on = ' . $this->sqlWaivedOn() . ','; } elseif( true == array_key_exists( 'WaivedOn', $this->getChangedColumns() ) ) { $strSql .= ' waived_on = ' . $this->sqlWaivedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'property_interest_formula_id' => $this->getPropertyInterestFormulaId(),
			'ar_accrual_batch_id' => $this->getArAccrualBatchId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'accrued_ar_transaction_id' => $this->getAccruedArTransactionId(),
			'accrued_ar_allocation_id' => $this->getAccruedArAllocationId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'accrual_datetime' => $this->getAccrualDatetime(),
			'accrual_amount' => $this->getAccrualAmount(),
			'is_forfeited' => $this->getIsForfeited(),
			'is_deleted' => $this->getIsDeleted(),
			'waived_by' => $this->getWaivedBy(),
			'waived_on' => $this->getWaivedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>