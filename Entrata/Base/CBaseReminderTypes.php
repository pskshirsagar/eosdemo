<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReminderTypes
 * Do not add any new functions to this class.
 */

class CBaseReminderTypes extends CEosPluralBase {

	/**
	 * @return CReminderType[]
	 */
	public static function fetchReminderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReminderType::class, $objDatabase );
	}

	/**
	 * @return CReminderType
	 */
	public static function fetchReminderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReminderType::class, $objDatabase );
	}

	public static function fetchReminderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reminder_types', $objDatabase );
	}

	public static function fetchReminderTypeById( $intId, $objDatabase ) {
		return self::fetchReminderType( sprintf( 'SELECT * FROM reminder_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>