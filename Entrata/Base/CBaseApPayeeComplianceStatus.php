<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeComplianceStatus extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_payee_compliance_statuses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApLegalEntityId;
	protected $m_intPropertyId;
	protected $m_intComplianceStatusId;
	protected $m_boolAllowNotCompliantPurchaseOrders;
	protected $m_boolAllowRequiredPurchaseOrders;
	protected $m_boolAllowPendingPurchaseOrders;
	protected $m_boolAllowNotCompliantInvoices;
	protected $m_boolAllowRequiredInvoices;
	protected $m_boolAllowPendingInvoices;
	protected $m_boolAllowNotCompliantPayments;
	protected $m_boolAllowRequiredPayments;
	protected $m_boolAllowPendingPayments;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowNotCompliantPurchaseOrders = false;
		$this->m_boolAllowRequiredPurchaseOrders = false;
		$this->m_boolAllowPendingPurchaseOrders = false;
		$this->m_boolAllowNotCompliantInvoices = false;
		$this->m_boolAllowRequiredInvoices = false;
		$this->m_boolAllowPendingInvoices = false;
		$this->m_boolAllowNotCompliantPayments = false;
		$this->m_boolAllowRequiredPayments = false;
		$this->m_boolAllowPendingPayments = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_legal_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intApLegalEntityId', trim( $arrValues['ap_legal_entity_id'] ) ); elseif( isset( $arrValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrValues['ap_legal_entity_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['compliance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceStatusId', trim( $arrValues['compliance_status_id'] ) ); elseif( isset( $arrValues['compliance_status_id'] ) ) $this->setComplianceStatusId( $arrValues['compliance_status_id'] );
		if( isset( $arrValues['allow_not_compliant_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantPurchaseOrders', trim( stripcslashes( $arrValues['allow_not_compliant_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_purchase_orders'] ) ) $this->setAllowNotCompliantPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_purchase_orders'] ) : $arrValues['allow_not_compliant_purchase_orders'] );
		if( isset( $arrValues['allow_required_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredPurchaseOrders', trim( stripcslashes( $arrValues['allow_required_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_required_purchase_orders'] ) ) $this->setAllowRequiredPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_purchase_orders'] ) : $arrValues['allow_required_purchase_orders'] );
		if( isset( $arrValues['allow_pending_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingPurchaseOrders', trim( stripcslashes( $arrValues['allow_pending_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_pending_purchase_orders'] ) ) $this->setAllowPendingPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_purchase_orders'] ) : $arrValues['allow_pending_purchase_orders'] );
		if( isset( $arrValues['allow_not_compliant_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantInvoices', trim( stripcslashes( $arrValues['allow_not_compliant_invoices'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_invoices'] ) ) $this->setAllowNotCompliantInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_invoices'] ) : $arrValues['allow_not_compliant_invoices'] );
		if( isset( $arrValues['allow_required_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredInvoices', trim( stripcslashes( $arrValues['allow_required_invoices'] ) ) ); elseif( isset( $arrValues['allow_required_invoices'] ) ) $this->setAllowRequiredInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_invoices'] ) : $arrValues['allow_required_invoices'] );
		if( isset( $arrValues['allow_pending_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingInvoices', trim( stripcslashes( $arrValues['allow_pending_invoices'] ) ) ); elseif( isset( $arrValues['allow_pending_invoices'] ) ) $this->setAllowPendingInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_invoices'] ) : $arrValues['allow_pending_invoices'] );
		if( isset( $arrValues['allow_not_compliant_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantPayments', trim( stripcslashes( $arrValues['allow_not_compliant_payments'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_payments'] ) ) $this->setAllowNotCompliantPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_payments'] ) : $arrValues['allow_not_compliant_payments'] );
		if( isset( $arrValues['allow_required_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredPayments', trim( stripcslashes( $arrValues['allow_required_payments'] ) ) ); elseif( isset( $arrValues['allow_required_payments'] ) ) $this->setAllowRequiredPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_payments'] ) : $arrValues['allow_required_payments'] );
		if( isset( $arrValues['allow_pending_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingPayments', trim( stripcslashes( $arrValues['allow_pending_payments'] ) ) ); elseif( isset( $arrValues['allow_pending_payments'] ) ) $this->setAllowPendingPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_payments'] ) : $arrValues['allow_pending_payments'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->set( 'm_intApLegalEntityId', CStrings::strToIntDef( $intApLegalEntityId, NULL, false ) );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function sqlApLegalEntityId() {
		return ( true == isset( $this->m_intApLegalEntityId ) ) ? ( string ) $this->m_intApLegalEntityId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setComplianceStatusId( $intComplianceStatusId ) {
		$this->set( 'm_intComplianceStatusId', CStrings::strToIntDef( $intComplianceStatusId, NULL, false ) );
	}

	public function getComplianceStatusId() {
		return $this->m_intComplianceStatusId;
	}

	public function sqlComplianceStatusId() {
		return ( true == isset( $this->m_intComplianceStatusId ) ) ? ( string ) $this->m_intComplianceStatusId : 'NULL';
	}

	public function setAllowNotCompliantPurchaseOrders( $boolAllowNotCompliantPurchaseOrders ) {
		$this->set( 'm_boolAllowNotCompliantPurchaseOrders', CStrings::strToBool( $boolAllowNotCompliantPurchaseOrders ) );
	}

	public function getAllowNotCompliantPurchaseOrders() {
		return $this->m_boolAllowNotCompliantPurchaseOrders;
	}

	public function sqlAllowNotCompliantPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowNotCompliantPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredPurchaseOrders( $boolAllowRequiredPurchaseOrders ) {
		$this->set( 'm_boolAllowRequiredPurchaseOrders', CStrings::strToBool( $boolAllowRequiredPurchaseOrders ) );
	}

	public function getAllowRequiredPurchaseOrders() {
		return $this->m_boolAllowRequiredPurchaseOrders;
	}

	public function sqlAllowRequiredPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowRequiredPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingPurchaseOrders( $boolAllowPendingPurchaseOrders ) {
		$this->set( 'm_boolAllowPendingPurchaseOrders', CStrings::strToBool( $boolAllowPendingPurchaseOrders ) );
	}

	public function getAllowPendingPurchaseOrders() {
		return $this->m_boolAllowPendingPurchaseOrders;
	}

	public function sqlAllowPendingPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowPendingPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotCompliantInvoices( $boolAllowNotCompliantInvoices ) {
		$this->set( 'm_boolAllowNotCompliantInvoices', CStrings::strToBool( $boolAllowNotCompliantInvoices ) );
	}

	public function getAllowNotCompliantInvoices() {
		return $this->m_boolAllowNotCompliantInvoices;
	}

	public function sqlAllowNotCompliantInvoices() {
		return ( true == isset( $this->m_boolAllowNotCompliantInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredInvoices( $boolAllowRequiredInvoices ) {
		$this->set( 'm_boolAllowRequiredInvoices', CStrings::strToBool( $boolAllowRequiredInvoices ) );
	}

	public function getAllowRequiredInvoices() {
		return $this->m_boolAllowRequiredInvoices;
	}

	public function sqlAllowRequiredInvoices() {
		return ( true == isset( $this->m_boolAllowRequiredInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingInvoices( $boolAllowPendingInvoices ) {
		$this->set( 'm_boolAllowPendingInvoices', CStrings::strToBool( $boolAllowPendingInvoices ) );
	}

	public function getAllowPendingInvoices() {
		return $this->m_boolAllowPendingInvoices;
	}

	public function sqlAllowPendingInvoices() {
		return ( true == isset( $this->m_boolAllowPendingInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotCompliantPayments( $boolAllowNotCompliantPayments ) {
		$this->set( 'm_boolAllowNotCompliantPayments', CStrings::strToBool( $boolAllowNotCompliantPayments ) );
	}

	public function getAllowNotCompliantPayments() {
		return $this->m_boolAllowNotCompliantPayments;
	}

	public function sqlAllowNotCompliantPayments() {
		return ( true == isset( $this->m_boolAllowNotCompliantPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredPayments( $boolAllowRequiredPayments ) {
		$this->set( 'm_boolAllowRequiredPayments', CStrings::strToBool( $boolAllowRequiredPayments ) );
	}

	public function getAllowRequiredPayments() {
		return $this->m_boolAllowRequiredPayments;
	}

	public function sqlAllowRequiredPayments() {
		return ( true == isset( $this->m_boolAllowRequiredPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingPayments( $boolAllowPendingPayments ) {
		$this->set( 'm_boolAllowPendingPayments', CStrings::strToBool( $boolAllowPendingPayments ) );
	}

	public function getAllowPendingPayments() {
		return $this->m_boolAllowPendingPayments;
	}

	public function sqlAllowPendingPayments() {
		return ( true == isset( $this->m_boolAllowPendingPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_legal_entity_id, property_id, compliance_status_id, allow_not_compliant_purchase_orders, allow_required_purchase_orders, allow_pending_purchase_orders, allow_not_compliant_invoices, allow_required_invoices, allow_pending_invoices, allow_not_compliant_payments, allow_required_payments, allow_pending_payments, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApLegalEntityId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlComplianceStatusId() . ', ' .
 						$this->sqlAllowNotCompliantPurchaseOrders() . ', ' .
 						$this->sqlAllowRequiredPurchaseOrders() . ', ' .
 						$this->sqlAllowPendingPurchaseOrders() . ', ' .
 						$this->sqlAllowNotCompliantInvoices() . ', ' .
 						$this->sqlAllowRequiredInvoices() . ', ' .
 						$this->sqlAllowPendingInvoices() . ', ' .
 						$this->sqlAllowNotCompliantPayments() . ', ' .
 						$this->sqlAllowRequiredPayments() . ', ' .
 						$this->sqlAllowPendingPayments() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; } elseif( true == array_key_exists( 'ApLegalEntityId', $this->getChangedColumns() ) ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; } elseif( true == array_key_exists( 'ComplianceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_purchase_orders = ' . $this->sqlAllowNotCompliantPurchaseOrders() . ','; } elseif( true == array_key_exists( 'AllowNotCompliantPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_purchase_orders = ' . $this->sqlAllowNotCompliantPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_purchase_orders = ' . $this->sqlAllowRequiredPurchaseOrders() . ','; } elseif( true == array_key_exists( 'AllowRequiredPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_purchase_orders = ' . $this->sqlAllowRequiredPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_purchase_orders = ' . $this->sqlAllowPendingPurchaseOrders() . ','; } elseif( true == array_key_exists( 'AllowPendingPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_purchase_orders = ' . $this->sqlAllowPendingPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_invoices = ' . $this->sqlAllowNotCompliantInvoices() . ','; } elseif( true == array_key_exists( 'AllowNotCompliantInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_invoices = ' . $this->sqlAllowNotCompliantInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_invoices = ' . $this->sqlAllowRequiredInvoices() . ','; } elseif( true == array_key_exists( 'AllowRequiredInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_invoices = ' . $this->sqlAllowRequiredInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_invoices = ' . $this->sqlAllowPendingInvoices() . ','; } elseif( true == array_key_exists( 'AllowPendingInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_invoices = ' . $this->sqlAllowPendingInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_payments = ' . $this->sqlAllowNotCompliantPayments() . ','; } elseif( true == array_key_exists( 'AllowNotCompliantPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_payments = ' . $this->sqlAllowNotCompliantPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_payments = ' . $this->sqlAllowRequiredPayments() . ','; } elseif( true == array_key_exists( 'AllowRequiredPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_payments = ' . $this->sqlAllowRequiredPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_payments = ' . $this->sqlAllowPendingPayments() . ','; } elseif( true == array_key_exists( 'AllowPendingPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_payments = ' . $this->sqlAllowPendingPayments() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_legal_entity_id' => $this->getApLegalEntityId(),
			'property_id' => $this->getPropertyId(),
			'compliance_status_id' => $this->getComplianceStatusId(),
			'allow_not_compliant_purchase_orders' => $this->getAllowNotCompliantPurchaseOrders(),
			'allow_required_purchase_orders' => $this->getAllowRequiredPurchaseOrders(),
			'allow_pending_purchase_orders' => $this->getAllowPendingPurchaseOrders(),
			'allow_not_compliant_invoices' => $this->getAllowNotCompliantInvoices(),
			'allow_required_invoices' => $this->getAllowRequiredInvoices(),
			'allow_pending_invoices' => $this->getAllowPendingInvoices(),
			'allow_not_compliant_payments' => $this->getAllowNotCompliantPayments(),
			'allow_required_payments' => $this->getAllowRequiredPayments(),
			'allow_pending_payments' => $this->getAllowPendingPayments(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>