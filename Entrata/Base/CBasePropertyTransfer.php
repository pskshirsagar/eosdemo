<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransfer extends CEosSingularBase {

	const TABLE_NAME = 'public.property_transfers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMigrationTokenId;
	protected $m_intPropertyTransferStatusTypeId;
	protected $m_strTransferName;
	protected $m_strStartOn;
	protected $m_strEmail;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPartial;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPartial = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['migration_token_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationTokenId', trim( $arrValues['migration_token_id'] ) ); elseif( isset( $arrValues['migration_token_id'] ) ) $this->setMigrationTokenId( $arrValues['migration_token_id'] );
		if( isset( $arrValues['property_transfer_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTransferStatusTypeId', trim( $arrValues['property_transfer_status_type_id'] ) ); elseif( isset( $arrValues['property_transfer_status_type_id'] ) ) $this->setPropertyTransferStatusTypeId( $arrValues['property_transfer_status_type_id'] );
		if( isset( $arrValues['transfer_name'] ) && $boolDirectSet ) $this->set( 'm_strTransferName', trim( stripcslashes( $arrValues['transfer_name'] ) ) ); elseif( isset( $arrValues['transfer_name'] ) ) $this->setTransferName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transfer_name'] ) : $arrValues['transfer_name'] );
		if( isset( $arrValues['start_on'] ) && $boolDirectSet ) $this->set( 'm_strStartOn', trim( $arrValues['start_on'] ) ); elseif( isset( $arrValues['start_on'] ) ) $this->setStartOn( $arrValues['start_on'] );
		if( isset( $arrValues['email'] ) && $boolDirectSet ) $this->set( 'm_strEmail', trim( stripcslashes( $arrValues['email'] ) ) ); elseif( isset( $arrValues['email'] ) ) $this->setEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email'] ) : $arrValues['email'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_partial'] ) && $boolDirectSet ) $this->set( 'm_boolIsPartial', trim( stripcslashes( $arrValues['is_partial'] ) ) ); elseif( isset( $arrValues['is_partial'] ) ) $this->setIsPartial( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_partial'] ) : $arrValues['is_partial'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMigrationTokenId( $intMigrationTokenId ) {
		$this->set( 'm_intMigrationTokenId', CStrings::strToIntDef( $intMigrationTokenId, NULL, false ) );
	}

	public function getMigrationTokenId() {
		return $this->m_intMigrationTokenId;
	}

	public function sqlMigrationTokenId() {
		return ( true == isset( $this->m_intMigrationTokenId ) ) ? ( string ) $this->m_intMigrationTokenId : 'NULL';
	}

	public function setPropertyTransferStatusTypeId( $intPropertyTransferStatusTypeId ) {
		$this->set( 'm_intPropertyTransferStatusTypeId', CStrings::strToIntDef( $intPropertyTransferStatusTypeId, NULL, false ) );
	}

	public function getPropertyTransferStatusTypeId() {
		return $this->m_intPropertyTransferStatusTypeId;
	}

	public function sqlPropertyTransferStatusTypeId() {
		return ( true == isset( $this->m_intPropertyTransferStatusTypeId ) ) ? ( string ) $this->m_intPropertyTransferStatusTypeId : 'NULL';
	}

	public function setTransferName( $strTransferName ) {
		$this->set( 'm_strTransferName', CStrings::strTrimDef( $strTransferName, 50, NULL, true ) );
	}

	public function getTransferName() {
		return $this->m_strTransferName;
	}

	public function sqlTransferName() {
		return ( true == isset( $this->m_strTransferName ) ) ? '\'' . addslashes( $this->m_strTransferName ) . '\'' : 'NULL';
	}

	public function setStartOn( $strStartOn ) {
		$this->set( 'm_strStartOn', CStrings::strTrimDef( $strStartOn, -1, NULL, true ) );
	}

	public function getStartOn() {
		return $this->m_strStartOn;
	}

	public function sqlStartOn() {
		return ( true == isset( $this->m_strStartOn ) ) ? '\'' . $this->m_strStartOn . '\'' : 'NOW()';
	}

	public function setEmail( $strEmail ) {
		$this->set( 'm_strEmail', CStrings::strTrimDef( $strEmail, 250, NULL, true ) );
	}

	public function getEmail() {
		return $this->m_strEmail;
	}

	public function sqlEmail() {
		return ( true == isset( $this->m_strEmail ) ) ? '\'' . addslashes( $this->m_strEmail ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPartial( $boolIsPartial ) {
		$this->set( 'm_boolIsPartial', CStrings::strToBool( $boolIsPartial ) );
	}

	public function getIsPartial() {
		return $this->m_boolIsPartial;
	}

	public function sqlIsPartial() {
		return ( true == isset( $this->m_boolIsPartial ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPartial ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, migration_token_id, property_transfer_status_type_id, transfer_name, start_on, email, updated_by, updated_on, created_by, created_on, is_partial )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMigrationTokenId() . ', ' .
 						$this->sqlPropertyTransferStatusTypeId() . ', ' .
 						$this->sqlTransferName() . ', ' .
 						$this->sqlStartOn() . ', ' .
 						$this->sqlEmail() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsPartial() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_token_id = ' . $this->sqlMigrationTokenId() . ','; } elseif( true == array_key_exists( 'MigrationTokenId', $this->getChangedColumns() ) ) { $strSql .= ' migration_token_id = ' . $this->sqlMigrationTokenId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_transfer_status_type_id = ' . $this->sqlPropertyTransferStatusTypeId() . ','; } elseif( true == array_key_exists( 'PropertyTransferStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_transfer_status_type_id = ' . $this->sqlPropertyTransferStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_name = ' . $this->sqlTransferName() . ','; } elseif( true == array_key_exists( 'TransferName', $this->getChangedColumns() ) ) { $strSql .= ' transfer_name = ' . $this->sqlTransferName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_on = ' . $this->sqlStartOn() . ','; } elseif( true == array_key_exists( 'StartOn', $this->getChangedColumns() ) ) { $strSql .= ' start_on = ' . $this->sqlStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; } elseif( true == array_key_exists( 'Email', $this->getChangedColumns() ) ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_partial = ' . $this->sqlIsPartial() . ','; } elseif( true == array_key_exists( 'IsPartial', $this->getChangedColumns() ) ) { $strSql .= ' is_partial = ' . $this->sqlIsPartial() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'migration_token_id' => $this->getMigrationTokenId(),
			'property_transfer_status_type_id' => $this->getPropertyTransferStatusTypeId(),
			'transfer_name' => $this->getTransferName(),
			'start_on' => $this->getStartOn(),
			'email' => $this->getEmail(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_partial' => $this->getIsPartial()
		);
	}

}
?>