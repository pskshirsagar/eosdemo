<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySsnExceptionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySsnExceptionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySsnExceptionType[]
	 */
	public static function fetchSubsidySsnExceptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySsnExceptionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySsnExceptionType
	 */
	public static function fetchSubsidySsnExceptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySsnExceptionType::class, $objDatabase );
	}

	public static function fetchSubsidySsnExceptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_ssn_exception_types', $objDatabase );
	}

	public static function fetchSubsidySsnExceptionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySsnExceptionType( sprintf( 'SELECT * FROM subsidy_ssn_exception_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>