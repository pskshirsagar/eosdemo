<?php

class CBaseDefaultPropertyMaintenanceLocationProblem extends CEosSingularBase {

	const TABLE_NAME = 'public.default_property_maintenance_location_problems';

	protected $m_intId;
	protected $m_intDefaultMaintenanceLocationId;
	protected $m_intDefaultMaintenanceProblemId;
	protected $m_intIsSystem;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceLocationId', trim( $arrValues['default_maintenance_location_id'] ) ); elseif( isset( $arrValues['default_maintenance_location_id'] ) ) $this->setDefaultMaintenanceLocationId( $arrValues['default_maintenance_location_id'] );
		if( isset( $arrValues['default_maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceProblemId', trim( $arrValues['default_maintenance_problem_id'] ) ); elseif( isset( $arrValues['default_maintenance_problem_id'] ) ) $this->setDefaultMaintenanceProblemId( $arrValues['default_maintenance_problem_id'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultMaintenanceLocationId( $intDefaultMaintenanceLocationId ) {
		$this->set( 'm_intDefaultMaintenanceLocationId', CStrings::strToIntDef( $intDefaultMaintenanceLocationId, NULL, false ) );
	}

	public function getDefaultMaintenanceLocationId() {
		return $this->m_intDefaultMaintenanceLocationId;
	}

	public function sqlDefaultMaintenanceLocationId() {
		return ( true == isset( $this->m_intDefaultMaintenanceLocationId ) ) ? ( string ) $this->m_intDefaultMaintenanceLocationId : 'NULL';
	}

	public function setDefaultMaintenanceProblemId( $intDefaultMaintenanceProblemId ) {
		$this->set( 'm_intDefaultMaintenanceProblemId', CStrings::strToIntDef( $intDefaultMaintenanceProblemId, NULL, false ) );
	}

	public function getDefaultMaintenanceProblemId() {
		return $this->m_intDefaultMaintenanceProblemId;
	}

	public function sqlDefaultMaintenanceProblemId() {
		return ( true == isset( $this->m_intDefaultMaintenanceProblemId ) ) ? ( string ) $this->m_intDefaultMaintenanceProblemId : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_maintenance_location_id' => $this->getDefaultMaintenanceLocationId(),
			'default_maintenance_problem_id' => $this->getDefaultMaintenanceProblemId(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>