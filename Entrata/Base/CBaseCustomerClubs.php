<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClubs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerClubs extends CEosPluralBase {

	/**
	 * @return CCustomerClub[]
	 */
	public static function fetchCustomerClubs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerClub', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerClub
	 */
	public static function fetchCustomerClub( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerClub', $objDatabase );
	}

	public static function fetchCustomerClubCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_clubs', $objDatabase );
	}

	public static function fetchCustomerClubByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerClub( sprintf( 'SELECT * FROM customer_clubs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClubsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerClubs( sprintf( 'SELECT * FROM customer_clubs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClubsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerClubs( sprintf( 'SELECT * FROM customer_clubs WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClubsByClubIdByCid( $intClubId, $intCid, $objDatabase ) {
		return self::fetchCustomerClubs( sprintf( 'SELECT * FROM customer_clubs WHERE club_id = %d AND cid = %d', ( int ) $intClubId, ( int ) $intCid ), $objDatabase );
	}

}
?>