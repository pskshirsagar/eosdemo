<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAssetLog extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_asset_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intCustomerAssetId;
	protected $m_intCustomerId;
	protected $m_intCustomerAssetTypeId;
	protected $m_intPriorCustomerAssetLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strNameOnAccount;
	protected $m_strAccountNumberEncrypted;
	protected $m_strRoutingNumber;
	protected $m_strInstitutionName;
	protected $m_strInstitutionStreetLine1;
	protected $m_strInstitutionStreetLine2;
	protected $m_strInstitutionStreetLine3;
	protected $m_strInstitutionCity;
	protected $m_strInstitutionStateCode;
	protected $m_strInstitutionProvince;
	protected $m_strInstitutionPostalCode;
	protected $m_strInstitutionCountryCode;
	protected $m_strContactName;
	protected $m_strContactPhoneNumber;
	protected $m_strContactPhoneExtension;
	protected $m_strContactFaxNumber;
	protected $m_strContactEmail;
	protected $m_fltMarketValue;
	protected $m_fltDisposalAmount;
	protected $m_fltCostToDispose;
	protected $m_fltDebtAmount;
	protected $m_fltCashValue;
	protected $m_fltInterestRate;
	protected $m_fltActualIncome;
	protected $m_strAssetEffectiveDate;
	protected $m_strDateEnded;
	protected $m_strDateDivested;
	protected $m_strLogDatetime;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_boolIsDisqualifyingDisposalReason;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';
		$this->m_boolIsDisqualifyingDisposalReason = false;
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strInstitutionStreetLine1' => 'addressLine1',
				'm_strInstitutionStreetLine2' => 'addressLine2',
				'm_strInstitutionStreetLine3' => 'addressLine3',
				'm_strInstitutionCity' => 'locality',
				'm_strInstitutionStateCode' => 'administrativeArea',
				'm_strInstitutionPostalCode' => 'postalCode',
				'm_strInstitutionCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['customer_asset_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerAssetId', trim( $arrValues['customer_asset_id'] ) ); elseif( isset( $arrValues['customer_asset_id'] ) ) $this->setCustomerAssetId( $arrValues['customer_asset_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_asset_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerAssetTypeId', trim( $arrValues['customer_asset_type_id'] ) ); elseif( isset( $arrValues['customer_asset_type_id'] ) ) $this->setCustomerAssetTypeId( $arrValues['customer_asset_type_id'] );
		if( isset( $arrValues['prior_customer_asset_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerAssetLogId', trim( $arrValues['prior_customer_asset_log_id'] ) ); elseif( isset( $arrValues['prior_customer_asset_log_id'] ) ) $this->setPriorCustomerAssetLogId( $arrValues['prior_customer_asset_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strNameOnAccount', trim( stripcslashes( $arrValues['name_on_account'] ) ) ); elseif( isset( $arrValues['name_on_account'] ) ) $this->setNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_on_account'] ) : $arrValues['name_on_account'] );
		if( isset( $arrValues['account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberEncrypted', trim( stripcslashes( $arrValues['account_number_encrypted'] ) ) ); elseif( isset( $arrValues['account_number_encrypted'] ) ) $this->setAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_encrypted'] ) : $arrValues['account_number_encrypted'] );
		if( isset( $arrValues['routing_number'] ) && $boolDirectSet ) $this->set( 'm_strRoutingNumber', trim( stripcslashes( $arrValues['routing_number'] ) ) ); elseif( isset( $arrValues['routing_number'] ) ) $this->setRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['routing_number'] ) : $arrValues['routing_number'] );
		if( isset( $arrValues['institution_name'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionName', trim( stripcslashes( $arrValues['institution_name'] ) ) ); elseif( isset( $arrValues['institution_name'] ) ) $this->setInstitutionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_name'] ) : $arrValues['institution_name'] );
		if( isset( $arrValues['institution_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine1', trim( stripcslashes( $arrValues['institution_street_line1'] ) ) ); elseif( isset( $arrValues['institution_street_line1'] ) ) $this->setInstitutionStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line1'] ) : $arrValues['institution_street_line1'] );
		if( isset( $arrValues['institution_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine2', trim( stripcslashes( $arrValues['institution_street_line2'] ) ) ); elseif( isset( $arrValues['institution_street_line2'] ) ) $this->setInstitutionStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line2'] ) : $arrValues['institution_street_line2'] );
		if( isset( $arrValues['institution_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine3', trim( stripcslashes( $arrValues['institution_street_line3'] ) ) ); elseif( isset( $arrValues['institution_street_line3'] ) ) $this->setInstitutionStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line3'] ) : $arrValues['institution_street_line3'] );
		if( isset( $arrValues['institution_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCity', trim( stripcslashes( $arrValues['institution_city'] ) ) ); elseif( isset( $arrValues['institution_city'] ) ) $this->setInstitutionCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_city'] ) : $arrValues['institution_city'] );
		if( isset( $arrValues['institution_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStateCode', trim( stripcslashes( $arrValues['institution_state_code'] ) ) ); elseif( isset( $arrValues['institution_state_code'] ) ) $this->setInstitutionStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_state_code'] ) : $arrValues['institution_state_code'] );
		if( isset( $arrValues['institution_province'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionProvince', trim( stripcslashes( $arrValues['institution_province'] ) ) ); elseif( isset( $arrValues['institution_province'] ) ) $this->setInstitutionProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_province'] ) : $arrValues['institution_province'] );
		if( isset( $arrValues['institution_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionPostalCode', trim( stripcslashes( $arrValues['institution_postal_code'] ) ) ); elseif( isset( $arrValues['institution_postal_code'] ) ) $this->setInstitutionPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_postal_code'] ) : $arrValues['institution_postal_code'] );
		if( isset( $arrValues['institution_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCountryCode', trim( stripcslashes( $arrValues['institution_country_code'] ) ) ); elseif( isset( $arrValues['institution_country_code'] ) ) $this->setInstitutionCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_country_code'] ) : $arrValues['institution_country_code'] );
		if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->set( 'm_strContactName', trim( stripcslashes( $arrValues['contact_name'] ) ) ); elseif( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
		if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneNumber', trim( stripcslashes( $arrValues['contact_phone_number'] ) ) ); elseif( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_number'] ) : $arrValues['contact_phone_number'] );
		if( isset( $arrValues['contact_phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneExtension', trim( stripcslashes( $arrValues['contact_phone_extension'] ) ) ); elseif( isset( $arrValues['contact_phone_extension'] ) ) $this->setContactPhoneExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_extension'] ) : $arrValues['contact_phone_extension'] );
		if( isset( $arrValues['contact_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strContactFaxNumber', trim( stripcslashes( $arrValues['contact_fax_number'] ) ) ); elseif( isset( $arrValues['contact_fax_number'] ) ) $this->setContactFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_fax_number'] ) : $arrValues['contact_fax_number'] );
		if( isset( $arrValues['contact_email'] ) && $boolDirectSet ) $this->set( 'm_strContactEmail', trim( stripcslashes( $arrValues['contact_email'] ) ) ); elseif( isset( $arrValues['contact_email'] ) ) $this->setContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_email'] ) : $arrValues['contact_email'] );
		if( isset( $arrValues['market_value'] ) && $boolDirectSet ) $this->set( 'm_fltMarketValue', trim( $arrValues['market_value'] ) ); elseif( isset( $arrValues['market_value'] ) ) $this->setMarketValue( $arrValues['market_value'] );
		if( isset( $arrValues['disposal_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDisposalAmount', trim( $arrValues['disposal_amount'] ) ); elseif( isset( $arrValues['disposal_amount'] ) ) $this->setDisposalAmount( $arrValues['disposal_amount'] );
		if( isset( $arrValues['cost_to_dispose'] ) && $boolDirectSet ) $this->set( 'm_fltCostToDispose', trim( $arrValues['cost_to_dispose'] ) ); elseif( isset( $arrValues['cost_to_dispose'] ) ) $this->setCostToDispose( $arrValues['cost_to_dispose'] );
		if( isset( $arrValues['debt_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDebtAmount', trim( $arrValues['debt_amount'] ) ); elseif( isset( $arrValues['debt_amount'] ) ) $this->setDebtAmount( $arrValues['debt_amount'] );
		if( isset( $arrValues['cash_value'] ) && $boolDirectSet ) $this->set( 'm_fltCashValue', trim( $arrValues['cash_value'] ) ); elseif( isset( $arrValues['cash_value'] ) ) $this->setCashValue( $arrValues['cash_value'] );
		if( isset( $arrValues['interest_rate'] ) && $boolDirectSet ) $this->set( 'm_fltInterestRate', trim( $arrValues['interest_rate'] ) ); elseif( isset( $arrValues['interest_rate'] ) ) $this->setInterestRate( $arrValues['interest_rate'] );
		if( isset( $arrValues['actual_income'] ) && $boolDirectSet ) $this->set( 'm_fltActualIncome', trim( $arrValues['actual_income'] ) ); elseif( isset( $arrValues['actual_income'] ) ) $this->setActualIncome( $arrValues['actual_income'] );
		if( isset( $arrValues['asset_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strAssetEffectiveDate', trim( $arrValues['asset_effective_date'] ) ); elseif( isset( $arrValues['asset_effective_date'] ) ) $this->setAssetEffectiveDate( $arrValues['asset_effective_date'] );
		if( isset( $arrValues['date_ended'] ) && $boolDirectSet ) $this->set( 'm_strDateEnded', trim( $arrValues['date_ended'] ) ); elseif( isset( $arrValues['date_ended'] ) ) $this->setDateEnded( $arrValues['date_ended'] );
		if( isset( $arrValues['date_divested'] ) && $boolDirectSet ) $this->set( 'm_strDateDivested', trim( $arrValues['date_divested'] ) ); elseif( isset( $arrValues['date_divested'] ) ) $this->setDateDivested( $arrValues['date_divested'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['is_disqualifying_disposal_reason'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisqualifyingDisposalReason', trim( stripcslashes( $arrValues['is_disqualifying_disposal_reason'] ) ) ); elseif( isset( $arrValues['is_disqualifying_disposal_reason'] ) ) $this->setIsDisqualifyingDisposalReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disqualifying_disposal_reason'] ) : $arrValues['is_disqualifying_disposal_reason'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setCustomerAssetId( $intCustomerAssetId ) {
		$this->set( 'm_intCustomerAssetId', CStrings::strToIntDef( $intCustomerAssetId, NULL, false ) );
	}

	public function getCustomerAssetId() {
		return $this->m_intCustomerAssetId;
	}

	public function sqlCustomerAssetId() {
		return ( true == isset( $this->m_intCustomerAssetId ) ) ? ( string ) $this->m_intCustomerAssetId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerAssetTypeId( $intCustomerAssetTypeId ) {
		$this->set( 'm_intCustomerAssetTypeId', CStrings::strToIntDef( $intCustomerAssetTypeId, NULL, false ) );
	}

	public function getCustomerAssetTypeId() {
		return $this->m_intCustomerAssetTypeId;
	}

	public function sqlCustomerAssetTypeId() {
		return ( true == isset( $this->m_intCustomerAssetTypeId ) ) ? ( string ) $this->m_intCustomerAssetTypeId : 'NULL';
	}

	public function setPriorCustomerAssetLogId( $intPriorCustomerAssetLogId ) {
		$this->set( 'm_intPriorCustomerAssetLogId', CStrings::strToIntDef( $intPriorCustomerAssetLogId, NULL, false ) );
	}

	public function getPriorCustomerAssetLogId() {
		return $this->m_intPriorCustomerAssetLogId;
	}

	public function sqlPriorCustomerAssetLogId() {
		return ( true == isset( $this->m_intPriorCustomerAssetLogId ) ) ? ( string ) $this->m_intPriorCustomerAssetLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNameOnAccount( $strNameOnAccount ) {
		$this->set( 'm_strNameOnAccount', CStrings::strTrimDef( $strNameOnAccount, 50, NULL, true ) );
	}

	public function getNameOnAccount() {
		return $this->m_strNameOnAccount;
	}

	public function sqlNameOnAccount() {
		return ( true == isset( $this->m_strNameOnAccount ) ) ? '\'' . addslashes( $this->m_strNameOnAccount ) . '\'' : 'NULL';
	}

	public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
		$this->set( 'm_strAccountNumberEncrypted', CStrings::strTrimDef( $strAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getAccountNumberEncrypted() {
		return $this->m_strAccountNumberEncrypted;
	}

	public function sqlAccountNumberEncrypted() {
		return ( true == isset( $this->m_strAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setRoutingNumber( $strRoutingNumber ) {
		$this->set( 'm_strRoutingNumber', CStrings::strTrimDef( $strRoutingNumber, 240, NULL, true ) );
	}

	public function getRoutingNumber() {
		return $this->m_strRoutingNumber;
	}

	public function sqlRoutingNumber() {
		return ( true == isset( $this->m_strRoutingNumber ) ) ? '\'' . addslashes( $this->m_strRoutingNumber ) . '\'' : 'NULL';
	}

	public function setInstitutionName( $strInstitutionName ) {
		$this->set( 'm_strInstitutionName', CStrings::strTrimDef( $strInstitutionName, 100, NULL, true ) );
	}

	public function getInstitutionName() {
		return $this->m_strInstitutionName;
	}

	public function sqlInstitutionName() {
		return ( true == isset( $this->m_strInstitutionName ) ) ? '\'' . addslashes( $this->m_strInstitutionName ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine1( $strInstitutionStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strInstitutionStreetLine1, $strAddressKey = 'default', 'm_strInstitutionStreetLine1' );
	}

	public function getInstitutionStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strInstitutionStreetLine1' );
	}

	public function sqlInstitutionStreetLine1() {
		return ( true == isset( $this->m_strInstitutionStreetLine1 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine1 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine2( $strInstitutionStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strInstitutionStreetLine2, $strAddressKey = 'default', 'm_strInstitutionStreetLine2' );
	}

	public function getInstitutionStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strInstitutionStreetLine2' );
	}

	public function sqlInstitutionStreetLine2() {
		return ( true == isset( $this->m_strInstitutionStreetLine2 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine2 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine3( $strInstitutionStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strInstitutionStreetLine3, $strAddressKey = 'default', 'm_strInstitutionStreetLine3' );
	}

	public function getInstitutionStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strInstitutionStreetLine3' );
	}

	public function sqlInstitutionStreetLine3() {
		return ( true == isset( $this->m_strInstitutionStreetLine3 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine3 ) . '\'' : 'NULL';
	}

	public function setInstitutionCity( $strInstitutionCity ) {
		$this->setPostalAddressField( 'locality', $strInstitutionCity, $strAddressKey = 'default', 'm_strInstitutionCity' );
	}

	public function getInstitutionCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strInstitutionCity' );
	}

	public function sqlInstitutionCity() {
		return ( true == isset( $this->m_strInstitutionCity ) ) ? '\'' . addslashes( $this->m_strInstitutionCity ) . '\'' : 'NULL';
	}

	public function setInstitutionStateCode( $strInstitutionStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strInstitutionStateCode, $strAddressKey = 'default', 'm_strInstitutionStateCode' );
	}

	public function getInstitutionStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strInstitutionStateCode' );
	}

	public function sqlInstitutionStateCode() {
		return ( true == isset( $this->m_strInstitutionStateCode ) ) ? '\'' . addslashes( $this->m_strInstitutionStateCode ) . '\'' : 'NULL';
	}

	public function setInstitutionProvince( $strInstitutionProvince ) {
		$this->set( 'm_strInstitutionProvince', CStrings::strTrimDef( $strInstitutionProvince, 50, NULL, true ) );
	}

	public function getInstitutionProvince() {
		return $this->m_strInstitutionProvince;
	}

	public function sqlInstitutionProvince() {
		return ( true == isset( $this->m_strInstitutionProvince ) ) ? '\'' . addslashes( $this->m_strInstitutionProvince ) . '\'' : 'NULL';
	}

	public function setInstitutionPostalCode( $strInstitutionPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strInstitutionPostalCode, $strAddressKey = 'default', 'm_strInstitutionPostalCode' );
	}

	public function getInstitutionPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strInstitutionPostalCode' );
	}

	public function sqlInstitutionPostalCode() {
		return ( true == isset( $this->m_strInstitutionPostalCode ) ) ? '\'' . addslashes( $this->m_strInstitutionPostalCode ) . '\'' : 'NULL';
	}

	public function setInstitutionCountryCode( $strInstitutionCountryCode ) {
		$this->setPostalAddressField( 'country', $strInstitutionCountryCode, $strAddressKey = 'default', 'm_strInstitutionCountryCode' );
	}

	public function getInstitutionCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strInstitutionCountryCode' );
	}

	public function sqlInstitutionCountryCode() {
		return ( true == isset( $this->m_strInstitutionCountryCode ) ) ? '\'' . addslashes( $this->m_strInstitutionCountryCode ) . '\'' : 'NULL';
	}

	public function setContactName( $strContactName ) {
		$this->set( 'm_strContactName', CStrings::strTrimDef( $strContactName, 50, NULL, true ) );
	}

	public function getContactName() {
		return $this->m_strContactName;
	}

	public function sqlContactName() {
		return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
	}

	public function setContactPhoneNumber( $strContactPhoneNumber ) {
		$this->set( 'm_strContactPhoneNumber', CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true ) );
	}

	public function getContactPhoneNumber() {
		return $this->m_strContactPhoneNumber;
	}

	public function sqlContactPhoneNumber() {
		return ( true == isset( $this->m_strContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' : 'NULL';
	}

	public function setContactPhoneExtension( $strContactPhoneExtension ) {
		$this->set( 'm_strContactPhoneExtension', CStrings::strTrimDef( $strContactPhoneExtension, 6, NULL, true ) );
	}

	public function getContactPhoneExtension() {
		return $this->m_strContactPhoneExtension;
	}

	public function sqlContactPhoneExtension() {
		return ( true == isset( $this->m_strContactPhoneExtension ) ) ? '\'' . addslashes( $this->m_strContactPhoneExtension ) . '\'' : 'NULL';
	}

	public function setContactFaxNumber( $strContactFaxNumber ) {
		$this->set( 'm_strContactFaxNumber', CStrings::strTrimDef( $strContactFaxNumber, 30, NULL, true ) );
	}

	public function getContactFaxNumber() {
		return $this->m_strContactFaxNumber;
	}

	public function sqlContactFaxNumber() {
		return ( true == isset( $this->m_strContactFaxNumber ) ) ? '\'' . addslashes( $this->m_strContactFaxNumber ) . '\'' : 'NULL';
	}

	public function setContactEmail( $strContactEmail ) {
		$this->set( 'm_strContactEmail', CStrings::strTrimDef( $strContactEmail, 240, NULL, true ) );
	}

	public function getContactEmail() {
		return $this->m_strContactEmail;
	}

	public function sqlContactEmail() {
		return ( true == isset( $this->m_strContactEmail ) ) ? '\'' . addslashes( $this->m_strContactEmail ) . '\'' : 'NULL';
	}

	public function setMarketValue( $fltMarketValue ) {
		$this->set( 'm_fltMarketValue', CStrings::strToFloatDef( $fltMarketValue, NULL, false, 4 ) );
	}

	public function getMarketValue() {
		return $this->m_fltMarketValue;
	}

	public function sqlMarketValue() {
		return ( true == isset( $this->m_fltMarketValue ) ) ? ( string ) $this->m_fltMarketValue : 'NULL';
	}

	public function setDisposalAmount( $fltDisposalAmount ) {
		$this->set( 'm_fltDisposalAmount', CStrings::strToFloatDef( $fltDisposalAmount, NULL, false, 4 ) );
	}

	public function getDisposalAmount() {
		return $this->m_fltDisposalAmount;
	}

	public function sqlDisposalAmount() {
		return ( true == isset( $this->m_fltDisposalAmount ) ) ? ( string ) $this->m_fltDisposalAmount : 'NULL';
	}

	public function setCostToDispose( $fltCostToDispose ) {
		$this->set( 'm_fltCostToDispose', CStrings::strToFloatDef( $fltCostToDispose, NULL, false, 4 ) );
	}

	public function getCostToDispose() {
		return $this->m_fltCostToDispose;
	}

	public function sqlCostToDispose() {
		return ( true == isset( $this->m_fltCostToDispose ) ) ? ( string ) $this->m_fltCostToDispose : 'NULL';
	}

	public function setDebtAmount( $fltDebtAmount ) {
		$this->set( 'm_fltDebtAmount', CStrings::strToFloatDef( $fltDebtAmount, NULL, false, 2 ) );
	}

	public function getDebtAmount() {
		return $this->m_fltDebtAmount;
	}

	public function sqlDebtAmount() {
		return ( true == isset( $this->m_fltDebtAmount ) ) ? ( string ) $this->m_fltDebtAmount : 'NULL';
	}

	public function setCashValue( $fltCashValue ) {
		$this->set( 'm_fltCashValue', CStrings::strToFloatDef( $fltCashValue, NULL, false, 4 ) );
	}

	public function getCashValue() {
		return $this->m_fltCashValue;
	}

	public function sqlCashValue() {
		return ( true == isset( $this->m_fltCashValue ) ) ? ( string ) $this->m_fltCashValue : 'NULL';
	}

	public function setInterestRate( $fltInterestRate ) {
		$this->set( 'm_fltInterestRate', CStrings::strToFloatDef( $fltInterestRate, NULL, false, 4 ) );
	}

	public function getInterestRate() {
		return $this->m_fltInterestRate;
	}

	public function sqlInterestRate() {
		return ( true == isset( $this->m_fltInterestRate ) ) ? ( string ) $this->m_fltInterestRate : 'NULL';
	}

	public function setActualIncome( $fltActualIncome ) {
		$this->set( 'm_fltActualIncome', CStrings::strToFloatDef( $fltActualIncome, NULL, false, 4 ) );
	}

	public function getActualIncome() {
		return $this->m_fltActualIncome;
	}

	public function sqlActualIncome() {
		return ( true == isset( $this->m_fltActualIncome ) ) ? ( string ) $this->m_fltActualIncome : 'NULL';
	}

	public function setAssetEffectiveDate( $strAssetEffectiveDate ) {
		$this->set( 'm_strAssetEffectiveDate', CStrings::strTrimDef( $strAssetEffectiveDate, -1, NULL, true ) );
	}

	public function getAssetEffectiveDate() {
		return $this->m_strAssetEffectiveDate;
	}

	public function sqlAssetEffectiveDate() {
		return ( true == isset( $this->m_strAssetEffectiveDate ) ) ? '\'' . $this->m_strAssetEffectiveDate . '\'' : 'NULL';
	}

	public function setDateEnded( $strDateEnded ) {
		$this->set( 'm_strDateEnded', CStrings::strTrimDef( $strDateEnded, -1, NULL, true ) );
	}

	public function getDateEnded() {
		return $this->m_strDateEnded;
	}

	public function sqlDateEnded() {
		return ( true == isset( $this->m_strDateEnded ) ) ? '\'' . $this->m_strDateEnded . '\'' : 'NULL';
	}

	public function setDateDivested( $strDateDivested ) {
		$this->set( 'm_strDateDivested', CStrings::strTrimDef( $strDateDivested, -1, NULL, true ) );
	}

	public function getDateDivested() {
		return $this->m_strDateDivested;
	}

	public function sqlDateDivested() {
		return ( true == isset( $this->m_strDateDivested ) ) ? '\'' . $this->m_strDateDivested . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setIsDisqualifyingDisposalReason( $boolIsDisqualifyingDisposalReason ) {
		$this->set( 'm_boolIsDisqualifyingDisposalReason', CStrings::strToBool( $boolIsDisqualifyingDisposalReason ) );
	}

	public function getIsDisqualifyingDisposalReason() {
		return $this->m_boolIsDisqualifyingDisposalReason;
	}

	public function sqlIsDisqualifyingDisposalReason() {
		return ( true == isset( $this->m_boolIsDisqualifyingDisposalReason ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisqualifyingDisposalReason ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, customer_asset_id, customer_id, customer_asset_type_id, prior_customer_asset_log_id, post_date, apply_through_post_date, description, details, name_on_account, account_number_encrypted, routing_number, institution_name, institution_street_line1, institution_street_line2, institution_street_line3, institution_city, institution_state_code, institution_province, institution_postal_code, institution_country_code, contact_name, contact_phone_number, contact_phone_extension, contact_fax_number, contact_email, market_value, disposal_amount, cost_to_dispose, debt_amount, cash_value, interest_rate, actual_income, asset_effective_date, date_ended, date_divested, log_datetime, is_post_date_ignored, is_opening_log, is_disqualifying_disposal_reason, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlCustomerAssetId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerAssetTypeId() . ', ' .
						$this->sqlPriorCustomerAssetLogId() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlNameOnAccount() . ', ' .
						$this->sqlAccountNumberEncrypted() . ', ' .
						$this->sqlRoutingNumber() . ', ' .
						$this->sqlInstitutionName() . ', ' .
						$this->sqlInstitutionStreetLine1() . ', ' .
						$this->sqlInstitutionStreetLine2() . ', ' .
						$this->sqlInstitutionStreetLine3() . ', ' .
						$this->sqlInstitutionCity() . ', ' .
						$this->sqlInstitutionStateCode() . ', ' .
						$this->sqlInstitutionProvince() . ', ' .
						$this->sqlInstitutionPostalCode() . ', ' .
						$this->sqlInstitutionCountryCode() . ', ' .
						$this->sqlContactName() . ', ' .
						$this->sqlContactPhoneNumber() . ', ' .
						$this->sqlContactPhoneExtension() . ', ' .
						$this->sqlContactFaxNumber() . ', ' .
						$this->sqlContactEmail() . ', ' .
						$this->sqlMarketValue() . ', ' .
						$this->sqlDisposalAmount() . ', ' .
						$this->sqlCostToDispose() . ', ' .
						$this->sqlDebtAmount() . ', ' .
						$this->sqlCashValue() . ', ' .
						$this->sqlInterestRate() . ', ' .
						$this->sqlActualIncome() . ', ' .
						$this->sqlAssetEffectiveDate() . ', ' .
						$this->sqlDateEnded() . ', ' .
						$this->sqlDateDivested() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						$this->sqlIsDisqualifyingDisposalReason() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_asset_id = ' . $this->sqlCustomerAssetId(). ',' ; } elseif( true == array_key_exists( 'CustomerAssetId', $this->getChangedColumns() ) ) { $strSql .= ' customer_asset_id = ' . $this->sqlCustomerAssetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_asset_type_id = ' . $this->sqlCustomerAssetTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerAssetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_asset_type_id = ' . $this->sqlCustomerAssetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_asset_log_id = ' . $this->sqlPriorCustomerAssetLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCustomerAssetLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_asset_log_id = ' . $this->sqlPriorCustomerAssetLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'NameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'AccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'RoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName(). ',' ; } elseif( true == array_key_exists( 'InstitutionName', $this->getChangedColumns() ) ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity(). ',' ; } elseif( true == array_key_exists( 'InstitutionCity', $this->getChangedColumns() ) ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionStateCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince(). ',' ; } elseif( true == array_key_exists( 'InstitutionProvince', $this->getChangedColumns() ) ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName(). ',' ; } elseif( true == array_key_exists( 'ContactName', $this->getChangedColumns() ) ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber(). ',' ; } elseif( true == array_key_exists( 'ContactFaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail(). ',' ; } elseif( true == array_key_exists( 'ContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_value = ' . $this->sqlMarketValue(). ',' ; } elseif( true == array_key_exists( 'MarketValue', $this->getChangedColumns() ) ) { $strSql .= ' market_value = ' . $this->sqlMarketValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disposal_amount = ' . $this->sqlDisposalAmount(). ',' ; } elseif( true == array_key_exists( 'DisposalAmount', $this->getChangedColumns() ) ) { $strSql .= ' disposal_amount = ' . $this->sqlDisposalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_to_dispose = ' . $this->sqlCostToDispose(). ',' ; } elseif( true == array_key_exists( 'CostToDispose', $this->getChangedColumns() ) ) { $strSql .= ' cost_to_dispose = ' . $this->sqlCostToDispose() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debt_amount = ' . $this->sqlDebtAmount(). ',' ; } elseif( true == array_key_exists( 'DebtAmount', $this->getChangedColumns() ) ) { $strSql .= ' debt_amount = ' . $this->sqlDebtAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_value = ' . $this->sqlCashValue(). ',' ; } elseif( true == array_key_exists( 'CashValue', $this->getChangedColumns() ) ) { $strSql .= ' cash_value = ' . $this->sqlCashValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_rate = ' . $this->sqlInterestRate(). ',' ; } elseif( true == array_key_exists( 'InterestRate', $this->getChangedColumns() ) ) { $strSql .= ' interest_rate = ' . $this->sqlInterestRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_income = ' . $this->sqlActualIncome(). ',' ; } elseif( true == array_key_exists( 'ActualIncome', $this->getChangedColumns() ) ) { $strSql .= ' actual_income = ' . $this->sqlActualIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_effective_date = ' . $this->sqlAssetEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'AssetEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' asset_effective_date = ' . $this->sqlAssetEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded(). ',' ; } elseif( true == array_key_exists( 'DateEnded', $this->getChangedColumns() ) ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_divested = ' . $this->sqlDateDivested(). ',' ; } elseif( true == array_key_exists( 'DateDivested', $this->getChangedColumns() ) ) { $strSql .= ' date_divested = ' . $this->sqlDateDivested() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disqualifying_disposal_reason = ' . $this->sqlIsDisqualifyingDisposalReason(). ',' ; } elseif( true == array_key_exists( 'IsDisqualifyingDisposalReason', $this->getChangedColumns() ) ) { $strSql .= ' is_disqualifying_disposal_reason = ' . $this->sqlIsDisqualifyingDisposalReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'customer_asset_id' => $this->getCustomerAssetId(),
			'customer_id' => $this->getCustomerId(),
			'customer_asset_type_id' => $this->getCustomerAssetTypeId(),
			'prior_customer_asset_log_id' => $this->getPriorCustomerAssetLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'name_on_account' => $this->getNameOnAccount(),
			'account_number_encrypted' => $this->getAccountNumberEncrypted(),
			'routing_number' => $this->getRoutingNumber(),
			'institution_name' => $this->getInstitutionName(),
			'institution_street_line1' => $this->getInstitutionStreetLine1(),
			'institution_street_line2' => $this->getInstitutionStreetLine2(),
			'institution_street_line3' => $this->getInstitutionStreetLine3(),
			'institution_city' => $this->getInstitutionCity(),
			'institution_state_code' => $this->getInstitutionStateCode(),
			'institution_province' => $this->getInstitutionProvince(),
			'institution_postal_code' => $this->getInstitutionPostalCode(),
			'institution_country_code' => $this->getInstitutionCountryCode(),
			'contact_name' => $this->getContactName(),
			'contact_phone_number' => $this->getContactPhoneNumber(),
			'contact_phone_extension' => $this->getContactPhoneExtension(),
			'contact_fax_number' => $this->getContactFaxNumber(),
			'contact_email' => $this->getContactEmail(),
			'market_value' => $this->getMarketValue(),
			'disposal_amount' => $this->getDisposalAmount(),
			'cost_to_dispose' => $this->getCostToDispose(),
			'debt_amount' => $this->getDebtAmount(),
			'cash_value' => $this->getCashValue(),
			'interest_rate' => $this->getInterestRate(),
			'actual_income' => $this->getActualIncome(),
			'asset_effective_date' => $this->getAssetEffectiveDate(),
			'date_ended' => $this->getDateEnded(),
			'date_divested' => $this->getDateDivested(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'is_disqualifying_disposal_reason' => $this->getIsDisqualifyingDisposalReason(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>