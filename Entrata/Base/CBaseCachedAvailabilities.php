<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedAvailabilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedAvailabilities extends CEosPluralBase {

	/**
	 * @return CCachedAvailability[]
	 */
	public static function fetchCachedAvailabilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCachedAvailability::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedAvailability
	 */
	public static function fetchCachedAvailability( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCachedAvailability::class, $objDatabase );
	}

	public static function fetchCachedAvailabilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_availabilities', $objDatabase );
	}

	public static function fetchCachedAvailabilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailability( sprintf( 'SELECT * FROM cached_availabilities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedAvailabilitiesByGenderIdByCid( $intGenderId, $intCid, $objDatabase ) {
		return self::fetchCachedAvailabilities( sprintf( 'SELECT * FROM cached_availabilities WHERE gender_id = %d AND cid = %d', $intGenderId, $intCid ), $objDatabase );
	}

}
?>