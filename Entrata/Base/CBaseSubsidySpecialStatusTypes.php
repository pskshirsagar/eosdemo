<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySpecialStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySpecialStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySpecialStatusType[]
	 */
	public static function fetchSubsidySpecialStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySpecialStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySpecialStatusType
	 */
	public static function fetchSubsidySpecialStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySpecialStatusType::class, $objDatabase );
	}

	public static function fetchSubsidySpecialStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_special_status_types', $objDatabase );
	}

	public static function fetchSubsidySpecialStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySpecialStatusType( sprintf( 'SELECT * FROM subsidy_special_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>