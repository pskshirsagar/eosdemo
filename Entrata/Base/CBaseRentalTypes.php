<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRentalTypes
 * Do not add any new functions to this class.
 */

class CBaseRentalTypes extends CEosPluralBase {

	/**
	 * @return CRentalType[]
	 */
	public static function fetchRentalTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRentalType::class, $objDatabase );
	}

	/**
	 * @return CRentalType
	 */
	public static function fetchRentalType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRentalType::class, $objDatabase );
	}

	public static function fetchRentalTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rental_types', $objDatabase );
	}

	public static function fetchRentalTypeById( $intId, $objDatabase ) {
		return self::fetchRentalType( sprintf( 'SELECT * FROM rental_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>