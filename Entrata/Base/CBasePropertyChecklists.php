<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyChecklists
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyChecklists extends CEosPluralBase {

	public static function fetchPropertyChecklists( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyChecklist', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertyChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyChecklist', $objDatabase );
	}

	public static function fetchPropertyChecklistCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_checklists', $objDatabase );
	}

	public static function fetchPropertyChecklistByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyChecklist( sprintf( 'SELECT * FROM property_checklists WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyChecklistsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyChecklists( sprintf( 'SELECT * FROM property_checklists WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyChecklistsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyChecklists( sprintf( 'SELECT * FROM property_checklists WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyChecklistsByChecklistIdByCid( $intChecklistId, $intCid, $objDatabase ) {
		return self::fetchPropertyChecklists( sprintf( 'SELECT * FROM property_checklists WHERE checklist_id = %d AND cid = %d', (int) $intChecklistId, (int) $intCid ), $objDatabase );
	}

}
?>