<?php

class CBaseRenewalChargeTypes extends CEosPluralBase {

    const TABLE_RENEWAL_CHARGE_TYPES = 'public.renewal_ar_triggers';

    public static function fetchRenewalChargeTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRenewalChargeType', $objDatabase );
    }

    public static function fetchRenewalChargeType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRenewalChargeType', $objDatabase );
    }

    public static function fetchRenewalChargeTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'renewal_ar_triggers', $objDatabase );
    }

    public static function fetchRenewalChargeTypeById( $intId, $objDatabase ) {
        return self::fetchRenewalChargeType( sprintf( 'SELECT * FROM renewal_ar_triggers WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>