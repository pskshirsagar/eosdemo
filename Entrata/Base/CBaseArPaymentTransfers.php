<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CArPaymentTransfers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPaymentTransfers extends CEosPluralBase {

	/**
	 * @return CArPaymentTransfer[]
	 */
	public static function fetchArPaymentTransfers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CArPaymentTransfer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPaymentTransfer
	 */
	public static function fetchArPaymentTransfer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CArPaymentTransfer', $objDatabase );
	}

	public static function fetchArPaymentTransferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_transfers', $objDatabase );
	}

	public static function fetchArPaymentTransferByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfer( sprintf( 'SELECT * FROM ar_payment_transfers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE ar_payment_id = %d AND cid = %d', ( int ) $intArPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByOldPropertyIdByCid( $intOldPropertyId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE old_property_id = %d AND cid = %d', ( int ) $intOldPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByNewPropertyIdByCid( $intNewPropertyId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE new_property_id = %d AND cid = %d', ( int ) $intNewPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByOldPaymentArTransactionIdByCid( $intOldPaymentArTransactionId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE old_payment_ar_transaction_id = %d AND cid = %d', ( int ) $intOldPaymentArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByNewPaymentArTransactionIdByCid( $intNewPaymentArTransactionId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE new_payment_ar_transaction_id = %d AND cid = %d', ( int ) $intNewPaymentArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByInvoiceApHeaderIdByCid( $intInvoiceApHeaderId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE invoice_ap_header_id = %d AND cid = %d', ( int ) $intInvoiceApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByPaymentApHeaderIdByCid( $intPaymentApHeaderId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE payment_ap_header_id = %d AND cid = %d', ( int ) $intPaymentApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByChargeArTransactionIdByCid( $intChargeArTransactionId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE charge_ar_transaction_id = %d AND cid = %d', ( int ) $intChargeArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByPaymentArTransactionIdByCid( $intPaymentArTransactionId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE payment_ar_transaction_id = %d AND cid = %d', ( int ) $intPaymentArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByOldBankAccountIdByCid( $intOldBankAccountId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE old_bank_account_id = %d AND cid = %d', ( int ) $intOldBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransfersByNewBankAccountIdByCid( $intNewBankAccountId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransfers( sprintf( 'SELECT * FROM ar_payment_transfers WHERE new_bank_account_id = %d AND cid = %d', ( int ) $intNewBankAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>