<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountMaskGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccountMaskGroups extends CEosPluralBase {

	/**
	 * @return CGlAccountMaskGroup[]
	 */
	public static function fetchGlAccountMaskGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlAccountMaskGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlAccountMaskGroup
	 */
	public static function fetchGlAccountMaskGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlAccountMaskGroup', $objDatabase );
	}

	public static function fetchGlAccountMaskGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_mask_groups', $objDatabase );
	}

	public static function fetchGlAccountMaskGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlAccountMaskGroup( sprintf( 'SELECT * FROM gl_account_mask_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountMaskGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchGlAccountMaskGroups( sprintf( 'SELECT * FROM gl_account_mask_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountMaskGroupsByGlGroupIdByCid( $intGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlAccountMaskGroups( sprintf( 'SELECT * FROM gl_account_mask_groups WHERE gl_group_id = %d AND cid = %d', ( int ) $intGlGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountMaskGroupsByGlAccountTreeIdByCid( $intGlAccountTreeId, $intCid, $objDatabase ) {
		return self::fetchGlAccountMaskGroups( sprintf( 'SELECT * FROM gl_account_mask_groups WHERE gl_account_tree_id = %d AND cid = %d', ( int ) $intGlAccountTreeId, ( int ) $intCid ), $objDatabase );
	}

}
?>