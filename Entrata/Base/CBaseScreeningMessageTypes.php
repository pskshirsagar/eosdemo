<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningMessageTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningMessageTypes extends CEosPluralBase {

	/**
	 * @return CScreeningMessageType[]
	 */
	public static function fetchScreeningMessageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningMessageType::class, $objDatabase );
	}

	/**
	 * @return CScreeningMessageType
	 */
	public static function fetchScreeningMessageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningMessageType::class, $objDatabase );
	}

	public static function fetchScreeningMessageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_message_types', $objDatabase );
	}

	public static function fetchScreeningMessageTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningMessageType( sprintf( 'SELECT * FROM screening_message_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>