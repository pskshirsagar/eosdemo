<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployeeContacts extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeContact[]
	 */
	public static function fetchCompanyEmployeeContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyEmployeeContact::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployeeContact
	 */
	public static function fetchCompanyEmployeeContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEmployeeContact::class, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_contacts', $objDatabase );
	}

	public static function fetchCompanyEmployeeContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContact( sprintf( 'SELECT * FROM company_employee_contacts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContacts( sprintf( 'SELECT * FROM company_employee_contacts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContacts( sprintf( 'SELECT * FROM company_employee_contacts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByCompanyEmployeeContactTypeIdByCid( $intCompanyEmployeeContactTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContacts( sprintf( 'SELECT * FROM company_employee_contacts WHERE company_employee_contact_type_id = %d AND cid = %d', ( int ) $intCompanyEmployeeContactTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContacts( sprintf( 'SELECT * FROM company_employee_contacts WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>