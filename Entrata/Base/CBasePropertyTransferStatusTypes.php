<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferStatusTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyTransferStatusTypes extends CEosPluralBase {

	/**
	 * @return CPropertyTransferStatusType[]
	 */
	public static function fetchPropertyTransferStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyTransferStatusType::class, $objDatabase );
	}

	/**
	 * @return CPropertyTransferStatusType
	 */
	public static function fetchPropertyTransferStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyTransferStatusType::class, $objDatabase );
	}

	public static function fetchPropertyTransferStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_transfer_status_types', $objDatabase );
	}

	public static function fetchPropertyTransferStatusTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyTransferStatusType( sprintf( 'SELECT * FROM property_transfer_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>