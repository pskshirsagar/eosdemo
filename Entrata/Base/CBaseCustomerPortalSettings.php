<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPortalSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPortalSettings extends CEosPluralBase {

	/**
	 * @return CCustomerPortalSetting[]
	 */
	public static function fetchCustomerPortalSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerPortalSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerPortalSetting
	 */
	public static function fetchCustomerPortalSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerPortalSetting', $objDatabase );
	}

	public static function fetchCustomerPortalSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_portal_settings', $objDatabase );
	}

	public static function fetchCustomerPortalSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSetting( sprintf( 'SELECT * FROM customer_portal_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPortalSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSettings( sprintf( 'SELECT * FROM customer_portal_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPortalSettingsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSettings( sprintf( 'SELECT * FROM customer_portal_settings WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>