<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwnerDocuments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwnerDocuments extends CEosPluralBase {

	/**
	 * @return COwnerDocument[]
	 */
	public static function fetchOwnerDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COwnerDocument', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COwnerDocument
	 */
	public static function fetchOwnerDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COwnerDocument', $objDatabase );
	}

	public static function fetchOwnerDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'owner_documents', $objDatabase );
	}

	public static function fetchOwnerDocumentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOwnerDocument( sprintf( 'SELECT * FROM owner_documents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchOwnerDocuments( sprintf( 'SELECT * FROM owner_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchOwnerDocuments( sprintf( 'SELECT * FROM owner_documents WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerDocumentsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchOwnerDocuments( sprintf( 'SELECT * FROM owner_documents WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>