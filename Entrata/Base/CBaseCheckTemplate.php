<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCheckTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.check_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCustomText;
	protected $m_fltTopMargin;
	protected $m_fltLeftMargin;
	protected $m_intPrintStubCheckNumber;
	protected $m_intPrintStubBankInfo;
	protected $m_intCenterCompanyInfo;
	protected $m_intCenterBankInfo;
	protected $m_intThreeChecksPerPage;
	protected $m_intIsAboveStubs;
	protected $m_intIsPrePrinted;
	protected $m_intIsDefault;
	protected $m_intIsSystem;
	protected $m_intIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intPrintStubCheckNumber = '0';
		$this->m_intPrintStubBankInfo = '0';
		$this->m_intCenterCompanyInfo = '0';
		$this->m_intCenterBankInfo = '0';
		$this->m_intThreeChecksPerPage = '0';
		$this->m_intIsAboveStubs = '0';
		$this->m_intIsPrePrinted = '0';
		$this->m_intIsDefault = '0';
		$this->m_intIsSystem = '0';
		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['custom_text'] ) && $boolDirectSet ) $this->set( 'm_strCustomText', trim( stripcslashes( $arrValues['custom_text'] ) ) ); elseif( isset( $arrValues['custom_text'] ) ) $this->setCustomText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_text'] ) : $arrValues['custom_text'] );
		if( isset( $arrValues['top_margin'] ) && $boolDirectSet ) $this->set( 'm_fltTopMargin', trim( $arrValues['top_margin'] ) ); elseif( isset( $arrValues['top_margin'] ) ) $this->setTopMargin( $arrValues['top_margin'] );
		if( isset( $arrValues['left_margin'] ) && $boolDirectSet ) $this->set( 'm_fltLeftMargin', trim( $arrValues['left_margin'] ) ); elseif( isset( $arrValues['left_margin'] ) ) $this->setLeftMargin( $arrValues['left_margin'] );
		if( isset( $arrValues['print_stub_check_number'] ) && $boolDirectSet ) $this->set( 'm_intPrintStubCheckNumber', trim( $arrValues['print_stub_check_number'] ) ); elseif( isset( $arrValues['print_stub_check_number'] ) ) $this->setPrintStubCheckNumber( $arrValues['print_stub_check_number'] );
		if( isset( $arrValues['print_stub_bank_info'] ) && $boolDirectSet ) $this->set( 'm_intPrintStubBankInfo', trim( $arrValues['print_stub_bank_info'] ) ); elseif( isset( $arrValues['print_stub_bank_info'] ) ) $this->setPrintStubBankInfo( $arrValues['print_stub_bank_info'] );
		if( isset( $arrValues['center_company_info'] ) && $boolDirectSet ) $this->set( 'm_intCenterCompanyInfo', trim( $arrValues['center_company_info'] ) ); elseif( isset( $arrValues['center_company_info'] ) ) $this->setCenterCompanyInfo( $arrValues['center_company_info'] );
		if( isset( $arrValues['center_bank_info'] ) && $boolDirectSet ) $this->set( 'm_intCenterBankInfo', trim( $arrValues['center_bank_info'] ) ); elseif( isset( $arrValues['center_bank_info'] ) ) $this->setCenterBankInfo( $arrValues['center_bank_info'] );
		if( isset( $arrValues['three_checks_per_page'] ) && $boolDirectSet ) $this->set( 'm_intThreeChecksPerPage', trim( $arrValues['three_checks_per_page'] ) ); elseif( isset( $arrValues['three_checks_per_page'] ) ) $this->setThreeChecksPerPage( $arrValues['three_checks_per_page'] );
		if( isset( $arrValues['is_above_stubs'] ) && $boolDirectSet ) $this->set( 'm_intIsAboveStubs', trim( $arrValues['is_above_stubs'] ) ); elseif( isset( $arrValues['is_above_stubs'] ) ) $this->setIsAboveStubs( $arrValues['is_above_stubs'] );
		if( isset( $arrValues['is_pre_printed'] ) && $boolDirectSet ) $this->set( 'm_intIsPrePrinted', trim( $arrValues['is_pre_printed'] ) ); elseif( isset( $arrValues['is_pre_printed'] ) ) $this->setIsPrePrinted( $arrValues['is_pre_printed'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCustomText( $strCustomText ) {
		$this->set( 'm_strCustomText', CStrings::strTrimDef( $strCustomText, 40, NULL, true ) );
	}

	public function getCustomText() {
		return $this->m_strCustomText;
	}

	public function sqlCustomText() {
		return ( true == isset( $this->m_strCustomText ) ) ? '\'' . addslashes( $this->m_strCustomText ) . '\'' : 'NULL';
	}

	public function setTopMargin( $fltTopMargin ) {
		$this->set( 'm_fltTopMargin', CStrings::strToFloatDef( $fltTopMargin, NULL, false, 4 ) );
	}

	public function getTopMargin() {
		return $this->m_fltTopMargin;
	}

	public function sqlTopMargin() {
		return ( true == isset( $this->m_fltTopMargin ) ) ? ( string ) $this->m_fltTopMargin : 'NULL';
	}

	public function setLeftMargin( $fltLeftMargin ) {
		$this->set( 'm_fltLeftMargin', CStrings::strToFloatDef( $fltLeftMargin, NULL, false, 4 ) );
	}

	public function getLeftMargin() {
		return $this->m_fltLeftMargin;
	}

	public function sqlLeftMargin() {
		return ( true == isset( $this->m_fltLeftMargin ) ) ? ( string ) $this->m_fltLeftMargin : 'NULL';
	}

	public function setPrintStubCheckNumber( $intPrintStubCheckNumber ) {
		$this->set( 'm_intPrintStubCheckNumber', CStrings::strToIntDef( $intPrintStubCheckNumber, NULL, false ) );
	}

	public function getPrintStubCheckNumber() {
		return $this->m_intPrintStubCheckNumber;
	}

	public function sqlPrintStubCheckNumber() {
		return ( true == isset( $this->m_intPrintStubCheckNumber ) ) ? ( string ) $this->m_intPrintStubCheckNumber : '0';
	}

	public function setPrintStubBankInfo( $intPrintStubBankInfo ) {
		$this->set( 'm_intPrintStubBankInfo', CStrings::strToIntDef( $intPrintStubBankInfo, NULL, false ) );
	}

	public function getPrintStubBankInfo() {
		return $this->m_intPrintStubBankInfo;
	}

	public function sqlPrintStubBankInfo() {
		return ( true == isset( $this->m_intPrintStubBankInfo ) ) ? ( string ) $this->m_intPrintStubBankInfo : '0';
	}

	public function setCenterCompanyInfo( $intCenterCompanyInfo ) {
		$this->set( 'm_intCenterCompanyInfo', CStrings::strToIntDef( $intCenterCompanyInfo, NULL, false ) );
	}

	public function getCenterCompanyInfo() {
		return $this->m_intCenterCompanyInfo;
	}

	public function sqlCenterCompanyInfo() {
		return ( true == isset( $this->m_intCenterCompanyInfo ) ) ? ( string ) $this->m_intCenterCompanyInfo : '0';
	}

	public function setCenterBankInfo( $intCenterBankInfo ) {
		$this->set( 'm_intCenterBankInfo', CStrings::strToIntDef( $intCenterBankInfo, NULL, false ) );
	}

	public function getCenterBankInfo() {
		return $this->m_intCenterBankInfo;
	}

	public function sqlCenterBankInfo() {
		return ( true == isset( $this->m_intCenterBankInfo ) ) ? ( string ) $this->m_intCenterBankInfo : '0';
	}

	public function setThreeChecksPerPage( $intThreeChecksPerPage ) {
		$this->set( 'm_intThreeChecksPerPage', CStrings::strToIntDef( $intThreeChecksPerPage, NULL, false ) );
	}

	public function getThreeChecksPerPage() {
		return $this->m_intThreeChecksPerPage;
	}

	public function sqlThreeChecksPerPage() {
		return ( true == isset( $this->m_intThreeChecksPerPage ) ) ? ( string ) $this->m_intThreeChecksPerPage : '0';
	}

	public function setIsAboveStubs( $intIsAboveStubs ) {
		$this->set( 'm_intIsAboveStubs', CStrings::strToIntDef( $intIsAboveStubs, NULL, false ) );
	}

	public function getIsAboveStubs() {
		return $this->m_intIsAboveStubs;
	}

	public function sqlIsAboveStubs() {
		return ( true == isset( $this->m_intIsAboveStubs ) ) ? ( string ) $this->m_intIsAboveStubs : '0';
	}

	public function setIsPrePrinted( $intIsPrePrinted ) {
		$this->set( 'm_intIsPrePrinted', CStrings::strToIntDef( $intIsPrePrinted, NULL, false ) );
	}

	public function getIsPrePrinted() {
		return $this->m_intIsPrePrinted;
	}

	public function sqlIsPrePrinted() {
		return ( true == isset( $this->m_intIsPrePrinted ) ) ? ( string ) $this->m_intIsPrePrinted : '0';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, system_code, name, description, custom_text, top_margin, left_margin, print_stub_check_number, print_stub_bank_info, center_company_info, center_bank_info, three_checks_per_page, is_above_stubs, is_pre_printed, is_default, is_system, is_disabled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlSystemCode() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCustomText() . ', ' .
 						$this->sqlTopMargin() . ', ' .
 						$this->sqlLeftMargin() . ', ' .
 						$this->sqlPrintStubCheckNumber() . ', ' .
 						$this->sqlPrintStubBankInfo() . ', ' .
 						$this->sqlCenterCompanyInfo() . ', ' .
 						$this->sqlCenterBankInfo() . ', ' .
 						$this->sqlThreeChecksPerPage() . ', ' .
 						$this->sqlIsAboveStubs() . ', ' .
 						$this->sqlIsPrePrinted() . ', ' .
 						$this->sqlIsDefault() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_text = ' . $this->sqlCustomText() . ','; } elseif( true == array_key_exists( 'CustomText', $this->getChangedColumns() ) ) { $strSql .= ' custom_text = ' . $this->sqlCustomText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' top_margin = ' . $this->sqlTopMargin() . ','; } elseif( true == array_key_exists( 'TopMargin', $this->getChangedColumns() ) ) { $strSql .= ' top_margin = ' . $this->sqlTopMargin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' left_margin = ' . $this->sqlLeftMargin() . ','; } elseif( true == array_key_exists( 'LeftMargin', $this->getChangedColumns() ) ) { $strSql .= ' left_margin = ' . $this->sqlLeftMargin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' print_stub_check_number = ' . $this->sqlPrintStubCheckNumber() . ','; } elseif( true == array_key_exists( 'PrintStubCheckNumber', $this->getChangedColumns() ) ) { $strSql .= ' print_stub_check_number = ' . $this->sqlPrintStubCheckNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' print_stub_bank_info = ' . $this->sqlPrintStubBankInfo() . ','; } elseif( true == array_key_exists( 'PrintStubBankInfo', $this->getChangedColumns() ) ) { $strSql .= ' print_stub_bank_info = ' . $this->sqlPrintStubBankInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' center_company_info = ' . $this->sqlCenterCompanyInfo() . ','; } elseif( true == array_key_exists( 'CenterCompanyInfo', $this->getChangedColumns() ) ) { $strSql .= ' center_company_info = ' . $this->sqlCenterCompanyInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' center_bank_info = ' . $this->sqlCenterBankInfo() . ','; } elseif( true == array_key_exists( 'CenterBankInfo', $this->getChangedColumns() ) ) { $strSql .= ' center_bank_info = ' . $this->sqlCenterBankInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' three_checks_per_page = ' . $this->sqlThreeChecksPerPage() . ','; } elseif( true == array_key_exists( 'ThreeChecksPerPage', $this->getChangedColumns() ) ) { $strSql .= ' three_checks_per_page = ' . $this->sqlThreeChecksPerPage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_above_stubs = ' . $this->sqlIsAboveStubs() . ','; } elseif( true == array_key_exists( 'IsAboveStubs', $this->getChangedColumns() ) ) { $strSql .= ' is_above_stubs = ' . $this->sqlIsAboveStubs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pre_printed = ' . $this->sqlIsPrePrinted() . ','; } elseif( true == array_key_exists( 'IsPrePrinted', $this->getChangedColumns() ) ) { $strSql .= ' is_pre_printed = ' . $this->sqlIsPrePrinted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'custom_text' => $this->getCustomText(),
			'top_margin' => $this->getTopMargin(),
			'left_margin' => $this->getLeftMargin(),
			'print_stub_check_number' => $this->getPrintStubCheckNumber(),
			'print_stub_bank_info' => $this->getPrintStubBankInfo(),
			'center_company_info' => $this->getCenterCompanyInfo(),
			'center_bank_info' => $this->getCenterBankInfo(),
			'three_checks_per_page' => $this->getThreeChecksPerPage(),
			'is_above_stubs' => $this->getIsAboveStubs(),
			'is_pre_printed' => $this->getIsPrePrinted(),
			'is_default' => $this->getIsDefault(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>