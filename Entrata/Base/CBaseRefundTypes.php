<?php

class CBaseRefundTypes extends CEosPluralBase {

    public static function fetchRefundTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRefundType', $objDatabase );
    }

    public static function fetchRefundType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRefundType', $objDatabase );
    }

    public static function fetchRefundTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'refund_types', $objDatabase );
    }

    public static function fetchRefundTypeById( $intId, $objDatabase ) {
        return self::fetchRefundType( sprintf( 'SELECT * FROM refund_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>