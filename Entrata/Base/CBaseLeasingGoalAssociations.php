<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoalAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeasingGoalAssociations extends CEosPluralBase {

	/**
	 * @return CLeasingGoalAssociation[]
	 */
	public static function fetchLeasingGoalAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeasingGoalAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeasingGoalAssociation
	 */
	public static function fetchLeasingGoalAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeasingGoalAssociation::class, $objDatabase );
	}

	public static function fetchLeasingGoalAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'leasing_goal_associations', $objDatabase );
	}

	public static function fetchLeasingGoalAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalAssociation( sprintf( 'SELECT * FROM leasing_goal_associations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchLeasingGoalAssociations( sprintf( 'SELECT * FROM leasing_goal_associations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalAssociationsByLeasingGoalIdByCid( $intLeasingGoalId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalAssociations( sprintf( 'SELECT * FROM leasing_goal_associations WHERE leasing_goal_id = %d AND cid = %d', $intLeasingGoalId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalAssociationsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalAssociations( sprintf( 'SELECT * FROM leasing_goal_associations WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalAssociationsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoalAssociations( sprintf( 'SELECT * FROM leasing_goal_associations WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>