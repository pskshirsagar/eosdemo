<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMigrationPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMigrationPreferences extends CEosPluralBase {

	/**
	 * @return CMigrationPreference[]
	 */
	public static function fetchMigrationPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMigrationPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMigrationPreference
	 */
	public static function fetchMigrationPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMigrationPreference', $objDatabase );
	}

	public static function fetchMigrationPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'migration_preferences', $objDatabase );
	}

	public static function fetchMigrationPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMigrationPreference( sprintf( 'SELECT * FROM migration_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMigrationPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchMigrationPreferences( sprintf( 'SELECT * FROM migration_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMigrationPreferencesByImportBatchIdByCid( $intImportBatchId, $intCid, $objDatabase ) {
		return self::fetchMigrationPreferences( sprintf( 'SELECT * FROM migration_preferences WHERE import_batch_id = %d AND cid = %d', ( int ) $intImportBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMigrationPreferencesByImportDataTypeIdByCid( $intImportDataTypeId, $intCid, $objDatabase ) {
		return self::fetchMigrationPreferences( sprintf( 'SELECT * FROM migration_preferences WHERE import_data_type_id = %d AND cid = %d', ( int ) $intImportDataTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>