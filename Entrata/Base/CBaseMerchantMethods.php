<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMerchantMethods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMerchantMethods extends CEosPluralBase {

	/**
	 * @return CMerchantMethod[]
	 */
	public static function fetchMerchantMethods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMerchantMethod::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMerchantMethod
	 */
	public static function fetchMerchantMethod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantMethod::class, $objDatabase );
	}

	public static function fetchMerchantMethodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_methods', $objDatabase );
	}

	public static function fetchMerchantMethodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethod( sprintf( 'SELECT * FROM merchant_methods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE merchant_account_id = %d AND cid = %d', ( int ) $intMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByProcessingBankAccountIdByCid( $intProcessingBankAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE processing_bank_account_id = %d AND cid = %d', ( int ) $intProcessingBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByMerchantGatewayIdByCid( $intMerchantGatewayId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE merchant_gateway_id = %d AND cid = %d', ( int ) $intMerchantGatewayId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE payment_type_id = %d AND cid = %d', ( int ) $intPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantMethodsByPaymentMediumIdByCid( $intPaymentMediumId, $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( sprintf( 'SELECT * FROM merchant_methods WHERE payment_medium_id = %d AND cid = %d', ( int ) $intPaymentMediumId, ( int ) $intCid ), $objDatabase );
	}

}
?>