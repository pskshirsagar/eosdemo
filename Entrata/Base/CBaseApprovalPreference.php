<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApprovalPreference extends CEosSingularBase {

	const TABLE_NAME = 'public.approval_preferences';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRouteTypeId;
	protected $m_intDefaultCompanyGroupId;
	protected $m_intDefaultCompanyUserId;
	protected $m_boolSendNotificationEmails;
	protected $m_boolResetRouteOnEdit;
	protected $m_boolIsEnabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsApproveCreatorStop;

	public function __construct() {
		parent::__construct();

		$this->m_boolSendNotificationEmails = false;
		$this->m_boolResetRouteOnEdit = true;
		$this->m_boolIsEnabled = false;
		$this->m_boolIsApproveCreatorStop = false;
		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['route_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteTypeId', trim( $arrValues['route_type_id'] ) ); elseif( isset( $arrValues['route_type_id'] ) ) $this->setRouteTypeId( $arrValues['route_type_id'] );
		if( isset( $arrValues['default_company_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCompanyGroupId', trim( $arrValues['default_company_group_id'] ) ); elseif( isset( $arrValues['default_company_group_id'] ) ) $this->setDefaultCompanyGroupId( $arrValues['default_company_group_id'] );
		if( isset( $arrValues['default_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCompanyUserId', trim( $arrValues['default_company_user_id'] ) ); elseif( isset( $arrValues['default_company_user_id'] ) ) $this->setDefaultCompanyUserId( $arrValues['default_company_user_id'] );
		if( isset( $arrValues['send_notification_emails'] ) && $boolDirectSet ) $this->set( 'm_boolSendNotificationEmails', trim( stripcslashes( $arrValues['send_notification_emails'] ) ) ); elseif( isset( $arrValues['send_notification_emails'] ) ) $this->setSendNotificationEmails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_notification_emails'] ) : $arrValues['send_notification_emails'] );
		if( isset( $arrValues['reset_route_on_edit'] ) && $boolDirectSet ) $this->set( 'm_boolResetRouteOnEdit', trim( stripcslashes( $arrValues['reset_route_on_edit'] ) ) ); elseif( isset( $arrValues['reset_route_on_edit'] ) ) $this->setResetRouteOnEdit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reset_route_on_edit'] ) : $arrValues['reset_route_on_edit'] );
		if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsEnabled', trim( stripcslashes( $arrValues['is_enabled'] ) ) ); elseif( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_enabled'] ) : $arrValues['is_enabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_approve_creator_stop'] ) && $boolDirectSet ) $this->set( 'm_boolIsApproveCreatorStop', trim( stripcslashes( $arrValues['is_approve_creator_stop'] ) ) ); elseif( isset( $arrValues['is_approve_creator_stop'] ) ) $this->setIsApproveCreatorStop( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_approve_creator_stop'] ) : $arrValues['is_approve_creator_stop'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRouteTypeId( $intRouteTypeId ) {
		$this->set( 'm_intRouteTypeId', CStrings::strToIntDef( $intRouteTypeId, NULL, false ) );
	}

	public function getRouteTypeId() {
		return $this->m_intRouteTypeId;
	}

	public function sqlRouteTypeId() {
		return ( true == isset( $this->m_intRouteTypeId ) ) ? ( string ) $this->m_intRouteTypeId : 'NULL';
	}

	public function setDefaultCompanyGroupId( $intDefaultCompanyGroupId ) {
		$this->set( 'm_intDefaultCompanyGroupId', CStrings::strToIntDef( $intDefaultCompanyGroupId, NULL, false ) );
	}

	public function getDefaultCompanyGroupId() {
		return $this->m_intDefaultCompanyGroupId;
	}

	public function sqlDefaultCompanyGroupId() {
		return ( true == isset( $this->m_intDefaultCompanyGroupId ) ) ? ( string ) $this->m_intDefaultCompanyGroupId : 'NULL';
	}

	public function setDefaultCompanyUserId( $intDefaultCompanyUserId ) {
		$this->set( 'm_intDefaultCompanyUserId', CStrings::strToIntDef( $intDefaultCompanyUserId, NULL, false ) );
	}

	public function getDefaultCompanyUserId() {
		return $this->m_intDefaultCompanyUserId;
	}

	public function sqlDefaultCompanyUserId() {
		return ( true == isset( $this->m_intDefaultCompanyUserId ) ) ? ( string ) $this->m_intDefaultCompanyUserId : 'NULL';
	}

	public function setSendNotificationEmails( $boolSendNotificationEmails ) {
		$this->set( 'm_boolSendNotificationEmails', CStrings::strToBool( $boolSendNotificationEmails ) );
	}

	public function getSendNotificationEmails() {
		return $this->m_boolSendNotificationEmails;
	}

	public function sqlSendNotificationEmails() {
		return ( true == isset( $this->m_boolSendNotificationEmails ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendNotificationEmails ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setResetRouteOnEdit( $boolResetRouteOnEdit ) {
		$this->set( 'm_boolResetRouteOnEdit', CStrings::strToBool( $boolResetRouteOnEdit ) );
	}

	public function getResetRouteOnEdit() {
		return $this->m_boolResetRouteOnEdit;
	}

	public function sqlResetRouteOnEdit() {
		return ( true == isset( $this->m_boolResetRouteOnEdit ) ) ? '\'' . ( true == ( bool ) $this->m_boolResetRouteOnEdit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEnabled( $boolIsEnabled ) {
		$this->set( 'm_boolIsEnabled', CStrings::strToBool( $boolIsEnabled ) );
	}

	public function getIsEnabled() {
		return $this->m_boolIsEnabled;
	}

	public function sqlIsEnabled() {
		return ( true == isset( $this->m_boolIsEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsApproveCreatorStop( $boolIsApproveCreatorStop ) {
		$this->set( 'm_boolIsApproveCreatorStop', CStrings::strToBool( $boolIsApproveCreatorStop ) );
	}

	public function getIsApproveCreatorStop() {
		return $this->m_boolIsApproveCreatorStop;
	}

	public function sqlIsApproveCreatorStop() {
		return ( true == isset( $this->m_boolIsApproveCreatorStop ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApproveCreatorStop ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, route_type_id, default_company_group_id, default_company_user_id, send_notification_emails, reset_route_on_edit, is_enabled, updated_by, updated_on, created_by, created_on, is_approve_creator_stop )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRouteTypeId() . ', ' .
 						$this->sqlDefaultCompanyGroupId() . ', ' .
 						$this->sqlDefaultCompanyUserId() . ', ' .
 						$this->sqlSendNotificationEmails() . ', ' .
 						$this->sqlResetRouteOnEdit() . ', ' .
 						$this->sqlIsEnabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsApproveCreatorStop() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; } elseif( true == array_key_exists( 'RouteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_company_group_id = ' . $this->sqlDefaultCompanyGroupId() . ','; } elseif( true == array_key_exists( 'DefaultCompanyGroupId', $this->getChangedColumns() ) ) { $strSql .= ' default_company_group_id = ' . $this->sqlDefaultCompanyGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_company_user_id = ' . $this->sqlDefaultCompanyUserId() . ','; } elseif( true == array_key_exists( 'DefaultCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' default_company_user_id = ' . $this->sqlDefaultCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_notification_emails = ' . $this->sqlSendNotificationEmails() . ','; } elseif( true == array_key_exists( 'SendNotificationEmails', $this->getChangedColumns() ) ) { $strSql .= ' send_notification_emails = ' . $this->sqlSendNotificationEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reset_route_on_edit = ' . $this->sqlResetRouteOnEdit() . ','; } elseif( true == array_key_exists( 'ResetRouteOnEdit', $this->getChangedColumns() ) ) { $strSql .= ' reset_route_on_edit = ' . $this->sqlResetRouteOnEdit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; } elseif( true == array_key_exists( 'IsEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approve_creator_stop  = ' . $this->sqlIsApproveCreatorStop() . ','; } elseif( true == array_key_exists( 'IsApproveCreatorStop', $this->getChangedColumns() ) ) { $strSql .= ' is_approve_creator_stop = ' . $this->sqlIsApproveCreatorStop() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'route_type_id' => $this->getRouteTypeId(),
			'default_company_group_id' => $this->getDefaultCompanyGroupId(),
			'default_company_user_id' => $this->getDefaultCompanyUserId(),
			'send_notification_emails' => $this->getSendNotificationEmails(),
			'reset_route_on_edit' => $this->getResetRouteOnEdit(),
			'is_enabled' => $this->getIsEnabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_approve_creator_stop' => $this->getIsApproveCreatorStop()
		);
	}

}
?>