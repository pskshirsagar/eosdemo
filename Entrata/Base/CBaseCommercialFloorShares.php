<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialFloorShares extends CEosPluralBase {

	public static function fetchCommercialFloorShares( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCommercialFloorShare', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCommercialFloorShare( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommercialFloorShare', $objDatabase );
	}

	public static function fetchCommercialFloorShareCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_floor_shares', $objDatabase );
	}

	public static function fetchCommercialFloorShareByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialFloorShare( sprintf( 'SELECT * FROM commercial_floor_shares WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchCommercialFloorSharesByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialFloorShares( sprintf( 'SELECT * FROM commercial_floor_shares WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchCommercialFloorSharesByCommercialLeaseDetailsIdByCid( $intCommercialLeaseDetailsId, $intCid, $objDatabase ) {
		return self::fetchCommercialFloorShares( sprintf( 'SELECT * FROM commercial_floor_shares WHERE commercial_lease_details_id = %d AND cid = %d', (int) $intCommercialLeaseDetailsId, (int) $intCid ), $objDatabase );
	}

	public static function fetchCommercialFloorSharesByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchCommercialFloorShares( sprintf( 'SELECT * FROM commercial_floor_shares WHERE property_floor_id = %d AND cid = %d', (int) $intPropertyFloorId, (int) $intCid ), $objDatabase );
	}

}
?>