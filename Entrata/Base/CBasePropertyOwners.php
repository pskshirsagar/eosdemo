<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyOwners
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyOwners extends CEosPluralBase {

	/**
	 * @return CPropertyOwner[]
	 */
	public static function fetchPropertyOwners( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyOwner', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyOwner
	 */
	public static function fetchPropertyOwner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyOwner', $objDatabase );
	}

	public static function fetchPropertyOwnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_owners', $objDatabase );
	}

	public static function fetchPropertyOwnerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyOwner( sprintf( 'SELECT * FROM property_owners WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOwnersByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyOwners( sprintf( 'SELECT * FROM property_owners WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOwnersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyOwners( sprintf( 'SELECT * FROM property_owners WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOwnersByCompanyOwnerIdByCid( $intCompanyOwnerId, $intCid, $objDatabase ) {
		return self::fetchPropertyOwners( sprintf( 'SELECT * FROM property_owners WHERE company_owner_id = %d AND cid = %d', ( int ) $intCompanyOwnerId, ( int ) $intCid ), $objDatabase );
	}

}
?>