<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDataNotes extends CEosPluralBase {

	/**
	 * @return CCustomerDataNote[]
	 */
	public static function fetchCustomerDataNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerDataNote', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerDataNote
	 */
	public static function fetchCustomerDataNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerDataNote', $objDatabase );
	}

	public static function fetchCustomerDataNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_data_notes', $objDatabase );
	}

	public static function fetchCustomerDataNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataNote( sprintf( 'SELECT * FROM customer_data_notes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataNotesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerDataNotes( sprintf( 'SELECT * FROM customer_data_notes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataNotesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataNotes( sprintf( 'SELECT * FROM customer_data_notes WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataNotesByCustomerDataTypeIdByCid( $intCustomerDataTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataNotes( sprintf( 'SELECT * FROM customer_data_notes WHERE customer_data_type_id = %d AND cid = %d', ( int ) $intCustomerDataTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataNotesByCustomerDataReferenceIdByCid( $intCustomerDataReferenceId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataNotes( sprintf( 'SELECT * FROM customer_data_notes WHERE customer_data_reference_id = %d AND cid = %d', ( int ) $intCustomerDataReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>