<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceAvailabilityExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceAvailabilityExceptions extends CEosPluralBase {

	/**
	 * @return CMaintenanceAvailabilityException[]
	 */
	public static function fetchMaintenanceAvailabilityExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceAvailabilityException', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceAvailabilityException
	 */
	public static function fetchMaintenanceAvailabilityException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceAvailabilityException', $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_availability_exceptions', $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilityException( sprintf( 'SELECT * FROM maintenance_availability_exceptions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilityExceptions( sprintf( 'SELECT * FROM maintenance_availability_exceptions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityExceptionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilityExceptions( sprintf( 'SELECT * FROM maintenance_availability_exceptions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityExceptionsByPropertyHolidayIdByCid( $intPropertyHolidayId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilityExceptions( sprintf( 'SELECT * FROM maintenance_availability_exceptions WHERE property_holiday_id = %d AND cid = %d', ( int ) $intPropertyHolidayId, ( int ) $intCid ), $objDatabase );
	}

}
?>