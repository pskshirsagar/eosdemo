<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotificationProperties extends CEosPluralBase {

	/**
	 * @return CNotificationProperty[]
	 */
	public static function fetchNotificationProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CNotificationProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CNotificationProperty
	 */
	public static function fetchNotificationProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNotificationProperty', $objDatabase );
	}

	public static function fetchNotificationPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notification_properties', $objDatabase );
	}

	public static function fetchNotificationPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchNotificationProperty( sprintf( 'SELECT * FROM notification_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchNotificationProperties( sprintf( 'SELECT * FROM notification_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationPropertiesByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {
		return self::fetchNotificationProperties( sprintf( 'SELECT * FROM notification_properties WHERE notification_id = %d AND cid = %d', ( int ) $intNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationPropertiesByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchNotificationProperties( sprintf( 'SELECT * FROM notification_properties WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>