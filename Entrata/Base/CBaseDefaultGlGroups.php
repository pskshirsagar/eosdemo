<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlGroups
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlGroups extends CEosPluralBase {

	/**
	 * @return CDefaultGlGroup[]
	 */
	public static function fetchDefaultGlGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlGroup::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlGroup
	 */
	public static function fetchDefaultGlGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlGroup::class, $objDatabase );
	}

	public static function fetchDefaultGlGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_groups', $objDatabase );
	}

	public static function fetchDefaultGlGroupById( $intId, $objDatabase ) {
		return self::fetchDefaultGlGroup( sprintf( 'SELECT * FROM default_gl_groups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultGlGroupsByGlAccountTypeId( $intGlAccountTypeId, $objDatabase ) {
		return self::fetchDefaultGlGroups( sprintf( 'SELECT * FROM default_gl_groups WHERE gl_account_type_id = %d', $intGlAccountTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlGroupsByDefaultGlTreeId( $intDefaultGlTreeId, $objDatabase ) {
		return self::fetchDefaultGlGroups( sprintf( 'SELECT * FROM default_gl_groups WHERE default_gl_tree_id = %d', $intDefaultGlTreeId ), $objDatabase );
	}

	public static function fetchDefaultGlGroupsByParentDefaultGlGroupId( $intParentDefaultGlGroupId, $objDatabase ) {
		return self::fetchDefaultGlGroups( sprintf( 'SELECT * FROM default_gl_groups WHERE parent_default_gl_group_id = %d', $intParentDefaultGlGroupId ), $objDatabase );
	}

	public static function fetchDefaultGlGroupsByOriginDefaultGlGroupId( $intOriginDefaultGlGroupId, $objDatabase ) {
		return self::fetchDefaultGlGroups( sprintf( 'SELECT * FROM default_gl_groups WHERE origin_default_gl_group_id = %d', $intOriginDefaultGlGroupId ), $objDatabase );
	}

	public static function fetchDefaultGlGroupsByGlGroupTypeId( $intGlGroupTypeId, $objDatabase ) {
		return self::fetchDefaultGlGroups( sprintf( 'SELECT * FROM default_gl_groups WHERE gl_group_type_id = %d', $intGlGroupTypeId ), $objDatabase );
	}

}
?>