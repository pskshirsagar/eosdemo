<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAppointmentHours
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAppointmentHours extends CEosPluralBase {

	/**
	 * @return CPropertyAppointmentHour[]
	 */
	public static function fetchPropertyAppointmentHours( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyAppointmentHour', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAppointmentHour
	 */
	public static function fetchPropertyAppointmentHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyAppointmentHour', $objDatabase );
	}

	public static function fetchPropertyAppointmentHourCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_appointment_hours', $objDatabase );
	}

	public static function fetchPropertyAppointmentHourByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAppointmentHour( sprintf( 'SELECT * FROM property_appointment_hours WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAppointmentHoursByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAppointmentHours( sprintf( 'SELECT * FROM property_appointment_hours WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAppointmentHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAppointmentHours( sprintf( 'SELECT * FROM property_appointment_hours WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>