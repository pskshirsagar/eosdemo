<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApCatalogItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApCatalogItems extends CEosPluralBase {

	/**
	 * @return CApCatalogItem[]
	 */
	public static function fetchApCatalogItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApCatalogItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApCatalogItem
	 */
	public static function fetchApCatalogItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApCatalogItem::class, $objDatabase );
	}

	public static function fetchApCatalogItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_catalog_items', $objDatabase );
	}

	public static function fetchApCatalogItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItem( sprintf( 'SELECT * FROM ap_catalog_items WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByCid( $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByUnitOfMeasureIdByCid( $intUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE unit_of_measure_id = %d AND cid = %d', ( int ) $intUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE ap_payee_account_id = %d AND cid = %d', ( int ) $intApPayeeAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApCatalogItemsByBuyerAccountVendorProductIdByCid( $intBuyerAccountVendorProductId, $intCid, $objDatabase ) {
		return self::fetchApCatalogItems( sprintf( 'SELECT * FROM ap_catalog_items WHERE buyer_account_vendor_product_id = %d AND cid = %d', ( int ) $intBuyerAccountVendorProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>