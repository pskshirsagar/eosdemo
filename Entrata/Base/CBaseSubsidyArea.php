<?php

class CBaseSubsidyArea extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_areas';

	protected $m_intId;
	protected $m_strHmfaCode;
	protected $m_strStateCode;
	protected $m_strName;
	protected $m_strEffectiveDate;
	protected $m_boolIsMetro;
	protected $m_boolIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_strEffectiveDate = 'now()';
		$this->m_boolIsMetro = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['hmfa_code'] ) && $boolDirectSet ) $this->set( 'm_strHmfaCode', trim( stripcslashes( $arrValues['hmfa_code'] ) ) ); elseif( isset( $arrValues['hmfa_code'] ) ) $this->setHmfaCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hmfa_code'] ) : $arrValues['hmfa_code'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['is_metro'] ) && $boolDirectSet ) $this->set( 'm_boolIsMetro', trim( stripcslashes( $arrValues['is_metro'] ) ) ); elseif( isset( $arrValues['is_metro'] ) ) $this->setIsMetro( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_metro'] ) : $arrValues['is_metro'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHmfaCode( $strHmfaCode ) {
		$this->set( 'm_strHmfaCode', CStrings::strTrimDef( $strHmfaCode, 16, NULL, true ) );
	}

	public function getHmfaCode() {
		return $this->m_strHmfaCode;
	}

	public function sqlHmfaCode() {
		return ( true == isset( $this->m_strHmfaCode ) ) ? '\'' . addslashes( $this->m_strHmfaCode ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setIsMetro( $boolIsMetro ) {
		$this->set( 'm_boolIsMetro', CStrings::strToBool( $boolIsMetro ) );
	}

	public function getIsMetro() {
		return $this->m_boolIsMetro;
	}

	public function sqlIsMetro() {
		return ( true == isset( $this->m_boolIsMetro ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMetro ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'hmfa_code' => $this->getHmfaCode(),
			'state_code' => $this->getStateCode(),
			'name' => $this->getName(),
			'effective_date' => $this->getEffectiveDate(),
			'is_metro' => $this->getIsMetro(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>