<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncementPsProducts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAnnouncementPsProducts extends CEosPluralBase {

	/**
	 * @return CAnnouncementPsProduct[]
	 */
	public static function fetchAnnouncementPsProducts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAnnouncementPsProduct', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAnnouncementPsProduct
	 */
	public static function fetchAnnouncementPsProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAnnouncementPsProduct', $objDatabase );
	}

	public static function fetchAnnouncementPsProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'announcement_ps_products', $objDatabase );
	}

	public static function fetchAnnouncementPsProductByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProduct( sprintf( 'SELECT * FROM announcement_ps_products WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementPsProductsByCid( $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProducts( sprintf( 'SELECT * FROM announcement_ps_products WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementPsProductsByAnnouncementIdByCid( $intAnnouncementId, $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProducts( sprintf( 'SELECT * FROM announcement_ps_products WHERE announcement_id = %d AND cid = %d', ( int ) $intAnnouncementId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementPsProductsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProducts( sprintf( 'SELECT * FROM announcement_ps_products WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>