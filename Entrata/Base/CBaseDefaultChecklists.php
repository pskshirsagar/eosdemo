<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChecklists
 * Do not add any new functions to this class.
 */

class CBaseDefaultChecklists extends CEosPluralBase {

	/**
	 * @return CDefaultChecklist[]
	 */
	public static function fetchDefaultChecklists( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultChecklist::class, $objDatabase );
	}

	/**
	 * @return CDefaultChecklist
	 */
	public static function fetchDefaultChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultChecklist::class, $objDatabase );
	}

	public static function fetchDefaultChecklistCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_checklists', $objDatabase );
	}

	public static function fetchDefaultChecklistById( $intId, $objDatabase ) {
		return self::fetchDefaultChecklist( sprintf( 'SELECT * FROM default_checklists WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultChecklistsByChecklistTypeId( $intChecklistTypeId, $objDatabase ) {
		return self::fetchDefaultChecklists( sprintf( 'SELECT * FROM default_checklists WHERE checklist_type_id = %d', ( int ) $intChecklistTypeId ), $objDatabase );
	}

	public static function fetchDefaultChecklistsByChecklistTriggerId( $intChecklistTriggerId, $objDatabase ) {
		return self::fetchDefaultChecklists( sprintf( 'SELECT * FROM default_checklists WHERE checklist_trigger_id = %d', ( int ) $intChecklistTriggerId ), $objDatabase );
	}

}
?>