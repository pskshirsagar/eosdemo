<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClientProcesses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClientProcesses extends CEosPluralBase {

	/**
	 * @return CClientProcess[]
	 */
	public static function fetchClientProcesses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CClientProcess', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CClientProcess
	 */
	public static function fetchClientProcess( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientProcess', $objDatabase );
	}

	public static function fetchClientProcessCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_processes', $objDatabase );
	}

	public static function fetchClientProcessByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchClientProcess( sprintf( 'SELECT * FROM client_processes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClientProcessesByCid( $intCid, $objDatabase ) {
		return self::fetchClientProcesses( sprintf( 'SELECT * FROM client_processes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>