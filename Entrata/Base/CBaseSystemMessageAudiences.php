<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemMessageAudiences
 * Do not add any new functions to this class.
 */

class CBaseSystemMessageAudiences extends CEosPluralBase {

	/**
	 * @return CSystemMessageAudience[]
	 */
	public static function fetchSystemMessageAudiences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemMessageAudience::class, $objDatabase );
	}

	/**
	 * @return CSystemMessageAudience
	 */
	public static function fetchSystemMessageAudience( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemMessageAudience::class, $objDatabase );
	}

	public static function fetchSystemMessageAudienceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_message_audiences', $objDatabase );
	}

	public static function fetchSystemMessageAudienceById( $intId, $objDatabase ) {
		return self::fetchSystemMessageAudience( sprintf( 'SELECT * FROM system_message_audiences WHERE id = %d', $intId ), $objDatabase );
	}

}
?>