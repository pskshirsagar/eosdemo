<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseChoreStatusTypes extends CEosPluralBase {

	/**
	 * @return CChoreStatusType[]
	 */
	public static function fetchChoreStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChoreStatusType::class, $objDatabase );
	}

	/**
	 * @return CChoreStatusType
	 */
	public static function fetchChoreStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChoreStatusType::class, $objDatabase );
	}

	public static function fetchChoreStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_status_types', $objDatabase );
	}

	public static function fetchChoreStatusTypeById( $intId, $objDatabase ) {
		return self::fetchChoreStatusType( sprintf( 'SELECT * FROM chore_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>