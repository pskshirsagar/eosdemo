<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountLoginTypes
 * Do not add any new functions to this class.
 */

class CBaseGlAccountLoginTypes extends CEosPluralBase {

	/**
	 * @return CGlAccountLoginType[]
	 */
	public static function fetchGlAccountLoginTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlAccountLoginType::class, $objDatabase );
	}

	/**
	 * @return CGlAccountLoginType
	 */
	public static function fetchGlAccountLoginType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlAccountLoginType::class, $objDatabase );
	}

	public static function fetchGlAccountLoginTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_login_types', $objDatabase );
	}

	public static function fetchGlAccountLoginTypeById( $intId, $objDatabase ) {
		return self::fetchGlAccountLoginType( sprintf( 'SELECT * FROM gl_account_login_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>