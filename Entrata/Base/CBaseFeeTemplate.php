<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.fee_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlAccountId;
	protected $m_intArCodeId;
	protected $m_intApPaymentTypeId;
	protected $m_intApPayeeLocationId;
	protected $m_intFeeTypeId;
	protected $m_intApRoutingTagId;
	protected $m_strName;
	protected $m_fltMinAmount;
	protected $m_fltMaxAmount;
	protected $m_boolIncludePaymentsInTransit;
	protected $m_boolIncludeSecurityDeposits;
	protected $m_boolIsAutoPay;
	protected $m_boolIsCashBasis;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPostAr;
	protected $m_boolIsPostToCurrentApPeriod;
	protected $m_intFeeCalculationTypeId;
	protected $m_fltFixedAmount;
	protected $m_fltPercent;
	protected $m_boolIsDisabled;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intApPayeeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIncludePaymentsInTransit = false;
		$this->m_boolIncludeSecurityDeposits = false;
		$this->m_boolIsAutoPay = false;
		$this->m_boolIsCashBasis = false;
		$this->m_boolIsPostAr = false;
		$this->m_boolIsPostToCurrentApPeriod = true;
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ap_payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentTypeId', trim( $arrValues['ap_payment_type_id'] ) ); elseif( isset( $arrValues['ap_payment_type_id'] ) ) $this->setApPaymentTypeId( $arrValues['ap_payment_type_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTypeId', trim( $arrValues['fee_type_id'] ) ); elseif( isset( $arrValues['fee_type_id'] ) ) $this->setFeeTypeId( $arrValues['fee_type_id'] );
		if( isset( $arrValues['ap_routing_tag_id'] ) && $boolDirectSet ) $this->set( 'm_intApRoutingTagId', trim( $arrValues['ap_routing_tag_id'] ) ); elseif( isset( $arrValues['ap_routing_tag_id'] ) ) $this->setApRoutingTagId( $arrValues['ap_routing_tag_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['min_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmount', trim( $arrValues['min_amount'] ) ); elseif( isset( $arrValues['min_amount'] ) ) $this->setMinAmount( $arrValues['min_amount'] );
		if( isset( $arrValues['max_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmount', trim( $arrValues['max_amount'] ) ); elseif( isset( $arrValues['max_amount'] ) ) $this->setMaxAmount( $arrValues['max_amount'] );
		if( isset( $arrValues['include_payments_in_transit'] ) && $boolDirectSet ) $this->set( 'm_boolIncludePaymentsInTransit', trim( stripcslashes( $arrValues['include_payments_in_transit'] ) ) ); elseif( isset( $arrValues['include_payments_in_transit'] ) ) $this->setIncludePaymentsInTransit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_payments_in_transit'] ) : $arrValues['include_payments_in_transit'] );
		if( isset( $arrValues['include_security_deposits'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeSecurityDeposits', trim( stripcslashes( $arrValues['include_security_deposits'] ) ) ); elseif( isset( $arrValues['include_security_deposits'] ) ) $this->setIncludeSecurityDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_security_deposits'] ) : $arrValues['include_security_deposits'] );
		if( isset( $arrValues['is_auto_pay'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoPay', trim( stripcslashes( $arrValues['is_auto_pay'] ) ) ); elseif( isset( $arrValues['is_auto_pay'] ) ) $this->setIsAutoPay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_pay'] ) : $arrValues['is_auto_pay'] );
		if( isset( $arrValues['is_cash_basis'] ) && $boolDirectSet ) $this->set( 'm_boolIsCashBasis', trim( stripcslashes( $arrValues['is_cash_basis'] ) ) ); elseif( isset( $arrValues['is_cash_basis'] ) ) $this->setIsCashBasis( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cash_basis'] ) : $arrValues['is_cash_basis'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_post_ar'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostAr', trim( stripcslashes( $arrValues['is_post_ar'] ) ) ); elseif( isset( $arrValues['is_post_ar'] ) ) $this->setIsPostAr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_ar'] ) : $arrValues['is_post_ar'] );
		if( isset( $arrValues['is_post_to_current_ap_period'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostToCurrentApPeriod', trim( stripcslashes( $arrValues['is_post_to_current_ap_period'] ) ) ); elseif( isset( $arrValues['is_post_to_current_ap_period'] ) ) $this->setIsPostToCurrentApPeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_to_current_ap_period'] ) : $arrValues['is_post_to_current_ap_period'] );
		if( isset( $arrValues['fee_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeCalculationTypeId', trim( $arrValues['fee_calculation_type_id'] ) ); elseif( isset( $arrValues['fee_calculation_type_id'] ) ) $this->setFeeCalculationTypeId( $arrValues['fee_calculation_type_id'] );
		if( isset( $arrValues['fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFixedAmount', trim( $arrValues['fixed_amount'] ) ); elseif( isset( $arrValues['fixed_amount'] ) ) $this->setFixedAmount( $arrValues['fixed_amount'] );
		if( isset( $arrValues['percent'] ) && $boolDirectSet ) $this->set( 'm_fltPercent', trim( $arrValues['percent'] ) ); elseif( isset( $arrValues['percent'] ) ) $this->setPercent( $arrValues['percent'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->set( 'm_intApPaymentTypeId', CStrings::strToIntDef( $intApPaymentTypeId, NULL, false ) );
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function sqlApPaymentTypeId() {
		return ( true == isset( $this->m_intApPaymentTypeId ) ) ? ( string ) $this->m_intApPaymentTypeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setFeeTypeId( $intFeeTypeId ) {
		$this->set( 'm_intFeeTypeId', CStrings::strToIntDef( $intFeeTypeId, NULL, false ) );
	}

	public function getFeeTypeId() {
		return $this->m_intFeeTypeId;
	}

	public function sqlFeeTypeId() {
		return ( true == isset( $this->m_intFeeTypeId ) ) ? ( string ) $this->m_intFeeTypeId : 'NULL';
	}

	public function setApRoutingTagId( $intApRoutingTagId ) {
		$this->set( 'm_intApRoutingTagId', CStrings::strToIntDef( $intApRoutingTagId, NULL, false ) );
	}

	public function getApRoutingTagId() {
		return $this->m_intApRoutingTagId;
	}

	public function sqlApRoutingTagId() {
		return ( true == isset( $this->m_intApRoutingTagId ) ) ? ( string ) $this->m_intApRoutingTagId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setMinAmount( $fltMinAmount ) {
		$this->set( 'm_fltMinAmount', CStrings::strToFloatDef( $fltMinAmount, NULL, false, 2 ) );
	}

	public function getMinAmount() {
		return $this->m_fltMinAmount;
	}

	public function sqlMinAmount() {
		return ( true == isset( $this->m_fltMinAmount ) ) ? ( string ) $this->m_fltMinAmount : 'NULL';
	}

	public function setMaxAmount( $fltMaxAmount ) {
		$this->set( 'm_fltMaxAmount', CStrings::strToFloatDef( $fltMaxAmount, NULL, false, 2 ) );
	}

	public function getMaxAmount() {
		return $this->m_fltMaxAmount;
	}

	public function sqlMaxAmount() {
		return ( true == isset( $this->m_fltMaxAmount ) ) ? ( string ) $this->m_fltMaxAmount : 'NULL';
	}

	public function setIncludePaymentsInTransit( $boolIncludePaymentsInTransit ) {
		$this->set( 'm_boolIncludePaymentsInTransit', CStrings::strToBool( $boolIncludePaymentsInTransit ) );
	}

	public function getIncludePaymentsInTransit() {
		return $this->m_boolIncludePaymentsInTransit;
	}

	public function sqlIncludePaymentsInTransit() {
		return ( true == isset( $this->m_boolIncludePaymentsInTransit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludePaymentsInTransit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeSecurityDeposits( $boolIncludeSecurityDeposits ) {
		$this->set( 'm_boolIncludeSecurityDeposits', CStrings::strToBool( $boolIncludeSecurityDeposits ) );
	}

	public function getIncludeSecurityDeposits() {
		return $this->m_boolIncludeSecurityDeposits;
	}

	public function sqlIncludeSecurityDeposits() {
		return ( true == isset( $this->m_boolIncludeSecurityDeposits ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeSecurityDeposits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoPay( $boolIsAutoPay ) {
		$this->set( 'm_boolIsAutoPay', CStrings::strToBool( $boolIsAutoPay ) );
	}

	public function getIsAutoPay() {
		return $this->m_boolIsAutoPay;
	}

	public function sqlIsAutoPay() {
		return ( true == isset( $this->m_boolIsAutoPay ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoPay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCashBasis( $boolIsCashBasis ) {
		$this->set( 'm_boolIsCashBasis', CStrings::strToBool( $boolIsCashBasis ) );
	}

	public function getIsCashBasis() {
		return $this->m_boolIsCashBasis;
	}

	public function sqlIsCashBasis() {
		return ( true == isset( $this->m_boolIsCashBasis ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCashBasis ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPostAr( $boolIsPostAr ) {
		$this->set( 'm_boolIsPostAr', CStrings::strToBool( $boolIsPostAr ) );
	}

	public function getIsPostAr() {
		return $this->m_boolIsPostAr;
	}

	public function sqlIsPostAr() {
		return ( true == isset( $this->m_boolIsPostAr ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostAr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostToCurrentApPeriod( $boolIsPostToCurrentApPeriod ) {
		$this->set( 'm_boolIsPostToCurrentApPeriod', CStrings::strToBool( $boolIsPostToCurrentApPeriod ) );
	}

	public function getIsPostToCurrentApPeriod() {
		return $this->m_boolIsPostToCurrentApPeriod;
	}

	public function sqlIsPostToCurrentApPeriod() {
		return ( true == isset( $this->m_boolIsPostToCurrentApPeriod ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostToCurrentApPeriod ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setFeeCalculationTypeId( $intFeeCalculationTypeId ) {
		$this->set( 'm_intFeeCalculationTypeId', CStrings::strToIntDef( $intFeeCalculationTypeId, NULL, false ) );
	}

	public function getFeeCalculationTypeId() {
		return $this->m_intFeeCalculationTypeId;
	}

	public function sqlFeeCalculationTypeId() {
		return ( true == isset( $this->m_intFeeCalculationTypeId ) ) ? ( string ) $this->m_intFeeCalculationTypeId : 'NULL';
	}

	public function setFixedAmount( $fltFixedAmount ) {
		$this->set( 'm_fltFixedAmount', CStrings::strToFloatDef( $fltFixedAmount, NULL, false, 2 ) );
	}

	public function getFixedAmount() {
		return $this->m_fltFixedAmount;
	}

	public function sqlFixedAmount() {
		return ( true == isset( $this->m_fltFixedAmount ) ) ? ( string ) $this->m_fltFixedAmount : 'NULL';
	}

	public function setPercent( $fltPercent ) {
		$this->set( 'm_fltPercent', CStrings::strToFloatDef( $fltPercent, NULL, false, 2 ) );
	}

	public function getPercent() {
		return $this->m_fltPercent;
	}

	public function sqlPercent() {
		return ( true == isset( $this->m_fltPercent ) ) ? ( string ) $this->m_fltPercent : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_account_id, ar_code_id, ap_payment_type_id, ap_payee_location_id, fee_type_id, ap_routing_tag_id, name, min_amount, max_amount, include_payments_in_transit, include_security_deposits, is_auto_pay, is_cash_basis, updated_by, updated_on, created_by, created_on, is_post_ar, is_post_to_current_ap_period, fee_calculation_type_id, fixed_amount, percent, is_disabled, details, ap_payee_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlApPaymentTypeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlFeeTypeId() . ', ' .
						$this->sqlApRoutingTagId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlMinAmount() . ', ' .
						$this->sqlMaxAmount() . ', ' .
						$this->sqlIncludePaymentsInTransit() . ', ' .
						$this->sqlIncludeSecurityDeposits() . ', ' .
						$this->sqlIsAutoPay() . ', ' .
						$this->sqlIsCashBasis() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsPostAr() . ', ' .
						$this->sqlIsPostToCurrentApPeriod() . ', ' .
						$this->sqlFeeCalculationTypeId() . ', ' .
						$this->sqlFixedAmount() . ', ' .
						$this->sqlPercent() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlApPayeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_type_id = ' . $this->sqlApPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_type_id = ' . $this->sqlApPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_type_id = ' . $this->sqlFeeTypeId(). ',' ; } elseif( true == array_key_exists( 'FeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_type_id = ' . $this->sqlFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId(). ',' ; } elseif( true == array_key_exists( 'ApRoutingTagId', $this->getChangedColumns() ) ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amount = ' . $this->sqlMinAmount(). ',' ; } elseif( true == array_key_exists( 'MinAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_amount = ' . $this->sqlMinAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amount = ' . $this->sqlMaxAmount(). ',' ; } elseif( true == array_key_exists( 'MaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_amount = ' . $this->sqlMaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_payments_in_transit = ' . $this->sqlIncludePaymentsInTransit(). ',' ; } elseif( true == array_key_exists( 'IncludePaymentsInTransit', $this->getChangedColumns() ) ) { $strSql .= ' include_payments_in_transit = ' . $this->sqlIncludePaymentsInTransit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_security_deposits = ' . $this->sqlIncludeSecurityDeposits(). ',' ; } elseif( true == array_key_exists( 'IncludeSecurityDeposits', $this->getChangedColumns() ) ) { $strSql .= ' include_security_deposits = ' . $this->sqlIncludeSecurityDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_pay = ' . $this->sqlIsAutoPay(). ',' ; } elseif( true == array_key_exists( 'IsAutoPay', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_pay = ' . $this->sqlIsAutoPay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis(). ',' ; } elseif( true == array_key_exists( 'IsCashBasis', $this->getChangedColumns() ) ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_ar = ' . $this->sqlIsPostAr(). ',' ; } elseif( true == array_key_exists( 'IsPostAr', $this->getChangedColumns() ) ) { $strSql .= ' is_post_ar = ' . $this->sqlIsPostAr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_to_current_ap_period = ' . $this->sqlIsPostToCurrentApPeriod(). ',' ; } elseif( true == array_key_exists( 'IsPostToCurrentApPeriod', $this->getChangedColumns() ) ) { $strSql .= ' is_post_to_current_ap_period = ' . $this->sqlIsPostToCurrentApPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_calculation_type_id = ' . $this->sqlFeeCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'FeeCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_calculation_type_id = ' . $this->sqlFeeCalculationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount(). ',' ; } elseif( true == array_key_exists( 'FixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent = ' . $this->sqlPercent(). ',' ; } elseif( true == array_key_exists( 'Percent', $this->getChangedColumns() ) ) { $strSql .= ' percent = ' . $this->sqlPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_account_id' => $this->getGlAccountId(),
			'ar_code_id' => $this->getArCodeId(),
			'ap_payment_type_id' => $this->getApPaymentTypeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'fee_type_id' => $this->getFeeTypeId(),
			'ap_routing_tag_id' => $this->getApRoutingTagId(),
			'name' => $this->getName(),
			'min_amount' => $this->getMinAmount(),
			'max_amount' => $this->getMaxAmount(),
			'include_payments_in_transit' => $this->getIncludePaymentsInTransit(),
			'include_security_deposits' => $this->getIncludeSecurityDeposits(),
			'is_auto_pay' => $this->getIsAutoPay(),
			'is_cash_basis' => $this->getIsCashBasis(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_post_ar' => $this->getIsPostAr(),
			'is_post_to_current_ap_period' => $this->getIsPostToCurrentApPeriod(),
			'fee_calculation_type_id' => $this->getFeeCalculationTypeId(),
			'fixed_amount' => $this->getFixedAmount(),
			'percent' => $this->getPercent(),
			'is_disabled' => $this->getIsDisabled(),
			'details' => $this->getDetails(),
			'ap_payee_id' => $this->getApPayeeId()
		);
	}

}
?>