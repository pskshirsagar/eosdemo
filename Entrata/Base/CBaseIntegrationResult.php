<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationResult extends CEosSingularBase {

	const TABLE_NAME = 'public.integration_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intWebMethodId;
	protected $m_intServiceId;
	protected $m_intIntegrationClientId;
	protected $m_intIntegrationServiceId;
	protected $m_intIntegrationQueueId;
	protected $m_intReferenceNumber;
	protected $m_strErrorMessage;
	protected $m_strResultDatetime;
	protected $m_fltIntegrationUtilityVersion;
	protected $m_fltRemoteProcessingTime;
	protected $m_fltProcessingTime;
	protected $m_intIsFailed;
	protected $m_boolUseAmazonFileStorage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltFileStorageTime;
	protected $m_strExtraInfo;
	protected $m_jsonExtraInfo;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFailed = '0';
		$this->m_boolUseAmazonFileStorage = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['web_method_id'] ) && $boolDirectSet ) $this->set( 'm_intWebMethodId', trim( $arrValues['web_method_id'] ) ); elseif( isset( $arrValues['web_method_id'] ) ) $this->setWebMethodId( $arrValues['web_method_id'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['integration_client_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientId', trim( $arrValues['integration_client_id'] ) ); elseif( isset( $arrValues['integration_client_id'] ) ) $this->setIntegrationClientId( $arrValues['integration_client_id'] );
		if( isset( $arrValues['integration_service_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationServiceId', trim( $arrValues['integration_service_id'] ) ); elseif( isset( $arrValues['integration_service_id'] ) ) $this->setIntegrationServiceId( $arrValues['integration_service_id'] );
		if( isset( $arrValues['integration_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationQueueId', trim( $arrValues['integration_queue_id'] ) ); elseif( isset( $arrValues['integration_queue_id'] ) ) $this->setIntegrationQueueId( $arrValues['integration_queue_id'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['error_message'] ) && $boolDirectSet ) $this->set( 'm_strErrorMessage', trim( stripcslashes( $arrValues['error_message'] ) ) ); elseif( isset( $arrValues['error_message'] ) ) $this->setErrorMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_message'] ) : $arrValues['error_message'] );
		if( isset( $arrValues['result_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResultDatetime', trim( $arrValues['result_datetime'] ) ); elseif( isset( $arrValues['result_datetime'] ) ) $this->setResultDatetime( $arrValues['result_datetime'] );
		if( isset( $arrValues['integration_utility_version'] ) && $boolDirectSet ) $this->set( 'm_fltIntegrationUtilityVersion', trim( $arrValues['integration_utility_version'] ) ); elseif( isset( $arrValues['integration_utility_version'] ) ) $this->setIntegrationUtilityVersion( $arrValues['integration_utility_version'] );
		if( isset( $arrValues['remote_processing_time'] ) && $boolDirectSet ) $this->set( 'm_fltRemoteProcessingTime', trim( $arrValues['remote_processing_time'] ) ); elseif( isset( $arrValues['remote_processing_time'] ) ) $this->setRemoteProcessingTime( $arrValues['remote_processing_time'] );
		if( isset( $arrValues['processing_time'] ) && $boolDirectSet ) $this->set( 'm_fltProcessingTime', trim( $arrValues['processing_time'] ) ); elseif( isset( $arrValues['processing_time'] ) ) $this->setProcessingTime( $arrValues['processing_time'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_intIsFailed', trim( $arrValues['is_failed'] ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( $arrValues['is_failed'] );
		if( isset( $arrValues['use_amazon_file_storage'] ) && $boolDirectSet ) $this->set( 'm_boolUseAmazonFileStorage', trim( stripcslashes( $arrValues['use_amazon_file_storage'] ) ) ); elseif( isset( $arrValues['use_amazon_file_storage'] ) ) $this->setUseAmazonFileStorage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_amazon_file_storage'] ) : $arrValues['use_amazon_file_storage'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['file_storage_time'] ) && $boolDirectSet ) $this->set( 'm_fltFileStorageTime', trim( $arrValues['file_storage_time'] ) ); elseif( isset( $arrValues['file_storage_time'] ) ) $this->setFileStorageTime( $arrValues['file_storage_time'] );
		if( isset( $arrValues['extra_info'] ) ) $this->set( 'm_strExtraInfo', trim( $arrValues['extra_info'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setWebMethodId( $intWebMethodId ) {
		$this->set( 'm_intWebMethodId', CStrings::strToIntDef( $intWebMethodId, NULL, false ) );
	}

	public function getWebMethodId() {
		return $this->m_intWebMethodId;
	}

	public function sqlWebMethodId() {
		return ( true == isset( $this->m_intWebMethodId ) ) ? ( string ) $this->m_intWebMethodId : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setIntegrationClientId( $intIntegrationClientId ) {
		$this->set( 'm_intIntegrationClientId', CStrings::strToIntDef( $intIntegrationClientId, NULL, false ) );
	}

	public function getIntegrationClientId() {
		return $this->m_intIntegrationClientId;
	}

	public function sqlIntegrationClientId() {
		return ( true == isset( $this->m_intIntegrationClientId ) ) ? ( string ) $this->m_intIntegrationClientId : 'NULL';
	}

	public function setIntegrationServiceId( $intIntegrationServiceId ) {
		$this->set( 'm_intIntegrationServiceId', CStrings::strToIntDef( $intIntegrationServiceId, NULL, false ) );
	}

	public function getIntegrationServiceId() {
		return $this->m_intIntegrationServiceId;
	}

	public function sqlIntegrationServiceId() {
		return ( true == isset( $this->m_intIntegrationServiceId ) ) ? ( string ) $this->m_intIntegrationServiceId : 'NULL';
	}

	public function setIntegrationQueueId( $intIntegrationQueueId ) {
		$this->set( 'm_intIntegrationQueueId', CStrings::strToIntDef( $intIntegrationQueueId, NULL, false ) );
	}

	public function getIntegrationQueueId() {
		return $this->m_intIntegrationQueueId;
	}

	public function sqlIntegrationQueueId() {
		return ( true == isset( $this->m_intIntegrationQueueId ) ) ? ( string ) $this->m_intIntegrationQueueId : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setErrorMessage( $strErrorMessage ) {
		$this->set( 'm_strErrorMessage', CStrings::strTrimDef( $strErrorMessage, 2000, NULL, true ) );
	}

	public function getErrorMessage() {
		return $this->m_strErrorMessage;
	}

	public function sqlErrorMessage() {
		return ( true == isset( $this->m_strErrorMessage ) ) ? '\'' . addslashes( $this->m_strErrorMessage ) . '\'' : 'NULL';
	}

	public function setResultDatetime( $strResultDatetime ) {
		$this->set( 'm_strResultDatetime', CStrings::strTrimDef( $strResultDatetime, -1, NULL, true ) );
	}

	public function getResultDatetime() {
		return $this->m_strResultDatetime;
	}

	public function sqlResultDatetime() {
		return ( true == isset( $this->m_strResultDatetime ) ) ? '\'' . $this->m_strResultDatetime . '\'' : 'NOW()';
	}

	public function setIntegrationUtilityVersion( $fltIntegrationUtilityVersion ) {
		$this->set( 'm_fltIntegrationUtilityVersion', CStrings::strToFloatDef( $fltIntegrationUtilityVersion, NULL, false, 4 ) );
	}

	public function getIntegrationUtilityVersion() {
		return $this->m_fltIntegrationUtilityVersion;
	}

	public function sqlIntegrationUtilityVersion() {
		return ( true == isset( $this->m_fltIntegrationUtilityVersion ) ) ? ( string ) $this->m_fltIntegrationUtilityVersion : 'NULL';
	}

	public function setRemoteProcessingTime( $fltRemoteProcessingTime ) {
		$this->set( 'm_fltRemoteProcessingTime', CStrings::strToFloatDef( $fltRemoteProcessingTime, NULL, false, 4 ) );
	}

	public function getRemoteProcessingTime() {
		return $this->m_fltRemoteProcessingTime;
	}

	public function sqlRemoteProcessingTime() {
		return ( true == isset( $this->m_fltRemoteProcessingTime ) ) ? ( string ) $this->m_fltRemoteProcessingTime : 'NULL';
	}

	public function setProcessingTime( $fltProcessingTime ) {
		$this->set( 'm_fltProcessingTime', CStrings::strToFloatDef( $fltProcessingTime, NULL, false, 4 ) );
	}

	public function getProcessingTime() {
		return $this->m_fltProcessingTime;
	}

	public function sqlProcessingTime() {
		return ( true == isset( $this->m_fltProcessingTime ) ) ? ( string ) $this->m_fltProcessingTime : 'NULL';
	}

	public function setIsFailed( $intIsFailed ) {
		$this->set( 'm_intIsFailed', CStrings::strToIntDef( $intIsFailed, NULL, false ) );
	}

	public function getIsFailed() {
		return $this->m_intIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_intIsFailed ) ) ? ( string ) $this->m_intIsFailed : '0';
	}

	public function setUseAmazonFileStorage( $boolUseAmazonFileStorage ) {
		$this->set( 'm_boolUseAmazonFileStorage', CStrings::strToBool( $boolUseAmazonFileStorage ) );
	}

	public function getUseAmazonFileStorage() {
		return $this->m_boolUseAmazonFileStorage;
	}

	public function sqlUseAmazonFileStorage() {
		return ( true == isset( $this->m_boolUseAmazonFileStorage ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseAmazonFileStorage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFileStorageTime( $fltFileStorageTime ) {
		$this->set( 'm_fltFileStorageTime', CStrings::strToFloatDef( $fltFileStorageTime, NULL, false, 4 ) );
	}

	public function getFileStorageTime() {
		return $this->m_fltFileStorageTime;
	}

	public function sqlFileStorageTime() {
		return ( true == isset( $this->m_fltFileStorageTime ) ) ? ( string ) $this->m_fltFileStorageTime : 'NULL';
	}

	public function setExtraInfo( $jsonExtraInfo ) {
		if( true == valObj( $jsonExtraInfo, 'stdClass' ) ) {
			$this->set( 'm_jsonExtraInfo', $jsonExtraInfo );
		} elseif( true == valJsonString( $jsonExtraInfo ) ) {
			$this->set( 'm_jsonExtraInfo', CStrings::strToJson( $jsonExtraInfo ) );
		} else {
			$this->set( 'm_jsonExtraInfo', NULL );
		}
		unset( $this->m_strExtraInfo );
	}

	public function getExtraInfo() {
		if( true == isset( $this->m_strExtraInfo ) ) {
			$this->m_jsonExtraInfo = CStrings::strToJson( $this->m_strExtraInfo );
			unset( $this->m_strExtraInfo );
		}
		return $this->m_jsonExtraInfo;
	}

	public function sqlExtraInfo() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getExtraInfo() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getExtraInfo() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, web_method_id, service_id, integration_client_id, integration_service_id, integration_queue_id, reference_number, error_message, result_datetime, integration_utility_version, remote_processing_time, processing_time, is_failed, use_amazon_file_storage, updated_by, updated_on, created_by, created_on, file_storage_time, extra_info )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlWebMethodId() . ', ' .
		          $this->sqlServiceId() . ', ' .
		          $this->sqlIntegrationClientId() . ', ' .
		          $this->sqlIntegrationServiceId() . ', ' .
		          $this->sqlIntegrationQueueId() . ', ' .
		          $this->sqlReferenceNumber() . ', ' .
		          $this->sqlErrorMessage() . ', ' .
		          $this->sqlResultDatetime() . ', ' .
		          $this->sqlIntegrationUtilityVersion() . ', ' .
		          $this->sqlRemoteProcessingTime() . ', ' .
		          $this->sqlProcessingTime() . ', ' .
		          $this->sqlIsFailed() . ', ' .
		          $this->sqlUseAmazonFileStorage() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlFileStorageTime() . ', ' .
		          $this->sqlExtraInfo() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_method_id = ' . $this->sqlWebMethodId() . ','; } elseif( true == array_key_exists( 'WebMethodId', $this->getChangedColumns() ) ) { $strSql .= ' web_method_id = ' . $this->sqlWebMethodId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_id = ' . $this->sqlIntegrationClientId() . ','; } elseif( true == array_key_exists( 'IntegrationClientId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_id = ' . $this->sqlIntegrationClientId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; } elseif( true == array_key_exists( 'IntegrationServiceId', $this->getChangedColumns() ) ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_queue_id = ' . $this->sqlIntegrationQueueId() . ','; } elseif( true == array_key_exists( 'IntegrationQueueId', $this->getChangedColumns() ) ) { $strSql .= ' integration_queue_id = ' . $this->sqlIntegrationQueueId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage() . ','; } elseif( true == array_key_exists( 'ErrorMessage', $this->getChangedColumns() ) ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; } elseif( true == array_key_exists( 'ResultDatetime', $this->getChangedColumns() ) ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_utility_version = ' . $this->sqlIntegrationUtilityVersion() . ','; } elseif( true == array_key_exists( 'IntegrationUtilityVersion', $this->getChangedColumns() ) ) { $strSql .= ' integration_utility_version = ' . $this->sqlIntegrationUtilityVersion() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_processing_time = ' . $this->sqlRemoteProcessingTime() . ','; } elseif( true == array_key_exists( 'RemoteProcessingTime', $this->getChangedColumns() ) ) { $strSql .= ' remote_processing_time = ' . $this->sqlRemoteProcessingTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_time = ' . $this->sqlProcessingTime() . ','; } elseif( true == array_key_exists( 'ProcessingTime', $this->getChangedColumns() ) ) { $strSql .= ' processing_time = ' . $this->sqlProcessingTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage() . ','; } elseif( true == array_key_exists( 'UseAmazonFileStorage', $this->getChangedColumns() ) ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_storage_time = ' . $this->sqlFileStorageTime() . ','; } elseif( true == array_key_exists( 'FileStorageTime', $this->getChangedColumns() ) ) { $strSql .= ' file_storage_time = ' . $this->sqlFileStorageTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extra_info = ' . $this->sqlExtraInfo() . ','; } elseif( true == array_key_exists( 'ExtraInfo', $this->getChangedColumns() ) ) { $strSql .= ' extra_info = ' . $this->sqlExtraInfo() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'web_method_id' => $this->getWebMethodId(),
			'service_id' => $this->getServiceId(),
			'integration_client_id' => $this->getIntegrationClientId(),
			'integration_service_id' => $this->getIntegrationServiceId(),
			'integration_queue_id' => $this->getIntegrationQueueId(),
			'reference_number' => $this->getReferenceNumber(),
			'error_message' => $this->getErrorMessage(),
			'result_datetime' => $this->getResultDatetime(),
			'integration_utility_version' => $this->getIntegrationUtilityVersion(),
			'remote_processing_time' => $this->getRemoteProcessingTime(),
			'processing_time' => $this->getProcessingTime(),
			'is_failed' => $this->getIsFailed(),
			'use_amazon_file_storage' => $this->getUseAmazonFileStorage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'file_storage_time' => $this->getFileStorageTime(),
			'extra_info' => $this->getExtraInfo()
		);
	}

}
?>