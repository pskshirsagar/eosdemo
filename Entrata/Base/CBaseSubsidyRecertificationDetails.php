<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRecertificationDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyRecertificationDetails extends CEosPluralBase {

	/**
	 * @return CSubsidyRecertificationDetail[]
	 */
	public static function fetchSubsidyRecertificationDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyRecertificationDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyRecertificationDetail
	 */
	public static function fetchSubsidyRecertificationDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyRecertificationDetail::class, $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_recertification_details', $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRecertificationDetail( sprintf( 'SELECT * FROM subsidy_recertification_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyRecertificationDetails( sprintf( 'SELECT * FROM subsidy_recertification_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRecertificationDetails( sprintf( 'SELECT * FROM subsidy_recertification_details WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailsBySubsidyTypeIdByCid( $intSubsidyTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRecertificationDetails( sprintf( 'SELECT * FROM subsidy_recertification_details WHERE subsidy_type_id = %d AND cid = %d', ( int ) $intSubsidyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRecertificationDetailsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRecertificationDetails( sprintf( 'SELECT * FROM subsidy_recertification_details WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

}
?>