<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationFilter extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.application_filters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_strProperties;
	protected $m_strPropertyFloorplans;
	protected $m_strUnitKinds;
	protected $m_strApplicationStageStatuses;
	protected $m_strScreeningResponseTypes;
	protected $m_strLeaseIntervalTypes;
	protected $m_strLeadSources;
	protected $m_strPsProducts;
	protected $m_strCompanyEmployees;
	protected $m_strApplications;
	protected $m_strFilterName;
	protected $m_strQuickSearch;
	protected $m_intLastContactMinDays;
	protected $m_intLastContactMaxDays;
	protected $m_intAdDelayDays;
	protected $m_strDesiredBedrooms;
	protected $m_strDesiredBathrooms;
	protected $m_strDesiredPets;
	protected $m_fltDesiredRentMin;
	protected $m_fltDesiredRentMax;
	protected $m_strDesiredOccupants;
	protected $m_strDesiredFloors;
	protected $m_strApplicationStartDate;
	protected $m_strApplicationEndDate;
	protected $m_strMoveInDateMin;
	protected $m_strMoveInDateMax;
	protected $m_strSortBy;
	protected $m_strSortDirection;
	protected $m_intIsIntegrated;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strSortDirection = 'DESC';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['properties'] ) && $boolDirectSet ) $this->set( 'm_strProperties', trim( stripcslashes( $arrValues['properties'] ) ) ); elseif( isset( $arrValues['properties'] ) ) $this->setProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['properties'] ) : $arrValues['properties'] );
		if( isset( $arrValues['property_floorplans'] ) && $boolDirectSet ) $this->set( 'm_strPropertyFloorplans', trim( stripcslashes( $arrValues['property_floorplans'] ) ) ); elseif( isset( $arrValues['property_floorplans'] ) ) $this->setPropertyFloorplans( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_floorplans'] ) : $arrValues['property_floorplans'] );
		if( isset( $arrValues['unit_kinds'] ) && $boolDirectSet ) $this->set( 'm_strUnitKinds', trim( stripcslashes( $arrValues['unit_kinds'] ) ) ); elseif( isset( $arrValues['unit_kinds'] ) ) $this->setUnitKinds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_kinds'] ) : $arrValues['unit_kinds'] );
		if( isset( $arrValues['application_stage_statuses'] ) && $boolDirectSet ) $this->set( 'm_strApplicationStageStatuses', trim( stripcslashes( $arrValues['application_stage_statuses'] ) ) ); elseif( isset( $arrValues['application_stage_statuses'] ) ) $this->setApplicationStageStatuses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_stage_statuses'] ) : $arrValues['application_stage_statuses'] );
		if( isset( $arrValues['screening_response_types'] ) && $boolDirectSet ) $this->set( 'm_strScreeningResponseTypes', trim( stripcslashes( $arrValues['screening_response_types'] ) ) ); elseif( isset( $arrValues['screening_response_types'] ) ) $this->setScreeningResponseTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['screening_response_types'] ) : $arrValues['screening_response_types'] );
		if( isset( $arrValues['lease_interval_types'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalTypes', trim( stripcslashes( $arrValues['lease_interval_types'] ) ) ); elseif( isset( $arrValues['lease_interval_types'] ) ) $this->setLeaseIntervalTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_interval_types'] ) : $arrValues['lease_interval_types'] );
		if( isset( $arrValues['lead_sources'] ) && $boolDirectSet ) $this->set( 'm_strLeadSources', trim( stripcslashes( $arrValues['lead_sources'] ) ) ); elseif( isset( $arrValues['lead_sources'] ) ) $this->setLeadSources( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_sources'] ) : $arrValues['lead_sources'] );
		if( isset( $arrValues['ps_products'] ) && $boolDirectSet ) $this->set( 'm_strPsProducts', trim( stripcslashes( $arrValues['ps_products'] ) ) ); elseif( isset( $arrValues['ps_products'] ) ) $this->setPsProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_products'] ) : $arrValues['ps_products'] );
		if( isset( $arrValues['company_employees'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEmployees', trim( stripcslashes( $arrValues['company_employees'] ) ) ); elseif( isset( $arrValues['company_employees'] ) ) $this->setCompanyEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_employees'] ) : $arrValues['company_employees'] );
		if( isset( $arrValues['applications'] ) && $boolDirectSet ) $this->set( 'm_strApplications', trim( stripcslashes( $arrValues['applications'] ) ) ); elseif( isset( $arrValues['applications'] ) ) $this->setApplications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applications'] ) : $arrValues['applications'] );
		if( isset( $arrValues['filter_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFilterName', trim( stripcslashes( $arrValues['filter_name'] ) ) ); elseif( isset( $arrValues['filter_name'] ) ) $this->setFilterName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_name'] ) : $arrValues['filter_name'] );
		if( isset( $arrValues['quick_search'] ) && $boolDirectSet ) $this->set( 'm_strQuickSearch', trim( stripcslashes( $arrValues['quick_search'] ) ) ); elseif( isset( $arrValues['quick_search'] ) ) $this->setQuickSearch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['quick_search'] ) : $arrValues['quick_search'] );
		if( isset( $arrValues['last_contact_min_days'] ) && $boolDirectSet ) $this->set( 'm_intLastContactMinDays', trim( $arrValues['last_contact_min_days'] ) ); elseif( isset( $arrValues['last_contact_min_days'] ) ) $this->setLastContactMinDays( $arrValues['last_contact_min_days'] );
		if( isset( $arrValues['last_contact_max_days'] ) && $boolDirectSet ) $this->set( 'm_intLastContactMaxDays', trim( $arrValues['last_contact_max_days'] ) ); elseif( isset( $arrValues['last_contact_max_days'] ) ) $this->setLastContactMaxDays( $arrValues['last_contact_max_days'] );
		if( isset( $arrValues['ad_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intAdDelayDays', trim( $arrValues['ad_delay_days'] ) ); elseif( isset( $arrValues['ad_delay_days'] ) ) $this->setAdDelayDays( $arrValues['ad_delay_days'] );
		if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_strDesiredBedrooms', trim( stripcslashes( $arrValues['desired_bedrooms'] ) ) ); elseif( isset( $arrValues['desired_bedrooms'] ) ) $this->setDesiredBedrooms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bedrooms'] ) : $arrValues['desired_bedrooms'] );
		if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_strDesiredBathrooms', trim( stripcslashes( $arrValues['desired_bathrooms'] ) ) ); elseif( isset( $arrValues['desired_bathrooms'] ) ) $this->setDesiredBathrooms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bathrooms'] ) : $arrValues['desired_bathrooms'] );
		if( isset( $arrValues['desired_pets'] ) && $boolDirectSet ) $this->set( 'm_strDesiredPets', trim( stripcslashes( $arrValues['desired_pets'] ) ) ); elseif( isset( $arrValues['desired_pets'] ) ) $this->setDesiredPets( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_pets'] ) : $arrValues['desired_pets'] );
		if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMin', trim( $arrValues['desired_rent_min'] ) ); elseif( isset( $arrValues['desired_rent_min'] ) ) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
		if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMax', trim( $arrValues['desired_rent_max'] ) ); elseif( isset( $arrValues['desired_rent_max'] ) ) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
		if( isset( $arrValues['desired_occupants'] ) && $boolDirectSet ) $this->set( 'm_strDesiredOccupants', trim( stripcslashes( $arrValues['desired_occupants'] ) ) ); elseif( isset( $arrValues['desired_occupants'] ) ) $this->setDesiredOccupants( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_occupants'] ) : $arrValues['desired_occupants'] );
		if( isset( $arrValues['desired_floors'] ) && $boolDirectSet ) $this->set( 'm_strDesiredFloors', trim( stripcslashes( $arrValues['desired_floors'] ) ) ); elseif( isset( $arrValues['desired_floors'] ) ) $this->setDesiredFloors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_floors'] ) : $arrValues['desired_floors'] );
		if( isset( $arrValues['application_start_date'] ) && $boolDirectSet ) $this->set( 'm_strApplicationStartDate', trim( $arrValues['application_start_date'] ) ); elseif( isset( $arrValues['application_start_date'] ) ) $this->setApplicationStartDate( $arrValues['application_start_date'] );
		if( isset( $arrValues['application_end_date'] ) && $boolDirectSet ) $this->set( 'm_strApplicationEndDate', trim( $arrValues['application_end_date'] ) ); elseif( isset( $arrValues['application_end_date'] ) ) $this->setApplicationEndDate( $arrValues['application_end_date'] );
		if( isset( $arrValues['move_in_date_min'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDateMin', trim( $arrValues['move_in_date_min'] ) ); elseif( isset( $arrValues['move_in_date_min'] ) ) $this->setMoveInDateMin( $arrValues['move_in_date_min'] );
		if( isset( $arrValues['move_in_date_max'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDateMax', trim( $arrValues['move_in_date_max'] ) ); elseif( isset( $arrValues['move_in_date_max'] ) ) $this->setMoveInDateMax( $arrValues['move_in_date_max'] );
		if( isset( $arrValues['sort_by'] ) && $boolDirectSet ) $this->set( 'm_strSortBy', trim( stripcslashes( $arrValues['sort_by'] ) ) ); elseif( isset( $arrValues['sort_by'] ) ) $this->setSortBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_by'] ) : $arrValues['sort_by'] );
		if( isset( $arrValues['sort_direction'] ) && $boolDirectSet ) $this->set( 'm_strSortDirection', trim( stripcslashes( $arrValues['sort_direction'] ) ) ); elseif( isset( $arrValues['sort_direction'] ) ) $this->setSortDirection( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_direction'] ) : $arrValues['sort_direction'] );
		if( isset( $arrValues['is_integrated'] ) && $boolDirectSet ) $this->set( 'm_intIsIntegrated', trim( $arrValues['is_integrated'] ) ); elseif( isset( $arrValues['is_integrated'] ) ) $this->setIsIntegrated( $arrValues['is_integrated'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setProperties( $strProperties ) {
		$this->set( 'm_strProperties', CStrings::strTrimDef( $strProperties, -1, NULL, true ) );
	}

	public function getProperties() {
		return $this->m_strProperties;
	}

	public function sqlProperties() {
		return ( true == isset( $this->m_strProperties ) ) ? '\'' . addslashes( $this->m_strProperties ) . '\'' : 'NULL';
	}

	public function setPropertyFloorplans( $strPropertyFloorplans ) {
		$this->set( 'm_strPropertyFloorplans', CStrings::strTrimDef( $strPropertyFloorplans, -1, NULL, true ) );
	}

	public function getPropertyFloorplans() {
		return $this->m_strPropertyFloorplans;
	}

	public function sqlPropertyFloorplans() {
		return ( true == isset( $this->m_strPropertyFloorplans ) ) ? '\'' . addslashes( $this->m_strPropertyFloorplans ) . '\'' : 'NULL';
	}

	public function setUnitKinds( $strUnitKinds ) {
		$this->set( 'm_strUnitKinds', CStrings::strTrimDef( $strUnitKinds, 2000, NULL, true ) );
	}

	public function getUnitKinds() {
		return $this->m_strUnitKinds;
	}

	public function sqlUnitKinds() {
		return ( true == isset( $this->m_strUnitKinds ) ) ? '\'' . addslashes( $this->m_strUnitKinds ) . '\'' : 'NULL';
	}

	public function setApplicationStageStatuses( $strApplicationStageStatuses ) {
		$this->set( 'm_strApplicationStageStatuses', CStrings::strTrimDef( $strApplicationStageStatuses, -1, NULL, true ) );
	}

	public function getApplicationStageStatuses() {
		return $this->m_strApplicationStageStatuses;
	}

	public function sqlApplicationStageStatuses() {
		return ( true == isset( $this->m_strApplicationStageStatuses ) ) ? '\'' . addslashes( $this->m_strApplicationStageStatuses ) . '\'' : 'NULL';
	}

	public function setScreeningResponseTypes( $strScreeningResponseTypes ) {
		$this->set( 'm_strScreeningResponseTypes', CStrings::strTrimDef( $strScreeningResponseTypes, -1, NULL, true ) );
	}

	public function getScreeningResponseTypes() {
		return $this->m_strScreeningResponseTypes;
	}

	public function sqlScreeningResponseTypes() {
		return ( true == isset( $this->m_strScreeningResponseTypes ) ) ? '\'' . addslashes( $this->m_strScreeningResponseTypes ) . '\'' : 'NULL';
	}

	public function setLeaseIntervalTypes( $strLeaseIntervalTypes ) {
		$this->set( 'm_strLeaseIntervalTypes', CStrings::strTrimDef( $strLeaseIntervalTypes, -1, NULL, true ) );
	}

	public function getLeaseIntervalTypes() {
		return $this->m_strLeaseIntervalTypes;
	}

	public function sqlLeaseIntervalTypes() {
		return ( true == isset( $this->m_strLeaseIntervalTypes ) ) ? '\'' . addslashes( $this->m_strLeaseIntervalTypes ) . '\'' : 'NULL';
	}

	public function setLeadSources( $strLeadSources ) {
		$this->set( 'm_strLeadSources', CStrings::strTrimDef( $strLeadSources, -1, NULL, true ) );
	}

	public function getLeadSources() {
		return $this->m_strLeadSources;
	}

	public function sqlLeadSources() {
		return ( true == isset( $this->m_strLeadSources ) ) ? '\'' . addslashes( $this->m_strLeadSources ) . '\'' : 'NULL';
	}

	public function setPsProducts( $strPsProducts ) {
		$this->set( 'm_strPsProducts', CStrings::strTrimDef( $strPsProducts, -1, NULL, true ) );
	}

	public function getPsProducts() {
		return $this->m_strPsProducts;
	}

	public function sqlPsProducts() {
		return ( true == isset( $this->m_strPsProducts ) ) ? '\'' . addslashes( $this->m_strPsProducts ) . '\'' : 'NULL';
	}

	public function setCompanyEmployees( $strCompanyEmployees ) {
		$this->set( 'm_strCompanyEmployees', CStrings::strTrimDef( $strCompanyEmployees, -1, NULL, true ) );
	}

	public function getCompanyEmployees() {
		return $this->m_strCompanyEmployees;
	}

	public function sqlCompanyEmployees() {
		return ( true == isset( $this->m_strCompanyEmployees ) ) ? '\'' . addslashes( $this->m_strCompanyEmployees ) . '\'' : 'NULL';
	}

	public function setApplications( $strApplications ) {
		$this->set( 'm_strApplications', CStrings::strTrimDef( $strApplications, -1, NULL, true ) );
	}

	public function getApplications() {
		return $this->m_strApplications;
	}

	public function sqlApplications() {
		return ( true == isset( $this->m_strApplications ) ) ? '\'' . addslashes( $this->m_strApplications ) . '\'' : 'NULL';
	}

	public function setFilterName( $strFilterName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFilterName', CStrings::strTrimDef( $strFilterName, 50, NULL, true ), $strLocaleCode );
	}

	public function getFilterName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFilterName', $strLocaleCode );
	}

	public function sqlFilterName() {
		return ( true == isset( $this->m_strFilterName ) ) ? '\'' . addslashes( $this->m_strFilterName ) . '\'' : 'NULL';
	}

	public function setQuickSearch( $strQuickSearch ) {
		$this->set( 'm_strQuickSearch', CStrings::strTrimDef( $strQuickSearch, -1, NULL, true ) );
	}

	public function getQuickSearch() {
		return $this->m_strQuickSearch;
	}

	public function sqlQuickSearch() {
		return ( true == isset( $this->m_strQuickSearch ) ) ? '\'' . addslashes( $this->m_strQuickSearch ) . '\'' : 'NULL';
	}

	public function setLastContactMinDays( $intLastContactMinDays ) {
		$this->set( 'm_intLastContactMinDays', CStrings::strToIntDef( $intLastContactMinDays, NULL, false ) );
	}

	public function getLastContactMinDays() {
		return $this->m_intLastContactMinDays;
	}

	public function sqlLastContactMinDays() {
		return ( true == isset( $this->m_intLastContactMinDays ) ) ? ( string ) $this->m_intLastContactMinDays : 'NULL';
	}

	public function setLastContactMaxDays( $intLastContactMaxDays ) {
		$this->set( 'm_intLastContactMaxDays', CStrings::strToIntDef( $intLastContactMaxDays, NULL, false ) );
	}

	public function getLastContactMaxDays() {
		return $this->m_intLastContactMaxDays;
	}

	public function sqlLastContactMaxDays() {
		return ( true == isset( $this->m_intLastContactMaxDays ) ) ? ( string ) $this->m_intLastContactMaxDays : 'NULL';
	}

	public function setAdDelayDays( $intAdDelayDays ) {
		$this->set( 'm_intAdDelayDays', CStrings::strToIntDef( $intAdDelayDays, NULL, false ) );
	}

	public function getAdDelayDays() {
		return $this->m_intAdDelayDays;
	}

	public function sqlAdDelayDays() {
		return ( true == isset( $this->m_intAdDelayDays ) ) ? ( string ) $this->m_intAdDelayDays : 'NULL';
	}

	public function setDesiredBedrooms( $strDesiredBedrooms ) {
		$this->set( 'm_strDesiredBedrooms', CStrings::strTrimDef( $strDesiredBedrooms, 240, NULL, true ) );
	}

	public function getDesiredBedrooms() {
		return $this->m_strDesiredBedrooms;
	}

	public function sqlDesiredBedrooms() {
		return ( true == isset( $this->m_strDesiredBedrooms ) ) ? '\'' . addslashes( $this->m_strDesiredBedrooms ) . '\'' : 'NULL';
	}

	public function setDesiredBathrooms( $strDesiredBathrooms ) {
		$this->set( 'm_strDesiredBathrooms', CStrings::strTrimDef( $strDesiredBathrooms, 240, NULL, true ) );
	}

	public function getDesiredBathrooms() {
		return $this->m_strDesiredBathrooms;
	}

	public function sqlDesiredBathrooms() {
		return ( true == isset( $this->m_strDesiredBathrooms ) ) ? '\'' . addslashes( $this->m_strDesiredBathrooms ) . '\'' : 'NULL';
	}

	public function setDesiredPets( $strDesiredPets ) {
		$this->set( 'm_strDesiredPets', CStrings::strTrimDef( $strDesiredPets, 240, NULL, true ) );
	}

	public function getDesiredPets() {
		return $this->m_strDesiredPets;
	}

	public function sqlDesiredPets() {
		return ( true == isset( $this->m_strDesiredPets ) ) ? '\'' . addslashes( $this->m_strDesiredPets ) . '\'' : 'NULL';
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		$this->set( 'm_fltDesiredRentMin', CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2 ) );
	}

	public function getDesiredRentMin() {
		return $this->m_fltDesiredRentMin;
	}

	public function sqlDesiredRentMin() {
		return ( true == isset( $this->m_fltDesiredRentMin ) ) ? ( string ) $this->m_fltDesiredRentMin : 'NULL';
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		$this->set( 'm_fltDesiredRentMax', CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2 ) );
	}

	public function getDesiredRentMax() {
		return $this->m_fltDesiredRentMax;
	}

	public function sqlDesiredRentMax() {
		return ( true == isset( $this->m_fltDesiredRentMax ) ) ? ( string ) $this->m_fltDesiredRentMax : 'NULL';
	}

	public function setDesiredOccupants( $strDesiredOccupants ) {
		$this->set( 'm_strDesiredOccupants', CStrings::strTrimDef( $strDesiredOccupants, 2000, NULL, true ) );
	}

	public function getDesiredOccupants() {
		return $this->m_strDesiredOccupants;
	}

	public function sqlDesiredOccupants() {
		return ( true == isset( $this->m_strDesiredOccupants ) ) ? '\'' . addslashes( $this->m_strDesiredOccupants ) . '\'' : 'NULL';
	}

	public function setDesiredFloors( $strDesiredFloors ) {
		$this->set( 'm_strDesiredFloors', CStrings::strTrimDef( $strDesiredFloors, 2000, NULL, true ) );
	}

	public function getDesiredFloors() {
		return $this->m_strDesiredFloors;
	}

	public function sqlDesiredFloors() {
		return ( true == isset( $this->m_strDesiredFloors ) ) ? '\'' . addslashes( $this->m_strDesiredFloors ) . '\'' : 'NULL';
	}

	public function setApplicationStartDate( $strApplicationStartDate ) {
		$this->set( 'm_strApplicationStartDate', CStrings::strTrimDef( $strApplicationStartDate, -1, NULL, true ) );
	}

	public function getApplicationStartDate() {
		return $this->m_strApplicationStartDate;
	}

	public function sqlApplicationStartDate() {
		return ( true == isset( $this->m_strApplicationStartDate ) ) ? '\'' . $this->m_strApplicationStartDate . '\'' : 'NULL';
	}

	public function setApplicationEndDate( $strApplicationEndDate ) {
		$this->set( 'm_strApplicationEndDate', CStrings::strTrimDef( $strApplicationEndDate, -1, NULL, true ) );
	}

	public function getApplicationEndDate() {
		return $this->m_strApplicationEndDate;
	}

	public function sqlApplicationEndDate() {
		return ( true == isset( $this->m_strApplicationEndDate ) ) ? '\'' . $this->m_strApplicationEndDate . '\'' : 'NULL';
	}

	public function setMoveInDateMin( $strMoveInDateMin ) {
		$this->set( 'm_strMoveInDateMin', CStrings::strTrimDef( $strMoveInDateMin, -1, NULL, true ) );
	}

	public function getMoveInDateMin() {
		return $this->m_strMoveInDateMin;
	}

	public function sqlMoveInDateMin() {
		return ( true == isset( $this->m_strMoveInDateMin ) ) ? '\'' . $this->m_strMoveInDateMin . '\'' : 'NULL';
	}

	public function setMoveInDateMax( $strMoveInDateMax ) {
		$this->set( 'm_strMoveInDateMax', CStrings::strTrimDef( $strMoveInDateMax, -1, NULL, true ) );
	}

	public function getMoveInDateMax() {
		return $this->m_strMoveInDateMax;
	}

	public function sqlMoveInDateMax() {
		return ( true == isset( $this->m_strMoveInDateMax ) ) ? '\'' . $this->m_strMoveInDateMax . '\'' : 'NULL';
	}

	public function setSortBy( $strSortBy ) {
		$this->set( 'm_strSortBy', CStrings::strTrimDef( $strSortBy, 240, NULL, true ) );
	}

	public function getSortBy() {
		return $this->m_strSortBy;
	}

	public function sqlSortBy() {
		return ( true == isset( $this->m_strSortBy ) ) ? '\'' . addslashes( $this->m_strSortBy ) . '\'' : 'NULL';
	}

	public function setSortDirection( $strSortDirection ) {
		$this->set( 'm_strSortDirection', CStrings::strTrimDef( $strSortDirection, 240, NULL, true ) );
	}

	public function getSortDirection() {
		return $this->m_strSortDirection;
	}

	public function sqlSortDirection() {
		return ( true == isset( $this->m_strSortDirection ) ) ? '\'' . addslashes( $this->m_strSortDirection ) . '\'' : '\'DESC\'';
	}

	public function setIsIntegrated( $intIsIntegrated ) {
		$this->set( 'm_intIsIntegrated', CStrings::strToIntDef( $intIsIntegrated, NULL, false ) );
	}

	public function getIsIntegrated() {
		return $this->m_intIsIntegrated;
	}

	public function sqlIsIntegrated() {
		return ( true == isset( $this->m_intIsIntegrated ) ) ? ( string ) $this->m_intIsIntegrated : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, properties, property_floorplans, unit_kinds, application_stage_statuses, screening_response_types, lease_interval_types, lead_sources, ps_products, company_employees, applications, filter_name, quick_search, last_contact_min_days, last_contact_max_days, ad_delay_days, desired_bedrooms, desired_bathrooms, desired_pets, desired_rent_min, desired_rent_max, desired_occupants, desired_floors, application_start_date, application_end_date, move_in_date_min, move_in_date_max, sort_by, sort_direction, is_integrated, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlProperties() . ', ' .
						$this->sqlPropertyFloorplans() . ', ' .
						$this->sqlUnitKinds() . ', ' .
						$this->sqlApplicationStageStatuses() . ', ' .
						$this->sqlScreeningResponseTypes() . ', ' .
						$this->sqlLeaseIntervalTypes() . ', ' .
						$this->sqlLeadSources() . ', ' .
						$this->sqlPsProducts() . ', ' .
						$this->sqlCompanyEmployees() . ', ' .
						$this->sqlApplications() . ', ' .
						$this->sqlFilterName() . ', ' .
						$this->sqlQuickSearch() . ', ' .
						$this->sqlLastContactMinDays() . ', ' .
						$this->sqlLastContactMaxDays() . ', ' .
						$this->sqlAdDelayDays() . ', ' .
						$this->sqlDesiredBedrooms() . ', ' .
						$this->sqlDesiredBathrooms() . ', ' .
						$this->sqlDesiredPets() . ', ' .
						$this->sqlDesiredRentMin() . ', ' .
						$this->sqlDesiredRentMax() . ', ' .
						$this->sqlDesiredOccupants() . ', ' .
						$this->sqlDesiredFloors() . ', ' .
						$this->sqlApplicationStartDate() . ', ' .
						$this->sqlApplicationEndDate() . ', ' .
						$this->sqlMoveInDateMin() . ', ' .
						$this->sqlMoveInDateMax() . ', ' .
						$this->sqlSortBy() . ', ' .
						$this->sqlSortDirection() . ', ' .
						$this->sqlIsIntegrated() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' properties = ' . $this->sqlProperties(). ',' ; } elseif( true == array_key_exists( 'Properties', $this->getChangedColumns() ) ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplans', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kinds = ' . $this->sqlUnitKinds(). ',' ; } elseif( true == array_key_exists( 'UnitKinds', $this->getChangedColumns() ) ) { $strSql .= ' unit_kinds = ' . $this->sqlUnitKinds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_statuses = ' . $this->sqlApplicationStageStatuses(). ',' ; } elseif( true == array_key_exists( 'ApplicationStageStatuses', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_statuses = ' . $this->sqlApplicationStageStatuses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_response_types = ' . $this->sqlScreeningResponseTypes(). ',' ; } elseif( true == array_key_exists( 'ScreeningResponseTypes', $this->getChangedColumns() ) ) { $strSql .= ' screening_response_types = ' . $this->sqlScreeningResponseTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypes', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_sources = ' . $this->sqlLeadSources(). ',' ; } elseif( true == array_key_exists( 'LeadSources', $this->getChangedColumns() ) ) { $strSql .= ' lead_sources = ' . $this->sqlLeadSources() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts(). ',' ; } elseif( true == array_key_exists( 'PsProducts', $this->getChangedColumns() ) ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employees = ' . $this->sqlCompanyEmployees(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployees', $this->getChangedColumns() ) ) { $strSql .= ' company_employees = ' . $this->sqlCompanyEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applications = ' . $this->sqlApplications(). ',' ; } elseif( true == array_key_exists( 'Applications', $this->getChangedColumns() ) ) { $strSql .= ' applications = ' . $this->sqlApplications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_name = ' . $this->sqlFilterName(). ',' ; } elseif( true == array_key_exists( 'FilterName', $this->getChangedColumns() ) ) { $strSql .= ' filter_name = ' . $this->sqlFilterName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quick_search = ' . $this->sqlQuickSearch(). ',' ; } elseif( true == array_key_exists( 'QuickSearch', $this->getChangedColumns() ) ) { $strSql .= ' quick_search = ' . $this->sqlQuickSearch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contact_min_days = ' . $this->sqlLastContactMinDays(). ',' ; } elseif( true == array_key_exists( 'LastContactMinDays', $this->getChangedColumns() ) ) { $strSql .= ' last_contact_min_days = ' . $this->sqlLastContactMinDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contact_max_days = ' . $this->sqlLastContactMaxDays(). ',' ; } elseif( true == array_key_exists( 'LastContactMaxDays', $this->getChangedColumns() ) ) { $strSql .= ' last_contact_max_days = ' . $this->sqlLastContactMaxDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_delay_days = ' . $this->sqlAdDelayDays(). ',' ; } elseif( true == array_key_exists( 'AdDelayDays', $this->getChangedColumns() ) ) { $strSql .= ' ad_delay_days = ' . $this->sqlAdDelayDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets(). ',' ; } elseif( true == array_key_exists( 'DesiredPets', $this->getChangedColumns() ) ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMin', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMax', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants(). ',' ; } elseif( true == array_key_exists( 'DesiredOccupants', $this->getChangedColumns() ) ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_floors = ' . $this->sqlDesiredFloors(). ',' ; } elseif( true == array_key_exists( 'DesiredFloors', $this->getChangedColumns() ) ) { $strSql .= ' desired_floors = ' . $this->sqlDesiredFloors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_start_date = ' . $this->sqlApplicationStartDate(). ',' ; } elseif( true == array_key_exists( 'ApplicationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' application_start_date = ' . $this->sqlApplicationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_end_date = ' . $this->sqlApplicationEndDate(). ',' ; } elseif( true == array_key_exists( 'ApplicationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' application_end_date = ' . $this->sqlApplicationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin(). ',' ; } elseif( true == array_key_exists( 'MoveInDateMin', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax(). ',' ; } elseif( true == array_key_exists( 'MoveInDateMax', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_by = ' . $this->sqlSortBy(). ',' ; } elseif( true == array_key_exists( 'SortBy', $this->getChangedColumns() ) ) { $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection(). ',' ; } elseif( true == array_key_exists( 'SortDirection', $this->getChangedColumns() ) ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated(). ',' ; } elseif( true == array_key_exists( 'IsIntegrated', $this->getChangedColumns() ) ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'properties' => $this->getProperties(),
			'property_floorplans' => $this->getPropertyFloorplans(),
			'unit_kinds' => $this->getUnitKinds(),
			'application_stage_statuses' => $this->getApplicationStageStatuses(),
			'screening_response_types' => $this->getScreeningResponseTypes(),
			'lease_interval_types' => $this->getLeaseIntervalTypes(),
			'lead_sources' => $this->getLeadSources(),
			'ps_products' => $this->getPsProducts(),
			'company_employees' => $this->getCompanyEmployees(),
			'applications' => $this->getApplications(),
			'filter_name' => $this->getFilterName(),
			'quick_search' => $this->getQuickSearch(),
			'last_contact_min_days' => $this->getLastContactMinDays(),
			'last_contact_max_days' => $this->getLastContactMaxDays(),
			'ad_delay_days' => $this->getAdDelayDays(),
			'desired_bedrooms' => $this->getDesiredBedrooms(),
			'desired_bathrooms' => $this->getDesiredBathrooms(),
			'desired_pets' => $this->getDesiredPets(),
			'desired_rent_min' => $this->getDesiredRentMin(),
			'desired_rent_max' => $this->getDesiredRentMax(),
			'desired_occupants' => $this->getDesiredOccupants(),
			'desired_floors' => $this->getDesiredFloors(),
			'application_start_date' => $this->getApplicationStartDate(),
			'application_end_date' => $this->getApplicationEndDate(),
			'move_in_date_min' => $this->getMoveInDateMin(),
			'move_in_date_max' => $this->getMoveInDateMax(),
			'sort_by' => $this->getSortBy(),
			'sort_direction' => $this->getSortDirection(),
			'is_integrated' => $this->getIsIntegrated(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>