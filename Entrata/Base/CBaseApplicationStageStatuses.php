<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStageStatuses
 * Do not add any new functions to this class.
 */

class CBaseApplicationStageStatuses extends CEosPluralBase {

	/**
	 * @return CApplicationStageStatus[]
	 */
	public static function fetchApplicationStageStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationStageStatus::class, $objDatabase );
	}

	/**
	 * @return CApplicationStageStatus
	 */
	public static function fetchApplicationStageStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationStageStatus::class, $objDatabase );
	}

	public static function fetchApplicationStageStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_stage_statuses', $objDatabase );
	}

	public static function fetchApplicationStageStatusById( $intId, $objDatabase ) {
		return self::fetchApplicationStageStatus( sprintf( 'SELECT * FROM application_stage_statuses WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByLeaseIntervalTypeId( $intLeaseIntervalTypeId, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE lease_interval_type_id = %d', $intLeaseIntervalTypeId ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByApplicationStageId( $intApplicationStageId, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE application_stage_id = %d', $intApplicationStageId ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByApplicationStatusId( $intApplicationStatusId, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE application_status_id = %d', $intApplicationStatusId ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByNextApplicationStageId( $intNextApplicationStageId, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE next_application_stage_id = %d', $intNextApplicationStageId ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByNextApplicationStatusId( $intNextApplicationStatusId, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE next_application_status_id = %d', $intNextApplicationStatusId ), $objDatabase );
	}

}
?>