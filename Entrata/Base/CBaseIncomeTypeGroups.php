<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIncomeTypeGroups
 * Do not add any new functions to this class.
 */

class CBaseIncomeTypeGroups extends CEosPluralBase {

	/**
	 * @return CIncomeTypeGroup[]
	 */
	public static function fetchIncomeTypeGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIncomeTypeGroup::class, $objDatabase );
	}

	/**
	 * @return CIncomeTypeGroup
	 */
	public static function fetchIncomeTypeGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIncomeTypeGroup::class, $objDatabase );
	}

	public static function fetchIncomeTypeGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'income_type_groups', $objDatabase );
	}

	public static function fetchIncomeTypeGroupById( $intId, $objDatabase ) {
		return self::fetchIncomeTypeGroup( sprintf( 'SELECT * FROM income_type_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>