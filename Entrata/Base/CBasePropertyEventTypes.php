<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEventTypes extends CEosPluralBase {

    const TABLE_PROPERTY_EVENT_TYPES = 'public.property_event_types';

    public static function fetchPropertyEventTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CPropertyEventType', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchPropertyEventType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPropertyEventType', $objDatabase );
    }

    public static function fetchPropertyEventTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'property_event_types', $objDatabase );
    }

    public static function fetchPropertyEventTypeByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchPropertyEventType( sprintf( 'SELECT * FROM property_event_types WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyEventTypesByCid( $intCid, $objDatabase ) {
        return self::fetchPropertyEventTypes( sprintf( 'SELECT * FROM property_event_types WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyEventTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchPropertyEventTypes( sprintf( 'SELECT * FROM property_event_types WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyEventTypesByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
        return self::fetchPropertyEventTypes( sprintf( 'SELECT * FROM property_event_types WHERE event_type_id = %d AND cid = %d', (int) $intEventTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyEventTypesByDefaultPropertyEventResultIdByCid( $intDefaultPropertyEventResultId, $intCid, $objDatabase ) {
        return self::fetchPropertyEventTypes( sprintf( 'SELECT * FROM property_event_types WHERE default_property_event_result_id = %d AND cid = %d', (int) $intDefaultPropertyEventResultId, (int) $intCid ), $objDatabase );
    }

}
?>