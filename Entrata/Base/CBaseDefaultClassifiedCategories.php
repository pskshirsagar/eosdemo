<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultClassifiedCategories
 * Do not add any new functions to this class.
 */

class CBaseDefaultClassifiedCategories extends CEosPluralBase {

	/**
	 * @return CDefaultClassifiedCategory[]
	 */
	public static function fetchDefaultClassifiedCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultClassifiedCategory::class, $objDatabase );
	}

	/**
	 * @return CDefaultClassifiedCategory
	 */
	public static function fetchDefaultClassifiedCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultClassifiedCategory::class, $objDatabase );
	}

	public static function fetchDefaultClassifiedCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_classified_categories', $objDatabase );
	}

	public static function fetchDefaultClassifiedCategoryById( $intId, $objDatabase ) {
		return self::fetchDefaultClassifiedCategory( sprintf( 'SELECT * FROM default_classified_categories WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultClassifiedCategoriesByDefaultClassifiedCategoryId( $intDefaultClassifiedCategoryId, $objDatabase ) {
		return self::fetchDefaultClassifiedCategories( sprintf( 'SELECT * FROM default_classified_categories WHERE default_classified_category_id = %d', $intDefaultClassifiedCategoryId ), $objDatabase );
	}

}
?>