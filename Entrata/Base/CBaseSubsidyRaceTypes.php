<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRaceTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyRaceTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyRaceType[]
	 */
	public static function fetchSubsidyRaceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyRaceType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyRaceType
	 */
	public static function fetchSubsidyRaceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyRaceType::class, $objDatabase );
	}

	public static function fetchSubsidyRaceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_race_types', $objDatabase );
	}

	public static function fetchSubsidyRaceTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyRaceType( sprintf( 'SELECT * FROM subsidy_race_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>