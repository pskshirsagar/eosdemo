<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.website_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_intHideOnProspectPortal;
	protected $m_intHideOnResidentPortal;
	protected $m_intHideOnJobPortal;
	protected $m_intHideOnMobilePortal;
	protected $m_intHideOnMobilePortalCorporatePage;
	protected $m_intHideMoreInfoButton;
	protected $m_intShowApplicationFeeOption;
	protected $m_intMoreInfoAction;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintWebsiteJourneys;
	protected $m_strSlugUrl;
	protected $m_jsonSlugUrl;

	public function __construct() {
		parent::__construct();

		$this->m_intHideOnProspectPortal = '0';
		$this->m_intHideOnResidentPortal = '0';
		$this->m_intHideOnJobPortal = '0';
		$this->m_intHideOnMobilePortal = '0';
		$this->m_intHideOnMobilePortalCorporatePage = '0';
		$this->m_intHideMoreInfoButton = '0';
		$this->m_intShowApplicationFeeOption = '0';
		$this->m_intMoreInfoAction = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['hide_on_prospect_portal'] ) && $boolDirectSet ) $this->set( 'm_intHideOnProspectPortal', trim( $arrValues['hide_on_prospect_portal'] ) ); elseif( isset( $arrValues['hide_on_prospect_portal'] ) ) $this->setHideOnProspectPortal( $arrValues['hide_on_prospect_portal'] );
		if( isset( $arrValues['hide_on_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_intHideOnResidentPortal', trim( $arrValues['hide_on_resident_portal'] ) ); elseif( isset( $arrValues['hide_on_resident_portal'] ) ) $this->setHideOnResidentPortal( $arrValues['hide_on_resident_portal'] );
		if( isset( $arrValues['hide_on_job_portal'] ) && $boolDirectSet ) $this->set( 'm_intHideOnJobPortal', trim( $arrValues['hide_on_job_portal'] ) ); elseif( isset( $arrValues['hide_on_job_portal'] ) ) $this->setHideOnJobPortal( $arrValues['hide_on_job_portal'] );
		if( isset( $arrValues['hide_more_info_button'] ) && $boolDirectSet ) $this->set( 'm_intHideMoreInfoButton', trim( $arrValues['hide_more_info_button'] ) ); elseif( isset( $arrValues['hide_more_info_button'] ) ) $this->setHideMoreInfoButton( $arrValues['hide_more_info_button'] );
		if( isset( $arrValues['show_application_fee_option'] ) && $boolDirectSet ) $this->set( 'm_intShowApplicationFeeOption', trim( $arrValues['show_application_fee_option'] ) ); elseif( isset( $arrValues['show_application_fee_option'] ) ) $this->setShowApplicationFeeOption( $arrValues['show_application_fee_option'] );
		if( isset( $arrValues['more_info_action'] ) && $boolDirectSet ) $this->set( 'm_intMoreInfoAction', trim( $arrValues['more_info_action'] ) ); elseif( isset( $arrValues['more_info_action'] ) ) $this->setMoreInfoAction( $arrValues['more_info_action'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['website_journeys'] ) && $boolDirectSet ) $this->set( 'm_arrintWebsiteJourneys', trim( $arrValues['website_journeys'] ) ); elseif( isset( $arrValues['website_journeys'] ) ) $this->setWebsiteJourneys( $arrValues['website_journeys'] );
		if( isset( $arrValues['slug_url'] ) ) $this->set( 'm_strSlugUrl', trim( $arrValues['slug_url'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setHideOnProspectPortal( $intHideOnProspectPortal ) {
		$this->set( 'm_intHideOnProspectPortal', CStrings::strToIntDef( $intHideOnProspectPortal, NULL, false ) );
	}

	public function getHideOnProspectPortal() {
		return $this->m_intHideOnProspectPortal;
	}

	public function sqlHideOnProspectPortal() {
		return ( true == isset( $this->m_intHideOnProspectPortal ) ) ? ( string ) $this->m_intHideOnProspectPortal : '0';
	}

	public function setHideOnResidentPortal( $intHideOnResidentPortal ) {
		$this->set( 'm_intHideOnResidentPortal', CStrings::strToIntDef( $intHideOnResidentPortal, NULL, false ) );
	}

	public function getHideOnResidentPortal() {
		return $this->m_intHideOnResidentPortal;
	}

	public function sqlHideOnResidentPortal() {
		return ( true == isset( $this->m_intHideOnResidentPortal ) ) ? ( string ) $this->m_intHideOnResidentPortal : '0';
	}

	public function setHideOnJobPortal( $intHideOnJobPortal ) {
		$this->set( 'm_intHideOnJobPortal', CStrings::strToIntDef( $intHideOnJobPortal, NULL, false ) );
	}

	public function getHideOnJobPortal() {
		return $this->m_intHideOnJobPortal;
	}

	public function sqlHideOnJobPortal() {
		return ( true == isset( $this->m_intHideOnJobPortal ) ) ? ( string ) $this->m_intHideOnJobPortal : '0';
	}

	public function setHideOnMobilePortal( $intHideOnMobilePortal ) {
		$this->set( 'm_intHideOnMobilePortal', CStrings::strToIntDef( $intHideOnMobilePortal, NULL, false ) );
	}

	public function getHideOnMobilePortal() {
		return $this->m_intHideOnMobilePortal;
	}

	public function sqlHideOnMobilePortal() {
		return ( true == isset( $this->m_intHideOnMobilePortal ) ) ? ( string ) $this->m_intHideOnMobilePortal : '0';
	}

	public function setHideOnMobilePortalCorporatePage( $intHideOnMobilePortalCorporatePage ) {
		$this->set( 'm_intHideOnMobilePortalCorporatePage', CStrings::strToIntDef( $intHideOnMobilePortalCorporatePage, NULL, false ) );
	}

	public function getHideOnMobilePortalCorporatePage() {
		return $this->m_intHideOnMobilePortalCorporatePage;
	}

	public function sqlHideOnMobilePortalCorporatePage() {
		return ( true == isset( $this->m_intHideOnMobilePortalCorporatePage ) ) ? ( string ) $this->m_intHideOnMobilePortalCorporatePage : '0';
	}

	public function setHideMoreInfoButton( $intHideMoreInfoButton ) {
		$this->set( 'm_intHideMoreInfoButton', CStrings::strToIntDef( $intHideMoreInfoButton, NULL, false ) );
	}

	public function getHideMoreInfoButton() {
		return $this->m_intHideMoreInfoButton;
	}

	public function sqlHideMoreInfoButton() {
		return ( true == isset( $this->m_intHideMoreInfoButton ) ) ? ( string ) $this->m_intHideMoreInfoButton : '0';
	}

	public function setShowApplicationFeeOption( $intShowApplicationFeeOption ) {
		$this->set( 'm_intShowApplicationFeeOption', CStrings::strToIntDef( $intShowApplicationFeeOption, NULL, false ) );
	}

	public function getShowApplicationFeeOption() {
		return $this->m_intShowApplicationFeeOption;
	}

	public function sqlShowApplicationFeeOption() {
		return ( true == isset( $this->m_intShowApplicationFeeOption ) ) ? ( string ) $this->m_intShowApplicationFeeOption : '0';
	}

	public function setMoreInfoAction( $intMoreInfoAction ) {
		$this->set( 'm_intMoreInfoAction', CStrings::strToIntDef( $intMoreInfoAction, NULL, false ) );
	}

	public function getMoreInfoAction() {
		return $this->m_intMoreInfoAction;
	}

	public function sqlMoreInfoAction() {
		return ( true == isset( $this->m_intMoreInfoAction ) ) ? ( string ) $this->m_intMoreInfoAction : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWebsiteJourneys( $arrintWebsiteJourneys ) {
		$this->set( 'm_arrintWebsiteJourneys', CStrings::strToArrIntDef( $arrintWebsiteJourneys, NULL ) );
	}

	public function getWebsiteJourneys() {
		return $this->m_arrintWebsiteJourneys;
	}

	public function sqlWebsiteJourneys() {
		return ( true == isset( $this->m_arrintWebsiteJourneys ) && true == valArr( $this->m_arrintWebsiteJourneys ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintWebsiteJourneys, NULL ) . '\'' : 'NULL';
	}

	public function setSlugUrl( $jsonSlugUrl ) {
		if( true == valObj( $jsonSlugUrl, 'stdClass' ) ) {
			$this->set( 'm_jsonSlugUrl', $jsonSlugUrl );
		} elseif( true == valJsonString( $jsonSlugUrl ) ) {
			$this->set( 'm_jsonSlugUrl', CStrings::strToJson( $jsonSlugUrl ) );
		} else {
			$this->set( 'm_jsonSlugUrl', NULL );
		}
		unset( $this->m_strSlugUrl );
	}

	public function getSlugUrl() {
		if( true == isset( $this->m_strSlugUrl ) ) {
			$this->m_jsonSlugUrl = CStrings::strToJson( $this->m_strSlugUrl );
			unset( $this->m_strSlugUrl );
		}
		return $this->m_jsonSlugUrl;
	}

	public function sqlSlugUrl() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSlugUrl() ) ) ) {
			return  '\'' . addslashes( CStrings::jsonToStrDef( $this->getSlugUrl() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
            ' . static::TABLE_NAME . '( id, cid, website_id, property_id, hide_on_prospect_portal, hide_on_resident_portal, hide_on_job_portal, hide_more_info_button, show_application_fee_option, more_info_action, order_num, created_by, created_on, website_journeys, slug_url )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlHideOnProspectPortal() . ', ' .
						$this->sqlHideOnResidentPortal() . ', ' .
						$this->sqlHideOnJobPortal() . ', ' .
						$this->sqlHideMoreInfoButton() . ', ' .
						$this->sqlShowApplicationFeeOption() . ', ' .
						$this->sqlMoreInfoAction() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
		                $this->sqlWebsiteJourneys() . ', ' .
		                $this->sqlSlugUrl() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_on_prospect_portal = ' . $this->sqlHideOnProspectPortal(). ',' ; } elseif( true == array_key_exists( 'HideOnProspectPortal', $this->getChangedColumns() ) ) { $strSql .= ' hide_on_prospect_portal = ' . $this->sqlHideOnProspectPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_on_resident_portal = ' . $this->sqlHideOnResidentPortal(). ',' ; } elseif( true == array_key_exists( 'HideOnResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' hide_on_resident_portal = ' . $this->sqlHideOnResidentPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_on_job_portal = ' . $this->sqlHideOnJobPortal(). ',' ; } elseif( true == array_key_exists( 'HideOnJobPortal', $this->getChangedColumns() ) ) { $strSql .= ' hide_on_job_portal = ' . $this->sqlHideOnJobPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_more_info_button = ' . $this->sqlHideMoreInfoButton(). ',' ; } elseif( true == array_key_exists( 'HideMoreInfoButton', $this->getChangedColumns() ) ) { $strSql .= ' hide_more_info_button = ' . $this->sqlHideMoreInfoButton() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_application_fee_option = ' . $this->sqlShowApplicationFeeOption(). ',' ; } elseif( true == array_key_exists( 'ShowApplicationFeeOption', $this->getChangedColumns() ) ) { $strSql .= ' show_application_fee_option = ' . $this->sqlShowApplicationFeeOption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' more_info_action = ' . $this->sqlMoreInfoAction(). ',' ; } elseif( true == array_key_exists( 'MoreInfoAction', $this->getChangedColumns() ) ) { $strSql .= ' more_info_action = ' . $this->sqlMoreInfoAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_journeys = ' . $this->sqlWebsiteJourneys(). ',' ; } elseif( true == array_key_exists( 'WebsiteJourneys', $this->getChangedColumns() ) ) { $strSql .= ' website_journeys = ' . $this->sqlWebsiteJourneys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' slug_url = ' . $this->sqlSlugUrl() ; } elseif( true == array_key_exists( 'SlugUrl', $this->getChangedColumns() ) ) { $strSql .= ' slug_url = ' . $this->sqlSlugUrl() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'property_id' => $this->getPropertyId(),
			'hide_on_prospect_portal' => $this->getHideOnProspectPortal(),
			'hide_on_resident_portal' => $this->getHideOnResidentPortal(),
			'hide_on_job_portal' => $this->getHideOnJobPortal(),
			'hide_more_info_button' => $this->getHideMoreInfoButton(),
			'show_application_fee_option' => $this->getShowApplicationFeeOption(),
			'more_info_action' => $this->getMoreInfoAction(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'website_journeys' => $this->getWebsiteJourneys(),
			'slug_url' => $this->getSlugUrl()
		);
	}

}
?>