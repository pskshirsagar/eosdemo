<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSettingGroups
 * Do not add any new functions to this class.
 */

class CBaseApplicationSettingGroups extends CEosPluralBase {

	/**
	 * @return CApplicationSettingGroup[]
	 */
	public static function fetchApplicationSettingGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CApplicationSettingGroup', $objDatabase );
	}

	/**
	 * @return CApplicationSettingGroup
	 */
	public static function fetchApplicationSettingGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationSettingGroup', $objDatabase );
	}

	public static function fetchApplicationSettingGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_setting_groups', $objDatabase );
	}

	public static function fetchApplicationSettingGroupById( $intId, $objDatabase ) {
		return self::fetchApplicationSettingGroup( sprintf( 'SELECT * FROM application_setting_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchApplicationSettingGroupsByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchApplicationSettingGroups( sprintf( 'SELECT * FROM application_setting_groups WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

	public static function fetchApplicationSettingGroupsByToolTipId( $intToolTipId, $objDatabase ) {
		return self::fetchApplicationSettingGroups( sprintf( 'SELECT * FROM application_setting_groups WHERE tool_tip_id = %d', ( int ) $intToolTipId ), $objDatabase );
	}

}
?>