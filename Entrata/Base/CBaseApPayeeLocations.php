<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeLocations extends CEosPluralBase {

	/**
	 * @return CApPayeeLocation[]
	 */
	public static function fetchApPayeeLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayeeLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeLocation
	 */
	public static function fetchApPayeeLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeLocation::class, $objDatabase );
	}

	public static function fetchApPayeeLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_locations', $objDatabase );
	}

	public static function fetchApPayeeLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeLocation( sprintf( 'SELECT * FROM ap_payee_locations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeLocationsByDefaultApRoutingTagIdByCid( $intDefaultApRoutingTagId, $intCid, $objDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE default_ap_routing_tag_id = %d AND cid = %d', $intDefaultApRoutingTagId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeLocationsByApLegalEntityIdByCid( $intApLegalEntityId, $intCid, $objDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE ap_legal_entity_id = %d AND cid = %d', $intApLegalEntityId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeLocationsByStoreIdByCid( $intStoreId, $intCid, $objDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE store_id = %d AND cid = %d', $intStoreId, $intCid ), $objDatabase );
	}

}
?>