<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRaceSubTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyRaceSubTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyRaceSubType[]
	 */
	public static function fetchSubsidyRaceSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidyRaceSubType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyRaceSubType
	 */
	public static function fetchSubsidyRaceSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidyRaceSubType::class, $objDatabase );
	}

	public static function fetchSubsidyRaceSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_race_sub_types', $objDatabase );
	}

	public static function fetchSubsidyRaceSubTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyRaceSubType( sprintf( 'SELECT * FROM subsidy_race_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyRaceSubTypesBySubsidyRaceTypeId( $intSubsidyRaceTypeId, $objDatabase ) {
		return self::fetchSubsidyRaceSubTypes( sprintf( 'SELECT * FROM subsidy_race_sub_types WHERE subsidy_race_type_id = %d', $intSubsidyRaceTypeId ), $objDatabase );
	}

}
?>