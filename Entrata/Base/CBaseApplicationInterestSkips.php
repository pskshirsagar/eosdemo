<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationInterestSkips
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationInterestSkips extends CEosPluralBase {

	/**
	 * @return CApplicationInterestSkip[]
	 */
	public static function fetchApplicationInterestSkips( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplicationInterestSkip::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationInterestSkip
	 */
	public static function fetchApplicationInterestSkip( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationInterestSkip::class, $objDatabase );
	}

	public static function fetchApplicationInterestSkipCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_interest_skips', $objDatabase );
	}

	public static function fetchApplicationInterestSkipByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterestSkip( sprintf( 'SELECT * FROM application_interest_skips WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestSkipsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationInterestSkips( sprintf( 'SELECT * FROM application_interest_skips WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestSkipsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterestSkips( sprintf( 'SELECT * FROM application_interest_skips WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestSkipsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterestSkips( sprintf( 'SELECT * FROM application_interest_skips WHERE application_id = %d AND cid = %d', $intApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestSkipsByApplicationInterestIdByCid( $intApplicationInterestId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterestSkips( sprintf( 'SELECT * FROM application_interest_skips WHERE application_interest_id = %d AND cid = %d', $intApplicationInterestId, $intCid ), $objDatabase );
	}

}
?>