<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileSignatures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileSignatures extends CEosPluralBase {

	/**
	 * @return CFileSignature[]
	 */
	public static function fetchFileSignatures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileSignature::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileSignature
	 */
	public static function fetchFileSignature( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileSignature::class, $objDatabase );
	}

	public static function fetchFileSignatureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_signatures', $objDatabase );
	}

	public static function fetchFileSignatureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileSignature( sprintf( 'SELECT * FROM file_signatures WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileSignaturesByCid( $intCid, $objDatabase ) {
		return self::fetchFileSignatures( sprintf( 'SELECT * FROM file_signatures WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileSignaturesByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchFileSignatures( sprintf( 'SELECT * FROM file_signatures WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileSignaturesBySignatureFileIdByCid( $intSignatureFileId, $intCid, $objDatabase ) {
		return self::fetchFileSignatures( sprintf( 'SELECT * FROM file_signatures WHERE signature_file_id = %d AND cid = %d', ( int ) $intSignatureFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileSignaturesByFileAssociationIdByCid( $intFileAssociationId, $intCid, $objDatabase ) {
		return self::fetchFileSignatures( sprintf( 'SELECT * FROM file_signatures WHERE file_association_id = %d AND cid = %d', ( int ) $intFileAssociationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileSignaturesByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchFileSignatures( sprintf( 'SELECT * FROM file_signatures WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>