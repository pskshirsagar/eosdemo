<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantAddresses extends CEosPluralBase {

	/**
	 * @return CApplicantAddress[]
	 */
	public static function fetchApplicantAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplicantAddress::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantAddress
	 */
	public static function fetchApplicantAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicantAddress::class, $objDatabase );
	}

	public static function fetchApplicantAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_addresses', $objDatabase );
	}

	public static function fetchApplicantAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddress( sprintf( 'SELECT * FROM applicant_addresses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE address_type_id = %d AND cid = %d', $intAddressTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByCompanyResidenceTypeIdByCid( $intCompanyResidenceTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE company_residence_type_id = %d AND cid = %d', $intCompanyResidenceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByTimeZoneIdByCid( $intTimeZoneId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE time_zone_id = %d AND cid = %d', $intTimeZoneId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantAddressesByMoveOutReasonListItemIdByCid( $intMoveOutReasonListItemId, $intCid, $objDatabase ) {
		return self::fetchApplicantAddresses( sprintf( 'SELECT * FROM applicant_addresses WHERE move_out_reason_list_item_id = %d AND cid = %d', $intMoveOutReasonListItemId, $intCid ), $objDatabase );
	}

}
?>