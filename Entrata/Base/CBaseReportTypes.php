<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportTypes
 * Do not add any new functions to this class.
 */

class CBaseReportTypes extends CEosPluralBase {

	/**
	 * @return CReportType[]
	 */
	public static function fetchReportTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportType::class, $objDatabase );
	}

	/**
	 * @return CReportType
	 */
	public static function fetchReportType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportType::class, $objDatabase );
	}

	public static function fetchReportTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_types', $objDatabase );
	}

	public static function fetchReportTypeById( $intId, $objDatabase ) {
		return self::fetchReportType( sprintf( 'SELECT * FROM report_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>