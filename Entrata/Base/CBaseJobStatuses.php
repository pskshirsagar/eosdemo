<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobStatuses
 * Do not add any new functions to this class.
 */

class CBaseJobStatuses extends CEosPluralBase {

	/**
	 * @return CJobStatus[]
	 */
	public static function fetchJobStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CJobStatus::class, $objDatabase );
	}

	/**
	 * @return CJobStatus
	 */
	public static function fetchJobStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobStatus::class, $objDatabase );
	}

	public static function fetchJobStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_statuses', $objDatabase );
	}

	public static function fetchJobStatusById( $intId, $objDatabase ) {
		return self::fetchJobStatus( sprintf( 'SELECT * FROM job_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>