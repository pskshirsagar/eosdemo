<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMobileApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMobileApplications extends CEosPluralBase {

	/**
	 * @return CMobileApplication[]
	 */
	public static function fetchMobileApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMobileApplication', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMobileApplication
	 */
	public static function fetchMobileApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMobileApplication', $objDatabase );
	}

	public static function fetchMobileApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mobile_applications', $objDatabase );
	}

	public static function fetchMobileApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMobileApplication( sprintf( 'SELECT * FROM mobile_applications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMobileApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchMobileApplications( sprintf( 'SELECT * FROM mobile_applications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMobileApplicationsByMobileApplicationTypeIdByCid( $intMobileApplicationTypeId, $intCid, $objDatabase ) {
		return self::fetchMobileApplications( sprintf( 'SELECT * FROM mobile_applications WHERE mobile_application_type_id = %d AND cid = %d', ( int ) $intMobileApplicationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>