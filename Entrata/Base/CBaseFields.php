<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFields
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFields extends CEosPluralBase {

	/**
	 * @return CField[]
	 */
	public static function fetchFields( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CField', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CField
	 */
	public static function fetchField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CField', $objDatabase );
	}

	public static function fetchFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fields', $objDatabase );
	}

	public static function fetchFieldByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchField( sprintf( 'SELECT * FROM fields WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldsByCid( $intCid, $objDatabase ) {
		return self::fetchFields( sprintf( 'SELECT * FROM fields WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldsByFieldTypeIdByCid( $intFieldTypeId, $intCid, $objDatabase ) {
		return self::fetchFields( sprintf( 'SELECT * FROM fields WHERE field_type_id = %d AND cid = %d', ( int ) $intFieldTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldsByFieldGroupIdByCid( $intFieldGroupId, $intCid, $objDatabase ) {
		return self::fetchFields( sprintf( 'SELECT * FROM fields WHERE field_group_id = %d AND cid = %d', ( int ) $intFieldGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldsByInputTypeIdByCid( $intInputTypeId, $intCid, $objDatabase ) {
		return self::fetchFields( sprintf( 'SELECT * FROM fields WHERE input_type_id = %d AND cid = %d', ( int ) $intInputTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>