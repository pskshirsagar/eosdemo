<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetGlAccountActivity extends CEosSingularBase {

	const TABLE_NAME = 'public.budget_gl_account_activities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetId;
	protected $m_intBudgetActivityTypeId;
	protected $m_intCompanyUserId;
	protected $m_intBudgetTabId;
	protected $m_intBudgetTabGlAccountId;
	protected $m_strPostMonth;
	protected $m_strComment;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intBudgetAssumptionMonthId;
	protected $m_intBudgetAssumptionId;
	protected $m_strOldValue;
	protected $m_strNewValue;
	protected $m_intBudgetTabGlAccountItemId;
	protected $m_intBudgetTabGlAccountItemMonthId;
	protected $m_intBudgetGlAccountActivityTargetId;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['budget_activity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetActivityTypeId', trim( $arrValues['budget_activity_type_id'] ) ); elseif( isset( $arrValues['budget_activity_type_id'] ) ) $this->setBudgetActivityTypeId( $arrValues['budget_activity_type_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['budget_tab_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabId', trim( $arrValues['budget_tab_id'] ) ); elseif( isset( $arrValues['budget_tab_id'] ) ) $this->setBudgetTabId( $arrValues['budget_tab_id'] );
		if( isset( $arrValues['budget_tab_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabGlAccountId', trim( $arrValues['budget_tab_gl_account_id'] ) ); elseif( isset( $arrValues['budget_tab_gl_account_id'] ) ) $this->setBudgetTabGlAccountId( $arrValues['budget_tab_gl_account_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['comment'] ) && $boolDirectSet ) $this->set( 'm_strComment', trim( stripcslashes( $arrValues['comment'] ) ) ); elseif( isset( $arrValues['comment'] ) ) $this->setComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comment'] ) : $arrValues['comment'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['budget_assumption_month_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetAssumptionMonthId', trim( $arrValues['budget_assumption_month_id'] ) ); elseif( isset( $arrValues['budget_assumption_month_id'] ) ) $this->setBudgetAssumptionMonthId( $arrValues['budget_assumption_month_id'] );
		if( isset( $arrValues['budget_assumption_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetAssumptionId', trim( $arrValues['budget_assumption_id'] ) ); elseif( isset( $arrValues['budget_assumption_id'] ) ) $this->setBudgetAssumptionId( $arrValues['budget_assumption_id'] );
		if( isset( $arrValues['old_value'] ) && $boolDirectSet ) $this->set( 'm_strOldValue', trim( stripcslashes( $arrValues['old_value'] ) ) ); elseif( isset( $arrValues['old_value'] ) ) $this->setOldValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_value'] ) : $arrValues['old_value'] );
		if( isset( $arrValues['new_value'] ) && $boolDirectSet ) $this->set( 'm_strNewValue', trim( stripcslashes( $arrValues['new_value'] ) ) ); elseif( isset( $arrValues['new_value'] ) ) $this->setNewValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_value'] ) : $arrValues['new_value'] );
		if( isset( $arrValues['budget_tab_gl_account_item_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabGlAccountItemId', trim( $arrValues['budget_tab_gl_account_item_id'] ) ); elseif( isset( $arrValues['budget_tab_gl_account_item_id'] ) ) $this->setBudgetTabGlAccountItemId( $arrValues['budget_tab_gl_account_item_id'] );
		if( isset( $arrValues['budget_tab_gl_account_item_month_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabGlAccountItemMonthId', trim( $arrValues['budget_tab_gl_account_item_month_id'] ) ); elseif( isset( $arrValues['budget_tab_gl_account_item_month_id'] ) ) $this->setBudgetTabGlAccountItemMonthId( $arrValues['budget_tab_gl_account_item_month_id'] );
		if( isset( $arrValues['budget_gl_account_activity_target_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetGlAccountActivityTargetId', trim( $arrValues['budget_gl_account_activity_target_id'] ) ); elseif( isset( $arrValues['budget_gl_account_activity_target_id'] ) ) $this->setBudgetGlAccountActivityTargetId( $arrValues['budget_gl_account_activity_target_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setBudgetActivityTypeId( $intBudgetActivityTypeId ) {
		$this->set( 'm_intBudgetActivityTypeId', CStrings::strToIntDef( $intBudgetActivityTypeId, NULL, false ) );
	}

	public function getBudgetActivityTypeId() {
		return $this->m_intBudgetActivityTypeId;
	}

	public function sqlBudgetActivityTypeId() {
		return ( true == isset( $this->m_intBudgetActivityTypeId ) ) ? ( string ) $this->m_intBudgetActivityTypeId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setBudgetTabId( $intBudgetTabId ) {
		$this->set( 'm_intBudgetTabId', CStrings::strToIntDef( $intBudgetTabId, NULL, false ) );
	}

	public function getBudgetTabId() {
		return $this->m_intBudgetTabId;
	}

	public function sqlBudgetTabId() {
		return ( true == isset( $this->m_intBudgetTabId ) ) ? ( string ) $this->m_intBudgetTabId : 'NULL';
	}

	public function setBudgetTabGlAccountId( $intBudgetTabGlAccountId ) {
		$this->set( 'm_intBudgetTabGlAccountId', CStrings::strToIntDef( $intBudgetTabGlAccountId, NULL, false ) );
	}

	public function getBudgetTabGlAccountId() {
		return $this->m_intBudgetTabGlAccountId;
	}

	public function sqlBudgetTabGlAccountId() {
		return ( true == isset( $this->m_intBudgetTabGlAccountId ) ) ? ( string ) $this->m_intBudgetTabGlAccountId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setComment( $strComment ) {
		$this->set( 'm_strComment', CStrings::strTrimDef( $strComment, -1, NULL, true ) );
	}

	public function getComment() {
		return $this->m_strComment;
	}

	public function sqlComment() {
		return ( true == isset( $this->m_strComment ) ) ? '\'' . addslashes( $this->m_strComment ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setBudgetAssumptionMonthId( $intBudgetAssumptionMonthId ) {
		$this->set( 'm_intBudgetAssumptionMonthId', CStrings::strToIntDef( $intBudgetAssumptionMonthId, NULL, false ) );
	}

	public function getBudgetAssumptionMonthId() {
		return $this->m_intBudgetAssumptionMonthId;
	}

	public function sqlBudgetAssumptionMonthId() {
		return ( true == isset( $this->m_intBudgetAssumptionMonthId ) ) ? ( string ) $this->m_intBudgetAssumptionMonthId : 'NULL';
	}

	public function setBudgetAssumptionId( $intBudgetAssumptionId ) {
		$this->set( 'm_intBudgetAssumptionId', CStrings::strToIntDef( $intBudgetAssumptionId, NULL, false ) );
	}

	public function getBudgetAssumptionId() {
		return $this->m_intBudgetAssumptionId;
	}

	public function sqlBudgetAssumptionId() {
		return ( true == isset( $this->m_intBudgetAssumptionId ) ) ? ( string ) $this->m_intBudgetAssumptionId : 'NULL';
	}

	public function setOldValue( $strOldValue ) {
		$this->set( 'm_strOldValue', CStrings::strTrimDef( $strOldValue, 100, NULL, true ) );
	}

	public function getOldValue() {
		return $this->m_strOldValue;
	}

	public function sqlOldValue() {
		return ( true == isset( $this->m_strOldValue ) ) ? '\'' . addslashes( $this->m_strOldValue ) . '\'' : 'NULL';
	}

	public function setNewValue( $strNewValue ) {
		$this->set( 'm_strNewValue', CStrings::strTrimDef( $strNewValue, 100, NULL, true ) );
	}

	public function getNewValue() {
		return $this->m_strNewValue;
	}

	public function sqlNewValue() {
		return ( true == isset( $this->m_strNewValue ) ) ? '\'' . addslashes( $this->m_strNewValue ) . '\'' : 'NULL';
	}

	public function setBudgetTabGlAccountItemId( $intBudgetTabGlAccountItemId ) {
		$this->set( 'm_intBudgetTabGlAccountItemId', CStrings::strToIntDef( $intBudgetTabGlAccountItemId, NULL, false ) );
	}

	public function getBudgetTabGlAccountItemId() {
		return $this->m_intBudgetTabGlAccountItemId;
	}

	public function sqlBudgetTabGlAccountItemId() {
		return ( true == isset( $this->m_intBudgetTabGlAccountItemId ) ) ? ( string ) $this->m_intBudgetTabGlAccountItemId : 'NULL';
	}

	public function setBudgetTabGlAccountItemMonthId( $intBudgetTabGlAccountItemMonthId ) {
		$this->set( 'm_intBudgetTabGlAccountItemMonthId', CStrings::strToIntDef( $intBudgetTabGlAccountItemMonthId, NULL, false ) );
	}

	public function getBudgetTabGlAccountItemMonthId() {
		return $this->m_intBudgetTabGlAccountItemMonthId;
	}

	public function sqlBudgetTabGlAccountItemMonthId() {
		return ( true == isset( $this->m_intBudgetTabGlAccountItemMonthId ) ) ? ( string ) $this->m_intBudgetTabGlAccountItemMonthId : 'NULL';
	}

	public function setBudgetGlAccountActivityTargetId( $intBudgetGlAccountActivityTargetId ) {
		$this->set( 'm_intBudgetGlAccountActivityTargetId', CStrings::strToIntDef( $intBudgetGlAccountActivityTargetId, NULL, false ) );
	}

	public function getBudgetGlAccountActivityTargetId() {
		return $this->m_intBudgetGlAccountActivityTargetId;
	}

	public function sqlBudgetGlAccountActivityTargetId() {
		return ( true == isset( $this->m_intBudgetGlAccountActivityTargetId ) ) ? ( string ) $this->m_intBudgetGlAccountActivityTargetId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_id, budget_activity_type_id, company_user_id, budget_tab_id, budget_tab_gl_account_id, post_month, comment, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, budget_assumption_month_id, budget_assumption_id, old_value, new_value, budget_tab_gl_account_item_id, budget_tab_gl_account_item_month_id, budget_gl_account_activity_target_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetId() . ', ' .
						$this->sqlBudgetActivityTypeId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlBudgetTabId() . ', ' .
						$this->sqlBudgetTabGlAccountId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlComment() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlBudgetAssumptionMonthId() . ', ' .
						$this->sqlBudgetAssumptionId() . ', ' .
						$this->sqlOldValue() . ', ' .
						$this->sqlNewValue() . ', ' .
						$this->sqlBudgetTabGlAccountItemId() . ', ' .
						$this->sqlBudgetTabGlAccountItemMonthId() . ', ' .
						$this->sqlBudgetGlAccountActivityTargetId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId(). ',' ; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_activity_type_id = ' . $this->sqlBudgetActivityTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetActivityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_activity_type_id = ' . $this->sqlBudgetActivityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_id = ' . $this->sqlBudgetTabId(). ',' ; } elseif( true == array_key_exists( 'BudgetTabId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_id = ' . $this->sqlBudgetTabId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId(). ',' ; } elseif( true == array_key_exists( 'BudgetTabGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comment = ' . $this->sqlComment(). ',' ; } elseif( true == array_key_exists( 'Comment', $this->getChangedColumns() ) ) { $strSql .= ' comment = ' . $this->sqlComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_assumption_month_id = ' . $this->sqlBudgetAssumptionMonthId(). ',' ; } elseif( true == array_key_exists( 'BudgetAssumptionMonthId', $this->getChangedColumns() ) ) { $strSql .= ' budget_assumption_month_id = ' . $this->sqlBudgetAssumptionMonthId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_assumption_id = ' . $this->sqlBudgetAssumptionId(). ',' ; } elseif( true == array_key_exists( 'BudgetAssumptionId', $this->getChangedColumns() ) ) { $strSql .= ' budget_assumption_id = ' . $this->sqlBudgetAssumptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_value = ' . $this->sqlOldValue(). ',' ; } elseif( true == array_key_exists( 'OldValue', $this->getChangedColumns() ) ) { $strSql .= ' old_value = ' . $this->sqlOldValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_value = ' . $this->sqlNewValue(). ',' ; } elseif( true == array_key_exists( 'NewValue', $this->getChangedColumns() ) ) { $strSql .= ' new_value = ' . $this->sqlNewValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_gl_account_item_id = ' . $this->sqlBudgetTabGlAccountItemId(). ',' ; } elseif( true == array_key_exists( 'BudgetTabGlAccountItemId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_gl_account_item_id = ' . $this->sqlBudgetTabGlAccountItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_gl_account_item_month_id = ' . $this->sqlBudgetTabGlAccountItemMonthId(). ',' ; } elseif( true == array_key_exists( 'BudgetTabGlAccountItemMonthId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_gl_account_item_month_id = ' . $this->sqlBudgetTabGlAccountItemMonthId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_gl_account_activity_target_id = ' . $this->sqlBudgetGlAccountActivityTargetId(). ',' ; } elseif( true == array_key_exists( 'BudgetGlAccountActivityTargetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_gl_account_activity_target_id = ' . $this->sqlBudgetGlAccountActivityTargetId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_id' => $this->getBudgetId(),
			'budget_activity_type_id' => $this->getBudgetActivityTypeId(),
			'company_user_id' => $this->getCompanyUserId(),
			'budget_tab_id' => $this->getBudgetTabId(),
			'budget_tab_gl_account_id' => $this->getBudgetTabGlAccountId(),
			'post_month' => $this->getPostMonth(),
			'comment' => $this->getComment(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'budget_assumption_month_id' => $this->getBudgetAssumptionMonthId(),
			'budget_assumption_id' => $this->getBudgetAssumptionId(),
			'old_value' => $this->getOldValue(),
			'new_value' => $this->getNewValue(),
			'budget_tab_gl_account_item_id' => $this->getBudgetTabGlAccountItemId(),
			'budget_tab_gl_account_item_month_id' => $this->getBudgetTabGlAccountItemMonthId(),
			'budget_gl_account_activity_target_id' => $this->getBudgetGlAccountActivityTargetId()
		);
	}

}
?>