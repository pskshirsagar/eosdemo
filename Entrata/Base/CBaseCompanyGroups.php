<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyGroups extends CEosPluralBase {

	/**
	 * @return CCompanyGroup[]
	 */
	public static function fetchCompanyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyGroup
	 */
	public static function fetchCompanyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyGroup', $objDatabase );
	}

	public static function fetchCompanyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_groups', $objDatabase );
	}

	public static function fetchCompanyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroup( sprintf( 'SELECT * FROM company_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyGroups( sprintf( 'SELECT * FROM company_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroups( sprintf( 'SELECT * FROM company_groups WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>