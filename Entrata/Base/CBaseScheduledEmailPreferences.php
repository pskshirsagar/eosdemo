<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledEmailPreferences extends CEosPluralBase {

	/**
	 * @return CScheduledEmailPreference[]
	 */
	public static function fetchScheduledEmailPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledEmailPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledEmailPreference
	 */
	public static function fetchScheduledEmailPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledEmailPreference', $objDatabase );
	}

	public static function fetchScheduledEmailPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_email_preferences', $objDatabase );
	}

	public static function fetchScheduledEmailPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledEmailPreference( sprintf( 'SELECT * FROM scheduled_email_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledEmailPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledEmailPreferences( sprintf( 'SELECT * FROM scheduled_email_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledEmailPreferencesByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		return self::fetchScheduledEmailPreferences( sprintf( 'SELECT * FROM scheduled_email_preferences WHERE scheduled_email_id = %d AND cid = %d', ( int ) $intScheduledEmailId, ( int ) $intCid ), $objDatabase );
	}

}
?>