<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStoredObjects
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseStoredObjects extends CEosPluralBase {

	/**
	 * @return CStoredObject[]
	 */
	public static function fetchStoredObjects( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CStoredObject::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CStoredObject
	 */
	public static function fetchStoredObject( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoredObject::class, $objDatabase );
	}

	public static function fetchStoredObjectCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stored_objects', $objDatabase );
	}

	public static function fetchStoredObjectByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchStoredObject( sprintf( 'SELECT * FROM stored_objects WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchStoredObjectsByCid( $intCid, $objDatabase ) {
		return self::fetchStoredObjects( sprintf( 'SELECT * FROM stored_objects WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchStoredObjectsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchStoredObjects( sprintf( 'SELECT * FROM stored_objects WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

}
?>