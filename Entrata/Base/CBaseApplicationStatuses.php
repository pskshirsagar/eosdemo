<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStatuses
 * Do not add any new functions to this class.
 */

class CBaseApplicationStatuses extends CEosPluralBase {

	/**
	 * @return CApplicationStatus[]
	 */
	public static function fetchApplicationStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationStatus::class, $objDatabase );
	}

	/**
	 * @return CApplicationStatus
	 */
	public static function fetchApplicationStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationStatus::class, $objDatabase );
	}

	public static function fetchApplicationStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_statuses', $objDatabase );
	}

	public static function fetchApplicationStatusById( $intId, $objDatabase ) {
		return self::fetchApplicationStatus( sprintf( 'SELECT * FROM application_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>