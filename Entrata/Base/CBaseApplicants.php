<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicants
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicants extends CEosPluralBase {

	/**
	 * @return CApplicant[]
	 */
	public static function fetchApplicants( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplicant::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicant
	 */
	public static function fetchApplicant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicant::class, $objDatabase );
	}

	public static function fetchApplicantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'view_applicants', $objDatabase );
	}

	public static function fetchApplicantByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicant( sprintf( 'SELECT * FROM view_applicants WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByMaritalStatusTypeIdByCid( $intMaritalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE marital_status_type_id = %d AND cid = %d', $intMaritalStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByOriginatingCustomerIdByCid( $intOriginatingCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE originating_customer_id = %d AND cid = %d', $intOriginatingCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByPrimaryPhoneNumberTypeIdByCid( $intPrimaryPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE primary_phone_number_type_id = %d AND cid = %d', $intPrimaryPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsBySecondaryPhoneNumberTypeIdByCid( $intSecondaryPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE secondary_phone_number_type_id = %d AND cid = %d', $intSecondaryPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByMessageOperatorIdByCid( $intMessageOperatorId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE message_operator_id = %d AND cid = %d', $intMessageOperatorId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByOccupationIdByCid( $intOccupationId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE occupation_id = %d AND cid = %d', $intOccupationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByCompanyIdentificationTypeIdByCid( $intCompanyIdentificationTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE company_identification_type_id = %d AND cid = %d', $intCompanyIdentificationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByTaxIdTypeIdByCid( $intTaxIdTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE tax_id_type_id = %d AND cid = %d', $intTaxIdTypeId, $intCid ), $objDatabase );
	}

}
?>