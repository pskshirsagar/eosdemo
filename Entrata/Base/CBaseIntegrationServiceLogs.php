<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationServiceLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationServiceLogs extends CEosPluralBase {

	/**
	 * @return CIntegrationServiceLog[]
	 */
	public static function fetchIntegrationServiceLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationServiceLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationServiceLog
	 */
	public static function fetchIntegrationServiceLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationServiceLog', $objDatabase );
	}

	public static function fetchIntegrationServiceLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_service_logs', $objDatabase );
	}

	public static function fetchIntegrationServiceLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationServiceLog( sprintf( 'SELECT * FROM integration_service_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationServiceLogsByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationServiceLogs( sprintf( 'SELECT * FROM integration_service_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationServiceLogsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchIntegrationServiceLogs( sprintf( 'SELECT * FROM integration_service_logs WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationServiceLogsByIntegrationServiceIdByCid( $intIntegrationServiceId, $intCid, $objDatabase ) {
		return self::fetchIntegrationServiceLogs( sprintf( 'SELECT * FROM integration_service_logs WHERE integration_service_id = %d AND cid = %d', ( int ) $intIntegrationServiceId, ( int ) $intCid ), $objDatabase );
	}

}
?>