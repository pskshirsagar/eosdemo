<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenuePostRent extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.revenue_post_rents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_intUnitSpaceId;
	protected $m_strProcessDatetime;
	protected $m_intIsOverride;
	protected $m_intIsApprove;
	protected $m_intIsRenewal;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intStdOverrideRent;
	protected $m_intStdOptimalRent;
	protected $m_strOverrideReason;
	protected $m_intRequestedBy;
	protected $m_strRequestedOn;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_strConstrainedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intBatchId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsOverride = '0';
		$this->m_intIsApprove = '0';
		$this->m_intIsRenewal = ( 0 );

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessDatetime', trim( $arrValues['process_datetime'] ) ); elseif( isset( $arrValues['process_datetime'] ) ) $this->setProcessDatetime( $arrValues['process_datetime'] );
		if( isset( $arrValues['is_override'] ) && $boolDirectSet ) $this->set( 'm_intIsOverride', trim( $arrValues['is_override'] ) ); elseif( isset( $arrValues['is_override'] ) ) $this->setIsOverride( $arrValues['is_override'] );
		if( isset( $arrValues['is_approve'] ) && $boolDirectSet ) $this->set( 'm_intIsApprove', trim( $arrValues['is_approve'] ) ); elseif( isset( $arrValues['is_approve'] ) ) $this->setIsApprove( $arrValues['is_approve'] );
		if( isset( $arrValues['is_renewal'] ) && $boolDirectSet ) $this->set( 'm_intIsRenewal', trim( $arrValues['is_renewal'] ) ); elseif( isset( $arrValues['is_renewal'] ) ) $this->setIsRenewal( $arrValues['is_renewal'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['std_override_rent'] ) && $boolDirectSet ) $this->set( 'm_intStdOverrideRent', trim( $arrValues['std_override_rent'] ) ); elseif( isset( $arrValues['std_override_rent'] ) ) $this->setStdOverrideRent( $arrValues['std_override_rent'] );
		if( isset( $arrValues['std_optimal_rent'] ) && $boolDirectSet ) $this->set( 'm_intStdOptimalRent', trim( $arrValues['std_optimal_rent'] ) ); elseif( isset( $arrValues['std_optimal_rent'] ) ) $this->setStdOptimalRent( $arrValues['std_optimal_rent'] );
		if( isset( $arrValues['override_reason'] ) && $boolDirectSet ) $this->set( 'm_strOverrideReason', trim( stripcslashes( $arrValues['override_reason'] ) ) ); elseif( isset( $arrValues['override_reason'] ) ) $this->setOverrideReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['override_reason'] ) : $arrValues['override_reason'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['requested_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestedOn', trim( $arrValues['requested_on'] ) ); elseif( isset( $arrValues['requested_on'] ) ) $this->setRequestedOn( $arrValues['requested_on'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['constrained_on'] ) && $boolDirectSet ) $this->set( 'm_strConstrainedOn', trim( $arrValues['constrained_on'] ) ); elseif( isset( $arrValues['constrained_on'] ) ) $this->setConstrainedOn( $arrValues['constrained_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['batch_id'] ) && $boolDirectSet ) $this->set( 'm_intBatchId', trim( $arrValues['batch_id'] ) ); elseif( isset( $arrValues['batch_id'] ) ) $this->setBatchId( $arrValues['batch_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setProcessDatetime( $strProcessDatetime ) {
		$this->set( 'm_strProcessDatetime', CStrings::strTrimDef( $strProcessDatetime, -1, NULL, true ) );
	}

	public function getProcessDatetime() {
		return $this->m_strProcessDatetime;
	}

	public function sqlProcessDatetime() {
		return ( true == isset( $this->m_strProcessDatetime ) ) ? '\'' . $this->m_strProcessDatetime . '\'' : 'NULL';
	}

	public function setIsOverride( $intIsOverride ) {
		$this->set( 'm_intIsOverride', CStrings::strToIntDef( $intIsOverride, NULL, false ) );
	}

	public function getIsOverride() {
		return $this->m_intIsOverride;
	}

	public function sqlIsOverride() {
		return ( true == isset( $this->m_intIsOverride ) ) ? ( string ) $this->m_intIsOverride : '0';
	}

	public function setIsApprove( $intIsApprove ) {
		$this->set( 'm_intIsApprove', CStrings::strToIntDef( $intIsApprove, NULL, false ) );
	}

	public function getIsApprove() {
		return $this->m_intIsApprove;
	}

	public function sqlIsApprove() {
		return ( true == isset( $this->m_intIsApprove ) ) ? ( string ) $this->m_intIsApprove : '0';
	}

	public function setIsRenewal( $intIsRenewal ) {
		$this->set( 'm_intIsRenewal', CStrings::strToIntDef( $intIsRenewal, NULL, false ) );
	}

	public function getIsRenewal() {
		return $this->m_intIsRenewal;
	}

	public function sqlIsRenewal() {
		return ( true == isset( $this->m_intIsRenewal ) ) ? ( string ) $this->m_intIsRenewal : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStdOverrideRent( $intStdOverrideRent ) {
		$this->set( 'm_intStdOverrideRent', CStrings::strToIntDef( $intStdOverrideRent, NULL, false ) );
	}

	public function getStdOverrideRent() {
		return $this->m_intStdOverrideRent;
	}

	public function sqlStdOverrideRent() {
		return ( true == isset( $this->m_intStdOverrideRent ) ) ? ( string ) $this->m_intStdOverrideRent : 'NULL';
	}

	public function setStdOptimalRent( $intStdOptimalRent ) {
		$this->set( 'm_intStdOptimalRent', CStrings::strToIntDef( $intStdOptimalRent, NULL, false ) );
	}

	public function getStdOptimalRent() {
		return $this->m_intStdOptimalRent;
	}

	public function sqlStdOptimalRent() {
		return ( true == isset( $this->m_intStdOptimalRent ) ) ? ( string ) $this->m_intStdOptimalRent : 'NULL';
	}

	public function setOverrideReason( $strOverrideReason ) {
		$this->set( 'm_strOverrideReason', CStrings::strTrimDef( $strOverrideReason, -1, NULL, true ) );
	}

	public function getOverrideReason() {
		return $this->m_strOverrideReason;
	}

	public function sqlOverrideReason() {
		return ( true == isset( $this->m_strOverrideReason ) ) ? '\'' . addslashes( $this->m_strOverrideReason ) . '\'' : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setRequestedOn( $strRequestedOn ) {
		$this->set( 'm_strRequestedOn', CStrings::strTrimDef( $strRequestedOn, -1, NULL, true ) );
	}

	public function getRequestedOn() {
		return $this->m_strRequestedOn;
	}

	public function sqlRequestedOn() {
		return ( true == isset( $this->m_strRequestedOn ) ) ? '\'' . $this->m_strRequestedOn . '\'' : 'NULL';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setConstrainedOn( $strConstrainedOn ) {
		$this->set( 'm_strConstrainedOn', CStrings::strTrimDef( $strConstrainedOn, -1, NULL, true ) );
	}

	public function getConstrainedOn() {
		return $this->m_strConstrainedOn;
	}

	public function sqlConstrainedOn() {
		return ( true == isset( $this->m_strConstrainedOn ) ) ? '\'' . $this->m_strConstrainedOn . '\'' : 'NULL';
	}

	public function setBatchId( $intBatchId ) {
		$this->set( 'm_intBatchId', CStrings::strToIntDef( $intBatchId, NULL, false ) );
	}

	public function getBatchId() {
		return $this->m_intBatchId;
	}

	public function sqlBatchId() {
		return ( true == isset( $this->m_intBatchId ) ) ? ( string ) $this->m_intBatchId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_id, unit_space_id, process_datetime, is_override, is_approve, is_renewal, updated_by, updated_on, created_by, created_on, std_override_rent, std_optimal_rent, override_reason, requested_by, requested_on, accepted_by, accepted_on, rejected_by, rejected_on, constrained_on, details, batch_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlProcessDatetime() . ', ' .
						$this->sqlIsOverride() . ', ' .
						$this->sqlIsApprove() . ', ' .
						$this->sqlIsRenewal() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlStdOverrideRent() . ', ' .
						$this->sqlStdOptimalRent() . ', ' .
						$this->sqlOverrideReason() . ', ' .
						$this->sqlRequestedBy() . ', ' .
						$this->sqlRequestedOn() . ', ' .
						$this->sqlAcceptedBy() . ', ' .
						$this->sqlAcceptedOn() . ', ' .
						$this->sqlRejectedBy() . ', ' .
						$this->sqlRejectedOn() . ', ' .
						$this->sqlConstrainedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlBatchId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_datetime = ' . $this->sqlProcessDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' process_datetime = ' . $this->sqlProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_override = ' . $this->sqlIsOverride(). ',' ; } elseif( true == array_key_exists( 'IsOverride', $this->getChangedColumns() ) ) { $strSql .= ' is_override = ' . $this->sqlIsOverride() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approve = ' . $this->sqlIsApprove(). ',' ; } elseif( true == array_key_exists( 'IsApprove', $this->getChangedColumns() ) ) { $strSql .= ' is_approve = ' . $this->sqlIsApprove() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal(). ',' ; } elseif( true == array_key_exists( 'IsRenewal', $this->getChangedColumns() ) ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_override_rent = ' . $this->sqlStdOverrideRent(). ',' ; } elseif( true == array_key_exists( 'StdOverrideRent', $this->getChangedColumns() ) ) { $strSql .= ' std_override_rent = ' . $this->sqlStdOverrideRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_optimal_rent = ' . $this->sqlStdOptimalRent(). ',' ; } elseif( true == array_key_exists( 'StdOptimalRent', $this->getChangedColumns() ) ) { $strSql .= ' std_optimal_rent = ' . $this->sqlStdOptimalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_reason = ' . $this->sqlOverrideReason(). ',' ; } elseif( true == array_key_exists( 'OverrideReason', $this->getChangedColumns() ) ) { $strSql .= ' override_reason = ' . $this->sqlOverrideReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy(). ',' ; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn(). ',' ; } elseif( true == array_key_exists( 'RequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy(). ',' ; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy(). ',' ; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' constrained_on = ' . $this->sqlConstrainedOn(). ',' ; } elseif( true == array_key_exists( 'ConstrainedOn', $this->getChangedColumns() ) ) { $strSql .= ' constrained_on = ' . $this->sqlConstrainedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_id = ' . $this->sqlBatchId(). ',' ; } elseif( true == array_key_exists( 'BatchId', $this->getChangedColumns() ) ) { $strSql .= ' batch_id = ' . $this->sqlBatchId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'process_datetime' => $this->getProcessDatetime(),
			'is_override' => $this->getIsOverride(),
			'is_approve' => $this->getIsApprove(),
			'is_renewal' => $this->getIsRenewal(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'std_override_rent' => $this->getStdOverrideRent(),
			'std_optimal_rent' => $this->getStdOptimalRent(),
			'override_reason' => $this->getOverrideReason(),
			'requested_by' => $this->getRequestedBy(),
			'requested_on' => $this->getRequestedOn(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'constrained_on' => $this->getConstrainedOn(),
			'details' => $this->getDetails(),
			'batch_id' => $this->getBatchId()
		);
	}

}
?>