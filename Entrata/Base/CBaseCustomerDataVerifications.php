<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataVerifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDataVerifications extends CEosPluralBase {

	/**
	 * @return CCustomerDataVerification[]
	 */
	public static function fetchCustomerDataVerifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerDataVerification', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerDataVerification
	 */
	public static function fetchCustomerDataVerification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerDataVerification', $objDatabase );
	}

	public static function fetchCustomerDataVerificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_data_verifications', $objDatabase );
	}

	public static function fetchCustomerDataVerificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerification( sprintf( 'SELECT * FROM customer_data_verifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByVerificationTypeIdByCid( $intVerificationTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE verification_type_id = %d AND cid = %d', ( int ) $intVerificationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByCustomerDataTypeIdByCid( $intCustomerDataTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE customer_data_type_id = %d AND cid = %d', ( int ) $intCustomerDataTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByCustomerDataReferenceIdByCid( $intCustomerDataReferenceId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE customer_data_reference_id = %d AND cid = %d', ( int ) $intCustomerDataReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDataVerificationsByCustomerDataReferenceLogIdByCid( $intCustomerDataReferenceLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerDataVerifications( sprintf( 'SELECT * FROM customer_data_verifications WHERE customer_data_reference_log_id = %d AND cid = %d', ( int ) $intCustomerDataReferenceLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>