<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBuildingAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBuildingAddresses extends CEosPluralBase {

	/**
	 * @return CPropertyBuildingAddress[]
	 */
	public static function fetchPropertyBuildingAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyBuildingAddress', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyBuildingAddress
	 */
	public static function fetchPropertyBuildingAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyBuildingAddress', $objDatabase );
	}

	public static function fetchPropertyBuildingAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_building_addresses', $objDatabase );
	}

	public static function fetchPropertyBuildingAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddress( sprintf( 'SELECT * FROM property_building_addresses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBuildingAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddresses( sprintf( 'SELECT * FROM property_building_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBuildingAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddresses( sprintf( 'SELECT * FROM property_building_addresses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBuildingAddressesByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddresses( sprintf( 'SELECT * FROM property_building_addresses WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBuildingAddressesByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddresses( sprintf( 'SELECT * FROM property_building_addresses WHERE address_type_id = %d AND cid = %d', ( int ) $intAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBuildingAddressesByTimeZoneIdByCid( $intTimeZoneId, $intCid, $objDatabase ) {
		return self::fetchPropertyBuildingAddresses( sprintf( 'SELECT * FROM property_building_addresses WHERE time_zone_id = %d AND cid = %d', ( int ) $intTimeZoneId, ( int ) $intCid ), $objDatabase );
	}

}
?>