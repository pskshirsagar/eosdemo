<?php

class CBaseDepositRefundTypes extends CEosPluralBase {

	/**
	 * @return CDepositRefundType[]
	 */
	public static function fetchDepositRefundTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDepositRefundType', $objDatabase );
	}

	/**
	 * @return CDepositRefundType
	 */
	public static function fetchDepositRefundType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDepositRefundType', $objDatabase );
	}

	public static function fetchDepositRefundTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deposit_refund_types', $objDatabase );
	}

	public static function fetchDepositRefundTypeById( $intId, $objDatabase ) {
		return self::fetchDepositRefundType( sprintf( 'SELECT * FROM deposit_refund_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>