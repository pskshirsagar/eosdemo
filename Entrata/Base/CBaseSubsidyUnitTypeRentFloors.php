<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyUnitTypeRentFloors
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyUnitTypeRentFloors extends CEosPluralBase {

	/**
	 * @return CSubsidyUnitTypeRentFloor[]
	 */
	public static function fetchSubsidyUnitTypeRentFloors( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyUnitTypeRentFloor', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyUnitTypeRentFloor
	 */
	public static function fetchSubsidyUnitTypeRentFloor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyUnitTypeRentFloor', $objDatabase );
	}

	public static function fetchSubsidyUnitTypeRentFloorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_unit_type_rent_floors', $objDatabase );
	}

	public static function fetchSubsidyUnitTypeRentFloorByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyUnitTypeRentFloor( sprintf( 'SELECT * FROM subsidy_unit_type_rent_floors WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyUnitTypeRentFloorsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyUnitTypeRentFloors( sprintf( 'SELECT * FROM subsidy_unit_type_rent_floors WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyUnitTypeRentFloorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyUnitTypeRentFloors( sprintf( 'SELECT * FROM subsidy_unit_type_rent_floors WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyUnitTypeRentFloorsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyUnitTypeRentFloors( sprintf( 'SELECT * FROM subsidy_unit_type_rent_floors WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>