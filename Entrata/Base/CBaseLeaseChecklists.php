<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseChecklists
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseChecklists extends CEosPluralBase {

	public static function fetchLeaseChecklists( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseChecklist', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchLeaseChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseChecklist', $objDatabase );
	}

	public static function fetchLeaseChecklistCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_checklists', $objDatabase );
	}

	public static function fetchLeaseChecklistByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklist( sprintf( 'SELECT * FROM lease_checklists WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseChecklists( sprintf( 'SELECT * FROM lease_checklists WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklists( sprintf( 'SELECT * FROM lease_checklists WHERE lease_id = %d AND cid = %d', (int) $intLeaseId, (int) $intCid ), $objDatabase );
	}

   public static function fetchLeaseChecklistsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklists( sprintf( 'SELECT * FROM lease_checklists WHERE lease_interval_id = %d AND cid = %d', (int) $intLeaseIntervalId, (int) $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistsByChecklistIdByCid( $intChecklistId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklists( sprintf( 'SELECT * FROM lease_checklists WHERE checklist_id = %d AND cid = %d', (int) $intChecklistId, (int) $intCid ), $objDatabase );
	}

}
?>