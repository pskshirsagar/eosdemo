<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComplianceRulesetCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_ruleset_categories';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intComplianceRulesetId;
	protected $m_intVendorCategoryTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['compliance_ruleset_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceRulesetId', trim( $arrValues['compliance_ruleset_id'] ) ); elseif( isset( $arrValues['compliance_ruleset_id'] ) ) $this->setComplianceRulesetId( $arrValues['compliance_ruleset_id'] );
		if( isset( $arrValues['vendor_category_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorCategoryTypeId', trim( $arrValues['vendor_category_type_id'] ) ); elseif( isset( $arrValues['vendor_category_type_id'] ) ) $this->setVendorCategoryTypeId( $arrValues['vendor_category_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setComplianceRulesetId( $intComplianceRulesetId ) {
		$this->set( 'm_intComplianceRulesetId', CStrings::strToIntDef( $intComplianceRulesetId, NULL, false ) );
	}

	public function getComplianceRulesetId() {
		return $this->m_intComplianceRulesetId;
	}

	public function sqlComplianceRulesetId() {
		return ( true == isset( $this->m_intComplianceRulesetId ) ) ? ( string ) $this->m_intComplianceRulesetId : 'NULL';
	}

	public function setVendorCategoryTypeId( $intVendorCategoryTypeId ) {
		$this->set( 'm_intVendorCategoryTypeId', CStrings::strToIntDef( $intVendorCategoryTypeId, NULL, false ) );
	}

	public function getVendorCategoryTypeId() {
		return $this->m_intVendorCategoryTypeId;
	}

	public function sqlVendorCategoryTypeId() {
		return ( true == isset( $this->m_intVendorCategoryTypeId ) ) ? ( string ) $this->m_intVendorCategoryTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_ruleset_id, vendor_category_type_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlComplianceRulesetId() . ', ' .
						$this->sqlVendorCategoryTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId(). ',' ; } elseif( true == array_key_exists( 'ComplianceRulesetId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_category_type_id = ' . $this->sqlVendorCategoryTypeId() ; } elseif( true == array_key_exists( 'VendorCategoryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_category_type_id = ' . $this->sqlVendorCategoryTypeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'compliance_ruleset_id' => $this->getComplianceRulesetId(),
			'vendor_category_type_id' => $this->getVendorCategoryTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>