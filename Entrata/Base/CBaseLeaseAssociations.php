<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseAssociations extends CEosPluralBase {

	/**
	 * @return CLeaseAssociation[]
	 */
	public static function fetchLeaseAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseAssociation
	 */
	public static function fetchLeaseAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseAssociation::class, $objDatabase );
	}

	public static function fetchLeaseAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_associations', $objDatabase );
	}

	public static function fetchLeaseAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociation( sprintf( 'SELECT * FROM lease_associations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE lease_interval_id = %d AND cid = %d', $intLeaseIntervalId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE lease_status_type_id = %d AND cid = %d', $intLeaseStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByArCascadeIdByCid( $intArCascadeId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE ar_cascade_id = %d AND cid = %d', $intArCascadeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByArCascadeReferenceIdByCid( $intArCascadeReferenceId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE ar_cascade_reference_id = %d AND cid = %d', $intArCascadeReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE ar_origin_id = %d AND cid = %d', $intArOriginId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE ar_origin_reference_id = %d AND cid = %d', $intArOriginReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByArOriginObjectIdByCid( $intArOriginObjectId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE ar_origin_object_id = %d AND cid = %d', $intArOriginObjectId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE quote_id = %d AND cid = %d', $intQuoteId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByQuoteLeaseTermIdByCid( $intQuoteLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE quote_lease_term_id = %d AND cid = %d', $intQuoteLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseAssociationsByMappingIdByCid( $intMappingId, $intCid, $objDatabase ) {
		return self::fetchLeaseAssociations( sprintf( 'SELECT * FROM lease_associations WHERE mapping_id = %d AND cid = %d', $intMappingId, $intCid ), $objDatabase );
	}

}
?>