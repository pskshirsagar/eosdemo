<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApAchTypes
 * Do not add any new functions to this class.
 */

class CBaseApAchTypes extends CEosPluralBase {

	/**
	 * @return CApAchType[]
	 */
	public static function fetchApAchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApAchType::class, $objDatabase );
	}

	/**
	 * @return CApAchType
	 */
	public static function fetchApAchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApAchType::class, $objDatabase );
	}

	public static function fetchApAchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_ach_types', $objDatabase );
	}

	public static function fetchApAchTypeById( $intId, $objDatabase ) {
		return self::fetchApAchType( sprintf( 'SELECT * FROM ap_ach_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>