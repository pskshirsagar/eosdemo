<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCompetitorStrategies
 * Do not add any new functions to this class.
 */

class CBaseDefaultCompetitorStrategies extends CEosPluralBase {

	/**
	 * @return CDefaultCompetitorStrategy[]
	 */
	public static function fetchDefaultCompetitorStrategies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultCompetitorStrategy', $objDatabase );
	}

	/**
	 * @return CDefaultCompetitorStrategy
	 */
	public static function fetchDefaultCompetitorStrategy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultCompetitorStrategy', $objDatabase );
	}

	public static function fetchDefaultCompetitorStrategyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_competitor_strategies', $objDatabase );
	}

	public static function fetchDefaultCompetitorStrategyById( $intId, $objDatabase ) {
		return self::fetchDefaultCompetitorStrategy( sprintf( 'SELECT * FROM default_competitor_strategies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultCompetitorStrategiesByRevenueCalcTypeId( $intRevenueCalcTypeId, $objDatabase ) {
		return self::fetchDefaultCompetitorStrategies( sprintf( 'SELECT * FROM default_competitor_strategies WHERE revenue_calc_type_id = %d', ( int ) $intRevenueCalcTypeId ), $objDatabase );
	}

}
?>