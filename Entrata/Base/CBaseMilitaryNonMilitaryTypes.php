<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryNonMilitaryTypes
 * Do not add any new functions to this class.
 */

class CBaseMilitaryNonMilitaryTypes extends CEosPluralBase {

	/**
	 * @return CMilitaryNonMilitaryType[]
	 */
	public static function fetchMilitaryNonMilitaryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryNonMilitaryType::class, $objDatabase );
	}

	/**
	 * @return CMilitaryNonMilitaryType
	 */
	public static function fetchMilitaryNonMilitaryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryNonMilitaryType::class, $objDatabase );
	}

	public static function fetchMilitaryNonMilitaryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_non_military_types', $objDatabase );
	}

	public static function fetchMilitaryNonMilitaryTypeById( $intId, $objDatabase ) {
		return self::fetchMilitaryNonMilitaryType( sprintf( 'SELECT * FROM military_non_military_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>