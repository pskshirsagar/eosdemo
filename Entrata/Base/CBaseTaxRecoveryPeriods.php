<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxRecoveryPeriods
 * Do not add any new functions to this class.
 */

class CBaseTaxRecoveryPeriods extends CEosPluralBase {

	/**
	 * @return CTaxRecoveryPeriod[]
	 */
	public static function fetchTaxRecoveryPeriods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaxRecoveryPeriod::class, $objDatabase );
	}

	/**
	 * @return CTaxRecoveryPeriod
	 */
	public static function fetchTaxRecoveryPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxRecoveryPeriod::class, $objDatabase );
	}

	public static function fetchTaxRecoveryPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tax_recovery_periods', $objDatabase );
	}

	public static function fetchTaxRecoveryPeriodById( $intId, $objDatabase ) {
		return self::fetchTaxRecoveryPeriod( sprintf( 'SELECT * FROM tax_recovery_periods WHERE id = %d', $intId ), $objDatabase );
	}

}
?>