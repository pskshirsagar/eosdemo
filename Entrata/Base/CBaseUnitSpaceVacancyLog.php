<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceVacancyLog extends CEosSingularBase {

	const TABLE_NAME = 'public.unit_space_vacancy_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitSpaceId;
	protected $m_intPeriodId;
	protected $m_strPostMonth;
	protected $m_intEstimatedDaysVacant;
	protected $m_intActualDaysVacant;
	protected $m_intFinancialDays;
	protected $m_fltGprRent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intEstimatedDaysVacant = '0';
		$this->m_intActualDaysVacant = '0';
		$this->m_intFinancialDays = '0';
		$this->m_fltGprRent = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['estimated_days_vacant'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedDaysVacant', trim( $arrValues['estimated_days_vacant'] ) ); elseif( isset( $arrValues['estimated_days_vacant'] ) ) $this->setEstimatedDaysVacant( $arrValues['estimated_days_vacant'] );
		if( isset( $arrValues['actual_days_vacant'] ) && $boolDirectSet ) $this->set( 'm_intActualDaysVacant', trim( $arrValues['actual_days_vacant'] ) ); elseif( isset( $arrValues['actual_days_vacant'] ) ) $this->setActualDaysVacant( $arrValues['actual_days_vacant'] );
		if( isset( $arrValues['financial_days'] ) && $boolDirectSet ) $this->set( 'm_intFinancialDays', trim( $arrValues['financial_days'] ) ); elseif( isset( $arrValues['financial_days'] ) ) $this->setFinancialDays( $arrValues['financial_days'] );
		if( isset( $arrValues['gpr_rent'] ) && $boolDirectSet ) $this->set( 'm_fltGprRent', trim( $arrValues['gpr_rent'] ) ); elseif( isset( $arrValues['gpr_rent'] ) ) $this->setGprRent( $arrValues['gpr_rent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setEstimatedDaysVacant( $intEstimatedDaysVacant ) {
		$this->set( 'm_intEstimatedDaysVacant', CStrings::strToIntDef( $intEstimatedDaysVacant, NULL, false ) );
	}

	public function getEstimatedDaysVacant() {
		return $this->m_intEstimatedDaysVacant;
	}

	public function sqlEstimatedDaysVacant() {
		return ( true == isset( $this->m_intEstimatedDaysVacant ) ) ? ( string ) $this->m_intEstimatedDaysVacant : '0';
	}

	public function setActualDaysVacant( $intActualDaysVacant ) {
		$this->set( 'm_intActualDaysVacant', CStrings::strToIntDef( $intActualDaysVacant, NULL, false ) );
	}

	public function getActualDaysVacant() {
		return $this->m_intActualDaysVacant;
	}

	public function sqlActualDaysVacant() {
		return ( true == isset( $this->m_intActualDaysVacant ) ) ? ( string ) $this->m_intActualDaysVacant : '0';
	}

	public function setFinancialDays( $intFinancialDays ) {
		$this->set( 'm_intFinancialDays', CStrings::strToIntDef( $intFinancialDays, NULL, false ) );
	}

	public function getFinancialDays() {
		return $this->m_intFinancialDays;
	}

	public function sqlFinancialDays() {
		return ( true == isset( $this->m_intFinancialDays ) ) ? ( string ) $this->m_intFinancialDays : '0';
	}

	public function setGprRent( $fltGprRent ) {
		$this->set( 'm_fltGprRent', CStrings::strToFloatDef( $fltGprRent, NULL, false, 2 ) );
	}

	public function getGprRent() {
		return $this->m_fltGprRent;
	}

	public function sqlGprRent() {
		return ( true == isset( $this->m_fltGprRent ) ) ? ( string ) $this->m_fltGprRent : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_space_id, period_id, post_month, estimated_days_vacant, actual_days_vacant, financial_days, gpr_rent, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlEstimatedDaysVacant() . ', ' .
 						$this->sqlActualDaysVacant() . ', ' .
 						$this->sqlFinancialDays() . ', ' .
 						$this->sqlGprRent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_days_vacant = ' . $this->sqlEstimatedDaysVacant() . ','; } elseif( true == array_key_exists( 'EstimatedDaysVacant', $this->getChangedColumns() ) ) { $strSql .= ' estimated_days_vacant = ' . $this->sqlEstimatedDaysVacant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_days_vacant = ' . $this->sqlActualDaysVacant() . ','; } elseif( true == array_key_exists( 'ActualDaysVacant', $this->getChangedColumns() ) ) { $strSql .= ' actual_days_vacant = ' . $this->sqlActualDaysVacant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_days = ' . $this->sqlFinancialDays() . ','; } elseif( true == array_key_exists( 'FinancialDays', $this->getChangedColumns() ) ) { $strSql .= ' financial_days = ' . $this->sqlFinancialDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gpr_rent = ' . $this->sqlGprRent() . ','; } elseif( true == array_key_exists( 'GprRent', $this->getChangedColumns() ) ) { $strSql .= ' gpr_rent = ' . $this->sqlGprRent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'period_id' => $this->getPeriodId(),
			'post_month' => $this->getPostMonth(),
			'estimated_days_vacant' => $this->getEstimatedDaysVacant(),
			'actual_days_vacant' => $this->getActualDaysVacant(),
			'financial_days' => $this->getFinancialDays(),
			'gpr_rent' => $this->getGprRent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>