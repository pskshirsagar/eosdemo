<?php

class CBaseMenuIcon extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.menu_icons';

	protected $m_intId;
	protected $m_intAdministrativeMenuTypeId;
	protected $m_strName;
	protected $m_strUri;
	protected $m_strImageName;
	protected $m_strSelectedIdentifier;
	protected $m_strTarget;
	protected $m_strAjaxDivName;
	protected $m_strRelatedModule;
	protected $m_strRelatedAction;
	protected $m_intShowInExtendedMenu;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intShowInExtendedMenu = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['administrative_menu_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAdministrativeMenuTypeId', trim( $arrValues['administrative_menu_type_id'] ) ); elseif( isset( $arrValues['administrative_menu_type_id'] ) ) $this->setAdministrativeMenuTypeId( $arrValues['administrative_menu_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['uri'] ) && $boolDirectSet ) $this->set( 'm_strUri', trim( stripcslashes( $arrValues['uri'] ) ) ); elseif( isset( $arrValues['uri'] ) ) $this->setUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['uri'] ) : $arrValues['uri'] );
		if( isset( $arrValues['image_name'] ) && $boolDirectSet ) $this->set( 'm_strImageName', trim( stripcslashes( $arrValues['image_name'] ) ) ); elseif( isset( $arrValues['image_name'] ) ) $this->setImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_name'] ) : $arrValues['image_name'] );
		if( isset( $arrValues['selected_identifier'] ) && $boolDirectSet ) $this->set( 'm_strSelectedIdentifier', trim( stripcslashes( $arrValues['selected_identifier'] ) ) ); elseif( isset( $arrValues['selected_identifier'] ) ) $this->setSelectedIdentifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['selected_identifier'] ) : $arrValues['selected_identifier'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( stripcslashes( $arrValues['target'] ) ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['target'] ) : $arrValues['target'] );
		if( isset( $arrValues['ajax_div_name'] ) && $boolDirectSet ) $this->set( 'm_strAjaxDivName', trim( stripcslashes( $arrValues['ajax_div_name'] ) ) ); elseif( isset( $arrValues['ajax_div_name'] ) ) $this->setAjaxDivName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ajax_div_name'] ) : $arrValues['ajax_div_name'] );
		if( isset( $arrValues['related_module'] ) && $boolDirectSet ) $this->set( 'm_strRelatedModule', trim( stripcslashes( $arrValues['related_module'] ) ) ); elseif( isset( $arrValues['related_module'] ) ) $this->setRelatedModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['related_module'] ) : $arrValues['related_module'] );
		if( isset( $arrValues['related_action'] ) && $boolDirectSet ) $this->set( 'm_strRelatedAction', trim( stripcslashes( $arrValues['related_action'] ) ) ); elseif( isset( $arrValues['related_action'] ) ) $this->setRelatedAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['related_action'] ) : $arrValues['related_action'] );
		if( isset( $arrValues['show_in_extended_menu'] ) && $boolDirectSet ) $this->set( 'm_intShowInExtendedMenu', trim( $arrValues['show_in_extended_menu'] ) ); elseif( isset( $arrValues['show_in_extended_menu'] ) ) $this->setShowInExtendedMenu( $arrValues['show_in_extended_menu'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAdministrativeMenuTypeId( $intAdministrativeMenuTypeId ) {
		$this->set( 'm_intAdministrativeMenuTypeId', CStrings::strToIntDef( $intAdministrativeMenuTypeId, NULL, false ) );
	}

	public function getAdministrativeMenuTypeId() {
		return $this->m_intAdministrativeMenuTypeId;
	}

	public function sqlAdministrativeMenuTypeId() {
		return ( true == isset( $this->m_intAdministrativeMenuTypeId ) ) ? ( string ) $this->m_intAdministrativeMenuTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setUri( $strUri ) {
		$this->set( 'm_strUri', CStrings::strTrimDef( $strUri, 4096, NULL, true ) );
	}

	public function getUri() {
		return $this->m_strUri;
	}

	public function sqlUri() {
		return ( true == isset( $this->m_strUri ) ) ? '\'' . addslashes( $this->m_strUri ) . '\'' : 'NULL';
	}

	public function setImageName( $strImageName ) {
		$this->set( 'm_strImageName', CStrings::strTrimDef( $strImageName, 240, NULL, true ) );
	}

	public function getImageName() {
		return $this->m_strImageName;
	}

	public function sqlImageName() {
		return ( true == isset( $this->m_strImageName ) ) ? '\'' . addslashes( $this->m_strImageName ) . '\'' : 'NULL';
	}

	public function setSelectedIdentifier( $strSelectedIdentifier ) {
		$this->set( 'm_strSelectedIdentifier', CStrings::strTrimDef( $strSelectedIdentifier, 240, NULL, true ) );
	}

	public function getSelectedIdentifier() {
		return $this->m_strSelectedIdentifier;
	}

	public function sqlSelectedIdentifier() {
		return ( true == isset( $this->m_strSelectedIdentifier ) ) ? '\'' . addslashes( $this->m_strSelectedIdentifier ) . '\'' : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, 240, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? '\'' . addslashes( $this->m_strTarget ) . '\'' : 'NULL';
	}

	public function setAjaxDivName( $strAjaxDivName ) {
		$this->set( 'm_strAjaxDivName', CStrings::strTrimDef( $strAjaxDivName, 240, NULL, true ) );
	}

	public function getAjaxDivName() {
		return $this->m_strAjaxDivName;
	}

	public function sqlAjaxDivName() {
		return ( true == isset( $this->m_strAjaxDivName ) ) ? '\'' . addslashes( $this->m_strAjaxDivName ) . '\'' : 'NULL';
	}

	public function setRelatedModule( $strRelatedModule ) {
		$this->set( 'm_strRelatedModule', CStrings::strTrimDef( $strRelatedModule, 240, NULL, true ) );
	}

	public function getRelatedModule() {
		return $this->m_strRelatedModule;
	}

	public function sqlRelatedModule() {
		return ( true == isset( $this->m_strRelatedModule ) ) ? '\'' . addslashes( $this->m_strRelatedModule ) . '\'' : 'NULL';
	}

	public function setRelatedAction( $strRelatedAction ) {
		$this->set( 'm_strRelatedAction', CStrings::strTrimDef( $strRelatedAction, 240, NULL, true ) );
	}

	public function getRelatedAction() {
		return $this->m_strRelatedAction;
	}

	public function sqlRelatedAction() {
		return ( true == isset( $this->m_strRelatedAction ) ) ? '\'' . addslashes( $this->m_strRelatedAction ) . '\'' : 'NULL';
	}

	public function setShowInExtendedMenu( $intShowInExtendedMenu ) {
		$this->set( 'm_intShowInExtendedMenu', CStrings::strToIntDef( $intShowInExtendedMenu, NULL, false ) );
	}

	public function getShowInExtendedMenu() {
		return $this->m_intShowInExtendedMenu;
	}

	public function sqlShowInExtendedMenu() {
		return ( true == isset( $this->m_intShowInExtendedMenu ) ) ? ( string ) $this->m_intShowInExtendedMenu : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'administrative_menu_type_id' => $this->getAdministrativeMenuTypeId(),
			'name' => $this->getName(),
			'uri' => $this->getUri(),
			'image_name' => $this->getImageName(),
			'selected_identifier' => $this->getSelectedIdentifier(),
			'target' => $this->getTarget(),
			'ajax_div_name' => $this->getAjaxDivName(),
			'related_module' => $this->getRelatedModule(),
			'related_action' => $this->getRelatedAction(),
			'show_in_extended_menu' => $this->getShowInExtendedMenu(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>