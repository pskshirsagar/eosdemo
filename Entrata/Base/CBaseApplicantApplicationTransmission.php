<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.applicant_application_transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intApplicantApplicationId;
	protected $m_intTransmissionId;
	protected $m_intTransmissionVendorId;
	protected $m_intCompanyTransmissionVendorId;
	protected $m_intCreditTransmissionResponseTypeId;
	protected $m_intCriminalTransmissionResponseTypeId;
	protected $m_intIdentifyTransmissionResponseTypeId;
	protected $m_intIndividualTransmissionResponseTypeId;
	protected $m_intTransmissionResponseTypeId;
	protected $m_intScreeningApplicantId;
	protected $m_strTransmissionRemotePrimaryKey;
	protected $m_strRemotePrimaryKey;
	protected $m_strTransmissionDatetime;
	protected $m_strReportName;
	protected $m_strResponse;
	protected $m_intHasCriminalFlag;
	protected $m_intHasCreditAlert;
	protected $m_intHasDataError;
	protected $m_intIsFailed;
	protected $m_strCriminalResponseOn;
	protected $m_strAdversePrintedOn;
	protected $m_strAdverseEmailedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strResponseText;

	public function __construct() {
		parent::__construct();

		$this->m_intHasCriminalFlag = '0';
		$this->m_intHasCreditAlert = '0';
		$this->m_intHasDataError = '0';
		$this->m_intIsFailed = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionId', trim( $arrValues['transmission_id'] ) ); elseif( isset( $arrValues['transmission_id'] ) ) $this->setTransmissionId( $arrValues['transmission_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransmissionVendorId', trim( $arrValues['company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['company_transmission_vendor_id'] ) ) $this->setCompanyTransmissionVendorId( $arrValues['company_transmission_vendor_id'] );
		if( isset( $arrValues['credit_transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditTransmissionResponseTypeId', trim( $arrValues['credit_transmission_response_type_id'] ) ); elseif( isset( $arrValues['credit_transmission_response_type_id'] ) ) $this->setCreditTransmissionResponseTypeId( $arrValues['credit_transmission_response_type_id'] );
		if( isset( $arrValues['criminal_transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCriminalTransmissionResponseTypeId', trim( $arrValues['criminal_transmission_response_type_id'] ) ); elseif( isset( $arrValues['criminal_transmission_response_type_id'] ) ) $this->setCriminalTransmissionResponseTypeId( $arrValues['criminal_transmission_response_type_id'] );
		if( isset( $arrValues['identify_transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIdentifyTransmissionResponseTypeId', trim( $arrValues['identify_transmission_response_type_id'] ) ); elseif( isset( $arrValues['identify_transmission_response_type_id'] ) ) $this->setIdentifyTransmissionResponseTypeId( $arrValues['identify_transmission_response_type_id'] );
		if( isset( $arrValues['individual_transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIndividualTransmissionResponseTypeId', trim( $arrValues['individual_transmission_response_type_id'] ) ); elseif( isset( $arrValues['individual_transmission_response_type_id'] ) ) $this->setIndividualTransmissionResponseTypeId( $arrValues['individual_transmission_response_type_id'] );
		if( isset( $arrValues['transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionResponseTypeId', trim( $arrValues['transmission_response_type_id'] ) ); elseif( isset( $arrValues['transmission_response_type_id'] ) ) $this->setTransmissionResponseTypeId( $arrValues['transmission_response_type_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['transmission_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionRemotePrimaryKey', trim( stripcslashes( $arrValues['transmission_remote_primary_key'] ) ) ); elseif( isset( $arrValues['transmission_remote_primary_key'] ) ) $this->setTransmissionRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transmission_remote_primary_key'] ) : $arrValues['transmission_remote_primary_key'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transmission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionDatetime', trim( $arrValues['transmission_datetime'] ) ); elseif( isset( $arrValues['transmission_datetime'] ) ) $this->setTransmissionDatetime( $arrValues['transmission_datetime'] );
		if( isset( $arrValues['report_name'] ) && $boolDirectSet ) $this->set( 'm_strReportName', trim( stripcslashes( $arrValues['report_name'] ) ) ); elseif( isset( $arrValues['report_name'] ) ) $this->setReportName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['report_name'] ) : $arrValues['report_name'] );
		if( isset( $arrValues['response'] ) && $boolDirectSet ) $this->set( 'm_strResponse', trim( stripcslashes( $arrValues['response'] ) ) ); elseif( isset( $arrValues['response'] ) ) $this->setResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response'] ) : $arrValues['response'] );
		if( isset( $arrValues['has_criminal_flag'] ) && $boolDirectSet ) $this->set( 'm_intHasCriminalFlag', trim( $arrValues['has_criminal_flag'] ) ); elseif( isset( $arrValues['has_criminal_flag'] ) ) $this->setHasCriminalFlag( $arrValues['has_criminal_flag'] );
		if( isset( $arrValues['has_credit_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasCreditAlert', trim( $arrValues['has_credit_alert'] ) ); elseif( isset( $arrValues['has_credit_alert'] ) ) $this->setHasCreditAlert( $arrValues['has_credit_alert'] );
		if( isset( $arrValues['has_data_error'] ) && $boolDirectSet ) $this->set( 'm_intHasDataError', trim( $arrValues['has_data_error'] ) ); elseif( isset( $arrValues['has_data_error'] ) ) $this->setHasDataError( $arrValues['has_data_error'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_intIsFailed', trim( $arrValues['is_failed'] ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( $arrValues['is_failed'] );
		if( isset( $arrValues['criminal_response_on'] ) && $boolDirectSet ) $this->set( 'm_strCriminalResponseOn', trim( $arrValues['criminal_response_on'] ) ); elseif( isset( $arrValues['criminal_response_on'] ) ) $this->setCriminalResponseOn( $arrValues['criminal_response_on'] );
		if( isset( $arrValues['adverse_printed_on'] ) && $boolDirectSet ) $this->set( 'm_strAdversePrintedOn', trim( $arrValues['adverse_printed_on'] ) ); elseif( isset( $arrValues['adverse_printed_on'] ) ) $this->setAdversePrintedOn( $arrValues['adverse_printed_on'] );
		if( isset( $arrValues['adverse_emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strAdverseEmailedOn', trim( $arrValues['adverse_emailed_on'] ) ); elseif( isset( $arrValues['adverse_emailed_on'] ) ) $this->setAdverseEmailedOn( $arrValues['adverse_emailed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['response_text'] ) && $boolDirectSet ) $this->set( 'm_strResponseText', trim( stripcslashes( $arrValues['response_text'] ) ) ); elseif( isset( $arrValues['response_text'] ) ) $this->setResponseText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_text'] ) : $arrValues['response_text'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setTransmissionId( $intTransmissionId ) {
		$this->set( 'm_intTransmissionId', CStrings::strToIntDef( $intTransmissionId, NULL, false ) );
	}

	public function getTransmissionId() {
		return $this->m_intTransmissionId;
	}

	public function sqlTransmissionId() {
		return ( true == isset( $this->m_intTransmissionId ) ) ? ( string ) $this->m_intTransmissionId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setCompanyTransmissionVendorId( $intCompanyTransmissionVendorId ) {
		$this->set( 'm_intCompanyTransmissionVendorId', CStrings::strToIntDef( $intCompanyTransmissionVendorId, NULL, false ) );
	}

	public function getCompanyTransmissionVendorId() {
		return $this->m_intCompanyTransmissionVendorId;
	}

	public function sqlCompanyTransmissionVendorId() {
		return ( true == isset( $this->m_intCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intCompanyTransmissionVendorId : 'NULL';
	}

	public function setCreditTransmissionResponseTypeId( $intCreditTransmissionResponseTypeId ) {
		$this->set( 'm_intCreditTransmissionResponseTypeId', CStrings::strToIntDef( $intCreditTransmissionResponseTypeId, NULL, false ) );
	}

	public function getCreditTransmissionResponseTypeId() {
		return $this->m_intCreditTransmissionResponseTypeId;
	}

	public function sqlCreditTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intCreditTransmissionResponseTypeId ) ) ? ( string ) $this->m_intCreditTransmissionResponseTypeId : 'NULL';
	}

	public function setCriminalTransmissionResponseTypeId( $intCriminalTransmissionResponseTypeId ) {
		$this->set( 'm_intCriminalTransmissionResponseTypeId', CStrings::strToIntDef( $intCriminalTransmissionResponseTypeId, NULL, false ) );
	}

	public function getCriminalTransmissionResponseTypeId() {
		return $this->m_intCriminalTransmissionResponseTypeId;
	}

	public function sqlCriminalTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intCriminalTransmissionResponseTypeId ) ) ? ( string ) $this->m_intCriminalTransmissionResponseTypeId : 'NULL';
	}

	public function setIdentifyTransmissionResponseTypeId( $intIdentifyTransmissionResponseTypeId ) {
		$this->set( 'm_intIdentifyTransmissionResponseTypeId', CStrings::strToIntDef( $intIdentifyTransmissionResponseTypeId, NULL, false ) );
	}

	public function getIdentifyTransmissionResponseTypeId() {
		return $this->m_intIdentifyTransmissionResponseTypeId;
	}

	public function sqlIdentifyTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intIdentifyTransmissionResponseTypeId ) ) ? ( string ) $this->m_intIdentifyTransmissionResponseTypeId : 'NULL';
	}

	public function setIndividualTransmissionResponseTypeId( $intIndividualTransmissionResponseTypeId ) {
		$this->set( 'm_intIndividualTransmissionResponseTypeId', CStrings::strToIntDef( $intIndividualTransmissionResponseTypeId, NULL, false ) );
	}

	public function getIndividualTransmissionResponseTypeId() {
		return $this->m_intIndividualTransmissionResponseTypeId;
	}

	public function sqlIndividualTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intIndividualTransmissionResponseTypeId ) ) ? ( string ) $this->m_intIndividualTransmissionResponseTypeId : 'NULL';
	}

	public function setTransmissionResponseTypeId( $intTransmissionResponseTypeId ) {
		$this->set( 'm_intTransmissionResponseTypeId', CStrings::strToIntDef( $intTransmissionResponseTypeId, NULL, false ) );
	}

	public function getTransmissionResponseTypeId() {
		return $this->m_intTransmissionResponseTypeId;
	}

	public function sqlTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intTransmissionResponseTypeId ) ) ? ( string ) $this->m_intTransmissionResponseTypeId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setTransmissionRemotePrimaryKey( $strTransmissionRemotePrimaryKey ) {
		$this->set( 'm_strTransmissionRemotePrimaryKey', CStrings::strTrimDef( $strTransmissionRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getTransmissionRemotePrimaryKey() {
		return $this->m_strTransmissionRemotePrimaryKey;
	}

	public function sqlTransmissionRemotePrimaryKey() {
		return ( true == isset( $this->m_strTransmissionRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strTransmissionRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTransmissionDatetime( $strTransmissionDatetime ) {
		$this->set( 'm_strTransmissionDatetime', CStrings::strTrimDef( $strTransmissionDatetime, -1, NULL, true ) );
	}

	public function getTransmissionDatetime() {
		return $this->m_strTransmissionDatetime;
	}

	public function sqlTransmissionDatetime() {
		return ( true == isset( $this->m_strTransmissionDatetime ) ) ? '\'' . $this->m_strTransmissionDatetime . '\'' : 'NOW()';
	}

	public function setReportName( $strReportName ) {
		$this->set( 'm_strReportName', CStrings::strTrimDef( $strReportName, -1, NULL, true ) );
	}

	public function getReportName() {
		return $this->m_strReportName;
	}

	public function sqlReportName() {
		return ( true == isset( $this->m_strReportName ) ) ? '\'' . addslashes( $this->m_strReportName ) . '\'' : 'NULL';
	}

	public function setResponse( $strResponse ) {
		$this->set( 'm_strResponse', CStrings::strTrimDef( $strResponse, -1, NULL, true ) );
	}

	public function getResponse() {
		return $this->m_strResponse;
	}

	public function sqlResponse() {
		return ( true == isset( $this->m_strResponse ) ) ? '\'' . addslashes( $this->m_strResponse ) . '\'' : 'NULL';
	}

	public function setHasCriminalFlag( $intHasCriminalFlag ) {
		$this->set( 'm_intHasCriminalFlag', CStrings::strToIntDef( $intHasCriminalFlag, NULL, false ) );
	}

	public function getHasCriminalFlag() {
		return $this->m_intHasCriminalFlag;
	}

	public function sqlHasCriminalFlag() {
		return ( true == isset( $this->m_intHasCriminalFlag ) ) ? ( string ) $this->m_intHasCriminalFlag : '0';
	}

	public function setHasCreditAlert( $intHasCreditAlert ) {
		$this->set( 'm_intHasCreditAlert', CStrings::strToIntDef( $intHasCreditAlert, NULL, false ) );
	}

	public function getHasCreditAlert() {
		return $this->m_intHasCreditAlert;
	}

	public function sqlHasCreditAlert() {
		return ( true == isset( $this->m_intHasCreditAlert ) ) ? ( string ) $this->m_intHasCreditAlert : '0';
	}

	public function setHasDataError( $intHasDataError ) {
		$this->set( 'm_intHasDataError', CStrings::strToIntDef( $intHasDataError, NULL, false ) );
	}

	public function getHasDataError() {
		return $this->m_intHasDataError;
	}

	public function sqlHasDataError() {
		return ( true == isset( $this->m_intHasDataError ) ) ? ( string ) $this->m_intHasDataError : '0';
	}

	public function setIsFailed( $intIsFailed ) {
		$this->set( 'm_intIsFailed', CStrings::strToIntDef( $intIsFailed, NULL, false ) );
	}

	public function getIsFailed() {
		return $this->m_intIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_intIsFailed ) ) ? ( string ) $this->m_intIsFailed : '0';
	}

	public function setCriminalResponseOn( $strCriminalResponseOn ) {
		$this->set( 'm_strCriminalResponseOn', CStrings::strTrimDef( $strCriminalResponseOn, -1, NULL, true ) );
	}

	public function getCriminalResponseOn() {
		return $this->m_strCriminalResponseOn;
	}

	public function sqlCriminalResponseOn() {
		return ( true == isset( $this->m_strCriminalResponseOn ) ) ? '\'' . $this->m_strCriminalResponseOn . '\'' : 'NULL';
	}

	public function setAdversePrintedOn( $strAdversePrintedOn ) {
		$this->set( 'm_strAdversePrintedOn', CStrings::strTrimDef( $strAdversePrintedOn, -1, NULL, true ) );
	}

	public function getAdversePrintedOn() {
		return $this->m_strAdversePrintedOn;
	}

	public function sqlAdversePrintedOn() {
		return ( true == isset( $this->m_strAdversePrintedOn ) ) ? '\'' . $this->m_strAdversePrintedOn . '\'' : 'NULL';
	}

	public function setAdverseEmailedOn( $strAdverseEmailedOn ) {
		$this->set( 'm_strAdverseEmailedOn', CStrings::strTrimDef( $strAdverseEmailedOn, -1, NULL, true ) );
	}

	public function getAdverseEmailedOn() {
		return $this->m_strAdverseEmailedOn;
	}

	public function sqlAdverseEmailedOn() {
		return ( true == isset( $this->m_strAdverseEmailedOn ) ) ? '\'' . $this->m_strAdverseEmailedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setResponseText( $strResponseText ) {
		$this->set( 'm_strResponseText', CStrings::strTrimDef( $strResponseText, -1, NULL, true ) );
	}

	public function getResponseText() {
		return $this->m_strResponseText;
	}

	public function sqlResponseText() {
		return ( true == isset( $this->m_strResponseText ) ) ? '\'' . addslashes( $this->m_strResponseText ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, applicant_application_id, transmission_id, transmission_vendor_id, company_transmission_vendor_id, credit_transmission_response_type_id, criminal_transmission_response_type_id, identify_transmission_response_type_id, individual_transmission_response_type_id, transmission_response_type_id, screening_applicant_id, transmission_remote_primary_key, remote_primary_key, transmission_datetime, report_name, response, has_criminal_flag, has_credit_alert, has_data_error, is_failed, criminal_response_on, adverse_printed_on, adverse_emailed_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on, response_text )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlApplicantApplicationId() . ', ' .
 						$this->sqlTransmissionId() . ', ' .
 						$this->sqlTransmissionVendorId() . ', ' .
 						$this->sqlCompanyTransmissionVendorId() . ', ' .
 						$this->sqlCreditTransmissionResponseTypeId() . ', ' .
 						$this->sqlCriminalTransmissionResponseTypeId() . ', ' .
 						$this->sqlIdentifyTransmissionResponseTypeId() . ', ' .
 						$this->sqlIndividualTransmissionResponseTypeId() . ', ' .
 						$this->sqlTransmissionResponseTypeId() . ', ' .
 						$this->sqlScreeningApplicantId() . ', ' .
 						$this->sqlTransmissionRemotePrimaryKey() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlTransmissionDatetime() . ', ' .
 						$this->sqlReportName() . ', ' .
 						$this->sqlResponse() . ', ' .
 						$this->sqlHasCriminalFlag() . ', ' .
 						$this->sqlHasCreditAlert() . ', ' .
 						$this->sqlHasDataError() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlCriminalResponseOn() . ', ' .
 						$this->sqlAdversePrintedOn() . ', ' .
 						$this->sqlAdverseEmailedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlResponseText() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId() . ','; } elseif( true == array_key_exists( 'TransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'CompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_transmission_response_type_id = ' . $this->sqlCreditTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'CreditTransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' credit_transmission_response_type_id = ' . $this->sqlCreditTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criminal_transmission_response_type_id = ' . $this->sqlCriminalTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'CriminalTransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' criminal_transmission_response_type_id = ' . $this->sqlCriminalTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identify_transmission_response_type_id = ' . $this->sqlIdentifyTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'IdentifyTransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' identify_transmission_response_type_id = ' . $this->sqlIdentifyTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' individual_transmission_response_type_id = ' . $this->sqlIndividualTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'IndividualTransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' individual_transmission_response_type_id = ' . $this->sqlIndividualTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_response_type_id = ' . $this->sqlTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'TransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_response_type_id = ' . $this->sqlTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_remote_primary_key = ' . $this->sqlTransmissionRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'TransmissionRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' transmission_remote_primary_key = ' . $this->sqlTransmissionRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; } elseif( true == array_key_exists( 'TransmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_name = ' . $this->sqlReportName() . ','; } elseif( true == array_key_exists( 'ReportName', $this->getChangedColumns() ) ) { $strSql .= ' report_name = ' . $this->sqlReportName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; } elseif( true == array_key_exists( 'Response', $this->getChangedColumns() ) ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_criminal_flag = ' . $this->sqlHasCriminalFlag() . ','; } elseif( true == array_key_exists( 'HasCriminalFlag', $this->getChangedColumns() ) ) { $strSql .= ' has_criminal_flag = ' . $this->sqlHasCriminalFlag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; } elseif( true == array_key_exists( 'HasCreditAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_data_error = ' . $this->sqlHasDataError() . ','; } elseif( true == array_key_exists( 'HasDataError', $this->getChangedColumns() ) ) { $strSql .= ' has_data_error = ' . $this->sqlHasDataError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criminal_response_on = ' . $this->sqlCriminalResponseOn() . ','; } elseif( true == array_key_exists( 'CriminalResponseOn', $this->getChangedColumns() ) ) { $strSql .= ' criminal_response_on = ' . $this->sqlCriminalResponseOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adverse_printed_on = ' . $this->sqlAdversePrintedOn() . ','; } elseif( true == array_key_exists( 'AdversePrintedOn', $this->getChangedColumns() ) ) { $strSql .= ' adverse_printed_on = ' . $this->sqlAdversePrintedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adverse_emailed_on = ' . $this->sqlAdverseEmailedOn() . ','; } elseif( true == array_key_exists( 'AdverseEmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' adverse_emailed_on = ' . $this->sqlAdverseEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; } elseif( true == array_key_exists( 'ResponseText', $this->getChangedColumns() ) ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'transmission_id' => $this->getTransmissionId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'company_transmission_vendor_id' => $this->getCompanyTransmissionVendorId(),
			'credit_transmission_response_type_id' => $this->getCreditTransmissionResponseTypeId(),
			'criminal_transmission_response_type_id' => $this->getCriminalTransmissionResponseTypeId(),
			'identify_transmission_response_type_id' => $this->getIdentifyTransmissionResponseTypeId(),
			'individual_transmission_response_type_id' => $this->getIndividualTransmissionResponseTypeId(),
			'transmission_response_type_id' => $this->getTransmissionResponseTypeId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'transmission_remote_primary_key' => $this->getTransmissionRemotePrimaryKey(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transmission_datetime' => $this->getTransmissionDatetime(),
			'report_name' => $this->getReportName(),
			'response' => $this->getResponse(),
			'has_criminal_flag' => $this->getHasCriminalFlag(),
			'has_credit_alert' => $this->getHasCreditAlert(),
			'has_data_error' => $this->getHasDataError(),
			'is_failed' => $this->getIsFailed(),
			'criminal_response_on' => $this->getCriminalResponseOn(),
			'adverse_printed_on' => $this->getAdversePrintedOn(),
			'adverse_emailed_on' => $this->getAdverseEmailedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'response_text' => $this->getResponseText()
		);
	}

}
?>