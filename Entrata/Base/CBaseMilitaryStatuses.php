<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryStatuses
 * Do not add any new functions to this class.
 */

class CBaseMilitaryStatuses extends CEosPluralBase {

	/**
	 * @return CMilitaryStatus[]
	 */
	public static function fetchMilitaryStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryStatus::class, $objDatabase );
	}

	/**
	 * @return CMilitaryStatus
	 */
	public static function fetchMilitaryStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryStatus::class, $objDatabase );
	}

	public static function fetchMilitaryStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_statuses', $objDatabase );
	}

	public static function fetchMilitaryStatusById( $intId, $objDatabase ) {
		return self::fetchMilitaryStatus( sprintf( 'SELECT * FROM military_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>