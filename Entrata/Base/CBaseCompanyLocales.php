<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyLocales
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyLocales extends CEosPluralBase {

	/**
	 * @return CCompanyLocale[]
	 */
	public static function fetchCompanyLocales( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyLocale::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyLocale
	 */
	public static function fetchCompanyLocale( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyLocale::class, $objDatabase );
	}

	public static function fetchCompanyLocaleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_locales', $objDatabase );
	}

	public static function fetchCompanyLocaleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyLocale( sprintf( 'SELECT * FROM company_locales WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyLocalesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyLocales( sprintf( 'SELECT * FROM company_locales WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>