<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApps
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApps extends CEosPluralBase {

	/**
	 * @return CCompanyApp[]
	 */
	public static function fetchCompanyApps( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyApp', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyApp
	 */
	public static function fetchCompanyApp( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyApp', $objDatabase );
	}

	public static function fetchCompanyAppCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_apps', $objDatabase );
	}

	public static function fetchCompanyAppByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyApp( sprintf( 'SELECT * FROM company_apps WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyAppsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApps( sprintf( 'SELECT * FROM company_apps WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyAppsByAppIdByCid( $intAppId, $intCid, $objDatabase ) {
		return self::fetchCompanyApps( sprintf( 'SELECT * FROM company_apps WHERE app_id = %d AND cid = %d', ( int ) $intAppId, ( int ) $intCid ), $objDatabase );
	}

}
?>