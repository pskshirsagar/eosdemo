<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUtilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUtilities extends CEosPluralBase {

	/**
	 * @return CUtility[]
	 */
	public static function fetchUtilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUtility::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUtility
	 */
	public static function fetchUtility( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUtility::class, $objDatabase );
	}

	public static function fetchUtilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'utilities', $objDatabase );
	}

	public static function fetchUtilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUtility( sprintf( 'SELECT * FROM utilities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUtilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchUtilities( sprintf( 'SELECT * FROM utilities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUtilitiesByUtilityTypeIdByCid( $intUtilityTypeId, $intCid, $objDatabase ) {
		return self::fetchUtilities( sprintf( 'SELECT * FROM utilities WHERE utility_type_id = %d AND cid = %d', $intUtilityTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUtilitiesByCategorizationTypeIdByCid( $intCategorizationTypeId, $intCid, $objDatabase ) {
		return self::fetchUtilities( sprintf( 'SELECT * FROM utilities WHERE categorization_type_id = %d AND cid = %d', $intCategorizationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUtilitiesByMappingIdByCid( $intMappingId, $intCid, $objDatabase ) {
		return self::fetchUtilities( sprintf( 'SELECT * FROM utilities WHERE mapping_id = %d AND cid = %d', $intMappingId, $intCid ), $objDatabase );
	}

}
?>