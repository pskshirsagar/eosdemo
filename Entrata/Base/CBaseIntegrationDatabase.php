<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationDatabase extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.integration_databases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationClientTypeId;
	protected $m_intIntegrationClientStatusTypeId;
	protected $m_intIntegrationVersionId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDatabaseName;
	protected $m_strServerName;
	protected $m_strPlatform;
	protected $m_strBaseUri;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strDevBaseUri;
	protected $m_strDevDatabaseName;
	protected $m_strDevUsernameEncrypted;
	protected $m_strDevPasswordEncrypted;
	protected $m_strDatabaseServerName;
	protected $m_strDatabaseUsernameEncrypted;
	protected $m_strDatabasePasswordEncrypted;
	protected $m_intIsUtilityUpdatePermission;
	protected $m_intIsSelfHosted;
	protected $m_strOnHoldOn;
	protected $m_intOnHoldBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIntegrationClientStatusTypeId = '1';
		$this->m_intIsUtilityUpdatePermission = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_client_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientTypeId', trim( $arrValues['integration_client_type_id'] ) ); elseif( isset( $arrValues['integration_client_type_id'] ) ) $this->setIntegrationClientTypeId( $arrValues['integration_client_type_id'] );
		if( isset( $arrValues['integration_client_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientStatusTypeId', trim( $arrValues['integration_client_status_type_id'] ) ); elseif( isset( $arrValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrValues['integration_client_status_type_id'] );
		if( isset( $arrValues['integration_version_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationVersionId', trim( $arrValues['integration_version_id'] ) ); elseif( isset( $arrValues['integration_version_id'] ) ) $this->setIntegrationVersionId( $arrValues['integration_version_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['database_name'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseName', trim( stripcslashes( $arrValues['database_name'] ) ) ); elseif( isset( $arrValues['database_name'] ) ) $this->setDatabaseName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_name'] ) : $arrValues['database_name'] );
		if( isset( $arrValues['server_name'] ) && $boolDirectSet ) $this->set( 'm_strServerName', trim( stripcslashes( $arrValues['server_name'] ) ) ); elseif( isset( $arrValues['server_name'] ) ) $this->setServerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['server_name'] ) : $arrValues['server_name'] );
		if( isset( $arrValues['platform'] ) && $boolDirectSet ) $this->set( 'm_strPlatform', trim( stripcslashes( $arrValues['platform'] ) ) ); elseif( isset( $arrValues['platform'] ) ) $this->setPlatform( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['platform'] ) : $arrValues['platform'] );
		if( isset( $arrValues['base_uri'] ) && $boolDirectSet ) $this->set( 'm_strBaseUri', trim( stripcslashes( $arrValues['base_uri'] ) ) ); elseif( isset( $arrValues['base_uri'] ) ) $this->setBaseUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_uri'] ) : $arrValues['base_uri'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( stripcslashes( $arrValues['username_encrypted'] ) ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username_encrypted'] ) : $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['dev_base_uri'] ) && $boolDirectSet ) $this->set( 'm_strDevBaseUri', trim( stripcslashes( $arrValues['dev_base_uri'] ) ) ); elseif( isset( $arrValues['dev_base_uri'] ) ) $this->setDevBaseUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dev_base_uri'] ) : $arrValues['dev_base_uri'] );
		if( isset( $arrValues['dev_database_name'] ) && $boolDirectSet ) $this->set( 'm_strDevDatabaseName', trim( stripcslashes( $arrValues['dev_database_name'] ) ) ); elseif( isset( $arrValues['dev_database_name'] ) ) $this->setDevDatabaseName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dev_database_name'] ) : $arrValues['dev_database_name'] );
		if( isset( $arrValues['dev_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDevUsernameEncrypted', trim( stripcslashes( $arrValues['dev_username_encrypted'] ) ) ); elseif( isset( $arrValues['dev_username_encrypted'] ) ) $this->setDevUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dev_username_encrypted'] ) : $arrValues['dev_username_encrypted'] );
		if( isset( $arrValues['dev_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDevPasswordEncrypted', trim( stripcslashes( $arrValues['dev_password_encrypted'] ) ) ); elseif( isset( $arrValues['dev_password_encrypted'] ) ) $this->setDevPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dev_password_encrypted'] ) : $arrValues['dev_password_encrypted'] );
		if( isset( $arrValues['database_server_name'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseServerName', trim( stripcslashes( $arrValues['database_server_name'] ) ) ); elseif( isset( $arrValues['database_server_name'] ) ) $this->setDatabaseServerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_server_name'] ) : $arrValues['database_server_name'] );
		if( isset( $arrValues['database_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseUsernameEncrypted', trim( stripcslashes( $arrValues['database_username_encrypted'] ) ) ); elseif( isset( $arrValues['database_username_encrypted'] ) ) $this->setDatabaseUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_username_encrypted'] ) : $arrValues['database_username_encrypted'] );
		if( isset( $arrValues['database_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDatabasePasswordEncrypted', trim( stripcslashes( $arrValues['database_password_encrypted'] ) ) ); elseif( isset( $arrValues['database_password_encrypted'] ) ) $this->setDatabasePasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_password_encrypted'] ) : $arrValues['database_password_encrypted'] );
		if( isset( $arrValues['is_utility_update_permission'] ) && $boolDirectSet ) $this->set( 'm_intIsUtilityUpdatePermission', trim( $arrValues['is_utility_update_permission'] ) ); elseif( isset( $arrValues['is_utility_update_permission'] ) ) $this->setIsUtilityUpdatePermission( $arrValues['is_utility_update_permission'] );
		if( isset( $arrValues['is_self_hosted'] ) && $boolDirectSet ) $this->set( 'm_intIsSelfHosted', trim( $arrValues['is_self_hosted'] ) ); elseif( isset( $arrValues['is_self_hosted'] ) ) $this->setIsSelfHosted( $arrValues['is_self_hosted'] );
		if( isset( $arrValues['on_hold_on'] ) && $boolDirectSet ) $this->set( 'm_strOnHoldOn', trim( $arrValues['on_hold_on'] ) ); elseif( isset( $arrValues['on_hold_on'] ) ) $this->setOnHoldOn( $arrValues['on_hold_on'] );
		if( isset( $arrValues['on_hold_by'] ) && $boolDirectSet ) $this->set( 'm_intOnHoldBy', trim( $arrValues['on_hold_by'] ) ); elseif( isset( $arrValues['on_hold_by'] ) ) $this->setOnHoldBy( $arrValues['on_hold_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationClientTypeId( $intIntegrationClientTypeId ) {
		$this->set( 'm_intIntegrationClientTypeId', CStrings::strToIntDef( $intIntegrationClientTypeId, NULL, false ) );
	}

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
	}

	public function sqlIntegrationClientTypeId() {
		return ( true == isset( $this->m_intIntegrationClientTypeId ) ) ? ( string ) $this->m_intIntegrationClientTypeId : 'NULL';
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		$this->set( 'm_intIntegrationClientStatusTypeId', CStrings::strToIntDef( $intIntegrationClientStatusTypeId, NULL, false ) );
	}

	public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

	public function sqlIntegrationClientStatusTypeId() {
		return ( true == isset( $this->m_intIntegrationClientStatusTypeId ) ) ? ( string ) $this->m_intIntegrationClientStatusTypeId : '1';
	}

	public function setIntegrationVersionId( $intIntegrationVersionId ) {
		$this->set( 'm_intIntegrationVersionId', CStrings::strToIntDef( $intIntegrationVersionId, NULL, false ) );
	}

	public function getIntegrationVersionId() {
		return $this->m_intIntegrationVersionId;
	}

	public function sqlIntegrationVersionId() {
		return ( true == isset( $this->m_intIntegrationVersionId ) ) ? ( string ) $this->m_intIntegrationVersionId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDatabaseName( $strDatabaseName ) {
		$this->set( 'm_strDatabaseName', CStrings::strTrimDef( $strDatabaseName, 240, NULL, true ) );
	}

	public function getDatabaseName() {
		return $this->m_strDatabaseName;
	}

	public function sqlDatabaseName() {
		return ( true == isset( $this->m_strDatabaseName ) ) ? '\'' . addslashes( $this->m_strDatabaseName ) . '\'' : 'NULL';
	}

	public function setServerName( $strServerName ) {
		$this->set( 'm_strServerName', CStrings::strTrimDef( $strServerName, 50, NULL, true ) );
	}

	public function getServerName() {
		return $this->m_strServerName;
	}

	public function sqlServerName() {
		return ( true == isset( $this->m_strServerName ) ) ? '\'' . addslashes( $this->m_strServerName ) . '\'' : 'NULL';
	}

	public function setPlatform( $strPlatform ) {
		$this->set( 'm_strPlatform', CStrings::strTrimDef( $strPlatform, 50, NULL, true ) );
	}

	public function getPlatform() {
		return $this->m_strPlatform;
	}

	public function sqlPlatform() {
		return ( true == isset( $this->m_strPlatform ) ) ? '\'' . addslashes( $this->m_strPlatform ) . '\'' : 'NULL';
	}

	public function setBaseUri( $strBaseUri ) {
		$this->set( 'm_strBaseUri', CStrings::strTrimDef( $strBaseUri, 4096, NULL, true ) );
	}

	public function getBaseUri() {
		return $this->m_strBaseUri;
	}

	public function sqlBaseUri() {
		return ( true == isset( $this->m_strBaseUri ) ) ? '\'' . addslashes( $this->m_strBaseUri ) . '\'' : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, 240, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setDevBaseUri( $strDevBaseUri ) {
		$this->set( 'm_strDevBaseUri', CStrings::strTrimDef( $strDevBaseUri, 4096, NULL, true ) );
	}

	public function getDevBaseUri() {
		return $this->m_strDevBaseUri;
	}

	public function sqlDevBaseUri() {
		return ( true == isset( $this->m_strDevBaseUri ) ) ? '\'' . addslashes( $this->m_strDevBaseUri ) . '\'' : 'NULL';
	}

	public function setDevDatabaseName( $strDevDatabaseName ) {
		$this->set( 'm_strDevDatabaseName', CStrings::strTrimDef( $strDevDatabaseName, 50, NULL, true ) );
	}

	public function getDevDatabaseName() {
		return $this->m_strDevDatabaseName;
	}

	public function sqlDevDatabaseName() {
		return ( true == isset( $this->m_strDevDatabaseName ) ) ? '\'' . addslashes( $this->m_strDevDatabaseName ) . '\'' : 'NULL';
	}

	public function setDevUsernameEncrypted( $strDevUsernameEncrypted ) {
		$this->set( 'm_strDevUsernameEncrypted', CStrings::strTrimDef( $strDevUsernameEncrypted, 240, NULL, true ) );
	}

	public function getDevUsernameEncrypted() {
		return $this->m_strDevUsernameEncrypted;
	}

	public function sqlDevUsernameEncrypted() {
		return ( true == isset( $this->m_strDevUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strDevUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setDevPasswordEncrypted( $strDevPasswordEncrypted ) {
		$this->set( 'm_strDevPasswordEncrypted', CStrings::strTrimDef( $strDevPasswordEncrypted, 240, NULL, true ) );
	}

	public function getDevPasswordEncrypted() {
		return $this->m_strDevPasswordEncrypted;
	}

	public function sqlDevPasswordEncrypted() {
		return ( true == isset( $this->m_strDevPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strDevPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setDatabaseServerName( $strDatabaseServerName ) {
		$this->set( 'm_strDatabaseServerName', CStrings::strTrimDef( $strDatabaseServerName, 50, NULL, true ) );
	}

	public function getDatabaseServerName() {
		return $this->m_strDatabaseServerName;
	}

	public function sqlDatabaseServerName() {
		return ( true == isset( $this->m_strDatabaseServerName ) ) ? '\'' . addslashes( $this->m_strDatabaseServerName ) . '\'' : 'NULL';
	}

	public function setDatabaseUsernameEncrypted( $strDatabaseUsernameEncrypted ) {
		$this->set( 'm_strDatabaseUsernameEncrypted', CStrings::strTrimDef( $strDatabaseUsernameEncrypted, 240, NULL, true ) );
	}

	public function getDatabaseUsernameEncrypted() {
		return $this->m_strDatabaseUsernameEncrypted;
	}

	public function sqlDatabaseUsernameEncrypted() {
		return ( true == isset( $this->m_strDatabaseUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strDatabaseUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setDatabasePasswordEncrypted( $strDatabasePasswordEncrypted ) {
		$this->set( 'm_strDatabasePasswordEncrypted', CStrings::strTrimDef( $strDatabasePasswordEncrypted, 240, NULL, true ) );
	}

	public function getDatabasePasswordEncrypted() {
		return $this->m_strDatabasePasswordEncrypted;
	}

	public function sqlDatabasePasswordEncrypted() {
		return ( true == isset( $this->m_strDatabasePasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strDatabasePasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setIsUtilityUpdatePermission( $intIsUtilityUpdatePermission ) {
		$this->set( 'm_intIsUtilityUpdatePermission', CStrings::strToIntDef( $intIsUtilityUpdatePermission, NULL, false ) );
	}

	public function getIsUtilityUpdatePermission() {
		return $this->m_intIsUtilityUpdatePermission;
	}

	public function sqlIsUtilityUpdatePermission() {
		return ( true == isset( $this->m_intIsUtilityUpdatePermission ) ) ? ( string ) $this->m_intIsUtilityUpdatePermission : '1';
	}

	public function setIsSelfHosted( $intIsSelfHosted ) {
		$this->set( 'm_intIsSelfHosted', CStrings::strToIntDef( $intIsSelfHosted, NULL, false ) );
	}

	public function getIsSelfHosted() {
		return $this->m_intIsSelfHosted;
	}

	public function sqlIsSelfHosted() {
		return ( true == isset( $this->m_intIsSelfHosted ) ) ? ( string ) $this->m_intIsSelfHosted : 'NULL';
	}

	public function setOnHoldOn( $strOnHoldOn ) {
		$this->set( 'm_strOnHoldOn', CStrings::strTrimDef( $strOnHoldOn, -1, NULL, true ) );
	}

	public function getOnHoldOn() {
		return $this->m_strOnHoldOn;
	}

	public function sqlOnHoldOn() {
		return ( true == isset( $this->m_strOnHoldOn ) ) ? '\'' . $this->m_strOnHoldOn . '\'' : 'NULL';
	}

	public function setOnHoldBy( $intOnHoldBy ) {
		$this->set( 'm_intOnHoldBy', CStrings::strToIntDef( $intOnHoldBy, NULL, false ) );
	}

	public function getOnHoldBy() {
		return $this->m_intOnHoldBy;
	}

	public function sqlOnHoldBy() {
		return ( true == isset( $this->m_intOnHoldBy ) ) ? ( string ) $this->m_intOnHoldBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_client_type_id, integration_client_status_type_id, integration_version_id, name, description, database_name, server_name, platform, base_uri, username_encrypted, password_encrypted, dev_base_uri, dev_database_name, dev_username_encrypted, dev_password_encrypted, database_server_name, database_username_encrypted, database_password_encrypted, is_utility_update_permission, is_self_hosted, on_hold_on, on_hold_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlIntegrationClientTypeId() . ', ' .
		          $this->sqlIntegrationClientStatusTypeId() . ', ' .
		          $this->sqlIntegrationVersionId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlDatabaseName() . ', ' .
		          $this->sqlServerName() . ', ' .
		          $this->sqlPlatform() . ', ' .
		          $this->sqlBaseUri() . ', ' .
		          $this->sqlUsernameEncrypted() . ', ' .
		          $this->sqlPasswordEncrypted() . ', ' .
		          $this->sqlDevBaseUri() . ', ' .
		          $this->sqlDevDatabaseName() . ', ' .
		          $this->sqlDevUsernameEncrypted() . ', ' .
		          $this->sqlDevPasswordEncrypted() . ', ' .
		          $this->sqlDatabaseServerName() . ', ' .
		          $this->sqlDatabaseUsernameEncrypted() . ', ' .
		          $this->sqlDatabasePasswordEncrypted() . ', ' .
		          $this->sqlIsUtilityUpdatePermission() . ', ' .
		          $this->sqlIsSelfHosted() . ', ' .
		          $this->sqlOnHoldOn() . ', ' .
		          $this->sqlOnHoldBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
                  $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_type_id = ' . $this->sqlIntegrationClientTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationClientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_type_id = ' . $this->sqlIntegrationClientTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_status_type_id = ' . $this->sqlIntegrationClientStatusTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationClientStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_status_type_id = ' . $this->sqlIntegrationClientStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_version_id = ' . $this->sqlIntegrationVersionId() . ','; } elseif( true == array_key_exists( 'IntegrationVersionId', $this->getChangedColumns() ) ) { $strSql .= ' integration_version_id = ' . $this->sqlIntegrationVersionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName() . ','; } elseif( true == array_key_exists( 'DatabaseName', $this->getChangedColumns() ) ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server_name = ' . $this->sqlServerName() . ','; } elseif( true == array_key_exists( 'ServerName', $this->getChangedColumns() ) ) { $strSql .= ' server_name = ' . $this->sqlServerName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' platform = ' . $this->sqlPlatform() . ','; } elseif( true == array_key_exists( 'Platform', $this->getChangedColumns() ) ) { $strSql .= ' platform = ' . $this->sqlPlatform() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_uri = ' . $this->sqlBaseUri() . ','; } elseif( true == array_key_exists( 'BaseUri', $this->getChangedColumns() ) ) { $strSql .= ' base_uri = ' . $this->sqlBaseUri() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_base_uri = ' . $this->sqlDevBaseUri() . ','; } elseif( true == array_key_exists( 'DevBaseUri', $this->getChangedColumns() ) ) { $strSql .= ' dev_base_uri = ' . $this->sqlDevBaseUri() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_database_name = ' . $this->sqlDevDatabaseName() . ','; } elseif( true == array_key_exists( 'DevDatabaseName', $this->getChangedColumns() ) ) { $strSql .= ' dev_database_name = ' . $this->sqlDevDatabaseName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_username_encrypted = ' . $this->sqlDevUsernameEncrypted() . ','; } elseif( true == array_key_exists( 'DevUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dev_username_encrypted = ' . $this->sqlDevUsernameEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_password_encrypted = ' . $this->sqlDevPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'DevPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dev_password_encrypted = ' . $this->sqlDevPasswordEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_server_name = ' . $this->sqlDatabaseServerName() . ','; } elseif( true == array_key_exists( 'DatabaseServerName', $this->getChangedColumns() ) ) { $strSql .= ' database_server_name = ' . $this->sqlDatabaseServerName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_username_encrypted = ' . $this->sqlDatabaseUsernameEncrypted() . ','; } elseif( true == array_key_exists( 'DatabaseUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' database_username_encrypted = ' . $this->sqlDatabaseUsernameEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_password_encrypted = ' . $this->sqlDatabasePasswordEncrypted() . ','; } elseif( true == array_key_exists( 'DatabasePasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' database_password_encrypted = ' . $this->sqlDatabasePasswordEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_utility_update_permission = ' . $this->sqlIsUtilityUpdatePermission() . ','; } elseif( true == array_key_exists( 'IsUtilityUpdatePermission', $this->getChangedColumns() ) ) { $strSql .= ' is_utility_update_permission = ' . $this->sqlIsUtilityUpdatePermission() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_self_hosted = ' . $this->sqlIsSelfHosted() . ','; } elseif( true == array_key_exists( 'IsSelfHosted', $this->getChangedColumns() ) ) { $strSql .= ' is_self_hosted = ' . $this->sqlIsSelfHosted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_hold_on = ' . $this->sqlOnHoldOn() . ','; } elseif( true == array_key_exists( 'OnHoldOn', $this->getChangedColumns() ) ) { $strSql .= ' on_hold_on = ' . $this->sqlOnHoldOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_hold_by = ' . $this->sqlOnHoldBy() . ','; } elseif( true == array_key_exists( 'OnHoldBy', $this->getChangedColumns() ) ) { $strSql .= ' on_hold_by = ' . $this->sqlOnHoldBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_client_type_id' => $this->getIntegrationClientTypeId(),
			'integration_client_status_type_id' => $this->getIntegrationClientStatusTypeId(),
			'integration_version_id' => $this->getIntegrationVersionId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'database_name' => $this->getDatabaseName(),
			'server_name' => $this->getServerName(),
			'platform' => $this->getPlatform(),
			'base_uri' => $this->getBaseUri(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'dev_base_uri' => $this->getDevBaseUri(),
			'dev_database_name' => $this->getDevDatabaseName(),
			'dev_username_encrypted' => $this->getDevUsernameEncrypted(),
			'dev_password_encrypted' => $this->getDevPasswordEncrypted(),
			'database_server_name' => $this->getDatabaseServerName(),
			'database_username_encrypted' => $this->getDatabaseUsernameEncrypted(),
			'database_password_encrypted' => $this->getDatabasePasswordEncrypted(),
			'is_utility_update_permission' => $this->getIsUtilityUpdatePermission(),
			'is_self_hosted' => $this->getIsSelfHosted(),
			'details' => $this->getDetails(),
			'on_hold_on' => $this->getOnHoldOn(),
			'on_hold_by' => $this->getOnHoldBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>