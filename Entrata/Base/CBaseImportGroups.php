<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportGroups
 * Do not add any new functions to this class.
 */

class CBaseImportGroups extends CEosPluralBase {

	/**
	 * @return CImportGroup[]
	 */
	public static function fetchImportGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CImportGroup::class, $objDatabase );
	}

	/**
	 * @return CImportGroup
	 */
	public static function fetchImportGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CImportGroup::class, $objDatabase );
	}

	public static function fetchImportGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_groups', $objDatabase );
	}

	public static function fetchImportGroupById( $intId, $objDatabase ) {
		return self::fetchImportGroup( sprintf( 'SELECT * FROM import_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>