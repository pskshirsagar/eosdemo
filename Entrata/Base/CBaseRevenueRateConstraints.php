<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueRateConstraints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueRateConstraints extends CEosPluralBase {

	/**
	 * @return CRevenueRateConstraint[]
	 */
	public static function fetchRevenueRateConstraints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueRateConstraint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueRateConstraint
	 */
	public static function fetchRevenueRateConstraint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueRateConstraint', $objDatabase );
	}

	public static function fetchRevenueRateConstraintCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_rate_constraints', $objDatabase );
	}

	public static function fetchRevenueRateConstraintByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueRateConstraint( sprintf( 'SELECT * FROM revenue_rate_constraints WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueRateConstraintsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueRateConstraints( sprintf( 'SELECT * FROM revenue_rate_constraints WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueRateConstraintsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueRateConstraints( sprintf( 'SELECT * FROM revenue_rate_constraints WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueRateConstraintsByRevenueRateConstraintTypeIdByCid( $intRevenueRateConstraintTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueRateConstraints( sprintf( 'SELECT * FROM revenue_rate_constraints WHERE revenue_rate_constraint_type_id = %d AND cid = %d', ( int ) $intRevenueRateConstraintTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>