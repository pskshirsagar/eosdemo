<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueCalcTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueCalcTypes extends CEosPluralBase {

	/**
	 * @return CRevenueCalcType[]
	 */
	public static function fetchRevenueCalcTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueCalcType::class, $objDatabase );
	}

	/**
	 * @return CRevenueCalcType
	 */
	public static function fetchRevenueCalcType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueCalcType::class, $objDatabase );
	}

	public static function fetchRevenueCalcTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_calc_types', $objDatabase );
	}

	public static function fetchRevenueCalcTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueCalcType( sprintf( 'SELECT * FROM revenue_calc_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>