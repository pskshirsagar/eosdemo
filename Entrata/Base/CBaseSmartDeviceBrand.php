<?php

class CBaseSmartDeviceBrand extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.smart_device_brands';

	protected $m_intId;
	protected $m_intApCodeId;
	protected $m_intSmartDeviceTypeId;
	protected $m_strName;
	protected $m_strMake;
	protected $m_strModel;
	protected $m_intOrderNum;
	protected $m_strUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['smart_device_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSmartDeviceTypeId', trim( $arrValues['smart_device_type_id'] ) ); elseif( isset( $arrValues['smart_device_type_id'] ) ) $this->setSmartDeviceTypeId( $arrValues['smart_device_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['make'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMake', trim( stripcslashes( $arrValues['make'] ) ) ); elseif( isset( $arrValues['make'] ) ) $this->setMake( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['make'] ) : $arrValues['make'] );
		if( isset( $arrValues['model'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strModel', trim( stripcslashes( $arrValues['model'] ) ) ); elseif( isset( $arrValues['model'] ) ) $this->setModel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['model'] ) : $arrValues['model'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedBy', trim( stripcslashes( $arrValues['updated_by'] ) ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['updated_by'] ) : $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_strCreatedBy', trim( stripcslashes( $arrValues['created_by'] ) ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['created_by'] ) : $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setSmartDeviceTypeId( $intSmartDeviceTypeId ) {
		$this->set( 'm_intSmartDeviceTypeId', CStrings::strToIntDef( $intSmartDeviceTypeId, NULL, false ) );
	}

	public function getSmartDeviceTypeId() {
		return $this->m_intSmartDeviceTypeId;
	}

	public function sqlSmartDeviceTypeId() {
		return ( true == isset( $this->m_intSmartDeviceTypeId ) ) ? ( string ) $this->m_intSmartDeviceTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 60, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setMake( $strMake, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMake', CStrings::strTrimDef( $strMake, 50, NULL, true ), $strLocaleCode );
	}

	public function getMake( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMake', $strLocaleCode );
	}

	public function sqlMake() {
		return ( true == isset( $this->m_strMake ) ) ? '\'' . addslashes( $this->m_strMake ) . '\'' : 'NULL';
	}

	public function setModel( $strModel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strModel', CStrings::strTrimDef( $strModel, 30, NULL, true ), $strLocaleCode );
	}

	public function getModel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strModel', $strLocaleCode );
	}

	public function sqlModel() {
		return ( true == isset( $this->m_strModel ) ) ? '\'' . addslashes( $this->m_strModel ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $strUpdatedBy ) {
		$this->set( 'm_strUpdatedBy', CStrings::strTrimDef( $strUpdatedBy, 50, NULL, true ) );
	}

	public function getUpdatedBy() {
		return $this->m_strUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_strUpdatedBy ) ) ? '\'' . addslashes( $this->m_strUpdatedBy ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $strCreatedBy ) {
		$this->set( 'm_strCreatedBy', CStrings::strTrimDef( $strCreatedBy, 50, NULL, true ) );
	}

	public function getCreatedBy() {
		return $this->m_strCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_strCreatedBy ) ) ? '\'' . addslashes( $this->m_strCreatedBy ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ap_code_id, smart_device_type_id, name, make, model, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlSmartDeviceTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlMake() . ', ' .
						$this->sqlModel() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' smart_device_type_id = ' . $this->sqlSmartDeviceTypeId(). ',' ; } elseif( true == array_key_exists( 'SmartDeviceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' smart_device_type_id = ' . $this->sqlSmartDeviceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' make = ' . $this->sqlMake(). ',' ; } elseif( true == array_key_exists( 'Make', $this->getChangedColumns() ) ) { $strSql .= ' make = ' . $this->sqlMake() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' model = ' . $this->sqlModel(). ',' ; } elseif( true == array_key_exists( 'Model', $this->getChangedColumns() ) ) { $strSql .= ' model = ' . $this->sqlModel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ap_code_id' => $this->getApCodeId(),
			'smart_device_type_id' => $this->getSmartDeviceTypeId(),
			'name' => $this->getName(),
			'make' => $this->getMake(),
			'model' => $this->getModel(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>