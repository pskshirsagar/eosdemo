<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupAssetLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGroupAssetLocations extends CEosPluralBase {

	/**
	 * @return CPropertyGroupAssetLocation[]
	 */
	public static function fetchPropertyGroupAssetLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyGroupAssetLocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGroupAssetLocation
	 */
	public static function fetchPropertyGroupAssetLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyGroupAssetLocation', $objDatabase );
	}

	public static function fetchPropertyGroupAssetLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_group_asset_locations', $objDatabase );
	}

	public static function fetchPropertyGroupAssetLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssetLocation( sprintf( 'SELECT * FROM property_group_asset_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssetLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssetLocations( sprintf( 'SELECT * FROM property_group_asset_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssetLocationsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssetLocations( sprintf( 'SELECT * FROM property_group_asset_locations WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssetLocationsByAssetLocationIdByCid( $intAssetLocationId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssetLocations( sprintf( 'SELECT * FROM property_group_asset_locations WHERE asset_location_id = %d AND cid = %d', ( int ) $intAssetLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>