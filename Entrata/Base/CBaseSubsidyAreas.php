<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyAreas
 * Do not add any new functions to this class.
 */

class CBaseSubsidyAreas extends CEosPluralBase {

	/**
	 * @return CSubsidyArea[]
	 */
	public static function fetchSubsidyAreas( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyArea::class, $objDatabase );
	}

	/**
	 * @return CSubsidyArea
	 */
	public static function fetchSubsidyArea( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyArea::class, $objDatabase );
	}

	public static function fetchSubsidyAreaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_areas', $objDatabase );
	}

	public static function fetchSubsidyAreaById( $intId, $objDatabase ) {
		return self::fetchSubsidyArea( sprintf( 'SELECT * FROM subsidy_areas WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>