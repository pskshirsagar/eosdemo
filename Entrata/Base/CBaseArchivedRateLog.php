<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArchivedRateLog extends CEosSingularBase {

	const TABLE_NAME = 'public.archived_rate_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intUnitSpaceId;
	protected $m_intRateId;
	protected $m_intArCascadeId;
	protected $m_intArCascadeReferenceId;
	protected $m_intArOriginId;
	protected $m_intArOriginReferenceId;
	protected $m_intArPhaseId;
	protected $m_intArTriggerId;
	protected $m_intArCodeTypeId;
	protected $m_intArCodeId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intArFormulaId;
	protected $m_intArFormulaReferenceId;
	protected $m_intSpaceConfigurationId;
	protected $m_intCustomerRelationshipId;
	protected $m_intOccupancyTypeId;
	protected $m_intOccupancyTypeReferenceId;
	protected $m_intOptimalRateId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_intLeaseTermMonths;
	protected $m_intWindowStartDays;
	protected $m_intWindowEndDays;
	protected $m_strWindowStartDate;
	protected $m_strWindowEndDate;
	protected $m_fltRateAmount;
	protected $m_fltRateIncreaseIncrement;
	protected $m_fltMonthToMonthMultiplier;
	protected $m_fltMaxAmount;
	protected $m_fltMinAmount;
	protected $m_fltNormalizedAmount;
	protected $m_fltNormalizedPercent;
	protected $m_intRateIntervalStart;
	protected $m_intRateIntervalOccurances;
	protected $m_intRateIntervalOffset;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_strEffectiveThroughDate;
	protected $m_strDeactivationDate;
	protected $m_boolShowOnWebsite;
	protected $m_boolShowInEntrata;
	protected $m_boolIsRenewal;
	protected $m_boolIsOptional;
	protected $m_boolIsLocked;
	protected $m_boolIsPostMonthIgnored;
	protected $m_boolIsPostDateIgnored;
	protected $m_boolIsEffectiveDatetimeIgnored;
	protected $m_boolIsAllowed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOccupancyTypeObjectId;

	public function __construct() {
		parent::__construct();

		$this->m_intArPhaseId = '2';
		$this->m_intLeaseTermId = '0';
		$this->m_intLeaseStartWindowId = '0';
		$this->m_intSpaceConfigurationId = '0';
		$this->m_intCustomerRelationshipId = '0';
		$this->m_intOccupancyTypeId = '1';
		$this->m_intOccupancyTypeReferenceId = '0';
		$this->m_intLeaseTermMonths = '0';
		$this->m_intWindowStartDays = '0';
		$this->m_intWindowEndDays = '10000';
		$this->m_strWindowStartDate = '01/01/1970';
		$this->m_strWindowEndDate = '12/31/2099';
		$this->m_fltRateAmount = '0';
		$this->m_fltRateIncreaseIncrement = '0';
		$this->m_fltMonthToMonthMultiplier = '0';
		$this->m_fltNormalizedAmount = '0';
		$this->m_fltNormalizedPercent = '0';
		$this->m_intRateIntervalStart = '1';
		$this->m_intRateIntervalOccurances = '1';
		$this->m_intRateIntervalOffset = '0';
		$this->m_strEffectiveThroughDate = '12/31/2099';
		$this->m_strDeactivationDate = '12/31/2099';
		$this->m_boolShowOnWebsite = true;
		$this->m_boolShowInEntrata = true;
		$this->m_boolIsRenewal = false;
		$this->m_boolIsOptional = false;
		$this->m_boolIsLocked = false;
		$this->m_boolIsPostMonthIgnored = false;
		$this->m_boolIsPostDateIgnored = false;
		$this->m_boolIsEffectiveDatetimeIgnored = false;
		$this->m_boolIsAllowed = true;
		$this->m_intOccupancyTypeObjectId = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['rate_id'] ) && $boolDirectSet ) $this->set( 'm_intRateId', trim( $arrValues['rate_id'] ) ); elseif( isset( $arrValues['rate_id'] ) ) $this->setRateId( $arrValues['rate_id'] );
		if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeId', trim( $arrValues['ar_cascade_id'] ) ); elseif( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
		if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeReferenceId', trim( $arrValues['ar_cascade_reference_id'] ) ); elseif( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
		if( isset( $arrValues['ar_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intArPhaseId', trim( $arrValues['ar_phase_id'] ) ); elseif( isset( $arrValues['ar_phase_id'] ) ) $this->setArPhaseId( $arrValues['ar_phase_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['ar_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intArFormulaId', trim( $arrValues['ar_formula_id'] ) ); elseif( isset( $arrValues['ar_formula_id'] ) ) $this->setArFormulaId( $arrValues['ar_formula_id'] );
		if( isset( $arrValues['ar_formula_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArFormulaReferenceId', trim( $arrValues['ar_formula_reference_id'] ) ); elseif( isset( $arrValues['ar_formula_reference_id'] ) ) $this->setArFormulaReferenceId( $arrValues['ar_formula_reference_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipId', trim( $arrValues['customer_relationship_id'] ) ); elseif( isset( $arrValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrValues['customer_relationship_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['occupancy_type_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeReferenceId', trim( $arrValues['occupancy_type_reference_id'] ) ); elseif( isset( $arrValues['occupancy_type_reference_id'] ) ) $this->setOccupancyTypeReferenceId( $arrValues['occupancy_type_reference_id'] );
		if( isset( $arrValues['optimal_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intOptimalRateId', trim( $arrValues['optimal_rate_id'] ) ); elseif( isset( $arrValues['optimal_rate_id'] ) ) $this->setOptimalRateId( $arrValues['optimal_rate_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermMonths', trim( $arrValues['lease_term_months'] ) ); elseif( isset( $arrValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrValues['lease_term_months'] );
		if( isset( $arrValues['window_start_days'] ) && $boolDirectSet ) $this->set( 'm_intWindowStartDays', trim( $arrValues['window_start_days'] ) ); elseif( isset( $arrValues['window_start_days'] ) ) $this->setWindowStartDays( $arrValues['window_start_days'] );
		if( isset( $arrValues['window_end_days'] ) && $boolDirectSet ) $this->set( 'm_intWindowEndDays', trim( $arrValues['window_end_days'] ) ); elseif( isset( $arrValues['window_end_days'] ) ) $this->setWindowEndDays( $arrValues['window_end_days'] );
		if( isset( $arrValues['window_start_date'] ) && $boolDirectSet ) $this->set( 'm_strWindowStartDate', trim( $arrValues['window_start_date'] ) ); elseif( isset( $arrValues['window_start_date'] ) ) $this->setWindowStartDate( $arrValues['window_start_date'] );
		if( isset( $arrValues['window_end_date'] ) && $boolDirectSet ) $this->set( 'm_strWindowEndDate', trim( $arrValues['window_end_date'] ) ); elseif( isset( $arrValues['window_end_date'] ) ) $this->setWindowEndDate( $arrValues['window_end_date'] );
		if( isset( $arrValues['rate_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRateAmount', trim( $arrValues['rate_amount'] ) ); elseif( isset( $arrValues['rate_amount'] ) ) $this->setRateAmount( $arrValues['rate_amount'] );
		if( isset( $arrValues['rate_increase_increment'] ) && $boolDirectSet ) $this->set( 'm_fltRateIncreaseIncrement', trim( $arrValues['rate_increase_increment'] ) ); elseif( isset( $arrValues['rate_increase_increment'] ) ) $this->setRateIncreaseIncrement( $arrValues['rate_increase_increment'] );
		if( isset( $arrValues['month_to_month_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltMonthToMonthMultiplier', trim( $arrValues['month_to_month_multiplier'] ) ); elseif( isset( $arrValues['month_to_month_multiplier'] ) ) $this->setMonthToMonthMultiplier( $arrValues['month_to_month_multiplier'] );
		if( isset( $arrValues['max_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmount', trim( $arrValues['max_amount'] ) ); elseif( isset( $arrValues['max_amount'] ) ) $this->setMaxAmount( $arrValues['max_amount'] );
		if( isset( $arrValues['min_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmount', trim( $arrValues['min_amount'] ) ); elseif( isset( $arrValues['min_amount'] ) ) $this->setMinAmount( $arrValues['min_amount'] );
		if( isset( $arrValues['normalized_amount'] ) && $boolDirectSet ) $this->set( 'm_fltNormalizedAmount', trim( $arrValues['normalized_amount'] ) ); elseif( isset( $arrValues['normalized_amount'] ) ) $this->setNormalizedAmount( $arrValues['normalized_amount'] );
		if( isset( $arrValues['normalized_percent'] ) && $boolDirectSet ) $this->set( 'm_fltNormalizedPercent', trim( $arrValues['normalized_percent'] ) ); elseif( isset( $arrValues['normalized_percent'] ) ) $this->setNormalizedPercent( $arrValues['normalized_percent'] );
		if( isset( $arrValues['rate_interval_start'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalStart', trim( $arrValues['rate_interval_start'] ) ); elseif( isset( $arrValues['rate_interval_start'] ) ) $this->setRateIntervalStart( $arrValues['rate_interval_start'] );
		if( isset( $arrValues['rate_interval_occurances'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalOccurances', trim( $arrValues['rate_interval_occurances'] ) ); elseif( isset( $arrValues['rate_interval_occurances'] ) ) $this->setRateIntervalOccurances( $arrValues['rate_interval_occurances'] );
		if( isset( $arrValues['rate_interval_offset'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalOffset', trim( $arrValues['rate_interval_offset'] ) ); elseif( isset( $arrValues['rate_interval_offset'] ) ) $this->setRateIntervalOffset( $arrValues['rate_interval_offset'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['effective_through_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveThroughDate', trim( $arrValues['effective_through_date'] ) ); elseif( isset( $arrValues['effective_through_date'] ) ) $this->setEffectiveThroughDate( $arrValues['effective_through_date'] );
		if( isset( $arrValues['deactivation_date'] ) && $boolDirectSet ) $this->set( 'm_strDeactivationDate', trim( $arrValues['deactivation_date'] ) ); elseif( isset( $arrValues['deactivation_date'] ) ) $this->setDeactivationDate( $arrValues['deactivation_date'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['show_in_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolShowInEntrata', trim( stripcslashes( $arrValues['show_in_entrata'] ) ) ); elseif( isset( $arrValues['show_in_entrata'] ) ) $this->setShowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_entrata'] ) : $arrValues['show_in_entrata'] );
		if( isset( $arrValues['is_renewal'] ) && $boolDirectSet ) $this->set( 'm_boolIsRenewal', trim( stripcslashes( $arrValues['is_renewal'] ) ) ); elseif( isset( $arrValues['is_renewal'] ) ) $this->setIsRenewal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_renewal'] ) : $arrValues['is_renewal'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptional', trim( stripcslashes( $arrValues['is_optional'] ) ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optional'] ) : $arrValues['is_optional'] );
		if( isset( $arrValues['is_locked'] ) && $boolDirectSet ) $this->set( 'm_boolIsLocked', trim( stripcslashes( $arrValues['is_locked'] ) ) ); elseif( isset( $arrValues['is_locked'] ) ) $this->setIsLocked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_locked'] ) : $arrValues['is_locked'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostMonthIgnored', trim( stripcslashes( $arrValues['is_post_month_ignored'] ) ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_month_ignored'] ) : $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostDateIgnored', trim( stripcslashes( $arrValues['is_post_date_ignored'] ) ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_date_ignored'] ) : $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_effective_datetime_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsEffectiveDatetimeIgnored', trim( stripcslashes( $arrValues['is_effective_datetime_ignored'] ) ) ); elseif( isset( $arrValues['is_effective_datetime_ignored'] ) ) $this->setIsEffectiveDatetimeIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_effective_datetime_ignored'] ) : $arrValues['is_effective_datetime_ignored'] );
		if( isset( $arrValues['is_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowed', trim( stripcslashes( $arrValues['is_allowed'] ) ) ); elseif( isset( $arrValues['is_allowed'] ) ) $this->setIsAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allowed'] ) : $arrValues['is_allowed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['occupancy_type_object_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeObjectId', trim( $arrValues['occupancy_type_object_id'] ) ); elseif( isset( $arrValues['occupancy_type_object_id'] ) ) $this->setOccupancyTypeObjectId( $arrValues['occupancy_type_object_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setRateId( $intRateId ) {
		$this->set( 'm_intRateId', CStrings::strToIntDef( $intRateId, NULL, false ) );
	}

	public function getRateId() {
		return $this->m_intRateId;
	}

	public function sqlRateId() {
		return ( true == isset( $this->m_intRateId ) ) ? ( string ) $this->m_intRateId : 'NULL';
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->set( 'm_intArCascadeId', CStrings::strToIntDef( $intArCascadeId, NULL, false ) );
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function sqlArCascadeId() {
		return ( true == isset( $this->m_intArCascadeId ) ) ? ( string ) $this->m_intArCascadeId : 'NULL';
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->set( 'm_intArCascadeReferenceId', CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false ) );
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function sqlArCascadeReferenceId() {
		return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? ( string ) $this->m_intArCascadeReferenceId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function sqlArOriginReferenceId() {
		return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
	}

	public function setArPhaseId( $intArPhaseId ) {
		$this->set( 'm_intArPhaseId', CStrings::strToIntDef( $intArPhaseId, NULL, false ) );
	}

	public function getArPhaseId() {
		return $this->m_intArPhaseId;
	}

	public function sqlArPhaseId() {
		return ( true == isset( $this->m_intArPhaseId ) ) ? ( string ) $this->m_intArPhaseId : '2';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function sqlArCodeTypeId() {
		return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : '0';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : '0';
	}

	public function setArFormulaId( $intArFormulaId ) {
		$this->set( 'm_intArFormulaId', CStrings::strToIntDef( $intArFormulaId, NULL, false ) );
	}

	public function getArFormulaId() {
		return $this->m_intArFormulaId;
	}

	public function sqlArFormulaId() {
		return ( true == isset( $this->m_intArFormulaId ) ) ? ( string ) $this->m_intArFormulaId : 'NULL';
	}

	public function setArFormulaReferenceId( $intArFormulaReferenceId ) {
		$this->set( 'm_intArFormulaReferenceId', CStrings::strToIntDef( $intArFormulaReferenceId, NULL, false ) );
	}

	public function getArFormulaReferenceId() {
		return $this->m_intArFormulaReferenceId;
	}

	public function sqlArFormulaReferenceId() {
		return ( true == isset( $this->m_intArFormulaReferenceId ) ) ? ( string ) $this->m_intArFormulaReferenceId : 'NULL';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : '0';
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->set( 'm_intCustomerRelationshipId', CStrings::strToIntDef( $intCustomerRelationshipId, NULL, false ) );
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function sqlCustomerRelationshipId() {
		return ( true == isset( $this->m_intCustomerRelationshipId ) ) ? ( string ) $this->m_intCustomerRelationshipId : '0';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setOccupancyTypeReferenceId( $intOccupancyTypeReferenceId ) {
		$this->set( 'm_intOccupancyTypeReferenceId', CStrings::strToIntDef( $intOccupancyTypeReferenceId, NULL, false ) );
	}

	public function getOccupancyTypeReferenceId() {
		return $this->m_intOccupancyTypeReferenceId;
	}

	public function sqlOccupancyTypeReferenceId() {
		return ( true == isset( $this->m_intOccupancyTypeReferenceId ) ) ? ( string ) $this->m_intOccupancyTypeReferenceId : '0';
	}

	public function setOptimalRateId( $intOptimalRateId ) {
		$this->set( 'm_intOptimalRateId', CStrings::strToIntDef( $intOptimalRateId, NULL, false ) );
	}

	public function getOptimalRateId() {
		return $this->m_intOptimalRateId;
	}

	public function sqlOptimalRateId() {
		return ( true == isset( $this->m_intOptimalRateId ) ) ? ( string ) $this->m_intOptimalRateId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->set( 'm_intLeaseTermMonths', CStrings::strToIntDef( $intLeaseTermMonths, NULL, false ) );
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	public function sqlLeaseTermMonths() {
		return ( true == isset( $this->m_intLeaseTermMonths ) ) ? ( string ) $this->m_intLeaseTermMonths : '0';
	}

	public function setWindowStartDays( $intWindowStartDays ) {
		$this->set( 'm_intWindowStartDays', CStrings::strToIntDef( $intWindowStartDays, NULL, false ) );
	}

	public function getWindowStartDays() {
		return $this->m_intWindowStartDays;
	}

	public function sqlWindowStartDays() {
		return ( true == isset( $this->m_intWindowStartDays ) ) ? ( string ) $this->m_intWindowStartDays : '0';
	}

	public function setWindowEndDays( $intWindowEndDays ) {
		$this->set( 'm_intWindowEndDays', CStrings::strToIntDef( $intWindowEndDays, NULL, false ) );
	}

	public function getWindowEndDays() {
		return $this->m_intWindowEndDays;
	}

	public function sqlWindowEndDays() {
		return ( true == isset( $this->m_intWindowEndDays ) ) ? ( string ) $this->m_intWindowEndDays : '10000';
	}

	public function setWindowStartDate( $strWindowStartDate ) {
		$this->set( 'm_strWindowStartDate', CStrings::strTrimDef( $strWindowStartDate, -1, NULL, true ) );
	}

	public function getWindowStartDate() {
		return $this->m_strWindowStartDate;
	}

	public function sqlWindowStartDate() {
		return ( true == isset( $this->m_strWindowStartDate ) ) ? '\'' . $this->m_strWindowStartDate . '\'' : 'NOW()';
	}

	public function setWindowEndDate( $strWindowEndDate ) {
		$this->set( 'm_strWindowEndDate', CStrings::strTrimDef( $strWindowEndDate, -1, NULL, true ) );
	}

	public function getWindowEndDate() {
		return $this->m_strWindowEndDate;
	}

	public function sqlWindowEndDate() {
		return ( true == isset( $this->m_strWindowEndDate ) ) ? '\'' . $this->m_strWindowEndDate . '\'' : 'NOW()';
	}

	public function setRateAmount( $fltRateAmount ) {
		$this->set( 'm_fltRateAmount', CStrings::strToFloatDef( $fltRateAmount, NULL, false, 2 ) );
	}

	public function getRateAmount() {
		return $this->m_fltRateAmount;
	}

	public function sqlRateAmount() {
		return ( true == isset( $this->m_fltRateAmount ) ) ? ( string ) $this->m_fltRateAmount : '0';
	}

	public function setRateIncreaseIncrement( $fltRateIncreaseIncrement ) {
		$this->set( 'm_fltRateIncreaseIncrement', CStrings::strToFloatDef( $fltRateIncreaseIncrement, NULL, false, 6 ) );
	}

	public function getRateIncreaseIncrement() {
		return $this->m_fltRateIncreaseIncrement;
	}

	public function sqlRateIncreaseIncrement() {
		return ( true == isset( $this->m_fltRateIncreaseIncrement ) ) ? ( string ) $this->m_fltRateIncreaseIncrement : '0';
	}

	public function setMonthToMonthMultiplier( $fltMonthToMonthMultiplier ) {
		$this->set( 'm_fltMonthToMonthMultiplier', CStrings::strToFloatDef( $fltMonthToMonthMultiplier, NULL, false, 4 ) );
	}

	public function getMonthToMonthMultiplier() {
		return $this->m_fltMonthToMonthMultiplier;
	}

	public function sqlMonthToMonthMultiplier() {
		return ( true == isset( $this->m_fltMonthToMonthMultiplier ) ) ? ( string ) $this->m_fltMonthToMonthMultiplier : '0';
	}

	public function setMaxAmount( $fltMaxAmount ) {
		$this->set( 'm_fltMaxAmount', CStrings::strToFloatDef( $fltMaxAmount, NULL, false, 2 ) );
	}

	public function getMaxAmount() {
		return $this->m_fltMaxAmount;
	}

	public function sqlMaxAmount() {
		return ( true == isset( $this->m_fltMaxAmount ) ) ? ( string ) $this->m_fltMaxAmount : 'NULL';
	}

	public function setMinAmount( $fltMinAmount ) {
		$this->set( 'm_fltMinAmount', CStrings::strToFloatDef( $fltMinAmount, NULL, false, 2 ) );
	}

	public function getMinAmount() {
		return $this->m_fltMinAmount;
	}

	public function sqlMinAmount() {
		return ( true == isset( $this->m_fltMinAmount ) ) ? ( string ) $this->m_fltMinAmount : 'NULL';
	}

	public function setNormalizedAmount( $fltNormalizedAmount ) {
		$this->set( 'm_fltNormalizedAmount', CStrings::strToFloatDef( $fltNormalizedAmount, NULL, false, 2 ) );
	}

	public function getNormalizedAmount() {
		return $this->m_fltNormalizedAmount;
	}

	public function sqlNormalizedAmount() {
		return ( true == isset( $this->m_fltNormalizedAmount ) ) ? ( string ) $this->m_fltNormalizedAmount : '0';
	}

	public function setNormalizedPercent( $fltNormalizedPercent ) {
		$this->set( 'm_fltNormalizedPercent', CStrings::strToFloatDef( $fltNormalizedPercent, NULL, false, 2 ) );
	}

	public function getNormalizedPercent() {
		return $this->m_fltNormalizedPercent;
	}

	public function sqlNormalizedPercent() {
		return ( true == isset( $this->m_fltNormalizedPercent ) ) ? ( string ) $this->m_fltNormalizedPercent : '0';
	}

	public function setRateIntervalStart( $intRateIntervalStart ) {
		$this->set( 'm_intRateIntervalStart', CStrings::strToIntDef( $intRateIntervalStart, NULL, false ) );
	}

	public function getRateIntervalStart() {
		return $this->m_intRateIntervalStart;
	}

	public function sqlRateIntervalStart() {
		return ( true == isset( $this->m_intRateIntervalStart ) ) ? ( string ) $this->m_intRateIntervalStart : '1';
	}

	public function setRateIntervalOccurances( $intRateIntervalOccurances ) {
		$this->set( 'm_intRateIntervalOccurances', CStrings::strToIntDef( $intRateIntervalOccurances, NULL, false ) );
	}

	public function getRateIntervalOccurances() {
		return $this->m_intRateIntervalOccurances;
	}

	public function sqlRateIntervalOccurances() {
		return ( true == isset( $this->m_intRateIntervalOccurances ) ) ? ( string ) $this->m_intRateIntervalOccurances : '1';
	}

	public function setRateIntervalOffset( $intRateIntervalOffset ) {
		$this->set( 'm_intRateIntervalOffset', CStrings::strToIntDef( $intRateIntervalOffset, NULL, false ) );
	}

	public function getRateIntervalOffset() {
		return $this->m_intRateIntervalOffset;
	}

	public function sqlRateIntervalOffset() {
		return ( true == isset( $this->m_intRateIntervalOffset ) ) ? ( string ) $this->m_intRateIntervalOffset : '0';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setEffectiveThroughDate( $strEffectiveThroughDate ) {
		$this->set( 'm_strEffectiveThroughDate', CStrings::strTrimDef( $strEffectiveThroughDate, -1, NULL, true ) );
	}

	public function getEffectiveThroughDate() {
		return $this->m_strEffectiveThroughDate;
	}

	public function sqlEffectiveThroughDate() {
		return ( true == isset( $this->m_strEffectiveThroughDate ) ) ? '\'' . $this->m_strEffectiveThroughDate . '\'' : 'NOW()';
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->set( 'm_strDeactivationDate', CStrings::strTrimDef( $strDeactivationDate, -1, NULL, true ) );
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function sqlDeactivationDate() {
		return ( true == isset( $this->m_strDeactivationDate ) ) ? '\'' . $this->m_strDeactivationDate . '\'' : 'NOW()';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInEntrata( $boolShowInEntrata ) {
		$this->set( 'm_boolShowInEntrata', CStrings::strToBool( $boolShowInEntrata ) );
	}

	public function getShowInEntrata() {
		return $this->m_boolShowInEntrata;
	}

	public function sqlShowInEntrata() {
		return ( true == isset( $this->m_boolShowInEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRenewal( $boolIsRenewal ) {
		$this->set( 'm_boolIsRenewal', CStrings::strToBool( $boolIsRenewal ) );
	}

	public function getIsRenewal() {
		return $this->m_boolIsRenewal;
	}

	public function sqlIsRenewal() {
		return ( true == isset( $this->m_boolIsRenewal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRenewal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->set( 'm_boolIsOptional', CStrings::strToBool( $boolIsOptional ) );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_boolIsOptional ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptional ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLocked( $boolIsLocked ) {
		$this->set( 'm_boolIsLocked', CStrings::strToBool( $boolIsLocked ) );
	}

	public function getIsLocked() {
		return $this->m_boolIsLocked;
	}

	public function sqlIsLocked() {
		return ( true == isset( $this->m_boolIsLocked ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLocked ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostMonthIgnored( $boolIsPostMonthIgnored ) {
		$this->set( 'm_boolIsPostMonthIgnored', CStrings::strToBool( $boolIsPostMonthIgnored ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_boolIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_boolIsPostMonthIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostMonthIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostDateIgnored( $boolIsPostDateIgnored ) {
		$this->set( 'm_boolIsPostDateIgnored', CStrings::strToBool( $boolIsPostDateIgnored ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_boolIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_boolIsPostDateIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostDateIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEffectiveDatetimeIgnored( $boolIsEffectiveDatetimeIgnored ) {
		$this->set( 'm_boolIsEffectiveDatetimeIgnored', CStrings::strToBool( $boolIsEffectiveDatetimeIgnored ) );
	}

	public function getIsEffectiveDatetimeIgnored() {
		return $this->m_boolIsEffectiveDatetimeIgnored;
	}

	public function sqlIsEffectiveDatetimeIgnored() {
		return ( true == isset( $this->m_boolIsEffectiveDatetimeIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEffectiveDatetimeIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAllowed( $boolIsAllowed ) {
		$this->set( 'm_boolIsAllowed', CStrings::strToBool( $boolIsAllowed ) );
	}

	public function getIsAllowed() {
		return $this->m_boolIsAllowed;
	}

	public function sqlIsAllowed() {
		return ( true == isset( $this->m_boolIsAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOccupancyTypeObjectId( $intOccupancyTypeObjectId ) {
		$this->set( 'm_intOccupancyTypeObjectId', CStrings::strToIntDef( $intOccupancyTypeObjectId, NULL, false ) );
	}

	public function getOccupancyTypeObjectId() {
		return $this->m_intOccupancyTypeObjectId;
	}

	public function sqlOccupancyTypeObjectId() {
		return ( true == isset( $this->m_intOccupancyTypeObjectId ) ) ? ( string ) $this->m_intOccupancyTypeObjectId : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, unit_space_id, rate_id, ar_cascade_id, ar_cascade_reference_id, ar_origin_id, ar_origin_reference_id, ar_phase_id, ar_trigger_id, ar_code_type_id, ar_code_id, lease_term_id, lease_start_window_id, ar_formula_id, ar_formula_reference_id, space_configuration_id, customer_relationship_id, occupancy_type_id, occupancy_type_reference_id, optimal_rate_id, period_id, reporting_period_id, effective_period_id, original_period_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, lease_term_months, window_start_days, window_end_days, window_start_date, window_end_date, rate_amount, rate_increase_increment, month_to_month_multiplier, max_amount, min_amount, normalized_amount, normalized_percent, rate_interval_start, rate_interval_occurances, rate_interval_offset, log_datetime, effective_date, effective_through_date, deactivation_date, show_on_website, show_in_entrata, is_renewal, is_optional, is_locked, is_post_month_ignored, is_post_date_ignored, is_effective_datetime_ignored, is_allowed, updated_by, updated_on, created_by, created_on, occupancy_type_object_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlRateId() . ', ' .
						$this->sqlArCascadeId() . ', ' .
						$this->sqlArCascadeReferenceId() . ', ' .
						$this->sqlArOriginId() . ', ' .
						$this->sqlArOriginReferenceId() . ', ' .
						$this->sqlArPhaseId() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlArCodeTypeId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlArFormulaId() . ', ' .
						$this->sqlArFormulaReferenceId() . ', ' .
						$this->sqlSpaceConfigurationId() . ', ' .
						$this->sqlCustomerRelationshipId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlOccupancyTypeReferenceId() . ', ' .
						$this->sqlOptimalRateId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlLeaseTermMonths() . ', ' .
						$this->sqlWindowStartDays() . ', ' .
						$this->sqlWindowEndDays() . ', ' .
						$this->sqlWindowStartDate() . ', ' .
						$this->sqlWindowEndDate() . ', ' .
						$this->sqlRateAmount() . ', ' .
						$this->sqlRateIncreaseIncrement() . ', ' .
						$this->sqlMonthToMonthMultiplier() . ', ' .
						$this->sqlMaxAmount() . ', ' .
						$this->sqlMinAmount() . ', ' .
						$this->sqlNormalizedAmount() . ', ' .
						$this->sqlNormalizedPercent() . ', ' .
						$this->sqlRateIntervalStart() . ', ' .
						$this->sqlRateIntervalOccurances() . ', ' .
						$this->sqlRateIntervalOffset() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlEffectiveThroughDate() . ', ' .
						$this->sqlDeactivationDate() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlShowInEntrata() . ', ' .
						$this->sqlIsRenewal() . ', ' .
						$this->sqlIsOptional() . ', ' .
						$this->sqlIsLocked() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsEffectiveDatetimeIgnored() . ', ' .
						$this->sqlIsAllowed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOccupancyTypeObjectId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_id = ' . $this->sqlRateId(). ',' ; } elseif( true == array_key_exists( 'RateId', $this->getChangedColumns() ) ) { $strSql .= ' rate_id = ' . $this->sqlRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId(). ',' ; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_phase_id = ' . $this->sqlArPhaseId(). ',' ; } elseif( true == array_key_exists( 'ArPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' ar_phase_id = ' . $this->sqlArPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_formula_id = ' . $this->sqlArFormulaId(). ',' ; } elseif( true == array_key_exists( 'ArFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' ar_formula_id = ' . $this->sqlArFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_formula_reference_id = ' . $this->sqlArFormulaReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArFormulaReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_formula_reference_id = ' . $this->sqlArFormulaReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId(). ',' ; } elseif( true == array_key_exists( 'CustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_reference_id = ' . $this->sqlOccupancyTypeReferenceId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_reference_id = ' . $this->sqlOccupancyTypeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_rate_id = ' . $this->sqlOptimalRateId(). ',' ; } elseif( true == array_key_exists( 'OptimalRateId', $this->getChangedColumns() ) ) { $strSql .= ' optimal_rate_id = ' . $this->sqlOptimalRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths(). ',' ; } elseif( true == array_key_exists( 'LeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_start_days = ' . $this->sqlWindowStartDays(). ',' ; } elseif( true == array_key_exists( 'WindowStartDays', $this->getChangedColumns() ) ) { $strSql .= ' window_start_days = ' . $this->sqlWindowStartDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_end_days = ' . $this->sqlWindowEndDays(). ',' ; } elseif( true == array_key_exists( 'WindowEndDays', $this->getChangedColumns() ) ) { $strSql .= ' window_end_days = ' . $this->sqlWindowEndDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_start_date = ' . $this->sqlWindowStartDate(). ',' ; } elseif( true == array_key_exists( 'WindowStartDate', $this->getChangedColumns() ) ) { $strSql .= ' window_start_date = ' . $this->sqlWindowStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_end_date = ' . $this->sqlWindowEndDate(). ',' ; } elseif( true == array_key_exists( 'WindowEndDate', $this->getChangedColumns() ) ) { $strSql .= ' window_end_date = ' . $this->sqlWindowEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_amount = ' . $this->sqlRateAmount(). ',' ; } elseif( true == array_key_exists( 'RateAmount', $this->getChangedColumns() ) ) { $strSql .= ' rate_amount = ' . $this->sqlRateAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_increase_increment = ' . $this->sqlRateIncreaseIncrement(). ',' ; } elseif( true == array_key_exists( 'RateIncreaseIncrement', $this->getChangedColumns() ) ) { $strSql .= ' rate_increase_increment = ' . $this->sqlRateIncreaseIncrement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_multiplier = ' . $this->sqlMonthToMonthMultiplier(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_multiplier = ' . $this->sqlMonthToMonthMultiplier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amount = ' . $this->sqlMaxAmount(). ',' ; } elseif( true == array_key_exists( 'MaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_amount = ' . $this->sqlMaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amount = ' . $this->sqlMinAmount(). ',' ; } elseif( true == array_key_exists( 'MinAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_amount = ' . $this->sqlMinAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' normalized_amount = ' . $this->sqlNormalizedAmount(). ',' ; } elseif( true == array_key_exists( 'NormalizedAmount', $this->getChangedColumns() ) ) { $strSql .= ' normalized_amount = ' . $this->sqlNormalizedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' normalized_percent = ' . $this->sqlNormalizedPercent(). ',' ; } elseif( true == array_key_exists( 'NormalizedPercent', $this->getChangedColumns() ) ) { $strSql .= ' normalized_percent = ' . $this->sqlNormalizedPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_start = ' . $this->sqlRateIntervalStart(). ',' ; } elseif( true == array_key_exists( 'RateIntervalStart', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_start = ' . $this->sqlRateIntervalStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_occurances = ' . $this->sqlRateIntervalOccurances(). ',' ; } elseif( true == array_key_exists( 'RateIntervalOccurances', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_occurances = ' . $this->sqlRateIntervalOccurances() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset(). ',' ; } elseif( true == array_key_exists( 'RateIntervalOffset', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveThroughDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate(). ',' ; } elseif( true == array_key_exists( 'DeactivationDate', $this->getChangedColumns() ) ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata(). ',' ; } elseif( true == array_key_exists( 'ShowInEntrata', $this->getChangedColumns() ) ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal(). ',' ; } elseif( true == array_key_exists( 'IsRenewal', $this->getChangedColumns() ) ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked(). ',' ; } elseif( true == array_key_exists( 'IsLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_effective_datetime_ignored = ' . $this->sqlIsEffectiveDatetimeIgnored(). ',' ; } elseif( true == array_key_exists( 'IsEffectiveDatetimeIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_effective_datetime_ignored = ' . $this->sqlIsEffectiveDatetimeIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed = ' . $this->sqlIsAllowed(). ',' ; } elseif( true == array_key_exists( 'IsAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_allowed = ' . $this->sqlIsAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_object_id = ' . $this->sqlOccupancyTypeObjectId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeObjectId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_object_id = ' . $this->sqlOccupancyTypeObjectId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'rate_id' => $this->getRateId(),
			'ar_cascade_id' => $this->getArCascadeId(),
			'ar_cascade_reference_id' => $this->getArCascadeReferenceId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_origin_reference_id' => $this->getArOriginReferenceId(),
			'ar_phase_id' => $this->getArPhaseId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'ar_code_type_id' => $this->getArCodeTypeId(),
			'ar_code_id' => $this->getArCodeId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'ar_formula_id' => $this->getArFormulaId(),
			'ar_formula_reference_id' => $this->getArFormulaReferenceId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'customer_relationship_id' => $this->getCustomerRelationshipId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'occupancy_type_reference_id' => $this->getOccupancyTypeReferenceId(),
			'optimal_rate_id' => $this->getOptimalRateId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'lease_term_months' => $this->getLeaseTermMonths(),
			'window_start_days' => $this->getWindowStartDays(),
			'window_end_days' => $this->getWindowEndDays(),
			'window_start_date' => $this->getWindowStartDate(),
			'window_end_date' => $this->getWindowEndDate(),
			'rate_amount' => $this->getRateAmount(),
			'rate_increase_increment' => $this->getRateIncreaseIncrement(),
			'month_to_month_multiplier' => $this->getMonthToMonthMultiplier(),
			'max_amount' => $this->getMaxAmount(),
			'min_amount' => $this->getMinAmount(),
			'normalized_amount' => $this->getNormalizedAmount(),
			'normalized_percent' => $this->getNormalizedPercent(),
			'rate_interval_start' => $this->getRateIntervalStart(),
			'rate_interval_occurances' => $this->getRateIntervalOccurances(),
			'rate_interval_offset' => $this->getRateIntervalOffset(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'effective_through_date' => $this->getEffectiveThroughDate(),
			'deactivation_date' => $this->getDeactivationDate(),
			'show_on_website' => $this->getShowOnWebsite(),
			'show_in_entrata' => $this->getShowInEntrata(),
			'is_renewal' => $this->getIsRenewal(),
			'is_optional' => $this->getIsOptional(),
			'is_locked' => $this->getIsLocked(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_effective_datetime_ignored' => $this->getIsEffectiveDatetimeIgnored(),
			'is_allowed' => $this->getIsAllowed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'occupancy_type_object_id' => $this->getOccupancyTypeObjectId()
		);
	}

}
?>