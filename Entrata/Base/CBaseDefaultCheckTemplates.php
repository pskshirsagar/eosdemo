<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCheckTemplates
 * Do not add any new functions to this class.
 */

class CBaseDefaultCheckTemplates extends CEosPluralBase {

	/**
	 * @return CDefaultCheckTemplate[]
	 */
	public static function fetchDefaultCheckTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCheckTemplate::class, $objDatabase );
	}

	/**
	 * @return CDefaultCheckTemplate
	 */
	public static function fetchDefaultCheckTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCheckTemplate::class, $objDatabase );
	}

	public static function fetchDefaultCheckTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_check_templates', $objDatabase );
	}

	public static function fetchDefaultCheckTemplateById( $intId, $objDatabase ) {
		return self::fetchDefaultCheckTemplate( sprintf( 'SELECT * FROM default_check_templates WHERE id = %d', $intId ), $objDatabase );
	}

}
?>