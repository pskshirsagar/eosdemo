<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeaderLogs extends CEosPluralBase {

	/**
	 * @return CGlHeaderLog[]
	 */
	public static function fetchGlHeaderLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlHeaderLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlHeaderLog
	 */
	public static function fetchGlHeaderLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlHeaderLog', $objDatabase );
	}

	public static function fetchGlHeaderLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_logs', $objDatabase );
	}

	public static function fetchGlHeaderLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLog( sprintf( 'SELECT * FROM gl_header_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByCid( $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_header_id = %d AND cid = %d', ( int ) $intGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlHeaderTypeIdByCid( $intGlHeaderTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_header_type_id = %d AND cid = %d', ( int ) $intGlHeaderTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_transaction_type_id = %d AND cid = %d', ( int ) $intGlTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlHeaderStatusTypeIdByCid( $intGlHeaderStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_header_status_type_id = %d AND cid = %d', ( int ) $intGlHeaderStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlBookIdByCid( $intGlBookId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_book_id = %d AND cid = %d', ( int ) $intGlBookId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByOffsettingGlHeaderIdByCid( $intOffsettingGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE offsetting_gl_header_id = %d AND cid = %d', ( int ) $intOffsettingGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByTemplateGlHeaderIdByCid( $intTemplateGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE template_gl_header_id = %d AND cid = %d', ( int ) $intTemplateGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByClosePeriodIdByCid( $intClosePeriodId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE close_period_id = %d AND cid = %d', ( int ) $intClosePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkPropertyIdByCid( $intBulkPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_property_id = %d AND cid = %d', ( int ) $intBulkPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkPropertyUnitIdByCid( $intBulkPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_property_unit_id = %d AND cid = %d', ( int ) $intBulkPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkCompanyDepartmentIdByCid( $intBulkCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_company_department_id = %d AND cid = %d', ( int ) $intBulkCompanyDepartmentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkGlDimensionIdByCid( $intBulkGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_gl_dimension_id = %d AND cid = %d', ( int ) $intBulkGlDimensionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkJobPhaseApHeaderIdByCid( $intBulkJobPhaseApHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_job_phase_ap_header_id = %d AND cid = %d', ( int ) $intBulkJobPhaseApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkContractApHeaderIdByCid( $intBulkContractApHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_contract_ap_header_id = %d AND cid = %d', ( int ) $intBulkContractApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByBulkPropertyBuildingIdByCid( $intBulkPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE bulk_property_building_id = %d AND cid = %d', ( int ) $intBulkPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderLogsByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderLogs( sprintf( 'SELECT * FROM gl_header_logs WHERE gl_header_schedule_id = %d AND cid = %d', ( int ) $intGlHeaderScheduleId, ( int ) $intCid ), $objDatabase );
	}

}
?>