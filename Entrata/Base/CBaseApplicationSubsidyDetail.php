<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationSubsidyDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.application_subsidy_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intPrimaryApplicantId;
	protected $m_intPreviousSubsidyContractTypeId;
	protected $m_intMoveInIncomeLevelTypeId;
	protected $m_intCurrentIncomeLevelTypeId;
	protected $m_intSubsidyWaiverTypeId;
	protected $m_intSubsidyCitizenshipEligibilityTypeId;
	protected $m_intSubsidyMinRentExemptionTypeId;
	protected $m_intSubsidyIncomeExceptionTypeId;
	protected $m_intSubsidyDisplacementStatusTypeId;
	protected $m_intSubsidyPreviousHousingTypeId;
	protected $m_intAnticipatedAdditionAdoption;
	protected $m_intAnticipatedAdditionPregnancy;
	protected $m_intAnticipatedAdditionFosterChild;
	protected $m_intInitialHouseholdSize;
	protected $m_fltInitialTotalAnnualIncome;
	protected $m_fltInitialPercentOfMedianIncome;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSubsidyCitizenshipEligibilityTypeId = '1';
		$this->m_intAnticipatedAdditionAdoption = '0';
		$this->m_intAnticipatedAdditionPregnancy = '0';
		$this->m_intAnticipatedAdditionFosterChild = '0';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['primary_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryApplicantId', trim( $arrValues['primary_applicant_id'] ) ); elseif( isset( $arrValues['primary_applicant_id'] ) ) $this->setPrimaryApplicantId( $arrValues['primary_applicant_id'] );
		if( isset( $arrValues['previous_subsidy_contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousSubsidyContractTypeId', trim( $arrValues['previous_subsidy_contract_type_id'] ) ); elseif( isset( $arrValues['previous_subsidy_contract_type_id'] ) ) $this->setPreviousSubsidyContractTypeId( $arrValues['previous_subsidy_contract_type_id'] );
		if( isset( $arrValues['move_in_income_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInIncomeLevelTypeId', trim( $arrValues['move_in_income_level_type_id'] ) ); elseif( isset( $arrValues['move_in_income_level_type_id'] ) ) $this->setMoveInIncomeLevelTypeId( $arrValues['move_in_income_level_type_id'] );
		if( isset( $arrValues['current_income_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrentIncomeLevelTypeId', trim( $arrValues['current_income_level_type_id'] ) ); elseif( isset( $arrValues['current_income_level_type_id'] ) ) $this->setCurrentIncomeLevelTypeId( $arrValues['current_income_level_type_id'] );
		if( isset( $arrValues['subsidy_waiver_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyWaiverTypeId', trim( $arrValues['subsidy_waiver_type_id'] ) ); elseif( isset( $arrValues['subsidy_waiver_type_id'] ) ) $this->setSubsidyWaiverTypeId( $arrValues['subsidy_waiver_type_id'] );
		if( isset( $arrValues['subsidy_citizenship_eligibility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCitizenshipEligibilityTypeId', trim( $arrValues['subsidy_citizenship_eligibility_type_id'] ) ); elseif( isset( $arrValues['subsidy_citizenship_eligibility_type_id'] ) ) $this->setSubsidyCitizenshipEligibilityTypeId( $arrValues['subsidy_citizenship_eligibility_type_id'] );
		if( isset( $arrValues['subsidy_min_rent_exemption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMinRentExemptionTypeId', trim( $arrValues['subsidy_min_rent_exemption_type_id'] ) ); elseif( isset( $arrValues['subsidy_min_rent_exemption_type_id'] ) ) $this->setSubsidyMinRentExemptionTypeId( $arrValues['subsidy_min_rent_exemption_type_id'] );
		if( isset( $arrValues['subsidy_income_exception_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeExceptionTypeId', trim( $arrValues['subsidy_income_exception_type_id'] ) ); elseif( isset( $arrValues['subsidy_income_exception_type_id'] ) ) $this->setSubsidyIncomeExceptionTypeId( $arrValues['subsidy_income_exception_type_id'] );
		if( isset( $arrValues['subsidy_displacement_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyDisplacementStatusTypeId', trim( $arrValues['subsidy_displacement_status_type_id'] ) ); elseif( isset( $arrValues['subsidy_displacement_status_type_id'] ) ) $this->setSubsidyDisplacementStatusTypeId( $arrValues['subsidy_displacement_status_type_id'] );
		if( isset( $arrValues['subsidy_previous_housing_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyPreviousHousingTypeId', trim( $arrValues['subsidy_previous_housing_type_id'] ) ); elseif( isset( $arrValues['subsidy_previous_housing_type_id'] ) ) $this->setSubsidyPreviousHousingTypeId( $arrValues['subsidy_previous_housing_type_id'] );
		if( isset( $arrValues['anticipated_addition_adoption'] ) && $boolDirectSet ) $this->set( 'm_intAnticipatedAdditionAdoption', trim( $arrValues['anticipated_addition_adoption'] ) ); elseif( isset( $arrValues['anticipated_addition_adoption'] ) ) $this->setAnticipatedAdditionAdoption( $arrValues['anticipated_addition_adoption'] );
		if( isset( $arrValues['anticipated_addition_pregnancy'] ) && $boolDirectSet ) $this->set( 'm_intAnticipatedAdditionPregnancy', trim( $arrValues['anticipated_addition_pregnancy'] ) ); elseif( isset( $arrValues['anticipated_addition_pregnancy'] ) ) $this->setAnticipatedAdditionPregnancy( $arrValues['anticipated_addition_pregnancy'] );
		if( isset( $arrValues['anticipated_addition_foster_child'] ) && $boolDirectSet ) $this->set( 'm_intAnticipatedAdditionFosterChild', trim( $arrValues['anticipated_addition_foster_child'] ) ); elseif( isset( $arrValues['anticipated_addition_foster_child'] ) ) $this->setAnticipatedAdditionFosterChild( $arrValues['anticipated_addition_foster_child'] );
		if( isset( $arrValues['initial_household_size'] ) && $boolDirectSet ) $this->set( 'm_intInitialHouseholdSize', trim( $arrValues['initial_household_size'] ) ); elseif( isset( $arrValues['initial_household_size'] ) ) $this->setInitialHouseholdSize( $arrValues['initial_household_size'] );
		if( isset( $arrValues['initial_total_annual_income'] ) && $boolDirectSet ) $this->set( 'm_fltInitialTotalAnnualIncome', trim( $arrValues['initial_total_annual_income'] ) ); elseif( isset( $arrValues['initial_total_annual_income'] ) ) $this->setInitialTotalAnnualIncome( $arrValues['initial_total_annual_income'] );
		if( isset( $arrValues['initial_percent_of_median_income'] ) && $boolDirectSet ) $this->set( 'm_fltInitialPercentOfMedianIncome', trim( $arrValues['initial_percent_of_median_income'] ) ); elseif( isset( $arrValues['initial_percent_of_median_income'] ) ) $this->setInitialPercentOfMedianIncome( $arrValues['initial_percent_of_median_income'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setPrimaryApplicantId( $intPrimaryApplicantId ) {
		$this->set( 'm_intPrimaryApplicantId', CStrings::strToIntDef( $intPrimaryApplicantId, NULL, false ) );
	}

	public function getPrimaryApplicantId() {
		return $this->m_intPrimaryApplicantId;
	}

	public function sqlPrimaryApplicantId() {
		return ( true == isset( $this->m_intPrimaryApplicantId ) ) ? ( string ) $this->m_intPrimaryApplicantId : 'NULL';
	}

	public function setPreviousSubsidyContractTypeId( $intPreviousSubsidyContractTypeId ) {
		$this->set( 'm_intPreviousSubsidyContractTypeId', CStrings::strToIntDef( $intPreviousSubsidyContractTypeId, NULL, false ) );
	}

	public function getPreviousSubsidyContractTypeId() {
		return $this->m_intPreviousSubsidyContractTypeId;
	}

	public function sqlPreviousSubsidyContractTypeId() {
		return ( true == isset( $this->m_intPreviousSubsidyContractTypeId ) ) ? ( string ) $this->m_intPreviousSubsidyContractTypeId : 'NULL';
	}

	public function setMoveInIncomeLevelTypeId( $intMoveInIncomeLevelTypeId ) {
		$this->set( 'm_intMoveInIncomeLevelTypeId', CStrings::strToIntDef( $intMoveInIncomeLevelTypeId, NULL, false ) );
	}

	public function getMoveInIncomeLevelTypeId() {
		return $this->m_intMoveInIncomeLevelTypeId;
	}

	public function sqlMoveInIncomeLevelTypeId() {
		return ( true == isset( $this->m_intMoveInIncomeLevelTypeId ) ) ? ( string ) $this->m_intMoveInIncomeLevelTypeId : 'NULL';
	}

	public function setCurrentIncomeLevelTypeId( $intCurrentIncomeLevelTypeId ) {
		$this->set( 'm_intCurrentIncomeLevelTypeId', CStrings::strToIntDef( $intCurrentIncomeLevelTypeId, NULL, false ) );
	}

	public function getCurrentIncomeLevelTypeId() {
		return $this->m_intCurrentIncomeLevelTypeId;
	}

	public function sqlCurrentIncomeLevelTypeId() {
		return ( true == isset( $this->m_intCurrentIncomeLevelTypeId ) ) ? ( string ) $this->m_intCurrentIncomeLevelTypeId : 'NULL';
	}

	public function setSubsidyWaiverTypeId( $intSubsidyWaiverTypeId ) {
		$this->set( 'm_intSubsidyWaiverTypeId', CStrings::strToIntDef( $intSubsidyWaiverTypeId, NULL, false ) );
	}

	public function getSubsidyWaiverTypeId() {
		return $this->m_intSubsidyWaiverTypeId;
	}

	public function sqlSubsidyWaiverTypeId() {
		return ( true == isset( $this->m_intSubsidyWaiverTypeId ) ) ? ( string ) $this->m_intSubsidyWaiverTypeId : 'NULL';
	}

	public function setSubsidyCitizenshipEligibilityTypeId( $intSubsidyCitizenshipEligibilityTypeId ) {
		$this->set( 'm_intSubsidyCitizenshipEligibilityTypeId', CStrings::strToIntDef( $intSubsidyCitizenshipEligibilityTypeId, NULL, false ) );
	}

	public function getSubsidyCitizenshipEligibilityTypeId() {
		return $this->m_intSubsidyCitizenshipEligibilityTypeId;
	}

	public function sqlSubsidyCitizenshipEligibilityTypeId() {
		return ( true == isset( $this->m_intSubsidyCitizenshipEligibilityTypeId ) ) ? ( string ) $this->m_intSubsidyCitizenshipEligibilityTypeId : '1';
	}

	public function setSubsidyMinRentExemptionTypeId( $intSubsidyMinRentExemptionTypeId ) {
		$this->set( 'm_intSubsidyMinRentExemptionTypeId', CStrings::strToIntDef( $intSubsidyMinRentExemptionTypeId, NULL, false ) );
	}

	public function getSubsidyMinRentExemptionTypeId() {
		return $this->m_intSubsidyMinRentExemptionTypeId;
	}

	public function sqlSubsidyMinRentExemptionTypeId() {
		return ( true == isset( $this->m_intSubsidyMinRentExemptionTypeId ) ) ? ( string ) $this->m_intSubsidyMinRentExemptionTypeId : 'NULL';
	}

	public function setSubsidyIncomeExceptionTypeId( $intSubsidyIncomeExceptionTypeId ) {
		$this->set( 'm_intSubsidyIncomeExceptionTypeId', CStrings::strToIntDef( $intSubsidyIncomeExceptionTypeId, NULL, false ) );
	}

	public function getSubsidyIncomeExceptionTypeId() {
		return $this->m_intSubsidyIncomeExceptionTypeId;
	}

	public function sqlSubsidyIncomeExceptionTypeId() {
		return ( true == isset( $this->m_intSubsidyIncomeExceptionTypeId ) ) ? ( string ) $this->m_intSubsidyIncomeExceptionTypeId : 'NULL';
	}

	public function setSubsidyDisplacementStatusTypeId( $intSubsidyDisplacementStatusTypeId ) {
		$this->set( 'm_intSubsidyDisplacementStatusTypeId', CStrings::strToIntDef( $intSubsidyDisplacementStatusTypeId, NULL, false ) );
	}

	public function getSubsidyDisplacementStatusTypeId() {
		return $this->m_intSubsidyDisplacementStatusTypeId;
	}

	public function sqlSubsidyDisplacementStatusTypeId() {
		return ( true == isset( $this->m_intSubsidyDisplacementStatusTypeId ) ) ? ( string ) $this->m_intSubsidyDisplacementStatusTypeId : 'NULL';
	}

	public function setSubsidyPreviousHousingTypeId( $intSubsidyPreviousHousingTypeId ) {
		$this->set( 'm_intSubsidyPreviousHousingTypeId', CStrings::strToIntDef( $intSubsidyPreviousHousingTypeId, NULL, false ) );
	}

	public function getSubsidyPreviousHousingTypeId() {
		return $this->m_intSubsidyPreviousHousingTypeId;
	}

	public function sqlSubsidyPreviousHousingTypeId() {
		return ( true == isset( $this->m_intSubsidyPreviousHousingTypeId ) ) ? ( string ) $this->m_intSubsidyPreviousHousingTypeId : 'NULL';
	}

	public function setAnticipatedAdditionAdoption( $intAnticipatedAdditionAdoption ) {
		$this->set( 'm_intAnticipatedAdditionAdoption', CStrings::strToIntDef( $intAnticipatedAdditionAdoption, NULL, false ) );
	}

	public function getAnticipatedAdditionAdoption() {
		return $this->m_intAnticipatedAdditionAdoption;
	}

	public function sqlAnticipatedAdditionAdoption() {
		return ( true == isset( $this->m_intAnticipatedAdditionAdoption ) ) ? ( string ) $this->m_intAnticipatedAdditionAdoption : '0';
	}

	public function setAnticipatedAdditionPregnancy( $intAnticipatedAdditionPregnancy ) {
		$this->set( 'm_intAnticipatedAdditionPregnancy', CStrings::strToIntDef( $intAnticipatedAdditionPregnancy, NULL, false ) );
	}

	public function getAnticipatedAdditionPregnancy() {
		return $this->m_intAnticipatedAdditionPregnancy;
	}

	public function sqlAnticipatedAdditionPregnancy() {
		return ( true == isset( $this->m_intAnticipatedAdditionPregnancy ) ) ? ( string ) $this->m_intAnticipatedAdditionPregnancy : '0';
	}

	public function setAnticipatedAdditionFosterChild( $intAnticipatedAdditionFosterChild ) {
		$this->set( 'm_intAnticipatedAdditionFosterChild', CStrings::strToIntDef( $intAnticipatedAdditionFosterChild, NULL, false ) );
	}

	public function getAnticipatedAdditionFosterChild() {
		return $this->m_intAnticipatedAdditionFosterChild;
	}

	public function sqlAnticipatedAdditionFosterChild() {
		return ( true == isset( $this->m_intAnticipatedAdditionFosterChild ) ) ? ( string ) $this->m_intAnticipatedAdditionFosterChild : '0';
	}

	public function setInitialHouseholdSize( $intInitialHouseholdSize ) {
		$this->set( 'm_intInitialHouseholdSize', CStrings::strToIntDef( $intInitialHouseholdSize, NULL, false ) );
	}

	public function getInitialHouseholdSize() {
		return $this->m_intInitialHouseholdSize;
	}

	public function sqlInitialHouseholdSize() {
		return ( true == isset( $this->m_intInitialHouseholdSize ) ) ? ( string ) $this->m_intInitialHouseholdSize : 'NULL';
	}

	public function setInitialTotalAnnualIncome( $fltInitialTotalAnnualIncome ) {
		$this->set( 'm_fltInitialTotalAnnualIncome', CStrings::strToFloatDef( $fltInitialTotalAnnualIncome, NULL, false, 2 ) );
	}

	public function getInitialTotalAnnualIncome() {
		return $this->m_fltInitialTotalAnnualIncome;
	}

	public function sqlInitialTotalAnnualIncome() {
		return ( true == isset( $this->m_fltInitialTotalAnnualIncome ) ) ? ( string ) $this->m_fltInitialTotalAnnualIncome : 'NULL';
	}

	public function setInitialPercentOfMedianIncome( $fltInitialPercentOfMedianIncome ) {
		$this->set( 'm_fltInitialPercentOfMedianIncome', CStrings::strToFloatDef( $fltInitialPercentOfMedianIncome, NULL, false, 2 ) );
	}

	public function getInitialPercentOfMedianIncome() {
		return $this->m_fltInitialPercentOfMedianIncome;
	}

	public function sqlInitialPercentOfMedianIncome() {
		return ( true == isset( $this->m_fltInitialPercentOfMedianIncome ) ) ? ( string ) $this->m_fltInitialPercentOfMedianIncome : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, primary_applicant_id, previous_subsidy_contract_type_id, move_in_income_level_type_id, current_income_level_type_id, subsidy_waiver_type_id, subsidy_citizenship_eligibility_type_id, subsidy_min_rent_exemption_type_id, subsidy_income_exception_type_id, subsidy_displacement_status_type_id, subsidy_previous_housing_type_id, anticipated_addition_adoption, anticipated_addition_pregnancy, anticipated_addition_foster_child, initial_household_size, initial_total_annual_income, initial_percent_of_median_income, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlPrimaryApplicantId() . ', ' .
 						$this->sqlPreviousSubsidyContractTypeId() . ', ' .
 						$this->sqlMoveInIncomeLevelTypeId() . ', ' .
 						$this->sqlCurrentIncomeLevelTypeId() . ', ' .
 						$this->sqlSubsidyWaiverTypeId() . ', ' .
 						$this->sqlSubsidyCitizenshipEligibilityTypeId() . ', ' .
 						$this->sqlSubsidyMinRentExemptionTypeId() . ', ' .
 						$this->sqlSubsidyIncomeExceptionTypeId() . ', ' .
 						$this->sqlSubsidyDisplacementStatusTypeId() . ', ' .
 						$this->sqlSubsidyPreviousHousingTypeId() . ', ' .
 						$this->sqlAnticipatedAdditionAdoption() . ', ' .
 						$this->sqlAnticipatedAdditionPregnancy() . ', ' .
 						$this->sqlAnticipatedAdditionFosterChild() . ', ' .
 						$this->sqlInitialHouseholdSize() . ', ' .
 						$this->sqlInitialTotalAnnualIncome() . ', ' .
 						$this->sqlInitialPercentOfMedianIncome() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId() . ','; } elseif( true == array_key_exists( 'PrimaryApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_subsidy_contract_type_id = ' . $this->sqlPreviousSubsidyContractTypeId() . ','; } elseif( true == array_key_exists( 'PreviousSubsidyContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' previous_subsidy_contract_type_id = ' . $this->sqlPreviousSubsidyContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_income_level_type_id = ' . $this->sqlMoveInIncomeLevelTypeId() . ','; } elseif( true == array_key_exists( 'MoveInIncomeLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_income_level_type_id = ' . $this->sqlMoveInIncomeLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_income_level_type_id = ' . $this->sqlCurrentIncomeLevelTypeId() . ','; } elseif( true == array_key_exists( 'CurrentIncomeLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' current_income_level_type_id = ' . $this->sqlCurrentIncomeLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_waiver_type_id = ' . $this->sqlSubsidyWaiverTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyWaiverTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_waiver_type_id = ' . $this->sqlSubsidyWaiverTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_citizenship_eligibility_type_id = ' . $this->sqlSubsidyCitizenshipEligibilityTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyCitizenshipEligibilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_citizenship_eligibility_type_id = ' . $this->sqlSubsidyCitizenshipEligibilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_min_rent_exemption_type_id = ' . $this->sqlSubsidyMinRentExemptionTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyMinRentExemptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_min_rent_exemption_type_id = ' . $this->sqlSubsidyMinRentExemptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_exception_type_id = ' . $this->sqlSubsidyIncomeExceptionTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeExceptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_exception_type_id = ' . $this->sqlSubsidyIncomeExceptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_displacement_status_type_id = ' . $this->sqlSubsidyDisplacementStatusTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyDisplacementStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_displacement_status_type_id = ' . $this->sqlSubsidyDisplacementStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_previous_housing_type_id = ' . $this->sqlSubsidyPreviousHousingTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyPreviousHousingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_previous_housing_type_id = ' . $this->sqlSubsidyPreviousHousingTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_addition_adoption = ' . $this->sqlAnticipatedAdditionAdoption() . ','; } elseif( true == array_key_exists( 'AnticipatedAdditionAdoption', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_addition_adoption = ' . $this->sqlAnticipatedAdditionAdoption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_addition_pregnancy = ' . $this->sqlAnticipatedAdditionPregnancy() . ','; } elseif( true == array_key_exists( 'AnticipatedAdditionPregnancy', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_addition_pregnancy = ' . $this->sqlAnticipatedAdditionPregnancy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_addition_foster_child = ' . $this->sqlAnticipatedAdditionFosterChild() . ','; } elseif( true == array_key_exists( 'AnticipatedAdditionFosterChild', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_addition_foster_child = ' . $this->sqlAnticipatedAdditionFosterChild() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_household_size = ' . $this->sqlInitialHouseholdSize() . ','; } elseif( true == array_key_exists( 'InitialHouseholdSize', $this->getChangedColumns() ) ) { $strSql .= ' initial_household_size = ' . $this->sqlInitialHouseholdSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_total_annual_income = ' . $this->sqlInitialTotalAnnualIncome() . ','; } elseif( true == array_key_exists( 'InitialTotalAnnualIncome', $this->getChangedColumns() ) ) { $strSql .= ' initial_total_annual_income = ' . $this->sqlInitialTotalAnnualIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_percent_of_median_income = ' . $this->sqlInitialPercentOfMedianIncome() . ','; } elseif( true == array_key_exists( 'InitialPercentOfMedianIncome', $this->getChangedColumns() ) ) { $strSql .= ' initial_percent_of_median_income = ' . $this->sqlInitialPercentOfMedianIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'primary_applicant_id' => $this->getPrimaryApplicantId(),
			'previous_subsidy_contract_type_id' => $this->getPreviousSubsidyContractTypeId(),
			'move_in_income_level_type_id' => $this->getMoveInIncomeLevelTypeId(),
			'current_income_level_type_id' => $this->getCurrentIncomeLevelTypeId(),
			'subsidy_waiver_type_id' => $this->getSubsidyWaiverTypeId(),
			'subsidy_citizenship_eligibility_type_id' => $this->getSubsidyCitizenshipEligibilityTypeId(),
			'subsidy_min_rent_exemption_type_id' => $this->getSubsidyMinRentExemptionTypeId(),
			'subsidy_income_exception_type_id' => $this->getSubsidyIncomeExceptionTypeId(),
			'subsidy_displacement_status_type_id' => $this->getSubsidyDisplacementStatusTypeId(),
			'subsidy_previous_housing_type_id' => $this->getSubsidyPreviousHousingTypeId(),
			'anticipated_addition_adoption' => $this->getAnticipatedAdditionAdoption(),
			'anticipated_addition_pregnancy' => $this->getAnticipatedAdditionPregnancy(),
			'anticipated_addition_foster_child' => $this->getAnticipatedAdditionFosterChild(),
			'initial_household_size' => $this->getInitialHouseholdSize(),
			'initial_total_annual_income' => $this->getInitialTotalAnnualIncome(),
			'initial_percent_of_median_income' => $this->getInitialPercentOfMedianIncome(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>