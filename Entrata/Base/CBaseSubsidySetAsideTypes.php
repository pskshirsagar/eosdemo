<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySetAsideTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySetAsideTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySetAsideType[]
	 */
	public static function fetchSubsidySetAsideTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySetAsideType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySetAsideType
	 */
	public static function fetchSubsidySetAsideType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySetAsideType::class, $objDatabase );
	}

	public static function fetchSubsidySetAsideTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_set_aside_types', $objDatabase );
	}

	public static function fetchSubsidySetAsideTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySetAsideType( sprintf( 'SELECT * FROM subsidy_set_aside_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>