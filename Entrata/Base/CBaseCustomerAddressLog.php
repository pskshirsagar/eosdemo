<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 *
 * @FIXME: These classes are temporarily manually edited for i18n release deadline.
 *          Please talk to ssontakke@entrata.com if you need to regenerate this class.
 *
 */
class CBaseCustomerAddressLog extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_address_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intCustomerAddressId;
	protected $m_intPriorCustomerAddressLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_intAddressTypeId;
	protected $m_intCompanyResidenceTypeId;
	protected $m_intTimeZoneId;
	protected $m_intMoveOutReasonListItemId;
	protected $m_strCommunityName;
	protected $m_strReasonForLeaving;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_fltMonthlyPaymentAmount;
	protected $m_strPaymentRecipient;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strCounty;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_boolIsVerified;
	protected $m_strAddressOwnerType;
	protected $m_boolIsNonUsAddress;
	protected $m_strLogDatetime;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_strRemotePrimaryKey;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_boolIsVerified = false;
		$this->m_strAddressOwnerType = 'Unknown';
		$this->m_boolIsNonUsAddress = false;
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strCounty' => 'dependentLocality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_address_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerAddressId', trim( $arrValues['customer_address_id'] ) ); elseif( isset( $arrValues['customer_address_id'] ) ) $this->setCustomerAddressId( $arrValues['customer_address_id'] );
		if( isset( $arrValues['prior_customer_address_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerAddressLogId', trim( $arrValues['prior_customer_address_log_id'] ) ); elseif( isset( $arrValues['prior_customer_address_log_id'] ) ) $this->setPriorCustomerAddressLogId( $arrValues['prior_customer_address_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['address_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddressTypeId', trim( $arrValues['address_type_id'] ) ); elseif( isset( $arrValues['address_type_id'] ) ) $this->setAddressTypeId( $arrValues['address_type_id'] );
		if( isset( $arrValues['company_residence_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyResidenceTypeId', trim( $arrValues['company_residence_type_id'] ) ); elseif( isset( $arrValues['company_residence_type_id'] ) ) $this->setCompanyResidenceTypeId( $arrValues['company_residence_type_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['move_out_reason_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutReasonListItemId', trim( $arrValues['move_out_reason_list_item_id'] ) ); elseif( isset( $arrValues['move_out_reason_list_item_id'] ) ) $this->setMoveOutReasonListItemId( $arrValues['move_out_reason_list_item_id'] );
		if( isset( $arrValues['community_name'] ) && $boolDirectSet ) $this->set( 'm_strCommunityName', trim( stripcslashes( $arrValues['community_name'] ) ) ); elseif( isset( $arrValues['community_name'] ) ) $this->setCommunityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['community_name'] ) : $arrValues['community_name'] );
		if( isset( $arrValues['reason_for_leaving'] ) && $boolDirectSet ) $this->set( 'm_strReasonForLeaving', trim( stripcslashes( $arrValues['reason_for_leaving'] ) ) ); elseif( isset( $arrValues['reason_for_leaving'] ) ) $this->setReasonForLeaving( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_for_leaving'] ) : $arrValues['reason_for_leaving'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['monthly_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyPaymentAmount', trim( $arrValues['monthly_payment_amount'] ) ); elseif( isset( $arrValues['monthly_payment_amount'] ) ) $this->setMonthlyPaymentAmount( $arrValues['monthly_payment_amount'] );
		if( isset( $arrValues['payment_recipient'] ) && $boolDirectSet ) $this->set( 'm_strPaymentRecipient', trim( stripcslashes( $arrValues['payment_recipient'] ) ) ); elseif( isset( $arrValues['payment_recipient'] ) ) $this->setPaymentRecipient( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_recipient'] ) : $arrValues['payment_recipient'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['county'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCounty', trim( stripcslashes( $arrValues['county'] ) ) ); elseif( isset( $arrValues['county'] ) ) $this->setCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county'] ) : $arrValues['county'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerified', trim( stripcslashes( $arrValues['is_verified'] ) ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
		if( isset( $arrValues['address_owner_type'] ) && $boolDirectSet ) $this->set( 'm_strAddressOwnerType', trim( stripcslashes( $arrValues['address_owner_type'] ) ) ); elseif( isset( $arrValues['address_owner_type'] ) ) $this->setAddressOwnerType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_owner_type'] ) : $arrValues['address_owner_type'] );
		if( isset( $arrValues['is_non_us_address'] ) && $boolDirectSet ) $this->set( 'm_boolIsNonUsAddress', trim( stripcslashes( $arrValues['is_non_us_address'] ) ) ); elseif( isset( $arrValues['is_non_us_address'] ) ) $this->setIsNonUsAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_non_us_address'] ) : $arrValues['is_non_us_address'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );

		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerAddressId( $intCustomerAddressId ) {
		$this->set( 'm_intCustomerAddressId', CStrings::strToIntDef( $intCustomerAddressId, NULL, false ) );
	}

	public function getCustomerAddressId() {
		return $this->m_intCustomerAddressId;
	}

	public function sqlCustomerAddressId() {
		return ( true == isset( $this->m_intCustomerAddressId ) ) ? ( string ) $this->m_intCustomerAddressId : 'NULL';
	}

	public function setPriorCustomerAddressLogId( $intPriorCustomerAddressLogId ) {
		$this->set( 'm_intPriorCustomerAddressLogId', CStrings::strToIntDef( $intPriorCustomerAddressLogId, NULL, false ) );
	}

	public function getPriorCustomerAddressLogId() {
		return $this->m_intPriorCustomerAddressLogId;
	}

	public function sqlPriorCustomerAddressLogId() {
		return ( true == isset( $this->m_intPriorCustomerAddressLogId ) ) ? ( string ) $this->m_intPriorCustomerAddressLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->set( 'm_intAddressTypeId', CStrings::strToIntDef( $intAddressTypeId, NULL, false ) );
	}

	public function getAddressTypeId() {
		return $this->m_intAddressTypeId;
	}

	public function sqlAddressTypeId() {
		return ( true == isset( $this->m_intAddressTypeId ) ) ? ( string ) $this->m_intAddressTypeId : 'NULL';
	}

	public function setCompanyResidenceTypeId( $intCompanyResidenceTypeId ) {
		$this->set( 'm_intCompanyResidenceTypeId', CStrings::strToIntDef( $intCompanyResidenceTypeId, NULL, false ) );
	}

	public function getCompanyResidenceTypeId() {
		return $this->m_intCompanyResidenceTypeId;
	}

	public function sqlCompanyResidenceTypeId() {
		return ( true == isset( $this->m_intCompanyResidenceTypeId ) ) ? ( string ) $this->m_intCompanyResidenceTypeId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setMoveOutReasonListItemId( $intMoveOutReasonListItemId ) {
		$this->set( 'm_intMoveOutReasonListItemId', CStrings::strToIntDef( $intMoveOutReasonListItemId, NULL, false ) );
	}

	public function getMoveOutReasonListItemId() {
		return $this->m_intMoveOutReasonListItemId;
	}

	public function sqlMoveOutReasonListItemId() {
		return ( true == isset( $this->m_intMoveOutReasonListItemId ) ) ? ( string ) $this->m_intMoveOutReasonListItemId : 'NULL';
	}

	public function setCommunityName( $strCommunityName ) {
		$this->set( 'm_strCommunityName', CStrings::strTrimDef( $strCommunityName, 50, NULL, true ) );
	}

	public function getCommunityName() {
		return $this->m_strCommunityName;
	}

	public function sqlCommunityName() {
		return ( true == isset( $this->m_strCommunityName ) ) ? '\'' . addslashes( $this->m_strCommunityName ) . '\'' : 'NULL';
	}

	public function setReasonForLeaving( $strReasonForLeaving ) {
		$this->set( 'm_strReasonForLeaving', CStrings::strTrimDef( $strReasonForLeaving, 240, NULL, true ) );
	}

	public function getReasonForLeaving() {
		return $this->m_strReasonForLeaving;
	}

	public function sqlReasonForLeaving() {
		return ( true == isset( $this->m_strReasonForLeaving ) ) ? '\'' . addslashes( $this->m_strReasonForLeaving ) . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setMonthlyPaymentAmount( $fltMonthlyPaymentAmount ) {
		$this->set( 'm_fltMonthlyPaymentAmount', CStrings::strToFloatDef( $fltMonthlyPaymentAmount, NULL, false, 2 ) );
	}

	public function getMonthlyPaymentAmount() {
		return $this->m_fltMonthlyPaymentAmount;
	}

	public function sqlMonthlyPaymentAmount() {
		return ( true == isset( $this->m_fltMonthlyPaymentAmount ) ) ? ( string ) $this->m_fltMonthlyPaymentAmount : 'NULL';
	}

	public function setPaymentRecipient( $strPaymentRecipient ) {
		$this->set( 'm_strPaymentRecipient', CStrings::strTrimDef( $strPaymentRecipient, 50, NULL, true ) );
	}

	public function getPaymentRecipient() {
		return $this->m_strPaymentRecipient;
	}

	public function sqlPaymentRecipient() {
		return ( true == isset( $this->m_strPaymentRecipient ) ) ? '\'' . addslashes( $this->m_strPaymentRecipient ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setCounty( $strCounty ) {
		$this->setPostalAddressField( 'dependentLocality', $strCounty, $strAddressKey = 'default', 'm_strCounty' );
	}

	public function getCounty() {
		return $this->getPostalAddressField( 'dependentLocality', 'default', 'm_strCounty' );
	}

	public function sqlCounty() {
		return ( true == isset( $this->m_strCounty ) ) ? '\'' . addslashes( $this->m_strCounty ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->set( 'm_boolIsVerified', CStrings::strToBool( $boolIsVerified ) );
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAddressOwnerType( $strAddressOwnerType ) {
		$this->set( 'm_strAddressOwnerType', CStrings::strTrimDef( $strAddressOwnerType, -1, NULL, true ) );
	}

	public function getAddressOwnerType() {
		return $this->m_strAddressOwnerType;
	}

	public function sqlAddressOwnerType() {
		return ( true == isset( $this->m_strAddressOwnerType ) ) ? '\'' . addslashes( $this->m_strAddressOwnerType ) . '\'' : '\'Unknown\'';
	}

	public function setIsNonUsAddress( $boolIsNonUsAddress ) {
		$this->set( 'm_boolIsNonUsAddress', CStrings::strToBool( $boolIsNonUsAddress ) );
	}

	public function getIsNonUsAddress() {
		return $this->m_boolIsNonUsAddress;
	}

	public function sqlIsNonUsAddress() {
		return ( true == isset( $this->m_boolIsNonUsAddress ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNonUsAddress ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, customer_address_id, prior_customer_address_log_id, post_date, apply_through_post_date, address_type_id, company_residence_type_id, time_zone_id, move_out_reason_list_item_id, community_name, reason_for_leaving, move_in_date, move_out_date, monthly_payment_amount, payment_recipient, street_line1, street_line2, street_line3, city, county, state_code, province, postal_code, country_code, is_verified, address_owner_type, is_non_us_address, log_datetime, is_post_date_ignored, is_opening_log, remote_primary_key, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerAddressId() . ', ' .
						$this->sqlPriorCustomerAddressLogId() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlAddressTypeId() . ', ' .
						$this->sqlCompanyResidenceTypeId() . ', ' .
						$this->sqlTimeZoneId() . ', ' .
						$this->sqlMoveOutReasonListItemId() . ', ' .
						$this->sqlCommunityName() . ', ' .
						$this->sqlReasonForLeaving() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlMonthlyPaymentAmount() . ', ' .
						$this->sqlPaymentRecipient() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlStreetLine2() . ', ' .
						$this->sqlStreetLine3() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlCounty() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlIsVerified() . ', ' .
						$this->sqlAddressOwnerType() . ', ' .
						$this->sqlIsNonUsAddress() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_address_id = ' . $this->sqlCustomerAddressId() . ','; } elseif( true == array_key_exists( 'CustomerAddressId', $this->getChangedColumns() ) ) { $strSql .= ' customer_address_id = ' . $this->sqlCustomerAddressId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_address_log_id = ' . $this->sqlPriorCustomerAddressLogId() . ','; } elseif( true == array_key_exists( 'PriorCustomerAddressLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_address_log_id = ' . $this->sqlPriorCustomerAddressLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; } elseif( true == array_key_exists( 'AddressTypeId', $this->getChangedColumns() ) ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_residence_type_id = ' . $this->sqlCompanyResidenceTypeId() . ','; } elseif( true == array_key_exists( 'CompanyResidenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_residence_type_id = ' . $this->sqlCompanyResidenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId() . ','; } elseif( true == array_key_exists( 'MoveOutReasonListItemId', $this->getChangedColumns() ) ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' community_name = ' . $this->sqlCommunityName() . ','; } elseif( true == array_key_exists( 'CommunityName', $this->getChangedColumns() ) ) { $strSql .= ' community_name = ' . $this->sqlCommunityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; } elseif( true == array_key_exists( 'ReasonForLeaving', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount() . ','; } elseif( true == array_key_exists( 'MonthlyPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_recipient = ' . $this->sqlPaymentRecipient() . ','; } elseif( true == array_key_exists( 'PaymentRecipient', $this->getChangedColumns() ) ) { $strSql .= ' payment_recipient = ' . $this->sqlPaymentRecipient() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; } elseif( true == array_key_exists( 'County', $this->getChangedColumns() ) ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_owner_type = ' . $this->sqlAddressOwnerType() . ','; } elseif( true == array_key_exists( 'AddressOwnerType', $this->getChangedColumns() ) ) { $strSql .= ' address_owner_type = ' . $this->sqlAddressOwnerType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_non_us_address = ' . $this->sqlIsNonUsAddress() . ','; } elseif( true == array_key_exists( 'IsNonUsAddress', $this->getChangedColumns() ) ) { $strSql .= ' is_non_us_address = ' . $this->sqlIsNonUsAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'customer_address_id' => $this->getCustomerAddressId(),
			'prior_customer_address_log_id' => $this->getPriorCustomerAddressLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'address_type_id' => $this->getAddressTypeId(),
			'company_residence_type_id' => $this->getCompanyResidenceTypeId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'move_out_reason_list_item_id' => $this->getMoveOutReasonListItemId(),
			'community_name' => $this->getCommunityName(),
			'reason_for_leaving' => $this->getReasonForLeaving(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'monthly_payment_amount' => $this->getMonthlyPaymentAmount(),
			'payment_recipient' => $this->getPaymentRecipient(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'county' => $this->getCounty(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'is_verified' => $this->getIsVerified(),
			'address_owner_type' => $this->getAddressOwnerType(),
			'is_non_us_address' => $this->getIsNonUsAddress(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>