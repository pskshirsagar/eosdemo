<?php

class CBaseInterCompanyStages extends CEosPluralBase {

	public static function fetchInterCompanyStages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInterCompanyStage', $objDatabase );
	}

	public static function fetchInterCompanyStage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInterCompanyStage', $objDatabase );
	}

	public static function fetchInterCompanyStageCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inter_company_stages', $objDatabase );
	}

	public static function fetchInterCompanyStageById( $intId, $objDatabase ) {
		return self::fetchInterCompanyStage( sprintf( 'SELECT * FROM inter_company_stages WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>