<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerContactTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerContactTypes extends CEosPluralBase {

	/**
	 * @return CCustomerContactType[]
	 */
	public static function fetchCustomerContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerContactType::class, $objDatabase );
	}

	/**
	 * @return CCustomerContactType
	 */
	public static function fetchCustomerContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerContactType::class, $objDatabase );
	}

	public static function fetchCustomerContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_contact_types', $objDatabase );
	}

	public static function fetchCustomerContactTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerContactType( sprintf( 'SELECT * FROM customer_contact_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>