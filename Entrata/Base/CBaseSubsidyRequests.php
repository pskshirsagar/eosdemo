<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyRequests extends CEosPluralBase {

	/**
	 * @return CSubsidyRequest[]
	 */
	public static function fetchSubsidyRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyRequest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyRequest
	 */
	public static function fetchSubsidyRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyRequest', $objDatabase );
	}

	public static function fetchSubsidyRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_requests', $objDatabase );
	}

	public static function fetchSubsidyRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRequest( sprintf( 'SELECT * FROM subsidy_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyRequests( sprintf( 'SELECT * FROM subsidy_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRequests( sprintf( 'SELECT * FROM subsidy_requests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRequestsBySubsidyRequestResponseTypeIdByCid( $intSubsidyRequestResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRequests( sprintf( 'SELECT * FROM subsidy_requests WHERE subsidy_request_response_type_id = %d AND cid = %d', ( int ) $intSubsidyRequestResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>