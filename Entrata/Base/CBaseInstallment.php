<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInstallment extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.installments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInstallmentPlanId;
	protected $m_strName;
	protected $m_strChargeStartDate;
	protected $m_strChargeEndDate;
	protected $m_boolUseBillingMonthAsPostMonth;
	protected $m_boolPostAllAtOnce;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintChargeMonths;
	protected $m_intBillingStartMonth;
	protected $m_strPostOnDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolUseBillingMonthAsPostMonth = false;
		$this->m_boolPostAllAtOnce = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['installment_plan_id'] ) && $boolDirectSet ) $this->set( 'm_intInstallmentPlanId', trim( $arrValues['installment_plan_id'] ) ); elseif( isset( $arrValues['installment_plan_id'] ) ) $this->setInstallmentPlanId( $arrValues['installment_plan_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['charge_start_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeStartDate', trim( $arrValues['charge_start_date'] ) ); elseif( isset( $arrValues['charge_start_date'] ) ) $this->setChargeStartDate( $arrValues['charge_start_date'] );
		if( isset( $arrValues['charge_end_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeEndDate', trim( $arrValues['charge_end_date'] ) ); elseif( isset( $arrValues['charge_end_date'] ) ) $this->setChargeEndDate( $arrValues['charge_end_date'] );
		if( isset( $arrValues['use_billing_month_as_post_month'] ) && $boolDirectSet ) $this->set( 'm_boolUseBillingMonthAsPostMonth', trim( stripcslashes( $arrValues['use_billing_month_as_post_month'] ) ) ); elseif( isset( $arrValues['use_billing_month_as_post_month'] ) ) $this->setUseBillingMonthAsPostMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_billing_month_as_post_month'] ) : $arrValues['use_billing_month_as_post_month'] );
		if( isset( $arrValues['post_all_at_once'] ) && $boolDirectSet ) $this->set( 'm_boolPostAllAtOnce', trim( stripcslashes( $arrValues['post_all_at_once'] ) ) ); elseif( isset( $arrValues['post_all_at_once'] ) ) $this->setPostAllAtOnce( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_all_at_once'] ) : $arrValues['post_all_at_once'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['charge_months'] ) && $boolDirectSet ) $this->set( 'm_arrintChargeMonths', trim( $arrValues['charge_months'] ) ); elseif( isset( $arrValues['charge_months'] ) ) $this->setChargeMonths( $arrValues['charge_months'] );
		if( isset( $arrValues['billing_start_month'] ) && $boolDirectSet ) $this->set( 'm_intBillingStartMonth', trim( $arrValues['billing_start_month'] ) ); elseif( isset( $arrValues['billing_start_month'] ) ) $this->setBillingStartMonth( $arrValues['billing_start_month'] );
		if( isset( $arrValues['post_on_date'] ) && $boolDirectSet ) $this->set( 'm_strPostOnDate', trim( $arrValues['post_on_date'] ) ); elseif( isset( $arrValues['post_on_date'] ) ) $this->setPostOnDate( $arrValues['post_on_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInstallmentPlanId( $intInstallmentPlanId ) {
		$this->set( 'm_intInstallmentPlanId', CStrings::strToIntDef( $intInstallmentPlanId, NULL, false ) );
	}

	public function getInstallmentPlanId() {
		return $this->m_intInstallmentPlanId;
	}

	public function sqlInstallmentPlanId() {
		return ( true == isset( $this->m_intInstallmentPlanId ) ) ? ( string ) $this->m_intInstallmentPlanId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setChargeStartDate( $strChargeStartDate ) {
		$this->set( 'm_strChargeStartDate', CStrings::strTrimDef( $strChargeStartDate, -1, NULL, true ) );
	}

	public function getChargeStartDate() {
		return $this->m_strChargeStartDate;
	}

	public function sqlChargeStartDate() {
		return ( true == isset( $this->m_strChargeStartDate ) ) ? '\'' . $this->m_strChargeStartDate . '\'' : 'NULL';
	}

	public function setChargeEndDate( $strChargeEndDate ) {
		$this->set( 'm_strChargeEndDate', CStrings::strTrimDef( $strChargeEndDate, -1, NULL, true ) );
	}

	public function getChargeEndDate() {
		return $this->m_strChargeEndDate;
	}

	public function sqlChargeEndDate() {
		return ( true == isset( $this->m_strChargeEndDate ) ) ? '\'' . $this->m_strChargeEndDate . '\'' : 'NULL';
	}

	public function setUseBillingMonthAsPostMonth( $boolUseBillingMonthAsPostMonth ) {
		$this->set( 'm_boolUseBillingMonthAsPostMonth', CStrings::strToBool( $boolUseBillingMonthAsPostMonth ) );
	}

	public function getUseBillingMonthAsPostMonth() {
		return $this->m_boolUseBillingMonthAsPostMonth;
	}

	public function sqlUseBillingMonthAsPostMonth() {
		return ( true == isset( $this->m_boolUseBillingMonthAsPostMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseBillingMonthAsPostMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostAllAtOnce( $boolPostAllAtOnce ) {
		$this->set( 'm_boolPostAllAtOnce', CStrings::strToBool( $boolPostAllAtOnce ) );
	}

	public function getPostAllAtOnce() {
		return $this->m_boolPostAllAtOnce;
	}

	public function sqlPostAllAtOnce() {
		return ( true == isset( $this->m_boolPostAllAtOnce ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostAllAtOnce ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setChargeMonths( $arrintChargeMonths ) {
		$this->set( 'm_arrintChargeMonths', CStrings::strToArrIntDef( $arrintChargeMonths, NULL ) );
	}

	public function getChargeMonths() {
		return $this->m_arrintChargeMonths;
	}

	public function sqlChargeMonths() {
		return ( true == isset( $this->m_arrintChargeMonths ) && true == valArr( $this->m_arrintChargeMonths ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintChargeMonths, NULL ) . '\'' : 'NULL';
	}

	public function setBillingStartMonth( $intBillingStartMonth ) {
		$this->set( 'm_intBillingStartMonth', CStrings::strToIntDef( $intBillingStartMonth, NULL, false ) );
	}

	public function getBillingStartMonth() {
		return $this->m_intBillingStartMonth;
	}

	public function sqlBillingStartMonth() {
		return ( true == isset( $this->m_intBillingStartMonth ) ) ? ( string ) $this->m_intBillingStartMonth : 'NULL';
	}

	public function setPostOnDate( $strPostOnDate ) {
		$this->set( 'm_strPostOnDate', CStrings::strTrimDef( $strPostOnDate, -1, NULL, true ) );
	}

	public function getPostOnDate() {
		return $this->m_strPostOnDate;
	}

	public function sqlPostOnDate() {
		return ( true == isset( $this->m_strPostOnDate ) ) ? '\'' . $this->m_strPostOnDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, installment_plan_id, name, charge_start_date, charge_end_date, use_billing_month_as_post_month, post_all_at_once, updated_by, updated_on, created_by, created_on, details, charge_months, billing_start_month, post_on_date, deleted_by, deleted_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlInstallmentPlanId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlChargeStartDate() . ', ' .
		          $this->sqlChargeEndDate() . ', ' .
		          $this->sqlUseBillingMonthAsPostMonth() . ', ' .
		          $this->sqlPostAllAtOnce() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlChargeMonths() . ', ' .
		          $this->sqlBillingStartMonth() . ', ' .
		          $this->sqlPostOnDate() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installment_plan_id = ' . $this->sqlInstallmentPlanId(). ',' ; } elseif( true == array_key_exists( 'InstallmentPlanId', $this->getChangedColumns() ) ) { $strSql .= ' installment_plan_id = ' . $this->sqlInstallmentPlanId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate(). ',' ; } elseif( true == array_key_exists( 'ChargeStartDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate(). ',' ; } elseif( true == array_key_exists( 'ChargeEndDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_billing_month_as_post_month = ' . $this->sqlUseBillingMonthAsPostMonth(). ',' ; } elseif( true == array_key_exists( 'UseBillingMonthAsPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' use_billing_month_as_post_month = ' . $this->sqlUseBillingMonthAsPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_all_at_once = ' . $this->sqlPostAllAtOnce(). ',' ; } elseif( true == array_key_exists( 'PostAllAtOnce', $this->getChangedColumns() ) ) { $strSql .= ' post_all_at_once = ' . $this->sqlPostAllAtOnce() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_months = ' . $this->sqlChargeMonths(). ',' ; } elseif( true == array_key_exists( 'ChargeMonths', $this->getChangedColumns() ) ) { $strSql .= ' charge_months = ' . $this->sqlChargeMonths() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_month = ' . $this->sqlBillingStartMonth(). ',' ; } elseif( true == array_key_exists( 'BillingStartMonth', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_month = ' . $this->sqlBillingStartMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_on_date = ' . $this->sqlPostOnDate(). ',' ; } elseif( true == array_key_exists( 'PostOnDate', $this->getChangedColumns() ) ) { $strSql .= ' post_on_date = ' . $this->sqlPostOnDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'installment_plan_id' => $this->getInstallmentPlanId(),
			'name' => $this->getName(),
			'charge_start_date' => $this->getChargeStartDate(),
			'charge_end_date' => $this->getChargeEndDate(),
			'use_billing_month_as_post_month' => $this->getUseBillingMonthAsPostMonth(),
			'post_all_at_once' => $this->getPostAllAtOnce(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'charge_months' => $this->getChargeMonths(),
			'billing_start_month' => $this->getBillingStartMonth(),
			'post_on_date' => $this->getPostOnDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>