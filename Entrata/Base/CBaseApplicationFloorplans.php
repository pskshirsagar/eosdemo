<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationFloorplans
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationFloorplans extends CEosPluralBase {

	/**
	 * @return CApplicationFloorplan[]
	 */
	public static function fetchApplicationFloorplans( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationFloorplan', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationFloorplan
	 */
	public static function fetchApplicationFloorplan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationFloorplan', $objDatabase );
	}

	public static function fetchApplicationFloorplanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_floorplans', $objDatabase );
	}

	public static function fetchApplicationFloorplanByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationFloorplan( sprintf( 'SELECT * FROM application_floorplans WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationFloorplansByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationFloorplans( sprintf( 'SELECT * FROM application_floorplans WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationFloorplansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplicationFloorplans( sprintf( 'SELECT * FROM application_floorplans WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationFloorplansByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationFloorplans( sprintf( 'SELECT * FROM application_floorplans WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationFloorplansByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchApplicationFloorplans( sprintf( 'SELECT * FROM application_floorplans WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

}
?>