<?php

class CBaseDefaultSystemMessage extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_system_messages';

	protected $m_intId;
	protected $m_intSystemMessageTypeId;
	protected $m_intSystemMessageAudienceId;
	protected $m_intSystemMessageCategoryId;
	protected $m_intSystemMessageTemplateId;
	protected $m_strKey;
	protected $m_strName;
	protected $m_strSubject;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintMergeFieldGroupIds;
	protected $m_arrstrRequiredInputTables;
	protected $m_boolIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTypeId', trim( $arrValues['system_message_type_id'] ) ); elseif( isset( $arrValues['system_message_type_id'] ) ) $this->setSystemMessageTypeId( $arrValues['system_message_type_id'] );
		if( isset( $arrValues['system_message_audience_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageAudienceId', trim( $arrValues['system_message_audience_id'] ) ); elseif( isset( $arrValues['system_message_audience_id'] ) ) $this->setSystemMessageAudienceId( $arrValues['system_message_audience_id'] );
		if( isset( $arrValues['system_message_category_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageCategoryId', trim( $arrValues['system_message_category_id'] ) ); elseif( isset( $arrValues['system_message_category_id'] ) ) $this->setSystemMessageCategoryId( $arrValues['system_message_category_id'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTemplateId', trim( $arrValues['system_message_template_id'] ) ); elseif( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( $arrValues['key'] ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( $arrValues['key'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['subject'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSubject', trim( $arrValues['subject'] ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( $arrValues['subject'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['merge_field_group_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintMergeFieldGroupIds', trim( $arrValues['merge_field_group_ids'] ) ); elseif( isset( $arrValues['merge_field_group_ids'] ) ) $this->setMergeFieldGroupIds( $arrValues['merge_field_group_ids'] );
		if( isset( $arrValues['required_input_tables'] ) && $boolDirectSet ) $this->set( 'm_arrstrRequiredInputTables', trim( $arrValues['required_input_tables'] ) ); elseif( isset( $arrValues['required_input_tables'] ) ) $this->setRequiredInputTables( $arrValues['required_input_tables'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemMessageTypeId( $intSystemMessageTypeId ) {
		$this->set( 'm_intSystemMessageTypeId', CStrings::strToIntDef( $intSystemMessageTypeId, NULL, false ) );
	}

	public function getSystemMessageTypeId() {
		return $this->m_intSystemMessageTypeId;
	}

	public function sqlSystemMessageTypeId() {
		return ( true == isset( $this->m_intSystemMessageTypeId ) ) ? ( string ) $this->m_intSystemMessageTypeId : 'NULL';
	}

	public function setSystemMessageAudienceId( $intSystemMessageAudienceId ) {
		$this->set( 'm_intSystemMessageAudienceId', CStrings::strToIntDef( $intSystemMessageAudienceId, NULL, false ) );
	}

	public function getSystemMessageAudienceId() {
		return $this->m_intSystemMessageAudienceId;
	}

	public function sqlSystemMessageAudienceId() {
		return ( true == isset( $this->m_intSystemMessageAudienceId ) ) ? ( string ) $this->m_intSystemMessageAudienceId : 'NULL';
	}

	public function setSystemMessageCategoryId( $intSystemMessageCategoryId ) {
		$this->set( 'm_intSystemMessageCategoryId', CStrings::strToIntDef( $intSystemMessageCategoryId, NULL, false ) );
	}

	public function getSystemMessageCategoryId() {
		return $this->m_intSystemMessageCategoryId;
	}

	public function sqlSystemMessageCategoryId() {
		return ( true == isset( $this->m_intSystemMessageCategoryId ) ) ? ( string ) $this->m_intSystemMessageCategoryId : 'NULL';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->set( 'm_intSystemMessageTemplateId', CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false ) );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 100, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKey ) : '\'' . addslashes( $this->m_strKey ) . '\'' ) : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setSubject( $strSubject, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSubject', CStrings::strTrimDef( $strSubject, -1, NULL, true ), $strLocaleCode );
	}

	public function getSubject( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSubject', $strLocaleCode );
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubject ) : '\'' . addslashes( $this->m_strSubject ) . '\'' ) : 'NULL';
	}

	public function setMergeFieldGroupIds( $arrintMergeFieldGroupIds ) {
		$this->set( 'm_arrintMergeFieldGroupIds', CStrings::strToArrIntDef( $arrintMergeFieldGroupIds, NULL ) );
	}

	public function getMergeFieldGroupIds() {
		return $this->m_arrintMergeFieldGroupIds;
	}

	public function sqlMergeFieldGroupIds() {
		return ( true == isset( $this->m_arrintMergeFieldGroupIds ) && true == valArr( $this->m_arrintMergeFieldGroupIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintMergeFieldGroupIds, NULL ) . '\'' : 'NULL';
	}

	public function setRequiredInputTables( $arrstrRequiredInputTables ) {
		$this->set( 'm_arrstrRequiredInputTables', CStrings::strToArrIntDef( $arrstrRequiredInputTables, NULL ) );
	}

	public function getRequiredInputTables() {
		return $this->m_arrstrRequiredInputTables;
	}

	public function sqlRequiredInputTables() {
		return ( true == isset( $this->m_arrstrRequiredInputTables ) && true == valArr( $this->m_arrstrRequiredInputTables ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrRequiredInputTables, NULL ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_message_type_id, system_message_audience_id, system_message_category_id, system_message_template_id, key, name, subject, details, merge_field_group_ids, required_input_tables, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSystemMessageTypeId() . ', ' .
						$this->sqlSystemMessageAudienceId() . ', ' .
						$this->sqlSystemMessageCategoryId() . ', ' .
						$this->sqlSystemMessageTemplateId() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlSubject() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMergeFieldGroupIds() . ', ' .
						$this->sqlRequiredInputTables() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_type_id = ' . $this->sqlSystemMessageTypeId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_type_id = ' . $this->sqlSystemMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_audience_id = ' . $this->sqlSystemMessageAudienceId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageAudienceId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_audience_id = ' . $this->sqlSystemMessageAudienceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_category_id = ' . $this->sqlSystemMessageCategoryId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_category_id = ' . $this->sqlSystemMessageCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject(). ',' ; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_group_ids = ' . $this->sqlMergeFieldGroupIds(). ',' ; } elseif( true == array_key_exists( 'MergeFieldGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_group_ids = ' . $this->sqlMergeFieldGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_input_tables = ' . $this->sqlRequiredInputTables(). ',' ; } elseif( true == array_key_exists( 'RequiredInputTables', $this->getChangedColumns() ) ) { $strSql .= ' required_input_tables = ' . $this->sqlRequiredInputTables() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_message_type_id' => $this->getSystemMessageTypeId(),
			'system_message_audience_id' => $this->getSystemMessageAudienceId(),
			'system_message_category_id' => $this->getSystemMessageCategoryId(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'key' => $this->getKey(),
			'name' => $this->getName(),
			'subject' => $this->getSubject(),
			'details' => $this->getDetails(),
			'merge_field_group_ids' => $this->getMergeFieldGroupIds(),
			'required_input_tables' => $this->getRequiredInputTables(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>