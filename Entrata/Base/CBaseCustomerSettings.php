<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSettings extends CEosPluralBase {

	/**
	 * @return CCustomerSetting[]
	 */
	public static function fetchCustomerSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSetting
	 */
	public static function fetchCustomerSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerSetting', $objDatabase );
	}

	public static function fetchCustomerSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_settings', $objDatabase );
	}

	public static function fetchCustomerSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSetting( sprintf( 'SELECT * FROM customer_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSettings( sprintf( 'SELECT * FROM customer_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSettingsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSettings( sprintf( 'SELECT * FROM customer_settings WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>