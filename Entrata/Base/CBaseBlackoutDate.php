<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBlackoutDate extends CEosSingularBase {

	const TABLE_NAME = 'public.blackout_dates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScheduledBlackoutDateId;
	protected $m_strBlackoutDate;
	protected $m_boolIsLeaseStartDate;
	protected $m_boolIsLeaseEndDate;
	protected $m_intIsMoveIn;
	protected $m_intIsMoveOut;
	protected $m_intIsAmenity;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLeaseStartDate = false;
		$this->m_boolIsLeaseEndDate = false;
		$this->m_intIsMoveIn = '0';
		$this->m_intIsMoveOut = '0';
		$this->m_intIsAmenity = '0';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['scheduled_blackout_date_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledBlackoutDateId', trim( $arrValues['scheduled_blackout_date_id'] ) ); elseif( isset( $arrValues['scheduled_blackout_date_id'] ) ) $this->setScheduledBlackoutDateId( $arrValues['scheduled_blackout_date_id'] );
		if( isset( $arrValues['blackout_date'] ) && $boolDirectSet ) $this->set( 'm_strBlackoutDate', trim( $arrValues['blackout_date'] ) ); elseif( isset( $arrValues['blackout_date'] ) ) $this->setBlackoutDate( $arrValues['blackout_date'] );
		if( isset( $arrValues['is_lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeaseStartDate', trim( stripcslashes( $arrValues['is_lease_start_date'] ) ) ); elseif( isset( $arrValues['is_lease_start_date'] ) ) $this->setIsLeaseStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_start_date'] ) : $arrValues['is_lease_start_date'] );
		if( isset( $arrValues['is_lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeaseEndDate', trim( stripcslashes( $arrValues['is_lease_end_date'] ) ) ); elseif( isset( $arrValues['is_lease_end_date'] ) ) $this->setIsLeaseEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_end_date'] ) : $arrValues['is_lease_end_date'] );
		if( isset( $arrValues['is_move_in'] ) && $boolDirectSet ) $this->set( 'm_intIsMoveIn', trim( $arrValues['is_move_in'] ) ); elseif( isset( $arrValues['is_move_in'] ) ) $this->setIsMoveIn( $arrValues['is_move_in'] );
		if( isset( $arrValues['is_move_out'] ) && $boolDirectSet ) $this->set( 'm_intIsMoveOut', trim( $arrValues['is_move_out'] ) ); elseif( isset( $arrValues['is_move_out'] ) ) $this->setIsMoveOut( $arrValues['is_move_out'] );
		if( isset( $arrValues['is_amenity'] ) && $boolDirectSet ) $this->set( 'm_intIsAmenity', trim( $arrValues['is_amenity'] ) ); elseif( isset( $arrValues['is_amenity'] ) ) $this->setIsAmenity( $arrValues['is_amenity'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScheduledBlackoutDateId( $intScheduledBlackoutDateId ) {
		$this->set( 'm_intScheduledBlackoutDateId', CStrings::strToIntDef( $intScheduledBlackoutDateId, NULL, false ) );
	}

	public function getScheduledBlackoutDateId() {
		return $this->m_intScheduledBlackoutDateId;
	}

	public function sqlScheduledBlackoutDateId() {
		return ( true == isset( $this->m_intScheduledBlackoutDateId ) ) ? ( string ) $this->m_intScheduledBlackoutDateId : 'NULL';
	}

	public function setBlackoutDate( $strBlackoutDate ) {
		$this->set( 'm_strBlackoutDate', CStrings::strTrimDef( $strBlackoutDate, -1, NULL, true ) );
	}

	public function getBlackoutDate() {
		return $this->m_strBlackoutDate;
	}

	public function sqlBlackoutDate() {
		return ( true == isset( $this->m_strBlackoutDate ) ) ? '\'' . $this->m_strBlackoutDate . '\'' : 'NOW()';
	}

	public function setIsLeaseStartDate( $boolIsLeaseStartDate ) {
		$this->set( 'm_boolIsLeaseStartDate', CStrings::strToBool( $boolIsLeaseStartDate ) );
	}

	public function getIsLeaseStartDate() {
		return $this->m_boolIsLeaseStartDate;
	}

	public function sqlIsLeaseStartDate() {
		return ( true == isset( $this->m_boolIsLeaseStartDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeaseStartDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLeaseEndDate( $boolIsLeaseEndDate ) {
		$this->set( 'm_boolIsLeaseEndDate', CStrings::strToBool( $boolIsLeaseEndDate ) );
	}

	public function getIsLeaseEndDate() {
		return $this->m_boolIsLeaseEndDate;
	}

	public function sqlIsLeaseEndDate() {
		return ( true == isset( $this->m_boolIsLeaseEndDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeaseEndDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMoveIn( $intIsMoveIn ) {
		$this->set( 'm_intIsMoveIn', CStrings::strToIntDef( $intIsMoveIn, NULL, false ) );
	}

	public function getIsMoveIn() {
		return $this->m_intIsMoveIn;
	}

	public function sqlIsMoveIn() {
		return ( true == isset( $this->m_intIsMoveIn ) ) ? ( string ) $this->m_intIsMoveIn : '0';
	}

	public function setIsMoveOut( $intIsMoveOut ) {
		$this->set( 'm_intIsMoveOut', CStrings::strToIntDef( $intIsMoveOut, NULL, false ) );
	}

	public function getIsMoveOut() {
		return $this->m_intIsMoveOut;
	}

	public function sqlIsMoveOut() {
		return ( true == isset( $this->m_intIsMoveOut ) ) ? ( string ) $this->m_intIsMoveOut : '0';
	}

	public function setIsAmenity( $intIsAmenity ) {
		$this->set( 'm_intIsAmenity', CStrings::strToIntDef( $intIsAmenity, NULL, false ) );
	}

	public function getIsAmenity() {
		return $this->m_intIsAmenity;
	}

	public function sqlIsAmenity() {
		return ( true == isset( $this->m_intIsAmenity ) ) ? ( string ) $this->m_intIsAmenity : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, scheduled_blackout_date_id, blackout_date, is_lease_start_date, is_lease_end_date, is_move_in, is_move_out, is_amenity, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlScheduledBlackoutDateId() . ', ' .
 						$this->sqlBlackoutDate() . ', ' .
 						$this->sqlIsLeaseStartDate() . ', ' .
 						$this->sqlIsLeaseEndDate() . ', ' .
 						$this->sqlIsMoveIn() . ', ' .
 						$this->sqlIsMoveOut() . ', ' .
 						$this->sqlIsAmenity() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_blackout_date_id = ' . $this->sqlScheduledBlackoutDateId() . ','; } elseif( true == array_key_exists( 'ScheduledBlackoutDateId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_blackout_date_id = ' . $this->sqlScheduledBlackoutDateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blackout_date = ' . $this->sqlBlackoutDate() . ','; } elseif( true == array_key_exists( 'BlackoutDate', $this->getChangedColumns() ) ) { $strSql .= ' blackout_date = ' . $this->sqlBlackoutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lease_start_date = ' . $this->sqlIsLeaseStartDate() . ','; } elseif( true == array_key_exists( 'IsLeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' is_lease_start_date = ' . $this->sqlIsLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lease_end_date = ' . $this->sqlIsLeaseEndDate() . ','; } elseif( true == array_key_exists( 'IsLeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' is_lease_end_date = ' . $this->sqlIsLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_in = ' . $this->sqlIsMoveIn() . ','; } elseif( true == array_key_exists( 'IsMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' is_move_in = ' . $this->sqlIsMoveIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; } elseif( true == array_key_exists( 'IsMoveOut', $this->getChangedColumns() ) ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_amenity = ' . $this->sqlIsAmenity() . ','; } elseif( true == array_key_exists( 'IsAmenity', $this->getChangedColumns() ) ) { $strSql .= ' is_amenity = ' . $this->sqlIsAmenity() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'scheduled_blackout_date_id' => $this->getScheduledBlackoutDateId(),
			'blackout_date' => $this->getBlackoutDate(),
			'is_lease_start_date' => $this->getIsLeaseStartDate(),
			'is_lease_end_date' => $this->getIsLeaseEndDate(),
			'is_move_in' => $this->getIsMoveIn(),
			'is_move_out' => $this->getIsMoveOut(),
			'is_amenity' => $this->getIsAmenity(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>