<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotificationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.notification_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intNotificationId;
	protected $m_intCompanyUserId;
	protected $m_strViewedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['notification_id'] ) && $boolDirectSet ) $this->set( 'm_intNotificationId', trim( $arrValues['notification_id'] ) ); elseif( isset( $arrValues['notification_id'] ) ) $this->setNotificationId( $arrValues['notification_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setNotificationId( $intNotificationId ) {
		$this->set( 'm_intNotificationId', CStrings::strToIntDef( $intNotificationId, NULL, false ) );
	}

	public function getNotificationId() {
		return $this->m_intNotificationId;
	}

	public function sqlNotificationId() {
		return ( true == isset( $this->m_intNotificationId ) ) ? ( string ) $this->m_intNotificationId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, notification_id, company_user_id, viewed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlNotificationId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlViewedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_id = ' . $this->sqlNotificationId() . ','; } elseif( true == array_key_exists( 'NotificationId', $this->getChangedColumns() ) ) { $strSql .= ' notification_id = ' . $this->sqlNotificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'notification_id' => $this->getNotificationId(),
			'company_user_id' => $this->getCompanyUserId(),
			'viewed_on' => $this->getViewedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>