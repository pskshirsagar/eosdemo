<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPet extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_pets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intPetTypeId;
	protected $m_intApplicantApplicationPetId;
	protected $m_intCustomerAnimalId;
	protected $m_strName;
	protected $m_strBreed;
	protected $m_strColor;
	protected $m_intWeight;
	protected $m_intAge;
	protected $m_strGender;
	protected $m_strDescription;
	protected $m_strLicenseCity;
	protected $m_strLicenseNumber;
	protected $m_strDateOfLastShots;
	protected $m_intIsHouseBroken;
	protected $m_intIsPetSpayedOrNeutered;
	protected $m_boolIsAssistanceAnimal;
	protected $m_strPetSpayedOrNeuteredDate;
	protected $m_strOwnersName;
	protected $m_strSpecialProvisions;
	protected $m_intPetNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsHouseBroken = '0';
		$this->m_boolIsAssistanceAnimal = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['pet_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPetTypeId', trim( $arrValues['pet_type_id'] ) ); elseif( isset( $arrValues['pet_type_id'] ) ) $this->setPetTypeId( $arrValues['pet_type_id'] );
		if( isset( $arrValues['applicant_application_pet_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationPetId', trim( $arrValues['applicant_application_pet_id'] ) ); elseif( isset( $arrValues['applicant_application_pet_id'] ) ) $this->setApplicantApplicationPetId( $arrValues['applicant_application_pet_id'] );
		if( isset( $arrValues['customer_animal_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerAnimalId', trim( $arrValues['customer_animal_id'] ) ); elseif( isset( $arrValues['customer_animal_id'] ) ) $this->setCustomerAnimalId( $arrValues['customer_animal_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['breed'] ) && $boolDirectSet ) $this->set( 'm_strBreed', trim( stripcslashes( $arrValues['breed'] ) ) ); elseif( isset( $arrValues['breed'] ) ) $this->setBreed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['breed'] ) : $arrValues['breed'] );
		if( isset( $arrValues['color'] ) && $boolDirectSet ) $this->set( 'm_strColor', trim( stripcslashes( $arrValues['color'] ) ) ); elseif( isset( $arrValues['color'] ) ) $this->setColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color'] ) : $arrValues['color'] );
		if( isset( $arrValues['weight'] ) && $boolDirectSet ) $this->set( 'm_intWeight', trim( $arrValues['weight'] ) ); elseif( isset( $arrValues['weight'] ) ) $this->setWeight( $arrValues['weight'] );
		if( isset( $arrValues['age'] ) && $boolDirectSet ) $this->set( 'm_intAge', trim( $arrValues['age'] ) ); elseif( isset( $arrValues['age'] ) ) $this->setAge( $arrValues['age'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( stripcslashes( $arrValues['gender'] ) ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gender'] ) : $arrValues['gender'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['license_city'] ) && $boolDirectSet ) $this->set( 'm_strLicenseCity', trim( stripcslashes( $arrValues['license_city'] ) ) ); elseif( isset( $arrValues['license_city'] ) ) $this->setLicenseCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['license_city'] ) : $arrValues['license_city'] );
		if( isset( $arrValues['license_number'] ) && $boolDirectSet ) $this->set( 'm_strLicenseNumber', trim( stripcslashes( $arrValues['license_number'] ) ) ); elseif( isset( $arrValues['license_number'] ) ) $this->setLicenseNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['license_number'] ) : $arrValues['license_number'] );
		if( isset( $arrValues['date_of_last_shots'] ) && $boolDirectSet ) $this->set( 'm_strDateOfLastShots', trim( $arrValues['date_of_last_shots'] ) ); elseif( isset( $arrValues['date_of_last_shots'] ) ) $this->setDateOfLastShots( $arrValues['date_of_last_shots'] );
		if( isset( $arrValues['is_house_broken'] ) && $boolDirectSet ) $this->set( 'm_intIsHouseBroken', trim( $arrValues['is_house_broken'] ) ); elseif( isset( $arrValues['is_house_broken'] ) ) $this->setIsHouseBroken( $arrValues['is_house_broken'] );
		if( isset( $arrValues['is_pet_spayed_or_neutered'] ) && $boolDirectSet ) $this->set( 'm_intIsPetSpayedOrNeutered', trim( $arrValues['is_pet_spayed_or_neutered'] ) ); elseif( isset( $arrValues['is_pet_spayed_or_neutered'] ) ) $this->setIsPetSpayedOrNeutered( $arrValues['is_pet_spayed_or_neutered'] );
		if( isset( $arrValues['is_assistance_animal'] ) && $boolDirectSet ) $this->set( 'm_boolIsAssistanceAnimal', trim( stripcslashes( $arrValues['is_assistance_animal'] ) ) ); elseif( isset( $arrValues['is_assistance_animal'] ) ) $this->setIsAssistanceAnimal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_assistance_animal'] ) : $arrValues['is_assistance_animal'] );
		if( isset( $arrValues['pet_spayed_or_neutered_date'] ) && $boolDirectSet ) $this->set( 'm_strPetSpayedOrNeuteredDate', trim( $arrValues['pet_spayed_or_neutered_date'] ) ); elseif( isset( $arrValues['pet_spayed_or_neutered_date'] ) ) $this->setPetSpayedOrNeuteredDate( $arrValues['pet_spayed_or_neutered_date'] );
		if( isset( $arrValues['owners_name'] ) && $boolDirectSet ) $this->set( 'm_strOwnersName', trim( stripcslashes( $arrValues['owners_name'] ) ) ); elseif( isset( $arrValues['owners_name'] ) ) $this->setOwnersName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['owners_name'] ) : $arrValues['owners_name'] );
		if( isset( $arrValues['special_provisions'] ) && $boolDirectSet ) $this->set( 'm_strSpecialProvisions', trim( stripcslashes( $arrValues['special_provisions'] ) ) ); elseif( isset( $arrValues['special_provisions'] ) ) $this->setSpecialProvisions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['special_provisions'] ) : $arrValues['special_provisions'] );
		if( isset( $arrValues['pet_number'] ) && $boolDirectSet ) $this->set( 'm_intPetNumber', trim( $arrValues['pet_number'] ) ); elseif( isset( $arrValues['pet_number'] ) ) $this->setPetNumber( $arrValues['pet_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPetTypeId( $intPetTypeId ) {
		$this->set( 'm_intPetTypeId', CStrings::strToIntDef( $intPetTypeId, NULL, false ) );
	}

	public function getPetTypeId() {
		return $this->m_intPetTypeId;
	}

	public function sqlPetTypeId() {
		return ( true == isset( $this->m_intPetTypeId ) ) ? ( string ) $this->m_intPetTypeId : 'NULL';
	}

	public function setApplicantApplicationPetId( $intApplicantApplicationPetId ) {
		$this->set( 'm_intApplicantApplicationPetId', CStrings::strToIntDef( $intApplicantApplicationPetId, NULL, false ) );
	}

	public function getApplicantApplicationPetId() {
		return $this->m_intApplicantApplicationPetId;
	}

	public function sqlApplicantApplicationPetId() {
		return ( true == isset( $this->m_intApplicantApplicationPetId ) ) ? ( string ) $this->m_intApplicantApplicationPetId : 'NULL';
	}

	public function setCustomerAnimalId( $intCustomerAnimalId ) {
		$this->set( 'm_intCustomerAnimalId', CStrings::strToIntDef( $intCustomerAnimalId, NULL, false ) );
	}

	public function getCustomerAnimalId() {
		return $this->m_intCustomerAnimalId;
	}

	public function sqlCustomerAnimalId() {
		return ( true == isset( $this->m_intCustomerAnimalId ) ) ? ( string ) $this->m_intCustomerAnimalId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBreed( $strBreed ) {
		$this->set( 'm_strBreed', CStrings::strTrimDef( $strBreed, 50, NULL, true ) );
	}

	public function getBreed() {
		return $this->m_strBreed;
	}

	public function sqlBreed() {
		return ( true == isset( $this->m_strBreed ) ) ? '\'' . addslashes( $this->m_strBreed ) . '\'' : 'NULL';
	}

	public function setColor( $strColor ) {
		$this->set( 'm_strColor', CStrings::strTrimDef( $strColor, 20, NULL, true ) );
	}

	public function getColor() {
		return $this->m_strColor;
	}

	public function sqlColor() {
		return ( true == isset( $this->m_strColor ) ) ? '\'' . addslashes( $this->m_strColor ) . '\'' : 'NULL';
	}

	public function setWeight( $intWeight ) {
		$this->set( 'm_intWeight', CStrings::strToIntDef( $intWeight, NULL, false ) );
	}

	public function getWeight() {
		return $this->m_intWeight;
	}

	public function sqlWeight() {
		return ( true == isset( $this->m_intWeight ) ) ? ( string ) $this->m_intWeight : 'NULL';
	}

	public function setAge( $intAge ) {
		$this->set( 'm_intAge', CStrings::strToIntDef( $intAge, NULL, false ) );
	}

	public function getAge() {
		return $this->m_intAge;
	}

	public function sqlAge() {
		return ( true == isset( $this->m_intAge ) ) ? ( string ) $this->m_intAge : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? '\'' . addslashes( $this->m_strGender ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setLicenseCity( $strLicenseCity ) {
		$this->set( 'm_strLicenseCity', CStrings::strTrimDef( $strLicenseCity, 50, NULL, true ) );
	}

	public function getLicenseCity() {
		return $this->m_strLicenseCity;
	}

	public function sqlLicenseCity() {
		return ( true == isset( $this->m_strLicenseCity ) ) ? '\'' . addslashes( $this->m_strLicenseCity ) . '\'' : 'NULL';
	}

	public function setLicenseNumber( $strLicenseNumber ) {
		$this->set( 'm_strLicenseNumber', CStrings::strTrimDef( $strLicenseNumber, 240, NULL, true ) );
	}

	public function getLicenseNumber() {
		return $this->m_strLicenseNumber;
	}

	public function sqlLicenseNumber() {
		return ( true == isset( $this->m_strLicenseNumber ) ) ? '\'' . addslashes( $this->m_strLicenseNumber ) . '\'' : 'NULL';
	}

	public function setDateOfLastShots( $strDateOfLastShots ) {
		$this->set( 'm_strDateOfLastShots', CStrings::strTrimDef( $strDateOfLastShots, -1, NULL, true ) );
	}

	public function getDateOfLastShots() {
		return $this->m_strDateOfLastShots;
	}

	public function sqlDateOfLastShots() {
		return ( true == isset( $this->m_strDateOfLastShots ) ) ? '\'' . $this->m_strDateOfLastShots . '\'' : 'NULL';
	}

	public function setIsHouseBroken( $intIsHouseBroken ) {
		$this->set( 'm_intIsHouseBroken', CStrings::strToIntDef( $intIsHouseBroken, NULL, false ) );
	}

	public function getIsHouseBroken() {
		return $this->m_intIsHouseBroken;
	}

	public function sqlIsHouseBroken() {
		return ( true == isset( $this->m_intIsHouseBroken ) ) ? ( string ) $this->m_intIsHouseBroken : '0';
	}

	public function setIsPetSpayedOrNeutered( $intIsPetSpayedOrNeutered ) {
		$this->set( 'm_intIsPetSpayedOrNeutered', CStrings::strToIntDef( $intIsPetSpayedOrNeutered, NULL, false ) );
	}

	public function getIsPetSpayedOrNeutered() {
		return $this->m_intIsPetSpayedOrNeutered;
	}

	public function sqlIsPetSpayedOrNeutered() {
		return ( true == isset( $this->m_intIsPetSpayedOrNeutered ) ) ? ( string ) $this->m_intIsPetSpayedOrNeutered : 'NULL';
	}

	public function setIsAssistanceAnimal( $boolIsAssistanceAnimal ) {
		$this->set( 'm_boolIsAssistanceAnimal', CStrings::strToBool( $boolIsAssistanceAnimal ) );
	}

	public function getIsAssistanceAnimal() {
		return $this->m_boolIsAssistanceAnimal;
	}

	public function sqlIsAssistanceAnimal() {
		return ( true == isset( $this->m_boolIsAssistanceAnimal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAssistanceAnimal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPetSpayedOrNeuteredDate( $strPetSpayedOrNeuteredDate ) {
		$this->set( 'm_strPetSpayedOrNeuteredDate', CStrings::strTrimDef( $strPetSpayedOrNeuteredDate, -1, NULL, true ) );
	}

	public function getPetSpayedOrNeuteredDate() {
		return $this->m_strPetSpayedOrNeuteredDate;
	}

	public function sqlPetSpayedOrNeuteredDate() {
		return ( true == isset( $this->m_strPetSpayedOrNeuteredDate ) ) ? '\'' . $this->m_strPetSpayedOrNeuteredDate . '\'' : 'NULL';
	}

	public function setOwnersName( $strOwnersName ) {
		$this->set( 'm_strOwnersName', CStrings::strTrimDef( $strOwnersName, 100, NULL, true ) );
	}

	public function getOwnersName() {
		return $this->m_strOwnersName;
	}

	public function sqlOwnersName() {
		return ( true == isset( $this->m_strOwnersName ) ) ? '\'' . addslashes( $this->m_strOwnersName ) . '\'' : 'NULL';
	}

	public function setSpecialProvisions( $strSpecialProvisions ) {
		$this->set( 'm_strSpecialProvisions', CStrings::strTrimDef( $strSpecialProvisions, 2000, NULL, true ) );
	}

	public function getSpecialProvisions() {
		return $this->m_strSpecialProvisions;
	}

	public function sqlSpecialProvisions() {
		return ( true == isset( $this->m_strSpecialProvisions ) ) ? '\'' . addslashes( $this->m_strSpecialProvisions ) . '\'' : 'NULL';
	}

	public function setPetNumber( $intPetNumber ) {
		$this->set( 'm_intPetNumber', CStrings::strToIntDef( $intPetNumber, NULL, false ) );
	}

	public function getPetNumber() {
		return $this->m_intPetNumber;
	}

	public function sqlPetNumber() {
		return ( true == isset( $this->m_intPetNumber ) ) ? ( string ) $this->m_intPetNumber : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, pet_type_id, applicant_application_pet_id, customer_animal_id, name, breed, color, weight, age, gender, description, license_city, license_number, date_of_last_shots, is_house_broken, is_pet_spayed_or_neutered, is_assistance_animal, pet_spayed_or_neutered_date, owners_name, special_provisions, pet_number, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlPetTypeId() . ', ' .
 						$this->sqlApplicantApplicationPetId() . ', ' .
 						$this->sqlCustomerAnimalId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlBreed() . ', ' .
 						$this->sqlColor() . ', ' .
 						$this->sqlWeight() . ', ' .
 						$this->sqlAge() . ', ' .
 						$this->sqlGender() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlLicenseCity() . ', ' .
 						$this->sqlLicenseNumber() . ', ' .
 						$this->sqlDateOfLastShots() . ', ' .
 						$this->sqlIsHouseBroken() . ', ' .
 						$this->sqlIsPetSpayedOrNeutered() . ', ' .
 						$this->sqlIsAssistanceAnimal() . ', ' .
 						$this->sqlPetSpayedOrNeuteredDate() . ', ' .
 						$this->sqlOwnersName() . ', ' .
 						$this->sqlSpecialProvisions() . ', ' .
 						$this->sqlPetNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_type_id = ' . $this->sqlPetTypeId() . ','; } elseif( true == array_key_exists( 'PetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' pet_type_id = ' . $this->sqlPetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_pet_id = ' . $this->sqlApplicantApplicationPetId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationPetId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_pet_id = ' . $this->sqlApplicantApplicationPetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_animal_id = ' . $this->sqlCustomerAnimalId() . ','; } elseif( true == array_key_exists( 'CustomerAnimalId', $this->getChangedColumns() ) ) { $strSql .= ' customer_animal_id = ' . $this->sqlCustomerAnimalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' breed = ' . $this->sqlBreed() . ','; } elseif( true == array_key_exists( 'Breed', $this->getChangedColumns() ) ) { $strSql .= ' breed = ' . $this->sqlBreed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color = ' . $this->sqlColor() . ','; } elseif( true == array_key_exists( 'Color', $this->getChangedColumns() ) ) { $strSql .= ' color = ' . $this->sqlColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; } elseif( true == array_key_exists( 'Weight', $this->getChangedColumns() ) ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' age = ' . $this->sqlAge() . ','; } elseif( true == array_key_exists( 'Age', $this->getChangedColumns() ) ) { $strSql .= ' age = ' . $this->sqlAge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' license_city = ' . $this->sqlLicenseCity() . ','; } elseif( true == array_key_exists( 'LicenseCity', $this->getChangedColumns() ) ) { $strSql .= ' license_city = ' . $this->sqlLicenseCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' license_number = ' . $this->sqlLicenseNumber() . ','; } elseif( true == array_key_exists( 'LicenseNumber', $this->getChangedColumns() ) ) { $strSql .= ' license_number = ' . $this->sqlLicenseNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_of_last_shots = ' . $this->sqlDateOfLastShots() . ','; } elseif( true == array_key_exists( 'DateOfLastShots', $this->getChangedColumns() ) ) { $strSql .= ' date_of_last_shots = ' . $this->sqlDateOfLastShots() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_house_broken = ' . $this->sqlIsHouseBroken() . ','; } elseif( true == array_key_exists( 'IsHouseBroken', $this->getChangedColumns() ) ) { $strSql .= ' is_house_broken = ' . $this->sqlIsHouseBroken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pet_spayed_or_neutered = ' . $this->sqlIsPetSpayedOrNeutered() . ','; } elseif( true == array_key_exists( 'IsPetSpayedOrNeutered', $this->getChangedColumns() ) ) { $strSql .= ' is_pet_spayed_or_neutered = ' . $this->sqlIsPetSpayedOrNeutered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_assistance_animal = ' . $this->sqlIsAssistanceAnimal() . ','; } elseif( true == array_key_exists( 'IsAssistanceAnimal', $this->getChangedColumns() ) ) { $strSql .= ' is_assistance_animal = ' . $this->sqlIsAssistanceAnimal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_spayed_or_neutered_date = ' . $this->sqlPetSpayedOrNeuteredDate() . ','; } elseif( true == array_key_exists( 'PetSpayedOrNeuteredDate', $this->getChangedColumns() ) ) { $strSql .= ' pet_spayed_or_neutered_date = ' . $this->sqlPetSpayedOrNeuteredDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owners_name = ' . $this->sqlOwnersName() . ','; } elseif( true == array_key_exists( 'OwnersName', $this->getChangedColumns() ) ) { $strSql .= ' owners_name = ' . $this->sqlOwnersName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_provisions = ' . $this->sqlSpecialProvisions() . ','; } elseif( true == array_key_exists( 'SpecialProvisions', $this->getChangedColumns() ) ) { $strSql .= ' special_provisions = ' . $this->sqlSpecialProvisions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_number = ' . $this->sqlPetNumber() . ','; } elseif( true == array_key_exists( 'PetNumber', $this->getChangedColumns() ) ) { $strSql .= ' pet_number = ' . $this->sqlPetNumber() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'pet_type_id' => $this->getPetTypeId(),
			'applicant_application_pet_id' => $this->getApplicantApplicationPetId(),
			'customer_animal_id' => $this->getCustomerAnimalId(),
			'name' => $this->getName(),
			'breed' => $this->getBreed(),
			'color' => $this->getColor(),
			'weight' => $this->getWeight(),
			'age' => $this->getAge(),
			'gender' => $this->getGender(),
			'description' => $this->getDescription(),
			'license_city' => $this->getLicenseCity(),
			'license_number' => $this->getLicenseNumber(),
			'date_of_last_shots' => $this->getDateOfLastShots(),
			'is_house_broken' => $this->getIsHouseBroken(),
			'is_pet_spayed_or_neutered' => $this->getIsPetSpayedOrNeutered(),
			'is_assistance_animal' => $this->getIsAssistanceAnimal(),
			'pet_spayed_or_neutered_date' => $this->getPetSpayedOrNeuteredDate(),
			'owners_name' => $this->getOwnersName(),
			'special_provisions' => $this->getSpecialProvisions(),
			'pet_number' => $this->getPetNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>