<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePrivacyDataRequestStatus extends CEosSingularBase {

	const TABLE_NAME = 'public.privacy_data_request_statuses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPrivacyDataRequestId;
	protected $m_strStatus;
	protected $m_strStatusDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['privacy_data_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPrivacyDataRequestId', trim( $arrValues['privacy_data_request_id'] ) ); elseif( isset( $arrValues['privacy_data_request_id'] ) ) $this->setPrivacyDataRequestId( $arrValues['privacy_data_request_id'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['status_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStatusDatetime', trim( $arrValues['status_datetime'] ) ); elseif( isset( $arrValues['status_datetime'] ) ) $this->setStatusDatetime( $arrValues['status_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPrivacyDataRequestId( $intPrivacyDataRequestId ) {
		$this->set( 'm_intPrivacyDataRequestId', CStrings::strToIntDef( $intPrivacyDataRequestId, NULL, false ) );
	}

	public function getPrivacyDataRequestId() {
		return $this->m_intPrivacyDataRequestId;
	}

	public function sqlPrivacyDataRequestId() {
		return ( true == isset( $this->m_intPrivacyDataRequestId ) ) ? ( string ) $this->m_intPrivacyDataRequestId : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, -1, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStatus ) : '\'' . addslashes( $this->m_strStatus ) . '\'' ) : 'NULL';
	}

	public function setStatusDatetime( $strStatusDatetime ) {
		$this->set( 'm_strStatusDatetime', CStrings::strTrimDef( $strStatusDatetime, -1, NULL, true ) );
	}

	public function getStatusDatetime() {
		return $this->m_strStatusDatetime;
	}

	public function sqlStatusDatetime() {
		return ( true == isset( $this->m_strStatusDatetime ) ) ? '\'' . $this->m_strStatusDatetime . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, privacy_data_request_id, status, status_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPrivacyDataRequestId() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlStatusDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' privacy_data_request_id = ' . $this->sqlPrivacyDataRequestId(). ',' ; } elseif( true == array_key_exists( 'PrivacyDataRequestId', $this->getChangedColumns() ) ) { $strSql .= ' privacy_data_request_id = ' . $this->sqlPrivacyDataRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_datetime = ' . $this->sqlStatusDatetime() ; } elseif( true == array_key_exists( 'StatusDatetime', $this->getChangedColumns() ) ) { $strSql .= ' status_datetime = ' . $this->sqlStatusDatetime() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'privacy_data_request_id' => $this->getPrivacyDataRequestId(),
			'status' => $this->getStatus(),
			'status_datetime' => $this->getStatusDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>