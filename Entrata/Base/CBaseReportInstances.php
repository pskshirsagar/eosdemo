<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportInstances
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportInstances extends CEosPluralBase {

	/**
	 * @return CReportInstance[]
	 */
	public static function fetchReportInstances( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportInstance::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportInstance
	 */
	public static function fetchReportInstance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportInstance::class, $objDatabase );
	}

	public static function fetchReportInstanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_instances', $objDatabase );
	}

	public static function fetchReportInstanceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportInstance( sprintf( 'SELECT * FROM report_instances WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByCid( $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE report_group_id = %d AND cid = %d', $intReportGroupId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE report_id = %d AND cid = %d', $intReportId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByReportVersionIdByCid( $intReportVersionId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE report_version_id = %d AND cid = %d', $intReportVersionId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE module_id = %d AND cid = %d', $intModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByParentModuleIdByCid( $intParentModuleId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE parent_module_id = %d AND cid = %d', $intParentModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE report_filter_id = %d AND cid = %d', $intReportFilterId, $intCid ), $objDatabase );
	}

	public static function fetchReportInstancesByDefaultReportInstanceIdByCid( $intDefaultReportInstanceId, $intCid, $objDatabase ) {
		return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE default_report_instance_id = %d AND cid = %d', $intDefaultReportInstanceId, $intCid ), $objDatabase );
	}

}
?>