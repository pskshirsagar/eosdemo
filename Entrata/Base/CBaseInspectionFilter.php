<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionFilter extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.inspection_filters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_strPropertyGroupIds;
	protected $m_strInspectionFormIds;
	protected $m_strInspectionStatusIds;
	protected $m_strCompanyEmployeeIds;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strKeywordSearch;
	protected $m_strSortBy;
	protected $m_strSortDirection;
	protected $m_strCreatedStartDate;
	protected $m_strCreatedEndDate;
	protected $m_strInspectedStartDate;
	protected $m_strInspectedEndDate;
	protected $m_strCompletedStartDate;
	protected $m_strCompletedEndDate;
	protected $m_strScheduledStartDate;
	protected $m_strScheduledEndDate;
	protected $m_boolFilterByDays;
	protected $m_intInspectionWithin;
	protected $m_strInspectionWithinDays;
	protected $m_strDueStartDate;
	protected $m_strDueEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strSortDirection = 'DESC';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['property_group_ids'] ) && $boolDirectSet ) $this->set( 'm_strPropertyGroupIds', trim( stripcslashes( $arrValues['property_group_ids'] ) ) ); elseif( isset( $arrValues['property_group_ids'] ) ) $this->setPropertyGroupIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_group_ids'] ) : $arrValues['property_group_ids'] );
		if( isset( $arrValues['inspection_form_ids'] ) && $boolDirectSet ) $this->set( 'm_strInspectionFormIds', trim( stripcslashes( $arrValues['inspection_form_ids'] ) ) ); elseif( isset( $arrValues['inspection_form_ids'] ) ) $this->setInspectionFormIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['inspection_form_ids'] ) : $arrValues['inspection_form_ids'] );
		if( isset( $arrValues['inspection_status_ids'] ) && $boolDirectSet ) $this->set( 'm_strInspectionStatusIds', trim( stripcslashes( $arrValues['inspection_status_ids'] ) ) ); elseif( isset( $arrValues['inspection_status_ids'] ) ) $this->setInspectionStatusIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['inspection_status_ids'] ) : $arrValues['inspection_status_ids'] );
		if( isset( $arrValues['company_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEmployeeIds', trim( stripcslashes( $arrValues['company_employee_ids'] ) ) ); elseif( isset( $arrValues['company_employee_ids'] ) ) $this->setCompanyEmployeeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_employee_ids'] ) : $arrValues['company_employee_ids'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['keyword_search'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strKeywordSearch', trim( stripcslashes( $arrValues['keyword_search'] ) ) ); elseif( isset( $arrValues['keyword_search'] ) ) $this->setKeywordSearch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keyword_search'] ) : $arrValues['keyword_search'] );
		if( isset( $arrValues['sort_by'] ) && $boolDirectSet ) $this->set( 'm_strSortBy', trim( stripcslashes( $arrValues['sort_by'] ) ) ); elseif( isset( $arrValues['sort_by'] ) ) $this->setSortBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_by'] ) : $arrValues['sort_by'] );
		if( isset( $arrValues['sort_direction'] ) && $boolDirectSet ) $this->set( 'm_strSortDirection', trim( stripcslashes( $arrValues['sort_direction'] ) ) ); elseif( isset( $arrValues['sort_direction'] ) ) $this->setSortDirection( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_direction'] ) : $arrValues['sort_direction'] );
		if( isset( $arrValues['created_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCreatedStartDate', trim( $arrValues['created_start_date'] ) ); elseif( isset( $arrValues['created_start_date'] ) ) $this->setCreatedStartDate( $arrValues['created_start_date'] );
		if( isset( $arrValues['created_end_date'] ) && $boolDirectSet ) $this->set( 'm_strCreatedEndDate', trim( $arrValues['created_end_date'] ) ); elseif( isset( $arrValues['created_end_date'] ) ) $this->setCreatedEndDate( $arrValues['created_end_date'] );
		if( isset( $arrValues['inspected_start_date'] ) && $boolDirectSet ) $this->set( 'm_strInspectedStartDate', trim( $arrValues['inspected_start_date'] ) ); elseif( isset( $arrValues['inspected_start_date'] ) ) $this->setInspectedStartDate( $arrValues['inspected_start_date'] );
		if( isset( $arrValues['inspected_end_date'] ) && $boolDirectSet ) $this->set( 'm_strInspectedEndDate', trim( $arrValues['inspected_end_date'] ) ); elseif( isset( $arrValues['inspected_end_date'] ) ) $this->setInspectedEndDate( $arrValues['inspected_end_date'] );
		if( isset( $arrValues['completed_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCompletedStartDate', trim( $arrValues['completed_start_date'] ) ); elseif( isset( $arrValues['completed_start_date'] ) ) $this->setCompletedStartDate( $arrValues['completed_start_date'] );
		if( isset( $arrValues['completed_end_date'] ) && $boolDirectSet ) $this->set( 'm_strCompletedEndDate', trim( $arrValues['completed_end_date'] ) ); elseif( isset( $arrValues['completed_end_date'] ) ) $this->setCompletedEndDate( $arrValues['completed_end_date'] );
		if( isset( $arrValues['scheduled_start_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartDate', trim( $arrValues['scheduled_start_date'] ) ); elseif( isset( $arrValues['scheduled_start_date'] ) ) $this->setScheduledStartDate( $arrValues['scheduled_start_date'] );
		if( isset( $arrValues['scheduled_end_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndDate', trim( $arrValues['scheduled_end_date'] ) ); elseif( isset( $arrValues['scheduled_end_date'] ) ) $this->setScheduledEndDate( $arrValues['scheduled_end_date'] );
		if( isset( $arrValues['filter_by_days'] ) && $boolDirectSet ) $this->set( 'm_boolFilterByDays', trim( stripcslashes( $arrValues['filter_by_days'] ) ) ); elseif( isset( $arrValues['filter_by_days'] ) ) $this->setFilterByDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_by_days'] ) : $arrValues['filter_by_days'] );
		if( isset( $arrValues['inspection_within'] ) && $boolDirectSet ) $this->set( 'm_intInspectionWithin', trim( $arrValues['inspection_within'] ) ); elseif( isset( $arrValues['inspection_within'] ) ) $this->setInspectionWithin( $arrValues['inspection_within'] );
		if( isset( $arrValues['inspection_within_days'] ) && $boolDirectSet ) $this->set( 'm_strInspectionWithinDays', trim( stripcslashes( $arrValues['inspection_within_days'] ) ) ); elseif( isset( $arrValues['inspection_within_days'] ) ) $this->setInspectionWithinDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['inspection_within_days'] ) : $arrValues['inspection_within_days'] );
		if( isset( $arrValues['due_start_date'] ) && $boolDirectSet ) $this->set( 'm_strDueStartDate', trim( $arrValues['due_start_date'] ) ); elseif( isset( $arrValues['due_start_date'] ) ) $this->setDueStartDate( $arrValues['due_start_date'] );
		if( isset( $arrValues['due_end_date'] ) && $boolDirectSet ) $this->set( 'm_strDueEndDate', trim( $arrValues['due_end_date'] ) ); elseif( isset( $arrValues['due_end_date'] ) ) $this->setDueEndDate( $arrValues['due_end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setPropertyGroupIds( $strPropertyGroupIds ) {
		$this->set( 'm_strPropertyGroupIds', CStrings::strTrimDef( $strPropertyGroupIds, 5000, NULL, true ) );
	}

	public function getPropertyGroupIds() {
		return $this->m_strPropertyGroupIds;
	}

	public function sqlPropertyGroupIds() {
		return ( true == isset( $this->m_strPropertyGroupIds ) ) ? '\'' . addslashes( $this->m_strPropertyGroupIds ) . '\'' : 'NULL';
	}

	public function setInspectionFormIds( $strInspectionFormIds ) {
		$this->set( 'm_strInspectionFormIds', CStrings::strTrimDef( $strInspectionFormIds, 5000, NULL, true ) );
	}

	public function getInspectionFormIds() {
		return $this->m_strInspectionFormIds;
	}

	public function sqlInspectionFormIds() {
		return ( true == isset( $this->m_strInspectionFormIds ) ) ? '\'' . addslashes( $this->m_strInspectionFormIds ) . '\'' : 'NULL';
	}

	public function setInspectionStatusIds( $strInspectionStatusIds ) {
		$this->set( 'm_strInspectionStatusIds', CStrings::strTrimDef( $strInspectionStatusIds, 2000, NULL, true ) );
	}

	public function getInspectionStatusIds() {
		return $this->m_strInspectionStatusIds;
	}

	public function sqlInspectionStatusIds() {
		return ( true == isset( $this->m_strInspectionStatusIds ) ) ? '\'' . addslashes( $this->m_strInspectionStatusIds ) . '\'' : 'NULL';
	}

	public function setCompanyEmployeeIds( $strCompanyEmployeeIds ) {
		$this->set( 'm_strCompanyEmployeeIds', CStrings::strTrimDef( $strCompanyEmployeeIds, 5000, NULL, true ) );
	}

	public function getCompanyEmployeeIds() {
		return $this->m_strCompanyEmployeeIds;
	}

	public function sqlCompanyEmployeeIds() {
		return ( true == isset( $this->m_strCompanyEmployeeIds ) ) ? '\'' . addslashes( $this->m_strCompanyEmployeeIds ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setKeywordSearch( $strKeywordSearch, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strKeywordSearch', CStrings::strTrimDef( $strKeywordSearch, 240, NULL, true ), $strLocaleCode );
	}

	public function getKeywordSearch( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strKeywordSearch', $strLocaleCode );
	}

	public function sqlKeywordSearch() {
		return ( true == isset( $this->m_strKeywordSearch ) ) ? '\'' . addslashes( $this->m_strKeywordSearch ) . '\'' : 'NULL';
	}

	public function setSortBy( $strSortBy ) {
		$this->set( 'm_strSortBy', CStrings::strTrimDef( $strSortBy, 240, NULL, true ) );
	}

	public function getSortBy() {
		return $this->m_strSortBy;
	}

	public function sqlSortBy() {
		return ( true == isset( $this->m_strSortBy ) ) ? '\'' . addslashes( $this->m_strSortBy ) . '\'' : 'NULL';
	}

	public function setSortDirection( $strSortDirection ) {
		$this->set( 'm_strSortDirection', CStrings::strTrimDef( $strSortDirection, 240, NULL, true ) );
	}

	public function getSortDirection() {
		return $this->m_strSortDirection;
	}

	public function sqlSortDirection() {
		return ( true == isset( $this->m_strSortDirection ) ) ? '\'' . addslashes( $this->m_strSortDirection ) . '\'' : '\'DESC\'';
	}

	public function setCreatedStartDate( $strCreatedStartDate ) {
		$this->set( 'm_strCreatedStartDate', CStrings::strTrimDef( $strCreatedStartDate, -1, NULL, true ) );
	}

	public function getCreatedStartDate() {
		return $this->m_strCreatedStartDate;
	}

	public function sqlCreatedStartDate() {
		return ( true == isset( $this->m_strCreatedStartDate ) ) ? '\'' . $this->m_strCreatedStartDate . '\'' : 'NULL';
	}

	public function setCreatedEndDate( $strCreatedEndDate ) {
		$this->set( 'm_strCreatedEndDate', CStrings::strTrimDef( $strCreatedEndDate, -1, NULL, true ) );
	}

	public function getCreatedEndDate() {
		return $this->m_strCreatedEndDate;
	}

	public function sqlCreatedEndDate() {
		return ( true == isset( $this->m_strCreatedEndDate ) ) ? '\'' . $this->m_strCreatedEndDate . '\'' : 'NULL';
	}

	public function setInspectedStartDate( $strInspectedStartDate ) {
		$this->set( 'm_strInspectedStartDate', CStrings::strTrimDef( $strInspectedStartDate, -1, NULL, true ) );
	}

	public function getInspectedStartDate() {
		return $this->m_strInspectedStartDate;
	}

	public function sqlInspectedStartDate() {
		return ( true == isset( $this->m_strInspectedStartDate ) ) ? '\'' . $this->m_strInspectedStartDate . '\'' : 'NULL';
	}

	public function setInspectedEndDate( $strInspectedEndDate ) {
		$this->set( 'm_strInspectedEndDate', CStrings::strTrimDef( $strInspectedEndDate, -1, NULL, true ) );
	}

	public function getInspectedEndDate() {
		return $this->m_strInspectedEndDate;
	}

	public function sqlInspectedEndDate() {
		return ( true == isset( $this->m_strInspectedEndDate ) ) ? '\'' . $this->m_strInspectedEndDate . '\'' : 'NULL';
	}

	public function setCompletedStartDate( $strCompletedStartDate ) {
		$this->set( 'm_strCompletedStartDate', CStrings::strTrimDef( $strCompletedStartDate, -1, NULL, true ) );
	}

	public function getCompletedStartDate() {
		return $this->m_strCompletedStartDate;
	}

	public function sqlCompletedStartDate() {
		return ( true == isset( $this->m_strCompletedStartDate ) ) ? '\'' . $this->m_strCompletedStartDate . '\'' : 'NULL';
	}

	public function setCompletedEndDate( $strCompletedEndDate ) {
		$this->set( 'm_strCompletedEndDate', CStrings::strTrimDef( $strCompletedEndDate, -1, NULL, true ) );
	}

	public function getCompletedEndDate() {
		return $this->m_strCompletedEndDate;
	}

	public function sqlCompletedEndDate() {
		return ( true == isset( $this->m_strCompletedEndDate ) ) ? '\'' . $this->m_strCompletedEndDate . '\'' : 'NULL';
	}

	public function setScheduledStartDate( $strScheduledStartDate ) {
		$this->set( 'm_strScheduledStartDate', CStrings::strTrimDef( $strScheduledStartDate, -1, NULL, true ) );
	}

	public function getScheduledStartDate() {
		return $this->m_strScheduledStartDate;
	}

	public function sqlScheduledStartDate() {
		return ( true == isset( $this->m_strScheduledStartDate ) ) ? '\'' . $this->m_strScheduledStartDate . '\'' : 'NULL';
	}

	public function setScheduledEndDate( $strScheduledEndDate ) {
		$this->set( 'm_strScheduledEndDate', CStrings::strTrimDef( $strScheduledEndDate, -1, NULL, true ) );
	}

	public function getScheduledEndDate() {
		return $this->m_strScheduledEndDate;
	}

	public function sqlScheduledEndDate() {
		return ( true == isset( $this->m_strScheduledEndDate ) ) ? '\'' . $this->m_strScheduledEndDate . '\'' : 'NULL';
	}

	public function setFilterByDays( $boolFilterByDays ) {
		$this->set( 'm_boolFilterByDays', CStrings::strToBool( $boolFilterByDays ) );
	}

	public function getFilterByDays() {
		return $this->m_boolFilterByDays;
	}

	public function sqlFilterByDays() {
		return ( true == isset( $this->m_boolFilterByDays ) ) ? '\'' . ( true == ( bool ) $this->m_boolFilterByDays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInspectionWithin( $intInspectionWithin ) {
		$this->set( 'm_intInspectionWithin', CStrings::strToIntDef( $intInspectionWithin, NULL, false ) );
	}

	public function getInspectionWithin() {
		return $this->m_intInspectionWithin;
	}

	public function sqlInspectionWithin() {
		return ( true == isset( $this->m_intInspectionWithin ) ) ? ( string ) $this->m_intInspectionWithin : 'NULL';
	}

	public function setInspectionWithinDays( $strInspectionWithinDays ) {
		$this->set( 'm_strInspectionWithinDays', CStrings::strTrimDef( $strInspectionWithinDays, 50, NULL, true ) );
	}

	public function getInspectionWithinDays() {
		return $this->m_strInspectionWithinDays;
	}

	public function sqlInspectionWithinDays() {
		return ( true == isset( $this->m_strInspectionWithinDays ) ) ? '\'' . addslashes( $this->m_strInspectionWithinDays ) . '\'' : 'NULL';
	}

	public function setDueStartDate( $strDueStartDate ) {
		$this->set( 'm_strDueStartDate', CStrings::strTrimDef( $strDueStartDate, -1, NULL, true ) );
	}

	public function getDueStartDate() {
		return $this->m_strDueStartDate;
	}

	public function sqlDueStartDate() {
		return ( true == isset( $this->m_strDueStartDate ) ) ? '\'' . $this->m_strDueStartDate . '\'' : 'NULL';
	}

	public function setDueEndDate( $strDueEndDate ) {
		$this->set( 'm_strDueEndDate', CStrings::strTrimDef( $strDueEndDate, -1, NULL, true ) );
	}

	public function getDueEndDate() {
		return $this->m_strDueEndDate;
	}

	public function sqlDueEndDate() {
		return ( true == isset( $this->m_strDueEndDate ) ) ? '\'' . $this->m_strDueEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, property_group_ids, inspection_form_ids, inspection_status_ids, company_employee_ids, title, description, keyword_search, sort_by, sort_direction, created_start_date, created_end_date, inspected_start_date, inspected_end_date, completed_start_date, completed_end_date, scheduled_start_date, scheduled_end_date, filter_by_days, inspection_within, inspection_within_days, due_start_date, due_end_date, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlPropertyGroupIds() . ', ' .
						$this->sqlInspectionFormIds() . ', ' .
						$this->sqlInspectionStatusIds() . ', ' .
						$this->sqlCompanyEmployeeIds() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlKeywordSearch() . ', ' .
						$this->sqlSortBy() . ', ' .
						$this->sqlSortDirection() . ', ' .
						$this->sqlCreatedStartDate() . ', ' .
						$this->sqlCreatedEndDate() . ', ' .
						$this->sqlInspectedStartDate() . ', ' .
						$this->sqlInspectedEndDate() . ', ' .
						$this->sqlCompletedStartDate() . ', ' .
						$this->sqlCompletedEndDate() . ', ' .
						$this->sqlScheduledStartDate() . ', ' .
						$this->sqlScheduledEndDate() . ', ' .
						$this->sqlFilterByDays() . ', ' .
						$this->sqlInspectionWithin() . ', ' .
						$this->sqlInspectionWithinDays() . ', ' .
						$this->sqlDueStartDate() . ', ' .
						$this->sqlDueEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds(). ',' ; } elseif( true == array_key_exists( 'PropertyGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_form_ids = ' . $this->sqlInspectionFormIds(). ',' ; } elseif( true == array_key_exists( 'InspectionFormIds', $this->getChangedColumns() ) ) { $strSql .= ' inspection_form_ids = ' . $this->sqlInspectionFormIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_status_ids = ' . $this->sqlInspectionStatusIds(). ',' ; } elseif( true == array_key_exists( 'InspectionStatusIds', $this->getChangedColumns() ) ) { $strSql .= ' inspection_status_ids = ' . $this->sqlInspectionStatusIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_ids = ' . $this->sqlCompanyEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_ids = ' . $this->sqlCompanyEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword_search = ' . $this->sqlKeywordSearch(). ',' ; } elseif( true == array_key_exists( 'KeywordSearch', $this->getChangedColumns() ) ) { $strSql .= ' keyword_search = ' . $this->sqlKeywordSearch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_by = ' . $this->sqlSortBy(). ',' ; } elseif( true == array_key_exists( 'SortBy', $this->getChangedColumns() ) ) { $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection(). ',' ; } elseif( true == array_key_exists( 'SortDirection', $this->getChangedColumns() ) ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_start_date = ' . $this->sqlCreatedStartDate(). ',' ; } elseif( true == array_key_exists( 'CreatedStartDate', $this->getChangedColumns() ) ) { $strSql .= ' created_start_date = ' . $this->sqlCreatedStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_end_date = ' . $this->sqlCreatedEndDate(). ',' ; } elseif( true == array_key_exists( 'CreatedEndDate', $this->getChangedColumns() ) ) { $strSql .= ' created_end_date = ' . $this->sqlCreatedEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspected_start_date = ' . $this->sqlInspectedStartDate(). ',' ; } elseif( true == array_key_exists( 'InspectedStartDate', $this->getChangedColumns() ) ) { $strSql .= ' inspected_start_date = ' . $this->sqlInspectedStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspected_end_date = ' . $this->sqlInspectedEndDate(). ',' ; } elseif( true == array_key_exists( 'InspectedEndDate', $this->getChangedColumns() ) ) { $strSql .= ' inspected_end_date = ' . $this->sqlInspectedEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_start_date = ' . $this->sqlCompletedStartDate(). ',' ; } elseif( true == array_key_exists( 'CompletedStartDate', $this->getChangedColumns() ) ) { $strSql .= ' completed_start_date = ' . $this->sqlCompletedStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_end_date = ' . $this->sqlCompletedEndDate(). ',' ; } elseif( true == array_key_exists( 'CompletedEndDate', $this->getChangedColumns() ) ) { $strSql .= ' completed_end_date = ' . $this->sqlCompletedEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_start_date = ' . $this->sqlScheduledStartDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledStartDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_start_date = ' . $this->sqlScheduledStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_date = ' . $this->sqlScheduledEndDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_date = ' . $this->sqlScheduledEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_by_days = ' . $this->sqlFilterByDays(). ',' ; } elseif( true == array_key_exists( 'FilterByDays', $this->getChangedColumns() ) ) { $strSql .= ' filter_by_days = ' . $this->sqlFilterByDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_within = ' . $this->sqlInspectionWithin(). ',' ; } elseif( true == array_key_exists( 'InspectionWithin', $this->getChangedColumns() ) ) { $strSql .= ' inspection_within = ' . $this->sqlInspectionWithin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_within_days = ' . $this->sqlInspectionWithinDays(). ',' ; } elseif( true == array_key_exists( 'InspectionWithinDays', $this->getChangedColumns() ) ) { $strSql .= ' inspection_within_days = ' . $this->sqlInspectionWithinDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_start_date = ' . $this->sqlDueStartDate(). ',' ; } elseif( true == array_key_exists( 'DueStartDate', $this->getChangedColumns() ) ) { $strSql .= ' due_start_date = ' . $this->sqlDueStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_end_date = ' . $this->sqlDueEndDate(). ',' ; } elseif( true == array_key_exists( 'DueEndDate', $this->getChangedColumns() ) ) { $strSql .= ' due_end_date = ' . $this->sqlDueEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'property_group_ids' => $this->getPropertyGroupIds(),
			'inspection_form_ids' => $this->getInspectionFormIds(),
			'inspection_status_ids' => $this->getInspectionStatusIds(),
			'company_employee_ids' => $this->getCompanyEmployeeIds(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'keyword_search' => $this->getKeywordSearch(),
			'sort_by' => $this->getSortBy(),
			'sort_direction' => $this->getSortDirection(),
			'created_start_date' => $this->getCreatedStartDate(),
			'created_end_date' => $this->getCreatedEndDate(),
			'inspected_start_date' => $this->getInspectedStartDate(),
			'inspected_end_date' => $this->getInspectedEndDate(),
			'completed_start_date' => $this->getCompletedStartDate(),
			'completed_end_date' => $this->getCompletedEndDate(),
			'scheduled_start_date' => $this->getScheduledStartDate(),
			'scheduled_end_date' => $this->getScheduledEndDate(),
			'filter_by_days' => $this->getFilterByDays(),
			'inspection_within' => $this->getInspectionWithin(),
			'inspection_within_days' => $this->getInspectionWithinDays(),
			'due_start_date' => $this->getDueStartDate(),
			'due_end_date' => $this->getDueEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>