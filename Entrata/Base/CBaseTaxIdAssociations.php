<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxIdAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTaxIdAssociations extends CEosPluralBase {

	/**
	 * @return CTaxIdAssociation[]
	 */
	public static function fetchTaxIdAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTaxIdAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTaxIdAssociation
	 */
	public static function fetchTaxIdAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxIdAssociation::class, $objDatabase );
	}

	public static function fetchTaxIdAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tax_id_associations', $objDatabase );
	}

	public static function fetchTaxIdAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociation( sprintf( 'SELECT * FROM tax_id_associations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociations( sprintf( 'SELECT * FROM tax_id_associations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociations( sprintf( 'SELECT * FROM tax_id_associations WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociations( sprintf( 'SELECT * FROM tax_id_associations WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociations( sprintf( 'SELECT * FROM tax_id_associations WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationsByTaxIdTypeIdByCid( $intTaxIdTypeId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociations( sprintf( 'SELECT * FROM tax_id_associations WHERE tax_id_type_id = %d AND cid = %d', $intTaxIdTypeId, $intCid ), $objDatabase );
	}

}
?>