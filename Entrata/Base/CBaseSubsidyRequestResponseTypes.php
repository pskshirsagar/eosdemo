<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRequestResponseTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyRequestResponseTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyRequestResponseType[]
	 */
	public static function fetchSubsidyRequestResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidyRequestResponseType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyRequestResponseType
	 */
	public static function fetchSubsidyRequestResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidyRequestResponseType::class, $objDatabase );
	}

	public static function fetchSubsidyRequestResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_request_response_types', $objDatabase );
	}

	public static function fetchSubsidyRequestResponseTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyRequestResponseType( sprintf( 'SELECT * FROM subsidy_request_response_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>