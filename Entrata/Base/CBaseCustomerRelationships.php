<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRelationships
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerRelationships extends CEosPluralBase {

	/**
	 * @return CCustomerRelationship[]
	 */
	public static function fetchCustomerRelationships( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerRelationship::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerRelationship
	 */
	public static function fetchCustomerRelationship( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerRelationship::class, $objDatabase );
	}

	public static function fetchCustomerRelationshipCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_relationships', $objDatabase );
	}

	public static function fetchCustomerRelationshipByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationship( sprintf( 'SELECT * FROM customer_relationships WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerRelationships( sprintf( 'SELECT * FROM customer_relationships WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipsByDefaultCustomerRelationshipIdByCid( $intDefaultCustomerRelationshipId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationships( sprintf( 'SELECT * FROM customer_relationships WHERE default_customer_relationship_id = %d AND cid = %d', $intDefaultCustomerRelationshipId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipsByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationships( sprintf( 'SELECT * FROM customer_relationships WHERE customer_relationship_group_id = %d AND cid = %d', $intCustomerRelationshipGroupId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipsByCustomerTypeIdByCid( $intCustomerTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationships( sprintf( 'SELECT * FROM customer_relationships WHERE customer_type_id = %d AND cid = %d', $intCustomerTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipsByApplicationFeeTypeIdByCid( $intApplicationFeeTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationships( sprintf( 'SELECT * FROM customer_relationships WHERE application_fee_type_id = %d AND cid = %d', $intApplicationFeeTypeId, $intCid ), $objDatabase );
	}

}
?>