<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentComponent extends CEosSingularBase {

	const TABLE_NAME = 'public.document_components';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDocumentTemplateId;
	protected $m_intDocumentTemplateComponentId;
	protected $m_intDocumentId;
	protected $m_intDocumentComponentId;
	protected $m_strComponentType;
	protected $m_strTemplateComponentKey;
	protected $m_intSize;
	protected $m_strSyncedOn;
	protected $m_intOrderNum;
	protected $m_intAllowsUpdates;
	protected $m_intIsModifiable;
	protected $m_intIsRemovable;
	protected $m_intIsAutoUpdated;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intAllowsUpdates = '0';
		$this->m_intIsModifiable = '1';
		$this->m_intIsRemovable = '1';
		$this->m_intIsAutoUpdated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateId', trim( $arrValues['document_template_id'] ) ); elseif( isset( $arrValues['document_template_id'] ) ) $this->setDocumentTemplateId( $arrValues['document_template_id'] );
		if( isset( $arrValues['document_template_component_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateComponentId', trim( $arrValues['document_template_component_id'] ) ); elseif( isset( $arrValues['document_template_component_id'] ) ) $this->setDocumentTemplateComponentId( $arrValues['document_template_component_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['document_component_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentComponentId', trim( $arrValues['document_component_id'] ) ); elseif( isset( $arrValues['document_component_id'] ) ) $this->setDocumentComponentId( $arrValues['document_component_id'] );
		if( isset( $arrValues['component_type'] ) && $boolDirectSet ) $this->set( 'm_strComponentType', trim( stripcslashes( $arrValues['component_type'] ) ) ); elseif( isset( $arrValues['component_type'] ) ) $this->setComponentType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['component_type'] ) : $arrValues['component_type'] );
		if( isset( $arrValues['template_component_key'] ) && $boolDirectSet ) $this->set( 'm_strTemplateComponentKey', trim( stripcslashes( $arrValues['template_component_key'] ) ) ); elseif( isset( $arrValues['template_component_key'] ) ) $this->setTemplateComponentKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['template_component_key'] ) : $arrValues['template_component_key'] );
		if( isset( $arrValues['size'] ) && $boolDirectSet ) $this->set( 'm_intSize', trim( $arrValues['size'] ) ); elseif( isset( $arrValues['size'] ) ) $this->setSize( $arrValues['size'] );
		if( isset( $arrValues['synced_on'] ) && $boolDirectSet ) $this->set( 'm_strSyncedOn', trim( $arrValues['synced_on'] ) ); elseif( isset( $arrValues['synced_on'] ) ) $this->setSyncedOn( $arrValues['synced_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['allows_updates'] ) && $boolDirectSet ) $this->set( 'm_intAllowsUpdates', trim( $arrValues['allows_updates'] ) ); elseif( isset( $arrValues['allows_updates'] ) ) $this->setAllowsUpdates( $arrValues['allows_updates'] );
		if( isset( $arrValues['is_modifiable'] ) && $boolDirectSet ) $this->set( 'm_intIsModifiable', trim( $arrValues['is_modifiable'] ) ); elseif( isset( $arrValues['is_modifiable'] ) ) $this->setIsModifiable( $arrValues['is_modifiable'] );
		if( isset( $arrValues['is_removable'] ) && $boolDirectSet ) $this->set( 'm_intIsRemovable', trim( $arrValues['is_removable'] ) ); elseif( isset( $arrValues['is_removable'] ) ) $this->setIsRemovable( $arrValues['is_removable'] );
		if( isset( $arrValues['is_auto_updated'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoUpdated', trim( $arrValues['is_auto_updated'] ) ); elseif( isset( $arrValues['is_auto_updated'] ) ) $this->setIsAutoUpdated( $arrValues['is_auto_updated'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDocumentTemplateId( $intDocumentTemplateId ) {
		$this->set( 'm_intDocumentTemplateId', CStrings::strToIntDef( $intDocumentTemplateId, NULL, false ) );
	}

	public function getDocumentTemplateId() {
		return $this->m_intDocumentTemplateId;
	}

	public function sqlDocumentTemplateId() {
		return ( true == isset( $this->m_intDocumentTemplateId ) ) ? ( string ) $this->m_intDocumentTemplateId : 'NULL';
	}

	public function setDocumentTemplateComponentId( $intDocumentTemplateComponentId ) {
		$this->set( 'm_intDocumentTemplateComponentId', CStrings::strToIntDef( $intDocumentTemplateComponentId, NULL, false ) );
	}

	public function getDocumentTemplateComponentId() {
		return $this->m_intDocumentTemplateComponentId;
	}

	public function sqlDocumentTemplateComponentId() {
		return ( true == isset( $this->m_intDocumentTemplateComponentId ) ) ? ( string ) $this->m_intDocumentTemplateComponentId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDocumentComponentId( $intDocumentComponentId ) {
		$this->set( 'm_intDocumentComponentId', CStrings::strToIntDef( $intDocumentComponentId, NULL, false ) );
	}

	public function getDocumentComponentId() {
		return $this->m_intDocumentComponentId;
	}

	public function sqlDocumentComponentId() {
		return ( true == isset( $this->m_intDocumentComponentId ) ) ? ( string ) $this->m_intDocumentComponentId : 'NULL';
	}

	public function setComponentType( $strComponentType ) {
		$this->set( 'm_strComponentType', CStrings::strTrimDef( $strComponentType, 50, NULL, true ) );
	}

	public function getComponentType() {
		return $this->m_strComponentType;
	}

	public function sqlComponentType() {
		return ( true == isset( $this->m_strComponentType ) ) ? '\'' . addslashes( $this->m_strComponentType ) . '\'' : 'NULL';
	}

	public function setTemplateComponentKey( $strTemplateComponentKey ) {
		$this->set( 'm_strTemplateComponentKey', CStrings::strTrimDef( $strTemplateComponentKey, 64, NULL, true ) );
	}

	public function getTemplateComponentKey() {
		return $this->m_strTemplateComponentKey;
	}

	public function sqlTemplateComponentKey() {
		return ( true == isset( $this->m_strTemplateComponentKey ) ) ? '\'' . addslashes( $this->m_strTemplateComponentKey ) . '\'' : 'NULL';
	}

	public function setSize( $intSize ) {
		$this->set( 'm_intSize', CStrings::strToIntDef( $intSize, NULL, false ) );
	}

	public function getSize() {
		return $this->m_intSize;
	}

	public function sqlSize() {
		return ( true == isset( $this->m_intSize ) ) ? ( string ) $this->m_intSize : 'NULL';
	}

	public function setSyncedOn( $strSyncedOn ) {
		$this->set( 'm_strSyncedOn', CStrings::strTrimDef( $strSyncedOn, -1, NULL, true ) );
	}

	public function getSyncedOn() {
		return $this->m_strSyncedOn;
	}

	public function sqlSyncedOn() {
		return ( true == isset( $this->m_strSyncedOn ) ) ? '\'' . $this->m_strSyncedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setAllowsUpdates( $intAllowsUpdates ) {
		$this->set( 'm_intAllowsUpdates', CStrings::strToIntDef( $intAllowsUpdates, NULL, false ) );
	}

	public function getAllowsUpdates() {
		return $this->m_intAllowsUpdates;
	}

	public function sqlAllowsUpdates() {
		return ( true == isset( $this->m_intAllowsUpdates ) ) ? ( string ) $this->m_intAllowsUpdates : '0';
	}

	public function setIsModifiable( $intIsModifiable ) {
		$this->set( 'm_intIsModifiable', CStrings::strToIntDef( $intIsModifiable, NULL, false ) );
	}

	public function getIsModifiable() {
		return $this->m_intIsModifiable;
	}

	public function sqlIsModifiable() {
		return ( true == isset( $this->m_intIsModifiable ) ) ? ( string ) $this->m_intIsModifiable : '1';
	}

	public function setIsRemovable( $intIsRemovable ) {
		$this->set( 'm_intIsRemovable', CStrings::strToIntDef( $intIsRemovable, NULL, false ) );
	}

	public function getIsRemovable() {
		return $this->m_intIsRemovable;
	}

	public function sqlIsRemovable() {
		return ( true == isset( $this->m_intIsRemovable ) ) ? ( string ) $this->m_intIsRemovable : '1';
	}

	public function setIsAutoUpdated( $intIsAutoUpdated ) {
		$this->set( 'm_intIsAutoUpdated', CStrings::strToIntDef( $intIsAutoUpdated, NULL, false ) );
	}

	public function getIsAutoUpdated() {
		return $this->m_intIsAutoUpdated;
	}

	public function sqlIsAutoUpdated() {
		return ( true == isset( $this->m_intIsAutoUpdated ) ) ? ( string ) $this->m_intIsAutoUpdated : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, document_template_id, document_template_component_id, document_id, document_component_id, component_type, template_component_key, size, synced_on, order_num, allows_updates, is_modifiable, is_removable, is_auto_updated, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDocumentTemplateId() . ', ' .
 						$this->sqlDocumentTemplateComponentId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlDocumentComponentId() . ', ' .
 						$this->sqlComponentType() . ', ' .
 						$this->sqlTemplateComponentKey() . ', ' .
 						$this->sqlSize() . ', ' .
 						$this->sqlSyncedOn() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlAllowsUpdates() . ', ' .
 						$this->sqlIsModifiable() . ', ' .
 						$this->sqlIsRemovable() . ', ' .
 						$this->sqlIsAutoUpdated() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateComponentId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_component_id = ' . $this->sqlDocumentComponentId() . ','; } elseif( true == array_key_exists( 'DocumentComponentId', $this->getChangedColumns() ) ) { $strSql .= ' document_component_id = ' . $this->sqlDocumentComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' component_type = ' . $this->sqlComponentType() . ','; } elseif( true == array_key_exists( 'ComponentType', $this->getChangedColumns() ) ) { $strSql .= ' component_type = ' . $this->sqlComponentType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_component_key = ' . $this->sqlTemplateComponentKey() . ','; } elseif( true == array_key_exists( 'TemplateComponentKey', $this->getChangedColumns() ) ) { $strSql .= ' template_component_key = ' . $this->sqlTemplateComponentKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' size = ' . $this->sqlSize() . ','; } elseif( true == array_key_exists( 'Size', $this->getChangedColumns() ) ) { $strSql .= ' size = ' . $this->sqlSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn() . ','; } elseif( true == array_key_exists( 'SyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_updates = ' . $this->sqlAllowsUpdates() . ','; } elseif( true == array_key_exists( 'AllowsUpdates', $this->getChangedColumns() ) ) { $strSql .= ' allows_updates = ' . $this->sqlAllowsUpdates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; } elseif( true == array_key_exists( 'IsModifiable', $this->getChangedColumns() ) ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; } elseif( true == array_key_exists( 'IsRemovable', $this->getChangedColumns() ) ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_updated = ' . $this->sqlIsAutoUpdated() . ','; } elseif( true == array_key_exists( 'IsAutoUpdated', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_updated = ' . $this->sqlIsAutoUpdated() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'document_template_id' => $this->getDocumentTemplateId(),
			'document_template_component_id' => $this->getDocumentTemplateComponentId(),
			'document_id' => $this->getDocumentId(),
			'document_component_id' => $this->getDocumentComponentId(),
			'component_type' => $this->getComponentType(),
			'template_component_key' => $this->getTemplateComponentKey(),
			'size' => $this->getSize(),
			'synced_on' => $this->getSyncedOn(),
			'order_num' => $this->getOrderNum(),
			'allows_updates' => $this->getAllowsUpdates(),
			'is_modifiable' => $this->getIsModifiable(),
			'is_removable' => $this->getIsRemovable(),
			'is_auto_updated' => $this->getIsAutoUpdated(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>