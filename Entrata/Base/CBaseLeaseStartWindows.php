<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseStartWindows
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseStartWindows extends CEosPluralBase {

	/**
	 * @return CLeaseStartWindow[]
	 */
	public static function fetchLeaseStartWindows( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseStartWindow::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseStartWindow
	 */
	public static function fetchLeaseStartWindow( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseStartWindow::class, $objDatabase );
	}

	public static function fetchLeaseStartWindowCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_start_windows', $objDatabase );
	}

	public static function fetchLeaseStartWindowByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindow( sprintf( 'SELECT * FROM lease_start_windows WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseStartStructureIdByCid( $intLeaseStartStructureId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE lease_start_structure_id = %d AND cid = %d', $intLeaseStartStructureId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByDefaultLeaseStartWindowIdByCid( $intDefaultLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE default_lease_start_window_id = %d AND cid = %d', $intDefaultLeaseStartWindowId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByOrganizationContractIdByCid( $intOrganizationContractId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE organization_contract_id = %d AND cid = %d', $intOrganizationContractId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseTermTypeIdByCid( $intLeaseTermTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartWindows( sprintf( 'SELECT * FROM lease_start_windows WHERE lease_term_type_id = %d AND cid = %d', $intLeaseTermTypeId, $intCid ), $objDatabase );
	}

}
?>