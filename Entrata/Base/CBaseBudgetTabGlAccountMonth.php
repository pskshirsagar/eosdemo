<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccountMonth extends CEosSingularBase {

	const TABLE_NAME = 'public.budget_tab_gl_account_months';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetId;
	protected $m_intBudgetTabGlAccountId;
	protected $m_strPostMonth;
	protected $m_fltAmount;
	protected $m_fltOriginalAmount;
	protected $m_boolIsOverridden;
	protected $m_boolIsDirty;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAmount = '0';
		$this->m_fltOriginalAmount = '0';
		$this->m_boolIsOverridden = false;
		$this->m_boolIsDirty = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['budget_tab_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabGlAccountId', trim( $arrValues['budget_tab_gl_account_id'] ) ); elseif( isset( $arrValues['budget_tab_gl_account_id'] ) ) $this->setBudgetTabGlAccountId( $arrValues['budget_tab_gl_account_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['original_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOriginalAmount', trim( $arrValues['original_amount'] ) ); elseif( isset( $arrValues['original_amount'] ) ) $this->setOriginalAmount( $arrValues['original_amount'] );
		if( isset( $arrValues['is_overridden'] ) && $boolDirectSet ) $this->set( 'm_boolIsOverridden', trim( stripcslashes( $arrValues['is_overridden'] ) ) ); elseif( isset( $arrValues['is_overridden'] ) ) $this->setIsOverridden( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_overridden'] ) : $arrValues['is_overridden'] );
		if( isset( $arrValues['is_dirty'] ) && $boolDirectSet ) $this->set( 'm_boolIsDirty', trim( stripcslashes( $arrValues['is_dirty'] ) ) ); elseif( isset( $arrValues['is_dirty'] ) ) $this->setIsDirty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dirty'] ) : $arrValues['is_dirty'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setBudgetTabGlAccountId( $intBudgetTabGlAccountId ) {
		$this->set( 'm_intBudgetTabGlAccountId', CStrings::strToIntDef( $intBudgetTabGlAccountId, NULL, false ) );
	}

	public function getBudgetTabGlAccountId() {
		return $this->m_intBudgetTabGlAccountId;
	}

	public function sqlBudgetTabGlAccountId() {
		return ( true == isset( $this->m_intBudgetTabGlAccountId ) ) ? ( string ) $this->m_intBudgetTabGlAccountId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : '0';
	}

	public function setOriginalAmount( $fltOriginalAmount ) {
		$this->set( 'm_fltOriginalAmount', CStrings::strToFloatDef( $fltOriginalAmount, NULL, false, 2 ) );
	}

	public function getOriginalAmount() {
		return $this->m_fltOriginalAmount;
	}

	public function sqlOriginalAmount() {
		return ( true == isset( $this->m_fltOriginalAmount ) ) ? ( string ) $this->m_fltOriginalAmount : '0';
	}

	public function setIsOverridden( $boolIsOverridden ) {
		$this->set( 'm_boolIsOverridden', CStrings::strToBool( $boolIsOverridden ) );
	}

	public function getIsOverridden() {
		return $this->m_boolIsOverridden;
	}

	public function sqlIsOverridden() {
		return ( true == isset( $this->m_boolIsOverridden ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOverridden ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDirty( $boolIsDirty ) {
		$this->set( 'm_boolIsDirty', CStrings::strToBool( $boolIsDirty ) );
	}

	public function getIsDirty() {
		return $this->m_boolIsDirty;
	}

	public function sqlIsDirty() {
		return ( true == isset( $this->m_boolIsDirty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDirty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_id, budget_tab_gl_account_id, post_month, amount, original_amount, is_overridden, is_dirty, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetId() . ', ' .
						$this->sqlBudgetTabGlAccountId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlOriginalAmount() . ', ' .
						$this->sqlIsOverridden() . ', ' .
						$this->sqlIsDirty() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId() . ','; } elseif( true == array_key_exists( 'BudgetTabGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_amount = ' . $this->sqlOriginalAmount() . ','; } elseif( true == array_key_exists( 'OriginalAmount', $this->getChangedColumns() ) ) { $strSql .= ' original_amount = ' . $this->sqlOriginalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_overridden = ' . $this->sqlIsOverridden() . ','; } elseif( true == array_key_exists( 'IsOverridden', $this->getChangedColumns() ) ) { $strSql .= ' is_overridden = ' . $this->sqlIsOverridden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dirty = ' . $this->sqlIsDirty() . ','; } elseif( true == array_key_exists( 'IsDirty', $this->getChangedColumns() ) ) { $strSql .= ' is_dirty = ' . $this->sqlIsDirty() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_id' => $this->getBudgetId(),
			'budget_tab_gl_account_id' => $this->getBudgetTabGlAccountId(),
			'post_month' => $this->getPostMonth(),
			'amount' => $this->getAmount(),
			'original_amount' => $this->getOriginalAmount(),
			'is_overridden' => $this->getIsOverridden(),
			'is_dirty' => $this->getIsDirty(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>