<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerIncomes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerIncomes extends CEosPluralBase {

	/**
	 * @return CCustomerIncome[]
	 */
	public static function fetchCustomerIncomes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerIncome::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerIncome
	 */
	public static function fetchCustomerIncome( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerIncome::class, $objDatabase );
	}

	public static function fetchCustomerIncomeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_incomes', $objDatabase );
	}

	public static function fetchCustomerIncomeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncome( sprintf( 'SELECT * FROM customer_incomes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM customer_incomes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM customer_incomes WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByIncomeTypeIdByCid( $intIncomeTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM customer_incomes WHERE income_type_id = %d AND cid = %d', $intIncomeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM customer_incomes WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM customer_incomes WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

}
?>