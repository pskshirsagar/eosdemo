<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceExceptions extends CEosPluralBase {

	/**
	 * @return CMaintenanceException[]
	 */
	public static function fetchMaintenanceExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceException::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceException
	 */
	public static function fetchMaintenanceException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceException::class, $objDatabase );
	}

	public static function fetchMaintenanceExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_exceptions', $objDatabase );
	}

	public static function fetchMaintenanceExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceException( sprintf( 'SELECT * FROM maintenance_exceptions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceExceptions( sprintf( 'SELECT * FROM maintenance_exceptions WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>