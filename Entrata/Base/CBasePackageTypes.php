<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageTypes
 * Do not add any new functions to this class.
 */

class CBasePackageTypes extends CEosPluralBase {

	/**
	 * @return CPackageType[]
	 */
	public static function fetchPackageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPackageType::class, $objDatabase );
	}

	/**
	 * @return CPackageType
	 */
	public static function fetchPackageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPackageType::class, $objDatabase );
	}

	public static function fetchPackageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'package_types', $objDatabase );
	}

	public static function fetchPackageTypeById( $intId, $objDatabase ) {
		return self::fetchPackageType( sprintf( 'SELECT * FROM package_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>