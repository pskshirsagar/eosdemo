<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyFloor extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_floors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyMediaFileId;
	protected $m_intPropertyBuildingId;
	protected $m_strRemotePrimaryKey;
	protected $m_strFloorTitle;
	protected $m_intFloorNumber;
	protected $m_strFloorCoordinates;
	protected $m_fltRentableSquareFeet;
	protected $m_fltUsableSquareFeet;
	protected $m_fltCamSquareFeet;
	protected $m_fltTotalSquareFeet;
	protected $m_fltCommercialSquareFeet;
	protected $m_fltRetailSquareFeet;
	protected $m_fltResidentialSquareFeet;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['floor_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFloorTitle', trim( stripcslashes( $arrValues['floor_title'] ) ) ); elseif( isset( $arrValues['floor_title'] ) ) $this->setFloorTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['floor_title'] ) : $arrValues['floor_title'] );
		if( isset( $arrValues['floor_number'] ) && $boolDirectSet ) $this->set( 'm_intFloorNumber', trim( $arrValues['floor_number'] ) ); elseif( isset( $arrValues['floor_number'] ) ) $this->setFloorNumber( $arrValues['floor_number'] );
		if( isset( $arrValues['floor_coordinates'] ) && $boolDirectSet ) $this->set( 'm_strFloorCoordinates', trim( stripcslashes( $arrValues['floor_coordinates'] ) ) ); elseif( isset( $arrValues['floor_coordinates'] ) ) $this->setFloorCoordinates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['floor_coordinates'] ) : $arrValues['floor_coordinates'] );
		if( isset( $arrValues['rentable_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltRentableSquareFeet', trim( $arrValues['rentable_square_feet'] ) ); elseif( isset( $arrValues['rentable_square_feet'] ) ) $this->setRentableSquareFeet( $arrValues['rentable_square_feet'] );
		if( isset( $arrValues['usable_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltUsableSquareFeet', trim( $arrValues['usable_square_feet'] ) ); elseif( isset( $arrValues['usable_square_feet'] ) ) $this->setUsableSquareFeet( $arrValues['usable_square_feet'] );
		if( isset( $arrValues['cam_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltCamSquareFeet', trim( $arrValues['cam_square_feet'] ) ); elseif( isset( $arrValues['cam_square_feet'] ) ) $this->setCamSquareFeet( $arrValues['cam_square_feet'] );
		if( isset( $arrValues['total_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSquareFeet', trim( $arrValues['total_square_feet'] ) ); elseif( isset( $arrValues['total_square_feet'] ) ) $this->setTotalSquareFeet( $arrValues['total_square_feet'] );
		if( isset( $arrValues['commercial_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltCommercialSquareFeet', trim( $arrValues['commercial_square_feet'] ) ); elseif( isset( $arrValues['commercial_square_feet'] ) ) $this->setCommercialSquareFeet( $arrValues['commercial_square_feet'] );
		if( isset( $arrValues['retail_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltRetailSquareFeet', trim( $arrValues['retail_square_feet'] ) ); elseif( isset( $arrValues['retail_square_feet'] ) ) $this->setRetailSquareFeet( $arrValues['retail_square_feet'] );
		if( isset( $arrValues['residential_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltResidentialSquareFeet', trim( $arrValues['residential_square_feet'] ) ); elseif( isset( $arrValues['residential_square_feet'] ) ) $this->setResidentialSquareFeet( $arrValues['residential_square_feet'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setFloorTitle( $strFloorTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFloorTitle', CStrings::strTrimDef( $strFloorTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getFloorTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFloorTitle', $strLocaleCode );
	}

	public function sqlFloorTitle() {
		return ( true == isset( $this->m_strFloorTitle ) ) ? '\'' . addslashes( $this->m_strFloorTitle ) . '\'' : 'NULL';
	}

	public function setFloorNumber( $intFloorNumber ) {
		$this->set( 'm_intFloorNumber', CStrings::strToIntDef( $intFloorNumber, NULL, false ) );
	}

	public function getFloorNumber() {
		return $this->m_intFloorNumber;
	}

	public function sqlFloorNumber() {
		return ( true == isset( $this->m_intFloorNumber ) ) ? ( string ) $this->m_intFloorNumber : 'NULL';
	}

	public function setFloorCoordinates( $strFloorCoordinates ) {
		$this->set( 'm_strFloorCoordinates', CStrings::strTrimDef( $strFloorCoordinates, 2000, NULL, true ) );
	}

	public function getFloorCoordinates() {
		return $this->m_strFloorCoordinates;
	}

	public function sqlFloorCoordinates() {
		return ( true == isset( $this->m_strFloorCoordinates ) ) ? '\'' . addslashes( $this->m_strFloorCoordinates ) . '\'' : 'NULL';
	}

	public function setRentableSquareFeet( $fltRentableSquareFeet ) {
		$this->set( 'm_fltRentableSquareFeet', CStrings::strToFloatDef( $fltRentableSquareFeet, NULL, false, 4 ) );
	}

	public function getRentableSquareFeet() {
		return $this->m_fltRentableSquareFeet;
	}

	public function sqlRentableSquareFeet() {
		return ( true == isset( $this->m_fltRentableSquareFeet ) ) ? ( string ) $this->m_fltRentableSquareFeet : 'NULL';
	}

	public function setUsableSquareFeet( $fltUsableSquareFeet ) {
		$this->set( 'm_fltUsableSquareFeet', CStrings::strToFloatDef( $fltUsableSquareFeet, NULL, false, 4 ) );
	}

	public function getUsableSquareFeet() {
		return $this->m_fltUsableSquareFeet;
	}

	public function sqlUsableSquareFeet() {
		return ( true == isset( $this->m_fltUsableSquareFeet ) ) ? ( string ) $this->m_fltUsableSquareFeet : 'NULL';
	}

	public function setCamSquareFeet( $fltCamSquareFeet ) {
		$this->set( 'm_fltCamSquareFeet', CStrings::strToFloatDef( $fltCamSquareFeet, NULL, false, 4 ) );
	}

	public function getCamSquareFeet() {
		return $this->m_fltCamSquareFeet;
	}

	public function sqlCamSquareFeet() {
		return ( true == isset( $this->m_fltCamSquareFeet ) ) ? ( string ) $this->m_fltCamSquareFeet : 'NULL';
	}

	public function setTotalSquareFeet( $fltTotalSquareFeet ) {
		$this->set( 'm_fltTotalSquareFeet', CStrings::strToFloatDef( $fltTotalSquareFeet, NULL, false, 4 ) );
	}

	public function getTotalSquareFeet() {
		return $this->m_fltTotalSquareFeet;
	}

	public function sqlTotalSquareFeet() {
		return ( true == isset( $this->m_fltTotalSquareFeet ) ) ? ( string ) $this->m_fltTotalSquareFeet : 'NULL';
	}

	public function setCommercialSquareFeet( $fltCommercialSquareFeet ) {
		$this->set( 'm_fltCommercialSquareFeet', CStrings::strToFloatDef( $fltCommercialSquareFeet, NULL, false, 4 ) );
	}

	public function getCommercialSquareFeet() {
		return $this->m_fltCommercialSquareFeet;
	}

	public function sqlCommercialSquareFeet() {
		return ( true == isset( $this->m_fltCommercialSquareFeet ) ) ? ( string ) $this->m_fltCommercialSquareFeet : 'NULL';
	}

	public function setRetailSquareFeet( $fltRetailSquareFeet ) {
		$this->set( 'm_fltRetailSquareFeet', CStrings::strToFloatDef( $fltRetailSquareFeet, NULL, false, 4 ) );
	}

	public function getRetailSquareFeet() {
		return $this->m_fltRetailSquareFeet;
	}

	public function sqlRetailSquareFeet() {
		return ( true == isset( $this->m_fltRetailSquareFeet ) ) ? ( string ) $this->m_fltRetailSquareFeet : 'NULL';
	}

	public function setResidentialSquareFeet( $fltResidentialSquareFeet ) {
		$this->set( 'm_fltResidentialSquareFeet', CStrings::strToFloatDef( $fltResidentialSquareFeet, NULL, false, 4 ) );
	}

	public function getResidentialSquareFeet() {
		return $this->m_fltResidentialSquareFeet;
	}

	public function sqlResidentialSquareFeet() {
		return ( true == isset( $this->m_fltResidentialSquareFeet ) ) ? ( string ) $this->m_fltResidentialSquareFeet : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, company_media_file_id, property_building_id, remote_primary_key, floor_title, floor_number, floor_coordinates, rentable_square_feet, usable_square_feet, cam_square_feet, total_square_feet, commercial_square_feet, retail_square_feet, residential_square_feet, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlFloorTitle() . ', ' .
						$this->sqlFloorNumber() . ', ' .
						$this->sqlFloorCoordinates() . ', ' .
						$this->sqlRentableSquareFeet() . ', ' .
						$this->sqlUsableSquareFeet() . ', ' .
						$this->sqlCamSquareFeet() . ', ' .
						$this->sqlTotalSquareFeet() . ', ' .
						$this->sqlCommercialSquareFeet() . ', ' .
						$this->sqlRetailSquareFeet() . ', ' .
						$this->sqlResidentialSquareFeet() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floor_title = ' . $this->sqlFloorTitle(). ',' ; } elseif( true == array_key_exists( 'FloorTitle', $this->getChangedColumns() ) ) { $strSql .= ' floor_title = ' . $this->sqlFloorTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floor_number = ' . $this->sqlFloorNumber(). ',' ; } elseif( true == array_key_exists( 'FloorNumber', $this->getChangedColumns() ) ) { $strSql .= ' floor_number = ' . $this->sqlFloorNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floor_coordinates = ' . $this->sqlFloorCoordinates(). ',' ; } elseif( true == array_key_exists( 'FloorCoordinates', $this->getChangedColumns() ) ) { $strSql .= ' floor_coordinates = ' . $this->sqlFloorCoordinates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_square_feet = ' . $this->sqlRentableSquareFeet(). ',' ; } elseif( true == array_key_exists( 'RentableSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' rentable_square_feet = ' . $this->sqlRentableSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usable_square_feet = ' . $this->sqlUsableSquareFeet(). ',' ; } elseif( true == array_key_exists( 'UsableSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' usable_square_feet = ' . $this->sqlUsableSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cam_square_feet = ' . $this->sqlCamSquareFeet(). ',' ; } elseif( true == array_key_exists( 'CamSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' cam_square_feet = ' . $this->sqlCamSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_square_feet = ' . $this->sqlTotalSquareFeet(). ',' ; } elseif( true == array_key_exists( 'TotalSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' total_square_feet = ' . $this->sqlTotalSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_square_feet = ' . $this->sqlCommercialSquareFeet(). ',' ; } elseif( true == array_key_exists( 'CommercialSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' commercial_square_feet = ' . $this->sqlCommercialSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retail_square_feet = ' . $this->sqlRetailSquareFeet(). ',' ; } elseif( true == array_key_exists( 'RetailSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' retail_square_feet = ' . $this->sqlRetailSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residential_square_feet = ' . $this->sqlResidentialSquareFeet(). ',' ; } elseif( true == array_key_exists( 'ResidentialSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' residential_square_feet = ' . $this->sqlResidentialSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'floor_title' => $this->getFloorTitle(),
			'floor_number' => $this->getFloorNumber(),
			'floor_coordinates' => $this->getFloorCoordinates(),
			'rentable_square_feet' => $this->getRentableSquareFeet(),
			'usable_square_feet' => $this->getUsableSquareFeet(),
			'cam_square_feet' => $this->getCamSquareFeet(),
			'total_square_feet' => $this->getTotalSquareFeet(),
			'commercial_square_feet' => $this->getCommercialSquareFeet(),
			'retail_square_feet' => $this->getRetailSquareFeet(),
			'residential_square_feet' => $this->getResidentialSquareFeet(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>