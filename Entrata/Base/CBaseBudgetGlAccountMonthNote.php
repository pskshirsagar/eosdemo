<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetGlAccountMonthNote extends CEosSingularBase {

	const TABLE_NAME = 'public.budget_gl_account_month_notes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetGlAccountMonthNoteTypeId;
	protected $m_intBudgetGlAccountMonthId;
	protected $m_intBudgetId;
	protected $m_intGlAccountId;
	protected $m_strPostMonth;
	protected $m_strNote;
	protected $m_strNoteDatetime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_gl_account_month_note_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetGlAccountMonthNoteTypeId', trim( $arrValues['budget_gl_account_month_note_type_id'] ) ); elseif( isset( $arrValues['budget_gl_account_month_note_type_id'] ) ) $this->setBudgetGlAccountMonthNoteTypeId( $arrValues['budget_gl_account_month_note_type_id'] );
		if( isset( $arrValues['budget_gl_account_month_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetGlAccountMonthId', trim( $arrValues['budget_gl_account_month_id'] ) ); elseif( isset( $arrValues['budget_gl_account_month_id'] ) ) $this->setBudgetGlAccountMonthId( $arrValues['budget_gl_account_month_id'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['note_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNoteDatetime', trim( $arrValues['note_datetime'] ) ); elseif( isset( $arrValues['note_datetime'] ) ) $this->setNoteDatetime( $arrValues['note_datetime'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetGlAccountMonthNoteTypeId( $intBudgetGlAccountMonthNoteTypeId ) {
		$this->set( 'm_intBudgetGlAccountMonthNoteTypeId', CStrings::strToIntDef( $intBudgetGlAccountMonthNoteTypeId, NULL, false ) );
	}

	public function getBudgetGlAccountMonthNoteTypeId() {
		return $this->m_intBudgetGlAccountMonthNoteTypeId;
	}

	public function sqlBudgetGlAccountMonthNoteTypeId() {
		return ( true == isset( $this->m_intBudgetGlAccountMonthNoteTypeId ) ) ? ( string ) $this->m_intBudgetGlAccountMonthNoteTypeId : 'NULL';
	}

	public function setBudgetGlAccountMonthId( $intBudgetGlAccountMonthId ) {
		$this->set( 'm_intBudgetGlAccountMonthId', CStrings::strToIntDef( $intBudgetGlAccountMonthId, NULL, false ) );
	}

	public function getBudgetGlAccountMonthId() {
		return $this->m_intBudgetGlAccountMonthId;
	}

	public function sqlBudgetGlAccountMonthId() {
		return ( true == isset( $this->m_intBudgetGlAccountMonthId ) ) ? ( string ) $this->m_intBudgetGlAccountMonthId : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 1024, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setNoteDatetime( $strNoteDatetime ) {
		$this->set( 'm_strNoteDatetime', CStrings::strTrimDef( $strNoteDatetime, -1, NULL, true ) );
	}

	public function getNoteDatetime() {
		return $this->m_strNoteDatetime;
	}

	public function sqlNoteDatetime() {
		return ( true == isset( $this->m_strNoteDatetime ) ) ? '\'' . $this->m_strNoteDatetime . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_gl_account_month_note_type_id, budget_gl_account_month_id, budget_id, gl_account_id, post_month, note, note_datetime, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlBudgetGlAccountMonthNoteTypeId() . ', ' .
 						$this->sqlBudgetGlAccountMonthId() . ', ' .
 						$this->sqlBudgetId() . ', ' .
 						$this->sqlGlAccountId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlNoteDatetime() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_gl_account_month_note_type_id = ' . $this->sqlBudgetGlAccountMonthNoteTypeId() . ','; } elseif( true == array_key_exists( 'BudgetGlAccountMonthNoteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_gl_account_month_note_type_id = ' . $this->sqlBudgetGlAccountMonthNoteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_gl_account_month_id = ' . $this->sqlBudgetGlAccountMonthId() . ','; } elseif( true == array_key_exists( 'BudgetGlAccountMonthId', $this->getChangedColumns() ) ) { $strSql .= ' budget_gl_account_month_id = ' . $this->sqlBudgetGlAccountMonthId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime() . ','; } elseif( true == array_key_exists( 'NoteDatetime', $this->getChangedColumns() ) ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_gl_account_month_note_type_id' => $this->getBudgetGlAccountMonthNoteTypeId(),
			'budget_gl_account_month_id' => $this->getBudgetGlAccountMonthId(),
			'budget_id' => $this->getBudgetId(),
			'gl_account_id' => $this->getGlAccountId(),
			'post_month' => $this->getPostMonth(),
			'note' => $this->getNote(),
			'note_datetime' => $this->getNoteDatetime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>