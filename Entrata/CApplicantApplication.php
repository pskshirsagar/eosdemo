<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CPropertyEmailRules;
use Psi\Eos\Entrata\CFileAssociations;
use Psi\Eos\Entrata\CUnitSpaces;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;
use Psi\Libraries\UtilHash\CHash;

class CApplicantApplication extends CBaseApplicantApplication {

	protected $m_objApplicant;
	protected $m_objApplication;
	protected $m_objClient;
	protected $m_objCustomerContact;

	protected $m_intPreviousLeaseStatusTypeId;
	protected $m_intPreviousCustomerTypeId;

	protected $m_objLease;
	protected $m_objProperty;
	protected $m_objPropertyFloorplan;
	protected $m_objPropertyUnit;
	protected $m_objMarketingMediaAssociation;
	protected $m_objLeaseCustomer;
	protected $m_objDefaultCompanyMediaFile;
	protected $m_objForm;
	protected $m_objApplicationDocument;
	protected $m_objApplicantOtherIncome;
	protected $m_objApplicantCurrentEmployer;
	protected $m_objApplicantPreviousEmployer;
	protected $m_objApplicantCurrentCompanyMoveOutReason;
	protected $m_objApplicantPreviousCompanyMoveOutReason;
	protected $m_objCompanyApplication;
	protected $m_objCustomerAssetSavingAccount;
	protected $m_objCustomerAssetCheckingAccount;
	protected $m_objApplicantEmergencyContact;
	protected $m_objApplicantPersonalReference1;
	protected $m_objApplicantPersonalReference2;
	protected $m_objOfferToRentFile;
	protected $m_objDocumentManager;
	protected $m_objObjectStorageGateway;
	protected $m_objRentRate;

	protected $m_arrobjLeadSources;
	protected $m_arrobjPropertyBuildings;
	protected $m_arrobjArPayments;
	protected $m_arrobjPaymentTypes;
	protected $m_arrstrCustomerTypes;
	protected $m_arrobjPropertyApplicationPreferences;
	protected $m_arrobjApplicantOtherVehicles;
	protected $m_arrobjApplicants;
	protected $m_arrobjCustomerPets;
	protected $m_arrobjPetRateAssociations;
	protected $m_arrobjCommunityAmenities;
	protected $m_arrobjApartmentAmenities;
	protected $m_arrobjAmenityLeaseAssociations;
	protected $m_arrobjCompanyResidenceTypes;
	protected $m_arrobjArTriggers;
	protected $m_arrobjRoommateInterestCategories;
	protected $m_arrobjRoommateInterestsOptions;
	protected $m_arrobjRoommateInterests;

	protected $m_arrstrCustomizedArTriggers;
	protected $m_arrobjStates;

	protected $m_arrobjApplicantApplicationFileAssociations;
	protected $m_arrobjFileAssociations;
	protected $m_arrobjLeaseDocumentAddendas;
	protected $m_arrobjIncomeTypes;
	protected $m_arrobjCustomerIncomes;
	protected $m_arrobjCustomerAssetTypes;
	protected $m_arrobjCustomerAssets;
	protected $m_arrobjMerchantAccounts;
	protected $m_arrobjMaritalStatusTypes;
	protected $m_arrobjGuarantors;
	protected $m_arrobjArTransactions;

	protected $m_arrmixOfferToRent;

	protected $m_arrstrIncomeTypeCustomFields;
	protected $m_arrstrCustomerAssetTypeCustomFields;

	protected $m_strExpirationDate;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameLastMatronymic;
	protected $m_strBirthDate;
	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_strEventDatetime;
	protected $m_strApplicationDatetime;
	protected $m_strLeaseEndDate;
	protected $m_strApplicantRelationshipName;
	protected $m_strLeaseStartDate;
	protected $m_strScreeningRecommendation;
	protected $m_strBuildingName;
    protected $m_strApplicantNameFull;

	protected $m_intDocumentTypeId;
	protected $m_intOldCustomerTypeId;
	protected $m_intOldCustomerRelationshipId;

	protected $m_intSignedLeaseFileAssociationsCount;
	protected $m_intTotalLeaseFileAssociationsCount;
	protected $m_intLeasingAgentId;
	protected $m_intLeaseId;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intLeadSourceId;
	protected $m_intOriginatingLeadSourceId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intOccupancyTypeId;

	protected $m_intUnitNumber;
	protected $m_intEventTypeId;
	protected $m_intEventResultId;
	protected $m_intLastAttemptDefaultEventResultId;
	protected $m_intLastContactDefaultEventResultId;
	protected $m_intLastContactDays;
	protected $m_intPsProductId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intCustomerId;
	protected $m_intScreeningApplicantResultId;
	protected $m_intCurrentApplicationStepId;
	protected $m_intIsSyncApplicationAndLease;
	protected $m_intLeaseCustomerId;
	protected $m_intDesiredBedroom;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningId;
	protected $m_intRequiresCapture;
	protected $m_intScreeningStatusTypeId;

	protected $m_strTaxNumberMasked;
	protected $m_strTaxNumberEncrypted;

	protected $m_boolIsLeaseGenerated;
	protected $m_boolIsLeaseUploaded;
	protected $m_boolIsLeasePacketE2eMigrated;
	protected $m_boolIsFromLeasingCenter;
	protected $m_boolLeaseInitiated;
	protected $m_boolIsForcedGuestCardIntegration;
	protected $m_boolIsPetPolicyConflicted;

	protected $m_objApplicationDataObject;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLeaseGenerated 				= false;
		$this->m_boolIsForcedGuestCardIntegration 	= false;

		$this->m_intIsSyncApplicationAndLease = true;

		return;
	}

	/**
	 * Create Functions
	 */

	public function createApplicantApplicationTransmission() {

		$objApplicantApplicationTransmission = new CApplicantApplicationTransmission();
		$objApplicantApplicationTransmission->setCid( $this->getCid() );
		$objApplicantApplicationTransmission->setApplicantApplicationId( $this->getId() );
		$objApplicantApplicationTransmission->setApplicationId( $this->getApplicationId() );

		return $objApplicantApplicationTransmission;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setApplicationId( $this->getApplicationId() );
		$objFileAssociation->setApplicantId( $this->getApplicantId() );
		$objFileAssociation->setCustomerId( $this->getCustomerId() );

		return $objFileAssociation;
	}

	public function createApplicationDuplicate() {

		$objApplicationDuplicate = new CApplicationDuplicate();

		$objApplicationDuplicate->setCid( $this->m_intCid );
		$objApplicationDuplicate->setApplicantApplicationId( $this->m_intId );
		$objApplicationDuplicate->setContactDatetime( date( 'm/d/Y' ) );

		return $objApplicationDuplicate;
	}

	public function createApplicantApplicationMedia() {

		$jsonData = new stdClass();
		$jsonData->uploaded_application_step_id = $this->getApplicationStepId();

		$objApplicantApplicationMedia = new CApplicantApplicationMedia();
		$objApplicantApplicationMedia->setCid( $this->m_intCid );
		$objApplicantApplicationMedia->setApplicantApplicationId( $this->m_intId );
		$objApplicantApplicationMedia->setData( $jsonData );

		return $objApplicantApplicationMedia;

	}

	public function createApplicationPayment() {

		$objApplicationPayment = new CApplicationPayment();
		$objApplicationPayment->setCid( $this->getCid() );
		$objApplicationPayment->setApplicationId( $this->getApplicationId() );
		$objApplicationPayment->setApplicantId( $this->getApplicantId() );

		return $objApplicationPayment;
	}

	public function createArPaymentImage() {

		$objArPaymentImage = new CArPaymentImage();
		$objArPaymentImage->setCid( $this->getCid() );
		$objArPaymentImage->setFileExtensionId( CFileExtension::IMAGE_PNG );
		$objArPaymentImage->setPaymentImageTypeId( CPaymentImageType::SIGNATURE );
		$objArPaymentImage->setImagePath( 'signature.png' );

		return $objArPaymentImage;
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getScreeningRecommendation() {
		return $this->m_strScreeningRecommendation;
	}

	public function getOrFetchClient( $objDatabase ) {

		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objDatabase );
		}

		return $this->m_objClient;
	}

	public function getOrFetchApplicant( $objDatabase, $boolLoadFromView = true ) {

		if( false == valObj( $this->m_objApplicant, 'CApplicant' ) ) {
			$this->m_objApplicant = $this->fetchApplicant( $objDatabase, $boolLoadFromView );
		}

		return $this->m_objApplicant;
	}

	public function getOrFetchApplication( $objDatabase ) {

		if( false == valObj( $this->m_objApplication, 'CApplication' ) ) {
			$this->m_objApplication = $this->fetchApplication( $objDatabase );
		}

		return $this->m_objApplication;
	}

	public function getOrFetchIsleadByIdByCid( $objDatabase ) {

		$strSql = 'SELECT
						aa.is_lead
					FROM
						applicant_applications aa
					WHERE
						aa.id = ' . ( int ) $this->getId() . '
						AND aa.cid = ' . ( int ) $this->getCid() . '
					LIMIT 1';

		$arrintIsLead = ( array ) fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintIsLead ) ) return false;

		return $arrintIsLead[0]['is_lead'];
	}

	/**
	 * Get Functions
	 */

	public function getObjectStorageGateway() {
		if( true == valObj( $this->m_objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return $this->m_objObjectStorageGateway;
		}

		$this->m_objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );
		$this->m_objObjectStorageGateway->initialize();

		return $this->m_objObjectStorageGateway;
	}

	public function getDesiredBedroom() {
		return $this->m_intDesiredBedroom;
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function getIsResponsibleForLease() {
		return in_array( $this->m_intCustomerTypeId, CCustomerType::$c_arrintResponsibleCustomerTypeIds );
	}

	public function getIsPrimary() {
		return ( CCustomerType::PRIMARY == $this->m_intCustomerTypeId ) ? true : false;
	}

	public function getRequiresApplication() {
		return in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds );
	}

	public function getExpirationDate() {
		return $this->m_strExpirationDate;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameLastMatronymic() {
		return $this->m_strNameLastMatronymic;
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getEventDatetime() {
		return $this->m_strEventDatetime;
	}

	public function getApplicationDatetime() {
		return $this->m_strApplicationDatetime;
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function getApplicantRelationshipName() {
		return $this->m_strApplicantRelationshipName;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function getDocumentTypeId() {
		return $this->m_intDocumentTypeId;
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getCurrentApplicationStepId() {
    	return $this->m_intCurrentApplicationStepId;
    }

	public function getIsSyncApplicationAndLease() {
		return $this->m_intIsSyncApplicationAndLease;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function getOriginatingLeadSourceId() {
		return $this->m_intOriginatingLeadSourceId;
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function getUnitNumber() {
		return $this->m_intUnitNumber;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function getEventResultId() {
		return $this->m_intEventResultId;
	}

	public function getLastAttemptDefaultEventResultId() {
		return $this->m_intLastAttemptDefaultEventResultId;
	}

	public function getLastContactDefaultEventResultId() {
		return $this->m_intLastContactDefaultEventResultId;
	}

	public function getLastContactDays() {
		return $this->m_intLastContactDays;
	}

	public function getIsPetPolicyConflicted() {
		return $this->m_boolIsPetPolicyConflicted;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function getIsLeaseGenerated() {
		return $this->m_boolIsLeaseGenerated;
	}

	public function getIsLeaseUploaded() {
		return $this->m_boolIsLeaseUploaded;
	}

	public function getIsLeasePacketE2eMigrated() {
		return $this->m_boolIsLeasePacketE2eMigrated;
	}

	public function getRequiresCapture() {
		return $this->m_intRequiresCapture;
	}

	public function getLeaseDocumentAddendas() {
		return $this->m_arrobjLeaseDocumentAddendas;
	}

	public function getCustomerPets() {
		return $this->m_arrobjCustomerPets;
	}

	public function getFileAssociations() {
		return $this->m_arrobjFileAssociations;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getOfferToRent() {
		return $this->m_arrmixOfferToRent;
	}

	public function getSignedLeaseFileAssociationsCount() {
		return $this->m_intSignedLeaseFileAssociationsCount;
	}

	public function getTotalLeaseFileAssociationsCount() {
		return $this->m_intTotalLeaseFileAssociationsCount;
	}

	public function getOfferToRentFile() {
		return $this->m_objOfferToRentFile;
	}

	public function getIsFromLeasingCenter() {
		return $this->m_boolIsFromLeasingCenter;
	}

	public function getLeaseInitiated() {
		return $this->m_boolLeaseInitiated;
	}

	public function getIsForcedGuestCardIntegration() {
		return $this->m_boolIsForcedGuestCardIntegration;
	}

	public function getCustomerTypeName() {
		return CCustomerType::createService()->customerTypeIdToStr( $this->getCustomerTypeId() );
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getLeaseCustomer() {
	 return $this->m_objLeaseCustomer;
	}

	public function getRemotePrimaryKey() {
		if( true == $this->getIsForcedGuestCardIntegration() ) {
			return $this->m_strGuestRemotePrimaryKey;
		} elseif( CLeaseIntervalType::APPLICATION == $this->m_intLeaseIntervalTypeId ) {
			return $this->m_strAppRemotePrimaryKey;
		} else {
			return NULL;
		}
	}

	public function getApplicantNameFull() {
		return $this->m_strApplicantNameFull;
	}

	public function getPreviousLeaseStatusTypeId() {
		return $this->m_intPreviousLeaseStatusTypeId;
	}

	public function getPreviousCustomerTypeId() {
		return $this->m_intPreviousCustomerTypeId;
	}

	public function getScreeningApplicantResultId() {
		return $this->m_intScreeningApplicantResultId;
	}

	public function getApplicationDataObject() {
		return $this->m_objApplicationDataObject;
	}

	public function getLeaseStartDate() {
	    return $this->m_strLeaseStartDate;
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function getOldCustomerTypeId() {
		return $this->m_intOldCustomerTypeId;
	}

	public function getOldCustomerRelationshipId() {
		return $this->m_intOldCustomerRelationshipId;
	}

	public function getCustomApplicationStatus() {

		return CApplicationStageStatus::createService()->getCustomApplicationStageStatus( $this->getLeaseIntervalTypeId(), $this->getApplicationStageId(), $this->getApplicationStatusId(), $this->getOccupancyTypeId() );
	}

	public function getCustomSubsidyCertificationStatus() {
		return CApplicationStageStatus::createService()->getCustomCertificationStageStatus( $this->getApplicationStageId(), $this->getApplicationStatusId(), $this->getOccupancyTypeId() );
	}

	public function getCustomApplicationStep() {
		return CApplicationStep::getCustomApplicationStep( $this->getApplicationStepId(), $this->getOccupancyTypeId() );
	}

	public function getDefaultCustomerRelationshipId( $objDatabase ) {

		$intPropertyId = $this->getPropertyId();

		if( true == is_null( $this->getPropertyId() ) ) {
			$intPropertyId = CApplications::fetchPropertyIdByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		}

		$intOccupancyTypeId			= ( true == valStr( $this->getOccupancyTypeId() ) ) ? $this->getOccupancyTypeId() : COccupancyType::CONVENTIONAL;
		$objCustomerRelationship 	= CCustomerRelationships::fetchDefaultCustomerRelationshipByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $intOccupancyTypeId, $intPropertyId, $this->getCustomerTypeId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerRelationship, 'CCustomerRelationship' ) ) {
			return $objCustomerRelationship->getId();
		}

		return NULL;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function getScreeningStatusTypeId() {
		return $this->m_intScreeningStatusTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setScreeningRecommendation( $intScreeningRecommendationTypeId ) {
		$this->m_strScreeningRecommendation = CScreeningApplicantResult::getScreeningRecommendationTypeName( $intScreeningRecommendationTypeId );
	}

	public function setDesiredBedroom( $strDesiredBedroom ) {
		$this->m_intDesiredBedroom = $strDesiredBedroom;
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->m_strTaxNumberMasked = $strTaxNumberMasked;
	}

	public function setExpirationDate( $strExpirationDate ) {
		$this->m_strExpirationDate = $strExpirationDate;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->m_strNameLastMatronymic = $strNameLastMatronymic;
	}

	public function setBirthDate( $strBirthDate ) {
		$this->m_strBirthDate = $strBirthDate;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setEventDatetime( $strEventDatetime ) {
		$this->m_strEventDatetime = $strEventDatetime;
	}

	public function setApplicationDatetime( $strApplicationDatetime ) {
		$this->m_strApplicationDatetime = $strApplicationDatetime;
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = $strLeaseEndDate;
	}

	public function setApplicantRelationshipName( $strApplicantRelationshipName ) {
		$this->m_strApplicantRelationshipName = $strApplicantRelationshipName;
	}

    public function setApplicantNameFull( $strApplicantNameFull ) {
        $this->m_strApplicantNameFull = $strApplicantNameFull;
    }

	public function setIsLeaseGenerated( $boolIsLeaseGenerated ) {
		$this->m_boolIsLeaseGenerated = $boolIsLeaseGenerated;
	}

	public function setIsLeaseUploaded( $boolIsLeaseUploaded ) {
		$this->m_boolIsLeaseUploaded = $boolIsLeaseUploaded;
	}

	public function setIsLeasePacketE2eMigrated( $boolIsLeasePacketE2eMigrated ) {
		$this->m_boolIsLeasePacketE2eMigrated = $boolIsLeasePacketE2eMigrated;
	}

	public function setIsFromLeasingCenter( $boolIsFromLeasingCenter ) {
		$this->m_boolIsFromLeasingCenter = $boolIsFromLeasingCenter;
	}

	public function setLeaseInitiated( $boolLeaseInitiated ) {
		$this->m_boolLeaseInitiated = $boolLeaseInitiated;
	}

	public function setIsForcedGuestCardIntegration( $boolIsForcedGuestCardIntegration ) {
		$this->m_boolIsForcedGuestCardIntegration = $boolIsForcedGuestCardIntegration;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	public function setDocumentTypeId( $intDocumentTypeId ) {
		$this->m_intDocumentTypeId = $intDocumentTypeId;
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->m_intLeasingAgentId = $intLeasingAgentId;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = $intCurrentApplicationStepId;
	}

	public function setIsSyncApplicationAndLease( $intIsSyncApplicationAndLease ) {
		$this->m_intIsSyncApplicationAndLease = $intIsSyncApplicationAndLease;
	}

	public function setRequiresCapture( $intRequiresCapture ) {
		$this->m_intRequiresCapture = $intRequiresCapture;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setLeaseCustomer( $objLeaseCustomer ) {
		$this->m_objLeaseCustomer = $objLeaseCustomer;
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->m_intLeadSourceId = $intLeadSourceId;
	}

	public function setOriginatingLeadSourceId( $intOriginatingLeadSourceId ) {
		$this->m_intOriginatingLeadSourceId = $intOriginatingLeadSourceId;
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->m_intApplicationStageId = $intApplicationStageId;
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->m_intApplicationStatusId = $intApplicationStatusId;
	}

	public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->m_intEventTypeId = $intEventTypeId;
	}

	public function setEventResultId( $intEventResultId ) {
		$this->m_intEventResultId = $intEventResultId;
	}

	public function setLastAttemptDefaultEventResultId( $intLastAttemptDefaultEventResultId ) {
		$this->m_intLastAttemptDefaultEventResultId = $intLastAttemptDefaultEventResultId;
	}

	public function setLastContactDefaultEventResultId( $intLastContactDefaultEventResultId ) {
		$this->m_intLastContactDefaultEventResultId = $intLastContactDefaultEventResultId;
	}

	public function setLastContactDays( $intLastContactDays ) {
		$this->m_intLastContactDays = $intLastContactDays;
	}

	public function setIsPetPolicyConflicted( $boolIsPetPolicyConflicted ) {
		$this->m_boolIsPetPolicyConflicted = $boolIsPetPolicyConflicted;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->m_intLeaseIntervalTypeId = $intLeaseIntervalTypeId;
	}

	public function setFileAssociations( $arrobjFileAssociations ) {
		$this->m_arrobjFileAssociations = $arrobjFileAssociations;
	}

	public function setLeaseDocumentAddendas( $arrobjLeaseDocumentAddendas ) {
		$this->m_arrobjLeaseDocumentAddendas = $arrobjLeaseDocumentAddendas;
	}

	public function setCustomerPets( $arrobjCustomerPets ) {
		$this->m_arrobjCustomerPets = $arrobjCustomerPets;
	}

	public function setOfferToRent( $arrmixOfferToRent ) {
		$this->m_arrmixOfferToRent = $arrmixOfferToRent;
	}

	public function setOfferToRentFile( $objOfferToRentFile ) {
		$this->m_objOfferToRentFile = $objOfferToRentFile;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setScreeningId( $intScreeningId ) {
		$this->m_intScreeningId = $intScreeningId;
	}

	public function setDefaultLeaseDocumentId( $arrobjPropertyApplicationPreferences ) {

		if( ( false == is_null( $this->getLeaseDocumentId() ) && 0 < $this->getLeaseDocumentId() ) || false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			return;
		}

		switch( $this->getCustomerTypeId() ) {

			case CCustomerType::RESPONSIBLE:
				if( array_key_exists( 'COAPPLICANT_DEFAULT_LEASE_AGREEMENT', $arrobjPropertyApplicationPreferences )
				&& 0 < $arrobjPropertyApplicationPreferences['COAPPLICANT_DEFAULT_LEASE_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['COAPPLICANT_DEFAULT_LEASE_AGREEMENT']->getValue() );
				}
				break;

			case CCustomerType::GUARANTOR:
				if( array_key_exists( 'GUARANTOR_DEFAULT_LEASE_AGREEMENT', $arrobjPropertyApplicationPreferences )
				&& 0 < $arrobjPropertyApplicationPreferences['GUARANTOR_DEFAULT_LEASE_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['GUARANTOR_DEFAULT_LEASE_AGREEMENT']->getValue() );
				}
				break;

			default:
				if( array_key_exists( 'PRIMARY_APPLICANT_DEFAULT_LEASE_AGREEMENT', $arrobjPropertyApplicationPreferences )
				    && 0 < $arrobjPropertyApplicationPreferences['PRIMARY_APPLICANT_DEFAULT_LEASE_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['PRIMARY_APPLICANT_DEFAULT_LEASE_AGREEMENT']->getValue() );
				}
				break;
		}

		return;
	}

	public function setDefaultLeaseRenewalDocumentId( $arrobjPropertyApplicationPreferences ) {

		if( ( false == is_null( $this->getLeaseDocumentId() ) && 0 < $this->getLeaseDocumentId() ) || false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			return;
		}

		switch( $this->getCustomerTypeId() ) {

			case CCustomerType::RESPONSIBLE:
				if( array_key_exists( 'COAPPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT', $arrobjPropertyApplicationPreferences )
				&& 0 < $arrobjPropertyApplicationPreferences['COAPPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['COAPPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() );
				}
				break;

			case CCustomerType::GUARANTOR:
				if( array_key_exists( 'GUARANTOR_DEFAULT_LEASE_RENEWAL_AGREEMENT', $arrobjPropertyApplicationPreferences )
				&& 0 < $arrobjPropertyApplicationPreferences['GUARANTOR_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['GUARANTOR_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() );
				}
				break;

			default:
				if( array_key_exists( 'PRIMARY_APPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT', $arrobjPropertyApplicationPreferences )
				    && 0 < $arrobjPropertyApplicationPreferences['PRIMARY_APPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() ) {
					$this->setLeaseDocumentId( $arrobjPropertyApplicationPreferences['PRIMARY_APPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT']->getValue() );
				}
				break;
		}

		return;
	}

	public function refreshExportedOn( $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND deleted_on IS NULL' : '';

		$strSql = 'SELECT exported_on FROM applicant_applications WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid() . $strCheckDeletedAASql;
		$arrstrExportedOn = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrExportedOn[0]['exported_on'] ) ) {
			$this->setExportedOn( $arrstrExportedOn[0]['exported_on'] );
		}
	}

	public function setSignedLeaseFileAssociationsCount( $intSignedLeaseFileAssociationsCount ) {
		$this->m_intSignedLeaseFileAssociationsCount = $intSignedLeaseFileAssociationsCount;
	}

	public function setTotalLeaseFileAssociationsCount( $intTotalLeaseFileAssociationsCount ) {
		$this->m_intTotalLeaseFileAssociationsCount = $intTotalLeaseFileAssociationsCount;
	}

	public function setPreviousLeaseStatusTypeId( $intPreviousLeaseStatusTypeId ) {
		$this->m_intPreviousLeaseStatusTypeId = $intPreviousLeaseStatusTypeId;
	}

	public function setPreviousCustomerTypeId( $intPreviousCustomerTypeId ) {
		$this->m_intPreviousCustomerTypeId = $intPreviousCustomerTypeId;
	}

	public function setScreeningApplicantResultId( $intScreeningApplicantResultId ) {
		$this->m_intScreeningApplicantResultId = $intScreeningApplicantResultId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {

		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setDocumentManager( $objDocumentManager ) {
		$this->m_objDocumentManager = $objDocumentManager;
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
	    $this->m_strLeaseStartDate = $strLeaseStartDate;
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->m_intScreeningRecommendationTypeId = $intScreeningRecommendationTypeId;
	}

	public function setOldCustomerTypeId( $intOldCustomerTypeId ) {
		$this->m_intOldCustomerTypeId = $intOldCustomerTypeId;
	}

	public function setOldCustomerRelationshipId( $intOldCustomerRelationshipId ) {
		$this->m_intOldCustomerRelationshipId = $intOldCustomerRelationshipId;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		return $this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->m_strTaxNumberEncrypted = $strTaxNumberEncrypted;
	}

	public function setScreeningStatusTypeId( $intScreeningStatusTypeId ) {
		$this->m_intScreeningStatusTypeId = $intScreeningStatusTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) )
			$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )
			$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['name_last_matronymic'] ) )
			$this->setNameLastMatronymic( $arrmixValues['name_last_matronymic'] );
		if( true == isset( $arrmixValues['birth_date'] ) )
			$this->setBirthDate( $arrmixValues['birth_date'] );
		if( true == isset( $arrmixValues['phone_number'] ) )
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['email_address'] ) )
			$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['event_datetime'] ) )
			$this->setEventDatetime( $arrmixValues['event_datetime'] );
		if( true == isset( $arrmixValues['application_datetime'] ) )
			$this->setApplicationDatetime( $arrmixValues['application_datetime'] );
		if( true == isset( $arrmixValues['lease_end_date'] ) )
			$this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		if( true == isset( $arrmixValues['leasing_agent_id'] ) )
			$this->setLeasingAgentId( $arrmixValues['leasing_agent_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) )
			$this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['customer_id'] ) )
			$this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['property_id'] ) )
			$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['property_unit_id'] ) )
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		if( true == isset( $arrmixValues['lead_source_id'] ) )
			$this->setLeadSourceId( $arrmixValues['lead_source_id'] );
		if( true == isset( $arrmixValues['originating_lead_source_id'] ) )
			$this->setOriginatingLeadSourceId( $arrmixValues['originating_lead_source_id'] );
		if( true == isset( $arrmixValues['application_stage_id'] ) )
			$this->setApplicationStageId( $arrmixValues['application_stage_id'] );
		if( true == isset( $arrmixValues['application_status_id'] ) )
			$this->setApplicationStatusId( $arrmixValues['application_status_id'] );
		if( true == isset( $arrmixValues['lease_interval_type_id'] ) )
			$this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		if( true == isset( $arrmixValues['application_stage_id'] ) )
			$this->setApplicationStageId( $arrmixValues['application_stage_id'] );
		if( true == isset( $arrmixValues['application_status_id'] ) )
			$this->setApplicationStatusId( $arrmixValues['application_status_id'] );
		if( true == isset( $arrmixValues['unit_number'] ) )
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['building_name'] ) )
			$this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['event_type_id'] ) )
			$this->setEventTypeId( $arrmixValues['event_type_id'] );
		if( true == isset( $arrmixValues['event_result_id'] ) )
			$this->setEventResultId( $arrmixValues['event_result_id'] );
		if( true == isset( $arrmixValues['last_contact_days'] ) )
			$this->setLastContactDays( $arrmixValues['last_contact_days'] );
		if( true == isset( $arrmixValues['is_pet_policy_conflicted'] ) )
			$this->setIsPetPolicyConflicted( $arrmixValues['is_pet_policy_conflicted'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) )
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['lease_interval_type_id'] ) )
			$this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		if( true == isset( $arrmixValues['desired_bedrooms'] ) )
			$this->setDesiredBedroom( $arrmixValues['desired_bedrooms'] );
		if( true == isset( $arrmixValues['tax_number_masked'] ) )
			$this->setTaxNumberMasked( $arrmixValues['tax_number_masked'] );
		if( true == isset( $arrmixValues['applicant_relationship_name'] ) )
			$this->setApplicantRelationshipName( $arrmixValues['applicant_relationship_name'] );
        if( true == isset( $arrmixValues['applicant_name_full'] ) )
            $this->setApplicantNameFull( $arrmixValues['applicant_name_full'] );
		if( true == isset( $arrmixValues['previous_lease_status_type_id'] ) )
			$this->setPreviousLeaseStatusTypeId( $arrmixValues['previous_lease_status_type_id'] );
		if( true == isset( $arrmixValues['previous_customer_type_id'] ) )
			$this->setPreviousCustomerTypeId( $arrmixValues['previous_customer_type_id'] );
		if( true == isset( $arrmixValues['lease_start_date'] ) )
		    $this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		if( true == isset( $arrmixValues['screening_applicant_result_id'] ) )
			$this->setScreeningApplicantResultId( $arrmixValues['screening_applicant_result_id'] );
		if( true == isset( $arrmixValues['screening_recommendation_type_id'] ) )
			$this->setScreeningRecommendationTypeId( $arrmixValues['screening_recommendation_type_id'] );
		if( true == isset( $arrmixValues['screening_id'] ) )
			$this->setScreeningId( $arrmixValues['screening_id'] );
		if( true == isset( $arrmixValues['lease_start_date'] ) )
		    $this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		if( true == isset( $arrmixValues['requires_capture'] ) )
			$this->setRequiresCapture( $arrmixValues['requires_capture'] );
		if( true == isset( $arrmixValues['occupancy_type_id'] ) )
			$this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		if( true == isset( $arrmixValues['old_customer_relationship_id'] ) )
			$this->setOldCustomerRelationshipId( $arrmixValues['old_customer_relationship_id'] );
		if( true == isset( $arrmixValues['old_customer_type_id'] ) )
			$this->setOldCustomerTypeId( $arrmixValues['old_customer_type_id'] );
		if( true == isset( $arrmixValues['last_attempt_default_event_result_id'] ) )
			$this->setLastAttemptDefaultEventResultId( $arrmixValues['last_attempt_default_event_result_id'] );
		if( true == isset( $arrmixValues['last_contact_default_event_result_id'] ) )
			$this->setLastContactDefaultEventResultId( $arrmixValues['last_contact_default_event_result_id'] );
		if( true == isset( $arrmixValues['screening_recommendation_type_id'] ) )
			$this->setScreeningRecommendation( $arrmixValues['screening_recommendation_type_id'] );
		if( true == isset( $arrmixValues['tax_number_encrypted'] ) )
			$this->setTaxNumberEncrypted( $arrmixValues['tax_number_encrypted'] );
		if( true == isset( $arrmixValues['screening_status_type_id'] ) )
			$this->setScreeningStatusTypeId( $arrmixValues['screening_status_type_id'] );

		$this->setAllowDifferentialUpdate( true );
		return;
	}

	/**
	 * Add Functions
	 */

	public function addCustomerPet( $objCustomerPet ) {
		$this->m_arrobjCustomerPets[$objCustomerPet->getId()] = $objCustomerPet;
	}

	public function addFileAssociation( $objFileAssociation ) {
		$this->m_arrobjFileAssociations[$objFileAssociation->getId()] = $objFileAssociation;
	}

	/**
	 * Send Functions
	 */

	public function sendManagerAndGuestCardEmails( $objDatabase, $objEmailDatabase = NULL, $strSubject = NULL, $boolIsIlsParser = false, $boolSendToManagerOnly = false, $boolIsSmsContact = false, $strMessage = NULL, $arrobjWebsitePreferences = NULL, $strExternalUrl = NULL, $boolIsFromSitetablet = false, $boolIsAvailabilityAlertOrScheduleTour = false, $intWebsiteId = NULL, $boolIsFromRequestInfo = false, $strHostDomainName = NULL, $intSwitchClsId = NULL, $intIlsId = NULL, $objCustomerHelper = NULL, $strRentOccupancyTriggerName = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrobjSystemEmails			= [];
		$arrstrPhoneNumberTypes		= [
			CPhoneNumberType::HOME		=> 'Home',
			CPhoneNumberType::OFFICE	=> 'Office',
			CPhoneNumberType::MOBILE	=> 'Cell'
		];

		$objClient 					= $this->getOrFetchClient( $objDatabase );
		$objApplicant				= $this->fetchApplicant( $objDatabase );
		$arrobjCustomerPhoneNumbers = ( array ) \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchOrderedCustomerPhoneNumbersByCustomerIdByCid( $objApplicant->getCustomerId(), $objClient->getId(), $objDatabase );
		$objApplication				= $this->getOrFetchApplication( $objDatabase );
		$objApplicationDuplicate	= $this->fetchApplicationDuplicate( $objDatabase );
		$arrobjLeadSources			= $objClient->fetchLeadSources( $objDatabase );
		$objProperty				= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchProperty( $objDatabase ) : $objApplicationDuplicate->getOrFetchProperty( $objDatabase );
		$objPropertyEmailRule		= NULL;

		// Emails should not be sent to disabled properties as per the requirement
		if( true == valObj( $objProperty, 'CProperty' ) && true == $objProperty->processSystemEmail() ) {
			return true;
		}

		$objPropertyUnit							= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchPropertyUnit( $objDatabase ) : $objApplicationDuplicate->fetchPropertyUnit( $objDatabase );
		$objUnitSpace								= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchUnitSpace( $objDatabase ) : $objApplicationDuplicate->fetchUnitSpace( $objDatabase );
	 	$objPropertyEmailRule 						= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::COMPANY_GUEST_CARD, $objProperty->getId(), $objDatabase );

		$intPropertyFloorplanId						= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getPropertyFloorplanId() : $objApplicationDuplicate->getPropertyFloorplanId();
		$objPropertyFloorplan						= $objProperty->fetchPropertyFloorplanById( $intPropertyFloorplanId, $objDatabase );

		$objApplication->setFloorplanName( ( true == valObj( $objPropertyFloorplan, 'CPropertyFloorplan' ) ) ? $objPropertyFloorplan->getFloorplanName() : NULL );

		$objPropertyBuilding						= ( true == valObj( $objPropertyUnit, 'CPropertyUnit', 'PropertyBuildingId' ) ) ? $objPropertyUnit->fetchPropertyBuilding( $objDatabase ) : NULL;
		$objMarketingMediaAssociation				= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objGustCardCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'GUEST_CARD_CUSTOM_TEXT', $objDatabase );
		$objLogoMarketingMediaAssociation			= $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$arrobjPropertyPreferences 					= $objProperty->fetchPropertyPreferencesByKeysKeyedByKey( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_GUESTCARD_NOTIFICATION_EMAIL', 'DONT_EXPORT_GUEST_CARDS_IF_NO_UNIT_SELECTED', 'SITETAB_GUESTCARD_NOTIFICATION_EMAIL', 'GUESTCARD_NOTIFICATION_EMAIL', 'LC_GUEST_CARD_NOTIFICATION_EMAIL', 'HIDE_BUILDING_UNIT_FLOOR_PLAN_BED_INFO' ],  $objDatabase );

		$boolIsFromLeasingCenter					= $this->getIsFromLeasingCenter();

		$objPrimediaPropertyManagerUser				= $this->fetchPrimediaPropertyManagerUser( $objClient, $objProperty, $objDatabase );
		$objPropertyTimeZone						= CTimeZones::fetchTimeZoneByPropertyIdByCid( $objProperty->getId(), $objClient->getId(), $objDatabase );

		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) $objPropertyUnit->setUnitCounts( $objPropertyUnit->fetchSpaceCount( $objDatabase ) );

		if( true == $boolIsAvailabilityAlertOrScheduleTour ) {
			$objPrimaryPropertyAddress		= $objProperty->getOrFetchPrimaryPropertyAddress( $objDatabase );
			$objPrimaryPropertyPhoneNumber  = $objProperty->getOrFetchPrimaryPropertyPhoneNumber( $objDatabase );
			$objOfficePhoneNumber  			= $objProperty->getOrFetchOfficePhoneNumber( $objDatabase );
		}

		if( true == valArr( $arrobjWebsitePreferences ) && array_key_exists( 'GUEST_CARD_EMAIL_TEMPLATE_TYPE', $arrobjWebsitePreferences ) && 1 == $arrobjWebsitePreferences['GUEST_CARD_EMAIL_TEMPLATE_TYPE']->getValue() ) {

			$arrobjPropertyFloorplans		        = $objProperty->fetchPublishedPropertyFloorplans( $objDatabase, true, $intLimit = 3 );

			$arrobjCommunityAmenities		        = $objProperty->fetchPublishedAmenityRateAssociationsByArCascadeIdByAmenityTypeIds( CArCascade::PROPERTY, [ CAmenityType::COMMUNITY ], $objDatabase );
			$arrobjPropertyPhotoMediaFiles	        = $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PHOTO, $objDatabase, false );
			$objOverviewMarketingMediaAssociation	= $objProperty->getOrFetchOverviewMarketingMediaAssociation( $objDatabase );
			$arrobjPropertyPreferences		        = $objProperty->getOrFetchPropertyPreferences( $objDatabase );
			$arrobjPropertyAddresses		        = $objProperty->fetchPublishedPropertyAddresses( $objDatabase );

			if( true == valArr( $arrobjPropertyFloorplans ) ) {
				$arrobjFloorplanMarketingMediaAssociations = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjPropertyFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ], $objClient->getId(), $objDatabase );

				if( true == valArr( $arrobjFloorplanMarketingMediaAssociations ) ) {
					$arrobjFloorplanMarketingMediaAssociations = rekeyObjects( 'ReferenceId', $arrobjFloorplanMarketingMediaAssociations );
				}
			}

			if( true == valObj( $objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
				$objProperty->setPropertyLogoMarketingMediaAssociation( $objMarketingMediaAssociation );
			}

			if( true == valObj( $objOverviewMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
				$objProperty->setOverviewMarketingMediaAssociation( $objOverviewMarketingMediaAssociation );
			}
		}

		if( false == $this->getIsFromLeasingCenter() ) {
			$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByApplicationIdByEventTypeIdsByPropertyIdByCid( $objApplication->getId(), [ CEventType::ONLINE_GUEST_CARD, CEventType::DUPLICATE_LEAD ], $objApplication->getPropertyId(), $objClient->getId(), $objDatabase );
		}

		$objTourOnsiteVisitEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchTourEventByLeaseIntervalIdByEventTypeIdsByCid( $objApplication->getLeaseIntervalId(), [ CEventType::TOUR, CEventType::ONSITE_VISIT ], [], $objClient->getId(), $objDatabase );

		if( false == valObj( $objTourOnsiteVisitEvent, 'CEvent' ) ) {
			$objTourOnsiteVisitEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchTourEventByLeaseIntervalIdByEventTypeIdsByCid( $objApplication->getLeaseIntervalId(), [ CEventType::SCHEDULED_APPOINTMENT ], [], $objClient->getId(), $objDatabase );
		}

		if( false == valObj( $objTourOnsiteVisitEvent, 'CEvent' ) ) {
			$objTourOnsiteVisitEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchTourEventByLeaseIntervalIdByEventTypeIdsByCid( $objApplication->getLeaseIntervalId(), [ CEventType::SCHEDULED_FOLLOWUP ], [ CEventSubType::MANUALLY_SCHEDULED_FOLLOW_UP ], $objClient->getId(), $objDatabase );
		}

		$arrintUnitSpaceIds = [];
		if( true == valObj( $objTourOnsiteVisitEvent, 'CEvent' ) ) {
			$arrobjEventReferences = ( array ) \Psi\Eos\Entrata\CEventReferences::createService()->fetchEventReferencesByEventIdByEventReferenceTypeIdByCid( $objTourOnsiteVisitEvent->getId(), CEventReferenceType::UNIT_SPACE, $objClient->getId(), $objDatabase );
			$arrintUnitSpaceIds    = array_keys( rekeyObjects( 'ReferenceId', $arrobjEventReferences ) );
		}

		if( false == valArr( $arrintUnitSpaceIds ) && true == valId( $objApplication->getUnitSpaceId() ) ) {
			$arrintUnitSpaceIds = [ $objApplication->getUnitSpaceId() ];
		}

		$arrobjUnitSpaces	= ( array ) CUnitSpaces::createService()->fetchUnitSpacesByIdsByCid( $arrintUnitSpaceIds, $objClient->getId(), $objDatabase );

		// This code handles the Lead management Vendor email
		$objPropertyPreference										= getArrayElementByKey( 'DONT_EXPORT_GUEST_CARDS_IF_NO_UNIT_SELECTED', $arrobjPropertyPreferences );
		$objSiteTabletGuestCardNotificationEmailPropertyPreference	= getArrayElementByKey( 'SITETAB_GUESTCARD_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );
		$objGuestCardNotificationEmailPropertyPreference			= getArrayElementByKey( 'GUESTCARD_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );

		// If this is a leasing center guest card, send it to the leasing center email.
		if( CPsProduct::LEASING_CENTER == $objApplication->getPsProductId() ) {
			$arrstrGuestCardNotificationEmailPropertyEmailAddresses		= $this->getEmailLocalCodeMapping( $objGuestCardNotificationEmailPropertyPreference, $objProperty );
			$objLcGuestCardNotificationEmailPropertyPreference			= getArrayElementByKey( 'LC_GUEST_CARD_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );
			$arrstrLcGuestCardNotificationEmailPropertyEmailAddresses	= $this->getEmailLocalCodeMapping( $objLcGuestCardNotificationEmailPropertyPreference, $objProperty );
			$arrstrPropertyEmailAddresses								= array_merge_recursive( $arrstrGuestCardNotificationEmailPropertyEmailAddresses, $arrstrLcGuestCardNotificationEmailPropertyEmailAddresses );
		} else {
			$arrstrPropertyEmailAddresses		= $this->getEmailLocalCodeMapping( $objGuestCardNotificationEmailPropertyPreference, $objProperty );
		}

		// If this is a site tablet guest card, send it to the sitetablet email.  This would be used so in office leads don't hit the call center.  That's why this condition appears before the transmission vendor option.
		if( CPsProduct::SITE_TABLET == $objApplication->getPsProductId() && true == valObj( $objSiteTabletGuestCardNotificationEmailPropertyPreference, 'CPropertyPreference' ) ) {
			$arrstrPropertyEmailAddresses = $this->getEmailLocalCodeMapping( $objSiteTabletGuestCardNotificationEmailPropertyPreference, $objProperty );
		}

		if( true == isset( $intSwitchClsId ) && false == is_null( $intSwitchClsId ) ) {
			$objPropertyLeadSource = $objProperty->fetchPropertyLeadSourceByLeadSourceId( $intSwitchClsId, $objDatabase );

			if( true == valObj( $objPropertyLeadSource, 'CPropertyLeadSource' ) && false == is_null( $objPropertyLeadSource->getEmailAddress() ) ) {
				$arrstrPropertyEmailAddresses[$objProperty->getLocaleCode()] = $objPropertyLeadSource->getEmailAddress();
			}
		} elseif( true == isset( $intIlsId ) && 0 < ( int ) $intIlsId ) {
			$objPropertyLeadSource = $objProperty->fetchPropertyLeadSourceByInternetListingServiceId( $intIlsId, $objDatabase );

			if( true == valObj( $objPropertyLeadSource, 'CPropertyLeadSource' ) && false == is_null( $objPropertyLeadSource->getEmailAddress() ) ) {
				$arrstrPropertyEmailAddresses[$objProperty->getLocaleCode()] = $objPropertyLeadSource->getEmailAddress();
			}
		}

		if( CPropertyType::SENIOR == $objProperty->getPropertyTypeId() ) {
			$this->m_objCustomerContact = CCustomerContacts::fetchCustomerContactByCustomerContactTypeIdByCustomerIdByCid( CCustomerContactType::REPRESENTATIVE, $objApplicant->getCustomerId(), $objApplicant->getCid(), $objDatabase );
			if( true == valObj( $this->m_objCustomerContact, 'CCustomerContact' ) ) {
				$arrstrPropertyEmailAddresses[$objProperty->getLocaleCode()] = $this->m_objCustomerContact->getEmailAddress();
			}
		}

		if( true == valStr( $objProperty->getLocaleCode() ) ) {
			try {
				$this->m_objLocaleContainer = CLocaleContainer::createService();
				if( true == valStr( $this->m_objLocaleContainer->getLocaleCode() ) ) {
					$this->m_objLocaleContainer->setPreferredLocaleCode( $this->m_objLocaleContainer->getLocaleCode(), $objDatabase, $objProperty );
				} else {
					$this->m_objLocaleContainer->setPreferredLocaleCode( $objProperty->getLocaleCode(), $objDatabase, $objProperty );
				}
			} catch( Exception $objException ) {
				$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
			}
		}

		// system email object
		$objSystemEmail = new CSystemEmail();
		$arrobjSystemEmails = array();

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign( 'property_email_rule', 											$objPropertyEmailRule );

		$objSmarty->assign_by_ref( 'applicant',										$objApplicant );
		$objSmarty->assign_by_ref( 'application',									$objApplication );
		$objSmarty->assign( 'tour_onsite_vist_event',						                $objTourOnsiteVisitEvent );
		$objSmarty->assign( 'unit_spaces',									                $arrobjUnitSpaces );
		$objSmarty->assign( 'EVENT_TYPE_SCHEDULED_FOLLOWUP',						        CEventType::SCHEDULED_FOLLOWUP );
		$objSmarty->assign( 'EVENT_TYPE_SCHEDULED_APPOINTMENT',						        CEventType::SCHEDULED_APPOINTMENT );
		$objSmarty->assign( 'EVENT_SUB_TYPE_MANUALLY_SCHEDULED_FOLLOW_UP',			        CEventSubType::MANUALLY_SCHEDULED_FOLLOW_UP );
		$objSmarty->assign_by_ref( 'property',										$objProperty );
		$objSmarty->assign_by_ref( 'property_unit',									$objPropertyUnit );
		$objSmarty->assign_by_ref( 'unit_space',										$objUnitSpace );
		$objSmarty->assign_by_ref( 'property_floorplan',								$objPropertyFloorplan );
		$objSmarty->assign_by_ref( 'lead_sources',									$arrobjLeadSources );
		$objSmarty->assign_by_ref( 'marketing_media_association',					$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',						$objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'client',											$objClient );
		$objSmarty->assign_by_ref( 'is_sms_contact',									$boolIsSmsContact );
		$objSmarty->assign_by_ref( 'is_from_leasing_center',							$boolIsFromLeasingCenter );
		$objSmarty->assign_by_ref( 'message',										$strMessage );
		$objSmarty->assign_by_ref( 'website_preferences',							$arrobjWebsitePreferences );
		$objSmarty->assign_by_ref( 'property_building',								$objPropertyBuilding );
		$objSmarty->assign_by_ref( 'phone_number_types',								$arrstrPhoneNumberTypes );
		$objSmarty->assign_by_ref( 'is_from_sitetablet',								$boolIsFromSitetablet );
		$objSmarty->assign_by_ref( 'property_time_zone',								$objPropertyTimeZone );
		$objSmarty->assign_by_ref( 'gust_card_custom_text_property_notification',	$objGustCardCustomTextPropertyNotification );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',									CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',										CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',													CONFIG_COMMON_PATH );

		$strWorkNumber = '';
		$strMobileNumber = '';
		if( true == valArr( $arrobjCustomerPhoneNumbers ) ) {
			foreach( $arrobjCustomerPhoneNumbers as $objCustomerPhoneNumber ) {
				if( false == $objCustomerPhoneNumber->getIsPrimary() && $objCustomerPhoneNumber->getPhoneNumberTypeId() == CPhoneNumberType::OFFICE ) {
					$strWorkNumber = $objCustomerPhoneNumber->getPhoneNumber();
					break;
				}
			}
			foreach( $arrobjCustomerPhoneNumbers as $objCustomerPhoneNumber ) {
				if( false == $objCustomerPhoneNumber->getIsPrimary() && $objCustomerPhoneNumber->getPhoneNumberTypeId() == CPhoneNumberType::MOBILE ) {
					$strMobileNumber = $objCustomerPhoneNumber->getPhoneNumber();
					break;
				}
			}
		}

		$objSmarty->assign( 'work_number', $strWorkNumber );
		$objSmarty->assign( 'mobile_number', $strMobileNumber );

		if( NULL != $objCustomerHelper ) {
			$objSmarty->assign_by_ref( 'customer_helper',			$objCustomerHelper );
			$objSmarty->assign( 'CUSTOMER_VOTE_LIKE',						CCustomerVote::LIKE );
			$objSmarty->assign( 'CUSTOMER_VOTE_DISLIKE',					CCustomerVote::DISLIKE );
			$objSmarty->assign( 'CUSTOMER_VOTE_COMMENT',					CCustomerVote::COMMENT );
			$objSmarty->assign( 'CUSTOMER_VOTE_TYPE_FLOORPLAN',				CCustomerVoteType::FLOORPLAN );
			$objSmarty->assign( 'CUSTOMER_VOTE_TYPE_BENEFIT_SELLING',		CCustomerVoteType::BENEFIT_SELLING );
			$objSmarty->assign( 'CUSTOMER_VOTE_TYPE_AMENITY',				CCustomerVoteType::AMENITY );
			$objSmarty->assign( 'CUSTOMER_VOTE_TYPE_QUALIFICATION',			CCustomerVoteType::QUALIFICATION );
			$objSmarty->assign( 'CUSTOMER_VOTE_TYPE_UNIT',					CCustomerVoteType::UNIT );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_NAME_FIRST',			CCustomerDataType::NAME_FIRST );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_NAME_LAST',				CCustomerDataType::NAME_LAST );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_USERNAME',				CCustomerDataType::USERNAME );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_PRIMARY_PHONE',			CCustomerDataType::PRIMARY_PHONE );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_PRIMARY_LEAD_SOURCE',	CCustomerDataType::PRIMARY_LEAD_SOURCE );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_OCCUPANTS',				CCustomerDataType::OCCUPANTS );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_PETS',					CCustomerDataType::PETS );
			$objSmarty->assign( 'CUSTOMER_DATA_TYPE_APPOINTMENT',			CCustomerDataType::APPOINTMENT );
		}

		if( true == $boolIsAvailabilityAlertOrScheduleTour ) {
			$objSmarty->assign_by_ref( 'primary_property_address',	$objPrimaryPropertyAddress );
			$objSmarty->assign_by_ref( 'primary_phone_number',		$objPrimaryPropertyPhoneNumber );
			$objSmarty->assign_by_ref( 'property_phone_number',		$objOfficePhoneNumber );
		}

		if( false == is_null( $strExternalUrl ) ) {
			$objSmarty->assign_by_ref( 'external_brochure', $strExternalUrl );
		}

		if( true == valArr( $arrobjPropertyPreferences ) && array_key_exists( 'HIDE_BUILDING_UNIT_FLOOR_PLAN_BED_INFO', $arrobjPropertyPreferences ) ) {
			$objSmarty->assign( 'is_hide_building_unit_floor_plan_bed_info',	true );
		} else {
			$objSmarty->assign( 'is_hide_building_unit_floor_plan_bed_info',	false );
		}

		$objSmarty->assign( 'media_library_uri', 					CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',			  					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',			CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'senders_ip_address',			        getRemoteIpAddress() );

		if( false == is_null( $objPrimediaPropertyManagerUser ) && ( strtotime( $objPrimediaPropertyManagerUser->getLastLogin() ) == strtotime( $objPrimediaPropertyManagerUser->getCreatedOn() ) ) ) {

			$objSmarty->assign( 'is_primedia_company',	true );

			$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'http://';
			$strRwxSuffix			= ( true == defined( CONFIG_RWX_LOGIN_SUFFIX ) ) ? CONFIG_RWX_LOGIN_SUFFIX : '.entrata.com';
			$strResidentWorksUrl 	= $strSecureHostPrefix . $this->m_objClient->getRwxDomain() . $strRwxSuffix;
			$objSmarty->assign( 'rwx_login_url', $strResidentWorksUrl );

			$objSmarty->assign( 'manager_user', $objPrimediaPropertyManagerUser );
		}

		$objSmarty->assign( 'rent_occupancy_trigger_name',	$strRentOccupancyTriggerName );

		$objAdsController = new CAdsController();
		$objAdsController->setClient( $objClient );
		$objAdsController->setClientDatabase( $objDatabase );

		$objCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objDatabase );

		$objSmarty->assign( 'company_preference',	$objCompanyPreference );
		$objSmarty->assign( 'ads_controller',		$objAdsController );

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objSmarty->assign_by_ref( 'object_storage_gateway', $objObjectStorageGateway );

		if( ( true == valArr( $arrobjWebsitePreferences ) && array_key_exists( 'GUEST_CARD_EMAIL_TEMPLATE_TYPE', $arrobjWebsitePreferences ) && 1 == $arrobjWebsitePreferences['GUEST_CARD_EMAIL_TEMPLATE_TYPE']->getValue() ) ) {

			$objSmarty->assign_by_ref( 'floorplan_amenities',		$arrobjFloorplanAmenities );
			$objSmarty->assign_by_ref( 'community_amenities',		$arrobjCommunityAmenities );

			if( true == valArr( $arrobjPropertyPhotoMediaFiles ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjPropertyPhotoMediaFiles ) ) {
				$objSmarty->assign_by_ref( 'property_photo_media_files', $arrobjPropertyPhotoMediaFiles );
			}

			$objSmarty->assign_by_ref( 'property_floorplans',			$arrobjPropertyFloorplans );
			$objSmarty->assign_by_ref( 'floorplan_medias',				$arrobjFloorplanMarketingMediaAssociations );
			$objSmarty->assign_by_ref( 'property_preferences',			$arrobjPropertyPreferences );
			$objSmarty->assign_by_ref( 'property_addresses',				$arrobjPropertyAddresses );

			$objSmarty->assign( 'address_type_primary',							CAddressType::PRIMARY );

			if( true == $boolIsFromRequestInfo ) {
				$objSmarty->assign( 'email_type',								'request_info' );
			} else {
				$objSmarty->assign( 'email_type',								'guest_card' );
			}

			$objSmarty->assign( 'base_name',								$_SERVER['HTTP_HOST'] );
			$objSmarty->assign( 'path_interfaces_prospect_portal', 			PATH_INTERFACES_PROSPECT_PORTAL );
			$objSmarty->assign( 'PROPERTY_TYPE_COMMERCIAL', 				CPropertyType::COMMERCIAL );
			$objSmarty->assign( 'entrata_website_url', 						CConfig::get( 'entrata_website_url' ) );

			if( true == isset( $intWebsiteId ) ) {
				$strWebsiteWrapperUrl	= \Psi\Eos\Entrata\CWebsiteWrappers::createService()->fetchWebsiteWrapperUrlByWebsiteIdByCid( ( int ) $intWebsiteId, $this->m_objClient->getId(), $objDatabase );
				$strPropertyWrapperUrl	= \Psi\Eos\Entrata\CPropertyWrappers::createService()->fetchPropertyWrapperUrlByPropertyIdByCid( $objProperty->getId(), $this->m_objClient->getId(), $objDatabase );
				$strWebsiteWrapperParsedUrl = parse_url( $strWebsiteWrapperUrl[0]['wrapper_url'] );
				$strWebsiteWrapperDomain = $strWebsiteWrapperParsedUrl['host'];

				$strPropertyWrapperParsedUrl = parse_url( $strPropertyWrapperUrl[0]['wrapper_url'] );
				$strPropertyWrapperDomain = $strPropertyWrapperParsedUrl['host'];
				$objSmarty->assign_by_ref( 'website_wrapper_domain',		$strWebsiteWrapperDomain );
				$objSmarty->assign_by_ref( 'property_wrapper_domain',	$strPropertyWrapperDomain );
			}

			if( true == isset( $strHostDomainName ) ) {
				$objSmarty->assign_by_ref( 'host_domain_name', $strHostDomainName );
			}

			$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_PROSPECT_PORTAL . 'property_system/property_page_system/_common/email_template_layout.tpl' );
		} elseif( ( true == valArr( $arrobjWebsitePreferences ) && array_key_exists( 'GUEST_CARD_EMAIL_TEMPLATE_TYPE', $arrobjWebsitePreferences ) && 2 == $arrobjWebsitePreferences['GUEST_CARD_EMAIL_TEMPLATE_TYPE']->getValue() ) || true == $boolIsAvailabilityAlertOrScheduleTour ) {
			$objSmarty->assign( 'email_type', 2 );
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/premium_leads_guest_card_email.tpl' );
		} elseif( NULL != $objCustomerHelper ) {
			$objSmarty->assign( 'email_type', 2 );
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/lc_guest_card_email.tpl' );
		} else {
			$objSmarty->assign( 'email_type', 2 );
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/guest_card_email.tpl' );
		}
		$objAdsController = new CAdsController();
		$objAdsController->setClient( $objClient );
		$objAdsController->setClientDatabase( $objDatabase );

		$objCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objDatabase );

		foreach( $arrstrPropertyEmailAddresses as $strKeyLocalAddress => $arrstrValueEmailAddress ) {

			if( true == valStr( $arrstrValueEmailAddress ) ) {
				$arrstrValueEmailAddress = CEmail::createEmailArrayFromString( $arrstrValueEmailAddress );
			}

			if( true == valArr( $arrstrValueEmailAddress ) && 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrValueEmailAddress ) ) {
				$arrstrManagerEmailIds = array_unique( $arrstrValueEmailAddress );
				if( true == valStr( $strKeyLocalAddress ) ) {
					try {
						$this->m_objLocaleContainer = CLocaleContainer::createService();
						if( true == valStr( $this->m_objLocaleContainer->getLocaleCode() ) ) {
							$this->m_objLocaleContainer->setPreferredLocaleCode( $this->m_objLocaleContainer->getLocaleCode(), $objDatabase, $objProperty );
						} else {
							$this->m_objLocaleContainer->setPreferredLocaleCode( $strKeyLocalAddress, $objDatabase, $objProperty );
						}
					} catch( Exception $objException ) {
						$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
					}
				}

				$objSystemEmail->setCid( $objClient->getId() );
				$objSystemEmail->setPropertyId( $objProperty->getId() );

				$objSmarty->assign( 'company_preference', $objCompanyPreference );
				$objSmarty->assign( 'ads_controller', $objAdsController );
				$objSmarty->assign( 'email_type', 1 );

				$strManagerHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/premium_notification_guest_card_email.tpl' );

				if( NULL != $objCustomerHelper ) {
					$strManagerHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/lc_guest_card_email.tpl' );
				}

				$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_GUEST_CARD );
				$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
				$objSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );
				$objSystemEmail->setHtmlContent( $strManagerHtmlEmailOutput );
				if( false == valStr( $strSubject ) ) {
					if( false == $boolIsFromLeasingCenter ) {
						$strSubject = ( ( true == isset( $strSubjectPrefix ) ) ? $strSubjectPrefix : '' ) . __( 'Online Guest Card Received Lead ID : {%d,0,nots}', [ $objApplication->getId() ] );
					} else {
						if( true == $objApplication->getIsGuestCardDeduped() ) {
							$strSubject = __( 'Online Guest Card Updated - Lead ID : {%d,0,nots}', [ $objApplication->getId() ] );
						} else {
							$strSubject = __( 'Online Guest Card Received - Lead ID : {%d,0,nots}', [ $objApplication->getId() ] );
						}

						if( false == is_null( $this->getUnitNumber() ) ) {
							$strSubject .= ' - ' . $this->getUnitNumber();
							if( false == is_null( $objApplicant->getNameFirst() ) && false == is_null( $objApplicant->getNameLast() ) ) {
								$strSubject .= ', ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast();
							} else {
								if( false == is_null( $objApplicant->getUsername() ) ) {
									$strSubject .= ' ' . $objApplicant->getUsername();
								}
							}
						} else {
							if( false == is_null( $objApplicant->getNameFirst() ) && false == is_null( $objApplicant->getNameLast() ) ) {
								$strSubject .= ' - ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast();
							} else {
								if( false == is_null( $objApplicant->getUsername() ) ) {
									$strSubject .= ' - ' . $objApplicant->getUsername();
								}
							}
						}

						$strSubject .= ', ' . $objProperty->getPropertyName();
					}
				} else {
					__( '{ %s,0 } Lead ID : { %d,1,nots }', [ $strSubject, $objApplication->getId() ] );
				}

				$objSystemEmail->setSubject( $strSubject );
				if( true == valArr( $arrstrManagerEmailIds ) ) {
					foreach( $arrstrManagerEmailIds as $arrstrManagerEmailId ) {
						$objSystemEmail->setToEmailAddress( $arrstrManagerEmailId );

						$objManagerSystemEmail = clone $objSystemEmail;

						$objPropertyPreference = getArrayElementByKey( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );

						if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 0 < strlen( $objPropertyPreference->getValue() ) ) {
							$objManagerSystemEmail->setFromEmailAddress( preg_replace( '/\s\s+/', '', $objPropertyPreference->getValue() ) );
						}

						if( false == is_null( $objApplicant->getUsername() ) ) {

							if( false == is_null( $objApplicant->getNameFirst() ) && false == is_null( $objApplicant->getNameLast() ) ) {
								$strReplyEmail = $objApplicant->getNameFirst() . '' . $objApplicant->getNameLast() . ' <' . $objApplicant->getUsername() . '> ';
							} else {
								$strReplyEmail = $objApplicant->getUsername();
							}

							$objManagerSystemEmail->setReplyToEmailAddress( $strReplyEmail );
						}
						array_push( $arrobjSystemEmails, $objManagerSystemEmail );
					}
				}
				if( true == valObj( $this->m_objLocaleContainer, 'CLocaleContainer' ) ) {
					$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
				}
			}
		}

		// the system email assignment applicant
		if( false == is_null( $objApplicant->getUsername() ) && false == $boolIsIlsParser && false == $boolSendToManagerOnly ) {

			$objApplicantSystemEmail = clone $objSystemEmail;
			$objApplicantSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objApplicantSystemEmail->setHtmlContent( $strHtmlEmailOutput );
 			$objApplicantSystemEmail->setApplicantId( $objApplicant->getId() );
 			$objApplicantSystemEmail->setCompanyEmployeeId( NULL );

			// Set from email address as per the property settings
			$objPropertyPreference = getArrayElementByKey( 'FROM_GUESTCARD_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );

			if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 0 < strlen( $objPropertyPreference->getValue() ) ) {
				$objApplicantSystemEmail->setFromEmailAddress( preg_replace( '/\s\s+/', '', $objPropertyPreference->getValue() ) );
			}

			$arrobjSystemEmails[] = $objApplicantSystemEmail;
		}

		$arrobjSystemEmails = ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );
		if( true == valObj( $objEmailDatabase, 'CDatabase' ) && true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				if( false == valStr( $objSystemEmail->getToEmailAddress() ) ) {
					continue;
				}

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					continue;
				}
			}
		}
		if( true == valObj( $this->m_objLocaleContainer, 'CLocaleContainer' ) ) {
			$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
		}
	}

	public function sendManagerAndApplicantEmails( $objClientDatabase, $objEmailDatabase = NULL, $strSubject = NULL, $arrobjArTransactions = NULL, $arrobjRecurringRates = NULL, $strSecureBaseName = NULL, $boolSendToManagerOnly = false, $intCompanyUserId = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrobjSystemEmails				= [];
		$arrobjCompanyEmployees			= [];
		$strCustomerFullName			= '';

		$this->loadCommonData( $objClientDatabase );
		$arrobjPropertyPreferences			= $this->m_objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_PAYMENT_NOTIFICATION_EMAIL', 'FROM_APPLICATION_NOTIFICATION_EMAIL', 'SEND_SIMPLE_APPLICATION_CONFIRMATION_EMAIL' ],  $objClientDatabase );
		$arrobjPropertyPreferences			= rekeyObjects( 'Key', $arrobjPropertyPreferences );
		$objClient							= $this->m_objProperty->fetchClient( $objClientDatabase );
		$arrintOccupancyTypeIds 			= ( true == valStr( $this->m_objApplication->getOccupancyTypeId() ) ) ? [ $this->m_objApplication->getOccupancyTypeId() ] : [ COccupancyType::CONVENTIONAL ];
		$arrobjCustomerRelationships		= ( array ) $this->m_objApplication->fetchCustomerRelationshipsByOccupancyTypeIds( $arrintOccupancyTypeIds, $objClientDatabase );

		$arrobjPropertyApplicationPreferences 	= ( array ) $this->m_objApplication->fetchPropertyApplicationPreferences( $objClientDatabase );

		$objAdsController = new CAdsController();
		$objAdsController->setClient( $objClient );
		$objAdsController->setClientDatabase( $objClientDatabase );

		$strPrimaryPreferredLocaleCode = $this->fetchPreferredLocaleCode( $objClientDatabase );
		CLocaleContainer::createService()->setPreferredLocaleCode( $strPrimaryPreferredLocaleCode, $this->m_objDatabase, $this->m_objProperty );

		$objCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objClientDatabase );

		$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objClientDatabase );

		if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
			$arrobjCompanyEmployees		= $this->m_objProperty->fetchActiveLeasingAgents( $this->m_objApplication->getLeasingAgentId(), $objClientDatabase );
		}

		if( false == valStr( $strSecureBaseName ) ) {
			$strSecureBaseName = $this->m_objApplication->generateWebsiteUrl( $objClientDatabase );
		}

		// If OTR file is there then send it as attachment.
		$objOfferToRentFile		= $this->getOfferToRentFile();
		$boolIsFileAttachment	= false;

		if( true == valObj( $objOfferToRentFile, 'CFile' ) && CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
			$boolIsFileAttachment = true;
		}
		// the smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$this->assignSmartyData( $objSmarty, $strSecureBaseName, $arrobjArTransactions, $arrobjRecurringRates );

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objSmarty->assign_by_ref( 'object_storage_gateway', $objObjectStorageGateway );

		$objSmarty->assign( 'customer_full_name',			$strCustomerFullName );
		$objSmarty->assign( 'customer_relationships',		$arrobjCustomerRelationships );
		$objSmarty->assign( 'email_type', 					1 ); // application email body content for Attachment
		$objSmarty->assign( 'application_status', 			$this->m_objApplication->getCustomApplicationStatus() );
		$objSmarty->assign( 'is_file_attachment', 			$boolIsFileAttachment ); // email attachment flag
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',		CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	                CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$objSmarty->assign_by_ref( 'company_employees', 	$arrobjCompanyEmployees );
		$objSmarty->assign_by_ref( 'property_preferences', 	$arrobjPropertyPreferences );
		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );

		$objSmarty->assign( 'email_type', 2 ); //  application email body content for applicant
		$objSmarty->assign( 'LEGAL_US_RESIDENT_YES',			 	CApplicantDetail::LEGAL_US_RESIDENT_YES );
		$objSmarty->assign( 'LEGAL_US_RESIDENT_YES_WITH_USCIS', 	CApplicantDetail::LEGAL_US_RESIDENT_YES_WITH_USCIS );
		$objSmarty->assign( 'LEGAL_US_RESIDENT_NO', 				CApplicantDetail::LEGAL_US_RESIDENT_NO );
		$objSmarty->assign( 'FREQUENCY_WEEKLY', 					CFrequency::WEEKLY );
		$objSmarty->assign( 'FREQUENCY_YEARLY', 					CFrequency::YEARLY );
		$objSmarty->assign( 'ads_controller', $objAdsController );

		$objSmarty->assign_by_ref( 'property_application_preferences', 	$arrobjPropertyApplicationPreferences );

		$intPropertyId = ( 0 < $this->m_objProperty->getPropertyId() ) ? $this->m_objProperty->getPropertyId() :  $this->m_objProperty->getId();

		$strApplicationUrl = $this->m_objApplication->generateWebsiteUrl( $objClientDatabase ) . 'Apartments/module/application_authentication/action/view_login/property[id]/' . $intPropertyId;

		if( CCustomerType::PRIMARY != $this->getCustomerTypeId() && true == is_null( $this->m_objApplicant->getPassword() ) && true == is_null( $this->getCompletedOn() ) ) {

			$strUniqueKey = \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );

			$objOneTimeLink = new COneTimeLink();
			$objOneTimeLink->setValues( [ 'cid' => $this->m_objClient->getId(), 'applicant_id' => $this->m_objApplicant->getId(), 'key_encrypted' => $strUniqueKey ] );

			switch( NULL ) {
				default:
					$objClientDatabase->begin();

					if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $objClientDatabase ) ) {

						trigger_error( 'one time link failed to inserted.', E_USER_ERROR );
						$objClientDatabase->rollback();
						break;
					}

					$objClientDatabase->commit();

					$strApplicationUrl = $this->m_objApplication->generateWebsiteUrl( $objClientDatabase ) . 'Apartments/module/application_authentication/action/create_applicant_password/property[id]/' . ( ( true == is_null( $this->m_objProperty->getPropertyId() ) ) ? $this->m_objProperty->getId() : $this->m_objProperty->getPropertyId() ) . '/applicant[key]/' . urlencode( $strUniqueKey ) . '/applicant[id]/' . $this->m_objApplicant->getId() . '/is_primary_applicant/0/';
			}
		}

		$objSmarty->assign( 'application_url',	$strApplicationUrl );

		$strPreferredLocaleCode = \CLanguage::ENGLISH;
		if( false == $boolSendToManagerOnly ) {
			if( true == valObj( $this->getLeaseCustomer(), 'CCustomer' ) ) {
				$objCustomer = $this->getLeaseCustomer();
			} else {
				$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
			}
			if( true == valObj( $objCustomer, 'CCustomer' ) && true == valStr( $objCustomer->getPreferredLocaleCode() ) ) {
				$strPreferredLocaleCode = $objCustomer->getPreferredLocaleCode();
			} elseif( true == valObj( $this->m_objProperty, 'CProperty' ) && true == valStr( $this->m_objProperty->getLocaleCode() ) ) {
				$strPreferredLocaleCode = $this->m_objProperty->getLocaleCode();
			}

			CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objClientDatabase, $this->m_objProperty );

			$strApplicantEmailSubject = __( 'Online Application Submitted' );
			$strApplicantHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_completion_simple_email.tpl' );

			CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
		}

		$objSmarty->assign( 'company_preference',						$objCompanyPreference );
		$objSmarty->assign( 'ads_controller',							$objAdsController );
		$objSmarty->assign( 'email_type', 								3 );//  application email body content for Application Manager

		$arrobjUnitTypes = $this->m_objProperty->fetchUnitTypes( $objClientDatabase );

		$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strRwxSuffix			= ( true == defined( CONFIG_RWX_LOGIN_SUFFIX ) ) ? CONFIG_RWX_LOGIN_SUFFIX : '.entrata.com';
		$strResidentWorksUrl	= NULL;

		if( false == is_null( $intCompanyUserId ) && $intCompanyUserId > 0 ) {

			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $this->m_objClient->getId(), $objClientDatabase );

			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
				$objModule = \Psi\Eos\Entrata\CModules::createService()->fetchModuleByModuleName( 'prospect_systemxxx', $objClientDatabase );

				if( true == valObj( $objModule, 'CModule' ) ) {
					$objCompanyUserPermission = $objCompanyUser->fetchCompanyUserPermissionByModuleId( $objModule->getId(), $objClientDatabase );

					if( true == valObj( $objCompanyUserPermission, 'CCompanyUserPermission' ) ) {
						$strResidentWorksUrl = $strSecureHostPrefix . $this->m_objClient->getRwxDomain() . $strRwxSuffix . '/?module=prospect_systemxxx';
					}
				}
			}
		}

		$strResidentWorksUrl = $strSecureHostPrefix . $this->m_objClient->getRwxDomain() . $strRwxSuffix . '/?module=prospect_systemxxx';

		$strCustomerTypeName = $this->getCustomerTypeName();
		if( $this->getCustomerTypeId() == CCustomerType::PRIMARY )
			$strCustomerTypeName = $strCustomerTypeName . ' applicant';

		$objSmarty->assign( 'residentworks_url',    $strResidentWorksUrl );
		$objSmarty->assign( 'unit_types',		    $arrobjUnitTypes );
		$objSmarty->assign( 'customer_type_name',	$strCustomerTypeName );

		$strRwxSuffix	= ( false == is_null( CConfig::get( 'rwx_login_suffix' ) ) ) ? CConfig::get( 'rwx_login_suffix' ) : '.entrata.com';

		$strApplicationPrintUrl = $strSecureHostPrefix . $this->m_objClient->getRwxDomain() . $strRwxSuffix . '?module=application_applicantsxxx&action=print_applicant_applications&application[id]=' . $this->m_objApplication->getId() . '&is_application_form=1&applicant[ids][]=' . $this->m_objApplicant->getId() . '&is_from_email=1';
		$objSmarty->assign( 'application_print_url', $strApplicationPrintUrl );

		if( true == valObj( $this->m_objProperty, 'CProperty' ) && true == valStr( $this->m_objProperty->getLocaleCode() ) ) {
			$strPreferredLocaleCode = $this->m_objProperty->getLocaleCode();
		}

		CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objClientDatabase, $this->m_objProperty );

		$strManagerHtmlEmailContent  = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_simple_email.tpl' );
		$strManagerEmailSubject = ( false == is_null( $strSubject ) ) ? $strSubject : __( 'Online Application Submitted' );

		$strPaymentManagerHtmlEmailContent 	= '';
		$arrstrPaymentEmailAddresses 		= [];

		if( true == valArr( $this->m_arrobjArPayments ) ) {
			//  application email body content for Payment Manager
			$arrstrPaymentEmailAddresses = $this->m_objProperty->fetchPaymentNotificationEmails( $objClientDatabase );

			if( true == valArr( $arrstrPaymentEmailAddresses ) ) {
				$objSmarty->assign( 'email_type', 4 );//   Payment Notification Mail for Payment Manager
				$strPaymentManagerHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );
				$strPaymentManagerEmailSubject = __( 'Payment Notification Email' );

				$objSmarty->assign( 'email_type', 3 );
			}
		}
		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );

		// system email object
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setPropertyId( $this->m_objProperty->getId() );
		$objSystemEmail->setCid( $this->m_objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::APPLICATION_DOCUMENT );
		$objSystemEmail->setSubject( $strManagerEmailSubject );
		$objSystemEmail->setHtmlContent( $strManagerHtmlEmailContent );

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'FROM_APPLICATION_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() ) ) {
			$strFromApplicationNotificationEmail = preg_replace( '/\s\s+/', '', $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() );
			$objSystemEmail->setFromEmailAddress( $strFromApplicationNotificationEmail );
			$objSystemEmail->setReplyToEmailAddress( $strFromApplicationNotificationEmail );
		}

		$arrstrAttachmentFilePaths	= [];
		$arrobjEmailAttachments = [];
		$arrobjApplicantApplicationMedias = ( array ) $this->fetchCompanyMediaFilesByMediaTypeId( CMediaType::APPLICATION, $objClientDatabase );

		if( true == valArr( $arrobjApplicantApplicationMedias ) ) {
			foreach( $arrobjApplicantApplicationMedias as $objApplicantApplicationMedia ) {

				$objEmailAttachment = new CEmailAttachment();
				$strAttachmentFileName = $objApplicantApplicationMedia->getFileName();
				$strAttachmentFilePath = PATH_MOUNTS . $objApplicantApplicationMedia->getFullsizeUri();
				$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
				$objEmailAttachment->setFileName( $strAttachmentFileName );

				$arrobjEmailAttachments[]		= $objEmailAttachment;
				$arrstrAttachmentFileNames[]	= $objApplicantApplicationMedia->getFileName();
				$arrstrAttachmentFilePaths[]	= PATH_MOUNTS . $objApplicantApplicationMedia->getFullsizeUri();
			}
		}

		// If OTR file is there then send it as attachment.
		$objOfferToRentFile = $this->getOfferToRentFile();

		if( true == valObj( $objOfferToRentFile, 'CFile' ) && CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {

			$this->m_objApplication->loadObjectStorageGateway();
			$objOfferToRentFile->setFileTypeSystemCode( CFileType::SYSTEM_CODE_OFFER_TO_RENT );
			$arrmixRequest = $objOfferToRentFile->fetchStoredObject( $this->getDatabase() )->createGatewayRequest();
			$objObjectStorageGatewayResponse = $this->m_objApplication->getObjectStorageGateway()->getObject( $arrmixRequest );

			$strContent = $objObjectStorageGatewayResponse['data'];

			$strAttachmentFileName = 'offer_to_rent' . '_' . date( 'Ymdhis' ) . '.pdf';

			$strAttachmentFilePath = PATH_NON_BACKUP_MOUNTS . 'email_attachment/' . $strAttachmentFileName;

			if( false == ( $strFileContent = CFileIo::filePutContents( $strAttachmentFilePath,  $strContent ) ) ) {
				trigger_error( 'Script failed to write data to directory.', E_USER_WARNING );
				return false;
			}

			$objOtrDocumentAttachment = new CEmailAttachment();
			$objOtrDocumentAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
			$objOtrDocumentAttachment->setFileName( $strAttachmentFileName );

			$arrobjEmailAttachments[]			= $objOtrDocumentAttachment;
			$arrstrAttachmentFilePaths[]		= $strAttachmentFilePath;

		}

		$arrstrPropertyEmailAddresses = $this->m_objProperty->fetchApplicationNotificationEmails( $objClientDatabase );

		// the system email assignment managerl
		if( true == [ $arrstrPropertyEmailAddresses ] && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrPropertyEmailAddresses ) ) {

			foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {

				$objManagerSystemEmail = clone $objSystemEmail;

				$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );

				$arrobjSystemEmails[] = $objManagerSystemEmail;
			}
		}

		if( true == valArr( $arrstrPaymentEmailAddresses ) && 0 < strlen( $strPaymentManagerHtmlEmailContent ) ) {

			$objSystemEmail->setHtmlContent( $strPaymentManagerHtmlEmailContent );
			$objSystemEmail->setSubject( $strPaymentManagerEmailSubject );
			$strFromPaymentNotificationEmail = NULL;
			if( true == isset( $arrobjPropertyPreferences['FROM_PAYMENT_NOTIFICATION_EMAIL'] ) && 0 < strlen( $arrobjPropertyPreferences['FROM_PAYMENT_NOTIFICATION_EMAIL']->getValue() ) ) {
				$strFromPaymentNotificationEmail = preg_replace( '/\s\s+/', '', $arrobjPropertyPreferences['FROM_PAYMENT_NOTIFICATION_EMAIL']->getValue() );
			}

			foreach( $arrstrPaymentEmailAddresses as $strEmailAddress ) {

				$objCurrentPaymentSystemEmail = clone $objSystemEmail;
				if( 0 < strlen( $strFromPaymentNotificationEmail ) ) {
					$objCurrentPaymentSystemEmail->setFromEmailAddress( $strFromPaymentNotificationEmail );
				}
				$objCurrentPaymentSystemEmail->setToEmailAddress( $strEmailAddress );
				$arrobjSystemEmails[] = $objCurrentPaymentSystemEmail;
			}
		}

		// the system email assignment applicant
		if( false == $boolSendToManagerOnly ) {

			$objApplicantSystemEmail = clone $objSystemEmail;

			$objApplicantSystemEmail->setSubject( $strApplicantEmailSubject );
			$objApplicantSystemEmail->setToEmailAddress( $this->m_objApplicant->getUsername() );
			$objApplicantSystemEmail->setHtmlContent( $strApplicantHtmlEmailContent );
			$arrobjSystemEmails[] = $objApplicantSystemEmail;
		}

		if( true == valArr( $arrobjSystemEmails ) ) {

			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) ) {
					continue;
				}

				if( true == valArr( $arrobjEmailAttachments ) ) {
					foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
						$objSystemEmail->addEmailAttachment( $objEmailAttachment );
					}
				}

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					trigger_error( 'Failed to insert system email.', E_USER_WARNING );
					continue;
				}
			}
		}
		CLocaleContainer::createService()->unsetPreferredLocaleCode( $this->m_objDatabase );

		if( false == isset( $objApplicantSystemEmail ) || false == valObj( $objApplicantSystemEmail, 'CSystemEmail' ) ) return true;

		if( true == valArr( $arrstrAttachmentFilePaths ) ) {
			foreach( $arrstrAttachmentFilePaths as $strAttachmentFilePath ) {
				if( true == file_exists( $strAttachmentFilePath ) ) {
					unlink( $strAttachmentFilePath );
				}
			}
		}
		return true;
	}

	public function sendPhoneAuthPendingEmails( $arrobjArPayments, $objClientDatabase, $objEmailDatabase = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrobjSystemEmails				= [];
		$arrobjCompanyEmployees			= [];
		$strCustomerFullName			= '';

		// load the reqd data
		$objClient			= $this->getOrFetchClient( $objClientDatabase );
		if( true == valObj( $objClient, 'CClient' ) ) {
			$strPrimaryPreferredLocaleCode = $this->fetchPreferredLocaleCode( $objClientDatabase );
			CLocaleContainer::createService()->setPreferredLocaleCode( $strPrimaryPreferredLocaleCode, $this->m_objDatabase, $this->m_objProperty );
		}
		// Only Applicant's Name( FN MN LN ) required for this email. Hence taking from 'applicants' table. No need to take Applicant's Name from 'view_applicants' - SAM
		$objApplicant					= $this->getOrFetchApplicant( $objClientDatabase, false );
		$objApplication					= $this->getOrFetchApplication( $objClientDatabase );
		$objLease						= $objApplication->getOrFetchLease( $objClientDatabase );
		$objProperty					= $objApplication->getOrFetchProperty( $objClientDatabase );
		$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
		$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		$arrobjPropertyApplicationPreferences 	= ( array ) $objApplication->fetchPropertyApplicationPreferences( $objClientDatabase );
		$arrobjPropertyPreferences				= ( array ) $objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_APPLICATION_NOTIFICATION_EMAIL', 'SEND_SIMPLE_APPLICATION_CONFIRMATION_EMAIL' ], $objClientDatabase );
		$arrobjPropertyPreferences				= rekeyObjects( 'Key', $arrobjPropertyPreferences );
		$arrobjPaymentTypes 					= CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );
		$arrobjMerchantAccounts					= ( array ) $this->m_objClient->fetchMerchantAccounts( $objClientDatabase );
		$strCustomerFullName					= CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $objApplicant->getCustomerId(), $objApplicant->getCid(), $objClientDatabase );

		$strSecureBaseName 		= $objApplication->generateWebsiteUrl( $objClientDatabase );
		$objAdsController		= NULL;
		$objCompanyPreference 	= $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objClientDatabase );

		if( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) ) {
			$objAdsController = new CAdsController();
			$objAdsController->setClient( $objClient );
			$objAdsController->setClientDatabase( $objClientDatabase );
		}

		unset( $arrobjPropertyApplicationPreferences['SHOW_APPLICANT_SCHEDULED_CHARGES'] );// to hide the scheduled charges section from the tpl file

		// the smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign_by_ref( 'client', 						$objClient );
		$objSmarty->assign_by_ref( 'applicant', 					$objApplicant );
		$objSmarty->assign_by_ref( 'application', 					$objApplication );
		$objSmarty->assign_by_ref( 'lease', 						$objLease );
		$objSmarty->assign_by_ref( 'property', 						$objProperty );
		$objSmarty->assign_by_ref( 'ads_controller',				$objAdsController );

		$objSmarty->assign_by_ref( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',	$objLogoMarketingMediaAssociation );

		$objSmarty->assign_by_ref( 'property_application_preferences', 	$arrobjPropertyApplicationPreferences );
		$objSmarty->assign_by_ref( 'property_preferences', 				$arrobjPropertyPreferences );
		$objSmarty->assign_by_ref( 'ar_payments', 						$arrobjArPayments );
		$objSmarty->assign_by_ref( 'payment_types',						$arrobjPaymentTypes );
		$objSmarty->assign_by_ref( 'merchant_accounts', 				$arrobjMerchantAccounts );
		$objSmarty->assign( 'terms_and_condition_path', 				$strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->m_objApplication->getId() . '&company_application_id=' . $this->m_objApplication->getCompanyApplicationId() );
		$objSmarty->assign( 'image_url',	                            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$objSmarty->assign( 'email_type', 2 ); // to applicant
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',				CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'customer_full_name',					$strCustomerFullName );
		$objSmarty->assign( 'ads_controller', $objAdsController );

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objSmarty->assign_by_ref( 'object_storage_gateway', $objObjectStorageGateway );

		$strApplicantHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/application_phone_auth_pending_email.tpl' );
		$strApplicantEmailSubject = __( 'Application Payment Notification Email' );

		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );

		$objSmarty->assign( 'email_type', 3 ); // to property manager

		$strPreferredLocaleCode = \CLanguage::ENGLISH;
		if( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) {
			$strPreferredLocaleCode = $objProperty->getLocaleCode();
		}
		CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objClientDatabase, $objProperty );

		$strPaymentManagerHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/application_phone_auth_pending_email.tpl' );
		$strPaymentManagerEmailSubject = __( 'Application Payment Notification Email' );

		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );

		// system email object
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::APPLICATION_DOCUMENT );

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'FROM_APPLICATION_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() ) ) {
			$strFromApplicationNotificationEmail = preg_replace( '/\s\s+/', '', $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() );
			$objSystemEmail->setFromEmailAddress( $strFromApplicationNotificationEmail );
			$objSystemEmail->setReplyToEmailAddress( $strFromApplicationNotificationEmail );
		}

		// notification emails
		$arrstrPaymentEmailAddresses = ( array ) $objProperty->fetchPaymentNotificationEmails( $objClientDatabase );

		foreach( $arrstrPaymentEmailAddresses as $strEmailAddress ) {
			$objCurrentPaymentSystemEmail = clone $objSystemEmail;

			$objCurrentPaymentSystemEmail->setToEmailAddress( $strEmailAddress );
			$objCurrentPaymentSystemEmail->setSubject( $strPaymentManagerEmailSubject );
			$objCurrentPaymentSystemEmail->setHtmlContent( $strPaymentManagerHtmlEmailContent );

			$arrobjSystemEmails[] = $objCurrentPaymentSystemEmail;
		}

		// applicant emails
		$objApplicantSystemEmail = clone $objSystemEmail;

		$objApplicantSystemEmail->setToEmailAddress( $this->m_objApplicant->getUsername() );
		$objApplicantSystemEmail->setSubject( $strApplicantEmailSubject );
		$objApplicantSystemEmail->setHtmlContent( $strApplicantHtmlEmailContent );
		CLocaleContainer::createService()->unsetPreferredLocaleCode( $this->m_objDatabase );
		$arrobjSystemEmails[] = $objApplicantSystemEmail;
		foreach( $arrobjSystemEmails as $objSystemEmail ) {
			if( true == valStr( $objSystemEmail->getToEmailAddress() ) && false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				trigger_error( 'Failed to insert system email.', E_USER_WARNING );
				continue;
			}
		}

		return true;
	}

	public function generateDocumentFile( $strHtmlContent, $objApplication, $objDatabase, $intApplicantId = NULL ) {

		// create HTML file for application email

		$objClient  	= $this->getOrFetchClient( $objDatabase );

		$strTitle 			= 'Application Adverse Action Letter';
		$strFileName 		= 'Application_Adverse_Action_Letter' . '_' . $intApplicantId . '_' . date( 'Ymdhis' ) . '.html';
		$strFileTypeCode	= CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER;

		$objFileAssociation = $objApplication->createFileAssociation();
		$objFile            = $objClient->createFile();

		$objFile->setFileExtensionId( CFileExtension::HTML );
		$objFile->setFileName( $strFileName );
		$objFile->setTitle( $strTitle );
		$objFile->setPropertyId( $objApplication->getPropertyId() );
		$objFile->setApplicationId( $objApplication->getId() );

		$objFileType = $objClient->fetchFileTypeBySystemCode( $strFileTypeCode, $objDatabase );

		if( true == valObj( $objFileType, 'CFileType' ) ) {
			$objFile->setFileTypeId( $objFileType->getId() );
			$objFile->setFileTypeSystemCode( $objFileType->getSystemCode() );
		}

		$strFilePath        = $objFile->buildFilePath( CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER );
		$objFile->setFilePath( $strFilePath );

		if( false == $objFile->generateAndPutRvStorageGatewayObject( [ 'data' => $strHtmlContent ], CScreeningUtils::getObjectStorageGateway() ) ) {
			if( false == $objFile->generateFile( $strHtmlContent ) ) {
				trigger_error( 'Failed to generate AA Letter for application ' . $objApplication->getId() . ' Cid ' . $objApplication->getCid() . ' Applicant id ' . ( int ) $intApplicantId, E_USER_WARNING );
				return false;
			}
		}

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objFile->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$objFile->deleteFile();
					$objDatabase->rollback();
					break;
				}

				$objFileAssociation->setFileId( $objFile->getId() );
				$objFileAssociation->setApplicantId( $this->getApplicantId() );
				$objFileAssociation->setPropertyId( $objApplication->getPropertyId() );

				if( false == $objFileAssociation->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();
				return true;
		}

		return false;
	}

	public function sendManagerAndGuestCardEmailsForDifferentialData( $objDatabase, $objEmailDatabase = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrobjSystemEmails		= [];

		$objClient  	= $this->getOrFetchClient( $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) return false;

		$objApplicant			= $this->getOrFetchApplicant( $objDatabase );
		$objApplication			= $this->getOrFetchApplication( $objDatabase );

		if( false == valObj( $objApplicant, 'CApplicant' ) || false == valObj( $objApplication, 'CApplication' ) ) return false;

		$arrobjLeadSources	= $objClient->fetchLeadSources( $objDatabase );
		$objProperty		= $objApplication->getOrFetchProperty( $objDatabase );

		// Emails should not be sent to disabled properties as per the requirement
		if( true == valObj( $objProperty, 'CProperty' ) && true == $objProperty->processSystemEmail() ) {
			return true;
		}

		if( true == valStr( $objProperty->getLocaleCode() ) ) {
			try {
				$this->m_objLocaleContainer = CLocaleContainer::createService();
				$this->m_objLocaleContainer->setPreferredLocaleCode( $objProperty->getLocaleCode(), $objDatabase, $objProperty );
			} catch( Exception $objException ) {
				$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
			}
		}

		$objPropertyUnit							= $objApplication->getOrFetchPropertyUnit( $objDatabase );
		$objUnitSpace								= $objApplication->getOrFetchUnitSpace( $objDatabase );
		$objPropertyFloorplan						= $objProperty->fetchPropertyFloorplanById( $objApplication->getPropertyFloorplanId(), $objDatabase );
		$objMarketingMediaAssociation				= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objGustCardCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'GUEST_CARD_CUSTOM_TEXT', $objDatabase );
		$objLogoMarketingMediaAssociation			= $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$arrobjPropertyPreferences 					= $objProperty->fetchPropertyPreferencesByKeysKeyedByKey( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_GUESTCARD_NOTIFICATION_EMAIL', 'PROSPECT_IN_EXTERNAL' ],  $objDatabase );
		$objPropertyTimeZone						= CTimeZones::fetchTimeZoneByPropertyIdByCid( $objProperty->getId(), $objClient->getId(), $objDatabase );
		$objPropertyEmailRule 						= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::COMPANY_GUEST_CARD, $objProperty->getId(), $objDatabase );

		$objPropertyPreference = getArrayElementByKey( 'PROSPECT_IN_EXTERNAL', $arrobjPropertyPreferences );

		// the smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign_by_ref( 'applicant', 					$objApplicant );
		$objSmarty->assign_by_ref( 'application', 					$objApplication );
		$objSmarty->assign_by_ref( 'property', 						$objProperty );
		$objSmarty->assign_by_ref( 'property_unit',					$objPropertyUnit );
		$objSmarty->assign_by_ref( 'unit_space', 					$objUnitSpace );
		$objSmarty->assign_by_ref( 'property_floorplan', 			$objPropertyFloorplan );
		$objSmarty->assign_by_ref( 'lead_sources', 					$arrobjLeadSources );
		$objSmarty->assign_by_ref( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', 	$objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'client', 						$objClient );
		$objSmarty->assign_by_ref( 'property_time_zone', 			$objPropertyTimeZone );

		$objSmarty->assign_by_ref( 'gust_card_custom_text_property_notification', 	$objGustCardCustomTextPropertyNotification );

		$objSmarty->assign( 'property_email_rule', 					$objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri', 					CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',			  					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',				CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	                        CONFIG_COMMON_PATH );

		$objAdsController = new CAdsController();
		$objAdsController->setClient( $objClient );
		$objAdsController->setClientDatabase( $objDatabase );

		$objCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objDatabase );

		$objSmarty->assign( 'company_preference',	$objCompanyPreference );
		$objSmarty->assign( 'ads_controller',		$objAdsController );

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objSmarty->assign_by_ref( 'object_storage_gateway', $objObjectStorageGateway );

		// system email object
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setPropertyId( $objProperty->getId() );

		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_GUEST_CARD );

		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		$objPropertyEmailAddresses	= $objProperty->fetchGuestCardNotificationPreferenceObj( $objDatabase );

		$arrstrPropertyEmailAddressesAll = $this->getEmailLocalCodeMapping( $objPropertyEmailAddresses, $objProperty );

		$objSmarty->assign( 'email_type',			  					2 );
		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/guest_card_email.tpl' );

		$objPropertyPreference = getArrayElementByKey( 'PROSPECT_IN_EXTERNAL', $arrobjPropertyPreferences );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( 'TO_PROPERTY' == $objPropertyPreference->getValue() || 'TO_BOTH' == $objPropertyPreference->getValue() ) ) {

			if( true == valArr( $arrstrPropertyEmailAddressesAll ) ) {

				foreach( $arrstrPropertyEmailAddressesAll as $strKeyCode => $arrstrEmailids ) {

					if( true == valArr( $arrstrEmailids ) && 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrEmailids ) ) {
						$arrstrPropertyEmailAddresses = array_unique( $arrstrEmailids );
					}

					if( true == valStr( $strKeyCode ) ) {
						try {
							$this->m_objLocaleContainer = CLocaleContainer::createService();
							$this->m_objLocaleContainer->setPreferredLocaleCode( $strKeyCode, $objDatabase, $objProperty );
						} catch( Exception $objException ) {
							$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
						}
					}
					$strSubject = __( 'Online Guest Card Received' );
					$objSystemEmail->setSubject( $strSubject );

					$objSmarty->assign( 'email_type',			  					1 );
					$strManagerHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/guest_card_email.tpl' );

					$objSystemEmail->setHtmlContent( $strManagerHtmlEmailOutput );
					foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
						$objSystemEmail->setToEmailAddress( $strEmailAddress );

						$objManagerSystemEmail = clone $objSystemEmail;

						$objPropertyPreferenceFromEmail = getArrayElementByKey( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );

						if( true == valObj( $objPropertyPreferenceFromEmail, 'CPropertyPreference' ) && 0 < strlen( $objPropertyPreferenceFromEmail->getValue() ) ) {
							$objManagerSystemEmail->setFromEmailAddress( $objPropertyPreferenceFromEmail->getValue() );
						}

						if( false == is_null( $objApplicant->getUsername() ) ) {

							if( false == is_null( $objApplicant->getNameFirst() ) && false == is_null( $objApplicant->getNameLast() ) ) {
								$strReplyEmail = $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . ' <' . $objApplicant->getUsername() . '> ';

							} else {
								$strReplyEmail = $objApplicant->getUsername();
							}

							$objManagerSystemEmail->setReplyToEmailAddress( $strReplyEmail );
						}

						$arrobjSystemEmails[] = $objManagerSystemEmail;
					}
				}
				if( true == valObj( $this->m_objLocaleContainer, 'CLocaleContainer' ) ) {
					$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
				}
			}
		}

		// the system email assignment applicant
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( 'TO_PROSPECT' == $objPropertyPreference->getValue() || 'TO_BOTH' == $objPropertyPreference->getValue() ) ) {

			$objApplicantSystemEmail = clone $objSystemEmail;
			$objApplicantSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objApplicantSystemEmail->setHtmlContent( $strHtmlEmailOutput );
			$objApplicantSystemEmail->setApplicantId( $objApplicant->getId() );

			// Set from email address as per the property settings
			$objPropertyPreference = getArrayElementByKey( 'FROM_GUESTCARD_NOTIFICATION_EMAIL', $arrobjPropertyPreferences );

			if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 0 < strlen( $objPropertyPreference->getValue() ) ) {
				$objApplicantSystemEmail->setFromEmailAddress( $objPropertyPreference->getValue() );
			}

			$arrobjSystemEmails[] = $objApplicantSystemEmail;
		}

		$arrobjSystemEmails = ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				if( false == valStr( $objSystemEmail->getToEmailAddress() ) )
					continue;

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) )
					continue;
			}
		}
		if( true == valObj( $this->m_objLocaleContainer, 'CLocaleContainer' ) ) {
			$this->m_objLocaleContainer->unsetPreferredLocaleCode( $objDatabase );
		}
		return true;
	}

	public function sendManagerAndApplicantEmailsForDifferentialData( $objClientDatabase, $objEmailDatabase = NULL, $strSubject = NULL, $arrobjArTransactions = NULL, $arrobjRecurringRates = NULL, $strSecureBaseName = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrobjSystemEmails				= [];

		$this->loadCommonData( $objClientDatabase );

		$arrobjPropertyPreferences			= $this->m_objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_APPLICATION_NOTIFICATION_EMAIL', 'APPLICANT_IN_EXTERNAL' ],  $objClientDatabase );
		$arrobjPropertyPreferences			= rekeyObjects( 'Key', $arrobjPropertyPreferences );
		$objClient							= $this->m_objProperty->fetchClient( $objClientDatabase );
		$arrintOccupancyTypeIds 			= ( true == valStr( $this->m_objApplication->getOccupancyTypeId() ) ) ? [ $this->m_objApplication->getOccupancyTypeId() ] : [ COccupancyType::CONVENTIONAL ];
		$arrobjCustomerRelationships		= ( array ) $this->m_objApplication->fetchCustomerRelationshipsByOccupancyTypeIds( $arrintOccupancyTypeIds, $objClientDatabase );

		// Emails should not be sent to disabled properties as per the requirement
		if( true == valObj( $this->m_objProperty, 'CProperty' ) && true == $this->m_objProperty->processSystemEmail() ) {
			// if we return false then it will trigger_error in CYardiRetrieveDifferentialDataTransport. So just returning as True and not processing for system email.[SB]
			return true;
		}

		$objAdsController = new CAdsController();
		$objAdsController->setClient( $objClient );
		$objAdsController->setClientDatabase( $objClientDatabase );

		$objCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'BLOCK_EMAIL_ADS', $objClientDatabase );

		// the smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objSmarty->assign_by_ref( 'object_storage_gateway', $objObjectStorageGateway );

		$this->assignSmartyData( $objSmarty, $strSecureBaseName, $arrobjArTransactions, $arrobjRecurringRates );
		$objSmarty->assign( 'customer_relationships',	$arrobjCustomerRelationships );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',	CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'email_type', 1 ); //  application email body content for Attachment
		$objSmarty->assign( 'ads_controller', $objAdsController );
		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );

		$objSmarty->assign( 'email_type', 2 ); //  application email body content for applicant
		$strApplicantHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );

		$objSmarty->assign( 'property_preferences', 	$arrobjPropertyPreferences );
		$objSmarty->assign( 'company_preference',	$objCompanyPreference );


		$objSmarty->assign( 'email_type', 3 );//  application email body content for Application Manager
		$strManagerHtmlEmailContent  = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );

		$strPaymentManagerHtmlEmailContent = '';

		if( true == valArr( $this->m_arrobjArPayments ) ) {
			//  application email body content for Payment Manager
			$strPaymentManagerHtmlEmailContent  = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );
		}

		// system email object
		$objSystemEmail = new CSystemEmail();
		$objEmailAttachment = new CEmailAttachment();

		$objSystemEmail->setPropertyId( $this->m_objProperty->getId() );
		$objSystemEmail->setCid( $this->m_objClient->getId() );

		if( false == is_null( $strSubject ) ) {
			$objSystemEmail->setSubject( $strSubject );
		} else {
			$objSystemEmail->setSubject( 'Online Application Submitted' );
		}

		$arrobjEmailAttachments = [];
		$arrstrAttachmentFilePaths = [];

		$strPropertyName 	= $this->m_objProperty->getPropertyName();
		$strAttachmentFileName 	= 'application_' . $strPropertyName . '_' . date( 'YmdHis' ) . '.html';

		$strAttachmentFileName = preg_replace( '/[^a-zA-Z0-9.]/', '', trim( $strAttachmentFileName ) );

		$strAttachmentFilePath = CONFIG_CACHE_DIRECTORY . $strAttachmentFileName;

		if( false == ( $strFileContent = CFileIo::filePutContents( $strAttachmentFilePath,  $strHtmlEmailOutput ) ) ) {
			trigger_error( 'Script failed to write data to mounts directory.', E_USER_WARNING );
			return false;
		}

		$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
		$objEmailAttachment->setFileName( $strAttachmentFileName );

		$arrstrAttachmentFileNames[]	= $strAttachmentFileName;
		$arrstrAttachmentFilePaths[]	= $strAttachmentFilePath;
		$arrobjEmailAttachments[]		= $objEmailAttachment;

		$arrobjApplicantApplicationMedias = ( array ) $this->fetchCompanyMediaFilesByMediaTypeId( CMediaType::APPLICATION, $objClientDatabase );

		if( true == valArr( $arrobjApplicantApplicationMedias ) ) {
			foreach( $arrobjApplicantApplicationMedias as $objApplicantApplicationMedia ) {

				$strAttachmentFileName 	= $objApplicantApplicationMedia->getFileName();
				$strAttachmentFilePath  = PATH_MOUNTS . $objApplicantApplicationMedia->getFullsizeUri();

				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
				$objEmailAttachment->setFileName( $strAttachmentFileName );

				$arrstrAttachmentFileNames[]	= $objApplicantApplicationMedia->getFileName();
				$arrstrAttachmentFilePaths[]	= $strAttachmentFilePath;
				$arrobjEmailAttachments[]		= $objEmailAttachment;

			}
		}

		// the policy document attachments

		if( true == valArr( $this->m_arrobjApplicantApplicationFileAssociations ) ) {

			foreach( $this->m_arrobjApplicantApplicationFileAssociations as $objApplicantApplicationFileAssociation ) {

				if( false == file_exists( $objApplicantApplicationFileAssociation->getFullFilePath() . $objApplicantApplicationFileAssociation->getFileName() ) )
					continue;

				$strContent = file_get_contents( $objApplicantApplicationFileAssociation->getFullFilePath() . $objApplicantApplicationFileAssociation->getFileName() );

				$objSmarty->assign( 'email_type', 5 ); //  application email body content for policy document Attachment.
				$objSmarty->assign( 'policy_document_content', preg_replace( '@style="height:400px;\s*overflow:auto;"@', '', $strContent ) );
				$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/applicant_application_email.tpl' );
				$strAttachmentFileName 	= 'policy_document_' . $objApplicantApplicationFileAssociation->getDocumentName() . '_' . date( 'YmdHis' ) . '.html';

				$strAttachmentFileName = preg_replace( '/[^a-zA-Z0-9.]/', '', trim( $strAttachmentFileName ) );

				$strAttachmentFilePath = CONFIG_CACHE_DIRECTORY . $strAttachmentFileName;

				if( false == ( $strFileContent = CFileIo::filePutContents( $strAttachmentFilePath,  $strHtmlEmailOutput ) ) ) {
					trigger_error( 'Script failed to write data to mounts directory.', E_USER_WARNING );
					return false;
				}

				$objPolicyDocumentAttachment = clone $objEmailAttachment;
				$objPolicyDocumentAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
				$objPolicyDocumentAttachment->setFileName( $strAttachmentFileName );

				$arrstrAttachmentFileNames[]		= $strAttachmentFileName;
				$arrobjEmailAttachments[]			= $objPolicyDocumentAttachment;
				$arrstrAttachmentFilePaths[]		= $strAttachmentFilePath;
			}
		}

		// OTR attached
		if( true == valArr( $this->m_arrmixOfferToRent ) && false == is_null( $this->m_arrmixOfferToRent['recurring'] ) ) {

			$objPropertyGlSetting		= $this->m_objProperty->getOrfetchPropertyGlSetting( $objClientDatabase );
			$this->m_objRentRate = $this->m_objApplication->fetchRateByChargeCodeId( $objPropertyGlSetting->getRentArCodeId(), $objClientDatabase );

			$this->m_arrobjGuarantors	= [];

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $this->m_arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $this->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
				$intRequiredApplicantAge = $this->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
			} else {
				$intRequiredApplicantAge = 18;
			}

			if( true == valArr( $this->m_arrobjApplicants ) ) {
				foreach( $this->m_arrobjApplicants as $intKey => $objApplicant ) {
					if( CCustomerType::GUARANTOR == $objApplicant->getCustomerTypeId() ) {
						$this->m_arrobjGuarantors[$objApplicant->getId()] = $objApplicant;
						unset( $this->m_arrobjApplicants[$intKey] );
					} elseif( false == $objApplicant->checkApplicantAge( $intRequiredApplicantAge ) ) {
						$objApplicant->getBirthDate();
						unset( $this->m_arrobjApplicants[$intKey] );
					}
				}
			}

			$strHtmlEmailOutput 				= $objSmarty->fetch( PATH_INTERFACES_PROSPECT_PORTAL . 'property_system/applicant_system/applicant_summary/view_offer_to_rent.tpl' );
			$strAttachmentFileName 				= 'Offer_To_Rent' . $strPropertyName . '_' . date( 'YmdHis' ) . '.html';
			$strAttachmentFileName 				= preg_replace( '/[^a-zA-Z0-9.]/', '', trim( $strAttachmentFileName ) );

			$strAttachmentFilePath = CONFIG_CACHE_DIRECTORY . $strAttachmentFileName;

			if( false == ( $strFileContent = CFileIo::filePutContents( $strAttachmentFilePath,  $strHtmlEmailOutput ) ) ) {
				trigger_error( 'Script failed to write data to mounts directory.', E_USER_WARNING );
				return false;
			}

			$objOtrDocumentAttachment = clone $objEmailAttachment;
			$objOtrDocumentAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
			$objOtrDocumentAttachment->setFileName( $strAttachmentFileName );

			$arrstrAttachmentFileNames[]		= $strAttachmentFileName;
			$arrobjEmailAttachments[]			= $objOtrDocumentAttachment;
			$arrstrAttachmentFilePaths[]		= $strAttachmentFilePath;
		}

		$objPropertyPreference = getArrayElementByKey( 'APPLICANT_IN_EXTERNAL', $arrobjPropertyPreferences );

		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::APPLICATION_DOCUMENT );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( 'TO_BOTH' == $objPropertyPreference->getValue() || 'TO_PROPERTY' == $objPropertyPreference->getValue() ) ) {

			$arrstrPaymentEmailAddresses	= $this->m_objProperty->fetchPaymentNotificationEmails( $objClientDatabase );
			$arrstrPropertyEmailAddresses	= $this->m_objProperty->fetchApplicationNotificationEmails( $objClientDatabase );

			if( true == valArr( $this->m_arrobjArPayments ) ) {
				if( true == [ $arrstrPaymentEmailAddresses ] && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrPaymentEmailAddresses ) ) {
					$arrstrPropertyEmailAddresses	= array_unique( array_merge( $arrstrPaymentEmailAddresses, $arrstrPropertyEmailAddresses ) );
				}
			}

			// the system email assignment managerl
			if( true == [ $arrstrPropertyEmailAddresses ] && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrPropertyEmailAddresses ) ) {

				foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
					$objSystemEmail->setToEmailAddress( $strEmailAddress );
					if( 0 < strlen( $strManagerHtmlEmailContent ) ) {
						$objSystemEmail->setHtmlContent( $strManagerHtmlEmailContent );
					}

					$objCurrentSystemEmail = clone $objSystemEmail;

					if( true == valArr( $arrobjPropertyPreferences ) ) {
						if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
							$objCurrentSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
						}
					}

					if( false == is_null( $this->m_objApplicant->getUsername() ) ) {

						if( false == is_null( $this->m_objApplicant->getNameFirst() ) && false == is_null( $this->m_objApplicant->getNameLast() ) ) {
							$objCurrentSystemEmail->setReplyToEmailAddress( $this->m_objApplicant->getNameFirst() . ' ' . $this->m_objApplicant->getNameLast() . ' <' . $this->m_objApplicant->getUsername() . '> ' );
						} else {
							$objCurrentSystemEmail->setReplyToEmailAddress( $this->m_objApplicant->getUsername() );
						}

					}

					$arrobjSystemEmails[] = $objCurrentSystemEmail;
				}
			}
		}

		// the system email assignment applicant
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( 'TO_BOTH' == $objPropertyPreference->getValue() || 'TO_APPLICANT' == $objPropertyPreference->getValue() ) ) {

			$objSystemEmail->setToEmailAddress( $this->m_objApplicant->getUsername() );
			$objApplicantSystemEmail = clone $objSystemEmail;

			if( 0 < strlen( $strApplicantHtmlEmailContent ) ) {
				$objApplicantSystemEmail->setHtmlContent( $strApplicantHtmlEmailContent );
			}

			// Set from email address as per the property settings
			if( true == valArr( $arrobjPropertyPreferences ) ) {
				if( true == array_key_exists( 'FROM_APPLICATION_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() ) ) {
					$objApplicantSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() );
				}
			}

			$arrobjSystemEmails[] = $objApplicantSystemEmail;
		}

		if( true == valArr( $arrobjSystemEmails ) ) {

			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) ) {
					continue;
				}

				if( true == valArr( $arrobjEmailAttachments ) ) {
					foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
						$objSystemEmail->addEmailAttachment( $objEmailAttachment );
					}
				}

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {

					trigger_error( 'Failed to insert system email.', E_USER_WARNING );
					continue;
				}

			}
		}
		if( true == valArr( $arrstrAttachmentFilePaths ) ) {
			foreach( $arrstrAttachmentFilePaths as $strAttachmentFilePath ) {
				unlink( $strAttachmentFilePath );
			}
		}

		return true;
	}

	public function sendTransmissionFailureEmail( $objDatabase, $objEmailDatabase = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrstrPhoneNumberTypes = [
			CPhoneNumberType::HOME	=> 'Home',
			CPhoneNumberType::OFFICE	=> 'Office',
			CPhoneNumberType::MOBILE	=> 'Cell'
		];

		$objClient 					= $this->getOrFetchClient( $objDatabase );
		$objApplicant				= $this->fetchApplicant( $objDatabase );
		$objApplication				= $this->getOrFetchApplication( $objDatabase );
		$objApplicationDuplicate	= $this->fetchApplicationDuplicate( $objDatabase );
		$arrobjLeadSources			= $objClient->fetchLeadSources( $objDatabase );
		$objProperty				= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchProperty( $objDatabase ) : $objApplicationDuplicate->getOrFetchProperty( $objDatabase );

		// Emails should not be sent to disabled properties as per the requirement
		if( true == valObj( $objProperty, 'CProperty' ) && true == $objProperty->processSystemEmail() ) {
			return true;
		}

		$objPropertyUnit			= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchPropertyUnit( $objDatabase ) : $objApplicationDuplicate->fetchPropertyUnit( $objDatabase );
		$objUnitSpace				= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getOrFetchUnitSpace( $objDatabase ) : $objApplicationDuplicate->fetchUnitSpace( $objDatabase );

		$intPropertyFloorplanId		= ( false == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) ? $objApplication->getPropertyFloorplanId() : $objApplicationDuplicate->getPropertyFloorplanId();
		$objPropertyFloorplan		= $objProperty->fetchPropertyFloorplanById( $intPropertyFloorplanId, $objDatabase );

		$objPropertyBuilding		= ( true == valObj( $objPropertyUnit, 'CPropertyUnit', 'PropertyBuildingId' ) ) ? $objPropertyUnit->fetchPropertyBuilding( $objDatabase ) : NULL;
		$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objPropertyPreferences 	= $objProperty->fetchPropertyPreferenceByKey( 'FAILED_GUESTCARD_NOTIFICATION_EMAIL_TO_LEAD_MANAGEMENT', $objDatabase );
		$objPropertyTimeZone		= CTimeZones::fetchTimeZoneByPropertyIdByCid( $objProperty->getId(), $objClient->getId(), $objDatabase );
		$objPropertyEmailRule		= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) $objPropertyUnit->setUnitCounts( $objPropertyUnit->fetchSpaceCount( $objDatabase ) );

		// the smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign_by_ref( 'applicant',										$objApplicant );
		$objSmarty->assign_by_ref( 'application',									$objApplication );
		$objSmarty->assign_by_ref( 'property',										$objProperty );
		$objSmarty->assign_by_ref( 'property_unit',									$objPropertyUnit );
		$objSmarty->assign_by_ref( 'unit_space',									$objUnitSpace );
		$objSmarty->assign_by_ref( 'property_floorplan',							$objPropertyFloorplan );
		$objSmarty->assign_by_ref( 'lead_sources',									$arrobjLeadSources );
		$objSmarty->assign_by_ref( 'marketing_media_association',					$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',					$objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'client',										$objClient );
		$objSmarty->assign_by_ref( 'website_preferences',							$arrobjWebsitePreferences );
		$objSmarty->assign_by_ref( 'property_building',								$objPropertyBuilding );
		$objSmarty->assign_by_ref( 'phone_number_types',							$arrstrPhoneNumberTypes );
		$objSmarty->assign_by_ref( 'property_time_zone',							$objPropertyTimeZone );

		$objSmarty->assign( 'property_email_rule',					$objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri', 					CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',			  					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',				CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',							CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'CONFIG_COMMON_PATH',					CONFIG_COMMON_PATH );

		$arrobjSystemEmails = [];
		$objSystemEmail	= new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setSubject( 'Application Transmission Failure Email.' );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		$objPropertyPreferences 		= $objProperty->fetchPropertyPreferenceByKey( 'FAILED_GUESTCARD_NOTIFICATION_EMAIL_TO_LEAD_MANAGEMENT', $objDatabase );
		$arrstrPropertyEmailAddresses	= ( true == valObj( $objPropertyPreferences, 'CPropertyPreference' ) ) ? CEmail::createEmailArrayFromString( $objPropertyPreferences->getValue() ) : [];

		if( true == valArr( $arrstrPropertyEmailAddresses ) ) {
			$objSystemEmail->setToEmailAddress( implode( ',', $arrstrPropertyEmailAddresses ) );
			$strManagerHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/transmission_failure_email.tpl' );

			if( 0 < strlen( $strManagerHtmlEmailContent ) ) {
				$objSystemEmail->setHtmlContent( $strManagerHtmlEmailContent );
			}

			$objSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

			$objSystemEmail	= CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objSystemEmail, $objPropertyEmailRule );

			if( false == valObj( $objSystemEmail, 'CSystemEmail' ) ) return true;

			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				trigger_error( 'System Email record failed to insert.', E_USER_WARNING );
				return;
			}

		}

		return true;
	}

	public function calculateTermMonth( $strLeaseStartDate, $intLeaseLength ) {

		if( true == is_null( $strLeaseStartDate ) || true == is_null( $intLeaseLength ) ) return NULL;

		$strLeaseEndDate 	= strtotime( date( 'Y-m-d', strtotime( $strLeaseStartDate ) ) . ' +' . $intLeaseLength . ' month' );
		$arrstrLeaseEndDate = getdate( $strLeaseEndDate );
		$intLeaseMonth		= $arrstrLeaseEndDate['mday'] - 1;
		$strLeaseTerm 		= $strLeaseStartDate . ' - ' . $arrstrLeaseEndDate['mon'] . '/' . $intLeaseMonth . '/' . $arrstrLeaseEndDate['year'];
		return $strLeaseTerm;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchForm( $objDatabase ) {
		return CForms::fetchFormByIdByCid( $this->getApplicationFormId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDocument( $objDatabase ) {
		return CDocuments::fetchApplicationDocumentByIdByCid( $this->getApplicationDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseDocument( $objDatabase ) {
		return CDocuments::fetchDocumentByIdByCid( $this->getLeaseDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationBySystemCode( $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdBySystemCodeByCid( $this->getApplicantId(), $this->getApplicationId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseFileAssociationsBySystemCodes( $arrstrSystemCodes, $objDatabase, $boolIsCustomerType = true ) {
		if( false == valArr( $arrstrSystemCodes ) ) return NULL;

		return CFileAssociations::createService()->fetchFileAssociationsByApplicantIdByApplicationIdBySystemCodesByCustomerTypeIdByCid( $this->getApplicantId(), $this->getApplicationId(), $arrstrSystemCodes, $this->getCustomerTypeId(), $this->getCid(), $objDatabase, $boolIsCustomerType );
	}

	public function fetchMainLeaseFile( $objDatabase ) {
		return \Psi\Eos\Entrata\CFiles::createService()->fetchFileByApplicationIdByApplicantIdByDocumentIdByCid( $this->getApplicationId(), $this->getApplicantId(), $this->getLeaseDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationById( $intDocumentAssociationId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdByIdByCid( $this->getApplicantId(), $this->getApplicationId(), $intDocumentAssociationId, $this->getCid(), $objDatabase );
	}

	public function fetchFilesByFileTypeSystemCodes( $arrstrFileTypeSystemCodes, $objDatabase ) {
		return \Psi\Eos\Entrata\CFiles::createService()->fetchFilesByApplicationIdByApplicantIdsByFileTypeSystemCodesByCid( $this->getApplicationId(), [ $this->getApplicantId() ], $arrstrFileTypeSystemCodes, $this->getCid(), $objDatabase );
	}

	public function fetchFileByFileTypeSystemCode( $strFileTypeSystemCode, $objDatabase ) {
		return \Psi\Eos\Entrata\CFiles::createService()->fetchFileByApplicationIdByApplicantIdByFileTypeSystemCodeByCid( $this->getApplicationId(), $this->getApplicantId(), $strFileTypeSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileSignaturesByFileAssociationIds( $arrintFileAssociationIds, $objDatabase, $intFileTypeId = NULL ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByApplicantApplicationIdByFileAssociationIdsByCid( $this->getId(), $arrintFileAssociationIds, $this->getCid(), $objDatabase, $intFileTypeId );
	}

	public function fetchFileSignaturesCountByFileAssociationIds( $arrintFileAssociationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesCountByApplicantApplicationIdByFileAssociationIdsByCid( $this->getId(), $arrintFileAssociationIds, $this->getCid(), $objDatabase );
	}

	public function fetchFileSignaturesByFileAssociationIdsToDelete( $arrintFileAssociationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByApplicantApplicationIdByFileAssociationIdsToDeleteByCid( $this->getId(), $arrintFileAssociationIds, $this->getCid(), $objDatabase );
	}

	public function fetchArPayments( $objDatabase, $intApplicationStageId = NULL, $intApplicationStatusId = NULL ) {
		return CArPayments::fetchApplicationArPaymentsByApplicantIdByApplicationIdByApplicationStageStatusIdByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCid(), $intApplicationStageId, $intApplicationStatusId, $objDatabase );
	}

	public function fetchArPaymentByApplicantIdByApplicationIdByCidByPaymentStatusId( $intPaymentType, $objDatabase ) {
		return CArPayments::fetchApplicationArPaymentByApplicantIdByApplicationIdByCidByPaymentStatusId( $this->getApplicantId(), $this->getApplicationId(),  $intPaymentType, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationPaymentByApplicationIdByArPaymentIdByCid( $intArPaymentId, $objDatabase ) {
		return CApplicationPayments::fetchApplicationPaymentByApplicationIdByArPaymentIdByCid( $this->getApplicationId(),  $intArPaymentId, $this->getCid(), $objDatabase );
	}

	public function fetchArTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPreApplicationArPayments( $objDatabase ) {
		return CArPayments::fetchPreApplicationArPaymentsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMainApplicationArPayments( $objDatabase ) {
		return CArPayments::fetchMainApplicationArPaymentsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationArPaymentsByApplicationStageStatusIds( $arrintApplicationStageStatusIds, $objDatabase, $boolIsPhoneAuthPending = false ) {
		return CArPayments::fetchArPaymentsByApplicantIdByApplicationIdByApplicationStageStatusIdsByCid( $this->getApplicantId(), $this->getApplicationId(), $arrintApplicationStageStatusIds, $this->getCid(), $objDatabase, $boolIsPhoneAuthPending );
	}

	public function fetchApplicationArPaymentStatusType( $objDatabase ) {
		return CArPayments::fetchApplicationArPaymentsStatusByApplicantIdByApplicationIdByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	// MEGARATES CHANGES : Removed function fetchApplicantApplicationPetsWithPetTypeName
	// Replacement Function: CCustomerPets::fetchCustomerPetsByLeaseIdByCustomerIdWithPetTypeName

	public function fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCustomerIdsByCidWithPetTypeName( $objDatabase ) {
		\Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCustomerIdsByCidWithPetTypeName( $this->getLeaseId(), $this->getLeaseIntervalTypeId(), [ $this->getCustomerId() ], $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicates( $objDatabase ) {
		return CApplicationDuplicates::fetchSimpleApplicationDuplicatesByApplicantApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicateById( $intContactId, $objDatabase ) {
		return CApplicationDuplicates::fetchApplicationDuplicateByApplicantApplicationIdByIdByCid( $this->m_intId, $intContactId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicate( $objDatabase ) {
		return CApplicationDuplicates::fetchApplicationDuplicateByApplicantApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByApplicantApplicationIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIdByDataKey( $intMediaTypeId, $strDataKey, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByApplicantApplicationIdByMediaTypeIdByDataKeyByCid( $this->getId(), $intMediaTypeId, $strDataKey, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileById( $intCompanyMediaFileId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationMediaByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return CApplicantApplicationMedias::fetchApplicantApplicationMediaByApplicantApplicationIdByCompanyMediaFileIdByCid( $this->getId(), $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantPropertyOccupation( $intPropertyId, $intOccupationId, $objDatabase ) {
		return CPropertyOccupations::fetchApplicantPropertyOccupationByApplicationIdByApplicantIdByOccupationIdByCid( $intPropertyId, $this->getApplicationId(), $this->getApplicantId(), $intOccupationId, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsByFileTypeSystemCode( $strSystemCode, $objDatabase, $boolIsExcludeDeletedFileAssociations = false ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantByApplicationIdByCustomerTypeIdByFileTypeSystemCodeByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCustomerTypeId(), $strSystemCode, $this->getCid(), $objDatabase, $boolIsExcludeDeletedFileAssociations );
	}

	public function fetchApplicantLeaseFileAssociations( $objDatabase ) {
		return CFileAssociations::createService()->fetchApplicantLeaseFileAssociationsByApplicationIdByApplicantIdByCid( $this->getApplicationId(), $this->getApplicantId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsToDelete( $objDatabase, $boolExcludeDeleted = false ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantByApplicationIdByCustomerTypeIdToDeleteByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCustomerTypeId(), $this->getCid(), $objDatabase, $boolExcludeDeleted );
	}

	public function fetchFileAssociationsByFileTypeSystemCodeByIsAttacheToEmail( $strSystemCode, $boolIsAttachToEmail, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantByApplicationIdByCustomerTypeIdByFileTypeSystemCodeByIsAttacheToEmailByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCustomerTypeId(), $strSystemCode, $boolIsAttachToEmail, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsByCustomerTypeIdByFileTypeSystemCode( $intCustomerTypeId, $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantByApplicationIdByCustomerTypeIdByFileTypeSystemCodeByCid( $this->getApplicantId(),  $this->getApplicationId(), $intCustomerTypeId, $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsCountByFileTypeSystemCode( $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsCountByApplicantByApplicationIdByCustomerTypeIdByFileTypeSystemCodeByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCustomerTypeId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationByFileTypeSystemCodeByDocumentId( $strSystemCode, $intDocumentId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdByFileTypeSystemCodeByDocumentIdByCid( $this->getApplicantId(), $this->getApplicationId(), $strSystemCode, $intDocumentId, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsByFileTypeSystemCodeByDocumentId( $strSystemCode, $intDocumentId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantIdByApplicationIdByFileTypeSystemCodeByDocumentIdByCid( $this->getApplicantId(), $this->getApplicationId(), $strSystemCode, $intDocumentId, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociations( $objDatabase ) {
		return CFileAssociations::createService()->fetchApplicantLeaseFileAssociationsByApplicationIdByApplicantIdByCid( $this->getApplicationId(), $this->getApplicantId(), $this->getCid(), $objDatabase );
	}

	public function fetchLastIntegratedApplicantApplicationTransmission( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLastIntegratedApplicantApplicationTransmissionByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationTransmissions( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationSubmission( $objDatabase ) {
		return CApplicantApplicationSubmissions::fetchApplicantApplicationSubmissionsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchGroupLeaseGeneratedApplicantIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantIdsByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicant( $objDatabase, $boolLoadFromView = true ) {
		return CApplicants::fetchSimpleApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $objDatabase, $boolLoadFromView );
	}

	public function fetchApplication( $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	public function fetchPreferredLocaleCode( $objDatabase ) {
		return CApplicants::fetchApplicantPrefferedLocaleCodeByApplicantIdByApplicationIdByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileSignatureByFileAssociationIdBySystemCode( $intFileAssociationId, $strSystemCode, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignatureByFileAssociationIdByApplicantApplicationIdBySystemCodeByCid( $intFileAssociationId, $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client', __( 'cid is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApplicantId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', __( 'Applicant id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApplicationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', __( 'Application id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_type_id', __( 'Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerRelationshipId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Relationship is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseSignature() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getLeaseSignature() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_signature', __( 'Digital Signautre is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valApplicationId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'lease_renewal':
				$boolIsValid &= $this->valLeaseSignature();
				break;

			case 'lease_modification':
				$boolIsValid &= $this->valCustomerTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId();
				break;

			default:
				// Default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function downloadApplicantApplicationDocument( $objClientDatabase ) {

		$objFile = \Psi\Eos\Entrata\CFiles::createService()->fetchFileByApplicationIdByApplicantIdByFileTypeSystemCodeByFileExtensionIdByCid( $this->getApplicationId(), $this->getApplicantId(), CFileType::SYSTEM_CODE_APPLICATION, CFileExtension::APPLICATION_PDF, $this->getCid(), $objClientDatabase );

		if( true == valObj( $objFile, 'CFile' ) ) {

			$objFile->downloadFileObject( $this->getObjectStorageGateway(), $objClientDatabase );

		}
	}

	public function parseAndSaveZendPdfFile( $strPdfContent, $strFilePath ) {
		try {
			$objPdf = Zend_Pdf::parse( $strPdfContent );
		} catch( Zend_Pdf_Exception $objException ) {
			trigger_error( 'Unable to parse document html content to create pdf.', E_USER_ERROR );
			return false;
		}

		try {
			$objPdf->save( $strFilePath );
		} catch( Zend_Pdf_Exception $objException ) {
			trigger_error( 'Unable to generate document.', E_USER_ERROR );
			return false;
		}
	}

	public function assignSmartyData( $objSmarty, $strSecureBaseName = NULL, $arrobjArTransactions = NULL, $arrobjRecurringRates = NULL ) {

		$objSmarty->assign( 'property_application_preferences', 		rekeyObjects( 'Key', $this->m_arrobjPropertyApplicationPreferences ) );
		CCustomerType::assignSmartyConstants( $objSmarty );

		$objSmarty->assign_by_ref( 'income_types',							$this->m_arrobjIncomeTypes );
		$objSmarty->assign_by_ref( 'customer_incomes',						$this->m_arrobjCustomerIncomes );
		$objSmarty->assign_by_ref( 'customer_asset_types',					$this->m_arrobjCustomerAssetTypes );
		$objSmarty->assign_by_ref( 'customer_assets',						$this->m_arrobjCustomerAssets );
		$objSmarty->assign_by_ref( 'income_type_custom_fields',				$this->m_arrstrIncomeTypeCustomFields );
		$objSmarty->assign_by_ref( 'customer_asset_type_custom_fields',		$this->m_arrstrCustomerAssetTypeCustomFields );

		$objSmarty->assign_by_ref( 'company_residence_types',				$this->m_arrobjCompanyResidenceTypes );
		$objSmarty->assign_by_ref( 'applicants',							$this->m_arrobjApplicants );
		$objSmarty->assign_by_ref( 'customer_pets',							$this->m_arrobjCustomerPets );
		$objSmarty->assign_by_ref( 'pet_rate_associations',					$this->m_arrobjPetRateAssociations );
		$objSmarty->assign_by_ref( 'ar_triggers',							$this->m_arrobjArTriggers );
		$objSmarty->assign_by_ref( 'ar_codes',								$this->m_arrobjArCodes );
		$objSmarty->assign_by_ref( 'customized_ar_triggers',				$this->m_arrstrCustomizedArTriggers );
		$objSmarty->assign_by_ref( 'community_amenities',					$this->m_arrobjCommunityAmenities );
		$objSmarty->assign_by_ref( 'apartment_amenities',						$this->m_arrobjApartmentAmenities );
		$objSmarty->assign_by_ref( 'amenity_lease_associations',			$this->m_arrobjAmenityLeaseAssociations );
		$objSmarty->assign( 'amenity_lease_associations_count',				\Psi\Libraries\UtilFunctions\count( $this->m_arrobjAmenityLeaseAssociations ) );

		$objSmarty->assign_by_ref( 'applicant_other_vehicles',				$this->m_arrobjApplicantOtherVehicles );
		$objSmarty->assign_by_ref( 'applicant_emergency_contact',			$this->m_objApplicantEmergencyContact );
		$objSmarty->assign_by_ref( 'applicant_personal_reference_1',		$this->m_objApplicantPersonalReference1 );
		$objSmarty->assign_by_ref( 'applicant_personal_reference_2',		$this->m_objApplicantPersonalReference2 );
		$objSmarty->assign_by_ref( 'roommate_interest_categories',			$this->m_arrobjRoommateInterestCategories );
		$objSmarty->assign_by_ref( 'roommate_interests',					$this->m_arrobjRoommateInterests );

		$objSmarty->assign_by_ref( 'states',								$this->m_arrobjStates );
		$objSmarty->assign_by_ref( 'marital_status_types',					$this->m_arrobjMaritalStatusTypes );
		$objSmarty->assign( 'customer_type_id',								$this->getCustomerTypeId() );
		$objSmarty->assign_by_ref( 'applicant_application',					$this );

		$objSmarty->assign_by_ref( 'customer_types',						$this->m_arrstrCustomerTypes );

		// Load All Application Data End

		$objSmarty->assign( 'application_document', 			$this->m_objApplicationDocument );
		$objSmarty->assign( 'lead_sources', 					$this->m_arrobjLeadSources );
		$objSmarty->assign( 'property', 						$this->m_objProperty );
		$objSmarty->assign( 'applicant', 						$this->m_objApplicant );
		$objSmarty->assign( 'company_application', 				$this->m_objCompanyApplication );
		$objSmarty->assign( 'lease', 							$this->m_objLease );
		$objSmarty->assign( 'lease_customer',					$this->m_objLeaseCustomer );
		$objSmarty->assign( 'property_unit', 					$this->m_objPropertyUnit );
		$objSmarty->assign( 'property_floorplan', 				$this->m_objPropertyFloorplan );
		$objSmarty->assign( 'form', 							$this->m_objForm );
		$objSmarty->assign( 'ar_payments', 						$this->m_arrobjArPayments );
		$objSmarty->assign( 'application',						$this->m_objApplication );
		$objSmarty->assign( 'payment_types',   					$this->m_arrobjPaymentTypes );
		$objSmarty->assign( 'merchant_accounts', 				$this->m_arrobjMerchantAccounts );
		$objSmarty->assign( 'ar_transactions', 					$arrobjArTransactions );
		$objSmarty->assign( 'recurring_rates', 					$arrobjRecurringRates );

		$objSmarty->assign( 'marketing_media_association', 		$this->m_objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file', 		$this->m_objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'media_library_uri', 				CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'property_buildings', 				$this->m_arrobjPropertyBuildings );
		$objSmarty->assign( 'base_uri',			  				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );

		if( true == valStr( $strSecureBaseName ) ) {
			$objSmarty->assign( 'terms_and_condition_path', 	$strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->m_objApplication->getId() . '&company_application_id=' . $this->m_objApplication->getCompanyApplicationId() );
		}

		$objSmarty->assign( 'desire_lease_term', 				$this->calculateTermMonth( $this->m_objApplication->getLeaseStartDate(), $this->m_objApplication->getTermMonth() ) );

		// OTR genration

		$objSmarty->assign( 'application', 						$this->m_objApplication );
		$objSmarty->assign( 'property', 						$this->m_objProperty );

		$objSmarty->assign( 'rent_rate', 						$this->m_objRentRate );
		$objSmarty->assign( 'property_preferences', 			$this->m_arrobjPropertyPreferences );
		$objSmarty->assign( 'co_applicants', 					$this->m_arrobjApplicants );
		$objSmarty->assign( 'guarantors', 						$this->m_arrobjGuarantors );

		$objSmarty->assign( 'company_specials', 				getArrayElementByKey( 'specials', 			$this->m_arrmixOfferToRent ) );
		$objSmarty->assign( 'property_sales_tax', 				getArrayElementByKey( 'sales_tax', 			$this->m_arrmixOfferToRent ) );
		$objSmarty->assign( 'move_in_charges', 					getArrayElementByKey( 'move_in', 			$this->m_arrmixOfferToRent ) );
		$objSmarty->assign( 'recurring_charges', 				getArrayElementByKey( 'recurring', 			$this->m_arrmixOfferToRent ) );
		$objSmarty->assign( 'application_charges', 				getArrayElementByKey( 'application', 		$this->m_arrmixOfferToRent ) );
		$objSmarty->assign( 'payment_schedule', 				getArrayElementByKey( 'payment_schedule', 	$this->m_arrmixOfferToRent ) );

		$objSmarty->assign( 'current_company_move_out_reason',		$this->m_objApplicantCurrentCompanyMoveOutReason );
		$objSmarty->assign( 'previous_company_move_out_reason',		$this->m_objApplicantPreviousCompanyMoveOutReason );

		$objSmarty->assign( 'company_identification_types',	$this->m_arrobjCompanyIdentificationTypes );
	}

	public function loadCommonData( $objClientDatabase ) {

		$this->m_objClient						= $this->getOrFetchClient( $objClientDatabase );
		$this->m_objApplicant					= $this->getOrFetchApplicant( $objClientDatabase );
		$this->m_objApplication					= $this->getOrFetchApplication( $objClientDatabase );
		$this->m_arrobjArPayments				= $this->fetchArPayments( $objClientDatabase );
		$this->m_objLease						= $this->m_objApplication->getOrFetchLease( $objClientDatabase );
		$this->m_objProperty					= $this->m_objApplication->getOrFetchProperty( $objClientDatabase );
		$this->m_arrobjMaritalStatusTypes		= CMaritalStatusTypes::fetchAllMaritalStatusTypes( $objClientDatabase );

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			$this->m_objLeaseCustomer = $this->m_objLease->fetchLeaseCustomerByCustomerId( $this->m_objApplicant->getCustomerId(), $objClientDatabase );
		}

		$this->m_objPropertyFloorplan				= $this->m_objProperty->fetchPropertyFloorplanById( $this->m_objApplication->getPropertyFloorplanId(), $objClientDatabase );
		$this->m_objPropertyUnit					= $this->m_objApplication->fetchPropertyUnit( $objClientDatabase );
		$this->m_arrobjPropertyBuildings			= $this->m_objProperty->fetchPropertyBuildings( $objClientDatabase );
		$this->m_objMarketingMediaAssociation		= $this->m_objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
		$this->m_arrobjMerchantAccounts				= $this->m_objClient->fetchMerchantAccounts( $objClientDatabase );
		$this->m_arrobjLeadSources					= $this->m_objClient->fetchLeadSources( $objClientDatabase );
		$this->m_objLogoMarketingMediaAssociation	= $this->m_objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		$this->m_arrobjPaymentTypes 				= CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );

		if( true == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) && 1 < $this->m_objPropertyUnit->getUnitCounts() ) {
			$this->m_objUnitSpace = $this->m_objApplication->fetchUnitSpace( $objClientDatabase );

			if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
				$this->m_objPropertyUnit->setUnitNumber( $this->m_objPropertyUnit->getUnitNumber() . '-' . $this->m_objUnitSpace->getSpaceNumber() );
			}

			unset( $this->m_objUnitSpace );
		}

		$this->m_arrstrCustomerTypes = CCustomerType::createService()->getCustomerTypeIdToStrArray();

		if( false == is_null( $this->m_objApplicant->getCurrentMoveOutReasonListItemId() ) && 0 < $this->m_objApplicant->getCurrentMoveOutReasonListItemId() ) {
			$this->m_objApplicantCurrentCompanyMoveOutReason = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemByIdByCid( $this->m_objApplicant->getCurrentMoveOutReasonListItemId(), $this->m_objClient->getId(), $objClientDatabase );
		}

		if( false == is_null( $this->m_objApplicant->getPreviousMoveOutReasonListItemId() ) && 0 < $this->m_objApplicant->getPreviousMoveOutReasonListItemId() ) {
			$this->m_objApplicantPreviousCompanyMoveOutReason = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemByIdByCid( $this->m_objApplicant->getPreviousMoveOutReasonListItemId(), $this->m_objClient->getId(), $objClientDatabase );
		}

		$this->m_arrobjCompanyIdentificationTypes	= $this->m_objProperty->fetchAssociatedCompanyIdentificationTypes( $objClientDatabase );

		// Commented policy documents code as it's needs to be re-coded.
		// Load Policy Documents
		$this->m_arrobjApplicantApplicationFileAssociations 	= $this->fetchFileAssociationsByFileTypeSystemCodeByIsAttacheToEmail( CFileType::SYSTEM_CODE_POLICY, $boolIsAttacheToEmail = true, $objClientDatabase );

		if( false == is_null( $this->getApplicationFormId() ) ) {
			$this->m_objForm = $this->fetchForm( $objClientDatabase );
		}

		if( true == valObj( $this->m_objForm, 'CForm' ) ) {
			$this->m_objApplicationDocument = $this->m_objForm->fetchApplicationDocument( $objClientDatabase );
			$arrobjFormAnswers				= $this->m_objForm->fetchFormAnswers( $objClientDatabase );
		}

		if( true == valObj( $this->m_objApplicationDocument, 'CDocument' ) ) {
			$this->m_objApplicationDocument->setFormAnswers( $arrobjFormAnswers );
			$this->m_objApplicationDocument->setIsPrint( true );
			$this->m_objApplicationDocument->setShowSecureData( false );
			$this->m_objApplicationDocument->loadGridEngineDisplayContent( $objClientDatabase );
			$this->m_objApplicationDocument->setContent( preg_replace( '/nowrap>/', '>', $this->m_objApplicationDocument->getContent() ) );
		}

		// Load All Application Data

		$this->m_arrobjApplicantOtherVehicles   = [];
		$this->m_arrobjCustomerPets 			= [];
		$this->m_arrobjPetRateAssociations 		= [];
		$this->m_arrobjCommunityAmenities	   	= [];
		$this->m_arrobjApartmentAmenities			= [];
		$this->m_arrobjAmenityLeaseAssociations	 	= [];
		$this->m_arrobjCompanyResidenceTypes	= [];

		$this->m_arrobjRoommateInterests			= [];
		$this->m_arrobjRoommateInterestCategories 	= [];

		$this->m_arrobjPropertyApplicationPreferences 	= $this->m_objApplication->fetchPropertyApplicationPreferences( $objClientDatabase );
		$this->m_arrobjApplicantOtherVehicles = CLeaseCustomerVehicles::fetchLeaseCustomerVehiclesByLeaseIdByCustomerIdByCid( $this->m_objApplication->getLeaseId(), $this->m_objApplicant->getCustomerId(), $this->m_objClient->getId(), $objClientDatabase );

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ROOMMATE', $this->m_arrobjPropertyApplicationPreferences ) && $this->m_objProperty->getPropertyTypeId() == CPropertyType::STUDENT ) {
			$this->m_arrobjRoommateInterestCategories 	= ( array ) CRoommateInterestCategories::fetchPublishedRoommateInterestCategories( $objClientDatabase );
			$this->m_arrobjRoommateInterestsOptions 	= ( array ) CRoommateInterestOptions::fetchRoommateInterestOptionsWithOrderByCid( $this->m_objClient->getId(), $objClientDatabase );
			$arrmixApplicantRoommateInterests			= rekeyArray( 'roommate_interest_option_id', ( array ) CApplicantRoommateInterests::fetchApplicantRoommateInterestsBySelectParametersByApplicantIdByLeaseIdByCid( 'ari.roommate_interest_id, ari.roommate_interest_option_id', $this->m_objApplicant->getId(), $this->m_objApplication->getLeaseId(), $this->m_objClient->getId(), $objClientDatabase ) );
			$this->m_arrobjRoommateInterests			= ( array ) CRoommateInterests::fetchRoommateInterestsByApplicantIdByLeaseIdByCid( $this->m_objApplicant->getId(), $this->m_objApplication->getLeaseId(), $this->m_objClient->getId(), $objClientDatabase );

			foreach( $this->m_arrobjRoommateInterests as $objPropertyRoommateInterest ) {
				$arrobjTempRoommateInterestOptions = [];
				if( true == valArr( $this->m_arrobjRoommateInterestsOptions ) ) {
					foreach( $this->m_arrobjRoommateInterestsOptions as $objRoommateInterestsOption ) {
						if( true == array_key_exists( $objRoommateInterestsOption->getId(), $arrmixApplicantRoommateInterests ) ) {
							$objRoommateInterestsOption->setIsSelected( 1 );
						}
						if( $objRoommateInterestsOption->getRoommateInterestId() == $objPropertyRoommateInterest->getId() ) {
							$arrobjTempRoommateInterestOptions[] = $objRoommateInterestsOption;
						}
					}
					$objPropertyRoommateInterest->setInterestOptions( $arrobjTempRoommateInterestOptions );
				}
			}
		}

		$this->m_arrobjIncomeTypes						= NULL;
		$this->m_arrobjCustomerIncomes					= NULL;
		$this->m_arrobjCustomerAssetTypes				= NULL;
		$this->m_arrobjCustomerAssets					= NULL;
		$this->m_arrstrIncomeTypeCustomFields 			= [];
		$this->m_arrstrCustomerAssetTypeCustomFields 	= [];

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_FINANCIAL', $this->m_arrobjPropertyApplicationPreferences ) ) {

			$this->m_arrobjIncomeTypes			= CIncomeTypes::fetchAllIncomeTypes( $objClientDatabase );
			$this->m_arrobjCustomerAssetTypes	= CCustomerAssetTypes::fetchAllCustomerAssetTypes( $objClientDatabase );

			$this->m_arrobjCustomerIncomes = $this->m_objApplicant->fetchCustomerIncomes( $objClientDatabase );

			if( true == valArr( $this->m_arrobjCustomerIncomes ) ) {
				$this->m_arrobjCustomerIncomes = rekeyObjects( 'IncomeTypeId', $this->m_arrobjCustomerIncomes );
				asort( $this->m_arrobjCustomerIncomes );
			}

			$this->m_arrobjCustomerAssets = $this->m_objApplicant->fetchCustomerAssets( $objClientDatabase );

			if( true == valArr( $this->m_arrobjCustomerAssets ) ) {
				$this->m_arrobjCustomerAssets 	= rekeyObjects( 'CustomerAssetTypeId', $this->m_arrobjCustomerAssets );
				asort( $this->m_arrobjCustomerAssets );
			}

			// Custom field setting for income and asset types
			$this->m_arrstrIncomeTypeCustomFields			= CIncomeType::createService()->getIncomeTypeCustomFields();
			$this->m_arrstrCustomerAssetTypeCustomFields	= CCustomerAssetType::createService()->getCustomerAssetTypeCustomFields();
		}

		$this->m_arrobjApplicants = [];

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_PEOPLE', $this->m_arrobjPropertyApplicationPreferences ) && CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {

			$this->m_arrobjApplicants = $this->m_objApplication->fetchApplicantsByCustomerTypeIds( [ CCustomerType::NOT_RESPONSIBLE, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT, CCustomerType::GUARANTOR ], $objClientDatabase );
		}

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_OPTION_PET', $this->m_arrobjPropertyApplicationPreferences ) && CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
			// PETS DATA
			$this->m_arrobjCustomerPets = \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCustomerIdWithPetTypeName( $this->m_objApplication->getLeaseId(), $this->m_objApplication->getLeaseIntervalId(), $this->m_objApplicant->getCustomerId(), $this->m_objClient->getId(), $objClientDatabase );

			if( true == valArr( $this->m_arrobjCustomerPets ) ) {
				$arrintPetTypeIds = [];

				$arrobjCustomerPetsTemp 		= rekeyObjects( 'PetTypeId', $this->m_arrobjCustomerPets );
				$arrintPetTypeIds 				= array_keys( $arrobjCustomerPetsTemp );

				$this->m_arrobjPetRateAssociations 			= $this->m_objProperty->fetchPetRateAssociationsByPetTypeIds( $arrintPetTypeIds, $objClientDatabase );

				if( true == valArr( $this->m_arrobjPetRateAssociations ) ) {
					$this->m_arrobjPetRateAssociations = rekeyObjects( 'ArOriginReferenceId', $this->m_arrobjPetRateAssociations );

					foreach( $this->m_arrobjPetRateAssociations as $objPetRateAssociation ) {

						$arrobjPetRateAssociationsRates = $objPetRateAssociation->fetchPetRates( $objClientDatabase );

						if( true == valArr( $arrobjPetRateAssociationsRates ) ) {
							foreach( $arrobjPetRateAssociationsRates as $objPetRateAssociationRate ) {
								$this->m_arrobjPetRateAssociations[$objPetRateAssociation->getArOriginReferenceId()]->addPetRate( $objPetRateAssociationRate );
							}
						}
					}
				}
			}

			// Amenities data
			$this->m_arrobjAmenityLeaseAssociations	= $this->m_objApplication->fetchAmenityLeaseAssociations( $objClientDatabase );
			$this->m_arrobjCompanyResidenceTypes	= $this->m_objClient->fetchCompanyResidenceTypes( $objClientDatabase );

			if( true == valArr( $this->m_arrobjAmenityLeaseAssociations ) ) {
				$arrintApartmentAmenitiesIds 	= [];
				$arrintCommunityAmenitiesIds = [];

				foreach( $this->m_arrobjAmenityLeaseAssociations as $objAmenityLeaseAssociation ) {

					if( CArCascade::SPACE == $objAmenityLeaseAssociation->getArCascadeId() ) {
						$arrintApartmentAmenitiesIds[] = $objAmenityLeaseAssociation->getArOriginReferenceId();
					} elseif( CArCascade::PROPERTY == $objAmenityLeaseAssociation->getArCascadeId() ) {
						$arrintCommunityAmenitiesIds[] = $objAmenityLeaseAssociation->getArOriginReferenceId();
					}
				}

				if( true == valArr( $arrintApartmentAmenitiesIds ) ) {
					$this->m_arrobjApartmentAmenities	= $this->m_objProperty->fetchAmenityRateAssociationsByRateAssociationIds( $arrintApartmentAmenitiesIds, $objClientDatabase );
					// Add rates to Unit Amenities.

					if( true == valArr( $this->m_arrobjApartmentAmenities ) ) {
						foreach( $this->m_arrobjApartmentAmenities as $objApartmentAmenity ) {
							$arrobjApartmentAmenityRates 			= $objApartmentAmenity->fetchRates( $objClientDatabase );

							if( true == valArr( $arrobjApartmentAmenityRates ) ) {
								foreach( $arrobjApartmentAmenityRates as $objApartmentAmenityRate ) {
									$this->m_arrobjApartmentAmenities[$objApartmentAmenity->getId()]->addRate( $objApartmentAmenityRate );
								}

							} else {
								unset( $this->m_arrobjApartmentAmenities[$objApartmentAmenity->getId()] );
							}
						}
					}
				}

				if( true == valArr( $arrintCommunityAmenitiesIds ) ) {
					$this->m_arrobjCommunityAmenities	= $this->m_objProperty->fetchAmenityRateAssociationsByRateAssociationIds( $arrintCommunityAmenitiesIds, $objClientDatabase );
					// Add rates to Unit Amenities.
					if( true == valArr( $this->m_arrobjCommunityAmenities ) ) {
						foreach( $this->m_arrobjCommunityAmenities as $objCommunityAmenity ) {
							$arrobjCommunityAmenityRates = $objCommunityAmenity->fetchRates( $objClientDatabase );
							if( true == valArr( $arrobjCommunityAmenityRates ) ) {
								foreach( $arrobjCommunityAmenityRates as $objCommunityAmenityRate ) {
									$this->m_arrobjCommunityAmenities[$objCommunityAmenity->getId()]->addRate( $objCommunityAmenityRate );
								}

							} else {
								unset( $this->m_arrobjCommunityAmenities[$objCommunityAmenity->getId()] );
							}
						}
					}
				}
			}

			if( true == valArr( $this->m_arrobjApartmentAmenities ) ) {
				$this->m_arrobjApartmentAmenities 				= rekeyObjects( 'ArOriginReferenceId', $this->m_arrobjApartmentAmenities );
			}

			if( true == valArr( $this->m_arrobjCommunityAmenities ) ) {
				$this->m_arrobjCommunityAmenities			= rekeyObjects( 'ArOriginReferenceId', $this->m_arrobjCommunityAmenities );
			}
		}

		$this->m_arrobjArTriggers			= $this->m_objClient->fetchStandardArTriggers( $objClientDatabase );
		$this->m_arrobjArCodes 	= $this->m_objClient->fetchArCodes( $objClientDatabase );

		$this->m_arrstrCustomizedArTriggers = ( array ) CArTrigger::loadCustomizedArTriggers( $objClientDatabase );

		$this->m_objApplicantEmergencyContact 	= NULL;
		$this->m_objApplicantPersonalReference1 = NULL;
		$this->m_objApplicantPersonalReference2 = NULL;

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_CONTACTS', $this->m_arrobjPropertyApplicationPreferences ) ) {

			$this->m_objApplicantEmergencyContact	 	= $this->m_objApplicant->fetchContactByContactTypeId( CCustomerContactType::EMERGENCY_CONTACT, 		$objClientDatabase );
			$this->m_objApplicantPersonalReference1		= $this->m_objApplicant->fetchContactByContactTypeId( CCustomerContactType::PERSONAL_REFERENCE, 		$objClientDatabase );
			$this->m_objApplicantPersonalReference2 	= $this->m_objApplicant->fetchContactByContactTypeId( CCustomerContactType::TYPE_2ND_PERSONAL_REFERENCE, 	$objClientDatabase );
		}

		if( true == valObj( $this->m_objApplication, 'CApplication' ) && true == is_numeric( $this->m_objApplication->getCompanyApplicationId() ) ) {
			$this->m_objCompanyApplication = $this->m_objApplication->fetchCompanyApplication( $objClientDatabase );
		}

		$this->m_arrobjStates = \Psi\Eos\Entrata\CStates::createService()->fetchAllStates( $objClientDatabase );

		if( true == is_null( $this->m_arrobjPropertyApplicationPreferences ) ) {
			$this->m_arrobjPropertyApplicationPreferences = [];
		}
	}

	public function buildFileAssociationFilePath() {
		return 'leases/' . $this->getApplicationId() . '/';
	}

	public function buildFilePath() {
		return date( 'Y/m/d', strtotime( $this->m_objApplication->getApplicationDatetime() ) ) . '/' . $this->m_objApplication->getLeaseIntervalId() . '/';
	}

	public function buildFileName( $strExtension = 'docx', $objDocument = NULL ) {

		if( true == valObj( $objDocument, 'CDocument' ) && CDocumentSubType::DOCUMENT_TEMPLATE == $objDocument->getDocumentSubTypeId() ) {
			$strFileNamePrefix = 'addendum_template_' . $objDocument->getId() . '_';
		} else {
			$strFileNamePrefix = 'addendum_document_';
		}

		return $strFileNamePrefix . $this->getId() . '_' . date( 'Ymdhis' ) . '_' . mt_rand() . '.' . $strExtension;
	}

	public function buildFileAssociationFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildFileAssociationFilePath();
	}

	public function buildFileAssociationFileName( $strExtension = 'docx', $objDocument = NULL ) {
		// OLD Format:
		// return 'lease_document_' . $this->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		if( true == valObj( $objDocument, 'CDocument' ) && CDocumentSubType::DOCUMENT_TEMPLATE == $objDocument->getDocumentSubTypeId() ) {
			$strFileNamePrefix = 'lease_template_' . $objDocument->getId() . '_';
		} else {
			$strFileNamePrefix = 'lease_document_';
		}

		return $strFileNamePrefix . $this->getId() . '_' . time() . '.' . $strExtension;
	}

	public function generateExpirationDate( $arrobjPropertyPreferences, $objDatabase = NULL, $boolIsFromResidentPortal4_0 = false )
    {
    	if (true == valObj($objDatabase, 'CDatabase')) {
            $this->getOrFetchApplication($objDatabase);
        }

        $strExpirationDate = NULL;

        if (false == valObj($this->m_objApplication, 'CApplication')) return;

        if ( CLeaseIntervalType::RENEWAL != $this->m_objApplication->getLeaseIntervalTypeId() ) {
            if (true == isset($arrobjPropertyPreferences['LEASE_OFFER_VALID_DAYS'])) {
                $strExpirationDate = date('m/d/Y', strtotime($this->getLeaseGeneratedOn() . ' + ' . ( int )$arrobjPropertyPreferences['LEASE_OFFER_VALID_DAYS']->getValue() . ' day'));
            }
            $this->setExpirationDate($strExpirationDate);
            return;
        }

        if( true == $boolIsFromResidentPortal4_0 ) {
            $renewalLeaseOfferValidDays = $arrobjPropertyPreferences['LEASE_OFFER_VALID_DAYS'];
        } else {
        	$renewalLeaseOfferValidDays = $arrobjPropertyPreferences['RENEWAL_LEASE_OFFER_VALID_DAYS'];
		}

		if ( false == isset( $renewalLeaseOfferValidDays ) ) return;

		$strExpirationDate = date('m/d/Y', strtotime($this->getLeaseGeneratedOn() . ' + ' . ( int )$renewalLeaseOfferValidDays->getValue() . ' day'));

		if ('END_OF_OFFER' == $renewalLeaseOfferValidDays->getValue()) {

			$objQuote = CQuotes::fetchAcceptedQuotesByApplicationIdByCid($this->m_objApplication->getId(), $this->getCid(), $objDatabase);

			if (true == valObj($objQuote, 'CQuote')) {
				$strExpirationDate = $objQuote->getExpiresOn();
			}

		}
        $this->setExpirationDate( $strExpirationDate );


    }

	public function deleteLeaseDocumentFiles( $arrobjDocumentAssociations ) {
		if( true == valArr( $arrobjDocumentAssociations ) ) {
			foreach( $arrobjDocumentAssociations as $objDocumentAssociation ) {
				$objDocumentAssociation->deleteFile();
			}
		}

		return true;
	}

	public function generateApplicationQuotesDocument( $intCurrentUserId, $objClientDatabase ) {

		$objApplicationSystemLibrary = new CApplicationSystemLibrary();
		$boolIsSuccess = $objApplicationSystemLibrary->generateApplicationQuotesDocument( $this->getApplicationId(), $this->getApplicantId(), $this->getId(), $this->getCid(), $intCurrentUserId, $objClientDatabase );

		return $boolIsSuccess;
	}

	/**
	 *  Insert & Update & Delete Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolRebuildFileAssociations = true ) {
		if( false == valId( $this->getId() ) || false == valStr( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolRebuildFileAssociations );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolRebuildFileAssociations );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsRebuildAssociation = true ) {

		if( true == is_null( $this->getCustomerRelationshipId() ) ) {
			$this->setCustomerRelationshipId( $this->getDefaultCustomerRelationshipId( $objDatabase ) );
		} elseif( true == valObj( $this->m_objApplication, 'CApplication' ) ) {
			$arrmixData = fetchData( 'SELECT id FROM customer_relationships WHERE cid = ' . ( int ) $this->getCid() . ' AND id = ' . ( int ) $this->getCustomerRelationshipId() . ' AND customer_type_id = ' . ( int ) $this->getCustomerTypeId(), $objDatabase );
			if( false == valArr( $arrmixData ) ) {
				$arrmixDefaultCustomerRelationship = CCustomerRelationships::fetchDefaultCustomerRelationshipsByOccupancyTypIdsByPropertyIdsByCustomerTypeIdByCid( $this->m_objApplication->getOccupancyTypeId(), [ $this->m_objApplication->getPropertyId() ], $this->getCustomerTypeId(), $this->getCid(), $objDatabase );
				if( true == valId( $arrmixDefaultCustomerRelationship[$this->m_objApplication->getPropertyId()] ) ) {
					$this->setCustomerRelationshipId( $arrmixDefaultCustomerRelationship[$this->m_objApplication->getPropertyId()] );
				}
			}
		}
		if( true == is_null( $this->getLeaseStatusTypeId() ) ) {
			$intLeaseStatusTypeId = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseStatusTypeIdByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
			if( true == valId( $intLeaseStatusTypeId ) ) {
				$this->setLeaseStatusTypeId( $intLeaseStatusTypeId );
			}
		}

		if( false == $boolReturnSqlOnly ) {

			$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase );

			if( false == $boolIsValid ) return false;

			if( true == $this->getIsSyncApplicationAndLease() && true == is_null( $this->m_intLeaseCustomerId ) ) {
				if( false == valObj( $this->m_objApplication, 'CApplication' ) ) {
					$this->m_objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
				}

				if( true == valObj( $this->m_objApplication, 'CApplication' ) && false == $this->m_objApplication->isGuestCard() ) {
					$this->m_objLease 	= \Psi\Eos\Entrata\CLeases::createService()->fetchScheduleLeaseByIdByCid( $this->m_objApplication->getLeaseId(), $this->getCid(), $objDatabase, $boolExcludeCancelledInterval = false );
					$boolIsValid 		&= CApplicationUtils::insertCustomer( $this, $intCurrentUserId, $objDatabase );

					if( true == $boolIsValid ) {
						$objApplicant	= $this->fetchApplicant( $objDatabase );
						$objLeaseCustomer = $this->m_objLease->getOrFetchLeaseCustomer( $objApplicant->getCustomerId(), $objDatabase );
						if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && true == valId( $intLeaseStatusTypeId ) ) {
							$objLeaseCustomer->setLeaseStatusTypeId( $intLeaseStatusTypeId );
							if( CLeaseStatusType::APPLICANT == $intLeaseStatusTypeId && CLeaseIntervalType::APPLICATION == $this->m_objApplication->getLeaseIntervalTypeId() ) {
								$objLeaseCustomer->setIsUngrouped( false );
							}
							$boolIsValid &= $objLeaseCustomer->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsIntegrated = false, $boolIsNotPrimaryLeaseCustomer = false, $boolDontUpdateUnitStatus = false, $boolIsAllowUpdateLeaseStatus = true );
						} else {
							$boolIsValid &= CApplicationUtils::insertLeaseCustomer( $this, $this->m_objLease, $intCurrentUserId, $objDatabase );
						}
					}
				}
			}

			if( false == $boolIsValid ) {
				return $boolIsValid;
			}

			// Enqueue associate calls to lead.
			$objApplicationDataObject = new CApplicationDataObject();
			$objApplicationDataObject->m_intApplicationId 	= $this->getApplicationId();
			$objApplicationDataObject->m_intPropertyId 		= $this->getPropertyId();
			$objApplicationDataObject->m_intCid 			= $this->getCid();
			$objApplicationDataObject->m_intCompanyUserId 	= $intCurrentUserId;
			$objApplicationDataObject->m_objDatabase 		= $objDatabase;

			$objApplicationMessageQueueSenderLibrary = new CApplicationMessageQueueSenderLibrary( $objApplicationDataObject );
			$objApplicationMessageQueueSenderLibrary->enqueueLeadsAssociateCalls();

			return $boolIsValid;
		} else {

			$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsRebuildAssociation = true, $boolIncludeDeletedAA = false ) {

		$this->m_intOldCustomerTypeId 	= NULL;
		$intCustomerRelationshipId		= 0;
		$strSql							= '';
		$boolIsValid					= true;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND deleted_on IS NULL' : '';

		$arrintResponse = ( array ) fetchData( 'SELECT customer_type_id, customer_relationship_id, lease_status_type_id FROM applicant_applications WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . $strCheckDeletedAASql, $objDatabase );
		if( true == isset( $arrintResponse[0]['customer_type_id'] ) ) $this->m_intOldCustomerTypeId = $arrintResponse[0]['customer_type_id'];
		if( true == isset( $arrintResponse[0]['customer_relationship_id'] ) ) $intCustomerRelationshipId = $arrintResponse[0]['customer_relationship_id'];
		$objApplication = $this->getApplication();

		if( ( $this->m_intOldCustomerTypeId != $this->getCustomerTypeId() || $intCustomerRelationshipId != $this->getCustomerRelationshipId() ) && true == valObj( $objApplication, 'CApplication' ) ) {
			$arrmixData = fetchData( 'SELECT id FROM customer_relationships WHERE cid = ' . ( int ) $this->getCid() . ' AND id = ' . ( int ) $this->getCustomerRelationshipId() . ' AND customer_type_id = ' . ( int ) $this->getCustomerTypeId(), $objDatabase );
			if( false == valArr( $arrmixData ) ) {
				$arrmixDefaultCustomerRelationship = CCustomerRelationships::fetchDefaultCustomerRelationshipsByOccupancyTypIdsByPropertyIdsByCustomerTypeIdByCid( [ $objApplication->getOccupancyTypeId() ], [ $objApplication->getPropertyId() ], $this->getCustomerTypeId(), $this->getCid(), $objDatabase );
				if( true == valId( $arrmixDefaultCustomerRelationship[$objApplication->getPropertyId()] ) ) {
					$this->setCustomerRelationshipId( $arrmixDefaultCustomerRelationship[$objApplication->getPropertyId()] );
				}
			}
		}

		if( true == is_null( $arrintResponse[0]['lease_status_type_id'] ) ) {
			$intLeaseStatusTypeId = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseStatusTypeIdByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
			if( true == valId( $intLeaseStatusTypeId ) ) {
				$this->setLeaseStatusTypeId( $intLeaseStatusTypeId );
			}
		}

		$arrstrResponse = ( array ) fetchData( 'SELECT * FROM applicant_applications WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
		$this->setSerializedOriginalValues( serialize( $arrstrResponse[0] ) );

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( true == $boolReturnSqlOnly ) {
			$strSql = '';

			$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly );
			$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		} else {
			$boolIsValid = true;

			$strReturnValue = $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase );
			$strReturnValue &= parent::update( $intCurrentUserId, $objDatabase );

			return $strReturnValue;
		}
	}

	public static function rebuildFileAssociationsForAllApplicants( $intApplicationId, $intCid, $intCurrentUserId, $objDatabase ) {
		$arrobjApplicantApplications = ( array ) \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false );
		$strSql = '';

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( CCustomerType::NON_LEASING_OCCUPANT == $objApplicantApplication->getCustomerTypeId() ) continue;
			$strSql .= $objApplicantApplication->rebuildFileAssociations( $intCurrentUserId, $objDatabase, true );
		}

		if( '' != $strSql ) {
			return fetchData( $strSql, $objDatabase );
		}

		return true;
	}

	public function rebuildFileAssociations( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplication = $this->getOrFetchApplication( $objDatabase );
		if( false == valObj( $objApplication, 'CApplication' ) ) {
			return;
		}
		$arrmixApplicantApplicationsWithMissingFileAssociations = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationWithMissingPolicyDocumentsByApplicationIdByCid( $objApplication->getId(), $objApplication->getCid(), $objDatabase );
		$boolIsRebuildFileAssociationsInClone = ( true == valObj( $objApplication, 'CApplication' ) && true == $objApplication->getFromCloneApplication() ) ? true : false;

		$strSql = '';
	if( false == $boolIsRebuildFileAssociationsInClone ) {
		if( false == is_null( $this->getLeaseSignedOn() ) && false == valArr( $arrmixApplicantApplicationsWithMissingFileAssociations ) ) return ( true == $boolReturnSqlOnly ) ? $strSql : true;
		if( true == valObj( $objApplication, 'CApplication' ) && CApplicationStep::SUMMARY < $this->getApplicationStepId() && false == in_array( $objApplication->getLeaseIntervalTypeId(), CLeaseIntervalType::$c_arrintCorpororateScreeningLeaseIntervalTypes ) && false == valArr( $arrmixApplicantApplicationsWithMissingFileAssociations ) ) return ( true == $boolReturnSqlOnly ) ? $strSql : true;
	}
		$boolIsValid = true;

	// if customer type is changed then Soft Delete file association of applicant
		if( true == valId( $this->m_intOldCustomerTypeId ) && $this->m_intOldCustomerTypeId != $this->m_intCustomerTypeId ) {
			$arrobjApplicantApplicationFileAssociationsToDelete = $this->fetchFileAssociationsToDelete( $objDatabase, true );
			$this->deleteFileAssociationsAndFileSignatures( $arrobjApplicantApplicationFileAssociationsToDelete, $intCurrentUserId, $objDatabase );
		}

		if( CCustomerType::NON_LEASING_OCCUPANT == $this->getCustomerTypeId() ) return;

		$intCompanyApplicationId = ( true == valObj( $objApplication, 'CApplication' ) ) ? $objApplication->getCompanyApplicationId() : NULL;

	//  Get availalble referece file associations for the current customer type created for application in his preffered locale or determine it based on preference hirarchy.
		CLocaleContainer::createService()->setPreferredLocaleCode( $this->fetchPreferredLocaleCode( $objDatabase ), $objDatabase, $objApplication->getOrFetchProperty( $objDatabase ) );
		$arrobjApplicationFileAssociations	= ( array ) CFileAssociations::createService()->fetchFileAssociationsByCustomerTypeIdByApplicationIdByFileTypeSystemCodeByCid( $this->getCustomerTypeId(), $this->getApplicationId(), CFileType::SYSTEM_CODE_POLICY, $this->getCid(), $objDatabase, $intCompanyApplicationId, $strApplicantPreferedLocaleCode ); 	// 31 ms
		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );

	//  If no referece file association exists for current customer type, then go back;
		if( false == valArr( $arrobjApplicationFileAssociations ) ) {
			return ( true == $boolReturnSqlOnly ) ? $strSql : true;
		}

	//  Get Existing file associations for current applicant if exists.
		$intApplicantApplicationFileAssociationCount 	= $this->fetchFileAssociationsCountByFileTypeSystemCode( CFileType::SYSTEM_CODE_POLICY, $objDatabase ); 		// 31 ms

	//  if referece file association count is less than or equal to existing file associatins for the current applicant, then go back;
		if( $intApplicantApplicationFileAssociationCount >= \Psi\Libraries\UtilFunctions\count( $arrobjApplicationFileAssociations ) ) {
			return ( true == $boolReturnSqlOnly ) ? $strSql : true;
		}

		$arrobjFileSignatures = [];

		if( true == valArr( $arrmixApplicantApplicationsWithMissingFileAssociations ) ) {
			foreach( $arrmixApplicantApplicationsWithMissingFileAssociations as $arrmixApplicantApplicationWithMissingFileAssociation ) {
				$strFileAssociationkey = $arrmixApplicantApplicationWithMissingFileAssociation['document_id'] . '_' . $arrmixApplicantApplicationWithMissingFileAssociation['aa_applicant_id'];
				$arrstrMissingFileAssociationKeys[] = $strFileAssociationkey;
			}
		}

		foreach( $arrobjApplicationFileAssociations as $objApplicationFileAssociation ) {

			$strFileAssociationSearchkey = $objApplicationFileAssociation->getDocumentId() . '_' . $this->getApplicantId();
			if( true == valArr( $arrstrMissingFileAssociationKeys ) && false == in_array( $strFileAssociationSearchkey, $arrstrMissingFileAssociationKeys ) ) {
				continue;
			}

			$objApplicantApplicationFileAssociation = clone $objApplicationFileAssociation;
			$objApplicantApplicationFileAssociation->setApplicantId( $this->getApplicantId() );
			$objApplicantApplicationFileAssociation->setId( $objApplicantApplicationFileAssociation->fetchNextId( $objDatabase ) );
			$objApplicantApplicationFileAssociation->setInspectionResponseId( NULL );
			$objApplicantApplicationFileAssociation->setMaintenanceRequestId( NULL );

			$objFileSignature = $objApplicantApplicationFileAssociation->createFileSignature();
			$objFileSignature->setApplicantApplicationId( $this->getId() );

			$arrobjFileSignatures[] = $objFileSignature;

			$arrobjApplicantApplicationFileAssociations[] = $objApplicantApplicationFileAssociation;
		}

		if( false == valArr( $arrobjApplicantApplicationFileAssociations ) ) return ( true == $boolReturnSqlOnly ) ? $strSql : false;

		switch( NULL ) {
			default:

				$boolIsValid = true;

			foreach( $arrobjApplicantApplicationFileAssociations as $objApplicantApplicationFileAssociation ) {
				$boolIsValid &= $objApplicantApplicationFileAssociation->validate( VALIDATE_INSERT );
			}

			if( true == valArr( $arrobjFileSignatures ) ) {
				foreach( $arrobjFileSignatures as $objFileSignature ) {
					$boolIsValid &= $objFileSignature->validate( VALIDATE_INSERT );
				}
			}

			if( false == $boolIsValid ) {
				trigger_error( 'File associations failed to pass validation.', E_USER_ERROR );
				return ( true == $boolReturnSqlOnly ) ? $strSql : false;
			}

			foreach( $arrobjApplicantApplicationFileAssociations as $objApplicantApplicationFileAssociation ) {

				$strReturnValue = $objApplicantApplicationFileAssociation->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

				$strErrorMsgs = '';
				if( false == $boolReturnSqlOnly && false == $strReturnValue ) {
					trigger_error( 'Failed to insert file association.', E_USER_ERROR );
					$boolIsValid &= $strReturnValue;
					break 2;
				}

				if( true == $boolReturnSqlOnly ) $strSql .= $strReturnValue;
			}

			if( true == valArr( $arrobjFileSignatures ) ) {
				foreach( $arrobjFileSignatures as $objFileSignature ) {
					$strReturnValue = $objFileSignature->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

					if( true == $boolReturnSqlOnly ) {
						$strSql .= $strReturnValue;
					} else {
						$boolIsValid &= $strReturnValue;
					}
				}
			}
		}

		return ( true == $boolReturnSqlOnly ) ? $strSql : $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolForceDeleteFileAssociations = true, $boolIsSoftDelete = true ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intCurrentUserId, $objDatabase, false, false );
		}

		$boolIsValid	= true;
		$strDeleteQuery	= '';

		if( true == $boolForceDeleteFileAssociations ) {
			$arrobjAAFileAssociations = ( array ) $this->fetchFileAssociationsToDelete( $objDatabase );
			$arrobjFileSignatures 	  = ( array ) $this->fetchFileSignaturesByFileAssociationIdsToDelete( array_keys( $arrobjAAFileAssociations ), $objDatabase );

			// Below code optimised to perform hard delete for file association and file signature records within a single database call
			if( true == valArr( $arrobjFileSignatures ) ) {
				$strDeleteQuery .= \Psi\Eos\Entrata\CFileSignatures::createService()->getBulkDeleteSql( $arrobjFileSignatures, $objDatabase );
			}

			if( true == valArr( $arrobjAAFileAssociations ) ) {
				$strDeleteQuery .= CFileAssociations::createService()->getBulkDeleteSql( $arrobjAAFileAssociations, $objDatabase );
			}

			$boolIsValid &= ( '' != $strDeleteQuery ) ? fetchData( $strDeleteQuery, $objDatabase ) : true;
		}

		if( true == $boolIsValid ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function insertApplicationPayments( $arrobjArPayments, $intApplicatinStageId, $intApplicatinStatusId, $intCurrentUserId, $objDatabase, $objPaymentDatabase = NULL, $intIsTerminal = 0, $strSignatureImageContent = NULL ) {
		if( false == valArr( $arrobjArPayments ) ) return;

		foreach( $arrobjArPayments as $objArPayment ) {

			// When applicant clicks on pay later option then we create the payment object, but we are not processing that payment, hence we are not getting the id. - NRW
			if( ( true == is_null( $objArPayment->getPaymentTypeId() ) || CPaymentType::CHECK == $objArPayment->getPaymentTypeId() || CPaymentType::EMONEY_ORDER == $objArPayment->getPaymentTypeId() ) && true == is_null( $objArPayment->getId() ) ) {
				continue;
			}

			$objApplicationPayment = $this->createApplicationPayment();
			$objApplicationPayment->setArPaymentId( $objArPayment->getId() );
			$objApplicationPayment->setApplicationStageId( $intApplicatinStageId );
			$objApplicationPayment->setApplicationStatusId( $intApplicatinStatusId );

			if( false == $objApplicationPayment->insert( $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( '-3', '', __( 'Failed to insert in application_payments' ) ) ); // Failed to insert in application_payments
				trigger_error( 'Failed to insert in application_payment for applicant application id [' . $this->getId() . '], ar_payment_id[' . $objArPayment->getId() . '] with amount[ ' . $objArPayment->getPaymentAmount() . ' ] for management comapany [' . $objArPayment->getCid() . ']', E_USER_WARNING );
				return false;
			}

			if( 1 == $intIsTerminal ) {

				$objArPaymentImage = $this->createArPaymentImage();
				$objArPaymentImage->setArPaymentId( $objArPayment->getId() );
				$objArPaymentImage->setImageName( 'signature' . $objArPayment->getId() . '.png' );
				$objArPaymentImage->setImageContent( base64_decode( $strSignatureImageContent ) );

				if( false == $objArPaymentImage->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( '-3', '', __( 'Failed to insert in ar_payment_images' ) ) );
					trigger_error( 'Failed to insert in ar_payment_images for applicant application id [' . $this->getId() . '], ar_payment_id[' . $objArPayment->getId() . '] with amount[ ' . $objArPayment->getPaymentAmount() . ' ] for management comapany [' . $objArPayment->getCid() . ']', E_USER_WARNING );
					return true;
				}

			}

		}

		return true;
	}

	public function deleteFileAssociationsAndFileSignatures( $arrobjApplicantApplicationFileAssociations, $intCurrentUserId, $objDatabase ) {

		$boolIsValid = true;
		if( false == valArr( $arrobjApplicantApplicationFileAssociations ) ) return true;

		$arrintFileAssociationIds = array_keys( $arrobjApplicantApplicationFileAssociations );

		if( true == valArr( $arrintFileAssociationIds ) ) {
			$arrobjFileSignatures = ( array ) $this->fetchFileSignaturesByFileAssociationIdsToDelete( $arrintFileAssociationIds, $objDatabase );
		}

		switch( NULL ) {
			default:

				foreach( $arrobjFileSignatures as $objFileSignature ) {
					if( false == $objFileSignature->delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) ) {
						$boolIsValid = false;
						break 2;
					}
				}

				// If record exists return true
				foreach( $arrobjApplicantApplicationFileAssociations as $objApplicantApplicationFileAssociation ) {
					if( false == $objApplicantApplicationFileAssociation->delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) ) {
						$boolIsValid = false;
						break 2;
					}
				}
		}

		return $boolIsValid;
	}

	public function deleteApplicantApplicationTransmissions( $intCurrentUserId, $objDatabase ) {
		$boolIsValid = true;

		$arrobjApplicantApplicationTransmissions = ( array ) $this->fetchApplicantApplicationTransmissions( $objDatabase );

		switch( NULL ) {
			default:
				foreach( $arrobjApplicantApplicationTransmissions as $objApplicantApplicationTransmission ) {
					if( false == $objApplicantApplicationTransmission->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid = false;
						break 2;
					}
				}
		}

		return $boolIsValid;
	}

	public function deleteApplicantApplicationSubmission( $intCurrentUserId, $objDatabase ) {
		$boolIsValid = true;

		$arrobjApplicantApplicationSubmissions = ( array ) $this->fetchApplicantApplicationSubmission( $objDatabase );

		switch( NULL ) {
			default:
				foreach( $arrobjApplicantApplicationSubmissions as $objApplicantApplicationSubmission ) {
					if( false == $objApplicantApplicationSubmission->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid = false;
						break 2;
					}
				}
		}

		return $boolIsValid;
	}

	public function getCurrentLeaseGroupApplicantNames( $arrobjApplicantApplications, $arrobjApplicants ) {

		$arrstrApplicantNames = [];

		unset( $arrobjApplicantApplications[$this->getId()] );

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {

			$strApplicantName = $arrobjApplicants[$objApplicantApplication->getApplicantId()]->getNameFirst();
			if( false == is_null( $arrobjApplicants[$objApplicantApplication->getApplicantId()]->getNameFirst() ) ) {
				$strApplicantName .= ' ' . $arrobjApplicants[$objApplicantApplication->getApplicantId()]->getNameLast();
			}

			$arrstrApplicantNames[] = $strApplicantName;
		}

		return $arrstrApplicantNames;

	}

	public function fetchPrimediaPropertyManagerUser( $objClient, $objProperty, $objDatabase ) {

		$objPrimediaCompanyUser = NULL;
		// Sales Lead Generate Industry Properties (created by HTML parser) are excluded
		if( 4429 != $objClient->getId() ) {
			$objPrimediaCompanyPreference = $objClient->fetchCompanyPreferenceByKey( 'PRIMEDIA_COMPANY_REMOTE_PRIMARY_KEY', $objDatabase );

			if( true == valObj( $objPrimediaCompanyPreference, 'CCompanyPreference' ) && false == is_null( $objProperty->getRemotePrimaryKey() ) ) {
				$boolIsPrimediaCompany				= true;
				$arrobjPrimediaCompanyEmployees  		= $objProperty->fetchCompanyEmployees( $objDatabase );

				if( true == valArr( $arrobjPrimediaCompanyEmployees ) ) {
					foreach( $arrobjPrimediaCompanyEmployees as $objPrimediaCompanyEmployee ) {
						$objPrimediaCompanyUser = $objClient->fetchCompanyUserByCompanyEmployeeId( $objPrimediaCompanyEmployee->getId(), $objDatabase );
						$objIlsImporter = new CIlsImporterController();
						$strCompanyUserPassword = $objIlsImporter->createPassword( $objProperty ->getId() );
						$objPrimediaCompanyUser->setPasswordEncrypted( $strCompanyUserPassword );
						break;
					}
				}
			}
		}
		return $objPrimediaCompanyUser;
	}

	public function fetchApplicantApplicationMediasCid( $objDatabase ) {
		return CApplicantApplicationMedias::fetchApplicantApplicationMediasByApplicantApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function loadApplicationDataObject( $intCurrentUserId, $objDatabase ) {

		$this->m_objApplicationDataObject = new CApplicationDataObject();

		$this->m_objApplicationDataObject->m_objApplicant				= $this->getOrFetchApplicant( $objDatabase );
		$this->m_objApplicationDataObject->m_objApplicantApplication	= $this;
		$this->m_objApplicationDataObject->m_objApplication				= $this->fetchApplication( $objDatabase );
		$this->m_objApplicationDataObject->m_objDatabase				= $objDatabase;
		$this->m_objApplicationDataObject->m_objClient					= $this->getOrFetchClient( $objDatabase );
		$this->m_objApplicationDataObject->m_intCompanyUserId			= $intCurrentUserId;
		$this->m_objApplicationDataObject->m_objProperty				= NULL;

		if( true == valObj( $this->m_objApplicationDataObject->m_objApplication, 'CApplication' ) ) {
			$this->m_objApplicationDataObject->m_objProperty = $this->m_objApplicationDataObject->m_objApplication->getOrfetchProperty( $objDatabase );
			if( true == valObj( $this->m_objApplicationDataObject->m_objProperty, 'CProperty' ) && true == in_array( COccupancyType::STUDENT, $this->m_objApplicationDataObject->m_objProperty->getOccupancyTypeIds() ) ) {
				$objPropertyPrefernce = $this->m_objApplicationDataObject->m_objProperty->fetchPropertyPreferenceByKey( 'ENABLE_SEMESTER_SELECTION', $objDatabase );
				if( true == valObj( $objPropertyPrefernce, 'CPropertyPreference' ) ) {
					$this->m_objApplicationDataObject->m_boolIsStudentProperty = true;
				}
 			}
 			if( true == valObj( $this->m_objApplicationDataObject->m_objProperty, 'CProperty' ) ) {
 				$this->m_objApplicationDataObject->m_arrobjPropertyPreferences = $this->m_objApplicationDataObject->m_objProperty->getOrFetchPropertyPreferences( $objDatabase );
 			}

		}

		return $this->m_objApplicationDataObject;
	}

	public function isGuestCard() {
		return ( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && CApplicationStage::PRE_APPLICATION == $this->getApplicationStageId() ) ? true : false;
	}

	public function fetchFileAssociationsByDataKey( $strDataKey, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantIdByApplicationIdByFileTypeIdByDataKeyByCid( $this->getApplicantId(), $this->getApplicationId(), $strDataKey, $this->getCid(), $objDatabase );
	}

	public function getScheduledAppointmentDate() {
		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) ) {
			return NULL;
		}
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $objApplication->getCid(), $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$objPropertyTimeZone = $objProperty->getOrfetchTimezone( $this->m_objDatabase );

		$objApplicationScheduledAppointment = \Psi\Eos\Entrata\CEvents::createService()->fetchScheduledAppointmentEventByLeaseIdByLeaseIntervalIdByCid( $objApplication->getLeaseId(), $objApplication->getLeaseIntervalId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplicationScheduledAppointment, 'CEvent' ) ) {
			return NULL;
		}

		return __( '{%t, 0, DATE_NUMERIC_STANDARD} from {%t, 1, TIME_SHORT} to {%t, 2, TIME_SHORT}', [ CInternationalDateTime::create( $objApplicationScheduledAppointment ->getScheduledDatetime(), $objPropertyTimeZone->getTimeZoneName() ), CInternationalDateTime::create( $objApplicationScheduledAppointment->getScheduledDatetime(), $objPropertyTimeZone->getTimeZoneName() ), CInternationalDateTime::create( $objApplicationScheduledAppointment->getScheduledEndDatetime(), $objPropertyTimeZone->getTimeZoneName() ) ] );
	}

	function getEmailLocalCodeMapping( $objPropertyPreference, $objProperty ) {
		$objJsonDetails = NULL;
		$arrstrEmailAddress = array();
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$objJsonDetails = $objPropertyPreference->getDetails()->_translated ?? NULL;
				$arrmixDetails = json_decode( json_encode( $objJsonDetails ), true );
				if( true == valArr( $arrmixDetails ) ) {
					foreach( $arrmixDetails as $strKey => $arrmixDetail ) {
						if( $strKey == 'field_locales' || $strKey == 'default_locale' ) {
							continue;
						}
						$arrstrToEmailAddress = explode( ',', $arrmixDetail['value'] );
						$arrstrEmailAddress[$strKey] = $arrstrToEmailAddress;
					}
				} else {
					if( true == valStr( $objProperty->getLocaleCode() ) ) {
						$arrstrEmailAddress[$objProperty->getLocaleCode()] = CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
					} else {
						$arrstrEmailAddress['en_US'] = CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
					}
				}
		}
		return $arrstrEmailAddress;
	}

	public function generateGCICConsentForm( $objCompanyUser, $objApplication, $objDatabase, $boolIsRequiredInformationOfGCICChanged = false ) {

		if( false == valObj( $objApplication, 'CApplication' ) ) {
			trigger_error( 'Failed to Application object while generating gcic document', E_USER_ERROR );
			return false;
		}

		$boolIsResidentVerifyEnabled 	= $objApplication->isResidentVerifyEnabled( $objDatabase );
		if( false == $boolIsResidentVerifyEnabled ) {
			return false;
		}

		$objClient      = $this->getOrFetchClient( $objDatabase );
		$objApplicant   = CApplicants::fetchSimpleApplicantByIdByCid( $this->getApplicantId(), $objClient->getId(), $objDatabase, false );

		if( false == valObj( $objApplicant, 'CApplicant' ) ) {
			trigger_error( 'Failed to Applicant object while generating gcic document. ApplicationId[' . $objApplication->getId() . '] CID[' . $objApplication->getCid() . ']', E_USER_ERROR );
			return false;
		}

		$objFileAssociation = CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdBySystemCodeByCid( $objApplicant->getId(), $this->getApplicationId(), CFileType::SYSTEM_CODE_GCIC, $objClient->getId(), $objDatabase );

		$boolIsNonGAAddress = false;
		if( false == $this->isGCICAddress( $objApplication->getPropertyId(), $objApplicant->getCustomerId(), $objDatabase ) ) {
			$boolIsNonGAAddress = true;
		}

		if( true == valObj( $objFileAssociation, 'CFileAssociation' ) ) {

			if( false == $boolIsRequiredInformationOfGCICChanged && false == $boolIsNonGAAddress ) {
				return true;
			}

			$objFileAssociation->setDeletedBy( $objCompanyUser->getId() );
			$objFileAssociation->setDeletedOn( 'NOW()' );
			if( false == $objFileAssociation->update( $objCompanyUser->getId(), $objDatabase ) ) {
				return false;
			}
		}

		if( $boolIsNonGAAddress ) {
			return false;
		}

		if( false == is_null( $objApplicant->getTaxNumberEncrypted() ) ) {
			$objApplicant->setTaxNumber( preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $objApplicant->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] ) ) );
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );
		$objSmarty->assign( 'applicant', $objApplicant );
		$strGCICDocumentContent = $objSmarty->fetch( PATH_INTERFACES_ENTRATA . 'prospects/applications_tab/application/application_screenings/resident_verify/gcic_document_template.tpl' );

		$objApplication->loadDocumentManager( $objDatabase, $objCompanyUser->getId() );
		$this->setDocumentManager( $objApplication->getDocumentManager() );

		$strFileTypeCode	= CFileType::SYSTEM_CODE_GCIC;
		$strTitle 			= CFileType::GEORGIA_CRIME_INFORMATION_CENTER_CONSENT_FORM_NAME;
		$strFileName 		= 'Georgia_Crime_Information_Center' . '_' . $objApplicant->getId() . '_' . date( 'Ymdhis' ) . '.pdf';

		$objFileAssociation = $objApplication->createFileAssociation();
		$objFileAssociation->setApplicantId( $objApplicant->getId() );
		$objFileAssociation->setCustomerId( $objApplicant->getCustomerId() );

		$strFilePath = $objFileAssociation->buildFilePath( $strFileTypeCode );
		$strTempPath = getMountsPath( $objApplication->getCid(), '/files/' . $strFilePath );

		$objFile = $objClient->createFile();
		$objFile->setFileExtensionId( CFileExtension::APPLICATION_PDF );
		$objFile->setFileName( $strFileName );
		$objFile->setTitle( $strTitle );
		$objFile->setPropertyId( $objApplication->getPropertyId() );
		$objFile->setFileTypeSystemCode( CFileType::SYSTEM_CODE_GCIC );
		$objFile->setApplicationId( $objApplication->getId() );

		$objFileType = $objClient->fetchFileTypeBySystemCode( $strFileTypeCode, $objDatabase );
		if( true == valObj( $objFileType, 'CFileType' ) ) {
			$objFile->setFileTypeId( $objFileType->getId() );
		}

		// key StorageGatway => Cid/propertyid/rv_gcic/yyyy/mm/dd/application id/filename
		$strStorageGetwayPath = $objFile->buildFilePath( $objFileType->getId() );
		$objFile->setFilePath( $strStorageGetwayPath );

		$objPrince = CPrinceFactory::createPrince();

		if( false == valObj( $objPrince, 'Prince' ) ) {
			trigger_error( 'Failed to load prince library object while generating gcic document for application ' . $objApplication->getId() . ' Cid ' . $objApplication->getCid() . ' Applicant id ' . $objApplicant->getId(), E_USER_WARNING );
			return false;
		}

		CFileIo::recursiveMakeDir( $strTempPath );

		$objPrince->setBaseURL( true === CONFIG_IS_SECURE_URL ? 'https://' : 'http://' . $_SERVER['HTTP_HOST'] );
		$objPrince->setHTML( 1 );
		$objPrince->convert_string_to_file( $strGCICDocumentContent, ( $strTempPath . $strFileName ) );

		// Creating the actual file for storage gateway
		$boolIsValid = $objFile->generateAndPutRvStorageGatewayObject( [ 'inputFile' => $strTempPath . $strFileName ], CScreeningUtils::getObjectStorageGateway() );
		if( false == $boolIsValid ) {
			trigger_error( 'Failed to process request for application ' . $objApplication->getId() . ' Cid ' . $objApplication->getCid() . ' Applicant id ' . $objApplicant->getId(), E_USER_WARNING );
			return false;
		}

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objFile->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$objFile->deleteFile();
					$objDatabase->rollback();
					break;
				}

				$objFileAssociation->setFileId( $objFile->getId() );
				$objFileAssociation->setApplicantId( $this->getApplicantId() );
				$objFileAssociation->setPropertyId( $objApplication->getPropertyId() );

				if( false == $objFileAssociation->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				// Log activity
				$objEventLibrary = $objApplication->createEventForGCICDocument( CEventType::GCIC_CONSENT_FORM_GENERATED, $objDatabase );
				$objEventLibrary->getEventLibraryDataObject()->setApplicant( $objApplicant );
				$objEventLibrary->getEventLibraryDataObject()->getEvent()->setIsExportEvent( false );
				$objEventLibrary->getEventLibraryDataObject()->getEvent()->setCustomerId( $objApplicant->getCustomerId() );
				$objEventLibrary->getEventLibraryDataObject()->getEvent()->setCompanyUser( $objCompanyUser );
				$objEventLibrary->buildEventDescription( $objApplication );

				if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
					trigger_error( 'Failed to insert GCIC generate consent form activity log. ' . $objApplicant->getNameFirst() . ' applicant id = ' . $objApplicant->getId(), E_USER_ERROR );
					$objDatabase->rollback();
					return false;
				}

				$objDatabase->commit();
				return true;
		}

		return false;
	}

	public function getLeadDocumentSigningLoginContent() {

		$objApplicant   = CApplicants::fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return NULL;
		}
		$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$strLoginId  = __( '<b>Login ID: </b>' ) . $objApplicant->getUsername();
		$strPassword = '';
		if( true == is_null( $objApplicant->getPasswordEncrypted() ) || 0 == strlen( $objApplicant->getPasswordEncrypted() ) ) {
			$strPassword = md5( uniqid( mt_rand(), true ) );
			$strPassword = \Psi\CStringService::singleton()->substr( $strPassword, 0, 6 );
			$objApplicant->setPassword( $strPassword );
			$strPassword = __( '<b>Password: </b>' ) . $strPassword;
			$objApplicant->setPasswordEncrypted( CHash::createService()->hashCustomer( $strPassword ) );
			$objApplicant->setRequiresPasswordChange( 1 );

			if( false == $objApplicant->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$this->m_objDatabase->rollback();
			}
		}

		$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $this->m_objDatabase );

		$strApplicationLayout = 'application_authentication';

		$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		$strApplicationUrl = $strSecureBaseName . 'Apartments/module/' . $strApplicationLayout . '/action/view_login/returning_applicant/1/property[id]/' . $objProperty->getId();

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES']->getValue() ) ) {
			$objOneTimeLink    = new COneTimeLink();
			$intUniqueId       = \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );
			$strApplicationUrl = $strSecureBaseName . 'Apartments/module/' . $strApplicationLayout . '/action/autologin/property[id]/' . $objProperty->getId() . '/application[id]/' . $objApplication->getId() . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $objApplicant->getId() . '/login_from_one_time_link/1/';

			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );

			$this->m_objDatabase->begin();

			if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert one time link.' ), NULL ) );
				$this->m_objDatabase->rollback();
			}

			$this->m_objDatabase->commit();
		}

		$strLink = __( '<a href="{%s, 0}"  target="_blank"><u>Click here</u></a> to review and sign your document.', [ $strApplicationUrl ] );

		$strLoginIdPassword = '<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td style="padding-top:20px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLoginId . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strPassword . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLink . '</td>
									</tr>
								</table>';

		return $strLoginIdPassword;

	}

	public function sendGCICESignEmail( $objApplication, $objCompanyUser, $objProperty, $objClient, $objDatabase, $objEmailDatabase, $boolIsRequiredInformationOfGCICChanged = false, $boolIsResendEmail = false ) {
		if( false == valObj( $objProperty, 'CProperty' ) || false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objCompanyUser, 'CCompanyUser' ) || false == valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$objApplicant = CApplicants::fetchApplicantByIdByApplicationIdByCid( $this->getApplicantId(), $this->getApplicationId(), $objClient->getId(), $objDatabase );

		if( false == valObj( $objApplicant, 'CApplicant' ) || false == valStr( $objApplicant->getUsername() ) ) {
			return false;
		}

		$objFileAssociation = CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdBySystemCodeByCid( $objApplicant->getId(), $this->getApplicationId(), CFileType::SYSTEM_CODE_GCIC, $objClient->getId(), $objDatabase );
		if( false == valObj( $objFileAssociation, 'CFileAssociation' ) ) {
			trigger_error( 'Failed to load GCIC file while sending gcic email. ApplicationId[' . $this->getId() . '] ApplicantId[' . $objApplicant->getId() . '] Cid[' . $objClient->getId() . '].', E_USER_ERROR );
			return false;
		}

		if( false == $this->isGCICAddress( $objApplication->getPropertyId(), $objApplicant->getCustomerId(), $objDatabase ) ) {
			return false;
		}
		if( false == $boolIsResendEmail && false == $boolIsRequiredInformationOfGCICChanged && true == $objApplication->isGCICEmailAlreadySent( $objApplicant->getCustomerId(), $objDatabase ) ) {
			return true;
		}

		$strEmailSubject = $objProperty->getPropertyName() . ' - Georgia Crime Information Center (GCIC) Consent Form';
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setId( $objSystemEmail->fetchNextId( $objEmailDatabase ) );
		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCREENING_EMAILS );
		$objSystemEmail->setCompanyEmployeeId( $objCompanyUser->getId() );
		$objSystemEmail->setSubject( $strEmailSubject );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );

		$strResidentVerifyPortalUrl = CScreeningUtils::getResidentVerifyPortalUrl() . '/gcicConsentForms/';

		$arrmixTemplateParameters = [];
		$arrmixTemplateParameters['PROPERTY_NAME']   = $objProperty->getPropertyName();
		$arrmixTemplateParameters['NAME_FIRST'] = $objApplicant->getNameFirst();
		$arrmixTemplateParameters['image_url']       = CONFIG_COMMON_PATH;

		$objOneTimeLink = new COneTimeLink();
		$strUniqueId = substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );
		$objOneTimeLink->setValues( [ 'cid' => $objClient->getId(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $strUniqueId ] );

		$strUrlKey = 'client[id]=' . $objClient->getId() . '&';
		$strUrlKey .= 'property[id]=' . $objProperty->getId() . '&';
		$strUrlKey .= 'application[id]=' . $objApplication->getId() . '&';
		$strUrlKey .= 'applicant[id]=' . $objApplicant->getId() . '&';
		$strUrlKey .= 'applicant[key]=' . $strUniqueId;
		$strEncryptedKey = rawurlencode( CEncryption::encrypt( $strUrlKey, CONFIG_KEY_ID ) );

		$strApplicationUrl = $strResidentVerifyPortalUrl . '?module=application_gcic_esign&action=view_gcic_document_page&keys=' . $strEncryptedKey;

		$arrmixTemplateParameters['application_url'] = $strApplicationUrl;

		$objSystemEmail->setApplicantId( $objApplicant->getId() );
		$objSystemEmail->setToEmailAddress( $objApplicant->getUsername() );

		$objRenderTemplate                      = new CRenderTemplate( PATH_INTERFACES_COMMON . 'system_emails/applications/gcic_esign_email.tpl', PATH_INTERFACES_COMMON, false, $arrmixTemplateParameters );
		$arrmixTemplateParameters['content']    = $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_COMMON . 'system_emails/applications/gcic_esign_email.tpl' );

		$objRenderTemplate     = new CRenderTemplate( 'ResidentVerify/reports/email_template.tpl', PATH_INTERFACES_COMMON, false, $arrmixTemplateParameters );
		$strClientEmailContent = $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_COMMON . 'ResidentVerify/reports/email_template.tpl' );

		$objSystemEmail->setHtmlContent( $strClientEmailContent );

		if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $objDatabase ) ) {
			trigger_error( 'Failed to insert GCIC one time link for applicant ' . $objApplicant->getNameFirst() . ' applicant id = ' . $objApplicant->getId(), E_USER_ERROR );
			return false;
		}

		if( false == $objSystemEmail->insert( $objCompanyUser->getId(), $objEmailDatabase, false, false ) ) {
			trigger_error( 'Failed to send GCIC Esign email to applicant ' . $objApplicant->getNameFirst() . ' applicant id = ' . $objApplicant->getId(), E_USER_ERROR );
			return false;
		}

		// Log activity
		$objEventLibrary = $objApplication->createEventForGCICDocument( CEventType::GCIC_CONSENT_FORM_EMAIL_SENT, $objDatabase );
		$objEventLibrary->getEventLibraryDataObject()->setApplicant( $objApplicant );
		$objEventLibrary->getEventLibraryDataObject()->getEvent()->setDataReferenceId( $objSystemEmail->getId() );
		$objEventLibrary->getEventLibraryDataObject()->getEvent()->setIsExportEvent( true );
		$objEventLibrary->getEventLibraryDataObject()->getEvent()->setCustomerId( $objApplicant->getCustomerId() );
		$objEventLibrary->getEventLibraryDataObject()->getEvent()->setCompanyUser( $objCompanyUser );
		$objEventLibrary->getEventLibraryDataObject()->setData( [ 'is_resend_email' => $boolIsResendEmail ] );
		$objEventLibrary->buildEventDescription( $objApplication );

		if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			trigger_error( 'Failed to insert GCIC Send email activity log. ' . $objApplicant->getNameFirst() . ' applicant id = ' . $objApplicant->getId(), E_USER_ERROR );
			return false;
		}

		return true;
	}

	public function isGCICAddress( $intPropertyId, $intCustomerId, $objDatabase ) {

		$objPrimaryPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $intPropertyId, CAddressType::PRIMARY, $this->getCid(), $objDatabase );
		$strPropertyStateCode = valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ? $objPrimaryPropertyAddress->getStateCode() : '';

		$objCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $intCustomerId, CAddressType::CURRENT, $this->getCid(), $objDatabase );
		if( false == valObj( $objCustomerAddress, 'CCustomerAddress' ) || false == CScreeningUtils::isGCICAddress( $strPropertyStateCode, $objCustomerAddress->getStateCode(), $intPropertyId, $this->getCid(), $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function isGCICFormEnabled( $intPropertyId ) {
		$objResidentVerifyLibrary = new CResidentVerifyLibrary();
		return $objResidentVerifyLibrary->isGCICFormEnabled( $intPropertyId, $this->getCid(), $this->m_objDatabase );
	}

	public function getLeadLeaseSigningInvitationContent() {

		$objApplicant   = CApplicants::fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );
		$objPrimaryApplicant = CApplicants::fetchPrimaryApplicantByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return NULL;
		}
		$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$strLoginId  = __( '<b>Login ID: </b>' ) . $objApplicant->getUsername();
		$strPassword = '';
		if( true == is_null( $objApplicant->getPasswordEncrypted() ) || 0 == strlen( $objApplicant->getPasswordEncrypted() ) ) {
			$strPassword = md5( uniqid( mt_rand(), true ) );
			$strPassword = \Psi\CStringService::singleton()->substr( $strPassword, 0, 6 );
			$objApplicant->setPassword( $strPassword );
			$strPassword = __( '<b>Password: </b>' ) . $strPassword;
			$objApplicant->setPasswordEncrypted( CHash::createService()->hashCustomer( $strPassword ) );
			$objApplicant->setRequiresPasswordChange( 1 );

			if( false == $objApplicant->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$strPassword = '';
			}
		}

		$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $this->m_objDatabase );
		$strPropertyPhoneNumber = $objProperty->getCustomPropertyOfficePhoneNumberText();

		$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $objProperty->getId();

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES']->getValue() ) ) {
			$objOneTimeLink    = new COneTimeLink();
			$intUniqueId       = \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );

			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );
			$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/autologin/property[id]/' . $objProperty->getId() . '/application[id]/' . $objApplication->getId() . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $objApplicant->getId() . '/login_from_one_time_link/1/';

			if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $objProperty->getId();
			}
		}

		$strLink = __( '<a href="{%s, 0}" target="_blank"><u>Click here</u></a> to review and sign your lease.', [ $strApplicationUrl ] );

		$strGuarantorOrApplicantFullName = ( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) ? $objPrimaryApplicant->getNameFirst() . ' ' . $objPrimaryApplicant->getNameLast() . ' ' . $objPrimaryApplicant->getNameLastMatronymic() : $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . ' ' . $objApplicant->getNameLastMatronymic();

		$strLeadLeaseSigningInvitationContent = __( '
									<tr>
										<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">Lease documents for ' . $strGuarantorOrApplicantFullName . ' at ' . $objProperty->getPropertyName() . ' are ready for your signature! Click on the link below to log in and follow the instructions to review and sign. ' . $strPropertyPhoneNumber . '</td>
									</tr>
									<tr>
										<td style="padding-top:20px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLoginId . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strPassword . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLink . '</td>
									</tr>' );

		return $strLeadLeaseSigningInvitationContent;

	}

	public function getApplicationLoginDetailsContent() {

		$objApplicant   = CApplicants::fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return NULL;
		}

		$strSecureBaseNameUrl		= $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		$strApplicationUrl 			= $strSecureBaseNameUrl . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $objApplication->getPropertyId();
		if( false == is_null( $objApplication->getInternetListingServiceId() ) && CInternetListingService::APARTMENTGUIDE_APPLICATION == $objApplication->getInternetListingServiceId() ) {
			$strApplicationUrl 		.= '/ils/' . $objApplication->getInternetListingServiceId() . '/ils_template/' . $objApplication->getInternetListingServiceId();
		}

		if( true == $objApplicant->getRequiresPasswordChange() ) {
			$strApplicationLoginDetailsContent = __( '<tr>
					<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">
						<p>
							Thank you for registering with us!<br/><br/>We have created an account for you so that you will be able to access your application at any time.<br/>If you have not yet completed your application, please <a href="{%s,0}"  target="_blank"><u>click here</u></a> to access it with the following user name and password:
						</p>
					</td>
				</tr>
				<tr>
					<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">
						Username : {%s,1}
					</td>
					<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">
						Password : {%s,2}
					</td>
				</tr>', [ $strApplicationUrl, $objApplicant->getUsername(), $objApplicant->getPassword() ] );
		} else {
			$strApplicationLoginDetailsContent = __( '<tr>
					<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">
						<p>
							Thank you for registering with us!<br/><br/>We have created an account for you so that you will be able to access your application at any time.<br/>If you have not yet completed your application, please <a href="{%s,0}" target="_blank"><u>click here</u></a> to access it with your username and the password you created.
						</p>
					</td>
				</tr>', [ $strApplicationUrl ] );
		}

		return $strApplicationLoginDetailsContent;

	}

	public function getProspectPortalAutoLoginLink() {
		$strProspectPortalAutoLoginLink = '';
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );
		$arrmixApplicants   = \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantUserNamePasswordById( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, CApplication::class ) ) {
			return NULL;
		}

		$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		if( false == valStr( $strSecureBaseName ) ) {
			return NULL;
		}

		$strProspectPortalAutoLoginLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $objApplication->getPropertyId();

		if( true == valArr( $arrmixApplicants ) && false == is_null( $arrmixApplicants[0]['username'] ) && false == is_null( $arrmixApplicants[0]['password_encrypted'] ) ) {

			$intUniqueId    = \Psi\CStringService::singleton()->substr( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encryptUrl( uniqid( mt_rand(), true ), CONFIG_SODIUM_KEY_ID ), 0, 240 );
			$objOneTimeLink = new COneTimeLink();
			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $this->getApplicantId(), 'key_encrypted' => $intUniqueId ] );

			if( true == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$strProspectPortalAutoLoginLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/autologin/property[id]/' . $objApplication->getPropertyId() . '/application[id]/' . $objApplication->getId() . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $this->getApplicantId() . '/login_from_one_time_link/1/';
			}
		}

		return $strProspectPortalAutoLoginLink;
	}

	public function sendApplicationPaymentReceiptEmail( $objArPayment, $objDatabase ) {

		if( false == \valObj( $objArPayment, CArPayment::class ) || false == \valObj( $objDatabase, \CDatabase::class ) ) {
			return false;
		}

		$objClient = $this->getOrFetchClient( $objDatabase );
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objArPayment->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objProperty, \CProperty::class ) ) {
			return false;
		}

		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByApplicantIdByCid( $this->getApplicationId(), $this->getApplicantId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objApplication, \CApplication::class ) ) {
			return false;
		}

		$objApplication->setProperty( $objProperty );

		$strSystemMessageKey = 'APPLICATION_PAYMENT_RECEIPT';

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setClient( $objClient );
		$objSystemMessageLibrary->setDatabase( $objDatabase );
		$objSystemMessageLibrary->setPropertyId( $objArPayment->getPropertyId() );

		$arrintPermissionedPsProductIds	= [ CPsProduct::MESSAGE_CENTER ];
		$arrintPsProductIds = ( array ) \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPsProductPermissionsByPsProductIdsByPropertyIdsByCid( $arrintPermissionedPsProductIds, [ $objProperty->getId() ], $objClient->getId(), $objDatabase );

		$objSystemMessageLibrary->setSystemEmailTypeId( CSystemEmailType::AR_PAYMENT_NOTIFICATION );

		$objSystemMessageLibrary->setCompanyUserId( SYSTEM_USER_ID );
		$objSystemMessageLibrary->setIsLeadDirectSendToConsumer( true );

		$arrmixPropertyPreferences = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesKeyValueByKeysByPropertyIdByCid( [ 'PAYMENT_NOTIFICATION_EMAIL' ], $objArPayment->getPropertyId(), $objClient->getId(), $objDatabase );
		$objSystemMessageLibrary->setSystemMessageAudienceIds( [ \CSystemMessageAudience::LEAD ] );

		if( true == valArr( $arrmixPropertyPreferences ) && true == isset( $arrmixPropertyPreferences['PAYMENT_NOTIFICATION_EMAIL'] ) ) {
			$arrmixPropertyManagerEmails = explode( ',', $arrmixPropertyPreferences['PAYMENT_NOTIFICATION_EMAIL'] );
			$objSystemMessageLibrary->setToEmailAddresses( $arrmixPropertyManagerEmails );
			$objSystemMessageLibrary->setSystemMessageAudienceIds( [ \CSystemMessageAudience::LEAD, \CSystemMessageAudience::PROPERTY ] );
		}

		$objSystemMessageLibrary->setSystemMessageKey( $strSystemMessageKey );

		$objSystemMessageLibrary->setApplication( $objApplication );

		$arrmixExtraMetaData = [ 'ar_payment_id' => $objArPayment->getId(), 'property_id' => $objProperty->getId(),
			'applications' => [ 'ar_payment_id' => $objArPayment->getId() ]
		];

		$objSystemMessageLibrary->setExtraMetaData( $arrmixExtraMetaData );
		$objSystemMessageLibrary->setIsSkipMigrationModeCheck( true );

		if( false == $objSystemMessageLibrary->execute() ) {
			trigger_error( 'Failed to send email for payment Id : ' . $objArPayment->getId(), E_USER_WARNING );
		}

		$objCustomer = $objArPayment->getOrFetchCustomer( $objDatabase );
		$objCustomer->logCustomerActivity( \CEventType::PAYMENT_RECEIPT, $objProperty->getId(), SYSTEM_USER_ID, $objArPayment, NULL, $objDatabase );

		return true;

	}

	public function getApplicationArPaymentReceiptEmailCustomText() {
		$objApplication                         = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByApplicantIdByCid( $this->getApplicationId(), $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );

		if( false == \Psi\Libraries\UtilFunctions\valObj( $objApplication, 'CApplication' ) ) {
			return NULL;
		}

		$arrobjPropertyApplicationPreferences   = ( array ) $objApplication->getOrFetchPropertyApplicationPreferences( $this->m_objDatabase );
		$arrobjPropertyApplicationPreferences   = rekeyObjects( 'Key', $arrobjPropertyApplicationPreferences );

		$strApplicationPaymentReceiptEmailCustomText = NULL;
		if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == valObj( $this, 'CApplicantApplication' ) ) {
			if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
				if( true == array_key_exists( 'APPLICATION_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT', $arrobjPropertyApplicationPreferences ) && true == valStr( $arrobjPropertyApplicationPreferences['APPLICATION_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT']->getValue() ) ) {
					$strApplicationPaymentReceiptEmailCustomText = $arrobjPropertyApplicationPreferences['APPLICATION_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT']->getValue();
				}
			} else {
				if( true == array_key_exists( 'APPLICATION_CO_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT', $arrobjPropertyApplicationPreferences ) && true == valStr( $arrobjPropertyApplicationPreferences['APPLICATION_CO_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT']->getValue() ) ) {
					$strApplicationPaymentReceiptEmailCustomText = $arrobjPropertyApplicationPreferences['APPLICATION_CO_APPLICANT_PAYMENT_RECEIPT_EMAIL_TEXT']->getValue();
				}
			}
		}

		return $strApplicationPaymentReceiptEmailCustomText;
	}

	public function getApplicantApplicationTourDetails() {
		$objApplication				= $this->getOrFetchApplication( $this->m_objDatabase );

		if( false == valObj( $objApplication, CApplication::class ) ) {
			return '';
		}

		$objApplicant				= $this->fetchApplicant( $this->m_objDatabase );

		if( false == valObj( $objApplicant, CApplicant::class ) ) {
			return '';
		}

		$objApplicationDuplicate	= $this->fetchApplicationDuplicate( $this->m_objDatabase );

		if( true == valObj( $objApplicationDuplicate, CApplicationDuplicate::class ) ) {
			$objApplication = $objApplicationDuplicate;
		}

		$objProperty				= $objApplication->getOrFetchProperty( $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return '';
		}

		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'HIDE_BUILDING_UNIT_FLOOR_PLAN_BED_INFO', $objProperty->getId(), $this->getCid(), $this->m_objDatabase );

		$objPropertyUnit			= ( false == valObj( $objApplication, CApplicationDuplicate::class ) ) ? $objApplication->getOrFetchPropertyUnit( $this->m_objDatabase ) : $objApplication->fetchPropertyUnit( $this->m_objDatabase );
		$objUnitSpace				= ( false == valObj( $objApplication, CApplicationDuplicate::class ) ) ? $objApplication->getOrFetchUnitSpace( $this->m_objDatabase ) : $objApplication->fetchUnitSpace( $this->m_objDatabase );

		$objPropertyBuilding		= ( true == valObj( $objPropertyUnit, CPropertyUnit::class, 'PropertyBuildingId' ) ) ? $objPropertyUnit->fetchPropertyBuilding( $this->m_objDatabase ) : NULL;

		$strOutput = '';
		if( ( 0 < $objApplication->getDesiredBedrooms() || 0 < $objApplication->getDesiredBathrooms() ) && false == valObj( $objPropertyPreference, CPropertyPreference::class ) ) {
			$intBeds = $objApplication->getDesiredBedrooms() ?? 0;
			$strBedText = ( 1 < $intBeds ) ? __( '{%d,0} Beds', [ $intBeds ] ) : __( '{%d,0} Bed', [ $intBeds ] );

			$intBaths = $objApplication->getDesiredBathrooms() ?? 0;
			$strBathText = ( 1 < $intBaths ) ? __( '{%d,0} Baths', [ $intBeds ] ) : __( '{%d,0} Bath', [ $intBeds ] );

			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Bed & Bath</span><br><span style="display: inline-block;">{%s,0}, {%s,1}</span></p>', [ $strBedText, $strBathText ] );
		}

		if( true == valObj( $objPropertyUnit, CPropertyUnit::class ) && false == $objPropertyUnit->getRemotePrimaryKey() && 0 < $objPropertyUnit->getUnitNumber() ) {
			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Unit Number</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $objPropertyUnit->getUnitNumber() ] );
		}

		if( false == is_null( $objApplicant->getCurrentPostalCode() ) ) {
			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Zip/Postal Code</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $objApplicant->getCurrentPostalCode() ] );
		}

		if( false == is_null( $objApplicant->getHasPets() ) ) {
			$strText = ( 0 == $objApplicant->getHasPets() ) ? __( 'No' ) : __( 'Yes' );
			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Has Pets</span><br><span style="display: inline-block;">{%s,0}</span></p>
							', [ $strText ] );
		}

		if( true == valObj( $objPropertyBuilding, CPropertyBuilding::class ) && false == valObj( $objPropertyPreference, CPropertyPreference::class ) ) {
			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Building</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $objPropertyBuilding->getBuildingName() ] );
		}

		if( true == valObj( $objPropertyUnit, CPropertyUnit::class ) && false == valObj( $objPropertyPreference, CPropertyPreference::class ) ) {
			$strText = $objPropertyUnit->getMarketingUnitNumber() ?? $objPropertyUnit->getUnitNumber();
			if( true == valObj( $objUnitSpace, CUnitSpace::class ) && 1 < $objPropertyUnit->getUnitCounts() && 0 < $objUnitSpace->getSpaceNumber() ) {
				$strText .= ' - ' . $objUnitSpace->getSpaceNumber();
			}

			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Marketing Apartment</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $strText ] );
		}

		return $strOutput;
	}

	public function getPrimediaCompanyUser() {
		$strOutput = '';
		$objClient = $this->fetchClient( $this->m_objDatabase );
		$objApplication				= $this->getOrFetchApplication( $this->m_objDatabase );
		$objApplicationDuplicate = $this->fetchApplicationDuplicate( $this->m_objDatabase );
		if( true == valObj( $objApplicationDuplicate, CApplicationDuplicate::class ) ) {
			$objApplication = $this->fetchApplicationDuplicate( $this->m_objDatabase );
		}
		$objProperty				= $objApplication->getOrFetchProperty( $this->m_objDatabase );
		$objPrimediaPropertyManagerUser				= $this->fetchPrimediaPropertyManagerUser( $objClient, $objProperty, $this->m_objDatabase );

		if( false == is_null( $objPrimediaPropertyManagerUser ) && ( strtotime( $objPrimediaPropertyManagerUser->getLastLogin() ) == strtotime( $objPrimediaPropertyManagerUser->getCreatedOn() ) ) ) {
			$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'http://';
			$strRwxSuffix			= ( true == defined( CONFIG_RWX_LOGIN_SUFFIX ) ) ? CONFIG_RWX_LOGIN_SUFFIX : '.entrata.com';
			$strResidentWorksUrl 	= $strSecureHostPrefix . $objClient->getRwxDomain() . $strRwxSuffix;

			$strOutput = __( '
								<tr>
									<td style="padding: 20px 30px; width: 600px; word-wrap:break-word;" colspan="2">
										<p class="title" style="margin-top: 10px; font-size: 24px; font-weight: 600; margin: 0 0 10px;">
											<p class="deets" style="margin: 15px 0; font-size: 16px;">Login Credentials</p>
											<p class="deets" style="margin: 15px 0; font-size: 16px;">
												<span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Login URL</span>
												<br>
												<span style="display: inline-block;"><a href="{%s,0}" target=\'_blank\'>{%s,1}</a></span>
											</p>
											<p class="deets" style="margin: 15px 0; font-size: 16px;">
												<span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Username</span>
												<br>
												<span style="display: inline-block;">{%s,2}</span>
											</p>
											<p class="deets" style="margin: 15px 0; font-size: 16px;">
												<span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Password</span>
												<br>
												<span style="display: inline-block;">{%s,3}</span>
											</p>
			 							</p>
									</td>
								</tr>
							', [ $strResidentWorksUrl, $strResidentWorksUrl, $objPrimediaPropertyManagerUser->getUsername(), $objPrimediaPropertyManagerUser->getPasswordEncrypted() ] );
		}

		return $strOutput;
	}

	public function getOneTimeScheduledCharges() {
		$strOneTimeLeaseScheduledCharges = $this->loadScheduledChargesByArTriggerIds( CArTrigger::$c_arrintNotificationLetterOneTimeTrigggers );
		return ( false == valStr( $strOneTimeLeaseScheduledCharges ) ) ? NULL : $strOneTimeLeaseScheduledCharges;
	}

	public function getRecurringScheduledCharges() {
		$strRecurringLeaseScheduledCharges = $this->loadScheduledChargesByArTriggerIds( CArTrigger::$c_arrintRecurringArTriggers );
		return ( false == valStr( $strRecurringLeaseScheduledCharges ) ) ? NULL : $strRecurringLeaseScheduledCharges;
	}

	public function getTotalScheduledCharges() {
		$strLeaseScheduledChargesTotal = $this->loadScheduledChargesByArTriggerIds( array_merge( CArTrigger::$c_arrintRecurringArTriggers, CArTrigger::$c_arrintNotificationLetterOneTimeTrigggers ), true );
		return ( false == valStr( $strLeaseScheduledChargesTotal ) ) ? NULL : $strLeaseScheduledChargesTotal;
	}

	public function loadScheduledChargesByArTriggerIds( $arrintArTriggerIds, $boolReturnTotalScheduledChargesAmountOnly = false ) {
		$strLeaseScheduledCharges = '';
		$objApplicant                        = $this->getOrFetchApplicant( $this->m_objDatabase, false );
		$objApplication                      = $this->getOrFetchApplication( $this->m_objDatabase );
		$objLease                            = $objApplication->getOrFetchLease( $this->m_objDatabase );
		if( false == valObj( $objLease, 'CLease' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return $strLeaseScheduledCharges;
		}

		$objLeaseCustomer                    = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $objApplicant->getCustomerId(), $objLease->getId(), $this->getCid(), $this->m_objDatabase );
		if( true == valObj( $objLeaseCustomer, CLeaseCustomer::class ) ) {
			$strLeaseScheduledCharges = $objLeaseCustomer->loadScheduledChargesByArTriggerIds( $arrintArTriggerIds, $boolReturnTotalScheduledChargesAmountOnly );
		}

		return $strLeaseScheduledCharges;

	}

	public function fetchFileAssociationsByFileTypeSystemCodeByDocumentSubTypeId( $strSystemCode, $intDocumentSubTypeId, $objDatabase, $boolIsExcludeDeletedFileAssociations = false ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantByApplicationIdByCustomerTypeIdByFileTypeSystemCodeByDocumentSubTypeIdByCid( $this->getApplicantId(), $this->getApplicationId(), $this->getCustomerTypeId(), $strSystemCode, $intDocumentSubTypeId, $this->getCid(), $objDatabase, $boolIsExcludeDeletedFileAssociations );
	}

	public function getScheduledAppointmentRescheduleOrCancelLink() {
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) ) {
			return NULL;
		}
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $objApplication->getCid(), $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchScheduledAppointmentEventByLeaseIdByLeaseIntervalIdByCid( $objApplication->getLeaseId(), $objApplication->getLeaseIntervalId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objEvent, 'CEvent' ) ) {
			return NULL;
		}
		$strBaseUrl = $objApplication->generateWebsiteUrl( $this->m_objDatabase ) . $objProperty->getOrFetchSeoCity( $this->m_objDatabase ) . '/' . $objProperty->getSeoPropertyName() . '/';
		$strRescheduleUriPart          = 'reschedule-tour/1/application-id/' . $this->getApplicationId() . '/applicant-id/' . $this->getApplicantId() . '/event-id/' . $objEvent->getId();
		$strRescheduleEncryptedURLPart = \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( $strRescheduleUriPart, CONFIG_SODIUM_KEY_ID );
		$strRescheduleUrl = $strBaseUrl . 'guest-card/tour/' . $strRescheduleEncryptedURLPart;

		$strCancelUriPart          = 'cancel-tour/1/application-id/' . $this->getApplicationId() . '/applicant-id/' . $this->getApplicantId() . '/event-id/' . $objEvent->getId();
		$strCancelEncryptedURLPart = \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( $strCancelUriPart, CONFIG_SODIUM_KEY_ID );
		$strCancelUrl = $strBaseUrl . 'guest-card/tour/' . $strCancelEncryptedURLPart;

		$strContent = __( ' Please click here to <a target="_blank" href="' . $strRescheduleUrl . '" border="0"> Reschedule </a> or <a target="_blank" href="' . $strCancelUrl . '" border="0"> Cancel </a> tour' );

		return $strContent;
	}

	public function getAllOrderedActiveCustomerPhoneNumbers() {
		$strContent = NULL;

		if( false == valId( $this->getCid() ) && false == valId( $this->getApplicantId() ) ) {
			return $strContent;
		}

		try {
			$objApplicant = \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );

			if( false == valObj( $objApplicant, 'CApplicant' ) ) {
				return $strContent;
			}

			$arrmixCustomerPhoneNumbers = ( array ) \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchOrderedCustomerPhoneNumbersByCustomerIdByCid( $objApplicant->getCustomerId(), $this->getCid(), $this->m_objDatabase );
			$strPhoneTypeName = NULL;

			foreach( $arrmixCustomerPhoneNumbers as $arrmixCustomerPhoneNumber ) {
				if( true == $arrmixCustomerPhoneNumber->getIsPrimary() ) {
					$strPhoneTypeName = 'Primary Phone';
				} else {
					$strPhoneTypeName = CPhoneNumberType::$c_arrstrValidResidentPhoneNumberTypes[$arrmixCustomerPhoneNumber->getPhoneNumberTypeId()] . ' Phone';
				}

				if( false == valObj( next( $arrmixCustomerPhoneNumbers ), 'CCustomerPhoneNumber' ) ) {
					$strContent .= '<div style="width:100%;float: left;">';
				} else {
					$strContent .= '<div style="width:100%;float: left;border-bottom: 1px solid #E6E7E8;">';
				}

				$strContent .= '<div style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;width:18%; float:left">' . $strPhoneTypeName . '</div>';
				$strContent .= '<div style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;font-weight: 500;width:30%; float:left">' . __( '{%h,0,STANDARD}', $arrmixCustomerPhoneNumber->getPhoneNumber() ) . '</div>';
				$strContent .= '<div style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;font-weight: 500;width:auto;float:left;"></div>';
				$strContent .= '</div>';
			}
		} catch( Exception $objException ) {
			$strContent = NULL;
		}

		return $strContent;
	}

	public function getRentalApplicationInvitationLoginContent() {
		$objApplicant   = \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return NULL;
		}

		$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$strContent = '<table cellpadding="0" cellspacing="0" border="0" style="width:550px; margin-top:20px;font:normal 13px/18px arial; color:#000000;"><tr><td>';
		$strCreatePwdLink = '';

		$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $objApplication->getPropertyId();

		if( true == is_null( $objApplicant->getPasswordEncrypted() ) || 0 == strlen( $objApplicant->getPasswordEncrypted() ) ) {
			$strEncryptedUniqueKey = \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );

			$objOneTimeLink = new COneTimeLink();
			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $this->getApplicantId(), 'key_encrypted' => $strEncryptedUniqueKey ] );
			$this->m_objDatabase->begin();

			if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert one time link.' ), NULL ) );
				$this->m_objDatabase->rollback();
			}

			$this->m_objDatabase->commit();
			$strCreatePwdLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/create_applicant_password/property[id]/' . ( ( true == is_null( $objProperty->getPropertyId() ) ) ? $objProperty->getId() : $objProperty->getPropertyId() ) . '/applicant[key]/' . urlencode( $strEncryptedUniqueKey ) . '/applicant[id]/' . $this->getApplicantId() . '/is_primary_applicant/' . ( ( $this->getCustomerTypeId() == \CCustomerType::PRIMARY ) ? 1 : 0 ) . '/application[id]/' . $this->getApplicationId() . '/';
		}

		if( true == valStr( $strCreatePwdLink ) ) {
			$strContent .= __( "<tr>
							<td>&#160;</td>
						</tr>
						<tr>
							<td>
								<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"font:normal 13px/18px arial; color:#000000;\">

										<tr>
											<td colspan=\"2\"></td>
										</tr>
										<tr>
											<td ><b>Login ID :</b></td>
											<td>{%s,0}</td>
										</tr>
										<tr>
											<td ><b>Password :</b>&nbsp;</td>
											<td style=\"align:left;\">&nbsp;Please <a href=\"{%s,1}\"  target=\"_blank\">
												<u><b>click here</b></u></a> to create a password and begin your online application.
											</td>
										</tr>
								</table>
							</td>
						</tr>", [ $objApplicant->getUsername(), $strCreatePwdLink ] );
		} else {
			$strContent .= __( "<tr>
							<td colspan=\"2\">&#160;</td>
						</tr>
						<tr>
							<td colspan=\"2\"><a href=\"{%s,0}\"  target=\"_blank\"><u><b>Click Here</b></u></a> access the login page.
								</td>
						</tr>", [ $strApplicationUrl ] );
		}

		$strContent .= '</table>';

		return $strContent;
	}

}

?>
