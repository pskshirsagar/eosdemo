<?php

class CReportDocument extends CBaseReportDocument {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportHistoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportDocumentTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>