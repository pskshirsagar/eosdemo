<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeePolicies
 * Do not add any new functions to this class.
 */

class CApPayeePolicies extends CBaseApPayeePolicies {

	public static function fetchMaxApPayeePoliciesByCidsByApPayeeIds( $arrintCids, $arrintApPayeeIds, $objClientDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						ap_payee_id,
						ap_payee_policy_type_id,
						insurance_end_date,
						max( liability_limit ) as current_amount
					FROM
						ap_payee_policies
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
					GROUP BY
						ap_payee_policy_type_id,
						insurance_end_date,
						ap_payee_id';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>