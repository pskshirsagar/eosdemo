<?php

class CCustomPolicyReviewLog extends CBaseCustomPolicyReviewLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentInsurancePolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVerified() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyReviewStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function buildFileData( $objFile, $strInputTypeFileName = 'policy_file_name', $objDatabase ) {

		if( true == is_null( $objFile->getId() ) || false == is_numeric( $objFile->getId() ) ) {
			$objFile->setId( $objFile->fetchNextId( $objDatabase ) );
		}

		$objFile->setTitle( $_FILES[$strInputTypeFileName]['name'] );

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = 'policy_document_' . $objFile->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsgs( __( 'Invalid file extension.' ) );
			return false;
		}

		$objFile->setFileExtensionId( $objFileExtension->getId() );

		$strPath = $objFile->getId() . '/policy_documents/';
		$objFile->setFilePath( $strPath );

		$objFile->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
		$objFile->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		$objFile->setFileName( $strFileName );
		$objFile->setFileType( $objFileExtension->getMimeType() );

		return $objFile;
	}

	public function buildFileAssociation(  $intLeaseId, $intFileId, $intUserId, $intCustomerId, $intApplicationId, $objClient ) {

		if( false == valId( $intLeaseId ) || false == valId( $intFileId ) || false == valId( $intUserId ) || false == valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$objFileAssociation = $objClient->createFileAssociation();
		$objFileAssociation->setLeaseId( $intLeaseId );
		$objFileAssociation->setFileId( $intFileId );
		$objFileAssociation->setCustomerId( $intCustomerId );
		$objFileAssociation->setApplicationId( $intApplicationId );

		return $objFileAssociation;
	}

}
?>