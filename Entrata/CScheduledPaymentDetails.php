<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentDetails
 * Do not add any new functions to this class.
 */

class CScheduledPaymentDetails extends CBaseScheduledPaymentDetails {

	/**
	 * Fetch Functions
	 */

	public static function fetchScheduledPaymentDetailByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM scheduled_payment_details WHERE scheduled_payment_id = ' . ( int ) $intScheduledPaymentId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchScheduledPaymentDetail( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentDetailsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						spd.*
					FROM
						scheduled_payments sp
						JOIN scheduled_payment_details spd ON ( sp.id = spd.scheduled_payment_id AND sp.cid = spd.cid )
					WHERE
						sp.cid = ' . ( int ) $intCid . '
						AND sp.customer_id = ' . ( int ) $intCustomerId . '';

		return self::fetchScheduledPaymentDetails( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentDetailCustomerEmailByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT billto_email_address FROM scheduled_payment_details WHERE scheduled_payment_id = ' . ( int ) $intScheduledPaymentId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchColumn( $strSql, 'billto_email_address', $objDatabase );
	}

}
?>