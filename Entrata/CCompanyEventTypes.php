<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEventTypes
 * Do not add any new functions to this class.
 */

class CCompanyEventTypes extends CBaseCompanyEventTypes {

	public static function fetchCompanyEventTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchObjects( $strSql, 'CCompanyEventType', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCompanyEventType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCompanyEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedCompanyEventTypes( $intCid, $objDatabase ) {
		return self::fetchCompanyEventTypes( 'SELECT * FROM company_event_types WHERE cid IN( ' . ( int ) $intCid . ', 1 ) AND is_published = 1 and deleted_by IS NULL ORDER BY id', $objDatabase );
	}

	public static function fetchDuplicateCompanyEventTypeCountByIdByNameByCid( $intCompanyEventTypeId, $strCompanyEventtypeName, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE
								cid = ' . ( int ) $intCid . '
								AND lower(name)=\'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strCompanyEventtypeName ) ) ) . '\' and deleted_by IS NULL';

		if( true == is_numeric( $intCompanyEventTypeId ) ) {
			$strWhereSql .= ' AND id != ' . ( int ) $intCompanyEventTypeId;
		}
		return self::fetchCompanyEventTypeCount( $strWhereSql, $objDatabase );
	}

	public static function fetchCompanyEventTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEventTypes( sprintf( 'SELECT * FROM company_event_types WHERE cid = %d or cid = %d and is_published = 1 and deleted_by IS NULL', ( int ) 1, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventType( sprintf( 'SELECT * FROM company_event_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

}
?>