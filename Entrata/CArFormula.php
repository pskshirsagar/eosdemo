<?php

class CArFormula extends CBaseArFormula {

	const FIXED_AMOUNT						= 1;
	const PER_ITEM							= 2;
	const PER_ITEM_WITH_VOLUME_PRICING		= 3;
	const PER_ITEM_WITH_TIERED_PRICING		= 4;
	const PERCENT_OF_CHARGE_CODE			= 5;
	const PERCENT_OF_CHARGE_CODE_GROUP		= 6;
	const PERCENT_OF_CHARGE_CODE_TYPE		= 7;
	const MARKET_TIMES_PERCENT				= 8;
	const CURRENT_TIMES_PERCENT				= 9;
	const MARKET_PLUS_FIXED_AMOUNT			= 10;
	const CURRENT_PLUS_FIXED_AMOUNT			= 11;
	const PERCENT_OF_SALES					= 12;
	const FULL_BAH							= 13;
	const FIXED_BAH							= 14;
	const FIXED_BAH_ADJUSTMENT				= 15;

	public static $c_arrintPercentageFormulas = [ self::PERCENT_OF_CHARGE_CODE, self::PERCENT_OF_CHARGE_CODE_GROUP, self::PERCENT_OF_CHARGE_CODE_TYPE, self::MARKET_TIMES_PERCENT, self::CURRENT_TIMES_PERCENT ];

	public static $c_arrintArCodeBasedFormulas = [ self::PERCENT_OF_CHARGE_CODE, self::PERCENT_OF_CHARGE_CODE_GROUP, self::PERCENT_OF_CHARGE_CODE_TYPE ];

	public static $c_arrintMarketAndCurrentFormulas = [ self::MARKET_TIMES_PERCENT, self::CURRENT_TIMES_PERCENT, self::MARKET_PLUS_FIXED_AMOUNT, self::CURRENT_PLUS_FIXED_AMOUNT ];

	public static $c_arrintLeaseIntervalBasedFormulas = [ self::FIXED_AMOUNT, self::PER_ITEM, self::FIXED_BAH, self::FIXED_BAH_ADJUSTMENT ];

	public static $c_arrintManualEditRenewalFormulas = [ self::CURRENT_TIMES_PERCENT, self::CURRENT_PLUS_FIXED_AMOUNT ];

	public static $c_arrintMarketPercentRenewalFormulas = [ self::MARKET_TIMES_PERCENT, self::MARKET_PLUS_FIXED_AMOUNT ];

	public static $c_arrintNonFlatFeeArFormulas = [
		self::PER_ITEM,
		self::PER_ITEM_WITH_VOLUME_PRICING,
		self::PER_ITEM_WITH_TIERED_PRICING,
		self::PERCENT_OF_CHARGE_CODE,
		self::PERCENT_OF_CHARGE_CODE_GROUP,
		self::PERCENT_OF_CHARGE_CODE_TYPE,
		self::MARKET_TIMES_PERCENT,
		self::CURRENT_TIMES_PERCENT,
		self::MARKET_PLUS_FIXED_AMOUNT,
		self::CURRENT_PLUS_FIXED_AMOUNT,
		self::PERCENT_OF_SALES,
		self::FULL_BAH,
		self::FIXED_BAH,
		self::FIXED_BAH_ADJUSTMENT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRenewalOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>