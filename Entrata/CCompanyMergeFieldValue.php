<?php

class CCompanyMergeFieldValue extends CBaseCompanyMergeFieldValue {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMergeFieldId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;

		if( false == valStr( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Error: Please use a valid company merge field value name for ' . $this->getValue() ) );
		}

		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valValue();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intUserId );
    	$this->setDeletedOn( ' NOW() ' );
    	$this->setUpdatedBy( $intUserId );
    	$this->setUpdatedOn( ' NOW() ' );

    	if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
    		return true;
    	}
    }

}
?>