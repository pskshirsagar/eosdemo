<?php

class CApplicationStageStatus extends CBaseApplicationStageStatus {

	protected $m_intOccupancyTypeId;
	protected $m_arrstrSpecialApplicationStageStatus;
	// Prospect
	const PROSPECT_STAGE1_COMPLETED	= 1;
	const PROSPECT_STAGE1_ON_HOLD	= 2;
	const PROSPECT_STAGE1_CANCELLED	= 3;

	const PROSPECT_STAGE2_STARTED	= 4;
	const PROSPECT_STAGE2_COMPLETED	= 5;
	const PROSPECT_STAGE2_APPROVED	= 6;
	const PROSPECT_STAGE2_ON_HOLD	= 7;
	const PROSPECT_STAGE2_CANCELLED	= 8;

	const PROSPECT_STAGE3_STARTED				= 9;
	const PROSPECT_STAGE3_PARTIALLY_COMPLETED 	= 10;
	const PROSPECT_STAGE3_COMPLETED				= 11;
	const PROSPECT_STAGE3_APPROVED				= 12;
	const PROSPECT_STAGE3_ON_HOLD				= 13;
	const PROSPECT_STAGE3_CANCELLED				= 14;

	const PROSPECT_STAGE4_STARTED				= 15;
	const PROSPECT_STAGE4_PARTIALLY_COMPLETED	= 16;
	const PROSPECT_STAGE4_COMPLETED				= 17;
	const PROSPECT_STAGE4_APPROVED				= 18;
	const PROSPECT_STAGE4_ON_HOLD				= 19;
	const PROSPECT_STAGE4_CANCELLED				= 20;

	// Renewal
	const RENEWAL_STAGE1_COMPLETED	= 21;
	const RENEWAL_STAGE1_APPROVED	= 22;
	const RENEWAL_STAGE1_ON_HOLD	= 23;
	const RENEWAL_STAGE1_CANCELLED	= 24;

	const RENEWAL_STAGE3_STARTED				= 25;
	const RENEWAL_STAGE3_PARTIALLY_COMPLETED 	= 26;
	const RENEWAL_STAGE3_COMPLETED				= 27;
	const RENEWAL_STAGE3_APPROVED				= 28;
	const RENEWAL_STAGE3_ON_HOLD				= 29;
	const RENEWAL_STAGE3_CANCELLED				= 30;

	const RENEWAL_STAGE4_STARTED				= 31;
	const RENEWAL_STAGE4_PARTIALLY_COMPLETED	= 32;
	const RENEWAL_STAGE4_COMPLETED				= 33;
	const RENEWAL_STAGE4_APPROVED				= 34;
	const RENEWAL_STAGE4_ON_HOLD				= 35;
	const RENEWAL_STAGE4_CANCELLED				= 36;

	// Lease Modification
	const LEASE_MODIFICATION_STAGE3_STARTED					= 37;
	const LEASE_MODIFICATION_STAGE3_PARTIALLY_COMPLETED 	= 38;
	const LEASE_MODIFICATION_STAGE3_COMPLETED				= 39;
	const LEASE_MODIFICATION_STAGE3_APPROVED				= 40;
	const LEASE_MODIFICATION_STAGE3_ON_HOLD					= 41;
	const LEASE_MODIFICATION_STAGE3_CANCELLED				= 42;

	const LEASE_MODIFICATION_STAGE4_STARTED					= 43;
	const LEASE_MODIFICATION_STAGE4_PARTIALLY_COMPLETED		= 44;
	const LEASE_MODIFICATION_STAGE4_COMPLETED				= 45;
	const LEASE_MODIFICATION_STAGE4_APPROVED				= 46;
	const LEASE_MODIFICATION_STAGE4_ON_HOLD					= 47;
	const LEASE_MODIFICATION_STAGE4_CANCELLED				= 48;

	// Transfer
	const TRANSFER_STAGE3_STARTED				= 49;
	const TRANSFER_STAGE3_PARTIALLY_COMPLETED 	= 50;
	const TRANSFER_STAGE3_COMPLETED				= 51;
	const TRANSFER_STAGE3_APPROVED				= 52;
	const TRANSFER_STAGE3_ON_HOLD				= 53;
	const TRANSFER_STAGE3_CANCELLED				= 54;

	const TRANSFER_STAGE4_STARTED				= 55;
	const TRANSFER_STAGE4_PARTIALLY_COMPLETED	= 56;
	const TRANSFER_STAGE4_COMPLETED				= 57;
	const TRANSFER_STAGE4_APPROVED				= 58;
	const TRANSFER_STAGE4_ON_HOLD				= 59;
	const TRANSFER_STAGE4_CANCELLED				= 60;

	const EXTENSION_STAGE3_STARTED				= 61;
	const EXTENSION_STAGE3_PARTIALLY_COMPLETED	= 62;
	const EXTENSION_STAGE3_COMPLETED			= 63;
	const EXTENSION_STAGE3_APPROVED				= 64;
	const EXTENSION_STAGE3_CANCELLED			= 65;

	const EXTENSION_STAGE4_STARTED				= 66;
	const EXTENSION_STAGE4_PARTIALLY_COMPLETED	= 67;
	const EXTENSION_STAGE4_COMPLETED			= 68;
	const EXTENSION_STAGE4_APPROVED				= 69;
	const EXTENSION_STAGE4_CANCELLED			= 70;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	// Application Status Ids
	public static $c_arrintLockQuotedRentApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintQuotedApplyButtonApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintCombineLeadApplicationStageStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE                => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintRenewalGroupStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintSyncRenewalGroupStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 			    => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintNonActiveApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintDashboardNonActiveApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::APPROVED ]
	];

	public static $c_arrintCustomLeadApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintUnfulfilledNonActiveApplicationStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ]
	];

	public static $c_arrintScreeningPermissionApplicationStatusIds = [
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::COMPLETED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintValidPaymentStepCompletedStatusIds = [
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 				=> [ CApplicationStatus::APPROVED ]
	];

	public static $c_arrintSmsNotificationOnApplicationStatusIds = [
		CApplicationStage::APPLICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED, CApplicationStatus::CANCELLED ],
		CApplicationStage::LEASE 		=> [ CApplicationStatus::PARTIALLY_COMPLETED ]
	];

	public static $c_arrintValidApplicantStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ]
	];

	public static $c_arrintResidentApplicantStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintActiveLeasePendingModificationStatusIds = [
		CApplicationStage::APPLICATION 	=> [ CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 		=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintApplicatinoCompletedStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintCompletedApplicationStatusIds = [
		CApplicationStage::APPLICATION 	=> [ CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 		=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintValidPostApplicationCompletedStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE       => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintActiveApplicationStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE       => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
	];

	public static $c_arrintApprovedApplicationStatusIds = [
		CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ]
	];

	public static $c_arrintLockDownApplicationStatusIds = [
		CApplicationStage::APPLICATION 	=> [ CApplicationStatus::APPROVED ],
		CApplicationStage::LEASE 		=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintLeaseGroupStatusIds = [
		CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public static $c_arrintLeaseSignPendingStatusIds = [
		CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ]
	];

	public static $c_arrintContactPointsInactiveStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
	];

	public static $c_arrintRenewalactiveStatusIds = [
		CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED, CApplicationStatus::CANCELLED ],
		CApplicationStage::APPLICATION 	=> [ CApplicationStatus::STARTED ]
	];

	public static $c_arrmixSubsidizedApplicationStatusIds = [
		self::PROSPECT_STAGE1_COMPLETED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Complete' ],
		self::PROSPECT_STAGE1_ON_HOLD => [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Archived' ],
		self::PROSPECT_STAGE1_CANCELLED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Cancelled' ],
		self::PROSPECT_STAGE3_STARTED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Started' ],
		self::PROSPECT_STAGE3_COMPLETED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Complete' ],
		self::PROSPECT_STAGE3_APPROVED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Processed' ],
		self::PROSPECT_STAGE4_APPROVED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Finalized' ],
		self::PROSPECT_STAGE3_CANCELLED => [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Cancelled' ]
	];

	public function getSubsidizedApplicationStatusIds() {
		return [
			self::PROSPECT_STAGE1_COMPLETED => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_ELIGIBILITY ), 'status' => __( 'Complete' ) ],
			self::PROSPECT_STAGE1_ON_HOLD   => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_ELIGIBILITY ), 'status' => __( 'Archived' ) ],
			self::PROSPECT_STAGE1_CANCELLED => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_ELIGIBILITY ), 'status' => __( 'Cancelled' ) ],
			self::PROSPECT_STAGE3_STARTED   => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_CERTIFICATION ), 'status' => __( 'Started' ) ],
			self::PROSPECT_STAGE3_COMPLETED => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_CERTIFICATION ), 'status' => __( 'Complete' ) ],
			self::PROSPECT_STAGE3_APPROVED  => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_CERTIFICATION ), 'status' => __( 'Processed' ) ],
			self::PROSPECT_STAGE4_APPROVED  => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_CERTIFICATION ), 'status' => __( 'Finalized' ) ],
			self::PROSPECT_STAGE3_CANCELLED => [ 'stage' => CApplicationStage::createService()->getAffordableApplicationStages( CApplicationStage::CUSTOM_STAGE_CERTIFICATION ), 'status' => __( 'Cancelled' ) ]
		];
	}

	public static $c_arrmixSubsidizedRenewalStatusIds = [
		self::RENEWAL_STAGE1_COMPLETED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Complete' ],
		self::RENEWAL_STAGE1_ON_HOLD	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Archived' ],
		self::RENEWAL_STAGE1_CANCELLED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_ELIGIBILITY, 'status' => 'Cancelled' ],

		self::RENEWAL_STAGE3_STARTED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Started' ],
		self::RENEWAL_STAGE3_COMPLETED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Complete' ],
		self::RENEWAL_STAGE3_APPROVED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Processed' ],
		self::RENEWAL_STAGE3_CANCELLED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Cancelled' ],

		self::RENEWAL_STAGE4_APPROVED	=> [ 'stage' => CApplicationStage::CUSTOM_STAGE_CERTIFICATION, 'status' => 'Finalized' ]
	];

	// Application Stage Status Ids
	public static $c_arrintProspectNonActiveApplicationStageStatusIds = [
		self::PROSPECT_STAGE1_ON_HOLD,
		self::PROSPECT_STAGE1_CANCELLED,
		self::PROSPECT_STAGE2_ON_HOLD,
		self::PROSPECT_STAGE2_CANCELLED,
		self::PROSPECT_STAGE3_ON_HOLD,
		self::PROSPECT_STAGE3_CANCELLED,
		self::PROSPECT_STAGE4_APPROVED
	];

	public static $c_arrintDashboardProspectNonActiveApplicationStageStatusIds = [
	    self::PROSPECT_STAGE1_ON_HOLD,
	    self::PROSPECT_STAGE1_CANCELLED,
		self::PROSPECT_STAGE2_ON_HOLD,
		self::PROSPECT_STAGE2_CANCELLED,
	    self::PROSPECT_STAGE3_ON_HOLD,
	    self::PROSPECT_STAGE3_CANCELLED,
	    self::PROSPECT_STAGE4_APPROVED
	];

	public static $c_arrintNonActiveApplicationStageStatusIds 	= [
		self::PROSPECT_STAGE1_ON_HOLD,
		self::PROSPECT_STAGE2_ON_HOLD,
		self::PROSPECT_STAGE3_ON_HOLD,
		self::RENEWAL_STAGE3_ON_HOLD,
		self::LEASE_MODIFICATION_STAGE3_ON_HOLD,
		self::TRANSFER_STAGE3_ON_HOLD,
		self::PROSPECT_STAGE1_CANCELLED,
		self::PROSPECT_STAGE2_CANCELLED,
		self::PROSPECT_STAGE3_CANCELLED,
		self::RENEWAL_STAGE1_CANCELLED,
		self::RENEWAL_STAGE3_CANCELLED,
		self::LEASE_MODIFICATION_STAGE3_CANCELLED,
		self::TRANSFER_STAGE3_CANCELLED,
		self::PROSPECT_STAGE4_APPROVED,
		self::RENEWAL_STAGE4_APPROVED,
		self::LEASE_MODIFICATION_STAGE4_APPROVED,
		self::TRANSFER_STAGE4_APPROVED
	];

	public static $c_arrintValidPostApplicationCompletedStageStatusIds = [
		self::PROSPECT_STAGE3_PARTIALLY_COMPLETED,
		self::PROSPECT_STAGE3_COMPLETED,
		self::PROSPECT_STAGE3_APPROVED,
		self::PROSPECT_STAGE4_STARTED,
		self::PROSPECT_STAGE4_PARTIALLY_COMPLETED,
		self::PROSPECT_STAGE4_COMPLETED,
		self::PROSPECT_STAGE4_APPROVED
	];

	// Application Stage Statuses
	public static $c_arrintBulkUnitAssignmentOtherStageStatuses = [
		self::RENEWAL_STAGE3_STARTED,
		self::RENEWAL_STAGE3_PARTIALLY_COMPLETED,
		self::RENEWAL_STAGE3_COMPLETED,
		self::RENEWAL_STAGE3_APPROVED,
		self::TRANSFER_STAGE3_APPROVED,
		self::RENEWAL_STAGE4_STARTED,
		self::TRANSFER_STAGE4_STARTED,
		self::RENEWAL_STAGE4_PARTIALLY_COMPLETED,
		self::TRANSFER_STAGE4_PARTIALLY_COMPLETED,
		self::RENEWAL_STAGE4_COMPLETED,
		self::TRANSFER_STAGE4_COMPLETED,
		self::RENEWAL_STAGE4_APPROVED,
		self::TRANSFER_STAGE4_APPROVED
	];

	public static $c_arrstrCombineLeadApplicationStageStatuses = [
		self::PROSPECT_STAGE1_COMPLETED				=> 'Guest Card Completed',
		self::PROSPECT_STAGE2_STARTED 				=> 'Pre-Qualification Started',
		self::PROSPECT_STAGE3_COMPLETED 			=> 'Pre-Qualification Completed',
		self::PROSPECT_STAGE3_APPROVED 				=> 'Pre-Qualification Approved',
		self::PROSPECT_STAGE3_STARTED 				=> 'Application Started',
		self::PROSPECT_STAGE3_PARTIALLY_COMPLETED 	=> 'Application Partially Completed',
		self::PROSPECT_STAGE3_COMPLETED 			=> 'Application Completed',
		self::PROSPECT_STAGE3_APPROVED 				=> 'Application Approved'
	];

	public static $c_arrstrNonActiveProspectStageStatuses = [
		self::PROSPECT_STAGE1_CANCELLED				=> 'Guest Card Cancelled',
		self::PROSPECT_STAGE1_ON_HOLD				=> 'Guest Card Archived',
		self::PROSPECT_STAGE2_CANCELLED				=> 'Pre-Qualification Cancelled',
		self::PROSPECT_STAGE2_ON_HOLD				=> 'Pre-Qualification Archived',
		self::PROSPECT_STAGE3_CANCELLED				=> 'Application Cancelled',
		self::PROSPECT_STAGE3_ON_HOLD				=> 'Application Archived',
		self::PROSPECT_STAGE4_APPROVED				=> 'Lease Approved'
	];

	public static $c_arrstrActiveProspectStageStatuses = [
		self::PROSPECT_STAGE1_COMPLETED,
		self::PROSPECT_STAGE2_APPROVED,
		self::PROSPECT_STAGE2_COMPLETED,
		self::PROSPECT_STAGE2_STARTED,
		self::PROSPECT_STAGE3_APPROVED,
		self::PROSPECT_STAGE3_COMPLETED,
		self::PROSPECT_STAGE3_PARTIALLY_COMPLETED,
		self::PROSPECT_STAGE3_STARTED,
		self::PROSPECT_STAGE4_COMPLETED,
		self::PROSPECT_STAGE4_STARTED,
		self::PROSPECT_STAGE4_PARTIALLY_COMPLETED
	];

	public static $c_arrintRenewalActiveApplicationStageStatusIds = [
		SELF::RENEWAL_STAGE1_COMPLETED,
		SELF::RENEWAL_STAGE3_STARTED,
		SELF::RENEWAL_STAGE3_PARTIALLY_COMPLETED,
		SELF::RENEWAL_STAGE3_COMPLETED,
		SELF::RENEWAL_STAGE3_APPROVED,
		SELF::RENEWAL_STAGE4_STARTED,
		SELF::RENEWAL_STAGE4_PARTIALLY_COMPLETED,
		SELF::RENEWAL_STAGE4_COMPLETED
	];

	public static $c_arrintRenewalInActiveApplicationStageStatusIds = [
		SELF::RENEWAL_STAGE1_CANCELLED,
		SELF::RENEWAL_STAGE3_CANCELLED,
		SELF::RENEWAL_STAGE4_APPROVED,
		SELF::RENEWAL_STAGE4_CANCELLED
	];

	public static $c_arrstrAllProspectApplicationStageStatuses = [
		self::PROSPECT_STAGE1_COMPLETED				=> 'Guest Card : Completed',
		self::PROSPECT_STAGE1_ON_HOLD				=> 'Guest Card : Archived',
		self::PROSPECT_STAGE1_CANCELLED				=> 'Guest Card : Cancelled',
		self::PROSPECT_STAGE2_STARTED				=> 'Pre-Qualification : Started',
		self::PROSPECT_STAGE2_COMPLETED				=> 'Pre-Qualification : Completed',
		self::PROSPECT_STAGE2_APPROVED				=> 'Pre-Qualification : Approved',
		self::PROSPECT_STAGE2_ON_HOLD				=> 'Pre-Qualification : Archived',
		self::PROSPECT_STAGE2_CANCELLED				=> 'Pre-Qualification : Cancelled',
		self::PROSPECT_STAGE3_STARTED				=> 'Application : Started',
		self::PROSPECT_STAGE3_PARTIALLY_COMPLETED	=> 'Application : Partially Completed',
		self::PROSPECT_STAGE3_COMPLETED				=> 'Application : Completed',
		self::PROSPECT_STAGE3_APPROVED				=> 'Application : Approved',
		self::PROSPECT_STAGE3_ON_HOLD				=> 'Application : Archived',
		self::PROSPECT_STAGE3_CANCELLED				=> 'Application : Cancelled',
		self::PROSPECT_STAGE4_STARTED				=> 'Lease : Started',
		self::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> 'Lease : Partially Completed',
		self::PROSPECT_STAGE4_COMPLETED				=> 'Lease : Completed',
		self::PROSPECT_STAGE4_APPROVED				=> 'Lease : Approved'
	];

	public function getAllProspectApplicationStageStatuses() {

		return [
			self::PROSPECT_STAGE1_COMPLETED				=> __( 'Guest Card : Completed' ),
			self::PROSPECT_STAGE1_ON_HOLD				=> __( 'Guest Card : Archived' ),
			self::PROSPECT_STAGE1_CANCELLED				=> __( 'Guest Card : Cancelled' ),
			self::PROSPECT_STAGE2_STARTED				=> __( 'Pre-Qualification : Started' ),
			self::PROSPECT_STAGE2_COMPLETED				=> __( 'Pre-Qualification : Completed' ),
			self::PROSPECT_STAGE2_APPROVED				=> __( 'Pre-Qualification : Approved' ),
			self::PROSPECT_STAGE2_ON_HOLD				=> __( 'Pre-Qualification : Archived' ),
			self::PROSPECT_STAGE2_CANCELLED				=> __( 'Pre-Qualification : Cancelled' ),
			self::PROSPECT_STAGE3_STARTED				=> __( 'Application : Started' ),
			self::PROSPECT_STAGE3_PARTIALLY_COMPLETED	=> __( 'Application : Partially Completed' ),
			self::PROSPECT_STAGE3_COMPLETED				=> __( 'Application : Completed' ),
			self::PROSPECT_STAGE3_APPROVED				=> __( 'Application : Approved' ),
			self::PROSPECT_STAGE3_ON_HOLD				=> __( 'Application : Archived' ),
			self::PROSPECT_STAGE3_CANCELLED				=> __( 'Application : Cancelled' ),
			self::PROSPECT_STAGE4_STARTED				=> __( 'Lease : Started' ),
			self::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> __( 'Lease : Partially Completed' ),
			self::PROSPECT_STAGE4_COMPLETED				=> __( 'Lease : Completed' ),
			self::PROSPECT_STAGE4_APPROVED				=> __( 'Lease : Approved' )
		];
	}

	public function getProspectApplicationOverviewStageStatuses() {
		$arrstrProspectApplicationOverviewStageStatuses = [
			self::PROSPECT_STAGE1_COMPLETED				=> [ 'status_index' => 1, 'description' => __( 'Guest card complete. You can now send invite to primary applicant to start application' ) ],
			self::PROSPECT_STAGE3_STARTED				=> [ 'status_index' => 2, 'description' => __( 'Application started. Applicant can now complete application online or onsite with permissioned user.' ) ],
			self::PROSPECT_STAGE3_PARTIALLY_COMPLETED	=> [ 'status_index' => 3, 'description' => __( 'Applicant(s) have provided some, but not all required application information.' ) ],
			self::PROSPECT_STAGE3_COMPLETED				=> [ 'status_index' => 4, 'description' => __( 'Applicant(s) have provided all required information. You can now process screening.' ) ],
			self::PROSPECT_STAGE3_APPROVED				=> [ 'status_index' => 5, 'description' => __( 'Screening completed and approved. You can now start the lease process.' ) ],
			self::PROSPECT_STAGE4_STARTED				=> [ 'status_index' => 6, 'description' => __( 'Lease generated, waiting on applicant(s) to sign leases.' ) ],
			self::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> [ 'status_index' => 7, 'description' => __( 'Some applicants have signed leases, but others have not.' ) ],
			self::PROSPECT_STAGE4_COMPLETED				=> [ 'status_index' => 8, 'description' => __( 'All applicants have signed leases. You can now countersign lease.' ) ],
			self::PROSPECT_STAGE4_APPROVED				=> [ 'status_index' => 9, 'description' => __( 'Lease has been countersigned.' ) ]
		];

		return $arrstrProspectApplicationOverviewStageStatuses;
	}

	public function getRenewalOfferSubStatuses() {

		$strRenewalOfferSubStatuses = [
			self::RENEWAL_STAGE3_STARTED                    => __( 'Renewal Offer Started' ),
			self::RENEWAL_STAGE3_PARTIALLY_COMPLETED        => __( 'Renewal Offer Partially Completed' ),
			self::RENEWAL_STAGE3_COMPLETED                  => __( 'Renewal Offer Completed' ),
			self::RENEWAL_STAGE3_APPROVED                   => __( 'Renewal Offer Approved' )
		];

		return $strRenewalOfferSubStatuses;
	}

	public function getRenewalLeaseSubStatuses() {

		$strRenewalLeaseSubStatuses = [
			self::RENEWAL_STAGE4_STARTED                => __( 'Renewal Lease Started' ),
			self::RENEWAL_STAGE4_PARTIALLY_COMPLETED    => __( 'Renewal Lease Partially Completed' ),
			self::RENEWAL_STAGE4_COMPLETED              => __( 'Renewal Lease Completed' )
		];

		return $strRenewalLeaseSubStatuses;
	}

	public function getEntrataCalendarMoveInDisplayStageStatuses() {
		return [
			self::PROSPECT_STAGE3_COMPLETED				=> __( 'Application : Completed' ),
			self::PROSPECT_STAGE3_APPROVED				=> __( 'Application : Approved' ),
			self::PROSPECT_STAGE4_STARTED				=> __( 'Lease : Started' ),
			self::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> __( 'Lease : Partially Completed' ),
			self::PROSPECT_STAGE4_COMPLETED				=> __( 'Lease : Completed' ),
			self::PROSPECT_STAGE4_APPROVED				=> __( 'Lease : Approved' ),
			self::TRANSFER_STAGE4_STARTED				=> __( 'Lease Transfer : Started' ),
			self::TRANSFER_STAGE4_PARTIALLY_COMPLETED	=> __( 'Lease Transfer : Partially Completed' ),
			self::TRANSFER_STAGE4_COMPLETED				=> __( 'Lease Transfer : Completed' ),
			self::TRANSFER_STAGE4_APPROVED				=> __( 'Lease Transfer : Approved' )
		];
	}

	public static $c_arrintDashboardNotProgressingApplicationStageStatusIds = [
		self::PROSPECT_STAGE1_COMPLETED,
		self::PROSPECT_STAGE2_STARTED,
		self::PROSPECT_STAGE2_COMPLETED,
		self::PROSPECT_STAGE2_APPROVED
	];

	public static $c_arrstrLeasingBoardApplicationStageStatuses = [
		self::PROSPECT_STAGE3_STARTED 				=> 'Application Started',
		self::PROSPECT_STAGE3_COMPLETED 			=> 'Application Completed',
		self::PROSPECT_STAGE4_STARTED 				=> 'Lease Started',
		self::PROSPECT_STAGE4_COMPLETED				=> 'Lease Completed'
	];

	public static $c_arrintValidPostTransferCompletedStageStatusIds = [
		self::TRANSFER_STAGE3_COMPLETED,
		self::TRANSFER_STAGE3_APPROVED,
		self::TRANSFER_STAGE4_APPROVED
	];

	public function getWaitlistStageStatuses() {
		$arrstrWaitlistStageStatuses = [
			CApplicationStageStatus::PROSPECT_STAGE1_COMPLETED => __( 'Guest Card Completed' ),
			CApplicationStageStatus::PROSPECT_STAGE3_STARTED   => __( 'Application Started' ),
			CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED => __( 'Application Completed' )
		];

		return $arrstrWaitlistStageStatuses;
	}

	public function getWaitlistStageStatusesAffordable() {
		$arrstrWaitlistStageStatusesAffordable = [
			CApplicationStageStatus::PROSPECT_STAGE1_COMPLETED => __( 'Eligibility Completed' ),
			CApplicationStageStatus::PROSPECT_STAGE3_STARTED   => __( 'Certification Started' ),
			CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED => __( 'Certification Completed' )
		];

		return $arrstrWaitlistStageStatusesAffordable;
	}

	public function getWaitlistStageStatusesMilitary() {
		$arrstrWaitlistStageStatuses = [
			CApplicationStageStatus::PROSPECT_STAGE3_STARTED   => __( 'Application Started' ),
			CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED => __( 'Application Completed' )
		];
		return $arrstrWaitlistStageStatuses;
	}

	public function getSpecialApplicationStageStatus() {

		if( valArr( $this->m_arrstrSpecialApplicationStageStatus ) ) {
			return $this->m_arrstrSpecialApplicationStageStatus;
		}

		$this->m_arrstrSpecialApplicationStageStatus = [
			CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED	=> __( 'Application Completed' ),
			CApplicationStageStatus::PROSPECT_STAGE3_APPROVED	=> __( 'Application Approved' ),
			CApplicationStageStatus::PROSPECT_STAGE4_STARTED	=> __( 'Lease Started' ),
			CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED	=> __( 'Lease Completed' ),
			CApplicationStageStatus::PROSPECT_STAGE4_APPROVED	=> __( 'Lease Approved' ),
			CApplicationStageStatus::RENEWAL_STAGE3_COMPLETED	=> __( 'Renewal Offer Completed' ),
			CApplicationStageStatus::RENEWAL_STAGE3_APPROVED	=> __( 'Renewal Offer Approved' ),
			CApplicationStageStatus::RENEWAL_STAGE4_STARTED		=> __( 'Renewal Lease Started' ),
			CApplicationStageStatus::RENEWAL_STAGE4_COMPLETED	=> __( 'Renewal Lease Completed' ),
			CApplicationStageStatus::RENEWAL_STAGE4_APPROVED	=> __( 'Renewal Lease Approved' )
		];

		return $this->m_arrstrSpecialApplicationStageStatus;
	}

	public function getBulkUnitAssignmentApplicationStageStatuses() {
		return [
			self::PROSPECT_STAGE3_APPROVED				=> __( 'Application Approved' ),
			self::PROSPECT_STAGE4_STARTED 				=> __( 'Lease Pending' ),
			self::PROSPECT_STAGE4_PARTIALLY_COMPLETED 	=> __( 'Lease Partially Signed' ),
			self::PROSPECT_STAGE4_COMPLETED 			=> __( 'Lease Completed' ),
			self::PROSPECT_STAGE4_APPROVED 				=> __( 'Lease Approved' )
		];
	}

	public function getActiveBrokerStageStatuses() {
		$arrstrActiveBrokerStageStatuses = [
			self::PROSPECT_STAGE3_STARTED				=> __( 'Application: Started' ),
			self::PROSPECT_STAGE3_PARTIALLY_COMPLETED	=> __( 'Application: Partially Completed' ),
			self::PROSPECT_STAGE3_COMPLETED				=> __( 'Application: Completed' ),
			self::PROSPECT_STAGE3_APPROVED				=> __( 'Application: Approved' ),
			self::PROSPECT_STAGE3_ON_HOLD				=> __( 'Application: Archived' ),
			self::PROSPECT_STAGE3_CANCELLED				=> __( 'Application: Cancelled' ),
			self::PROSPECT_STAGE4_STARTED				=> __( 'Lease: Started' ),
			self::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> __( 'Lease: Partially Completed' ),
			self::PROSPECT_STAGE4_COMPLETED				=> __( 'Lease: Completed' ),
			self::PROSPECT_STAGE4_APPROVED				=> __( 'Lease: Approved' )
		];

		return $arrstrActiveBrokerStageStatuses;
	}

	public static $c_arrstrIlsApiLeadApplicationStageStatus = [
		CApplicationStageStatus::PROSPECT_STAGE4_STARTED				=> 'Lease Started',
		CApplicationStageStatus::PROSPECT_STAGE4_PARTIALLY_COMPLETED	=> 'Lease Partially Completed',
		CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED				=> 'Lease Completed',
	];

	public static $c_arrintOfferTemplateStatusIds = [
		CApplicationStage::APPLICATION 			=> [ CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
	];

	public function getName() {
		return self::getCustomApplicationStageStatus( self::getLeaseIntervalTypeId(), self::getApplicationStageId(), self::getApplicationStatusId(), $this->getApplicationOccupancyTypeId() );
	}

	public function getCustomApplicationStageStatus( $intLeaseIntervalTypeId, $intApplicationStageId, $intApplicationStatusId, $intOccupancyTypeId = COccupancyType::CONVENTIONAL, $intApplicationId = NULL, $intCid = NULL, $objDatabase = NULL ) {
		return CApplicationStage::createService()->applicationStageIdtoStr( $intLeaseIntervalTypeId, $intApplicationStageId, $intOccupancyTypeId, $intApplicationId, $intCid, $objDatabase ) . ': ' . CApplicationStatus::createService()->applicationStatusIdToStr( $intApplicationStatusId, $intLeaseIntervalTypeId, $intApplicationStageId, $intOccupancyTypeId );
	}

	public function getCustomCertificationStageStatus( $intApplicationStageId, $intApplicationStatusId, $intOccupancyTypeId, $intApplicationId = NULL, $intCid = NULL, $objDatabase = NULL ) {
		return CApplicationStage::createService()->getCertificationStageStatus( $intOccupancyTypeId, $intApplicationStageId, $intApplicationId, $intCid, $objDatabase ) . ': ' . CApplicationStatus::createService()->applicationStatusIdToStr( $intApplicationStatusId, NULL, $intApplicationStageId, $intOccupancyTypeId );
	}

	public function getCustomApplicationStage( $intLeaseIntervalTypeId, $intApplicationStageId, $intOccupancyTypeId = COccupancyType::CONVENTIONAL ) {
		return \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '_', CApplicationStage::createService()->applicationStageIdtoStr( $intLeaseIntervalTypeId, $intApplicationStageId, $intOccupancyTypeId ) ) );
	}

	public function getApplicationOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	// set Method

	public function setApplicationOccupancyTypeId( $intApplicationStatusId ) {
		$this->m_intOccupancyTypeId = CStrings::strToIntDef( $intApplicationStatusId, NULL, false );
	}

	public function applicationStageStatusIdToStr( $intStageStatusId ) {
		switch( $intStageStatusId ) {
			case self::PROSPECT_STAGE3_STARTED:
				return 'Application Open';
				break;

			case self::PROSPECT_STAGE3_COMPLETED:
				return 'Application Completed';
				break;

			case self::PROSPECT_STAGE3_APPROVED:
				return 'Application Approved';
				break;

			case self::RENEWAL_STAGE3_STARTED:
				return 'Renewal Offer Extended';
				break;

			case self::RENEWAL_STAGE3_APPROVED:
				return 'Renewal Offer Accepted';
				break;

			case self::PROSPECT_STAGE4_STARTED:
				return 'Lease Pending';
				break;

			case self::PROSPECT_STAGE4_COMPLETED:
				return 'Lease Completed';
				break;

			default:
				// default case
				break;
		}
	}

}
?>
