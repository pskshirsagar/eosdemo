<?php

class CPropertyMaintenanceTemplate extends CBasePropertyMaintenanceTemplate {

	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_intParentMaintenanceTemplateId;
	protected $m_intDaysToComplete;
	protected $m_intMaintenancePriorityId;
	protected $m_intMaintenanceLocationId;
	protected $m_intMaintenanceProblemId;
	protected $m_arrintOccupancyTypeIds;
	protected $m_strPropertyName;
	protected $m_strTemplateDescriptionDetails;
	protected $m_intIsDisabled;
	protected $m_intTotalPropertyDefaultTemplates;

	protected $m_boolExcludeWeekendsHolidays;
	protected $m_boolIsMoveOutDateAsStartDate;


	/**
	 * Set Functions
	 */

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->m_intMaintenanceRequestTypeId = $intMaintenanceRequestTypeId;
	}

	public function setParentMaintenanceTemplateId( $intParentMaintenanceTemplateId ) {
		$this->m_intParentMaintenanceTemplateId = $intParentMaintenanceTemplateId;
	}

	public function setDaysToComplete( $intDaysToComplete ) {
		$this->m_intDaysToComplete = $intDaysToComplete;
	}

	public function setExcludeWeekendsHolidays( $boolExcludeWeekendsHolidays ) {
		$this->m_boolExcludeWeekendsHolidays = CStrings::strToBool( $boolExcludeWeekendsHolidays );
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->m_intMaintenancePriorityId = CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false );
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->m_intMaintenanceLocationId = CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false );
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->m_intMaintenanceProblemId = CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->m_arrintOccupancyTypeIds = CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL );
	}

	public function setIsMoveOutDateAsStartDate( $boolIsMoveOutDateAsStartDate ) {
		$this->m_boolIsMoveOutDateAsStartDate = CStrings::strToBool( $boolIsMoveOutDateAsStartDate );
	}

	public function setTemplateDescriptionDetails( $strTemplateDescriptionDetails ) {
		$this->m_strTemplateDescriptionDetails = $strTemplateDescriptionDetails;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = CStrings::strToIntDef( $intIsDisabled, NULL, false );
	}

	public function setTotalPropertyDefaultTemplates( $intTotalPropertyDefaultTemplates ) {
		$this->m_intTotalPropertyDefaultTemplates = CStrings::strToIntDef( $intTotalPropertyDefaultTemplates, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['title'] ) ) 	$this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['description'] ) ) $this->setDescription( $arrmixValues['description'] );
		if( true == isset( $arrmixValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrmixValues['maintenance_request_type_id'] );
		if( true == isset( $arrmixValues['parent_maintenance_template_id'] ) ) 	$this->setParentMaintenanceTemplateId( $arrmixValues['parent_maintenance_template_id'] );
		if( true == isset( $arrmixValues['days_to_complete'] ) ) 	$this->setDaysToComplete( $arrmixValues['days_to_complete'] );
		if( true == isset( $arrmixValues['exclude_weekends_holidays'] ) ) 	$this->setExcludeWeekendsHolidays( $arrmixValues['exclude_weekends_holidays'] );
		if( true == isset( $arrmixValues['maintenance_priority_id'] ) ) 	$this->setMaintenancePriorityId( $arrmixValues['maintenance_priority_id'] );
		if( true == isset( $arrmixValues['maintenance_location_id'] ) ) 	$this->setMaintenanceLocationId( $arrmixValues['maintenance_location_id'] );
		if( true == isset( $arrmixValues['maintenance_problem_id'] ) ) 	$this->setMaintenanceProblemId( $arrmixValues['maintenance_problem_id'] );
		if( true == isset( $arrmixValues['property_name'] ) ) 	$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['occupancy_type_ids'] ) ) 	$this->setOccupancyTypeIds( $arrmixValues['occupancy_type_ids'] );
		if( true == isset( $arrmixValues['is_move_out_date_as_start_date'] ) ) 	$this->setIsMoveOutDateAsStartDate( $arrmixValues['is_move_out_date_as_start_date'] );
		if( true == isset( $arrmixValues['template_description_details'] ) ) 	$this->setTemplateDescriptionDetails( $arrmixValues['template_description_details'] );
		if( true == isset( $arrmixValues['is_disabled'] ) ) 	$this->setIsDisabled( $arrmixValues['is_disabled'] );
		if( true == isset( $arrmixValues['total_property_default_templates'] ) ) 	$this->setTotalPropertyDefaultTemplates( $arrmixValues['total_property_default_templates'] );

		return;
	}

	/**
	 * Get Functions
	 */

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function getParentMaintenanceTemplateId() {
		return $this->m_intParentMaintenanceTemplateId;
	}

	public function getDaysToComplete() {
		return $this->m_intDaysToComplete;
	}

	public function getExcludeWeekendsHolidays() {
		return $this->m_boolExcludeWeekendsHolidays;
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function getIsMoveOutDateAsStartDate() {
		return $this->m_boolIsMoveOutDateAsStartDate;
	}

	public function getTemplateDescriptionDetails() {
		return $this->m_strTemplateDescriptionDetails;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getTotalPropertyDefaultTemplates() {
		return $this->m_intTotalPropertyDefaultTemplates;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupCount() {
		$boolIsValid = true;

		if( false == valId( $this->getGroupCount() ) || 0 >= $this->getGroupCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_count', 'Please enter valid number of units in a bulk group.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsBulkGroup = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( true == $boolIsBulkGroup ) {
					$boolIsValid &= $this->valGroupCount();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>