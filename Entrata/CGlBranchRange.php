<?php

class CGlBranchRange extends CBaseGlBranchRange {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlTreeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStartNumberRange() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndNumberRange() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>