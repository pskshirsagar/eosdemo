<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyCategories
 * Do not add any new functions to this class.
 */

class CCompanyCategories extends CBaseCompanyCategories {

	public static function fetchSimpleCompanyCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCategories( sprintf( 'SELECT * FROM %s WHERE cid = %d ORDER BY order_num', 'company_categories', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyCategoriesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

//	    $strSql = "SELECT
//	    				DISTINCT ON (cc.id) cc.*
//	    			FROM
//	    				company_categories AS cc,
//	    				property_categories AS pc
//	    			WHERE
//	    				pc.property_id IN ( " . implode(',', $arrintPropertyIds ) . " )
//	    				AND pc.company_category_id = cc.id ";

		$strSql = 'SELECT
		 				cc.*
		    		 FROM
		    		 	company_categories cc
		    		 WHERE
		    		 	cc.id IN (
		 							SELECT
									   DISTINCT ( cc.id )
									FROM
									   company_categories cc
									   JOIN property_categories pc ON ( pc.cid = cc.cid AND pc.company_category_id = cc.id )
									WHERE
									   cc.cid = ' . ( int ) $intCid . '
									   AND pc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . '  )
		    					)
		    			AND cc.cid = ' . ( int ) $intCid . '
		    		 ORDER BY
		    			cc.order_num';

		return self::fetchCompanyCategories( $strSql, $objDatabase );
	}

	public static function fetchCompanyCategoriesDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
						cc.id,
						cc.name,
	 					array_to_string(array_agg(pc.property_id), \',\') as properties,
	 					cc.order_num
					FROM
						company_categories cc
					JOIN property_categories pc ON ( pc.cid = cc.cid AND pc.company_category_id = cc.ID	AND pc.is_published = 1 )
					JOIN properties p on (p.cid = cc.cid and p.id = pc.property_id and p.is_disabled = 0)
					WHERE
						cc.cid = ' . ( int ) $intCid . '
					AND cc.is_published = 1
					AND pc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . '  )
					GROUP BY
						cc.id, cc.name, cc.order_num
					ORDER BY
		    			cc.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyCategories( sprintf( 'SELECT DISTINCT ON (cc.id) cc.* FROM %s AS cc, %s AS pc WHERE pc.property_id = %d AND cc.cid = %d AND pc.company_category_id = cc.id AND pc.cid = cc.cid', 'company_categories', 'property_categories', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnassociatedCompanyCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
		  				DISTINCT ON ( cc.id )
	  					cc.*
		  			FROM
		  				company_categories cc
						LEFT OUTER JOIN property_categories pc ON ( cc.id::int = pc.company_category_id::int AND pc.property_id = ' . ( int ) $intPropertyId . ' AND cc.cid = pc.cid )
					WHERE
						pc.id IS NULL
						AND cc.cid = ' . ( int ) $intCid;

		return self::fetchCompanyCategories( $strSql, $objDatabase );
	}

	public static function fetchCompanyCategoryByIdByPropertyIdByCid( $intCompanyCategoryId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
		  				cc.*
		  			FROM
		  				company_categories cc
						JOIN property_categories pc ON ( pc.cid = cc.cid AND cc.id = pc.company_category_id )
					WHERE
						pc.property_id = ' . ( int ) $intPropertyId . '
						AND cc.id = ' . ( int ) $intCompanyCategoryId . '
						AND cc.cid = ' . ( int ) $intCid;

		return self::fetchCompanyCategory( $strSql, $objDatabase );
	}

	public static function fetchCompanyCategoriesByNamesByCid( $arrstrCompanyCategories, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrCompanyCategories ) ) return NULL;

		foreach( $arrstrCompanyCategories as $strCompanyCategory ) {
			$arrstrOrParameters[] = ' name ILIKE \'' . str_replace( '\'', '\`', trim( addslashes( $strCompanyCategory ) ) ) . '\' ';
		}

		$strSql = 'SELECT * FROM
						company_categories
					WHERE
						( ' . implode( ' OR ', $arrstrOrParameters ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyCategories( $strSql, $objDatabase );
	}

	public static function fetchCompanyCategoriesByIdsByCid( $arrintCompanyCategoryIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyCategoryIds ) ) return NULL;

		return self::fetchCompanyCategories( sprintf( 'SELECT * FROM %s WHERE id IN ( %s ) AND cid = %d ORDER BY order_num, id', 'company_categories', implode( ',', $arrintCompanyCategoryIds ), ( int ) $intCid ), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public static function generateNewCompanyCategories( $strNewCompanyCategories, $arrmixRequestForm, $arrmixFormFields, $intCid, $intPropertyId, $objDatabase ) {
		if( true == is_null( $strNewCompanyCategories ) || 0 == strlen( trim( $strNewCompanyCategories ) ) ) return NULL;

		$arrobjCompanyCategories = [];

		// Compress \r\n. Multiple Enter in between lines.
		$strRepairedNewCategories = preg_replace( '/(\r\n)+/', '\r\n', trim( $strNewCompanyCategories ) );

		// Replace commas with new lines
		$strRepairedNewCategories = str_replace( '\'', '`', str_replace( ';', '\r\n', $strRepairedNewCategories ) );

		// Explode, using \r\n as delimiter.
		$arrstrCompanyCategories = explode( '\r\n', $strRepairedNewCategories );

		// Make array unique
		$arrstrCompanyCategories = array_unique( $arrstrCompanyCategories );

		if( false == valArr( $arrstrCompanyCategories ) ) return NULL;

		// Get current associations so that we don't try to reinsert a property_category already associated
		$arrobjCurrentAssociatedCompanyCategories = self::fetchCompanyCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase );

		// Check database to see if any of these company categories already exist for this client
		$arrobjExistingCompanyCategories = self::fetchCompanyCategoriesByNamesByCid( $arrstrCompanyCategories, $intCid, $objDatabase );

		if( true == valArr( $arrobjExistingCompanyCategories ) ) {
			foreach( $arrobjExistingCompanyCategories as $objExistingCompanyCategory ) {

				if( true == in_array( \Psi\CStringService::singleton()->strtolower( $objExistingCompanyCategory->getName() ), array_map( 'strtolower', $arrstrCompanyCategories ) ) ) {

					// Remove the element that is already in the database from the array
					$arrstrNewCompanyCategories = [];

					if( true == valArr( $arrstrCompanyCategories ) ) {
						foreach( $arrstrCompanyCategories as $strCompanyCategory ) {
							if( trim( \Psi\CStringService::singleton()->strtolower( $strCompanyCategory ) ) != trim( \Psi\CStringService::singleton()->strtolower( $objExistingCompanyCategory->getName() ) ) ) {
								array_push( $arrstrNewCompanyCategories, $strCompanyCategory );
							}
						}
					}

					$arrstrCompanyCategories = $arrstrNewCompanyCategories;

					// Make sure not to send back a CCompanyCategory if it is already associated to the property.
					if( false == in_array( $objExistingCompanyCategory->getId(), array_keys( $arrobjCurrentAssociatedCompanyCategories ) ) ) {
						// Add $objExistingCompanyCategory to return array
						array_push( $arrobjCompanyCategories, $objExistingCompanyCategory );
					}
				}
			}
		}

		foreach( $arrstrCompanyCategories as $strCompanyCategory ) {
			if( false == is_null( trim( $strCompanyCategory ) ) && 0 < strlen( $strCompanyCategory ) ) {
				$objCompanyCategory = new CCompanyCategory();
				$objCompanyCategory->setName( trim( $strCompanyCategory ) );
				$objCompanyCategory->setCid( $intCid );
				$objCompanyCategory->applyRequestForm( $arrmixRequestForm, $arrmixFormFields );
				array_push( $arrobjCompanyCategories, $objCompanyCategory );
			}
		}

		return $arrobjCompanyCategories;
	}

}
?>