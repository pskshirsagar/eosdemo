<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitOfMeasureConversions
 * Do not add any new functions to this class.
 */

class CUnitOfMeasureConversions extends CBaseUnitOfMeasureConversions {

	public static function fetchUnitOfMeasureConversionsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						unit_of_measure_conversions uomc
					WHERE
						uomc.cid = ' . ( int ) $intCid . '
					ORDER BY
						id';
		return self::fetchUnitOfMeasureConversions( $strSql, $objClientDatabase );

	}

	public static function fetchUnitOfMeasureConversionsByFromOrToUnitOfMeasureIdsByApCodeIdsByCid( $arrstrFromOrTo, $arrintApCodeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrFromOrTo ) || false == ( $arrintApCodeIds = getIntValuesFromArr( $arrintApCodeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						uomc.*,
						ac.id AS ap_code_id
					FROM
						unit_of_measure_conversions uomc
						LEFT JOIN ap_codes ac ON ( uomc.cid = ac.cid AND ( uomc.from_unit_of_measure_id = ac.unit_of_measure_id OR uomc.to_unit_of_measure_id = ac.unit_of_measure_id ) )
					WHERE
						( uomc.from_unit_of_measure_id, uomc.to_unit_of_measure_id ) IN ( ' . implode( ',', $arrstrFromOrTo ) . ' )
						AND uomc.cid = ' . ( int ) $intCid . '
						AND ac.id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )';

		return self::fetchUnitOfMeasureConversions( $strSql, $objClientDatabase, false );
	}

	public static function fetchUnitOfMeasureConversionsByFromUnitOfMeasureIdByToUnitOfMeasureIdsByCid( $intFromUnitOfMeasureId, $arrintToUnitOfMeasureIds, $intCid, $objDatabase ) {
		if( false == ( $arrintToUnitOfMeasureIds = getIntValuesFromArr( $arrintToUnitOfMeasureIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						unit_of_measure_conversions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( ( from_unit_of_measure_id = ' . ( int ) $intFromUnitOfMeasureId . ' AND to_unit_of_measure_id IN ( ' . sqlIntImplode( $arrintToUnitOfMeasureIds ) . ' ) ) 
							  OR ( to_unit_of_measure_id = ' . ( int ) $intFromUnitOfMeasureId . ' AND from_unit_of_measure_id IN ( ' . sqlIntImplode( $arrintToUnitOfMeasureIds ) . ' ) ) )';

		return parent::fetchUnitOfMeasureConversions( $strSql, $objDatabase );
	}

}
?>