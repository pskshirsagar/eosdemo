<?php

class CPropertyCamPeriod extends CBasePropertyCamPeriod {

	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strReconciliationYear;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues );

		if( isset( $arrmixValues['start_date'] ) && $boolDirectSet ) {
			$this->set( 'm_strStartDate', trim( $arrmixValues['start_date'] ) );
		} elseif( isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( $arrmixValues['start_date'] );
		}

		if( isset( $arrmixValues['end_date'] ) && $boolDirectSet ) {
			$this->set( 'm_strEndDate', trim( $arrmixValues['end_date'] ) );
		} elseif( isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( $arrmixValues['end_date'] );
		}

		if( isset( $arrmixValues['reconciliation_year'] ) ) {
			$this->setReconciliationYear( $arrmixValues['reconciliation_year'] );
		}

	}

	public function setReconciliationYear( $strReconciliationYear ) {
		$this->set( 'm_strReconciliationYear', CStrings::strTrimDef( $strReconciliationYear, -1, NULL, true ) );
	}

	public function getReconciliationYear() {
		return $this->m_strReconciliationYear;
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCamPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReconciledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReconciledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>
