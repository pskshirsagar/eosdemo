<?php

class CPropertyUnitStartWindowSetting extends CBasePropertyUnitStartWindowSetting {

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Id required - CPropertyUnitStartWindowSetting::valId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Management Id required - CLeaseCustomer::valCid()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 0 >= ( int ) $this->m_intPropertyId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Property Id required - CLeaseCustomer::valPropertyId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyUnitId ) || 0 >= ( int ) $this->m_intPropertyUnitId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Property Unit Id required - CLeaseCustomer::valPropertyUnitId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intLeaseTermId ) || 0 >= ( int ) $this->m_intLeaseTermId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Lease Term Id required - CLeaseCustomer::valLeaseTermId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intLeaseStartWindowId ) || 0 >= ( int ) $this->m_intLeaseStartWindowId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Lease Start Window Id required - CLeaseCustomer::valLeaseStartWindowId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valGenderId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intGenderId ) || 0 >= ( int ) $this->m_intGenderId ) {
			$boolIsValid = false;
			trigger_error( __( 'Invalid Property Unit Request:  Gender Id required - CLeaseCustomer::valGenderId()' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valLeaseTermId();
				$boolIsValid &= $this->valLeaseStartWindowId();
				$boolIsValid &= $this->valGenderId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createPropertyUnitStartWindowSetting( $objBulkUnitAssignmentFilter, $intPropertyUnitId, $intCid ) {
		if( false == valObj( $objBulkUnitAssignmentFilter, 'CBulkUnitAssignmentFilter' ) || false == valId( $intPropertyUnitId ) || false == valId( $intCid ) ) return NULL;

		$objPropertyUnitStartWindowSetting = new CPropertyUnitStartWindowSetting();
		$objPropertyUnitStartWindowSetting->setCid( $intCid );
		$objPropertyUnitStartWindowSetting->setPropertyId( $objBulkUnitAssignmentFilter->getPropertyId() );
		$objPropertyUnitStartWindowSetting->setPropertyUnitId( $intPropertyUnitId );
		$objPropertyUnitStartWindowSetting->setLeaseTermId( $objBulkUnitAssignmentFilter->getLeaseTermId() );
		$objPropertyUnitStartWindowSetting->setLeaseStartWindowId( $objBulkUnitAssignmentFilter->getLeaseStartWindowId() );

		return $objPropertyUnitStartWindowSetting;
	}

	public static function createPropertyUnitStartWindowSettingByUnitMixFilter( $objUnitMixFilter, $intPropertyUnitId, $intCid ) {
		if( false === valObj( $objUnitMixFilter, \Psi\Entrata\Library\BUA\Filters\CUnitMixFilter::class ) || false === valId( $intPropertyUnitId ) || false === valId( $intCid ) ) return NULL;

		$objPropertyUnitStartWindowSetting = new CPropertyUnitStartWindowSetting();
		$objPropertyUnitStartWindowSetting->setCid( $intCid );
		$objPropertyUnitStartWindowSetting->setPropertyId( $objUnitMixFilter->getPropertyId() );
		$objPropertyUnitStartWindowSetting->setPropertyUnitId( $intPropertyUnitId );
		$objPropertyUnitStartWindowSetting->setLeaseTermId( $objUnitMixFilter->getLeaseTermId() );
		$objPropertyUnitStartWindowSetting->setLeaseStartWindowId( $objUnitMixFilter->getLeaseStartWindowId() );

		return $objPropertyUnitStartWindowSetting;
	}

}
?>