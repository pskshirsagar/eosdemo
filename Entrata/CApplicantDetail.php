<?php

class CApplicantDetail extends CBaseApplicantDetail {

	const LEGAL_US_RESIDENT_YES 			= 1;
	const LEGAL_US_RESIDENT_YES_WITH_USCIS 	= 2;
	const LEGAL_US_RESIDENT_NO 				= 3;

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getApplicantId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRelocationReason() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getRelocationReason() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relocation_reason', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasFelonyConviction() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasFelonyConviction() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_felony_conviction', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFelonyIsResolved() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getFelonyIsResolved() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_is_resolved', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFelonyComments() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getFelonyComments() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_comments', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFelonyYear() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getFelonyYear() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_year', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSmokes() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getSmokes() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'smokes', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasPets() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasPets() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_pets', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPetDescription() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getPetDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasBeenEvicted() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasBeenEvicted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_been_evicted', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasBrokenLease() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasBrokenLease() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_broken_lease', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasBankruptcy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasBankruptcy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_bankruptcy', '' ) );
        // }

        return $boolIsValid;
    }

    public function valBankruptcyCounty() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getBankruptcyCounty() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_county', '' ) );
        // }

        return $boolIsValid;
    }

    public function valBankruptcyStateCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getBankruptcyStateCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valBankruptcyYear() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getBankruptcyYear() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_year', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasPublicJudgment() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasPublicJudgment() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_public_judgment', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPublicJudgmentYear() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getPublicJudgmentYear() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'public_judgment_year', '' ) );
        // }

        return $boolIsValid;
    }

    public function valWasSuedForRent() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getWasSuedForRent() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'was_sued_for_rent', '' ) );
        // }

        return $boolIsValid;
    }

    public function valWasSuedForDamages() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getWasSuedForDamages() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'was_sued_for_damages', '' ) );
        // }

        return $boolIsValid;
    }

    public function valReferenceComments() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getReferenceComments() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_comments', '' ) );
        // }

        return $boolIsValid;
    }

    public function valReferencesApprovedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getReferencesApprovedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'references_approved_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valReferencesApprovedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getReferencesApprovedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'references_approved_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>