<?php

class CPropertyMaintenancePriority extends CBasePropertyMaintenancePriority {

	protected $m_intMaintenancePriorityPropertyCount;
	protected $m_intMaintenancePriorityDueIntervalValuesCount;
	protected $m_intMaintenancePriorityUseCalendarHoursValuesCount;
	protected $m_intMaintenancePriorityHourTypeValuesCount;

	protected $m_strName;
	protected $m_strIntegrationClientTypeName;
	protected $m_strMaintenancePriorityTypeName;

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

    	if( true == valArr( $arrmixFormFields ) ) {
            $arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
        }

        $this->setValues( $arrmixRequestForm, false );

        return;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, true, $boolDirectSet );

        if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
        if( true == isset( $arrmixValues['show_on_resident_portal_company_level'] ) ) $this->setShowOnResidentPortalCompanyLevel( $arrmixValues['show_on_resident_portal_company_level'] );
        if( true == isset( $arrmixValues['is_published_company_level'] ) ) $this->setIsPublishedCompanyLevel( $arrmixValues['is_published_company_level'] );
        if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
        if( true == isset( $arrmixValues['properties_count'] ) ) $this->m_intMaintenancePriorityPropertyCount = trim( $arrmixValues['properties_count'] );
        if( true == isset( $arrmixValues['maintenance_priority_type_name'] ) ) $this->setMaintenancePriorityTypeName( $arrmixValues['maintenance_priority_type_name'] );
	    if( true == isset( $arrmixValues['due_interval_values_count'] ) ) $this->setMaintenancePriorityDueIntervalValuesCount( $arrmixValues['due_interval_values_count'] );
	    if( true == isset( $arrmixValues['use_calendar_hours_values_count'] ) ) $this->setMaintenancePriorityUseCalendarHoursValuesCount( $arrmixValues['use_calendar_hours_values_count'] );
	    if( true == isset( $arrmixValues['maintenance_hour_type_id_values_count'] ) ) $this->setMaintenancePriorityHourTypeValuesCount( $arrmixValues['maintenance_hour_type_id_values_count'] );
        return;
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function setShowOnResidentPortalCompanyLevel( $intShowOnResidentPortalCompanyLevel ) {
    	$this->m_intShowOnResidentPortalCompanyLevel = $intShowOnResidentPortalCompanyLevel;
    }

    public function getShowOnResidentPortalCompanyLevel() {
    	return $this->m_intShowOnResidentPortalCompanyLevel;
    }

    public function setIsPublishedCompanyLevel( $intIsPublished ) {
    	$this->m_intIsPublishedCompanyLevel = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublishedCompanyLevel() {
    	return $this->m_intIsPublishedCompanyLevel;
    }

    public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
    	$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
    }

    public function getIntegrationClientTypeName() {
    	return $this->m_strIntegrationClientTypeName;
    }

    public function setMaintenancePriorityPropertyCount( $intMaintenancePriorityPropertyCount ) {
    	$this->m_intMaintenancePriorityPropertyCount = CStrings::strToIntDef( $intMaintenancePriorityPropertyCount, NULL, false );
    }

    public function getMaintenancePriorityPropertyCount() {
    	return $this->m_intMaintenancePriorityPropertyCount;
    }

	public function setMaintenancePriorityDueIntervalValuesCount( $intMaintenancePriorityDueIntervalValuesCount ) {
		$this->m_intMaintenancePriorityDueIntervalValuesCount = CStrings::strToIntDef( $intMaintenancePriorityDueIntervalValuesCount, NULL, false );
	}

	public function getMaintenancePriorityDueIntervalValuesCount() {
		return $this->m_intMaintenancePriorityDueIntervalValuesCount;
	}

	public function setMaintenancePriorityUseCalendarHoursValuesCount( $intMaintenancePriorityUseCalendarHoursValuesCount ) {
		$this->m_intMaintenancePriorityUseCalendarHoursValuesCount = CStrings::strToIntDef( $intMaintenancePriorityUseCalendarHoursValuesCount, NULL, false );
	}

	public function getMaintenancePriorityUseCalendarHoursValuesCount() {
		return $this->m_intMaintenancePriorityUseCalendarHoursValuesCount;
	}

	public function setMaintenancePriorityHourTypeValuesCount( $intMaintenancePriorityHourTypeValuesCount ) {
		$this->m_intMaintenancePriorityHourTypeValuesCount = CStrings::strToIntDef( $intMaintenancePriorityHourTypeValuesCount, NULL, false );
	}

	public function getMaintenancePriorityHourTypeValuesCount() {
		return $this->m_intMaintenancePriorityHourTypeValuesCount;
	}

    public function setMaintenancePriorityTypeName( $strMaintenancePriorityTypeName ) {
    	$this->m_strMaintenancePriorityTypeName = $strMaintenancePriorityTypeName;
    }

    public function getMaintenancePriorityTypeName() {
    	return $this->m_strMaintenancePriorityTypeName;
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenancePriorityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPropertyMaintenancePriorityTypeMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenancePriorityId() ) ) {

    		$arrobjPropertyMaintenanceTemplates = \Psi\Eos\Entrata\CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByCidByPropertyIdByMaintenancePriotiryId( $this->getCid(), $this->getPropertyId(), $this->getMaintenancePriorityId(), $objDatabase );

    		if( true == valArr( $arrobjPropertyMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', 'Maintenance template(s) exists for the maintenance priority.' ) );
    		}

    	}

    	return $boolIsValid;
    }

    public function valDefaultPropertyMaintenancePriority( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenancePriorityId() ) ) {
    		$arrmixDefaultPropertyMaintenancePriority = \Psi\Eos\Entrata\CPropertyMaintenancePriorities::createService()->fetchDefaultPropertyMaintenancePriorityByPropertyMaintenancePriorityIdByCid( $this->getMaintenancePriorityId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrmixDefaultPropertyMaintenancePriority ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', 'Maintenance Priority set as Default at property level cannot be removed or disabled.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase=NULL, $boolIsFromPropertySettingTemplates = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	break;

            case VALIDATE_UPDATE:
            	 	$boolIsValid &= $this->valPropertyMaintenancePriorityTypeMaintenanceTemplates( $objDatabase );
            	 	$boolIsValid &= $this->valDefaultPropertyMaintenancePriority( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	if( false == $boolIsFromPropertySettingTemplates ) {
            		$boolIsValid &= $this->valDefaultPropertyMaintenancePriority( $objDatabase );
            	}

            	$boolIsValid &= $this->valPropertyMaintenancePriorityTypeMaintenanceTemplates( $objDatabase );
            	break;

            case 'validate_bulk_update':
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>