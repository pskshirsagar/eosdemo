<?php

class CSharedFilters extends CBaseSharedFilters {

	public static function fetchSharedFiltersWithUserDetailsByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		if( false == isset( $intReportFilterId ) ) return NULL;

		$strSql = '	SELECT
						sf.id,
						sf.report_filter_id,
						sf.company_user_id,
						sf.company_group_id
 					FROM
						shared_filters sf
						JOIN report_filters rf ON ( rf.cid = sf.cid AND rf.id = sf.report_filter_id )
						LEFT JOIN company_users cu ON ( cu.cid = sf.cid AND cu.id = sf.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						LEFT JOIN company_user_groups cug ON ( cug.cid = sf.cid AND cug.id = sf.company_group_id )
					WHERE
						sf.cid = ' . ( int ) $intCid . '
						AND sf.deleted_by IS NULL
						AND sf.deleted_on IS NULL
						AND sf.report_filter_id = ' . ( int ) $intReportFilterId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSharedFilterDetailsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		if( false == isset( $intCompanyGroupId ) ) return NULL;
		$strSql = '
			SELECT
				rf.name AS filter_name,
				util_get_translated( \'title\', r.title, r.details ) AS report_name
			FROM
				shared_filters sf
				JOIN report_filters rf ON ( sf.cid = rf.cid AND sf.report_filter_id = rf.id  )
				JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
			WHERE
				sf.cid = ' . ( int ) $intCid . '
				AND sf.company_group_id = ' . ( int ) $intCompanyGroupId . '
				AND sf.deleted_by IS NULL
				AND sf.deleted_on IS NULL';
		return fetchData( $strSql, $objDatabase );
	}

}
?>