<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallCustomerTypes
 * Do not add any new functions to this class.
 */

class CScheduledCallCustomerTypes extends CBaseScheduledCallCustomerTypes {

	/**
	 * Fetch Functions
	 */

    public static function fetchScheduledCallCustomerTypesByScheduledCallIdKeyedByCustomerTypeIdByCid( $intScheduledCallId, $intCid, $objDatabase ) {

        $strSql = 'SELECT * FROM scheduled_call_customer_types WHERE scheduled_call_id = ' . ( int ) $intScheduledCallId . ' AND cid = ' . ( int ) $intCid;
    	$arrobjScheduledCallCustomerTypes = self::fetchScheduledCallCustomerTypes( $strSql, $objDatabase );
    	return rekeyObjects( 'LeaseStatusTypeId', $arrobjScheduledCallCustomerTypes );
    }
}
?>