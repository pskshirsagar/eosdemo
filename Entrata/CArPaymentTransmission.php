<?php

class CArPaymentTransmission extends CBaseArPaymentTransmission {

	protected $m_intUnprocessedItemCount;
	protected $m_intPropertyItemCount;
	protected $m_fltTotalAmount;
	protected $m_strPropertyName;

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['unprocessed_item_count'] ) ) $this->setUnprocessedItemCount( $arrValues['unprocessed_item_count'] );
        if( true == isset( $arrValues['property_item_count'] ) ) $this->setPropertyItemCount( $arrValues['property_item_count'] );
        if( true == isset( $arrValues['unprocessed_amount'] ) ) $this->setUnprocessedAmount( $arrValues['unprocessed_amount'] );

        return;
    }

    public function setUnprocessedItemCount( $intUnprocessedItemCount ) {
		$this->m_intUnprocessedItemCount = $intUnprocessedItemCount;
    }

    public function setPropertyItemCount( $intPropertyItemCount ) {
		$this->m_intPropertyItemCount = $intPropertyItemCount;
    }

    public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
    }

	public function setUnprocessedAmount( $fltUnprocessedAmount ) {
        $this->m_fltUnProcessedAmount = CStrings::strToFloatDef( $fltUnprocessedAmount, NULL, false, 2 );
    }

    /**
     * Get Functions
     */

    public function getUnprocessedItemCount() {
		return $this->m_intUnprocessedItemCount;
    }

    public function getPropertyItemCount() {
		return $this->m_intPropertyItemCount;
    }

    public function getPropertyName() {
		return $this->m_strPropertyName;
    }

    public function getUnprocessedAmount() {
		return $this->m_fltUnProcessedAmount;
    }

    /**
     * Validate Functions
     */

    public function valTotalAmount() {
        $boolIsValid = true;

        if( 0 >= $this->getTotalAmount() ) {
        	$this->addErrorMsg( new CErrorMsg( NULL, 'total_amount', __( 'Transmission batch amount must be greater than zero.' ) ) );
        	return false;
        }

        return $boolIsValid;
    }

    public function valItemCount() {
        $boolIsValid = true;

		if( 0 >= $this->getItemCount() ) {
        	$this->addErrorMsg( new CErrorMsg( NULL, 'item_count', __( 'Transmission item count must be greater than zero.' ) ) );
        	return false;
        }

        return $boolIsValid;
    }

    public function valTransmissionTotals( $objPaymentDatabase ) {
		$boolIsValid = true;

		$arrobjArPayments = $this->fetchArPayments( $objPaymentDatabase );

		$fltTotalAmount = 0;
		$intItemCount = 0;

		if( true == valArr( $arrobjArPayments ) ) {
			foreach( $arrobjArPayments as $objArPayment ) {
				$intItemCount++;
				$fltTotalAmount += $objArPayment->getTotalAmount();
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( NULL, 'item_count', __( 'No payments for this batch were transmitted to server.  Batch cannot be closed.' ) ) );
			$boolIsValid = false;
			return;
		}

		if( 0 != bccomp( $fltTotalAmount, $this->getTotalAmount(), 2 ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'total_amount', 'The sum of the payments submitted for this batch does not equal the batch amount ( ' . $fltTotalAmount . ' <> ' . $this->getTotalAmount() . ' )' ) );
			$boolIsValid = false;
		}

		if( ( float ) $intItemCount != ( int ) $this->getItemCount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'item_count', __( 'The number of items submitted for this batch does not equal the item count on the batch.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
    }

	/**
	 * @param $objPaymentDatabase
	 * @param $intArPaymentTransmissionId
	 * @param $arrintPropertyId
	 * @param $intCompanyMerchantAccountId
	 * @param $intClientId
	 *
	 * @return bool|void
	 */
	public function valDocScanTransmissionTotals( $objPaymentDatabase, $intArPaymentTransmissionId, $arrintPropertyId, $intCompanyMerchantAccountId, $intClientId ) {

		$arrobjArPayments = CArPayments::fetchCustomArPaymentsByArPaymentTransmissionIdByCompanyMenrchantAccountIdByPropertyIdsByCid( $intArPaymentTransmissionId, $arrintPropertyId, $intCompanyMerchantAccountId, $intClientId, $objPaymentDatabase );

		$boolIsValid = true;
		$fltTotalAmount = 0;
		$intItemCount = 0;

		if( true == valArr( $arrobjArPayments ) ) {
			foreach( $arrobjArPayments as $objArPayment ) {
				$intItemCount++;
				$fltTotalAmount += $objArPayment->getTotalAmount();
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( NULL, 'item_count', __( 'No payments for this batch were transmitted to server.  Batch cannot be closed.' ) ) );
			$boolIsValid = false;
			return;
		}

		if( 0 != bccomp( $fltTotalAmount, $this->getTotalAmount(), 2 ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'total_amount', __( 'The sum of the payments submitted for this batch does not equal the batch amount ( {%f, flt_total_amt, p:2} <> {%f, total_amt, p:2} )', [ 'flt_total_amt' => $fltTotalAmount, 'total_amt' => $this->getTotalAmount() ] ) ) );
			$boolIsValid = false;
		}

		if( ( float ) $intItemCount != ( int ) $this->getItemCount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'item_count', __( 'The number of items submitted for this batch does not equal the item count on the batch.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objPaymentDatabase = NULL, $intArPaymentTransmissionId = NULL, $arrintPropertyId = NULL, $intCompanyMerchantAccountId = NULL, $intClientId = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'validate_batch_posting':
            	$boolIsValid &= $this->valTotalAmount();
            	$boolIsValid &= $this->valItemCount();
            	$boolIsValid &= $this->valTransmissionTotals( $objPaymentDatabase );
            	break;

			case 'validate_doc_scan_batch_posting':
				$boolIsValid &= $this->valTotalAmount();
				$boolIsValid &= $this->valItemCount();
				$boolIsValid &= $this->valDocScanTransmissionTotals( $objPaymentDatabase, $intArPaymentTransmissionId, $arrintPropertyId, $intCompanyMerchantAccountId, $intClientId );
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchArPayments( $objPaymentDatabase ) {
		return CArPayments::fetchCustomArPaymentsByArPaymentTransmissionIdByCid( $this->m_intId, $this->m_intCid, $objPaymentDatabase );
    }

    public function fetchProperty( $objDatabase ) {
    	return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );
    }

    public function fetchCompanyMerchantAccount( $objPaymentDatabase ) {
    	return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->m_intCompanyMerchantAccountId, $objPaymentDatabase );
    }

    public function fetchMerchantAccount( $objDatabase ) {
    	return CMerchantAccounts::fetchCustomMerchantAccountByIdByCid( $this->m_intCompanyMerchantAccountId, $this->getCid(), $objDatabase );
    }

    /**
     * Other Functions
     */

    public function postBatch( $intCompanyUserId, $objDatabase ) {
		$this->setConfirmedOn( date( 'm/d/Y H:i:s' ) );

		$boolIsValid = $this->update( $intCompanyUserId, $objDatabase );
		return $boolIsValid;
    }

}

?>