<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportConfigurations
 * Do not add any new functions to this class.
 */

class CApExportConfigurations extends CBaseApExportConfigurations {

	public static function fetchApExportConfigurationByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApExportConfiguration( sprintf( 'SELECT * FROM ap_export_configurations WHERE ap_export_batch_id = %d AND cid = %d', ( int ) $intApExportBatchId, ( int ) $intCid ), $objDatabase );
	}

}
?>