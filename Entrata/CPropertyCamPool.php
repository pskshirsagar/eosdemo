<?php

class CPropertyCamPool extends CBasePropertyCamPool {

	const PROPERTY_CAM_POOL_SQUARE_FOOTAGE_TYPE_ENTIRE_PROPERTY     = 'ENTIRE_PROPERTY';
	const PROPERTY_CAM_POOL_SQUARE_FOOTAGE_TYPE_SUM_OF_TENANTS      = 'SUM_OF_TENANTS';
	const PROPERTY_CAM_POOL_SQUARE_FOOTAGE_TYPE_CUSTOM              = 'CUSTOM';

	protected $m_fltTotalAnnualBudget;

	protected $m_boolIsActiveLease;

	protected $m_strEffectiveDate;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues );
		if( isset( $arrmixValues['total_annual_budget'] ) ) $this->setTotalAnnualBudget( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['total_annual_budget'] ) : $arrmixValues['total_annual_budget'] );
		if( isset( $arrmixValues['effective_date'] ) ) $this->setEffectiveDate( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['effective_date'] ) : $arrmixValues['effective_date'] );
		if( isset( $arrmixValues['is_active_lease'] ) ) $this->setIsActiveLease( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['is_active_lease'] ) : $arrmixValues['is_active_lease'] );
		if( isset( $arrmixValues['property_cam_pool_square_footage_type'] ) ) $this->setPropertyCamPoolSquareFootageType( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['property_cam_pool_square_footage_type'] ) : $arrmixValues['property_cam_pool_square_footage_type'] );
	}

	public function valId() {
		return true;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'CAM pool name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPoolSquareFeet() {
		$boolIsValid = true;

		 if( false == is_null( $this->getPoolSquareFeet() ) ) {
			 if( 0 >= $this->getPoolSquareFeet() ) {
				 $boolIsValid = false;
				 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pool_square_feet', __( 'CAM pool squarefeet cannot be zero or negative.' ) ) );
			 }
		 }
		return $boolIsValid;
	}

	public function valIsControllable() {
		return true;
	}

	public function getTotalAnnualBudget() {
		return $this->m_fltTotalAnnualBudget;
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function getIsActiveLease() {
		return $this->m_boolIsActiveLease;
	}

	public function setTotalAnnualBudget( $fltTotalAnnualBudget ) {
		$this->m_fltTotalAnnualBudget = $fltTotalAnnualBudget;
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->m_strEffectiveDate = $strEffectiveDate;
	}

	public function setIsActiveLease( $boolIsActiveLease ) {
		$this->m_boolIsActiveLease = $boolIsActiveLease;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPoolSquareFeet();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>
