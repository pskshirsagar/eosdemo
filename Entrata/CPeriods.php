<?php

use Psi\Libraries\Queue\Message\CMessageStatusMessage;

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPeriods
 * Do not add any new functions to this class.
 */

class CPeriods extends CBasePeriods {

	public static function fetchPaginatedPeriodsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objPagination, $objClientDatabase, $objPeriodClosingsFilter = NULL, $intOffSet = NULL, $intPageLimit = NULL, $intPeriodId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrstrOrConditions		= [];
		$arrstrAndConditions	= [];
		$arrstrSortBy			= [ 'property_name', 'post_month' ];

		$strOrderBy				= 'p.property_name ASC, pd.post_month DESC';

		if( true == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) && true == valStr( $objPeriodClosingsFilter->getSortBy() )
			&& true == in_array( $objPeriodClosingsFilter->getSortBy(), $arrstrSortBy ) ) {

			if( 'post_month' == $objPeriodClosingsFilter->getSortBy() ) {
				$strOrderBy = $objPeriodClosingsFilter->getSortBy() . ' ' . $objPeriodClosingsFilter->getSortDirection();
			} else {
				$strOrderBy = $objPeriodClosingsFilter->getSortBy() . ' ' . $objPeriodClosingsFilter->getSortDirection() . ', post_month DESC';
			}
		}

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) || false == valArr( $objPeriodClosingsFilter->getPropertyIds() ) ) {
			$strLoadPropertyJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( pd.cid = lp.cid AND lp.property_id = pd.property_id )';
		}

		if( true == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) ) {

			if( true == valArr( $objPeriodClosingsFilter->getPropertyIds() ) ) {
				$strLoadPropertyJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objPeriodClosingsFilter->getPropertyIds() ) . ' ] ) lp ON ( pd.cid = lp.cid AND lp.property_id = pd.property_id )';
			}

			if( true == valStr( $objPeriodClosingsFilter->getPostMonthSearch() ) && true == CValidation::validateDate( $objPeriodClosingsFilter->getPostMonthSearch() ) ) {
				$arrstrAndConditions[] = 'pd.post_month = \'' . $objPeriodClosingsFilter->getPostMonthSearch() . '\'';
			}

			if( 1 == $objPeriodClosingsFilter->getCurrentPeriod() ) {
				$arrstrOrConditions[] = ' pd.post_month IN( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month ) ';
			}

			if( 1 == $objPeriodClosingsFilter->getOpenPeriod() ) {
				$arrstrOrConditions[] = '(
											( pd.post_month < pgs.ar_post_month AND ar_locked_on IS NULL )
											OR ( pd.post_month < pgs.ap_post_month AND ap_locked_on IS NULL )
											OR ( pd.post_month < pgs.gl_post_month AND gl_locked_on IS NULL )
										)';
			}

			if( 1 == $objPeriodClosingsFilter->getClosedPeriod() ) {
				$arrstrOrConditions[] = ' COALESCE ( ar_locked_on, ap_locked_on, gl_locked_on ) IS NOT NULL ';
			}
		}

		$strCondition = NULL;

		if( true == valId( $intPeriodId ) ) {
			$strCondition = ' AND pd.id = ' . ( int ) $intPeriodId;
		}
		if( true == valArr( $arrstrAndConditions ) ) {
			$strCondition .= ' AND ' . implode( ' AND ', $arrstrAndConditions ) . '';
		}

		if( true == valArr( $arrstrOrConditions ) ) {
			$strCondition = $strCondition . ' AND ( ' . implode( ' OR ', $arrstrOrConditions ) . ' )';
		}

		$strSql = 'SELECT
						pd.id,
						pd.post_month,
						pd.property_id,
						ar_start_date,
						ap_start_date,
						gl_start_date,
						ar_end_date,
						ap_end_date,
						gl_end_date,
						ar_locked_on,
						ap_locked_on,
						gl_locked_on,
						is_ar_rollback,
						is_ap_rollback,
						is_gl_rollback,
						p.property_name,
						pgs.ar_post_month,
						pgs.ap_post_month,
						pgs.gl_post_month
					FROM
						periods pd ' . $strLoadPropertyJoin . '
						JOIN properties p ON lp.cid = p.cid AND lp.property_id = p.id
						JOIN property_gl_settings pgs ON pgs.cid = pd.cid AND pgs.property_id = pd.property_id 
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.post_month >= \'1/1/1970\'::DATE
						AND pd.post_month <= GREATEST( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month )
						' . $strCondition . '
					ORDER BY ' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		} else {
			$strSql .= ' OFFSET ' . ( int ) $intOffSet . ' LIMIT ' . ( int ) $intPageLimit;
		}

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objPeriodClosingsFilter = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrstrOrConditions		= [];
		$arrstrAndConditions	= [];

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) || false == valArr( $objPeriodClosingsFilter->getPropertyIds() ) ) {
			$strLoadPropertyJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( pd.cid = lp.cid AND lp.property_id = pd.property_id )';
		}

		if( true == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) ) {

			if( true == valArr( $objPeriodClosingsFilter->getPropertyIds() ) ) {
				$strLoadPropertyJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objPeriodClosingsFilter->getPropertyIds() ) . ' ] ) lp ON ( pd.cid = lp.cid AND lp.property_id = pd.property_id )';
			}

			if( true == valStr( $objPeriodClosingsFilter->getPostMonthSearch() ) && true == CValidation::validateDate( $objPeriodClosingsFilter->getPostMonthSearch() ) ) {
				$arrstrAndConditions[] = 'pd.post_month = \'' . $objPeriodClosingsFilter->getPostMonthSearch() . '\'';
			}

			if( 1 == $objPeriodClosingsFilter->getCurrentPeriod() ) {
				$arrstrOrConditions[] = '(
											( pd.post_month = pgs.ar_post_month )
											OR
											( pd.post_month = pgs.ap_post_month )
											OR
											( pd.post_month = pgs.gl_post_month )
										)';
			}

			if( 1 == $objPeriodClosingsFilter->getOpenPeriod() ) {
				$arrstrOrConditions[] = '(
											( pd.post_month < pgs.ar_post_month AND ar_locked_on IS NULL )
											OR
											( pd.post_month < pgs.ap_post_month AND ap_locked_on IS NULL )
											OR
											( pd.post_month < pgs.gl_post_month AND gl_locked_on IS NULL )
										)';
			}

			if( 1 == $objPeriodClosingsFilter->getClosedPeriod() ) {
				$arrstrOrConditions[] = '( ar_locked_on IS NOT NULL
											OR ap_locked_on IS NOT NULL
											OR gl_locked_on IS NOT NULL )';
			}
		}

		$strCondition = NULL;

		if( true == valArr( $arrstrAndConditions ) ) {
			$strCondition = ' AND ( ' . implode( ' AND ', $arrstrAndConditions ) . ' )';
		}

		if( true == valArr( $arrstrOrConditions ) ) {
			$strCondition .= ' AND ( ' . implode( ' OR ', $arrstrOrConditions ) . ' )';
		}

		$strSql = 'SELECT
						COUNT( pd.id )
					FROM
						periods pd ' . $strLoadPropertyJoin . '
						JOIN properties p ON ( lp.cid = p.cid AND lp.property_id = p.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.post_month > \'1/1/1970\'
						AND (
								( pd.post_month <= pgs.ar_post_month AND pd.is_ar_rollback = 0 )
								OR
								( pd.post_month <= pgs.ap_post_month AND pd.is_ap_rollback = 0 )
								OR
								( pd.post_month <= pgs.gl_post_month AND pd.is_gl_rollback = 0 )
							) ' . $strCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPeriodPostMonthsByPropertyIdsPeriodClosingsFilterByCid( $arrintPropertyIds, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
			|| ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
			|| false == valArr( $arrintPropertyIds ) ) {

			return NULL;
		}

		$strCondition = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition = $strCondition . '
							AND p.post_month = pgs.ar_post_month
							AND is_ar_rollback = 0';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition = $strCondition . '
							AND p.post_month = pgs.ap_post_month
							AND is_ap_rollback = 0';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition = $strCondition . '
							AND p.post_month = pgs.gl_post_month
							AND is_gl_rollback = 0';
		}

		$strSql = 'SELECT
						DISTINCT p.post_month
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strCondition . '
					ORDER BY
						p.post_month';

		$arrstrPostMonths = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrstrPostMonths ) ) {
			return NULL;
		}

		foreach( $arrstrPostMonths as $arrstrPostMonth ) {
			$arrstrResult[] = $arrstrPostMonth['post_month'];
		}

		return $arrstrResult;
	}

	public static function fetchLockedPeriodsByPeriodClosingsFilterByCid( $objPeriodClosingsFilter, $intCid, $objClientDatabase, $boolIsFromAp = false ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
			|| ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
			|| false == valArr( $objPeriodClosingsFilter->getPropertyIds() ) ) {

			return NULL;
		}

		$strCondition = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition = $strCondition . '
							AND p.post_month < pgs.ar_post_month
							AND ar_locked_on IS NOT NULL';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strLogicalOperator = ( true == $boolIsFromAp ) ? ' AND ( ' : ' AND ';
			$strCondition = $strCondition . $strLogicalOperator . '
							p.post_month < pgs.ap_post_month
							AND ap_locked_on IS NOT NULL';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLogicalOperator = ( true == $boolIsFromAp ) ? ' OR ' : ' AND ';
			$strEndingBracket = ( true == $boolIsFromAp ) ? ' )' : '';
			$strCondition = $strCondition . $strLogicalOperator . '
							p.post_month < pgs.gl_post_month
							AND gl_locked_on IS NOT NULL' . $strEndingBracket;
		}

		$strSql = 'SELECT
						p.*
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $objPeriodClosingsFilter->getPropertyIds() ) . ' )
						AND p.post_month = \'' . addslashes( $objPeriodClosingsFilter->getPostMonth() ) . '\'' .
						$strCondition;

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodByIdByCid( $intPeriodId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pd.*,
						p.property_name
					FROM
						periods pd
						JOIN properties p ON ( pd.cid = p.cid AND pd.property_id = p.id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.id = ' . ( int ) $intPeriodId;

		return self::fetchPeriod( $strSql, $objClientDatabase );
	}

	public static function fetchOpenPostMonthsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.post_month
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid ANd pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ( p.post_month < pgs.ar_post_month AND ar_locked_on IS NULL )
								OR ( p.post_month < pgs.ap_post_month AND ap_locked_on IS NULL )
								OR ( p.post_month < pgs.gl_post_month AND gl_locked_on IS NULL ) )
					ORDER BY
						p.post_month';

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrintResults	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			$arrintResults[] = $arrmixDatum['post_month'];
		}

		return $arrintResults;
	}

	public static function fetchLockedOrAdvancedPeriodsCountByPostMonthByChoreCategoryIdByPropertyIdsByCid( $strPostMonth, $intChoreCategoryId, $arrintPropertyIds, $intCid, $objClientDatabase, $boolIsWithPending = false, $boolIsAdvance = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == CValidation::valDate( $strPostMonth ) ) {
			return 0;
		}

		$strCondition = NULL;

		if( CChoreCategory::AR_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'AND ar_locked_on IS NOT NULL';
		} elseif( CChoreCategory::AP_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'AND ap_locked_on IS NOT NULL';
		} elseif( CChoreCategory::GL_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'AND gl_locked_on IS NOT NULL';
		}

		if( true == $boolIsAdvance && true == $boolIsWithPending ) {

			if( CChoreCategory::AR_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND is_ar_rollback = 0 AND p.post_month <= pgs.ar_post_month';
			} elseif( CChoreCategory::AP_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND is_ap_rollback = 0 AND p.post_month <= pgs.ap_post_month';
			} elseif( CChoreCategory::GL_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND is_gl_rollback = 0 AND p.post_month <= pgs.gl_post_month';
			}
		} elseif( true == $boolIsAdvance ) {

			if( CChoreCategory::AR_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.ar_post_month';
			} elseif( CChoreCategory::AP_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.ap_post_month';
			} elseif( CChoreCategory::GL_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.gl_post_month ';
			}
		} elseif( true == $boolIsWithPending ) {

			if( CChoreCategory::AR_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.ar_post_month';
			} elseif( CChoreCategory::AP_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.ap_post_month';
			} elseif( CChoreCategory::GL_CLOSE == $intChoreCategoryId ) {
				$strCondition = 'AND p.post_month < pgs.gl_post_month';
			}
		}

		$strSql = 'SELECT
						COUNT( p.id )
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.post_month = \'' . $strPostMonth . '\'
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						' . $strCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCurrentPeriodsForAdvanceByPropertyIdsByPostMonthByCid( $arrintPropertyIds, $strPostMonth, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						pgs.ar_post_month,
						pgs.ap_post_month,
						pgs.gl_post_month
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.post_month = \'' . $strPostMonth . '\'
						AND ( ( is_ar_rollback = 0 AND p.post_month = pgs.ar_post_month )
								OR ( is_ap_rollback = 0 AND p.post_month = pgs.ap_post_month )
								OR ( is_gl_rollback = 0 AND p.post_month = pgs.gl_post_month ) )';

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

    public static function fetchOpenPeriodsByPostMonthByPropertyIdsByPeriodClosingsFilterByCid( $strPostMonth, $arrintPropertyIds, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {
		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' ) || ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() ) || false == valArr( $arrintPropertyIds ) ) {

			return NULL;
		}

		$strCondition = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition = $strCondition . '
							AND pd.post_month <= pgs.ar_post_month';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition = $strCondition . '
							AND pd.post_month <= pgs.ap_post_month';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition = $strCondition . '
							AND pd.post_month <= pgs.gl_post_month';
		}

		$strSql = 'SELECT
						pd.*,
						p.property_name
					FROM
						periods pd
						JOIN properties p ON ( pd.cid = p.cid AND pd.property_id = p.id )
						LEFT JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pd.post_month = \'' . date( 'm-d-Y', strtotime( $strPostMonth ) ) . '\'' . $strCondition . '
					ORDER BY
						p.property_name';

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodIdsByPropertyIdsByPeriodClosingsFilterByCid( $arrintPropertyIds, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
			|| false == valStr( $objPeriodClosingsFilter->getPostMonth() )
			|| ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
			|| false == valArr( $arrintPropertyIds ) ) {

			return NULL;
		}

		$strCondition = NULL;
		$arrstrResult = [];

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition  = 'AND ( p.post_month <= pgs.ar_post_month OR p.post_month <= pgs.ap_post_month OR p.post_month <= pgs.gl_post_month )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition  = 'AND ( p.post_month <= pgs.ar_post_month OR p.post_month <= pgs.ap_post_month )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition  = 'AND ( p.post_month <= pgs.ap_post_month OR p.post_month <= pgs.gl_post_month )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition  = 'AND ( p.post_month <= pgs.ar_post_month OR p.post_month <= pgs.gl_post_month )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition  = 'AND p.post_month <= pgs.ar_post_month';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition  = 'AND p.post_month <= pgs.ap_post_month';
		} else {
			$strCondition  = 'AND p.post_month <= pgs.gl_post_month';
		}

		$strSql = 'SELECT
						p.id
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . '  )
						AND p.post_month = \'' . addslashes( $objPeriodClosingsFilter->getPostMonth() ) . '\'' .
						$strCondition;

		$arrintPeriodIds = fetchData( $strSql, $objClientDatabase );

		foreach( $arrintPeriodIds as $arrintPeriodId ) {
			$arrstrResult[] = $arrintPeriodId['id'];
		}

		return $arrstrResult;
	}

	public static function fetchPeriodsByPropertyIdsByPostMonthByCid( $arrintPropertyIds, $strPostMonth, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						periods
					WHERE
						cid = ' . ( int ) $intCid . '
						AND post_month = \'' . addslashes( $strPostMonth ) . '\'
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodsByIdsCid( $arrintPeriodIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPeriodIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						periods
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintPeriodIds ) . ' )';

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodByChoreIdByCid( $intChoreId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						chores c
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . ' )
						JOIN periods p ON ( cr.cid = p.cid AND cr.reference_id = p.id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.id = ' . ( int ) $intChoreId;

		return self::fetchPeriod( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodIdsByChoreTypeIdByChoreCategoryIdByCid( $intChoreTypeId, $intChoreCategoryId, $intCid, $objClientDatabase ) {

		if( CChoreCategory::AR_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'p.is_ar_rollback = 0 AND p.post_month <= pgs.ar_post_month AND p.ar_locked_on IS NULL';
		} elseif( CChoreCategory::AP_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'p.is_ap_rollback = 0 AND p.post_month <= pgs.ap_post_month AND p.ap_locked_on IS NULL';
		} elseif( CChoreCategory::GL_CLOSE == $intChoreCategoryId ) {
			$strCondition = 'p.is_gl_rollback = 0 AND p.post_month <= pgs.gl_post_month AND p.gl_locked_on IS NULL';
		} else {
			return NULL;
		}

		$strSql = 'SELECT
						p.id AS period_id
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.property_id = pgs.property_id AND pgs.activate_standard_posting = TRUE )
						JOIN chore_types ct ON ( ct.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ct.id = ' . ( int ) $intChoreTypeId . '
						AND NOT EXISTS (
										SELECT
											NULL
										FROM
											chores c
											JOIN chore_references cr ON ( cr.cid = p.cid AND cr.reference_id = p.id AND cr.chore_id = c.id )
										WHERE
											ct.cid = c.cid
											AND ct.id = c.chore_type_id
											AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . '
										)
						AND ' . $strCondition;

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrintResults	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			$arrintResults[] = $arrmixDatum['period_id'];
		}

		return $arrintResults;
	}

	public static function fetchPeriodByPropertyIdByPostMonthByCid( $intPropertyId, $strPostMonth, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						periods
					WHERE
						cid = ' . ( int ) $intCid . '
						AND post_month = \'' . addslashes( $strPostMonth ) . '\'
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchPeriod( $strSql, $objClientDatabase );
	}

	public static function fetchMissingPostMonthsByCid( $intCid, $objClientDatabase, $arrintPropertyIds = [] ) {

		$strPropertiesCondition = ( true == valArr( $arrintPropertyIds ) ) ? 'AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';

		$strSql = '
			SELECT
				pgs.property_id,
				MIN( post_month_range.post_month ) AS post_month
			FROM
				property_gl_settings pgs
				JOIN properties pr ON pr.id = pgs.property_id AND pr.is_disabled = 0 and pr.is_managerial = 0
				JOIN LATERAL
					(
						SELECT generate_series(
							LEAST( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month, \'1/1/2000\' ),
							GREATEST( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month, DATE_TRUNC( \'month\', CURRENT_DATE ) ) + INTERVAL \'6 month\',
							\'1 MONTH\'
						)::DATE AS post_month
					) AS post_month_range ON TRUE
				LEFT JOIN periods p ON p.cid = pgs.cid AND p.property_id = pgs.property_id AND p.post_month = post_month_range.post_month
			WHERE
				pgs.cid = ' . ( int ) $intCid . '
				AND pgs.activate_standard_posting = true
				AND p.id IS NULL
				' . $strPropertiesCondition . '
			GROUP BY
				pgs.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodsByAPAndGLStartDateByAPAndGLEndDateByPropertyIdsByCid( $strStartDate, $arrintPropertyIds, $boolIsLockedPeriod, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPropertyIds ) || false == valStr( $strStartDate ) || false == CValidation::validateDate( $strStartDate ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == $boolIsLockedPeriod ) {
			$strCondition .= '	AND ap_locked_on IS NULL
								AND gl_locked_on IS NULL';
		}

		$strSql = 'SELECT
						*
					FROM
						periods
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ap_start_date <= \'' . $strStartDate . '\'
						AND ap_end_date >= \'' . $strStartDate . '\'
						AND gl_start_date <= \'' . $strStartDate . '\'
						AND gl_end_date >= \'' . $strStartDate . '\'' .
						$strCondition;

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolIscheckForLock = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolIscheckForLock ) ? '' : ' AND ap_locked_on IS NULL AND gl_locked_on IS NULL';

		$strSql = 'SELECT
						*
					FROM
						periods
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						' . $strWhereCondition;

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchCustomOpenPostMonthsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.post_month
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid ANd pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.post_month <= pgs.ar_post_month 
						AND ar_locked_on IS NULL
					ORDER BY
						p.post_month';

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrintResults	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			$arrintResults[] = $arrmixDatum['post_month'];
		}

		return $arrintResults;
	}

	public static function fetchCustomClosedPostMonthsByPostMonthByPropertyIdByCid( $strPostMonth, $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.post_month
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid ANd pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id = ' . ( int ) $intPropertyId . '
						AND p.post_month = \'' . $strPostMonth . '\' 
						AND ar_locked_on IS NOT NULL';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrmixData ) ) {
			return array_column( $arrmixData, 'post_month' );
		}

		return false;
	}

	public static function fetchPeriodsForAdvanceOrLockByPropertyIdsByCid( $arrintPropertyIds, $intCid, CDatabase $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				p.*
			FROM
				periods p
				JOIN property_gl_settings pgs ON pgs.cid = p.cid AND pgs.property_id = p.property_id
			WHERE
				p.cid = ' . ( int ) $intCid . '
				AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				AND p.post_month BETWEEN LEAST( pgs.ar_lock_month, pgs.ap_lock_month, pgs.gl_lock_month ) - INTERVAL \'1 month\' 
									AND GREATEST( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month, date_trunc( \'month\', CURRENT_DATE ) ) + INTERVAL \'2 month\'';

		return self::fetchPeriods( $strSql, $objClientDatabase );
	}

	public static function fetchPeriodCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( p.id ), 
						p.property_id
					FROM
						periods p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.post_month > \'1/1/1970\'
						AND (
								( p.post_month <= pgs.ar_post_month AND is_ar_rollback = 0 )
								OR
								( p.post_month <= pgs.ap_post_month AND is_ap_rollback = 0 )
								OR
								( p.post_month <= pgs.gl_post_month AND is_gl_rollback = 0 )
							)
					GROUP BY 
						p.property_id';
			return $arrintPropertyWisePeriodCount = fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchOpenPeriodsByPropertyIdByPeriodClosingsFilterByCid( $intPropertyId, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
		    || ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
		    || false == valId( $intPropertyId ) || false == valStr( $objPeriodClosingsFilter->getPostMonth() ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strLockPostMonth = $strCondition  = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( pd.ar_locked_on IS NULL OR pd.ap_locked_on IS NULL OR pd.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month )';
			$strCondition  = 'AND ( pd.ar_locked_on IS NULL OR pd.ap_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( pd.ap_locked_on IS NULL OR pd.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( pd.ar_locked_on IS NULL OR pd.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strLockPostMonth = 'pgs.ar_lock_month';
			$strCondition  = 'AND pd.ar_locked_on IS NULL';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strLockPostMonth = 'pgs.ap_lock_month';
			$strCondition  = 'AND pd.ap_locked_on IS NULL';
		} else {
			$strLockPostMonth = 'pgs.gl_lock_month';
			$strCondition  = 'AND pd.gl_locked_on IS NULL';
		}

		$strLockPostMonth .= ' + INTERVAL \'1 MONTH\'';

		$strSql = 'SELECT 
						pd.*, pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month
					FROM
						periods pd
						JOIN property_gl_settings pgs ON( pd.cid = pgs.cid AND pd.property_id = pgs.property_id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.property_id = ' . ( int ) $intPropertyId . '
						AND pd.post_month BETWEEN ' . $strLockPostMonth . ' AND \'' . $objPeriodClosingsFilter->getPostMonth() . '\'
						' . $strCondition . '
					ORDER BY
						pd.post_month ASC';

		return self::fetchPeriods( $strSql, $objClientDatabase );

	}

	public static function fetchSearchCriteria( $objManagementFeeFilter, $boolShowDisabledData = false ) {

		if( false === valObj( $objManagementFeeFilter, 'CManagementFeesFilter' ) ) {
			return NULL;
		}

		$arrstrSqlCondition = [];

		if( valArr( $objManagementFeeFilter->getPropertyGroupIds() ?? NULL ) ) {
			$arrstrSqlCondition[] = 'p2.id IN ( SELECT property_id FROM load_properties ( ARRAY[ p2.cid ], ARRAY [ ' . implode( ',', ( array ) $objManagementFeeFilter->getPropertyGroupIds() ) . ' ] ) lp WHERE lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' )';
		}

		if( valArr( $objManagementFeeFilter->getFeeTemplateIds() ?? NULL ) ) {
			$arrstrSqlCondition[] = 'ft.id IN( ' . implode( ',', $objManagementFeeFilter->getFeeTemplateIds() ) . ' )';
		}

		if( valArr( $objManagementFeeFilter->getIntercompanyArEntityIds() ?? NULL ) ) {
			$arrstrSqlCondition[] = 'prop.id IN( ' . implode( ',', $objManagementFeeFilter->getIntercompanyArEntityIds() ) . ' ) AND ft.is_post_ar IS TRUE';
		}

		if( valArr( $objManagementFeeFilter->getPeriodIds() ?? NULL ) ) {
			$arrstrSqlCondition[] = 'p.id IN( ' . implode( ',', $objManagementFeeFilter->getPeriodIds() ) . ' )';
		}

		if( valId( $objManagementFeeFilter->getFeeStatusId() ?? NULL ) ) {
			if( CFee::PENDING_FEES == $objManagementFeeFilter->getFeeStatusId() ) {
				$arrstrSqlCondition[] = '( f.id IS NULL )';
			} elseif( CFee::COMPLETED_FEES == $objManagementFeeFilter->getFeeStatusId() ) {
				$arrstrSqlCondition[] = '( f.id IS NOT NULL AND f.status = \'' . CMessageStatusMessage::STATUS_COMPLETED . '\' )';
			} elseif( CFee::PROCESSING_FEES == $objManagementFeeFilter->getFeeStatusId() ) {
				$arrstrSqlCondition[] = 'f.status = \'' . CMessageStatusMessage::STATUS_PROCESSING . '\'';
			} elseif( CFee::FAILED_FEES == $objManagementFeeFilter->getFeeStatusId() ) {
				$arrstrSqlCondition[] = 'f.status = \'' . CMessageStatusMessage::STATUS_FAILED . '\'';
			} elseif( CFee::PENDING_AND_FAILED_FEES == $objManagementFeeFilter->getFeeStatusId() ) {
				$arrstrSqlCondition[] = '( f.id IS NULL OR f.errors IS NOT NULL )';
			}
		}

		if( true === $objManagementFeeFilter->getIsHideLockedApPeriod() ) {
			$arrstrSqlCondition[] = '( true = ft.is_post_to_current_ap_period OR ( p.post_month::DATE > ( select ap_lock_month from property_gl_settings pgs where p.property_id = pgs.property_id AND p.cid = pgs.cid ) ) )';
		}

		return $arrstrSqlCondition;
	}

	public function fetchFirstOpenDate( $intCid, $intPropertyId, $objDatabase ) {

		// get start date of oldest open period so that we can disallow editing of closed periods
		$strSql = '
			SELECT
				p.ar_start_date
			FROM
				property_gl_settings pgs
				JOIN periods p ON p.cid = pgs.cid AND p.property_id = pgs.property_id
					AND p.post_month = ( CASE WHEN pgs.activate_standard_posting = true THEN pgs.ar_lock_month ELSE pgs.ar_soft_lock_month END ) + INTERVAL \'1 month\'
			WHERE
				pgs.cid = ' . ( int ) $intCid . '
				AND pgs.property_id = ' . ( int ) $intPropertyId;
		$strDateOpenPeriodStartDate = CEosPluralBase::fetchColumn( $strSql, 'ar_start_date', $objDatabase );

		if( $strDateOpenPeriodStartDate == NULL ) {
			$objDateOpenPeriodStartDate = new DateTime( '1970-01-01' );
		} else {
			$objDateOpenPeriodStartDate = new DateTime( $strDateOpenPeriodStartDate );
		}

		return $objDateOpenPeriodStartDate;
	}

}