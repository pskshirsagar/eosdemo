<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentInsurancePolicyHistories
 * Do not add any new functions to this class.
 */

class CResidentInsurancePolicyHistories extends CBaseResidentInsurancePolicyHistories {

	public static function fetchResidentInsurancePolicyHistoryByPolicyIdByLeaseIdByCid( $objResidentInsurancePolicy, $objDatabase, $intResidentInsurancePolicyId = NULL ) {

		if( false == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) return NULL;

		$strAndResidentInsurancePolicyId = ( false == is_null( $intResidentInsurancePolicyId ) ) ? ' AND resident_insurance_policy_id = ' . ( int ) $intResidentInsurancePolicyId : '';

		$strSql = 'SELECT
						*
					FROM
						resident_insurance_policy_histories
					WHERE
						lease_id = ' . ( int ) $objResidentInsurancePolicy->getLeaseId() . '
						AND cid = ' . ( int ) $objResidentInsurancePolicy->getCid() . '
						AND carrier_policy_number = \'' . addslashes( $objResidentInsurancePolicy->getPolicyNumber() ) . '\'
						AND remote_primary_key is NOT NULL
						' . $strAndResidentInsurancePolicyId . '
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchResidentInsurancePolicyHistory( $strSql, $objDatabase );
	}

}
?>