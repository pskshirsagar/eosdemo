<?php

class CScreeningDocumentExportRequest extends CBaseScreeningDocumentExportRequest {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Invalid Client Id.' ) );
		}
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getPropertyIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_ids', 'Please Select Property.' ) );
		}
		return $boolIsValid;
	}

	public function valScreeningDocumentExportRequestStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToDate() {
		$boolIsValid = true;

		if( ( false == valStr( $this->getToDate() ) || false == valStr( $this->getFromDate() ) || 0 >= strlen( $this->getToDate() ) || 0 >= strlen( $this->getFromDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'From Date And To Date Cannot Be Empty.' ) );
		} else {
			if( strtotime( $this->getToDate() ) < strtotime( $this->getFromDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'To Date Should Be Greater Than From Date.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getFileTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', 'Invalid Screening Document.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyIds();
				$boolIsValid &= $this->valFileTypeId();
				$boolIsValid &= $this->valToDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>