<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetAssumptionMonths
 * Do not add any new functions to this class.
 */

class CBudgetAssumptionMonths extends CBaseBudgetAssumptionMonths {

	public static function fetchBudgetAssumptionMonthsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		if( false === valId( $intBudgetId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bam.*,
						ba.key
					FROM
						budget_assumption_months bam
						JOIN budget_assumptions ba ON ( ba.cid = bam.cid AND ba.id = bam.budget_assumption_id AND ba.budget_id = bam.budget_id )
					WHERE
						ba.cid = ' . ( int ) $intCid . '
						AND ba.budget_id = ' . ( int ) $intBudgetId;

		return self::fetchBudgetAssumptionMonths( $strSql, $objDatabase );

	}

}

?>