<?php

use Psi\Libraries\UtilErrorMsg\CErrorMsg;

class CMoveInScheduleGroup extends CBaseMoveInScheduleGroup {

	protected $m_intPropertyId;

	public function valMoveInScheduleTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getMoveInScheduleTypeId() ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'schedule_type_id', __( 'Please select schedule type.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( false === valStr( $this->getName() ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Move-in schedule name should not be empty.' ) ) );
			$boolIsValid = false;
		} else {
			if( false !== valStr( $this->getName() ) ) {
				$intMoveInscheduleGroupCount = \Psi\Eos\Entrata\CMoveInScheduleGroups::createService()->checkMoveInScheduleGroupsNameExistByNameByPropertyIdByCid( $this->getName(), $this->getPropertyId(), $this->getCid(), $objDatabase, $this->getId() );
				if( true == valId( $intMoveInscheduleGroupCount ) ) {
					$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Identical schedule names may not be used.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valMoveInScheduleTypeId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/***
	 * Set unctions
	 */

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId, NULL, true );
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, true );
	}

	public function setMoveInScheduleGroupDefaultValuesByScheduleType( $boolIsConventionalMoveInSchedule = false, $arrmixGroupDefaultData = [] ) {
		if( true == $boolIsConventionalMoveInSchedule ) {
			$this->setOccupancyTypeId( COccupancyType::CONVENTIONAL );
			$this->setName( CMoveInSchedule::CONVENTIONAL_MOVE_IN_SCHEDULE_NAME );
			$this->setMoveInScheduleTypeId( CMoveInScheduleType::PROPERTY );
			$this->setUseAppointments( 1 );
		} else {
			$this->setOccupancyTypeId( COccupancyType::STUDENT );

			if( true == valArr( $arrmixGroupDefaultData ) ) {
				$this->setName( $arrmixGroupDefaultData['name'] );
				$this->setMoveInScheduleTypeId( $arrmixGroupDefaultData['schedule_type_id'] );
				$this->setUseAppointments( $arrmixGroupDefaultData['use_appointments'] );
			}

		}
	}

	/***
	 * Get functions
	 */

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	/***
	 *  Set Values
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lease_term_id'] ) ) {
			$this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		}
		if( true == isset( $arrmixValues['lease_start_window_id'] ) ) {
			$this->setLeaseStartWindowId( $arrmixValues['lease_start_window_id'] );
		}
	}

	/***
	 *  Other functions
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}

?>
