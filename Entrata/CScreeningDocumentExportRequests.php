<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDocumentExportRequests
 * Do not add any new functions to this class.
 */

class CScreeningDocumentExportRequests extends CBaseScreeningDocumentExportRequests {

	public static function fetchInProgressScreeningDocumentExportRequest( $objDatabase ) {

		$strSql = 'SELECT
						sder.*,
						array_to_json( sder.property_ids ) as property_ids,
						ft.system_code
					FROM
						screening_document_export_requests sder
						JOIN file_types ft ON ft.id = sder.file_type_id 
					WHERE
						sder.screening_document_export_request_status_type_id = ' . CScreeningDocumentExportRequestStatusType::SCREENING_DOCUMENT_EXPORT_REQUEST_STATUS_TYPE_IN_PROGRESS . '
						AND sder.is_active = true';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequests( sprintf( 'SELECT * FROM screening_document_export_requests WHERE cid = %d AND is_active = true ORDER BY id DESC', $intCid ), $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestByIdByCid( $intScreeningDocumentExportRequestId, $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequest( sprintf( 'SELECT * FROM screening_document_export_requests WHERE id = %d and cid = %d ORDER BY id DESC', $intScreeningDocumentExportRequestId, $intCid ), $objDatabase );
	}

}
?>