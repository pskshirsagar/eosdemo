<?php

class CDefaultGlTree extends CBaseDefaultGlTree {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlTreeTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultGlChartId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRestrictedDefaultGlTreeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAcccountNumberPattern() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccountNumberDelimiter() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>