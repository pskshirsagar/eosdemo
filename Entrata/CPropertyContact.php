<?php

class CPropertyContact extends CBasePropertyContact {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyContactTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisplayName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getPhoneNumber() ) ) return $boolIsValid;

		$intPhoneNumberLength = \Psi\CStringService::singleton()->strlen( $this->getPhoneNumber() );

		if( false == \Psi\CStringService::singleton()->preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid.' ) ) );
		} elseif( 0 < $intPhoneNumberLength && 10 > $intPhoneNumberLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number must be {%d, 0} digits.', [ 10 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getFaxNumber() ) ) return $boolIsValid;

		$intFaxLength = \Psi\CStringService::singleton()->strlen( $this->getFaxNumber() );

		if( false == \Psi\CStringService::singleton()->preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->getFaxNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Fax number is not valid.' ) ) );
		} elseif( 0 < $intFaxLength && 10 > $intFaxLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Fax number must be {%d, 0} digits.', [ 10 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getPostalCode() ) ) return $boolIsValid;

		$intPostalCodeLength = mb_strlen( str_replace( '-', '', $this->getPostalCode() ) );

		if( 0 < $intPostalCodeLength && CCountry::CODE_USA == $this->getCountryCode() && ( false == \Psi\CStringService::singleton()->preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getPostalCode() ) && false == \Psi\CStringService::singleton()->preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code must be {%d, 0} or {%d, 1} characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.', [ 5, 10 ] ) ) );
		}

		if( 0 < $intPostalCodeLength && CCountry::CODE_CANADA == $this->getCountryCode() && ( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z]{1}\d{1}[a-zA-Z]{1} \d{1}[a-zA-Z]{1}\d{1}$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code must be {%d, 0} characters in XXX XXX format and should contain only alphanumeric characters.', [ 6 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getEmailAddress() ) ) return $boolIsValid;

		if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaxProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valEmailAddress();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>