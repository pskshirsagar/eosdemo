<?php

class CReservationFeeOccurrenceType extends CBaseReservationFeeOccurrenceType {

	const PER_HOUR			= 1;
	const PER_RESRVATION 	= 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'RESERVATION_FEE_OCCURRENCE_TYPE_PER_HOUR', self::PER_HOUR );
		$objSmarty->assign( 'RESERVATION_FEE_OCCURRENCE_TYPE_PER_RESRVATION', self::PER_RESRVATION );
	}
}
?>