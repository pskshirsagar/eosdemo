<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportVersions
 * Do not add any new functions to this class.
 */

class CDefaultReportVersions extends CBaseDefaultReportVersions {

	public static function fetchDefaultReportVersions( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CDefaultReportVersion', $objDatabase );
	}

	public static function fetchDefaultReportVersion( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CDefaultReportVersion', $objDatabase );
	}

	public static function fetchDefaultReportVersionsByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		$strSql = '
			SELECT
				*
			FROM
				default_report_versions drv
			WHERE
				drv.default_report_id = ' . ( int ) $intDefaultReportId . '
			ORDER BY
				drv.major DESC,
				drv.minor DESC
		';

		return self::fetchDefaultReportVersions( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportVersionsByDefaultReportIds( $arrintDefaultReportIds, $objDatabase ) {
		if( false == valArr( $arrintDefaultReportIds ) ) return;

		$strSql = '
			SELECT
				*
			FROM
				default_report_versions drv
			WHERE
				drv.default_report_id IN  ( ' . implode( ', ', $arrintDefaultReportIds ) . ' )
			ORDER BY
				drv.major DESC,
				drv.minor DESC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportVersionMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				default_report_versions';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportVersionByDefaultReportIdByMajorByMinor( $intDefaultReportId, $intMajor, $intMinor, $objDatabase ) {

			$strSql = '
				SELECT
					id
				FROM
					default_report_versions
				WHERE
					default_report_id = ' . ( int ) $intDefaultReportId . ' AND major =' . ( int ) $intMajor . ' AND minor = ' . ( int ) $intMinor;

			return self::fetchDefaultReportVersion( $strSql, $objDatabase );

	}

	public static function fetchDefaultReportVersionIsDefaultByDefaultReportId( $intDefaultReportId, $objDatabase ) {

		$strSql = '
				SELECT
					*
				FROM
					default_report_versions
				WHERE
					default_report_id = ' . ( int ) $intDefaultReportId . '
					AND is_default = TRUE';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDefaultReportVersionChangeLogById( $intDefaultReportVersionId, $objDatabase ) {
		$strSql = '
				SELECT
					rvc.id
				FROM
					default_report_versions drv
					JOIN report_version_changes rvc ON rvc.default_report_version_id = drv.id
				WHERE
					drv.id = ' . ( int ) $intDefaultReportVersionId . '
					AND rvc.deleted_by IS NULL
					AND rvc.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportVersionByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		$strSql = '
			SELECT
				drv.*
			FROM
				default_report_versions AS drv
			WHERE
				drv.default_report_id = ' . ( int ) $intDefaultReportId . '
				AND drv.is_default = TRUE';

		return self::fetchDefaultReportVersion( $strSql, $objDatabase );
	}

}
