<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPaymentTypes
 * Do not add any new functions to this class.
 */

class CPropertyPaymentTypes extends CBasePropertyPaymentTypes {

	/**
	 * @param bool|null $boolIsAllowInEntrata If NULL then this option will be ignored (do not apply any additional conditions)
	 * @param bool|null $boolAllowInResidentPortal If NULL then this option will be ignored (do not apply any additional conditions)
	 * @param bool|null $boolAllowInProspectPortal If NULL then this option will be ignored (do not apply any additional conditions)
	 * @param bool|null $mixRequireCertifiedFunds
	 * 				If NULL then this option will be ignored (do not apply any additional conditions).
	 * 				If TRUE::bool or FALSE::bool then we apply is_certified_funds condition for this value
	 * 				If Array() then we check for payment_type_id keys and apply their boolean values as conditions.
	 */
	public static function fetchPropertyPaymentTypesByAllowInEntrataByAllowInResidentPortalByAllowInProspectPortalByIsCertifiedFundsByPropertyIdByCid( $boolIsAllowInEntrata, $boolAllowInResidentPortal, $boolAllowInProspectPortal, $mixRequireCertifiedFunds, $intPropertyId, $intCid, $objDatabase ) {
		$strWhereConditions = sprintf( '
					ppt.property_id = %d
					AND ppt.cid = %d',
			( int ) $intPropertyId,
			( int ) $intCid );

		if( NULL !== $boolIsAllowInEntrata ) {
			$strWhereConditions .= " AND ppt.allow_in_entrata = '" . CStrings::strToBool( $boolIsAllowInEntrata ) . "'";
		}

		if( NULL !== $boolAllowInResidentPortal ) {
			$strWhereConditions .= " AND ppt.allow_in_resident_portal = '" . CStrings::strToBool( $boolAllowInResidentPortal ) . "'";
		}

		if( NULL !== $boolAllowInProspectPortal ) {
			$strWhereConditions .= " AND ppt.allow_in_prospect_portal = '" . CStrings::strToBool( $boolAllowInProspectPortal ) . "'";
		}

		if( true == is_bool( $mixRequireCertifiedFunds ) ) {
			$strWhereConditions .= " AND ppt.is_certified_funds = '" . ( ( false == CStrings::strToBool( $mixRequireCertifiedFunds ) ) ? 0 : 1 ) . "'";
		} elseif( true == valArr( $mixRequireCertifiedFunds ) ) {
			$arrstrCertifiedConditions			= [];
			$arrintCertifiedPaymentTypes		= array_keys( $mixRequireCertifiedFunds, true );
			$arrintNotCertifiedPaymentTypes		= array_keys( $mixRequireCertifiedFunds, false );
			// If set to NULL or just not specified then we don't check the is_certified_funds value
			$arrintAllSpecifiedPaymentTypes		= array_merge( $arrintCertifiedPaymentTypes, $arrintNotCertifiedPaymentTypes );

			if( false == empty( $arrintCertifiedPaymentTypes ) ) $arrstrCertifiedConditions[] = '( ppt.payment_type_id IN ( ' . implode( ',', $arrintCertifiedPaymentTypes ) . ' ) AND ppt.is_certified_funds = TRUE )';
			if( false == empty( $arrintNotCertifiedPaymentTypes ) ) $arrstrCertifiedConditions[] = '( ppt.payment_type_id IN ( ' . implode( ',', $arrintNotCertifiedPaymentTypes ) . ' ) AND ppt.is_certified_funds = FALSE )';
			if( false == empty( $arrintAllSpecifiedPaymentTypes ) ) $arrstrCertifiedConditions[] = 'ppt.payment_type_id NOT IN ( ' . implode( ',', $arrintAllSpecifiedPaymentTypes ) . ' )';

			$strWhereConditions .= ' AND ( ' . implode( ' OR ', $arrstrCertifiedConditions ) . ' ) ';
		}

		$strSql = '
				SELECT
					ppt.*,
					pt.name as payment_type_name
				FROM property_payment_types AS ppt
					JOIN payment_types AS pt ON( pt.id = ppt.payment_type_id AND pt.is_published = 1 )
				WHERE' . $strWhereConditions;

		return self::fetchPropertyPaymentTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyPaymentTypesDefaultArCodeArCodeCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintExcludedPaymentTypeIds = [], $boolIsSortByOrderNum = false ) {

		$strCondition = ( true == valArr( $arrintExcludedPaymentTypeIds ) ) ? ' AND pt.id NOT IN ( ' . implode( ', ', $arrintExcludedPaymentTypeIds ) . ' ) ' : '';

		if( true == $boolIsSortByOrderNum ) {
			$strOrderBy = 'pt.order_num ASC';
		} else {
			$strOrderBy = 'pt.id ASC';
		}

		$strSql = sprintf( '
				WITH sub AS(
						SELECT
							pptac.cid,
							pptac.property_id,
							pptac.payment_type_id,
							COUNT( pptac.id ) AS ar_code_count,
							MAX( CASE
									WHEN pptac.is_default = TRUE THEN pptac.ar_code_id
									ELSE NULL
								END ) AS default_ar_code_id
						FROM property_payment_type_ar_codes AS pptac
							INNER JOIN view_ar_codes AS vac ON(
										vac.property_id = pptac.property_id
										AND vac.cid = pptac.cid
										AND vac.id = pptac.ar_code_id
										AND vac.is_disabled::boolean = FALSE
									)
						WHERE
							pptac.property_id = %d
							AND pptac.cid = %d
						GROUP BY pptac.cid, pptac.property_id, pptac.payment_type_id
					)
				SELECT
					ppt.*,
					util_get_system_translated( \'name\', pt.name, pt.details ) AS payment_type_name,
					COALESCE( sub.ar_code_count, 0 ) AS ar_code_count,
					COALESCE( sub.default_ar_code_id, NULL ) AS default_ar_code_id
				FROM property_payment_types AS ppt
					JOIN payment_types AS pt ON( pt.id = ppt.payment_type_id AND pt.is_published = 1 )
					LEFT JOIN sub ON(
							sub.payment_type_id = ppt.payment_type_id
						)
				WHERE
					ppt.property_id = %d
					AND ppt.cid = %d
					' . $strCondition . '
				ORDER BY ' . $strOrderBy,
			( int ) $intPropertyId,
			( int ) $intCid,
			( int ) $intPropertyId,
			( int ) $intCid );

		return self::fetchPropertyPaymentTypes( $strSql, $objDatabase );
	}

	public static function fetchPaymentIdsByCertifiedFundsByPropertyId( $intPropertyId, $objDatabase ) {
		$strSql = 'SELECT payment_type_id from property_payment_types where is_certified_funds = FALSE and property_id= ' . ( int ) $intPropertyId;
		$arrintPaymentTypes = fetchData( $strSql, $objDatabase );
		$arrintFinalData = [];

		if( true == valArr( $arrintPaymentTypes ) ) {
			foreach( $arrintPaymentTypes as $arrintPaymentType ) {
				$arrintFinalData[$arrintPaymentType['payment_type_id']] = $arrintPaymentType['payment_type_id'];
			}
		}

		return $arrintFinalData;
	}

	public static function fetchCustomPropertyPaymentTypesByPropertyIdsByCid( $arrintPaymentTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ppt.cid,
						ppt.property_id,
						array_to_string( array_agg( ppt.payment_type_id ORDER BY payment_type_id ), \',\' ) as payment_type_ids
					FROM
						property_payment_types ppt
					WHERE
						ppt.cid = ' . ( int ) $intCid . '
						AND  ppt.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ')
						AND ppt.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND ppt.allow_in_entrata = TRUE
					 GROUP BY
						ppt.cid,
						ppt.property_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPaymentTypesByPaymentTypeIdsByAllowInEntrataByCid( $arrintPaymentTypeIds, $boolIsAllowInEntrata, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ppt.property_id,
						ppt.payment_type_id
					FROM
						property_payment_types ppt
					WHERE
						ppt.cid = ' . ( int ) $intCid . '
						AND ppt.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' )
						AND ppt.allow_in_entrata = \'' . CStrings::strToBool( $boolIsAllowInEntrata ) . '\'';

		$arrintPaymentTypes = fetchData( $strSql, $objDatabase );
		$arrintFinalData    = [];

		if( true == valArr( $arrintPaymentTypes ) ) {
			foreach( $arrintPaymentTypes as $arrintPaymentType ) {
				$arrintFinalData[$arrintPaymentType['property_id']][$arrintPaymentType['payment_type_id']] = $arrintPaymentType['payment_type_id'];
			}
		}

		return $arrintFinalData;
	}

	public static function fetchPropertyPaymentTypesByCidByPropertyIdByPaymentTypeIdsByAllowInEntrata( $intCid, $intPropertyId, $arrintPaymentTypeIds, $boolIsAllowInEntrata, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valArr( $arrintPaymentTypeIds ) || false == is_bool( $boolIsAllowInEntrata ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ppt.id,
						ppt.payment_type_id
					FROM
						property_payment_types AS ppt
					WHERE
						ppt.cid = ' . ( int ) $intCid . '
						AND ppt.property_id = ' . ( int ) $intPropertyId . '
						AND ppt.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' )
						AND ppt.allow_in_entrata = \'' . CStrings::strToBool( $boolIsAllowInEntrata ) . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPaymentTypesByPaymentTypeIdsByAllowInResidentPortalByCidByPropertyIds( $arrintPaymentTypeIds, $boolAllowInResidentPortal, $arrintPropertyIds, $intCid, $objDatabase ) {
		$arrintFinalData = [];
		if( false == valArr( $arrintPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return $arrintFinalData;
		}
		$strSql = 'SELECT
						ppt.property_id,
						ppt.payment_type_id
					FROM
						property_payment_types ppt
					WHERE
						ppt.cid = ' . ( int ) $intCid . '
						AND ppt.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ppt.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' )';

		if( false != $boolAllowInResidentPortal ) {
		    $strSql .= ' AND ppt.allow_in_resident_portal = \'' . CStrings::strToBool( $boolAllowInResidentPortal ) . '\'';
		}

		$arrintPaymentTypes = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintPaymentTypes ) ) {
			foreach( $arrintPaymentTypes as $arrintPaymentType ) {
				$arrintFinalData[$arrintPaymentType['property_id']][$arrintPaymentType['payment_type_id']] = $arrintPaymentType['payment_type_id'];
			}
		}

		return $arrintFinalData;
	}

}
?>