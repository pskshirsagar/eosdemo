<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFiles
 * Do not add any new functions to this class.
 */

class CFiles extends CBaseFiles {

	public static function fetchFilefetchFileByRemotePrimaryKeyByCid( $intCid, $strFileRemotePrimaryKey, $objDatabase ) {
		$strSql = 'SELECT * FROM
						files
					WHERE
						remote_primary_key = \'' . addslashes( $strFileRemotePrimaryKey ) . '\'
						AND cid = ' . ( int ) $intCid . '
                    LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedFilesByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intMaintenanceRequestId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
					WHERE
						fa.maintenance_request_id = ' . ( int ) $intMaintenanceRequestId . '
						AND fa.deleted_by IS NULL
						AND f.remote_primary_key IS NULL
						AND f.cid = ' . ( int ) $intCid;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByInspectionIdByInspectionFormLocationProblemIdByCid( $intInspectionId, $intInspectionFormLocationProblemId, $intCid, $objDatabase, $intInspectionResponseId = NULL ) {

		$intPropertyUnitLocationId = NULL;

		if( true == preg_match( '/_/', $intInspectionFormLocationProblemId ) ) {
			$arrintLocationProblem = explode( '_', $intInspectionFormLocationProblemId );
			$intInspectionFormLocationProblemId = $arrintLocationProblem[0];
			$intPropertyUnitLocationId = $arrintLocationProblem[1];
		}

		 $strSql = 'SELECT
						f.id,
						f.file_name,
						f.file_extension_id,
						f.file_size,
						f.file_upload_date,
						f.file_path,
						ir.id AS responce_id,
						fa.id AS file_association_id ,
						ir.inspection_form_location_problem_id AS inspection_form_location_problem_id,
                            ( SELECT COUNT(1) FROM file_associations fa1 WHERE fa1.cid=ir.cid AND fa1.file_id =
                            f.id ) AS action_count
					FROM inspection_responses ir
                         INNER JOIN file_associations fa ON ir.cid = fa.cid AND ir.id = fa.inspection_response_id
                         INNER JOIN files f ON fa.cid = f.cid AND fa.file_id = f.id
                    WHERE	ir.cid = ' . ( int ) $intCid . '
							AND ir.inspection_id = ' . ( int ) $intInspectionId . '
							AND ir.inspection_form_location_problem_id=' . ( int ) $intInspectionFormLocationProblemId .
							( ( true == valId( $intPropertyUnitLocationId ) ? ' AND ir.property_unit_maintenance_location_id =' . ( int ) $intPropertyUnitLocationId : '' ) .
							( ( true == valId( $intInspectionResponseId ) ? ' AND ir.id =' . ( int ) $intInspectionResponseId : '' ) ) );

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByApplicantIdByFileTypeSystemCodeByFileExtensionIdByCid( $intApplicationId, $intApplicantId, $strSystemCode, $intExtensionId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						f.*
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN file_extensions fe ON (fe.id = f.file_extension_id )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id = ' . ( int ) $intApplicantId . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
						AND f.file_extension_id = ' . ( int ) $intExtensionId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileByCustomerIdByFileTypeSystemCodeByCid( $intCustomerId, $strSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						f.*
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.customer_id = ' . ( int ) $intCustomerId . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdsByExcludingFileTypeSystemCodesByCid( $arrintApplicationIds, $arrstrSystemCodes, $intCid, $objDatabase, $boolIsIncludeCancelLeaseFiles = false, $boolIsResidentVerifyEnabledProperty = false ) {
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						fa.lease_id,
						CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									ELSE \'' . __( 'e-Sign: Lease Modification' ) . '\'
								END
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
                                    \'' . __( 'Application: Policy' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
                                    \'' . __( 'e-Sign: Addenda' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
                                    \'' . __( 'Automated' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
                                     \'' . __( 'Application Upload' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
                                    \'' . __( 'Renewal Offer Sent' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
                                     \'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
                                     \'' . __( 'Transfer Document' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_WORK_ORDER . '\', \'' . CFileType::SYSTEM_CODE_WAITLIST . '\' ) OR ft.system_code = ANY( ARRAY[ \'' . implode( "', '", CFileType::$c_arrstrAffordableFileTypes ) . '\' ] ) THEN
                                     \'\'
	                        ELSE \'' . __( 'Upload' ) . '\'
						END AS file_type_name,
						fa.file_signed_on,
						fa.sign_initiated_on,
						fa.application_id AS application_id,
						fa.require_sign AS require_sign,
						fa.deleted_by';

						if( true == $boolIsResidentVerifyEnabledProperty ) {
							$strSql .= ', d.document_sub_type_id, fa.applicant_id';
						}

		$strSql .= '
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )';

					if( true == $boolIsResidentVerifyEnabledProperty ) {
						$strSql .= ' LEFT JOIN documents d ON ( f.document_id = d.id AND f.cid = d.cid )';
					}

		$strSql .= '
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN events e ON ( e.event_type_id = ' . CEventType::DOCUMENT_DELETED . ' AND e.data_reference_id = f.id AND e.cid = f.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )
					WHERE
						fa.application_id IN ( \'' . implode( '\',\'', $arrintApplicationIds ) . '\' )
						AND CASE WHEN ( e.event_type_id = ' . CEventType::DOCUMENT_DELETED . ' ) THEN
								e.data_reference_id = f.id AND ( fa.deleted_by IS NULL AND e.id IS NOT NULL )
							ELSE
								true
							END
						AND CASE WHEN ( f.file_id IS NOT NULL ) THEN
								ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\'
							ELSE
								true
							END
						AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) OR ft.system_code IS NULL )
						AND fa.cid = ' . ( int ) $intCid;

						if( false == $boolIsIncludeCancelLeaseFiles ) {
							$strSql .= ' AND fa.deleted_by IS NULL';
						}

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicantIdsByExcludingFileTypeSystemCodesByCid( $arrintApplicantIds, $arrstrFileTypeSystemCodes, $intCid, $objDatabase, $boolShowInPortal = false, $boolIncludeDeletedAA = false, $intFileId = NULL ) {
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (f.id) f.*
						, ft.system_code AS file_type_system_code
						, ft.name AS file_type_name
						, fa.application_id AS application_id
						, fa.applicant_id AS applicant_id
						, fa.require_sign AS require_sign
						, fa.customer_id AS customer_id
						, fa.file_signed_on AS file_signed_on
						, fa.id AS file_association_id
						, COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
						, fa.ip_address
						, d.is_bluemoon
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid)
						JOIN applicant_applications aa ON ( fa.applicant_id = aa.applicant_id AND fa.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						LEFT JOIN documents d ON ( d.cid = f.cid AND d.id = f.document_id )
					WHERE
						fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' ) OR ft.system_code IS NULL )
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL';

		if( true == $boolShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		if( NULL != $intFileId ) {
			$strSql .= ' AND f.id = ' . ( int ) $intFileId;
		}

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicantIdsByCustomerIdsByLeaseIdsByExcludingFileTypeSystemCodesByCid( $arrintApplicantIds, $arrintCustomerIds, $arrintLeaseIds, $arrstrFileTypeSystemCodes, $intCid, $objDatabase, $boolShowInPortal = false, $boolSeparateLeaseDocuments = false, $boolIsShowDeletedDocuments = false, $boolIsFromResidentPortal = false, $intFileId = NULL ) {
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) {
			return NULL;
		}
		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::LEASE_MODIFICATION . '\' THEN \'' . __( 'e-Sign: Lease Modifications' ) . '\'
									ELSE \'' . __( 'e-Sign: Replace Lease' ) . '\'
								END
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
                                    \'' . __( 'Application: Policy' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
                                    \'' . __( 'e-Sign: Addenda' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
                                    \'' . __( 'Automated' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
                                     \'' . __( 'Application Upload' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
                                    \'' . __( 'Renewal Offer Sent' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
                                     \'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
                                     \'' . __( 'Transfer Document' ) . '\'
	                        WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_WAITLIST . '\' ) OR ft.system_code = ANY( ARRAY[ \'' . implode( "', '", CFileType::$c_arrstrAffordableFileTypes ) . '\' ] ) THEN
	                                  \'\'
	                        ELSE \'' . __( 'Upload' ) . '\'
						END AS file_type_name,
						fa.id AS file_association_id,
						fa.customer_id AS customer_id,
						fa.lease_id AS lease_id,
						fa.require_sign,
						fa.file_signed_on,
						fa.sign_initiated_on,
						fa.application_id AS application_id,
						fa.applicant_id AS applicant_id,
						fa.deleted_by,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name,
						fa.ip_address,
						d.is_bluemoon';

		if( true == $boolSeparateLeaseDocuments ) {
			$strSql .= ' ,
					    CASE WHEN ft.system_code = \'LEASE\' AND tfa.total_file_associations_count = sfa.signed_file_associations_count THEN
					         \'eSign: Lease\'
					    WHEN ft.system_code = \'LEASE\' AND tfa.total_file_associations_count > sfa.signed_file_associations_count AND 0 < sfa.signed_file_associations_count THEN
					        \'Partially-eSigned\'
					    WHEN ft.system_code = \'LEASE\' THEN
					        \'Pre-signed\'
					    END AS file_generation_type';
		}

		$strSql .= ' FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' ) OR ft.system_code IS NULL ) )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid ';
		$strSql .= ( true == valArr( $arrintCustomerIds ) && true == $boolIsFromResidentPortal ) ? ' AND fa.customer_id IS NOT NULL AND fa.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) )' : ' ) ';
        $strSql .= ' JOIN lease_customers lc ON ( fa.lease_id = lc.lease_id AND fa.cid = lc.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )';
		if( true == $boolSeparateLeaseDocuments ) {
			$strSql .= ' LEFT JOIN ( SELECT
                                        file_id,
										cid,
                                        COUNT(id) AS total_file_associations_count
					                 FROM
					                     file_associations
					                 WHERE
					                     applicant_id IS NOT NULL
					                     AND deleted_by IS NULL
					                     AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
					                     AND cid = ' . ( int ) $intCid . '
					                     GROUP BY file_id,
					                              cid
					                ) AS tfa ON ( tfa.file_id = f.id AND tfa.cid = f.cid )
				       LEFT JOIN ( SELECT
				                      isfa.file_id AS file_id,
					                  isfa.cid,
				                      COUNT(file_association_id) AS signed_file_associations_count
				                  FROM (
				                          SELECT
				                              ifa.id AS file_association_id,
				                              ifa.file_id,
					                          ifa.cid,
				                              ifa.applicant_id,
				                              ifs.page_number,
				                              ifs.agreement_datetime AS agreement_datetime,
				                              rank() OVER ( PARTITION BY ifa.id ORDER BY ifs.page_number DESC )
				                          FROM
				                              file_associations ifa
				                              JOIN file_signatures ifs ON ( ifs.file_association_id = ifa.id AND ifs.cid = ifa.cid )
				                          WHERE
				                              ifa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
				                              AND ifa.cid = ' . ( int ) $intCid . '
				                              AND ifa.applicant_id IS NOT NULL
				                              AND ifa.deleted_by IS NULL
				                      ) AS isfa
				                  WHERE
				                      isfa.rank = 1
				                      AND isfa.agreement_datetime IS NOT NULL
				                      AND isfa.cid = ' . ( int ) $intCid . '
				                  GROUP BY
				                      file_id,
				                      cid
				                ) AS sfa ON ( sfa.file_id = f.id AND sfa.cid = f.cid )';
		}

        $strSql .= 'LEFT JOIN documents d ON ( d.cid = f.cid AND d.id = f.document_id )';

		$strSql .= ' WHERE
						fa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		if( false == $boolIsShowDeletedDocuments ) {
			$strSql .= ' AND fa.deleted_by IS NULL';
		}

		$strSql .= ' AND CASE WHEN ( f.file_id IS NOT NULL ) THEN
								( ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\' ';

								if( true == $boolShowInPortal ) {
									$strSql .= ' OR ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ';
								}

								$strSql .= ' )
							ELSE
								true
							END
						AND ( ( fa.customer_id IS NULL AND fa.applicant_id IS NULL )';

		$strSql .= ( true == valArr( $arrintApplicantIds ) ) ? ' OR fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )' : '';
		$strSql .= ( true == valArr( $arrintCustomerIds ) ) ? ' OR fa.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )' : '';

		$strSql .= ' ) AND f.cid = ' . ( int ) $intCid;

		if( true == $boolShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		if( NULL != $intFileId ) {
			$strSql .= ' AND f.id = ' . ( int ) $intFileId;
		}

		$strSql .= ' ORDER BY f.id ASC';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesCountByApplicantIdsByLeaseIdsByExcludingFileTypeSystemCodesByCid( $arrintApplicantIds, $arrintLeaseIds, $arrstrFileTypeSystemCodes, $intCid, $objDatabase, $boolShowInPortal = false ) {

		if( false == valArr( $arrstrFileTypeSystemCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT(f.id)
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' ) OR ft.system_code IS NULL )
						AND fa.deleted_by IS NULL';

		if( true == valArr( $arrintApplicantIds ) && true == valArr( $arrintLeaseIds ) ) {
			$strSql .= ' AND ( fa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						OR fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )) AND fa.cid = ' . ( int ) $intCid;
		} elseif( true == valArr( $arrintApplicantIds ) ) {
			$strSql .= ' AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND fa.cid = ' . ( int ) $intCid;
		} elseif( true == valArr( $arrintLeaseIds ) ) {
			$strSql .= ' AND fa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND fa.cid = ' . ( int ) $intCid;
		}

		if( true == $boolShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintFileCount[0]['count'] ) ) {
			return $arrintFileCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchFileByApplicationIdByIdByCid( $intApplicationId, $intCid, $intId, $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*, fa.application_id AS application_id
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND f.id = ' . ( int ) $intId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL ';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdByIdsByCid( $intApplicationId, $arrintIds, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid)
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND f.id IN ( \'' . implode( '\',\'', $arrintIds ) . '\' )
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByApplicantIdByDocumentIdByCid( $intApplicationId, $intApplicantId, $intLeaseDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						ft.name AS file_type_name
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id = ' . ( int ) $intApplicantId . '
						AND f.document_id = ' . ( int ) $intLeaseDocumentId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
					LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByApplicantIdByLeaseDocumentIdByCid( $intApplicationId, $intApplicantId, $intLeaseDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
						, ft.system_code AS file_type_system_code
						, ft.name AS file_type_name
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id = ' . ( int ) $intApplicantId . '
						AND f.document_id = ' . ( int ) $intLeaseDocumentId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
					LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdByApplicantIdsByLeaseDocumentIdByCid( $intApplicationId, $arrintApplicantIds, $intLeaseDocumentId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) ) {
			return;
		}

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						ft.name AS file_type_name
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND f.document_id = ' . ( int ) $intLeaseDocumentId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByLeaseIdIdByCustomerIdByLeaseDocumentIdByCid( $intLeaseId, $intCustomerId, $intLeaseDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
						, ft.system_code AS file_type_system_code
						, ft.name AS file_type_name
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.lease_id = ' . ( int ) $intLeaseId . '
						AND fa.customer_id = ' . ( int ) $intCustomerId . '
						AND f.document_id = ' . ( int ) $intLeaseDocumentId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						AND f.file_upload_date IS NOT NULL
					ORDER BY
						f.file_upload_date DESC
					LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileByFileNameByFileTypeIdByCid( $strFileName, $intFileTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
					FROM
						files f
					WHERE
						f.file_type_id = ' . ( int ) $intFileTypeId . '
					AND	f.file_name = \'' . ( string ) addslashes( $strFileName ) . '\'
					AND f.cid = ' . ( int ) $intCid . '
					ORDER BY id DESC LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchCustomFileByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						d.is_bluemoon,
						d.locale_code,
						fa.file_signed_on,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_types ft ON( f.cid = ' . ( int ) $intCid . ' AND f.file_type_id = ft.id AND f.cid = ft.cid )
						LEFT JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						LEFT JOIN documents d ON ( d.cid = f.cid AND f.document_id = d.id )
					WHERE
						f.id = ' . ( int ) $intId;

		 return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchIntegratedFilesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT f.* FROM
						files f,
					    file_associations fa,
					    lease_customers lc
					WHERE
						f.id = fa.file_id AND f.cid = fa.cid
					AND fa.lease_id = lc.lease_id AND fa.cid = lc.cid
					AND f.cid = ' . ( int ) $intCid . '
					AND fa.lease_id = ' . ( int ) $intLeaseId . '
					AND f.remote_primary_key IS NOT NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedFilesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
					FROM
						files f,
						file_associations fa,
						lease_customers lc
					WHERE
						f.id = fa.file_id AND f.cid = fa.cid
						AND fa.lease_id = lc.lease_id AND fa.cid = lc.cid
						AND f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND f.remote_primary_key IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByFileTypeSystemCodeByCid( $intApplicationId, $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_types ft ON (f.file_type_id = ft.id AND f.cid = ft.cid )
						LEFT JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid)
					WHERE
						ft.system_code = \'' . ( string ) addslashes( $strSystemCode ) . '\'
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
					ORDER BY f.id DESC LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdsByFileTypeSystemCodeByCid( $arrintQuoteIds, $arrintApplicationId, $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_types ft ON (f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid AND fa.quote_id IN (' . sqlIntImplode( $arrintQuoteIds ) . ')  AND fa.application_id IN (' . sqlIntImplode( $arrintApplicationId ) . '))
						
					WHERE
						ft.system_code = \'' . ( string ) addslashes( $strSystemCode ) . '\'
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdsByApplicantIdsByDocumentIdsByCid( $arrintApplicationIds, $arrintApplicantIds, $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
						, ft.system_code AS file_type_system_code
						, ft.name AS file_type_name
						, fa.application_id AS application_id
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid)
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid)
					WHERE
						fa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND f.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchEsignedLeaseFilesCountByCustomerIdByApplicantIdsByLeaseIdsByCid( $intCustomerId, $arrintApplicantIds, $arrintLeaseIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
							COUNT(f.id)
						FROM
							files f
							JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
							JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
							JOIN applications a ON ( a.id = fa.application_id AND a.cid = fa.cid AND a.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ))
							JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid AND lease_document_id IS NOT NULL ' . $strCheckDeletedAASql . ' )
							JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.customer_id = ' . ( int ) $intCustomerId . ')
						WHERE
							fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
							AND fa.cid = ' . ( int ) $intCid . '
							AND f.document_id = aa.lease_document_id
							AND fa.cid = aa.cid
							AND aa.lease_signed_on IS NOT NULL
							AND aa.lease_ip_address IS NOT NULL
							AND aa.lease_signature IS NOT NULL
							AND fa.deleted_by IS NULL ';

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintFileCount[0]['count'] ) ) {
			return $arrintFileCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchEsignedLeaseFilesCountByApplicationIdsByApplicantIdsByDocumentIdsByCid( $arrintApplicationIds, $arrintApplicantIds, $arrintDocumentIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						COUNT(f.id)
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN applications a ON ( a.id = fa.application_id AND a.cid = fa.cid )
						JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						fa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND f.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND fa.cid = ' . ( int ) $intCid . '
						AND aa.lease_signed_on IS NOT NULL
						AND aa.lease_ip_address IS NOT NULL
						AND aa.lease_signature IS NOT NULL
						AND fa.deleted_by IS NULL ';

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintFileCount[0]['count'] ) ) {
			return $arrintFileCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchFileByScheduledEmailIdByFileTypeSystemCodeByCid( $intScheduledEmailId, $strSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*
						, ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByScheduledEmailIdByFileTypeSystemCodeByCid( $intScheduledEmailId, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id)
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByCampaignIdByFileTypeSystemCodeByCid( $intCampaignId, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id)
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.campaign_id = ' . ( int ) $intCampaignId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesKeyIdByCampaignIdByFileTypeSystemCodeByCid( $intCampaignId, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id)
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.campaign_id = ' . ( int ) $intCampaignId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesByScheduledTaskEmailIdByFileTypeSystemCodeByCid( $intScheduledTaskEmailId, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id)
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.scheduled_task_email_id = ' . ( int ) $intScheduledTaskEmailId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesBySystemMessageIdByFileTypeSystemCodeByCid( $intSystemMessageId, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (f.id)
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.system_message_id = ' . ( int ) $intSystemMessageId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
						AND fa.deleted_by IS NULL
						ORDER BY f.id DESC ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesCountByPropertyIdsBySystemMessageKeyBySystemMessageAudienceIdByCid( $arrintPropertyIds, $strSystemMessageKey, $intSystemMessageAudienceId, $intCid, $objDatabase ) {

		if( false === valArr( $arrintPropertyIds ) || false === valId( $intCid ) || false === valStr( $strSystemMessageKey ) || false === valId( $intSystemMessageAudienceId ) ) {
			return NULL;
		}

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds );

		$strSql = ' SELECT 
                        COUNT(*)
                    FROM (    
                        SELECT
							COUNT(fa.file_id) as file_count
						FROM
							file_associations AS fa
							JOIN system_messages as sm ON fa.system_message_id = sm.id
						WHERE
							fa.cid = ' . ( int ) $intCid . '
							AND fa.deleted_by IS NULL
							AND sm.key = \'' . ( string ) $strSystemMessageKey . '\'
							AND sm.system_message_audience_id = ' . ( int ) $intSystemMessageAudienceId . '
							AND sm.property_id IN ( \'' . implode( '\',\'', $arrintPropertyIds ) . '\' )
							GROUP BY fa.file_id ) as subQuery 
					WHERE subQuery.file_count != ' . ( int ) $intPropertyCount;

		$arrintData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintData[0]['count'] ) ) {
			return $arrintData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchFilesByFilesFilterByCid( $objFilesFilter, $intCid, $objDatabase ) {

		if( false == valObj( $objFilesFilter, 'CFilesFilter' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT f.*,
						ft.system_code AS file_type_system_code,
						ft.name AS file_type_name,
						fa.application_id,
						fa.lease_id AS lease_id';

		if( false == is_null( $objFilesFilter->getApPayeeId() ) ) {
			$strSql .= ',
						apl.location_name AS ap_payee_location_name ';
		}

		$strSql .= ' FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )';

		if( false == is_null( $objFilesFilter->getApPayeeId() ) ) {
			$strSql .= ' JOIN ap_payee_locations apl ON ( fa.ap_payee_location_id = apl.id AND fa.cid = apl.cid )';
		}

		if( true == valArr( $objFilesFilter->getLeaseIds() ) ) {
			$strSql  .= ' JOIN lease_customers lc ON ( fa.lease_id = lc.lease_id AND fa.cid = lc.cid )';
		}

		$strSql  .= ' WHERE';

		if( false == is_null( $objFilesFilter->getApPayeeId() ) ) {
			$strSql  .= ' fa.ap_payee_id = ' . $objFilesFilter->getApPayeeId() . ' AND fa.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getApplicationIds() ) ) {
			$strSql  .= ' fa.application_id IN ( ' . implode( ',', $objFilesFilter->getApplicationIds() ) . ' ) AND fa.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getLeaseIds() ) ) {
			$strSql  .= ' fa.lease_id IN ( ' . implode( ',', $objFilesFilter->getLeaseIds() ) . ' ) AND fa.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFilesFilter->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( true === valId( $objFilesFilter->getOwnerId() ) ) {
			$strSql  .= ' fa.owner_id = ' . $objFilesFilter->getOwnerId() . ' AND fa.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( false == is_null( $objFilesFilter->getFromDate() ) && false == is_null( $objFilesFilter->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFilesFilter->getFromDate() . '\'::date
							AND f.created_on::date <= \'' . $objFilesFilter->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' fa.deleted_by IS NULL';

		if( true == valStr( $objFilesFilter->getSortBy() ) ) {
			$strSql .= ' ORDER BY ' . $objFilesFilter->getSortBy() . ' ' . $objFilesFilter->getSortDirection();
		}

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase, $strSystemCode = NULL ) {

		$strWhereCondition = ( false == is_null( $strSystemCode ) ) ? ' AND ft.system_code = \'' . $strSystemCode . '\'' : '';

		$strSql = ' SELECT
						f.*,
						ft.name AS file_type_name,
						ft.system_code AS file_type_system_code
					FROM
						file_associations AS fa
						JOIN files AS f ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types AS ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.ap_header_id = ' . ( int ) $intApHeaderId . '
						AND fa.deleted_by IS NULL
						' . $strWhereCondition . '
						ORDER BY f.id DESC ';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objDatabase, $strSystemCode = NULL ) {

		if( false == valArr( $arrintApPaymentIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == valStr( $strSystemCode ) ) ? ' AND ft.system_code = \'' . $strSystemCode . '\'' : '';

		$strSql = 'SELECT
						f.*,
						ft.system_code as file_type_system_code,
						ah_invoices.id AS file_association_id
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft ON ( f.cid = ft.cid AND f.file_type_id = ft.id )
						JOIN ap_payments ap ON ( ap.cid = f.cid AND ap.id = fa.ap_payment_id )
						JOIN ap_headers ah_payments ON ( ah_payments.cid = ap.cid AND ah_payments.ap_payment_id = ap.id )
						JOIN ap_headers ah_invoices ON ( ah_invoices.cid = ah_payments.cid AND ah_invoices.ap_payment_id IS NULL AND ah_payments.ap_payment_id IS NOT NULL AND ( ah_invoices.id = ANY( string_to_array( ah_payments.header_number,\',\' )::integer[] ) ) )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND fa.ap_payment_id IN ( ' . implode( ',', getIntValuesFromArr( $arrintApPaymentIds ) ) . ' )
						AND fa.deleted_by IS NULL
					ORDER BY 
						f.id DESC ';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	/**
	 * @FixMe - Need to refactor following query to remove union clause.
	 */

	public static function fetchFilesByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {

		if( false == valId( $intApPaymentId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
					(
						SELECT
							f.*,
							ah_invoices.header_number AS invoice_number,
							ap.payment_number AS payment_number,
							ft.name AS file_type_name,
							ft.system_code AS file_type_system_code,
							ft.order_num AS ft_order_num,
							fa.id AS file_association_id,
							fa.ap_payment_id,
							fa.ap_header_id,
							fe.mime_type
						FROM 
							files f
							JOIN file_extensions fe ON (fe.id = f.file_extension_id)
							JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id)
							JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid  )
							JOIN ap_headers ah_invoices ON (ah_invoices.cid = fa.cid AND ah_invoices.id = fa.ap_header_id)
							JOIN ap_headers ah_payments ON ( ah_payments.cid = ah_invoices.cid AND ah_invoices.ap_payment_id IS NULL AND ah_payments.ap_payment_id IS NOT NULL AND ( ah_invoices.id = ANY( string_to_array( ah_payments.header_number,\',\' )::integer[] ) ) )
							LEFT JOIN ap_payments ap ON ( ap.cid = ah_invoices.cid AND ap.id = ah_payments.ap_payment_id )
						WHERE
							ah_payments.cid = ' . ( int ) $intCid . '
							AND ah_payments.ap_payment_id = ' . ( int ) $intApPaymentId . '
						UNION
						SELECT
							f.*,
							NULL AS invoice_number,
							ap.payment_number AS payment_number,
							ft.name AS file_type_name,
							ft.system_code AS file_type_system_code,
							ft.order_num AS ft_order_num,
							fa.id AS file_association_id,
							fa.ap_payment_id,
							NUll AS ap_header_id,
							fe.mime_type
						FROM
							files f
							JOIN file_extensions fe ON (fe.id = f.file_extension_id)
							JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id)
							JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid  )
							JOIN ap_payments ap ON ( ap.cid = fa.cid AND ap.id = fa.ap_payment_id )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.id = ' . ( int ) $intApPaymentId . '
					) records 
					ORDER BY 
						ft_order_num ASC,
						ap_header_id ASC, 
						file_association_id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCheckCopyFilesCountByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						count( fa.id ) AS check_copy_file_count
					FROM 
						files f
						JOIN file_associations fa ON (fa.cid = f.cid AND fa.file_id = f.id AND fa.deleted_by IS NULL AND fa.deleted_on IS NULL)
						JOIN file_types ft ON (f.file_type_id = ft.id AND f.cid = ft.cid)
						JOIN ap_payments ap ON (ap.cid = fa.cid AND ap.id = fa.ap_payment_id)
					WHERE 
						ap.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'CHECKCOPY\'
						AND ap.id = ' . ( int ) $intApPaymentId . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesByIdsByCid( $arrintIds, $intCid, $objClientDatabase, $strOrderBy = '', $boolIsActiveFile  = false ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strCondition   = '';

		if( true == $boolIsActiveFile ) {
			$strCondition	= ' AND f.details->\'show_in_entrata\' is NULL'; // this will exclude the archived files
		}

		if( true == empty( $strOrderBy ) ) {
			$strOrderBy = ' check_component_type_id ASC ';
		}

		$strSql = 'SELECT
						f.*,
						fa.check_component_type_id,
						fa.application_id,
						fa.lease_id,
						ft.system_code AS file_type_system_code,
						fa.file_signed_on,
						CASE
                            WHEN f.details ->> \'show_in_entrata\' IS NULL THEN FALSE
                            ELSE TRUE
                        END AS show_in_entrata
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )
						' . $strCondition . '
					ORDER BY
						fa.' . $strOrderBy;

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleFileDetailsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						file_path,
						file_name,
		                title
					FROM
						files
					WHERE
						cid  = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCountUncategorizedFilesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $intPropertyId = NULL ) {

		$strSql = ' SELECT
						count ( * )
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN file_metadatas fm ON ( f.id = fm.file_id AND f.cid = fm.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.created_by = ' . ( int ) $intCompanyUserId . '
						AND ft.system_code = \'' . CFileType::SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT . '\'';

						if( true == valId( $intPropertyId ) ) {
							$strSql .= ' AND f.property_id = ' . ( int ) $intPropertyId;
						}

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchUncategorizedFilesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $intOffset = NULL, $intLimit = NULL, $intPropertyId = NULL ) {

		$strSql = 'SELECT
						f.*,
						fm.entity_type_id,
						util_get_translated(\'data\', fm.data->>\'entity_id\', fm.details) as entity_id,
						util_get_translated( \'entity_name\', fm.entity_name, fm.details ),
						util_get_translated(\'data\', fm.data->>\'person_first_name1\', fm.details) as person_first_name1,
						util_get_translated(\'data\', fm.data->>\'person_last_name1\', fm.details) as person_last_name1,
						util_get_translated(\'data\', fm.data->>\'document_type_id\', fm.details) as document_type_id,
						util_get_translated(\'data\', fm.data->>\'ap_payee_location_id\', fm.details) as ap_payee_location_id,
						util_get_translated( \'note\', fn.note, fn.details ) as note,
						fm.multival_data as tags,
						CASE
						WHEN fm.data->>\'uploaded_file_name\' <> \'\' THEN util_get_translated(\'data\', fm.data->>\'uploaded_file_name\', fm.details )
						ELSE util_get_translated( \'title\', f.title, f.details )
						END AS uploaded_file_name
						
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN file_metadatas fm ON ( f.id = fm.file_id AND f.cid = fm.cid )
						LEFT JOIN file_notes fn ON ( f.id = fn.file_id AND f.cid = fn.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.created_by = ' . ( int ) $intCompanyUserId;

						if( true == valId( $intPropertyId ) ) {
							$strSql .= ' AND f.property_id = ' . ( int ) $intPropertyId;
						}

		 $strSql .= ' AND ft.system_code = \'' . CFileType::SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT . '\'
					ORDER BY
						f.id';

		if( true == is_numeric( $intOffset ) && true == is_numeric( $intLimit ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUncategorizedFilesPropertyIdByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT f.property_id
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN properties p on ( f.property_id = p.id and f.cid = p.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.created_by = ' . ( int ) $intCompanyUserId . ' 
						AND ft.system_code = \'' . CFileType::SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT . '\'';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[] = $arrintPropertyIds['property_id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchFilesByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.gl_header_id = ' . ( int ) $intGlHeaderId . '
						AND fa.deleted_by IS NULL
					ORDER BY f.id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByFileTypeIdByCid( $intApplicationId, $intFileTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intFileTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND f.file_type_id = ' . ( int ) $intFileTypeId . '
					ORDER BY f.id DESC LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdByApplicantIdsByFileTypeIdByCid( $intApplicationId, $arrintApplicantIds, $intFileTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intFileTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND f.file_type_id = ' . ( int ) $intFileTypeId . '
						AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
					ORDER BY
						f.id DESC LIMIT ' . \Psi\Libraries\UtilFunctions\count( $arrintApplicantIds );

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicantIdBySystemCodesByCid( $intApplicantId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						fa.application_id,
						fa.applicant_id,
						fa.id AS file_association_id,
						fa.file_signed_on,
						fa.require_sign,
						( SELECT COUNT(id) FROM file_signatures WHERE file_association_id = fa.id AND cid = fa.cid AND agreement_datetime IS NULL AND deleted_by IS NULL AND cid = ' . ( int ) $intCid . ' ) AS unsigned_file_signatures_count,
						d.description,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND fa.applicant_id = ' . ( int ) $intApplicantId . ' AND fa.deleted_by IS NULL )
						JOIN documents d ON ( d.id = f.document_id AND d.cid = f.cid AND f.cid = ' . ( int ) $intCid . ' )
					WHERE f.show_in_portal = 1';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchRequireSignFilesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						fa.application_id,
						fa.applicant_id,
						fa.id AS file_association_id,
						( SELECT COUNT(id) FROM file_signatures WHERE file_association_id = fa.id AND cid = fa.cid AND agreement_datetime IS NULL AND deleted_by IS NULL AND cid = ' . ( int ) $intCid . ' ) AS unsigned_file_signatures_count,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND fa.applicant_id = ' . ( int ) $intApplicantId . ' AND fa.deleted_by IS NULL AND fa.require_sign = 1 )
						JOIN file_types ft ON( f.file_type_id = ft.id AND f.cid = ft.cid )
						LEFT JOIN documents d ON ( d.cid = f.cid AND d.id = f.document_id )
					WHERE
						( ft.system_code NOT IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\', \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\'  ) OR ft.system_code IS NULL )';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFirstUnSignedFileByCustomerIdByFileTypeSystemCodeByLeaseStatusTypeIdsByCid( $intCustomerId, $strSystemCode, $arrintLeaseStatusTypeIds, $intCid, $objDatabase, $boolRequireSign = false, $boolCheckShowInPortal = false ) {

		if( false == valArr( $arrintLeaseStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_types ft ON( ft.id = f.file_type_id AND ft.system_code = \'' . $strSystemCode . '\' AND ft.cid = f.cid)
						JOIN file_associations fa ON( f.id = fa.file_id AND fa.customer_id =' . ( int ) $intCustomerId . 'AND fa.deleted_on IS NULL AND fa.file_signed_on IS NULL AND fa.cid = f.cid )
						JOIN leases l ON (l.id = fa.lease_id AND l.cid = fa.cid )
						JOIN lease_customers lc ON ( l.cid = lc.cid
														AND l.id = lc.lease_id
														AND l.cid = lc.cid
														AND lc.customer_id = l.primary_customer_id
														AND lc.lease_status_type_id IN ( \'' . implode( '\',\'', $arrintLeaseStatusTypeIds ) . '\' ) )
						LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )

					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.details->\'migration_details\'->>\'source_cid\' IS NULL
                        AND f.details->\'migration_details\'->>\'source_property_id\' IS NULL
						AND ft.system_code != \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\'';

		if( true == $boolRequireSign ) {
			$strSql .= ' AND fa.require_sign = 1';
		}

		if( true == $boolCheckShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		$strSql .= ' ORDER BY fa.created_on DESC LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFirstUnSignedFileByCustomerIdByLeaseStatusTypeIdsByCid( $intCustomerId, $arrintLeaseStatusTypeIds, $intCid, $objDatabase, $strOrderBy = 'ASC' ) {

		if( false == valArr( $arrintLeaseStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id
					FROM
						files f
						JOIN file_types ft ON( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON( f.id = fa.file_id AND fa.customer_id =' . ( int ) $intCustomerId . ' AND fa.require_sign = 1 AND fa.deleted_on IS NULL AND fa.file_signed_on IS NULL AND fa.cid = f.cid )
						JOIN leases l ON (l.id = fa.lease_id AND l.cid = fa.cid )
						JOIN lease_customers lc ON ( l.cid = lc.cid
														AND l.id = lc.lease_id
														AND l.cid = lc.cid
														AND lc.customer_id = l.primary_customer_id
														AND lc.lease_status_type_id IN ( \'' . implode( '\',\'', $arrintLeaseStatusTypeIds ) . '\' ) )

					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.show_in_portal = 1
						AND ( ft.system_code NOT IN(  \'' . implode( '\',\'', CFileType::$c_arrintSigningDocumentFileType ) . '\' ) OR ft.system_code IS NULL )
						AND f.details->\'migration_details\'->>\'source_cid\' IS NULL
                        AND f.details->\'migration_details\'->>\'source_property_id\' IS NULL
					ORDER BY fa.created_on ' . $strOrderBy . ' LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileByFileTypeSystemCodeByFileNameByCid( $strSystemCode, $strFileName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code = \'' . $strSystemCode . '\' )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND lower( f.file_name ) = \'' . \Psi\CStringService::singleton()->strtolower( $strFileName ) . '\'
					ORDER BY f.id DESC LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						f.*,
						fa.check_component_type_id
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN bank_accounts ba ON ( ba.cid = fa.cid AND ba.id = fa.bank_account_id )
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND ba.id = ' . ( int ) $intBankAccountId . '
					ORDER BY
						fa.check_component_type_id ASC';

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchFilesByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintBankAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						fa.check_component_type_id,
						fa.bank_account_id
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN bank_accounts ba ON ( ba.cid = fa.cid AND ba.id = fa.bank_account_id )
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND ba.id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
					ORDER BY
						fa.check_component_type_id ASC';

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchLatestFileByLeaseIdBySystemCodeByCid( $intLeaseId, $strSystemCode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND ft.system_code = \'' . $strSystemCode . '\'
						AND fa.deleted_by IS NULL
					ORDER BY f.id DESC LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchLatestFilePacketByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSystemCode = CFileType::SYSTEM_CODE_LEASE_PACKET;
		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id AND ft.system_code = \'' . $strSystemCode . '\' )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND fa.customer_id IS NULL
					ORDER BY fa.created_on DESC LIMIT 1 OFFSET 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByLeaseIdBySystemCodeByCid( $intLeaseId, $strSystemCode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND ft.system_code = \'' . $strSystemCode . '\'
						AND fa.deleted_by IS NULL
					ORDER BY f.id ASC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApBatchIdByCid( $intApBatchId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ah.ap_batch_id,
						ah.id AS ap_header_id
					FROM
						files f
						LEFT JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						LEFT JOIN ap_headers ah ON ( fa.cid = ah.cid AND fa.ap_header_id = ah.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ah.ap_batch_id = ' . ( int ) $intApBatchId . '
						AND fa.deleted_by IS NULL
					ORDER BY
						f.id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
						AND fa.deleted_by IS NULL
					ORDER BY
						f.id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchApplicantOtherESignFilesByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
					    f.*
					FROM
					    files f
					    JOIN file_types ft ON ( f.file_type_id = ft.id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\' AND ft.cid = f.cid )
					    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					    JOIN applicant_applications aa ON ( aa.applicant_id = fa.applicant_id AND aa.application_id = fa.application_id AND aa.cid = fa.cid AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '  )
					WHERE
					    fa.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
					    AND fa.deleted_by IS NULL
					    AND fa.deleted_on IS NULL
					    AND fa.file_id NOT IN (
                            SELECT
                                fa.file_id
                            FROM
                                file_associations fa
                            WHERE
                                cid = ' . ( int ) $intCid . '
                                AND fa.application_id = ' . ( int ) $intApplicationId . '
                                AND fa.applicant_id = ' . ( int ) $intApplicantId . '
                                AND fa.deleted_by IS NULL
                                AND fa.deleted_on IS NULL
                        )
                   ORDER BY
                        f.id';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCustomerOtherESignFilesByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
					    f.*,
						ft.system_code AS file_type_system_code
					FROM
					    files f
					    JOIN file_types ft ON ( f.file_type_id = ft.id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\' AND ft.cid = f.cid )
					    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					    JOIN Lease_customers lc ON ( lc.customer_id = fa.customer_id AND lc.lease_id = fa.lease_id AND lc.cid = fa.cid AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '  )
					WHERE
					    fa.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
					    AND fa.deleted_by IS NULL
					    AND fa.deleted_on IS NULL
					    AND fa.file_id NOT IN (
                            SELECT
                                fa.file_id
                            FROM
                                file_associations fa
                            WHERE
                                cid = ' . ( int ) $intCid . '
                                AND fa.lease_id = ' . ( int ) $intLeaseId . '
                                AND fa.customer_id = ' . ( int ) $intCustomerId . '
                                AND fa.deleted_by IS NULL
                                AND fa.deleted_on IS NULL
                        )
                   ORDER BY
                        f.id';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileWithSystemCodeByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,ft.system_code AS file_type_system_code
				   FROM
						files f
						LEFT JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid  )
			       WHERE
				        f.id = ' . ( int ) $intId . ' AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchLockedFileByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name,
						d.locale_code
				   FROM
						files f
						LEFT JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid  )
						LEFT JOIN documents d ON ( d.cid = f.cid AND d.id = f.document_id )
			       WHERE
				        f.id = ' . ( int ) $intId . '
				        AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByCompanyUserReportHistoryIdsByCidByExtension( $arrintCompanyUserReportHistoryIds, $strExtension, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserReportHistoryIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
				   FROM files f
				   JOIN file_associations fa ON f.id = fa.file_id AND f.cid = fa.cid
				   JOIN company_user_report_histories curh ON fa.company_user_report_history_id = curh.id AND fa.cid = curh.cid
				   JOIN file_extensions fe ON f.file_extension_id = fe.id
				   WHERE curh.id IN (' . implode( ',', $arrintCompanyUserReportHistoryIds ) . ')
				   AND f.cid = ' . ( int ) $intCid . '
				   AND fe.extension IN( \'' . $strExtension . '\', \'zip\' )
				   AND ( f.file_path LIKE \'%.' . $strExtension . '\' OR f.file_path LIKE \'%.' . $strExtension . '.zip\' ) ';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByCompanyUserReportHistoryIdsByCid( $arrintCompanyUserReportHistoryIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserReportHistoryIds ) ) return NULL;

		$strSql = 'SELECT
						f.*
				   FROM files f
				   JOIN file_associations fa ON f.id = fa.file_id AND f.cid = fa.cid
				   JOIN company_user_report_histories curh ON fa.company_user_report_history_id = curh.id AND fa.cid = curh.cid
				   WHERE curh.id IN (' . implode( ',', $arrintCompanyUserReportHistoryIds ) . ')
				   AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intGlHeaderScheduleId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.gl_header_schedule_id = ' . ( int ) $intGlHeaderScheduleId . '
						AND fa.deleted_by IS NULL
					ORDER BY f.id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByGlHeaderScheduleIdsByCid( $arrintGlHeaderScheduleIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintGlHeaderScheduleIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						fa.gl_header_schedule_id
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.gl_header_schedule_id IN ( \'' . implode( '\',\'', $arrintGlHeaderScheduleIds ) . '\' )
						AND fa.deleted_by IS NULL
					ORDER BY f.id DESC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCountCountersignRequiredFilesByApplicationIdByMasterDocumentIdByCompanyUserIdBySystemCodeByCid( $intApplicationId, $intDocumentId, $intCompanyUserId, $arrstrSystemCodes, $intCid, $objDatabase, $boolAllowApproveSignLease = false ) {
		$strSql = 'SELECT
						COUNT( DISTINCT ( f.id ) ) AS count
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						LEFT JOIN document_addendas da ON ( f.document_id = da.document_id AND f.cid = da.cid AND da.master_document_id = ' . ( int ) $intDocumentId . ' )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND
						CASE WHEN cu.is_administrator = 0 AND da.countersign_company_group_ids IS NOT NULL THEN
							ARRAY
							(
									SELECT
									company_group_id
									FROM
									company_user_groups
									WHERE
									cid = ' . ( int ) $intCid . '
									AND company_user_id = ' . ( int ) $intCompanyUserId . '
							) && da.countersign_company_group_ids
							WHEN cu.is_administrator = 1 OR da.countersign_company_group_ids IS NULL THEN
							TRUE
						END
						AND f.countersigned_on IS NULL
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL';

		if( true == $boolAllowApproveSignLease ) {
			$strSql .= ' AND f.require_countersign = 1
						 AND fa.file_signed_on IS NOT NULL
						 AND fa.applicant_id IS NULL
						 AND fa.customer_id IS NULL';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['count'] ) ) {
			return $arrstrData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchCountCountersignRequiredFilesByApplicationIdsBySystemCodeByCid( $arrintApplicationIds, $strSystemCode, $intCid, $objDatabase ) {

		$arrstrData = [];
		if( false == valArr( $arrintApplicationIds ) ) {
			return $arrstrData;
		}

		$strSql = 'SELECT
					  fa.application_id
					FROM
					  file_associations fa
					  JOIN file_signatures fs ON (fa.id = fs.file_association_id AND fa.cid = fs.cid
					    )
					WHERE 
					  fa.application_id IN (  ' . sqlIntImplode( $arrintApplicationIds ) . ' )  AND
					  fa.cid = ' . ( int ) $intCid . ' AND
					  fa.deleted_by IS NULL AND
					  fa.deleted_on IS NULL AND
					  fs.deleted_by IS NULL AND
					  fs.deleted_on IS NULL AND
					  EXISTS (
					           SELECT
					             1
					           FROM
					             files f
					             JOIN file_types ft ON (ft.id = f.file_type_id AND f.cid = ft.cid)
					           WHERE
					             ft.system_code = \'' . addslashes( $strSystemCode ) . '\' AND
					             f.id = fa.file_id AND
					             f.cid = fa.cid AND
					             f.countersigned_on IS NULL
					  )
					GROUP BY fa.application_id';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['application_id'] ) ) {
			return $arrstrData[0];
		} else {
			return $arrstrData;
		}
	}

	public static function fetchCountersignRequiredLeaseFilesByApplicationIdByApplicantIdsByCid( $intApplicationId, $arrintApplicantIds, $intCid, $objDatabase, $boolIncludeCountersignedFiles = false, $intFileId = NULL ) {
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}

				$strSql = 'SELECT
							    f.*,
								ft.system_code AS file_type_system_code
							FROM
							    files f
							    JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
							    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
							WHERE
							    f.cid = ' . ( int ) $intCid . '
							    AND fa.application_id = ' . ( int ) $intApplicationId . '
							    AND fa.applicant_id IN ( \'' . implode( '\',\'', $arrintApplicantIds ) . '\' )
							    AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\'
							    AND fa.deleted_by IS NULL
							    AND fa.deleted_on IS NULL';

				if( false == $boolIncludeCountersignedFiles ) {
					$strSql .= ' AND f.countersigned_on IS NULL  AND f.countersigned_by IS NULL';
				}

				if( NULL != $intFileId ) {
					$strSql .= ' AND f.file_id =' . ( int ) $intFileId;
				}

				$strSql .= ' ORDER BY f.order_num';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchMainLeaseFileByApplicationIdByApplicantIdsByCid( $intApplicationId, $arrintApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						 f.*
					FROM
					    files f
					    JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
					    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
					    f.cid = ' . ( int ) $intCid . '
					    AND fa.application_id = ' . ( int ) $intApplicationId . '
					    AND fa.applicant_id IN ( \'' . implode( '\',\'', $arrintApplicantIds ) . '\' )
					    AND f.file_id IS NULL
					    AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\'
					    AND f.require_countersign <> 1
					    AND f.countersigned_on IS NULL
					    AND f.countersigned_by IS NULL
					    AND fa.deleted_by IS NULL
					    AND fa.deleted_on IS NULL
					    LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchCountersignedLeaseFilesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							DISTINCT f.*,
							ft.system_code AS file_type_system_code
							FROM
							    files f
							    JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
							    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
							WHERE
							    f.cid = ' . ( int ) $intCid . '
							    AND fa.application_id = ' . ( int ) $intApplicationId . '
							    AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\'
							    AND f.countersigned_on IS NOT NULL
							    AND fa.deleted_by IS NULL
							    AND fa.deleted_on IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCountersignedLeasePacketFilesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							DISTINCT f.*,
							ft.system_code AS file_type_system_code
							FROM
							    files f
							    JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
							    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
							WHERE
							    f.cid = ' . ( int ) $intCid . '
							    AND fa.application_id = ' . ( int ) $intApplicationId . '
							    AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\'
							    AND f.countersigned_on IS NOT NULL
							    AND fa.deleted_by IS NULL
							    AND fa.deleted_on IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchIntegratedFilesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    DISTINCT f.*
					FROM
					    files f
					    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					    JOIN leases l ON ( fa.lease_id = l.id AND fa.cid = l.cid )
					WHERE
					    f.cid = ' . ( int ) $intCid . '
					    AND l.property_id = ' . ( int ) $intPropertyId . '
					    AND f.remote_primary_key IS NOT NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByIdsByFileTypeSystemCodesByCid( $arrintFileIds, $arrstrSystemCodes, $intCid, $objDatabase, $boolIncludeChildrenFiles = false, $intApplicantId = NULL ) {

		if( false == valArr( $arrintFileIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						fa.file_signed_on,
						d.locale_code,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ( f.id IN ( ' . implode( ',', $arrintFileIds ) . ' )';

		if( true == $boolIncludeChildrenFiles ) {
			$strSql .= ' OR f.file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )';
		}

		if( false == is_null( $intApplicantId ) ) {
			$strSql .= ' AND fa.applicant_id =' . ( int ) $intApplicantId;
		}

		$strSql .= ' )
					AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
					AND fa.deleted_by IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchSignedLeaseFilesByMasterFileIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						signed_file.*,
						f.id AS master_file_id,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.deleted_on IS NULL )
						JOIN file_associations signed_file_association ON ( signed_file_association.application_id = fa.application_id AND signed_file_association.applicant_id = fa.applicant_id AND signed_file_association.cid = f.cid AND signed_file_association.deleted_on IS NULL )
						JOIN file_types ft ON ( ft.cid = f.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_SIGNED . '\' )
						JOIN files signed_file ON ( signed_file.id = signed_file_association.file_id AND signed_file.file_type_id = ft.id AND signed_file.cid = f.cid )
					WHERE
						f.id IN (' . implode( ',', $arrintIds ) . ')
						AND f.cid = ' . ( int ) $intCid;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchParentFileByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.name AS file_type_name,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON( f.cid = ' . ( int ) $intCid . ' AND f.file_type_id = ft.id AND f.cid = ft.cid )
					WHERE
						f.id = ( SELECT
									CASE WHEN
										f1.file_id IS NOT NULL THEN f1.file_id
									ELSE
										f1.id
									END AS file_id
								FROM
									files f1
								WHERE
									f1.id = ' . ( int ) $intId . '
									AND f1.cid = ' . ( int ) $intCid . ' )
						AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchLeasePacketFilesByCustomerIdBySystemCodesByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeSignFile = true, $boolCheckShowInPortal = false ) {

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id,
						ft.system_code AS file_type_system_code,
						ft.name AS file_type_name,
						fa.application_id AS application_id,
						fa.applicant_id AS applicant_id,
						fa.require_sign AS require_sign,
						fa.customer_id AS customer_id,
						fa1.file_signed_on AS file_signed_on
					FROM
						file_associations fa
						JOIN files f ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON (ft.cid = f.cid AND ft.id = f.file_type_id AND ft.system_code = \'' . \CFileType::SYSTEM_CODE_LEASE_PACKET . '\' )
						JOIN file_associations fa1 ON( fa.cid = fa1.cid AND f.id = fa1.file_id AND fa1.customer_id = ' . ( int ) $intCustomerId . ' AND fa1.cid = ' . ( int ) $intCid . '  )
						LEFT JOIN applications a ON ( a.cid = fa.cid AND a.id = fa.application_id )
						LEFT JOIN lease_intervals li ON ( a.cid = li.cid AND a.lease_id = li.lease_id AND li.id = a.lease_interval_id )
						LEFT JOIN leases l ON ( l.cid = fa.cid AND l.id = fa.lease_id )
						LEFT JOIN lease_intervals li1 ON ( li1.cid = l.cid  AND li1.lease_id =l.id AND li1.id = l.active_lease_interval_id )
					WHERE
					    fa.cid = ' . ( int ) $intCid . '
					    AND li1.lease_status_type_id <> ' . \CLeaseStatusType::PAST . '
					    AND fa.customer_id IS NULL
					    AND fa.applicant_id IS NULL
					    AND fa.file_signed_on IS NULL
					    AND fa.deleted_by IS NULL
					    AND fa.deleted_on IS NULL
					    AND f.details->\'migration_details\'->>\'source_cid\' IS NULL
                        AND f.details->\'migration_details\'->>\'source_property_id\' IS NULL';

		if( true == $boolCheckShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		if( false == $boolIncludeSignFile ) {
			$strSql .= ' AND fa1.file_signed_on IS NULL';
		}

		$strSql .= ' AND
						CASE WHEN ( li.lease_status_type_id IS NULL OR li.lease_status_type_id IN ( ' . implode( ',', \CLeaseStatusType::$c_arrintLeaseSigningLeaseStatusTypes ) . ' )  ) THEN
							TRUE
						END
					ORDER BY fa.application_id DESC NULLS FIRST, li.lease_interval_type_id DESC, f.id DESC';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByFileIdByFileTypeSystemCodesByCid( $intFileId, $arrstrSystemCodes, $intCid, $objDatabase, $boolIncludeDeleted = false ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id,
						ft.system_code AS file_type_system_code,
						fa.file_signed_on,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
					WHERE
						f.file_id = ' . ( int ) $intFileId . '
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )';

		if( false == $boolIncludeDeleted ) {
			$strSql .= ' AND fa.deleted_by IS NULL ';
		}
		$strSql .=' AND f.cid = ' . ( int ) $intCid . '
					ORDER BY f.order_num';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCountFilesByApplicationIdByFileTypeSystemCodesByCid( $intApplicationId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						COUNT(f.id) AS signed_file_count
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.deleted_by IS NULL
						AND f.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['signed_file_count'] ) ) {
			return $arrstrData[0]['signed_file_count'];
		} else {
			return 0;
		}
	}

	public static function fetchFilesWithChildFilesByIdByCid( $intFileId, $intCid, $objDatabase, $intPropertyId = NULL, $boolIncludeCountersignedFiles = false ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						d.locale_code,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ( f.id = ' . ( int ) $intFileId . ' OR f.file_id = ' . ( int ) $intFileId . ' )';

		if( true == $boolIncludeCountersignedFiles ) {
			$strSql .= ' AND fa.approved_on IS NOT NULL AND fa.approved_by IS NOT NULL';
		}

		if( true == isset ( $intPropertyId ) ) {
			$strSql .= ' AND f.property_id = ' . ( int ) $intPropertyId;
		}

		$strSql .= ' ORDER BY f.order_num, f.id';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchRecursiveChildrenFilesByParentFileIdsByCid( $arrintParentFileIds, $objStdObjectContainer, $intCid, $objDatabase, $boolIncludeCountersignedFiles = false ) {

		if( false == valArr( $arrintParentFileIds ) ) {
			return $objStdObjectContainer;
		}

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.file_id IN ( ' . implode( ',', $arrintParentFileIds ) . ' )
						AND f.cid = ' . ( int ) $intCid;

		if( true == $boolIncludeCountersignedFiles ) {
			$strSql .= ' AND f.countersigned_by IS NOT NULL AND f.countersigned_on IS NOT NULL';
		}

		$strSql .= ' ORDER BY
						f.order_num';

		$arrobjChildrenFiles = self::fetchFiles( $strSql, $objDatabase );

		if( true == valArr( $arrobjChildrenFiles ) ) {
			$objStdObjectContainer->m_arrobjFiles = array_merge( $objStdObjectContainer->m_arrobjFiles, $arrobjChildrenFiles );
			self::fetchRecursiveChildrenFilesByParentFileIdsByCid( array_keys( $arrobjChildrenFiles ), $objStdObjectContainer, $intCid, $objDatabase, $boolIncludeCountersignedFiles );
		} else {
			return $objStdObjectContainer->m_arrobjFiles;
		}
	}

	public static function fetchFilesCountWithFileTypeByFilesFilterByCid( $objFilesFilter, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		if( false == valObj( $objFilesFilter, 'CFilesFilter' ) ) {
			return NULL;
		}
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
                        file_type_id,
                        COUNT( file_type_id ),
                        system_code,
                        name
                    FROM
                        (
                          ( SELECT
                                  COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
                                  ft.system_code,
                                  ft.name,
                                  f.cid,
                                  f.id AS file_id
                              FROM
                                  files f
                                  LEFT JOIN documents d ON (d.id = f.document_id AND d.cid = f.cid)
                                  LEFT JOIN file_types ft ON ( ft.id = COALESCE( d.file_type_id, f.file_type_id ) AND f.cid = ft.cid )
                                  JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
                                  JOIN cached_applications apps ON ( fa.application_id = apps.id AND f.cid = apps.cid )
						          JOIN properties p ON ( p.id = apps.property_id AND p.cid = apps.cid )
						          LEFT JOIN unit_spaces us ON ( us.id = apps.unit_space_id AND us.cid = apps.cid ' . $strCheckDeletedUnitSpacesSql . ' )
                              WHERE
                                  f.cid = ' . ( int ) $intCid . 'AND';

		if( true == valArr( $objFilesFilter->getPropertyIds() ) ) {
			$strSql  .= ' apps.property_id IN ( ' . implode( ',', $objFilesFilter->getPropertyIds() ) . ' ) AND apps.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFilesFilter->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( false == is_null( $objFilesFilter->getFromDate() ) && false == is_null( $objFilesFilter->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFilesFilter->getFromDate() . '\'::date
                            AND f.created_on::date <= \'' . $objFilesFilter->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' fa.deleted_by IS NULL
					 AND fa.application_id IS NOT NULL
					 AND f.file_type_id IS NOT NULL';

		$strSql .= ' ) UNION ';

		$strSql .= '( SELECT
                            COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
                            ft.system_code,
                            ft.name,
                            f.cid,
                            f.id AS file_id
                      FROM
                            files f
                            LEFT JOIN documents d ON (d.id = f.document_id AND d.cid = f.cid)
                            LEFT JOIN file_types ft ON ( ft.id = COALESCE( d.file_type_id, f.file_type_id ) AND f.cid = ft.cid )
                            JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
                            JOIN cached_leases l ON ( fa.lease_id = l.id AND f.cid = l.cid )
					        JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
					        LEFT JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid ' . $strCheckDeletedUnitSpacesSql . ' )
                      WHERE
                            f.cid = ' . ( int ) $intCid . ' AND';

		$strSql  .= ' ';

		if( true == valArr( $objFilesFilter->getPropertyIds() ) ) {
			$strSql  .= 'l.property_id IN ( ' . implode( ',', $objFilesFilter->getPropertyIds() ) . ' ) AND l.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFilesFilter->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( false == is_null( $objFilesFilter->getFromDate() ) && false == is_null( $objFilesFilter->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFilesFilter->getFromDate() . '\'::date
                            AND f.created_on::date <= \'' . $objFilesFilter->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' fa.deleted_by IS NULL
					 AND fa.application_id IS NULL
					 AND f.file_type_id IS NOT NULL)';

		$arrstrInsuranceFileTypes = [ '\'' . CFileType::SYSTEM_CODE_PROOF_OF_INSURANCE . '\'', '\'' . CFileType::SYSTEM_CODE_POLICY . '\'' ];

		$strSql .= ' UNION ( SELECT
                            COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
                            ft.system_code,
                            ft.name,
                            f.cid,
                            f.id AS file_id
                      FROM
                            files f
                            LEFT JOIN documents d ON (d.id = f.document_id AND d.cid = f.cid)
                            JOIN file_types ft ON ( ft.id = COALESCE( d.file_type_id, f.file_type_id ) AND f.cid = ft.cid AND ft.system_code IN ( ' . implode( ', ', $arrstrInsuranceFileTypes ) . ') )
                            JOIN resident_insurance_policies rip ON ( f.cid = rip.cid AND f.file_name = rip.proof_of_coverage_file_name )
                            JOIN insurance_policy_customers ipc ON ( rip.cid = ipc.cid AND ipc.resident_insurance_policy_id = rip.id )
                            JOIN cached_leases l ON ( ipc.lease_id = l.id AND f.cid = l.cid )
					        JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
					        LEFT JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid ' . $strCheckDeletedUnitSpacesSql . ' )
                      WHERE
                            f.cid = ' . ( int ) $intCid . ' AND';

		$strSql  .= ' ';

		if( true == valArr( $objFilesFilter->getPropertyIds() ) ) {
			$strSql  .= 'l.property_id IN ( ' . implode( ',', $objFilesFilter->getPropertyIds() ) . ' ) AND l.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFilesFilter->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFilesFilter->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( false == is_null( $objFilesFilter->getFromDate() ) && false == is_null( $objFilesFilter->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFilesFilter->getFromDate() . '\'::date
                            AND f.created_on::date <= \'' . $objFilesFilter->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' rip.proof_of_coverage_file_name IS NOT NULL AND ipc.lease_id IS NOT NULL)';

		$strSql .= ') AS sub
                    GROUP BY
                        sub.file_type_id,
                        sub.system_code,
                        sub.name,
                        sub.cid';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchRecentlyUploadedFilesByCidByDocumentSearchFilter( $arrmixSearchCriterias = [], $arrintEntityTypeIds, $intCid, $objDatabase, $boolIsIncludeFileTypes = false ) {

		$strSql = '	SELECT
							f.id,
							COALESCE( util_get_translated( \'title\', f.title, f.details ), f.file_name ) AS title,
							f.file_name,
							fm.entity_type_id AS entity_type_id,
							util_get_translated(\'data\', NULL, fm.details)::jsonb->> \'external_source_system\' AS external_source_system,
							f.created_on,
							util_get_translated( \'entity_name\', fm.entity_name, fm.details ) AS entity_name,
							ft.name file_type_name,
							ft.id file_type_id,
							 COALESCE ( ce.name_first || \' \' || ce.name_last, cu.username ) AS uploaded_by_name,
							tz.time_zone_name
						FROM
							files f
							JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
							JOIN file_metadatas fm ON ( fm.file_id = f.id AND fm.cid = f.cid )';

		$strSql .= ' JOIN properties p ON ( f.cid = p.cid AND f.property_id = p.id )
							JOIN time_zones tz ON ( tz.id = p.time_zone_id )
							LEFT JOIN company_users cu ON ( f.created_by = cu.id AND cu.cid = f.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						WHERE
							f.cid = ' . ( int ) $intCid . '';

		$strSql .= 'AND ( ( fm.entity_type_id IN (' . implode( ',', $arrintEntityTypeIds ) . ' ) ) ';

		if( true == in_array( CEntityType::BLUE_MOON_DOCUMENTS, $arrintEntityTypeIds ) ) {
			$strSql .= ' OR ( fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\' ) ';
		}
		$strSql .= ' ) ';

		$strSql .= ' AND ( ( fm.data->>\'sys_is_hidden\' ) IS NULL OR ( fm.data->>\'sys_is_hidden\' <> \'1\') )';

		if( true == valId( $arrmixSearchCriterias['entity_type_id'] ) ) {
			if( CEntityType::RESIDENT == $arrmixSearchCriterias['entity_type_id'] ) {

				if( false == array_key_exists( 'lease_status_type_id', $arrmixSearchCriterias ) || '' == $arrmixSearchCriterias['lease_status_type_id'] ) {
					$strLeaseStatusTypeIds = '\'' . CLeaseStatusType::PAST . '\',\'' . CLeaseStatusType::CANCELLED . '\',\'' . CLeaseStatusType::APPLICANT . '\'';

					$strSql .= ' AND CASE
				      WHEN
				        fm.data?\'lease_status_type_id\'
				      THEN
				        fm.data->>\'lease_status_type_id\' NOT IN (' . $strLeaseStatusTypeIds . ')
				      ELSE
				        TRUE
				    END';
				}

			} elseif( CEntityType::BLUE_MOON_DOCUMENTS == $arrmixSearchCriterias['entity_type_id'] ) {
				$strSql .= ' AND ( fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\' ) ';
			} else {
				$strSql .= ' AND fm.entity_type_id = ' . $arrmixSearchCriterias['entity_type_id'] . ' ';
			}

			if( CEntityType::LEAD == $arrmixSearchCriterias['entity_type_id'] && ( false == array_key_exists( 'application_stage_status_id', $arrmixSearchCriterias ) || '' == $arrmixSearchCriterias['application_stage_status_id'] ) ) {
				$arrintExcludeApplicationStageStatusIds = [
					CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::PRE_QUALIFICATION	=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::LEASE 				=> [ CApplicationStatus::APPROVED ]
				];

				$strSql .= ' AND
						CASE
				            WHEN
				                fm.data?\'application_stage_id\' AND fm.data?\'application_status_id\'
				            THEN
								( fm.data->>\'application_stage_id\', fm.data->>\'application_status_id\' ) NOT IN ( ' . sqlStrMultiImplode( $arrintExcludeApplicationStageStatusIds ) . ')
							 ELSE
				                 TRUE
				        END';
			}
		}

		if( true == CValidation::validateDate( $arrmixSearchCriterias['uploaded_date_min'] ) ) {
			$strMinDate = '\'' . date( 'm/d/Y', strtotime( $arrmixSearchCriterias['uploaded_date_min'] ) ) . ' 00:00:00' . '\'';
			$strSql .= ' AND f.created_on::DATE >= ' . $strMinDate;
		}

		if( true == CValidation::validateDate( $arrmixSearchCriterias['uploaded_date_max'] ) ) {
			$strMaxDate = '\'' . date( 'm/d/Y', strtotime( $arrmixSearchCriterias['uploaded_date_max'] ) ) . ' 12:59:59' . '\'';
			$strSql .= ' AND f.created_on::DATE <= ' . $strMaxDate;
		}

		if( true == array_key_exists( 'document_name', $arrmixSearchCriterias ) && true == valStr( $arrmixSearchCriterias['document_name'] ) ) {
			$strSql .= ' AND f.title ILIKE \'%' . trim( addslashes( $arrmixSearchCriterias['document_name'] ) ) . '%\'';
		}

		if( true == array_key_exists( 'file_extensions', $arrmixSearchCriterias ) && true == valArr( $arrmixSearchCriterias['file_extensions'] ) ) {
			$strSql .= ' AND f.file_extension_id IN  ( ' . implode( ',', $arrmixSearchCriterias['file_extensions'] ) . ' ) ';
		}

		if( true == $boolIsIncludeFileTypes ) {
			$strSql .= ' AND ( (ft.system_code IN ( \'' . implode( '\',\'', CFileType::$c_arrstrDocStorageFileTypes ) . '\' ) ) OR ( ft.is_system = 0 AND ft.system_code IS NULL ) ) ';
		}

		$strSql .= ' ORDER BY fm.id DESC OFFSET ' . ( int ) $arrmixSearchCriterias['recent_upload_listing_offset'] . ' LIMIT ' . ( int ) $arrmixSearchCriterias['recent_upload_listing_limit'];

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonGeneratedFileCountByApplicationIdBySystemCodeByCid( $intApplicationId, $strSystemCode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( DISTINCT ( f.id ) ) AS non_generated_file_count
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
					    AND f.file_upload_date IS NULL
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['non_generated_file_count'] ) ) {
			return $arrstrData[0]['non_generated_file_count'];
		} else {
			return 0;
		}
	}

	public static function fetchFilesByApHeaderIdsByCid( $arrintPoApHeaderIds, $intCid, $objDatabase, $intFileTypeId = NULL, $strOrderBy = 'f.id DESC' ) {

		if( false == valArr( $arrintPoApHeaderIds ) ) {
			return NULL;
		}
		if( 'f.title' == $strOrderBy ) {
			$strOrderBy = $objDatabase->getCollateSort( $strOrderBy, true );
		}

		$strWhereCondition = ( false == is_null( $intFileTypeId ) ) ? ' AND f.file_type_id = ' . ( int ) $intFileTypeId : '';

		$strSql = 'SELECT
						f.*,
						fa.ap_header_id AS file_association_id,
						ft.system_code as file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.cid = f.cid AND f.file_type_id = ft.id)
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.ap_header_id IN ( ' . implode( ',', $arrintPoApHeaderIds ) . ' )
						AND fa.deleted_by IS NULL
						' . $strWhereCondition . '
					ORDER BY ' . $strOrderBy;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchSimpleFilesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchFilesByApplicationIdBySystemCodesByCid( $intApplicationId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.deleted_by IS NULL';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchPacketFilesCountByApplicationIdBySystemCodesByCid( $intApplicationId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT ( f.id ) ) AS packet_file_count
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.deleted_by IS NULL';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['packet_file_count'] ) ) {
			return $arrstrData[0]['packet_file_count'];
		} else {
			return 0;
		}
	}

	public static function fetchfilesByInspectionIdsByCid( $arrintInspectionIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintInspectionIds ) ) {
			return NULL;
		}

		$strSql = 'select
						f.id,
						f.file_name,
						f.file_extension_id,
						f.file_size,
						f.file_upload_date,
						f.file_path,
						ir.id AS responce_id,
						fa.id AS file_association_id ,
						ir.inspection_id,
						ir.inspection_form_location_problem_id AS inspection_form_location_problem_id,
                            ( select COUNT(1) from file_associations fa1 where fa1.cid=ir.cid and fa1.file_id =
                            f.id ) AS action_count,
						ir.maintenance_problem_id,
						CASE 
							WHEN ir.property_unit_maintenance_location_id IS NULL 
								THEN iflp.inspection_form_location_id
							ELSE ir.property_unit_maintenance_location_id
						END AS inspection_form_location_id
					from inspection_responses ir
                         inner join file_associations fa on ir.cid = fa.cid and ir.id = fa.inspection_response_id
                         inner join files f on fa.cid = f.cid and fa.file_id = f.id
                         join inspection_form_location_problems iflp ON ( iflp.cid = ir.cid AND iflp.id = ir.inspection_form_location_problem_id )
                         LEFT JOIN maintenance_problems mpa ON ( mpa.id = ir.maintenance_problem_id AND mpa.cid = ir.cid AND mpa.maintenance_problem_type_id IN ( ' . CMaintenanceProblemType::INSPECTION_ACTION_REPAIR . ', ' . CMaintenanceProblemType::INSPECTION_ACTION_REPLACE . ', ' . CMaintenanceProblemType::INSPECTION_ACTION_CLEAN . ' ) )
                    where	ir.cid = ' . ( int ) $intCid . '
							AND ir.inspection_id IN ( ' . implode( ',', $arrintInspectionIds ) . ' )
							AND ( mpa.is_published = 1 OR mpa.is_published IS NULL ) ';

		return self::fetchFiles( $strSql, $objDatabase, false );
	}

	public static function fetchCustomFilesByIds( $arrintFileIds, $objDatabase ) {

		if( false == valArr( $arrintFileIds = array_filter( $arrintFileIds ), 1 ) ) {
			return [];
		}

		$strSql = 'SELECT
						*
					FROM
						files
					WHERE
						id IN ( ' . implode( ', ', $arrintFileIds ) . ' )';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileCountByTitleByPropertyIdByCid( $strTitle, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strTitle ) ) {
			return NULL;
		}
		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strWhere = ' WHERE title = \'' . addslashes( $strTitle ) . '\'
							AND property_id = ' . ( int ) $intPropertyId . '
							AND cid = ' . ( int ) $intCid;

		return self::fetchFileCount( $strWhere, $objDatabase );
	}

	public static function fetchFilesByFileMetadataIdsByCid( $intCid, $arrintFileMetadataIds, $objDatabase ) {
		if( false == valArr( $arrintFileMetadataIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						p.property_name,
						ft.name AS file_type_name,
						fm.data->>\'person_first_name1\' AS file_metadata_person_first_name,
						fm.data->>\'person_last_name1\' AS file_metadata_person_last_name
					FROM
						files f
						JOIN file_metadatas fm ON ( fm.file_id = f.id AND fm.cid = f.cid )
						JOIN properties p ON ( p.id = f.property_id AND p.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fm.id IN ( ' . implode( ',', $arrintFileMetadataIds ) . ') ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByLeasIdByCompanyUserIdByCid( $intLeaseId, $intCompanyUserId, $intCid, $objDatabase, $arrintFileIds = [], $boolFromApplication = true, $boolIncludeCurrentUserCountersignedFiles = false, $arrintApplicantIds = [] ) {

		if( true == $boolIncludeCurrentUserCountersignedFiles ) {
			$strCondition = ' AND f.updated_by = ' . ( int ) $intCompanyUserId;
		} else {
			$strCondition = ' AND f.countersigned_on IS NULL AND f.countersigned_by IS NULL';
		}

		$strSql = 'SELECT
						DISTINCT(f.*),
						ft.system_code AS file_type_system_code,
						fa.application_id,
						fa.lease_id
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN document_addendas da ON ( f.cid = da.cid AND f.document_id = da.document_id AND f.document_addenda_id = da.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND fa.lease_id =' . ( int ) $intLeaseId . '
						AND
							CASE WHEN cu.is_administrator = 0 AND da.countersign_company_group_ids IS NOT NULL THEN
							ARRAY
							(
								SELECT
									company_group_id
								FROM
									company_user_groups
								WHERE
									cid = ' . ( int ) $intCid . '
									AND company_user_id = ' . ( int ) $intCompanyUserId . '
							) && da.countersign_company_group_ids
							WHEN cu.is_administrator = 1 OR da.countersign_company_group_ids IS NULL THEN
								TRUE
							END
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )';

		if( true == $boolFromApplication ) {
			$strSql .= 'AND fa.application_id IS NOT NULL';
		}

		if( true == valArr( $arrintApplicantIds ) ) {
			$strSql .= ' AND fa.applicant_id IN ( ' . sqlIntImplode( $arrintApplicantIds ) . ' )';
		}

		if( true == valArr( $arrintFileIds ) ) {
			$strSql .= ' AND ( f.id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) OR  f.file_id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) )';
		}

		$strSql .= ' ORDER BY f.id ASC';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $arrintFileIds = [], $boolFromApplication = true, $boolIncludeCurrentUserCountersignedFiles = false, $arrintApplicantIds = [], $intApplicationId = NULL, $boolIsForActivityLog = false, $boolIsApproveSignedLease = false ) {

		if( true == $boolIncludeCurrentUserCountersignedFiles ) {
			if( true == $boolIsForActivityLog ) {
				$strCondition = ' AND fa.approved_by = ' . ( int ) $intCompanyUserId;
			} else {
				$strCondition = ' AND f.updated_by = ' . ( int ) $intCompanyUserId;
			}

		} else {
			$strCondition = ' AND fa.file_signed_on IS NOT NULL AND f.countersigned_on IS NULL AND f.countersigned_by IS NULL';

			if( false == $boolIsApproveSignedLease ) {
				$strCondition .= ' AND fa.approved_on IS NULL AND fa.approved_by IS NULL';
			}
		}

		$strSql = 'SELECT
						DISTINCT(f.*),
						ft.system_code AS file_type_system_code,
						fa.application_id,
						fa.lease_id,
						d.is_bluemoon,
						d.locale_code,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
						LEFT JOIN documents d ON ( d.cid = f.cid AND f.document_id = d.id )
						LEFT JOIN document_addendas da ON ( da.cid = f.cid AND da.document_id = f.document_id AND da.id = f.document_addenda_id )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND f.details->\'migration_details\'->>\'source_cid\' IS NULL
                        AND f.details->\'migration_details\'->>\'source_property_id\' IS NULL
						AND f.require_countersign = 1
						AND
							CASE WHEN cu.is_administrator = 0 AND da.countersign_company_group_ids IS NOT NULL THEN
							ARRAY
							(
								SELECT
									company_group_id
								FROM
									company_user_groups
								WHERE
									cid = ' . ( int ) $intCid . '
									AND company_user_id = ' . ( int ) $intCompanyUserId . '
							) && da.countersign_company_group_ids
							WHEN cu.is_administrator = 1 OR da.countersign_company_group_ids IS NULL THEN
								TRUE
							END
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )';

		if( true == $boolFromApplication ) {
			$strSql .= 'AND fa.application_id IS NOT NULL';
		}

		if( true == valArr( $arrintApplicantIds ) ) {
			$strSql .= ' AND fa.applicant_id IN ( ' . sqlIntImplode( $arrintApplicantIds ) . ' )';
		}

		if( false == is_null( $intApplicationId ) ) {
			$strSql .= ' AND fa.application_id = ' . ( int ) $intApplicationId;
		}

		if( true == valArr( $arrintFileIds ) ) {
			$strSql .= ' AND ( f.id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) OR  f.file_id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) )';
		}

		$strSql .= ' ORDER BY f.id ASC';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByMasterDocumentIdsByApplicationIdsByCompanyUserIdByCid( $arrintMasterDocumentIds, $arrintApplicationIds = NULL, $intCompanyUserId, $intCid, $objDatabase, $intLeaseId = NULL ) {

		$strSql = 'SELECT
						DISTINCT(f.*),
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN applications a ON ( fa.cid = a.cid AND fa.application_id = a.id AND fa.lease_id = a.lease_id )
						LEFT JOIN document_addendas da ON ( f.cid = da.cid AND f.document_id = da.document_id AND f.document_addenda_id = da.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL 
						AND ( ( a.lease_approved_on IS NULL AND a.application_status_id <> ' . CApplicationStatus::APPROVED . ' ) OR ( f.countersigned_on IS NULL AND f.countersigned_by IS NULL AND fa.approved_on IS NULL AND fa.approved_by IS NULL ) ) ';

		if( true == valArr( $arrintMasterDocumentIds ) ) {
			$strSql .= ' AND CASE WHEN da.master_document_id IS NOT NULL THEN
									da.master_document_id IN ( ' . implode( ',', $arrintMasterDocumentIds ) . ' )
								ELSE
									TRUE
							END';
		}

		$strSql .= ' AND
							CASE WHEN cu.is_administrator = 0 AND da.countersign_company_group_ids IS NOT NULL THEN
							ARRAY
							(
								SELECT
									company_group_id
								FROM
									company_user_groups
								WHERE
									cid = ' . ( int ) $intCid . '
									AND company_user_id = ' . ( int ) $intCompanyUserId . '
							) && da.countersign_company_group_ids
							WHEN cu.is_administrator = 1 OR da.countersign_company_group_ids IS NULL THEN
								TRUE
							END
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )';

		if( true == valArr( $arrintApplicationIds ) ) {
			$strSql .= 'AND fa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';
		}

		if( false == is_null( $intLeaseId ) ) {
			$strSql .= ' AND fa.lease_id = ' . ( int ) $intLeaseId;
		}

		$strSql .= ' ORDER BY f.id ASC';
		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchUploadedLeaseFilesByApplicationIdsByCompanyUserIdByCid( $arrintApplicationIds = NULL, $intCompanyUserId, $intCid, $objDatabase, $intLeaseId = NULL ) {

		$strSql = 'SELECT
						DISTINCT(f.*),
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN applications a ON ( fa.cid = a.cid AND fa.application_id = a.id AND fa.lease_id = a.lease_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ( ( a.lease_approved_on IS NULL AND a.application_status_id <>  ' . CApplicationStatus::APPROVED . ' ) OR ( f.countersigned_on IS NULL AND f.countersigned_by IS NULL AND fa.approved_on IS NULL AND fa.approved_by IS NULL ) )
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )';

		if( true == valArr( $arrintApplicationIds ) ) {
			$strSql .= 'AND fa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';
		}

		if( false == is_null( $intLeaseId ) ) {
			$strSql .= ' AND fa.lease_id = ' . ( int ) $intLeaseId;
		}

		$strSql .= ' ORDER BY f.id ASC';
		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchRequiredCountersignLeaseFilesCountByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT( COUNT(f.*) )
					FROM
						files f
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.countersigned_by IS NULL AND f.countersigned_on IS NULL
						AND fa.approved_by IS NULL AND fa.approved_on IS NULL
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintFileCount[0]['count'] ) ) {
			return $arrintFileCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchLeaseFileByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						f.*
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.id = ' . ( int ) $intId . '
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' )';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApPaymentBatchIdsByCid( $arrintApPaymentBatchIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApPaymentBatchIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT f.*,
						fa.ap_payment_batch_id,
						fe.extension,
						fe.mime_type,
						fa.ap_payment_id
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id AND fa.ap_payment_batch_id IN ( ' . implode( ',', $arrintApPaymentBatchIds ) . ' ) )
						JOIN file_extensions fe ON ( fe.id = f.file_extension_id )
					WHERE
						fa.cid = ' . ( int ) $intCid;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchDocumentIdByFileIdByFileTypeSystemCodeByCid( $intFileId, $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT f.document_id
					FROM
						files f
						JOIN file_types ft ON( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
						AND f.id = ' . ( int ) $intFileId;

		return self::fetchColumn( $strSql, 'document_id', $objDatabase );
	}

	public static function fetchBluemoonImportedFilesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						LEFT JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND fa.id IS NULL
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchLockedFilesWithSystemCodeByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						d.is_bluemoon,
						d.locale_code,
						COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
				   FROM
						files f
						LEFT JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid  )
						LEFT JOIN documents d ON ( d.cid = f.cid AND f.document_id = d.id )
			       WHERE
				        f.id IN (' . implode( ',', $arrintIds ) . ')
				        AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchLeaseAddendumFilesByPacketFileIdByCid( $intFileId, $intCid, $objDatabase, $arrintPropertyIds, $boolIncludeCountersignedFiles = true ) {

		if( false == valId( $intFileId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\'  )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.file_id = ' . ( int ) $intFileId;

		if( true == $boolIncludeCountersignedFiles ) {
			$strSql .= ' AND f.countersigned_by IS NOT NULL AND f.countersigned_on IS NOT NULL';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND f.property_id IN ( \'' . implode( '\',\'', $arrintPropertyIds ) . '\' )';
		}

		$strSql .= ' ORDER BY f.order_num';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileByTitleByCidByPropertyIdByCreatedBy( $strTitle, $intCid, $intPropertyId, $intCreatedBy, $objDatabase ) {

		$strSql = ' SELECT
						f.*
					FROM
						files AS f
					WHERE
						f.title = \'' . addslashes( $strTitle ) . '\'
						AND f.cid = ' . ( int ) $intCid . '
						AND f.property_id = ' . ( int ) $intPropertyId . '
						AND f.created_by = ' . ( int ) $intCreatedBy . '
					LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchSignedFileByFileIdByCid( $intPacketId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
                    FROM
						files f
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid  )
                    WHERE
                        f.file_id = ' . ( int ) $intPacketId . '
                        AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\' )
                        AND f.cid = ' . ( int ) $intCid . '
                    LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesCountByFileIdByFileTypeSystemCodesByCid( $intFileId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						COUNT( DISTINCT (f.id ) )
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.file_id = ' . ( int ) $intFileId . '
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND ( fa.applicant_id IS NOT NULL OR fa.customer_id IS NOT NULL )
						AND fa.deleted_by IS NULL
						AND f.cid = ' . ( int ) $intCid;

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintFileCount[0]['count'] ) ) {
			return $arrintFileCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchFilesByExcludingFileTypeSystemCodesByCidByFileIds( $arrintIds, $arrstrSystemCodes, $intCid, $objDatabase, $boolIsIncludeCancelLeaseFiles = false ) {
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						fa.lease_id,
						CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									ELSE \'' . __( 'e-Sign: Lease Modification' ) . '\'
								END
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
                                    \'' . __( 'Application: Policy' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
                                    \'' . __( 'e-Sign: Addenda' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
                                    \'' . __( 'Automated' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
                                     \'' . __( 'Application Upload' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
                                    \'' . __( 'Renewal Offer Sent' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
                                     \'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
                                     \'' . __( 'Transfer Document' ) . '\'
	                        ELSE \'' . __( 'Upload' ) . '\'
						END AS file_type_name,
						fa.file_signed_on,
						fa.sign_initiated_on,
						fa.application_id AS application_id,
						fa.require_sign AS require_sign,
						fa.deleted_by

					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )

					WHERE
						( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) OR ft.system_code IS NULL )
						AND fa.cid = ' . ( int ) $intCid . '
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		if( false == $boolIsIncludeCancelLeaseFiles ) {
			$strSql .= ' AND fa.deleted_by IS NULL';
		}

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByCustomerDataVerificationIdsByCustomerIdByPropertyIdByCid( $arrintCustomerDataVerificationIds, $intCustomerId, $intPropetyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerDataVerificationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					f.*,
					cdv.customer_data_type_id
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.property_id = f.property_id AND fa.file_id = f.id )
						JOIN customer_data_verifications cdv ON ( cdv.cid = fa.cid AND cdv.customer_id = fa.customer_id AND cdv.id = fa.customer_data_verification_id )
					WHERE
						fa.customer_data_verification_id IN(  ' . sqlIntImplode( $arrintCustomerDataVerificationIds ) . ' )
						AND fa.cid = ' . ( int ) $intCid . '
						AND fa.property_id = ' . ( int ) $intPropetyId . '
						AND fa.customer_id = ' . ( int ) $intCustomerId . '
						AND fa.deleted_by IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchActiveRenewalQuoteFilesByApplicationIdsByCids( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*
					FROM
						(
						  SELECT
							  f.*,
							  q.application_id,
							  q.id AS quote_id,
							  rank( ) OVER ( PARTITION BY q.cid, q.application_id ORDER BY
												CASE
										            WHEN q.leasing_tier_special_id IS NULL
														THEN 1
										            WHEN sg.special_group_type_id != ' . CSpecialGroupType::CONVENTIONAL_LEASING . '
														 AND s.start_date <= ca.lease_start_date AND s.end_date >= ca.lease_start_date
														THEN 2
										            WHEN sg.special_group_type_id = ' . CSpecialGroupType::CONVENTIONAL_LEASING . '
														 AND s.max_days_to_lease_start >= ( ca.lease_start_date - CURRENT_DATE )
														 AND CASE
																WHEN cl.lease_interval_type_id =  ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN s.min_days_to_lease_start <= ( ca.lease_start_date - CURRENT_DATE )
																ELSE TRUE
															 END
														THEN 2
										            ELSE 0
									          END DESC, q.expires_on,f.created_on DESC ) AS rank
						  FROM
							  quotes q
							  JOIN file_associations fa ON ( q.cid = fa.cid AND q.id = fa.quote_id AND q.application_id = fa.application_id )
							  JOIN files f ON ( fa.cid = f.cid AND f.id = fa.file_id )
							  JOIN file_types ft ON ( ft.cid = f.cid AND f.file_type_id = ft.id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' )
							  JOIN cached_applications ca ON ( q.cid = ca.cid AND q.application_id = ca.id )
							  JOIN cached_leases cl ON ( ca.cid = cl.cid AND cl.id = ca.lease_id )
							  LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id AND s.deleted_on IS NULL )
							  LEFT JOIN special_groups sg ON ( s.cid = sg.cid AND s.special_group_id = sg.id AND sg.deleted_on IS NULL AND sg.deleted_by IS NULL )
						  WHERE
							  q.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
							  AND q.cid = ' . ( int ) $intCid . '
							  AND q.is_renewal IS TRUE
							  AND CASE
									WHEN s.id IS NOT NULL THEN
										s.deleted_on IS NULL AND COALESCE ( s.quantity_remaining, 1 ) > 0
									ELSE TRUE
								  END
							  AND ( q.expires_on >= CURRENT_DATE OR q.expires_on IS NULL )
							  AND q.leasing_tier_special_id IS NULL
						) AS f

					WHERE
						f.rank = 1;';

		return self::fetchFiles( $strSql, $objDatabase );

	}

	public static function fetchSimpleFilesForFilesExport( $objFileSearch, $intCid, $objDatabase, $strOrganisedBy = NULL ) {

		$strResidentNameSql 			= '';
		$strApplicantNameSql 			= '';
		$strUnitResidentNameJoinsSql 	= '';

		if( 'unitnumber_status_lastname' == $strOrganisedBy ) {

			$strApplicantNameSql .= 'CASE WHEN apps.name_last IS NOT NULL THEN
											CONCAT ( COALESCE(us.unit_number_cache, \'Other_Units\'),\'-\', COALESCE(cl.lease_status_type, astg.name), \'-\', apps.name_last )
										ELSE
											CONCAT ( COALESCE(us.unit_number_cache, \'Other_Units\'),\'-\', COALESCE(cl.lease_status_type, astg.name), \'-\', apps.name_first )
										END AS name_resident';

			$strResidentNameSql .= 'CASE WHEN l.name_last IS NOT NULL THEN
										CONCAT ( COALESCE(us.unit_number_cache, \'Other_Units\'), \'-\', l.lease_status_type, \'-\', l.name_last )
									 ELSE
										CONCAT ( COALESCE(us.unit_number_cache, \'Other_Units\' ), \'-\', l.lease_status_type, \'-\', l.name_first )
									 END AS name_resident';

			$strUnitResidentNameJoinsSql .= 'JOIN application_stages astg ON ( apps.application_stage_id = astg.id )
											LEFT JOIN cached_leases cl ON ( apps.cid = cl.cid AND apps.lease_id = cl.id )';

		} else {

			$strApplicantNameSql .= 'CASE WHEN  apps.name_last IS NOT NULL THEN
										CONCAT ( apps.name_last, \' - \', apps.name_first )
									 ELSE
										apps.name_first
									 END AS name_resident';

			$strResidentNameSql .= 'CASE WHEN  l.name_last IS NOT NULL THEN
										CONCAT ( l.name_last, \' - \', l.name_first )
									ELSE
										l.name_first
									END AS name_resident';
		}

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						cid,
						file_type_id,
						file_id,
						system_code,
						NAME,
						file_name,
						title,
						file_path,
						property_id,
						property_name,
						unit_number_cache,
						building_name,
						name_resident
					FROM
						(
						  ( SELECT
								  COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
								  COALESCE( ft.system_code, dft.system_code ) AS system_code,
								  COALESCE( dft.name, ft.name ) AS name,
								  util_get_translated( \'title\', f.title, f.details ),
								  f.cid,
								  f.id AS file_id,
								  f.file_name,
								  f.file_path,
								  p.id AS property_id,
								  p.property_name,
								  us.unit_number_cache,
								  us.building_name,'
		          . $strApplicantNameSql . '
							  FROM
									files f
									LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
									LEFT JOIN file_types dft ON ( dft.id = d.file_type_id AND d.cid = dft.cid )
									LEFT JOIN file_types ft ON ( ft.id = f.file_type_id AND f.cid = ft.cid )
									
									JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
									JOIN cached_applications apps ON ( fa.application_id = apps.id AND f.cid = apps.cid )'
		          . $strUnitResidentNameJoinsSql . '
									JOIN properties p ON ( p.id = apps.property_id AND p.cid = apps.cid )
									LEFT JOIN unit_spaces us ON ( us.id = apps.unit_space_id AND us.cid = apps.cid AND us.deleted_on IS NULL )
							  WHERE
								  f.cid = ' . ( int ) $intCid . ' AND';

		if( true == valArr( $objFileSearch->getPropertyIds() ) ) {
			$strSql  .= ' apps.property_id IN ( ' . implode( ',', $objFileSearch->getPropertyIds() ) . ' ) AND apps.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFileSearch->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFileSearch->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( false == is_null( $objFileSearch->getFromDate() ) && false == is_null( $objFileSearch->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFileSearch->getFromDate() . '\'::date
							AND f.created_on::date <= \'' . $objFileSearch->getToDate() . '\'::date ) AND';
		}

		if( true == valArr( $objFileSearch->getFileTypeIds() ) ) {

			$strSql .= ' CASE 
                            WHEN f.document_id IS NOT NULL AND d.file_type_id IS NOT NULL THEN 
                               ( d.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            ELSE
                                ( f.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            END
                        AND ';
		}

		$strSql .= ' fa.deleted_by IS NULL
					 AND fa.application_id IS NOT NULL
					 AND f.file_type_id IS NOT NULL';

		$strSql .= ' ) UNION ';

		$strSql .= '( SELECT
							COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
							COALESCE( ft.system_code, dft.system_code ) AS system_code,
							COALESCE( dft.name, ft.name ) AS name,
							util_get_translated( \'title\', f.title, f.details ),
							f.cid,
							f.id AS file_id,
							f.file_name,
							f.file_path,
							p.id AS property_id,
							p.property_name,
							us.unit_number_cache,
							us.building_name,'
		           . $strResidentNameSql . '
					  FROM
							  files f
							  LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
							  LEFT JOIN file_types dft ON ( dft.id = d.file_type_id AND d.cid = dft.cid )
							  LEFT JOIN file_types ft ON ( ft.id = f.file_type_id AND f.cid = ft.cid )
							  JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
							  JOIN cached_leases l ON ( fa.lease_id = l.id AND f.cid = l.cid )
							  JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
							  LEFT JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid AND us.deleted_on IS NULL )
					  WHERE
							f.cid = ' . ( int ) $intCid . ' AND';

		$strSql  .= ' ';

		if( true == valArr( $objFileSearch->getPropertyIds() ) ) {
			$strSql  .= 'l.property_id IN ( ' . implode( ',', $objFileSearch->getPropertyIds() ) . ' ) AND l.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFileSearch->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFileSearch->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( true == valArr( $objFileSearch->getFileTypeIds() ) ) {

			$strSql .= ' CASE 
                            WHEN f.document_id IS NOT NULL AND d.file_type_id IS NOT NULL THEN 
                               ( d.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            ELSE
                                ( f.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            END
                        AND ';
		}

		if( false == is_null( $objFileSearch->getFromDate() ) && false == is_null( $objFileSearch->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFileSearch->getFromDate() . '\'::date
							AND f.created_on::date <= \'' . $objFileSearch->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' fa.deleted_by IS NULL
					 AND fa.application_id IS NULL
					 AND f.file_type_id IS NOT NULL )';

		$arrstrInsuranceFileTypes = [ '\'' . CFileType::SYSTEM_CODE_PROOF_OF_INSURANCE . '\'', '\'' . CFileType::SYSTEM_CODE_POLICY . '\'' ];

		$strSql .= ' UNION (';

		$strSql .= 'SELECT
						COALESCE( d.file_type_id, f.file_type_id ) AS file_type_id,
						fft.system_code,
						ft.name,
						f.file_name as title,
						f.cid,
						f.id AS file_id,
						f.file_name,
						f.file_path,
						p.id AS property_id,
						p.property_name,
						us.unit_number_cache,
						us.building_name,'
		           . $strResidentNameSql . '
					FROM 
						files f
						JOIN file_types fft ON ( fft.id = f.file_type_id AND f.cid = fft.cid )
					    LEFT JOIN documents d ON (d.id = f.document_id AND d.cid = f.cid)						
					    JOIN file_types ft ON ( ft.id = COALESCE( d.file_type_id, f.file_type_id ) AND f.cid = ft.cid AND ft.system_code IN ( ' . implode( ', ', $arrstrInsuranceFileTypes ) . ' ) )
					    JOIN resident_insurance_policies rip ON ( f.cid = rip.cid AND f.file_name = rip.proof_of_coverage_file_name )
					    JOIN insurance_policy_customers ipc ON ( rip.cid = ipc.cid AND ipc.resident_insurance_policy_id = rip.id )
					    JOIN cached_leases l ON ( ipc.lease_id = l.id AND f.cid = l.cid )
						JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
						LEFT JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid AND us.deleted_on IS NULL )
					WHERE
						f.cid = ' . ( int ) $intCid . ' AND';

		$strSql  .= ' ';

		if( true == valArr( $objFileSearch->getPropertyIds() ) ) {
			$strSql  .= 'l.property_id IN ( ' . implode( ',', $objFileSearch->getPropertyIds() ) . ' ) AND l.cid = ' . ( int ) $intCid . ' AND ';
		}

		if( true == valArr( $objFileSearch->getExludingFileTypeSystemCodes() ) ) {
			$strSql .= '( ft.system_code NOT IN ( \'' . implode( '\',\'', $objFileSearch->getExludingFileTypeSystemCodes() ) . '\' ) OR ft.system_code IS NULL ) AND';
		}

		if( true == valArr( $objFileSearch->getFileTypeIds() ) ) {

			$strSql .= ' CASE 
                            WHEN f.document_id IS NOT NULL AND d.file_type_id IS NOT NULL THEN 
                               ( d.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            ELSE
                                ( f.file_type_id IN ( ' . implode( ',', array_filter( $objFileSearch->getFileTypeIds() ) ) . ' ) )
                            END
                        AND ';
		}

		if( false == is_null( $objFileSearch->getFromDate() ) && false == is_null( $objFileSearch->getToDate() ) ) {
			$strSql  .= ' ( f.created_on::date >= \'' . $objFileSearch->getFromDate() . '\'::date
							AND f.created_on::date <= \'' . $objFileSearch->getToDate() . '\'::date ) AND';
		}

		$strSql .= ' rip.proof_of_coverage_file_name IS NOT NULL
					 AND ipc.lease_id IS NOT NULL';

		$strSql .= ' )';
		$strSql .= ') AS sub
					GROUP BY
						sub.file_type_id,
						sub.system_code,
						sub.name,
						sub.title,
						sub.cid,
						sub.file_id,
						sub.file_name,
						sub.file_path,
						sub.property_id,
						sub.property_name,
						sub.unit_number_cache,
						sub.building_name,
						sub.name_resident';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCounterSignatureRequiredFilesCountByFileIdsByFileTypeSystemCodesByCid( $arrintFileIds, $arrstrSystemCodes, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						f.file_id,
						COUNT ( DISTINCT ( f.id ) )
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND fa.deleted_by IS NULL
						AND f.countersigned_on IS NULL
						AND fa.approved_on IS NULL
						AND f.cid = ' . ( int ) $intCid . '
					GROUP BY f.file_id';

		$arrmixFilesCount = fetchData( $strSql, $objDatabase );

		return $arrmixFilesCount;
	}

	public static function fetchCountCountersignRequiredFilesByApplicationIdBySystemCodeByCid( $intApplicationId, $strSystemCode, $intCid, $objDatabase ) {

		$arrintFileCount = [];

		$strSql = 'SELECT
						COUNT ( f.id )
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.applicant_id IS NULL AND fa.customer_id IS NULL )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
						AND fa.file_signed_on IS NOT NULL
						AND f.countersigned_on IS NULL
						AND fa.approved_on IS NULL
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL';

		$arrintFileCount = fetchData( $strSql, $objDatabase );

			if( true == isset ( $arrintFileCount[0]['count'] ) ) {
				return $arrintFileCount[0]['count'];
			} else {
				return 0;
			}
	}

	public static function fetchLeaseFilesByApplicationIdByApplicantIdsByFileIdByCid( $intApplicationId, $arrintApplicantIds, $intFileId = NULL, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeCountersignedFiles = true ) {
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		// TODO: Need to work on this query for fetching distinct records from result

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid AND f.cid = ' . ( int ) $intCid . ' )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
				        JOIN applicant_applications aa ON ( fa.applicant_id = aa.applicant_id AND fa.cid = aa.cid AND aa.application_id = ' . ( int ) $intApplicationId . $strCheckDeletedAASql . '  )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id IN ( \'' . implode( '\',\'', $arrintApplicantIds ) . '\' )
						AND fa.deleted_by IS NULL
						AND f.cid = ' . ( int ) $intCid;

		if( false == $boolIncludeCountersignedFiles ) {
			$strSql .= ' AND f.countersigned_on IS NULL
						AND fa.approved_on IS NULL ';
		}
		if( NULL != $intFileId ) {
			$strSql .= ' AND f.file_id = ' . ( int ) $intFileId;
		}

		$strSql .= ' ORDER BY aa.customer_type_id ASC, aa.lease_generated_on, f.order_num';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCountersignedFilesByApplicationIdByFileTypeSystemCodesByCid( $intApplicationId, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							DISTINCT f.*,
							ft.system_code AS file_type_system_code
							FROM
							    files f
							    JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
							    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
							WHERE
							    f.cid = ' . ( int ) $intCid . '
							    AND fa.application_id = ' . ( int ) $intApplicationId . '
							    AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
							    AND f.countersigned_on IS NOT NULL
							    AND fa.deleted_by IS NULL
							    AND fa.deleted_on IS NULL';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCountCurrentUserApprovedLeaseFilesByApplicationIdByCompanyUserIdByCid( $intApplicationId, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT ( DISTINCT ( f.id ) ) AS count
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid AND f.cid = ' . ( int ) $intCid . ' )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id IS NOT NULL
						AND fa.deleted_by IS NULL
						AND f.countersigned_by = ' . ( int ) $intCompanyUserId . '
						AND fa.approved_on IS NOT NULL';

		$arrintFileCount = fetchData( $strSql, $objDatabase );

		return $arrintFileCount[0]['count'];
	}

	public static function fetchLeasePacketFileBySystemCodeByLeasIdByCid( $intLeaseId, $strSystemCode, $intCid, $objDatabase, $boolExcludeUploadSignedLease = false ) {

		if( false == valStr( $strSystemCode ) ) {
			return NULL;
		}
		if( false == valId( $intCid ) && false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == $boolExcludeUploadSignedLease ) {
			$strCondition = ' AND f.require_countersign = 1 ';
		}

		$strSql = 'SELECT
						fa.id,
						fa.cid,
						fa.lease_id,
						fa.file_id
					FROM
						files f
						JOIN file_types ft ON ( f.cid = ft.cid AND f.file_type_id = ft.id AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\' )
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
					WHERE
						fa.application_id IS NULL
						AND fa.lease_id IS NOT NULL
						AND fa.customer_id IS NULL
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND f.countersigned_by IS NULL
						AND f.countersigned_on IS NULL
						AND f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . $strCondition . '
					LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );

	}

	public static function fetchIdsByFileIdByFileTypeSystemCodeByCid( $intFileId, $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT f.id
					FROM
						files f
						JOIN file_types ft ON( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . addslashes( $strSystemCode ) . '\'
						AND f.file_id = ' . ( int ) $intFileId;

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchCountFilesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $arrintFileIds = [], $boolFromApplication = true, $boolIncludeCurrentUserCountersignedFiles = false, $arrintApplicantIds = [], $intApplicationId = NULL ) {

		if( true == $boolIncludeCurrentUserCountersignedFiles ) {
			$strCondition = ' AND f.updated_by = ' . ( int ) $intCompanyUserId;
		} else {
			$strCondition = ' AND f.countersigned_on IS NULL AND f.countersigned_by IS NULL
							  AND fa.file_signed_on IS NOT NULL
							  AND fa.approved_on IS NULL AND fa.approved_by IS NULL';
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT(f.id) ) AS count
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
						LEFT JOIN document_addendas da ON ( da.cid = f.cid AND da.document_id = f.document_id AND da.id = f.document_addenda_id )
						JOIN company_users cu ON ( cu.cid = f.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND f.require_countersign = 1
						AND
							CASE WHEN cu.is_administrator = 0 AND da.countersign_company_group_ids IS NOT NULL THEN
							ARRAY
							(
								SELECT
									company_group_id
								FROM
									company_user_groups
								WHERE
									cid = ' . ( int ) $intCid . '
									AND company_user_id = ' . ( int ) $intCompanyUserId . '
							) && da.countersign_company_group_ids
							WHEN cu.is_administrator = 1 OR da.countersign_company_group_ids IS NULL THEN
								TRUE
							END
						AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )';

		if( true == $boolFromApplication ) {
			$strSql .= 'AND fa.application_id IS NOT NULL';
		}

		if( true == valArr( $arrintApplicantIds ) ) {
			$strSql .= ' AND fa.applicant_id IN ( ' . sqlIntImplode( $arrintApplicantIds ) . ' )';
		}

		if( false == is_null( $intApplicationId ) ) {
			$strSql .= ' AND fa.application_id = ' . ( int ) $intApplicationId;
		}

		if( true == valArr( $arrintFileIds ) ) {
			$strSql .= ' AND ( f.id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) OR  f.file_id IN ( ' . sqlIntImplode( $arrintFileIds ) . ' ) )';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

			return $arrstrData[0]['count'];

	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedApplicationsByDashboardFilterByCompanyUserByCid( $objDashboardFilter, $objCompanyUser, $intCid, $arrintScreeningFailedApplicationIds, $intOffset, $intPageLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strWhere = '';
		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND file_records.priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strWhere .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )
							OR ca.leasing_agent_id IS NULL ) ';
		}

		if( true == valArr( $arrintScreeningFailedApplicationIds ) ) {
			$strWhere .= ' AND f.applicant_id NOT IN ( ' . implode( ',', $arrintScreeningFailedApplicationIds ) . ' )';
		}

		$strSql = 'SELECT
			                    *
						FROM
						    (
						      SELECT
						          DISTINCT( file_records.* ),
						          CASE
						            WHEN TRUE = file_records.is_pet_policy_conflicted AND EXISTS ( (
                                         SELECT
                                             1
                                         FROM
                                             property_application_preferences AS pap
                                         WHERE
                                             pap.cid = ' . ( int ) $intCid . '
                                             AND pap.property_id = file_records.property_id
                                             AND pap.company_application_id = file_records.company_application_id
                                             AND pap.cid = file_records.cid
                                             AND pap.key = \'HIDE_OPTION_PET\'
						          ) ) THEN 1
						            ELSE 0
						          END AS is_pet_policy_conflicted,
						      c.name_first,
							  c.name_last,
					          COALESCE( c.name_last || \' , \' || c.name_first, c.name_last, c.name_first ) AS applicant_name,
					          RANK() OVER( PARTITION BY fa_1.cid, fa_1.file_id ORDER BY lc.customer_type_id, lc.id ) AS rank1

						      FROM
						          (
						            SELECT
										CASE
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_move_in_date_within\') != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'urgent_move_in_date_within\' )::int ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_lease_type_ids\') != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'urgent_lease_type_ids\' )::integer[] ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_document_signed_since\') != \'\' AND CURRENT_DATE >= ( f.file_signed_on::date + ( dp.approvals_esign_docs->>\'urgent_document_signed_since\' )::int ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_balance_amount\') != \'\' AND f.balance = ( dp.approvals_esign_docs->>\'urgent_balance_amount\' )::float THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'important_document_signed_since\') != \'\' AND CURRENT_DATE >= ( f.file_signed_on::date + ( dp.approvals_esign_docs->>\'important_document_signed_since\' )::int ) THEN
											2
										WHEN TRIM(dp.approvals_esign_docs->>\'important_lease_type_ids\') != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'important_lease_type_ids\' )::integer[] ) THEN
											2
										WHEN TRIM(dp.approvals_esign_docs->>\'important_move_in_date_within\') != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'important_move_in_date_within\' )::int ) THEN
											2
										WHEN TRIM(dp.approvals_esign_docs->>\'important_balance_amount\') != \'\' AND f.balance = ( dp.approvals_esign_docs->>\'important_balance_amount\' )::float THEN
											2
										ELSE
											1
										END AS priority,
						                ca.id AS id,
						                ap.lease_signed_on,
										f.lease_id,
						                f.cid,
						                f.applicant_id,
						                f.application_id AS app_id,
						                f.is_allow_lease_progression,
						                ca.property_id,
						                ca.is_pet_policy_conflicted,
						                ca.guest_remote_primary_key,
						                ca.app_remote_primary_key,
						                ca.application_approved_on,
						                COALESCE ( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
						                ca.company_application_id,
						                CASE
						                  WHEN ca.id IS NULL THEN f.file_signed_on
						                  ELSE ca.lease_completed_on
						                END AS lease_completed_on,
						                p.property_name,
						                sar.screening_decision_type_id,
						                ca.lease_interval_type_id,
						                ca.lease_type_id,
						                ap.require_screening,
						                ap.customer_type_id,
						                sum( CASE WHEN ap.require_screening = ' . CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES . ' THEN 1 ELSE 0 END ) OVER( PARTITION BY ap.cid, ap.application_id ) As total_required_screening_applicants,
						                sum( CASE WHEN ap.require_screening = ' . CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_OPTIONAL . ' THEN 1 ELSE 0 END ) OVER( PARTITION BY ap.cid, ap.application_id ) As total_optional_screening_applicants,
										CASE WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' THEN \'' . __( 'New Lease' ) . '\'
										WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' THEN \'' . __( 'Renewal' ) . '\'
										WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' THEN \'' . __( 'Transfer' ) . '\'
										WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::LEASE_MODIFICATION . ' THEN \'' . __( 'Lease Modification' ) . '\'
										WHEN f.application_id IS NULL THEN \'' . __( 'Replace Lease' ) . '\'
										ELSE \'\'
										END AS lease_interval_type,
						                CASE
						                  WHEN ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ' = sar.request_status_type_id AND 1 = f.is_resident_verify
						                  THEN sar.id
						                END AS screening_application_request_id,
						                CASE
						                  WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' ) AND pp1.value IS NULL THEN 0
						                  WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' AND ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' ) AND pp3.value IS NULL THEN 0
						                  ELSE COALESCE ( f.is_resident_verify, 0 )
						                END AS is_resident_verify,
						                f.id AS file_id,
						                f.title AS document_name,
						                CASE
						                        WHEN f.balance IS NULL THEN 0.00
						                        ELSE f.balance
                                        END AS balance,
                                        cl.occupancy_type_id
						            FROM
						                (
						                  SELECT DISTINCT
						                      f.*,
						                      fa.lease_id,
						                      fa.application_id,
						                      fa.applicant_id,
						                      fa.file_signed_on,
						                      pp2.id AS is_allow_lease_progression,
						                      CASE
						                        WHEN pvt.cid IS NULL THEN 0
						                        ELSE 1
						                      END AS is_resident_verify,
						                      SUM( at.transaction_amount ) OVER ( PARTITION BY at.lease_id, at.cid,fa.customer_id ) AS balance
						                  FROM
						                      files f
						                      JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'TRUE' : 'FALSE' ) . ' ) lp ON ( lp.property_id = f.property_id )
						                      JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' )
						                      JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.deleted_on IS NULL AND fa.lease_id IS NOT NULL )
						                      LEFT JOIN property_preferences pp2 ON ( pp2.cid = f.cid AND pp2.property_id = f.property_id AND pp2.key = \'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION\' AND pp2.value IS NOT NULL )
						                      LEFT JOIN
						                      (
						                        SELECT
						                            DISTINCT ctv.cid,
						                            ptv.property_id,
						                            1 AS is_resident_verify
						                        FROM
						                            company_transmission_vendors ctv
						                            JOIN property_transmission_vendors ptv ON ( ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
						                            JOIN property_products p_prod ON ( ctv.cid = p_prod.cid AND ptv.property_id = p_prod.property_id AND ( p_prod.ps_product_id = ' . CPsProduct::RESIDENT_VERIFY . ' OR p_prod.ps_product_id IS NULL ) )
						                        WHERE
						                            ctv.cid = ' . ( int ) $intCid . '
						                            AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						                      ) pvt ON ( pvt.property_id = f.property_id )
						                      LEFT JOIN ar_transactions at ON ( at.lease_id = fa.lease_id AND fa.cid = at.cid )
						                  WHERE
						                      ft.cid = ' . ( int ) $intCid . '
						                       AND CASE
						                        	WHEN fa.application_id IS NULL THEN 
                                                    	fa.customer_id IS NULL
						                        	ELSE
                                                    	fa.customer_id IS NOT NULL 
						                      	    END
						                      AND fa.approved_by IS NULL
						                      AND ( fa.application_id IS NOT NULL OR f.require_countersign = 1 )
						                ) f
						                JOIN cached_leases cl ON ( cl.id = f.lease_id AND cl.cid = f.cid )
						                JOIN properties p ON ( p.cid = f.cid AND p.id = f.property_id )
						                LEFT JOIN cached_applications ca ON ( ca.id = f.application_id AND ca.cid = f.cid AND ca.cancelled_on IS NULL )
						                LEFT JOIN applicant_applications ap ON ( ap.application_id = f.application_id AND ap.applicant_id = f.applicant_id AND ap.cid = f.cid AND ap.lease_signed_on IS NOT NULL )
						                LEFT JOIN property_preferences pp ON ( pp.cid = f.cid AND pp.property_id = f.property_id AND pp.key = \'REQUIRE_RENTERS_INSURANCE_FOR_LEASE_APPROVAL\' AND pp.value IS NOT NULL )
						                LEFT JOIN property_preferences pp1 ON ( pp1.cid = f.cid AND pp1.property_id = f.property_id AND pp1.key = \'REQUIRE_SCREENING_AFTER_RENEWAL\' AND pp1.value IS NOT NULL )
						                LEFT JOIN property_preferences pp3 ON ( pp3.cid = f.cid AND pp3.property_id = f.property_id AND pp3.key = \'REQUIRE_SCREENING\' AND pp3.value IS NOT NULL )
						                LEFT JOIN screening_application_requests sar ON ( f.cid = sar.cid AND ca.id = sar.application_id )
						                LEFT JOIN
						                (
						                  SELECT
						                      COUNT ( ( f1.id ) ) AS addendas_count,
						                      f1.file_id
						                  FROM
						                      files f1
						                      JOIN file_types ft1 ON ( f1.cid = ft1.cid AND ft1.id = f1.file_type_id )
						                      JOIN company_users cu ON ( cu.cid = f1.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						                      LEFT JOIN document_addendas da ON ( da.cid = f1.cid AND da.document_id = f1.document_id AND da.id = f1.document_addenda_id )
						                  WHERE
						                      f1.cid = ' . ( int ) $intCid . '
						                      AND EXISTS (
						                                   SELECT
						                                       1
						                                   FROM
						                                       file_associations fa1
						                                   WHERE
						                                       ( f1.cid = fa1.cid
						                                       AND f1.id = fa1.file_id )
						                                       AND fa1.deleted_by IS NULL
						                                       AND fa1.deleted_on IS NULL
						                                       AND fa1.approved_on IS NULL
						                                       AND fa1.approved_by IS NULL
						                      )
						                      AND CASE
						                            WHEN cu.is_administrator = 0
						                      AND da.countersign_company_group_ids IS NOT NULL THEN ARRAY
						                      (
						                        SELECT
						                            company_group_id
						                        FROM
						                            company_user_groups
						                        WHERE
						                            CID = cu.cid
						                            AND company_user_id = cu.id
						                      ) && da.countersign_company_group_ids
						                            WHEN cu.is_administrator = 1
						                      OR da.countersign_company_group_ids IS NULL THEN TRUE
						                          END
						                      AND ft1.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
						                  GROUP BY
						                      f1.file_id
						                ) AS allowed_files ON ( f.id = allowed_files.file_id )
						                LEFT JOIN dashboard_priorities dp ON ( dp.cid = f.cid )
						            WHERE
						                f.cid = ' . ( int ) $intCid . '
						                AND ( ca.lease_interval_type_id IS NOT NULL
							                OR ( f.application_id IS NULL
							                AND EXISTS ( (
							                               SELECT
							                                   1
							                               FROM
							                                   files cf
							                                   JOIN file_types ft ON ( ft.cid = cf.cid AND ft.id = cf.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_SIGNED . '\' )
							                               WHERE
							                                   cf.cid = ' . ( int ) $intCid . '
							                                   AND cf.file_id = f.id
							                ) ) ) )
					                    AND ( ( allowed_files.file_id IS NOT NULL AND 0 < allowed_files.addendas_count ) OR ( f.document_id IS NULL ) )
						                AND (
						                        ( CASE WHEN f.is_resident_verify = 1 THEN 
							                            CASE WHEN sar.id IS NOT NULL THEN 
						                                        sar.request_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
										                        AND ( CASE WHEN sar.screening_decision_type_id IS NOT NULL THEN
										                                sar.screening_decision_type_id <> ' . CScreeningDecisionType::DENIED . ' 
										                                AND CASE WHEN sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '  
										                                            AND EXISTS ( 
														                                        SELECT 
														                                            1 
																								FROM
																									screening_application_condition_sets sacs 
																					            WHERE
																									sacs.application_id = sar.application_id 
																									AND sacs.cid = sar.cid 
																									AND sacs.satisfied_by IS NULL 
																									AND sacs.satisfied_on IS NULL
																									AND sacs.is_active = 1
																								) THEN
																			                        ap.lease_signed_on IS NOT NULL
																			                        AND ap.lease_signature IS NOT NULL
																			                        AND ap.lease_ip_address IS NOT NULL
																			                     ELSE
																			                         TRUE
														                    END 
														            END )
									                        WHEN sar.id IS NULL THEN   
										                         sar.id IS NULL
										                  END       
											       ELSE
											            TRUE
							                       END 
										           AND ( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
										                AND ( ( (
									                          SELECT
									                              sub_rip.confirmed_on
									                          FROM
									                              insurance_policy_customers ipc
									                              JOIN resident_insurance_policies sub_rip ON ( sub_rip.cid = ipc.cid AND sub_rip.id = ipc.resident_insurance_policy_id AND sub_rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ', ' . CInsurancePolicyStatusType::ENROLLED . ' ) )
									                          WHERE
									                              ipc.cid = ipc.cid
									                              AND ipc.cid = f.cid
									                              AND ca.lease_id = ipc.lease_id
									                              AND f.application_id = ipc.application_id
									                          ORDER BY
									                              sub_rip.id DESC
									                          LIMIT
									                              1
									                        ) IS NOT NULL ) )
									                AND pp.id IS NOT NULL )
										                OR ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ', ' . CLeaseIntervalType::TRANSFER . ' )
										                OR pp.id IS NULL 
									                )
										                ' . $strWhere . '
										                AND NOT EXISTS (
										                                 SELECT
										                                     1
										                                 FROM
										                                     applicant_applications aa
										                                 WHERE
										                                     aa.cid = ca.cid
										                                     AND aa.application_id = ca.id
										                                     AND aa.customer_type_id = 1
										                                     AND aa.deleted_on IS NOT NULL
									                    )
								                AND ca.application_stage_id = ' . CApplicationStage::LEASE . '
								                AND (
								                        CASE
						                                     WHEN is_allow_lease_progression IS NULL
						                                        THEN ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
						                                     ELSE
						                                        ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' ) AND ap.lease_signed_on IS NOT NULL
						                                END
								                    )
								                )
						                        OR ( ca.id IS NULL AND f.file_signed_on IS NOT NULL AND ( cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' OR cl.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ' OR cl.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' )
						                    )
						            )
						          ) file_records
						            JOIN file_associations fa_1 ON ( file_records.cid = fa_1.cid AND file_records.file_id = fa_1.file_id AND ( ( file_records.id = fa_1.application_id AND file_records.applicant_id = fa_1.applicant_id ) OR ( fa_1.application_id IS NULL ) ) )
                                    JOIN lease_customers lc ON ( fa_1.cid = lc.cid AND fa_1.lease_id = lc.lease_id AND fa_1.customer_id = lc.customer_id )
                                    JOIN customers c ON ( fa_1.customer_id = c.id AND fa_1.cid = c.cid )
                                    WHERE
                                        file_records.cid = ' . ( int ) $intCid . '
						                ' . $strPriorityWhere . '
						    ) t
						WHERE 
								CASE
						            WHEN ( is_allow_lease_progression IS NULL OR app_id IS NULL )
						                THEN rank1 = 1
						            ELSE
						                rank1 IS NOT NULL
						        END
						ORDER BY
							' . $strSortBy . '
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intPageLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApproveLeasesCountByDashboardFilterByCompanyUserByCid( $objDashboardFilter, $objCompanyUser, $intCid, $arrintScreeningFailedApplicationIds, $objDatabase, $intMaxExecutionTimeOut = 0, $boolDashboadReport = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return;
		}

		$strWhere = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND file_records.priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strWhere .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )
						OR ca.leasing_agent_id IS NULL ) ';
		}

		if( true == valArr( $arrintScreeningFailedApplicationIds ) ) {
			$strWhere .= ' AND f.applicant_id NOT IN ( ' . implode( ',', $arrintScreeningFailedApplicationIds ) . ' )';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';
		if( false == $boolDashboadReport ) {
			$strSql .= 'SELECT
					    COUNT ( * ),
					    COALESCE( max(priority), 1 ) AS priority
					FROM
					    (
					      SELECT
					          DISTINCT( file_records.* ),
							  RANK() OVER( PARTITION BY fa_1.cid, fa_1.file_id ORDER BY lc.customer_type_id, lc.id ) AS rank1
					      FROM
					          (';
		} else {
			$strSql .= '
				SELECT
					t1.*
				FROM( ';
		}

		$strSql .= 'SELECT
										(CASE
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_move_in_date_within\') != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'urgent_move_in_date_within\' )::int ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_lease_type_ids\') != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'urgent_lease_type_ids\' )::integer[] ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'urgent_document_signed_since\') != \'\' AND CURRENT_DATE >= ( f.file_signed_on::date + ( dp.approvals_esign_docs->>\'urgent_document_signed_since\' )::int ) THEN
											3
										WHEN TRIM(dp.approvals_esign_docs->>\'important_document_signed_since\') != \'\' AND CURRENT_DATE >= ( f.file_signed_on::date + ( dp.approvals_esign_docs->>\'important_document_signed_since\' )::int ) THEN
											2
										WHEN TRIM(dp.approvals_esign_docs->>\'important_lease_type_ids\') != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'important_lease_type_ids\' )::integer[] ) THEN
											2
										WHEN TRIM(dp.approvals_esign_docs->>\'important_move_in_date_within\') != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'important_move_in_date_within\' )::int ) THEN
												2
											ELSE
												1
										END) AS priority,
						                ca.id AS id,
						                f.lease_id,
						                f.cid,
						                ap.lease_signed_on,
						                f.applicant_id,
						                f.application_id AS app_id,
						                f.is_allow_lease_progression,
						                f.id AS file_id,
						                p.property_name,
						                p.id AS property_id
						                ';

		if( false != $boolDashboadReport ) {
			$strSql .= ', COUNT(f.lease_id) AS approvals_documents, f.rank12';
		}
		$strSql .= '
					                FROM
					                (
					                  SELECT
					                      f.*,
					                      fa.lease_id,
					                      fa.application_id,
					                      fa.applicant_id,
					                      fa.file_signed_on,
					                      pp2.id AS is_allow_lease_progression,
					                      CASE
					                        WHEN pvt.cid IS NULL THEN 0
					                        ELSE 1
					                      END AS is_resident_verify,
					                      RANK() OVER( PARTITION BY fa.cid, fa.file_id ORDER BY lc.customer_type_id, lc.id ) AS rank12
					                  FROM
					                      files f
					                      JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, TRUE ) lp ON ( lp.property_id = f.property_id )
					                      JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' )
					                      JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.deleted_on IS NULL AND fa.lease_id IS NOT NULL )
					                      LEFT JOIN lease_customers lc ON fa.cid = lc.cid AND fa.lease_id = lc.lease_id AND fa.customer_id = lc.customer_id
					                      LEFT JOIN property_preferences pp2 ON ( pp2.cid = f.cid AND pp2.property_id = f.property_id AND pp2.key = \'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION\' AND pp2.value IS NOT NULL )
						                  LEFT JOIN
					                      (
					                        SELECT
					                            DISTINCT ctv.cid,
					                            ptv.property_id,
					                            1 AS is_resident_verify
					                        FROM
					                            company_transmission_vendors ctv
					                            JOIN property_transmission_vendors ptv ON ( ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
					                            JOIN property_products p_prod ON ( ctv.cid = p_prod.cid AND ptv.property_id = p_prod.property_id AND ( p_prod.ps_product_id = ' . CPsProduct::RESIDENT_VERIFY . ' OR p_prod.ps_product_id IS NULL ) )
					                        WHERE
					                            ctv.cid = ' . ( int ) $intCid . '
					                            AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						                      ) pvt ON ( pvt.property_id = f.property_id )
						                  WHERE
						                      ft.cid = ' . ( int ) $intCid . '
						                      ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
						                       AND CASE
						                        	WHEN fa.application_id IS NULL THEN 
                                                    	fa.customer_id IS NULL
						                        	ELSE
                                                    	fa.customer_id IS NOT NULL 
						                      END
					                      AND fa.approved_by IS NULL
					                      AND ( fa.application_id IS NOT NULL OR f.require_countersign = 1 )
					                ) f					                
					                JOIN cached_leases cl ON ( cl.id = f.lease_id AND cl.cid = f.cid )
					                JOIN properties p ON ( p.cid = f.cid AND p.id = f.property_id )
					                LEFT JOIN cached_applications ca ON ( ca.id = f.application_id AND ca.cid = f.cid AND ca.cancelled_on IS NULL )
					                LEFT JOIN applicant_applications ap ON ( ap.application_id = f.application_id AND ap.applicant_id = f.applicant_id AND ap.cid = f.cid AND ap.lease_signed_on IS NOT NULL )
						            LEFT JOIN property_preferences pp ON ( pp.cid = f.cid AND pp.property_id = f.property_id AND pp.key = \'REQUIRE_RENTERS_INSURANCE_FOR_LEASE_APPROVAL\' AND pp.value IS NOT NULL )
					                LEFT JOIN screening_application_requests sar ON ( f.cid = sar.cid AND ca.id = sar.application_id )
					                LEFT JOIN
					                (
					                  SELECT
					                      COUNT ( ( f1.id ) ) AS addendas_count,
					                      f1.file_id
					                  FROM
					                      files f1
					                      JOIN file_types ft1 ON ( f1.cid = ft1.cid AND ft1.id = f1.file_type_id )
					                      JOIN company_users cu ON ( cu.cid = f1.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					                      LEFT JOIN document_addendas da ON ( da.cid = f1.cid AND da.document_id = f1.document_id AND da.id = f1.document_addenda_id )
					                  WHERE
					                      f1.cid = ' . ( int ) $intCid . '
					                      AND EXISTS (
					                                   SELECT
					                                       1
					                                   FROM
					                                       file_associations fa1
					                                   WHERE
					                                       ( f1.cid = fa1.cid
					                                       AND f1.id = fa1.file_id )
					                                       AND fa1.deleted_by IS NULL
					                                       AND fa1.deleted_on IS NULL
					                                       AND fa1.approved_on IS NULL
					                                       AND fa1.approved_by IS NULL
					                      )
					                      AND CASE
					                            WHEN cu.is_administrator = 0
					                      AND da.countersign_company_group_ids IS NOT NULL THEN ARRAY
					                      (
					                        SELECT
					                            company_group_id
					                        FROM
					                            company_user_groups
					                        WHERE
					                            CID = cu.cid
					                            AND company_user_id = cu.id
					                      ) && da.countersign_company_group_ids
					                            WHEN cu.is_administrator = 1
					                      OR da.countersign_company_group_ids IS NULL THEN TRUE
					                          END
					                      AND ft1.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
					                  GROUP BY
					                      f1.file_id
					                ) AS allowed_files ON ( f.id = allowed_files.file_id )
					                LEFT JOIN dashboard_priorities dp ON ( dp.cid = f.cid )
					            WHERE
					                f.cid = ' . ( int ) $intCid . '					            	
					                AND ( ca.lease_interval_type_id IS NOT NULL
							                OR ( f.application_id IS NULL
							                AND exists (  
							                (
							                  SELECT
							                      1
							                  FROM
							                      files cf
							                      JOIN file_types ft ON ( ft.cid = cf.cid AND ft.id = cf.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_SIGNED . '\' )
							                  WHERE
							                      cf.cid = ' . ( int ) $intCid . '
							                      and cf.file_id = f.id
							                ) ) ) ) 
					                AND ( ( allowed_files.file_id IS NOT NULL
					                AND 0 < allowed_files.addendas_count )
					                OR ( f.document_id IS NULL ) )
					                AND (
					                        ( CASE WHEN f.is_resident_verify = 1 THEN 
							                            CASE WHEN sar.id IS NOT NULL THEN 
						                                        sar.request_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
										                        AND ( CASE WHEN sar.screening_decision_type_id IS NOT NULL THEN
										                                sar.screening_decision_type_id <> ' . CScreeningDecisionType::DENIED . ' 
										                                AND CASE WHEN sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '  
										                                            AND EXISTS ( 
														                                        SELECT 
														                                            1 
																								FROM
																									screening_application_condition_sets sacs 
																					            WHERE
																									sacs.application_id = sar.application_id 
																									AND sacs.cid = sar.cid 
																									AND sacs.satisfied_by IS NULL 
																									AND sacs.satisfied_on IS NULL
																									AND sacs.is_active = 1
																								) THEN
																			                        ap.lease_signed_on IS NOT NULL
																			                        AND ap.lease_signature IS NOT NULL
																			                        AND ap.lease_ip_address IS NOT NULL
																			                     ELSE
																			                         TRUE
														                    END 
														            END )
									                        WHEN sar.id IS NULL THEN   
										                         sar.id IS NULL
										                  END       
											       ELSE
											            TRUE
							                       END 
									                AND ( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
									                AND ( ( (
									                          SELECT
									                              sub_rip.confirmed_on
									                          FROM
									                              insurance_policy_customers ipc
									                              JOIN resident_insurance_policies sub_rip ON ( sub_rip.cid = ipc.cid AND sub_rip.id = ipc.resident_insurance_policy_id AND sub_rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ', ' . CInsurancePolicyStatusType::ENROLLED . ' ) )
									                          WHERE
									                              ipc.cid = ipc.cid
									                              AND ipc.cid = f.cid
									                              AND ca.lease_id = ipc.lease_id
									                              AND f.application_id = ipc.application_id
									                          ORDER BY
									                              sub_rip.id DESC
									                          LIMIT
									                              1
									                ) IS NOT NULL ) )
									                AND pp.id IS NOT NULL )
									                OR ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ', ' . CLeaseIntervalType::TRANSFER . ' )
									                OR pp.id IS NULL )
									                ' . $strWhere . '
									                AND NOT EXISTS (
									                                 SELECT
									                                     1
									                                 FROM
									                                     applicant_applications aa
									                                 WHERE
									                                     aa.cid = ca.cid
									                                     AND aa.application_id = ca.id
									                                     AND aa.customer_type_id = 1
									                                     AND aa.deleted_on IS NOT NULL
								                    )
							                AND ca.application_stage_id = ' . CApplicationStage::LEASE . '
							                AND (
								                    CASE
						                                  WHEN is_allow_lease_progression IS NULL
						                                     THEN ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
						                                   ELSE
						                                      ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' ) AND ap.lease_signed_on IS NOT NULL
						                             END
						                         )
								            )
					                        OR ( ca.id IS NULL AND f.file_signed_on IS NOT NULL AND ( cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' OR cl.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ' OR cl.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ')
					                    )
					            )
					            GROUP BY 
									dp.approvals_esign_docs ->> \'urgent_move_in_date_within\', 
									dp.approvals_esign_docs ->> \'urgent_lease_type_ids\',
									dp.approvals_esign_docs ->> \'important_lease_type_ids\',
									dp.approvals_esign_docs ->> \'important_move_in_date_within\',
					                p.id,
					                p.property_name,
					                ca.id,
					                priority,
					                f.lease_id,
					                f.cid,
					                f.id,
					                ap.lease_signed_on,
						            f.application_id,
						            f.applicant_id,
						            f.is_allow_lease_progression,
						            p.property_name';

		if( false == $boolDashboadReport ) {
			$strSql .= ' ) file_records
						            JOIN file_associations fa_1 ON ( file_records.cid = fa_1.cid AND file_records.file_id = fa_1.file_id AND ( ( file_records.id = fa_1.application_id AND file_records.applicant_id = fa_1.applicant_id ) OR ( fa_1.application_id IS NULL ) ) )
                                    JOIN lease_customers lc ON ( fa_1.cid = lc.cid AND fa_1.lease_id = lc.lease_id AND fa_1.customer_id = lc.customer_id )
                                    JOIN customers c ON ( fa_1.customer_id = c.id AND fa_1.cid = c.cid )
                                WHERE
                                    file_records.cid = ' . ( int ) $intCid . ' 
						          ' . $strPriorityWhere . '
						    ) t
							WHERE
								CASE
						           WHEN ( is_allow_lease_progression IS NULL OR app_id IS NULL )
					               THEN rank1 = 1
						           ELSE
						               rank1 IS NOT NULL
						        END';
			return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objDatabase );
		} else {
			$strSql .= ', f.rank12 ) AS t1
				WHERE
					CASE
						WHEN ( t1.is_allow_lease_progression IS NULL OR t1.app_id IS NULL )
							THEN t1.rank12 = 1
						ELSE t1.rank12 IS NOT NULL
					END
			';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesByFileIdsByFileTypeSystemCodesByCid( $arrintFileIds, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}
		if( false == valArr( $arrintFileIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND fa.deleted_by IS NULL
						AND f.cid = ' . ( int ) $intCid . '
					ORDER BY f.order_num';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						fa.ap_payment_batch_id,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id )
						LEFT JOIN file_types ft ON ( ft.cid = f.cid and ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.deleted_by IS NULL
						AND f.id IN ( ' . implode( ',', $arrintFileIds ) . ' ) ';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesForBulkApprovalByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						fa.check_component_type_id,
						fa.application_id,
						fa.lease_id,
						ft.system_code AS file_type_system_code,
						fa.approved_by,
						fa.approved_on,
						CASE
                            WHEN fa1.id IS NULL THEN NULL
                            ELSE fa1.file_signed_on
                        END AS file_signed_on
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						LEFT JOIN file_associations fa1 ON ( fa1.cid = fa.cid AND fa1.file_id = fa.file_id AND fa1.customer_id IS NOT NULL AND fa1.file_signed_on IS NOT NULL )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid  = ' . ( int ) $intCid . '
						AND f.id IN ( ' . implode( ',', $arrintIds ) . ' )
					ORDER BY
						fa.check_component_type_id ASC';

		return parent::fetchFiles( $strSql, $objClientDatabase );
	}

	public static function fetchPolicyPacketCountByApplicationIdByDocumentIdBySystemCode( $intApplicationId, $intDocumentId, $strSystemCode, $intCid, $objDatabase ) {

		if( false == valId( $intApplicationId ) || false == valId( $intDocumentId ) ) {
			return NULL;
		}
		if( false == valStr( $strSystemCode ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*
					FROM
						files f
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . $strSystemCode . '\'
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND f.document_id = ' . ( int ) $intDocumentId;

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileByLeaseIdBySystemCodeByPreCollectionTypeByCid( $intLeaseId, $strSystemCode, $strPreCollectionType, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.file_name
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND ft.system_code = \'' . $strSystemCode . '\'
						AND fa.deleted_by IS NULL
		                AND f.file_name LIKE \'' . $strPreCollectionType . '%\'
					ORDER BY
		                f.id DESC
		            LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileDetailsByIdByCid( $intFileId, $intCid, $objDatabase ) {

		if( false == valId( $intFileId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.title,
						f.id as file_id,
						ap.id as ap_payee_id,
						ap.company_name,
						app.id As ap_payment_id,
						app.payment_number
					FROM
						files f
						JOIN file_associations fa ON( f.cid =fa.cid AND f.id = fa.file_id )
						JOIN ap_payees ap ON( fa.cid = ap.cid AND fa.ap_payee_id = ap.id)
						JOIN ap_payments app ON( fa.cid = app.cid AND fa.ap_payment_id = app.id)
					WHERE
						f.id = ' . ( int ) $intFileId . '
						AND f.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesBySubsidyHapRequestIdByCid( $intSubsidyHapRequestId, $intCid, $objDatabase ) {

		if( false == valId( $intSubsidyHapRequestId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
					JOIN
						file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND fa.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
						AND fa.deleted_by IS NULL 
						AND fa.deleted_on IS NULL
					ORDER BY
						f.document_id ASC ';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesBySubsidyCertificationIdByCid( $intSubsidyCertificationId, $intCid, $objDatabase, $intLimit = 0, $boolIsTrial = false ) {

		if( false == valId( $intSubsidyCertificationId ) || false == valId( $intCid ) ) return NULL;

		$strLimit = ( 0 < ( int ) $intLimit ) ? ' LIMIT ' . ( int ) $intLimit . '' : '';

		$strFileNameCondition = ( false == $boolIsTrial ) ? ' AND f.file_name NOT LIKE \'TRIAL_%\' ' : '';

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.subsidy_certification_id = ' . ( int ) $intSubsidyCertificationId . $strFileNameCondition . '
						AND fa.deleted_by IS NULL 
						AND fa.deleted_on IS NULL 
						ORDER BY f.id DESC' . $strLimit;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesBySubsidySpecialClaimIdByCid( $intSubsidySpecialClaimId, $intCid, $objDatabase ) {

		if( false == valId( $intSubsidySpecialClaimId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
					JOIN
						file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND fa.subsidy_special_claim_id = ' . ( int ) $intSubsidySpecialClaimId . '
						AND fa.deleted_by IS NULL 
						AND fa.deleted_on IS NULL
					ORDER BY
						f.file_type_id ASC';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdsByExcludingFileTypeSystemCodesByCompanyUserByCid( $arrintApplicationIds, $arrstrSystemCodes, $objCompanyUser, $intCid, $objDatabase, $boolIsIncludeCancelLeaseFiles = false, $boolIsResidentVerifyEnabledProperty = false, $arrintDocumentIds = NULL, $strUpdatedOn = NULL, $boolIsExcludeDeletedFileAssociations = false, $boolExcludeFilesWithoutSystemCode = false, $arrintApplicantIds = NULL ) {
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}
		$strOrCondition = ' OR ft.system_code IS NULL ';
		if( false != $boolExcludeFilesWithoutSystemCode ) {
			$strOrCondition = NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						fa.lease_id,
						CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									ELSE \'' . __( 'e-Sign: Lease Modification' ) . '\'
								END
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
                                    \'' . __( 'Application: Policy' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
                                    \'' . __( 'e-Sign: Addenda' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
                                    \'' . __( 'Automated' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
                                     \'' . __( 'Application Upload' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
                                    \'' . __( 'Renewal Offer Sent' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
                                     \'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
                                     \'' . __( 'Transfer Document' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_WAITLIST . '\' ) OR ft.system_code = ANY( ARRAY[ \'' . implode( "', '", CFileType::$c_arrstrAffordableFileTypes ) . '\' ] ) THEN
                                     \'\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_WORK_ORDER . '\' OR ft.system_code IS NOT NULL OR f.details IS NOT NULL THEN
                            		ft.name
	                        ELSE \'' . __( 'Upload' ) . '\'
						END AS file_type_name,
						fa.file_signed_on,
						fa.sign_initiated_on,
						fa.application_id AS application_id,
						fa.require_sign AS require_sign,
						fa.deleted_by,
						fa.quote_id,
						q.accepted_on AS quote_offer_accepted_on,
						q.created_on As quote_created_on,
						util_get_translated( \'name\', lt.name, lt.details , \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS lease_term_name';

		if( 1 == $objCompanyUser->getIsAdministrator() ) {
			$strSql .= ', TRUE AS is_allowed_user_countersign';
		} else {
			$strSql .= ', CASE
								WHEN da.countersign_company_group_ids IS NOT NULL THEN ARRAY
								(
								 SELECT
					      cug.company_group_id
					  FROM
					      company_user_groups cug
					  WHERE
					      cug.cid = ' . ( int ) $intCid .
					      ' AND cug.company_user_id = ' . ( int ) $objCompanyUser->getId() .
					') && da.countersign_company_group_ids
					      WHEN da.countersign_company_group_ids IS NULL THEN TRUE
					    END AS is_allowed_user_countersign';
		}

		if( true == $boolIsResidentVerifyEnabledProperty ) {
			$strSql .= ', d.document_sub_type_id, fa.applicant_id';
		}

		$strSql .= '
					FROM
						file_associations fa
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )';

		if( true == $boolIsResidentVerifyEnabledProperty ) {
			$strSql .= ' LEFT JOIN documents d ON ( f.document_id = d.id AND f.cid = d.cid )';
		}

		$strSql .= '
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						LEFT JOIN events e ON ( e.event_type_id = ' . CEventType::DOCUMENT_DELETED . ' AND e.data_reference_id = f.id AND e.cid = f.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )
						LEFT JOIN quotes q ON (ca.cid = q.cid AND q.application_id = ca.id AND q.id = fa.quote_id )
						LEFT JOIN quote_lease_terms qlt ON ( qlt.cid = q.cid AND qlt.id = q.selected_quote_lease_term_id )
						LEFT JOIN lease_terms lt ON ( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id AND lt.id = qlt.lease_term_id )
						LEFT JOIN document_addendas da ON ( da.cid = f.cid AND da.document_id = f.document_id AND da.id = f.document_addenda_id )
					WHERE ( fa.application_id IN ( \'' . implode( '\',\'', $arrintApplicationIds ) . '\' ) ';

			if( true == valArr( $arrintApplicantIds ) ) {
				$strSql .= ' OR ( fa.applicant_id IN ( \'' . implode( '\',\'', $arrintApplicantIds ) . '\' ) AND ft.system_code =	\'' . CFileType::SYSTEM_CODE_APPLICANT_IDENTIFICATION . '\' ) ';
			}

			$strSql .= ' ) AND CASE WHEN ( e.event_type_id = ' . CEventType::DOCUMENT_DELETED . ' ) THEN
								e.data_reference_id = f.id AND ( fa.deleted_by IS NULL AND e.id IS NOT NULL )
							ELSE
								true
							END
						AND CASE WHEN ( f.file_id IS NOT NULL ) THEN
								ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\'
							ELSE
								true
							END
						AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' ) ' . $strOrCondition . ')
						AND fa.cid = ' . ( int ) $intCid;

		if( false == $boolIsIncludeCancelLeaseFiles || true == $boolIsExcludeDeletedFileAssociations ) {
			$strSql .= ' AND fa.deleted_by IS NULL';
		}
		if( false != valArr( $arrintDocumentIds ) ) {
			$strSql .= ' AND f.id IN ( ' . implode( ',', $arrintDocumentIds ) . ')';
		}
		if( false != valStr( $strUpdatedOn ) ) {
			$strSql .= ' AND f.updated_on >= \'' . date( 'm/d/Y', strtotime( $strUpdatedOn ) ) . ' 00:00:00\'';
		}

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileBySubsidyCertificationIdByFileTypeIdByCid( $intSubsidyCertificationId, $intFileTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.property_id = f.property_id AND fa.file_id = f.id AND fa.deleted_on IS NULL )
						JOIN subsidy_certifications sc ON ( sc.cid = fa.cid AND sc.property_id = fa.property_id AND sc.lease_id = fa.lease_id AND sc.id = fa.subsidy_certification_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.file_type_id = ' . ( int ) $intFileTypeId . '
						AND sc.id = ' . ( int ) $intSubsidyCertificationId . '
					LIMIT 1';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesBySubsidyCertificationIdByFileTypeSystemCodesByCid( $intSubsidyCertificationId, $arrstrFileTypeSystemCodes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) return;

		$strSql = 'SELECT
						DISTINCT ON ( ft.id )
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON ( ft.cid = f.cid AND ft.id = f.file_type_id )
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.property_id = f.property_id AND fa.file_id = f.id AND fa.deleted_on IS NULL )
						JOIN subsidy_certifications sc ON ( sc.cid = fa.cid AND sc.property_id = fa.property_id AND sc.lease_id = fa.lease_id AND sc.id = fa.subsidy_certification_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ft.system_code IN ( \'' . implode( "', '", $arrstrFileTypeSystemCodes ) . '\' )
						AND sc.id = ' . ( int ) $intSubsidyCertificationId;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicantIdsByCustomerIdsByLeaseIdsByExcludingFileTypeSystemCodesByCompanyUserByCid( $arrintApplicantIds, $arrintCustomerIds, $arrintLeaseIds, $arrstrFileTypeSystemCodes, $objCompanyUser, $intCid, $objDatabase, $boolShowInPortal = false, $boolSeparateLeaseDocuments = false, $boolIsShowDeletedDocuments = false, $boolIsFromResidentPortal = false, $arrintDocumentIds = NULL, $strUpdatedOn = NULL, $boolExcludeFilesWithoutSystemCode = false ) {
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) {
			return NULL;
		}
		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}
		$strOrCondition = ' OR ft.system_code IS NULL ';
		if( false != $boolExcludeFilesWithoutSystemCode ) {
			$strOrCondition = NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (f.id) f.*,
						ft.system_code AS file_type_system_code,
						CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::LEASE_MODIFICATION . '\' THEN \'' . __( 'e-Sign: Lease Modifications' ) . '\'
									ELSE \'' . __( 'e-Sign: Replace Lease' ) . '\'
								END
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_VIOLATION_MANAGER . '\' THEN ft.name || \' - \' || v.name
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
                                    \'' . __( 'Application: Policy' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
                                    \'' . __( 'e-Sign: Addenda' ) . '\'
                            WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
                                    \'' . __( 'Automated' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
                                     \'' . __( 'Application Upload' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
                                    \'' . __( 'Renewal Offer Sent' ) . '\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
                                     \'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
                                     \'' . __( 'Transfer Document' ) . '\'
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_WAITLIST . '\' ) OR ft.system_code = ANY( ARRAY[ \'' . implode( "', '", CFileType::$c_arrstrAffordableFileTypes ) . '\' ] ) THEN
                                     \'\'
                            WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_WORK_ORDER . '\' OR ft.system_code IS NOT NULL OR f.details IS NOT NULL THEN
                            		ft.name
	                        ELSE \'' . __( ' Upload' ) . '\'
						END AS file_type_name,
						v.violation_datetime As violation_datetime,
						fa.id AS file_association_id,
						fa.customer_id AS customer_id,
						fa.lease_id AS lease_id,
						fa.require_sign,
						fa.file_signed_on,
						fa.sign_initiated_on,
						fa.application_id AS application_id,
						fa.deleted_by';

		if( 1 == $objCompanyUser->getIsAdministrator() ) {
			$strSql .= ', TRUE AS is_allowed_user_countersign';
		} else {
			$strSql .= ', CASE
								WHEN da.countersign_company_group_ids IS NOT NULL THEN ARRAY
								(
								 SELECT
					      company_group_id
					  FROM
					      company_user_groups
					  WHERE
					      cid = ' . ( int ) $intCid .
						 ' AND company_user_id = ' . ( int ) $objCompanyUser->getId() .
						 ') && da.countersign_company_group_ids
								      WHEN da.countersign_company_group_ids IS NULL THEN TRUE
								    END AS is_allowed_user_countersign';
		}

		if( true == $boolSeparateLeaseDocuments ) {
			$strSql .= ' ,
					    CASE WHEN ft.system_code = \'LEASE\' AND tfa.total_file_associations_count = sfa.signed_file_associations_count THEN
					         \'eSign: Lease\'
					    WHEN ft.system_code = \'LEASE\' AND tfa.total_file_associations_count > sfa.signed_file_associations_count AND 0 < sfa.signed_file_associations_count THEN
					        \'Partially-eSigned\'
					    WHEN ft.system_code = \'LEASE\' THEN
					        \'Pre-signed\'
					    END AS file_generation_type';
		}

		$strSql .= ' FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' )' . $strOrCondition . ' ) )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						LEFT JOIN violations v ON ( v.cid = fa.cid AND v.id = fa.violation_id )
						LEFT JOIN document_addendas da ON ( da.cid = f.cid AND da.document_id = f.document_id AND da.id = f.document_addenda_id )';
		$strSql .= ( true == valArr( $arrintCustomerIds ) && true == $boolIsFromResidentPortal ) ? ' JOIN file_associations fa1 ON ( fa1.file_id = fa.file_id AND fa1.cid = fa.cid AND fa1.customer_id IS NOT NULL AND fa1.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) )' : '';
		$strSql .= ' JOIN lease_customers lc ON ( fa.lease_id = lc.lease_id AND fa.cid = lc.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )';
		if( true == $boolSeparateLeaseDocuments ) {
			$strSql .= ' LEFT JOIN ( SELECT
                                        file_id,
										cid,
                                        COUNT(id) AS total_file_associations_count
					                 FROM
					                     file_associations
					                 WHERE
					                     applicant_id IS NOT NULL
					                     AND deleted_by IS NULL
					                     AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
					                     AND cid = ' . ( int ) $intCid . '
					                     GROUP BY file_id,
					                              cid
					                ) AS tfa ON ( tfa.file_id = f.id AND tfa.cid = f.cid )
				       LEFT JOIN ( SELECT
				                      isfa.file_id AS file_id,
					                  isfa.cid,
				                      COUNT(file_association_id) AS signed_file_associations_count
				                  FROM (
				                          SELECT
				                              ifa.id AS file_association_id,
				                              ifa.file_id,
					                          ifa.cid,
				                              ifa.applicant_id,
				                              ifs.page_number,
				                              ifs.agreement_datetime AS agreement_datetime,
				                              rank() OVER ( PARTITION BY ifa.id ORDER BY ifs.page_number DESC )
				                          FROM
				                              file_associations ifa
				                              JOIN file_signatures ifs ON ( ifs.file_association_id = ifa.id AND ifs.cid = ifa.cid )
				                          WHERE
				                              ifa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
				                              AND ifa.cid = ' . ( int ) $intCid . '
				                              AND ifa.applicant_id IS NOT NULL
				                              AND ifa.deleted_by IS NULL
				                      ) AS isfa
				                  WHERE
				                      isfa.rank = 1
				                      AND isfa.agreement_datetime IS NOT NULL
				                      AND isfa.cid = ' . ( int ) $intCid . '
				                  GROUP BY
				                      file_id,
				                      cid
				                ) AS sfa ON ( sfa.file_id = f.id AND sfa.cid = f.cid )';
		}

		$strSql .= ' WHERE
						fa.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		if( false == $boolIsShowDeletedDocuments ) {
			$strSql .= ' AND fa.deleted_by IS NULL';
		}

		$strSql .= ' AND CASE WHEN ( f.file_id IS NOT NULL ) THEN
								( ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\' ';

		if( true == $boolShowInPortal ) {
			$strSql .= ' OR ft.system_code <>	\'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ';
		}

		$strSql .= ' )
							ELSE
								true
							END
						AND ( ( fa.customer_id IS NULL AND fa.applicant_id IS NULL )';

		$strSql .= ( true == valArr( $arrintApplicantIds ) ) ? ' OR fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )' : '';
		$strSql .= ( true == valArr( $arrintCustomerIds ) ) ? ' OR fa.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )' : '';

		$strSql .= ' ) AND f.cid = ' . ( int ) $intCid;

		if( true == $boolShowInPortal ) {
			$strSql .= ' AND f.show_in_portal = 1';
		}

		if( false != valArr( $arrintDocumentIds ) ) {
			$strSql .= ' AND f.id IN ( ' . implode( ',', $arrintDocumentIds ) . ')';
		}
		if( false != valStr( $strUpdatedOn ) ) {
			$strSql .= ' AND f.updated_on >= \'' . date( 'm/d/Y', strtotime( $strUpdatedOn ) ) . ' 00:00:00\'';
		}

		$strSql .= ' ORDER BY f.id ASC';

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchFilesByLeaseIdByFileTypeSystemCodesByCid( $intLeaseId, $arrstrFileTypeSystemCodes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) return;

		$strSql = 'SELECT
						f.id,
						f.title,
						f.file_name,
						f.created_on
					FROM
						files f
						JOIN file_associations fa on ( f.cid = fa.cid AND f.id = fa.file_id )
						JOIN file_types ft on ( ft.cid = f.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.lease_id = ' . ( int ) $intLeaseId . '
						AND ft.system_code IN ( ' . sqlStrImplode( $arrstrFileTypeSystemCodes ) . ')
						AND fa.deleted_by IS NULL
					ORDER BY 
						f.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileByApplicationIdByFileTypeSystemCodesByDocumentSubTypeIdByCidByDocumentName( $intApplicationId, $arrstrSystemCodes, $intDocumentSubTypeId, $intCid, $strDocumentName, $objDatabase, $boolIsIncludeDeletedFile = false ) {
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN documents d ON ( f.document_id = d.id AND f.cid = d.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND d.name like \'%' . $strDocumentName . '%\'
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )';

		if( false == $boolIsIncludeDeletedFile ) {
			$strSql .= ' AND fa.deleted_by IS NULL';
		}

		$strSql .= '	AND f.cid = ' . ( int ) $intCid . '
						ORDER BY f.order_num LIMIT 1';

		$strSql = ' SELECT
						DISTINCT ON (id) *	
					FROM ( ' . $strSql . ') AS files
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserDocumentsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql  = 'SELECT
						f.*
					FROM
						files f
						JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = f.file_type_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . CFileType::SYSTEM_CODE_USER_DOCUMENT . '\'
						AND f.company_user_id = ' . ( int ) $intCompanyUserId . '
					ORDER BY
						f.id desc';

		return fetchData( $strSql, $objDatabase );
	}

	public static function areAllApplicantsAreCounterSigned( $intApplicationId, $intCid, $objDatabase ) {

		$strSql  = 'SELECT 
						1
					FROM 
						files f
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id AND fa.deleted_by IS NULL AND fa.deleted_on IS NULL  )
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'LP\' )   
					WHERE
					  fa.application_id = ' . ( int ) $intApplicationId . '
					  AND f.cid = ' . ( int ) $intCid . '
					  AND f.countersigned_by IS NOT NULL
					  AND f.countersigned_on IS NOT NULL  ';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResult ) ) ? true : false;
	}

	public static function fetchFileByMacPeriodIdByMacPeriodStatusIdBySystemCodeByCid( $intMacPeriodId, $strFileSystemCode, $intCid, $objDatabase ) {
		if( false == valId( $intMacPeriodId ) || false == valStr( $strFileSystemCode ) ) return false;

		$strSql = 'SELECT
						f.*,
						ft.system_code as file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft ON( f.cid = ft.cid AND f.file_type_id = ft.id AND ft.system_code = \'' . $strFileSystemCode . '\' )
					WHERE
						fa.military_mac_period_id =' . ( int ) $intMacPeriodId . '
						AND f.cid = ' . ( int ) $intCid . '
					ORDER BY f.id desc
						LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFileTypeByLeaseIdsByFileTypeIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintFileTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $intCustomerId = NULL ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintFileTypeIds ) ) {
			return NULL;
		}

		$strWhereSql = ( true == valId( $intCustomerId ) ) ? ' AND fa.customer_id = ' . ( int ) $intCustomerId : '';

		$strSql = ' SELECT
						f.file_type_id, ca.lease_id
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN (
							SELECT sub.*
                                FROM (
                                       SELECT a.cid,
                                              a.id,
                                              a.lease_id,
                                              rank() OVER( PARTITION BY a.cid, a.lease_id ORDER BY a.id) as rank
                                       FROM applications a
                                            JOIN cached_leases c ON (c.cid = a.cid AND c.property_id =
                                              a.property_id AND c.id = a.lease_id )
                                            LEFT JOIN lease_intervals li ON (li.cid = a.cid AND
                                              li.property_id = a.property_id AND li.lease_id = a.lease_id
                                              AND a.lease_interval_id = li.id AND li.lease_start_date <=
                                              c.lease_start_date AND li.lease_interval_type_id NOT IN (' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' ) AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::CANCELLED . ' ) )
                                          WHERE a.cid = ' . ( int ) $intCid . ' AND
                                             a.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND
                                             a.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND
                                             CASE
                                               WHEN c.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN li.id IS NOT NULL
                                               ELSE a.lease_interval_id = c.active_lease_interval_id
                                             END
                                       ORDER BY a.created_on DESC
                                     ) as sub
                                WHERE sub.rank = 1
						) ca on ( ca.cid = fa.cid 
									AND (
									fa.application_id = ca.id
									OR fa.lease_id = ca.lease_id
									)
								) 
                    WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.file_type_id IN ( ' . implode( ',', $arrintFileTypeIds ) . ' )  
						AND fa.deleted_by IS NULL 
						AND fa.deleted_on IS NULL 
						AND ( f.details -> \'file_verification\' -> \'current_status\' IS NULL  OR f.details -> \'file_verification\' ->> \'current_status\' = \'VERIFIED\' )
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) '
						. $strWhereSql . '
					GROUP BY 	
					   f.file_type_id, 
					   ca.lease_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesForFilesExportByFileSearchArray( $arrmixFileSearch, $intCid, $objDatabase ) {

		$strApplicantNameSql         = '';
		$strActiveApplicantsAndResidentsSql = '';

		if( 'active_applicants_only' == $arrmixFileSearch['entity_type'] || 'active_applicants_and_residents_only' == $arrmixFileSearch['entity_type'] ) {
			$strActiveApplicantsAndResidentsSql = '  AND  (  ca.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . '  ) OR  ca.application_status_id IS NULL )';

		}

		$arrstrRequiredFilesSystemCodes = [
			CFileType::SYSTEM_CODE_LEASE_DOCUMENT,
			CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE,
			CFileType::SYSTEM_CODE_LEASE_ADDENDUM,
			CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET,
			CFileType::SYSTEM_CODE_LEASE_PACKET,
			CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID,
			CFileType::SYSTEM_CODE_SIGNED,
			CFileType::SYSTEM_CODE_PRE_SIGNED,
			CFileType::SYSTEM_CODE_PARTIALLY_SIGNED,
			CFileType::SYSTEM_CODE_TENANT_STATEMENT,
			CFileType::SYSTEM_CODE_WORK_ORDER,
			CFileType::SYSTEM_CODE_MAINTENANCE_ATTACHMENT,
			CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT
		];

		$arrintRequiredFileTypeIds = CFileTypes::fetchFileTypeIdsBySystemCodesByCid( $arrstrRequiredFilesSystemCodes, $intCid, $objDatabase );

		$arrintDocumentLevelFileTypeIds = [ $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID], $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_TENANT_STATEMENT], $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT] ];

		$arrstrInsuranceFileTypes = [ '\'' . CFileType::SYSTEM_CODE_PROOF_OF_INSURANCE . '\'', '\'' . CFileType::SYSTEM_CODE_POLICY . '\'' ];

		if( 'unit_status_last_name' == $arrmixFileSearch['organize_by'] || 'unit_lastname_doctype_docid_filename' == $arrmixFileSearch['organize_by'] ) {

			$strApplicantNameSql .= 'COALESCE(cl.name_last, ca.name_last) AS name_resident';

		} else {

			$strApplicantNameSql .= 'CASE
                             WHEN COALESCE(cl.name_last, ca.name_last) IS NOT NULL 
                             THEN CONCAT(COALESCE(cl.name_last, ca.name_last), \' - \', COALESCE(cl.name_first, ca.name_first))
                             ELSE COALESCE(cl.name_first, ca.name_first)
                         END AS name_resident';
		}

		$strSql = 'WITH files_records AS(
                  SELECT
                     f.cid,
                     f.id as file_id,
                     f.file_id as parent_file_id,
                     f.file_name,
                     f.title,
                     f.file_path,
                     f.show_in_portal,
                     ft.id as file_type_id,
                     ft.name,
                     COALESCE( fft.system_code, ft.system_code ) as system_code,
                     ft.name as file_type_name,
                     p.id as property_id,
                     p.property_name,
                     us.unit_number_cache,
                     us.building_name,
                     lst.name as file_metadata_lease_status_type_name,
                      ' . $strApplicantNameSql . ',
                      COALESCE( cl.lease_start_date, ca.lease_start_date ) as lease_start_date,
                      COALESCE( cl.primary_phone_number, ca.primary_phone_number ) as primary_phone_number,
                      ca.email_address as email_address                      
                  FROM
                     files f
                      LEFT JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id AND d.file_type_id IN ( ' . implode( ',', $arrintDocumentLevelFileTypeIds ) . ' ) )
                      LEFT JOIN file_types fft ON ( f.cid = fft.cid AND f.file_type_id = fft.id )
                      LEFT JOIN file_types ft ON ( f.cid = ft.cid AND ft.id = COALESCE( d.file_type_id, f.file_type_id ) )
                      LEFT JOIN file_associations fa ON (f.cid = fa.cid AND f.id = fa.file_id AND fa.deleted_on IS NULL ) 
                      LEFT JOIN applicant_applications aa  ON ( fa.applicant_id = aa.applicant_id AND fa.cid = aa.cid )
                      LEFT JOIN cached_applications ca ON ( f.cid = ca.cid AND ca.id =  COALESCE(fa.application_id, aa.application_id ) ) 
                      LEFT JOIN cached_leases cl ON ( f.cid = cl.cid AND cl.id = COALESCE( fa.lease_id, ca.lease_id ) )
                      LEFT JOIN properties p ON ( f.cid = p.cid AND p.id = COALESCE( cl.property_id, ca.property_id ) )
                      LEFT JOIN unit_spaces us ON ( f.cid = us.cid AND us.id = COALESCE( cl.unit_space_id, ca.unit_space_id ) AND us.deleted_on IS NULL )
                      LEFT JOIN lease_status_types lst ON ( lst.id = COALESCE( cl.lease_status_type_id, ca.lease_status_type_id ) )
                  WHERE
                     f.cid = ' . ( int ) $intCid . '
                     AND f.file_type_id IS NOT NULL ';

		if( false == is_null( $arrmixFileSearch['from_date'] ) && false == is_null( $arrmixFileSearch['to_date'] ) ) {
			$strSql  .= ' AND ( f.created_on::date >= \'' . $arrmixFileSearch['from_date'] . '\'::date AND f.created_on::date <= \'' . $arrmixFileSearch['to_date'] . '\'::date )';
		}

		if( true == valArr( $arrmixFileSearch['properties'] ) ) {
			$strSql  .= ' AND COALESCE( cl.property_id, ca.property_id ) IN ( ' . implode( ',', $arrmixFileSearch['properties'] ) . ' ) ';
		}

		if( true == valArr( $arrmixFileSearch['lease_status_type_ids'] ) ) {
			$strSql  .= ' AND cl.lease_status_type_id IN ( ' . implode( ',', $arrmixFileSearch['lease_status_type_ids'] ) . ' ) ';
		}

		if( true == valStr( $arrmixFileSearch['unit_space_ids'] ) ) {
			$strSql  .= ' AND us.id IN ( ' . $arrmixFileSearch['unit_space_ids']  . ' ) ';
		}

		$strSql .= $strActiveApplicantsAndResidentsSql;

		if( true == valArr( $arrmixFileSearch['file_types'] ) ) {

			$arrintLeasePacketFileTypeIds = [
				$arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_LEASE_ADDENDUM],
				$arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_SIGNED],
				$arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_PRE_SIGNED],
				$arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_PARTIALLY_SIGNED],
				$arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_LEASE_DOCUMENT]
			];

			if( true == valArr( array_intersect( $arrintLeasePacketFileTypeIds, $arrmixFileSearch['file_types'] ) ) ) {
				$arrmixFileSearch['file_types'][] = $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_LEASE_PACKET];
			}

			if( true == in_array( $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE], $arrmixFileSearch['file_types'] ) ) {
				$arrmixFileSearch['file_types'][] = $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET];
			}

			if( true == in_array( $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_WORK_ORDER], $arrmixFileSearch['file_types'] ) ) {
				$arrmixFileSearch['file_types'][] = $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_WORK_ORDER];
				$arrmixFileSearch['file_types'][] = $arrintRequiredFileTypeIds[CFileType::SYSTEM_CODE_MAINTENANCE_ATTACHMENT];
			}

			$strSql .= ' AND CASE
             WHEN f.document_id IS NOT NULL AND d.file_type_id IS NOT NULL 
             THEN d.file_type_id IN ( ' . implode( ',', $arrmixFileSearch['file_types'] ) . ' )
             ELSE f.file_type_id IN ( ' . implode( ',', $arrmixFileSearch['file_types'] ) . ' )
           END ';
		}

		$strSql .= ')
            SELECT
               fr.cid,
               fr.file_id,
               fr.parent_file_id as parent_file_id,
               fr.file_name,
               fr.title,
               fr.file_path,
               fr.file_type_id,
               fr.name,
               fr.system_code,
               fr.file_type_name,
               fr.property_id,
               fr.property_name,
               fr.unit_number_cache,
               fr.building_name,
               fr.file_metadata_lease_status_type_name,
               fr.name_resident,
               
               fr.lease_start_date,
               fr.primary_phone_number,
               fr.email_address,
               fr.show_in_portal
            FROM
               files_records fr
            WHERE
               fr.system_code <> \'' . CFileType::SYSTEM_CODE_LEASE_ESA . '\' OR fr.system_code IS NULL ';

		if( true == valArr( $arrmixFileSearch['exclude_system_code'] ) ) {
			$strSql .= ' AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrmixFileSearch['exclude_system_code'] ) . '\' ) OR ft.system_code IS NULL )';
		}

		$strSql .= '
            GROUP BY
               fr.cid,
               fr.file_id,
               fr.parent_file_id,
               fr.file_name,
               fr.title,
               fr.file_path,
               fr.file_type_id,
               fr.name,
               fr.system_code,
               fr.file_type_name,
               fr.property_id,
               fr.property_name,
               fr.unit_number_cache,
               fr.building_name,
               fr.file_metadata_lease_status_type_name,
               fr.name_resident,
			   fr.lease_start_date,
               fr.primary_phone_number,
               fr.show_in_portal,
               fr.email_address
             ORDER BY fr.file_id, fr.parent_file_id';

		if( true == valId( $arrmixFileSearch['LIMIT'] ) && ( 0 == $arrmixFileSearch['OFFSET'] || true == valId( $arrmixFileSearch['OFFSET'] ) ) ) {
			$strSql .= ' LIMIT ' . $arrmixFileSearch['LIMIT'] . ' OFFSET ' . $arrmixFileSearch['OFFSET'];
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsFileCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						sc.id,
						COUNT(fa.file_id) As file_count
					FROM
						subsidy_certifications sc
						JOIN file_associations fa ON (sc.cid = fa.cid AND sc.id = fa.subsidy_certification_id)
						JOIN files fi ON( sc.cid = fa.cid AND sc.property_id = fi.property_id AND fa.document_id = fi.document_id AND fa.file_id = fi.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND fi.file_name NOT LIKE \'TRIAL_%\'
						AND sc.deleted_by IS NULL
						AND fa.deleted_by IS NULL
					GROUP BY
						sc.id
					ORDER BY
						sc.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFilesByGlHeaderIdsByCid( $arrintGlHeaderIds, $intCid, $objDatabase, $intFileTypeId = NULL, $strOrderBy = 'f.id DESC' ) {

		if( false == valArr( $arrintGlHeaderIds ) ) {
			return NULL;
		}
		$strWhereCondition = ( false == is_null( $intFileTypeId ) ) ? ' AND f.file_type_id = ' . ( int ) $intFileTypeId : '';

		if( 'f.title' == $strOrderBy ) {
			$strOrderBy = $objDatabase->getCollateSort( $strOrderBy, true );
		}

		$strSql = 'SELECT
						f.*,
						fa.gl_header_id AS file_association_id,
						ft.system_code as file_type_system_code
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON ( ft.cid = f.cid AND f.file_type_id = ft.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.gl_header_id IN ( ' . implode( ',', $arrintGlHeaderIds ) . ' )
						AND fa.deleted_by IS NULL
						' . $strWhereCondition . '
					ORDER BY ' . $strOrderBy;
		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchApPaymentFilesByApHeaderIdsByFileTypeSystemCodesByCid( $arrintApHeaderIds, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) || false == valArr( $arrstrSystemCodes ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code,
						ap.id AS ap_payment_id,
						ah_invoices.id AS ap_header_id,
						app.id AS ap_payee_id,
						app.company_name
					FROM
						files f
						JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id )
						JOIN file_types ft ON ( f.cid = ft.cid AND f.file_type_id = ft.id )
						JOIN ap_payments ap ON ( ap.cid = f.cid AND ap.id = fa.ap_payment_id )
						JOIN ap_headers ah_payments ON ( ah_payments.cid = ap.cid AND ah_payments.ap_payment_id = ap.id )
						JOIN ap_headers ah_invoices ON ( ah_invoices.cid = ah_payments.cid AND ah_invoices.reversal_ap_header_id IS NULL AND
						( LOWER ( TRIM ( ah_payments.header_number ) ) LIKE  TEXT( ah_invoices.id )
							OR LOWER ( TRIM ( ah_payments.header_number ) ) LIKE CONCAT( \'%,\', ah_invoices.id) 
							OR LOWER ( TRIM ( ah_payments.header_number ) ) LIKE CONCAT( ah_invoices.id, \',%\')
							OR LOWER ( TRIM ( ah_payments.header_number ) ) LIKE CONCAT( \'%,\', ah_invoices.id, \',%\')
						) )
						JOIN ap_payees app ON ( app.cid = ah_invoices.cid AND app.id = ah_invoices.ap_payee_id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND ft.system_code IN ( ' . sqlStrImplode( $arrstrSystemCodes ) . ' )
						AND fa.deleted_by IS NULL
						AND ah_invoices.id IN ( ' . sqlIntImplode( $arrintApHeaderIds ) . ' )
						AND ah_invoices.deleted_on IS NULL
						AND ap.payment_status_type_id <> ' . CPaymentStatusType::VOIDED . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'app.company_name' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInvoicesFilesByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApHeaderIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						f.*,
						ft.system_code as file_type_system_code,
						ah.id AS ap_header_id,
						ap.id AS ap_payee_id,
						ap.company_name,
						1 AS is_invoice_attachment
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.cid = f.cid AND f.file_type_id = ft.id)
						JOIN ap_headers ah ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id )
						JOIN ap_payees ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id IN ( ' . sqlIntImplode( $arrintApHeaderIds ) . ' )
						AND ah.deleted_on IS NULL
						AND fa.deleted_by IS NULL
					ORDER BY
						' . $objDatabase->getCollateSort( 'ap.company_name' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileByPropertyRateAssociationIdByPropertyIdByCid( $objPropertyAmenityReservation, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    f.*
					FROM
					    files f
					    LEFT JOIN property_amenity_availability_file_associations paaf ON ( paaf.file_id = f.id AND paaf.cid = f.cid )
					    LEFT JOIN property_amenity_availabilities paa ON ( paa.id = paaf.property_amenity_availability_id AND paa.cid = f.cid )
					WHERE
					    paa.cid = ' . ( int ) $intCid . '
					    AND paa.property_id = ' . ( int ) $objPropertyAmenityReservation->getPropertyId() . '
					    AND paa.rate_association_id = ' . ( int ) $objPropertyAmenityReservation->getRateAssociationId();

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchFilesByApplicationIdByApplicantIdsBySystemCodesByCid( $intApplicationId, $arrintApplicantIds, $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						f.*
					FROM
						files f
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND fa.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )';

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public function fetchTemporaryFilesByIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						f.*,
						ft.system_code AS file_type_system_code
					FROM
						files f
						JOIN file_types ft ON( f.cid = ft.cid AND f.file_type_id = ft.id )
					WHERE
						f.id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND f.cid = ' . ( int ) $intCid;

		return parent::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchSimpleFileDetailsByCustomerIdByFileIdByCid( $intCustomerId, $intFileId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
							f.id,
							f.file_name,
							f.file_path,
							ft.system_code as file_type_system_code
						FROM
							file_associations fa
							JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid )
							JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						WHERE
							fa.customer_id = ' . ( int ) $intCustomerId . '
							AND fa.cid = ' . ( int ) $intCid . '
							AND fa.file_id = ' . ( int ) $intFileId . '
							AND fa.deleted_by IS NULL
							LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSignatureFilesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
							f.*
						FROM
							files f
							JOIN file_types ft ON ( f.cid = ft.cid AND f.file_type_id = ft.id AND  ft.system_code = \'' . CFileType::SYSTEM_CODE_SIGNATURE . '\'  )
						WHERE
							f.cid = ' . ( int ) $intCid . '
							AND f.property_id = ' . ( int ) $intPropertyId . '
							AND f.title IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchFilesByCidByOrganizationIdByOrganizationContractId( $intCid, $intOrganizationId, $intOrgaizationContractId, $objDatabase, $intOffset = NULL, $intLimit = NULL, $boolShowInPortal = false ) {

		$strOffsetLimit = '';
		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strOffsetLimit = ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;
		}

		$strWhereShowInPortal = ( true == $boolShowInPortal ) ? ' AND f.show_in_portal = 1 ' : '';

		$strSql = ' SELECT
                        f.*,
                        ft.name as file_type_name
					FROM
                        files f
                        JOIN file_types ft ON ( f.cid = ft.cid AND f.file_type_id = ft.id )
                        JOIN file_associations fa ON ( fa.cid = ft.cid AND fa.file_id = f.id )
					WHERE
                        f.cid = ' . ( int ) $intCid . '
                        AND fa.organization_id = ' . ( int ) $intOrganizationId . '
                        AND fa.organization_contract_id = ' . ( int ) $intOrgaizationContractId . '
                        AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL' . $strWhereShowInPortal . '
						ORDER BY created_on DESC' . $strOffsetLimit;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchGroupDocumentsByIdByCid( $intOrganizationId, $intCid, $objDatabase, $intOffset = NULL, $intLimit = NULL, $boolShowInPortal = false ) {
		$strOffsetLimit = '';
		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strOffsetLimit = ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;
		}

		$strWhereShowInPortal = ( true == $boolShowInPortal ) ? ' AND f.show_in_portal = 1 ' : '';

		$strSql = 'SELECT
				    f.id,
				    f.cid,
				    fa.id as file_association_id,
				    f.title,
				    f.file_name,
				    f.file_path,
				    f.show_in_portal,
				    f.file_type_id,
				    ft.name as file_type_name,
				    f.created_on,
				    fa.organization_id,
				    f.details
				FROM
				    files f 
				    JOIN file_associations fa ON ( fa.cid = f.cid AND fa.file_id = f.id AND fa.organization_id = ' . ( int ) $intOrganizationId . ' AND fa.organization_contract_id IS NULL AND fa.deleted_by IS NULL AND fa.deleted_on IS NULL )
				    JOIN file_types ft ON (ft.cid = f.cid AND ft.id = f.file_type_id)
				WHERE
				    f.cid = ' . ( int ) $intCid . $strWhereShowInPortal . '
					ORDER BY created_on DESC' . $strOffsetLimit;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCustomFacilitiesFilesExcludeStorageVendor( $strStorageVendor, $objDatabase ) {
		$strSql = '
				SELECT
					fe.extension,
					f.*
				FROM files f
				JOIN file_associations fa ON f.cid = fa.cid AND f.id = fa.file_id
				JOIN file_extensions fe ON f.file_extension_id = fe.id
				WHERE
				NOT EXISTS (
					SELECT
						*
					FROM
						stored_objects so
					WHERE
						so.cid = f.cid
					AND f.id = so.reference_id
					AND so.vendor = \'' . $strStorageVendor . '\'
				)
				AND ( fa.maintenance_request_id IS NOT NULL OR fa.inspection_response_id IS NOT NULL );
			';

		return self::fetchFiles( $strSql, $objDatabase, true );
	}

	public static function fetchUnExportedFilesByApplicationIdByFileTypeSystemCodesByCid( $intApplicationId, $arrstrSystemCodes, $intCid, $objDatabase, $boolIsFromGeneratingRemainingFiles = false, $intFileExtensionId = NULL ) {

		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strFileExtensionsql = ( 0 < $intFileExtensionId ) ? ' AND f.file_extension_id = ' . ( int ) $intFileExtensionId : NULL;

		$strFileDocument = ( false == $boolIsFromGeneratingRemainingFiles ) ? 'fa.document_id AS document_id,' : '';

		$strSql = 'SELECT
						f.*,
						fa.applicant_id,'
		          . $strFileDocument .
		          'fa.application_id AS application_id,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
						AND fa.deleted_by IS NULL
						AND f.remote_primary_key IS NULL
						AND f.cid = ' . ( int ) $intCid . $strFileExtensionsql;

		if( true == $boolIsFromGeneratingRemainingFiles ) {
			$strSql .= ' AND f.file_upload_date IS NULL';
		}

		$strSql .= ' ORDER BY f.order_num';

		$strSql = ' SELECT
						DISTINCT ON (id) *
					FROM ( ' . $strSql . ') AS files
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchFiles( $strSql, $objDatabase );
	}

	public static function fetchCustomFileInfoByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						f.id,
						f.file_name,
						fa.id AS file_association_id,
						f.title
					FROM
						files f
						JOIN file_associations fa ON( f.cid = fa.cid AND f.id = fa.file_id )
					WHERE
						f.id = ' . ( int ) $intId . '
						AND f.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomFileByApplicationIdByFileTypeSystemCodesByDocumentSubTypeIdByCid( $intApplicationId, $arrstrSystemCodes, $intDocumentSubTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrSystemCodes ) ) {
			return NULL;
		}

		$strSql = ' SELECT
            count(fa.id),
            applicant_id
          FROM
            file_associations fa
            JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
            JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
            JOIN documents d ON ( f.document_id = d.id AND f.cid = d.cid )
          WHERE
            fa.application_id = ' . ( int ) $intApplicationId . '
            AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
            AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
            AND fa.cid =  ' . ( int ) $intCid . '
            AND fa.file_signed_on IS NOT NULL
            AND fa.deleted_by IS NULL
          GROUP BY applicant_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomFileDetailsByApplicationIdByApplicantIdsByFileTypeSystemCodeByCid( $intApplicationId, $arrintApplicantIds, $strFileTypeSystemCode, $intCid, $objDatabase ) {

		if( ( '' == $intApplicationId ) || ( false == valArr( $arrintApplicantIds ) ) || ( '' == $strFileTypeSystemCode ) ) {
			return;
		}

		$strSql = 'SELECT
            f.title,
            f.id as file_id,
            fa.applicant_id            
          FROM
            files f
            JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
            JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
          WHERE
            fa.application_id = ' . ( int ) $intApplicationId . '
            AND fa.applicant_id IN(  ' . sqlIntImplode( $arrintApplicantIds ) . ' )
            AND ft.system_code = \'' . ( string ) addslashes( $strFileTypeSystemCode ) . '\'
            AND fa.cid = ' . ( int ) $intCid . '
            AND fa.file_signed_on IS NOT NULL
            AND fa.deleted_by IS NULL
          ORDER BY
            f.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileByIdByFileTypeSystemCodeByCid( $intId, $strSystemCode, $intCid, $objDatabase ) {

		if( false == valStr( $strSystemCode ) || false == valId( $intId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						f.*,
						fa.id AS file_association_id,
						ft.system_code AS file_type_system_code
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						f.id = ' . $intId . '
						AND ft.system_code =\'' . $strSystemCode . '\' 
						AND fa.deleted_by IS NULL
						AND f.cid = ' . ( int ) $intCid;

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchResidentUploadedDocumentsByVerificationStatusByCid( $objDashboardFilter, $intCid, $strVerificationStatus, $objDatabase, $boolReturnCountOnly = false, $intOffset = NULL, $intPageLimit = NULL, $strSortBy = NULL, $strIsPassReport = false, $arrintPropertyIds = [] ) {

		$strOrderBy = '';
		$strOffset = '';
		$strLimit = '';
		$strGroupBy = '';
		$strSelect = 'DISTINCT ON (f.id) f.id,
						cl.property_name,
						cl.unit_number_cache,
						cl.id as lease_id,';
		if( true == $strIsPassReport ) {
			$strSelect = 'COUNT( DISTINCT f.id ) AS approvals_documents, f.property_id,';
			$strGroupBy = 'GROUP BY 
								c.name_full,
								ft.name,
								f.created_on,
								f.property_id,
								cl.primary_customer_id,
								ca.id,
								priority';
		}
		if( true == $boolReturnCountOnly ) {
			$strSql = 'SELECT count(1)
						FROM
							files f
							JOIN file_associations fa  ON f.cid = fa.cid and f.id = fa.file_id AND fa.customer_id IS NOT NULL
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, TRUE ) lp ON ( lp.property_id = f.property_id )
						WHERE
							f.cid = ' . ( int ) $intCid . '
							AND f.details->\'file_verification\'->\'current_status\' IS NOT NULL
							AND f.details->\'file_verification\'->>\'current_status\' = \'' . $strVerificationStatus . '\'
							AND fa.deleted_by IS NULL';
		} else {
			$strCondition = '';

			if( true == valArr( $arrintPropertyIds ) ) {

				$strCondition = ' AND cl.property_id IN ( '. implode( ',', $arrintPropertyIds ) .' ) ';

			}
			$strSql = 'SELECT
						' . $strSelect . '
						c.name_full as applicant_name,
						ft.name as file_type_name,
						f.created_on,
						cl.primary_customer_id as customer_id,
						ca.id as application_id,
						CASE
							WHEN TRIM(dp.approvals_document_verification->>\'urgent_document_uploaded_days\') != \'\' AND CURRENT_DATE >= ( f.created_on::date + ( dp.approvals_document_verification->>\'urgent_document_uploaded_days\' )::int ) THEN
								3
							WHEN TRIM(dp.approvals_document_verification->>\'urgent_document_type_ids\') != \'\' AND f.file_type_id = ANY ( ( dp.approvals_document_verification->>\'urgent_document_type_ids\' )::integer[] ) THEN
								3
							WHEN TRIM(dp.approvals_document_verification->>\'important_document_uploaded_days\') != \'\' AND CURRENT_DATE >= ( f.created_on::date + ( dp.approvals_document_verification->>\'important_document_uploaded_days\' )::int ) THEN
								2
							WHEN TRIM(dp.approvals_document_verification->>\'important_document_type_ids\') != \'\' AND f.file_type_id = ANY ( ( dp.approvals_document_verification->>\'important_document_type_ids\' )::integer[] ) THEN
								2
							ELSE
								1
						END as priority
					FROM
						files f
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, TRUE ) lp ON ( lp.property_id = f.property_id )
						JOIN file_types ft ON ( f.cid = ft.cid and f.file_type_id = ft.id )
						JOIN file_associations fa  ON ( f.cid = fa.cid and f.id = fa.file_id AND fa.customer_id IS NOT NULL )
						JOIN customers c ON ( fa.cid = c.cid AND fa.customer_id = c.id )
						JOIN lease_customers lc ON ( lc.cid = c.cid AND lc.customer_id = c.id )
						JOIN cached_leases cl ON ( lc.cid = cl.cid AND lc.lease_id = cl.id )
						LEFT JOIN cached_applications ca ON ( ca.cid = cl.cid AND ca.lease_id = cl.id AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND fa.lease_id IS NULL )
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = f.cid )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.details->\'file_verification\'->\'current_status\' IS NOT NULL
						AND f.details->\'file_verification\'->>\'current_status\' = \'' . $strVerificationStatus . '\'
						AND fa.deleted_by IS NULL 
						' . $strCondition . $strGroupBy . '';
		}

		if( false == $strIsPassReport ) {
			$strOrderBy = ' ORDER BY ';
		}
		if( $strSortBy != NULL ) {

			if( 'c.name_full' == $strSortBy || 'cl.property_name' == $strSortBy ) {
				$strOrderBy .= $objDatabase->getCollateSort( $strSortBy );
			} else if( 'ft.name' == $strSortBy ) {
				$strOrderBy .= $objDatabase->getCollateSort( $strSortBy, true );
			} else {
				$strOrderBy .= $strSortBy;
			}
		}
		if( $intOffset != NULL ) {
			$strOffset = ' OFFSET ' . ( int ) $intOffset;
		}
		if( $intPageLimit != NULL ) {
			$strLimit = ' LIMIT ' . ( int ) $intPageLimit;
		}
		$strSql .= $strOffset . $strLimit;

		if( false == $boolReturnCountOnly ) {
			$strSql = 'SELECT * FROM ( ' . $strSql . ') as sub ' . $strOrderBy;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchVerificationFileByIdByCid( $intFileId, $intCid, $objDatabase ) {

		if( false == valId( $intFileId ) ) return NULL;

		$strSql = 'SELECT
				    f.*, 
				    c.id as customer_id, 
				    cl.id as lease_id,
				    c.name_full as customer_name,
				    ca.lease_end_date,
				    ca.lease_interval_type_id,
				    ft.system_code as file_type_system_code,
				    ca.id as application_id
				FROM
				    files f
				    JOIN file_types ft ON (f.cid = ft.cid AND f.file_type_id = ft.id )
				    JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
				    JOIN customers c ON ( fa.cid = c.cid AND fa.customer_id = c.id )
				    LEFT JOIN lease_customers lc ON ( c.cid = lc.cid AND c.id = lc.customer_id )
				    LEFT JOIN cached_leases cl ON ( lc.cid = cl.cid AND cl.id = COALESCE(fa.lease_id, lc.lease_id ) )
				    LEFT JOIN cached_applications ca ON ( ( ca.id = fa.application_id OR ca.lease_id = cl.id ) AND ca.cid = fa.cid AND ca.lease_status_type_id NOT IN ( 6,2 ) )
				WHERE
				    f.id = ' . ( int ) $intFileId . '
				    AND fa.cid = ' . ( int ) $intCid . '
				    AND fa.deleted_by IS NULL
				ORDER BY ca.lease_interval_type_id desc
				LIMIT 1';

		return parent::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchLatestFilesBySystemCodeByApplicationIdByApplicantIdByCid( $strSystemCode, $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						f.*
					FROM
						file_associations fa
						JOIN files f ON (f.id = fa.file_id AND f.cid = fa.cid )
						JOIN file_types ft ON (ft.id = f.file_type_id AND ft.cid = f.cid )
					WHERE
						fa.application_id = ' . ( int ) $intApplicationId . '
						AND fa.applicant_id = ' . ( int ) $intApplicantId . '
						AND ft.system_code LIKE \'' . $strSystemCode . '\'
						AND fa.cid = ' . ( int ) $intCid . '
						ORDER BY f.id desc
						LIMIT 1';

		return self::fetchFile( $strSql, $objDatabase );
	}

	public static function fetchCustomFileDetailsByIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == valId( $intId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (f.id) f.*
						, ft.system_code AS file_type_system_code
						, CASE
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\', \'' . CFileType::SYSTEM_CODE_PRE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_PARTIALLY_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_SIGNED . '\', \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' ) THEN
								CASE
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::APPLICATION . '\' THEN \'' . __( 'e-Sign: New Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::RENEWAL . '\'  THEN \'' . __( 'e-Sign: Renewal Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::TRANSFER . '\' THEN \'' . __( 'e-Sign: Transfer Lease' ) . '\'
									WHEN ca.lease_interval_type_id = \'' . CLeaseIntervalType::LEASE_MODIFICATION . '\' THEN \'' . __( 'e-Sign: Lease Modifications' ) . '\'
									ELSE \'' . __( 'e-Sign: Replace Lease' ) . '\'
								END
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_POLICY . '\' THEN
									\'' . __( 'Application: Policy' ) . '\'
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE . '\', \'' . CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET . '\' ) THEN
									\'' . __( 'e-Sign: Addenda' ) . '\'
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_OFFER_TO_RENT . '\', \'' . CFileType::SYSTEM_CODE_GUEST_CARD . '\' ) THEN
									\'' . __( 'Automated' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_APPLICATION . '\' THEN
									\'' . __( 'Application Upload' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT . '\' THEN
									\'' . __( 'Renewal Offer Sent' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED . '\' THEN
									\'' . __( 'Renewal Offer Accepted' ) . '\'
							WHEN ft.system_code = \'' . CFileType::SYSTEM_CODE_UNIT_TRANSFER . '\' THEN
									\'' . __( 'Transfer Document' ) . '\'
							WHEN ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_WAITLIST . '\' ) OR ft.system_code = ANY( ARRAY[ \'' . implode( "', '", CFileType::$c_arrstrAffordableFileTypes ) . '\' ] ) THEN
									\'\'
							ELSE \'' . __( 'Upload' ) . '\'
						END AS file_type_name
						, fa.application_id AS application_id
						, fa.applicant_id AS applicant_id
						, fa.require_sign AS require_sign
						, fa.customer_id AS customer_id
						, fa.file_signed_on AS file_signed_on
						, fa.id AS file_association_id
						, COALESCE( d.details->>\'marketing_name\', f.title ) as marketing_name
					FROM
						files f
						JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid)
						JOIN lease_customers lc ON ( fa.lease_id = lc.lease_id AND fa.cid = lc.cid )
						LEFT JOIN cached_applications ca ON ( ca.cid = fa.cid AND fa.application_id = ca.id )
						LEFT JOIN documents d ON ( d.cid = f.cid AND d.id = f.document_id )
					WHERE
						f.id = ' . ( int ) $intId . '
						AND fa.cid = ' . ( int ) $intCid . '
						AND f.show_in_portal = 1';

		return self::fetchFiles( $strSql, $objDatabase );
	}

}
?>
