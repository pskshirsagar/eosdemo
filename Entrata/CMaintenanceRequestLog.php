<?php

class CMaintenanceRequestLog extends CBaseMaintenanceRequestLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorMaintenanceRequestLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectivePeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldMaintenanceStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldMaintenanceStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldMaintenancePriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenancePriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewIsDeleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldIsDeleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostMonthIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>