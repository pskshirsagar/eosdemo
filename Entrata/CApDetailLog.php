<?php

class CApDetailLog extends CBaseApDetailLog {

	protected $m_strPropertyName;
	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;
	protected $m_strBankAccountName;
	protected $m_strUnitNumber;
	protected $m_strItemName;
	protected $m_strReturnDate;
	protected $m_strReturnedQuantity;
	protected $m_strReturnedUnitCost;
	protected $m_strReturnPostMonth;
	protected $m_strUnitOfMeasureName;
	protected $m_strGlDimensionName;
	protected $m_strPropertyBuildingName;
	protected $m_strApContractName;

	protected $m_fltQuantityReceived;

	public function mapApDetailLog( $objApDetail ) {

		$this->setCid( $objApDetail->getCid() );
		$this->setApHeaderId( $objApDetail->getApHeaderId() );
		$this->setApDetailId( $objApDetail->getId() );
		$this->setApPhysicalStatusTypeId( $objApDetail->getApPhysicalStatusTypeId() );
		$this->setGlTransactionTypeId( $objApDetail->getGlTransactionTypeId() );
// 		$this->setChargeArTransactionId( $objApDetail->getChargeArTransactionId() );
		$this->setAssetTransactionId( $objApDetail->getAssetTransactionId() );
		$this->setBankAccountId( $objApDetail->getBankAccountId() );
		$this->setPropertyId( $objApDetail->getPropertyId() );
		$this->setInterCoPropertyId( $objApDetail->getInterCoPropertyId() );
		$this->setPropertyUnitId( $objApDetail->getPropertyUnitId() );
		$this->setPropertyBuildingId( $objApDetail->getPropertyBuildingId() );
		$this->setApFormulaId( $objApDetail->getApFormulaId() );
		$this->setCompanyDepartmentId( $objApDetail->getCompanyDepartmentId() );
		$this->setGlDimensionId( $objApDetail->getGlDimensionId() );
		$this->setApContractId( $objApDetail->getApContractId() );
		// $this->setJobPhaseApDetailId( $objApDetail->getJobPhaseApDetailId() );
		// $this->setContractApDetailId( $objApDetail->getContractApDetailId() );
		$this->setPoApDetailId( $objApDetail->getPoApDetailId() );
		$this->setReversalApDetailId( $objApDetail->getReversalApDetailId() );
		$this->setScheduledApDetailId( $objApDetail->getScheduledApDetailId() );
		$this->setTemplateApDetailId( $objApDetail->getTemplateApDetailId() );
		$this->setReimbursementApDetailId( $objApDetail->getReimbursementApDetailId() );
//		$this->setReimbursedApAllocationId( $objApDetail->getReimbursedApAllocationId() );
// 		$this->setReimbursementArTransactionId( $objApDetail->getReimbursementArTransactionId() );
// 		$this->setInterCompanyTypeId( $objApDetail->getInterCompanyTypeId() );
// 		$this->setInterCompanyStageId( $objApDetail->getInterCompanyStageId() );
		$this->setUnitOfMeasureId( $objApDetail->getUnitOfMeasureId() );
		$this->setApCatalogItemId( $objApDetail->getApCatalogItemId() );
		$this->setApCodeId( $objApDetail->getApCodeId() );
		$this->setApPayeeSubAccountId( $objApDetail->getApPayeeSubAccountId() );
		$this->setJobApCodeId( $objApDetail->getJobApCodeId() );
		$this->setGlAccountId( $objApDetail->getGlAccountId() );
		$this->setApGlAccountId( $objApDetail->getApGlAccountId() );
		$this->setWipGlAccountId( $objApDetail->getWipGlAccountId() );
		$this->setPurchasesClearingGlAccountId( $objApDetail->getPurchasesClearingGlAccountId() );
		$this->setPendingReimbursementsGlAccountId( $objApDetail->getPendingReimbursementsGlAccountId() );
		$this->setInterCoApGlAccountId( $objApDetail->getInterCoApGlAccountId() );
		$this->setBankGlAccountId( $objApDetail->getBankGlAccountId() );
		$this->setAccrualDebitGlAccountId( $objApDetail->getAccrualDebitGlAccountId() );
		$this->setAccrualCreditGlAccountId( $objApDetail->getAccrualCreditGlAccountId() );
		$this->setCashDebitGlAccountId( $objApDetail->getCashDebitGlAccountId() );
		$this->setCashCreditGlAccountId( $objApDetail->getCashCreditGlAccountId() );
// 		$this->setReimbursedDebitGlAccountId( $objApDetail->getReimbursedDebitGlAccountId() );
// 		$this->setReimbursedCreditGlAccountId( $objApDetail->getReimbursedCreditGlAccountId() );
		$this->setInterCoArTransactionId( $objApDetail->getInterCoArTransactionId() );
		$this->setRefundArTransactionId( $objApDetail->getRefundArTransactionId() );
		$this->setApExportBatchId( $objApDetail->getApExportBatchId() );
		$this->setFeeId( $objApDetail->getFeeId() );
// 		$this->setPeriodId( $objApDetail->getPeriodId() );
		$this->setPostMonth( $objApDetail->getPostMonth() );
		$this->setRemotePrimaryKey( $objApDetail->getRemotePrimaryKey() );
		$this->setTransactionDatetime( $objApDetail->getTransactionDatetime() );
		$this->setQuantityOrdered( $objApDetail->getQuantityOrdered() );
		$this->setRate( $objApDetail->getRate() );
		$this->setSubtotalAmount( $objApDetail->getSubtotalAmount() );
		$this->setTaxAmount( $objApDetail->getTaxAmount() );
		$this->setDiscountAmount( $objApDetail->getDiscountAmount() );
		$this->setShippingAmount( $objApDetail->getShippingAmount() );
		$this->setPreApprovalAmount( $objApDetail->getPreApprovalAmount() );
		$this->setTransactionAmount( $objApDetail->getTransactionAmount() );
		$this->setTransactionAmountDue( $objApDetail->getTransactionAmountDue() );
		$this->setInitialAmountDue( $objApDetail->getInitialAmountDue() );
// 		$this->setReimbursedAmount( $objApDetail->getReimbursedAmount() );
		$this->setDrawnAmount( $objApDetail->getDrawnAmount() );
		$this->setDescription( $objApDetail->getDescription() );
		$this->setIsConfidential( $objApDetail->getIsConfidential() );
		$this->setIs1099( $objApDetail->getIs1099() );
		$this->setIsApprovedForPayment( $objApDetail->getIsApprovedForPayment() );
		$this->setIsDeleted( $objApDetail->getIsDeleted() );
		$this->setIsReversed( $objApDetail->getIsReversed() );
		$this->setPaymentApprovedBy( $objApDetail->getPaymentApprovedBy() );
		$this->setPaymentApprovedOn( $objApDetail->getPaymentApprovedOn() );
		$this->setExportedBy( $objApDetail->getExportedBy() );
		$this->setExportedOn( $objApDetail->getExportedOn() );
		$this->setApprovedBy( $objApDetail->getApprovedBy() );
		$this->setApprovedOn( $objApDetail->getApprovedOn() );
		$this->setDeletedBy( $objApDetail->getDeletedBy() );
		$this->setDeletedOn( $objApDetail->getDeletedOn() );
		$this->setApTransactionTypeId( $objApDetail->getApTransactionTypeId() );
		$this->setReimbursementMethodId( $objApDetail->getReimbursementMethodId() );
		$this->setRetentionAmount( $objApDetail->getRetentionAmount() );
	}

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getGlDimensionName() {
		return $this->m_strGlDimensionName;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getItemName() {
		return $this->m_strItemName;
	}

	public function getReturnDate() {
		return $this->m_strReturnDate;
	}

	public function getReturnedQuantity() {
		return $this->m_strReturnedQuantity;
	}

	public function getReturnedUnitCost() {
		return $this->m_strReturnedUnitCost;
	}

	public function getReturnPostMonth() {
		return $this->m_strReturnPostMonth;
	}

	public function getUnitOfMeasureName() {
		return $this->m_strUnitOfMeasureName;
	}

	public function getPropertyBuildingName() {
		return $this->m_strPropertyBuildingName;
	}

	public function getApContractName() {
		return $this->m_strApContractName;
	}

	public function getQuantityReceived() {
		return $this->m_fltQuantityReceived;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setGlDimensionName( $strGlDimensionName ) {
		$this->m_strGlDimensionName = $strGlDimensionName;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setPropertyBuildingName( $strPropertyBuildingName ) {
		$this->m_strPropertyBuildingName = $strPropertyBuildingName;
	}

	public function setApContractName( $strApContractName ) {
		$this->m_strApContractName = $strApContractName;
	}

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = $strBankAccountName;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setItemName( $strItemName ) {
		$this->m_strItemName = $strItemName;
	}

	public function setReturnDate( $strReturnDate ) {
		$this->m_strReturnDate = $strReturnDate;
	}

	public function setReturnPostMonth( $strReturnPostMonth ) {
		$this->m_strReturnPostMonth = $strReturnPostMonth;
	}

	public function setReturnedQuantity( $strReturnedQuantity ) {
		$this->m_strReturnedQuantity = $strReturnedQuantity;
	}

	public function setReturnedUnitCost( $strReturnedUnitCost ) {
		$this->m_strReturnedUnitCost = $strReturnedUnitCost;
	}

	public function setUnitOfMeasureName( $strUnitOfMeasureName ) {
		$this->m_strUnitOfMeasureName = $strUnitOfMeasureName;
	}

	public function setQuantityReceived( $fltQuantityReceived ) {
		$this->m_fltQuantityReceived = CStrings::strToFloatDef( $fltQuantityReceived, NULL, false, 6 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['gl_dimension_name'] ) ) $this->setGlDimensionName( $arrmixValues['gl_dimension_name'] );
		if( true == isset( $arrmixValues['gl_account_name'] ) ) $this->setGlAccountName( $arrmixValues['gl_account_name'] );
		if( true == isset( $arrmixValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrmixValues['gl_account_number'] );
		if( true == isset( $arrmixValues['bank_account_name'] ) ) $this->setBankAccountName( $arrmixValues['bank_account_name'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['item_name'] ) ) $this->setItemName( $arrmixValues['item_name'] );
		if( true == isset( $arrmixValues['return_date'] ) ) $this->setReturnDate( $arrmixValues['return_date'] );
		if( true == isset( $arrmixValues['return_post_month'] ) ) $this->setReturnPostMonth( $arrmixValues['return_post_month'] );
		if( true == isset( $arrmixValues['returned_quantity'] ) ) $this->setReturnedQuantity( $arrmixValues['returned_quantity'] );
		if( true == isset( $arrmixValues['quantity_received'] ) ) $this->setQuantityReceived( $arrmixValues['quantity_received'] );
		if( true == isset( $arrmixValues['returned_unit_cost'] ) ) $this->setReturnedUnitCost( $arrmixValues['returned_unit_cost'] );
		if( true == isset( $arrmixValues['unit_of_measure_name'] ) ) $this->setUnitOfMeasureName( $arrmixValues['unit_of_measure_name'] );
		if( true == isset( $arrmixValues['property_building_name'] ) ) $this->setPropertyBuildingName( $arrmixValues['property_building_name'] );
		if( true == isset( $arrmixValues['ap_contract_name'] ) ) $this->setApContractName( $arrmixValues['ap_contract_name'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				// Do not validate
				break;
		}

		return $boolIsValid;
	}

}
?>