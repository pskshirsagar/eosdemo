<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueTierTypes
 * Do not add any new functions to this class.
 */

class CRevenueTierTypes extends CBaseRevenueTierTypes {

	public static function fetchRevenueTierTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRevenueTierType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRevenueTierType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRevenueTierType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>