<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportTypes
 * Do not add any new functions to this class.
 */

class CAccountingExportTypes extends CBaseAccountingExportTypes {

	public static function fetchAccountingExportTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CAccountingExportType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAccountingExportType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CAccountingExportType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>