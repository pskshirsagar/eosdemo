<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExpenseLogs
 * Do not add any new functions to this class.
 */

class CCustomerExpenseLogs extends CBaseCustomerExpenseLogs {

	public static function fetchRecentCustomerExpenseLogByCustomerIdByIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cel.*
					FROM
						customer_expense_logs cel
					WHERE
						cel.cid = ' . ( int ) $intCid . '
						AND cel.customer_id = ' . ( int ) $intCustomerId . '
						AND cel.customer_expense_id = ' . ( int ) $intId . '
						AND cel.is_post_date_ignored = 0
					ORDER BY cel.log_datetime DESC
					LIMIT 1';

		return self::fetchCustomerExpenseLog( $strSql, $objDatabase );
	}

}
?>