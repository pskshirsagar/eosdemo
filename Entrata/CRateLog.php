<?php

class CRateLog extends CBaseRateLog {

	protected $m_intLeaseId;
	protected $m_intArOriginObjectId;
	protected $m_intArCodeTypeId;
	protected $m_intArTriggerTypeId;
	protected $m_intLeaseExpiringCount;
	protected $m_intSpecialId;
	protected $m_intOfferId;
	protected $m_intSpecialGroupTypeId;
	protected $m_intSpecialQuantityRemaining;
	protected $m_intSpecialMaxDaysToLeaseStart;
	protected $m_intSpecialMinDaysToLeaseStart;
	protected $m_intLeaseAssociationId;
	protected $m_intArCodeGroupId;
	protected $m_intMultiplierTotal;
	protected $m_intLeaseIntervalId;
	protected $m_intBaseRentRenewalOptionId;
	protected $m_intArLookUpTableId;

	protected $m_strSpecialStartDate;
	protected $m_strSpecialEndDate;
	protected $m_strMemo;
	protected $m_strArCodeName;
	protected $m_strSpecialName;
	protected $m_strLeaseTermName;
	protected $m_strLeaseTermExpiryDate;
	protected $m_strAddOnTypeName;
	protected $m_strArTriggerName;
	protected $m_strMoveInDate;

	protected $m_boolExceedsExpirationLimit;
	protected $m_boolIsAutoLeaseApprove;
	protected $m_boolIsStudentProperty;
	protected $m_boolIsCloned;

	protected $m_fltTaxPercent;
	protected $m_fltTaxAmount;

	/**
	 * Get Funtions
	 */

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getArOriginObjectId() {
		return $this->m_intArOriginObjectId;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getArCodeGroupId() {
		return $this->m_intArCodeGroupId;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getSpecialName() {
		return $this->m_strSpecialName;
	}

	public function getSpecialStartDate() {
		return $this->m_strSpecialStartDate;
	}

	public function getSpecialEndDate() {
		return $this->m_strSpecialEndDate;
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function getSpecialQuantityRemaining() {
		return $this->m_intSpecialQuantityRemaining;
	}

	public function getSpecialMaxDaysToLeaseStart() {
		return $this->m_intSpecialMaxDaysToLeaseStart;
	}

	public function getSpecialMinDaysToLeaseStart() {
		return $this->m_intSpecialMinDaysToLeaseStart;
	}

	public function getLeaseAssociationId() {
		return $this->m_intLeaseAssociationId;
	}

	public function getLeaseExpiringCount() {
		return $this->m_intLeaseExpiringCount;
	}

	public function getSpecialGroupTypeId() {
		return $this->m_intSpecialGroupTypeId;
	}

	public function getExceedsExpirationLimit() {
		return $this->m_boolExceedsExpirationLimit;
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function getOfferId() {
		return $this->m_intOfferId;
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getTaxAmount() {
		return ( float ) $this->m_fltTaxAmount;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getBaseRentRenewalOptionId() {
		return $this->m_intBaseRentRenewalOptionId;
	}

	/**
	 * @return string
	 */
	public function getLeaseTermExpiryDate() {
		return $this->m_strLeaseTermExpiryDate;
	}

	public function getAddOnTypeName() {
		return $this->m_strAddOnTypeName;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getIsAutoLeaseApprove() {
		return $this->m_boolIsAutoLeaseApprove;
	}

	public function getIsStudentProperty() {
		return $this->m_boolIsStudentProperty;
	}

	public function getMultiplierTotal() {
		$this->m_intMultiplierTotal;
	}

	public function getArLookUpTableId() {
		return $this->m_intArLookUpTableId;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getIsCloned() {
		return $this->m_boolIsCloned;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_origin_object_id'] ) && true == $boolDirectSet )
			$this->m_intArOriginObjectId = trim( $arrmixValues['ar_origin_object_id'] );
		elseif( true == isset( $arrmixValues['ar_origin_object_id'] ) )
			$this->setArOriginObjectId( $arrmixValues['ar_origin_object_id'] );

		if( true == isset( $arrmixValues['memo'] ) ) $this->setMemo( $arrmixValues['memo'] );
		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['ar_code_group_id'] ) ) $this->setArCodeGroupId( $arrmixValues['ar_code_group_id'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrmixValues['ar_trigger_type_id'] );
		if( true == isset( $arrmixValues['special_name'] ) ) $this->setSpecialName( $arrmixValues['special_name'] );
		if( true == isset( $arrmixValues['special_start_date'] ) ) $this->setSpecialStartDate( $arrmixValues['special_start_date'] );
		if( true == isset( $arrmixValues['special_end_date'] ) ) $this->setSpecialEndDate( $arrmixValues['special_end_date'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['special_quantity_remaining'] ) ) $this->setSpecialQuantityRemaining( $arrmixValues['special_quantity_remaining'] );
		if( true == isset( $arrmixValues['special_max_days_to_lease_start'] ) ) $this->setSpecialMaxDaysToLeaseStart( $arrmixValues['special_max_days_to_lease_start'] );

		if( true == isset( $arrmixValues['special_min_days_to_lease_start'] ) ) $this->setSpecialMinDaysToLeaseStart( $arrmixValues['special_min_days_to_lease_start'] );
		if( true == isset( $arrmixValues['lease_association_id'] ) ) $this->setLeaseAssociationId( $arrmixValues['lease_association_id'] );
		if( true == isset( $arrmixValues['special_start_date'] ) ) $this->setSpecialStartDate( $arrmixValues['special_start_date'] );
		if( true == isset( $arrmixValues['special_end_date'] ) ) $this->setSpecialEndDate( $arrmixValues['special_end_date'] );

		if( true == isset( $arrmixValues['lease_expiring_count'] ) ) $this->setLeaseExpiringCount( $arrmixValues['lease_expiring_count'] );
		if( true == isset( $arrmixValues['exceeds_expiration_limit'] ) ) $this->setExceedsExpirationLimit( $arrmixValues['exceeds_expiration_limit'] );
		if( true == isset( $arrmixValues['special_group_type_id'] ) ) $this->setSpecialGroupTypeId( $arrmixValues['special_group_type_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['special_id'] ) ) $this->setSpecialId( $arrmixValues['special_id'] );
		if( true == isset( $arrmixValues['lease_term_expiry_date'] ) )  $this->setLeaseTermExpiryDate( $arrmixValues['lease_term_expiry_date'] );
		if( true == isset( $arrmixValues['add_on_type_name'] ) )  $this->setAddOnTypeName( $arrmixValues['add_on_type_name'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) )  $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
		if( true == isset( $arrmixValues['offer_id'] ) ) $this->setOfferId( $arrmixValues['offer_id'] );
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );
		if( true == isset( $arrmixValues['is_auto_lease_approve'] ) ) $this->setIsAutoLeaseApprove( $arrmixValues['is_auto_lease_approve'] );
		if( true == isset( $arrmixValues['tax_amount'] ) ) $this->setTaxAmount( $arrmixValues['tax_amount'] );
		if( true == isset( $arrmixValues['is_student_property'] ) ) $this->setIsStudentProperty( $arrmixValues['is_student_property'] );
		if( true == isset( $arrmixValues['multiplier_total'] ) ) $this->setMultiplierTotal( $arrmixValues['multiplier_total'] );
		if( true == isset( $arrmixValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		if( true == isset( $arrmixValues['base_rent_renewal_option_id'] ) ) $this->setBaseRentRenewalOptionId( $arrmixValues['base_rent_renewal_option_id'] );
		if( true == isset( $arrmixValues['ar_lookup_table_id'] ) ) $this->setArLookUpTableId( $arrmixValues['ar_lookup_table_id'] );
		if( true == isset( $arrmixValues['ar_lookup_table_id'] ) ) $this->setMoveInDate( $arrmixValues['ar_lookup_table_id'] );
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setArOriginObjectId( $intArOriginObjectId ) {
		$this->m_intArOriginObjectId = $intArOriginObjectId;
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = $intArCodeTypeId;
	}

	public function setArCodeGroupId( $intArCodeGroupId ) {
		$this->m_intArCodeGroupId = $intArCodeGroupId;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->m_intArTriggerTypeId = ( int ) $intArTriggerTypeId;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setSpecialName( $strSpecialName ) {
		$this->m_strSpecialName = $strSpecialName;
	}

	public function setMemo( $strMemo ) {
		$this->m_strMemo = $strMemo;
	}

	public function setSpecialQuantityRemaining( $intSpecialQuantityRemaining ) {
		$this->m_intSpecialQuantityRemaining = $intSpecialQuantityRemaining;
	}

	public function setSpecialMaxDaysToLeaseStart( $intSpecialMaxDaysToLeaseStart ) {
		$this->m_intSpecialMaxDaysToLeaseStart = $intSpecialMaxDaysToLeaseStart;
	}

	public function setSpecialMinDaysToLeaseStart( $intSpecialMinDaysToLeaseStart ) {
		$this->m_intSpecialMinDaysToLeaseStart = $intSpecialMinDaysToLeaseStart;
	}

	public function setLeaseAssociationId( $intLeaseAssociationId ) {
		$this->m_intLeaseAssociationId = $intLeaseAssociationId;
	}

	public function setLeaseExpiringCount( $intLeaseExpiringCount ) {
		$this->m_intLeaseExpiringCount = $intLeaseExpiringCount;
	}

	public function setExceedsExpirationLimit( $boolExceedsExpirationLimit ) {
		$this->m_boolExceedsExpirationLimit = $boolExceedsExpirationLimit;
	}

	public function setSpecialGroupTypeId( $intSpecialGroupTypeId ) {
		$this->m_intSpecialGroupTypeId = $intSpecialGroupTypeId;
	}

	public function setSpecialId( $intSpecialId ) {
		$this->m_intSpecialId = $intSpecialId;
	}

	public function setSpecialEndDate( $strSpecialEndDate ) {
		if( true == CValidation::validateDate( $strSpecialEndDate ) )
			$this->m_strSpecialEndDate = $strSpecialEndDate;
		else {
			$this->m_strSpecialEndDate = NULL;
        }
	}

	public function setSpecialStartDate( $strSpecialStartDate ) {
		if( true == CValidation::validateDate( $strSpecialStartDate ) )
			$this->m_strSpecialStartDate = $strSpecialStartDate;
		else {
			$this->m_strSpecialStartDate = NULL;
        }
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 6 ) );
	}

	/**
	 * @param string $strLeaseTermExpiryDate
	 */
	public function setLeaseTermExpiryDate( $strLeaseTermExpiryDate ) {
		$this->m_strLeaseTermExpiryDate = $strLeaseTermExpiryDate;
	}

	public function setAddOnTypeName( $strAddOnTypeName ) {
		$this->m_strAddOnTypeName = $strAddOnTypeName;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

	public function setOfferId( $intOfferId ) {
		$this->m_intOfferId = $intOfferId;
	}

	public function setIsAutoLeaseApprove( $boolIsAutoLeaseApprove ) {
		$this->m_boolIsAutoLeaseApprove = $boolIsAutoLeaseApprove;
	}

	public function setIsStudentProperty( $boolIsStudentProperty ) {
		return $this->m_boolIsStudentProperty = $boolIsStudentProperty;
	}

	public function setMultiplierTotal( $intMultiplierTotal ) {
		$this->m_intMultiplierTotal = $intMultiplierTotal;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		return $this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setBaseRentRenewalOptionId( $intBaseRentRenewalOptionId ) {
		return $this->m_intBaseRentRenewalOptionId = $intBaseRentRenewalOptionId;
	}

	public function setArLookUpTableId( $intArLookupTableId ) {
		return $this->m_intArLookUpTableId = $intArLookupTableId;
	}

	public function setMoveInDate( $strMoveInDate ) {
		return $this->m_strMoveInDate = $strMoveInDate;
	}

	public function setIsCloned( $boolIsCloned ) {
		return $this->m_boolIsCloned = $boolIsCloned;
	}

	/**
	 * Create functions
	 */

	public function createScheduledCharge() {
		$objScheduledCharge = new CScheduledCharge();

		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setRateId( $this->getRateId() );
		$objScheduledCharge->setRateLogId( $this->getId() );
		$objScheduledCharge->setArCodeId( $this->getArCodeId() );
		$objScheduledCharge->setArCodeTypeId( $this->getArCodeTypeId() );
		$objScheduledCharge->setArTriggerId( $this->getArTriggerId() );
		$objScheduledCharge->setArCascadeId( $this->getArCascadeId() );
		$objScheduledCharge->setArCascadeReferenceId( $this->getArCascadeReferenceId() );
		$objScheduledCharge->setArOriginId( $this->getArOriginId() );
		$objScheduledCharge->setArOriginReferenceId( $this->getArOriginReferenceId() );
		$objScheduledCharge->setArOriginObjectId( $this->getArOriginObjectId() );
		$objScheduledCharge->setArFormulaId( $this->getArFormulaId() );
		$objScheduledCharge->setArFormulaReferenceId( $this->getArFormulaReferenceId() );
		$objScheduledCharge->setMemo( $this->getMemo() );
		$objScheduledCharge->setChargeAmount( $this->getRateAmount() );
		$objScheduledCharge->setLeaseTermMonths( $this->getLeaseTermMonths() );
		$objScheduledCharge->setUnitSpaceId( $this->getUnitSpaceId() );
		$objScheduledCharge->setTaxAmount( $this->getTaxAmount() );

		if( true == valId( $this->getLeaseStartWindowId() ) ) {
			$objScheduledCharge->setLeaseStartWindowId( $this->getLeaseStartWindowId() );
		}

		if( true == valId( $this->getLeaseTermId() ) ) {
			$objScheduledCharge->setLeaseTermId( $this->getLeaseTermId() );
		}
		$objScheduledCharge->setMonthToMonthMultiplier( $this->getMonthToMonthMultiplier() );
		$objScheduledCharge->setNormalizedAmount( $this->getNormalizedAmount() );
		$objScheduledCharge->setNormalizedPercent( $this->getNormalizedPercent() );
		$objScheduledCharge->setRateIntervalStart( $this->getRateIntervalStart() );
		$objScheduledCharge->setRateIntervalOccurances( $this->getRateIntervalOccurances() );
		$objScheduledCharge->setRateIntervalOffset( $this->getRateIntervalOffset() );
		$objScheduledCharge->setLeaseAssociationId( $this->getLeaseAssociationId() );

		$objScheduledCharge->setIsCachedToLease( true );

		if( true == in_array( $this->getArFormulaId(), [ CArFormula::PERCENT_OF_CHARGE_CODE, CArFormula::PERCENT_OF_CHARGE_CODE_GROUP, CArFormula::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
			$objScheduledCharge->setChargePercent( $this->getRateIncreaseIncrement() );
		}

		if( CArFormula::PER_ITEM == $this->getArFormulaId() ) {
			$objScheduledCharge->setChargeMultiplierAmount( $this->getRateIncreaseIncrement() );
		}

		return $objScheduledCharge;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function isEffectiveOn( $strEffectiveOn ) : bool {
		return strtotime( $this->getEffectiveDate() ) <= strtotime( $strEffectiveOn )
		&& strtotime( $this->getEffectiveThroughDate() ) >= strtotime( $strEffectiveOn )
		&& strtotime( $this->getDeactivationDate() ) >= strtotime( $strEffectiveOn );
	}

	public function endedAsOn( $strDate ) : bool {
		return strtotime( $this->getEndDate() ) <= strtotime( $strDate );
	}

	public function getUniqueCascadedHash() : string {
		$arrmixPartitionValue = [
			'cid' => ( int ) $this->getCid(),
			'property_id' => ( int ) $this->getPropertyId(),
			'ar_origin_id' => ( int ) $this->getArOriginId(),
			'ar_origin_reference_id' => ( int ) $this->getArOriginReferenceId(), // Need to be careful about 0s
			'ar_origin_object_id' => ( int ) $this->getArOriginObjectId(), // CASE WHEN ar_origin_id = 3 AND ( ( ar_formula_id = 2 AND ar_formula_reference_id = 7 ) OR ar_formula_id = 1 ) THEN 0 ELSE ar_origin_object_id END,
			'unit_type_id' => ( int ) $this->getUnitTypeId(),
			'unit_space_id' => ( int ) $this->getUnitSpaceId(),
			'ar_trigger_id' => ( int ) $this->getArTriggerId(),
			'ar_code_id' => ( int ) $this->getArCodeId(),
			'customer_relationship_id' => ( int ) $this->getCustomerRelationshipId(),
			'space_configuration_id' => ( int ) $this->getSpaceConfigurationId(),
			'lease_term_months' => ( int ) $this->getLeaseTermMonths(),
			'lease_term_id' => ( int ) $this->getLeaseTermId(),
			'lease_start_window_id' => ( int ) $this->getLeaseStartWindowId(),
			'window_start_days' => $this->getWindowStartDays(),
			'window_start_date' => strtotime( $this->getWindowStartDate() ),
			'is_renewal' => ( int ) $this->getIsRenewal()
		];

		return sha1( json_encode( $arrmixPartitionValue ) );
	}

	public function toArray() {
		$arrmixRateLog = parent::toArray(); // TODO: Change the autogenerated stub

		$arrmixRateLog['ar_code_group_id'] = $this->getArCodeGroupId();
		$arrmixRateLog['ar_code_name'] = $this->getArCodeName();

		return $arrmixRateLog;
	}

	/**
	 * @return mixed
	 */
	public function getEndDate() : mixed {
		return date( 'm/d/Y', min( strtotime( $this->getEffectiveThroughDate() ), strtotime( $this->getDeactivationDate() ) ) );
	}

}
?>