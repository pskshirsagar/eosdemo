<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantContacts
 * Do not add any new functions to this class.
 */

class CApplicantContacts extends CBaseApplicantContacts {

	public static function fetchApplicantContactByApplicantIdByContactTypeIdByCid( $intApplicantId, $intContactTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM applicant_contacts WHERE applicant_id::int = ' . ( int ) $intApplicantId . ' AND cid::int = ' . ( int ) $intCid . ' AND customer_contact_type_id::int = ' . ( int ) $intContactTypeId . ' LIMIT 1';
		return self::fetchApplicantContact( $strSql, $objDatabase );
	}

	public static function fetchApplicantContactsByContactTypeIdByApplicationIdByCid( $intContactTypeId, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND a.deleted_on IS NULL' : '';

		$strSql = 'SELECT ac.* FROM applicant_contacts as ac INNER JOIN applicant_applications as a ON ( ac.applicant_id = a.applicant_id AND ac.cid = a.cid ) AND a.application_id = ' . ( int ) $intApplicationId . $strCheckDeletedAASql . ' AND a.cid = ' . ( int ) $intCid . ' AND ac.customer_contact_type_id::int = ' . ( int ) $intContactTypeId . ' ORDER BY ac.applicant_id';
		return self::fetchApplicantContacts( $strSql, $objDatabase );
	}

	public static function fetchApplicantContactsByContactTypeIdsByApplicantIdsByCid( $arrintContactTypeIds, $arrintApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintContactTypeIds ) || false == valArr( $arrintApplicantIds ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						applicant_contacts
					WHERE
						applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND customer_contact_type_id IN( ' . implode( ',', $arrintContactTypeIds ) . ' )
					ORDER BY
						applicant_id';
		return self::fetchApplicantContacts( $strSql, $objDatabase );
	}

	public static function fetchApplicantContactsByContactTypeIdByCustomerIdByCid( $intContactTypeId, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intContactTypeId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT ac.*, a.id as customer_applicant_id
					FROM applicants a
						JOIN customers c ON (c.id = a.customer_id AND c.cid = a.cid)
						LEFT JOIN applicant_contacts ac ON( ac.applicant_id = a.id  AND ac.customer_contact_type_id = ' . $intContactTypeId . ' )
					WHERE c.id = ' . ( int ) $intCustomerId . ' 
					AND c.cid =  ' . ( int ) $intCid . '
					ORDER BY
						ac.created_on, ac.updated_on DESC
					LIMIT 1';

		return self::fetchApplicantContact( $strSql, $objDatabase );
	}

	public static function fetchApplicantContactsByContactTypeIdByCustomerIdsByCid( $intContactTypeId, $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valId( $intContactTypeId ) || false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT ac.*, a.id as customer_applicant_id
					FROM applicants a
						JOIN customers c ON( c.id = a.customer_id AND c.cid = a.cid )
						LEFT JOIN applicant_contacts ac ON( ac.applicant_id = a.id  AND ac.customer_contact_type_id = ' . $intContactTypeId . ' )
					WHERE c.id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
					AND c.cid =  ' . ( int ) $intCid . '
					ORDER BY
						ac.id DESC';

		return self::fetchApplicantContacts( $strSql, $objDatabase );
	}

}
?>