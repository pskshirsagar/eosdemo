<?php

class CCompanyUserPreference extends CBaseCompanyUserPreference {

	const INVOICES_PAST_POST_MONTH					= 'INVOICES_PAST_POST_MONTH';
	const INVOICES_FUTURE_POST_MONTH				= 'INVOICES_FUTURE_POST_MONTH';
	const CHECKS_PAST_POST_MONTH					= 'CHECKS_PAST_POST_MONTH';
	const CHECKS_FUTURE_POST_MONTH					= 'CHECKS_FUTURE_POST_MONTH';
	const GENERAL_JOURNAL_PAST_POST_MONTH			= 'GENERAL_JOURNAL_PAST_POST_MONTH';
	const GENERAL_JOURNAL_FUTURE_POST_MONTH			= 'GENERAL_JOURNAL_FUTURE_POST_MONTH';
	const AR_PAST_POST_MONTH						= 'AR_PAST_POST_MONTH';
	const AR_FUTURE_POST_MONTH						= 'AR_FUTURE_POST_MONTH';
	const NPS_EMAIL									= 'NPS_EMAIL';
	const NPS_POP_UP								= 'NPS_POP_UP';
	const NPS_PHONE									= 'NPS_PHONE';
	const SEND_CLIENT_EXECUTIVE_NPS					= 'SEND_CLIENT_EXECUTIVE_NPS';
	const SYNC_TO_XMPP								= 'SYNC_TO_XMPP';
	const SYNC_TO_XMPP_ENTRATA_NOTIFICATION			= 'SYNC_TO_XMPP_ENTRATA_NOTIFICATION';
	const APPROVED_SIGNER							= 'APPROVED_SIGNER';
	const DEFAULT_TO_RESIDENT_FRIENDLY				= 'DEFAULT_TO_RESIDENT_FRIENDLY';
	const ACCESS_PROPERTY_SETTING_VALIDATION_REPORT = 'ACCESS_PROPERTY_SETTING_VALIDATION_REPORT';
	const DEFAULT_LEDGER_TO_SHOW_REVERSALS			= 'DEFAULT_LEDGER_TO_SHOW_REVERSALS';
	const SIMPLIMENTATION_PRIMARY_USER				= 'SIMPLIMENTATION_PRIMARY_USER';
	const SIMPLIMENTATION_SECONDARY_USER			= 'SIMPLIMENTATION_SECONDARY_USER';
	const AP_TRANSACTIONS_MULTI_PROPERTY_FORMAT		= 'AP_TRANSACTIONS_MULTI_PROPERTY_FORMAT';

	const SHOW_CALL_NOTIFICATIONS                 = 'SHOW_CALL_NOTIFICATIONS';
	const ENABLE_ENTRATA_CALENDAR                 = 'ENABLE_ENTRATA_CALENDAR';
	const ALLOW_RESIDENTWORKS_ADS                 = 'ALLOW_RESIDENTWORKS_ADS';
	const NOTIFICATION_PANEL_CHAT_USER            = 'NOTIFICATION_PANEL_CHAT_USER';
	const ALLOW_CHATS                             = 'ALLOW_CHATS';
	const ALLOW_TEXTING                           = 'ALLOW_TEXTING';
	const ACCESSIBLE_ENTRATA_CALENDAR_EVENT_TYPES = 'ACCESSIBLE_ENTRATA_CALENDAR_EVENT_TYPES';
	const SITE_TABLET_USER                          = 'SITE_TABLET_USER';
	const SITE_TABLET_USER_PACKAGES_ONLY            = 'SITE_TABLET_USER_PACKAGES_ONLY';
	const ENABLE_SEND_BULK_EMAIL_SMS                = 'ENABLE_SEND_BULK_EMAIL_SMS';
	const EMERGENCY_TEXTING                         = 'EMERGENCY_TEXTING';
	const DEFAULT_JES_TO_JOB_COSTING_FORMAT         = 'DEFAULT_JES_TO_JOB_COSTING_FORMAT';

	const USE_USERNAME_INSTEAD_OF_EMAIL         = 1;

	const DENY                                  = 0;
	const ALLOW                                 = 1;
	const USE_GROUP_PREFERENCES                 = 2;

	const RAPID_ONLY_WEEKLY_EMAIL_REMAINDER	    = 1;
	const STANDARD_ONLY_WEEKLY_EMAIL_REMAINDER 	= 2;
	const BOTH_WEEKLY_EMAIL_REMINDER            = 3;
	const NO_WEEKLY_EMAIL_REMINDER              = 0;

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameFull;
	protected $m_boolIsAdministrator;

	public static $c_arrintReleaseNotesSettings = [
		'No'                           => self::NO_WEEKLY_EMAIL_REMINDER,
		'Rapid'                        => self::RAPID_ONLY_WEEKLY_EMAIL_REMAINDER,
		'Standard Only'                => self::STANDARD_ONLY_WEEKLY_EMAIL_REMAINDER,
		'Both (Rapid and Standard)'    => self::BOTH_WEEKLY_EMAIL_REMINDER
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllowDenyGroupPreferences() {
		if( isset( $this->m_arrintAllowDenyGroupPreferences ) )
			return $this->m_arrintAllowDenyGroupPreferences;

		$this->m_arrintAllowDenyGroupPreferences = [
			self::USE_GROUP_PREFERENCES     => __( 'Use group preferences' ),
			self::ALLOW                     => __( 'Yes' ),
			self::DENY                      => __( 'No' )
		];
		return $this->m_arrintAllowDenyGroupPreferences;
	}

	public function getCompanyUserPreferencesFields() {

		return [
			'is_lead_alert_user'                            		=> __( 'Lead Alert User' ),
			'is_available_for_chat'                         		=> __( 'Available For Chat' ),
			'notification_panel_chat_user'                  		=> __( 'Chat/Text Panel User' ),
			'show_call_notifications'                       		=> __( 'Show Call Notifications' ),
			'allow_residentworks_ads'                       		=> __( 'Allow entrata Announcements' ),
			'insurance_stats_report'                        		=> __( 'Resident Insurance Statistics Email Frequency' ),
			'allow_chats'											=> __( 'Allow Chats' ),
			'allow_texting'											=> __( 'Allow 2-Way Texting' ),
			'enable_send_bulk_email_sms'                    		=> __( 'Enable Send Bulk Email/SMS' ),
			'lease_agent_dashboard_calendar_type'           		=> __( 'Leasing Agent Calendar Type' ),
			'lease_agent_dashboard_gcalendar_username'      		=> __( 'Leasing Agent Username' ),
			'lease_agent_dashboard_gcalendar_hostname'      		=> __( 'Leasing Agent Host name' ),
			'lease_agent_dashboard_gcalendar_timezone'      		=> __( 'Leasing Agent Timezone' ),
			'dashboard_ad'                                  		=> __( 'Dashboard Announcement Last Viewed' ),
			'site_tablet_user'                              		=> __( 'Site Tablet User' ),
			'site_tablet_user_packages_only'                		=> __( 'Site Tablet Packages Only' ),
			'approval_routing_master'                       		=> __( 'Approval Master' ),
			'approved_signer'                               		=> __( 'Approved Signer' ),
			'enable_entrata_calendar'								=> __( 'Enable Entrata User\'s Calendar' ),
			'accessible_entrata_calendar_event_types'				=> __( 'Accessible Entrata Calendar Event Types' ),
			'default_to_resident_friendly'                  		=> __( 'Default To Resident Friendly Mode' ),
			'default_ledger_to_show_reversals'              		=> __( 'Default Ledger To Show Reversals' ),
			'set_out_of_office_for_self'                    		=> __( 'User can set out of office for themselves' ),
			'set_out_of_office_for_others'                  		=> __( 'User can set out of office for other users' ),
			'access_property_setting_validation_report'     		=> __( 'Access property setting validation report' ),
			'view_only_lead_applicant_activity_log'         		=> __( 'Make leads/applicants read only' ),
			'leasing_center_statistics_email_frequency'             => __( 'Leasing Center Statistics Email Frequency' ),
			'leasing_center_statistics_email_frequencies_weekly'    => __( 'Weekly' ),
			'leasing_center_statistics_email_frequencies_monthly'   => __( 'Monthly' ),
			'leasing_center_statistics_email_frequencies_quarterly' => __( 'Quarterly' ),
//			'LEASE_AGENT_DASHBOARD_CALENDAR_TYPE'                   => __( 'LEASE_AGENT_DASHBOARD_CALENDAR_TYPE' )
		];

	}

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id does not appear to be valid.' ) );
        }

        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;

    	if( false == isset( $this->m_intCompanyUserId ) || ( 1 > $this->m_intCompanyUserId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'Company user id does not appear to be valid.' ) );
        }

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

        if( false == isset( $this->m_strKey ) || ( false == $this->m_strKey ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Company user preference key must be provided.' ) );
        }

        return $boolIsValid;
    }

    public function valValue() {

        $boolIsValid = true;

        if( true == is_null( $this->getValue() ) || true == preg_match( '/^\s+/', $this->getValue() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', sprintf( '%s is required.', \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->m_strKey ) ) ) ) ) );
        } else {

	        if( true == \Psi\CStringService::singleton()->strstr( $this->m_strKey, 'CRAIGS_LIST_USERNAME' ) && false == CValidation::validateEmailAddresses( $this->getValue() ) ) {
	        	$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', sprintf( '%s not in valid format. (test@example.com)', \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->m_strKey ) ) ) ) ) );
	        }

        }

        return $boolIsValid;
    }

    public function getPassword() {
	    return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getValue(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['is_administrator'] ) ) {
			$this->setIsAdministrator( $arrmixValues['is_administrator'] );
		}
		return;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setIsAdministrator( $boolIsAdministrator ) {
		$this->m_boolIsAdministrator = $boolIsAdministrator;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFull() {
		if( valStr( $this->m_strNameFirst ) || valStr( $this->m_strNameLast ) ) {
			return trim( $this->m_strNameFirst . ' ' . $this->m_strNameLast );
		}
	}

	public function getIsAdministrator() {
		return $this->m_boolIsAdministrator;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valValue();
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>