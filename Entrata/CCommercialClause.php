<?php

class CCommercialClause extends CBaseCommercialClause {

	const WEBSITE       = 1;
	const SPREADSHEET   = 2;
	const DOCUMENT      = 3;

	const COMMERCIAL_CLAUSE_REFERENCE_TYPE = 'commercial_clauses';

	protected $m_intFileId;
	protected $m_intFileAssociationId;

	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strTitle;

	protected $m_arrstrCommercialClauseExternalLinkTypes;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCommercialClauseExternalLinks() {

		$this->m_arrstrCommercialClauseExternalLinkTypes = [
			self::WEBSITE     => __( 'Website' ),
			self::SPREADSHEET => __( 'Spreadsheet' ),
			self::DOCUMENT    => __( 'Document' )
		];

		return $this->m_arrstrCommercialClauseExternalLinkTypes;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getFileAssociationId() {
		return $this->m_intFileAssociationId;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues );

		if( isset( $arrmixValues['file_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intFileId', trim( $arrmixValues['file_id'] ) );
		} elseif( isset( $arrmixValues['file_id'] ) ) {
			$this->setFileId( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['file_id'] ) : $arrmixValues['file_id'] );
		}

		if( isset( $arrmixValues['file_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strFileName', trim( $arrmixValues['file_name'] ) );
		} elseif( isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
		}

		if( isset( $arrmixValues['file_path'] ) && $boolDirectSet ) {
			$this->set( 'm_strFilePath', trim( $arrmixValues['file_path'] ) );
		} elseif( isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['file_path'] ) : $arrmixValues['file_path'] );
		}

		if( isset( $arrmixValues['title'] ) && $boolDirectSet ) {
			$this->set( 'm_strTitle', trim( $arrmixValues['title'] ) );
		} elseif( isset( $arrmixValues['title'] ) ) {
			$this->setTitle( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['title'] ) : $arrmixValues['title'] );
		}

		if( isset( $arrmixValues['file_association_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intFileAssociationId', trim( $arrmixValues['file_association_id'] ) );
		} elseif( isset( $arrmixValues['file_association_id'] ) ) {
			$this->setFileAssociationId( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['file_association_id'] ) : $arrmixValues['file_association_id'] );
		}

	}

	public function setFileId( $intFileId ) {
		$this->m_intFileId = $intFileId;
	}

	public function setFileAssociationId( $intFileAssociationId ) {
		$this->m_intFileAssociationId = $intFileAssociationId;
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 4096, NULL, true ) );
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 4096, NULL, true ) );
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( !valId( $this->getLeaseId() ) ) {
			$boolIsValid = false;
			$strLabel = \Psi\CStringService::singleton()->ucwords( $this->getLabel() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease id is required for {%s, 0}.', [ $strLabel ] ) ) );
		}

		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;

		if( !valStr( $this->getKey() ) ) {
			$boolIsValid = false;
			$strLabel = \Psi\CStringService::singleton()->ucwords( $this->getLabel() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', __( 'Commercial clause key is required for {%s, 0}.', [ $strLabel ] ) ) );
		}

		return $boolIsValid;
	}

	public function valLabel() {
		$boolIsValid = true;

		if( !valStr( $this->getLabel() ) ) {
			$boolIsValid = false;
			$strLabel = \Psi\CStringService::singleton()->ucwords( $this->getLabel() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'label', __( 'Commercial clause label is required for {%s, 0}.', [ $strLabel ] ) ) );
		}

		return $boolIsValid;
	}

	public function valClause() : bool {
		$boolIsValid = true;

		if( !valStr( $this->getClause() ) && !in_array( $this->getKey(), CCommercialClauseKey::$c_arrstrNonMandatoryCommercialClauseKeys, true ) ) {
			$boolIsValid = false;
			$strLabel = \Psi\CStringService::singleton()->ucwords( $this->getLabel() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'clause', __( 'Commercial clause is required for {%s, 0}.', [ $strLabel ] ) ) );
		}

		return $boolIsValid;
	}

	public function valReference() {
		$boolIsValid = true;

		if( !valStr( $this->getReference() ) ) {
			$boolIsValid = false;
			$strLabel = \Psi\CStringService::singleton()->ucwords( $this->getLabel() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference', __( 'Reference is required for {%s, 0}.', [ $strLabel ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valLabel();
				$boolIsValid &= $this->valClause();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( false === valId( $intCurrentUserId ) || false === valObj( $objDatabase, 'CDatabase' ) ) return false;

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}