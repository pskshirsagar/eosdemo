<?php

class CCachedRate extends CBaseCachedRate {
	const DATE_FORMAT           = 'Y-m-d';
	const CACHED_RATES_MAX_DAYS = 90;

	public function setEffectiveDate( $strEffectiveDate ) {

		// Make sure we have an integer representation of the date.
		if( true == is_string( $strEffectiveDate ) ) {
			$intEffectiveDate = strtotime( $strEffectiveDate );
		} elseif( true == valObj( $strEffectiveDate, 'DateTime' ) ) {
			$intEffectiveDate = $strEffectiveDate->getTimestamp();
		} else {
			$intEffectiveDate = $strEffectiveDate;
		}

		$strEffectiveDate = NULL;

		if( false == is_null( $intEffectiveDate ) ) {
			$strEffectiveDate = date( CCachedRate::DATE_FORMAT, $intEffectiveDate );
		}

		parent::setEffectiveDate( $strEffectiveDate );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitKindId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCachedDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>