<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyIdentificationTypes
 * Do not add any new functions to this class.
 */

class CPropertyIdentificationTypes extends CBasePropertyIdentificationTypes {

	public static function fetchPropertyIdentificationTypeByPropertyIdByCompanyIdentificationTypeIdByCid( $intPropertyId, $intCompanyIdentificationTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT pit.*,cit.system_code,cit.name,cit.description
						 FROM
						 		property_identification_types pit,
						 		company_identification_types cit
						 WHERE
						 		pit.cid = cit.cid
						 	AND	pit.company_identification_type_id = cit.id
						 	AND	pit.company_identification_type_id = ' . ( int ) $intCompanyIdentificationTypeId . '
						 	AND pit.property_id = ' . ( int ) $intPropertyId . '
						 	AND pit.cid = ' . ( int ) $intCid . ' LIMIT 1 ';
		return self::fetchPropertyIdentificationType( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdentificationTypeByPropertyIdByRemotePrimaryKeyByCid( $intPropertyId, $strRemotePrimarykey, $intCid, $objDatabase ) {

		$strSql = 'SELECT *
						 FROM
						 		property_identification_types
						 WHERE
						 		property_id = ' . ( int ) $intPropertyId . '
						 	AND remote_primary_key ILIKE \'' . trim( addslashes( $strRemotePrimarykey ) ) . '\'
						 	AND cid = ' . ( int ) $intCid . ' LIMIT 1 ';

		return self::fetchPropertyIdentificationType( $strSql, $objDatabase );
	}

	public static function fetchAssociatedPropertyIdentificationTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

			$strSql = 'SELECT
							pit.*,
							util_get_translated( \'name\', cit.name, cit.details ,\'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name
						FROM
							property_identification_types AS pit
							JOIN company_identification_types AS cit ON ( pit.company_identification_type_id = cit.id AND pit.cid = cit.cid )
						WHERE
							pit.property_id =' . ( int ) $intPropertyId . '
							AND pit.cid =' . ( int ) $intCid;

				return fetchData( $strSql, $objDatabase );
	}

}
?>