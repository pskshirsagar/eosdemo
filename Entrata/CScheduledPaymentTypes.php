<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTypes
 * Do not add any new functions to this class.
 */

class CScheduledPaymentTypes extends CBaseScheduledPaymentTypes {

	public static function fetchScheduledPaymentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScheduledPaymentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScheduledPaymentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledPaymentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>