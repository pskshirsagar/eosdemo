<?php

class CPropertyAmenityAvailability extends CBasePropertyAmenityAvailability {

	protected $m_strAmenityName;
	protected $m_strAmenityDescription;

	protected $m_arrboolAmenityAvailability;

	protected $m_intRateAmount;
	protected $m_intAmenityId;
	protected $m_intIsPublished;
	protected $m_intRateAssociationId;
	protected $m_intArOriginReferenceId;
	protected $m_intReservationscount;
	protected $m_intCompanyMediaFileId;

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues );

		if( isset( $arrstrValues['ar_origin_reference_id'] ) && $boolDirectSet )
			$this->m_intArOriginReferenceId = trim( $arrstrValues['ar_origin_reference_id'] );
		elseif ( isset( $arrstrValues['ar_origin_reference_id'] ) )
			$this->setArOriginReferenceId( $arrstrValues['ar_origin_reference_id'] );

		if( isset( $arrstrValues['name'] ) && $boolDirectSet )
			$this->m_strAmenityName = trim( $arrstrValues['name'] );
		elseif ( isset( $arrstrValues['name'] ) )
			$this->setName( $arrstrValues['name'] );

		if( isset( $arrstrValues['description'] ) && $boolDirectSet )
			$this->m_strAmenityDescription = trim( $arrstrValues['description'] );
		elseif ( isset( $arrstrValues['description'] ) )
			$this->setDescription( $arrstrValues['description'] );

		if( isset( $arrstrValues['rate_amount'] ) && $boolDirectSet )
			$this->m_intAmenityFee = trim( $arrstrValues['rate_amount'] );
		elseif ( isset( $arrstrValues['rate_amount'] ) )
			$this->setRateAmount( $arrstrValues['rate_amount'] );

		if( isset( $arrstrValues['amenity_id'] ) && $boolDirectSet )
			$this->m_intAmenityId = trim( $arrstrValues['amenity_id'] );
		elseif ( isset( $arrstrValues['amenity_id'] ) )
			$this->setAmenityId( $arrstrValues['amenity_id'] );

		if( isset( $arrstrValues['is_published'] ) && $boolDirectSet )
			$this->m_intIsPublished = trim( $arrstrValues['is_published'] );
		elseif ( isset( $arrstrValues['is_published'] ) )
			$this->setIsPublished( $arrstrValues['is_published'] );

		if( isset( $arrstrValues['rate_association_id'] ) && $boolDirectSet )
			$this->m_intRateAssociationId = trim( $arrstrValues['rate_association_id'] );
		elseif ( isset( $arrstrValues['rate_association_id'] ) )
			$this->setRateAssociationId( $arrstrValues['rate_association_id'] );

		if( isset( $arrstrValues['company_media_file_id'] ) && $boolDirectSet )
			$this->m_intCompanyMediaFileId = trim( $arrstrValues['company_media_file_id'] );
		elseif ( isset( $arrstrValues['company_media_file_id'] ) )
			$this->setCompanyMediaFileId( $arrstrValues['company_media_file_id'] );

		if( isset( $arrstrValues['reservations_count'] ) && $boolDirectSet )
			$this->m_intReservationsCount = trim( $arrstrValues['reservations_count'] );
		elseif ( isset( $arrstrValues['reservations_count'] ) )
			$this->setReservationsCount( $arrstrValues['reservations_count'] );

		return;
	}

	public function setName( $strName ) {
		$this->m_strAmenityName = $strName;
	}

	public function getName() {
		return $this->m_strAmenityName;
	}

	public function setDescription( $strDescription ) {
		$this->m_strAmenityDescription = $strDescription;
	}

	public function getDescription() {
		return $this->m_strAmenityDescription;
	}

	public function setRateAmount( $intRateAmount ) {
		$this->m_intRateAmount = CStrings::strToIntDef( $intRateAmount, NULL, false );
	}

	public function getRateAmount() {
		return $this->m_intRateAmount;
	}

	public function setAmenityId( $intAmenityId ) {
		$this->m_intAmenityId = $intAmenityId;
	}

	public function getAmenityId() {
		return $this->m_intAmenityId;
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->m_intArOriginReferenceId = $intArOriginReferenceId;
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function setIsPublished( $intIsPublished ) {
		$this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function setRateAssociationId( $intRateAssociationId ) {
		$this->m_intRateAssociationId = CStrings::strToIntDef( $intRateAssociationId, NULL, false );
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->m_intCompanyMediaFileId = CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false );
	}

	public function getRateAssociationId() {
		return $this->m_intRateAssociationId;
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function setReservationsCount( $intReservationsCount ) {
		$this->m_intReservationsCount = CStrings::strToIntDef( $intReservationsCount, NULL, false );
	}

	public function getReservationsCount() {
		return $this->m_intReservationsCount;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRateAssociationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReservationFeeOccurrenceTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAvailability() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeResidentAutomatically() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequirePaymentImmediately() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequirePropertyApproval() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsReservedByDay() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valUpdateBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdateOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

    public function setBoolAvailability() {

    	$arrboolAvailability = [];
    	true == ( $this->isSundayBlocked( $this->m_intAvailability ) ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == ( $this->isMondayBlocked( $this->m_intAvailability ) ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTuesdayBlocked( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isWednesdayBlocked( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isThursdayBlocked( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isFridayBlocked( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isSaturdayBlocked( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isZeroSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isOneSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwoSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isThreeSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isFourSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isFiveSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isSixSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isSevenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isEightSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isNineSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isElevenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwelveSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isThirteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isFourteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isFifteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isSixteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isSeventeenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isEighteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isNineteenSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwentySelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwentyoneSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwentytwoSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	true == $this->isTwentythreeSelected( $this->m_intAvailability ) ? array_push( $arrboolAvailability, true ) : array_push( $arrboolAvailability, false );

    	$this->m_arrboolAmenityAvailability = $arrboolAvailability;
    }

    public function getAvailabilityEdit( $intIndex ) {
    	return $this->m_arrboolAmenityAvailability[$intIndex];
    }

    // get the blocked days from intAvailability

    public function getBlockDays() {
		$arrstrBlockDays = [];
		$strBlockdays = NULL;

		if( true == $this->isSundayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'Su' );
		}
		if( true == $this->isMondayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'M' );
		}
		if( true == $this->isTuesdayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'Tu' );
		}
		if( true == $this->isWednesdayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'W' );
		}
		if( true == $this->isThursdayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'Th' );
		}
		if( true == $this->isFridayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'F' );
		}
		if( true == $this->isSaturdayBlocked( $this->m_intAvailability ) ) {
			array_push( $arrstrBlockDays, 'Sa' );
		}

		$intCountBlockDays = \Psi\Libraries\UtilFunctions\count( $arrstrBlockDays );
		for( $intI = 0; $intI < $intCountBlockDays; $intI++ ) {
			if( $intI == ( \Psi\Libraries\UtilFunctions\count( $arrstrBlockDays ) - 1 ) ) {
				$strBlockdays .= $arrstrBlockDays[$intI];
			} else {
				$strBlockdays .= $arrstrBlockDays[$intI] . ', ';
			}
		}

		return $strBlockdays;
	}

	public function getAvailableHours() {

		$arrstrAvailableHours = [];

		if( true == $this->isZeroSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '12am' );
		}

		if( true == $this->isOneSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '1am' );
		}

		if( true == $this->isTwoSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '2am' );
		}

		if( true == $this->isThreeSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '3am' );
		}

		if( true == $this->isFourSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '4am' );
		}

		if( true == $this->isFiveSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '5am' );
		}

		if( true == $this->isSixSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '6am' );
		}

		if( true == $this->isSevenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '7am' );
		}

		if( true == $this->isEightSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '8am' );
		}

		if( true == $this->isNineSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '9am' );
		}

		if( true == $this->isTenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '10am' );
		}

		if( true == $this->isElevenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '11am' );
		}

		if( true == $this->isTwelveSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '12pm' );
		}

		if( true == $this->isThirteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '1pm' );
		}

		if( true == $this->isFourteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '2pm' );
		}

		if( true == $this->isFifteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '3pm' );
		}

		if( true == $this->isSixteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '4pm' );
		}

		if( true == $this->isSeventeenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '5pm' );
		}

		if( true == $this->isEighteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '6pm' );
		}

		if( true == $this->isNineteenSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '7pm' );
		}

		if( true == $this->isTwentySelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '8pm' );
		}

		if( true == $this->isTwentyoneSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '9pm' );
		}

		if( true == $this->isTwentytwoSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '10pm' );
		}

		if( true == $this->isTwentythreeSelected( $this->m_intAvailability ) ) {
			array_push( $arrstrAvailableHours, '11pm' );
		}

		$intAvailableHoursCount = \Psi\Libraries\UtilFunctions\count( $arrstrAvailableHours );
		if( 0 == $intAvailableHoursCount ) {
			return 'No Block HRS';
		}

		for( $intI = 0; $intI < $intAvailableHoursCount; $intI++ ) {
			if( $intI == ( \Psi\Libraries\UtilFunctions\count( $arrstrAvailableHours ) - 1 ) ) {
				echo $arrstrAvailableHours[$intI];
			} else {
				echo $arrstrAvailableHours[$intI] . ', ';
			}
		}

		return;
	}

    public function isSundayBlocked( $intAvailability ) {
    	$intSundayMask = 1 << 0;
    	$boolBlockSunday = ( 0 == ( $intAvailability & $intSundayMask ) ) ? true : false;
    	return $boolBlockSunday;
    }

    private function isMondayBlocked( $intAvailability ) {
    	$intMondayMask = 1 << 1;
    	$boolBlockMonday = ( 0 == ( $intAvailability & $intMondayMask ) ) ? true : false;
    	return $boolBlockMonday;
    }

    private function isTuesdayBlocked( $intAvailability ) {
    	$intTuesdayMask = 1 << 2;
    	$boolBlockTuesday = ( 0 == ( $intAvailability & $intTuesdayMask ) ) ? true : false;
    	return $boolBlockTuesday;
    }

    private function isWednesdayBlocked( $intAvailability ) {
    	$intWednesdayMask = 1 << 3;
    	$boolBlockWednesday = ( 0 == ( $intAvailability & $intWednesdayMask ) ) ? true : false;
    	return $boolBlockWednesday;
    }

    private function isThursdayBlocked( $intAvailability ) {
    	$intThursdayMask = 1 << 4;
    	$boolBlockThursday = ( 0 == ( $intAvailability & $intThursdayMask ) ) ? true : false;
    	return $boolBlockThursday;
    }

    private function isFridayBlocked( $intAvailability ) {
    	$intFridayMask = 1 << 5;
    	$boolBlockFriday = ( 0 == ( $intAvailability & $intFridayMask ) ) ? true : false;
    	return $boolBlockFriday;
    }

    private function isSaturdayBlocked( $intAvailability ) {
    	$intSaturdayMask = 1 << 6;
    	$boolBlockSaturday = ( 0 == ( $intAvailability & $intSaturdayMask ) ) ? true : false;
    	return $boolBlockSaturday;
    }

    private function isZeroSelected( $intAvailability ) {
    	$intZeroMask = 1 << 7;
    	$boolBlockZero = ( 0 == ( $intAvailability & $intZeroMask ) ) ? true : false;
    	return $boolBlockZero;
    }

    private function isOneSelected( $intAvailability ) {
    	$intOneMask = 1 << 8;
    	$boolBlockOne = ( 0 == ( $intAvailability & $intOneMask ) ) ? true : false;
    	return $boolBlockOne;
    }

    private function isTwoSelected( $intAvailability ) {
    	$intTwoMask = 1 << 9;
    	$boolBlockTwo = ( 0 == ( $intAvailability & $intTwoMask ) ) ? true : false;
    	return $boolBlockTwo;
    }

    private function isThreeSelected( $intAvailability ) {
    	$intThreeMask = 1 << 10;
    	$boolBlockThree = ( 0 == ( $intAvailability & $intThreeMask ) ) ? true : false;
    	return $boolBlockThree;
    }

    private function isFourSelected( $intAvailability ) {
    	$intFourMask = 1 << 11;
    	$boolBlockFour = ( 0 == ( $intAvailability & $intFourMask ) ) ? true : false;
    	return $boolBlockFour;
    }

    private function isFiveSelected( $intAvailability ) {
    	$intFiveMask = 1 << 12;
    	$boolBlockFive = ( 0 == ( $intAvailability & $intFiveMask ) ) ? true : false;
    	return $boolBlockFive;
    }

    private function isSixSelected( $intAvailability ) {
    	$intSixMask = 1 << 13;
    	$boolBlockSix = ( 0 == ( $intAvailability & $intSixMask ) ) ? true : false;
    	return $boolBlockSix;
    }

    private function isSevenSelected( $intAvailability ) {
    	$intSevenMask = 1 << 14;
    	$boolBlockSeven = ( 0 == ( $intAvailability & $intSevenMask ) ) ? true : false;
    	return $boolBlockSeven;
    }

    private function isEightSelected( $intAvailability ) {
    	$intEightMask = 1 << 15;
    	$boolBlockEight = ( 0 == ( $intAvailability & $intEightMask ) ) ? true : false;
    	return $boolBlockEight;
    }

    private function isNineSelected( $intAvailability ) {
    	$intNineMask = 1 << 16;
    	$boolBlockNine = ( 0 == ( $intAvailability & $intNineMask ) ) ? true : false;
    	return $boolBlockNine;
    }

    private function isTenSelected( $intAvailability ) {
    	$intTenMask = 1 << 17;
    	$boolBlockTen = ( 0 == ( $intAvailability & $intTenMask ) ) ? true : false;
    	return $boolBlockTen;
    }

    private function isElevenSelected( $intAvailability ) {
    	$intElevenMask = 1 << 18;
    	$boolBlockEleven = ( 0 == ( $intAvailability & $intElevenMask ) ) ? true : false;
    	return $boolBlockEleven;
    }

    private function isTwelveSelected( $intAvailability ) {
    	$intTwelveMask = 1 << 19;
    	$boolBlockTwelve = ( 0 == ( $intAvailability & $intTwelveMask ) ) ? true : false;
    	return $boolBlockTwelve;
    }

    private function isThirteenSelected( $intAvailability ) {
    	$intThirteenMask = 1 << 20;
    	$boolBlockThirteen = ( 0 == ( $intAvailability & $intThirteenMask ) ) ? true : false;
    	return $boolBlockThirteen;
    }

    private function isFourteenSelected( $intAvailability ) {
    	$intFourteenMask = 1 << 21;
    	$boolBlockFourteen = ( 0 == ( $intAvailability & $intFourteenMask ) ) ? true : false;
    	return $boolBlockFourteen;
    }

    private function isFifteenSelected( $intAvailability ) {
    	$intFifteenMask = 1 << 22;
    	$boolBlockFifteen = ( 0 == ( $intAvailability & $intFifteenMask ) ) ? true : false;
    	return $boolBlockFifteen;
    }

    private function isSixteenSelected( $intAvailability ) {
    	$intSixteenMask = 1 << 23;
    	$boolBlockSixteen = ( 0 == ( $intAvailability & $intSixteenMask ) ) ? true : false;
    	return $boolBlockSixteen;
    }

    private function isSeventeenSelected( $intAvailability ) {
    	$intSeventeenMask = 1 << 24;
    	$boolBlockSeventeen = ( 0 == ( $intAvailability & $intSeventeenMask ) ) ? true : false;
    	return $boolBlockSeventeen;
    }

    private function isEighteenSelected( $intAvailability ) {
    	$intEighteenMask = 1 << 25;
    	$boolBlockEighteen = ( 0 == ( $intAvailability & $intEighteenMask ) ) ? true : false;
    	return $boolBlockEighteen;
    }

    private function isNineteenSelected( $intAvailability ) {
    	$intNineteenMask = 1 << 26;
    	$boolBlockNineteen = ( 0 == ( $intAvailability & $intNineteenMask ) ) ? true : false;
    	return $boolBlockNineteen;
    }

    private function isTwentySelected( $intAvailability ) {
    	$intTwentyMask = 1 << 27;
    	$boolBlockTwenty = ( 0 == ( $intAvailability & $intTwentyMask ) ) ? true : false;
    	return $boolBlockTwenty;
    }

    private function isTwentyoneSelected( $intAvailability ) {
    	$intTwentyoneMask = 1 << 28;
    	$boolBlockTwentyone = ( 0 == ( $intAvailability & $intTwentyoneMask ) ) ? true : false;
    	return $boolBlockTwentyone;
    }

    private function isTwentytwoSelected( $intAvailability ) {
    	$intTwentytwoMask = 1 << 29;
    	$boolBlockTwentytwo = ( 0 == ( $intAvailability & $intTwentytwoMask ) ) ? true : false;
    	return $boolBlockTwentytwo;
    }

    private function isTwentythreeSelected( $intAvailability ) {
    	$intTwentythreeMask = 1 << 30;
    	$boolBlockTwentythree = ( 0 == ( $intAvailability & $intTwentythreeMask ) ) ? true : false;
    	return $boolBlockTwentythree;
    }

	public function calculateAmenityAvailability() {
    	$arrstrJsonData			= [];
    	$arrstrFinalJsonData	= [];

    	$arrmixHoursFunction = [ 0  => $this->isZeroSelected( $this->m_intAvailability ),
            1  => $this->isOneSelected( $this->m_intAvailability ),
            2  => $this->isTwoSelected( $this->m_intAvailability ),
            3  => $this->isThreeSelected( $this->m_intAvailability ),
            4  => $this->isFourSelected( $this->m_intAvailability ),
            5  => $this->isFiveSelected( $this->m_intAvailability ),
            6  => $this->isSixSelected( $this->m_intAvailability ),
            7  => $this->isSevenSelected( $this->m_intAvailability ),
            8  => $this->isEightSelected( $this->m_intAvailability ),
            9 => $this->isNineSelected( $this->m_intAvailability ),
            10 => $this->isTenSelected( $this->m_intAvailability ),
            11 => $this->isElevenSelected( $this->m_intAvailability ),
            12 => $this->isTwelveSelected( $this->m_intAvailability ),
            13 => $this->isThirteenSelected( $this->m_intAvailability ),
            14 => $this->isFourteenSelected( $this->m_intAvailability ),
            15 => $this->isFifteenSelected( $this->m_intAvailability ),
            16 => $this->isSixteenSelected( $this->m_intAvailability ),
            17 => $this->isSeventeenSelected( $this->m_intAvailability ),
            18 => $this->isEighteenSelected( $this->m_intAvailability ),
            19 => $this->isNineteenSelected( $this->m_intAvailability ),
            20 => $this->isTwentySelected( $this->m_intAvailability ),
            21 => $this->isTwentyoneSelected( $this->m_intAvailability ),
            22 => $this->isTwentytwoSelected( $this->m_intAvailability ),
            23 => $this->isTwentythreeSelected( $this->m_intAvailability )
	    ];

    	for( $intPosition = 0; $intPosition <= 6; $intPosition++ ) {
    		switch( $intPosition ) {

    			case '0':
    				$arrstrJsonData['sunday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isSundayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '1':
    				$arrstrJsonData['monday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isMondayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '2':
    				$arrstrJsonData['tuesday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isTuesdayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '3':
    				$arrstrJsonData['wednesday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isWednesdayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '4':
    				$arrstrJsonData['thursday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isThursdayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '5':
    				$arrstrJsonData['friday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isFridayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			case '6':
    				$arrstrJsonData['saturday'] = $this->bulidJsonDataFormat( $arrmixHoursFunction, $this->isSaturdayBlocked( $this->m_intAvailability ), $this->m_intIsReservedByDay );
    				break;

    			default:
    				// Not implemented
    				break;
    		}
    	}

    	return json_encode( $arrstrJsonData );

    }

    public function bulidJsonDataFormat( $arrmixHoursFunction, $boolDayBlocked, $boolIsReservedByDay ) {
    	$arrstrJsonMixFinalData = [];

    	if( true == $boolIsReservedByDay ) {
    		for( $intPosition = 0; $intPosition <= 23; $intPosition++ ) {
    			$arrstrJsonMixFinalData[] = ( true == $boolDayBlocked ) ? 1 : 0;
    		}
    	} else {
    		foreach( $arrmixHoursFunction as $arrmixHourFunction ) {
    			$arrstrJsonMixFinalData[] = ( true == $boolDayBlocked || true == $arrmixHourFunction ) ? 1 : 0;
    		}
    	}
    	return $arrstrJsonMixFinalData;

    }

    public function buildBlockDays( $strDay ) {
    	$arrstrBlockDays	= [];
    	$strBlockdays		= NULL;

    	if( 'sunday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'Su' );
    	}
    	if( 'monday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'M' );
    	}
    	if( 'tuesday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'Tu' );
    	}
    	if( 'wednesday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'W' );
    	}
    	if( 'thursday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'Th' );
    	}
    	if( 'friday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'F' );
    	}
    	if( 'saturday' == $strDay ) {
    		array_push( $arrstrBlockDays, 'Sa' );
    	}

		for( $intI = 0; $intI < \Psi\Libraries\UtilFunctions\count( $arrstrBlockDays ); $intI++ ) {
    		$strBlockdays = $arrstrBlockDays[$intI] . ', ';
    	}

    	return \Psi\CStringService::singleton()->substr( $strBlockdays, 0, -2 );
    }

}
?>
