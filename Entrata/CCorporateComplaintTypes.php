<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateComplaintTypes
 * Do not add any new functions to this class.
 */

class CCorporateComplaintTypes extends CBaseCorporateComplaintTypes {

	public static function fetchCorporateComplaintTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCorporateComplaintType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCorporateComplaintType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCorporateComplaintType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedCorporateComplaintTypes( $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						corporate_complaint_types
					WHERE
						is_published = TRUE 
					ORDER BY
						order_num';

		return self::fetchCorporateComplaintTypes( $strSql, $objDatabase );
	}

	public static function fetchAllCorporateComplaintTypes( $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						corporate_complaint_types
					ORDER BY
						order_num';

		return self::fetchCorporateComplaintTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedCorporateComplaintTypeById( $intId, $objDatabase ) {
		if( false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						corporate_complaint_types
					WHERE
						id = ' . ( int ) $intId . '
						AND is_published = TRUE';

		return self::fetchCorporateComplaintType( $strSql, $objDatabase );
	}

	public static function fetchCorporateComplaintTypesByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						corporate_complaint_types
					WHERE
						id IN( ' . sqlIntImplode( $arrintIds ) . ' )';

		return self::fetchCorporateComplaintTypes( $strSql, $objDatabase );
	}

}
?>