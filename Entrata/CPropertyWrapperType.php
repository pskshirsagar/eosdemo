<?php

class CPropertyWrapperType extends CBasePropertyWrapperType {

	const PROSPECT_PORTAL			= 1;
	const RESIDENT_PORTAL			= 2;
	const PAYMENT 					= 3;
	const MAINTENANCE				= 4;
	const ENROLLMENT 				= 5;
}
?>