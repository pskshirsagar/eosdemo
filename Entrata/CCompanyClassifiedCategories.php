<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyClassifiedCategories
 * Do not add any new functions to this class.
 */

class CCompanyClassifiedCategories extends CBaseCompanyClassifiedCategories {

	public static function fetchAssociatedCompanyClassifiedCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ccc.id,
						ccc.name,
						CASE
							WHEN ccc.code = \'EE\'
							THEN 1
							ELSE 0
						END category_order
					FROM
						company_classified_categories ccc
						LEFT JOIN property_classified_categories pcc ON ( ccc.id = pcc.company_classified_category_id AND ccc.cid = pcc.cid )
					WHERE
						ccc.cid =  ' . ( int ) $intCid . '
						AND pcc.property_id =  ' . ( int ) $intPropertyId . '
					ORDER BY
						category_order DESC,
						ccc.name ASC';

		return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchCompanyClassifiedCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
	$strSql = 'SELECT util_set_locale(\'' . \CLocaleContainer::createService()->getLocaleCode() . '\', \'' . \CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						ccc.id,
						util_get_system_translated( \'name\', ccc.name, ccc.details ) AS name,
						ccc.description
					FROM
						company_classified_categories ccc
						LEFT JOIN property_classified_categories pcc ON ( ccc.id = pcc.company_classified_category_id AND ccc.cid = pcc.cid )
					WHERE
						ccc.cid =  ' . ( int ) $intCid . '
						AND pcc.property_id =  ' . ( int ) $intPropertyId . '
					ORDER BY
						ccc.name ASC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDuplicateCompanyClassifiedCategoryCountByIdByNameByCid( $intCompanyClassifiedCategoryId, $strCompanyClassifiedCategoryName, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE
								cid = ' . ( int ) $intCid . '
								AND lower(name)=\'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strCompanyClassifiedCategoryName ) ) ) . '\'';

		if( true == is_numeric( $intCompanyClassifiedCategoryId ) ) {
			$strWhereSql .= ' AND id != ' . ( int ) $intCompanyClassifiedCategoryId;
		}

		return self::fetchCompanyClassifiedCategoryCount( $strWhereSql, $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoriesCountByPropertyIdByCid( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						count( ccc.id )
					FROM
						company_classified_categories ccc,
						property_classified_categories pcc
					WHERE
						ccc.id = pcc.company_classified_category_id
						AND ccc.cid = pcc.cid
						AND ccc.cid = ' . ( int ) $intCid . '
						AND pcc.property_id = ' . ( int ) $intPropertyId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrstrData[0]['count'] ) ) return $arrstrData[0]['count'];
		return 0;
	}

	public static function fetchPaginatedCompanyClassifiedCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $intPageNo, $intCountPerPage, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intCountPerPage * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intCountPerPage;

		$strSql = 'SELECT
						ccc.id, ccc.name, ccc.description, count(cc.id) as ads_count, ccc.details
					FROM
						company_classified_categories ccc
					LEFT JOIN
						property_classified_categories pcc ON( ccc.id = pcc.company_classified_category_id AND ccc.cid = pcc.cid)
					LEFT JOIN
				 		customer_classifieds cc ON( cc.company_classified_category_id = ccc.id AND cc.cid = pcc.cid AND pcc.property_id = cc.property_id
													AND cc.is_published = 1 AND cc.deleted_on IS NULL AND (cc.hide_on IS NULL OR cc.hide_on > now()))
					WHERE
						pcc.property_id = ' . ( int ) $intPropertyId . '
						AND pcc.cid =  ' . ( int ) $intCid . '
					GROUP BY ccc.id, ccc.name, ccc.description, ccc.details
					ORDER BY id desc OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchObjects( $strSql, 'CCompanyClassifiedCategory', $objDatabase );
	}

	public static function fetchCompanyClassifiedCategoryByCompanyClassifiedCategoryIdByPropertyIdByCid( $intClassifiedCategoryId, $intPropertyId, $intCid, $objDatabase ) {
		 $strSql = 'SELECT
						ccc.name,
						pcc.is_admin_restricted,
						ccc.details
					FROM
						company_classified_categories ccc
					LEFT JOIN
						property_classified_categories pcc ON( ccc.id = pcc.company_classified_category_id AND ccc.cid = pcc.cid )
					WHERE
						pcc.property_id = ' . ( int ) $intPropertyId . '
						AND ccc.id = ' . ( int ) $intClassifiedCategoryId . '
						AND pcc.cid =  ' . ( int ) $intCid . '
				ORDER BY ccc.id desc';

		return self::fetchObject( $strSql, 'CCompanyClassifiedCategory', $objDatabase );
	}

	public static function fetchCustomCompanyClassifiedCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ccc.id
					FROM
						company_classified_categories ccc
					LEFT JOIN
						property_classified_categories pcc ON( ccc.id = pcc.company_classified_category_id AND ccc.cid = pcc.cid)
					LEFT JOIN
				 		customer_classifieds cc ON( cc.company_classified_category_id = ccc.id AND cc.cid = pcc.cid AND pcc.property_id = cc.property_id
													AND cc.is_published = 1 AND cc.deleted_on IS NULL AND (cc.hide_on IS NULL OR cc.hide_on > now()))
					WHERE
						pcc.property_id = ' . ( int ) $intPropertyId . '
						AND pcc.cid =  ' . ( int ) $intCid . '
					GROUP BY ccc.id
					ORDER BY id desc';

		return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchCustomCompanyClassifiedCategoryByIdByCid( $intCompanyClassifiedCategoryId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyClassifiedCategoryId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_classified_categories
					WHERE
						id = ' . ( int ) $intCompanyClassifiedCategoryId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchObject( $strSql, 'CCompanyClassifiedCategory', $objDatabase );

	}

	public static function fetchCustomCompanyClassifiedCategoriesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ccc.*
					FROM
						company_classified_categories ccc
					WHERE
						ccc.cid = ' . ( int ) $intCid . ' ORDER BY id DESC';

		return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );

	}

}
?>