<?php

class CChoreType extends CBaseChoreType {

	protected $m_strChoreCategoryName;
	protected $m_strDefaultChoreTypeName;

	protected $m_intChoreStatusTypeId;
	protected $m_intItemCount;

	/**
	 * Get Functions
	 */

	public function getChoreCategoryName() {
		return $this->m_strChoreCategoryName;
	}

	public function getChoreStatusTypeId() {
		return $this->m_intChoreStatusTypeId;
	}

	public function getItemCount() {
		return $this->m_intItemCount;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['chore_category_name'] ) ) $this->setChoreCategoryName( $arrmixValues['chore_category_name'] );
		if( true == isset( $arrmixValues['chore_status_type_id'] ) ) $this->setChoreStatusTypeId( $arrmixValues['chore_status_type_id'] );
		if( true == isset( $arrmixValues['item_count'] ) ) $this->setItemCount( $arrmixValues['item_count'] );

		return;
	}

	public function setChoreCategoryName( $strChoreCategoryName ) {
		$this->m_strChoreCategoryName = CStrings::strTrimDef( $strChoreCategoryName, -1, NULL, true );
	}

	public function setChoreStatusTypeId( $intChoreStatusTypeId ) {
		$this->m_intChoreStatusTypeId = $intChoreStatusTypeId;
	}

	public function setItemCount( $intItemCount ) {
		$this->m_intItemCount = $intItemCount;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultChoreTypeId( $objClientDatabase ) {

		if( true == valId( $this->getId() ) ) {

			$objChoreType = CChoreTypes::fetchChoreTypeByIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

			if( $objChoreType->getChoreCategoryId() != $this->getChoreCategoryId() ) {

				$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
							AND chore_type_id =' . ( int ) $this->getId();

				$intChoreCount = CChores::fetchChoreCount( $strWhere, $objClientDatabase );

				if( 0 < $intChoreCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_chore_Type_id', __( 'Closing task is being used in "{%s, 0}" tasks.', [ $this->getChoreCategoryName() ] ) ) );
					return false;
				}
			}
		}

		if( 0 == $this->getIsSystem() ) {

			$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
							AND chore_category_id = ' . ( int ) $this->getChoreCategoryId() . '
							AND default_chore_type_id = ' . ( int ) $this->getDefaultChoreTypeId() . '
							AND is_system <> 1
							AND id <> ' . ( int ) $this->getId();

			$intChoreTypeCount = CChoreTypes::fetchChoreTypeCount( $strWhere, $objClientDatabase );

			if( 0 < $intChoreTypeCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_chore_Type_id', __( 'Closing task for default closing task type is already exist.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valChoreCategoryId() {

		$boolIsValid = true;

		if( false == valId( $this->getChoreCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chore_category_id', __( 'Closing task for is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Closing task name is required.' ) ) );
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
						AND name ILIKE \'' . addslashes( $this->getName() ) . '\'
						AND id <> ' . ( int ) $this->getId();

		$intChoreTypeCount = CChoreTypes::fetchChoreTypeCount( $strWhere, $objClientDatabase );

		if( 0 < $intChoreTypeCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Closing task name already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSendEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredToLockMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredToAdvanceMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn( $objClientDatabase ) {

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
						AND chore_type_id =' . ( int ) $this->getId();

		$intChoreCount = CChores::fetchChoreCount( $strWhere, $objClientDatabase );

		if( 0 < $intChoreCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Closing task is being used in system.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valChoreCategoryId();
				$boolIsValid &= $this->valDefaultChoreTypeId( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletedOn( $objClientDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Validate Functions
	 */

	public function fetchDefaultChoreTypesByChoreCategoryId( $objClientDatabase ) {
		return CDefaultChoreTypes::fetchDefaultChoreTypesByChoreCategoryId( $this->getChoreCategoryId(), $objClientDatabase );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPeriodIds( $objClientDatabase ) {
		return CPeriods::fetchPeriodIdsByChoreTypeIdByChoreCategoryIdByCid( $this->getId(), $this->getChoreCategoryId(), $this->getCid(), $objClientDatabase );
	}

}
?>