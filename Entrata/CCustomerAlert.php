<?php

class CCustomerAlert extends CBaseCustomerAlert {

	const PRIORITY_RED		= 1;
	const PRIORITY_YELLOW	= 2;
	const PRIORITY_GREEN	= 3;
	const PRIORITY_BLUE		= 4;
	const DEFAULT_PRIORITY_LEVEL = self::PRIORITY_YELLOW;

	const PRIORITY_PAUSED_SCHEDULED_PAYMENT	= 1;
	const PRIORITY_LATE_RENT				= 2;
	const PRIORITY_MOVE_IN_CHECKLIST		= 3;
	const PRIORITY_MOVE_OUT_CHECKLIST		= 4;
	const PRIORITY_INSPECTION				= 5;
	const PRIORITY_AR_PAYMENT_RETURN		= 6;
	const PRIORITY_SCHEDULED_PAYMENT_RETURN = 6;
	const PRIORITY_SIGN_LEASE				= 7;
	const PRIORITY_RENEWAL_OFFER			= 7;
	const PRIORITY_TRANSFER_QUOTE_OFFER		= 7;
	const PRIORITY_ESIGN					= 8;
	const PRIORITY_RATING_AND_REVIEW		= 9;
	const PRIORITY_OPT_IN_SMS				= 10;
	const PRIORITY_OPT_OUT_EMAIL_INVOICES	= 11;
	const PRIORITY_PARCEL					= 12;
	const PRIORITY_ROOMMATE_PAYMENT			= 13;
	const PRIORITY_SCHEDULED_CHARGE			= 14;
	const PRIORITY_RELINK_PAYMENT_ACCOUNT	= 1;

	protected $m_objGenerator;
	protected $m_strAlertTypeName;
	protected $m_strAlertTypeDescription;
	protected $m_strDetails;
	protected $m_strAlertType;
	protected $m_intFileId;

	public function __construct() {
		parent::__construct();
		return;
	}

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['alert_type_name'] ) ) {
			$this->setAlertTypeName( $arrmixValues['alert_type_name'] );
		}
		if( true == isset( $arrmixValues['alert_type_description'] ) ) {
			$this->setAlertTypeDescription( $arrmixValues['alert_type_description'] );
		}
		if( true == isset( $arrmixValues['alert_type_detail'] ) ) {
			$this->setDetails( $arrmixValues['alert_type_detail'] );
		}

		if( true == isset( $arrmixValues['alert_type'] ) ) {
			$this->setAlertType( $arrmixValues['alert_type'] );
		}

		if( true == isset( $arrmixValues['file_id'] ) ) {
			$this->setFileId( $arrmixValues['file_id'] );
		}
	}

	// Get Functions

	public function getIsResidentPortalPopup() {
		return in_array( $this->getCustomerAlertTypeId(), CCustomerAlertType::$c_arrintResidentPortalPopupIds );
	}

	public function getIsShowInModal() {
        return in_array( $this->getCustomerAlertTypeId(), CCustomerAlertType::$c_arrintShowInModalIds );
	}

	public function getPriorityColor() {
		switch( $this->getPriority() ) {
			case self::PRIORITY_RED:
				return 'red';
				break;
			case self::PRIORITY_YELLOW:
			case self::DEFAULT_PRIORITY_LEVEL:
				return 'yellow';
				break;
			case self::PRIORITY_GREEN:
				return 'green';
				break;
			case self::PRIORITY_BLUE:
				return 'blue';
				break;
			default:
				return 'yellow';
		}
	}

	public function getDaysBeforeRedisplay() {
		return NULL;
	}

	public function getAlertTypeName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function getAlertTypeDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function getAlertType() {
		return $this->m_strAlertType;
	}

	public function getName( $strLocale = NULL ) {
		return $this->getTranslated( 'name', $strLocale ?? CLocaleContainer::createService()->getLocaleCode(), false );
	}

	// Set Functions

	public function setAlertTypeName( $strAlertTypeName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strAlertTypeName, 50, NULL, true ), $strLocaleCode );
	}

	public function setAlertTypeDescription( $strAlertTypeDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strAlertTypeDescription, 50, NULL, true ), $strLocaleCode );
	}

	public function setAlertType( $strAlertType ) {
		$this->m_strAlertType 	= $strAlertType;
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	/*
	 * Validation functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerAlertTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActionText() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDismissedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDismissedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>