<?php

class CDefaultFaq extends CBaseDefaultFaq {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultFaqTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>