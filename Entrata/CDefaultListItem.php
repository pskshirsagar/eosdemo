<?php

class CDefaultListItem extends CBaseDefaultListItem {

	const DEFAULT_LIST_ITEM_OTHER 							= 40;
	const DEFAULT_LIST_ITEM_COMBINED 						= 65;
	const DEFAULT_LIST_ITEM_CLONED 							= 66;
	const DEFAULT_LIST_ITEM_ARCHIVED 						= 72;
	const DEFAULT_LIST_ITEM_AFFORDABLE_DEATH 				= 73;
	const DEFAULT_LIST_ITEM_AFFORDABLE_OTHER 				= 74;
	const DEFAULT_LIST_ITEM_SCREENING_DECISION_DENIED 		= 88;
	const DEFAULT_LIST_ITEM_CONDITION_NOT_MET 				= 89;
	const DEFAULT_LIST_ITEM_MERGED                          = 90;

	public static $c_strExcludedFromReports = ' (Excluded from Reports)';

	public static $c_arrintNotProgressionActionExcludeDefaultListItemIds = [
		self::DEFAULT_LIST_ITEM_COMBINED,
		self::DEFAULT_LIST_ITEM_CLONED
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getExcludedFromReports() {
		$c_strExcludedFromReports = __( '(Excluded from Reports)' );

		return $c_strExcludedFromReports;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valListTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowInEntrata() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowOnWebsite() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>