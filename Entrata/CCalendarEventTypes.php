<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventTypes
 * Do not add any new functions to this class.
 */

class CCalendarEventTypes extends CBaseCalendarEventTypes {

	public static function fetchCalendarEventTypesByCidByRemoveResidentEvents( $intCid, $objDatabase, $boolRemoveResidentEvents, $boolCanAccessLeads = NULL, $boolCanAccessResidents = NULL, $boolCanEditPropertyCalendars = NULL, $boolIsFromAccessParameters = false, $boolIsSkipMoveInSchedulerEvent = true ) {

		// Remove Resident/Leasing event types if the user cannot edit property calendars
		if( false == $boolCanEditPropertyCalendars ) {
			$boolCanAccessLeads = false;
			$boolCanAccessResidents = false;
		}

		$strCategoryName = '';
		$strWhereSql = '';
		if( true == $boolIsFromAccessParameters ) {
			$strCategoryName = 'util_get_translated( \'name\', cec.name, cec.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS calendar_event_category_name ';
		} else {
			$strCategoryName = 'cec.name AS calendar_event_category_name';
		}

		if( true == $boolIsSkipMoveInSchedulerEvent ) {
			$strWhereSql = ' AND cet.default_calendar_event_type_id <> ' . CDefaultCalendarEventType::MOVE_IN_SCHEDULER;
		}

		if( true == $boolRemoveResidentEvents ) {
			$strSql = 'SELECT
							cet.*,
							util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name, 
							util_get_translated( \'description\', cet.description, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS description,'
							. $strCategoryName . '
						FROM
							calendar_event_types cet
							JOIN calendar_event_categories cec ON cec.id = cet.calendar_event_category_id
						WHERE
							cet.cid = ' . ( int ) $intCid . $strWhereSql . '
							AND ( cec.id IN ( 1';
			if( true == $boolCanAccessLeads ) {
				$strSql .= ',3 ) ';
			} else {
				$strSql .= ' ) ';
			}
			if( true == $boolCanAccessResidents ) {
				$strSql .= 'OR cet.name = \'Resident Appointment\' )';
			} else {
				$strSql .= ' ) ';
			}
			$strSql .= 'ORDER BY
						cec.id, cet.name';

			return self::fetchCalendarEventTypes( $strSql, $objDatabase );
		} else {
			$strSql = 'SELECT
							cet.*,
							util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name,
							util_get_translated( \'description\', cet.description, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS description,'
							. $strCategoryName . '
						FROM
							calendar_event_types cet
							JOIN calendar_event_categories cec ON cec.id = cet.calendar_event_category_id
						WHERE
							cet.cid = ' . ( int ) $intCid . $strWhereSql . '
						ORDER BY
							cec.id, cet.name';

			return self::fetchCalendarEventTypes( $strSql, $objDatabase );
		}
	}

	public static function fetchCalendarEventTypesByCalendarEventCategoryIdByCid( $intCalendarEventCategoryId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
							cet.*,
							util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name, 
							util_get_translated( \'description\', cet.description, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS description,
							cec.name AS calendar_event_category_name
						FROM
							calendar_event_types cet
							JOIN calendar_event_categories cec ON ( cec.id = cet.calendar_event_category_id )
						WHERE
							cet.cid = ' . ( int ) $intCid . ' AND
							cet.calendar_event_category_id = ' . ( int ) $intCalendarEventCategoryId . '
						ORDER BY
							cec.id, cet.name';

		return self::fetchCalendarEventTypes( $strSql, $objDatabase );
	}

	public static function fetchSimpleCalendarEventTypeByDefaultCalendarEventTypeIdByCid( $intDefaultCalendarEventTypeId, $intCid, $objDatabase ) {
			$strSql = 'SELECT
							cet.id
						FROM 
							calendar_event_types cet
						WHERE 
							default_calendar_event_type_id = ' . ( int ) $intDefaultCalendarEventTypeId . '
							AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

}
?>