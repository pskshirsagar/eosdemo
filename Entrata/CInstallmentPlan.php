<?php

class CInstallmentPlan extends CBaseInstallmentPlan {

	protected $m_strLeaseTermName;
	protected $m_strWindowStartDate;
	protected $m_strWindowEndDate;
	protected $m_strSpecialDetails;

	protected $m_intSpecialRecipientId;
	protected $m_intSpecialTypeId;
	protected $m_intRateAssociationId;
	protected $m_intArOriginId;

	const INSTALLMENT_PLAN_ADDED = 1;
	const INSTALLMENT_PLAN_UPDATED = 2;
	const INSTALLMENT_PLAN_REMOVED = 3;
	const INSTALLMENT_PLAN_STATUS_APPLIED = 1;
	const INSTALLMENT_PLAN_STATUS_APPLY = 2;
	const INSTALLMENT_PLAN_STATUS_DISCOUNT_NEEDED = 3;

	public function valPaymentArCodeId() {
		$boolIsValid = true;
		$intPaymentArCodeId = $this->getPaymentArCodeId();
		if( true == empty( $intPaymentArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_ar_code_id', __( 'Please select payment charge code.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getWindowStartDate() {
		return $this->m_strWindowStartDate;
	}

	public function getWindowEndDate() {
		return $this->m_strWindowEndDate;
	}

	public function getSpecialRecipientId() {
		return $this->m_intSpecialRecipientId;
	}

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	public function getSpecialDetails() {
		return $this->m_strSpecialDetails;
	}

	public function getRateAssociationId() {
		return $this->m_intRateAssociationId;
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		return $this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setSpecialRecipientId( $intSpecialRecipientId ) {
		return $this->m_intSpecialRecipientId = $intSpecialRecipientId;
	}

	public function setWindowEndDate( $strWindowEndDate ) {
		return $this->m_strWindowEndDate = $strWindowEndDate;
	}

	public function setWindowStartDate( $strWindowStartDate ) {
		return $this->m_strWindowStartDate = $strWindowStartDate;
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		return $this->m_intSpecialTypeId = $intSpecialTypeId;
	}

	public function setSpecialDetails( $strSpecialDetails ) {
		return $this->m_strSpecialDetails = $strSpecialDetails;
	}

	public function setRateAssociationId( $intRateAssociationId ) {
		return $this->m_intRateAssociationId = $intRateAssociationId;
	}

	public function setArOriginId( $intArOriginId ) {
		return $this->m_intArOriginId = $intArOriginId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lease_term_name'] ) ) {
			$this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		}

		if( true == isset( $arrmixValues['window_start_date'] ) ) {
			$this->setWindowStartDate( $arrmixValues['window_start_date'] );
		}

		if( true == isset( $arrmixValues['window_end_date'] ) ) {
			$this->setWindowEndDate( $arrmixValues['window_end_date'] );
		}

		if( true == isset( $arrmixValues['special_recipient_id'] ) ) {
			$this->setSpecialRecipientId( $arrmixValues['special_recipient_id'] );
		}

		if( true == isset( $arrmixValues['special_type_id'] ) ) {
			$this->setSpecialTypeId( $arrmixValues['special_type_id'] );
		}

		if( true == isset( $arrmixValues['special_details'] ) ) {
			$this->setSpecialDetails( $arrmixValues['special_details'] );
		}

		if( true == isset( $arrmixValues['rate_association_id'] ) ) {
			$this->setRateAssociationId( $arrmixValues['rate_association_id'] );
		}

		if( true == isset( $arrmixValues['ar_origin_id'] ) ) {
			$this->setArOriginId( $arrmixValues['ar_origin_id'] );
		}
	}

	public function valLeaseTermId( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getLeaseTermId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term_id', __( 'Property lease term id required.' ) ) );
			$boolIsValid = false;
		}

		if( 0 > ( int ) CLeaseTerms::fetchExistingLeaseTermsCountByIdByCid( $this->getLeaseTermId(), $this->getCid(), $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term_id', __( 'This lease term id does not exist' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valLeaseStartWindowId( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getLeaseStartWindowId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_window_id', __( 'Lease start window is required.' ) ) );
			$boolIsValid = false;
		}

		$objLeaseStartWindow = CLeaseStartWindows::fetchLeaseStartWindowByIdByCid( $this->getLeaseStartWindowId(), $this->getCid(), $objDatabase );
		if( false == valObj( $objLeaseStartWindow, CLeaseStartWindow::class ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_window_id', __( 'This lease start window id does not exist.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function valName() {
    	$boolIsValid = true;
    	// It is required
    	if( true == is_null( $this->getName() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
    		$boolIsValid = false;
    	} elseif( 1 > strlen( $this->getName() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
    		$boolIsValid = false;
    	}
    	return $boolIsValid;
    }

    public function valDescription() {
    	$boolIsValid = true;
    	if( true == is_null( $this->getDescription() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Description is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valCid() {
    	$boolIsValid = true;
    	// It is required
    	if( true == is_null( $this->getCid() || 1 > $this->getCid() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'A Client Id is required - CInstallmentPlan' ) ) );
    		return false;
    	}

    	return $boolIsValid;
    }

    public function valId() {
    	$boolIsValid = true;
    	// It is required
    	if( true == is_null( $this->getId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Id is required - CInstallmentPlan' ) ) );
    		$boolIsValid = false;
    	}
    	// Must be greater than zero
    	if( 1 > $this->getId() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Id has to be greater than zero - CInstallmentPlan' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valExistingLeaseIntervalsForInstallmentPlan( $objDatabase, $boolIsFromOrganizationContract = false ) {

    	$boolIsValid = true;

	    if( 0 < ( int ) \Psi\Eos\Entrata\CLeaseIntervalInstallmentPlans::createService()->fetchExistingLeaseIntervalInstallmentPlansCountByInstallmentPlanIdByCid( $this->getId(), $this->getCid(), $objDatabase ) ) {
	    	$strErrorMessage = ( true == $boolIsFromOrganizationContract ) ? 'Installment plan "' . $this->getName() . '" cannot be deleted, as it is associated to one or more leases' : 'You can not delete, this option is associated to one or more leases.';
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'installment_plan_id', __( '{%s,0}', [ $strErrorMessage ] ) ) );
    		$boolIsValid = false;
    	} elseif( 0 < ( int ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchExistingScheduledChargesCountByInstallmentPlanIdByCid( $this->getId(), $this->getCid(), $objDatabase ) ) {
		    $strErrorMessage = ( true == $boolIsFromOrganizationContract ) ? 'Installment plan "' . $this->getName() . '" cannot be deleted, as it is associated to one or more scheduled charges' : 'You can not delete, this option is associated to one or more scheduled charges.';
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_special_id', __( '{%s,0}', [ $strErrorMessage ] ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valPercentage() {
    	$boolIsValid = true;

    	if( false == is_null( $this->getPercentage() ) && ( false == is_numeric( $this->getPercentage() ) || 0 >= $this->getPercentage() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', __( 'Percentage must be whole number or greater than zero.' ) ) );

    	} elseif( false == $this->getIsProratedOverLifeOfLease() && 100 < $this->getPercentage() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', __( 'Percentage must be greater than zero or less than or equal to 100.' ) ) );

    	} elseif( false == is_null( $this->getPercentage() ) && 0 == $this->getPercentage() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', __( 'Percentage must be whole number or greater than zero.' ) ) );

    	} elseif( false == is_null( $this->getAmount() ) && 0 == $this->getAmount() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', __( 'Amount is required.' ) ) );

    	} elseif( true == is_null( $this->getAmount() ) && '' == $this->getPercentage() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', __( 'Amount must be whole number or greater than zero.' ) ) );

    	}
    	return $boolIsValid;
    }

    public function createInstallment() {

    	$objInstallment = new CInstallment();
    	$objInstallment->setCid( $this->getCid() );
    	$objInstallment->setInstallmentPlanId( $this->getId() );

    	return $objInstallment;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsSemesterSelectionEnabled = false, $boolIsFromOrganizationContract = false ) {
    	$boolIsValid = true;
    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valCid();
    			$boolIsValid &= $this->valName();
    			$boolIsValid &= $this->valDescription();
    			$boolIsValid &= $this->valPaymentArCodeId();
    			$boolIsValid &= $this->valLeaseTermId( $objDatabase );
    			if( true == $boolIsSemesterSelectionEnabled ) {
				    $boolIsValid &= $this->valLeaseStartWindowId( $objDatabase );
			    }
    			break;

    		case VALIDATE_DELETE:
    			$boolIsValid &= $this->valExistingLeaseIntervalsForInstallmentPlan( $objDatabase, $boolIsFromOrganizationContract );
    			$boolIsValid &= $this->valId();
    			$boolIsValid &= $this->valCid();
    			break;

    		default:
    		$boolIsValid = false;
    	}

        return $boolIsValid;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function generateArchieveScreeningPropertyLeaseTermAssociationsSQL( $intCurrentUserId ) {
		return 'UPDATE
				       property_lease_term_installment_plan_associations
				   SET
				       is_published = false,
				       updated_by = ' . ( int ) $intCurrentUserId . ',
				       updated_on = NOW ()
				   WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND default_installment_plan_id = ' . ( int ) $this->getId() . '
						AND is_published = true
						AND property_id = ' . ( int ) $this->getPropertyId() . ';';
	}

	public function setReassociateInstallmentPlan( $boolReassociateInstallmentPlan ) {
		$this->setDetailsField( 'reassociate_installment_plan', $boolReassociateInstallmentPlan );
	}

	public function getReassociateInstallmentPlan() {
		return ( bool ) $this->getDetailsField( 'reassociate_installment_plan' );
	}

}
?>