<?php

class CCompanyEmployeeContact extends CBaseCompanyEmployeeContact {

	protected $m_arrstrCompanyEmployeeContactAvailabilities;
	protected $m_strNameFull;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPhoneNumber1;
	protected $m_strPhoneNumber2;
	protected $m_strEmailAddress;
	protected $m_strCompanyEmployeeContactAvailabilityTypeName;
	protected $m_intPhoneNumber1IsPublished;
	protected $m_intPhoneNumber2IsPublished;
	protected $m_intCompanyEmployeeContactAvailabilityTypeId;
	protected $m_intWeekDay;
	protected $m_intPropertyInstructionId;

	public function __construct() {
		parent::__construct();

		$this->m_intPhoneNumber1IsPublished = 1;
		$this->m_intPhoneNumber2IsPublished = 1;
		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( isset( $arrmixValues['phone_number1'] ) ) $this->setPhoneNumber1( $arrmixValues['phone_number1'] );
		if( isset( $arrmixValues['phone_number2'] ) ) $this->setPhoneNumber2( $arrmixValues['phone_number2'] );
		if( isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( isset( $arrmixValues['phone_number1_is_published'] ) ) $this->setPhoneNumber1IsPublished( $arrmixValues['phone_number1_is_published'] );
		if( isset( $arrmixValues['phone_number2_is_published'] ) ) $this->setPhoneNumber2IsPublished( $arrmixValues['phone_number2_is_published'] );
		if( isset( $arrmixValues['company_employee_contact_availability_type_id'] ) ) $this->setCompanyEmployeeContactAvailabilityTypeId( $arrmixValues['company_employee_contact_availability_type_id'] );
		if( isset( $arrmixValues['week_day'] ) ) $this->setWeekDay( $arrmixValues['week_day'] );
		if( isset( $arrmixValues['property_instruction_id'] ) ) $this->setPropertyInstructionId( $arrmixValues['property_instruction_id'] );
		if( isset( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
		if( isset( $arrmixValues['company_employee_contact_availability_type_name'] ) ) $this->setCompanyEmployeeContactAvailabilityTypeName( $arrmixValues['company_employee_contact_availability_type_name'] );
		return;
	}

	 /**
	 * SetFunctions
	 */

	public function setCompanyEmployeeContactAvailabilities( $arrstrCompanyEmployeeContactAvailabilities ) {
		$this->m_arrstrCompanyEmployeeContactAvailabilities = $arrstrCompanyEmployeeContactAvailabilities;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = trim( $strNameFirst );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = trim( $strNameLast );
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setPhoneNumber1( $strPhoneNumber1 ) {
		$this->m_strPhoneNumber1 = trim( $strPhoneNumber1 );
	}

	public function setPhoneNumber2( $strPhoneNumber2 ) {
		$this->m_strPhoneNumber2 = trim( $strPhoneNumber2 );
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = trim( $strEmailAddress );
	}

	public function setPhoneNumber1IsPublished( $intPhoneNumber1IsPublished ) {
		$this->m_intPhoneNumber1IsPublished = ( int ) $intPhoneNumber1IsPublished;
	}

	public function setPhoneNumber2IsPublished( $intPhoneNumber2IsPublished ) {
		$this->m_intPhoneNumber2IsPublished = ( int ) $intPhoneNumber2IsPublished;
	}

	public function setCompanyEmployeeContactAvailabilityTypeId( $intCompanyEmployeeContactAvailabilityTypeId ) {
		$this->m_intCompanyEmployeeContactAvailabilityTypeId = ( int ) $intCompanyEmployeeContactAvailabilityTypeId;
	}

	public function setCompanyEmployeeContactAvailabilityTypeName( $strCompanyEmployeeContactAvailabilityTypeName ) {
		$this->m_strCompanyEmployeeContactAvailabilityTypeName = $strCompanyEmployeeContactAvailabilityTypeName;
	}

	public function setWeekDay( $intWeekDay ) {
		$this->m_intWeekDay = ( int ) $intWeekDay;
	}

	public function setPropertyInstructionId( $intPropertyInstructionId ) {
		$this->m_intPropertyInstructionId = ( int ) $intPropertyInstructionId;
	}

	/**
	 * Get Functions
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFull() {
		if( false == valStr( $this->m_strNameFull ) ) {
			$this->m_strNameFull = trim( $this->m_strNameFirst . ' ' . $this->m_strNameLast );
		}

		return $this->m_strNameFull;
	}

	public function getPhoneNumber1() {
		return  preg_replace( '/\D/', '', $this->m_strPhoneNumber1 );
	}

	public function getPhoneNumber2() {
		return  preg_replace( '/\D/', '', $this->m_strPhoneNumber2 );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getPhoneNumber1IsPublished() {
		return $this->m_intPhoneNumber1IsPublished;
	}

	public function getPhoneNumber2IsPublished() {
		return $this->m_intPhoneNumber2IsPublished;
	}

	public function getCompanyEmployeeContactAvailabilityTypeId() {
		return $this->m_intCompanyEmployeeContactAvailabilityTypeId;
	}

	public function getCompanyEmployeeContactAvailabilityTypeName() {
		return $this->m_strCompanyEmployeeContactAvailabilityTypeName;
	}

	public function getWeekDay() {
		return $this->m_intWeekDay;
	}

	public function getPropertyInstructionId() {
		return $this->m_intPropertyInstructionId;
	}

	public function getCompanyEmployeeContactAvailabilities() {
		return $this->m_arrstrCompanyEmployeeContactAvailabilities;
	}

	public function valConflictCompanyEmployeeContact( CDatabase $objDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Application Error: Provide database not proper.' ), E_USER_ERROR );
		}

		$objExistingCompanyEmployeeContact = CCompanyEmployeeContacts::fetchConflictingCompanyEmployeeContactByCidByPropertyIdByCompanyEmployeeContactTypeIdByPhoneNumber1ByNameFirstByNameLast( $this->getCid(), $this->getPropertyId(), $this->getCompanyEmployeeContactTypeId(), $this->getPhoneNumber1(), $this->getNameFirst(), $this->getNameLast(), $objDatabase, $this->getId() );

		if( true == valObj( $objExistingCompanyEmployeeContact, 'CCompanyEmployeeContact' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number1', __( 'The company employee already exists with these details, please use the same.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsPrimaryCompanyEmployeeContact() {
		$boolIsValid = true;

		if( true == $this->getIsPrimary() && 1 < \Psi\Libraries\UtilFunctions\count( $this->getCorporateComplaintTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corporate_complaint_type_ids', __( 'Primary Contacts can only have one Complaint Type selected.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictCompanyEmployeeContact( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_corporate_company_employee_contact':
				$boolIsValid &= $this->valConflictCompanyEmployeeContact( $objDatabase );
				$boolIsValid &= $this->valIsPrimaryCompanyEmployeeContact();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	* Creater Functions
	*/

	public function createCall( $objPhoneNumberType, $objCallAgent, $objVoipDatabase ) {
		$objCall = new CCall();

		$objCall->setCid( $this->getCid() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setCallTypeId( CCallType::LEASING_CENTER_OUTBOUND );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallResultId( CCallResult::WORK_ORDER );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setCompanyEmployeeId( $this->getCompanyEmployeeId() );
		$objCall->setDestinationPhoneNumber( $this->getCompanyEmployeePhoneNumberByPhoneNumberTypeId( $objPhoneNumberType->getId() ) );
		$objCall->setOriginPhoneNumber( CCallPhoneNumber::LEASING_CENTER_LINE );
		$objCall->setEmployeeId( $objCallAgent->getEmployeeId() );

		if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call record.', E_USER_WARNING );
			return NULL;
		}

		return $objCall;
	}

	public function createEmergencyMaintenanceCall( $intEmployeeId, $objVoipDatabase, $intPhoneNumberTypeId = NULL, $intCallAgentId = NULL, $intPhysicalPhoneExtension = NULL, $strCustomerPhoneNumber = NULL ) {
		$objCall = new CCall();
		$objCall->setCid( $this->getCid() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setCallTypeId( CCallType::LEASING_CENTER_OUTBOUND );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallResultId( CCallResult::WORK_ORDER );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setCompanyEmployeeId( $this->getCompanyEmployeeId() );
		$objCall->setDestinationPhoneNumber( $this->getCompanyEmployeePhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId ) );
		$objCall->setOriginPhoneNumber( CCallPhoneNumber::LEASING_CENTER_LINE );
		$objCall->setEmployeeId( $intEmployeeId );
		$objCall->setCallAgentId( $intCallAgentId );
		$objCall->setOriginExtension( $intPhysicalPhoneExtension );
		$objCall->setAttemptCount( 1 );
		$objCall->setCustomerPhoneNumber( $strCustomerPhoneNumber );

		if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call record.', E_USER_WARNING );
			return NULL;
		}

		return $objCall;
	}

	public function getCompanyEmployeePhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId ) {
		$strCompanyEmployeePhoneNumber = NULL;

		switch( $intPhoneNumberTypeId ) {
			case CPhoneNumberType::PRIMARY:
				$strCompanyEmployeePhoneNumber = $this->getPhoneNumber1();
				break;

			default:
				$strCompanyEmployeePhoneNumber = $this->getPhoneNumber2();
				break;
		}

		return $strCompanyEmployeePhoneNumber;
	}

}
?>