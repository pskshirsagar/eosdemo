<?php

class CApCodeType extends CBaseApCodeType {

	const GL_ACCOUNT			= 1;
	const CATALOG_ITEMS			= 2;
	const JOB_COST				= 3;
	const INTER_COMPANY_INCOME	= 4;
	const INTER_COMPANY_LOANS	= 5;
	const CAM_EXPENSES      	= 6;

	public static $c_arrstrApCodeTypes = [
		self::INTER_COMPANY_INCOME	=> 'Inter Company Income',
		self::INTER_COMPANY_LOANS	=> 'Inter Company Loans'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>