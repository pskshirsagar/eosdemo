<?php

class CArCodeType extends CBaseArCodeType {

	const PAYMENT					= 1;
	const RENT						= 2;
	const OTHER_INCOME				= 3;
	const EXPENSE					= 4;
	const ASSET						= 5;
	const EQUITY					= 6;
	const DEPOSIT					= 7;
	const REFUND					= 8;
	const OTHER_LIABILITY			= 9;
	const INTER_COMPANY_RECEIVABLE	= 10;
	const INTER_COMPANY_LOAN		= 11;
	const MANAGEMENT_FEES			= 12;

	public static $c_arrintHideArCodeTypeIds						= [ CArCodeType::PAYMENT, CArCodeType::REFUND ];

	public static $c_arrintLiabilityArCodeTypeIds					= [ CArCodeType::DEPOSIT, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintNonReservedLiabilityArCodeTypeIds		= [ CArCodeType::DEPOSIT, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintChargeArCodeTypeIds						= [ CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_INCOME ];

	public static $c_arrintRentArCodeTypeIds						= [ CArCodeType::RENT ];

	public static $c_arrintOtherFeesArCodeTypes						= [ CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintRentAndOtherFeesArCodeTypes				= [ CArCodeType::RENT, CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintDepositAndOtherFeesArCodeTypes			= [ CArCodeType::DEPOSIT, CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintIntegratedInterChangeableArCodeTypes		= [ CArCodeType::ASSET, CArCodeType::DEPOSIT, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::MANAGEMENT_FEES, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY, CArCodeType::RENT ];

	public static $c_arrintAllArCodeTypes							= [ CArCodeType::PAYMENT, CArCodeType::RENT, CArCodeType::OTHER_INCOME, CArCodeType::EXPENSE, CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::DEPOSIT, CArCodeType::REFUND, CArCodeType::OTHER_LIABILITY, CArCodeType::INTER_COMPANY_RECEIVABLE, CArCodeType::INTER_COMPANY_LOAN, CArCodeType::MANAGEMENT_FEES ];

	public static $c_arrintRentDepositAndOtherFeesArCodeTypes		= [ CArCodeType::RENT, CArCodeType::DEPOSIT, CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintInterCompanyArCodeTypes					= [ CArCodeType::INTER_COMPANY_RECEIVABLE, CArCodeType::INTER_COMPANY_LOAN ];

	public static $c_arrintExcludeFromEffectiveRentArCodeTypes 		= [ CArCodeType::REFUND, CArCodeType::PAYMENT, CArCodeType::DEPOSIT, CArCodeType::INTER_COMPANY_RECEIVABLE, CArCodeType::INTER_COMPANY_LOAN, CArCodetype::MANAGEMENT_FEES ];

	public static $c_arrintExcludeRenewalArCodeTypeIds				= [ CArCodeType::REFUND, CArCodeType::PAYMENT, CArCodeType::MANAGEMENT_FEES, CArCodeType::INTER_COMPANY_RECEIVABLE ];

	public static $c_arrintRentDepositeAndOtherChargesArCodeTypes	= [ CArCodeType::RENT, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY, CArCodeType::EXPENSE ];

	public static $c_arrintTransferOtherFeesArCodeTypes				= [ CArCodeType::ASSET, CArCodeType::EQUITY, CArCodeType::EXPENSE, CArCodeType::OTHER_LIABILITY ];

	public static $c_arrintExcludeFromStraightLineLedgerArCodeTypes	= [ CArCodeType::PAYMENT, CArCodeType::INTER_COMPANY_LOAN, CArCodeType::DEPOSIT, CArCodeType::INTER_COMPANY_RECEIVABLE, CArCodeType::REFUND ];

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'AR_CODE_TYPE_PAYMENT', self::PAYMENT );
		$objSmarty->assign( 'AR_CODE_TYPE_RENT', self::RENT );
		$objSmarty->assign( 'AR_CODE_TYPE_OTHER_INCOME', self::OTHER_INCOME );
		$objSmarty->assign( 'AR_CODE_TYPE_EXPENSE', self::EXPENSE );
		$objSmarty->assign( 'AR_CODE_TYPE_ASSET', self::ASSET );
		$objSmarty->assign( 'AR_CODE_TYPE_EQUITY', self::EQUITY );
		$objSmarty->assign( 'AR_CODE_TYPE_DEPOSIT', self::DEPOSIT );
		$objSmarty->assign( 'AR_CODE_TYPE_REFUND', self::REFUND );
		$objSmarty->assign( 'AR_CODE_TYPE_OTHER_LIABILITY',	self::OTHER_LIABILITY );
		$objSmarty->assign( 'AR_CODE_TYPE_INTER_COMPANY_RECEIVABLE', self::INTER_COMPANY_RECEIVABLE );
		$objSmarty->assign( 'AR_CODE_TYPE_INTER_COMPANY_LOAN', self::INTER_COMPANY_LOAN );
		$objSmarty->assign( 'AR_CODE_TYPE_MANAGEMENT_FEES',	self::MANAGEMENT_FEES );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters = array() ) {
		$arrmixTemplateParameters['AR_CODE_TYPE_PAYMENT']                  = self::PAYMENT;
		$arrmixTemplateParameters['AR_CODE_TYPE_RENT']                     = self::RENT;
		$arrmixTemplateParameters['AR_CODE_TYPE_OTHER_INCOME']             = self::OTHER_INCOME;
		$arrmixTemplateParameters['AR_CODE_TYPE_EXPENSE']                  = self::EXPENSE;
		$arrmixTemplateParameters['AR_CODE_TYPE_ASSET']                    = self::ASSET;
		$arrmixTemplateParameters['AR_CODE_TYPE_EQUITY']                   = self::EQUITY;
		$arrmixTemplateParameters['AR_CODE_TYPE_DEPOSIT']                  = self::DEPOSIT;
		$arrmixTemplateParameters['AR_CODE_TYPE_REFUND']                   = self::REFUND;
		$arrmixTemplateParameters['AR_CODE_TYPE_OTHER_LIABILITY']          = self::OTHER_LIABILITY;
		$arrmixTemplateParameters['AR_CODE_TYPE_INTER_COMPANY_RECEIVABLE'] = self::INTER_COMPANY_RECEIVABLE;
		$arrmixTemplateParameters['AR_CODE_TYPE_INTER_COMPANY_LOAN']       = self::INTER_COMPANY_LOAN;
		$arrmixTemplateParameters['AR_CODE_TYPE_MANAGEMENT_FEES']          = self::MANAGEMENT_FEES;
		return $arrmixTemplateParameters;
	}

	public static function getArCodeTypeNameByArCodeTypeId( $intArCodeTypeId ) {
		switch( $intArCodeTypeId ) {
			case self::PAYMENT:
				return __( 'Payment' );
				break;

			case self::RENT:
				return __( 'Rent' );
				break;

			case self::OTHER_INCOME:
				return __( 'Other Income' );
				break;

			case self::EXPENSE:
				return __( 'Expense' );
				break;

			case self::ASSET:
				return __( 'Asset' );
				break;

			case self::EQUITY:
				return __( 'Equity' );

			case self::REFUND:
				return __( 'Refund' );
				break;

			case self::DEPOSIT:
				return __( 'Deposit' );
				break;

			case self::OTHER_LIABILITY:
				return __( 'Other Liability' );
				break;

			case self::INTER_COMPANY_RECEIVABLE:
				return __( 'Inter-Company Receivable' );
				break;

			case self::INTER_COMPANY_LOAN:
				return __( 'Inter-Company Loan' );
				break;

			case self::MANAGEMENT_FEES:
				return __( 'Management Fees' );
				break;
			default:
				$boolIsValid = true;
				break;
		}
	}

}
?>