<?php

class CCachedFloorplan extends CBaseCachedFloorplan {

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentableUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManualMinMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManualMaxMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManualMinTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManualMaxTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMinTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMaxTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAvgTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentableUnitSpaceIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableUnitSpaceIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsManualRentRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsManualDepositRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>