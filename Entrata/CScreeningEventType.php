<?php

class CScreeningEventType extends CBaseScreeningEventType {

	const DENY                              = 1;
	const APPROVE_WITH_CONDITION            = 2;
	const CONDITIONAL_GUARANTOR_COMPLETED   = 3;
	const DECISION_APPROVE                  = 4;
	const CONTINGENTLY_APPROVED             = 5;
	const RESULT_GUARANTOR_FAIL             = 6;

	public static $c_arrintEventAssociatedWithNotifications = [ self::DENY, self::DECISION_APPROVE, self::CONDITIONAL_GUARANTOR_COMPLETED ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>