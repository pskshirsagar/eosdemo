<?php

class CLateFeeFormulaCodes extends CBaseLateFeeFormulaCodes {

	public static function fetchPropertyLateFeeFormulaCodesByLatefeeFormulaIdByCid( $intLatefeeFormulaId, $intCid, $objDatabase ) {
		return self::fetchLateFeeFormulaCodes( sprintf( 'SELECT * FROM late_fee_formula_codes WHERE late_fee_formula_id = %d AND cid = %d', ( int ) $intLatefeeFormulaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLateFeeFormulaCodesByParentLateFeeFormulaIdByCid( $intParentLateFeeFormulaId, $intCid, $objDatabase ) {
	    $strSql = 'SELECT
						lffc.*,
						lff.name AS late_fee_formula_name,
						ac.name AS ar_code_name
					FROM
						late_fee_formula_codes lffc
						JOIN late_fee_formulas lff ON ( lffc.cid = lff.cid AND lffc.late_fee_formula_id = lff.id )
						JOIN ar_codes ac ON ( lffc.cid = ac.cid AND lffc.ar_code_id = ac.id )
					WHERE
						lff.is_disabled = FALSE
						AND lff.parent_late_fee_formula_id = ' . ( int ) $intParentLateFeeFormulaId . '
						AND lffc.cid = ' . ( int ) $intCid;

	    return fetchData( $strSql, $objDatabase );
	}

}
?>