<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledBlackoutDates
 * Do not add any new functions to this class.
 */

class CScheduledBlackoutDates extends CBaseScheduledBlackoutDates {

	public static function fetchScheduledBlackoutDateByFrequencyIdByScheduleByStartDateByPropertyIdByCid( $intFrequencyId, $strSchedule, $strStartDate, $intPropertyId, $intCid, $objDatabase, $intScheduledBlackoutDateId = NULL ) {

		$strSql = '	SELECT *
    				FROM
    					scheduled_blackout_dates
    				WHERE
    					cid = ' . ( int ) $intCid .
    					( ( false == is_null( $intScheduledBlackoutDateId ) ) ? ' AND id <> ' . ( int ) $intScheduledBlackoutDateId : '' ) . '
    					AND property_id = ' . ( int ) $intPropertyId . '
    					AND frequency_id = ' . ( int ) $intFrequencyId . '
    					AND schedule = \'' . $strSchedule . '\'
    					AND start_date = \'' . $strStartDate . '\'
    				LIMIT 1';

		return self::fetchScheduledBlackoutDate( $strSql, $objDatabase );
	}

	public static function fetchAllScheduledBlackoutDates( $objDatabase ) {

		$strSql = '	SELECT *
    				FROM
    					scheduled_blackout_dates';

		return self::fetchScheduledBlackoutDates( $strSql, $objDatabase );
	}

	public static function fetchScheduledBlackoutDatesDeatilsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						DISTINCT sbd.*
					FROM
						scheduled_blackout_dates sbd
					WHERE
						sbd.cid = ' . ( int ) $intCid . '
						AND sbd.property_id = ' . ( int ) $intPropertyId;

		return self::fetchScheduledBlackoutDates( $strSql, $objDatabase );
	}

	public static function fetchAllScheduledBlackoutDatesByPropertyIdsByCidForBulkEdit( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
				       DISTINCT ON ( sbd.start_date ,sbd.frequency_id, sbd.schedule, sbd.is_lease_start_date, sbd.is_lease_end_date, sbd.is_move_in, sbd.is_move_out, sbd.end_date ) sbd.*,
		 				(
			             SELECT
			                 count ( DISTINCT property_id )
			             FROM
			                 scheduled_blackout_dates bd
			             WHERE
			                 bd.start_date = sbd.start_date
			                 AND bd.schedule = sbd.schedule
			                 AND bd.cid = sbd.cid
							 AND bd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
			           ) AS properties_count
				   FROM
				       scheduled_blackout_dates sbd
				   WHERE
					   sbd.cid = ' . ( int ) $intCid . '
				       AND sbd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
				   GROUP BY
				       sbd.id,
				       sbd.cid,
				       sbd.start_date,
				       sbd.schedule';

		return self::fetchScheduledBlackoutDates( $strSql, $objDatabase );
	}

	public static function fetchScheduleBlackOutDatesbyUsingDetails( $arrintPropertyIds, $intCid, $strSchedule, $strStartDate, $intFrequencyId, $intIsMoveIn, $intIsMoveOut, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
				       DISTINCT ON (sbd.property_id) sbd.*
				  FROM
				      scheduled_blackout_dates sbd
				  WHERE
				      property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
				      AND cid = ' . ( int ) $intCid . '
				      AND schedule = \'' . ( string ) $strSchedule . '\'
				      AND start_date::date = \'' . ( string ) $strStartDate . '\'::date
				      AND frequency_id = ' . ( int ) $intFrequencyId . '
				      AND is_move_in = ' . ( int ) $intIsMoveIn . '
				      AND is_move_out = ' . ( int ) $intIsMoveOut;

		return self::fetchScheduledBlackoutDates( $strSql, $objDatabase );
	}

	public static function fetchScheduledBlackoutDateByFrequencyIdByScheduleByStartDateByMoveInAndOutByPropertyIdsByCid( $intFrequencyId, $strSchedule, $strStartDate, $intIsMoveIn, $intIsMoveOut, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					 	*
    				FROM
    					scheduled_blackout_dates
    				WHERE
    					cid = ' . ( int ) $intCid . '
    					AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
    					AND is_move_in = ' . ( int ) $intIsMoveIn . '
    					AND is_move_out = ' . ( int ) $intIsMoveOut . '
    					AND frequency_id = ' . ( int ) $intFrequencyId . '
    					AND schedule = \'' . ( string ) $strSchedule . '\'';
    		if( false == is_null( $strStartDate ) ) {
    			$strSql .= ' AND start_date =  \'' . ( string ) $strStartDate . '\'::date';
			}

		return self::fetchScheduledBlackoutDates( $strSql, $objDatabase );
	}

	public static function fetchScheduledBlackoutDatesHolidaysByFrequencyIdByPropertyIdByCid( $intFrequencyId, $intPropertyId, $intCid, $objDatabase, $intScheduledBlackoutdateId = NULL ) {

		$strSqlWhere = '';
		if( true == valId( $intScheduledBlackoutdateId ) ) {
			$strSqlWhere .= ' AND id NOT IN( ' . ( int ) $intScheduledBlackoutdateId . ' )';
		}

		$strSql = '	SELECT
						substring( schedule, 10 ) as holiday_dates
    				FROM
    					scheduled_blackout_dates
    				WHERE
    					cid = ' . ( int ) $intCid . '
	    				AND property_id = ' . ( int ) $intPropertyId . '
    					AND frequency_id = ' . ( int ) $intFrequencyId . '
    					AND is_amenity = 1' . $strSqlWhere;

		$arrstrHolidays = fetchData( $strSql, $objDatabase );

		$arrstrHolidayDates = [];

		if( true == valArr( $arrstrHolidays ) ) {
			foreach( $arrstrHolidays as $arrstrHoliday ) {
				$arrstrHolidayDates[] = $arrstrHoliday['holiday_dates'];
			}
		}

		return $arrstrHolidayDates;
	}

}
?>