<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetConditions
 * Do not add any new functions to this class.
 */
class CAssetConditions extends CBaseAssetConditions {

	public static function fetchAssetConditionsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_conditions ac
					WHERE
						ac.cid = ' . ( int ) $intCid . '
						AND ac.deleted_by IS NULL
						AND ac.deleted_on IS NULL
					ORDER BY
						ac.name';

		return self::fetchAssetConditions( $strSql, $objClientDatabase );
	}

	public static function fetchAssetConditionByIdByCid( $intAssetConditionId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_conditions ac
					WHERE
						ac.cid = ' . ( int ) $intCid . '
						AND ac.id = ' . ( int ) $intAssetConditionId . '
						AND ac.deleted_by IS NULL
					ORDER BY
						ac.name';

		return self::fetchAssetCondition( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledAssetConditionsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_conditions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND is_disabled = false
					ORDER BY
						name';

		return self::fetchAssetConditions( $strSql, $objClientDatabase );
	}
}
?>