<?php

class CGlHeaderStatusType extends CBaseGlHeaderStatusType {

	const POSTED		= 1;
	const TEMPORARY		= 2;
	const REVERSED		= 3;
	const DELETED		= 4;
	const ROUTED		= 5;
	const REJECTED		= 6;

	public static $c_arrintExcludedGlHeaderStatusTypes = [ self::TEMPORARY, self::DELETED, self::ROUTED, self::REJECTED ];

	public static $c_arrstrGlHeaderStatusTypes = [
		self::POSTED		=> 'Posted',
		self::TEMPORARY		=> 'Paused',
		self::REVERSED		=> 'Reversed',
		self::DELETED		=> 'Deleted',
		self::ROUTED		=> 'Routed',
		self::REJECTED		=> 'Rejected'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	/**
	 * Other functions
	 */

	public static function loadGlHeaderStatusTypeNameArray() {

		$arrstrGlHeaderStatusTypesNameArray = [];

		$arrstrGlHeaderStatusTypesNameArray[self::TEMPORARY]	= 'Temporary';
		$arrstrGlHeaderStatusTypesNameArray[self::POSTED]		= 'Posted';
		$arrstrGlHeaderStatusTypesNameArray[self::REVERSED]		= 'Reversed';
		$arrstrGlHeaderStatusTypesNameArray[self::DELETED]		= 'Deleted';
		$arrstrGlHeaderStatusTypesNameArray[self::ROUTED]		= 'Advance';

		return $arrstrGlHeaderStatusTypesNameArray;
	}

}
?>