<?php

class CBudgetAssumption extends CBaseBudgetAssumption {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetWorkbookAssumptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createBudgetAssumptionMonth() {

		$objBudgetAssumptionMonth = new CBudgetAssumptionMonth();
		$objBudgetAssumptionMonth->setCid( $this->getCid() );
		$objBudgetAssumptionMonth->setBudgetId( $this->getBudgetId() );
		$objBudgetAssumptionMonth->setBudgetAssumptionId( $this->getId() );

		return $objBudgetAssumptionMonth;
	}

}
?>