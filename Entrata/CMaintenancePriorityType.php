<?php

class CMaintenancePriorityType extends CBaseMaintenancePriorityType {

	const PREVENTATIVE	= 1;
	const LOW 			= 2;
	const MEDIUM		= 3;
	const HIGH			= 4;
	const EMERGENCY		= 5;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenancePriorityData() {
		if( isset( $this->m_arrmixMaintenancePriorityData ) )
			return $this->m_arrmixMaintenancePriorityData;

		$this->m_arrmixMaintenancePriorityData = [
			self::PREVENTATIVE	=> [
				'priority_colour'	=> 'blue',
				'priority_title'	=> __( 'PREVENTATIVE' )
			],
			self::LOW	=> [
				'priority_colour'	=> 'light-yellow',
				'priority_title'	=> __( 'LOW' )
			],
			self::MEDIUM	=> [
				'priority_colour'	=> 'yellow',
				'priority_title'	=> __( 'MEDIUM' )
			],
			self::HIGH	=> [
				'priority_colour'	=> 'light-red',
				'priority_title'	=> __( 'HIGH' )
			],
			self::EMERGENCY	=> [
				'priority_colour'	=> 'dark-red',
				'priority_title'	=> __( 'EMERGENCY' )
			]
		];
		return $this->m_arrmixMaintenancePriorityData;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>