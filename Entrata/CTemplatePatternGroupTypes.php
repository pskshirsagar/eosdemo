<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTemplatePatternGroupTypes
 * Do not add any new functions to this class.
 */

class CTemplatePatternGroupTypes extends CBaseTemplatePatternGroupTypes {

	public static function fetchTemplatePatternGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTemplatePatternGroupType', $objDatabase );
	}

	public static function fetchTemplatePatternGroupTypesDetails( $objDatabase ) {
		$strSql = 'SELECT * FROM template_pattern_group_types ORDER BY id';
		return self::fetchTemplatePatternGroupTypes( $strSql,  $objDatabase );
	}

	public static function fetchTemplatePatternsByPatternsGroupName( $strName, $objDatabase ) {
		$strSql = "SELECT * FROM template_pattern_group_types WHERE name like '" . $strName . "'";
		return self::fetchTemplatePatternGroupTypes( $strSql,  $objDatabase );
	}

}
?>