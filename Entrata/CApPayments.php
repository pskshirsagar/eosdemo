<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayments
 * Do not add any new functions to this class.
 */

class CApPayments extends CBaseApPayments {

	private static function createTempTableApPayments( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_payments_listing\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strJoin              = '';
		$strColumn            = '';
		$strCondition         = '';
		$strSubQueryCondition = '';
		$strPropertyCondition = '';

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valId( $objApTransactionsFilter->getCreatedBy() ) ) {
				$strCondition .= ' AND ap.created_by = ' . ( int ) $objApTransactionsFilter->getCreatedBy();
			}

			if( true == valArr( $objApTransactionsFilter->getBankAccountIds() ) ) {
				$strCondition .= ' AND ap.bank_account_id IN ( ' . implode( ',', $objApTransactionsFilter->getBankAccountIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApPayeeIds() ) ) {
				$strCondition .= ' AND ah.ap_payee_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApPaymentTypeId() ) ) {
				$strCondition .= ' AND ap.ap_payment_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPaymentTypeId() ) . ' )';
			}

			if( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId() ) {

				if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
				}

				if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
				}
			}

			if( true == valStr( $objApTransactionsFilter->getPaymentNumber() ) ) {
				$strCondition .= ' AND ap.payment_number ILIKE \'%' . addslashes( $objApTransactionsFilter->getPaymentNumber() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {
				$strCondition .= ' AND ap.payee_name ILIKE \'%' . addslashes( $objApTransactionsFilter->getPayeeName() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getVendorCode() ) ) {
				$strCondition .= ' AND apl.vendor_code ILIKE \'%' . addslashes( $objApTransactionsFilter->getVendorCode() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPostMonth() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostMonth() ) ) {
				$strCondition .= ' AND ah.post_month = \'' . addslashes( $objApTransactionsFilter->getPostMonth() ) . '\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && false == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateFrom() ) ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';

			} elseif( true == valStr( $objApTransactionsFilter->getDateTo() ) && false == valStr( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateTo() ) ) ) {

				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';

			} elseif( true == valStr( $objApTransactionsFilter->getDateFrom() ) && true == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateFrom() ) ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateTo() ) ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';
				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';
			}

			if( true == is_numeric( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
				$strCondition .= ' AND ap.payment_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
			}

			if( true == is_numeric( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
				$strCondition .= ' AND ap.payment_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
			}

			$strCondition .= ' AND ap.is_unclaimed_property = ' . ( ( true == ( boolean ) $objApTransactionsFilter->getIsUnclaimedProperty() ) ? 'true' : 'false' );

			if( true == $objApTransactionsFilter->getIsUnclaimedProperty() ) {

				$strSubQueryCondition = ' AND sub.id NOT IN (	SELECT
																	ad.unclaimed_ap_payment_id
																FROM
																	ap_headers ah
																	JOIN ap_details ad ON ad.cid = ah.cid AND ad.ap_header_id = ah.id
																WHERE
																	ah.cid = ' . ( int ) $intCid . '
																	AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_INVOICE . '
																	AND ah.reversal_ap_header_id IS NULL
																	AND ad.unclaimed_ap_payment_id IS NOT NULL
															)';
			}

			if( true == valArr( $objApTransactionsFilter->getPropertyGroupIds() ) ) {
				$strPropertyCondition .= '
					AND EXISTS (
						SELECT
							NULL
						FROM
							properties AS p
							JOIN property_group_associations AS pga ON pga.cid = p.cid AND p.id = pga.property_id
							JOIN property_groups AS pg ON pg.cid = pga.cid AND pga.property_group_id = pg.id
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND pga.property_id = ad.property_id
							AND p.is_disabled = 0
							AND pg.id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyGroupIds() ) . ' )
							AND pg.deleted_by IS NULL
							AND pg.deleted_on IS NULL
					) ';
			} else {
				$strPropertyCondition .= ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			$strLateralWhereCondition = '';
			if( true == $boolIsFromBulkVoidPayments ) {

				if( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId()[0] ) {

					if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
						$strCondition .= ' AND ap.payment_number::INT >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
					}

					if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
						$strCondition .= ' AND ap.payment_number::INT <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
					}
				}
				$strLateralWhereCondition .= ' AND ah_charge.reversal_ap_header_id IS NULL AND gd.gl_reconciliation_id IS NULL AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED;
			}

			if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

				if( ( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				      && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) )
				    && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strSubQueryCondition .= ' AND ( gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::RECONCILED . ' OR gl_reconciliation_status_type_id IS NULL )';

				} elseif( ( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				            && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) ) {
					// true always
					$strSubQueryCondition = '';

				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND ( gl_reconciliation_id IS NULL OR ( gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . ') )';

				} elseif( false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED;
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND ( gl_reconciliation_id IS NOT NULL OR ( gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . ' ) )';
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED;
				} else {

					if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
						$strSubQueryCondition .= ' AND gl_reconciliation_id IS NULL';
					}

					if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
					    && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

						$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::PAUSED;
					}

				}

				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strCondition .= ' AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strSubQueryCondition .= ' OR payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} else {
					$strCondition .= ' AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED;
				}
			}

			$strJoin = '
				LEFT JOIN LATERAL(
					SELECT
						gh.gl_transaction_type_id AS gh_gl_transaction_type_id,
						adr.reimbursement_ap_detail_id,
						CASE
							WHEN ( adr.reimbursement_ap_detail_id IS NOT NULL OR ( ' . CApTransactionType::STANDARD_REIMBURSEMENT . ' = ad_charge.ap_transaction_type_id AND ' . CReimbursementMethod::NO_REIMBURSEMENT . ' = ad_charge.reimbursement_method_id ) ) THEN \'true\'
							ELSE \'false\'
						END AS is_reimbursement,
						gd.gl_reconciliation_id,
						gr.gl_reconciliation_status_type_id
					FROM
						ap_details ad
						LEFT JOIN ap_allocations aa ON aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id
						LEFT JOIN gl_headers gh ON gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . '
						LEFT JOIN gl_details gd ON gd.cid = gh.cid AND gd.gl_header_id = gh.id 
							AND gd.amount =
								CASE
										WHEN aa.origin_ap_allocation_id IS NOT NULL 
										OR aa.lump_ap_header_id IS NOT NULL THEN
											aa.allocation_amount * - 1 ELSE aa.allocation_amount 
										END 
							AND gd.property_id =
										CASE
												WHEN ba.reimbursed_property_id <> aa.property_id THEN
												ba.reimbursed_property_id ELSE aa.property_id 
											END 
						LEFT JOIN gl_reconciliations gr ON gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id
						LEFT JOIN ap_details ad_charge ON ad_charge.cid = aa.cid AND ad_charge.id = aa.charge_ap_detail_id
						LEFT JOIN ap_detail_reimbursements adr ON ( ad_charge.cid = adr.cid AND ad_charge.id = adr.original_ap_detail_id AND adr.is_deleted = false ) 
					WHERE
						ah.cid = ad.cid
						AND ah.id = ad.ap_header_id
						AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
						AND ah.post_month = ad.post_month
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
						' . $strLateralWhereCondition . '
					LIMIT 1
				) AS ad_sub ON TRUE';

			$strColumn = '
				,CASE
					WHEN ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_ACH . ' OR ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_CHECK . ' THEN (
						CASE
							WHEN ad_sub.gl_reconciliation_id IS NOT NULL AND ad_sub.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
							WHEN ad_sub.gl_reconciliation_id IS NOT NULL AND ad_sub.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::RECONCILED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::PENDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PENDING] . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::RECEIVED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECEIVED] . ' [' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . ']' . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDING] . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURING] . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURED] . '\'
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
							WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
						END )
					WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
					WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
					ELSE CASE
							WHEN ad_sub.gl_reconciliation_id IS NOT NULL AND ad_sub.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
							WHEN ad_sub.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
							ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
						END
				END AS payment_status_type_name,
				ad_sub.gl_reconciliation_status_type_id,
				ad_sub.gl_reconciliation_id';

		}

		$strSql = '
			DROP TABLE IF EXISTS pg_temp.ah1;
			CREATE TEMP TABLE ah1 AS (
				SELECT
					cid,
					ap_payment_id,
					property_ids,
					CASE WHEN 1 = icount( property_ids ) THEN property_name ELSE \'Multiple\' END AS property_name,
					id
				FROM(
					SELECT
						ah.cid,
						ah.ap_payment_id,
						MAX( LEAST( ah.id, ah.reversal_ap_header_id ) ) AS id
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payment_id IS NOT NULL
					GROUP BY
						ah.cid, ah.ap_payment_id
				) ah
				JOIN LATERAL (
					SELECT
						ARRAY_AGG ( DISTINCT ad.property_id ) AS property_ids,
						MAX ( P.property_name ) AS property_name 
					FROM
						ap_details ad
						JOIN properties p ON ad.cid = p.cid AND ad.property_id = p.id
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id = ah.id
						AND ad.deleted_on IS NULL
						' . $strPropertyCondition . '
					GROUP BY
						ad.ap_header_id
				) ad_count ON TRUE
			);
			ANALYZE ah1;

			DROP TABLE IF EXISTS pg_temp.temp_ap_payments_listing;
			CREATE TABLE pg_temp.temp_ap_payments_listing AS (
					SELECT
						sub.*
					FROM
						(
							SELECT
								ap.id,
								CASE
									WHEN ( ad_sub.reimbursement_ap_detail_id IS NOT NULL OR ad_sub.is_reimbursement = true ) THEN \'true\'
									ELSE \'false\'
								END AS is_reimbursement_invoice,
								ba.account_name,
								ap.bank_account_id,
								apt.name AS ap_payment_type,
								apt.id AS ap_payment_type_id,
								ap.payment_date,
								ap.cid,
								ap.payment_amount,
								func_format_refund_customer_names( ap.payee_name ) AS payee_name,
								ap.payment_number,
								ah.post_month,
								ah.id AS ap_header_id,
								ah.is_initial_import::INT is_initial_import,
								ah1.property_name,
								ah1.property_ids[1] AS property_id,
								ap.payment_status_type_id,
								apl.vendor_code
								' . $strColumn . '
							FROM
								ap_payments ap
								JOIN ah1 ON ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id
								JOIN ap_headers ah ON ah.cid = ah1.cid AND ah.id = ah1.id
								JOIN ap_payment_types apt ON apt.id = ap.ap_payment_type_id
								JOIN bank_accounts ba ON ba.cid = ap.cid AND ba.id = ap.bank_account_id
								JOIN ap_payees ap1 ON ap1.cid = ah.cid AND ap1.id = ah.ap_payee_id
								JOIN ap_payee_locations apl ON apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id
								' . $strJoin . '
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.ap_payment_type_id IN( ' . implode( ',', $arrintApPaymentTypeIds ) . ' ) ' .
		          $strCondition . '
						) sub
					WHERE
						sub.cid = ' . ( int ) $intCid . $strSubQueryCondition . '
			);
			ANALYZE pg_temp.temp_ap_payments_listing;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	public static function fetchPaginatedApPaymentsFiltered( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false, $intPageNo, $intPageSize ) {

		if( false == valArr( $arrintApPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		self::createTempTableApPayments( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter, $boolIsFromBulkVoidPayments );

		$strOrderBy   = 'id DESC';
		$arrstrSortBy = [ 'payment_number', 'payment_date', 'post_month', 'payee_name', 'vendor_code', 'property_name', 'ap_payment_type', 'account_name', 'payment_amount', 'payment_status_type_name' ];

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {
			$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection();
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT * FROM pg_temp.temp_ap_payments_listing ORDER BY ' . addslashes( $strOrderBy ) . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCountApPaymentsFiltered( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false ) {

		if( false == valArr( $arrintApPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		self::createTempTableApPayments( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter, $boolIsFromBulkVoidPayments );

		$strSql = 'SELECT count( distinct id ) FROM pg_temp.temp_ap_payments_listing;';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	/**
	 * @param      $intCid
	 * @param      $arrintApPaymentTypeIds
	 * @param      $arrintPropertyIds
	 * @param      $objClientDatabase
	 * @param null $objApTransactionsFilter
	 * @param bool $boolIsFromBulkVoidPayments
	 * @return bool|void
	 * @deprecated
	 */
 	private static function createTempTableApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false, $boolShowDisabledData = false ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_payments_listing\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strJoin              = '';
		$strColumn            = '';
		$strCondition         = '';
		$strSubQueryCondition = '';
		$strPropertyCondition = '';

		$strOrderBy   = 'sub.id DESC';
		$arrstrSortBy = [ 'payment_number', 'payment_date', 'post_month', 'payee_name', 'vendor_code', 'property_name', 'ap_payment_type', 'account_name', 'payment_amount', 'payment_status_type_name' ];

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {
			$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection();
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valId( $objApTransactionsFilter->getCreatedBy() ) ) {
				$strCondition .= ' AND ap.created_by = ' . ( int ) $objApTransactionsFilter->getCreatedBy();
			}

			if( true == valArr( $objApTransactionsFilter->getBankAccountIds() ) ) {
				$strCondition .= ' AND ap.bank_account_id IN ( ' . implode( ',', $objApTransactionsFilter->getBankAccountIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApPayeeIds() ) ) {
				$strCondition .= ' AND ah.ap_payee_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApPaymentTypeId() ) ) {
				$strCondition .= ' AND ap.ap_payment_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPaymentTypeId() ) . ' )';
			}

			if( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId() ) {

				if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
				}

				if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
				}
			}

			if( true == valStr( $objApTransactionsFilter->getPaymentNumber() ) ) {
				$strCondition .= ' AND ap.payment_number ILIKE \'%' . addslashes( $objApTransactionsFilter->getPaymentNumber() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {
				$strCondition .= ' AND ap.payee_name ILIKE \'%' . addslashes( $objApTransactionsFilter->getPayeeName() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getVendorCode() ) ) {
				$strCondition .= ' AND apl.vendor_code ILIKE \'%' . addslashes( $objApTransactionsFilter->getVendorCode() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPostMonth() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostMonth() ) ) {
				$strCondition .= ' AND ah.post_month = \'' . addslashes( $objApTransactionsFilter->getPostMonth() ) . '\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && false == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateFrom() ) ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';

			} elseif( true == valStr( $objApTransactionsFilter->getDateTo() ) && false == valStr( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateTo() ) ) ) {

				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';

			} elseif( true == valStr( $objApTransactionsFilter->getDateFrom() ) && true == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateFrom() ) ) && true == CValidation::validateDate( ltrim( $objApTransactionsFilter->getDateTo() ) ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';
				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';
			}

			if( true == is_numeric( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
				$strCondition .= ' AND ap.payment_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
			}

			if( true == is_numeric( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
				$strCondition .= ' AND ap.payment_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
			}

			$strCondition .= ' AND ap.is_unclaimed_property = ' . ( ( true == ( boolean ) $objApTransactionsFilter->getIsUnclaimedProperty() ) ? 'true' : 'false' );

			if( true == $objApTransactionsFilter->getIsUnclaimedProperty() ) {

				$strSubQueryCondition = ' AND sub.id NOT IN (	SELECT
																	ad.unclaimed_ap_payment_id
																FROM
																	ap_headers ah
																	JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
																WHERE
																	ah.cid = ' . ( int ) $intCid . '
																	AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_INVOICE . '
																	AND ah.reversal_ap_header_id IS NULL
																	AND ad.unclaimed_ap_payment_id IS NOT NULL
															)';
			}

			if( true == valArr( $objApTransactionsFilter->getPropertyGroupIds() ) ) {
				$strPropertyCondition .= ' AND ad.property_id IN ( SELECT
																pga.property_id
															FROM
																properties AS p
																JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
															WHERE
																p.cid = ' . ( int ) $intCid . '
																' . ( ( false == $boolShowDisabledData ) ? ' AND p.is_disabled = 0 ' : '' ) . '
																AND pg.id IN ( SELECT
																pga.property_id
															FROM
																properties AS p
																JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
															WHERE
																p.cid = ' . ( int ) $intCid . '
																' . ( ( false == $boolShowDisabledData ) ? ' AND p.is_disabled = 0 ' : '' ) . '
																AND pg.id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyGroupIds() ) . ' )
																AND pg.deleted_by IS NULL
																AND pg.deleted_on IS NULL
															 )
															AND pg.deleted_by IS NULL
															AND pg.deleted_on IS NULL
															) ';
			} else {
				$strPropertyCondition .= ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			$strJoin   = 'LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
								LEFT JOIN ap_details ad_charge ON ( ad_charge.cid = aa.cid AND ad_charge.id = aa.charge_ap_detail_id )
								LEFT JOIN ap_detail_reimbursements adr ON ( ad_charge.cid = adr.cid AND ad_charge.id = adr.original_ap_detail_id AND adr.is_deleted = false )
								LEFT JOIN ap_headers ah_charge ON ( ah_charge.cid = ad_charge.cid AND ah_charge.id = ad_charge.ap_header_id )
								LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
								LEFT JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id
														AND gd.amount = CASE
																		WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL THEN
																			aa.allocation_amount * - 1
																		ELSE
																			aa.allocation_amount
																	END
													AND gd.property_id = CASE
																			WHEN ba.reimbursed_property_id <> aa.property_id THEN
																				ba.reimbursed_property_id
																			ELSE
																				aa.property_id
																			END
														)
								LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id )';
			$strColumn = ' ,CASE
								WHEN ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_ACH . ' OR ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_CHECK . ' THEN (
									CASE
										WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
										WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::RECONCILED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::PENDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PENDING] . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::RECEIVED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECEIVED] . ' [' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . ']' . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDING] . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURING] . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURED] . '\'
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
										WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
									END )
								WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
								WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
								ELSE CASE
										WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
										WHEN gd.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
										ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
									END
							END AS payment_status_type_name,
								MAX( ah_charge.ap_header_sub_type_id ) OVER ( PARTITION BY ap.id ORDER BY ap.id ) as ap_header_sub_type_id,
								gr.gl_reconciliation_status_type_id';

			if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				    || true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				    || true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strColumn .= ', first_value ( gd.gl_reconciliation_id ) OVER ( PARTITION BY ap.id ORDER BY gd.gl_reconciliation_id IS NULL, ap.id ) AS gl_reconciliation_id';
				}

				if( false == $objApTransactionsFilter->getIsInitialImport() ) {
					$strCondition         .= ' AND NOT ah.is_initial_import ';
					$strGlHeaderCondition = ' AND gh.gl_transaction_type_id = ' . ( int ) CGlTransactionType::AP_ALLOCATION;
				} else {
					$strCondition         .= ' AND ah.is_initial_import ';
					$strGlHeaderCondition = ' AND ( gh.id IS NULL OR gh.gl_transaction_type_id = ' . ( int ) CGlTransactionType::AP_ALLOCATION . ' ) ';
				}

				if( ( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				      && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) )
				    && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strCondition         .= $strGlHeaderCondition;
					$strSubQueryCondition .= ' AND ( gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::RECONCILED . ' OR gl_reconciliation_status_type_id IS NULL )';

				} elseif( ( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				            && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) ) {

					$strCondition .= $strGlHeaderCondition;

				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition = ' AND ( gl_reconciliation_id IS NULL OR ( gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . ') )';

				} elseif( false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED;
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				          && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND ( gl_reconciliation_id IS NOT NULL OR ( gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . ' ) )';
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

					$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED;
				} else {

					if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
						$strSubQueryCondition .= ' AND gl_reconciliation_id IS NULL';
					}

					if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
					    && false == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

						$strSubQueryCondition .= ' AND gl_reconciliation_id IS NOT NULL AND gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::PAUSED;
					}

					if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
					    || true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

						$strCondition .= $strGlHeaderCondition;
					}

				}

				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strCondition .= ' AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strSubQueryCondition .= ' OR payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} else {
					$strCondition .= ' AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED;
				}
			}
		}

		if( true == $boolIsFromBulkVoidPayments ) {

			if( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId()[0] ) {

				if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
				}

				if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::INT <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
				}
			}

			$strCondition .= ' AND ah_charge.reversal_ap_header_id IS NULL AND gd.gl_reconciliation_id IS NULL AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED;
		}

		$strSql = 'CREATE temp TABLE ah1 AS ( SELECT
                 cid,
                 ap_payment_id,
                 property_name,
                 property_ids,
                 MAX ( CASE
                         WHEN reversal_ap_header_id IS NOT NULL AND id = ap_header_max THEN NULL
                         ELSE id
                        END ) AS id
             FROM
                 (
                   SELECT
                       ah.id,
                       MAX ( ah.id ) over ( PARTITION BY ah.cid, ah.ap_payment_id ) ap_header_max,
                       ah.ap_payment_id,
                       ah.reversal_ap_header_id,
                       ah.cid,
                       CASE WHEN 1 = icount( ad_count.property_ids )
                               THEN ad_count.property_name
                            ELSE \'Multiple\'
                       END AS property_name,     
                       ad_count.property_ids 
                   FROM
                       ap_headers ah
                       JOIN LATERAL (
                            SELECT 
                                ad.ap_header_id,
                                array_agg( DISTINCT ad.property_id ) AS property_ids,
                                MAX( p.property_name ) AS property_name
                            FROM
                                ap_details ad
                                JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
                            WHERE
                                 ad.cid = ' . ( int ) $intCid . '
                                 AND ad.ap_header_id = ah.id
                                 AND ad.deleted_on IS NULL ' .
		          $strPropertyCondition . '
                            GROUP BY
                                1
                        ) ad_count ON TRUE
                   WHERE
                       ah.cid = ' . ( int ) $intCid . '
                       AND ah.ap_payment_id IS NOT NULL
                 ) sub1
             GROUP BY
                 1, 2, 3, 4 );

			ANALYZE ah1;
			DROP TABLE IF EXISTS pg_temp.temp_ap_payments_listing;
			CREATE TABLE pg_temp.temp_ap_payments_listing AS (
					SELECT
						sub.*
					FROM
						(
							SELECT
								DISTINCT ap.id,
								CASE
									WHEN ( adr.id IS NOT NULL
											OR ( ( ' . CApTransactionType::STANDARD_REIMBURSEMENT . ' = ad_charge.ap_transaction_type_id AND ' . CReimbursementMethod::NO_REIMBURSEMENT . ' = ad_charge.reimbursement_method_id ) OR ' . CApTransactionType::STANDARD_REIMBURSEMENT_NEW . ' = ad_charge.ap_transaction_type_id )
											OR ( ( MAX( CASE WHEN ad_charge.reimbursement_method_id IN ( ' . CReimbursementMethod::STANDARD_REIMBURSEMENT_DO_NOT_USE_SUBLEDGERS . ', ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . ' ) THEN 1 ELSE 0 END) OVER ( PARTITION BY ad_charge.ap_header_id ) ) = 1 ) )
									THEN \'true\'
									ELSE \'false\'
								END AS is_reimbursement_invoice,
								ba.account_name,
								ap.bank_account_id,
								apt.name AS ap_payment_type,
								apt.id AS ap_payment_type_id,
								ap.payment_date,
								ap.cid,
								ap.payment_amount,
								func_format_refund_customer_names( ap.payee_name ) AS payee_name,
								ap.payment_number,
								ah.post_month,
								ah.id AS ap_header_id,
								CASE WHEN ah.is_initial_import THEN 1 ELSE 0 END AS is_initial_import,
								ah1.property_name,
								ar.ap_payment_type_id as remittance_payment_type_id,
								MAX ( ad_charge.property_id ) OVER ( PARTITION BY ah1.ap_payment_id ORDER BY ah1.ap_payment_id ) as property_id,
								ap.payment_status_type_id,
								apl.vendor_code
								' . $strColumn . '
							FROM
								ap_payments ap
								LEFT JOIN  ah1 ON ( ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id )
								LEFT JOIN ap_headers ah ON ( ah.cid = ah1.cid AND ah.id = ah1.id )
								LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
								LEFT JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
								LEFT JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
								LEFT JOIN ap_payees ap1 ON ( ap1.cid = ah.cid AND ap1.id = ah.ap_payee_id )
								LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
								JOIN ap_remittances ar ON ( ar.cid = ap.cid AND ar.id = ah.ap_remittance_id )
								' . $strJoin . '
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.ap_payment_type_id IN( ' . implode( ',', $arrintApPaymentTypeIds ) . ' ) ' .
		          $strCondition . '
						) sub
					WHERE
						sub.cid = ' . ( int ) $intCid . $strSubQueryCondition . '
					ORDER BY
					' . addslashes( $strOrderBy ) . '
			);
			ANALYZE pg_temp.temp_ap_payments_listing;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	/**
	 * @param      $intPageNo
	 * @param      $intPageSize
	 * @param      $intCid
	 * @param      $arrintApPaymentTypeIds
	 * @param      $arrintPropertyIds
	 * @param      $objClientDatabase
	 * @param null $objApTransactionsFilter
	 * @param bool $boolIsFromBulkVoidPayments
	 * @return array|null
	 * @deprecated
	 */
	public static function fetchPaginatedApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intPageNo, $intPageSize, $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false, $boolShowDisabledData= false ) {
		if( false == valArr( $arrintApPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		self::createTempTableApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter, $boolIsFromBulkVoidPayments, $boolShowDisabledData );

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT * FROM pg_temp.temp_ap_payments_listing OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	/**
	 * @param      $intCid
	 * @param      $arrintApPaymentTypeIds
	 * @param      $arrintPropertyIds
	 * @param      $objClientDatabase
	 * @param null $objApTransactionsFilter
	 * @param bool $boolIsFromBulkVoidPayments
	 * @return |null
	 * @deprecated
	 */
	public static function fetchTotalPaginatedApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsFromBulkVoidPayments = false ) {

		if( false == valArr( $arrintApPaymentTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		self::createTempTableApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intCid, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionsFilter, $boolIsFromBulkVoidPayments );

		$strSql = 'SELECT count( distinct id ) FROM pg_temp.temp_ap_payments_listing;';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApPaymentsForCheckPrintingByPaymentNumbersByCid( $arrmixBankAccountIdWiseCheckNumbers, $intCid, $objClientDatabase ) {

		// $arrmixBankAccountIdWiseCheckNumbers  array should have following format :
		// $arrmixBankAccountIdWiseCheckNumbers = array(
		// 													'bank_account_id' => array( 'check_number_1','check_number_2',....  ),
		// 													.
		// 													.
		// 												);

		if( false == valArr( $arrmixBankAccountIdWiseCheckNumbers ) ) {
			return NULL;
		}

		$arrstrSqls = [];

		foreach( $arrmixBankAccountIdWiseCheckNumbers as $intBankAccountId => $arrintCheckNumbers ) {

			if( false == valArr( $arrintCheckNumbers ) ) {
				continue;
			}

			$arrstrSqls[] = 'SELECT
								ap.id,
								ap.payment_number
							FROM
								ap_payments ap
								JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ' )
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.ap_payment_type_id = ' . CApPaymentType::CHECK . '
								AND ap.bank_account_id = ' . ( int ) $intBankAccountId . '
								AND	ap.payment_number :: Integer IN ( ' . implode( ',', $arrintCheckNumbers ) . ' )
								AND ap.payment_status_type_id != ' . CPaymentStatusType::VOIDED;
		}

		$strSql = ( true == valArr( $arrstrSqls ) ) ? implode( ' UNION ', $arrstrSqls ) : NULL;

		$strSql .= ' ORDER BY id';

		if( false == is_null( $strSql ) ) {
			return fetchData( $strSql, $objClientDatabase );
		}

		return NULL;
	}

	public static function fetchManualApPaymentByBankAccountIdByCheckNumberByCid( $intBankAccountId, $intCheckNumber, $intCid, $objClientDatabase ) {

		if( false == valId( $intBankAccountId ) || false == is_numeric( $intCheckNumber ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_quick_check = 1
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND payment_number :: Integer = ' . ( int ) $intCheckNumber . '
						AND payment_status_type_id = ' . CPaymentStatusType::RECEIVED . '
						AND ap_payment_type_id = ' . CApPaymentType::CHECK . '
						AND is_reversed = 0
					LIMIT 1';

		return self::fetchApPayment( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByCidByApHeaderId( $intCid, $intApHeaderId, $objClientDatabase, $boolCheckUnclaimed = false ) {

		if( true == $boolCheckUnclaimed ) {

			$strSqlCond = 'AND ap.is_unclaimed_property = true
						   AND ah.is_posted = true';
		} else {

			$strSqlCond = 'AND ah.reversal_ap_header_id IS NULL
						   AND ap.payment_status_type_id <> ' . CPaymentStatusType::VOIDED . '';
		}

		$strSql = 'SELECT
						ap.*,
						apt.name as ap_payment_type_name
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ap.id = ah.ap_payment_id AND ap.cid = ah.cid )
						LEFT JOIN ap_payment_types apt ON ( ap.ap_payment_type_id = apt.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND	ah.ap_payment_id IS NOT NULL
						AND (
							LOWER( TRIM( ah.header_number ) ) LIKE \'' . $intApHeaderId . '\'
							OR LOWER( TRIM( ah.header_number ) ) LIKE \'%,' . $intApHeaderId . '\'
							OR LOWER( TRIM( ah.header_number ) ) LIKE \'' . $intApHeaderId . ',%\'
							OR LOWER( TRIM( ah.header_number ) ) LIKE \'%,' . $intApHeaderId . ',%\'
						)' . $strSqlCond;

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchCountOfReconciledApPaymentsByApPaymentIdsByGlTransactionTypeIdByCid( $arrintApPaymentIds, $intGlTransactionTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strCondition = NULL;

		if( CGlTransactionType::AP_ALLOCATION == $intGlTransactionTypeId ) {
			$strCondition .= ' AND gtt.id = ' . CGlTransactionType::AP_ALLOCATION . ' ';
		} else {
			$strCondition .= ' AND gtt.id  IN ( ' . CGlTransactionType::AP_ALLOCATION . ', ' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ' ) ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ap.id, gh.gl_transaction_type_id )
						ap.id AS ap_payment_id
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
						JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
				 		JOIN gl_transaction_types gtt ON ( gtt.id = gh.gl_transaction_type_id )
					WHERE
						ap.id IN( ' . implode( ',', $arrintApPaymentIds ) . ' )
						AND gh.cid = ' . ( int ) $intCid . '
						AND gd.gl_reconciliation_id IS NOT NULL ' . $strCondition . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
					ORDER BY
						ap.id';

		$arrmixResult = fetchData( $strSql, $objClientDatabase );

		return \Psi\Libraries\UtilFunctions\count( $arrmixResult );
	}

	public static function fetchSimpleApPaymentByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*,
						func_format_refund_customer_names( payee_name ) AS formatted_payee_name
					FROM
						ap_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId;

		return self::fetchApPayment( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByAccountingBatchIdByCid( $intAccountingExportBatchId, $intCid, $objClientDatabase, $objAccountingExportBatch = NULL ) {

		$strSelect = $strJoin = $strWhere = '';
		if( false != valObj( $objAccountingExportBatch, 'CAccountingExportBatch' ) && false != valId( $objAccountingExportBatch->getTransmissionVendorId() ) && CTransmissionVendor::PP_EXPORT_POSPAYEXTRACTCUSTOM == $objAccountingExportBatch->getTransmissionVendorId() ) {
			$strSelect = ' ,COALESCE(ca.street_line1, apr.street_line1, apl.street_line1) AS street_line1,
						COALESCE(ca.street_line2, apr.street_line2, apl.street_line2) AS street_line2,
						COALESCE(ca.street_line3, apr.street_line3, apl.street_line3) AS street_line3,
						COALESCE(ca.street_line1, apr.street_line1, apl.street_line1) AS street_line1,
						COALESCE(ca.city, apr.city, apl.city) AS city,
						COALESCE(ca.postal_code, apr.postal_code, apl.postal_code) AS postal_code,
						COALESCE(ca.state_code, apr.state_code, apl.state_code) AS state_code,
						COALESCE(ca.country_code, apr.country_code, apl.country_code) AS country_code, 
						ap1.secondary_number,
						ap1.details as ap_payee_details';
			$strJoin = ' LEFT JOIN ap_remittances apr ON ( ap.cid = apr.cid AND apr.ap_payee_id = ah.ap_payee_id AND apr.id = ah.ap_remittance_id )
			LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid and apl.ap_payee_id = ah.ap_payee_id )
			LEFT JOIN lease_customers lc ON ( lc.cid = ah.cid AND lc.id = ah.lease_customer_id )
			LEFT JOIN customer_addresses ca ON ( ca.cid = lc.cid AND ca.customer_id = lc.customer_id AND ca.address_type_id = ' . CAddressType::FORWARDING . ' )';
			if( false != valId( $objAccountingExportBatch->getDetails()->check_number_range_from ) && false != valId( $objAccountingExportBatch->getDetails()->check_number_range_to ) ) {
				$strWhere = ' AND ap.payment_number::int >= ' . $objAccountingExportBatch->getDetails()->check_number_range_from . '
						AND ap.payment_number::int <= ' . $objAccountingExportBatch->getDetails()->check_number_range_to . '
						AND ap.ap_payment_type_id = ' . CApPaymentType::CHECK . '';
			}
		}
		$strSql = 'SELECT
						DISTINCT ON (ap.id,ah.id)
						ap.*';
		$strSql .= $strSelect;
		$strSql .= ' ,func_format_refund_customer_names( ap.payee_name, true ) AS formatted_payee_name,
						ah.gl_transaction_type_id,
						ah.id AS parent_ap_header_id,
						ah.reversal_ap_header_id,
						ah.header_number,
						ah.post_date,
						ah.post_month,
						ah.ap_payee_id,
						ah.ap_payee_location_id,
						p.lookup_code,
						p.property_name
					FROM
						ap_payments ap
						LEFT JOIN ap_headers ah ON ( ap.id = ah.ap_payment_id AND ah.cid = ap.cid AND ah.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN ap_details ad ON (ad.ap_header_id = ah.id AND ah.cid = ap.cid AND ad.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN ap_payees ap1 ON (ap1.id = ah.ap_payee_id AND ah.cid = ap1.cid AND ap1.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN properties p ON (p.id = ad.property_id AND ah.cid = p.cid AND p.cid = ' . ( int ) $intCid . ' )';
		$strSql .= $strJoin;
		$strSql .= ' WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ah.accounting_export_batch_id = ' . ( int ) $intAccountingExportBatchId;
		$strSql .= $strWhere;

		return self::fetchApPayments( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPaymentsCountByBankAccountIdByApPaymentTypesByPaymentNumberByCid( $intBankAccountId, $arrintApPaymentTypeIds, $strPaymentNumber, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						COUNT( DISTINCT id ) AS count
					FROM
						ap_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND payment_number = \'' . $strPaymentNumber . '\'
						AND ap_payment_type_id IN ( ' . implode( ',', $arrintApPaymentTypeIds ) . ' )';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCustomApPaymentsByBankAccountIdsAndByPaymentNumbersByApPaymentTypeIdsByCid( $arrmixPaymentNumbersByBankAccountId, $arrintApPaymentTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrmixPaymentNumbersByBankAccountId ) || false == valArr( $arrintApPaymentTypeIds ) ) {
			return NULL;
		}

		$strConditions = [];

		foreach( $arrmixPaymentNumbersByBankAccountId as $intBankAccountId => $arrstrPaymentNumbers ) {
			$strConditions[] = '( bank_account_id ) = ' . ( int ) $intBankAccountId . ' AND payment_number = ANY ( ARRAY [ \'' . implode( '\',\'', $arrstrPaymentNumbers ) . '\' ] )';
		}

		$strCondition = implode( ' OR ', $strConditions );

		$strSql = 'SELECT
						*
					FROM
						ap_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payment_type_id IN ( ' . implode( ',', $arrintApPaymentTypeIds ) . ' )
						AND ( ' . $strCondition . ' )
					ORDER BY
						payment_number::INT';

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsForPrintingByIdsByCid( $arrmixParameters ) {

		$boolIsCheckCopyDocument = getArrayElementByKey( 'is_check_copy_document', $arrmixParameters );

		$intCid                          = getArrayElementByKey( 'client_id', $arrmixParameters );
		$intApPaymentPrintingOrderTypeId = getArrayElementByKey( 'ap_payment_printing_order_type_id', $arrmixParameters );

		$objClientDatabase = getArrayElementByKey( 'client_database', $arrmixParameters );

		$arrintApPaymentIds = getArrayElementByKey( 'ap_payment_ids', $arrmixParameters );

		$strOrderBy = 'id ';

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		if( true == $boolIsCheckCopyDocument ) {
			$strWhereCondition = 'AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECEIVED . ', ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::VOIDED . ' )';
		} else {
			$strWhereCondition = 'AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECEIVED . ', ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::CAPTURING . ' )';
		}

		if( NULL != $intApPaymentPrintingOrderTypeId ) {

			if( CApPaymentPrintingOrderType::PROPERTY_AND_VENDOR == $intApPaymentPrintingOrderTypeId ) {
				$strOrderBy = ' p.property_name NULLS LAST,
								formatted_payee_name ';

			} elseif( CApPaymentPrintingOrderType::VENDOR_AND_PROPERTY == $intApPaymentPrintingOrderTypeId ) {
				$strOrderBy = ' formatted_payee_name,
								p.property_name NULLS LAST ';

			} elseif( CApPaymentPrintingOrderType::BANK_ACCOUNT_AND_VENDOR == $intApPaymentPrintingOrderTypeId ) {
				$strOrderBy = ' ba.account_name,
								formatted_payee_name ';

			} elseif( CApPaymentPrintingOrderType::BANK_ACCOUNT_AND_PROPERTY == $intApPaymentPrintingOrderTypeId ) {
				$strOrderBy = ' ba.account_name,
								p.property_name NULLS LAST ';

			} elseif( CApPaymentPrintingOrderType::PROPERTY_AND_BUILDING_UNIT_AND_VENDOR == $intApPaymentPrintingOrderTypeId ) {
				$strOrderBy = ' p.property_name NULLS LAST,
								CASE
									WHEN pb.building_name IS NULL AND pu.unit_number IS NULL THEN 1
									ELSE 0
								END ASC,
								SUBSTRING( pb.building_name, \'\' ) ASC NULLS FIRST,
								SUBSTRING( pb.building_name, \'[^0-9_].*$\' ) ASC NULLS FIRST,
								( SUBSTRING( pb.building_name, \'^[0-9]+\' ) )::NUMERIC ASC NULLS FIRST,
								LENGTH ( pu.unit_number ) ASC NULLS FIRST,
								SUBSTRING( pu.unit_number FROM \'^\\d* *(.*?)(\\d+)?$\' ) ASC NULLS FIRST,
								pu.unit_number ASC NULLS FIRST,
								formatted_payee_name ';
			}
		}

		$strSql = 'SELECT
					    ap.*,
					    func_format_refund_customer_names ( ap.payee_name, TRUE ) AS formatted_payee_name,
					    ba.account_name,
					    ph.payment_header_ap_payee_id,
					    ph.ap_payee_id,
					    ph.header_number,
					    ph.ap_payee_location_id,
					    ph.ap_payee_account_id,
					    apl.vendor_code,
					    p.property_name,
					    pb.building_name,
					    pu.unit_number
					FROM
						ap_payments ap
						JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )    
						JOIN LATERAL(
							SELECT 
								payment_ah.cid,
								payment_ah.ap_payee_id AS payment_header_ap_payee_id,
								payment_ah.ap_payee_id,
								payment_ah.header_number,
								payment_ah.ap_payee_location_id,
								payment_ah.ap_payee_account_id,
								MAX( invoice_ad.property_id ) AS max_property_id,
								MAX( invoice_ad.property_building_id ) AS max_property_building_id,
								MAX( invoice_ad.property_unit_id ) AS max_property_unit_id,         
								COUNT( DISTINCT invoice_ad.property_id ) AS property_count,
								COUNT( DISTINCT invoice_ad.property_building_id ) AS property_building_count,
								COUNT( DISTINCT invoice_ad.property_unit_id ) AS property_unit_count
							FROM
								ap_headers payment_ah
								JOIN ap_details payment_ad ON ( payment_ad.cid = payment_ah.cid AND payment_ad.ap_header_id = payment_ah.id AND payment_ad.deleted_on IS NULL )
								JOIN ap_allocations aa ON ( aa.cid = payment_ad.cid AND ( payment_ad.id = aa.charge_ap_detail_id OR payment_ad.id = aa.credit_ap_detail_id ) AND NOT aa.is_deleted )                                  
								JOIN ap_details invoice_ad ON ( invoice_ad.cid = aa.cid AND invoice_ad.deleted_on IS NULL AND ( invoice_ad.id = aa.charge_ap_detail_id OR invoice_ad.id = aa.credit_ap_detail_id ) ) 
								JOIN ap_headers invoice_ah ON ( invoice_ah.cid = payment_ah.cid AND invoice_ah.id = invoice_ad.ap_header_id )
							WHERE
								payment_ah.cid = ' . ( int ) $intCid . ' 
								AND payment_ah.ap_payment_id = ap.id
								AND payment_ah.deleted_by IS NULL
								AND payment_ah.deleted_on IS NULL
							GROUP BY payment_ah.cid,
								payment_ah.ap_payee_id,
								payment_ah.ap_payee_id,
								payment_ah.header_number,
								payment_ah.ap_payee_location_id,
								payment_ah.ap_payee_account_id
							LIMIT 1              
					)ph ON TRUE
					JOIN ap_payee_locations apl ON ( apl.cid = ph.cid AND apl.id = ph.ap_payee_location_id )
					LEFT JOIN properties p ON ( p.cid = ph.cid AND p.id = ( CASE WHEN 1 < ph.property_count THEN NULL ELSE ph.max_property_id END ) )
					LEFT JOIN property_buildings pb ON ( pb.cid = ph.cid AND pb.id = ( CASE WHEN 1 < ph.property_building_count THEN NULL ELSE ph.max_property_building_id END ) )
					LEFT JOIN property_units pu ON ( pu.cid = ph.cid AND pu.id = ( CASE WHEN 1 < ph.property_unit_count THEN NULL ELSE ph.max_property_unit_id END ) )
				WHERE
					ap.cid = ' . ( int ) $intCid . '
					AND	ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
					' . $strWhereCondition . '
				ORDER BY
					' . $strOrderBy;

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsForVoidingByIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap.*,
						func_format_refund_customer_names( ap.payee_name, true ) AS formatted_payee_name,
						ah.ap_payee_id,
						ah.ap_payee_location_id
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ' )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND	ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
						AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECEIVED . ', ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::PENDING . ', ' . CPaymentStatusType::CAPTURING . ', ' . CPaymentStatusType::VOIDING . ' )
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
					ORDER BY
						id';

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentDetailsByApPaymentBatchIdsByCid( $arrintApPaymentBatchIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentBatchIds ) ) {
			return NULL;
		}

		$strSql = '
			SELECT DISTINCT ON ( ah_payment.ap_payment_id )
				ap.cid,
				ah_payment.id AS id,
				ah_payment.ap_payment_id,
				apt.name AS payment_type,
				apt.id AS ap_payment_type_id,
				ap.payment_status_type_id,
				ap.ap_payment_batch_id
			FROM
				ap_payments ap
				JOIN ap_headers ah_payment ON ap.cid = ah_payment.cid AND ap.id = ah_payment.ap_payment_id
				JOIN ap_payment_types apt ON apt.id = ap.ap_payment_type_id
			WHERE
				ap.cid = ' . ( int ) $intCid . '
				AND ap.ap_payment_batch_id IN ( ' . implode( ',', $arrintApPaymentBatchIds ) . ' )
				AND apt.is_published = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap.*,
						func_format_refund_customer_names ( ap.payee_name, TRUE ) AS formatted_payee_name
					FROM
						ap_payments ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
					ORDER BY
						ap.id';

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByCidBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $objDatabase, $arrintPropertyIds ) {

		$objApTransactionsFilter        = new CApTransactionsFilter();
		$arrstrAdvancedSearchParameters = [];
		$strSql                         = '';

		$arrstrChecksValues = [];

		if( false == is_null( $arrmixFastLookup['bank_account_ids'] ) ) {
			$arrstrChecksValues['bank_account_ids'] = explode( ',', $arrmixFastLookup['bank_account_ids'] );
		}
		if( false == is_null( $arrmixFastLookup['ap_payment_type_id'] ) ) {
			$arrstrChecksValues['ap_payment_type_id'] = $arrmixFastLookup['ap_payment_type_id'];
		}
		if( false == is_null( $arrmixFastLookup['payment_number'] ) ) {
			$arrstrChecksValues['payment_number'] = $arrmixFastLookup['payment_number'];
		}
		if( false == is_null( $arrmixFastLookup['check_number_min'] ) ) {
			$arrstrChecksValues['check_number_min'] = $arrmixFastLookup['check_number_min'];
		}
		if( false == is_null( $arrmixFastLookup['check_number_max'] ) ) {
			$arrstrChecksValues['check_number_max'] = $arrmixFastLookup['check_number_max'];
		}
		if( false == is_null( $arrmixFastLookup['post_month'] ) ) {
			$arrstrChecksValues['post_month'] = $arrmixFastLookup['post_month'];
		}
		if( false == is_null( $arrmixFastLookup['check_date_from'] ) ) {
			$arrstrChecksValues['check_date_from'] = $arrmixFastLookup['check_date_from'];
		}
		if( false == is_null( $arrmixFastLookup['check_date_to'] ) ) {
			$arrstrChecksValues['check_date_to'] = $arrmixFastLookup['check_date_to'];
		}
		if( false == is_null( $arrmixFastLookup['amount_min'] ) ) {
			$arrstrChecksValues['amount_min'] = $arrmixFastLookup['amount_min'];
		}
		if( false == is_null( $arrmixFastLookup['amount_max'] ) ) {
			$arrstrChecksValues['amount_max'] = $arrmixFastLookup['amount_max'];
		}
		if( false == is_null( $arrmixFastLookup['payment_status_type_id'] ) ) {
			$arrstrChecksValues['payment_status_type_id'] = $arrmixFastLookup['payment_status_type_id'];
		}
		if( false == is_null( $arrmixFastLookup['payee_name'] ) ) {
			$arrstrChecksValues['payee_name'] = $arrmixFastLookup['payee_name'];
		}
		if( false == is_null( $arrmixFastLookup['is_unclaimed_property'] ) ) {
			$arrstrChecksValues['is_unclaimed_property'] = $arrmixFastLookup['is_unclaimed_property'];
		}
		if( false == is_null( $arrmixFastLookup['job_id'] ) ) {
			$arrstrChecksValues['job_id'] = $arrmixFastLookup['job_id'];
		}

		if( true == isset( $arrstrChecksValues['ap_payment_type_id'] ) ) {
			$objApTransactionsFilter->setApPaymentTypeId( $arrstrChecksValues['ap_payment_type_id'] );
		}
		if( true == isset( $arrstrChecksValues['payment_number'] ) ) {
			$objApTransactionsFilter->setPaymentNumber( $arrstrChecksValues['payment_number'] );
		}
		if( true == isset( $arrstrChecksValues['check_number_min'] ) ) {
			$objApTransactionsFilter->setCheckMinNumber( $arrstrChecksValues['check_number_min'] );
		}
		if( true == isset( $arrstrChecksValues['check_number_max'] ) ) {
			$objApTransactionsFilter->setCheckMaxNumber( $arrstrChecksValues['check_number_max'] );
		}
		if( true == isset( $arrstrChecksValues['post_month'] ) ) {
			$objApTransactionsFilter->setPostMonth( $arrstrChecksValues['post_month'] );
		}
		if( true == isset( $arrstrChecksValues['check_date_from'] ) ) {
			$objApTransactionsFilter->setDateFrom( $arrstrChecksValues['check_date_from'] );
		}
		if( true == isset( $arrstrChecksValues['check_date_to'] ) ) {
			$objApTransactionsFilter->setDateTo( $arrstrChecksValues['check_date_to'] );
		}
		if( true == isset( $arrstrChecksValues['amount_min'] ) ) {
			$objApTransactionsFilter->setMinTransactionAmount( $arrstrChecksValues['amount_min'] );
		}
		if( true == isset( $arrstrChecksValues['amount_max'] ) ) {
			$objApTransactionsFilter->setMaxTransactionAmount( $arrstrChecksValues['amount_max'] );
		}
		if( true == isset( $arrstrChecksValues['payment_status_type_id'] ) ) {
			$objApTransactionsFilter->setPaymentStatusTypeId( $arrstrChecksValues['payment_status_type_id'] );
		}
		if( true == isset( $arrstrChecksValues['payee_name'] ) ) {
			$objApTransactionsFilter->setPayeeName( $arrstrChecksValues['payee_name'] );
		}
		if( true == isset( $arrstrChecksValues['is_unclaimed_property'] ) ) {
			$objApTransactionsFilter->setIsUnclaimedProperty( $arrstrChecksValues['is_unclaimed_property'] );
		}
		if( true == isset( $arrstrChecksValues['bank_account_ids'] ) && 'on' != trim( $arrstrChecksValues['bank_account_ids'][0] ) ) {
			$objApTransactionsFilter->setBankAccountIds( $arrstrChecksValues['bank_account_ids'] );
		}
		if( true == isset( $arrstrChecksValues['job_id'] ) ) {
			$objApTransactionsFilter->setJobId( $arrstrChecksValues['job_id'] );
		}

		$strCondition = '';

		if( true == valArr( $objApTransactionsFilter->getBankAccountIds() ) ) {
			$strCondition .= ' AND ap.bank_account_id IN ( ' . implode( ',', $objApTransactionsFilter->getBankAccountIds() ) . ' )';
		}

		if( true == valStr( $objApTransactionsFilter->getIsUnclaimedProperty() ) ) {
			$strCondition .= ' AND ap.is_unclaimed_property = ' . ( ( true == ( boolean ) $objApTransactionsFilter->getIsUnclaimedProperty() ) ? 'true' : 'false' );
		}

		if( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId() ) {

			if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
				$strCondition .= ' AND ap.payment_number :: Integer >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
			}

			if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
				$strCondition .= ' AND ap.payment_number :: Integer <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
			}
		}

		if( true == valStr( $objApTransactionsFilter->getPostMonth() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostMonth() ) ) {
			$strCondition .= ' AND ah.post_month = \'' . addslashes( $objApTransactionsFilter->getPostMonth() ) . '\' ';
		}

		if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && false == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) ) {
			$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';

		} elseif( true == valStr( $objApTransactionsFilter->getDateTo() ) && false == valStr( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {
			$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';

		} elseif( true == valStr( $objApTransactionsFilter->getDateFrom() ) && true == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {
			$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';
			$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == is_numeric( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
			$strCondition .= ' AND ap.payment_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
		}

		if( true == is_numeric( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
			$strCondition .= ' AND ap.payment_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
		}

		if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
			$strCondition .= ' AND ad.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) ';
		} else {
			$strCondition .= ' AND CASE
							WHEN ah.post_month IS NULL
								THEN ad.property_id IS NULL
 								ELSE ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
 							END';
		}

		if( true == is_numeric( $objApTransactionsFilter->getPaymentStatusTypeId() ) ) {
			$strCondition .= ' AND ap.payment_status_type_id = ' . ( int ) $objApTransactionsFilter->getPaymentStatusTypeId();
		}

		$arrintApPaymentTypeIds = [ CApPaymentType::CASH, CApPaymentType::WIRE_TRANSFER, CApPaymentType::CHECK, CApPaymentType::CREDIT_CARD, CApPaymentType::DEBIT_CARD, CApPaymentType::WRITTEN_CHECK ];
		if( true == is_numeric( $objApTransactionsFilter->getApPaymentTypeId() ) ) {
			$arrintApPaymentTypeIds = [ $objApTransactionsFilter->getApPaymentTypeId() ];
		}

		if( true == is_numeric( $objApTransactionsFilter->getJobId() ) ) {

			$strCondition     .= ' AND jp.job_id = ' . ( int ) $objApTransactionsFilter->getJobId();
			$strJoinCondition = ' JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id ) JOIN ap_details ad1 ON ( ad1.cid = aa.cid AND ad1.id = aa.charge_ap_detail_id ) JOIN job_phases jp on ( ad1.cid = jp.cid AND ad1.job_phase_id = jp.id )';
		}

		$strSql = ' SELECT
						sub.*,
						CASE
							WHEN ( 1 < sub.property_count )
							THEN \'Multiple\'
							ELSE (
									SELECT
										DISTINCT property_name
									FROM
										properties p
										JOIN ap_details ad ON ( p.cid = ad.cid AND p.id = ad.property_id )
									WHERE
										ad.cid = ' . ( int ) $intCid . '
										AND ad.ap_header_id = sub.ap_header_id
										AND ad.cid = sub.cid
										AND ad.deleted_by IS NULL
										AND ad.deleted_on IS NULL
								)
						END AS property_name,
						CASE
							WHEN ( 1 < sub.property_count )
							THEN (	SELECT
										ARRAY_TO_STRING( ARRAY_AGG( property_name ), \',\' )
									FROM
										properties p
										JOIN ap_details ad ON ( p.cid = ad.cid AND p.id = ad.property_id )
									WHERE
										ad.cid = ' . ( int ) $intCid . '
										AND ad.ap_header_id = sub.ap_header_id
										AND ad.cid = sub.cid
										AND ad.deleted_by IS NULL
										AND ad.deleted_on IS NULL
									)
							ELSE NULL
						END AS properties_list
					FROM
						(
							SELECT
								DISTINCT ap.id,
 								ba.account_name,
 								apt.name AS ap_payment_type,
								ap.payment_date,
								ap.payment_amount,
								func_format_refund_customer_names( ap.payee_name ) AS payee_name,
								ap.payment_number,
								ah.post_month,
								ap.cid,
								ah.id AS ap_header_id,
								( SELECT COUNT( DISTINCT property_id ) FROM ap_details WHERE cid =  ' . ( int ) $intCid . ' AND ap_header_id = ah.id AND cid = ah.cid AND deleted_by IS NULL AND deleted_on IS NULL) AS property_count
 							FROM
 								ap_payments ap
								LEFT JOIN (
											SELECT
												MAX ( id ) AS id,
												ah.cid,
		 										ah.ap_payment_id
											FROM
												ap_headers ah
											WHERE
												ah.cid = ' . ( int ) $intCid . '
												AND ah.ap_payment_id IS NOT NULL
												AND CASE
														WHEN ah.reversal_ap_header_id IS NOT NULL THEN ah.id NOT IN (
																												SELECT
																													MAX( id )
																												FROM
																													ap_headers
																												WHERE
																													cid = ' . ( int ) $intCid . '
																													AND ap_payment_id IS NOT NULL
																												GROUP BY
																													cid,
																													ap_payment_id
																											)
														ELSE 1 = 1
													END
											GROUP BY
												ah.cid,
												ah.ap_payment_id
								) AS ah1 ON ( ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id )
								LEFT JOIN ap_headers ah ON ( ah.cid = ah1.cid AND ah.id = ah1.id )
								LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
 								LEFT JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
 								LEFT JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
 								LEFT JOIN ap_payees ap1 ON ( ap1.cid = ah.cid AND ap1.id = ah.ap_payee_id )
								LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )'
		          . $strJoinCondition . '
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.ap_payment_type_id IN( ' . implode( ',', $arrintApPaymentTypeIds ) . ' ) ' . $strCondition;

		$arrstrAdvancedSearchParameters = [];
		$strLookUpType                  = $arrmixFastLookup['lookup_type'];

		if( CApPayment::LOOKUP_TYPE_ACCOUNTING_CHECKES_PAYEE == $strLookUpType ) {

			if( true == valStr( $objApTransactionsFilter->getPaymentNumber() ) ) {
				$strSql .= ' AND ap.payment_number ILIKE \'%' . addslashes( $objApTransactionsFilter->getPaymentNumber() ) . '%\' ';
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ap.payee_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'apl.vendor_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

		} elseif( CApPayment::LOOKUP_TYPE_ACCOUNTING_CHECKS_PAYMENT_NUMBER == $strLookUpType ) {

			if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {
				$strSql .= ' AND ( ap.payee_name ILIKE \'%' . addslashes( $objApTransactionsFilter->getPayeeName() ) . '%\' OR apl.vendor_code ILIKE \'%' . addslashes( $objApTransactionsFilter->getPayeeName() ) . '%\' ) ';
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ap.payment_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql .= ') sub ORDER BY sub.id DESC ';

		$arrstrSearchResults = fetchData( $strSql, $objDatabase );

		if( CApPayment::LOOKUP_TYPE_ACCOUNTING_CHECKES_PAYEE == $strLookUpType ) {
			$arrstrSearchResults = rekeyArray( 'payee_name', $arrstrSearchResults );
			$arrstrSearchResults = array_values( ( array ) $arrstrSearchResults );
		}

		return $arrstrSearchResults;
	}

	public static function fetchPaginatedApPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intApContractId = NULL, $intJobId = NULL, $objApTransactionsFilter = NULL, $objPagination = NULL ) {

		if( false == valId( $intCid ) || ( true == is_null( $intJobId ) && true == is_null( $intApContractId ) ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == is_null( $intJobId ) ) ? 'ad1.ap_contract_id = ' . ( int ) $intApContractId : 'jp.job_id = ' . ( int ) $intJobId;

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valStr( $objApTransactionsFilter->getPaymentNumber() ) ) {
				$strWhereCondition .= ' AND ap.payment_number ILIKE \'%' . addslashes( $objApTransactionsFilter->getPaymentNumber() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {
				$strWhereCondition .= ' AND ap.payee_name ILIKE \'%' . addslashes( $objApTransactionsFilter->getPayeeName() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getPostMonth() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostMonth() ) ) {
				$strWhereCondition .= ' AND ah1.post_month = \'' . addslashes( $objApTransactionsFilter->getPostMonth() ) . '\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && false == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) ) {

				$strWhereCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';

			} elseif( true == valStr( $objApTransactionsFilter->getDateTo() ) && false == valStr( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {

				$strWhereCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';

			} elseif( true == valStr( $objApTransactionsFilter->getDateFrom() ) && true == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {

				$strWhereCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';
				$strWhereCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';
			}

			if( true == is_numeric( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
				$strWhereCondition .= ' AND ap.payment_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
			}

			if( true == is_numeric( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
				$strWhereCondition .= ' AND ap.payment_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
			}

			if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

				$arrstrAndConditions = [];
				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrAndConditions['where_clause'][] = ' gd.gl_reconciliation_id IS NULL';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrAndConditions['where_clause'][] = ' gd.gl_reconciliation_id IS NOT NULL';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrAndConditions['where_clause'][] = ' gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED;
				}

				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) && true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$strWhereCondition .= ' AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} elseif( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrAndConditions['where_clause'][] = ' ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} else {
					$strWhereCondition .= ' AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED;
				}

				if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
					$strWhereCondition .= ' AND ( ' . implode( ' OR ', $arrstrAndConditions['where_clause'] ) . ' )';
				}

			}
		}

		$strJoinCondition = ( false == is_null( $intJobId ) ) ? 'JOIN job_phases jp on ( ad1.cid = jp.cid AND ad1.job_phase_id = jp.id)' : '';

		$strSql = ' SELECT
								DISTINCT ap.id,
								ap.payment_number,
								ap.payment_date,
								ap.cid,
								ap.payment_amount,
								ap.bank_account_id,
								ap.payment_status_type_id,
								apt.name AS ap_payment_type,
								ba.account_name,
								func_format_refund_customer_names( ap.payee_name ) AS payee_name,
								ah1.post_month,
								ah1.id AS ap_header_id,
								CASE
									WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
								ELSE CASE
									WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
									WHEN gd.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
								ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
								END
								END AS payment_status_type_name,
								array_agg( DISTINCT( ah2.header_number ) ) AS header_numbers,
								sub_f.file_id
							FROM
								ap_payments ap
								JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
								JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
								LEFT JOIN(
							                SELECT
												ah.*,
												row_number() over( PARTITION BY  ah.cid, ah.ap_payment_id ORDER BY ID DESC ) as row_number
											FROM
												ap_headers ah
											WHERE
												ah.cid = ' . ( int ) $intCid . '
												AND ah.ap_payment_id IS NOT NULL
												AND CASE
														WHEN ah.reversal_ap_header_id IS NOT NULL THEN ah.id NOT IN (
																												SELECT
																													MAX( id )
																												FROM
																													ap_headers
																												WHERE
																													cid = ' . ( int ) $intCid . '
																													AND ap_payment_id IS NOT NULL
																												GROUP BY
																													cid,
																													ap_payment_id
																											)
														ELSE 1 = 1
													END
								) AS ah1 ON ( ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id AND ah1.row_number = 1 )
								LEFT JOIN ap_details ad ON ( ah1.cid = ad.cid AND ah1.id = ad.ap_header_id AND ah1.gl_transaction_type_id = ad.gl_transaction_type_id AND ah1.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
								LEFT JOIN ap_payees ap1 ON ( ap1.cid = ah1.cid AND ap1.id = ah1.ap_payee_id )
								JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
								JOIN ap_details ad1 ON ( ad1.cid = aa.cid AND ad1.id = aa.charge_ap_detail_id )
								' . $strJoinCondition . '
								JOIN ap_headers ah2 ON ( ah2.cid = ad1.cid AND ah2.id = ad1.ap_header_id )
								LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( 4,5 ) )
								LEFT JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id
														AND gd.amount = CASE
																		WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL THEN
																			aa.allocation_amount * - 1
																		ELSE
																			aa.allocation_amount
																	END
													AND gd.property_id = CASE
																			WHEN ba.reimbursed_property_id <> aa.property_id THEN
																				ba.reimbursed_property_id
																			ELSE
																				aa.property_id
																			END
														)
								LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id )
								LEFT JOIN (
                                   	SELECT 
                                		f.cid,
                                		f.id as file_id,
                                		fa.ap_payment_id 
                                    FROM
                                    	file_associations fa
                                    	JOIN files f ON ( fa.cid = f.cid AND f.id = fa.file_id )
                                    	JOIN file_types ft ON ( ft.cid = f.cid AND f.file_type_id = ft.id AND ft.system_code = \'LW\' )
                                    WHERE 
                                    	fa.deleted_by IS NULL
                                ) AS sub_f ON ( sub_f.cid = ap.cid AND sub_f.ap_payment_id = ap.id )
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND CASE
										WHEN ah1.post_month IS NULL
											THEN ad.property_id IS NULL
											ELSE ad.property_id = ' . ( int ) $intPropertyId . '
										END
								AND ' . $strWhereCondition . '
							GROUP BY
								ap.id,
								ap.bank_account_id,
								ap.payment_date,
								ap.cid,
								apt.name,
								ba.account_name,
								ah1.post_month,
								ah1.id,
								ah1.cid,
								gd.gl_reconciliation_id,
								gr.gl_reconciliation_status_type_id,
								sub_f.file_id
						ORDER BY ap.id DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPaymentsByApPayeeIdByJobIdOrApContractIdByCid( $intApPayeeId, $intCid, $objDatabase, $intApContractId = NULL, $intJobId = NULL, $intApPaymentId = NULL ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intCid ) || ( true == is_null( $intJobId ) && true == is_null( $intApContractId ) ) ) {
			return NULL;
		}

		$strWhereCondition               = ( true == is_null( $intJobId ) ) ? 'ad1.ap_contract_id = ' . ( int ) $intApContractId : 'jp.job_id = ' . ( int ) $intJobId;
		$strJoinCondition                = ( false == is_null( $intJobId ) ) ? ' JOIN job_phases jp on ( ad1.cid = jp.cid AND ad1.job_phase_id = jp.id )' : '';
		$strWhereConditionForApPaymentId = ( false == is_null( $intApPaymentId ) ) ? ' AND fa.ap_payment_id <> ' . ( int ) $intApPaymentId : '';

		$strSql = ' SELECT
						DISTINCT ap.id,
						ap.payment_number,
						ah1.id AS ap_payment_header_id
					FROM
						ap_payments ap
						LEFT JOIN(
					 				SELECT
										ah.*,
										row_number() over( PARTITION BY ah.cid, ah.ap_payment_id ORDER BY ID DESC ) as row_number
									FROM
										ap_headers ah
									WHERE
										ah.cid = ' . ( int ) $intCid . '
										AND ah.ap_payment_id IS NOT NULL
										AND CASE
												WHEN ah.reversal_ap_header_id IS NOT NULL THEN ah.id NOT IN (
																										SELECT
																											MAX( id )
																										FROM
																											ap_headers
																										WHERE
																											cid = ' . ( int ) $intCid . '
																											AND ap_payment_id IS NOT NULL
																										GROUP BY
																											cid,
																											ap_payment_id
																									)
												ELSE 1 = 1
											END
									) AS ah1 ON ( ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id AND ah1.row_number = 1 
													AND ah1.ap_payment_id NOT IN (
																	SELECT fa.ap_payment_id
																	FROM file_associations fa
																	JOIN files f ON(f.cid = fa.cid AND f.id = fa.file_id)
																	JOIN file_types ft ON(f.cid = ft.cid AND f.file_type_id = ft.id)
																	Where fa.cid = ah1.cid 
																		AND fa.ap_payee_id = ah1.ap_payee_id 
																		AND ft.system_code =  \'' . CFileType::SYSTEM_CODE_LIEN_WAIVER . '\'
																		' . $strWhereConditionForApPaymentId . '
																	)
												)
						JOIN ap_details ad ON ( ah1.cid = ad.cid AND ah1.id = ad.ap_header_id AND ah1.gl_transaction_type_id = ad.gl_transaction_type_id AND ah1.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
						JOIN ap_details ad1 ON ( ad1.cid = aa.cid AND ad1.id = aa.charge_ap_detail_id )
						' . $strJoinCondition . '
						
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ah1.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ' . $strWhereCondition . '
					ORDER BY 
						ap.payment_number DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPaymentsByCidByApContractIdOrByJobid( $intCid, $objDatabase, $intApContractId = NULL, $intJobId = NULL, $arrmixLienWaiversFilter ) {

		if( false == valId( $intCid ) || ( true == is_null( $intJobId ) && true == is_null( $intApContractId ) ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == is_null( $intJobId ) ) ? ' AND ad_charge.ap_contract_id = ' . ( int ) $intApContractId : ' AND jp.job_id = ' . ( int ) $intJobId;
		$strJoinCondition  = ( false == is_null( $intJobId ) ) ? ' JOIN job_phases jp on ( ad_charge.cid = jp.cid AND ad_charge.job_phase_id = jp.id )' : '';

		if( true == valArr( $arrmixLienWaiversFilter['ap_payee_ids'] ) ) {
			$strWhereCondition .= ' AND fa.ap_payee_id IN( ' . implode( ',', $arrmixLienWaiversFilter['ap_payee_ids'] ) . ' )';
		}

		if( true == valStr( $arrmixLienWaiversFilter['from_upload_date'] ) ) {
			$strWhereCondition .= ' AND f.file_upload_date :: date >= \'' . date( 'Y-m-d', strtotime( $arrmixLienWaiversFilter['from_upload_date'] ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $arrmixLienWaiversFilter['to_upload_date'] ) ) {
			$strWhereCondition .= ' AND f.file_upload_date :: date <= \'' . date( 'Y-m-d', strtotime( $arrmixLienWaiversFilter['to_upload_date'] ) ) . ' 23:59:59\'';
		}

		$strSql = 'SELECT
						f.id,
						f.cid,
						f.title,
						f.file_upload_date,
						f.file_path,
						f.file_name,
						array_agg(DISTINCT ah_charge.header_number order by ah_charge.header_number DESC ) AS header_numbers,
						ap.payee_name,
						ap.payment_number,
						ap.id AS payment_id
					FROM
						ap_payments ap
						JOIN file_associations fa ON (ap.cid = fa.cid AND ap.id = fa.ap_payment_id)
						JOIN ap_headers ah ON (ah.cid = ap.cid AND ah.ap_payment_id = ap.id)
						JOIN files f ON(f.cid = fa.cid AND f.id = fa.file_id)
						JOIN file_types ft ON(f.cid = ft.cid AND f.file_type_id = ft.id)
						JOIN ap_details ad ON (ah.cid = ad.cid AND ad.ap_header_id = ah.id AND ad.deleted_by IS NULL )
						JOIN ap_allocations aa ON (aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id)
						JOIN ap_details ad_charge ON (aa.cid = ad_charge.cid AND aa.charge_ap_detail_id = ad_charge.id)
						' . $strJoinCondition . '
						JOIN ap_headers ah_charge ON (ah_charge.cid = ad_charge.cid AND ad_charge.ap_header_id = ah_charge.id)
					WHERE
						ft.cid = ' . ( int ) $intCid . '
						AND ft.system_code =  \'' . CFileType::SYSTEM_CODE_LIEN_WAIVER . '\'
						' . $strWhereCondition . '
					GROUP BY 
						f.id,
						f.cid,
						ap.payee_name,
						ap.payment_number,
						ap.id
					ORDER BY
						f.id 
					DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPaymentBillingDetailsByIdsByCid( $strPaymentInfo, $intCid, $objDatabase ) {
		$strSql = '
			WITH payment_info AS (
				SELECT 
					(string_to_array(payments.t, \'^\'))[1]::INTEGER as id, 
					(string_to_array(payments.t, \'^\'))[2]::INTEGER as ap_payment_id, 
					(string_to_array(payments.t, \'^\'))[3]::NUMERIC as company_charge_amount,
					(string_to_array(payments.t, \'^\'))[4]::INTEGER as charge_code_id,
					(string_to_array(payments.t, \'^\'))[5]::INTEGER AS transaction_id
				FROM 
				(
					SELECT unnest(
						string_to_array(
							\'' . $strPaymentInfo . '\',
							\'~\'
						)
					) as t
				) as payments
			)
			SELECT
				ap.id as payment_id, payment_number, ba.account_name, payee_name, payment_amount, payment_date, cpi.company_charge_amount as fee_amount, cpi.charge_code_id, cpi.transaction_id
			FROM
				ap_payments ap 
				INNER JOIN bank_accounts ba ON ba.id = ap.bank_account_id and ba.cid = ap.cid
				INNER JOIN payment_info cpi ON cpi.ap_payment_id = ap.id 
			WHERE
				ap.cid = ' . ( int ) $intCid . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveApPaymentsByPaymentApHeaderIdsByCid( $arrobjPaymentApHeaders, $intCid, $objClientDatabase ) {

		$strConditions       = NULL;
		$strConditionGroupBy = NULL;

		if( false == valArr( $arrobjPaymentApHeaders ) ) {
			return NULL;
		}
		$arrstrRekeyedPaymentNumbers = array_keys( rekeyObjects( 'HeaderNumber', $arrobjPaymentApHeaders ) );

		$arrstrPaymentNumbers = explode( ',', implode( ',', $arrstrRekeyedPaymentNumbers ) );

		if( \Psi\Libraries\UtilFunctions\count( $arrstrRekeyedPaymentNumbers ) < \Psi\Libraries\UtilFunctions\count( $arrstrPaymentNumbers ) ) {
			foreach( array_unique( $arrstrPaymentNumbers ) as $arrstrPaymentNumber ) {
				$strConditions .= 'OR ah.header_number LIKE \'%' . $arrstrPaymentNumber . '%\' ';
			}
		} else {
			$strConditionGroupBy .= 'GROUP BY    ah.header_number';
		}

		$strSql = 'SELECT
						array_to_string( array_agg( ap.payment_number) , \',\' ) as payment_numbers
					FROM
						ap_headers ah
						JOIN ap_headers ah1 ON ( ah.cid = ah1.cid and ah.ap_payment_id = ah1.ap_payment_id AND ah1.reversal_ap_header_id IS NULL )
						LEFT JOIN ap_payments ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ( ah.header_number LIKE \'%' . implode( '%\' OR ah.header_number LIKE \'%', $arrstrRekeyedPaymentNumbers ) . '%\'
						' . $strConditions . ' )
						AND ah.deleted_by IS NULL
					' . $strConditionGroupBy . '
					HAVING 
						COUNT( ap.payment_number ) > 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByIdsByApPaymentTypesByCids( $arrintApPaymentTypeIds, $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentTypeIds ) || false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
						AND ap_payment_type_id IN ( ' . implode( ',', $arrintApPaymentTypeIds ) . ' )
					ORDER BY
						id';

		return self::fetchApPayments( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByApHeaderExportBatchIdsByCid( $arrintApHeaderExportBatchIds, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						ah.accounting_export_batch_id,
						min ( p.property_name ) as property_name,
						min ( ba.account_name ) AS account_name,
						min( util_get_translated( \'name\', tv.name, tv.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) ) AS batch_name,
						min( tv.id ) AS batch_type_id,
						CASE
							WHEN COUNT ( DISTINCT ( ad.property_id ) ) = 1 THEN min ( ad.property_id )
							ELSE 2
						END AS batch_properties,
						CASE
							WHEN COUNT ( DISTINCT ( ap.bank_account_id ) ) = 1 THEN min ( ap.bank_account_id )
							ELSE 2
						END AS batch_bank_accounts,
						CASE
							WHEN COUNT ( DISTINCT ( ba.transmission_vendor_id) ) = 1 THEN 1
							ELSE 2
						END AS batch_bank_formats
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
						JOIN bank_accounts ba ON ( ba.id = ap.bank_account_id AND ba.cid = ap.cid )
						JOIN transmission_vendors tv ON ( tv.id = ba.transmission_vendor_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ah.cid AND apc.ap_payee_id = ah.ap_payee_id AND tv.id = ' . CTransmissionVendor::PP_EXPORT_COM_DATA_BANK . ' )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ah.accounting_export_batch_id IN ( ' . implode( ',', $arrintApHeaderExportBatchIds ) . ' )
					GROUP BY
						ah.accounting_export_batch_id
					ORDER BY
						ah.accounting_export_batch_id DESC ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsCountLocked( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPaymentIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						ap.id,
						ap.payment_number
					FROM
						ap_payments ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
						AND NOT pg_try_advisory_xact_lock( util.util_calc_advisory_lock_key( ' . ( int ) $intCid . ', ' . CAdvisoryLockKey::VOID_AP_PAYMENT_PROCESS . ' ), ap.id )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByHeaderNumbersByCid( $arrstrHeaderNumbers, $intCid, $objDatabase, $boolIncludeVoidedPayments = false ) {

		if( false == valArr( $arrstrHeaderNumbers ) ) {
			return NULL;
		}
		$strImplodeStatement = '\'%' . implode( '%\',\'%', $arrstrHeaderNumbers ) . '%\'';

		$strSql = 'SELECT
						ah.header_number,
						ah.post_month,
						ah.post_date,
						ah.id as ap_header_id,
						apt.name AS ap_payment_type_name,
						pst.name AS payment_status_type_name,
						rt.name AS return_type_name,
						ap.id,
						ap.payment_status_type_id,
						ap.payment_memo,
						ap.payment_date,
						ap.issue_datetime,
						ap.payee_name,
						ap.payment_amount,
						ap.payment_number,
						ap.is_reversed,
						ap.is_quick_check,
						aa.charge_ap_detail_id as ap_detail_id,
						CASE
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
							WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
							ELSE CASE
								WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
								WHEN gh.id IS NULL THEN \'Historical\'
								WHEN gd.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
								ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
								END
							END AS reconcilation_status_name,
							ba.check_bank_name,
							ap.bank_account_id,
							ba.account_name as account_name
					FROM
						ap_headers ah
						JOIN ap_payments ap ON ( ap.id = ah.ap_payment_id AND ap.cid = ah.cid )
						LEFT JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
						LEFT JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id )
						LEFT JOIN return_types rt ON ( rt.id = ap.return_type_id )
						LEFT JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id )
						LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
						LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND
						gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
						LEFT JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id
													AND gd.amount = CASE
																		WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL THEN
																			aa.allocation_amount * - 1
																		ELSE
																			aa.allocation_amount
																	END
													AND gd.property_id = aa.property_id )
						LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.gl_transaction_type_id = ' . CGlTransactionType::AP_PAYMENT . '
						AND ah.ap_payment_id IS NOT NULL
						AND ah.header_number LIKE ANY( array[ ' . $strImplodeStatement . ' ] )
						AND ah.is_template IS FALSE';
		if( false == $boolIncludeVoidedPayments ) {
			$strSql .= ' AND ap.payment_status_type_id <> ' . CPaymentStatusType::VOIDED;
		}
		$strSql .= ' ORDER BY ah.header_number ASC, ap.id DESC';
		return self::fetchApPayments( $strSql, $objDatabase, false );
	}

}
?>
