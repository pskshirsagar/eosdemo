<?php

class CPressRelease extends CBasePressRelease {

    public function valCid() {
        $boolIsValid = true;

 	    if( true == is_null( $this->getCid() ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client is required.' ) ) );
	    }

        return $boolIsValid;
    }

    public function valPressReleaseCategoryId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPressReleaseCategoryId() ) ) {

            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'press_release_category_id', __( 'Press release category is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTitle( $objDatabase = NULL ) {

        $boolIsValid = true;

        if( true == is_null( $this->getTitle() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valTeaser() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getTeaser() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'teaser', '' ) );
        // }

        return $boolIsValid;
    }

    public function valBody() {
        $boolIsValid = true;

        if( true == is_null( $this->getBody() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'body', __( 'Body is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valReleaseDatetime() {
        $boolIsValid = true;

        $todaysDate = date( 'm/d/Y H:i:s' );

        if( true == is_null( $this->getReleaseDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_datetime', __( 'Release date is required.' ) ) );
        }

        if( false == is_null( $this->getReleaseDatetime() ) && false == CValidation::validateDate( \Psi\CStringService::singleton()->substr( $this->getReleaseDatetime(), 0, 10 ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_datetime', __( 'Date is not formatted properly.' ) ) );
        }

        return $boolIsValid;
    }

	public function valRedirectionUrl() {
		$boolIsValid = true;

		if( true == valStr( $this->getRedirectUrl() ) && false == CValidation::checkUrl( $this->getRedirectUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'redirect_url', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	// $boolIsValid &= $this->valPressReleaseCategoryId();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valTitle( $objDatabase );
            	$boolIsValid &= $this->valBody();
            	$boolIsValid &= $this->valReleaseDatetime();
	            $boolIsValid &= $this->valRedirectionUrl();
            	break;

            case VALIDATE_UPDATE:
            	// $boolIsValid &= $this->valPressReleaseCategoryId();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valTitle( $objDatabase );
            	$boolIsValid &= $this->valBody();
            	$boolIsValid &= $this->valReleaseDatetime();
	            $boolIsValid &= $this->valRedirectionUrl();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>