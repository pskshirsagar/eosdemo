<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDetailLogs
 * Do not add any new functions to this class.
 */

class CGlDetailLogs extends CBaseGlDetailLogs {

	public static function fetchCusotmGlDetailLogsByGlHeaderLogIdsByCid( $arrintGlHeaderLogIds, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false ) {

		if( false == valArr( $arrintGlHeaderLogIds ) ) {
			return NULL;
		}

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						gdl.*,
						p.property_name,
						gat.name AS gl_account_name,
						gat.formatted_account_number AS gl_account_number,
						pu.unit_number,
						cd.name as company_department_name,
						gd.name as gl_dimension_name
					FROM
						gl_detail_logs gdl
						JOIN properties p ON ( gdl.property_id = p.id AND gdl.cid = p.cid )
						JOIN gl_accounts ga ON ( gdl.cid = ga.cid AND CASE
																				WHEN
																					gdl.accrual_gl_account_id IS NOT NULL
																				THEN
																					gdl.accrual_gl_account_id = ga.id
																				ELSE
																					gdl.cash_gl_account_id = ga.id
																				END )
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN property_units pu ON ( gdl.cid = pu.cid AND gdl.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN company_departments cd ON ( cd.cid = gdl.cid AND gdl.company_department_id = cd.id )
						LEFT JOIN gl_dimensions gd ON ( gd.cid = gdl.cid AND gdl.gl_dimension_id = gd.id )
					WHERE
						gdl.cid = ' . ( int ) $intCid . '
						AND gdl.gl_header_log_id IN( ' . implode( ',', $arrintGlHeaderLogIds ) . ' )
					ORDER BY
						gdl.gl_detail_id ASC';

		return self::fetchGlDetailLogs( $strSql, $objClientDatabase );
	}
}
?>