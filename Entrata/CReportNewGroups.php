<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewGroups
 * Do not add any new functions to this class.
 */

class CReportNewGroups extends CBaseReportNewGroups {

	public static function fetchReportGroupsWithInstances( int $intCid, CDatabase $objDatabase, int $intReportGroupTypeId = CReportGroupType::STANDARD, $intCompanyUserId = NULL, $arrintPsProductIds = [], $arrintModuleIds = [], $arrstrSearchCriteria = [] ) {

		// Enforce product support, module permissions
		if( !valArr( $arrintPsProductIds ) ) {
			return [];
		}
		// We are applying permissions only on company reports.
		$strModulePermissionCondition = '';
		if( valArr( $arrintModuleIds ) ) {
			$strModulePermissionCondition = 'AND ( ri.module_id IS NULL OR ( ri.module_id IN( ' . implode( ',', $arrintModuleIds ) . ' ) ) )';
		}
		// Apply search filtering
		$strGroupsCondition = $strFilterCondition = $strColumnCondition = $strReportTitleCondition = $strReportTypeCondition = '';
		if( valArr( $arrstrSearchCriteria ) ) {
			if( valArr( $arrstrSearchCriteria['groups'] ) && 'all' != current( $arrstrSearchCriteria['groups'] ) ) {
				$strGroupsCondition = ' AND rg.id IN ( ' . implode( ',', $arrstrSearchCriteria['groups'] ) . ' ) ';
			}
			if( valStr( $arrstrSearchCriteria['keywords'] ) ) {
				$arrstrSearchCriteria['keywords'] = \Psi\CStringService::singleton()->strtolower( getSanitizedFormField( $arrstrSearchCriteria['keywords'], true ) );
				$strReportTitleCondition = ' AND ( LOWER( r.title ) LIKE \'%' . $arrstrSearchCriteria['keywords'] . '%\' OR LOWER( r.description ) LIKE \'%' . $arrstrSearchCriteria['keywords'] . '%\' )';
				if( valStr( CLocaleContainer::createService()->getLocaleCode() ) ) {
					$strReportTitleCondition = 'AND ( 
						LOWER( util_get_translated( \'title\', r.title, r.details ) ) LIKE LOWER( \'%' . $arrstrSearchCriteria['keywords'] . '%\' ) OR 
						LOWER( util_get_translated( \'description\', r.description, r.details ) ) LIKE LOWER( \'%' . $arrstrSearchCriteria['keywords'] . '%\' )
					)';
				}
			}

			if( valArr( $arrstrSearchCriteria['report_filters'] ) && 'all' != current( $arrstrSearchCriteria['report_filters'] ) ) {
				$strFilterCondition = ' JOIN report_filter_descriptions rfd ON ( rfd.default_report_version_id = drv.id AND rfd.filter_key IN ( ' . '\'' . implode( "','", $arrstrSearchCriteria['report_filters'] ) . '\'' . ' ) )';
			}

			if( valArr( $arrstrSearchCriteria['report_columns'] ) && 'all' != current( $arrstrSearchCriteria['report_columns'] ) ) {
				$strColumnCondition = ' JOIN report_datasets rd ON ( rd.default_report_version_id = drv.id )
					 JOIN report_columns rc ON (rd.id = rc.report_dataset_id AND rc.column_key IN ( ' . '\'' . implode( "','", $arrstrSearchCriteria['report_columns'] ) . '\'' . ' ))';
			}
		}
		$strPreCondition['default_report_version_join'] = valStr( $strColumnCondition ) || valStr( $strFilterCondition ) ? ' JOIN default_report_versions drv ON rv.default_report_version_id = drv.id' : '';
		$strPreCondition['distinct'] = valStr( $strColumnCondition ) || valStr( $strFilterCondition ) ? 'DISTINCT ' : '';
		$strPreCondition['order_by'] = valStr( $strColumnCondition ) || valStr( $strFilterCondition ) ? '' : 'ORDER BY ri.name';

		if( valArr( $arrstrSearchCriteria['report_type'] ) && 'all' != current( $arrstrSearchCriteria['report_type'] ) ) {
			if( 'system_and_custom_reports' == current( $arrstrSearchCriteria['report_type'] ) ) {
				$strReportTypeCondition = 'AND r.report_type_id IN ( ' . CReportType::SYSTEM . ', ' . CReportType::CUSTOM . ' ) ';
			} else {
				$strReportTypeCondition = 'AND r.report_type_id = ' . CReportType::SAP;
			}
		}

		// Remove expired reports and the reports which don't have product permissions.
		$strWhereCondition	= ' AND CASE WHEN TRUE = rv.is_latest THEN COALESCE( rv.expiration, now() ) >= now() ELSE TRUE END';
		$strSql = '
			SELECT
				rg.*,
				jsonb_agg(
					' . $strPreCondition['distinct'] . 'jsonb_build_object(
						\'id\', ri.id,
						\'cid\', ri.cid,
						\'report_id\', ri.report_id,
						\'report_version_id\', ri.report_version_id,
						\'module_id\', ri.module_id,
						\'name\', util_get_translated( \'name\', ri.name, ri.details ),
						\'description\', util_get_translated( \'description\', ri.description, ri.details ),
						\'filters\', ri.filters,
						\'report_group_instance_id\', rgi.id,
						\'report\', jsonb_build_object(
							\'id\', rv.report_id,
							\'cid\', rv.cid,
							\'report_type_id\', r.report_type_id,
							\'name\', r.name,
							\'title\', util_get_translated( \'title\', r.title, r.details ),
							\'created_on\', r.created_on,
							\'description\', util_get_translated( \'description\', r.description, r.details ),
							\'report_version_id\', rv.id,
							\'definition\', rv.definition,
							\'major\', rv.major,
							\'minor\', rv.minor,
							\'expiration\', rv.expiration,
							\'is_expired\', COALESCE( rv.expiration, NOW() ) < NOW(),
							\'title_addendum\', rv.title_addendum,
							\'report_instance_id\', ri.id,
							\'allow_only_psi_admin\', rv.definition ->> \'allow_only_psi_admin\',
							\'is_quick_link\', CASE WHEN quick_link.report_new_instance_id IS NOT NULL THEN TRUE ELSE FALSE END,
							\'update_available\', CASE
								WHEN ( lrv.id <> rv.id ) AND COALESCE ( lrv.expiration, NOW ( ) ) >= NOW ( ) AND ( FALSE = ( lrv.definition ->> \'allow_only_psi_admin\' )::BOOLEAN ) AND COALESCE ( ( lrv.definition ->> \'is_published\' )::BOOLEAN, TRUE ) = TRUE THEN  ( CASE
									WHEN lrv.major < rv.major THEN FALSE
									WHEN lrv.major = rv.major AND lrv.minor < rv.minor THEN FALSE
									ELSE ( CASE
												WHEN ( ( lrv.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedClient\' )::TEXT IS NOT NULL THEN lrv.cid = ANY ( ( \'{ \' || trim ( both \'[]\' FROM ( ( lrv.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedClient\' )::TEXT ) || \' }\' )::INT [ ] )
												WHEN ( ( lrv.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedCid\' )::TEXT IS NOT NULL THEN lrv.cid = ANY ( ( \'{ \' || trim ( both \'[]\' FROM ( ( lrv.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedCid\' )::TEXT ) || \' }\' )::INT [ ] )
												ELSE TRUE
											END )
									END ) 
								ELSE FALSE
							END
						)
					) ' . $strPreCondition['order_by'] . '
				) AS report_instances
			FROM
				report_new_groups rg
				LEFT JOIN report_new_group_instances rgi ON rg.cid = rgi.cid AND rg.id = rgi.report_new_group_id
				LEFT JOIN report_new_instances ri ON ri.cid = rgi.cid AND ri.id = rgi.report_new_instance_id AND ri.deleted_on IS NULL ' . $strModulePermissionCondition . '
				LEFT JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
				' . $strPreCondition['default_report_version_join'] . '
				' . $strFilterCondition . '
				' . $strColumnCondition . '
				LEFT JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN ( rv.definition->>\'allow_only_admin\' )::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN ( rv.definition->>\'allow_only_psi_admin\' )::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				LEFT JOIN reports r ON rv.cid = r.cid AND rv.report_id = r.id
				LEFT JOIN LATERAL 
				(
					SELECT * FROM ( SELECT
						pr.cid,
						pr.id ,
						row_number() OVER ( PARTITION BY pr.cid, pr.report_id ) as DUPLICATE_REPORT
					FROM 
					product_reports pr
					WHERE
						r.cid = pr.cid
						AND r.id = pr.report_id
						AND ( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) ) ) AS dup_pr
						WHERE dup_pr.DUPLICATE_REPORT = 1
					)pro_rep ON TRUE
				LEFT JOIN LATERAL(
					SELECT
						rngi.report_new_instance_id
					FROM
						report_new_groups AS rng
						JOIN report_new_group_instances AS rngi ON rng.cid = rngi.cid AND rng.id = rngi.report_new_group_id 
					WHERE rng.report_group_type_id = ' . CReportGroupType::QUICK_LINK . '
						AND rngi.report_new_instance_id = ri.id
						AND rng.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND rngi.deleted_by IS NULL
				) AS quick_link ON TRUE
				LEFT JOIN report_versions lrv ON ( lrv.cid = rv.cid  AND lrv.report_id  = rv.report_id AND lrv.is_latest = TRUE )
			WHERE
				rg.cid = ' . ( int ) $intCid . '
				AND rg.report_group_type_id = ' . ( int ) $intReportGroupTypeId . '
				' . ( is_null( $intCompanyUserId ) ? '' : 'AND rg.company_user_id = ' . ( int ) $intCompanyUserId ) . '
				AND rg.deleted_on IS NULL ' . $strGroupsCondition . '
				AND ri.deleted_on IS NULL ' . $strReportTitleCondition . '
				AND rgi.deleted_by IS NULL
				' . $strReportTypeCondition . '
				AND CASE
						WHEN r.report_type_id IN ( ' . CReportType::SYSTEM . ', ' . CReportType::CUSTOM . ' ) THEN util.util_to_bool(rv.definition ->> \'is_published\') = TRUE
						ELSE TRUE
					END
				' . $strWhereCondition . '
			GROUP BY
				rg.cid,
				rg.id
			ORDER BY
				util_to_numeric(rg.details->>\'position\'),
				rg.name
			;
		';
		return self::fetchReportNewGroups( $strSql, $objDatabase );
	}

	/**
	 * @param $intCompanyUserId
	 * @param $intCid
	 * @param $objDatabase
	 * @param array $arrstrSearchCriteria
	 * @param array $arrintPsProductIds
	 * @return CReportNewGroup[]
	 */
	public static function fetchReportLibrary( $intCompanyUserId, $intCid, $objDatabase, $arrstrSearchCriteria = [], $arrintPsProductIds = [] ) {

		// Enforce product support, module permissions
		if( !valArr( $arrintPsProductIds ) ) {
			return [];
		}
		// Apply search filtering
		$strReportInstanceCondition = $strReportTitleCondition = $strReportColumnCondition = $strReportFilterCondition = $strCategoriesCondition = $strCategoryNameCondition = '';
		if( true == valArr( $arrstrSearchCriteria ) ) {
			if( true == valStr( $arrstrSearchCriteria['keywords'] ) ) {
				$arrstrSearchCriteria['keywords'] = \Psi\CStringService::singleton()->strtolower( getSanitizedFormField( $arrstrSearchCriteria['keywords'], true ) );
				$strReportTitleCondition = ' AND ( LOWER( r.title ) LIKE \'%' . $arrstrSearchCriteria['keywords'] . '%\' OR LOWER( r.description ) LIKE \'%' . $arrstrSearchCriteria['keywords'] . '%\' )';
				if( true == valStr( CLocaleContainer::createService()->getLocaleCode() ) ) {
					$strReportTitleCondition = 'AND ( 
						LOWER( util_get_translated( \'title\', r.title, r.details ) ) LIKE LOWER( \'%' . $arrstrSearchCriteria['keywords'] . '%\' ) OR 
						LOWER( util_get_translated( \'description\', r.description, r.details ) ) LIKE LOWER( \'%' . $arrstrSearchCriteria['keywords'] . '%\' )
					)';
				}
			}
			if( true == valArr( $arrstrSearchCriteria['report_columns'] ) && 'all' != current( $arrstrSearchCriteria['report_columns'] ) ) {
				$strColumnKey = implode( "','", $arrstrSearchCriteria['report_columns'] );
				$strReportColumnCondition = 'JOIN report_datasets rd ON ( rd.default_report_version_id = drv.id )
					JOIN report_columns rc ON (rd.id = rc.report_dataset_id AND rc.column_key IN ( ' . '\'' . $strColumnKey . '\'' . ' ))';
			}
			if( true == valArr( $arrstrSearchCriteria['report_filters'] ) && 'all' != current( $arrstrSearchCriteria['report_filters'] ) ) {
				$strFilterKey = implode( "','", $arrstrSearchCriteria['report_filters'] );
				$strReportFilterCondition = 'JOIN report_filter_descriptions rfd ON ( rfd.default_report_version_id = drv.id AND rfd.filter_key IN ( ' . '\'' . $strFilterKey . '\'' . ' ) )';
			}
			if( true == valArr( $arrstrSearchCriteria['categories'] ) && 'all' != current( $arrstrSearchCriteria['categories'] ) ) {
				$strCategoriesCondition = ' AND m.id IN ( ' . implode( ',', $arrstrSearchCriteria['categories'] ) . ' ) ';
			}
			if( valStr( $arrstrSearchCriteria['category_names'] ) ) {
				$strCategoryNameCondition = ' AND LOWER( util_get_translated( \'title\', m.title, m.details ) ) LIKE LOWER( \'' . $arrstrSearchCriteria['category_names'] . '\' ) ';
			}
			if( true == valArr( $arrstrSearchCriteria['report_options'] ) && 'all' != current( $arrstrSearchCriteria['report_options'] ) ) {
				if( 'true' == current( $arrstrSearchCriteria['report_options'] ) ) {
					$strReportInstanceCondition = ' JOIN report_new_instances ri ON ( r.cid = ri.cid AND r.id = ri.report_id AND ri.deleted_by IS NULL )
						JOIN report_new_group_instances rngi ON ( rngi.cid = ri.cid AND ri.id = rngi.report_new_instance_id )
						JOIN report_new_groups rng ON ( rng.cid = rngi.cid AND rng.id = rngi.report_new_group_id AND rng.report_group_type_id = ' . CReportGroupType::STANDARD . ' )';
				} else {
					$strReportInstanceCondition = ' JOIN report_new_instances ri ON ( r.cid = ri.cid AND r.id NOT IN ( SELECT report_id FROM report_new_instances where cid = r.cid ) AND ri.deleted_by IS NULL ) ';
				}
			}
		}

		$strSql = '
			SELECT
				m.id,
				util_get_translated( \'title\', m.title, m.details ) AS name,
				jsonb_agg(
					jsonb_build_object(
						\'id\', ri.id,
						\'cid\', ri.cid,
						\'report_id\', ri.report_id,
						\'report_version_id\', ri.report_version_id,
						\'module_id\', m.id,
						\'name\', ri.name,
						\'description\', ri.description,
						\'filters\', ri.filters,
						\'report\', jsonb_build_object(
							\'id\', rv.report_id,
							\'cid\', rv.cid,
							\'report_type_id\', r.report_type_id,
							\'name\', r.name,
							\'title\', util_get_translated( \'title\', r.title, r.details ),
							\'created_on\', r.created_on,
							\'description\', util_get_translated( \'description\', r.description, r.details ),
							\'title_addendum\', rv.title_addendum,
							\'report_version_id\', rv.id,
							\'definition\', rv.definition,
							\'major\', rv.major,
							\'minor\', rv.minor,
							\'expiration\', rv.expiration,
							\'report_instance_id\', ri.id
						)
					) ORDER BY ri.name
				) AS report_instances
			FROM
				reports r
				JOIN report_versions rv ON rv.cid = r.cid AND rv.report_id = r.id
				JOIN default_report_versions drv ON drv.id = rv.default_report_version_id AND drv.is_default = TRUE
				JOIN (
					SELECT
						row_number() OVER( PARTITION BY r.cid, r.id, rv.id ) AS row_number,
						dr.default_report_group_id,
						NULL::INTEGER AS id,
						rv.cid,
						rv.report_id,
						r.details,
						rv.id AS report_version_id,
						NULL::INTEGER AS module_id,
						util_get_translated( \'title\', r.title, r.details ) AS name,
						util_get_translated( \'description\', r.description, r.details ) AS description,
						\'{}\'::JSONB AS filters
					FROM
						default_reports dr
						JOIN default_report_versions drv ON dr.id = drv.default_report_id AND drv.is_default = TRUE
						' . $strReportColumnCondition . '
						' . $strReportFilterCondition . '
						JOIN reports r ON r.cid = ' . ( int ) $intCid . ' AND r.default_report_id = dr.id AND r.is_published = true ' . $strReportTitleCondition . '
						' . $strReportInstanceCondition . '
						JOIN report_versions rv ON rv.cid = r.cid AND rv.report_id = r.id
						LEFT JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
						CASE
							WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
							ELSE TRUE
						END
						AND
						CASE
							WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
							ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						END
						LEFT JOIN product_reports pr ON r.cid = pr.cid AND r.id = pr.report_id
					WHERE 
						r.cid = ' . ( int ) $intCid . ' 
						AND cu.id IS NOT NULL
						AND ( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
					ORDER BY
						dr.title
				) ri ON rv.cid = ri.cid AND rv.id = ri.report_version_id
				LEFT JOIN default_report_groups drg ON ri.default_report_group_id = drg.id
				LEFT JOIN modules m ON m.id = drg.parent_module_id
			WHERE 
				r.cid = ' . ( int ) $intCid . '
				' . $strCategoriesCondition . '
				' . $strCategoryNameCondition . '
				AND ri.row_number = 1
				AND CASE WHEN TRUE = rv.is_latest THEN COALESCE( rv.expiration, now() ) >= now() ELSE TRUE END
			GROUP BY
				m.id
			ORDER BY
				m.title
		';

		return self::fetchReportNewGroups( $strSql, $objDatabase );
	}

	public static function fetchDuplicateReportGroups( CreportNewGroup $objReportGroup, $objDatabase ) {
		$strGroupIdSql = NULL !== $objReportGroup->getId() ? ' AND rg.id != ' . $objReportGroup->getId() : '';
		$strSql = '
			SELECT
				*
			FROM
				report_new_groups rg
			WHERE
				rg.cid = ' . ( int ) $objReportGroup->getCid() . '
				AND rg.deleted_by IS NULL
				AND rg.report_group_type_id = ' . ( int ) $objReportGroup->getReportGroupTypeId() . '
				' . ( CReportGroupType::MY_REPORTS == $objReportGroup->getReportGroupTypeId() || CReportGroupType::QUICK_LINK == $objReportGroup->getReportGroupTypeId() ? 'AND rg.company_user_id = ' . ( int ) $objReportGroup->getCompanyUserId() : '' ) . '
				AND LOWER( rg.name ) = LOWER( ' . $objReportGroup->sqlName() . ' )' . $strGroupIdSql;

		return self::fetchReportNewGroups( $strSql, $objDatabase );
	}

	public static function fetchReportNewGroupsByCompanyUserIdByReportGroupTypeIdByCid( $intCompanyUserId, $intReportGroupTypeId, $intCid, $objDatabase ) {
		$strCompanyUserCondition = '';
		if( CReportGroupType::MY_REPORTS == $intReportGroupTypeId || CReportGroupType::QUICK_LINK == $intReportGroupTypeId ) {
			$strCompanyUserCondition = 'AND company_user_id = ' . ( int ) $intCompanyUserId;
		}

		$strSql = '
			SELECT
				*
			FROM
				report_new_groups
			WHERE
				cid = ' . ( int ) $intCid . '
				' . $strCompanyUserCondition . '
				AND report_group_type_id = ' . ( int ) $intReportGroupTypeId . '
				AND deleted_by IS NULL
			ORDER BY
				name';

		return self::fetchReportNewGroups( $strSql, $objDatabase );
	}

	public static function fetchReportNewGroupsWithScheduleDetailsByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						rni.id AS report_instance_id,
						rni.report_id,
						rni.report_version_id,
						rni.module_id,
						rs.id AS report_schedule_id,
						rng.id AS packet_id,
						rng.is_internal,
						util_get_translated( \'name\', rni.name, rni.details ) AS report_name,
						util_get_translated( \'name\', rng.name, rng.details ) AS packet_name,
						CASE
							WHEN rs.id IS NULL THEN 1
							ELSE rs.report_schedule_type_id
						END AS packet_type_id,
						rs.download_options,
						st.start_on
					FROM
						report_new_groups rng
						JOIN report_new_group_instances rngi ON ( rngi.cid = rng.cid AND rngi.report_new_group_id = rng.id AND rng.report_group_type_id = ' . CReportGroupType::PACKET . ' )
						JOIN report_new_instances rni ON ( rni.cid = rngi.cid AND rni.id = rngi.report_new_instance_id )
						LEFT JOIN report_schedules rs ON ( rs.cid = rni.cid AND rs.report_new_group_id = rng.id AND rs.deleted_by IS NULL AND strpos ( rs.download_options::TEXT, \'"is_internal": true\' )::INTEGER = 0 )
						LEFT JOIN scheduled_tasks st ON ( st.cid = rs.cid AND st.id = rs.scheduled_task_id )
					WHERE
						rni.cid = ' . ( int ) $intCid . '
						AND rng.id = ' . ( int ) $intId . '
						AND rni.deleted_by IS NULL
						AND st.deleted_by IS NULL
						AND rni.deleted_on IS NULL
						AND st.deleted_on IS NULL
						AND rngi.deleted_by IS NULL
					ORDER BY
						rngi.id';
	// Adding order by clause so that this listing should show instances in order in which user added those.
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportNewGroupsByNameByCid( $strGroupName, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				*
			FROM
				report_new_groups
			WHERE
				cid = ' . ( int ) $intCid . '
				AND name = ' . '\'' . $strGroupName . '\'
				AND deleted_by IS NULL';

		return self::fetchReportNewGroup( $strSql, $objDatabase );
	}

	public static function fetchReportNewPacketsByReportInstanceIdsByCid( array $arrintReportNewInstanceId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				rg.id,
				util_get_translated( \'name\', rg.name, rg.details ) AS name
			FROM
				report_new_instances ri
				JOIN report_new_group_instances rgi ON rgi.cid = ri.cid AND rgi.report_new_instance_id = ri.id
				JOIN report_new_groups rg ON rg.cid = rgi.cid AND rg.id = rgi.report_new_group_id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id IN ( ' . implode( ', ', $arrintReportNewInstanceId ) . ' )
				AND rg.report_group_type_id = ' . CReportGroupType::PACKET . '
				AND ri.deleted_by IS NULL
				AND rg.deleted_by IS NULL';

		return self::fetchReportNewGroups( $strSql, $objDatabase );

	}

	public static function fetchReportNewGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						rg.id,
						util_get_translated( \'name\', rg.name, rg.details ) AS report_group_name,
						rg.company_user_id AS user_id,
						ri.id AS report_instance_id,
						util_get_translated( \'name\', ri.name, ri.details ) AS name,
						r.id AS report_id,
						rv.id AS report_version_id,
						rv.major,
						rv.minor
					FROM
						report_new_groups AS rg
						JOIN report_new_group_instances AS rgi ON rgi.cid = rg.cid and rg.id = rgi.report_new_group_id
						JOIN report_new_instances AS ri ON ri.cid = rgi.cid AND rgi.report_new_instance_id = ri.id
						JOIN report_versions AS rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
						JOIN reports AS r ON r.cid = ri.cid AND r.id = ri.report_id
						LEFT JOIN LATERAL(
							SELECT
								rngi.report_new_instance_id
							FROM
								report_new_groups AS rng
								JOIN report_new_group_instances AS rngi ON rng.cid = rngi.cid AND rng.id = rngi.report_new_group_id 
							WHERE rng.report_group_type_id = ' . CReportGroupType::QUICK_LINK . '
								AND rngi.report_new_instance_id = ri.id
								AND rng.company_user_id = ' . ( int ) $intCompanyUserId . '
						) AS quick_link ON TRUE
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.report_group_type_id = ' . CReportGroupType::MY_REPORTS . '
						AND rg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND rg.deleted_on IS NULL
						AND ri.deleted_on IS NULL
						AND COALESCE( util_to_bool(rv.definition ->> \'is_published\'), TRUE) = TRUE
						AND COALESCE( r.is_published, TRUE ) = TRUE
						AND COALESCE( rv.expiration, CURRENT_DATE ) >= CURRENT_DATE
						AND quick_link.report_new_instance_id IS NOT NULL
					ORDER BY
						ri.name';

		return fetchData( $strSql, $objDatabase );
	}

}
