<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationAssociations
 * Do not add any new functions to this class.
 */

class CApplicationAssociations extends CBaseApplicationAssociations {

	public static function fetchApplicationAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationAssociation', $objDatabase, DATA_CACHE_MEMORY, NULL, false, $boolIsReturnKeyedArray );
	}

	public static function fetchApplicationAssociation( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationAssociation', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationAssociationByApplicationIdByCustomerAddressIdByCid( $intApplicationId, $intCustomerAddressId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApplicationId ) || false == valId( $intCustomerAddressId ) ) return NULL;

		$strSql = 'SELECT
						aa.*,
					    cal.address_type_id,
					    cal.customer_address_id
					FROM
						application_associations aa
						JOIN customer_address_logs cal
						ON ( aa.cid = cal.cid and aa.customer_data_type_reference_id = cal.id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND cal.customer_address_id = ' . ( int ) $intCustomerAddressId;

		return parent::fetchApplicationAssociation( $strSql, $objDatabase );
	}

    public static function fetchApplicationAssociationsByApplicationIdByCustomerDataTypeIdByCid( $intApplicationId, $intCustomerDataTypeId, $intCid, $objDatabase ) {

	    if( false == valId( $intApplicationId ) || false == valId( $intCustomerDataTypeId ) ) return NULL;

        $strSql = 'SELECT 
	                    *
                    FROM 
	                    application_associations aa 
                    WHERE
	                    aa.cid = ' . ( int ) $intCid . '
                        AND aa.application_id = ' . ( int ) $intApplicationId . '
                        AND aa.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId;

        return parent::fetchApplicationAssociations( $strSql, $objDatabase );
    }

	public static function fetchCountDuplicateApplicationAssociationByApplicationDetailsbyCustomerDetails( $intApplicationId, $intCid, $intLeaseIntervalId, $intCustomerDataTypeId, $intCustomerDataTypeReferenceId, $objDatabase ) : int {
		if( false === valId( $intApplicationId ) || false === valId( $intCid ) || false === valId( $intLeaseIntervalId ) || false === valId( $intCustomerDataTypeId ) || false === valId( $intCustomerDataTypeReferenceId ) ) return 0;

		$strSql = ' SELECT 
						count(1) 
					FROM 
						application_associations aa 
					WHERE 
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND aa.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId . '
						AND aa.customer_data_type_reference_id = ' . ( int ) $intCustomerDataTypeReferenceId . ' LIMIT 1';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true === valArr( $arrmixData ) ) {
			return $arrmixData[0]['count'];
		}
		return 0;
	}

}
?>