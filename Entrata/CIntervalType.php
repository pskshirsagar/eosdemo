<?php

class CIntervalType extends CBaseIntervalType {

	Protected $m_arrmixIntervalTypes;

	const MINUTES = 1;
	const HOURS = 2;
	const DAYS = 3;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getIntervalTypes() {
		if( true == isset( $this->m_arrmixIntervalTypes ) ) {
			return $this->m_arrmixIntervalTypes;
		}

		$this->m_arrmixIntervalTypes = [
			self::MINUTES => [ 'type' => __( 'minutes' ) ],
			self::HOURS   => [ 'type' => __( 'hours' ) ],
			self::DAYS    => [ 'type' => __( 'days' ) ]
		];

	return $this->m_arrmixIntervalTypes;
	}

	public function getIntervalTypeById( $intIntervalTypeId ) {
		$this->getIntervalTypes();
		return $this->m_arrmixIntervalTypes[$intIntervalTypeId]['type'] ?? '';
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>