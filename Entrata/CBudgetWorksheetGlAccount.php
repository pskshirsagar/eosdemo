<?php

class CBudgetWorksheetGlAccount extends CBaseBudgetWorksheetGlAccount {

	const PERCENTAGE_CONSTANT	= 100;

	protected $m_strGlAccountName;
	protected $m_strPropertyName;

	/**
	 * Get Functions
	 */

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * Set Functions
	 */

	public function setGlAccountName( $strGlAccountName ) {
		$this->set( 'm_strGlAccountName', CStrings::strTrimDef( $strGlAccountName, -1, NULL, true ) );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['gl_account_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strGlAccountName', trim( $arrmixValues['gl_account_name'] ) );
		} elseif( isset( $arrmixValues['gl_account_name'] ) ) {
			$this->setGlAccountName( $arrmixValues['gl_account_name'] );
		}

		if( isset( $arrmixValues['property_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strPropertyName', trim( $arrmixValues['property_name'] ) );
		} elseif( isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}

	}

	/**
	 * Val Functions
	 */

	public function valBudgetDataSourceTypeId( &$arrmixBudgetWorksheetGlAccountErrors ) {
		$boolIsValid = true;

		if( false == valId( $this->getBudgetDataSourceTypeId() ) ) {
			$boolIsValid = false;
			$arrmixBudgetWorksheetGlAccountErrors['data_source_gl_account_names'][] = $this->getGlAccountName();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_data_source_type_id', __( 'Budget data source type is required to GL account for property  {%s, 0}.', [ $this->getPropertyName() ] ) ) );
		}

		if( CBudgetDataSourceType::CUSTOM_FORMULAS == $this->getBudgetDataSourceTypeId() && false == valStr( $this->getFormula() ) ) {
			$boolIsValid = false;
			$arrmixBudgetWorksheetGlAccountErrors['custom_formula_gl_account_names'][] = $this->getGlAccountName();
		}

		return $boolIsValid;
	}

	public function valFormula( $arrstrBudgetWorkbookAssumptionKeys ) {
		$boolIsValid = true;

		if( false == valStr( $this->getFormula() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', __( 'Custom formula is required.' ) ) );
			return false;
		}

		$objExpressionEvaluator = new CExpressionEvaluator();
		$objExpressionEvaluator->addSymbolTable( 'last', $arrstrBudgetWorkbookAssumptionKeys, true );

		if( preg_match( '/([\+\-\*\/]\s*[\+\-\*\/])/', $this->getFormula() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', 'Please correct the formula.' ) );
		}

		if( false == $objExpressionEvaluator->validateExpression( $this->getFormula() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', $objExpressionEvaluator->getErrorMessage() ) );
		}

		return $boolIsValid;
	}

	public function valBudgetDataSourceSubTypeId( &$arrmixBudgetWorksheetGlAccountErrors ) {
		$boolIsValid = true;

		if( false == valId( $this->getBudgetDataSourceTypeId() ) ) {
			return $boolIsValid;
		}

		if( CBudgetDataSourceType::SYSTEM_DATA == $this->getBudgetDataSourceTypeId() ) {

			if( false == valId( $this->getBudgetDataSourceSubTypeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_data_source_sub_type_id', __( 'Pull initial data from is required to GL account for property  {%s, 0}.', [ $this->getPropertyName() ] ) ) );
				$arrmixBudgetWorksheetGlAccountErrors['pull_initial_data_from_gl_account_names'][] = $this->getGlAccountName();
			}

			if( self::PERCENTAGE_CONSTANT < abs( $this->getAdjustmentPercent() ) ) {
				$boolIsValid = false;
				$arrmixBudgetWorksheetGlAccountErrors['adjustment_percent'][] = $this->getGlAccountName();
			}
		}

		return $boolIsValid;
	}

	public function valGlAccountId() {

		$boolIsValid = true;

		if( false == valId( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			switch( $this->getBudgetWorksheetDataTypeId() ) {

				case CBudgetWorksheetDataType::BAD_DEBT:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Total bad debt GL account is required for property  {%s, 0}.', [ $this->getPropertyName() ] ) ) );
					break;

				case CBudgetWorksheetDataType::RECOVERABLE_BAD_DEBT:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Recoverable bad debt GL account is required for property  {%s, 0}.', [ $this->getPropertyName() ] ) ) );
					break;

				case CBudgetWorksheetDataType::CONCESSION:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Concession GL account is required for property  {%s, 0}.', [ $this->getPropertyName() ] ) ) );
					break;

				default:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is required.' ) ) );

			}
		}

		return $boolIsValid;

	}

	public function validate( $strAction, &$arrmixBudgetWorksheetGlAccountErrors = NULL, $arrmixParameters = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBudgetDataSourceTypeId( $arrmixBudgetWorksheetGlAccountErrors );
				$boolIsValid &= $this->valBudgetDataSourceSubTypeId( $arrmixBudgetWorksheetGlAccountErrors );
				break;

			case 'validate_formula':
				$boolIsValid &= $this->valFormula( $arrmixParameters['budget_workbook_assumption_keys'] );
				break;

			case 'validate_concession':
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valBudgetDataSourceTypeId( $arrmixBudgetWorksheetGlAccountErrors = [] );
				$boolIsValid &= $this->valBudgetDataSourceSubTypeId( $arrmixBudgetWorksheetGlAccountErrors = [] );
				break;

			case 'validate_unrecoverable_bad_debt':
				$boolIsValid &= $this->valGlAccountId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create functions
	 */

	public function createBudgetWorksheetItem() {
		$objBudgetWorksheetItem = new CBudgetWorksheetItem();
		$objBudgetWorksheetItem->setCid( $this->getCid() );
		$objBudgetWorksheetItem->setBudgetWorksheetId( $this->getBudgetWorksheetId() );
		$objBudgetWorksheetItem->setGlAccountId( $this->getGlAccountId() );
		$objBudgetWorksheetItem->setBudgetDataSourceTypeId( $this->getBudgetDataSourceTypeId() );

		return $objBudgetWorksheetItem;
	}

}
?>