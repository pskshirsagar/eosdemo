<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CRouteRule extends CBaseRouteRule {

	protected $m_intPropertyId;
	protected $m_intRouteRuleId;
	protected $m_intMaximumAmount;
	protected $m_intMinimumAmount;
	protected $m_intRouteRuleTypeId;

	protected $m_strJobCategoryName;

	protected $m_arrintPropertyIds;
	protected $m_arrintReferenceNumbers;

	protected $m_arrstrArCodeNames;
	protected $m_arrstrCompanyNames;
	protected $m_arrstrGlAccountNames;
	protected $m_arrstrJobCostCodes;
	protected $m_arrstrRoutingTagNames;
	protected $m_arrstrCompanyUserNames;
	protected $m_arrstrCompanyGroupNames;
	protected $m_arrstrInvoicePoTypeNames;

	protected $m_arrobjRuleStops;
	protected $m_arrobjRuleConditions;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseAllConditions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRouteId();
				break;

			case VALIDATE_DELETE:
			default:
				//
				break;
		}

		return $boolIsValid;
	}

	public function valDuplicateRuteRule( $arrobjCurrentRuleConditions, $objDatabase ) {

		$boolIsDuplicateRouteRule				= false;
		$arrintRouteRuleTypeIdsWithDetails		= CRouteRuleType::$c_arrintRouteRuleTypeIdsWithDetails;
		$arrintRouteRuleTypeIdsWithoutDetail	= CRouteRuleType::$c_arrintRouteRuleTypeIdsWithoutDetails;

		// s1: fetch all route rule of slected route
		$arrobjRouteRules = ( array ) \Psi\Eos\Entrata\CRouteRules::createService()->fetchActiveRouteRulesByRouteIdByCid( $this->getRouteId(), $this->getCid(), $objDatabase );

		if( true == is_numeric( $this->getId() ) ) {
			unset( $arrobjRouteRules[$this->getId()] );
		}

		if( false == valArr( $arrobjRouteRules ) ) return $boolIsDuplicateRouteRule;

		$arrobjRuleConditions = \Psi\Eos\Entrata\CRuleConditions::createService()->fetchRuleConditionsByRouteRuleIdsByCid( array_keys( $arrobjRouteRules ), $this->getCid(), $objDatabase );

		CObjectModifiers::createService()->nestObjects( $arrobjRuleConditions, $arrobjRouteRules );

		// s2: loop through the fetched route rule
		foreach( $arrobjRouteRules as $objRouteRule ) {

			// s3: check  if route rule reference type id matach
			if( $objRouteRule->getRouteTypeId() != $this->getRouteTypeId() ) continue;

			// s4: check use_all_conditions
			if( $objRouteRule->getUseAllConditions() != $this->getUseAllConditions() ) continue;

			// s5: if old rule have condition or not
			$arrobjOldRuleConditions = $objRouteRule->getRuleConditions();

			if( false == valArr( $arrobjOldRuleConditions ) ) continue;

			$arrobjOldRuleConditions = rekeyObjects( 'RouteRuleTypeId', $objRouteRule->getRuleConditions() );

			// s6: check  number of conditions are same in old and current.
			if( \Psi\Libraries\UtilFunctions\count( $arrobjCurrentRuleConditions ) != \Psi\Libraries\UtilFunctions\count( $arrobjOldRuleConditions ) ) continue;

			// s7: check both old and new rule has same conditions
			$arrmixDiffrance = array_diff_key( $arrobjCurrentRuleConditions, $arrobjOldRuleConditions );

			if( valArr( $arrmixDiffrance ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrmixDiffrance ) ) continue;

			// s8: Check the conditions detail.
			$intDuplicateConditionCounter = 0;

			foreach( $arrobjCurrentRuleConditions as $objCurrentRuleCondition ) {

				$objOldRuleCondition = getArrayElementByKey( $objCurrentRuleCondition->getRouteRuleTypeId(), $arrobjOldRuleConditions );

				if( false == valObj( $objOldRuleCondition, 'CRuleCondition' ) ) continue;

				// s9: check rule condition types without details
				if( true == in_array( $objCurrentRuleCondition->getRouteRuleTypeId(), $arrintRouteRuleTypeIdsWithoutDetail ) ) {

					// s10 : if rule condition is between then check both max and min else check only max amount.
					if( true == in_array( $objCurrentRuleCondition->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH, CRouteRuleType::INCREASE_BUDGET_BY, CRouteRuleType::INCREASE_THE_BUDGET_BY_PERCENT ] ) ) {

						if( $objCurrentRuleCondition->getMinimumAmount() == $objOldRuleCondition->getMinimumAmount() && $objCurrentRuleCondition->getMaximumAmount() == $objOldRuleCondition->getMaximumAmount() ) {
							$intDuplicateConditionCounter++;
						}

					} elseif( $objCurrentRuleCondition->getMaximumAmount() == $objOldRuleCondition->getMaximumAmount() ) {
						$intDuplicateConditionCounter++;
					}
				}

				// s11: check rule condition types with details
				if( true == in_array( $objCurrentRuleCondition->getRouteRuleTypeId(), $arrintRouteRuleTypeIdsWithDetails ) ) {

					$arrmixDiffranceDetail = array_intersect( $objCurrentRuleCondition->getArrayConditionDetails(), $objOldRuleCondition->getArrayConditionDetails() );

					if( true == valArr( $arrmixDiffranceDetail ) ) {
						$intDuplicateConditionCounter++;
					}
				}

				// s13 : if all rule-conditions of current and old route rule are matching then set $boolIsDuplicateRouteRule = true and break the foreach of routes
				if( $intDuplicateConditionCounter == \Psi\Libraries\UtilFunctions\count( $arrobjCurrentRuleConditions ) ) {
					$boolIsDuplicateRouteRule = true;
					break;
				}
			}
		}

		return $boolIsDuplicateRouteRule;
	}

	// Get Functions

	public function getRouteRuleTypeId() {
		return $this->m_intRouteRuleTypeId;
	}

	public function getMinimumAmount() {
		return $this->m_intMinimumAmount;
	}

	public function getMaximumAmount() {
		return $this->m_intMaximumAmount;
	}

	public function getGlAccountNames() {
		return $this->m_arrstrGlAccountNames;
	}

	public function getJobCostCodes() {
		return $this->m_arrstrJobCostCodes;
	}

	public function getCompanyUserNames() {
		return $this->m_arrstrCompanyUserNames;
	}

	public function getCompanyGroupNames() {
		return $this->m_arrstrCompanyGroupNames;
	}

	public function getRouteRuleId() {
		return $this->m_intRouteRuleId;
	}

	public function getRuleConditions() {
		return $this->m_arrobjRuleConditions;
	}

	public function getRuleStops() {
		return $this->m_arrobjRuleStops;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getArCodeNames() {
		return $this->m_arrstrArCodeNames;
	}

	public function getCompanyNames() {
		return $this->m_arrstrCompanyNames;
	}

	public function getAccountNames() {
		return $this->m_arrstrAccountNames;
	}

	public function getReferenceNumbers() {
		return $this->m_arrintReferenceNumbers;
	}

	public function getRoutingTagNames() {
		return $this->m_arrstrRoutingTagNames;
	}

	public function getJobCategoryName() {
		return $this->m_strJobCategoryName;
	}

	public function getFmoTypeByReferenceNumbers() {
		return CLeaseActionFinancialMoveOutLibrary::$c_arrstrFinancialMoveOutTypes[reset( json_decode( $this->m_arrintReferenceNumbers ) )];
	}

	public function getInvoicePoTypeNames() {
		return $this->m_arrstrInvoicePoTypeNames;
	}

	// Set Functions

	public function setRouteRuleTypeId( $intRouteRuleTypeId ) {
		$this->m_intRouteRuleTypeId = $intRouteRuleTypeId;
	}

	public function setMinimumAmount( $intMinimumAmount ) {
		$this->m_intMinimumAmount = $intMinimumAmount;
	}

	public function setMaximumAmount( $intMaximumAmount ) {
		$this->m_intMaximumAmount = $intMaximumAmount;
	}

	public function setGlAccountNames( $arrstrGlAccountNames ) {
		$this->m_arrstrGlAccountNames = $arrstrGlAccountNames;
	}

	public function setCompanyUserNames( $arrstrCompanyUserNames ) {
		$this->m_arrstrCompanyUserNames = $arrstrCompanyUserNames;
	}

	public function setCompanyGroupNames( $arrstrCompanyGroupNames ) {
		$this->m_arrstrCompanyGroupNames = $arrstrCompanyGroupNames;
	}

	public function setRouteRuleId( $intRouteRuleId ) {
		$this->m_intRouteRuleId = $intRouteRuleId;
	}

	public function setRuleCondition( $objRuleCondition ) {
		$this->m_arrobjRuleConditions[$objRuleCondition->getId()] = $objRuleCondition;
	}

	public function setRuleStop( $objRuleStop ) {
		$this->m_arrobjRuleStops[$objRuleStop->getId()] = $objRuleStop;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setArCodeNames( $arrstrArCodeNames ) {
		$this->m_arrstrArCodeNames = $arrstrArCodeNames;
	}

	public function setCompanyNames( $arrstrCompanyNames ) {
		$this->m_arrstrCompanyNames = $arrstrCompanyNames;
	}

	public function setAccountNames( $arrstrAccountNames ) {
		$this->m_arrstrAccountNames = $arrstrAccountNames;
	}

	public function setJobCostCodes( $arrstrJobCostCodes ) {
		$this->m_arrstrJobCostCodes = $arrstrJobCostCodes;
	}

	public function setReferenceNumbers( $arrintReferenceNumbers ) {
		$this->m_arrintReferenceNumbers = $arrintReferenceNumbers;
	}

	public function setMaxOrderNumber( $objClientDatabase ) {

		$objRouteRule = \Psi\Eos\Entrata\CRouteRules::createService()->fetchLastOrderNumberByRouteIdByCid( $this->m_intRouteId, $this->getCid(), $objClientDatabase );
		if( true == valObj( $objRouteRule, 'CRouteRule' ) ) {
			$this->m_intOrderNum = $objRouteRule->getOrderNum() + 1;
		} else {
			$this->m_intOrderNum = 1;
		}
	}

	public function setRoutingTagNames( $arrstrRoutingTagNames ) {
		$this->m_arrstrRoutingTagNames = $arrstrRoutingTagNames;
	}

	public function setJobCategoryName( $strJobCategoryName ) {
		$this->m_strJobCategoryName = $strJobCategoryName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['route_rule_type_id'] ) )			$this->setRouteRuleTypeId( $arrmixValues['route_rule_type_id'] );
		if( true == isset( $arrmixValues['minimum_amount'] ) )				$this->setMinimumAmount( $arrmixValues['minimum_amount'] );
		if( true == isset( $arrmixValues['maximum_amount'] ) )				$this->setMaximumAmount( $arrmixValues['maximum_amount'] );
		if( true == isset( $arrmixValues['gl_account_names'] ) )			$this->setGlAccountNames( $arrmixValues['gl_account_names'] );
		if( true == isset( $arrmixValues['company_user_names'] ) )			$this->setCompanyUserNames( $arrmixValues['company_user_names'] );
		if( true == isset( $arrmixValues['route_rule_id'] ) )				$this->setRouteRuleId( $arrmixValues['route_rule_id'] );
		if( true == isset( $arrmixValues['property_id'] ) )					$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['company_names'] ) )				$this->setCompanyNames( $arrmixValues['company_names'] );
		if( true == isset( $arrmixValues['ar_code_names'] ) )				$this->setArCodeNames( $arrmixValues['ar_code_names'] );
		if( true == isset( $arrmixValues['account_names'] ) )				$this->setAccountNames( $arrmixValues['account_names'] );
		if( true == isset( $arrmixValues['reference_numbers'] ) )			$this->setReferenceNumbers( $arrmixValues['reference_numbers'] );
		if( true == isset( $arrmixValues['routing_tag_names'] ) )			$this->setRoutingTagNames( $arrmixValues['routing_tag_names'] );
		if( true == isset( $arrmixValues['job_type_name'] ) )				$this->setJobTypeName( $arrmixValues['job_type_name'] );
		if( true == isset( $arrmixValues['job_cost_codes'] ) )			    $this->setJobCostCodes( $arrmixValues['job_cost_codes'] );
		if( true == isset( $arrmixValues['invoice_po_type_names'] ) )	    $this->setInvoicePoTypeNames( $arrmixValues['invoice_po_type_names'] );
		if( true == isset( $arrmixValues['job_category_name'] ) )			$this->setJobCategoryName( $arrmixValues['job_category_name'] );
		return;
	}

	public function setInvoicePoTypeNames( $arrstrInvoicePOTypeNames ) {
		$this->m_arrstrInvoicePoTypeNames = $arrstrInvoicePOTypeNames;
	}

	// Add Functions

	public function addRuleCondition( $objRuleCondition ) {
		$this->m_arrobjRuleConditions[$objRuleCondition->getId()] = $objRuleCondition;
	}

	public function addRuleStop( $objRuleStop ) {
		$this->m_arrobjRuleStops[$objRuleStop->getId()] = $objRuleStop;
	}

	public function addPropertyId( $intPropertyId ) {
		$this->m_arrintPropertyIds[$intPropertyId] = $intPropertyId;
	}

	// Fetch Functions

	public function fetchRuleConditions( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRuleConditions::createService()->fetchAssociatedRuleConditionsByRouteRuleIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRuleStops( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchCustomRuleStopsByRouteRuleIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRouteRuleAssociations( $intReferenceId, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRouteRuleAssociations::createService()->fetchAllApprovedRuleStopResultsByReferenceIdByRouteRuleIdByRouteTypeIdByCid( $intReferenceId, $this->getId(), $this->getRouteTypeId(), $this->getCid(), $objClientDatabase );
	}

	// Create Functions

	public function createRuleCondition() {
		$objRuleCondition = new CRuleCondition();

		$objRuleCondition->setRouteId( $this->getRouteId() );
		$objRuleCondition->setRouteRuleId( $this->getId() );
		$objRuleCondition->setCid( $this->getCid() );

		return $objRuleCondition;
	}

	public function createRuleStop() {
		$objRuleStop = new CRuleStop();

		$objRuleStop->setRouteId( $this->getRouteId() );
		$objRuleStop->setRouteRuleId( $this->getId() );
		$objRuleStop->setCid( $this->getCid() );

		return $objRuleStop;
	}

}
?>