<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CHeaderFooterOptionTypes
 * Do not add any new functions to this class.
 */

class CHeaderFooterOptionTypes extends CBaseHeaderFooterOptionTypes {

	public static function fetchHeaderFooterOptionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CHeaderFooterOptionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchHeaderFooterOptionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CHeaderFooterOptionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedHeaderFooterOptionTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						header_footer_option_types
					WHERE
						is_published = TRUE';

		return parent::fetchHeaderFooterOptionTypes( $strSql, $objDatabase );
	}
}
?>