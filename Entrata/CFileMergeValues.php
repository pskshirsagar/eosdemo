<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMergeValues
 * Do not add any new functions to this class.
 */

class CFileMergeValues extends CBaseFileMergeValues {

	public static function fetchFileMergeValueCountByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValueCount( 'WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchFileMergeValueCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValueCount( 'WHERE lease_id = ' . ( int ) $intLeaseId . ' AND cid = ' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						application_id IS NOT NULL
						AND file_id IS NULL
						AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByDocumentIdsByCidByPropertyIds( $arrintDocumentIds, $intCid, $arrintDeletingPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		if( true == valArr( $arrintDeletingPropertyIds ) ) {
			$strJoin 	= ' JOIN applications aa ON ( fm.application_id = aa.id AND fm.cid = aa.cid ) ';
			$strWhere	= ' AND aa.property_id IN ( ' . implode( ',', $arrintDeletingPropertyIds ) . ' ) ';
		} else {
			$strJoin 	= '';
			$strWhere	= '';
		}

		$strSql = 'SELECT
						fm.*
					FROM
						file_merge_values fm
						' . $strJoin . '
					WHERE
						fm.application_id IS NOT NULL
						AND fm.file_id IS NULL
						AND fm.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND fm.cid = ' . ( int ) $intCid . $strWhere;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchDocumentIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT value FROM file_merge_values WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . ' AND field = \'selected_document_ids\' AND file_id IS NULL LIMIT 1';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrintDocumentIds = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrintDocumentIds = explode( ',', $arrmixValue['value'] );
			}
		}

		return $arrintDocumentIds;
	}

	public static function fetchDocumentIdsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT value FROM file_merge_values WHERE lease_id = ' . ( int ) $intLeaseId . ' AND cid = ' . ( int ) $intCid . ' AND field = \'selected_document_ids\' AND file_id IS NULL LIMIT 1';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrintDocumentIds = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrintDocumentIds = explode( ',', $arrmixValue['value'] );
			}
		}

		return $arrintDocumentIds;
	}

	public static function fetchFileMergeValuesByApplicationIdByFileIdsByCid( $intApplicationId, $arrintFileIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT * FROM file_merge_values WHERE cid = ' . ( int ) $intCid . ' AND application_id = ' . ( int ) $intApplicationId . ' AND file_id IN ( ' . implode( $arrintFileIds ) . ' )';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByApplicationIdByFieldsByCid( $intApplicationId, $arrstrFields, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND field IN ( \'' . implode( '\',\'', $arrstrFields ) . '\' )
						AND file_id IS NULL ';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByLeaseIdByFieldsByCid( $intLeaseId, $arrstrFields, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
						AND field IN ( \'' . implode( '\',\'', $arrstrFields ) . '\' )
						AND file_id IS NULL ';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND file_id IS NULL
					ORDER BY id ASC';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchSavedFileMergeValuesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
						AND file_id IS NULL
						AND application_id IS NULL
					ORDER BY id ASC';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentIdByApplicationIdByCid( $intDocumentId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						document_id = ' . ( int ) $intDocumentId . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						application_id IN (' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						file_id IN (' . implode( ',', $arrintFileIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentIdByLeaseIdByCid( $intDocumentId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						document_id = ' . ( int ) $intDocumentId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchLatestFileMergeValuesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'WITH file_merge_valuse_created_on AS (
						SELECT
							id,
							application_id,
							cid,
							created_on
						FROM
							file_merge_values
						WHERE
							application_id = ' . ( int ) $intApplicationId . '
							AND cid = ' . ( int ) $intCid . '
							AND file_id IS NOT NULL
						ORDER BY id DESC
						LIMIT 1
					)

					SELECT
						fmv.*
					FROM
						file_merge_values fmv,
						file_merge_valuse_created_on fmvc
					WHERE
						fmv.application_id = ' . ( int ) $intApplicationId . '
						AND fmv.cid = ' . ( int ) $intCid . '
						AND fmv.file_id IS NOT NULL
						AND fmv.created_on = fmvc.created_on';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentIdsByFieldsByCId( $arrintDocumentIds, $arrstrFields, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						document_id in ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND field IN ( \'' . implode( '\',\'', $arrstrFields ) . '\' )
						AND file_id IS NULL ';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentAddendaIdsByCId( $arrintDocumentAddendaIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentAddendaIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						file_merge_values
					WHERE
						document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND file_id IS NULL ';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

	public static function fetchLatestSavedFileMergeValueByApplicationIdByDocumentIdByFieldByCid( $intApplicationId, $intDocumentId, $strField, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						fmv.*
					FROM
						file_merge_values fmv
						JOIN files f ON(fmv.cid = f.cid AND fmv.file_id = f.id)
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id AND fa.deleted_on IS NULL )
					WHERE
						fmv.application_id = ' . ( int ) $intApplicationId . '
						AND fmv.document_id = ' . ( int ) $intDocumentId . '
						AND fmv.field = \'' . ( string ) $strField . '\'
						AND fmv.cid = ' . ( int ) $intCid . '
					ORDER BY FMV.created_on DESC LIMIT 1';

		return self::fetchFileMergeValue( $strSql, $objDatabase );
	}

	public static function fetchLatestSavedFileMergeValuesByApplicationIdByDocumentIdsByCid( $intApplicationId, $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						fmv.*
					FROM
						file_merge_values fmv
						JOIN files f ON(fmv.cid = f.cid AND fmv.file_id = f.id)
						JOIN file_associations fa ON ( f.cid = fa.cid AND f.id = fa.file_id AND fa.deleted_on IS NULL )
					WHERE
						fmv.application_id = ' . ( int ) $intApplicationId . '
						AND fmv.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND fmv.cid = ' . ( int ) $intCid . '
					ORDER BY FMV.created_on DESC';

		return self::fetchFileMergeValues( $strSql, $objDatabase );
	}

}
?>