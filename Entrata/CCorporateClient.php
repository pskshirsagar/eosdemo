<?php

use Psi\Libraries\UtilHash\CHash;

class CCorporateClient extends CBaseCorporateClient {

	protected $m_boolIsLoggedIn;

    /**
     * Set Functions
     */

    public function setPasswordAnswerEncrypted( $strPasswordAnswer ) {

    	$this->m_strPasswordAnswerEncrypted = CHash::createService()->hashPasswordAnswer( $strPasswordAnswer );
    }

    /**
     * Validate Functions
     */

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBilltoCompanyName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strBilltoCompanyName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_company_name', 'Bill to company name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUsername( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_strUsername ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is required.' ) );
        } elseif( false == is_null( $this->m_strUsername ) ) {

        	if( false == preg_match( '/^[\w\d\_\.]{4,}$/',  $this->m_strUsername ) ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username should have atlease 4 alphanumeric characters.' ) );
				return false;
        	}

        	$intPreExistingUserCount = CCorporateClients::fetchCompetingUserCountByUserNameByIdByCid( $this->m_strUsername, $this->m_intId, $this->getCid(),  $objDatabase );

			if( 0 < $intPreExistingUserCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is already in use.' ) );
			}
        }

        return $boolIsValid;
    }

    public function valPassword() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strPassword ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
        } elseif( false == is_null( $this->m_strPassword ) ) {
        	if( false == preg_match( '/^[\w\d\_\*\@\#\$\%\^\&\*\(\)\-\+\=\{\}\[\]\;\;\:\,\<\>\?\.]{4,}$/',  $this->m_strPassword ) ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Invalid character used in a password.' ) );
				return false;
        	}

        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            		$boolIsValid &= $this->valBilltoCompanyName();
            		$boolIsValid &= $this->valUsername( $objDatabase );
            		$boolIsValid &= $this->valPassword();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function login( $objDatabase ) {

		$objCorporateClient = CCorporateClients::fetchCorporateClientByUsernamePasswordCid( $this->m_strUsername, $this->m_strPassword, $this->m_intCid, $objDatabase );

		if( true == valObj( $objCorporateClient, 'CCorporateClient' ) ) {
			$this->m_intId 						= $objCorporateClient->getId();
			$this->m_intDontAllowLogin 			= $objCorporateClient->getDontAllowLogin();
			$this->m_intDontAcceptPayments 		= $objCorporateClient->getDontAcceptPayments();
			$this->m_intInPoorStanding 			= $objCorporateClient->getInPoorStanding();
			$this->m_intIsResponsibleForPayment = $objCorporateClient->getIsResponsibleForPayment();
			$this->m_strLastLogin 				= $objCorporateClient->getLastLogin();
			$this->m_boolIsLoggedIn 			= true;
		} else {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid username and/or password', 304 ) );
            return false;
		}

		return $objCorporateClient;
	}
}
?>