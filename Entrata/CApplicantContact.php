<?php

class CApplicantContact extends CBaseApplicantContact {

	protected $m_intPropertyId;
	protected $m_intApplicationId;
	protected $m_intCurrentApplicationStepId;
	protected $m_intCustomerApplicantId;
	protected $m_boolIsNew;

    /**
     * Get Functions
     */

    public function getIsNew() {
    	return $this->m_boolIsNew;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getApplicationId() {
    	return $this->m_intApplicationId;
    }

    public function getCurrentApplicationStepId() {
    	return $this->m_intCurrentApplicationStepId;
    }

    /**
     * Set Functions
     */

    public function setIsNew( $boolIsNew ) {
    	$this->m_boolIsNew = $boolIsNew;
    }

    public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = ( 0 == strlen( $strStreetLine1 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) ) ) );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = ( 0 == strlen( $strStreetLine2 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) ) ) );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = ( 0 == strlen( $strStreetLine3 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) ) ) );
	}

	public function setCity( $strCity ) {
		$this->m_strCity = ( 0 == strlen( $strCity ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCity, 50, NULL, true ) ) ) );
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = ( 0 == strlen( $strNameFirst ) ? NULL : CStrings::getFormattedName( $strNameFirst ) );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = ( 0 == strlen( $strNameLast ) ? NULL : CStrings::getFormattedName( $strNameLast ) );
	}

	public function setRelationship( $strRelationship ) {
		$this->m_strRelationship = ( 0 == strlen( $strRelationship ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strRelationship, 240, NULL, true ) ) ) );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = $intCurrentApplicationStepId;
	}

	public function setCustomerApplicantId( $intCustomerApplicantId ) {
		$this->m_intCustomerApplicantId = CStrings::strToIntDef( $intCustomerApplicantId, NULL, false );
	}

	public function getCustomerApplicantId() {
		return $this->m_intCustomerApplicantId;
	}

    /**
     * Validate Functions
     */

	public function mapCorrespondingCustomerContactData( $objCustomerContact ) {

		$this->setCustomerContactTypeId( $objCustomerContact->getCustomerContactTypeId() );
		$this->setNameFirst( $objCustomerContact->getNameFirst() );
		$this->setNameLast( $objCustomerContact->getNameLast() );
		$this->setPhoneNumber( $objCustomerContact->getPhoneNumber() );
		$this->setMobileNumber( $objCustomerContact->getMobileNumber() );
		$this->setFaxNumber( $objCustomerContact->getFaxNumber() );
		$this->setEmailAddress( $objCustomerContact->getEmailAddress() );
		$this->setRelationship( $objCustomerContact->getRelationship() );
		$this->setStreetLine1( $objCustomerContact->getStreetLine1() );
		$this->setStreetLine2( $objCustomerContact->getStreetLine2() );
		$this->setStreetLine3( $objCustomerContact->getStreetLine3() );
		$this->setCity( $objCustomerContact->getCity() );
		$this->setStateCode( $objCustomerContact->getStateCode() );
		$this->setProvince( $objCustomerContact->getProvince() );
		$this->setPostalCode( $objCustomerContact->getPostalCode() );
		$this->setCountryCode( $objCustomerContact->getCountryCode() );
		$this->setAge( $objCustomerContact->getAge() );
		$this->setHasAccessToUnit( ( $objCustomerContact->getHasAccessToUnit() ) ? $objCustomerContact->getHasAccessToUnit() : 0 );
	}

    public function validate( $strAction, $strCustomerContactType = NULL, $strSectionName, $arrobjPropertyApplicationPreferences = NULL, $boolIsValidateRequired = true ) {
        $boolIsValid = true;

		$objApplicantContact = new CApplicantContactValidator();
		$objApplicantContact->setApplicantContact( $this );

		$boolIsValid &= $objApplicantContact->validate( $strAction, $strCustomerContactType, $strSectionName, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired );

        return $boolIsValid;
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intPsProductId = NULL ) {

		$arrintResponse = fetchData( 'SELECT * FROM applicant_contacts WHERE customer_contact_type_id = ' . ( int ) $this->getCustomerContactTypeId() . ' AND applicant_id = ' . ( int ) $this->getApplicantId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
		$this->setSerializedOriginalValues( serialize( $arrintResponse[0] ) );

		$objApplicationLogLibrary = new CApplicationLogLibrary();
		if( true == $boolReturnSqlOnly ) {
			$strSql = '';

			$strSql .= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction = VALIDATE_UPDATE, $arrstrOriginalValues = NULL, $intPsProductId );

			return $strSql;
		} else {
			$boolIsValid = true;

			$boolIsValid &= parent::insert( $intCurrentUserId, $objDatabase );
			$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly = false, $strApplicationAction = VALIDATE_UPDATE, $arrstrOriginalValues = NULL, $intPsProductId );

			return $boolIsValid;
		}
	}

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intPsProductId = NULL ) {

    	$arrintResponse = fetchData( 'SELECT * FROM applicant_contacts WHERE id = ' . ( int ) $this->getId() . ' AND customer_contact_type_id = ' . ( int ) $this->getCustomerContactTypeId() . ' AND applicant_id = ' . ( int ) $this->getApplicantId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
		$this->setSerializedOriginalValues( serialize( $arrintResponse[0] ) );

    	$objApplicationLogLibrary = new CApplicationLogLibrary();
    	if( true == $boolReturnSqlOnly ) {
    		$strSql = '';

    		$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction = VALIDATE_UPDATE, $arrstrOriginalValues = NULL, $intPsProductId );

    		return $strSql;
    	} else {
    		$boolIsValid = true;

    		$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase );
    		$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly = false, $strApplicationAction = VALIDATE_UPDATE, $arrstrOriginalValues = NULL, $intPsProductId );

    		return $boolIsValid;
    	}
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intPsProductId = NULL ) {

    	if( false == valId( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $intPsProductId );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $intPsProductId );
    	}
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['customer_applicant_id'] ) ) $this->setCustomerApplicantId( $arrmixValues['customer_applicant_id'] );

	}

}
?>