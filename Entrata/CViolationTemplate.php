<?php
use Psi\Eos\Entrata\CViolationTemplates;

class CViolationTemplate extends CBaseViolationTemplate {

	public function createViolationTemplateNotice() {
		$objViolationTemplateNotice = new CViolationTemplateNotice();
		$objViolationTemplateNotice->setCid( $this->getCid() );
		$objViolationTemplateNotice->setViolationTemplateId( $this->getId() );

		return $objViolationTemplateNotice;
	}

	public function createViolation() {
		$objViolation = new CViolation();
		$objViolation->setCid( $this->getCid() );
		$objViolation->setViolationTemplateId( $this->getId() );

		return $objViolation;
	}

	/*
     * Setter functions
	 */

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	/*
     * Getter functions
	 */

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_ids'] ) ) {
			$this->setPropertyIds( $arrmixValues['property_ids'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		$objViolationTemplate = CViolationTemplates::createService()->fetchViolationTemplateByCidByName( $this->m_intCid, $this->getName(), $objDatabase );
		if( true == is_object( $objViolationTemplate ) && true == valObj( $objViolationTemplate, 'CViolationTemplate' ) && $objViolationTemplate->getId() != $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( ' Template {%s, 0} already exists.', [ $objViolationTemplate->getName() ] ) ) );
		} elseif( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Violation name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valViolationTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getViolationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'violation_type_id', __( 'Violation type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getOccupancyTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_type_ids', __( 'Occupancy type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnresolvedViolation( $objDatabase ) {
		$boolIsValid = true;

		$intCount = CViolationTemplates::createService()->fetchSimpleUnresolvedViolationCountByCidByTemplateId( $this->getCid(), $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resolved_on',  __( 'You cannot delete a violations template with unresolved violations pending.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPropertyGroupIds( $objDatabase ) {
		$boolIsValid = true;
		$intViolationsCount = Psi\Eos\Entrata\CViolations::createService()->fetchCustomViolationsCountByViolationTemplateIdByPropertyIdsByCid( $this->getId(), $this->getPropertyIds(), $this->getCid(), $objDatabase );

		if( true == valId( $intViolationsCount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  __( 'You cannot delete a property from violation template since violations associated to it.' ) ) );
		}
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsViolationExist = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valViolationTypeId();
				$boolIsValid &= $this->valOccupancyTypeIds();
				if( true == $boolIsViolationExist ) {
					$boolIsValid &= $this->valPropertyGroupIds( $objDatabase );
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valUnresolvedViolation( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );
		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>