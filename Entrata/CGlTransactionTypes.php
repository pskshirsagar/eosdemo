<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTransactionTypes
 * Do not add any new functions to this class.
 */

class CGlTransactionTypes extends CBaseGlTransactionTypes {

	public static function fetchGlTransactionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlTransactionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllGlTransactionTypes( $objDatabase, $boolIsPublished = false ) {
		$strSql = 'SELECT * FROM gl_transaction_types ORDER BY order_num';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_transaction_types WHERE is_published = 1 ORDER BY id';
		}

		return self::fetchGlTransactionTypes( $strSql, $objDatabase );
	}
}
?>