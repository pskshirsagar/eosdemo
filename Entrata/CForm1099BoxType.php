<?php

class CForm1099BoxType extends CBaseForm1099BoxType {

	const FORM_1099_BOX_1		= 89;
	const FORM_1099_BOX_4 		= 4;
	const FORM_1099_BOX_5A 		= 5;
	const FORM_1099_BOX_5B		= 6;
	const FORM_1099_BOX_6A		= 7;
	const FORM_1099_BOX_6B		= 8;
	const FORM_1099_BOX_7A		= 9;
	const FORM_1099_BOX_7B		= 10;

	const FORM_1099_BOX_2		= 2;
	const FORM_1099_BOX_13		= 11;
	const FORM_1099_BOX_14		= 12;
	const FORM_1099_BOX_15A 	= 13;
	const FORM_1099_BOX_15B		= 14;
	const FORM_1099_BOX_16A		= 15;
	const FORM_1099_BOX_16B		= 16;
	const FORM_1099_BOX_17A		= 17;
	const FORM_1099_BOX_17B		= 18;
	const FORM_1099_BOX_18A		= 19;
	const FORM_1099_BOX_18B		= 20;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>