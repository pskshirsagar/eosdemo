<?php

class CPropertySmsMessage extends CBasePropertySmsMessage {

	protected $m_strScheduledSendDate;
	protected $m_strScheduledSendTime;

	public function __construct() {
		parent::__construct();

		// $this->m_strScheduledSendTime = '09';
		$this->m_strScheduledSendDate = date( 'm/d/Y' );

		return;
	}

	/**
	 * Get Functions
	 */

	public function getScheduledSendDate() {
		return $this->m_strScheduledSendDate;
	}

	public function getScheduledSendTime() {
		return $this->m_strScheduledSendTime;
	}

	/**
	 * Set Functions
	 */

	public function setScheduledSendDate( $strScheduledSendDate ) {
		$this->m_strScheduledSendDate = CStrings::strTrimDef( $strScheduledSendDate, -1, NULL, true );
	}

	public function setScheduledSendTime( $strScheduledSendTime ) {
		$this->m_strScheduledSendTime = CStrings::strTrimDef( $strScheduledSendTime, -1, NULL, true );
	}

 	public function setScheduledSendDatetime( $strScheduledSendDatetime ) {
 		parent::setScheduledSendDatetime( $strScheduledSendDatetime );
 		$this->m_strScheduledSendDate = date( 'm/d/Y', strtotime( $strScheduledSendDatetime ) );
 		$this->m_strScheduledSendTime = date( 'H', strtotime( $strScheduledSendDatetime ) );
	}

	public function resetScheduledSendDatetime() {
		if( 0 < strlen( $this->m_strScheduledSendDate ) && 0 < strlen( $this->m_strScheduledSendTime ) ) {
			parent::setScheduledSendDatetime( date( 'Y-m-d H:i:s', ( strtotime( $this->m_strScheduledSendDate ) + ( 3600 * ( int ) $this->m_strScheduledSendTime ) ) ) );
		}
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['scheduled_send_date'] ) ) $this->setScheduledSendDate( $arrstrValues['scheduled_send_date'] );
		if( true == isset( $arrstrValues['scheduled_send_time'] ) ) $this->setScheduledSendTime( $arrstrValues['scheduled_send_time'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Valid ID is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client ID is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
		$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property ID is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMessage() {

		$boolIsValid = true;

		if( 0 == strlen( trim( $this->getMessage() ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message is required.' ) ) );

		} elseif( ( CMessage::SMS_LENGTH - 20 ) < \Psi\CStringService::singleton()->strlen( ( trim( $this->getMessage() ) ) ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message should not exceed 140 characters' ) ) );
		}

		return $boolIsValid;
	}

	public function valMessageTypeId() {

		$boolIsValid = true;

		if( 0 == strlen( trim( $this->getMessageTypeId() ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_type_id', __( 'Subscription List selection is required.' ) ) );

		}
		return $boolIsValid;
	}

	public function valScheduledSendDate() {
		$boolIsValid = true;
		$arrintSendDateYear = explode( '/', $this->getScheduledSendDate() );

		// Validation example
		if( true == is_null( $this->getScheduledSendDate() ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', __( 'Send date is required.' ) ) );

		} elseif( $arrintSendDateYear[2] > ( date( 'Y' ) + 10 ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', __( 'Send date year should not be greater than' ) . ' ' . ( date( 'Y' ) + 10 ) ) );

		} elseif( strtotime( $this->getScheduledSendDate() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', __( 'Send date should be greater than current date.' ) ) );

		} elseif( ( 0 != date( 'G', strtotime( $this->getScheduledSendDatetime() ) ) ) && ( strtotime( $this->getScheduledSendDatetime() ) < time() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_date', __( 'Send time should be greater than current time.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valScheduledSendDate();
				$boolIsValid &= $this->valMessageTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function fetchRecipientCustomers( $objDatabase ) {

		$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::FUTURE ];

		$strSql = 'SELECT DISTINCT ON (c.id)
						c.*
					FROM
						customers c,
						lease_customers lc,
						leases l,
						sms_enrollments se
					WHERE
						c.id = lc.customer_id
						AND c.cid = lc.cid
						AND lc.lease_id = l.id
						AND lc.cid = l.cid
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND l.property_id = ' . ( int ) $this->getPropertyId() . '
						AND c.cid = ' . ( int ) $this->getCid() . '
						AND se.customer_id = c.id
						AND se.cid = c.cid
						AND c.mobile_number IS NOT NULL
						AND se.message_type_id = ' . ( int ) CMessageType::PROPERTY_NOTIFICATION . '
						AND se.disabled_on IS NULL ';

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomers( $strSql, $objDatabase );
	}

	public function fetchRecipientApplicants( $objDatabase, $boolIncludeDeletedAA = false ) {

		$arrintApplicationStageStatusIds = [
            CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::COMPLETED ],
            CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
            CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
		];

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (appt.id)
						appt.*,
						apps.lease_id,
						apps.lease_interval_id
					FROM
						applicants AS appt
						JOIN applicant_applications AS aa ON (aa.applicant_id = appt.id AND aa.cid = appt.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications AS apps ON (apps.id = aa.application_id AND apps.cid = appt.cid )
						JOIN applicant_details ad ON ( ad.applicant_id = appt.id AND ad.cid = appt.cid )
					WHERE
						appt.cid = ' . ( int ) $this->getCid() . '
						AND apps.property_id = ' . ( int ) $this->getPropertyId() . '
						AND (apps.application_stage_id,apps.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND ad.sms_activated_on IS NOT NULL
						AND ad.sms_cancelled_on IS NULL';

		return CApplicants::fetchApplicants( $strSql, $objDatabase );
	}

}
?>