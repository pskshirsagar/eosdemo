<?php

class CReportDocumentType extends CBaseReportDocumentType {

	const JSON	= 1;
	const XLSX	= 2;
	const PDF	= 3;
	const CSV	= 4;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function getReportDocumentTypeIdByOutputType( $strOutputType ) {

		switch( $strOutputType ) {

			case CReportRenderer::OUTPUT_SCREEN:
				return self::REPORT_DOCUMENT_TYPE_JSON;

			case CReportRenderer::OUTPUT_PDF:
				return self::REPORT_DOCUMENT_TYPE_PDF;

			case CReportRenderer::OUTPUT_SPREADSHEET:
				return self::REPORT_DOCUMENT_TYPE_XLSX;

			case CReportRenderer::OUTPUT_CSV:
				return self::REPORT_DOCUMENT_TYPE_CSV;

			default:
				// Unknown output type
				return NULL;
		}
	}

	public function getFileExtensionById( $intReportDocumentTypeId ) {

		switch( $intReportDocumentTypeId ) {

			case self::JSON:
				return 'json';

			case self::XLSX:
				return 'xlsx';

			case self::PDF:
				return 'pdf';

			case self::CSV:
				return 'csv';

			default:
				// Unknown file type
				return NULL;
		}
	}

}
?>