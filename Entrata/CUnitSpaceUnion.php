<?php

class CUnitSpaceUnion extends CBaseUnitSpaceUnion {

	const ALL_LEASE_TERMS = 'all_lease_terms';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseTermId( $boolIsMilitaryProperty ) {
        $boolIsValid = true;
        if( false == $boolIsMilitaryProperty && false == valId( $this->getLeaseTermId() ) ) {
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term_id', __( 'Lease term id is required for Student property.' ) ) );
	        $boolIsValid = false;
        }
        return $boolIsValid;
    }

	public function valLeaseStartWindowId( $boolIsMilitaryProperty ) {
		$boolIsValid = true;
		if( false == $boolIsMilitaryProperty && false == valId( $this->getLeaseStartWindowId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_window_id', __( 'Lease start window id is required for Student property..' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valGroupUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnionDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $boolIsMilitaryProperty = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLeaseTermId( $boolIsMilitaryProperty );
				$boolIsValid &= $this->valLeaseStartWindowId( $boolIsMilitaryProperty );
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        	    break;
        }

        return $boolIsValid;
    }

}
?>