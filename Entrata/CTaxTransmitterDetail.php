<?php

class CTaxTransmitterDetail extends CBaseTaxTransmitterDetail {

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {
		$strPlainTaxNumber = $this->getTaxNumber();
		if( 4 == strlen( $strPlainTaxNumber ) ) {
			return $strPlainTaxNumber;
		} else {
			return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ) );
		}
	}

	public function getCheckAccountNumber() {
		return CEncryption::decryptText( $this->m_strCheckAccountNumberEncrypted, CONFIG_KEY_ACH_ACCOUNT_NUMBER );
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == valStr( $strPlainTaxNumber ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
			$this->setTaxNumberMasked( CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ) ) );
		}
	}

	public function setTaxNumberMasked( $strTaxNumberMasked = NULL ) {
		$this->m_strTaxNumberMasked = $this->getTaxNumberMasked();
	}

	public function valCompanyName() {
		$boolIsValid = true;

		if( false == valStr( $this->getCompanyName() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Company name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valControlCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getControlCode() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_code', 'Control Code is required.' ) );

		} elseif( 5 > strlen( $this->getControlCode() ) || 5 < strlen( $this->getControlCode() ) || false == preg_match( '/^[a-zA-Z0-9]+$/', $this->getControlCode() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_code', 'TCC must be 5 characters and include only letters and numbers.' ) );
		}

		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;

		if( false == valStr( $this->getContactName() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_name', 'Contact name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;

		if( false == preg_match( '/\(\d{3}\)\s\d{3}-\d{4}\|/', $this->getContactPhoneNumber() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', 'Contact phone number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getContactEmailAddress() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', 'Contact email address is required.' ) );
		} elseif( true == valStr( $this->getContactEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getContactEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', 'Contact email address is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		if( false == valStr( $this->getStreetLine1() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Address Line1 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( false == valStr( $this->getCity() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getStateCode() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State Code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getPostalCode() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal Code is required.' ) );
		} else if( true == valStr( $this->getPostalCode() ) && 0 < strlen( str_replace( '-', '', $this->getPostalCode() ) )
		           && false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getPostalCode() )
		           && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getPostalCode() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;

		if( false == valStr( $this->getTaxNumber() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'Transmitter TIN is required.' ) );
		} elseif( false == is_null( $this->getTaxNumber() ) && false == preg_match( '/^([\d]{9})/', $this->getTaxNumber() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'TIN must be 9 digits and should be numeric only.' ) );

		}
		return $boolIsValid;
	}

	public function valTaxNumberMasked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valTaxNumberEncrypted();
				$boolIsValid &= $this->valControlCode();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valContactName();
				$boolIsValid &= $this->valContactPhoneNumber();
				$boolIsValid &= $this->valContactEmailAddress();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>