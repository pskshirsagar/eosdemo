<?php

class COnSiteContactPhoneNumber extends CBaseOnSiteContactPhoneNumber {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intPropertyId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyOnSiteContactId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intPropertyOnSiteContactId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_on_site_contact_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPhoneNumberTypeId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intPhoneNumberTypeId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMarketingName() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strMarketingName ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marketing_name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strPhoneNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function valExtension() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strExtension ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intIsPublished ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intOrderNum ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>