<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryNonMilitaryTypes
 * Do not add any new functions to this class.
 */

class CMilitaryNonMilitaryTypes extends CBaseMilitaryNonMilitaryTypes {

	public static function fetchMilitaryNonMilitaryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryNonMilitaryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchMilitaryNonMilitaryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryNonMilitaryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedMilitaryNonMilitaryTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM military_non_military_types WHERE is_published=true order by order_num';
		return self::fetchMilitaryNonMilitaryTypes( $strSql, $objDatabase );
	}

}
?>