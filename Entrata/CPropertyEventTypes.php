<?php

class CPropertyEventTypes extends CBasePropertyEventTypes {

	public static function fetchPropertyEventTypeByPropertyIdByEventTypeIdByExcludingIdByCid( $intPropertyId, $intEventTypeId, $intPropertyEventResultId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						 *
						FROM
							property_event_types
						WHERE
							property_id = ' . ( int ) $intPropertyId . '
							AND event_type_id = ' . ( int ) $intEventTypeId . '
							AND cid = ' . ( int ) $intCid;

							if( 0 < $intPropertyEventResultId ) {
								$strSql .= ' AND default_property_event_result_id != ' . ( int ) $intPropertyEventResultId;
							}

		return self::fetchPropertyEventType( $strSql, $objDatabase );
	}

	public static function fetchDefaultPropertyEventResultIdsByPropertyIdByEventTypeIdsByCid( $intPropertyId, $arrintEventTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintEventTypeIds ) ) return NULL;

		$strSql = 'SELECT
					*
				   FROM
					property_event_types
				   WHERE
					property_id = ' . ( int ) $intPropertyId . '
					AND event_type_id IN ( ' . implode( ',', $arrintEventTypeIds ) . ' )
					AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyEventTypes( $strSql, $objDatabase );
	}

}
?>