<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReports
 * Do not add any new functions to this class.
 */

class CReports extends CBaseReports {

	public static function fetchReportsByIdsCid( $arrintReportIds, $intCid, $objDatabase ) {
		return self::fetchReports( sprintf( 'SELECT * FROM reports WHERE id IN ( %s )  AND cid = %d AND is_published', implode( $arrintReportIds, ',' ), ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportByIdCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReport( sprintf( 'SELECT * FROM reports WHERE id = %d  AND cid = %d AND is_published', $intReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportsByReportingTabModuleIdByCid( $intModuleId, $arrintPsProductIds, $intCid, $objDatabase, $intCompanyUserId = NULL ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return [];
		}

		// To list all category reports under favorite section for PSI user.
		$strModuleIdCondition = CModule::FAVORITE_REPORT_TAB != $intModuleId ? 'AND COALESCE( drg.parent_module_id, ' . ( int ) $intModuleId . ' ) = ' . ( int ) $intModuleId : '';

		$strCompanyUserJoinSql = '';
		if( false == is_null( $intCompanyUserId ) ) {
			$strCompanyUserJoinSql = 'JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END';
		}

		// We want to fetch all reports which exist by default under the current module
		// TODO: if/when there are client-specific or user-defined reports, we'll need to include those here as well
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				reports r
				JOIN report_versions rv ON r.cid = rv.cid AND r.id = rv.report_id
				' . $strCompanyUserJoinSql . '
				LEFT JOIN default_reports dr ON r.default_report_id = dr.id
				LEFT JOIN default_report_groups drg ON dr.default_report_group_id = drg.id
				LEFT JOIN product_reports pr ON r.cid = pr.cid AND r.id = pr.report_id
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.is_published = TRUE
				AND rv.is_default = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				' . $strModuleIdCondition . '
				AND ( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
			ORDER BY
				r.title,
				rv.major DESC,
				rv.minor DESC';

		return self::fetchReports( $strSql, $objDatabase, false );
	}

	public static function fetchCustomReportsByIdsByCid( $arrintReportIds, $intCid, $objDatabase, $boolIsLatestVersion = false ) {

		$strWhereClause	= NULL;

		if( false == valArr( $arrintReportIds ) ) {
			return [];
		}

		if( true == $boolIsLatestVersion ) {
			$strWhereClause = ' AND rv.is_latest = true ';
		}

		$strSql = '
			SELECT
				r.*,
				rv.definition,
				rv.id as report_version_id
			FROM
				reports r
				JOIN report_versions rv ON r.cid = rv.cid AND r.id = rv.report_id
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.id IN ( ' . implode( $arrintReportIds, ',' ) . ' )
				AND r.is_published = TRUE
				AND ( rv.definition->>\'is_published\' )::boolean = TRUE' . $strWhereClause;

		return self::fetchReports( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintReportInstanceIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReport[]
	 */
	public static function fetchReportsByReportInstanceIdsByCid( $arrintReportInstanceIds, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				report_instances ri
				JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
				JOIN report_versions rv ON ri.cid = rv.cid AND ri.report_version_id = rv.id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				' . ( true == valArr( $arrintReportInstanceIds ) ? 'AND ri.id IN( ' . implode( ',', $arrintReportInstanceIds ) . ' )' : '' ) . '
		';

		return self::fetchReports( $strSql, $objDatabase );
	}

	public static function fetchReportsByReportVersionIdsByCid( $arrintReportVersionIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintReportVersionIds ) ) {
			return [];
		}

		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				report_versions rv
				JOIN reports r ON rv.cid = r.cid AND rv.report_id = r.id
			WHERE
				rv.cid = ' . ( int ) $intCid . '
				AND rv.id IN ( ' . implode( ',', $arrintReportVersionIds ) . ' )
				AND r.is_published = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
		';

		return self::fetchReports( $strSql, $objDatabase, false );
	}

	/**
	 * @param $intReportInstanceId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReport
	 */
	public static function fetchReportByReportInstanceIdByCid( $intReportInstanceId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.title_addendum,
				rv.definition,
				rv.major,
				rv.minor,
				rv.expiration,
				rv.is_latest
			FROM
				reports r
				JOIN report_instances ri ON ( r.cid = ri.cid AND r.id = ri.report_id )
				JOIN report_versions rv ON ( ri.cid = rv.cid AND ri.report_version_id = rv.id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id = ' . ( int ) $intReportInstanceId;
		return self::fetchReport( $strSql, $objDatabase );
	}

	public static function fetchReportByNameByReportTypeIdByCid( $strReportName, $intReportTypeId = 1, $intCid, $objDatabase, $strVersion = NULL, $boolExcludeExpiredReportVersions = false ) {

		if( false == is_null( $strVersion ) && false == empty( $strVersion ) ) {
			list( $intMajor, $intMinor ) = array_map( 'intval', explode( '.', $strVersion ) );
			$strVersionCondition = 'AND rv.major = ' . ( int ) $intMajor . ' AND rv.minor = ' . ( int ) $intMinor;
		} else {
			$strVersionCondition = 'AND rv.is_latest = TRUE';
		}

		$strExpirationWhereConditionSql = '';
		if( false == $boolExcludeExpiredReportVersions ) {
			$strExpirationWhereConditionSql = 'AND COALESCE( rv.expiration, NOW() ) >= NOW()';
		}

		if( CReportType::SYSTEM == $intReportTypeId ) {
			$strReportTypeIdCondition = 'AND r.report_type_id IN ( ' . implode( ',', CReportType::$c_arrintSystemReportTypes ) . ' )';
		} else {
			$strReportTypeIdCondition = 'AND r.report_type_id = ' . ( int ) $intReportTypeId . '';
		}

		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.expiration,
				rv.is_latest
			FROM
				reports r
				JOIN report_versions rv ON r.cid = rv.cid AND r.id = rv.report_id
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.name LIKE \'' . $strReportName . '\'
				' . $strReportTypeIdCondition . '
				AND r.is_published = TRUE
				' . $strExpirationWhereConditionSql . '
				' . $strVersionCondition;

		return self::fetchReport( $strSql, $objDatabase );
	}

	public static function fetchPublishedReportsForApiByCid( $intCid, $objCompanyUser, $objDatabase, $arrstrReportNames = NULL, $fltReportVersion = NULL, $boolIsNewReporting = false ) {
		$strDisntinctSql    = '';
		$strJoinSql         = '';
		$strDeletedSql       = '';
		$strInstanceTable = 'report_instances';
		if( false != $boolIsNewReporting ) {
			$strInstanceTable = 'report_new_instances';
		}
		if( false != valArr( $arrstrReportNames ) ) {
			$strDisntinctSql = ' DISTINCT ON ( rv.report_id ) ';
		}
		if( false == valStr( $fltReportVersion ) ) {
			$strJoinSql     = ' JOIN ' . $strInstanceTable . ' ri ON ( ri.cid = rv.cid AND ri.report_version_id = rv.id ) ';
			$strDeletedSql   = ' AND ri.deleted_on IS NULL 
			                    AND ri.deleted_by IS NULL';
		}

		$strSql = ' SELECT
                        ' . $strDisntinctSql . '
						r.*,
						rv.id as report_version_id,
						rv.definition
					FROM
						reports r
						JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id )
						' . $strJoinSql . '
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.is_published = TRUE
						AND r.is_allowed_for_api = TRUE
						AND COALESCE( rv.expiration, NOW() ) >= NOW()';
		if( false != valArr( $arrstrReportNames ) ) {
			$strSql .= ' AND LOWER( r.name ) IN (\'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrstrReportNames ) ) . '\') ';
		}
		if( false != valStr( $fltReportVersion ) ) {
			$strSql .= ' AND ( rv.major || \'.\' || rv.minor ) = \'' . $fltReportVersion . '\'';
		}

		$strSql .= ' AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, TRUE) = TRUE
					AND (((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\')::TEXT IS NULL OR rv.cid = ANY ((\'{ \' || trim(both \'[]\'
				FROM 
					((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\'):: TEXT) || \' }\')::INT [ ]))';
		if( CCompanyUserType::ADMIN != $objCompanyUser->getCompanyUserTypeId() ) {
			$strSql .= ' AND COALESCE((rv.definition ->> \'allow_only_psi_admin\')::BOOLEAN, FALSE) = FALSE ';
		}
		$strSql .= $strDeletedSql;
		$strSql .= ' ORDER BY 
						rv.report_id,
						rv.id DESC';

		return self::fetchReports( $strSql, $objDatabase );
	}

	public static function fetchPublishedReportsByNamesByCid( $arrstrReportName,$intCid, $objDatabase ) {

		if( false == valArr( $arrstrReportName ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						r.*,
						rv.definition,
						rv.id AS report_version_id
					FROM
						reports r
						JOIN report_versions rv ON ( r.cid = rv.cid AND r.id = rv.report_id )
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.is_published = TRUE
						AND rv.is_latest = TRUE
						AND LOWER( r.name ) IN ( \'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrstrReportName ) ) . '\' )';
		return self::fetchReports( $strSql, $objDatabase );
	}

	public static function fetchPublishedReportByNameByCid( $strReportName, $intCid, $objDatabase ) {

		if( false == valStr( $strReportName ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						r.*
					FROM
						reports r
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.is_published = TRUE
						AND LOWER( r.name ) LIKE ( \'' . \Psi\CStringService::singleton()->strtolower( $strReportName ) . '\' )
					LIMIT 1';

		return self::fetchReport( $strSql, $objDatabase );
	}

	/**
	 * @param $intReportFilterId
	 * @param $intCid
	 * @param $objDatabase
	 * @param $intReportVersionId
	 * @return CReport
	 */
	public static function fetchReportObjectByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase, $intReportVersionId = NULL ) {
		if( false == isset( $intReportFilterId ) ) {
			return NULL;
		}

		if( false == is_null( $intReportVersionId ) ) {
			$strReportVersionCondition = 'AND rv.id = ' . ( int ) $intReportVersionId;
		} else {
			$strReportVersionCondition = 'AND rv.is_latest = TRUE';
		}

		$strSql = '
			SELECT
				r.*,
				rv.definition,
				rv.major,
				rv.minor
			FROM
				report_filters rf
				JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
				JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id )
			WHERE
				rf.id = ' . ( int ) $intReportFilterId . '
				AND r.cid = ' . ( int ) $intCid . '
				' . $strReportVersionCondition . '
		';

		return self::fetchReport( $strSql, $objDatabase );
	}

	public static function fetchReportByReportVersionIdByCid( $intReportVersionId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				reports r
				JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id )
			WHERE
				rv.cid = ' . ( int ) $intCid . '
				AND rv.id = ' . ( int ) $intReportVersionId . '
		';

		return self::fetchReport( $strSql, $objDatabase );
	}

	public static function fetchReportByIdByCidWithDefaultVersion( $intReportId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration,
				rv.title_addendum
			FROM
				reports r
				JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id ) AND rv.is_latest = TRUE
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.id = ' . ( int ) $intReportId . '
		';

		return self::fetchReport( $strSql, $objDatabase );
	}

	public static function fetchReportNavigation( $intCompanyUserId, $intCid, $strParentModuleName, $arrintPsProductIds, $arrintModuleIds, $objDatabase, $boolShowEmptyGroups = false, $boolIgnoreInstanceNames = false, $boolIncludeExpiringReports = true ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return [];
		}

		$strDistinctReportGroupIdCondition = $strDistinctOrderByCondition = $strReportPacketOuterSelectSql = $strReportPacketOuterOrderBySql = '';

		$arrstrPermissionConditions = [];
		if( false == in_array( $strParentModuleName, [ CModule::FAVORITE_REPORTS, CModule::REPORT_PACKETS ] ) ) {
			// We are not currently requiring permissions to view report packets or favorite reports
			$arrstrPermissionConditions[] = 'rg.module_id IN( ' . implode( ',', $arrintModuleIds ) . ' )';
			$arrstrPermissionConditions[] = 'AND ( ri.module_id IS NULL OR ( ri.module_id IN( ' . implode( ',', $arrintModuleIds ) . ' ) AND cu.id IS NOT NULL ) ) AND';
		} elseif( $strParentModuleName == CModule::REPORT_PACKETS ) {
			$strDistinctReportGroupIdCondition = 'DISTINCT ON ( rg.id, r.id, ri.id )';
			$strDistinctOrderByCondition = 'rg.id,
											r.id,
											ri.id,
											rs.id,';
		}

		$strTitleSelect				= ( $boolIgnoreInstanceNames ) ? 'r.title,' : 'util_get_translated( \'name\', ri.name, ri.details ) AS title,';
		$strParentModuleCondition	= ( false == is_null( $strParentModuleName ) ? 'AND m.name = \'' . $strParentModuleName . '\'' : '' );
		$strReportInstanceJoinType	= ( $boolShowEmptyGroups ? 'LEFT JOIN' : 'JOIN' );
		$strAdditionalOrderBy		= ( CModule::REPORT_PACKETS == $strParentModuleName ? 'rg.created_on,' : '' );
		$strIsInternalCondition		= implode( PHP_EOL, $arrstrPermissionConditions ) . '
										( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
										AND CASE WHEN TRUE = rv.is_latest THEN COALESCE( rv.expiration, now() ) >= now() ELSE TRUE END
										AND CASE WHEN rg.report_group_type_id = ' . CReportGroupType::MY_REPORTS . ' THEN rg.company_user_id = ' . ( int ) $intCompanyUserId . ' ELSE TRUE END';

		// To prevent showing an empty packet if the report version expired is not the the default version.
		$strReportPacketsSql = !$boolIncludeExpiringReports ? ' AND COALESCE( rv.expiration, NOW() ) >= NOW() ' : '';

		if( CModule::REPORT_PACKETS == $strParentModuleName ) {
			$strReportPacketOuterSelectSql = ' SELECT report_packet_list.* 
												FROM ( ';
			$strReportPacketOuterOrderBySql = ' ) AS report_packet_list 
												ORDER BY 
													report_packet_list.report_group_name,
													report_packet_list.report_instance_id,
													report_packet_list.report_instance_id,
													report_packet_list.title';
			// Adding report_packet_list.report_instance_id in order by clause so that this listing should show instances in order in which user added those.
		}
		$strSql = '
			WITH latest_report AS ( SELECT
				report_version_details.*
			FROM
				(
					SELECT
						rv.cid,
						rv.id AS report_version_id,
						rv.report_id,
						rv.major,
						rv.minor,
						rv.is_default,
						rv.is_latest,
						rv.expiration,
						rv.definition,
						row_number ( ) OVER ( Partition BY rv.report_id
					ORDER BY
						rv.is_default DESC ) AS default_count
					FROM
						report_versions rv
					WHERE
						rv.cid = ' . ( int ) $intCid . '
						AND ( rv.is_default = TRUE
						OR rv.is_latest = TRUE )
					ORDER BY
						rv.is_default DESC
				) AS report_version_details
			WHERE
				report_version_details.default_count = 1
				AND COALESCE ( ( CASE
									WHEN report_version_details.is_default = FALSE THEN NULL
									ELSE TRUE
								 END ), report_version_details.is_latest ) = TRUE )
			' . $strReportPacketOuterSelectSql . '
			SELECT
				' . $strDistinctReportGroupIdCondition . 'rg.id AS report_group_id,
				rg.id AS report_group_id,
				sch.scheduled_data ::text,
				util_get_translated( \'name\', rg.name, rg.details ) AS report_group_name,
				rg.details,
				rg.position_number,
				rg.report_group_type_id,
				rg.is_internal,
				rg.created_by,
				rg.company_user_id,
				util_get_translated( \'name\', rgt.name, rgt.details ) AS report_group_type,
				rgt.details,
				r.id,
				r.report_type_id,
				r.name,
				r.details,
				COALESCE( util_get_translated( \'description\', r.description, r.details ), util_get_translated( \'description\', ri.description, ri.details ) ) AS description,
				r.is_schedulable,
				ri.id AS report_instance_id,
				ri.module_id,
				ri.details,
				' . $strTitleSelect . '
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				( lr.major ||\'.\'|| lr.minor ) AS newest_version,
				rv.expiration,
				rv.title_addendum,
				rv.is_latest,
				rv.is_default,
				COALESCE( rv.expiration, NOW() ) < NOW() AS is_expired,
				CASE
					WHEN ( lr.report_version_id <> rv.id ) AND COALESCE ( lr.expiration, NOW ( ) ) >= NOW ( ) AND ( FALSE = ( lr.definition ->> \'allow_only_psi_admin\' )::BOOLEAN ) AND COALESCE ( ( lr.definition ->> \'is_published\' )::BOOLEAN, TRUE ) = TRUE THEN  ( CASE
						WHEN lr.major < rv.major THEN FALSE
						WHEN lr.major = rv.major AND lr.minor < rv.minor THEN FALSE
						ELSE ( CASE
									WHEN ( ( lr.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedClient\' )::TEXT IS NOT NULL THEN lr.cid = ANY ( ( \'{ \' || trim ( both \'[]\' FROM ( ( lr.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedClient\' )::TEXT ) || \' }\' )::INT [ ] )
									WHEN ( ( lr.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedCid\' )::TEXT IS NOT NULL THEN lr.cid = ANY ( ( \'{ \' || trim ( both \'[]\' FROM ( ( lr.definition ->> \'validation_callbacks\' )::json ->> \'isAllowedCid\' )::TEXT ) || \' }\' )::INT [ ] )
									ELSE TRUE
								END )
						END ) 
					ELSE FALSE
				END AS update_available,
				rf.id AS report_filter_id,
				rf.is_public,
				rf.name AS report_filter_name,
				rs.id AS report_schedule_id,
				rs.download_options,
				util_get_translated( \'name\', rst.name, rst.details ) AS report_schedule_type,
				rst.details,
				CASE
					WHEN r.report_type_id = ' . CReportType::SAP . ' THEN TRUE
					WHEN rip.default_report_version_id IS NOT NULL THEN TRUE
					ELSE FALSE
				END AS has_item_description,
				COALESCE( util.util_to_bool( ri.details->>\'is_quick_link\' ), FALSE ) AS is_quick_link
			FROM
				modules m
				JOIN report_groups rg ON rg.parent_module_id = m.id
				JOIN report_group_types rgt ON rgt.id = rg.report_group_type_id
				' . $strReportInstanceJoinType . ' report_instances ri ON rg.cid = ri.cid AND rg.id = ri.report_group_id AND ri.deleted_on IS NULL
				LEFT JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
				LEFT JOIN report_versions rv ON ri.cid = rv.cid AND ri.report_version_id = rv.id
				LEFT JOIN report_versions latest_version ON latest_version.cid = rv.cid AND latest_version.report_id = rv.report_id AND latest_version.is_latest = TRUE
				LEFT JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				LEFT JOIN report_filters rf ON rf.cid = ri.cid AND rf.id = ri.report_filter_id AND rf.deleted_on IS NULL
				LEFT JOIN product_reports pr ON r.cid = pr.cid AND r.id = pr.report_id
				LEFT JOIN report_schedules rs ON rs.cid = rg.cid AND rs.report_group_id = rg.id AND rs.deleted_on IS NULL
				LEFT JOIN report_schedule_types rst ON rst.id = COALESCE( rs.report_schedule_type_id, ' . CReportScheduleType::MANUAL . ' )
				LEFT JOIN report_item_descriptions rip ON ( rip.default_report_version_id = rv.default_report_version_id )
				LEFT JOIN LATERAL (
					SELECT 
						json_build_object( \'id\',schedules.id, \'is_internal\',schedules.is_internal  ) AS scheduled_data
					FROM (
							SELECT
								rep_sch.id,
								CASE
									WHEN ( strpos ( rep_sch.download_options::TEXT, \'"is_internal": true\' )::INTEGER > 0 ) THEN TRUE
									ELSE NULL
								END AS is_internal,
								row_number() OVER( PARTITION BY rep_sch.report_group_id ORDER BY rep_sch.id DESC ) AS latest
							FROM
								report_schedules rep_sch 
							WHERE 
								rep_sch.cid = rg.cid AND rep_sch.report_group_id = rg.id AND rep_sch.deleted_on IS NULL ) AS schedules
					WHERE 
						schedules.latest = 1
				) sch ON ( TRUE )
				LEFT JOIN latest_report lr ON ( lr.cid = rv.cid  AND lr.report_id  = rv.report_id )
			WHERE
				rg.cid = ' . ( int ) $intCid . '
				AND m.parent_module_id = ' . CModule::REPORT_SYSTEM . '
				' . $strParentModuleCondition . '
				AND rg.deleted_on IS NULL
				AND ri.deleted_on IS NULL
				AND COALESCE( r.is_published, TRUE ) = TRUE
				AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
				' . $strReportPacketsSql . '
				AND CASE WHEN TRUE = rg.is_internal THEN TRUE ELSE ' . $strIsInternalCondition . ' END
			GROUP BY
				rg.cid,
				rg.id,
				sch.scheduled_data ::text,
				rgt.id,
				r.cid,
				r.id,
				ri.cid,
				ri.id,
				rv.cid,
				rv.id,
				rf.cid,
				rf.id,
				rs.cid,
				rs.id,
				rst.id,
				lr.report_version_id,
				lr.report_version_id,
				lr.major,
				lr.minor,
				lr.cid,
				lr.expiration,
				lr.definition,
				latest_version.cid,
				latest_version.id,
				cu.id,
				rip.default_report_version_id
			ORDER BY
				' . $strDistinctOrderByCondition . '
				' . $strAdditionalOrderBy . '
				rg.position_number,
				rg.name,
				ri.name,
				r.name,
				rf.name' . $strReportPacketOuterOrderBySql;

		return self::fetchReports( $strSql, $objDatabase, false );
	}

	public static function fetchReportsBySearchKeywordByFilterTypeByCid( $strSearchKeyword, $strFilterType, $intCompanyUserId, $arrintPsProductIds, $arrintModuleIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) return [];

		$strSqlSearch = 'AND LOWER( ri.' . $strFilterType . ' ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' ) ';
		if( true == valStr( CLocaleContainer::createService()->getLocaleCode() ) ) {
			$strSqlSearch = 'AND ( 
				LOWER( util_get_translated( \'name\', ri.name, ri.details ) ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' ) OR 
				LOWER( util_get_translated( \'title\', r.title, r.details ) ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' )
			)';
		}

		$arrstrPermissionConditions[] = 'ri.module_id IN( ' . implode( ',', $arrintModuleIds ) . ' )';
		$arrstrPermissionConditions[] = '( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )';

		$strSql = '
			SELECT
				r.id,
				r.name,
				r.report_type_id,
				r.details,
				ri.id AS report_instance_id,
				util_get_translated( \'name\', ri.name, ri.details ) AS title,
				ri.report_filter_id,
				rv.major,
				rv.minor,
				rv.id AS report_version_id,
				rv.definition,
				util_get_translated( \'name\', rg.name, rg.details ) AS report_group_name
			FROM
				modules m
				JOIN report_instances ri ON ri.module_id = m.id AND ri.cid = ' . ( int ) $intCid . ' AND ri.deleted_on IS NULL
				JOIN report_groups rg ON rg.cid = ri.cid and rg.id = ri.report_group_id
				LEFT JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
				LEFT JOIN report_versions rv ON ri.cid = rv.cid AND ri.report_version_id = rv.id
				JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				LEFT JOIN product_reports pr ON r.cid = pr.cid AND r.id = pr.report_id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND rg.report_group_type_id = ' . CReportGroupType::STANDARD . '
				AND ri.deleted_on IS NULL
				AND COALESCE( r.is_published, TRUE ) = TRUE
				AND (rv.definition->>\'is_published\')::BOOLEAN = TRUE
				AND COALESCE( rv.expiration, now() ) >= now()
				AND ' . implode( ' AND ', $arrstrPermissionConditions ) . '
				' . $strSqlSearch . '
			ORDER BY
				ri.name,
				rv.major,
				rv.minor,
				r.name';

		return self::fetchReports( $strSql, $objDatabase, false );
	}

	// region Fetch Dependent Reports By Primary Object ID

	public static function fetchReportsByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				report_instances ri
				JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
				JOIN report_versions rv ON ri.cid = rv.cid AND ri.report_version_id = rv.id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.report_group_id = ' . ( int ) $intReportGroupId . '
				AND ri.deleted_by IS NULL
				AND ri.deleted_on IS NULL
			ORDER BY
				ri.id';

		return rekeyObjects( 'ReportVersionId', self::fetchReports( $strSql, $objDatabase, false ) ?: [] );
	}

	public static function fetchReportsByReportNewGroupIdByCid( $intReportNewGroupId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_Id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				report_new_groups rg
				JOIN report_new_group_instances rgi ON rgi.cid = rg.cid AND rgi.report_new_group_id = rg.id
				JOIN report_new_instances ri ON ri.cid = rgi.cid AND ri.id = rgi.report_new_instance_id
				JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
				JOIN reports r ON r.cid = rv.cid AND r.id = rv.report_id
			WHERE
				rg.cid = ' . ( int ) $intCid . '
				AND rg.id = ' . ( int ) $intReportNewGroupId . '
				AND ri.deleted_by IS NULL
				AND ri.deleted_on IS NULL
		';

		return rekeyObjects( 'ReportVersionId', self::fetchReports( $strSql, $objDatabase, false ) ?: [] );
	}

	public static function fetchReportsByReportNewInstanceIdByCid( $intReportNewInstanceId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.*,
				rv.id AS report_version_Id,
				rv.definition,
				rv.major,
				rv.minor,
				rv.is_latest,
				rv.expiration
			FROM
				report_new_instances ri
				JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
				JOIN reports r ON r.cid = rv.cid AND r.id = rv.report_id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id = ' . ( int ) $intReportNewInstanceId . '
				AND ri.deleted_by IS NULL
		';

		return rekeyObjects( 'ReportVersionId', self::fetchReports( $strSql, $objDatabase, false ) ?: [] );
	}

	// endregion

	public static function fetchReportsHavingReportFiltersByCid( $intCompanyUserId, $intCid, $objDatabase, $boolIsNewReportSystem = false ) {
		$strJoinSql = $strReportVersionJoinConditionSql = '';
		$strReportNameSql = ' r.name, ';
		if( true == $boolIsNewReportSystem ) {
			$strJoinSql = ' JOIN report_new_instances ri ON ( r.cid = ri.cid AND r.id = ri.report_id )
				JOIN report_new_group_instances rgn ON ( rgn.cid = ri.cid AND rgn.report_new_instance_id = ri.id )
				JOIN report_new_groups rg ON ( rg.cid = ri.cid AND rg.id = rgn.report_new_group_id ) ';
			$strReportNameSql = ' ri.name, ri.id AS report_instance_id, ';
			$strReportInstanceSql = ' NULL AS report_instance_id, ';
			$strReportVersionJoinConditionSql = ' AND rv.id = ri.report_version_id ';
		}

		$strSql = '(
			SELECT
				r.id,
				' . $strReportNameSql . '
				r.title,
				r.report_type_id,
				r.is_schedulable,
				rv.id AS report_version_id,
				rv.major,
				rv.minor,
				rv.definition
			FROM
				reports r
				' . $strJoinSql . '
				JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id ' . $strReportVersionJoinConditionSql . ' )
				JOIN default_report_versions drv ON ( rv.default_report_version_id = drv.id )
				JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.is_schedulable = TRUE
				AND r.is_published = TRUE
				AND (rv.definition->>\'is_published\')::BOOLEAN = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND r.report_type_id IN( ' . implode( ',', [ CReportType::SYSTEM, CReportType::CUSTOM ] ) . ')
		)
			UNION ALL
		(
			SELECT
				r.id,
				r.name,
				' . $strReportInstanceSql . '
				r.title,
				r.report_type_id,
				true AS is_schedulable,
				rv.id AS report_version_id,
				rv.major,
				rv.minor,
				rv.definition
			FROM
				reports r
				JOIN report_versions rv ON rv.cid = r.cid AND rv.report_id = r.id
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.report_type_id = ' . CReportType::SAP . '
		)
		ORDER BY
			title,
			major DESC,
			minor DESC
		';

		return self::fetchReports( $strSql, $objDatabase, false );
	}

	public static function fetchReportsByDefaultReportIdsByCid( $arrintDefaultReportIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDefaultReportIds ) ) {
			return [];
		}

		$strSql = '
			SELECT
				r.*
			FROM
				reports r
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.default_report_id IN ( ' . implode( $arrintDefaultReportIds, ',' ) . ' )';

		return self::fetchReports( $strSql, $objDatabase );
	}

	public static function getAllReportsWithAllVersions( $objDatabase, array $intDefaultReportIds = [], $intMajor = NULL, $intMinor = NULL ) : array {

		$strWhereCondition = '';
		if( valArr( $intDefaultReportIds ) ) {
			$strWhereCondition .= ' AND dr.id IN ( ' . sqlIntImplode( $intDefaultReportIds ) . ' )';
		}

		if( valId( $intMajor ) ) {
			$strWhereCondition .= ' AND drv.major = ' . $intMajor;
		}

		if( valId( $intMinor ) ) {
			$strWhereCondition .= ' AND drv.minor = ' . $intMinor;
		}

		$strSql = '
			SELECT
				*
			FROM
				default_reports AS dr
				JOIN default_report_versions AS drv ON drv.default_report_id = dr.id
			WHERE
				dr.is_published = true
				' . $strWhereCondition . '
			ORDER BY
				dr.name,
				drv.id';

		return self::fetchReports( $strSql, $objDatabase );

	}

	public static function fetchReportNewInstancesBySearchKeywordByFilterTypeByCid( $strSearchKeyword, $strFilterType, $intCompanyUserId, $arrintPsProductIds, $arrintModuleIds, $intCid, $objDatabase, $intReportGroupTypeId ) : array {
		if( !valArr( $arrintPsProductIds ) ) return [];

		$strReportNewGroupInstancesWhere = '';
		if( CReportGroupType::MY_REPORTS == $intReportGroupTypeId ) {
			$strReportNewGroupInstancesWhere = 'AND rngi.created_by = ' . ( int ) $intCompanyUserId;
		}

		$strSqlSearch = 'AND LOWER( rni.' . $strFilterType . ' ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' ) ';
		if( valStr( CLocaleContainer::createService()->getLocaleCode() ) ) {
			$strSqlSearch = 'AND ( 
				LOWER( util_get_translated( \'name\', rni.name, rni.details ) ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' ) OR 
				LOWER( util_get_translated( \'title\', r.title, r.details ) ) LIKE LOWER( \'%' . $strSearchKeyword . '%\' )
			)';
		}

		$arrstrPermissionConditions[] = 'rni.module_id IN( ' . implode( ',', $arrintModuleIds ) . ' )';
		$arrstrPermissionConditions[] = '( pr.ps_product_id IS NULL OR pr.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )';

		$strSql = '
			SELECT
				r.id,
				r.name,
				r.report_type_id,
				r.details,
				rni.id AS report_new_instance_id,
				util_get_translated( \'name\', rni.name, rni.details ) AS report_new_instance_name,
				rv.major,
				rv.minor,
				rv.id AS report_version_id,
				rv.definition,
				util_get_translated( \'name\', rng.name, rng.details ) AS report_new_group_name
			FROM
				modules m
				JOIN report_new_instances AS rni ON rni.module_id = m.id AND rni.cid = ' . ( int ) $intCid . '
				JOIN report_new_group_instances AS rngi ON rngi.cid = rni.cid AND rngi.report_new_instance_id = rni.id
				JOIN report_new_groups AS rng ON rng.cid = rngi.cid AND rng.id = rngi.report_new_group_id
				LEFT JOIN reports AS r ON rni.cid = r.cid AND rni.report_id = r.id
				LEFT JOIN report_versions AS rv ON rni.cid = rv.cid AND rni.report_version_id = rv.id
				JOIN company_users AS cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN util.util_to_bool( rv.definition ->> \'allow_only_admin\' ) THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN util.util_to_bool( rv.definition ->> \'allow_only_psi_admin\' ) THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				LEFT JOIN product_reports AS pr ON r.cid = pr.cid AND r.id = pr.report_id
			WHERE
				rni.cid = ' . ( int ) $intCid . '
				' . $strReportNewGroupInstancesWhere . '
				AND rng.report_group_type_id = ' . ( int ) $intReportGroupTypeId . '
				AND rni.deleted_on IS NULL
				AND rngi.deleted_by IS NULL
				AND COALESCE( r.is_published, TRUE ) = TRUE
				AND CASE
						WHEN r.report_type_id IN ( ' . CReportType::SYSTEM . ' , ' . CReportType::CUSTOM . ' ) THEN util.util_to_bool(rv.definition ->> \'is_published\') = TRUE
						ELSE TRUE
					END
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND ' . implode( ' AND ', $arrstrPermissionConditions ) . '
				' . $strSqlSearch . '
			ORDER BY
				rni.name,
				rv.major,
				rv.minor,
				r.name';

		return ( array ) self::fetchReports( $strSql, $objDatabase, false );
	}

}