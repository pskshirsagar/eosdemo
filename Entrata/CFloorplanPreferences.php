<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFloorplanPreferences
 * Do not add any new functions to this class.
 */

class CFloorplanPreferences extends CBaseFloorplanPreferences {

	public static function fetchFloorplanPreferencesByPropertyFloorplanIdKeyedByKeyByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM floorplan_preferences WHERE property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . ' AND cid = ' . ( int ) $intCid;
		$arrobjFloorplanPreferences = self::fetchFloorplanPreferences( $strSql, $objDatabase );
		$arrobjRekeyedFloorplanPreferences = [];

		if( true == valArr( $arrobjFloorplanPreferences ) ) {
			foreach( $arrobjFloorplanPreferences as $objFloorplanPreference ) {
				$arrobjRekeyedFloorplanPreferences[$objFloorplanPreference->getKey()] = $objFloorplanPreference;
			}
		}

		return $arrobjRekeyedFloorplanPreferences;
	}

}
?>