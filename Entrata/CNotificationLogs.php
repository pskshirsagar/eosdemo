<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationLogs
 * Do not add any new functions to this class.
 */

class CNotificationLogs extends CBaseNotificationLogs {

    public static function fetchNotificationLogByCompanyUserIdByNotificationIdByCid( $intCompanyUserId, $intNotificationId, $intCid, $objDatabase ) {
    	return parent::fetchNotificationLog( sprintf( 'SELECT * FROM notification_logs WHERE company_user_id = %d AND notification_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intNotificationId, ( int ) $intCid ), $objDatabase );
    }

    public static function fetchNotificationViewedCompanyUsersByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
						cu.id,
						ce.name_first,
						ce.name_last,
						nl.viewed_on
					FROM
						notification_logs nl
						JOIN company_users cu ON ( cu.id = nl.company_user_id AND cu.cid = nl.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						nl.cid = ' . ( int ) $intCid . '
						AND nl.notification_id = ' . ( int ) $intNotificationId . '
						AND nl.viewed_on IS NOT NULL
					ORDER BY name_first';
    	return fetchData( $strSql, $objDatabase );
    }

}
?>