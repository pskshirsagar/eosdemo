<?php

class CCommercialSuite extends CBaseCommercialSuite {

	protected $m_fltExistingTotalSquareFeet;
	protected $m_fltExistingUsableSquareFeet;
	protected $m_fltExistingRentableSquareFeet;

	public function getExistingTotalSquareFeet() {
		return $this->m_fltExistingTotalSquareFeet;
	}

	public function getExistingUsableSquareFeet() {
		return $this->m_fltExistingUsableSquareFeet;
	}

	public function getExistingRentableSquareFeet() {
		return $this->m_fltExistingRentableSquareFeet;
	}

	public function setExistingTotalSquareFeet( $fltTotalSquareFeet ) {
		$this->set( 'm_fltExistingTotalSquareFeet', CStrings::strToFloatDef( $fltTotalSquareFeet, NULL, false, 4 ) );
	}

	public function setExistingUsableSquareFeet( $fltUsableSquareFeet ) {
		$this->set( 'm_fltExistingUsableSquareFeet', CStrings::strToFloatDef( $fltUsableSquareFeet, NULL, false, 4 ) );
	}

	public function setExistingRentableSquareFeet( $fltRentableSquareFeet ) {
		$this->set( 'm_fltExistingRentableSquareFeet', CStrings::strToFloatDef( $fltRentableSquareFeet, NULL, false, 4 ) );
	}

	public function valSuiteNumber( $objDatabase ) {

		if( false === valStr( trim( $this->getSuiteNumber() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'suite_number', __( 'Suite Number is required.' ) ) );
			return false;
		}
		if( false == $this->valCheckDuplicationBySuiteNumber( $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'suite_number', __( 'Suite Number {%s, 0} already exists.', [ $this->getSuiteNumber() ] ) ) );
			return false;
		}

		return true;
	}

	public function valCheckDuplicationBySuiteNumber( $objDatabase ) {
		$strSqlCondition = valId( $this->m_intId ) ? ' AND id != ' . $this->m_intId : '';

		$strSql = 'SELECT
						id
				   FROM
						commercial_suites
				   WHERE
						LOWER ( suite_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $this->getSuiteNumber() ) ) ) . '\' ' . $strSqlCondition . '
						AND property_floor_id = ' . $this->getPropertyFloorId() . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND deleted_by IS NULL';
		$arrintSuitesIds	= fetchData( $strSql, $objDatabase );

		return valArr( $arrintSuitesIds ) ? false : true;
	}

	public function valPropertyFloorId() {
		if( false === valId( $this->getPropertyFloorId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floor_id', __( 'Floor is required.' ) ) );
			return false;
		}
		return true;
	}

	public function valCommercialSquareFeet( $strUnitOfMeasureForArea ) {
		$boolIsValid = true;

		if( false === is_numeric( $this->getSquareFeet() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} required.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getUsableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Total rentable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet_total', __( 'The Usable {%s, 0} cannot exceed Rentable {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getRentableSquareFeet() > $this->getSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Rentable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCommercialSuiteSquareFeet( $objDatabase, $strUnitOfMeasureForArea ) {
		$boolIsValid = true;

		$objPropertyFloor                = \Psi\Eos\Entrata\CPropertyFloors::createService()->fetchPropertyFloorByIdByCid( $this->getPropertyFloorId(), $this->getCid(), $objDatabase );
		$arrmixFloorAllocatedSqftDetails = \Psi\Eos\Entrata\CCommercialSuites::createService()->fetchCustomCommercialSuitesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$arrmixFloorAllocatedSqftDetails = rekeyArray( 'property_floor_id', $arrmixFloorAllocatedSqftDetails );

		$intTotalSquareFeet          = ( NULL != $objPropertyFloor->getTotalSquareFeet() ) ? $objPropertyFloor->getTotalSquareFeet() : 0;
		$intUsableSquareSquareFeet   = ( NULL != $objPropertyFloor->getUsableSquareFeet() ) ? $objPropertyFloor->getUsableSquareFeet() : 0;
		$intRentableSquareSquareFeet = ( NULL != $objPropertyFloor->getRentableSquareFeet() ) ? $objPropertyFloor->getRentableSquareFeet() : 0;

		$intEditedTotalSuiteSquareFeet    = 0;
		$intEditedUsableSuiteSquareFeet   = 0;
		$intEditedRentableSuiteSquareFeet = 0;
		if( true === valArr( $arrmixFloorAllocatedSqftDetails[$this->getPropertyFloorId()] ) && $arrmixFloorAllocatedSqftDetails[$this->getPropertyFloorId()]['property_floor_id'] == $this->getPropertyFloorId() ) {
			$intEditedTotalSuiteSquareFeet    = $this->getExistingTotalSquareFeet();
			$intEditedUsableSuiteSquareFeet   = $this->getExistingUsableSquareFeet();
			$intEditedRentableSuiteSquareFeet = $this->getExistingRentableSquareFeet();
		}

		// Suite Available Total SQFT
		$fltSuitesAvailableTotalSqft = ( $intTotalSquareFeet - $arrmixFloorAllocatedSqftDetails[$this->getPropertyFloorId()]['suites_square_feet'] ) + $intEditedTotalSuiteSquareFeet;
		// Suite Available Usable SQFT
		$fltSuitesAvailableUsableSqft = ( $intUsableSquareSquareFeet - $arrmixFloorAllocatedSqftDetails[$this->getPropertyFloorId()]['suites_usable_square_feet'] ) + $intEditedUsableSuiteSquareFeet;
		// Suite Available Rentable SQFT
		$fltSuitesAvailableRentableSqft = ( $intRentableSquareSquareFeet - $arrmixFloorAllocatedSqftDetails[$this->getPropertyFloorId()]['suites_rentable_square_feet'] ) + $intEditedRentableSuiteSquareFeet;

		if( false === is_null( $this->getSquareFeet() ) && $this->getSquareFeet() > $fltSuitesAvailableTotalSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Total {%s, 2} of this suite must be equal to or less than the unallocated Total {%s, 2} of {%s, 0} Floor: {%f, 3, p:2}.', [ $objPropertyFloor->getFloorTitle(), $objPropertyFloor->getTotalSquareFeet(), $strUnitOfMeasureForArea, $fltSuitesAvailableTotalSqft ] ) ) );
			$boolIsValid = false;
		}

		if( false === is_null( $this->getUsableSquareFeet() ) && $this->getUsableSquareFeet() > $fltSuitesAvailableUsableSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Usable {%s, 2} of this suite must be equal to or less than the unallocated Usable {%s, 2} of {%s, 0} Floor: {%f, 3, p:2}.', [ $objPropertyFloor->getFloorTitle(), $objPropertyFloor->getUsableSquareFeet(), $strUnitOfMeasureForArea, $fltSuitesAvailableUsableSqft ] ) ) );
			$boolIsValid = false;
		}

		if( false === is_null( $this->getRentableSquareFeet() ) && $this->getRentableSquareFeet() > $fltSuitesAvailableRentableSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Rentable {%s, 2} of this suite must be equal to or less than the unallocated Rentable {%s, 2} of {%s, 0} Floor: {%f, 3, p:2}.', [ $objPropertyFloor->getFloorTitle(), $objPropertyFloor->getRentableSquareFeet(), $strUnitOfMeasureForArea, $fltSuitesAvailableRentableSqft ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( string $strAction, $objDatabase = NULL, $strUnitOfMeasureForArea = 'square feet' ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSuiteNumber( $objDatabase );
				$boolIsValid &= $this->valPropertyFloorId();
				$boolIsValid &= $this->valCommercialSquareFeet( $strUnitOfMeasureForArea );
				$boolIsValid &= $this->valCommercialSuiteSquareFeet( $objDatabase, $strUnitOfMeasureForArea );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false === $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>