<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CAuthenticationLogs extends CBaseAuthenticationLogs {

	public static function fetchAuthenticationLogByCompanyUserIdByIdByCid( $intCompanyUserId, $intAuthenticationLogId, $intCid, $objDatabase, $strCurrentSessionId = NULL ) {

		$strWhere = ( true == valStr( $strCurrentSessionId ) ) ? ' AND session_id = \'' . $strCurrentSessionId . '\'' : '';

		$strSql = 'SELECT * FROM authentication_logs WHERE logout_datetime IS NULL AND company_user_id = ' . ( int ) $intCompanyUserId . ' AND id = ' . ( int ) $intAuthenticationLogId . ' AND cid = ' . ( int ) $intCid . $strWhere;

		return self::fetchAuthenticationLog( $strSql, $objDatabase );
	}

	public static function fetchAuthenticationLogByApplicantIdByIdByCid( $intApplicantId, $intAuthenticationLogId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM authentication_logs WHERE logout_datetime IS NULL AND applicant_id = ' . ( int ) $intApplicantId . ' AND id = ' . ( int ) $intAuthenticationLogId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchAuthenticationLog( $strSql, $objDatabase );
	}

	public static function fetchLastAuthenticationLogsByApplicantIdsByCid( $arrintApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						al.*
					FROM
						( SELECT
								al.*,
								RANK( ) OVER ( PARTITION BY al.cid, al.applicant_id ORDER BY al.id DESC ) AS rank
							FROM
								authentication_logs al
							WHERE
								al.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
								AND al.cid = ' . ( int ) $intCid . ' ) AS al
						WHERE
						al.rank = 1;';

		return self::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

	public static function fetchLastAuthenticationLogsByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON ( al.applicant_id )
						al.*
					FROM
						authentication_logs al
						LEFT JOIN applicant_applications aa ON ( al.applicant_id = aa.applicant_id AND al.cid = aa.cid ' . $strCheckDeletedAASql . ')
					WHERE
						CASE
							WHEN
								aa.lease_generated_on IS NOT NULL
							THEN
								al.login_datetime < aa.lease_generated_on
							ELSE
								al.login_datetime < CURRENT_TIMESTAMP
						END
						AND al.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND al.cid = ' . ( int ) $intCid . '
					ORDER BY al.applicant_id, al.id DESC';

		return self::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

	public static function fetchAuthenticationLogByApPayeeIdByIdByCid( $intApPayeeId, $intAuthenticationLogId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM authentication_logs WHERE logout_datetime IS NULL AND ap_payee_id = ' . ( int ) $intApPayeeId . ' AND id = ' . ( int ) $intAuthenticationLogId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchAuthenticationLog( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanyUserAuthenticationLogsByCompanyUserIdByCid( $intPageNo, $intPageSize, $intCompanyUserId, $intCid, $boolIsSiteTablet, $objDatabase ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;
		$strLogCondition = ( 1 == $boolIsSiteTablet ) ? ' authentication_log_type_id = ' . CAuthenticationLogType::SITE_TABLET : ' authentication_log_type_id <> ' . CAuthenticationLogType::SITE_TABLET;

		$strSql = 'SELECT
						*
					 FROM
						authentication_logs
					WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid . '
						AND authentication_log_type_id <> ' . CAuthenticationLogType::CHAT_PANEL . ' 
						AND ' . $strLogCondition . '
						AND login_datetime IS NOT NULL
				ORDER BY
						id DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserAuthenticationLogsCountByCompanyUserIdByCid( $intCompanyUserId, $intCid, $boolIsSiteTablet, $objDatabase ) {

		$strLogCondition = ( 1 == $boolIsSiteTablet ) ? ' authentication_log_type_id = ' . CAuthenticationLogType::SITE_TABLET : ' authentication_log_type_id <> ' . CAuthenticationLogType::SITE_TABLET;

		$strSql = 'SELECT
						count( id )
					 FROM
						authentication_logs
					WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid . '
						AND ' . $strLogCondition . '
						AND login_datetime IS NOT NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchLoginsCountByStastisticsEmailFilter( $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.property_id,
						COUNT ( e.id ) as logins_count
					FROM
						events e
					WHERE
						e.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
						AND e.event_type_id IN ( ' . CEventType::RESIDENT_LOG_IN . ', ' . CEventType::RESIDENT_LOG_IN_BY_MANAGER . ' )
						AND e.event_datetime >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::timestamptz
						AND e.event_datetime <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::timestamptz
						AND e.property_id IN ( ' . implode( ',', $objStastisticsEmailFilter->getPropertyIds() ) . ' )
		 			GROUP BY
						e.property_id
				';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPreviousAuthenticationLogByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM authentication_logs WHERE company_user_id = ' . ( int ) $intCompanyUserId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC OFFSET 1 LIMIT 1';
		return self::fetchAuthenticationLog( $strSql, $objDatabase );
	}

	public static function fetchLatestPasswordRotationDaysByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						( CURRENT_DATE - al.created_on::date ) AS rotation_days, al.created_on
					FROM
						authentication_logs al
					WHERE
						al.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND al.cid = ' . ( int ) $intCid . '
						AND al.previous_password IS NOT NULL
						ORDER BY al.created_on DESC
					LIMIT 1';

		$arrintRotationDays = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintRotationDays ) || false == isset( $arrintRotationDays[0]['rotation_days'] ) ) $arrintRotationDays = [ [ 'rotation_days' => 0 ] ];

		return $arrintRotationDays[0];
	}

	public static function fetchPreviousPasswordsByCompanyUserIdByRotationNumberByCid( $intCompanyUserId, $intRotationNumber, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						previous_password
					FROM
						authentication_logs
					WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid . '
						AND previous_password IS NOT NULL
					ORDER BY
						created_on DESC';

		if( $intRotationNumber > 0 ) {
			$strSql .= ' LIMIT ' . ( int ) $intRotationNumber;
		}

		return self::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

	public static function fetchAuthenticationLogsCountByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( al.applicant_id )
					FROM
						authentication_logs al
						LEFT JOIN applicant_applications aa ON ( al.applicant_id = aa.applicant_id AND al.cid = aa.cid )
					WHERE
						al.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND al.cid = ' . ( int ) $intCid;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchAuthenticationLogsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						authentication_logs
					WHERE
						customer_id IN (' . implode( ',', $arrintCustomerIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return self::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintCompanyUserIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return \CAuthenticationLog[]
	 */
	public static function fetchLatestAuthenticationLogByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						( 
							SELECT
								*,
								RANK() OVER ( PARTITION BY al.cid, al.company_user_id ORDER BY al.id DESC ) AS rank
							FROM
								authentication_logs al
							WHERE
								al.cid = ' . ( int ) $intCid . '
								AND al.company_user_id IN (' . implode( ',', $arrintCompanyUserIds ) . ')
								AND al.previous_password IS NULL
								AND al.login_datetime IS NOT NULL
								AND al.logout_datetime IS NULL
								AND al.session_id IS NOT NULL
						) subq
					WHERE
						subq.rank <= 5';

		return parent::fetchAuthenticationLogs( $strSql, $objDatabase );
	}

}
?>