<?php

class CCompanyTransmissionVendor extends CBaseCompanyTransmissionVendor {
	use TEosStoredObject;
	protected $m_intPropertyId;
	protected $m_intDocumentId;
	protected $m_strKey;
	protected $m_intCompanyUserId;
	protected $m_objApplication;
	protected $m_objLease;
	protected $m_objProperty;
	protected $m_objClient;
	protected $m_arrmixKeyValueData;
	protected $m_strStatus;
	protected $m_boolKeyBased;
	protected $m_strMailDeliveryCode;
	protected $m_intAccountingExportSchedulerId;
	protected $m_strConfirmPassphrase;
	protected $m_boolIsInternalPgpKey;
	protected $m_boolIsExternalPgpKey;
	protected $m_boolIsPublicKey;

	const KEY_SIZE_1024_BITS = 1024;
	const KEY_SIZE_2048_BITS = 2048;

	const KEY_I00 = 'I00';
	const KEY_I10 = 'I10';
	const KEY_I20 = 'I20';
	const KEY_D00 = 'D00';
	const KEY_D10 = 'D10';
	const KEY_D20 = 'D20';
	const KEY_F00 = 'F00';
	const KEY_F10 = 'F10';
	const KEY_F20 = 'F20';
	const KEY_500 = '500';
	const KEY_510 = '510';
	const KEY_520 = '520';
	const KEY_G00 = 'G00';
	const KEY_G10 = 'G10';
	const KEY_G20 = 'G20';
	const KEY_300 = '300';
	const KEY_310 = '310';
	const KEY_320 = '320';
	const KEY_C00 = 'C00';
	const KEY_C10 = 'C10';
	const KEY_C20 = 'C20';
	const KEY_E00 = 'E00';
	const KEY_E10 = 'E10';
	const KEY_E20 = 'E20';
	const KEY_400 = '400';
	const KEY_410 = '410';
	const KEY_420 = '420';
	const KEY_H00 = 'H00';
	const KEY_H10 = 'H10';
	const KEY_H20 = 'H20';
	const KEY_UPS = 'UPS';
	const KEY_UPN = 'UPN';
	const KEY_UPSS = 'UPSS';
	const KEY_UPNS = 'UPNS';
	const KEY_UES = 'UES';
	const KEY_UEN = 'UEN';
	const KEY_BNDL = 'BNDL';
	const KEY_BPS = 'BPS';
	const KEY_BPN = 'BPN';
	const KEY_BES = 'BES';
	const KEY_BEN = 'BEN';
	const YARDI_SIPP_PLATFORM = 'SQL Server';

	const YARDI_SIPP_INTERFACEENTITY = 'Entrata Vendor Invoicing';

	const YARDI_SIPP_INTERFACELICENSE = 'MIIBEAYJKwYBBAGCN1gDoIIBATCB/gYKKwYBBAGCN1gDAaCB7zCB7AIDAgABAgJoAQICAIAEAAQQ2M3tDQOmY/yf2XxKoN9JjASByNboJvToTKOXSygy8cu46exlILcDf2TEwk9l/6zGMrLw3LgY7Q48fBIFgXEKLaaCpt4xApgtmvS3CVQ02syR+Dg6TGvgvrBeKqJMzHoJjMHMvCBq7Z/GX2vzqMPyZOpb3XavX9ia/GSoGsyvxBQZGcEwQ1PYxwBgMe+7my3DRn+tW9jbXH8+B8RRf7m7jpIO/jIQdabCPvSg3hLofLgKs7cNA/MeW2QHI9qUdFH8i5D+r8WV7XI94mbohBP91bZwG8zABP20yrPp';

	public static $c_arrstrAllMailDeliveryCodes = [
		self::KEY_I00 => 'Certified Mail - no guaranteed time to payee',
		self::KEY_I10 => 'Certified Mail - no guaranteed time to Overnight address 1',
		self::KEY_I20 => 'Certified Mail - no guaranteed time to Overnight address 2',
		self::KEY_D00 => 'FedEx 2-day - 2nd day no guaranteed time to payee',
		self::KEY_D10 => 'FedEx 2-day - 2nd day no guaranteed time to Overnight address 1',
		self::KEY_D20 => 'FedEx 2-day - no guaranteed time to Overnight address 2',
		self::KEY_F00 => 'FedEx International  - no guaranteed time to payee',
		self::KEY_F10 => 'FedEx International, no guaranteed time to Overnight address 1',
		self::KEY_F20 => 'FedEx International, no guaranteed time to Overnight address 2',
		self::KEY_500 => 'FedEx Priority - 10am guarantee to payee',
		self::KEY_510 => 'FedEx Priority - 10am guarantee to Overnight address 1',
		self::KEY_520 => 'FedEx Priority - 10am guarantee to Overnight address 2',
		self::KEY_G00 => 'FedEx Standard - next day, no guaranteed time to payee',
		self::KEY_G10 => 'FedEx Standard - next day, no guaranteed time to Overnight address 1',
		self::KEY_G20 => 'FedEx Standard - next day, no guaranteed time to Overnight address 2',
		self::KEY_300 => 'Foreign Mail, no guarantee to payee',
		self::KEY_310 => 'Foreign Mail, no guaranteed time to Overnight address 1',
		self::KEY_320 => 'Foreign Mail, no guaranteed time to Overnight address 2',
		self::KEY_C00 => 'UPS 2-day - 2nd day no guaranteed time to payee',
		self::KEY_C10 => 'UPS 2-day - 2nd day no guaranteed time to Overnight address 1',
		self::KEY_C20 => 'UPS 2-day - 2nd day no guaranteed time to Overnight address 2',
		self::KEY_E00 => 'UPS International - no guaranteed time to payee',
		self::KEY_E10 => 'UPS International - no guaranteed time to Overnight address 1',
		self::KEY_E20 => 'UPS International - no guaranteed time to Overnight address 2',
		self::KEY_400 => 'UPS Priority - 10am guarantee to payee',
		self::KEY_410 => 'UPS Priority - 10am guarantee to payee, back to Overnight address 1',
		self::KEY_420 => 'UPS Priority - 10am guarantee to payee, back to Overnight address 2',
		self::KEY_H00 => 'UPS Standard -  next day, no guaranteed time to payee',
		self::KEY_H10 => 'UPS Standard - next day, no guaranteed time to Overnight address 1',
		self::KEY_H20 => 'UPS Standard - next day, no guaranteed time to Overnight address 2'
	];

	public static $c_arrstrIntegratedPaymentMailDeliveryCodes = [
		self::KEY_UPS   => 'UPS Priority Signature',
		self::KEY_UPN   => 'UPS Priority; No Signature',
		self::KEY_UPSS  => 'UPS Priority; Saturday Delivery',
		self::KEY_UPNS  => 'UPS Priority, No Signature; Saturday Delivery',
		self::KEY_UES   => 'UPS Economy; Signature',
		self::KEY_UEN   => 'UPS Economy, No Signature',
		self::KEY_BNDL  => 'Bundled to Payer via United States Postal Service 1st Class',
		self::KEY_BPS   => 'Bundled to Payer via UPS Priority; Signature',
		self::KEY_BPN   => 'Bundled to Payer via UPS Priority, No Signature',
		self::KEY_BES   => 'Bundled to Payer via UPS Economy; Signature',
		self::KEY_BEN   => 'Bundles to Pater via UPS Economy; No Signature'
	];

	public function __construct() {
        parent::__construct();
        $this->m_boolAllowDifferentialUpdate = true;
    }

    /**
     * Get Functions
     */

	public function getUsername() {
		if( false == valStr( $this->getUsernameEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getUsernameEncrypted(), CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getPassword() {
		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
    }

    public function getDocumentId() {
    	return $this->m_intDocumentId;
    }

    public function getKey() {
		return $this->m_strKey;
    }

    public function getCompanyUserId() {
		return $this->m_strCompanyUserId;
    }

    public function getApplication() {
		return $this->m_objApplication;
    }

    public function getLease() {
    	return $this->m_objLease;
    }

    public function getProperty() {
		return $this->m_objProperty;
    }

    public function getClient() {
		return $this->m_objClient;
    }

    public function getKeyValueData() {
		return $this->m_arrmixKeyValueData;
    }

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function getKeyBased() {
		return $this->m_boolKeyBased;
	}

	public function getAccountingExportSchedulerId() {
		return $this->m_intAccountingExportSchedulerId;
	}

	public function getPassphrase() {
		if( !valStr( $this->getPassphraseEncrypted() ) ) {
			return NULL;
		}
		return ( CSyncEncryption::createService() )->decryptText( $this->m_strPassphraseEncrypted );
	}

	public function getConfirmPassphrase() {
		return $this->m_strConfirmPassphrase;
	}

	public function getIsInternalPgpKey() {
		return $this->m_boolIsInternalPgpKey;
	}

	public function getIsExternalPgpKey() {
		return $this->m_boolIsExternalPgpKey;
	}

	public function getAdditionalNodesField( $mixPath ) {
		$jsonDetails = $this->getAdditionalNodes() ?? new stdClass();
		$mixValue = $this->getObjectPath( $jsonDetails, $mixPath );
		return $mixValue;
	}

	public function getIsPublicKey() {
		return $this->m_boolIsPublicKey;
	}

	/**
	 * Set Functions
	 */

	public function setUsername( $strUsername ) {
		if( true == valStr( $strUsername ) ) {
			$this->setUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
    }

    public function setDocumentId( $intDocumentId ) {
    	$this->m_intDocumentId = $intDocumentId;
    }

	public function setKey( $strKey ) {
		$this->m_strKey = $strKey;
    }

	public function setCompanyUserId( $strCompanyUserId ) {
		$this->m_strCompanyUserId = $strCompanyUserId;
    }

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
    }

    public function setLease( $objLease ) {
    	$this->m_objLease = $objLease;
    }

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
    }

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
    }

	public function setKeyValueData( $arrmixKeyValueData ) {
		$this->m_arrmixKeyValueData = $arrmixKeyValueData;
    }

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function setKeyBased( $boolKeyBased ) {
		$this->m_boolKeyBased = $boolKeyBased;
	}

	public function setAccountingExportSchedulerId( $intAccountingExportSchedulerId ) {
		$this->m_intAccountingExportSchedulerId = $intAccountingExportSchedulerId;
	}

	public function setPassphrase( $strPassphrase ) {
		$this->setPassphraseEncrypted( ( ( valStr( $strPassphrase ) ) ? CSyncEncryption::createService()->encryptText( $strPassphrase ) : NULL ) );
	}

	public function setConfirmPassphrase( $strConfirmPassphrase ) {
		$this->m_strConfirmPassphrase = $strConfirmPassphrase;
	}

	public function setIsInternalPgpKey( $boolIsInternalPgpKey ) {
		$this->m_boolIsInternalPgpKey = $boolIsInternalPgpKey;
	}

	public function setIsExternalPgpKey( $boolIsExternalPgpKey ) {
		$this->m_boolIsExternalPgpKey = $boolIsExternalPgpKey;
	}

	public function setIsPublicKey( $boolIsPublicKey ) {
		$this->m_boolIsPublicKey = $boolIsPublicKey;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['username'] ) ) 							$this->setUsername( $arrmixValues['username'] );
        if( true == isset( $arrmixValues['password'] ) ) 							$this->setPassword( $arrmixValues['password'] );
        if( true == isset( $arrmixValues['property_id'] ) )							$this->setPropertyId( $arrmixValues['property_id'] );
        if( true == isset( $arrmixValues['document_id'] ) )							$this->setDocumentId( $arrmixValues['document_id'] );
        if( true == isset( $arrmixValues['key'] ) ) 								$this->setKey( $arrmixValues['key'] );
        if( true == isset( $arrmixValues['status'] ) ) 								$this->setStatus( $arrmixValues['status'] );
        if( true == isset( $arrmixValues['key_based'] ) ) 							$this->setKeyBased( $arrmixValues['key_based'] );
        if( true == isset( $arrmixValues['passphrase'] ) ) 							$this->setPassphrase( $arrmixValues['passphrase'] );
        if( true == isset( $arrmixValues['confirm_passphrase'] ) ) 					$this->setConfirmPassphrase( $arrmixValues['confirm_passphrase'] );
		if( true == isset( $arrmixValues['accounting_export_scheduler_id'] ) )     	$this->setAccountingExportSchedulerId( $arrmixValues['accounting_export_scheduler_id'] );
		if( true == isset( $arrmixValues['internal_pgp_key'] ) ) 					$this->setIsInternalPgpKey( $arrmixValues['internal_pgp_key'] );
		if( true == isset( $arrmixValues['external_pgp_key'] ) ) 					$this->setIsExternalPgpKey( $arrmixValues['external_pgp_key'] );
		if( true == isset( $arrmixValues['is_public_key'] ) ) 					    $this->setIsPublicKey( $arrmixValues['is_public_key'] );
        return;
    }

	/**
	 * Validation Functions
	 */

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        if( true == is_null( $this->getCid() ) ) {
          	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valTransmissionTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getTransmissionTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_type_id', __( 'Transmission type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTransmissionVendorId() {
        $boolIsValid = true;

        if( true == is_null( $this->getTransmissionVendorId() ) ) {
           $boolIsValid = false;
           $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_vendor_id', __( 'Transmission vendor is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTransmissionConnectionTypeId() {
        $boolIsValid = true;

      	if( true == is_null( $this->getTransmissionConnectionTypeId() ) ) {
       	    $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_connection_type_id', __( 'Transmission connection type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valUsername() {
		$boolIsValid = true;

		if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::LEVEL_1, CTransmissionVendor::LEAD_TO_LEASE, CTransmissionVendor::LEAD_TRACKING_SOLUTIONS, CTransmissionVendor::ONSITE, CTransmissionVendor::PEOPLESOFT ] ) ) {
    		return $boolIsValid;
		}

		if( false == valStr( $this->getUsername() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Username is required. ' ) ) );
		} else {
			$boolIsValidUsername = $this->validateTransmissionVendorUsername( $this->getUsername() );
			if( false == $boolIsValidUsername ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Whitespace is not allowed in the username.' ) ) );
			}
		}

		return $boolIsValid;
    }

    public function validateTransmissionVendorUsername( $strUsername = '' ) {
    	$boolIsValidUsername = true;

    	if( 0 < \Psi\CStringService::singleton()->strpos( $strUsername, ' ' ) ) {
    		$boolIsValidUsername = false;
    	}

    	return $boolIsValidUsername;
    }

 	public function valPassword() {
    	$boolIsValid = true;

    	if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::LEVEL_1, CTransmissionVendor::LEAD_TO_LEASE, CTransmissionVendor::LEAD_TRACKING_SOLUTIONS, CTransmissionVendor::ONSITE, CTransmissionVendor::PEOPLESOFT ] ) ) {
    		return $boolIsValid;
		}

        if( false == valStr( $this->getPassword() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password is required.' ) ) );
		}

        return $boolIsValid;
    }

    public function valName( $strAction, $objDatabase ) {
       $boolIsValid = true;

       if( false == valStr( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

       } else {
       		$objCompanyTransmissionVendor = \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchConflictingCompanyTransmissionVendor( $this, $objDatabase );

	        if( true == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) ) {
	        	if( true == is_null( $this->getId() ) && VALIDATE_INSERT == $strAction ) {
	        		$boolIsValid = false;
	        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already being used. Please enter different name.' ) ) );

	        	} elseif( $this->getId() != $objCompanyTransmissionVendor->getId() && VALIDATE_UPDATE == $strAction ) {
	        		$boolIsValid = false;
	        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already being used. Please enter different name.' ) ) );
	        	}
	        }
       }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;

        if( false == is_null( $this->getTransmissionConnectionTypeId() ) && 1 == $this->getTransmissionConnectionTypeId() ) {

         	if( true == is_null( $this->getEmailAddress() ) ) {
         		$boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );

         	} elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter email address in proper format.' ) ) );
			}
        }

        return $boolIsValid;
    }

	public function valClientNumber() {
		$boolIsValid = true;

		if( false != in_array( $this->getTransmissionTypeId(), CTransmissionType::$c_arrintAccountingExportTransmissionTypes ) ) {
			if( false == \Psi\CStringService::singleton()->preg_match( '/^[\w\/\\\]*$/', $this->getClientNumber() ) ) {
			$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', __( 'File path cannot contain special characters except {%s, 0}', [ '_ \ /' ] ) ) );
			}
		} else {
			if( true == is_null( $this->getClientNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_number', __( 'Client number is required.' ) ) );
			}
		}
		return $boolIsValid;
	}

    public function valVendorCode() {
        $boolIsValid = true;

        if( true == is_null( $this->getVendorCode() ) ) {
        	$boolIsValid = false;

        	if( CTransmissionVendor::LEAD_TO_LEASE == $this->getTransmissionVendorId() ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', __( 'AppKey is required.' ) ) );

        	} elseif( false != in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', __( 'Server Name is required.' ) ) );
	        } else {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', __( 'Vendor code is required.' ) ) );
        	}
        }

        return $boolIsValid;
    }

    public function valRequestUrl() {

    	$boolIsValid = true;

    	if( CTransmissionVendor::LEVEL_1 == $this->getTransmissionVendorId() ) {
    		return $boolIsValid;
    	}
    	if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::BLUE_MOON, CTransmissionVendor::APARTMENTS, CTransmissionVendor::YIELDSTAR_EXPORT ] ) ) {
    		 if( true == is_null( $this->m_strRequestUrl ) || 0 == strlen( $this->m_strRequestUrl ) ) {
    		 	$boolIsValid = false;
    		 	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Address is required.' ) ) );
    		 }

    		 return $boolIsValid;

	    } elseif( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) && CTransmissionConnectionType::URL == $this->getTransmissionConnectionTypeId() ) {
		    if( true == is_null( $this->m_strRequestUrl ) || 0 == strlen( $this->m_strRequestUrl ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Request Url is required.' ) ) );

		    } elseif( false == CValidation::checkUrl( $this->m_strRequestUrl ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Request Url is not correct.' ) ) );
		    }

		    return $boolIsValid;
	    } elseif( $this->getTransmissionVendorId() == CTransmissionVendor::WELLS_FARGO_PAYMENT_MANAGER || true == in_array( $this->getTransmissionTypeId(), CTransmissionType::$c_arrintExportTransmissionTypes ) ) {
			if( false == valStr( $this->getRequestUrl() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Host name is required.' ) ) );
		    } elseif( filter_var( $this->getRequestUrl(), FILTER_VALIDATE_IP ) === false && filter_var( gethostbyname( $this->getRequestUrl() ), FILTER_VALIDATE_IP ) === false && $this->getTransmissionTypeId() != CTransmissionType::DATA_EXPORT ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Please enter a valid host name.' ) ) );
			}
			return $boolIsValid;
		} else {
    		if( true == is_null( $this->m_strRequestUrl ) || 0 == strlen( $this->m_strRequestUrl ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Request Url is required.' ) ) );

    		} elseif( false == CValidation::checkUrl( $this->m_strRequestUrl ) ) {
        		$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Request Url is not correct.' ) ) );
        	}

    		return $boolIsValid;
    	}
    }

    public function valFileNamePrefix() {
		$boolIsValid = true;

		if( false == valStr( $this->getAdditionalNodes()->prefix ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'File Name Prefix is required.' ) ) );
		}
		return $boolIsValid;
	}

    public function valRequestUrlOrIp() {

    	$boolIsValid = true;
		if( false == valStr( $this->getRequestUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Host name is required.' ) ) );
		} elseif( filter_var( $this->getRequestUrl(), FILTER_VALIDATE_IP ) === false && filter_var( gethostbyname( $this->getRequestUrl() ), FILTER_VALIDATE_IP ) === false ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', __( 'Please enter a valid host name.' ) ) );
		}
		return $boolIsValid;
    }

	public function valRetrievalUrl() {

		if( false == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::SAFERENT, CTransmissionVendor::TAZ_WORKS, CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
			return true;
		}

		$boolIsValid = true;
		if( true == is_null( $this->m_strRetrievalUrl ) || 0 == strlen( trim( $this->m_strRetrievalUrl ) ) ) {
			$boolIsValid = false;
			if( in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrival_url', __( 'Database Name is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrival_url', __( 'Retrieval Url is required.' ) ) );
			}

			return $boolIsValid;

		} elseif( false == CValidation::checkUrl( $this->m_strRequestUrl ) ) {
			$boolIsValid = false;
			if( in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrival_url', __( 'Database Name is not correct.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrival_url', __( 'Retrieval Url is not correct.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPort() {

		$boolIsValid = true;
		if( false != in_array( $this->getTransmissionTypeId(), CTransmissionType::$c_arrintExportTransmissionTypes ) ) {
			if( false == is_null( $this->getPort() ) ) {
				if( false == is_numeric( $this->getPort() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port is not valid.' ) ) );
				}
			}
			if( true == is_null( $this->getPort() ) && CTransmissionType::DATA_EXPORT == $this->getTransmissionTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port is required.' ) ) );
			}
		} else {
			if( true == is_null( $this->getPort() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

    public function valUsernameEncrypted() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valAssociatedProperties( $objDatabase ) {

		$boolIsValid 			= true;
		$intTransmissionCount	= 0;

		$strWhereCondition = ' WHERE company_transmission_vendor_id = ' . ( int ) $this->m_intId . ' AND cid = ' . ( int ) $this->getCid();

		$intTransmissionCount 	= CPropertyTransmissionVendors::fetchPropertyTransmissionVendorCount( $strWhereCondition, $objDatabase );
		$intTransmissionCount 	= ( 0 == $intTransmissionCount ) ? CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionCount( $strWhereCondition, $objDatabase ) : $intTransmissionCount;
		$intTransmissionCount 	= ( 0 == $intTransmissionCount ) ? CTransmissions::fetchTransmissionCount( $strWhereCondition, $objDatabase ) : $intTransmissionCount;

		if( 0 != $intTransmissionCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} this vendor has already associated records.', [ $this->getName() ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
    }

    public function valEncryptionTypeId() {
    	$boolIsValid = true;

    	return $boolIsValid;
    }

    public function valPublicKey() {
    	$boolIsValid = true;

    	return $boolIsValid;
    }

    public function valSecretKeyEncrypted() {
    	$boolIsValid = true;

    	return $boolIsValid;
    }

    public function valPassphraseEncrypted() {
		$boolIsValid = true;

		if( false == valStr( $this->getPassphraseEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'passphrase', __( 'Passphrase is required.' ) ) );
		}

		if( false != $boolIsValid ) {
			if( false != $boolIsValid && ( 8 > \Psi\CStringService::singleton()->strlen( $this->getPassphraseEncrypted() ) || false == \Psi\CStringService::singleton()->preg_match( '/[a-z]/i', $this->getPassphraseEncrypted() ) || false == \Psi\CStringService::singleton()->preg_match( '/\d/', $this->getPassphraseEncrypted() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'passphrase', __( 'Passphrase must contain at least eight characters, including at least one letter and one number.' ) ) );
			}
		}

		return $boolIsValid;
    }

	public function valConfirmPassphrase() {
		$boolIsValid = true;
		if( false == valStr( $this->getConfirmPassphrase() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'passphrase', __( 'Confirm Passphrase is required.' ) ) );
		}
		if( false != $boolIsValid && 0 !== strcmp( $this->getConfirmPassphrase(), $this->getPassphrase() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_pass_phrase', __( 'Passphrase and Confirm Passphrase does not match.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPgpExpiryTime() {
		$boolIsValid = true;
		if( true == is_null( $this->getAdditionalNodes()->pgp_settings->sign_key->expire_in ) || 0 === preg_match( '~[0-9]~', $this->getAdditionalNodes()->pgp_settings->sign_key->expire_in ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pgp_internal_key_expire_in', __( 'Expires In is required.' ) ) );
		}
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:

            	if( false == $boolIsValid ) {
					return $boolIsValid;
            	}

	            $boolIsValid &= $this->valCid();
	            $boolIsValid &= $this->valTransmissionTypeId();
	            $boolIsValid &= $this->valTransmissionVendorId();
	            $boolIsValid &= $this->valName( $strAction, $objDatabase );

				if( CTransmissionType::WELLS_FARGO_PAYMENT_MANAGER == $this->getTransmissionTypeId() ) {
					$boolIsValid &= $this->valFileNamePrefix();
				}

		        if( false == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::CUSTOM, CTransmissionVendor::YIELDSTAR, CTransmissionVendor::LRO ] ) ) {
			        $boolIsValid &= $this->valUsername();
			        $boolIsValid &= $this->valPassword();
		        }

	            if( false == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::YIELDSTAR, CTransmissionVendor::LRO, CTransmissionVendor::UBSEXPORT, CTransmissionVendor::BLUE_MOON_DOC_STORAGE, CTransmissionVendor::CUSTOM, CTransmissionVendor::COMPLIANCE_DEPOT, CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
			        $boolIsValid &= $this->valRequestUrl();
	            }

				if( false == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::VAULTWARE, CTransmissionVendor::CUSTOM, CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
					$boolIsValid &= $this->valTransmissionConnectionTypeId();
					$boolIsValid &= $this->valRetrievalUrl();
					$boolIsValid &= $this->valEmailAddress();
				}

				if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::VAULTWARE, CTransmissionVendor::YIELDSTAR, CTransmissionVendor::LRO, CTransmissionVendor::UBSEXPORT ] ) ) {
					$boolIsValid &= $this->valClientNumber();
				}

				if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::LEAD_TO_LEASE, CTransmissionVendor::LEAD_TRACKING_SOLUTIONS, CTransmissionVendor::VAULTWARE, CTransmissionVendor::BLUE_MOON_DOC_STORAGE ] ) ) {
					$boolIsValid &= $this->valVendorCode();
				}

				if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::APARTMENTS, CTransmissionVendor::DATA_EXPORT ] ) ) {
					$boolIsValid &= $this->valPort();
				}
				if( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::GL_EXPORT_YARDI_SIPP, CTransmissionVendor::AP_EXPORT_YARDI_SIPP ] ) ) {
					$boolIsValid &= $this->valRequestUrl();
					$boolIsValid &= $this->valRetrievalUrl();
					$boolIsValid &= $this->valVendorCode();
				}
				break;

            case VALIDATE_DELETE:
        		if( false == $boolIsValid ) {
            		return $boolIsValid;
            	}

            	$boolIsValid &= $this->valAssociatedProperties( $objDatabase );
				break;

	        case 'validate_test_connection':
		        $boolIsValid &= $this->valRequestUrlOrIp();
		        $boolIsValid &= $this->valUsername();
		        break;

		    case 'validate_ftp':
				if( false == $boolIsValid ) {
					return $boolIsValid;
				}
				$strAction = VALIDATE_INSERT;
				if( false != valId( $this->getId() ) ) {
					$strAction = VALIDATE_UPDATE;
				}
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
				$boolIsValid &= $this->valUsername();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valRequestUrl();
				break;

			case 'validate_sftp_password_based':
				if( false == $boolIsValid ) {
					return $boolIsValid;
				}
				$strAction = VALIDATE_INSERT;
				if( false != valId( $this->getId() ) ) {
					$strAction = VALIDATE_UPDATE;
				}
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
				$boolIsValid &= $this->valUsername();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valRequestUrl();
				if( CTransmissionType::WELLS_FARGO_PAYMENT_MANAGER == $this->getTransmissionTypeId() ) {
					$boolIsValid &= $this->valFileNamePrefix();
				}
				break;

			case 'validate_sftp_key_based':
				if( false == $boolIsValid ) {
					return $boolIsValid;
				}
				$strAction = VALIDATE_INSERT;
				if( false != valId( $this->getId() ) ) {
					$strAction = VALIDATE_UPDATE;
				}
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
				$boolIsValid &= $this->valPassphraseEncrypted();
				$boolIsValid &= $this->valConfirmPassphrase();
				if( false != in_array( $this->getTransmissionTypeId(), [ CTransmissionType::WELLS_FARGO_PAYMENT_MANAGER, CTransmissionType::INTEGRATED_PAYABLES ] ) ) {
					$boolIsValid &= $this->valFileNamePrefix();
				}
				break;

			case 'insert_or_update_export':
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valRequestUrl();
				$boolIsValid &= $this->valUserName();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valClientNumber();
				$boolIsValid &= $this->valPort();
				break;

			case 'validate_pgp_keys':
				if( false != $this->getIsInternalPgpKey() ) {
					$boolIsValid &= $this->valPgpExpiryTime();
				}
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function fetchPropertyTransmissionVendorByPropertyId( $intPropertyId, $objDatabase ) {
    	return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByCompanyTransmissionVendorIdByCid( $intPropertyId, $this->m_intId, $this->m_objClient->getId(), $objDatabase );
    }

	/**
	 * @return \CTransmission
	 */

	public function createTransmission() {

		$objTransmission = new CTransmission();

		$objTransmission->setDefaults();
		$objTransmission->setCid( $this->getCid() );
    	$objTransmission->setTransmissionTypeId( $this->getTransmissionTypeId() );
		$objTransmission->setTransmissionVendorId( $this->getTransmissionVendorId() );
		$objTransmission->setCompanyTransmissionVendorId( $this->getId() );

		return $objTransmission;
    }

    public function process() {

    	$objTransmissonLibrary = CTransmissionFactory::createTransmissionLibrary( $this );

    	if( false == is_object( $objTransmissonLibrary ) ) return false;

    	$boolIsValid = $objTransmissonLibrary->process();

    	if( true == valArr( $this->getErrorMsgs() ) ) {

    		foreach( $this->getErrorMsgs() as $objErrorMessage ) {
    			$this->addErrorMsg( $objErrorMessage );
    		}
    	}

    	return $boolIsValid;
    }

    // this function is overridden specifically to handle the Blue Moon Lease Execution Documents' name Whenever the name of CTV changes
    // We have a dependency to keep the document name same as the CTV name.

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	$this->unSerializeAndSetOriginalValues();

    	if( $this->sqlName() != $this->getOriginalValueByFieldName( 'name' ) ) {
    		$objDocument = CDocuments::fetchBlueMoonLeaseDocumentByNameByCid( $this->getOriginalValueByFieldName( 'name' ), $this->getCid(), $objDatabase );
    	}

    	$strResult = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );

    	if( true == valObj( $objDocument, 'CDocument' ) && false != $strResult && $this->getName() != $objDocument->getName() ) {
    		$objDocument->setName( $this->getName() );

    		if( false == $objDocument->update( $intCurrentUserId, $objDatabase ) ) {
    			trigger_error( 'Document Object failed to update Id ' . $objDocument->getId(), E_USER_WARNING );
    		}
    	}

    	return $strResult;
    }

	public function getAdditionalNodesWithData( $objStdScreeningApplicationDetails ) {

		$objApplicant               = $objStdScreeningApplicationDetails->objApplicant;
		$objDatabase                = $objStdScreeningApplicationDetails->database;
		$arrmixAdditionalNodes      = $objStdScreeningApplicationDetails->AdditionalNodes;
		$arrmixAdditionalXmlNodes   = [];

		$arrobjCustomerIncomes = rekeyObjects( 'CustomerId', ( array ) \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCidByIncomeTypeIdsByCustomerIds( $objApplicant->getCid(), [ CIncomeType::CURRENT_EMPLOYER ], array_keys( rekeyObjects( 'CustomerId', [ $objApplicant ] ) ), $objDatabase ), $boolHasMultipleObjectsWithSameKey = true );

		$arrobjApplicantCustomerIncomes = ( true == isset( $arrobjCustomerIncomes[$objApplicant->getCustomerId()] ) ) ? $arrobjCustomerIncomes[$objApplicant->getCustomerId()] : [];

		$intApplicantEmployerAmount       = 0;
		$arrstrCurrentEmployerNames       = [];
		$arrmixCurrentEmployerDateStarted = [];
		$arrmixCurrentEmployerDateEnded   = [];

		foreach( $arrobjApplicantCustomerIncomes as $objCustomerIncome ) {
			$intApplicantEmployerAmount += CScreeningUtils::getPeriodicIncome( $objCustomerIncome );
			array_push( $arrstrCurrentEmployerNames, $objCustomerIncome->getInstitutionName() );
			array_push( $arrmixCurrentEmployerDateStarted, $objCustomerIncome->getDateStarted() );
			array_push( $arrmixCurrentEmployerDateEnded, $objCustomerIncome->getDateEnded() );
		}

		$strCurrentEmployerName      = implode( ',', $arrstrCurrentEmployerNames );
		$strCurrentEmployerEndDate   = implode( ',', $arrmixCurrentEmployerDateEnded );
		$strCurrentEmployerStartDate = implode( ',', $arrmixCurrentEmployerDateStarted );

		if( true == valArr( $arrmixAdditionalNodes ) ) {

			foreach( $arrmixAdditionalNodes as $strNode => $arrmixAdditionalNode ) {

				switch( $strNode ) {

					case 'EmploymentGrossIncome':
						// Fetch monthly income of all current employers
						$fltApplicantEmployerAmount = 0;

						$arrobjApplicantEmployers = $objApplicant->fetchCurrentCustomerIncomes( $objDatabase );
						if( true == valArr( $arrobjApplicantEmployers ) ) {
							foreach( $arrobjApplicantEmployers as $objApplicantEmployer ) {
								$fltApplicantEmployerAmount += CScreeningUtils::getPeriodicIncome( $objApplicantEmployer );
							}
						}

						$arrmixAdditionalXmlNodes['EmploymentGrossIncome'] = ( float ) $fltApplicantEmployerAmount;
						break;

					case 'EmploymentType':
						$arrmixAdditionalXmlNodes['EmploymentType'] = 'Current';
						break;

					case 'EmployerName':
						$arrmixAdditionalXmlNodes['EmployerName'] = $strCurrentEmployerName;
						break;

					case 'EmploymentStartDate':
						$arrmixAdditionalXmlNodes['EmploymentStartDate'] = $strCurrentEmployerStartDate;
						break;

					case 'EmploymentEndDate':
						$arrmixAdditionalXmlNodes['EmploymentEndDate'] = $strCurrentEmployerEndDate;
						break;

					case 'OtherIncomeAmount':
						$fltApplicantAllOtherIncome = 0.0;

						// Fetch all other income for BCS
						if( CTransmissionVendor::BCS == $this->getTransmissionVendorId() ) {
							$arrmixApplicantAllOtherIncomes = $objApplicant->fetchAllOtherCustomerIncomes( $objDatabase );
						} else {
							// Fetch all other income
							$arrmixApplicantAllOtherIncomes = $objApplicant->fetchOtherCustomerIncomes( $objDatabase );
						}

						if( true == valArr( $arrmixApplicantAllOtherIncomes ) ) {
							foreach( $arrmixApplicantAllOtherIncomes as $objOtherIncome ) {
								$fltApplicantAllOtherIncome += CScreeningUtils::getPeriodicIncome( $objOtherIncome );
							}
						}
						$arrmixAdditionalXmlNodes['OtherIncomeAmount'] = ( float ) $fltApplicantAllOtherIncome;
						break;

					default:
						// default condition
						break;
				}
			}
		}

		return $arrmixAdditionalXmlNodes;
	}

	/**
	 * @param null $strVendor
	 * @return mixed|null
	 */
	protected function calcStorageContainer( $strVendor = NULL ) {

		return CConfig::get( 'OSG_BUCKET_SYSTEM_TRANSMISSIONS_DATA' );

	}

	/**
	 * @param null $strReferenceTag : value for reference tag will be file name with ext.- .ppk or .asc
	 * @param null $strVendor
	 * @return string
	 */
	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		$strParentFolder = 'pgp_keys';
		$arrstrFileNamePart = explode( '.', $strReferenceTag );
		if( 'ppk' == end( $arrstrFileNamePart ) ) {
			$strParentFolder = 'sftp_keys';
		}

		return $strParentFolder . '/' . $this->getCid() . '/' . $this->getId() . '/' . $this->calcFileName( $strParentFolder );
	}

	public function getStorageGateWay() {
		return CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3;
	}

	/**
	 * @param $strReferenceTag : value for $strReferenceTag is either sftp_keys or pgp_keys
	 * @return string
	 */
	public function calcFileName( $strReferenceTag = 'sftp_keys' ) {
		$strFileName = NULL;
		if( 'sftp_keys' == $strReferenceTag ) {
			$strFileName = str_replace( ' ', '_', str_replace( "'", '_', $this->getName() ) );
			if( false != $this->getIsPublicKey() ) {
				$strFileName .= '_public';
			}
			$strFileName .= '.ppk';
		} elseif( 'pgp_keys' == $strReferenceTag ) {
			$strKeyName = $this->getAdditionalNodesField( [ 'pgp_settings', 'sign_key', 'key_name' ] );
			if( false != $this->getIsExternalPgpKey() ) {
				$strKeyName  = $this->getAdditionalNodesField( [ 'pgp_settings', 'encryption_key', 'key_name' ] ); // getting file name with ext.
				$strFileName = 'external_' . $strKeyName;
			} else if( false != $this->getIsPublicKey() ) {
				$strFileName = $strKeyName . '_public_key.asc';
			} elseif( false == $this->getIsPublicKey() ) {
				$strFileName = 'encrypted_' . $strKeyName . '_private_key.asc';
			}
		}

		return $strFileName;
	}

	/**
	 * @param      $strReferenceTag : value for referenceTag is sftp_keys or pgp_keys
	 * @param bool $boolRawData
	 * @return object|null
	 */
	public function getKeyResponse( $strReferenceTag, $boolRawData = false ) {
		$strFileName     = $this->calcFileName( $strReferenceTag );
		$objStoredObject = $this->fetchStoredObject( $this->m_objDatabase, $strFileName );
		// The below needs to be removed once we migrate the existing keys
		if( false == valId( $objStoredObject->getId() ) && 'sftp_keys' == $strReferenceTag ) {
			$objStoredObject->setContainer( 'Global/sftpkeys/' . $objStoredObject->getCid() . '/' );
			$objStoredObject->setKey( $strFileName );
			$objStoredObject->setVendor( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM );
		}

		$arrmixObjectConfiguration = $objStoredObject->createGatewayRequest();
		// Fix Temprory fix till migration for sftp and pgp
		$arrstrKeyPart = explode( '/', $arrmixObjectConfiguration['key'] );
		if( $arrstrKeyPart[0] == $objStoredObject->getCid() || ( false == valId( $objStoredObject->getId() ) && 'sftp_keys' == $strReferenceTag ) ) {
			unset( $arrmixObjectConfiguration['cid'] );
		}
		unset( $arrmixObjectConfiguration['legacyStorageGateway'] );
		unset( $arrmixObjectConfiguration['legacyContainer'] );
		unset( $arrmixObjectConfiguration['legacyKey'] );
		$arrmixGatewayResponse     = $this->getObjectStorageGateway( $objStoredObject->getVendor() )->getObject( $arrmixObjectConfiguration );

		return ( true == $boolRawData ) ? ( $arrmixGatewayResponse['data'] ?? NULL ) : $arrmixGatewayResponse;

	}

	private function getObjectStorageGateway( $strObjectStorageGatewayName = CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM ) {
		return CObjectStorageGatewayFactory::createObjectStorageGateway( $strObjectStorageGatewayName );
	}

}
?>