<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposalStatusTypes
 * Do not add any new functions to this class.
 */

class CProposalStatusTypes extends CBaseProposalStatusTypes {

	public static function fetchProposalStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CProposalStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchProposalStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CProposalStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>