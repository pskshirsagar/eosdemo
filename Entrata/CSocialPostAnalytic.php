<?php

class CSocialPostAnalytic extends CBaseSocialPostAnalytic {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSocialPostId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLikesCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommentCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShareCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewsCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClicksCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>