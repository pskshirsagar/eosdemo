<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CApplicationInterest extends CBaseApplicationInterest {

	protected $m_objPropertyFloorplan;
	protected $m_objPropertyBuilding;
	protected $m_objApplication;
	protected $m_objPropertyUnit;
	protected $m_objUnitSpace;
	protected $m_objAddOn;

	protected $m_strReason;
	protected $m_strRemoveFromWaitList;

	protected $m_arrintWaitlistApplicationRentDepositScheduledCharges;

	/**
	 * Val Functions
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valApplicationId() {
		$boolIsValid = true;
        if( true == is_null( $this->getApplicationId() ) ) {
       		$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', __( 'Application id is required.' ) ) );
       	}
       	return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
   	$boolIsValid = true;
       if( true == is_null( $this->getPropertyFloorplanId() ) ) {
	       	$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Property floorplan id is required.' ) ) );
       }
       return $boolIsValid;
    }

    public function valUnitSpaceId() {
       $boolIsValid = true;
       if( true == is_null( $this->getUnitSpaceId() ) ) {
	       	$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'Unit space id is required.' ) ) );
       }
       return $boolIsValid;
    }

    public function valAddOnId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
   		$boolIsValid = true;
       	if( true == is_null( $this->getEventTypeId() ) ) {
	      	 $boolIsValid = false;
	       	 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_type_id', __( 'Event type id is required.' ) ) );
      	}
        return $boolIsValid;
    }

    public function valIsEmailOffer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSmsOffer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPhoneOffer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMailOffer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validateOfferType() {
    	 $boolIsValid = true;

    	if( true == is_null( $this->getIsEmailOffer() ) && true == is_null( $this->getIsSmsOffer() ) && true == is_null( $this->getIsPhoneOffer() ) && true == is_null( $this->getIsMailOffer() ) ) {
    		 $boolIsValid = false;
    		 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, 'is_email_offer', __( 'Select atleast one communication type. ' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valReason() {
    	$boolIsValid = true;

    	if( 0 == strlen( trim( $this->getReason() ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, 'reason', __( 'Reason is required.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case 'decline_offer':
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valEventTypeId();
        		$boolIsValid &= $this->valReason();
        		break;

            case VALIDATE_INSERT:
	            $boolIsValid &= $this->valCid();
	            $boolIsValid &= $this->valPropertyId();
	            $boolIsValid &= $this->valPropertyFloorplanId();
            	$boolIsValid &= $this->valUnitSpaceId();
	            $boolIsValid &= $this->valApplicationId();
	            $boolIsValid &= $this->valEventTypeId();
            	$boolIsValid &= $this->validateOfferType();
	        	break;

        	case VALIDATE_ADD_ON_INSERT:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valAddOnId();
        		$boolIsValid &= $this->valApplicationId();
        		$boolIsValid &= $this->valEventTypeId();
        		$boolIsValid &= $this->validateOfferType();
        		break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getPropertyFloorplan() {
    	return $this->m_objPropertyFloorplan;
    }

    public function getPropertyBuilding() {
    	return $this->m_objPropertyBuilding;
    }

    public function getPropertyUnit() {
    	return $this->m_objPropertyUnit;
    }

    public function getUnitSpace() {
    	return $this->m_objUnitSpace;
    }

	public function getApplication() {
		return $this->m_objApplication;
	}

    public function getReason() {
    	return $this->m_strReason;
    }

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getRemoveFromWaitList() {
		return $this->m_strRemoveFromWaitList;
	}

	public function getWaitlistApplicationRentDepositScheduledCharges() {
		return $this->m_arrintWaitlistApplicationRentDepositScheduledCharges;
	}

    /**
     * Set Functions
     */

    public function setPropertyFloorplan( $objPropertyFloorplan ) {
    	$this->m_objPropertyFloorplan = $objPropertyFloorplan;
    }

    public function setPropertyBuilding( $objPropertyBuilding ) {
    	$this->m_objPropertyBuilding = $objPropertyBuilding;
    }

    public function setPropertyUnit( $objPropertyUnit ) {
    	$this->m_objPropertyUnit = $objPropertyUnit;
    }

    public function setUnitSpace( $objUnitSpace ) {
    	$this->m_objUnitSpace = $objUnitSpace;
    }

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

    public function setReason( $strReason ) {
    	$this->m_strReason = $strReason;
    }

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function setRemoveFromWaitList( $strRemoveFromWaitList ) {
		$this->m_strRemoveFromWaitList = $strRemoveFromWaitList;
	}

	public function setWaitlistApplicationRentDepositScheduledCharges( $arrintWaitlistApplicationRentDepositScheduledCharges ) {
		$this->m_arrintWaitlistApplicationRentDepositScheduledCharges = $arrintWaitlistApplicationRentDepositScheduledCharges;
	}

    /**
     * Other Functions
     */

    public function loadPropertyInfo( $objDatabase ) {
    	$this->m_objPropertyFloorplan 	= ( 0 < $this->getPropertyFloorplanId() ) ? $this->getOrFetchPropertyFloorplan( $objDatabase ) : NULL;
    	$this->m_objPropertyUnit 		= ( 0 < $this->getPropertyFloorplanId() ) ? $this->getOrFetchPropertyUnit( $objDatabase ) : NULL;
    	$this->m_objUnitSpace 			= ( 0 < $this->getPropertyFloorplanId() ) ? $this->getOrFetchUnitSpace( $objDatabase ) : NULL;
    	$this->m_objPropertyBuilding	= ( 0 < $this->getPropertyFloorplanId() ) ? $this->getOrFetchPropertyBuilding( $objDatabase ) : NULL;

    	if( true == valObj( $this->m_objPropertyFloorplan, 'CPropertyFloorplan' ) ) {
    		$objFloorplanMarketingMediaAssociation= $this->m_objPropertyFloorplan->fetchActiveMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::FLOORPLAN_2D, $objDatabase );
   			$this->m_objPropertyFloorplan->setMarketingMediaAssociation( $objFloorplanMarketingMediaAssociation );
    	}

    	if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
			$arrobjApartmentAmenities 	= $this->m_objUnitSpace->fetchPublishedAmenityRateAssociations( $objDatabase );
			$arrobjSpecials 	= $this->m_objUnitSpace->getOrFetchSpecials( $objDatabase );

			$this->m_objUnitSpace->setApartmentAmenities( $arrobjApartmentAmenities );
			$this->m_objUnitSpace->setSpecials( $arrobjSpecials );
    	}
    }

	public function getOrFetchPropertyFloorplan( $objDatabase ) {
		if( true == is_null( $this->getPropertyFloorplanId() ) || false == is_numeric( $this->getPropertyFloorplanId() ) ) return NULL;

		if( true == valObj( $this->m_objPropertyFloorplan, 'CPropertyFloorplan' ) )	return $this->m_objPropertyFloorplan;

		return $this->fetchPropertyFloorplan( $objDatabase );
	}

	public function getOrFetchPropertyUnit( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		if( true == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) ) return $this->m_objPropertyUnit;

		return $this->fetchPropertyUnit( $objDatabase );
	}

	public function getOrFetchUnitSpace( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) return $this->m_objUnitSpace;

		return $this->fetchUnitSpace( $objDatabase );
	}

	public function getOrFetchPropertyBuilding( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		if( true == valObj( $this->m_objPropertyBuilding, 'CPropertyBuilding' ) ) return $this->m_objPropertyBuilding;

		return $this->fetchPropertyBuilding( $objDatabase );
	}

	public function getOrFetchAddOn( $objDatabase ) {
		if( true == is_null( $this->getAddOnId() ) || false == is_numeric( $this->getAddOnId() ) ) return NULL;

		if( true == valObj( $this->m_objAddOn, 'CAddOn' ) ) return $this->m_objAddOn;

		return $this->fetchAddOn( $objDatabase );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyFloorplan( $objDatabase ) {
		if( true == is_null( $this->getPropertyFloorplanId() ) || false == is_numeric( $this->getPropertyFloorplanId() ) ) return NULL;

		$this->m_objPropertyFloorplan = \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByCid( $this->getPropertyFloorplanId(), $this->getCid(), $objDatabase );

		return $this->m_objPropertyFloorplan;
	}

	public function fetchPropertyUnit( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		$this->m_objPropertyUnit = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByUnitSpaceIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );

		return $this->m_objPropertyUnit;
	}

	public function fetchUnitSpace( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		$this->m_objUnitSpace = CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );

		return $this->m_objUnitSpace;
	}

	public function fetchPropertyBuilding( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

		$this->m_objPropertyBuilding = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingByCidByPropertyIdByUnitSpaceId( $this->getCid(), $this->getPropertyId(), $this->getUnitSpaceId(), $objDatabase );

		return $this->m_objPropertyBuilding;
	}

	public function fetchApplication( $objDatabase ) {

		if( true == is_null( $this->getApplicationId() ) || false == is_numeric( $this->getApplicationId() ) ) return NULL;

		$this->m_objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );

		return $this->m_objApplication;
	}

	public function getOrFetchApplication( $objDatabase = NULL ) {

		$objDatabase = ( false == valObj( $objDatabase, 'CDatabase' ) ) ? $this->getDatabase() : $objDatabase;

		if( false == valObj( $this->m_objApplication, 'CApplication' ) ) {
			return $this->fetchApplication( $objDatabase );
		}

		return $this->m_objApplication;
	}

	public function fetchAddOn( $objDatabase ) {
		if( true == is_null( $this->getAddOnId() ) || false == is_numeric( $this->getAddOnId() ) ) return NULL;

		$this->m_objAddOn = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnByIdByCid( $this->getAddOnId(), $this->getCid(), $objDatabase );

		return $this->m_objAddOn;
	}

	public function exportAcceptOffer( $intCompanyUserId, $objProperty, $objDatabase ) {
		$boolIsValid                 = true;

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_OFFER_ACCEPTED, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplicationInterest( $this );
		$objGenericWorker->setApplication( $this->getOrFetchApplication() );
		$objGenericWorker->setProperty( $objProperty );

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportOfferCancelAcceptance( $intCompanyUserId, $objProperty, $objDatabase ) {
		$boolIsValid                 = true;

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$arrstrDetailsForOffer                = ( array ) $this->getDetails();

		if( false == valStr( $arrstrDetailsForOffer['emh_offer_accepted_on'] ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_OFFER_CANCELLED, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplicationInterest( $this );
		$objGenericWorker->setApplication( $this->getOrFetchApplication() );
		$objGenericWorker->setProperty( $objProperty );

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function export( $intCompanyUserId, $objProperty, $objDatabase ) {
		$boolIsValid 								= true;
		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) return true;

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

		// Don't export the application if assigned unit does not belongs to property associated.

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && false == is_null( $objUnitSpace->getPropertyId() ) && false == is_null( $this->getPropertyId() ) && ( $objUnitSpace->getPropertyId() != $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Assigned unit does not belongs to property associated. Remove and re-asscociate unit to clone application.' ), NULL ) );
			return false;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_OFFER, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplicationInterest( $this );
		$objGenericWorker->setApplication( $this->getApplication() );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setUnitSpace( $objUnitSpace );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportUpdate( $intCompanyUserId, $objProperty, $strReason, $objDatabase, $boolIsOfferExpired = false ) {

		$boolIsValid = true;

		if( true == valStr( $this->getEmhOfferExportedOn() ) ) return $boolIsValid;
		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return $boolIsValid;

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return $boolIsValid;

		if( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) return $boolIsValid;

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );
		// Don't export the application if assigned unit does not belongs to property associated.

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && false == is_null( $objUnitSpace->getPropertyId() ) && false == is_null( $this->getPropertyId() ) && ( $objUnitSpace->getPropertyId() != $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Assigned unit does not belongs to property associated. Remove and re-asscociate unit to clone application.' ), NULL ) );
			return false;
		}
		if( true == $boolIsOfferExpired ) {
			$intIntegrationServiceId = CIntegrationService::UPDATE_OFFER_EXPIRED;
		} else {
			$intIntegrationServiceId = CIntegrationService::UPDATE_OFFER_DECLINED;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), $intIntegrationServiceId, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplication( $this->getApplication() );
		$objGenericWorker->setApplicationInterest( $this );
		$objGenericWorker->setProperty( $objProperty );
		if( true == $boolIsOfferExpired ) {
			$objGenericWorker->setExpiredDate( date( 'm/d/Y' ) );
		} else {
			$objGenericWorker->setRejectionReason( $strReason );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

}
?>