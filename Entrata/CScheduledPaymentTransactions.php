<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTransactions
 * Do not add any new functions to this class.
 */

class CScheduledPaymentTransactions extends CBaseScheduledPaymentTransactions {

	public static function fetchCustomScheduledPaymentTransactionsByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransactions( sprintf( 'SELECT * FROM %s WHERE scheduled_payment_id = %d AND cid = %d ORDER BY created_on DESC', 'scheduled_payment_transactions', ( int ) $intScheduledPaymentId, ( int ) $intCid ), $objDatabase );
    }

	public static function fetchScheduledPaymentTransactionsDateByScheduledPaymentIdByCid( $arrintScheduledPaymentIds, $intCid, $objDatabase ) {
		$strSql	= 'SELECT 
						cid,
						scheduled_payment_id,
						created_on 
					FROM 
						scheduled_payment_transactions 
					WHERE 
						scheduled_payment_id IN( ' . implode( ',', $arrintScheduledPaymentIds ) . ' )
						AND cid = ' . ( int ) $intCid . ' 
						and created_on > ( NOW() - interval \'6 days\' ) 
					ORDER BY 
						created_on DESC';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponse ) ) {
			return $arrmixResponse;
		} else {
			return false;
		}
	}

}
?>