<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerContacts
 * Do not add any new functions to this class.
 */

class CCustomerContacts extends CBaseCustomerContacts {

	public static function fetchCustomerContactByCustomerContactTypeIdByCustomerIdByCid( $intCustomerContactTypeId, $intCustomerId, $intCid, $objDatabase, $strSortOrder = NULL ) {
		return self::fetchCustomerContact( sprintf( 'SELECT * FROM %s WHERE customer_id = %d AND customer_contact_type_id = %d AND cid = %d ' . ( ( true == valStr( $strSortOrder ) ) ? 'ORDER BY id ' . $strSortOrder : '' ) . ' LIMIT 1', 'customer_contacts', ( int ) $intCustomerId, ( int ) $intCustomerContactTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;
		$strSql = 'SELECT * FROM customer_contacts WHERE customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function deleteCustomerContactsByCustomerIdByCustomerContactTypeIdsByCid( $intCustomerId, $arrintCustomerContactTypeIds, $intCid, $objDatabase ) {
		if( true == is_null( $intCustomerId ) || false == valArr( $arrintCustomerContactTypeIds ) ) return NULL;
		$strSql = 'DELETE 
					FROM 
						customer_contacts 
					WHERE 
						customer_id = ' . ( int ) $intCustomerId . ' 
						AND customer_contact_type_id IN ( ' . implode( ',', $arrintCustomerContactTypeIds ) . ' )
						AND cid = ' . ( int ) $intCid;
		return false == is_null( $objDatabase->execute( $strSql ) );
	}

	public static function fetchCustomerContactsByCustomerContactTypeIdByPropertyIdByCid( $intCustomerContactTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '
				    SELECT
							DISTINCT(cc.id),
	                       	c.id as customer_id,
							cc.phone_number,
							cc.mobile_number,
							( c.name_first || \' \' || c.name_last ) as customer_name,
							( cc.name_first || \' \' || cc.name_last ) as contact_name,
							cc.email_address
                      FROM
							customer_contacts cc
	                        JOIN customers c ON ( c.id = cc.customer_id AND c.cid = cc.cid )
	                        JOIN lease_customers lc ON ( lc.customer_id = cc.customer_id AND lc.cid = cc.cid )
	                        JOIN leases l ON  ( l.id = lc.lease_id AND l.cid = lc.cid )
	                        JOIN lease_intervals li ON ( li.lease_id = l.id AND li .lease_status_type_id <> 2 AND li.cid = l.cid )
                      WHERE cc.cid = ' . ( int ) $intCid . '
							AND l.property_id = ' . ( int ) $intPropertyId . '
							AND cc.customer_contact_type_id = ' . ( int ) $intCustomerContactTypeId . '
							AND ( cc.phone_number IS NOT NULL OR cc.mobile_number IS NOT NULL)
							ORDER BY customer_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerContactTypeIdsByCustomerIdByCid( $arrintCustomerContactTypeIds, $intCustomerId, $intCid, $objDatabase, $intLimit = NULL ) {
		if( false == valArr( $arrintCustomerContactTypeIds ) ) return NULL;
		$intLimit = ( true == valId( $intLimit ) ) ? ' LIMIT ' . $intLimit : '';
		$strSql = 'SELECT * FROM customer_contacts WHERE customer_id = ' . ( int ) $intCustomerId . ' AND customer_contact_type_id IN ( ' . implode( ',', $arrintCustomerContactTypeIds ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY CASE customer_contact_type_id WHEN 13 THEN 1 WHEN 11 THEN 2 WHEN 12 THEN 3 ELSE 4 END ' . $intLimit;
		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactByEmailAddressByCustomerContactTypeIdByCid( $strEmailAddress, $intCustomerContactTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM customer_contacts WHERE email_address LIKE \'' . $strEmailAddress . '\' AND customer_contact_type_id = ' . ( int ) $intCustomerContactTypeId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchCustomerContact( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsWithCommercialClausesByCustomerIdsByLeaseIdByCid( $arrintCustomerIds, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT 
						cc.*,
						ccl.reference
					FROM 
						customer_contacts cc 
						LEFT JOIN commercial_clauses ccl ON( cc.cid = ccl.cid  AND ccl.lease_id = ' . ( int ) $intLeaseId . ' AND ccl.key=\'CONTACT_REFERENCE\' AND ccl.clause = cc.id::VARCHAR )
					WHERE 
						cc.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) 
						AND cc.cid = ' . ( int ) $intCid;

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactWithCommercialClausesByIdByLeaseIdByCid( $intContactId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intContactId ) ) return NULL;

		$strSql = 'SELECT 
						cc.*,
						ccl.id AS commercial_clause_id,
						ccl.reference
					FROM 
						customer_contacts cc 
						LEFT JOIN commercial_clauses ccl ON( cc.cid = ccl.cid  AND ccl.lease_id = ' . ( int ) $intLeaseId . ' AND ccl.key=\'CONTACT_REFERENCE\' AND ccl.clause = cc.id::VARCHAR )
					WHERE 
						cc.id = ' . $intContactId . ' 
						AND cc.cid = ' . ( int ) $intCid . '
						LIMIT 1';

		return self::fetchCustomerContact( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerContactTypeIdByEmergencyContactNamesByPropertyIdByCid( $intCustomerContactTypeId, $arrstrEmergencyContactNames, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cc.*
                    FROM
						customer_contacts cc
                        JOIN customers c ON ( c.id = cc.customer_id AND c.cid = cc.cid )
                        JOIN lease_customers lc ON ( lc.customer_id = cc.customer_id AND lc.cid = cc.cid )
                        JOIN leases l ON  ( l.id = lc.lease_id AND l.cid = lc.cid )
                    WHERE 
                        cc.cid = ' . ( int ) $intCid . '
						AND l.property_id = ' . ( int ) $intPropertyId . '
						AND cc.customer_contact_type_id = ' . ( int ) $intCustomerContactTypeId . '
						AND cc.name_first IN ( \'' . implode( '\',\'', $arrstrEmergencyContactNames ) . '\' )
						ORDER BY cc.name_first';

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerContactTypeIdByCustomerIdByNameByCid( $intCustomerContactTypeId, $intCustomerId, $strNameFirst, $strNameLast, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
							* 
					FROM 
						customer_contacts 
					WHERE 
						customer_id = ' . ( int ) $intCustomerId . ' 
						AND customer_contact_type_id = ' . ( int ) $intCustomerContactTypeId . ' 
						AND name_first = \'' . $strNameFirst . '\'
						AND name_last = \'' . $strNameLast . '\'
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerContact( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsByContactTypeIdsByCustomerIdsByCid( $arrintCustomerContactTypeIds, $arrintCustomerIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						* 
					FROM 
						customer_contacts 
					WHERE 
						customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )  
						AND customer_contact_type_id IN ( ' . implode( ',', $arrintCustomerContactTypeIds ) . ' ) 
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerContacts( $strSql, $objDatabase );

	}

	public static function fetchCustomerContactsByCustomerContactTypeIdsByCustomerIdByLeaseidByCid( $arrintCustomerContactTypeIds, $intCustomerId, $intLeaseId, $intCid, $objDatabase, $intLimit = NULL ) {
		if( false == valArr( $arrintCustomerContactTypeIds ) ) return NULL;
		$intLimit = ( true == valId( $intLimit ) ) ? ' LIMIT ' . $intLimit : '';
		$strSql = 'SELECT 
						cc.*,
						ccl.id AS commercial_clause_id,
						ccl.reference
					FROM 
						customer_contacts cc 
						LEFT JOIN commercial_clauses ccl ON( cc.cid = ccl.cid  AND ccl.lease_id = ' . ( int ) $intLeaseId . ' AND ccl.key=\'CONTACT_REFERENCE\' AND ccl.clause = cc.id::VARCHAR )
					WHERE 
						customer_id = ' . ( int ) $intCustomerId . ' 
						AND customer_contact_type_id IN ( ' . sqlIntImplode( $arrintCustomerContactTypeIds ) . ' ) 
						AND cc.cid = ' . ( int ) $intCid . ' 
					ORDER BY CASE customer_contact_type_id WHEN 13 THEN 1 WHEN 11 THEN 2 WHEN 12 THEN 3 ELSE 4 END ' . $intLimit;
		return self::fetchCustomerContacts( $strSql, $objDatabase );

	}

	public static function fetchCustomerContactsWithCustomerContactTypeByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		if( false === valId( $intCustomerId ) || false === valId( $intLeaseId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = 'SELECT 
						CASE WHEN cc.company_name IS NULL THEN \'Personal\' ELSE \'Company\' END AS contact_group,
						util_get_translated( \'name\', cct.name, cct.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS customer_contact_type,
						cc.*
					FROM 
						customer_contacts cc 
						JOIN customer_contact_types cct ON( cc.customer_contact_type_id = cct.id AND cct.is_published = 1 )
					WHERE 
						cc.customer_id = ' . ( int ) $intCustomerId . '
						AND cc.customer_contact_type_id IN ( ' . sqlIntImplode( CCustomerContactType::$c_arrintTenantCustomerContactTypes ) . ' ) 
						AND cc.email_address IS NOT NULL
						AND cc.cid = ' . ( int ) $intCid . '
					ORDER BY
						contact_group DESC,
						cc.name_first';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactByIdByCustomerIdByCid( $intId, $intCustomerId, $intCid, $objDatabase ) {

		if( false === valId( $intId ) || false === valId( $intCustomerId ) || false === valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
					* 
					FROM 
						customer_contacts 
					WHERE 
						id = ' . ( int ) $intId . ' 
						AND customer_id = ' . ( int ) $intCustomerId . ' 
						AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerContact( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactFullNameByIdByCid( $intId, $intCid, $objDatabase ) {

		if( false === valId( $intId ) || false === valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						func_format_customer_name( name_first, name_last ) AS name_first
					FROM 
						customer_contacts
					WHERE 
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) || false == valId( $intCid ) ) {
			return [];
		}

		$strSql = ' SELECT
                      *
                    FROM
                        customer_contacts
                    WHERE 
                        id IN ( ' . implode( ',', $arrintIds ) . ' ) 
                        AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerContactsByCustomerContactTypeIdsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $arrintCustomerContactTypeIds ) {
		if( false == valArr( $arrintCustomerContactTypeIds ) || false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						id,
						name_first,
						name_last,
						phone_number,
						email_address,
						customer_contact_type_id,
						customer_id
					FROM
						customer_contacts
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND customer_contact_type_id IN ( ' . implode( ',', $arrintCustomerContactTypeIds ) . ' ) 
					ORDER BY 
						CASE customer_contact_type_id 
							WHEN ' . CCustomerContactType::PRIMARY . ' THEN 1
							WHEN ' . CCustomerContactType::BILLING . ' THEN 2
							WHEN ' . CCustomerContactType::LEGAL . ' THEN 3
							ELSE 4
						END,
						name_first,
						name_last';

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchPrimaryCustomerContactsWithCommercialClausesByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		if( false === valId( $intCustomerId ) || false === valId( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						cc.*,
						ccl.reference
					FROM 
						customer_contacts cc 
						LEFT JOIN commercial_clauses ccl ON( cc.cid = ccl.cid  AND ccl.lease_id = ' . ( int ) $intLeaseId . ' AND ccl.key=\'CONTACT_REFERENCE\' AND ccl.clause = cc.id::VARCHAR )
					WHERE 
						cc.customer_id = ' . ( int ) $intCustomerId . '
						AND cc.customer_contact_type_id = ' . CCustomerContactType::PRIMARY . '
						AND cc.cid = ' . ( int ) $intCid;

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

	public static function fetchCommercialCustomerContactByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		if( false === valId( $intCustomerId ) || false === valId( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cc.*,
						CASE
							WHEN ( util.util_to_bool ( cc.details ->> \'is_default\' ) IS TRUE AND cc.customer_contact_type_id = ' . CCustomerContactType::PRIMARY . ' ) THEN 1
							WHEN ( cc.customer_contact_type_id = ' . CCustomerContactType::PRIMARY . ' ) THEN 2
							WHEN ( cc.customer_contact_type_id = ' . CCustomerContactType::BILLING . ' ) THEN 3
							WHEN ( cc.customer_contact_type_id = ' . CCustomerContactType::LEGAL . ' ) THEN 4
						END AS email_priority
					FROM
						customer_contacts cc
						LEFT JOIN commercial_clauses ccl ON( cc.cid = ccl.cid  AND ccl.lease_id = ' . ( int ) $intLeaseId . ' AND ccl.key=\'CONTACT_REFERENCE\' AND ccl.clause = cc.id::VARCHAR )
					WHERE
						cc.customer_id = ' . ( int ) $intCustomerId . '
						AND cc.cid = ' . ( int ) $intCid . '
						AND cc.email_address IS NOT NULL
					ORDER BY email_priority LIMIT 1';

		return self::fetchCustomerContact( $strSql, $objDatabase );
	}

	public static function fetchpublishedCustomerContactsWithCustomerContactTypeByCustomerIdByLeaseIdByCid( $arrintCustomerIds, $arrstrCompanyTypes, $arrstrCustomerTypes, $intCid, $objDatabase ) {
		if( false === valArr( $arrintCustomerIds ) || false === valId( $intCid ) || false === valArr( $arrstrCompanyTypes ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strWhereClause = '';
		if( true === in_array( 'Personal', $arrstrCompanyTypes ) ) {
			$strWhereClause .= ' AND cc.company_name IS NULL';
		}

		if( true === in_array( 'Company', $arrstrCompanyTypes ) ) {
			$strWhereClause .= ' AND cc.company_name IS NOT NULL';
		}

		if( true === in_array( 'Personal', $arrstrCompanyTypes ) && true === in_array( 'Company', $arrstrCompanyTypes ) ) {
			$strWhereClause = '';
		}
		$strSql = 'SELECT 
						CASE WHEN cc.company_name IS NULL THEN \'Personal\' ELSE \'Company\' END AS contact_group,
						cct.name AS customer_contact_type,
						cc.email_address AS username,
						cc.*
					FROM 
						customer_contacts cc 
						JOIN customer_contact_types cct ON( cc.customer_contact_type_id = cct.id AND cct.is_published = 1 )
						JOIN customers c ON (cc.cid = c.cid AND cc.customer_id = c.id)
					WHERE 
						cc.customer_id IN (' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND cct.name IN ( ' . sqlStrImplode( $arrstrCustomerTypes ) . ' )
						AND cc.email_address IS NOT NULL
						AND cc.cid = ' . ( int ) $intCid . '
						' . $strWhereClause . '
					ORDER BY
						contact_group DESC,
						cc.name_first';

		return self::fetchCustomerContacts( $strSql, $objDatabase );
	}

}
?>