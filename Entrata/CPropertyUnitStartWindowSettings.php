<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUnitStartWindowSettings
 * Do not add any new functions to this class.
 */

class CPropertyUnitStartWindowSettings extends CBasePropertyUnitStartWindowSettings {

	public static function fetchPropertyUnitStartWindowSettingByCidByPropertyIdByPropertyUnitIdByLeaseTermIdByLeaseStartWindowId( $intCid, $intPropertyId, $intPropertyUnitId, $intLeaseTermId, $intLeaseStartWindowId, $objDatabase ) {

		$strSql = 'SELECT
						pusws.*
					FROM
						property_unit_start_window_settings pusws
					WHERE
						pusws.cid = ' . ( int ) $intCid . '
						AND pusws.property_id = ' . ( int ) $intPropertyId . '
						AND pusws.property_unit_id = ' . ( int ) $intPropertyUnitId . '
						AND pusws.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND pusws.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;

		return self::fetchPropertyUnitStartWindowSetting( $strSql, $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByCidByPropertyIdByPropertyUnitIdsByLeaseStartWindowIds( $intCid, $intPropertyId, $arrintPropertyUnitIds, $arrintLeaseStartWindowIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyUnitIds ) || false == valArr( $arrintLeaseStartWindowIds ) ) return false;

		$strSql = 'SELECT
						pusws.*
					FROM
						property_unit_start_window_settings pusws
					WHERE
						pusws.cid = ' . ( int ) $intCid . '
						AND pusws.property_id = ' . ( int ) $intPropertyId . '
						AND pusws.property_unit_id IN ( ' . implode( ', ', $arrintPropertyUnitIds ) . ' )
						AND pusws.lease_start_window_id IN ( ' . implode( ', ', $arrintLeaseStartWindowIds ) . ' ) ';

		return self::fetchPropertyUnitStartWindowSettings( $strSql, $objDatabase );
	}

}
?>