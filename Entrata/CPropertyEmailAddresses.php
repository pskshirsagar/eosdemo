<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEmailAddresses
 * Do not add any new functions to this class.
 */

class CPropertyEmailAddresses extends CBasePropertyEmailAddresses {

	const EMAIL_ADDRESS_TYPE_ID = 1;

	public static function fetchPublishedPropertyEmailAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddresses( sprintf( 'SELECT * FROM %s WHERE property_id = %d AND cid = %d AND is_published = 1 ORDER BY order_num', 'property_email_addresses', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressByPropertyIdEmailAddressTypeIdByCid( $intPropertyId, $intEmailAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddress( sprintf( 'SELECT * FROM %s WHERE property_id::int = %d::int AND email_address_type_id::int = %d::int AND cid::int = %d::int LIMIT 1', 'property_email_addresses', ( int ) $intPropertyId, ( int ) $intEmailAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressByPropertyIdEmailAddressTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intPropertyId, $intEmailAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddress( sprintf( 'SELECT * FROM %s WHERE property_id::int = %d::int AND email_address_type_id::int = %d::int AND cid::int = %d AND lower(remote_primary_key) = \'%s\' LIMIT 1', 'property_email_addresses', ( int ) $intPropertyId, ( int ) $intEmailAddressTypeId, ( int ) $intCid, \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strRemotePrimaryKey ) ) ) ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdByCid( $arrintPropertyIds, $intEmailAddressTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		 $strSql = 'SELECT * FROM property_email_addresses WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND email_address_type_id = ' . ( int ) $intEmailAddressTypeId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase );
	}

	public static function fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdByCids( $arrintPropertyIds, $intEmailAddressTypeId, $arrintCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCid ) ) return NULL;
		$strSql = 'SELECT * FROM property_email_addresses WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND email_address_type_id = ' . ( int ) $intEmailAddressTypeId . ' AND cid IN ( ' . implode( ',', $arrintCid ) . ' ) ';
		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchPropertyEmailAddressesByPropertyIdExcludeEmailAddressTypeIdsByCid( $intPropertyId, $arrintExcludeEmailAddressTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintExcludeEmailAddressTypeIds ) ) return NULL;
		 $strSql = 'SELECT * FROM property_email_addresses WHERE property_id = ' . ( int ) $intPropertyId . ' AND email_address_type_id NOT IN ( ' . implode( ',', $arrintExcludeEmailAddressTypeIds ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY order_num';
		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase );
	}

	public static function fetchPropertyEmailAddressByIdByPropertyIdByCid( $intPropertyEmailAddressId, $intPropertyId, $intCid, $objDatabase ) {
		if ( false == isset ( $intPropertyId ) ) return NULL;
		$strSql = 'SELECT * FROM property_email_addresses WHERE property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intPropertyEmailAddressId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyEmailAddress( $strSql, $objDatabase );
	}

    public static function fetchPrimaryPropertyEmailAddressesByCid( $intCid, $objDatabase ) {
   		$strSql = 'SELECT distinct(email_address) FROM property_email_addresses WHERE cid = ' . ( int ) $intCid . ' AND email_address_type_id = ' . CEmailAddressType::PRIMARY;
		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase );
    }

    public static function fetchSimplePrimaryPropertyEmailAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT email_address FROM property_email_addresses WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND email_address_type_id = ' . CEmailAddressType::PRIMARY;
		return parent::fetchColumn( $strSql, 'email_address', $objDatabase );
    }

   	public static function fetchPropertyEmailAddressByPropertyIdByEmailAddresssTypeIdByCid( $intPropertyId, $intEmailAddressTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_email_addresses WHERE property_id = ' . ( int ) $intPropertyId . ' AND email_address_type_id = ' . ( int ) $intEmailAddressTypeId . ' AND is_published = 1 AND cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchPropertyEmailAddress( $strSql, $objDatabase );
	}

	public static function fetchPropertyEmailAddressByPropertyIdsByCidByEmailAddressTypeId( $arrintPropertyIds, $intCid, $intEmailAddressTypeId, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pea.*
					FROM
						property_email_addresses pea
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND email_address_type_id = ' . ( int ) $intEmailAddressTypeId;

		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase );
	}

	public static function fetchCustomBlockedPropertyEmailAddresses( $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ( lower ( pea.email_address ) ) AS email_address
					FROM
						property_email_addresses pea
						JOIN property_preferences pp ON ( pea.cid = pp.cid AND pea.property_id = pp.property_id )
						JOIN properties p ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						pp.key = \'BLOCK_NEWSLETTER_EMAILS\'
						AND p.is_disabled = 0
						AND pea.email_address IS NOT NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomBlockedPropertyEmailAddressesByCompanyPreferences( $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT(lower(email_address)) AS email_address
					FROM property_email_addresses pea
						 JOIN company_preferences cp ON ( pea.cid = cp.cid )
					WHERE
						cp.key = \'BLOCK_NEWSLETTERS\'
						AND pea.email_address IS NOT NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdsByCid( $arrintPropertyIds, $arrintEmailAddressTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintEmailAddressTypeIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						property_email_addresses 
					WHERE
						email_address IS NOT NULL
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND email_address_type_id IN ( ' . implode( ',', $arrintEmailAddressTypeIds ) . ' ) 
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyEmailAddresses( $strSql, $objDatabase );
	}

}
?>