<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyCharities
 * Do not add any new functions to this class.
 */

class CCompanyCharities extends CBaseCompanyCharities {

	public static function fetchSimpleCompanyCharitiesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCharities( sprintf( 'SELECT * FROM company_charities WHERE cid = %d AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY is_published DESC, order_num ASC, name ASC', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPublishedCompanyCharitiesByCid( $intCid, $objDatabase, $boolIsGlobal = false ) {
		$strSql = 'SELECT * FROM
						company_charities
					WHERE
						 cid = ' . ( int ) $intCid . '
						 AND is_published = 1
						 AND deleted_by IS NULL AND deleted_on IS NULL';

		$strSql .= ( true == $boolIsGlobal ) ? ' AND is_global = 1' : '';
		$strSql .= ' ORDER BY
						name ASC';

		return self::fetchCompanyCharities( $strSql, $objDatabase );
	}

    public static function fetchPublishedCompanyCharitiesByPropertyIdCid( $intPropertyId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT
						cc.*
					FROM
						company_charities cc
						JOIN property_charities pc ON ( cc.cid = pc.cid AND cc.id = pc.company_charity_id )
					WHERE
				 		cc.cid = ' . ( int ) $intCid . '
				 		AND cc.deleted_by IS NULL AND cc.deleted_on IS NULL
						AND pc.disabled_by IS NULL AND pc.disabled_on IS NULL
				 		AND pc.property_id = ' . ( int ) $intPropertyId . '
				 		AND cc.is_published = 1
    				ORDER BY
				 		cc.name ASC';

    	return self::fetchCompanyCharities( $strSql, $objDatabase );
    }

    public static function fetchCompanyCharitiesDataByCid( $intCid, $objDatabase ) {
    	if( true == is_null( $intCid ) ) return NULL;

    	$strSql = 'SELECT
						cc.id,
   						cc.name
					FROM
						company_charities cc
					WHERE
			 			cc.cid = ' . ( int ) $intCid . '
			 			AND cc.deleted_by IS NULL AND cc.deleted_on IS NULL
    				ORDER BY
			 			cc.name ASC';

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchCustomCompanyCharityByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT  company_media_file_id, name, url, description FROM company_charities WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid;

		$arrstrResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrResponse[0] ) ) return $arrstrResponse[0];

		return NULL;

	}

}
?>