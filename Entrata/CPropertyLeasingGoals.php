<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLeasingGoals
 * Do not add any new functions to this class.
 */

class CPropertyLeasingGoals extends CBasePropertyLeasingGoals {

	public static function fetchPorpertyLeasingGoalsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		 $strSql = 'SELECT
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ) AS term_month,
					    plg.lease_term_id,
					    sum ( plg.goal_count ) AS goal_count,
					    plg.is_published,
					    plg.is_renewal,
		 				plg.lease_start_window_id,
		 				lsw.start_date,
		 				lsw.end_date,
					    MAX ( lt.name ) AS lease_term_name
					FROM
					    property_leasing_goals plg
					    JOIN lease_terms lt ON ( lt.cid = plg.cid AND lt.id = plg.lease_term_id )
					    JOIN property_charge_settings pcs ON ( pcs.cid = plg.cid AND pcs.property_id = plg.property_id )
					    JOIN lease_start_windows lsw ON ( lsw.cid = plg.cid AND lsw.lease_term_id = plg.lease_term_id AND lsw.property_id = plg.property_id )
					WHERE
					    plg.cid = ' . ( int ) $intCid . '
					    AND plg.property_id = ' . ( int ) $intPropertyId . '
					    AND plg.property_floorplan_id IS NULL
					    AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					    AND lt.is_disabled = FALSE
					    AND lsw.end_date > CURRENT_DATE
					    AND lsw.deleted_on IS NULL
					    AND lt.deleted_on IS NULL
					GROUP BY
					    term_month,
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ),
					    plg.lease_term_id,
					    plg.is_published,
					    plg.is_renewal,
					    plg.lease_start_window_id,
					    lsw.start_date,
		 				lsw.end_date,
					    to_timestamp( to_char( DATE( plg.goal_date ),\'1 Mon YYYY\'), \'1 Mon YYYY\' )
					ORDER BY
					    to_timestamp( to_char( DATE( plg.goal_date ),\'1 Mon YYYY\'), \'1 Mon YYYY\' ),
					    plg.lease_term_id,
					    plg.is_renewal';

		$arrmixTempPropertyLeasingGoals = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixTempPropertyLeasingGoals ) ) return [];

		$arrmixFinalPropertyLeasingGoals = [];

		foreach( $arrmixTempPropertyLeasingGoals as $arrmixTempPropertyLeasingGoal ) {
			$arrmixFinalPropertyLeasingGoals[$arrmixTempPropertyLeasingGoal['lease_term_id']][$arrmixTempPropertyLeasingGoal['term_month']][$arrmixTempPropertyLeasingGoal['is_renewal']] = $arrmixTempPropertyLeasingGoal;
		}

		return $arrmixFinalPropertyLeasingGoals;
	}

	public static function fetchPropertyLeasingGoalMonthsByPropertyIdByLeaseTermIdByLeaseStartWindowIdByCid( $intPropertyId, $intLeaseTermId, $intLeaseStartWindowId, $intCid, $objDatabase, $boolIsFromPropertyFloorplanLeasingGoals = false ) {

		$strWhere = ' AND property_floorplan_id IS NULL';
		if( true == $boolIsFromPropertyFloorplanLeasingGoals ) {
			$strWhere = ' AND property_floorplan_id IS NOT NULL';
		}

		$strSql = 'SELECT
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ) AS term_month,
					    max ( is_published ) as is_published
					FROM
					    property_leasing_goals plg
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
						. $strWhere . '
					GROUP BY
					    term_month,
					    to_timestamp ( to_char ( DATE ( plg.goal_date ), \'1 Mon YYYY\' ), \'1 Mon YYYY\' )
					ORDER BY
					    to_timestamp ( to_char ( DATE ( plg.goal_date ), \'1 Mon YYYY\' ), \'1 Mon YYYY\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsForFloorplanByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ) AS term_month,
					    SUM ( plg.goal_count ) AS goal_count,
					    plg.is_published,
					    plg.is_renewal,
					    plg.lease_term_id,
					    plg.lease_start_window_id,
						lsw.start_date,
						lsw.end_date,
					    MAX ( lt.name ) AS lease_term_name,
					    MAX ( pf.floorplan_name ) AS floor_plan_name,
					    MAX ( plg.property_floorplan_id ) AS floor_plan_id
					FROM
					    property_leasing_goals plg
					    JOIN lease_terms lt ON ( lt.cid = plg.cid AND lt.id = plg.lease_term_id )
					    JOIN property_charge_settings pcs ON ( pcs.cid = plg.cid AND pcs.property_id = plg.property_id )
					    JOIN lease_start_windows lsw ON ( lsw.cid = plg.cid AND lsw.lease_term_id = plg.lease_term_id AND lsw.property_id = plg.property_id )
					    JOIN property_floorplans pf ON ( pf.cid = plg.cid AND pf.property_id = plg.property_id AND pf.id = plg.property_floorplan_id AND pf.deleted_on IS NULL )
					WHERE
					    plg.cid = ' . ( int ) $intCid . '
					    AND plg.property_id = ' . ( int ) $intPropertyId . '
					    AND plg.property_floorplan_id IS NOT NULL
					    AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					    AND lt.is_disabled = FALSE
					    AND lsw.end_date > CURRENT_DATE
					    AND lsw.deleted_on IS NULL
					GROUP BY
					    term_month,
					    plg.property_floorplan_id,
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ),
					    plg.is_renewal,
					    plg.is_published,
					    plg.lease_term_id,
					    plg.lease_start_window_id,
					    lsw.start_date,
						lsw.end_date,
					    to_timestamp( to_char( DATE( plg.goal_date ),\'1 Mon YYYY\'), \'1 Mon YYYY\' )
					ORDER BY
					    to_timestamp( to_char( DATE( plg.goal_date ),\'1 Mon YYYY\'), \'1 Mon YYYY\' ),
					    plg.property_floorplan_id';

		$arrmixTempPropertyFloorplanLeasingGoals = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixTempPropertyFloorplanLeasingGoals ) ) return [];
		$arrmixFinalPropertyFloorplanLeasingGoals = [];

		foreach( $arrmixTempPropertyFloorplanLeasingGoals as $arrmixTempPropertyFloorplanLeasingGoal ) {
			$arrmixFinalPropertyFloorplanLeasingGoals[$arrmixTempPropertyFloorplanLeasingGoal['lease_term_id']][$arrmixTempPropertyFloorplanLeasingGoal['term_month']][$arrmixTempPropertyFloorplanLeasingGoal['floor_plan_id']][$arrmixTempPropertyFloorplanLeasingGoal['is_renewal']] = $arrmixTempPropertyFloorplanLeasingGoal;
		}

		return $arrmixFinalPropertyFloorplanLeasingGoals;
	}

	public static function fetchPorpertyLeasingGoalMonthsByLeaseTermIdByLeaseStartWindowIdByByPropertyIdCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase, $boolIsFromPropertyFloorplanLeasingGoals = false ) {

		$strWhere = ' AND property_floorplan_id IS NULL';
		if( true == $boolIsFromPropertyFloorplanLeasingGoals ) {
			$strWhere = ' AND property_floorplan_id IS NOT NULL';
		}

		$strSql = 'SELECT
					    to_char ( DATE ( plg.goal_date ), \'Mon YYYY\' ) AS term_month,
					    is_published
					FROM
					    property_leasing_goals plg
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
						    . $strWhere . '
					GROUP BY
					    term_month,
					    is_published,
					    to_timestamp( to_char( DATE( plg.goal_date ), \'1 Mon YYYY\'), \'1 Mon YYYY\' )
					ORDER BY
					    to_timestamp( to_char( DATE( plg.goal_date ), \'1 Mon YYYY\'), \'1 Mon YYYY\' ),
					    is_published';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGoalCountByLeaseTermIdByLeaseStartWindowIdByIsRenewalByStartDateByEndDateByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intIsRenewal, $strStartDate, $strEndDate, $intPropertyId, $intCid, $objDatabase, $intPropertyFloorPlanId = NULL, $boolIsFromDailyGoals = false ) {

		if( false == is_null( $intPropertyFloorPlanId ) ) {
			$strAndCondition = ' AND property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId;
		} elseif( true == $boolIsFromDailyGoals ) {
			$strAndCondition = ' AND property_floorplan_id IS NULL';
		} else {
			$strAndCondition = ' AND property_floorplan_id IS NOT NULL';
		}

		$strSql = 'SELECT
					    sum(goal_count) as count
					FROM
					    property_leasing_goals
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
					    . $strAndCondition . '
					    AND is_renewal = ' . ( int ) $intIsRenewal . '
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )';

		$arrintCount = fetchData( $strSql, $objDatabase );

		return $arrintCount[0]['count'];
	}

	public static function fetchPropertyLeasingGoalsByStartDateByEndDateByLeaseTermIdByLeaseStartWindowIdByTypeByPropertyIdByCid( $strStartDate, $strEndDate, $intLeaseTermId, $intLeaseStartWindowId,  $intPropertyFloorPlanId, $boolIsRenewal, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = '';
		if( false == is_null( $intPropertyFloorPlanId ) ) {
			$strCondition = ' AND property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId;
		}

		$strSql = 'SELECT
					    *
					FROM
					    property_leasing_goals
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
							. $strCondition . '
					    AND is_renewal = ' . ( int ) $boolIsRenewal . '
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )';

		return self::fetchPropertyLeasingGoals( $strSql, $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase, $boolIsIncludePropertyFloorplanId = true, $boolIsSelectFloorplanId = true ) {

		$strSelectSql = '';
		$strAndCondition = ' AND property_floorplan_id IS NULL';

		if( true == $boolIsIncludePropertyFloorplanId ) {
			$strSelectSql = 'property_floorplan_id,';
			$strAndCondition = ' AND property_floorplan_id IS NOT NULL';
		}

		if( false == $boolIsSelectFloorplanId ) {
			$strSelectSql = '';
		}

		$strSql = 'SELECT
					    to_char ( DATE ( goal_date ), \'mm/1/YYYY\' ) AS term_month,
					    is_renewal,
						' . $strSelectSql . '
					    sum ( goal_count ) AS total_count
					FROM
					    property_leasing_goals
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
					    	. $strAndCondition . '
					GROUP BY
					    term_month,'
							. $strSelectSql . '
					    is_renewal';

		return fetchData( $strSql, $objDatabase );
	}

	public static function updatePropertyLeasingGoalsByLeaseTermIdByLeaseStartWindowIdByStartDateByEndDateByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $strStartDate, $strEndDate, $boolIsPublished, $intPropertyId, $intCid, $objDatabase, $boolIsPropertyLeasingOnly = false ) {

		if( true == $boolIsPublished ) {
			$strUpdateQuery = ' is_published = 1';
		} else {
			$strUpdateQuery = ' is_published = 0';
		}

		if( true == $boolIsPropertyLeasingOnly ) {
			$strAndCondition = ' AND property_floorplan_id IS NULL';
		} else {
			$strAndCondition = ' AND property_floorplan_id IS NOT NULL';
		}

		$strSql = 'UPDATE
					    property_leasing_goals
					SET
					     ' . $strUpdateQuery . '
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' ) '
						    		. $strAndCondition;

		$strResultValue = fetchData( $strSql, $objDatabase );

		return ( false === $strResultValue ) ? false : true;

	}

	public static function deletePropertyLeasingGoalsByLeaseTermIdByLeaseStartWindowIdByStartDateByEndDateByIsRenewalByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $strStartDate, $strEndDate, $intType, $intPropertyId, $intCid, $objDatabase, $intPropertyFloorPlanId = NULL ) {

		$strAndCondition = '';
		if( false == is_null( $intPropertyFloorPlanId ) ) {
			$strAndCondition = ' AND property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId;
		} else {
			$strAndCondition = ' AND property_floorplan_id IS NULL';
		}

		$strSql = 'DELETE
					FROM
						property_leasing_goals
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND is_renewal = ' . ( int ) $intType . '
					    AND lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId
						    . $strAndCondition . '
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )';

		fetchData( $strSql, $objDatabase );
	}

	public static function deletePropertyLeasingGoalsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_null( $intLeaseTermId ) ) {
			$strAndCondition = ' AND property_floorplan_id IS NULL AND lease_term_id = ' . ( int ) $intLeaseTermId . ' AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;
		} else {
			$strAndCondition = ' AND property_floorplan_id IS NOT NULL';
		}

		$strSql = 'DELETE
					FROM
						property_leasing_goals
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . $strAndCondition;

		fetchData( $strSql, $objDatabase );
	}

}
?>