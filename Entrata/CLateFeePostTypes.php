<?php

class CLateFeePostTypes extends CBaseLateFeePostTypes {

	public static function fetchLateFeePostTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CLateFeePostType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchLateFeePostType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CLateFeePostType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>