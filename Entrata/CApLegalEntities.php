<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApLegalEntities
 * Do not add any new functions to this class.
 */

class CApLegalEntities extends CBaseApLegalEntities {

	public static function fetchApLegalEntitiesDetailByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						ale.id,
						ale.entity_name,
						ale.tax_number_encrypted,
						ale.receives_1099,
						ale.vendor_entity_id,
						ot.name AS owner_name,
						ft.name AS form_type_name,
						fbt.description AS form_box_type_name,
						fbt.name AS form_box_type_id
					FROM
						ap_legal_entities ale
						LEFT JOIN owner_types ot ON( ale.cid = ot.cid AND ale.owner_type_id = ot.id )
						LEFT JOIN form_1099_types ft ON( ale.form_1099_type_id = ft.id )
						LEFT JOIN form_1099_box_types fbt ON( ale.form_1099_box_type_id = fbt.id )
					WHERE
						ale.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ale.cid = ' . ( int ) $intCid . ' 
						AND ale.deleted_by IS NULL 
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApLegalEntityByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase, $boolIsSoftDelete = true ) {

		if( false == valId( $intId ) || false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;
		$strWhere = '';

		if( true == $boolIsSoftDelete ) {
			$strWhere .= ' AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';
		}

		$strSql = 'SELECT
						ale.*
					FROM
						ap_legal_entities ale
					WHERE
						ale.id = ' . ( int ) $intId . '
						AND ale.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND	ale.cid = ' . ( int ) $intCid . $strWhere;

		return self::fetchApLegalEntity( $strSql, $objClientDatabase );

	}

	public static function fetchApLegalEntityByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $boolIsVendorEntity = false ) {
		$strWhere = '';

		if( false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;

		if( true == $boolIsVendorEntity ) {
			$strWhere .= ' AND ale.vendor_entity_id IS NOT NULL ';
		}
		$strSql = 'SELECT
						ale.*
					FROM
						ap_legal_entities ale
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND	ale.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL ' .
						 $strWhere . '
						ORDER BY id ASC
						LIMIT 1';
		return self::fetchApLegalEntity( $strSql, $objClientDatabase );

	}

	public static function fetchApLegalEntitiesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolIsLegalEntity = false ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valId( $intCid ) ) return NULL;

		$strJoinCondition = '';
		$strJoin = 'LEFT JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND ale.id = apl.ap_legal_entity_id )';

		if( false == $boolIsLegalEntity ) {
			$strJoinCondition = ' AND apl.is_primary';
			$strJoin = 'JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND ale.id = apl.ap_legal_entity_id )';
		}

		$strSql = 'SELECT
						ale.*,
						ale.entity_name AS payee_name,
						ot.default_owner_type_id
					FROM
						ap_legal_entities ale
						' . $strJoin . '
						LEFT JOIN owner_types ot ON ( ot.cid = ale.cid AND ot.id = ale.owner_type_id AND ot.is_enabled IS TRUE ) 
					WHERE
						ale.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND	ale.cid = ' . ( int ) $intCid . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL' .
		                $strJoinCondition;

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );

	}

	public static function fetchAllApLegalEntitiesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolIsFromConsumer = false ) {

		if( false == valArr( $arrintApPayeeIds = array_filter( $arrintApPayeeIds ) ) ) {
			return NULL;
		}

		$strJoin = 'JOIN default_owner_types dot ON( dot.id = ot.default_owner_type_id ) ';
		if( true == $boolIsFromConsumer ) {
			$strJoin = 'LEFT JOIN default_owner_types dot ON( dot.id = ot.default_owner_type_id ) ';
		}

		$strSql = 'SELECT
						ale.*,  
						ot.default_owner_type_id
					FROM
						ap_legal_entities ale
						LEFT JOIN owner_types ot ON( ale.owner_type_id = ot.id AND ale.cid = ot.cid )
						' . $strJoin . '
					WHERE
						ale.ap_payee_id IN ( ' . sqlIntImplode( $arrintApPayeeIds ) . ' )
						AND	ale.cid = ' . ( int ) $intCid . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );
	}

	public static function fetchApLegalEntitiesDetailsByApPayeeIdByCidByIds( $intApPayeeId, $intCid, $arrintIds = [], $objDatabase ) {

		$strWhere = '';

		if( true == valArr( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) $strWhere .= ' AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

		$strSql = 'SELECT
						id,
						vendor_entity_id,
						entity_name,
						ap_payee_id
					FROM
						ap_legal_entities
					WHERE
						ap_payee_id  = ' . ( int ) $intApPayeeId . '
						AND	cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL ' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnsyncApLegalEntityByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						vendor_entity_id IS NULL
						AND id = ' . ( int ) $intId . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND	cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntity( $strSql, $objClientDatabase );
	}

	public static function fetchUnsyncApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						cid = ' . ( int ) $intCid . '
						AND vendor_entity_id IS NULL
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						cid = ' . ( int ) $intCid . '
						AND vendor_entity_id IS NOT NULL
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );
	}

	public static function fetchApLegalEntitiesCountByCid( $intCid, $objDatabase, $arrintApLegalEntityIds = NULL, $boolIsTaxIdRequired = true ) {

		$strWhere = ' AND ale.tax_number_encrypted IS NOT NULL';

		if( false == $boolIsTaxIdRequired ) {
			$strWhere = ' AND ale.tax_number_encrypted IS NULL';
		}

		if( true == ( $arrintApLegalEntityIds = getIntValuesFromArr( $arrintApLegalEntityIds ) ) ) {
			$strWhere .= ' AND ale.id NOT IN ( ' . implode( ',', $arrintApLegalEntityIds ) . ' )';
		}

		$strSql = 'SELECT
						count( DISTINCT ale.id )
					FROM
						ap_legal_entities ale
						JOIN ap_payees app ON( ale.cid = app.cid AND ale.ap_payee_id = app.id )
						JOIN ap_payee_locations apl ON ( apl.ap_payee_id = app.id AND apl.cid = app.cid AND apl.ap_legal_entity_id = ale.id AND apl.cid = ale.cid )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ale.vendor_entity_id IS NULL
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND app.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )'
						. $strWhere . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return $arrintCount[0]['count'];
	}

	public static function fetchPaginatedApLegalEntitiesDetailsByCid( $intCid, $objPagination, $objDatabase, $arrintApLegalEntityIds = NULL, $boolIsTaxIdRequired = true ) {

		$strWhere = ' AND ale.tax_number_encrypted IS NOT NULL';

		if( false == $boolIsTaxIdRequired ) {
			$strWhere = ' AND ale.tax_number_encrypted IS NULL';
		}

		if( true == ( $arrintApLegalEntityIds = getIntValuesFromArr( $arrintApLegalEntityIds ) ) ) {
			$strWhere .= ' AND ale.id NOT IN ( ' . implode( ',', $arrintApLegalEntityIds ) . ' )';
		}

		$strPaginationSql = '';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strPaginationSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$strSql = 'SELECT
						DISTINCT ON ( ale.id,app.company_name )
						ale.id,
						ale.ap_payee_id,
						ale.entity_name,
						CASE
							WHEN app.is_on_site IS true THEN \'Onsite\' ELSE \'Remote\'
						END as type,
						app.company_name,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.country_code,
						apl.city,
						apl.state_code,
						apl.postal_code
					FROM
						ap_legal_entities ale
						JOIN ap_payees app ON( ale.cid = app.cid AND ale.ap_payee_id = app.id )
						JOIN ap_payee_locations apl ON( ale.cid = apl.cid AND ale.id = apl.ap_legal_entity_id)
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ale.vendor_entity_id IS NULL
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND app.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						' . $strWhere . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
						type,
						ale.id,
						ale.ap_payee_id,
						ale.entity_name,
						app.company_name,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.country_code,
						apl.city,
						apl.state_code,
						apl.postal_code
					ORDER BY app.company_name, ale.id
						' . $strPaginationSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApLegalEntityDetailByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						id,
						entity_name, 
						ap_payee_id 
					FROM
						ap_legal_entities
					WHERE 
						id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSyncedVendorEntityIdsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						vendor_entity_id
					FROM
						ap_legal_entities
					WHERE
						vendor_entity_id IS NOT NULL
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApLegalEntityByIdsByApPayeeIdByCid( $arrintIds, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) || false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ale.*
					FROM
						ap_legal_entities ale
					WHERE
						ale.id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND ale.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND	ale.cid = ' . ( int ) $intCid . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );

	}

	public static function fetchCountApLegalEntitiesByVendorEntityIdsByCid( $arrintVendorEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintVendorEntityIds ) ) return NULL;

		$strSql = ' WHERE
						vendor_entity_id IN ( ' . implode( ',', $arrintVendorEntityIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return parent::fetchApLegalEntityCount( $strSql, $objDatabase );

	}

	public static function fetchApLegalEntitiesByVendorEntityIdsByCIds( $arrintVendorEntityId, $arrintCIds, $objClientDatabase, $boolIsActiveAndHoldOnly = false ) {

		if( false == valArr( $arrintCIds ) || false == valArr( $arrintVendorEntityId ) ) {
			return NULL;
		}
		$strWhere = '';
		if( true == $boolIsActiveAndHoldOnly ) {

			$strWhere = ' AND ap.ap_payee_status_type_id IN( ' . sqlIntImplode( [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ] ) . ' )';
		}

		$strSql = 'SELECT
						ale.*
					FROM
						ap_legal_entities AS ale
						JOIN ap_payees AS ap ON( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
					WHERE
						ale.vendor_entity_id IN ( ' . sqlIntImplode( $arrintVendorEntityId ) . ' )
						AND ale.cid IN ( ' . sqlIntImplode( $arrintCIds ) . ' )
						' . $strWhere . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase, false );
	}

	public static function fetchApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase, $boolIsSoftDelete = false, $strOrderBy = 'id' ) {

		$strWhere = '';

		if( true == $boolIsSoftDelete ) {
				$strWhere = ' AND deleted_by IS NULL
						AND deleted_on IS NULL';
		}

		$strWhere .= ' ORDER BY ' . $strOrderBy;

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid  = ' . ( int ) $intCid . $strWhere;

		return self::fetchApLegalEntities( $strSql, $objDatabase );
	}

	public static function fetchUnSyncedApLegalEntitiesByCids( $arrintCIds, $objDatabase ) {

		if( false == ( $arrintCIds = getIntValuesFromArr( $arrintCIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ale.id,
						ale.cid,
						ale.ap_payee_id,
						ale.tax_number_encrypted
					FROM
						ap_legal_entities ale
						JOIN ap_payees app ON( ale.cid = app.cid AND ale.ap_payee_id = app.id )
					WHERE
						ale.tax_number_encrypted IS NOT NULL
						AND ale.vendor_entity_id IS NULL
						AND app.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND ale.cid IN (' . sqlIntImplode( $arrintCIds ) . ' )
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApLegalEntitiesCountsByCids( $arrintCIds, $objDatabase ) {

		if( false == valArr( $arrintCIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT(ale.id) as total,
						COUNT( CASE WHEN ale.vendor_entity_id IS NOT NULL THEN ale.id END ) as synced,
						ale.cid
					FROM
						ap_legal_entities ale
						JOIN ap_payees app ON( ale.cid = app.cid AND ale.ap_payee_id = app.id )
					WHERE
						ale.cid IN (' . sqlIntImplode( $arrintCIds ) . ' )
						AND app.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
						ale.cid	';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApLegalEntitiesByVendorEntityIdsByCid( $arrintVendorEntityIds, $intCid, $objDatabase ) {

		if( false == ( $arrintVendorEntityIds = getIntValuesFromArr( $arrintVendorEntityIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						vendor_entity_id IN( ' . sqlIntImplode( $arrintVendorEntityIds ) . ' )
					AND cid = ' . ( int ) $intCid . ' 
					AND deleted_by IS NULL
					AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objDatabase );

	}

	public static function fetchSyncedApLegalEntityDetailsByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCId, $objDatabase ) {

		$strSql = 'SELECT
						vendor_entity_id
					FROM
						ap_legal_entities
					WHERE
						id = ' . ( int ) $intId . '
					AND ap_payee_id =  ' . ( int ) $intApPayeeId . '
					AND cid = ' . ( int ) $intCId . ' 
					AND deleted_by IS NULL
					AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function updateApLegalEntityByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCId, $intCompanyUserId ) {

		$strSql = 'UPDATE
						public.ap_legal_entities
					SET
						vendor_entity_id = NULL,
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCompanyUserId . '
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCId . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . ' 
						AND deleted_by IS NULL
						AND deleted_on IS NULL;';

		return $strSql;
	}

	public static function fetchApLegalEntitiesByEntityNameByCid( $strEntityName, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM ap_legal_entities
						WHERE
						entity_name = \'' . addslashes( trim( $strEntityName ) ) . '\'
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objDatabase );
	}

	public static function fetchApLegalEntitiesByIdsByCids( $arrintIds, $arrintCIds, $objClientDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) || false == ( $arrintCIds = getIntValuesFromArr( $arrintCIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ale.id,
						ale.cid,
						ale.entity_name
					FROM
						ap_legal_entities AS ale
						JOIN ap_payees AS ap ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
					WHERE
						ale.id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND ale.cid IN( ' . sqlIntImplode( $arrintCIds ) . ' )
						AND ap.ap_payee_status_type_id IN( ' . sqlIntImplode( [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ] ) . ' )
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApLegalEntitiesByCIds( $arrintCIds, $objClientDatabase ) {

		if( false == ( $arrintCIds = getIntValuesFromArr( $arrintCIds ) ) )
			return NULL;

		$strSql = 'SELECT
						ale.id,
						ale.cid,
						ale.ap_payee_id,
						ale.vendor_entity_id,
						ale.entity_name
					FROM
						ap_legal_entities AS ale
						JOIN ap_payees AS ap ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
					WHERE
						ale.cid IN ( ' . sqlIntImplode( $arrintCIds ) . ' )
						AND ap.ap_payee_status_type_id IN( ' . sqlIntImplode( [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ] ) . ' )
						AND ale.vendor_entity_id IS NOT NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApLegalEntitiesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) && false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND cid = ' . $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );
	}

	public static function fetchApLegalEntitiesByApRemittanceIdsByApPayeeIdByCid( $arrintApRemittanceIds, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApRemittanceIds ) || false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_legal_entities
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_remittance_id IN ( ' . sqlIntImplode( $arrintApRemittanceIds ) . ' ) 
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApLegalEntitiesByCid( $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ale.id
					FROM
						ap_legal_entities ale
						JOIN ap_payees app ON ( ale.cid = app.cid AND ale.ap_payee_id = app.id )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND app.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
					AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ' , ' . CApPayeeType::INTERCOMPANY . ' , ' . CApPayeeType::LENDER . ' )
					AND ale.deleted_by IS NULL
					AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApLegalEntitiesByVendorEntityIdsByCid( $arrintVendorEntityIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintVendorEntityIds = getIntValuesFromArr( $arrintVendorEntityIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apa.id,
						ale.ap_payee_id,
						apl.ap_legal_entity_id,
						apa.account_number,
						ale.vendor_entity_id,
						apl.store_id
					FROM
						ap_legal_entities ale
						JOIN ap_payee_locations apl ON ( ale.id = apl.ap_legal_entity_id AND ale.cid = apl.cid )
						LEFT JOIN ap_payee_accounts apa ON ( apl.id = apa.ap_payee_location_id AND apl.cid = apa.cid AND apa.is_disabled IS FALSE AND apa.account_number IS NOT NULL AND apa.buyer_account_id IS NULL )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ale.vendor_entity_id IN ( ' . sqlIntImplode( $arrintVendorEntityIds ) . ' )
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedSyncAndNonSyncApLegalEntitiesDetailsByCid( $intCid, $objPagination, $arrstrFilterValue, $objDatabase, $boolIsSyncEntity = false ) {

		$strPaginationSql = $strOrder = $strOrderByField = $strSortDirection = '';

		$strWhereClause = self::buildSearchCriteria( $arrstrFilterValue, $boolIsSyncEntity );

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strPaginationSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

			$strSortDirection = ( 1 == $objPagination->getOrderByType() ) ? ' DESC ' : ' ASC ';

			$strOrderByField = $objPagination->getOrderByField();
		}

		$arrstrSortBy = [
			'company_name' => 'ap.company_name ' . $strSortDirection,
			'entity_name' => 'ale.entity_name ' . $strSortDirection . ', ap.company_name, ale.last_invited_to_sync',
			'last_invited_to_sync' => 'ale.last_invited_to_sync ' . $strSortDirection . ', ap.company_name, ale.entity_name',
			'type' => 'type ' . $strSortDirection . ', ap.company_name, ale.entity_name'
		];

		$strOrderByField = ( true == valStr( $strByColumnName = getArrayElementByKey( $strOrderByField, $arrstrSortBy ) ) ) ? ' ORDER BY ' . addslashes( $strByColumnName ) : ' ORDER BY ap.company_name, ale.entity_name, ale.last_invited_to_sync';

		$strSql = 'SELECT
						ale.id,
						ap.company_name,
						ale.ap_payee_id,
						ale.entity_name,
						CASE
							WHEN ap.is_on_site IS true THEN \'Onsite\' ELSE \'Remote\'
						END as type,
						ap.company_name,
						max ( apl.street_line1 ) AS street_line1,
                        max ( apl.street_line2 ) AS street_line2,
                        max ( apl.street_line3 ) AS street_line3,
                        max ( apl.country_code ) AS country_code,
                        max ( apl.city ) AS city,
                        max ( apl.state_code ) AS state_code,
                        max ( apl.postal_code ) AS postal_code,
						ale.last_invited_to_sync,
						count( apl.id ) AS location_count
					FROM
						ap_legal_entities ale
						JOIN ap_payees ap ON( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON( ale.cid = apl.cid AND ale.id = apl.ap_legal_entity_id )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND ap.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						AND ale.tax_number_encrypted IS NOT NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND ale.vendor_entity_id IS NULL
						' . $strWhereClause . '
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apl.disabled_on IS NULL
					GROUP BY
						type,
						ale.id,
						ale.ap_payee_id,
						ale.entity_name,
						ap.company_name,
						ale.last_invited_to_sync 
						' . $strOrderByField
						. $strPaginationSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountSyncAndNonSyncApLegalEntitiesDetailsByCid( $intCid, $objDatabase, $arrstrFilterValue = [], $boolIsSyncEntity = false ) {

		$strWhereClause = self::buildSearchCriteria( $arrstrFilterValue, $boolIsSyncEntity );

		$strSql = 'SELECT
						COUNT( DISTINCT ale.id )
					FROM
						ap_legal_entities ale
						JOIN ap_payees ap ON( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON( ale.cid = apl.cid AND ale.id = apl.ap_legal_entity_id)
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND ap.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						AND ale.tax_number_encrypted IS NOT NULL
						' . $strWhereClause . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND ale.vendor_entity_id IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apl.disabled_on IS NULL';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return $arrintCount[0]['count'];
	}

	public static function buildSearchCriteria( $arrstrFilterValue, $boolIsSyncEntity ) {

		$strWhereClause = '';

		$strQuickSearch            = getArrayElementByKey( 'quick_search', $arrstrFilterValue );
		$arrstrEntityNames         = getArrayElementByKey( 'entity_name', $arrstrFilterValue );
		$arrmixTaxNumbersHash      = getArrayElementByKey( 'tax_number_hash', $arrstrFilterValue );
		$boolIsFromInviteVendors    = getArrayElementByKey( 'from_invite_vendors', $arrstrFilterValue );

		if( true == valArr( $arrmixTaxNumbersHash ) && true == valArr( $arrstrEntityNames ) ) {
			if( false == $boolIsSyncEntity ) {
				$strWhereClause = ' AND ( ale.tax_number_hash NOT IN ( ' . sqlStrImplode( $arrmixTaxNumbersHash ) . ' ) AND LOWER( ale.entity_name ) NOT IN ( ' . sqlStrImplode( $arrstrEntityNames ) . ' ) )';
			} else {
				$strWhereClause = ' AND ( ale.tax_number_hash IN ( ' . sqlStrImplode( $arrmixTaxNumbersHash ) . ' ) OR LOWER( ale.entity_name ) IN ( ' . sqlStrImplode( $arrstrEntityNames ) . ' ) )';
			}
		}

		if( true == valStr( $strQuickSearch ) ) {
			$strWhereClause .= ' AND LOWER(ap.company_name) like LOWER ( \'%' . trim( addslashes( $strQuickSearch ) ) . '%\' )';
		}

		if( true == $boolIsFromInviteVendors ) {
			$strWhereClause .= 'AND COALESCE( ( ap.details::jsonb ->>\'' . \CApPayee::IS_REMOVE_FROM_INVITE_VENDORS . '\' )::BOOLEAN, FALSE ) = FALSE';
		}

		return $strWhereClause;
	}

	public static function fetchActiveApLegalEntitiesByIdsByCids( $arrintIds, $arrintCIds, $objClientDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) || false == ( $arrintCIds = getIntValuesFromArr( $arrintCIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ale.*
					FROM
						ap_legal_entities AS ale
						JOIN ap_payees AS ap ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id )
					WHERE
						ale.id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND ale.cid IN( ' . sqlIntImplode( $arrintCIds ) . ' )
						AND ap.ap_payee_status_type_id IN( ' . sqlIntImplode( [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ] ) . ' )
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase, false );
	}

	public static function fetchCountApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		$strSql = ' WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid  = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return parent::fetchApLegalEntityCount( $strSql, $objDatabase );
	}

	public static function fetchActiveApLegalEntitiesByIds( $arrintApLegalEntityIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApLegalEntityIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ale.*,
						ale.entity_name AS payee_name,
						ot.default_owner_type_id
					FROM
						ap_legal_entities ale
						LEFT JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND ale.id = apl.ap_legal_entity_id )
						LEFT JOIN owner_types ot ON ( ot.cid = ale.cid AND ot.id = ale.owner_type_id AND ot.is_enabled IS TRUE )
					WHERE
						ale.id IN ( ' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND	ale.cid = ' . ( int ) $intCid . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApLegalEntities( $strSql, $objClientDatabase );

	}

}
?>