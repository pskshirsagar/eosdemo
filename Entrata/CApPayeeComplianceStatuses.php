<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeComplianceStatuses
 * Do not add any new functions to this class.
 */

class CApPayeeComplianceStatuses extends CBaseApPayeeComplianceStatuses {

	public static function fetchApPayeeComplianceStatusesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						* 
					FROM
						ap_payee_compliance_statuses
					WHERE
						cid = ' . ( int ) $intCid . '
					ORDER BY
						cid;';

		return parent::fetchApPayeeComplianceStatuses( $strSql, $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objDatabase ) {

		if( false == ( $arrintApPayeeIds = getIntValuesFromArr( $arrintApPayeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM
						ap_payee_compliance_statuses
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ANY ( ARRAY[' . sqlIntImplode( $arrintApPayeeIds ) . '] )';

		return self::fetchApPayeeComplianceStatuses( $strSql, $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByPropertyIdsByApLegalEntityIdsByApPayeeIdsByCid( $arrintPropertyIds, $arrintApLegalEntityIds, $arrintApPayeeIds, $intCid, $objDatabase ) {

		if( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) || false == ( $arrintApLegalEntityIds = getIntValuesFromArr( $arrintApLegalEntityIds ) ) || false == ( $arrintApPayeeIds = getIntValuesFromArr( $arrintApPayeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_compliance_statuses
					WHERE
						property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND ap_legal_entity_id IN( ' . sqlIntImplode( $arrintApLegalEntityIds ) . ' )
						AND ap_payee_id IN( ' . sqlIntImplode( $arrintApPayeeIds ) . ' )
						AND cid = ' . ( int ) $intCid . ';';

		return self::fetchApPayeeComplianceStatuses( $strSql, $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusCountByApPayeeIdByApLegalEntityIdByPropertyIdsByCid( $intApPayeeId, $intApLegalEntityId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_legal_entity_id = ' . ( int ) $intApLegalEntityId . '
						AND property_id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND compliance_status_id IN( ' . CComplianceStatus::NON_COMPLIANT . ',' . CComplianceStatus::PENDING . ',' . CComplianceStatus::REQUIRED . ' )';

		return parent::fetchApPayeeComplianceStatusCount( $strSql, $objDatabase );
	}

	public static function fetchNonCompliantApPayeeComplianceStatusCountByApPayeeIdByPropertyIdsByCid( $intApPayeeId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valId( $intApPayeeId ) || false == valArr( $arrintPropertyIds ) ) return 0;

		$strSql = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND compliance_status_id = ' . CComplianceStatus::NON_COMPLIANT;

		return parent::fetchApPayeeComplianceStatusCount( $strSql, $objDatabase );
	}

}
?>