<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPaymentAllowanceTypes
 * Do not add any new functions to this class.
 */

class CPaymentAllowanceTypes extends CBasePaymentAllowanceTypes {

	public static function fetchPaymentAllowanceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentAllowanceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaymentAllowanceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentAllowanceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedPaymentAllowanceTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM payment_allowance_types WHERE is_published=1 order by order_num';
		return self::fetchPaymentAllowanceTypes( $strSql, $objDatabase );
	}
}
?>