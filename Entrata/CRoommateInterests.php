<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterests
 * Do not add any new functions to this class.
 */

class CRoommateInterests extends CBaseRoommateInterests {

	// Note: if any changes made in this function you should do same for Psi\Eos\Entrata\fetchRoommateInterestsWithOptionsByPropertyIdByCid function

	public static function fetchRoommateInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ri.*
					FROM
						roommate_interests ri
						JOIN property_roommate_interests pri ON ( pri.cid = ri.cid AND pri.roommate_interest_id = ri.id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND pri.is_published = 1
						AND pri.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						ri.roommate_interest_category_id,
						pri.order_num';

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

 	public static function fetchRoommateInterestsByApplicantIdByLeaseIdByCid( $intApplicantId, $intLeaseId, $intCid, $objDatabase, $intPropertyId = NULL, $boolFetchGenderPreference = false ) {

 		if( false == is_numeric( $intApplicantId ) || false == is_numeric( $intCid ) ) return NULL;

 		$strJoinClause	= '';
 		$strOrderBy 	= 'ari.id ASC ';

 		if( false == is_null( $intPropertyId ) ) {
 			$strJoinClause	= 'LEFT JOIN property_roommate_interests pri ON ( pri.cid = ari.cid AND pri.roommate_interest_id = ari.roommate_interest_id AND pri.property_id = ' . ( int ) $intPropertyId . ' )';
 			$strOrderBy		= 'pri.order_num ';
 		}

 		$strWhere = ( true == $boolFetchGenderPreference ? ' OR ri.system_code = \'GN\' ' : '' );

		$strSql = 'SELECT
						ri.*,
						ari.written_response
					FROM
						applicant_roommate_interests ari
						JOIN roommate_interests ri ON ( ri.cid = ari.cid AND ( ri.id = ari.roommate_interest_id ' . $strWhere . ' ) )'
						. $strJoinClause . '
					WHERE
						ari.cid = ' . ( int ) $intCid . '
						AND ari.applicant_id = ' . ( int ) $intApplicantId . '
						AND ari.lease_id = ' . ( int ) $intLeaseId . '
					ORDER BY
						' . $strOrderBy;

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchRoommateInterestsByIdsByCid( $arrintRoommateInterestIds, $intCid, $objDatabase, $intInputTypeId = NULL ) {

		if( false == valArr( $arrintRoommateInterestIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhere = '';
		if( true == valId( $intInputTypeId ) ) {
			$strWhere = ' AND input_type_id = ' . ( int ) $intInputTypeId;
		}

		$strSql = 'SELECT * FROM roommate_interests WHERE cid = ' . ( int ) $intCid . ' AND id IN (' . implode( ',', $arrintRoommateInterestIds ) . ')' . $strWhere;

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchAllRoommateInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintInputTypeIds = NULL ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ri.*
					FROM
					    roommate_interests ri
					    JOIN property_roommate_interests pri ON ( pri.cid = ri.cid AND pri.roommate_interest_id = ri.id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
					    AND pri.property_id = ' . ( int ) $intPropertyId .
					    ( ( true == valArr( $arrintInputTypeIds ) ) ? ' AND ri.input_type_id IN ( ' . implode( ', ', $arrintInputTypeIds ) . ')' : '' ) . '
					ORDER BY
					    ri.roommate_interest_category_id,
					    pri.order_num';

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchActiveRoommateInterestsByCid( $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) return NULL;

		$strSql = '	SELECT 
						*
					FROM
						roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
					    roommate_interest_category_id,
					    order_num';

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchUnassociatedRoommateInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
			   			ri.*
					FROM
						roommate_interests ri
						LEFT JOIN property_roommate_interests pri ON ( pri.cid = ri.cid AND pri.roommate_interest_id = ri.id AND pri.property_id = ' . ( int ) $intPropertyId . ' )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND pri.roommate_interest_id IS NULL
					    AND ri.deleted_by IS NULL
						AND ri.deleted_on IS NULL
					ORDER BY
						ri.id';

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumByCid( $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						MAX( order_num ) AS max_order_num
					FROM
						roommate_interests
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'max_order_num', $objDatabase );
	}

	// Note: if any changes made in this function you should do same for Psi\Eos\Entrata\fetchRoommateInterestsWithCustomerAnswersByCustomerIdByLeaseIdByPropertyIdByCid function

	public static function fetchRoommateInterestsByCustomerIdByLeaseIdByPropertyIdByCid( $intCustomerId, $intLeaseId, $intPropertyId, $intCid, $objDatabase, $boolFetchGenderPreference = false, $arrintRoommateInterestIds = [] ) {
		if( false == is_numeric( $intCustomerId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhere = ( true == $boolFetchGenderPreference ? ' OR ri.system_code = \'GN\' ' : '' );
		$strWhereClause = ' ';
		if( true == valArr( $arrintRoommateInterestIds ) ) {
			$strRoommateInterestIds     = implode( ',', $arrintRoommateInterestIds );
			$strWhereClause = ' AND cri.roommate_interest_id IN ( ' . $strRoommateInterestIds . ' ) ';
		}
		$strSql = 'SELECT
					    ri.*,
					    cri.written_response
					FROM
					    customer_roommate_interests cri
					    JOIN roommate_interests ri ON ( ri.cid = cri.cid AND ( ri.id = cri.roommate_interest_id ' . $strWhere . ' ) )
					    LEFT JOIN property_roommate_interests pri ON ( pri.cid = cri.cid AND pri.roommate_interest_id = cri.roommate_interest_id AND pri.property_id = ' . ( int ) $intPropertyId . ' )
					WHERE
					    cri.cid = ' . ( int ) $intCid . '
					    AND cri.customer_id = ' . ( int ) $intCustomerId . '
					    AND cri.lease_id = ' . ( int ) $intLeaseId . $strWhereClause . '
					ORDER BY
						pri.order_num ';

		return self::fetchRoommateInterests( $strSql, $objDatabase );
	}

}
?>