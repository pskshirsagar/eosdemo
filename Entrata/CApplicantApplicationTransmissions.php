<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationTransmissions
 * Do not add any new functions to this class.
 */

class CApplicantApplicationTransmissions extends CBaseApplicantApplicationTransmissions {

	public static function fetchApplicantApplicationTransmissionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == is_null( $intApplicationId ) ) return NULL;
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions aat
						JOIN applicant_applications aa ON ( aat.cid = aa.cid AND aat.applicant_application_id = aa.id ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId;

		return self::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLastIntegratedApplicantApplicationTransmissionByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_application_id = ' . ( int ) $intApplicantApplicationId . '
						AND remote_primary_key IS NOT NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchLastestApplicantApplicationTransmissionByTransmissionIdByCid( $intTransmissionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						transmission_datetime
					FROM
						applicant_application_transmissions
					WHERE
						transmission_id = ' . ( int ) $intTransmissionId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchLastApplicantApplicationTransmissionByApplicantApplicationIdByLastTransmissionIdByCid( $intApplicantApplicationId, $intTransmissionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_application_id = ' . ( int ) $intApplicantApplicationId . '
						AND transmission_id <> ' . ( int ) $intTransmissionId . '
					ORDER BY
						id DESC
					LIMIT
						1';

		return self::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIdsByCid( $intApplicationId, $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          *,
					          MAX ( id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
					      FROM
					          applicant_application_transmissions
					      WHERE
					          cid = ' . ( int ) $intCid . '
					          AND application_id = ' . ( int ) $intApplicationId . '
					          AND applicant_application_id IN ( ' . implode( ', ', $arrintApplicantApplicationIds ) . ' )
					    ) AS sub
					WHERE
					    sub.id = max_application_id
					    AND sub.cid = ' . ( int ) $intCid;

		return self::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_application_id IN ( ' . implode( ', ', $arrintApplicantApplicationIds ) . ' )
					ORDER BY
						transmission_id DESC
					LIMIT
						' . \Psi\Libraries\UtilFunctions\count( $arrintApplicantApplicationIds );

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
					ORDER BY
						id DESC
					LIMIT
						1';

		return parent::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicantApplicationTransmissionsByTransmissionIdByCid( $intTransmissionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND transmission_id = ' . ( int ) $intTransmissionId . '
					ORDER BY
						applicant_application_id DESC';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestGuarantorApplicantApplicationTransmissionByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions as aat
						JOIN applicant_applications as aa ON ( aat.applicant_application_id = aa.id AND aat.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id = ' . CCustomerType::GUARANTOR . '
					ORDER BY aat.id desc LIMIT 1';

		return parent::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchLatestPrimaryApplicantApplicationTransmissionByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions aat
						LEFT JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
					ORDER BY
						aat.id DESC
					LIMIT
						1';

		return parent::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIdsByCid( $intApplicationId, $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return false;

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          *,
					          MAX ( aat.id ) OVER ( PARTITION BY aat.applicant_application_id ) AS max_applicant_application_id
					      FROM
					          applicant_application_transmissions aat
					      WHERE
					          aat.cid = ' . ( int ) $intCid . '
					          AND aat.application_id = ' . ( int ) $intApplicationId . '
					          AND aat.applicant_application_id IN ( ' . implode( ', ', $arrintApplicantApplicationIds ) . ' )
					    ) AS sub
					WHERE
					    sub.id = max_applicant_application_id
					    AND sub.cid = ' . ( int ) $intCid;

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( false == valArr( $arrintCustomerTypeIds ) ) return false;

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions aat
						LEFT JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
					ORDER BY
						aat.id DESC';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionByApplicationIdByApplicantApplicationIdByCid( $intApplicationId, $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions aat
						LEFT JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId . '
						AND aa.id = ' . ( int ) $intApplicantApplicationId . '
					ORDER BY
						aat.id DESC
					LIMIT
						1';

		return parent::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchTotalLatestApplicantApplicationTransmissionsForTransmissionApprovedByApplicantApplicationIdsByCustomerTypeIdsByCid( $arrintApplicantApplicationIds, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicantApplicationIds ) ) return true;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						sum( case when transmission_response_type_id = ' . CTransmissionResponseType::APPROVED . ' then 1 else 0 end ) as approved_transmission_count
					FROM
						(
							SELECT
								aat.*,
								MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
							FROM
								applicant_application_transmissions aat
								LEFT JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
							WHERE
								aat.cid = ' . ( int ) $intCid . '
								AND aat.applicant_application_id IN ( ' . implode( ', ', $arrintApplicantApplicationIds ) . ' )
								AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
						) AS sub
					WHERE
						id = max_application_id';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrData[0]['approved_transmission_count'] ) ) {
			return $arrstrData[0]['approved_transmission_count'];
		}

		return NULL;
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
					    *
					FROM
						(
						  SELECT
							  aat.*,
							  aa.customer_type_id,
							  MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
						  FROM
							  applicant_application_transmissions aat
							  JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
						  WHERE
							  aat.cid = ' . ( int ) $intCid . '
							  AND aat.application_id = ' . ( int ) $intApplicationId . '
							  AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
						  ORDER BY
							  aat.id DESC
						) AS sub
					WHERE
						id = max_application_id';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicationIdByCustomerIdByLeaseIdByLeaseIntervalTypeIdsByCid( $intApplicationId, $arrintCustomerIds, $intLeaseId, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						*
					FROM
						(
							SELECT
								aat.*,
								MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_applicant_application_transmission_id,
								apps.name_first,
								apps.name_last,
								apps.customer_id
							FROM
								applicant_application_transmissions aat
								LEFT JOIN applicant_applications aa ON ( aa.cid = aat.cid AND aa.id = aat.applicant_application_id ' . $strCheckDeletedAASql . ' )
								JOIN applicants apps ON ( apps.cid = aa.cid AND apps.id = aa.applicant_id )
								JOIN customers c ON ( c.cid = apps.cid AND c.id = apps.customer_id )
								JOIN lease_customers lc ON ( c.cid = lc.cid AND c.id = lc.customer_id )
							WHERE
								aat.application_id IN (
														SELECT
															ca.id
														FROM
															cached_applications ca
														WHERE
															ca.lease_interval_type_id IN ( ' . implode( ', ', $arrintLeaseIntervalTypeIds ) . ' )
															AND ca.cid = ' . ( int ) $intCid . '
								)
								AND c.id IN ( ' . implode( ', ', $arrintCustomerIds ) . ' )
								AND lc.lease_id = ' . ( int ) $intLeaseId . '
								AND aat.application_id = ' . ( int ) $intApplicationId . '
								AND aat.cid = ' . ( int ) $intCid . '
								AND aat.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						) AS sub
					WHERE
						id = max_applicant_application_transmission_id';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $intApplicantApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						aat.*
					FROM
						applicant_application_transmissions aat
						LEFT JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId . '
						AND aat.applicant_application_id = ' . ( int ) $intApplicantApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
					ORDER BY
						aat.id DESC';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchTotalLatestApplicantApplicationTransmissionsCountByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						count ( id )
					FROM
						(
							SELECT
								aat.*,
								aa.customer_type_id,
								MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
							FROM
								applicant_application_transmissions aat
								JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
							WHERE
								aat.cid = ' . ( int ) $intCid . '
								AND aat.application_id = ' . ( int ) $intApplicationId . '
								AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
							ORDER BY
								aat.id DESC
						) AS sub
					WHERE
						id = max_application_id';

		$arrintLatestTransmissionCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintLatestTransmissionCount[0] ) && true == array_key_exists( 'count', $arrintLatestTransmissionCount[0] ) ) {
			return $arrintLatestTransmissionCount[0]['count'];
		}

		return;
	}

	public static function fetchTotalLatestApplicantApplicationTransmissionsCountByApplicationIdsByCustomerTypeIdsByCid( $arrintApplicationIds, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						application_id,
						count ( id )
					FROM
						(
							SELECT
								aat.id,
								aat.application_id,
								MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
							FROM
								applicant_application_transmissions aat
								JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
							WHERE
								aat.cid = ' . ( int ) $intCid . '
								AND aat.application_id IN ( ' . implode( ', ', $arrintApplicationIds ) . ' )
								AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
							ORDER BY
								aat.id DESC
						) AS sub
					WHERE
						id = max_application_id
					GROUP BY
						application_id';

		$arrintApplicationsLatestTransmissionsCount = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'application_id', $arrintApplicationsLatestTransmissionsCount );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicationIdsByCustomerTypeIdsByCid( $arrintApplicationIds, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( array_filter( $arrintApplicationIds ) ) || false == valArr( array_filter( $arrintCustomerTypeIds ) ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
					    *
					FROM
						(
							SELECT
								aat.*,
								aa.customer_type_id,
								MAX ( aat.id ) OVER ( PARTITION BY applicant_application_id ) AS max_application_id
							FROM
								applicant_application_transmissions aat
								JOIN applicant_applications aa ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
							WHERE
								aat.cid = ' . ( int ) $intCid . '
								AND aat.application_id IN ( ' . implode( ', ',  $arrintApplicationIds ) . ' )
								AND aa.customer_type_id IN ( ' . implode( ', ', $arrintCustomerTypeIds ) . ' )
							ORDER BY
								aat.id DESC
						) AS sub
					WHERE
						id = max_application_id';

			return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByTransmissionIdByCid( $intTransmissionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND transmission_id = ' . ( int ) $intTransmissionId . '
					ORDER BY
						id DESC';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationIdsByApplicationIdByCidByApplicantApplicationIds( $intApplicationId, $intCid,  $arrintApplicantApplicationIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ( applicant_application_id )
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND applicant_application_id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ')';

		return parent::fetchApplicantApplicationTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicantApplicationTransmissionsByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          aat.*,
					          MAX ( aat.id ) OVER ( PARTITION BY aat.applicant_application_id ) AS max_application_id
					      FROM
					          applicant_application_transmissions aat
					          LEFT JOIN transmissions t ON ( t.cid = aat.cid AND t.application_id = aat.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' )
					          LEFT JOIN applicant_applications aa ON ( aa.cid = aat.cid AND aat.application_id = aa.application_id AND aa.applicant_id = ' . ( int ) $intApplicantId . ' )
					      WHERE
					          aat.cid = ' . ( int ) $intCid . '
					          AND aat.application_id = ' . ( int ) $intApplicationId . '
					    ) AS sub
					WHERE
					    sub.id = max_application_id
					    AND sub.cid = ' . ( int ) $intCid;

		return self::fetchApplicantApplicationTransmission( $strSql, $objDatabase );
	}

	public static function fetchLatestCustomApplicantApplicationTransmissionByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						transmission_remote_primary_key
					FROM
						applicant_application_transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
					ORDER BY
						id DESC
					LIMIT
						1';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult[0];
	}

	public static function fetchApplicantApplicationTransmissionsCountByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == is_null( $intApplicationId ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						count(aat.id)
					FROM
						applicant_application_transmissions aat
						JOIN transmissions t ON ( t.cid = aat.cid AND t.application_id = aat.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' )
						JOIN applicant_applications aa ON ( aat.cid = aa.cid AND aat.applicant_application_id = aa.id ' . $strCheckDeletedAASql . ' )
					WHERE
						aat.cid = ' . ( int ) $intCid . '
						AND aat.application_id = ' . ( int ) $intApplicationId;

		$arrintApplicationsTransmissionsCount = fetchData( $strSql, $objDatabase );

		return  current( $arrintApplicationsTransmissionsCount )['count'];
	}

}
?>