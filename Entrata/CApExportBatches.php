<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportBatches
 * Do not add any new functions to this class.
 */

class CApExportBatches extends CBaseApExportBatches {

	public static function fetchPaginatedApExportBatchesByCid( $intCid, $objPagination, $objClientDatabase, $objApExportBatchesFilter = NULL ) {

		$arrstrAndSearchParameters = [];

		if( true == valObj( $objApExportBatchesFilter, 'CApExportBatchesFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objApExportBatchesFilter, $intCid );
		}

		$strSql = 'SELECT
						aeb.*,
						CASE
							WHEN MAX ( ad.property_id ) <> MIN ( ad.property_id ) THEN \'Multiple\'
							ELSE MAX ( p.property_name )
						END AS property_name
					FROM
						ap_export_batches aeb
						LEFT JOIN ap_details ad ON ( aeb.cid = ad.cid AND aeb.id = ad.ap_export_batch_id )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
					WHERE
						aeb.cid = ' . ( int ) $intCid . '
					    AND aeb.deleted_on is NULL
 					    AND aeb.deleted_by is NULL';

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		$strSql .= ' GROUP BY
						aeb.id,
						aeb.cid,
						aeb.ap_export_batch_type_id,
						aeb.scheduled_ap_export_batch_id,
						aeb.batch_datetime,
						aeb.is_failed,
						aeb.updated_by,
						aeb.updated_on,
						aeb.created_by,
						aeb.created_on
					ORDER BY
						aeb.batch_datetime DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchApExportBatches( $strSql, $objClientDatabase );
	}

	public static function fetchApExportBatchesCountByCid( $intCid, $objClientDatabase, $objApExportBatchesFilter = NULL ) {

		$arrstrAndSearchParameters = [];

		if( true == valObj( $objApExportBatchesFilter, 'CApExportBatchesFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objApExportBatchesFilter, $intCid );
		}

		$strSql = 'SELECT
						COUNT( DISTINCT( aeb.id ) )
					FROM
						ap_export_batches aeb
						LEFT JOIN ap_details ad ON ( aeb.cid = ad.cid AND aeb.id = ad.ap_export_batch_id )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
					WHERE
						aeb.cid = ' . ( int ) $intCid . '
					    AND aeb.deleted_on is NULL
 					    AND aeb.deleted_by is NULL';

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchSearchCriteria( $objApExportBatchesFilter, $intCid ) {

		$arrstrAndSearchParameters = [];

		if( true == valArr( $objApExportBatchesFilter->getPropertyGroupIds() ) ) {
			$arrstrAndSearchParameters[] = ' ad.property_id IN (    SELECT
																		pga.property_id
                                                                    FROM
                                                                        properties AS p
                                                                        JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
                                                                        JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
                                                                    WHERE
                                                                        p.cid = ' . ( int ) $intCid . '
                                                                        AND p.disabled_on IS NULL
                                                                        AND pg.id IN ( ' . implode( ',', $objApExportBatchesFilter->getPropertyGroupIds() ) . ' )' . '
                                                                        AND pg.deleted_by IS NULL
                                                                        AND pg.deleted_on IS NULL
                                                                )';
		}

		if( true == valStr( $objApExportBatchesFilter->getStartDate() ) && 'Min' != $objApExportBatchesFilter->getStartDate() ) {
			$arrstrAndSearchParameters[] = ' aeb.batch_datetime >= \'' . date( 'Y-m-d', strtotime( $objApExportBatchesFilter->getStartDate() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApExportBatchesFilter->getEndDate() ) && 'Max' != $objApExportBatchesFilter->getEndDate() ) {
			$arrstrAndSearchParameters[] = ' aeb.batch_datetime <= \'' . date( 'Y-m-d', strtotime( $objApExportBatchesFilter->getEndDate() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApExportBatchesFilter->getPostMonth() ) ) {
			$arrstrAndSearchParameters[] = ' ad.post_month = \'' . $objApExportBatchesFilter->getPostMonth() . '\'';
		}

		if( true == valStr( $objApExportBatchesFilter->getExportType() ) ) {
			$arrstrAndSearchParameters[] = ' aeb.ap_export_batch_type_id = \'' . ( int ) $objApExportBatchesFilter->getExportType() . '\'';
		}

		return $arrstrAndSearchParameters;
	}

	public static function fetchRefundsExportedInLastSixMonthsByPropertyIdByCid( $intPropertyId, $intCid, $arrintApExportBatchTypes, $objClientDatabase, $arrintExcludeResidentIds = NULL ) {

		$strDateBeforeSixMonthsdate = date( 'Y/m/d H:i:s', strtotime( date( 'Y/m/d 00:00:00' ) . '-6 months' ) );

		$strSql = 'WITH temp_ap_export_batch AS 
					( SELECT
						aeb_temp.cid,
						ad_temp.property_id,
                        aeb_temp.batch_datetime
                    FROM
						ap_export_batches aeb_temp
						JOIN ap_details ad_temp ON ( ad_temp.cid = aeb_temp.cid AND ad_temp.ap_export_batch_id = aeb_temp.id )
					WHERE
						aeb_temp.cid = ' . ( int ) $intCid . '
						AND ad_temp.property_id = ' . ( int ) $intPropertyId . '
						AND aeb_temp.ap_export_batch_type_id IN ( ' . implode( ',', $arrintApExportBatchTypes ) . ' )
						AND aeb_temp.deleted_by IS NULL
						AND aeb_temp.deleted_on IS NULL
					ORDER BY
						aeb_temp.id DESC 
					OFFSET 1
					LIMIT 1)';

		$strSql .= 'SELECT
					DISTINCT ( aeb.id ),
					ah.ap_payee_id,
					ad.ap_header_id,
					COALESCE( lc.customer_id , ah.ap_payee_id ) As vendor_code,
					func_format_refund_customer_names( ah.header_memo ) AS refund_customer_names,
					lc.customer_id as resident_id,
					c.name_first,
					c.name_last,
					c.phone_number,
					c.email_address,
					ca.street_line1,
					ca.street_line2,
					ca.city,
					ca.state_code,
					ca.postal_code,
					ca.province,
					pp1.value as country_format,
					COALESCE( ca.country_code,  pa.country_code,  ua.country_code   ) as country_code
				FROM
					ap_export_batches aeb
					JOIN ap_details ad ON (aeb.id = ad.ap_export_batch_id AND aeb.cid = ad.cid)
					JOIN ap_headers ah ON (ad.ap_header_id = ah.id AND ad.cid = ah.cid)
					JOIN lease_customers lc ON (ah.cid = lc.cid AND ah.lease_customer_id = lc.id)
					JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
					LEFT JOIN lease_processes lp ON (lp.cid = lc.cid AND lp.lease_id = lc.lease_id AND lp.customer_id IS NULL)
					LEFT JOIN lease_customers lc1 ON (lc.cid = lc1.cid AND lc.lease_id = lc1.lease_id AND (CASE
                                                                                              WHEN ah.lease_customer_id IS NOT NULL AND lp.refund_method_id = ' . CRefundMethod::MULTIPLE_REFUNDS . ' THEN lc1.id = ah.lease_customer_id
                                                                                              ELSE lc1.customer_type_id = ' . CCustomerType::PRIMARY . '
                                                                                            END))
					LEFT JOIN customer_addresses ca ON (ca.cid = lc1.cid AND ca.customer_id = lc1.customer_id AND ca.address_type_id = ' . CAddressType::FORWARDING . ')
					LEFT JOIN property_addresses pa ON ( pa.cid = ad.cid AND pa.property_id = ad.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
					LEFT JOIN unit_addresses ua ON ( ua.cid = ad.cid and ua.property_unit_id = ad.property_unit_id )
					LEFT JOIN property_preferences pp1 ON ( ad.cid = pp1.cid AND ad.property_id = pp1.property_id AND pp1.key = \'AP_EXPORT_COUNTRY_FORMAT\' )
					LEFT JOIN temp_ap_export_batch taeb ON ( taeb.cid = ad.cid AND taeb.property_id = ad.property_id )
				WHERE
					ad.cid = ' . ( int ) $intCid . '
					AND ad.property_id = ' . ( int ) $intPropertyId . '
					AND ah.lease_customer_id IS NOT NULL
					AND ah.refund_ar_transaction_id IS NOT NULL
					AND aeb.ap_export_batch_type_id IN ( ' . implode( ',', $arrintApExportBatchTypes ) . ' )
					AND ( ca.updated_on > taeb.batch_datetime OR ca.created_on > taeb.batch_datetime )
					AND aeb.batch_datetime BETWEEN \'' . $strDateBeforeSixMonthsdate . '\' AND NOW()';
		if( false != valArr( $arrintExcludeResidentIds ) ) {
			$strSql .= ' AND lc.customer_id NOT IN ( ' . implode( ',', $arrintExcludeResidentIds ) . ' )';
		}

		return fetchData( $strSql, $objClientDatabase );

	}

}
?>