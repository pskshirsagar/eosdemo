<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseGenerationTypes
 * Do not add any new functions to this class.
 */

class CLeaseGenerationTypes extends CBaseLeaseGenerationTypes {

    public static function fetchLeaseGenerationTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CLeaseGenerationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchLeaseGenerationType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CLeaseGenerationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>