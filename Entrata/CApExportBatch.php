<?php

class CApExportBatch extends CBaseApExportBatch {

	protected $m_strPropertyName;
	protected $m_arrobjApDetails;
	protected $m_strFileName;
	protected $m_strFileExtension;
	protected $m_strFileContent;
	protected $m_arrstrFileName;
	protected $m_arrstrFileContent;
	protected $m_intFileCount;

	protected $m_boolIsShowFtp;
	protected $m_boolIsClientFtp;
	protected $m_boolIsEntrataFtp;
	protected $m_intConnectionType;
	protected $m_strHostName;
	protected $m_strUserName;
	protected $m_strPassword;
	protected $m_strFilePath;
	protected $m_intPort;

	protected $m_arrstrMissingRequiredFields = [];

	public $m_objExportsFileLibrary;

	const CONNECTION_TYPE_FTP  = 1;
	const CONNECTION_TYPE_SFTP = 2;

	const FILE_EXTENSION_CSV = 'csv';
	const FILE_EXTENSION_XLS = 'xls';
	const FILE_EXTENSION_TXT = 'txt';
	const FILE_EXTENSION_IIF = 'iif';

	const COUNTRY_FORMAT_THREE_DIGIT_COUNTRY_CODE = 1;
	const COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE   = 2;
	const COUNTRY_FORMAT_COUNTRY_NAME             = 3;

	public function getApDetails() {
		return $this->m_arrobjApDetails;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getShowFtp() {
		return $this->m_boolIsShowFtp;
	}

	public function getEntrataFtp() {
		return $this->m_boolIsEntrataFtp;
	}

	public function getClientFtp() {
		return $this->m_boolIsClientFtp;
	}

	public function getConnectionType() {
		return $this->m_intConnectionType;
	}

	public function getHostName() {
		return $this->m_strHostName;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function setShowFtp( $boolIsShowFtp ) {
		$this->m_boolIsShowFtp = $boolIsShowFtp;
	}

	public function setEntrataFtp( $boolIsEntrataFtp ) {
		$this->m_boolIsEntrataFtp = $boolIsEntrataFtp;
	}

	public function setClientFtp( $boolIsClientFtp ) {
		$this->m_boolIsClientFtp = $boolIsClientFtp;
	}

	public function setConnectionType( $intConnectionType ) {
		$this->m_intConnectionType = $intConnectionType;
	}

	public function setHostName( $strHostName ) {
		$this->m_strHostName = $strHostName;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setPort( $intPort ) {
		$this->m_intPort = $intPort;
	}

	public function setApDetails( $arrobjApDetails ) {
		$this->m_arrobjApDetails = $arrobjApDetails;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}

		if( true == isset( $arrmixValues['show_ftp'] ) ) {
			$this->setShowFtp( $arrmixValues['show_ftp'] );
		}

		if( true == isset( $arrmixValues['connection_type'] ) ) {
			$this->setConnectionType( $arrmixValues['connection_type'] );
		}

		if( true == isset( $arrmixValues['entrata_ftp'] ) ) {
			if( self::CONNECTION_TYPE_SFTP == $this->getConnectionType() ) {
				$this->setEntrataFtp( false );
				$this->setClientFtp( true );
			} else {
				$this->setEntrataFtp( $arrmixValues['entrata_ftp'] );
				$this->setClientFtp( false );
				if( false == $this->getEntrataFtp() ) {
					$this->setClientFtp( true );
				}
			}
		}

		if( true == isset( $arrmixValues['host_ip'] ) ) {
			$this->setHostName( $arrmixValues['host_ip'] );
		}

		if( true == isset( $arrmixValues['user'] ) ) {
			$this->setUserName( $arrmixValues['user'] );
		}

		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( $arrmixValues['password'] );
		}

		if( true == isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( $arrmixValues['file_path'] );
		}

		if( true == isset( $arrmixValues['port'] ) ) {
			$this->setPort( $arrmixValues['port'] );
		}

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApExportBatchTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApExportBatchTypeId() ) ) {
			$boolIsValid = false;
			$this->m_arrstrMissingRequiredFields[] = 'Export batch type';
		}

		return $boolIsValid;
	}

	public function valBatchDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFailed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHostName() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() ) {
			$strHostName = str_replace( 'sftp://', '', $this->getHostName() );
			if( false == valStr( $strHostName ) ) {
				$boolIsValid = false;
				$this->m_arrstrMissingRequiredFields[] = 'Host name';
			}

			if( false != valStr( $strHostName ) && false == filter_var( $strHostName, FILTER_VALIDATE_IP ) && false == filter_var( gethostbyname( $strHostName ), FILTER_VALIDATE_IP ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host_ip', 'Host name is not valid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valUserName() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false == valStr( $this->getUserName() ) ) {
			$boolIsValid = false;
			$this->m_arrstrMissingRequiredFields[] = 'Username';
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false == valStr( $this->getPassword() ) ) {
			$boolIsValid = false;
			$this->m_arrstrMissingRequiredFields[] = 'Password';
		}

		return $boolIsValid;
	}

	public function valPort() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && self::CONNECTION_TYPE_SFTP == $this->getConnectionType() && '' != $this->getPort() && false == is_numeric( $this->getPort() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', 'Port is not valid. ' ) );
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false != valStr( $this->getFilePath() ) && false == preg_match( '/^[\w\/\\\]*$/', $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', 'File path cannot contain special characters except _ \ / ' ) );
		}

		return $boolIsValid;
	}

	public function addRequiredErrorMsg() {
		if( false != valArr( $this->m_arrstrMissingRequiredFields ) ) {
			$strMessage = implode( ', ', $this->m_arrstrMissingRequiredFields );
			$strMessage = \Psi\Libraries\UtilFunctions\count( $this->m_arrstrMissingRequiredFields ) > 1 ? $strMessage . ' are required.' : $strMessage . ' is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strMessage ) );
		}

		return NULL;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valApExportBatchTypeId();
				$boolIsValid &= $this->valHostName();
				$boolIsValid &= $this->valUserName();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valPort();
				$boolIsValid &= $this->valFilePath();
				$this->addRequiredErrorMsg();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function parseCustomerNameAndAddress( $strCustomerName, $strCustomerAddress ) {

		$intMaxLength		= 35;
		$arrstrNames		= [];
		$arrstrCustomerInfo	= [];

		if( false == valStr( $strCustomerName ) && false == valStr( $strCustomerAddress ) ) {
			return $arrstrCustomerInfo;
		}

		$strCustomerName		= \Psi\CStringService::singleton()->wordwrap( $strCustomerName, $intMaxLength, '~~' );
		$strCustomerAddress		= \Psi\CStringService::singleton()->wordwrap( $strCustomerAddress, $intMaxLength, '~~' );

		$arrstrCustomerName		= array_filter( explode( '~~', $strCustomerName ) );
		$arrstrCustomerAddress	= array_filter( explode( '~~', $strCustomerAddress ) );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrCustomerName ) ) {

			if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrCustomerAddress ) ) {

				$arrstrNames[0]		= $arrstrCustomerName[0];
				$arrstrCustomerInfo	= array_merge( $arrstrNames, $arrstrCustomerAddress );
			} else {

				$intCountEmptyAddressRows = 3 - \Psi\Libraries\UtilFunctions\count( $arrstrCustomerAddress );

				for( $intIndex = 0; $intIndex < $intCountEmptyAddressRows; $intIndex++ ) {

					if( true == isset( $arrstrCustomerName[$intIndex] ) && true == valStr( $arrstrCustomerName[$intIndex] ) ) {
						$arrstrNames[$intIndex] = $arrstrCustomerName[$intIndex];
					}
				}

				$arrstrCustomerInfo = array_merge( $arrstrNames, $arrstrCustomerAddress );
			}

			$arrstrCustomerNameAndAddress	= array_merge( $arrstrCustomerName, $arrstrCustomerAddress );

			if( 4 >= \Psi\Libraries\UtilFunctions\count( $arrstrCustomerNameAndAddress ) ) {
				$arrstrCustomerInfo = $arrstrCustomerNameAndAddress;
			}

		} else {
			$arrstrCustomerName[0]	= NULL;
			$arrstrCustomerInfo		= array_merge( $arrstrCustomerName, $arrstrCustomerAddress );
		}

		return $arrstrCustomerInfo;
	}

	public function export( $arrmixExportData, $arrmixConnectionData = NULL ) {

		$this->m_objExportsFileLibrary = new CExportsFileLibrary();

		switch( $this->getApExportBatchTypeId() ) {
			case CApExportBatchType::TIMBERLINE_PM:
				return $this->exportInTimberlinePmFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData );
				break;

			case CApExportBatchType::PEOPLESOFT:
				return $this->exportInPeopleSoftFormat( $arrmixExportData );
				break;

			case CApExportBatchType::YARDI:
				return $this->exportInYardiFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData );
				break;

			case CApExportBatchType::YARDI_ETL_INVOICE_REGISTER:
			case CApExportBatchType::YARDI_ETL_FINPAYABLES:
				return $this->exportInYardiEtlInvoiceRegisterFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData );
				break;

			default:
				trigger_error( 'Unexpected ap export batch type.', E_USER_ERROR );
				break;
		}
		return true;
	}

	public function exportInPeopleSoftFormat( $arrmixExportData ) {

		$boolIsValid					= true;
		$intMaxLength					= 35;
		$arrobjTransmissions            = [];
		$arrmixApHeaders				= [];
		$arrmixPropertyPreferences		= [];
		$arrmixApDetails				= $arrmixExportData['ap_details'];
		$intCompanyUserId				= $arrmixExportData['cid'];
		$objDatabase					= $arrmixExportData['db_obj'];
		$boolIsIncludeReversals			= $arrmixExportData['is_include_reversals'];

		$objCompanyTranmissionVendor	= \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByTransmissionTypeIdByTransmissionVendorIdByCid( CTransmissionType::FINANCIAL, CTransmissionVendor::PEOPLESOFT, $this->getCid(), $objDatabase );

		if( false == valObj( $objCompanyTranmissionVendor, 'CCompanyTransmissionVendor' ) || false == valStr( $objCompanyTranmissionVendor->getRequestUrl() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load company transmission vendor object or its URL.', NULL ) );
			return false;
		}

		$arrintPropertyIds			= array_filter( array_keys( rekeyArray( 'property_id', $arrmixApDetails ) ) );
		$arrobjPropertyPreferences	= ( array ) \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysKeyedByKeyByPropertyIdsByCid( [ 'AP_BUSINESS_UNIT', 'PEOPLESOFT_BANK_CODE', 'PEOPLESOFT_BANK_ACCOUNT', 'PROPERTY_GL_BU' ], $arrintPropertyIds, $this->getCid(), $objDatabase );

		foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
			$arrmixPropertyPreferences[$objPropertyPreference->getPropertyId()][$objPropertyPreference->getKey()] = $objPropertyPreference->getValue();
		}

		$this->m_arrobjApDetails = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByIdsByCid( array_filter( array_keys( rekeyArray( 'id', $arrmixApDetails ) ) ), $this->getCid(), $objDatabase, !$boolIsIncludeReversals );

		$arrmixParameters = [
			'url' => $objCompanyTranmissionVendor->getRequestUrl() . 'VOUCHER_BUILD.1.wsdl',
			'connection_timeout' => 10,
			'execution_timeout' => 30,
			'ssl_verify_peer' => false,
			'ssl_verify_host' => false,
			'request_method' => 'POST',
			'ssl_version' => 3
		];

		$objExternalRequest = new CExternalRequest();
		$objExternalRequest->setParameters( $arrmixParameters );

		// Rearrange array so that we can easily create one xml per invoice with multiple distributions if invoice has multiple line items.
		// Copying only required fields
		foreach( $arrmixApDetails as $arrmixApDetail ) {
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['id']						= $arrmixApDetail['ap_header_id'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['cid']	= $arrmixApDetail['cid'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['property_id']			= $arrmixApDetail['property_id'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['property_name']			= $arrmixApDetail['property_name'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['customer_name']			= $arrmixApDetail['customer_name'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['code_three_digit']		= $arrmixApDetail['code_three_digit'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['forwarding_address']		= $arrmixApDetail['forwarding_address'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['city']					= $arrmixApDetail['city'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['state_code']				= $arrmixApDetail['state_code'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['postal_code']			= $arrmixApDetail['postal_code'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['invoice_id']				= $arrmixApDetail['invoice_id'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['move_out_date']			= $arrmixApDetail['move_out_date'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['accounting_date']		= $arrmixApDetail['accounting_date'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['total_amount']			= $arrmixApDetail['total_amount'];

			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['ap_details'][$arrmixApDetail['id']]['id']					= $arrmixApDetail['id'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['ap_details'][$arrmixApDetail['id']]['transaction_amount']	= $arrmixApDetail['transaction_amount'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['ap_details'][$arrmixApDetail['id']]['account_number']		= $arrmixApDetail['account_number'];
			$arrmixApHeaders[$arrmixApDetail['ap_header_id']]['ap_details'][$arrmixApDetail['id']]['line_item_description']	= $arrmixApDetail['line_item_description'];
		}

		foreach( $arrmixApHeaders as $arrmixApHeader ) {

			$intDistributionNumber		= 1;
			$intLineNumber				= 1;

			$strCustomerName			= NULL;
			$strCustomerAddress1		= NULL;
			$strCustomerAddress2		= NULL;
			$strCustomerAddress3		= NULL;

			$strApBusinessUnit			= ( true == isset( $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['AP_BUSINESS_UNIT'] ) ) ? $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['AP_BUSINESS_UNIT'] : NULL;
			$strPeoplesoftBankCode		= ( true == isset( $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PEOPLESOFT_BANK_CODE'] ) ) ? $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PEOPLESOFT_BANK_CODE'] : NULL;
			$strPeoplesoftBankAccount	= ( true == isset( $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PEOPLESOFT_BANK_ACCOUNT'] ) ) ? $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PEOPLESOFT_BANK_ACCOUNT'] : NULL;
			$strPropertyGlBu			= ( true == isset( $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PROPERTY_GL_BU'] ) ) ? $arrmixPropertyPreferences[$arrmixApHeader['property_id']]['PROPERTY_GL_BU'] : NULL;

			$arrstrCustomerInfo			= ( array ) $this->parseCustomerNameAndAddress( $arrmixApHeader['customer_name'], $arrmixApHeader['forwarding_address'] );

			// Need to trim long words having length > max length
			foreach( $arrstrCustomerInfo as $strKey => $strValue ) {
				if( true == valStr( $strValue ) ) {
					$arrstrCustomerInfo[$strKey] = \Psi\CStringService::singleton()->substr( $strValue, 0, $intMaxLength );
				}
			}

			if( true == isset( $arrstrCustomerInfo[0] ) ) {
				$strCustomerName = \Psi\CStringService::singleton()->htmlspecialchars( $arrstrCustomerInfo[0] );
			}

			if( true == isset( $arrstrCustomerInfo[1] ) ) {
				$strCustomerAddress1 = \Psi\CStringService::singleton()->htmlspecialchars( $arrstrCustomerInfo[1] );
			}

			if( true == isset( $arrstrCustomerInfo[2] ) ) {
				$strCustomerAddress2 = \Psi\CStringService::singleton()->htmlspecialchars( $arrstrCustomerInfo[2] );
			}

			if( true == isset( $arrstrCustomerInfo[3] ) ) {
				$strCustomerAddress3 = \Psi\CStringService::singleton()->htmlspecialchars( $arrstrCustomerInfo[3] );
			}

			$strPostString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vouc="http://xmlns.oracle.com/Enterprise/Tools/schemas/VOUCHER_BUILD.VERSION_2">
									<soapenv:Header/>
									<soapenv:Body>
										<vouc:VOUCHER_BUILD>
											<vouc:MsgData>
												<vouc:Transaction>
													<vouc:VCHR_VNDR_STG class="R">
														<vouc:BUSINESS_UNIT IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT>
														<vouc:VOUCHER_ID IsChanged="Y">NEXT</vouc:VOUCHER_ID>
														<!--Optional:-->
														<vouc:NAME1 IsChanged="Y">' . $strCustomerName . '</vouc:NAME1>
														<!--Optional:-->
														<vouc:EMAILID IsChanged="Y"></vouc:EMAILID>
														<!--Optional:-->
														<vouc:COUNTRY IsChanged="Y">' . $arrmixApHeader['code_three_digit'] . '</vouc:COUNTRY>
														<!--Optional:-->
														<vouc:ADDRESS1 IsChanged="Y">' . $strCustomerAddress1 . '</vouc:ADDRESS1>
														<!--Optional:-->
														<vouc:ADDRESS2 IsChanged="Y">' . $strCustomerAddress2 . '</vouc:ADDRESS2>
														<!--Optional:-->
														<vouc:ADDRESS3 IsChanged="Y">' . $strCustomerAddress3 . '</vouc:ADDRESS3>
														<!--Optional:-->
														<vouc:ADDRESS4 IsChanged="Y"></vouc:ADDRESS4>
														<!--Optional:-->
														<vouc:CITY IsChanged="Y">' . $arrmixApHeader['city'] . '</vouc:CITY>
														<!--Optional:-->
														<vouc:NUM1 IsChanged="Y"></vouc:NUM1>
														<!--Optional:-->
														<vouc:NUM2 IsChanged="Y"></vouc:NUM2>
														<!--Optional:-->
														<vouc:HOUSE_TYPE IsChanged="Y"></vouc:HOUSE_TYPE>
														<!--Optional:-->
														<vouc:ADDR_FIELD1 IsChanged="Y"></vouc:ADDR_FIELD1>
														<!--Optional:-->
														<vouc:ADDR_FIELD2 IsChanged="Y"></vouc:ADDR_FIELD2>
														<!--Optional:-->
														<vouc:ADDR_FIELD3 IsChanged="Y"></vouc:ADDR_FIELD3>
														<!--Optional:-->
														<vouc:COUNTY IsChanged="Y"></vouc:COUNTY>
														<!--Optional:-->
														<vouc:STATE IsChanged="Y">' . $arrmixApHeader['state_code'] . '</vouc:STATE>
														<!--Optional:-->
														<vouc:POSTAL IsChanged="Y">' . $arrmixApHeader['postal_code'] . '</vouc:POSTAL>
														<!--Optional:-->
														<vouc:GEO_CODE IsChanged="Y"></vouc:GEO_CODE>
														<!--Optional:-->
														<vouc:IN_CITY_LIMIT IsChanged="Y"></vouc:IN_CITY_LIMIT>
													</vouc:VCHR_VNDR_STG>
													<vouc:VCHR_HDR_STG class="R">
														<vouc:BUSINESS_UNIT IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT>
														<vouc:VOUCHER_ID IsChanged="Y">NEXT</vouc:VOUCHER_ID>
														<!--Optional:-->
														<vouc:VOUCHER_STYLE IsChanged="Y">SGLP</vouc:VOUCHER_STYLE>
														<!--Optional:-->
														<vouc:INVOICE_ID IsChanged="Y">' . $arrmixApHeader['invoice_id'] . '</vouc:INVOICE_ID>
														<!--Optional:-->
														<vouc:INVOICE_DT IsChanged="Y">' . $arrmixApHeader['move_out_date'] . '</vouc:INVOICE_DT>
														<!--Optional:-->
														<vouc:VENDOR_ID IsChanged="Y">1000103489</vouc:VENDOR_ID>
														<!--Optional:-->
														<vouc:VNDR_LOC IsChanged="Y"></vouc:VNDR_LOC>
														<!--Optional:-->
														<vouc:ADDRESS_SEQ_NUM IsChanged="Y">1</vouc:ADDRESS_SEQ_NUM>
														<!--Optional:-->
														<vouc:GRP_AP_ID IsChanged="Y"></vouc:GRP_AP_ID>
														<!--Optional:-->
														<vouc:ORIGIN IsChanged="Y">SOD</vouc:ORIGIN>
														<!--Optional:-->
														<vouc:OPRID IsChanged="Y">PSI</vouc:OPRID>
														<!--Optional:-->
														<vouc:ACCOUNTING_DT IsChanged="Y">' . $arrmixApHeader['accounting_date'] . '</vouc:ACCOUNTING_DT>
														<!--Optional:-->
														<vouc:POST_VOUCHER IsChanged="Y"></vouc:POST_VOUCHER>
														<!--Optional:-->
														<vouc:DST_CNTRL_ID IsChanged="Y"></vouc:DST_CNTRL_ID>
														<!--Optional:-->
														<vouc:VOUCHER_ID_RELATED IsChanged="Y"></vouc:VOUCHER_ID_RELATED>
														<!--Optional:-->
														<vouc:GROSS_AMT IsChanged="Y">' . $arrmixApHeader['total_amount'] . '</vouc:GROSS_AMT>
														<!--Optional:-->
														<vouc:DSCNT_AMT IsChanged="Y"></vouc:DSCNT_AMT>
														<!--Optional:-->
														<vouc:TAX_EXEMPT IsChanged="Y">Y</vouc:TAX_EXEMPT>
														<!--Optional:-->
														<vouc:SALETX_AMT IsChanged="Y"></vouc:SALETX_AMT>
														<!--Optional:-->
														<vouc:FREIGHT_AMT IsChanged="Y"></vouc:FREIGHT_AMT>
														<!--Optional:-->
														<vouc:MISC_AMT IsChanged="Y"></vouc:MISC_AMT>
														<!--Optional:-->
														<vouc:PYMNT_TERMS_CD IsChanged="Y">00</vouc:PYMNT_TERMS_CD>
														<!--Optional:-->
														<vouc:ENTERED_DT IsChanged="Y">' . date( 'Y/m/d' ) . '</vouc:ENTERED_DT>
														<!--Optional:-->
														<vouc:TXN_CURRENCY_CD IsChanged="Y">USD</vouc:TXN_CURRENCY_CD>
														<!--Optional:-->
														<vouc:RT_TYPE IsChanged="Y"></vouc:RT_TYPE>
														<!--Optional:-->
														<vouc:RATE_MULT IsChanged="Y"></vouc:RATE_MULT>
														<!--Optional:-->
														<vouc:RATE_DIV IsChanged="Y"></vouc:RATE_DIV>
														<!--Optional:-->
														<vouc:VAT_ENTRD_AMT IsChanged="Y"></vouc:VAT_ENTRD_AMT>
														<!--Optional:-->
														<vouc:MATCH_ACTION IsChanged="Y">N</vouc:MATCH_ACTION>
														<!--Optional:-->
														<vouc:CUR_RT_SOURCE IsChanged="Y"></vouc:CUR_RT_SOURCE>
														<!--Optional:-->
														<vouc:DSCNT_AMT_FLG IsChanged="Y"></vouc:DSCNT_AMT_FLG>
														<!--Optional:-->
														<vouc:DUE_DT_FLG IsChanged="Y"></vouc:DUE_DT_FLG>
														<!--Optional:-->
														<vouc:VCHR_APPRVL_FLG IsChanged="Y"></vouc:VCHR_APPRVL_FLG>
														<!--Optional:-->
														<vouc:BUSPROCNAME IsChanged="Y"></vouc:BUSPROCNAME>
														<!--Optional:-->
														<vouc:APPR_RULE_SET IsChanged="Y"></vouc:APPR_RULE_SET>
														<!--Optional:-->
														<vouc:VAT_DCLRTN_POINT IsChanged="Y"></vouc:VAT_DCLRTN_POINT>
														<!--Optional:-->
														<vouc:VAT_CALC_TYPE IsChanged="Y"></vouc:VAT_CALC_TYPE>
														<!--Optional:-->
														<vouc:VAT_CALC_GROSS_NET IsChanged="Y"></vouc:VAT_CALC_GROSS_NET>
														<!--Optional:-->
														<vouc:VAT_RECALC_FLG IsChanged="Y"></vouc:VAT_RECALC_FLG>
														<!--Optional:-->
														<vouc:VAT_CALC_FRGHT_FLG IsChanged="Y"></vouc:VAT_CALC_FRGHT_FLG>
														<!--Optional:-->
														<vouc:VAT_TREATMENT_GRP IsChanged="Y"></vouc:VAT_TREATMENT_GRP>
														<!--Optional:-->
														<vouc:COUNTRY_SHIP_FROM IsChanged="Y"></vouc:COUNTRY_SHIP_FROM>
														<!--Optional:-->
														<vouc:STATE_SHIP_FROM IsChanged="Y"></vouc:STATE_SHIP_FROM>
														<!--Optional:-->
														<vouc:COUNTRY_SHIP_TO IsChanged="Y"></vouc:COUNTRY_SHIP_TO>
														<!--Optional:-->
														<vouc:STATE_SHIP_TO IsChanged="Y"></vouc:STATE_SHIP_TO>
														<!--Optional:-->
														<vouc:COUNTRY_VAT_BILLFR IsChanged="Y"></vouc:COUNTRY_VAT_BILLFR>
														<!--Optional:-->
														<vouc:COUNTRY_VAT_BILLTO IsChanged="Y"></vouc:COUNTRY_VAT_BILLTO>
														<!--Optional:-->
														<vouc:VAT_EXCPTN_CERTIF IsChanged="Y"></vouc:VAT_EXCPTN_CERTIF>
														<!--Optional:-->
														<vouc:VAT_ROUND_RULE IsChanged="Y"></vouc:VAT_ROUND_RULE>
														<!--Optional:-->
														<vouc:COUNTRY_LOC_SELLER IsChanged="Y"></vouc:COUNTRY_LOC_SELLER>
														<!--Optional:-->
														<vouc:STATE_LOC_SELLER IsChanged="Y"></vouc:STATE_LOC_SELLER>
														<!--Optional:-->
														<vouc:COUNTRY_LOC_BUYER IsChanged="Y"></vouc:COUNTRY_LOC_BUYER>
														<!--Optional:-->
														<vouc:STATE_LOC_BUYER IsChanged="Y"></vouc:STATE_LOC_BUYER>
														<!--Optional:-->
														<vouc:COUNTRY_VAT_SUPPLY IsChanged="Y"></vouc:COUNTRY_VAT_SUPPLY>
														<!--Optional:-->
														<vouc:STATE_VAT_SUPPLY IsChanged="Y"></vouc:STATE_VAT_SUPPLY>
														<!--Optional:-->
														<vouc:COUNTRY_VAT_PERFRM IsChanged="Y"></vouc:COUNTRY_VAT_PERFRM>
														<!--Optional:-->
														<vouc:STATE_VAT_PERFRM IsChanged="Y"></vouc:STATE_VAT_PERFRM>
														<!--Optional:-->
														<vouc:STATE_VAT_DEFAULT IsChanged="Y"></vouc:STATE_VAT_DEFAULT>
														<!--Optional:-->
														<vouc:PREPAID_REF IsChanged="Y"></vouc:PREPAID_REF>
														<!--Optional:-->
														<vouc:PREPAID_AUTO_APPLY IsChanged="Y"></vouc:PREPAID_AUTO_APPLY>
														<!--Optional:-->
														<vouc:DESCR254_MIXED IsChanged="Y"></vouc:DESCR254_MIXED>
														<!--Optional:-->
														<vouc:EIN_FEDERAL IsChanged="Y"></vouc:EIN_FEDERAL>
														<!--Optional:-->
														<vouc:EIN_STATE_LOCAL IsChanged="Y"></vouc:EIN_STATE_LOCAL>
														<!--Optional:-->
														<vouc:BUSINESS_UNIT_PO IsChanged="Y"></vouc:BUSINESS_UNIT_PO>
														<!--Optional:-->
														<vouc:PO_ID IsChanged="Y"></vouc:PO_ID>
														<!--Optional:-->
														<vouc:PACKSLIP_NO IsChanged="Y"></vouc:PACKSLIP_NO>
														<!--Optional:-->
														<vouc:PAY_TRM_BSE_DT_OPT IsChanged="Y"></vouc:PAY_TRM_BSE_DT_OPT>
														<!--Optional:-->
														<vouc:VAT_CALC_MISC_FLG IsChanged="Y"></vouc:VAT_CALC_MISC_FLG>
														<!--Optional:-->
														<vouc:PAY_SCHEDULE_TYPE IsChanged="Y"></vouc:PAY_SCHEDULE_TYPE>
														<!--Optional:-->
														<vouc:TAX_GRP IsChanged="Y"></vouc:TAX_GRP>
														<!--Optional:-->
														<vouc:TAX_PYMNT_TYPE IsChanged="Y"></vouc:TAX_PYMNT_TYPE>
														<!--Optional:-->
														<vouc:INSPECT_DT IsChanged="Y"></vouc:INSPECT_DT>
														<!--Optional:-->
														<vouc:INV_RECPT_DT IsChanged="Y"></vouc:INV_RECPT_DT>
														<!--Optional:-->
														<vouc:RECEIPT_DT IsChanged="Y"></vouc:RECEIPT_DT>
														<!--Optional:-->
														<vouc:BILL_OF_LADING IsChanged="Y"></vouc:BILL_OF_LADING>
														<!--Optional:-->
														<vouc:CARRIER_ID IsChanged="Y"></vouc:CARRIER_ID>
														<!--Optional:-->
														<vouc:DOC_TYPE IsChanged="Y"></vouc:DOC_TYPE>
														<!--Optional:-->
														<vouc:DSCNT_DUE_DT IsChanged="Y"></vouc:DSCNT_DUE_DT>
														<!--Optional:-->
														<vouc:DSCNT_PRORATE_FLG IsChanged="Y"></vouc:DSCNT_PRORATE_FLG>
														<!--Optional:-->
														<vouc:DUE_DT IsChanged="Y"></vouc:DUE_DT>
														<!--Optional:-->
														<vouc:FRGHT_CHARGE_CODE IsChanged="Y"></vouc:FRGHT_CHARGE_CODE>
														<!--Optional:-->
														<vouc:LC_ID IsChanged="Y"></vouc:LC_ID>
														<!--Optional:-->
														<vouc:MISC_CHARGE_CODE IsChanged="Y"></vouc:MISC_CHARGE_CODE>
														<!--Optional:-->
														<vouc:REMIT_ADDR_SEQ_NUM IsChanged="Y"></vouc:REMIT_ADDR_SEQ_NUM>
														<!--Optional:-->
														<vouc:SALETX_CHARGE_CODE IsChanged="Y"></vouc:SALETX_CHARGE_CODE>
														<!--Optional:-->
														<vouc:VCHR_BLD_CODE IsChanged="Y"></vouc:VCHR_BLD_CODE>
														<!--Optional:-->
														<vouc:BUSINESS_UNIT_AR IsChanged="Y"></vouc:BUSINESS_UNIT_AR>
														<!--Optional:-->
														<vouc:CUST_ID IsChanged="Y"></vouc:CUST_ID>
														<!--Optional:-->
														<vouc:ITEM IsChanged="Y"></vouc:ITEM>
														<!--Optional:-->
														<vouc:ITEM_LINE IsChanged="Y"></vouc:ITEM_LINE>
														<!--Optional:-->
														<vouc:VCHR_SRC IsChanged="Y">XML</vouc:VCHR_SRC>
														<!--Optional:-->
														<vouc:VAT_EXCPTN_TYPE IsChanged="Y"></vouc:VAT_EXCPTN_TYPE>
														<!--Optional:-->
														<vouc:TERMS_BASIS_DT IsChanged="Y"></vouc:TERMS_BASIS_DT>
														<!--Optional:-->
														<vouc:USER_VCHR_CHAR1 IsChanged="Y"></vouc:USER_VCHR_CHAR1>
														<!--Optional:-->
														<vouc:USER_VCHR_CHAR2 IsChanged="Y"></vouc:USER_VCHR_CHAR2>
														<!--Optional:-->
														<vouc:USER_VCHR_DEC IsChanged="Y"></vouc:USER_VCHR_DEC>
														<!--Optional:-->
														<vouc:USER_VCHR_DATE IsChanged="Y"></vouc:USER_VCHR_DATE>
														<!--Optional:-->
														<vouc:USER_VCHR_NUM1 IsChanged="Y"></vouc:USER_VCHR_NUM1>
														<!--Optional:-->
														<vouc:USER_HDR_CHAR1 IsChanged="Y"></vouc:USER_HDR_CHAR1>';

			foreach( $arrmixApHeader['ap_details'] as $arrmixApDetail ) {

				$strPostString .= '	<vouc:VCHR_LINE_STG class="R">
															<vouc:BUSINESS_UNIT IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT>
															<vouc:VOUCHER_ID IsChanged="Y">NEXT</vouc:VOUCHER_ID>
															<vouc:VOUCHER_LINE_NUM IsChanged="Y">' . $intLineNumber . '</vouc:VOUCHER_LINE_NUM>
															<!--Optional:-->
															<vouc:BUSINESS_UNIT_PO IsChanged="Y"></vouc:BUSINESS_UNIT_PO>
															<!--Optional:-->
															<vouc:PO_ID IsChanged="Y"></vouc:PO_ID>
															<!--Optional:-->
															<vouc:LINE_NBR IsChanged="Y"></vouc:LINE_NBR>
															<!--Optional:-->
															<vouc:SCHED_NBR IsChanged="Y"></vouc:SCHED_NBR>
															<!--Optional:-->
															<vouc:DESCR IsChanged="Y">' . \Psi\CStringService::singleton()->htmlspecialchars( $arrmixApDetail['line_item_description'] ) . '</vouc:DESCR>
															<!--Optional:-->
															<vouc:MERCHANDISE_AMT IsChanged="Y">' . $arrmixApDetail['transaction_amount'] . '</vouc:MERCHANDISE_AMT>
															<!--Optional:-->
															<vouc:ITM_SETID IsChanged="Y"></vouc:ITM_SETID>
															<!--Optional:-->
															<vouc:INV_ITEM_ID IsChanged="Y"></vouc:INV_ITEM_ID>
															<!--Optional:-->
															<vouc:QTY_VCHR IsChanged="Y"></vouc:QTY_VCHR>
															<!--Optional:-->
															<vouc:STATISTIC_AMOUNT IsChanged="Y"></vouc:STATISTIC_AMOUNT>
															<!--Optional:-->
															<vouc:UNIT_OF_MEASURE IsChanged="Y"></vouc:UNIT_OF_MEASURE>
															<!--Optional:-->
															<vouc:UNIT_PRICE IsChanged="Y"></vouc:UNIT_PRICE>
															<!--Optional:-->
															<vouc:DSCNT_APPL_FLG IsChanged="Y"></vouc:DSCNT_APPL_FLG>
															<!--Optional:-->
															<vouc:TAX_CD_VAT IsChanged="Y"></vouc:TAX_CD_VAT>
															<!--Optional:-->
															<vouc:BUSINESS_UNIT_RECV IsChanged="Y"></vouc:BUSINESS_UNIT_RECV>
															<!--Optional:-->
															<vouc:RECEIVER_ID IsChanged="Y"></vouc:RECEIVER_ID>
															<!--Optional:-->
															<vouc:RECV_LN_NBR IsChanged="Y"></vouc:RECV_LN_NBR>
															<!--Optional:-->
															<vouc:RECV_SHIP_SEQ_NBR IsChanged="Y"></vouc:RECV_SHIP_SEQ_NBR>
															<!--Optional:-->
															<vouc:MATCH_LINE_OPT IsChanged="Y"></vouc:MATCH_LINE_OPT>
															<!--Optional:-->
															<vouc:DISTRIB_MTHD_FLG IsChanged="Y"></vouc:DISTRIB_MTHD_FLG>
															<!--Optional:-->
															<vouc:SHIPTO_ID IsChanged="Y"></vouc:SHIPTO_ID>
															<!--Optional:-->
															<vouc:SUT_BASE_ID IsChanged="Y"></vouc:SUT_BASE_ID>
															<!--Optional:-->
															<vouc:TAX_CD_SUT IsChanged="Y"></vouc:TAX_CD_SUT>
															<!--Optional:-->
															<vouc:ULTIMATE_USE_CD IsChanged="Y"></vouc:ULTIMATE_USE_CD>
															<!--Optional:-->
															<vouc:SUT_EXCPTN_TYPE IsChanged="Y"></vouc:SUT_EXCPTN_TYPE>
															<!--Optional:-->
															<vouc:SUT_EXCPTN_CERTIF IsChanged="Y"></vouc:SUT_EXCPTN_CERTIF>
															<!--Optional:-->
															<vouc:SUT_APPLICABILITY IsChanged="Y"></vouc:SUT_APPLICABILITY>
															<!--Optional:-->
															<vouc:VAT_APPLICABILITY IsChanged="Y"></vouc:VAT_APPLICABILITY>
															<!--Optional:-->
															<vouc:VAT_TXN_TYPE_CD IsChanged="Y"></vouc:VAT_TXN_TYPE_CD>
															<!--Optional:-->
															<vouc:VAT_USE_ID IsChanged="Y"></vouc:VAT_USE_ID>
															<!--Optional:-->
															<vouc:ADDR_SEQ_NUM_SHIP IsChanged="Y"></vouc:ADDR_SEQ_NUM_SHIP>
															<!--Optional:-->
															<vouc:DESCR254_MIXED IsChanged="Y"></vouc:DESCR254_MIXED>
															<!--Optional:-->
															<vouc:BUSINESS_UNIT_GL IsChanged="Y">99345</vouc:BUSINESS_UNIT_GL>
															<!--Optional:-->
															<vouc:ACCOUNT IsChanged="Y">227700</vouc:ACCOUNT>
															<!--Optional:-->
															<vouc:ALTACCT IsChanged="Y"></vouc:ALTACCT>
															<!--Optional:-->
															<vouc:OPERATING_UNIT IsChanged="Y"></vouc:OPERATING_UNIT>
															<!--Optional:-->
															<vouc:PRODUCT IsChanged="Y"></vouc:PRODUCT>
															<!--Optional:-->
															<vouc:FUND_CODE IsChanged="Y"></vouc:FUND_CODE>
															<!--Optional:-->
															<vouc:CLASS_FLD IsChanged="Y"></vouc:CLASS_FLD>
															<!--Optional:-->
															<vouc:PROGRAM_CODE IsChanged="Y"></vouc:PROGRAM_CODE>
															<!--Optional:-->
															<vouc:BUDGET_REF IsChanged="Y"></vouc:BUDGET_REF>
															<!--Optional:-->
															<vouc:AFFILIATE IsChanged="Y"></vouc:AFFILIATE>
															<!--Optional:-->
															<vouc:AFFILIATE_INTRA1 IsChanged="Y"></vouc:AFFILIATE_INTRA1>
															<!--Optional:-->
															<vouc:AFFILIATE_INTRA2 IsChanged="Y"></vouc:AFFILIATE_INTRA2>
															<!--Optional:-->
															<vouc:CHARTFIELD1 IsChanged="Y"></vouc:CHARTFIELD1>
															<!--Optional:-->
															<vouc:CHARTFIELD2 IsChanged="Y"></vouc:CHARTFIELD2>
															<!--Optional:-->
															<vouc:CHARTFIELD3 IsChanged="Y"></vouc:CHARTFIELD3>
															<!--Optional:-->
															<vouc:DEPTID IsChanged="Y"></vouc:DEPTID>
															<!--Optional:-->
															<vouc:PROJECT_ID IsChanged="Y"></vouc:PROJECT_ID>
															<!--Optional:-->
															<vouc:BUSINESS_UNIT_PC IsChanged="Y"></vouc:BUSINESS_UNIT_PC>
															<!--Optional:-->
															<vouc:ACTIVITY_ID IsChanged="Y"></vouc:ACTIVITY_ID>
															<!--Optional:-->
															<vouc:ANALYSIS_TYPE IsChanged="Y"></vouc:ANALYSIS_TYPE>
															<!--Optional:-->
															<vouc:RESOURCE_TYPE IsChanged="Y"></vouc:RESOURCE_TYPE>
															<!--Optional:-->
															<vouc:RESOURCE_CATEGORY IsChanged="Y"></vouc:RESOURCE_CATEGORY>
															<!--Optional:-->
															<vouc:RESOURCE_SUB_CAT IsChanged="Y"></vouc:RESOURCE_SUB_CAT>
															<!--Optional:-->
															<vouc:TAX_DSCNT_FLG IsChanged="Y"></vouc:TAX_DSCNT_FLG>
															<!--Optional:-->
															<vouc:TAX_FRGHT_FLG IsChanged="Y"></vouc:TAX_FRGHT_FLG>
															<!--Optional:-->
															<vouc:TAX_MISC_FLG IsChanged="Y"></vouc:TAX_MISC_FLG>
															<!--Optional:-->
															<vouc:TAX_VAT_FLG IsChanged="Y"></vouc:TAX_VAT_FLG>
															<!--Optional:-->
															<vouc:PHYSICAL_NATURE IsChanged="Y"></vouc:PHYSICAL_NATURE>
															<!--Optional:-->
															<vouc:VAT_RCRD_INPT_FLG IsChanged="Y"></vouc:VAT_RCRD_INPT_FLG>
															<!--Optional:-->
															<vouc:VAT_RCRD_OUTPT_FLG IsChanged="Y"></vouc:VAT_RCRD_OUTPT_FLG>
															<!--Optional:-->
															<vouc:VAT_TREATMENT IsChanged="Y"></vouc:VAT_TREATMENT>
															<!--Optional:-->
															<vouc:VAT_SVC_SUPPLY_FLG IsChanged="Y"></vouc:VAT_SVC_SUPPLY_FLG>
															<!--Optional:-->
															<vouc:VAT_SERVICE_TYPE IsChanged="Y"></vouc:VAT_SERVICE_TYPE>
															<!--Optional:-->
															<vouc:COUNTRY_LOC_BUYER IsChanged="Y"></vouc:COUNTRY_LOC_BUYER>
															<!--Optional:-->
															<vouc:STATE_LOC_BUYER IsChanged="Y"></vouc:STATE_LOC_BUYER>
															<!--Optional:-->
															<vouc:COUNTRY_LOC_SELLER IsChanged="Y"></vouc:COUNTRY_LOC_SELLER>
															<!--Optional:-->
															<vouc:STATE_LOC_SELLER IsChanged="Y"></vouc:STATE_LOC_SELLER>
															<!--Optional:-->
															<vouc:COUNTRY_VAT_SUPPLY IsChanged="Y"></vouc:COUNTRY_VAT_SUPPLY>
															<!--Optional:-->
															<vouc:STATE_VAT_SUPPLY IsChanged="Y"></vouc:STATE_VAT_SUPPLY>
															<!--Optional:-->
															<vouc:COUNTRY_VAT_PERFRM IsChanged="Y"></vouc:COUNTRY_VAT_PERFRM>
															<!--Optional:-->
															<vouc:STATE_VAT_PERFRM IsChanged="Y"></vouc:STATE_VAT_PERFRM>
															<!--Optional:-->
															<vouc:STATE_SHIP_FROM IsChanged="Y"></vouc:STATE_SHIP_FROM>
															<!--Optional:-->
															<vouc:STATE_VAT_DEFAULT IsChanged="Y"></vouc:STATE_VAT_DEFAULT>
															<!--Optional:-->
															<vouc:REQUESTOR_ID IsChanged="Y"></vouc:REQUESTOR_ID>
															<!--Optional:-->
															<vouc:IST_TXN_FLG IsChanged="Y"></vouc:IST_TXN_FLG>
															<!--Optional:-->
															<vouc:WTHD_SW IsChanged="Y"></vouc:WTHD_SW>
															<!--Optional:-->
															<vouc:WTHD_CD IsChanged="Y"></vouc:WTHD_CD>
															<!--Optional:-->
															<vouc:MFG_ID IsChanged="Y"></vouc:MFG_ID>
															<!--Optional:-->
															<vouc:USER_VCHR_CHAR1 IsChanged="Y"></vouc:USER_VCHR_CHAR1>
															<!--Optional:-->
															<vouc:USER_VCHR_CHAR2 IsChanged="Y"></vouc:USER_VCHR_CHAR2>
															<!--Optional:-->
															<vouc:USER_VCHR_DEC IsChanged="Y"></vouc:USER_VCHR_DEC>
															<!--Optional:-->
															<vouc:USER_VCHR_DATE IsChanged="Y"></vouc:USER_VCHR_DATE>
															<!--Optional:-->
															<vouc:USER_VCHR_NUM1 IsChanged="Y"></vouc:USER_VCHR_NUM1>
															<!--Optional:-->
															<vouc:USER_LINE_CHAR1 IsChanged="Y"></vouc:USER_LINE_CHAR1>
															<!--Optional:-->
															<vouc:USER_SCHED_CHAR1 IsChanged="Y"></vouc:USER_SCHED_CHAR1>
															<vouc:VCHR_DIST_STG class="R">
																<vouc:BUSINESS_UNIT IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT>
																<vouc:VOUCHER_ID IsChanged="Y">NEXT</vouc:VOUCHER_ID>
																<vouc:VOUCHER_LINE_NUM IsChanged="Y">' . $intLineNumber . '</vouc:VOUCHER_LINE_NUM>
																<vouc:DISTRIB_LINE_NUM IsChanged="Y">' . $intDistributionNumber . '</vouc:DISTRIB_LINE_NUM>
																<!--Optional:-->
																<vouc:BUSINESS_UNIT_GL IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT_GL>
																<!--Optional:-->
																<vouc:ACCOUNT IsChanged="Y">' . $arrmixApDetail['account_number'] . '</vouc:ACCOUNT>
																<!--Optional:-->
																<vouc:ALTACCT IsChanged="Y"></vouc:ALTACCT>
																<!--Optional:-->
																<vouc:DEPTID IsChanged="Y"></vouc:DEPTID>
																<!--Optional:-->
																<vouc:STATISTICS_CODE IsChanged="Y"></vouc:STATISTICS_CODE>
																<!--Optional:-->
																<vouc:STATISTIC_AMOUNT IsChanged="Y"></vouc:STATISTIC_AMOUNT>
																<!--Optional:-->
																<vouc:QTY_VCHR IsChanged="Y"></vouc:QTY_VCHR>
																<!--Optional:-->
																<vouc:DESCR IsChanged="Y">' . \Psi\CStringService::singleton()->htmlspecialchars( $arrmixApDetail['line_item_description'] ) . '</vouc:DESCR>
																<!--Optional:-->
																<vouc:MERCHANDISE_AMT IsChanged="Y">' . $arrmixApDetail['transaction_amount'] . '</vouc:MERCHANDISE_AMT>
																<!--Optional:-->
																<vouc:BUSINESS_UNIT_PO IsChanged="Y"></vouc:BUSINESS_UNIT_PO>
																<!--Optional:-->
																<vouc:PO_ID IsChanged="Y"></vouc:PO_ID>
																<!--Optional:-->
																<vouc:LINE_NBR IsChanged="Y"></vouc:LINE_NBR>
																<!--Optional:-->
																<vouc:SCHED_NBR IsChanged="Y"></vouc:SCHED_NBR>
																<!--Optional:-->
																<vouc:PO_DIST_LINE_NUM IsChanged="Y"></vouc:PO_DIST_LINE_NUM>
																<!--Optional:-->
																<vouc:BUSINESS_UNIT_PC IsChanged="Y"></vouc:BUSINESS_UNIT_PC>
																<!--Optional:-->
																<vouc:ACTIVITY_ID IsChanged="Y"></vouc:ACTIVITY_ID>
																<!--Optional:-->
																<vouc:ANALYSIS_TYPE IsChanged="Y"></vouc:ANALYSIS_TYPE>
																<!--Optional:-->
																<vouc:RESOURCE_TYPE IsChanged="Y"></vouc:RESOURCE_TYPE>
																<!--Optional:-->
																<vouc:RESOURCE_CATEGORY IsChanged="Y"></vouc:RESOURCE_CATEGORY>
																<!--Optional:-->
																<vouc:RESOURCE_SUB_CAT IsChanged="Y"></vouc:RESOURCE_SUB_CAT>
																<!--Optional:-->
																<vouc:ASSET_FLG IsChanged="Y"></vouc:ASSET_FLG>
																<!--Optional:-->
																<vouc:BUSINESS_UNIT_AM IsChanged="Y"></vouc:BUSINESS_UNIT_AM>
																<!--Optional:-->
																<vouc:ASSET_ID IsChanged="Y"></vouc:ASSET_ID>
																<!--Optional:-->
																<vouc:PROFILE_ID IsChanged="Y"></vouc:PROFILE_ID>
																<!--Optional:-->
																<vouc:COST_TYPE IsChanged="Y"></vouc:COST_TYPE>
																<!--Optional:-->
																<vouc:VAT_TXN_TYPE_CD IsChanged="Y"></vouc:VAT_TXN_TYPE_CD>
																<!--Optional:-->
																<vouc:RECV_DIST_LINE_NUM IsChanged="Y"></vouc:RECV_DIST_LINE_NUM>
																<!--Optional:-->
																<vouc:OPERATING_UNIT IsChanged="Y"></vouc:OPERATING_UNIT>
																<!--Optional:-->
																<vouc:PRODUCT IsChanged="Y"></vouc:PRODUCT>
																<!--Optional:-->
																<vouc:FUND_CODE IsChanged="Y"></vouc:FUND_CODE>
																<!--Optional:-->
																<vouc:CLASS_FLD IsChanged="Y"></vouc:CLASS_FLD>
																<!--Optional:-->
																<vouc:PROGRAM_CODE IsChanged="Y"></vouc:PROGRAM_CODE>
																<!--Optional:-->
																<vouc:BUDGET_REF IsChanged="Y"></vouc:BUDGET_REF>
																<!--Optional:-->
																<vouc:AFFILIATE IsChanged="Y"></vouc:AFFILIATE>
																<!--Optional:-->
																<vouc:AFFILIATE_INTRA1 IsChanged="Y"></vouc:AFFILIATE_INTRA1>
																<!--Optional:-->
																<vouc:AFFILIATE_INTRA2 IsChanged="Y"></vouc:AFFILIATE_INTRA2>
																<!--Optional:-->
																<vouc:CHARTFIELD1 IsChanged="Y"></vouc:CHARTFIELD1>
																<!--Optional:-->
																<vouc:CHARTFIELD2 IsChanged="Y"></vouc:CHARTFIELD2>
																<!--Optional:-->
																<vouc:CHARTFIELD3 IsChanged="Y"></vouc:CHARTFIELD3>
																<!--Optional:-->
																<vouc:PROJECT_ID IsChanged="Y"></vouc:PROJECT_ID>
																<!--Optional:-->
																<vouc:BUDGET_DT IsChanged="Y"></vouc:BUDGET_DT>
																<!--Optional:-->
																<vouc:ENTRY_EVENT IsChanged="Y"></vouc:ENTRY_EVENT>
																<!--Optional:-->
																<vouc:JRNL_LN_REF IsChanged="Y"></vouc:JRNL_LN_REF>
																<!--Optional:-->
																<vouc:VAT_APORT_CNTRL IsChanged="Y"></vouc:VAT_APORT_CNTRL>
																<!--Optional:-->
																<vouc:USER_VCHR_CHAR1 IsChanged="Y"></vouc:USER_VCHR_CHAR1>
																<!--Optional:-->
																<vouc:USER_VCHR_CHAR2 IsChanged="Y"></vouc:USER_VCHR_CHAR2>
																<!--Optional:-->
																<vouc:USER_VCHR_DEC IsChanged="Y"></vouc:USER_VCHR_DEC>
																<!--Optional:-->
																<vouc:USER_VCHR_DATE IsChanged="Y"></vouc:USER_VCHR_DATE>
																<!--Optional:-->
																<vouc:USER_VCHR_NUM1 IsChanged="Y"></vouc:USER_VCHR_NUM1>
																<!--Optional:-->
																<vouc:USER_DIST_CHAR1 IsChanged="Y"></vouc:USER_DIST_CHAR1>
																<!--Optional:-->
																<vouc:OPEN_ITEM_KEY IsChanged="Y"></vouc:OPEN_ITEM_KEY>
															</vouc:VCHR_DIST_STG>
															<!--Optional:-->
															<vouc:PSCAMA class="R">
																<!--Optional:-->
																<vouc:LANGUAGE_CD IsChanged="Y"></vouc:LANGUAGE_CD>
																<!--Optional:-->
																<vouc:AUDIT_ACTN IsChanged="Y"></vouc:AUDIT_ACTN>
																<!--Optional:-->
																<vouc:BASE_LANGUAGE_CD IsChanged="Y"></vouc:BASE_LANGUAGE_CD>
																<!--Optional:-->
																<vouc:MSG_SEQ_FLG IsChanged="Y"></vouc:MSG_SEQ_FLG>
																<!--Optional:-->
																<vouc:PROCESS_INSTANCE IsChanged="Y"></vouc:PROCESS_INSTANCE>
																<!--Optional:-->
																<vouc:PUBLISH_RULE_ID IsChanged="Y"></vouc:PUBLISH_RULE_ID>
																<!--Optional:-->
																<vouc:MSGNODENAME IsChanged="Y"></vouc:MSGNODENAME>
															</vouc:PSCAMA>
														</vouc:VCHR_LINE_STG>';
				$intLineNumber++;
				$intDistributionNumber++;
			}

			$strPostString .= '
														<!--Optional:-->
														<vouc:PSCAMA class="R">
															<!--Optional:-->
															<vouc:LANGUAGE_CD IsChanged="Y"></vouc:LANGUAGE_CD>
															<!--Optional:-->
															<vouc:AUDIT_ACTN IsChanged="Y"></vouc:AUDIT_ACTN>
															<!--Optional:-->
															<vouc:BASE_LANGUAGE_CD IsChanged="Y"></vouc:BASE_LANGUAGE_CD>
															<!--Optional:-->
															<vouc:MSG_SEQ_FLG IsChanged="Y"></vouc:MSG_SEQ_FLG>
															<!--Optional:-->
															<vouc:PROCESS_INSTANCE IsChanged="Y"></vouc:PROCESS_INSTANCE>
															<!--Optional:-->
															<vouc:PUBLISH_RULE_ID IsChanged="Y"></vouc:PUBLISH_RULE_ID>
															<!--Optional:-->
															<vouc:MSGNODENAME IsChanged="Y"></vouc:MSGNODENAME>
														</vouc:PSCAMA>
													</vouc:VCHR_HDR_STG>
													<vouc:VCHR_PYMT_STG class="R">
														<vouc:BUSINESS_UNIT IsChanged="Y">' . $strApBusinessUnit . '</vouc:BUSINESS_UNIT>
														<vouc:VOUCHER_ID IsChanged="Y">NEXT</vouc:VOUCHER_ID>
														<vouc:PYMNT_CNT IsChanged="Y">1</vouc:PYMNT_CNT>
														<!--Optional:-->
														<vouc:BANK_CD IsChanged="Y">' . $strPeoplesoftBankCode . '</vouc:BANK_CD>
														<!--Optional:-->
														<vouc:BANK_ACCT_KEY IsChanged="Y">' . $strPeoplesoftBankAccount . '</vouc:BANK_ACCT_KEY>
														<!--Optional:-->
														<vouc:PYMNT_METHOD IsChanged="Y">CHK</vouc:PYMNT_METHOD>
														<!--Optional:-->
														<vouc:PYMNT_MESSAGE IsChanged="Y">' . \Psi\CStringService::singleton()->htmlspecialchars( trim( $arrmixApHeader['property_name'] . ' ' . $strPropertyGlBu ) ) . '</vouc:PYMNT_MESSAGE>
														<!--Optional:-->
														<vouc:PYMNT_HANDLING_CD IsChanged="Y">SD</vouc:PYMNT_HANDLING_CD>
														<!--Optional:-->
														<vouc:PYMNT_HOLD IsChanged="Y">N</vouc:PYMNT_HOLD>
														<!--Optional:-->
														<vouc:PYMNT_HOLD_REASON IsChanged="Y"></vouc:PYMNT_HOLD_REASON>
														<!--Optional:-->
														<vouc:MESSAGE_CD IsChanged="Y"></vouc:MESSAGE_CD>
														<!--Optional:-->
														<vouc:PYMNT_GROSS_AMT IsChanged="Y"></vouc:PYMNT_GROSS_AMT>
														<!--Optional:-->
														<vouc:PYMNT_SEPARATE IsChanged="Y"></vouc:PYMNT_SEPARATE>
														<!--Optional:-->
														<vouc:SCHEDULED_PAY_DT IsChanged="Y"></vouc:SCHEDULED_PAY_DT>
														<!--Optional:-->
														<vouc:PYMNT_GROUP_CD IsChanged="Y"></vouc:PYMNT_GROUP_CD>
														<!--Optional:-->
														<vouc:EFT_LAYOUT_CD IsChanged="Y"></vouc:EFT_LAYOUT_CD>
													</vouc:VCHR_PYMT_STG>
													<vouc:PSCAMA class="R">
														<!--Optional:-->
														<vouc:LANGUAGE_CD IsChanged="Y"></vouc:LANGUAGE_CD>
														<!--Optional:-->
														<vouc:AUDIT_ACTN IsChanged="Y">A</vouc:AUDIT_ACTN>
														<!--Optional:-->
														<vouc:BASE_LANGUAGE_CD IsChanged="Y"></vouc:BASE_LANGUAGE_CD>
														<!--Optional:-->
														<vouc:MSG_SEQ_FLG IsChanged="Y"></vouc:MSG_SEQ_FLG>
														<!--Optional:-->
														<vouc:PROCESS_INSTANCE IsChanged="Y"></vouc:PROCESS_INSTANCE>
														<!--Optional:-->
														<vouc:PUBLISH_RULE_ID IsChanged="Y"></vouc:PUBLISH_RULE_ID>
														<!--Optional:-->
														<vouc:MSGNODENAME IsChanged="Y"></vouc:MSGNODENAME>
													</vouc:PSCAMA>
												</vouc:Transaction>
											</vouc:MsgData>
										</vouc:VOUCHER_BUILD>
									</soapenv:Body>
								</soapenv:Envelope>';

			$arrmixHeaders = [
				'Content-Type: text/xml; charset=utf-8',
				'Accept: text/xml',
				'Cache-Control: no-cache',
				'Pragma: no-cache',
				'SOAPAction: "PSI_VOUCHER.v1"',
				'Content-length: ' . strlen( $strPostString )
			];

			$objExternalRequest->setRequestData( $strPostString );
			$objExternalRequest->setHeaderInfo( $arrmixHeaders );
			$strCurlResponse = $objExternalRequest->execute( $boolGetResponse = true );

			$strCurlResponse = ( true == valStr( $strCurlResponse ) ) ? serialize( $strCurlResponse ) : NULL;

			$objTransmission = new CTransmission();
			$objTransmission->setCid( $this->getCid() );
			$objTransmission->setTransmissionTypeId( $objCompanyTranmissionVendor->getTransmissionTypeId() );
			$objTransmission->setTransmissionVendorId( $objCompanyTranmissionVendor->getTransmissionVendorId() );
			$objTransmission->setCompanyTransmissionVendorId( $objCompanyTranmissionVendor->getId() );
			$objTransmission->setPropertyId( $arrmixApHeader['property_id'] );
			$objTransmission->setRequestContent( serialize( $strPostString ) );
			$objTransmission->setResponseContent( $strCurlResponse );
			$objTransmission->setTransmissionDatetime( date( 'm/d/Y H:i:s' ) );

			if( true == valStr( $strCurlResponse ) ) {

				$boolIsValid &= false;
				$objTransmission->setIsFailed( 1 );
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strCurlResponse, NULL ) );
			}

			foreach( $arrmixApHeader['ap_details'] as $arrmixApDetail ) {

				if( true == array_key_exists( $arrmixApDetail['id'], $this->m_arrobjApDetails ) && true == valStr( $strCurlResponse ) ) {

					$this->m_arrobjApDetails[$arrmixApDetail['id']]->setExportedBy( NULL );
					$this->m_arrobjApDetails[$arrmixApDetail['id']]->setExportedOn( NULL );
				} elseif( true == array_key_exists( $arrmixApDetail['id'], $this->m_arrobjApDetails ) && false == valStr( $strCurlResponse ) ) {

					$this->m_arrobjApDetails[$arrmixApDetail['id']]->setExportedBy( $intCompanyUserId );
					$this->m_arrobjApDetails[$arrmixApDetail['id']]->setExportedOn( date( 'm/d/Y H:i:s' ) );
				}
			}

			$arrobjTransmissions[] = $objTransmission;
		}

		unset( $objExternalRequest );

		// Insert transmissions log. Database transaction ( commit and rollaback ) is not used intentionally as we need to insert every success/failed curl request.
		foreach( $arrobjTransmissions as $objTransmission ) {
			$boolIsValid &= $objTransmission->insert( $intCompanyUserId, $objDatabase );
		}

		if( $boolIsValid == false ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to export batch of peoplesoft.', NULL ) );
		}
		return $boolIsValid;

	}

	public function exportInTimberlinePmFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData = NULL ) {
		$arrmixApDetails              = $arrmixExportData['ap_details'];
		$boolIsConsolidatedProperties = $arrmixExportData['is_consolidated_properties'];
		$intPreviousApHeaderId        = 0;

		foreach( $arrmixApDetails as $intPropertyId => $arrmixTempApDetail ) {
			$strTempFileContent = '';
			foreach( $arrmixTempApDetail as $intKey => $arrmixApDetail ) {
				$strFormattedTransactionAmount = str_replace( ',', '', \Psi\CStringService::singleton()->substr( number_format( $arrmixApDetail['transaction_amount'], 2 ), 0, 12 ) );
				$strSubTotalAmount             = str_replace( ',', '', \Psi\CStringService::singleton()->substr( number_format( $arrmixApDetail['subtotal_amount'], 2 ), 0, 12 ) );
				$strPropertyCode               = $arrmixApDetail['property_code'] . '-' . ( $arrmixApDetail['secondary_number'] ? : $arrmixApDetail['account_number'] );

				if( $intPreviousApHeaderId == 0 || $intPreviousApHeaderId != $arrmixApDetail['ap_header_id'] ) {

					$strTempFileContent .= '"APIF","' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['external_id'], 0, 10 ) . '","' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['header_number'], 0, 15 ) . '","' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['header_memo'], 0, 30 ) . '",' . $strFormattedTransactionAmount . ',,,,"' . str_replace( '/', '', $arrmixApDetail['post_date'] ) .
					                       '","' . str_replace( '/', '', $arrmixApDetail['post_date'] ) . '","","' . str_replace( '/', '', $arrmixApDetail['due_date'] ) . '","' . str_replace( '/', '', $arrmixApDetail['post_date'] ) . '","","","","","","","",""' . "\r\n";
					$strTempFileContent .= '"APDF","","","","","","","","","","","' . \Psi\CStringService::singleton()->substr( $strPropertyCode, 0, 10 ) . '","","","","","",' . $strSubTotalAmount . ',"","","","","","","","","","","","","","' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['description'], 0, 30 ) . '","","",';

				} elseif( $intPreviousApHeaderId == $arrmixApDetail['ap_header_id'] ) {
					$strTempFileContent .= '"APDF","","","","","","","","","","","' . \Psi\CStringService::singleton()->substr( $strPropertyCode, 0, 10 ) . '","","","","","",' . $strSubTotalAmount . ',"","","","","","","","","","","","","","' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['description'], 0, 30 ) . '","","",';
				}

				$strTempFileContent    .= "\r\n";
				$intPreviousApHeaderId = $arrmixApDetail['ap_header_id'];
			}

			$this->m_arrstrFileContent[] = $strTempFileContent;
			$this->m_arrstrFileName[]    = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_timberline_ap_export_batch_no_' . $this->getId() . '_property_id_ ' . ( int ) $intPropertyId . '.txt';
		}

		if( false != $arrmixExportData['show_ftp'] ) {
			$intCid = $arrmixExportData['cid'];
			foreach( $this->m_arrstrFileContent as $intIndex => $strFileContent ) {
				$arrmixExportData['file_name']      = $this->m_arrstrFileName[$intIndex];
				$arrmixExportData['file_content']   = $strFileContent;
				$arrmixExportData['file_extension'] = self::FILE_EXTENSION_IIF;

				$boolIsSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $intCid );
				if( false == $boolIsSuccess ) {
					break;
				}
			}

			return $boolIsSuccess;
		}

		$this->m_intFileCount = \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContent );
		if( true == $boolIsConsolidatedProperties ) {
			$this->m_strFileContent = implode( "\r\n", $this->m_arrstrFileContent );
			$this->m_strFileName    = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_timberline_ap_export_batch_no_' . $this->getId() . '.txt';
			$this->m_intFileCount   = 1;
		} elseif( 1 == $this->m_intFileCount ) {
			$this->m_strFileName    = $this->m_arrstrFileName[0];
			$this->m_strFileContent = $this->m_arrstrFileContent[0];
		}

		if( true == $boolIsScript ) {
			$arrmixFileData = 1 == $this->m_intFileCount ? [ 'fileName' => $this->m_strFileName, 'fileContent' => $this->m_strFileContent ] : [ 'fileName' => $this->m_arrstrFileName, 'fileContent' => $this->m_arrstrFileContent ];

			return $arrmixFileData;
		}

		return $this->download();
	}

	public function exportInYardiFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData = NULL ) {
		$arrmixApDetails              = $arrmixExportData['ap_details'];
		$objDatabase                  = $arrmixExportData['db_obj'];
		$boolIsConsolidatedProperties = $arrmixExportData['is_consolidated_properties'];

		$arrstrFileContentFinPayableColumns = [
			[
				'FinPayables'
			],
			[
				'TRANNUM', 'PERSON', 'OFFSET', 'ACCRUAL', 'POSTMONTH', 'DATE', 'DUEDATE', 'AMOUNT', 'PROPERTY', 'ACCOUNT', 'NOTES', 'REF', 'USERDEFINEDFIELD4', 'CHECKNUM', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3', 'SEGMENT4',
				'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'DESC', 'EXPENSETYPE', 'DETAILTAXAMOUNT1', 'DETAILTAXAMOUNT2', 'DETAILTRANAMOUNT', 'DETAILVATTRANTYPEID',
				'DETAILVATRATEID', 'TRANCURRENCYID', 'EXCHANGERATE', 'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE', 'DOCUMENTSEQUENCENUMBER', 'DISPLAYTYPE', 'Company',
				'FundingEntity', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2', 'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4', 'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7',
				'USERDEFINEDFIELD8', 'USERDEFINEDFIELD9', 'USERDEFINEDFIELD10', 'INTERNATIONALPAYMENTTYPE', 'WORKFLOW', 'WORKFLOWSTATUS', 'WORKFLOWSTEP', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3', 'DETAILFIELD4',
				'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8', 'NOTES2', 'PONUM', 'PODETAILID', 'TRANDATE', 'ISCONSOLIDATECHECKS', 'REMITTANCEVENDOR', 'RETENTION', 'ORIGINALUREF'
			]
		];
		$arrstrFileContentFinVendorColumns  = [
			[
				'FinVendors'
			],
			[
				'Vendor_Code', 'Currency', 'Tax_Authority', 'Ext_Ref_Vendor_Id', 'Last_Name', 'First_Name', 'Salutation', 'Address1', 'Address2', 'Address3', 'Address4', 'City', 'State', 'ZipCode', 'Country',
				'Phone_Number_1', 'Phone_Number_2', 'Phone_Number_3', 'Phone_Number_4', 'Phone_Number_5', 'Phone_Number_6', 'Phone_Number_7', 'Phone_Number_8', 'Phone_Number_9', 'Phone_Number_10', 'Government_ID',
				'Government_Name', 'Email', 'Alternate_Email', 'Workers_Comp_Expiration_Date', 'Liability_Expiration_Date', 'Contact', 'Is_Contractor', 'Is_InActive', 'InActive_Date', 'Is_Require_Contract',
				'Vendor_Status', 'User_Defined_Field1', 'User_Defined_Field2', 'User_Defined_Field3', 'User_Defined_Field4', 'User_Defined_Field5', 'User_Defined_Field6', 'User_Defined_Field7', 'User_Defined_Field8',
				'User_Defined_Field9', 'User_Defined_Field10', 'User_Defined_Field11', 'User_Defined_Field12', 'Gets', 'Usual_Account_Code', 'Sales_Tax', 'Tax_Registered', 'Tax_Registered_Number', 'Domestic_Tax_Tran_Type',
				'Cross_Border_Tax_Tran_Type', 'Tax_Point', 'PST_Exempt', 'Notes', 'Consolidate', 'Cheque_Memo_From_Invoice', 'Hold_Payments', 'EFT', 'No_Signature', 'On_Cheques_Over', 'Memo', 'PO_Required',
				'Discount_Percent', 'Discount_Day', 'Payment_Terms', 'Days_From_Invoice_Or_Month', 'Employee', 'Vendor_Priority', 'Language'
			]
		];

		$this->m_intFileCount                 = 1;
		$arrstrFileContentFinPayable          = [];
		$arrstrFileContentFinVendor           = [];
		$arrmixRefundsExportedInLastSixMonths = [];
		$arrobjCountriesByCountryCode         = \Psi\Eos\Entrata\CCountries::createService()->fetchAllCountries( $objDatabase );

		foreach( $arrmixApDetails as $intPropertyId => $arrmixTempApDetail ) {
			// remove the line-items which are already present in the batch
			$arrintExcludeResidentIds                             = getArrayFieldValuesByFieldName( 'resident_id', $arrmixApDetails[$intPropertyId] );
			$arrmixRefundsExportedInLastSixMonths[$intPropertyId] = CApExportBatches::fetchRefundsExportedInLastSixMonthsByPropertyIdByCid( $intPropertyId, $arrmixExportData['client_id'], [ CApExportBatchType::YARDI ], $objDatabase, $arrintExcludeResidentIds );

			// rekey according to resident_id to avoid repeated entries for same resident and remove entries which are already present in the batch
			if( false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
				$arrmixRefundsExportedInLastSixMonths[$intPropertyId] = rekeyArray( 'resident_id', $arrmixRefundsExportedInLastSixMonths[$intPropertyId] );
			}

			$strPropertyName           = $arrmixTempApDetail[0]['property_name'];
			$arrintRenderedResidentIds = [];
			foreach( $arrmixTempApDetail as $intKey => $arrmixApDetail ) {
				$strVendorCode = 'E' . $arrmixApDetail['ap_payee_id'];
				if( false == is_null( $arrmixApDetail['resident_id'] ) ) {
					$strVendorCode = 'E' . $arrmixApDetail['resident_id'];
				}

				$strExpenseType = 'Expense';
				if( false == is_null( $arrmixApDetail['expense_type'] ) ) {
					$strExpenseType = $arrmixApDetail['expense_type'];
				}

				$strCountryCode = '';
				if( isset( $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']] ) ) {
					$strCountryCode = $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']]->getCodeThreeDigit();
					if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixApDetail['country_format'] ) {
						$strCountryCode = $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']]->getName();
					} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixApDetail['country_format'] ) {
						$strCountryCode = $arrmixApDetail['country_code'];
					}
				}

				$arrstrFileContentFinPayable[] = [
					$arrmixApDetail['ap_header_id'], $strVendorCode, $arrmixApDetail['cash_account_number'], $arrmixApDetail['accrual_account_number'], date( 'm/d/Y', strtotime( $arrmixApDetail['post_month'] ) ),
					date( 'm/d/Y', strtotime( $arrmixApDetail['transaction_datetime'] ) ), $arrmixApDetail['due_date'], $arrmixApDetail['subtotal_amount'], '"' . $arrmixApDetail['lookup_code'] . '"',
					$arrmixApDetail['account_number'], '"' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['note'], 0, 255 ) . '"', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '"' . $strExpenseType . '"', '', '',
					'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Stub Check', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
					'', '', '', '', '', '', '', '', '1', '', '', ''
				];

				if( false != isset( $arrmixApDetail['lease_customer_id'] ) && false != isset( $arrmixApDetail['refund_ar_transaction_id'] ) && false == in_array( $arrmixApDetail['resident_id'], $arrintRenderedResidentIds ) ) {
					$this->m_intFileCount         = 2;
					$arrintRenderedResidentIds[]  = $arrmixApDetail['resident_id'];
					$arrstrFileContentFinVendor[] = [
						$strVendorCode, '', '', $arrmixApDetail['resident_id'], '"' . $arrmixApDetail['name_last'] . '"', '"' . $arrmixApDetail['name_first'] . '"', '', '"' . $arrmixApDetail['street_line1'] . '"',
						'"' . $arrmixApDetail['street_line2'] . '"', '', '', '"' . $arrmixApDetail['city'] . '"', '"' . $arrmixApDetail['state_code'] . '"', $arrmixApDetail['postal_code'], $strCountryCode,
						$arrmixApDetail['phone_number'], '', '', '', '', '', '', '', '', '', '', '', $arrmixApDetail['email_address'], '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '',
						'', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}
			}

			if( false == $boolIsConsolidatedProperties ) {
				$this->m_arrstrFileName[]    = 'finpayble_' . date( 'Y_m_d_His' ) . '_' . $strPropertyName . '_' . $arrmixExportData['export_batch_id'] . '.csv';
				$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinPayableColumns, $arrstrFileContentFinPayable );

				if( 2 == $this->m_intFileCount || false != valArr( $arrmixRefundsExportedInLastSixMonths[$intPropertyId] ) ) {
					// Include vendors information in finvendors file for the refunds which were exported in last 6 months and after that the forwarding address of it was changed for this property
					if( false != valArr( $arrmixRefundsExportedInLastSixMonths[$intPropertyId] ) ) {
						foreach( $arrmixRefundsExportedInLastSixMonths as $intPropertyId => $arrmixRefundExportedInLastSixMonths ) {
							foreach( $arrmixRefundExportedInLastSixMonths as $arrmixPropertyRefund ) {
								$strCountryCode = '';
								if( isset( $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']] ) ) {
									$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getCodeThreeDigit();
									if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixPropertyRefund['country_format'] ) {
										$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getName();
									} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixPropertyRefund['country_format'] ) {
										$strCountryCode = $arrmixPropertyRefund['country_code'];
									}
								}

								$arrstrFileContentFinVendor[] = [
									'E' . $arrmixPropertyRefund['vendor_code'], '', '', $arrmixPropertyRefund['resident_id'], '"' . $arrmixPropertyRefund['name_last'] . '"', '"' . $arrmixPropertyRefund['name_first'] . '"',
									' ', '"' . $arrmixPropertyRefund['street_line1'] . '"', '"' . $arrmixPropertyRefund['street_line2'] . '"', '', '', '"' . $arrmixPropertyRefund['city'] . '"',
									'"' . $arrmixPropertyRefund['state_code'] . '"', $arrmixPropertyRefund['postal_code'], $strCountryCode, $arrmixPropertyRefund['phone_number'], '', '', '', '', '', '', '', '',
									' ', '', '', $arrmixPropertyRefund['email_address'], '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
									' ', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
								];
							}
						}
					}

					$this->m_arrstrFileName[]    = 'finvendor_' . date( 'Y_m_d_His' ) . '_' . $strPropertyName . '_' . $arrmixExportData['export_batch_id'] . '.csv';
					$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinVendorColumns, $arrstrFileContentFinVendor );
				}

				// if the properties are not consolidated then reset the last transactions array each time
				$arrmixRefundsExportedInLastSixMonths = [];
				$arrstrFileContentFinPayable          = [];
				$arrstrFileContentFinVendor           = [];
			}
		}

		if( false != $boolIsConsolidatedProperties ) {
			$this->m_arrstrFileName[]    = 'finpayable_' . date( 'Y_m_d_His' ) . '_' . $arrmixExportData['company_name'] . '_' . $arrmixExportData['export_batch_id'] . '.csv';
			$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinPayableColumns, $arrstrFileContentFinPayable );

			if( 2 == $this->m_intFileCount || false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
				// Include vendors information in finvendors file for the refunds which were exported in last 6 months and after that the forwarding address of it was changed.
				if( false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
					foreach( $arrmixRefundsExportedInLastSixMonths as $intPropertyId => $arrmixRefundExportedInLastSixMonths ) {
						foreach( $arrmixRefundExportedInLastSixMonths as $arrmixPropertyRefund ) {
							$strCountryCode = '';
							if( false != valArr( $arrobjCountriesByCountryCode ) && isset( $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']] ) ) {
								$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getCodeThreeDigit();
								if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixPropertyRefund['country_format'] ) {
									$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getName();
								} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixPropertyRefund['country_format'] ) {
									$strCountryCode = $arrmixPropertyRefund['country_code'];
								}
							}

							$arrstrFileContentFinVendor[] = [
								'E' . $arrmixPropertyRefund['vendor_code'], '', '', $arrmixPropertyRefund['resident_id'], '"' . $arrmixPropertyRefund['name_last'] . '"',
								'"' . $arrmixPropertyRefund['name_first'] . '"', '', '"' . $arrmixPropertyRefund['street_line1'] . '"', '"' . $arrmixPropertyRefund['street_line2'] . '"', '', '',
								'"' . $arrmixPropertyRefund['city'] . '"', '"' . $arrmixPropertyRefund['state_code'] . '"', $arrmixPropertyRefund['postal_code'], $strCountryCode,
								$arrmixPropertyRefund['phone_number'], '', '', '', '', '', '', '', '', '', '', '', $arrmixPropertyRefund['email_address'], '', '', '', '', '', '',
								' ', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '',
								' ', '', '', '', '', '', '', '', '', ''
							];
						}
					}
				}

				if( false != valArr( $arrstrFileContentFinVendor ) ) {
					$this->m_arrstrFileName[]    = 'finvendor_' . date( 'Y_m_d_His' ) . '_' . strtotime( $arrmixApDetail['transaction_datetime'] ) . '_' . $arrmixExportData['company_name'] . '_' . $arrmixExportData['export_batch_id'] . '.csv';
					$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinVendorColumns, $arrstrFileContentFinVendor );
				} else {
					// if single property is selected but is consolidated was set true and if finVendor data is not available, then we should create only finPayable file
					$this->m_intFileCount = 1;
				}
			}
		}

		if( true == $arrmixExportData['show_ftp'] ) {
			$intCid = $arrmixExportData['cid'];
			foreach( $this->m_arrstrFileName as $intIndex => $strFileName ) {
				$arrmixExportData['file_name']      = $this->m_arrstrFileName[$intIndex];
				$arrmixExportData['file_content']   = $this->m_arrstrFileContent[$intIndex];
				$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;

				$boolIsSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $intCid );
				if( false == $boolIsSuccess ) {
					break;
				}
			}

			return $boolIsSuccess;
		}

		$arrstrFileContent = [];
		foreach( $this->m_arrstrFileContent as $arrstrFileContentTemp ) {
			$strFileContent = '';
			foreach( $arrstrFileContentTemp as $arrstrContent ) {
				$strFileContent .= implode( ',', $arrstrContent );
				$strFileContent .= "\r\n";
			}
			$arrstrFileContent[] = $strFileContent;
		}
		$this->m_arrstrFileContent = $arrstrFileContent;

		if( 1 == $this->m_intFileCount ) {
			$this->m_strFileName = $this->m_arrstrFileName[0];
			$this->m_strFileContent = $arrstrFileContent[0];
		}

		if( true == $boolIsScript ) {
			$arrmixFileData = 1 == $this->m_intFileCount ? [ 'fileName' => $this->m_strFileName, 'fileContent' => $this->m_strFileContent ] : [ 'fileName' => $this->m_arrstrFileName, 'fileContent' => $this->m_arrstrFileContent ];

			return $arrmixFileData;
		}

		return $this->download();
	}

	public function exportInYardiEtlInvoiceRegisterFormat( $arrmixExportData, $boolIsScript = false, $arrmixConnectionData = NULL ) {
		$arrmixApDetails              = $arrmixExportData['ap_details'];
		$objDatabase                  = $arrmixExportData['db_obj'];
		$boolIsConsolidatedProperties = $arrmixExportData['is_consolidated_properties'];

		if( CApExportBatchType::YARDI_ETL_FINPAYABLES == $this->getApExportBatchTypeId() ) {
			$strFinFileNamePrefix               = 'Yardi_ETL_FinPayables_';
			$arrstrFileContentFinPayableColumns = [
				[
					'FinPayables'
				],
				[
					'TRANNUM', 'PERSON', 'OFFSET', 'ACCRUAL', 'POSTMONTH', 'DATE', 'DUEDATE', 'AMOUNT', 'PROPERTY', 'REF_PROPERTY_ID', 'ACCOUNT', 'NOTES', 'REF', 'USERDEFINEDFIELD4', 'CHECKNUM', 'SEGMENT1', 'SEGMENT2',
					'SEGMENT3', 'SEGMENT4', 'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'DetailNotes', 'EXPENSETYPE', 'DETAILTAXAMOUNT1', 'DETAILTAXAMOUNT2',
					'DETAILTRANAMOUNT', 'DETAILVATTRANTYPEID', 'DETAILVATRATEID', 'TRANCURRENCYID', 'EXCHANGERATE', 'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE',
					'DOCUMENTSEQUENCENUMBER', 'DISPLAYTYPE', 'Company', 'FundingEntity', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2', 'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4',
					'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'USERDEFINEDFIELD9', 'USERDEFINEDFIELD10', 'INTERNATIONALPAYMENTTYPE', 'WORKFLOW', 'WORKFLOWSTATUS',
					'WORKFLOWSTEP', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3', 'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8', 'NOTES2', 'PONUM','PODETAILID', 'TRANDATE',
					'ISCONSOLIDATECHECKS', 'REMITTANCEVENDOR', 'ADJUSTMENT', 'RETENTION', 'ORIGINALUREF', 'CREDITMEMO', 'ADJUSTMENT'
				]
			];
		} else {
			$strFinFileNamePrefix               = 'finpayble_';
			$arrstrFileContentFinPayableColumns = [
				[
					'FinInvoiceRegisters'
				],
				[
					'TRANNUM', 'PERSON', 'OFFSET', 'ACCRUAL', 'POSTMONTH', 'DATE', 'DUEDATE', 'AMOUNT', 'PROPERTY', 'ACCOUNT', 'NOTES', 'REF', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3', 'SEGMENT4', 'SEGMENT5', 'SEGMENT6',
					'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'TAXAMOUNT1', 'TAXAMOUNT2', 'EXCHANGERATE', 'EXCHANGERATEDATE', 'FROMDATE', 'TODATE', 'EXPENSETYPE', 'DETAILNOTES',
					'PONUM', 'PODETAILID', 'DISPLAYTYPE', 'Company', 'FundingEntity', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2', 'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4',
					'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'USERDEFINEDFIELD9', 'USERDEFINEDFIELD10', 'WORKFLOW', 'WORKFLOWSTATUS', 'WORKFLOWSTEP', 'DETAILFIELD1',
					'DETAILFIELD2', 'DETAILFIELD3', 'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8', 'ISCONSOLIDATECHECKS'
				]
			];
		}

		$arrstrFileContentFinVendorColumns = [
			[
				'FinVendors'
			],
			[
				'Vendor_Code', 'Currency', 'Tax_Authority', 'Ext_Ref_Vendor_Id', 'Last_Name', 'First_Name', 'Salutation', 'Address1', 'Address2', 'Address3', 'Address4', 'City', 'State', 'ZipCode', 'Country',
				'Phone_Number_1', 'Phone_Number_2', 'Phone_Number_3', 'Phone_Number_4', 'Phone_Number_5', 'Phone_Number_6', 'Phone_Number_7', 'Phone_Number_8', 'Phone_Number_9', 'Phone_Number_10', 'Government_ID',
				'Government_Name', 'Email', 'Alternate_Email', 'Workers_Comp_Expiration_Date', 'Liability_Expiration_Date', 'Contact', 'Is_Contractor', 'Is_InActive', 'InActive_Date', 'Is_Require_Contract', 'Vendor_Status',
				'User_Defined_Field1', 'User_Defined_Field2', 'User_Defined_Field3', 'User_Defined_Field4', 'User_Defined_Field5', 'User_Defined_Field6', 'User_Defined_Field7', 'User_Defined_Field8', 'User_Defined_Field9',
				'User_Defined_Field10', 'User_Defined_Field11', 'User_Defined_Field12', 'Gets', 'Usual_Account_Code', 'Sales_Tax', 'Tax_Registered', 'Tax_Registered_Number', 'Domestic_Tax_Tran_Type', 'Cross_Border_Tax_Tran_Type',
				'Tax_Point', 'PST_Exempt', 'Notes', 'Consolidate', 'Cheque_Memo_From_Invoice', 'Hold_Payments', 'EFT', 'No_Signature', 'On_Cheques_Over', 'Memo', 'PO_Required', 'Discount_Percent', 'Discount_Day', 'Payment_Terms',
				'Days_From_Invoice_Or_Month', 'Employee', 'Vendor_Priority', 'Language'
			]
		];

		$this->m_intFileCount                 = 1;
		$arrstrFileContentFinPayable          = [];
		$arrstrFileContentFinVendor           = [];
		$arrmixRefundsExportedInLastSixMonths = [];
		$arrobjCountriesByCountryCode         = \Psi\Eos\Entrata\CCountries::createService()->fetchAllCountries( $objDatabase );

		$arrintApHeaderIds = [];
		foreach( $arrmixApDetails as $intPropertyId => $arrmixTempApDetail ) {
			foreach( $arrmixTempApDetail as $intKey => $arrmixApDetail ) {
				$arrintApHeaderIds[$arrmixApDetail['ap_header_id']] = $arrmixApDetail['ap_header_id'];
			}
		}
		$arrmixApHeaders = \Psi\Eos\Entrata\CApHeaderLeaseCustomers::createService()->fetchApHeaderLeaseCustomersByApHeaderIdsByCid( $arrintApHeaderIds, $objDatabase->getCid(), $objDatabase );
		$arrmixApHeaders = rekeyArray( 'ap_header_id', $arrmixApHeaders );

		foreach( $arrmixApDetails as $intPropertyId => $arrmixTempApDetail ) {
			// remove the line-items which are already present in the batch
			$arrintExcludeResidentIds                             = getArrayFieldValuesByFieldName( 'resident_id', $arrmixApDetails[$intPropertyId] );
			$arrmixRefundsExportedInLastSixMonths[$intPropertyId] = CApExportBatches::fetchRefundsExportedInLastSixMonthsByPropertyIdByCid( $intPropertyId, $arrmixExportData['client_id'], [ CApExportBatchType::YARDI_ETL_INVOICE_REGISTER ], $objDatabase, $arrintExcludeResidentIds );

			// rekey according to resident_id to avoid repeated entries for same resident and remove entries which are already present in the batch
			if( false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
				$arrmixRefundsExportedInLastSixMonths[$intPropertyId] = rekeyArray( 'resident_id', $arrmixRefundsExportedInLastSixMonths[$intPropertyId] );
			}

			$strPropertyName           = $arrmixTempApDetail[0]['property_name'];
			$arrintRenderedResidentIds = [];
			foreach( $arrmixTempApDetail as $intKey => $arrmixApDetail ) {
				$strVendorCode = $arrmixApDetail['ap_payee_id'];
				if( false == is_null( $arrmixApDetail['resident_id'] ) ) {
					$strVendorCode = $arrmixApDetail['resident_id'];
				}

				$strExpenseType = 'Expense';
				if( false == is_null( $arrmixApDetail['expense_type'] ) ) {
					$strExpenseType = $arrmixApDetail['expense_type'];
				}

				$strCountryCode = '';
				if( isset( $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']] ) ) {
					$strCountryCode = $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']]->getCodeThreeDigit();
					if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixApDetail['country_format'] ) {
						$strCountryCode = $arrobjCountriesByCountryCode[$arrmixApDetail['country_code']]->getName();
					} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixApDetail['country_format'] ) {
						$strCountryCode = $arrmixApDetail['country_code'];
					}
				}

				if( CApExportBatchType::YARDI_ETL_FINPAYABLES == $this->getApExportBatchTypeId() ) {
					$strVendorCode                 = 'e' . $strVendorCode;
					$strApHeaderIdTemp             = '3' . $arrmixApDetail['ap_header_id'];
					$arrstrFileContentFinPayable[] = [
						$strApHeaderIdTemp, $strVendorCode, $arrmixApDetail['cash_account_number'], $arrmixApDetail['accrual_account_number'], date( 'm/d/Y', strtotime( $arrmixApDetail['post_month'] ) ),
						date( 'm/d/Y', strtotime( $arrmixApDetail['transaction_datetime'] ) ), $arrmixApDetail['due_date'], $arrmixApDetail['subtotal_amount'], '"' . $arrmixApDetail['lookup_code'] . '"',
						'"' . $arrmixApDetail['property_id'] . '"', $arrmixApDetail['account_number'], '"' . str_replace( ',', ';', \Psi\CStringService::singleton()->substr( $arrmixApDetail['note'], 0, 255 ) ) . '"', $arrmixApDetail['ap_header_id'], '', '', '',
						' ', '', '', '', '', '', '', '', '', '', '', '', '"' . $strExpenseType . '"', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
						' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TRUE', '', '', '', '', '0', '0'
					];
				} else {
					$arrstrFileContentFinPayable[] = [
						$arrmixApDetail['ap_header_id'], $strVendorCode, $arrmixApDetail['cash_account_number'], $arrmixApDetail['accrual_account_number'], date( 'm/d/Y', strtotime( $arrmixApDetail['post_month'] ) ),
						date( 'm/d/Y', strtotime( $arrmixApDetail['transaction_datetime'] ) ), $arrmixApDetail['due_date'], $arrmixApDetail['subtotal_amount'], '"' . $arrmixApDetail['lookup_code'] . '"',
						$arrmixApDetail['account_number'], '"' . \Psi\CStringService::singleton()->substr( $arrmixApDetail['note'], 0, 255 ) . '"', $arrmixApDetail['ap_header_id'], '', '', '', '', '', '', '', '', '', '', '',
						' ', '', '', '', '', '', '', '"' . $strExpenseType . '"', 'SECURITY DEPOSIT REFUND', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
						' ', '', '', '', '', '', '', '', '', '', '1'
					];
				}

				if( false != isset( $arrmixApDetail['lease_customer_id'] ) && false != isset( $arrmixApDetail['refund_ar_transaction_id'] ) && false == in_array( $arrmixApDetail['resident_id'], $arrintRenderedResidentIds ) ) {
					$this->m_intFileCount        = 2;
					$arrintRenderedResidentIds[] = $arrmixApDetail['resident_id'];

					$strVendorNames = '';
					if( false != isset( $arrmixApDetail['refund_customer_names'] ) ) {
						if( false != isset( $arrmixApHeaders[$arrmixApDetail['ap_header_id']] ) && $arrmixApHeaders[$arrmixApDetail['ap_header_id']]['count'] > 1 ) {
							$strVendorNames = str_replace( ', ', ' & ', $arrmixApDetail['refund_customer_names'] );
						} else {
							$strVendorNames = str_replace( ', ', ' ', $arrmixApDetail['refund_customer_names'] );
						}
					}

					$arrstrFileContentFinVendor[] = [
						$strVendorCode, '', '', $arrmixApDetail['resident_id'], '"' . $strVendorNames . '"', '', '', '"' . $arrmixApDetail['street_line1'] . '"',
						'"' . $arrmixApDetail['street_line2'] . '"', '', '', '"' . $arrmixApDetail['city'] . '"', '"' . $arrmixApDetail['state_code'] . '"',
						$arrmixApDetail['postal_code'], $strCountryCode, $arrmixApDetail['phone_number'], '', '', '', '', '', '', '', '', '', '', '',
						$arrmixApDetail['email_address'], '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '',
						' ', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}
			}
			if( false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
				foreach( $arrmixRefundsExportedInLastSixMonths as $arrmixTempRefundsExportedInLastSixMonths ) {
					foreach( $arrmixTempRefundsExportedInLastSixMonths as $arrmixApDetail ) {
						$arrmixApHeadersForLastSixMonths[$arrmixApDetail['ap_header_id']] = $arrmixApDetail['ap_header_id'];
					}
				}
			}

			$arrmixApHeadersForLastSixMonths = \Psi\Eos\Entrata\CApHeaderLeaseCustomers::createService()->fetchApHeaderLeaseCustomersByApHeaderIdsByCid( $arrmixApHeadersForLastSixMonths, $this->getCid(), $objDatabase );
			$arrmixApHeadersForLastSixMonths = rekeyArray( 'ap_header_id', $arrmixApHeadersForLastSixMonths );

			if( false == $boolIsConsolidatedProperties ) {
				$this->m_arrstrFileName[]    = $strFinFileNamePrefix . date( 'Y_m_d_His' ) . '_' . $strPropertyName . '_' . $arrmixExportData['export_batch_id'] . '.csv';
				$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinPayableColumns, $arrstrFileContentFinPayable );

				if( 2 == $this->m_intFileCount || false != valArr( $arrmixRefundsExportedInLastSixMonths[$intPropertyId] ) ) {
					// Include vendors information in finvendors file for the refunds which were exported in last 6 months and after that the forwarding address of it was changed for this property
					if( false != valArr( $arrmixRefundsExportedInLastSixMonths[$intPropertyId] ) ) {
						$this->m_intFileCount = 2;
						foreach( $arrmixRefundsExportedInLastSixMonths as $intPropertyId => $arrmixRefundExportedInLastSixMonths ) {
							foreach( $arrmixRefundExportedInLastSixMonths as $arrmixPropertyRefund ) {
								$strCountryCode = '';
								if( isset( $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']] ) ) {
									$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getCodeThreeDigit();
									if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixPropertyRefund['country_format'] ) {
										$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getName();
									} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixPropertyRefund['country_format'] ) {
										$strCountryCode = $arrmixPropertyRefund['country_code'];
									}
								}

								if( false != isset( $arrmixPropertyRefund['refund_customer_names'] ) ) {
									if( false != isset( $arrmixApHeadersForLastSixMonths[$arrmixPropertyRefund['ap_header_id']] ) && $arrmixApHeadersForLastSixMonths[$arrmixPropertyRefund['ap_header_id']]['count'] > 1 ) {
										$strVendorNames = str_replace( ', ', ' & ', $arrmixPropertyRefund['refund_customer_names'] );
									} else {
										$strVendorNames = str_replace( ', ', ' ', $arrmixPropertyRefund['refund_customer_names'] );
									}
								}

								$arrstrFileContentFinVendor[] = [
									$arrmixPropertyRefund['vendor_code'], '', '', $arrmixPropertyRefund['resident_id'], '"' . $strVendorNames . '"', '', '', '"' . $arrmixPropertyRefund['street_line1'] . '"',
									'"' . $arrmixPropertyRefund['street_line2'] . '"', '', '', '"' . $arrmixPropertyRefund['city'] . '"', '"' . $arrmixPropertyRefund['state_code'] . '"',
									$arrmixPropertyRefund['postal_code'], $strCountryCode, $arrmixPropertyRefund['phone_number'], '', '', '', '', '', '', '', '', '', '', '',
									$arrmixPropertyRefund['email_address'], '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
									' ', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
								];
							}
						}
					}

					$this->m_arrstrFileName[]    = 'finvendor_' . date( 'Y_m_d_His' ) . '_' . $strPropertyName . '_' . $arrmixExportData['export_batch_id'] . '.csv';
					$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinVendorColumns, $arrstrFileContentFinVendor );
				}

				// if the properties are not consolidated then reset the last transactions array each time
				$arrmixRefundsExportedInLastSixMonths = [];
				$arrstrFileContentFinPayable          = [];
				$arrstrFileContentFinVendor           = [];
			}
		}

		if( false != $boolIsConsolidatedProperties ) {
			$this->m_arrstrFileName[]    = $strFinFileNamePrefix . date( 'Y_m_d_His' ) . '_' . $arrmixExportData['company_name'] . '_' . $arrmixExportData['export_batch_id'] . '.csv';
			$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinPayableColumns, $arrstrFileContentFinPayable );

			if( 2 == $this->m_intFileCount || false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
				// Include vendors information in finvendors file for the refunds which were exported in last 6 months and after that the forwading address of it was changed
				if( false != valArr( $arrmixRefundsExportedInLastSixMonths ) ) {
					$this->m_intFileCount = 2;
					foreach( $arrmixRefundsExportedInLastSixMonths as $intPropertyId => $arrmixRefundExportedInLastSixMonths ) {
						foreach( $arrmixRefundExportedInLastSixMonths as $arrmixPropertyRefund ) {
							$strCountryCode = '';
							if( false != valArr( $arrobjCountriesByCountryCode ) && isset( $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']] ) ) {
								$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getCodeThreeDigit();
								if( self::COUNTRY_FORMAT_COUNTRY_NAME == $arrmixPropertyRefund['country_format'] ) {
									$strCountryCode = $arrobjCountriesByCountryCode[$arrmixPropertyRefund['country_code']]->getName();
								} elseif( self::COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE == $arrmixPropertyRefund['country_format'] ) {
									$strCountryCode = $arrmixPropertyRefund['country_code'];
								}
							}

							if( false != isset( $arrmixPropertyRefund['refund_customer_names'] ) ) {
								if( false != isset( $arrmixApHeadersForLastSixMonths[$arrmixPropertyRefund['ap_header_id']] ) && $arrmixApHeadersForLastSixMonths[$arrmixPropertyRefund['ap_header_id']]['count'] > 1 ) {
									$strVendorNames = str_replace( ', ', ' & ', $arrmixPropertyRefund['refund_customer_names'] );
								} else {
									$strVendorNames = str_replace( ', ', ' ', $arrmixPropertyRefund['refund_customer_names'] );
								}
							}

							$arrstrFileContentFinVendor[] = [
								$arrmixPropertyRefund['vendor_code'], '', '', $arrmixPropertyRefund['resident_id'], '"' . $strVendorNames . '"', '', '', '"' . $arrmixPropertyRefund['street_line1'] . '"',
								'"' . $arrmixPropertyRefund['street_line2'] . '"', '', '', '"' . $arrmixPropertyRefund['city'] . '"', '"' . $arrmixPropertyRefund['state_code'] . '"',
								$arrmixPropertyRefund['postal_code'],
								$strCountryCode, $arrmixPropertyRefund['phone_number'], '', '', '', '', '', '', '', '', '', '', '', $arrmixPropertyRefund['email_address'],
								' ', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
								' ', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
							];
						}
					}
				}

				if( false != valArr( $arrstrFileContentFinVendor ) ) {
					$this->m_arrstrFileName[]    = 'finvendor_' . date( 'Y_m_d_His' ) . '_' . strtotime( $arrmixApDetail['transaction_datetime'] ) . '_' . $arrmixExportData['company_name'] . '_' . $arrmixExportData['export_batch_id'] . '.csv';
					$this->m_arrstrFileContent[] = array_merge( $arrstrFileContentFinVendorColumns, $arrstrFileContentFinVendor );
				} else {
					// if single property is selected but is consolidated was set true and if finVendor data is not available, then we should create only finPayable file
					$this->m_intFileCount = 1;
				}
			}
		}

		if( true == $arrmixExportData['show_ftp'] ) {
			$intCid = $arrmixExportData['cid'];
			foreach( $this->m_arrstrFileName as $intIndex => $strFileName ) {
				$arrmixExportData['file_name']      = $this->m_arrstrFileName[$intIndex];
				$arrmixExportData['file_content']   = $this->m_arrstrFileContent[$intIndex];
				$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;

				$boolIsSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $intCid );
				if( false == $boolIsSuccess ) {
					break;
				}
			}

			return $boolIsSuccess;
		}

		$arrstrFileContent = [];
		foreach( $this->m_arrstrFileContent as $arrstrFileContentTemp ) {
			$strFileContent = '';
			foreach( $arrstrFileContentTemp as $arrstrContent ) {
				$strFileContent .= implode( ',', $arrstrContent );
				$strFileContent .= "\r\n";
			}
			$arrstrFileContent[] = $strFileContent;
		}
		$this->m_arrstrFileContent = $arrstrFileContent;

		if( 1 == $this->m_intFileCount ) {
			$this->m_strFileName = $this->m_arrstrFileName[0];
			$this->m_strFileContent = $arrstrFileContent[0];
		}

		if( true == $boolIsScript ) {
			$arrmixFileData = 1 == $this->m_intFileCount ? [ 'fileName' => $this->m_strFileName, 'fileContent' => $this->m_strFileContent ] : [ 'fileName' => $this->m_arrstrFileName, 'fileContent' => $this->m_arrstrFileContent ];

			return $arrmixFileData;
		}

		return $this->download();
	}

	public function download() {
		switch( $this->getApExportBatchTypeId() ) {
			case CApExportBatchType::TIMBERLINE_PM:
				$strApExportType = 'TimberlinePm';
				break;

			case CApExportBatchType::PEOPLESOFT:
				$strApExportType = 'PeopleSoft';
				break;

			case CApExportBatchType::YARDI:
				$strApExportType = 'Yardi';
				break;

			case CApExportBatchType::YARDI_ETL_INVOICE_REGISTER:
				$strApExportType = 'YardiEtl_InvoiceRegister';
				break;

			case CApExportBatchType::YARDI_ETL_FINPAYABLES:
				$strApExportType = 'Yardi_ETL_FinPayables';
				break;

			default:
				$strApExportType = 'ApExport';
				break;
		}

		if( 1 == $this->m_intFileCount ) {
			ob_end_clean();
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: private', false );
			header( 'Content-type: text/plain' );
			header( 'Content-Disposition: attachment; filename="' . $this->m_strFileName . '"' );
			header( 'filename="' . $this->m_strFileName . '"' );
			header( 'title="' . $this->m_strFileName . '"' );
			header( 'Content-length: ' . strlen( $this->m_strFileContent ) );
			header( 'Content-Transfer-Encoding: binary' );

			print $this->m_strFileContent;
			exit;
		} else {
			foreach( $this->m_arrstrFileContent as $intKey => $strFileContent ) {
				// Temporary File path
				$strCacheFilename = $this->m_arrstrFileName[$intKey];

				$strFileDownloadPath = 'APExport' . DIRECTORY_SEPARATOR . $strCacheFilename;
				$strTempDir          = CConfig::get( 'cache_directory' );
				// Check if the cache directory exists. If it doesn't create it.
				if( !is_dir( $strTempDir . 'APExport' . DIRECTORY_SEPARATOR ) ) {
					if( false == CFileIo::recursiveMakeDir( $strTempDir . 'APExport' . DIRECTORY_SEPARATOR ) ) {
						trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

						return false;
					}
				}
				$arrstrFiles[] = $strTempDir . $strFileDownloadPath;
				CFileIo::filePutContents( $strTempDir . $strFileDownloadPath, $strFileContent );
			}

			$strTempDir = CConfig::get( 'cache_directory' );
			if( false == is_dir( $strTempDir . 'APExport/' ) ) {
				if( false == CFileIo::recursiveMakeDir( $strTempDir . 'APExport/' ) ) {
					trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

					return false;
				}
			}
			// Create a zip file.
			$strZipFileName = $strTempDir . 'APExport/' . $strApExportType . date( '_m_d_y_His', mktime() ) . '.zip';
			$objZipArchive  = new ZipArchive();
			$objZipArchive->open( $strZipFileName, ZipArchive::CREATE );

			// Loop through all files that needs to be zipped.
			foreach( $arrstrFiles as $strFile ) {
				if( true == valFile( $strFile ) ) {
					$strLocalFileName = basename( $strFile );
					$objZipArchive->addFile( $strFile, $strLocalFileName );
				} else {
					trigger_error( 'Not a valid file to zip.', E_USER_WARNING );

					return false;
				}
			}
			$objZipArchive->close();
			ob_end_clean();
			header( 'Pragma:public' );
			header( 'Expires:0' );
			header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control:public' );
			header( 'Content-Description:File Transfer' );
			header( 'Content-type:application/zip' );
			header( 'Content-Disposition: attachment; filename=' . basename( $strZipFileName ) );
			header( 'Content-Length: ' . filesize( $strZipFileName ) );
			header( 'Content-Transfer-Encoding:binary' );

			// Disable output compression as it creates problem for large file downloads.
			if( ini_get( 'zlib.output_compression' ) ) {
				ini_set( 'zlib.output_compression', 'Off' );
			}
			$resFilePointer = CFileIo::fileOpen( $strZipFileName, 'rb' );
			while( !feof( $resFilePointer ) ) {
				print ( fread( $resFilePointer, 1024 * 8 ) );
				ob_flush();
				flush();
			}
			fclose( $resFilePointer );
			unlink( $strZipFileName );
			if( false != valArr( $arrstrFiles ) ) {
				foreach( $arrstrFiles as $strFile ) {
					unlink( $strFile );
				}
			}
			exit;
		}
	}

	public function unsetApExportBatchIdFromApDetailsByApExportBatchIdByCid( $intApExportBatchId, $intCurrentUserId, $intCid, $objDatabase ) {
		$arrobjApDetails = ( array ) CBaseApDetails::fetchApDetailsByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objDatabase );
		if( true == valArr( $arrobjApDetails ) ) {
			foreach( $arrobjApDetails as $objApDetail ) {
				$objApDetail->setApExportBatchId( NULL );
			}
			if( false == CApDetails::bulkUpdate( $arrobjApDetails, [ 'ap_export_batch_id', 'updated_by', 'updated_on' ], $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
			return true;
		}
		return true;
	}

}
?>