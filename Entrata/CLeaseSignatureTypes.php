<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseSignatureTypes
 * Do not add any new functions to this class.
 */

class CLeaseSignatureTypes extends CBaseLeaseSignatureTypes {

    public static function fetchLeaseSignatureTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CLeaseSignatureType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchLeaseSignatureType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CLeaseSignatureType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>