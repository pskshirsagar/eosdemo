<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInstallmentPlans
 * Do not add any new functions to this class.
 */

class CInstallmentPlans extends CBaseInstallmentPlans {

	public static function fetchInstallmentPlansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsForRenewalOffer = false, $boolIncludeCommercialLeaseTerms = false, $boolIsStudentSemesterSelectionEnabled = false, $intOrganizationContractsWindowId = NULL ) {

		$strWhereCondition = '';
		if( true == $boolIsForRenewalOffer ) {
			$strWhereCondition .= 'AND ip.is_published = TRUE';
		}
		$strWhereCondition .= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereCondition .= ( true == valId( $intOrganizationContractsWindowId ) ) ? ' AND lsw.id = ' . ( int ) $intOrganizationContractsWindowId : '';

		$strSelectSubSql  = '';
		$strSelectSql     = '';
		$strGroupBy       = '';
		$strOrderBy       = '';
		$strSubGroupBy    = '';

		if( true == $boolIsStudentSemesterSelectionEnabled ) {
			$strSelectSubSql = 'sub.lease_start_window_id, sub.start_date, sub.end_date,';

			$strSelectSql = 'util_get_translated( \'name\', lt.name, lt.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,
                     lsw.id as lease_start_window_id, lsw.start_date, lsw.end_date,';

			$strJoinSql = ' JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id AND ip.lease_start_window_id = lsw.id AND lsw.deleted_on IS NULL AND lsw.is_active = \'true\' ) ';

			$strGroupBy = ' lt.name,
                        lsw.id, lsw.start_date, lsw.end_date, ';
			$strOrderBy = ' lsw.start_date,
                        lsw.end_date,
                        lsw.end_date > CURRENT_DATE, ';

			$strSubGroupBy = 'sub.lease_start_window_id, sub.start_date, sub.end_date,';

			if( false == valId( $intOrganizationContractsWindowId ) ) {
				$strWhereCondition .= ' AND lsw.organization_contract_id IS NULL';
			} else {
				$strJoinSql .= ' JOIN installments i ON ( i.cid = ip.cid AND i.installment_plan_id = ip.id AND i.deleted_by IS NULL )
								 LEFT JOIN ledger_filters lf ON ( lf.cid = ip.cid AND lf.id = ip.ledger_filter_id )
								 LEFT JOIN property_ledger_filters plf ON ( plf.cid = lf.cid AND plf.property_id = ip.property_id AND lf.id = plf.ledger_filter_id )';

				$strSelectSubSql .= ' sub.ledger_name, sub.installments_count, sub.leases_count_without_ip, sub.organization_contract_leases_exists, sub.reassociate_installment_plan, sub.show_in_entrata,';
				$strSelectSql    .= ' CASE WHEN lf.name IS NOT NULL THEN lf.name ELSE NULL END as ledger_name, count( i.id ) AS installments_count,
										EXISTS (
											SELECT
												1
											FROM 
												cached_leases l
												JOIN cached_applications ca ON ( l.cid = ca.cid AND l.property_id = ca.property_id AND l.id = ca.lease_id AND l.active_lease_interval_id = ca.lease_interval_id )
											WHERE 
												l.cid = ' . ( int ) $intCid . ' AND
												l.property_id = ' . ( int ) $intPropertyId . ' AND
												l.organization_contract_id IS NOT NULL AND
												ca.lease_status_type_id IN ( ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CURRENT . ' ) AND
												ca.lease_term_id = lt.id AND
												ca.lease_start_window_id = lsw.id
										) AS organization_contract_leases_exists,
										EXISTS (
											SELECT
												1
											FROM 
												cached_leases l
												JOIN cached_applications ca ON ( l.cid = ca.cid AND l.property_id = ca.property_id AND l.id = ca.lease_id AND l.active_lease_interval_id = ca.lease_interval_id )
												LEFT JOIN lease_interval_installment_plans liip ON ( l.cid = liip.cid AND ip.id = liip.installment_plan_id AND liip.lease_interval_id = l.active_lease_interval_id )
											WHERE 
												l.cid = ' . ( int ) $intCid . ' AND
												l.property_id = ' . ( int ) $intPropertyId . ' AND
												l.organization_contract_id IS NOT NULL AND
												ca.lease_status_type_id IN ( ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CURRENT . ' ) AND
												ca.lease_term_id = lt.id AND
												ca.lease_start_window_id = lsw.id AND
												liip.id IS NULL
										) AS leases_count_without_ip,
										r.show_in_entrata,
										CASE
											WHEN \'true\' = JSONB_EXTRACT_PATH_TEXT( ip.details, \'reassociate_installment_plan\' ) THEN TRUE
											ELSE FALSE
										END AS reassociate_installment_plan,';

				$strSubGroupBy   .= ' sub.ledger_name, sub.installments_count, sub.leases_count_without_ip, sub.organization_contract_leases_exists, sub.reassociate_installment_plan, sub.show_in_entrata,';
				$strGroupBy      .= ' lf.name, r.show_in_entrata,';
				$strWhereCondition .= ' AND ip.deleted_by IS NULL ';
			}
		} else {
			$strSelectSql      .= 'util_get_translated( \'name\', lt.name, lt.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,';
			$strWhereCondition .= ' AND lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL;
			$strGroupBy        .= 'lt.name, lt.term_month,';
			$strOrderBy        .= 'lt.term_month,';
		}

		$strSql = 'SELECT sub.id,
                       sub.name,
                       ' . $strSelectSubSql . '
                       sub.installment_plan_id,
                       sub.installment_plan_name,
                       sub.old_name,
                       sub.installment_plan_description,
                       sub.lease_term_id,
                       sub.is_published,
                       array_to_string(array_agg(sub.rate_amount), \',\') as rate_amount,
                       array_to_string(array_agg(sub.max_rate_amount), \',\') as max_rate_amount,
                       array_to_string(array_agg(sub.ar_formula_id), \',\') as ar_formula_id,
                       sub.special_id,
                       sub.use_lease_start_windows_for_rent,
                       sub.use_space_configurations_for_rent,
                       CASE
                         WHEN count(sub.ar_formula_id) > 1 THEN 1
                         ELSE 0
                       END AS is_varies
                   FROM ( 
			               SELECT
			                  lt.id,
			                  ' . $strSelectSql . '
			                  ip.id as installment_plan_id,
			                  util_get_translated( \'name\', ip.name, ip.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as installment_plan_name,
			                  ip.name AS old_name,
			                  util_get_translated( \'description\', ip.description, ip.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as installment_plan_description,
			                  ip.lease_term_id,
			                  CASE WHEN ip.is_published = \'true\' THEN \'true\' ELSE \'false\' END AS is_published,
			                  CASE WHEN r.ar_formula_id > ' . CArFormula::FIXED_AMOUNT . ' THEN
			                     min( r.rate_increase_increment )
			                  ELSE
			                     min( r.rate_amount )
			                  END as rate_amount,
			                  CASE WHEN r.ar_formula_id > ' . CArFormula::FIXED_AMOUNT . ' THEN
			                     max( r.rate_increase_increment )
			                  ELSE
			                     max( r.rate_amount )
			                  END as max_rate_amount,
			                  r.ar_formula_id,
			                  ip.special_id,
			                  CASE WHEN paor.use_lease_start_windows_for_rent = \'true\' THEN 1 ELSE 0 END AS use_lease_start_windows_for_rent,
			                  CASE WHEN paor.use_space_configurations_for_rent = \'true\' THEN 1 ELSE 0 END AS use_space_configurations_for_rent
			               FROM
			                  lease_terms lt
			                  JOIN installment_plans ip ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id )
			                  ' . $strJoinSql . '
			                  LEFT JOIN rates r ON ( ip.cid = r.cid AND r.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ip.special_id = r.ar_origin_reference_id AND r.is_allowed = \'true\' ' . ( ( false == valId( $intOrganizationContractsWindowId ) ) ? ' AND r.show_in_entrata IS TRUE' : '' ) . ' AND ( r.rate_amount <> 0 OR r.rate_increase_increment <> 0 ) )
			                  LEFT JOIN property_ar_origin_rules paor ON ( paor.cid = ip.cid AND paor.property_id = ip.property_id AND paor.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
			               WHERE
			                  lt.cid = ' . ( int ) $intCid . '
			                  AND ip.property_id = ' . ( int ) $intPropertyId . '
			                  AND lt.is_disabled = \'false\'
			                  AND lt.deleted_on IS NULL
			                  AND ip.deleted_by IS NULL  
			                  ' . $strWhereCondition . '
			               GROUP BY
			                  lt.id,
			                  ' . $strGroupBy . '
			                           lt.details,
			                  ip.id,
			                  ip.name,
			                  ip.description,
			                  ip.details,
			                  ip.lease_term_id,
			                  ip.is_published,
			                  paor.use_lease_start_windows_for_rent,
			                  paor.use_space_configurations_for_rent,
			                  r.ar_formula_id,
			                  ip.special_id
			               ORDER BY
			                  lt.id,
			                  ' . $strOrderBy . '
			                  ip.id
					) as sub 
         GROUP BY sub.id,
                   sub.name,
                   ' . $strSubGroupBy . '
                   sub.installment_plan_id,
                   sub.installment_plan_name,
                   sub.old_name,
                   sub.installment_plan_description,
                   sub.lease_term_id,
                   sub.is_published,
                   sub.special_id,
                   sub.use_lease_start_windows_for_rent,
                   sub.use_space_configurations_for_rent';

		$arrmixInstallmentPlans = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixInstallmentPlans ) ) return [];

		$intLeaseTermId = 0;
		$intCounter = 0;

		foreach( $arrmixInstallmentPlans as $arrmixInstallmentPlan ) {

			if( true == valStr( $arrmixInstallmentPlan['id'] ) ) {
				$intCounter++;
			}

			if( $intLeaseTermId != $arrmixInstallmentPlan['lease_start_window_id'] && 0 != $intLeaseTermId ) {

				$intCounter = 0;
				if( true == valStr( $arrmixInstallmentPlan['id'] ) ) {
					$intCounter++;
				}
			}

			$intLeaseTermId = $arrmixInstallmentPlan['lease_start_window_id'];
			$arrmixInstallmentPlans['installments_option_count'][$intLeaseTermId] = $intCounter;
		}

		return $arrmixInstallmentPlans;
	}

	public static function fetchCustomInstallmentPlanByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ip.id,
						ip.payment_ar_code_id,
						util_get_translated( \'name\', ip.name, ip.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,
						ip.name AS old_name,
						util_get_translated( \'description\', ip.description, ip.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS description,
						ip.description AS old_description,
		                ip.lease_start_window_id,
						CASE WHEN ip.is_published = \'true\' THEN \'true\' ELSE \'false\' END AS is_published,
						ip.lease_term_id,r.ar_code_id,
						s.id AS special_id,
						r.id AS rate_id,
						r.ar_code_id,
						r.rate_amount,
						r.ar_formula_id,
						lt.name AS lease_term_name,
						lsw.start_date,
						lsw.end_date,
						lsw.renewal_billing_start_date,
						lsw.billing_end_date
					FROM
						installment_plans ip
						LEFT JOIN specials s ON ( ip.cid = s.cid AND ip.special_id = s.id )
   						LEFT JOIN rates r ON ( ip.cid = r.cid AND r.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.id = r.ar_origin_reference_id )
   						LEFT JOIN lease_terms lt ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id )
   						LEFT JOIN lease_start_windows lsw ON ( ip.cid = lsw.cid AND ip.lease_term_id = lsw.lease_term_id AND ip.lease_start_window_id = lsw.id )
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND ip.id = ' . ( int ) $intId . '
						AND ip.deleted_by IS NULL ';

		$arrmixInstallmentPlan = fetchData( $strSql, $objDatabase );

		return $arrmixInstallmentPlan[0];
	}

	public static function fetchInstallmentPlansByPropertyIdByLeaseTermIdByCid( $intPropertyId, $intLeaseTermId, $intCid, $objDatabase, $arrintExistingIds = [], $intLeaseStartWindowId = NULL, $boolIncludeCommercialLeaseTerms = false, $boolReturnObjects = false, $arrintTobeDeletedInstallmentLedgerIds = NULL, $intOrganizationContractId = NULL, $boolSkipPlansWithoutSpecialRates = false ) {

		$strJoinSql = '';
		$strExistingIdCondition = '';

		if( false == valId( $intLeaseTermId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;
		$strWhereCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereCondition .= ( true == valId( $intLeaseStartWindowId ) ) ? 'AND ip.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId : '';

		$strSelectFields   = 'ip.id, ip.name, ip.description, ip.ledger_filter_id';
		if( true == valArr( $arrintTobeDeletedInstallmentLedgerIds ) ) {
			$strSelectFields    = ' ip.* ';
			$strWhereCondition .= ' AND ip.ledger_filter_id IN ( ' . implode( ',', $arrintTobeDeletedInstallmentLedgerIds ) . ' ) ';
		}

		if( false == valId( $intOrganizationContractId ) ) {
			$strWhereCondition .= ' AND lsw.organization_contract_id IS NULL';
		} else {
			$strJoinSql .= ' LEFT JOIN ledger_filters lf ON ( lf.cid = ip.cid AND lf.id = ip.ledger_filter_id ) ';
			$strSelectFields .= ', lf.name as ledger_filter_name, lf.default_ledger_filter_id';
			$strWhereCondition .= ' AND ip.deleted_on IS NULL AND ip.deleted_by IS NULL';

			if( true == $boolSkipPlansWithoutSpecialRates ) {
				$strJoinSql .= ' LEFT JOIN LATERAL (
									SELECT
									    sp.id
									FROM
									    rate_associations ra
									    JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id  )
									WHERE
									    ra.cid = ip.cid
									    AND ra.property_id = ip.property_id
									    AND ra.ar_origin_reference_id = ip.special_id
									    AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
									    AND sp.special_type_id = ' . CSpecialType::INSTALLMENT_PLAN . '
									    AND EXISTS (
											SELECT
											  1
											FROM
											  rate_logs r
											WHERE
											  r.cid = sp.cid
											  AND r.show_in_entrata IS TRUE
											  AND r.ar_origin_id = ra.ar_origin_id
											  AND r.ar_origin_reference_id = sp.id
											  AND r.is_allowed = TRUE
											  AND r.rate_amount <> 0
											  AND r.deactivation_date > CURRENT_DATE
											  AND CURRENT_DATE BETWEEN r.effective_date AND r.effective_through_date
											  AND r.effective_through_date >= r.effective_date
											  AND r.is_effective_datetime_ignored = FALSE
									    )
								) AS installment_special ON TRUE';

				$strWhereCondition .= ' AND CASE WHEN ip.special_id IS NOT NULL THEN installment_special.id IS NOT NULL ELSE TRUE END';
			}
		}

		if( true == valArr( $arrintExistingIds ) ) {
			$strExistingIdCondition = ' OR ip.id IN ( ' . implode( ',', $arrintExistingIds ) . ' ) ';
		}

		$strSql = 'SELECT
						' . $strSelectFields . '
					FROM
						lease_terms lt
						JOIN installment_plans ip ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id )
						LEFT JOIN lease_start_windows lsw ON( ip.cid = lsw.cid AND ip.lease_start_window_id = lsw.id AND ip.lease_term_id = lsw.lease_term_id )
						' . $strJoinSql . '
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND ip.property_id = ' . ( int ) $intPropertyId . '
						AND ip.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND ( ip.is_published = TRUE ' . $strExistingIdCondition . ' )
						AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '
						AND lt.deleted_on IS NULL
						AND ip.deleted_by IS NULL 
						' . $strWhereCondition . '
					ORDER BY
						ip.id';

		if( true == $boolReturnObjects ) {
			return self::fetchInstallmentPlans( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupInstallmentPlansByPropertyIdByLeaseTermIdByCid( $intPropertyId, $intLeaseTermId, $intCid, $objDatabase, $intExistingId = NULL, $intLeaseStartWindowId = NULL, $boolIncludeCommercialLeaseTerms = false, $boolReturnObjects = false, $arrintTobeDeletedInstallmentLedgerIds = NULL ) {

		if( false == valId( $intLeaseTermId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;
		$strWhereCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereCondition .= ( true == valId( $intLeaseStartWindowId ) ) ? 'AND ip.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId : '';

		$strSelectFields   = 'ip.id, ip.name, ip.description, ip.ledger_filter_id';
		if( true == valArr( $arrintTobeDeletedInstallmentLedgerIds ) ) {
			$strSelectFields    = ' * ';
			$strWhereCondition .= ' AND ip.ledger_filter_id IN ( ' . implode( ',', $arrintTobeDeletedInstallmentLedgerIds ) . ' ) ';
		}

		$strSql = 'SELECT
						' . $strSelectFields . '
					FROM
						lease_terms lt
						JOIN installment_plans ip ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id )
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND ip.property_id = ' . ( int ) $intPropertyId . '
						AND ip.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND ( ip.is_published = TRUE OR ip.id = ' . ( int ) $intExistingId . ' )
						AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '
						AND lt.deleted_on IS NULL
						AND ip.deleted_by IS NULL 
						' . $strWhereCondition . '
					ORDER BY
						ip.id';

		if( true == $boolReturnObjects ) {
			return self::fetchInstallmentPlans( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveInstallmentPlansCountByPropertyIdByLeaseTermIdByCid( $intPropertyId, $intLeaseTermId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseTermId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;
		$strWhereSql = ' WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND lease_term_id = ' . ( int ) $intLeaseTermId . '
							AND is_published = TRUE
							AND deleted_by IS NULL ';
		return parent::fetchInstallmentPlanCount( $strWhereSql, $objDatabase );
	}

	public static function fetchInstallmentPlansByIdsByCId( $arrintInstallmentPlanIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintInstallmentPlanIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						installment_plans ip
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND ip.id IN( ' . implode( ',', $arrintInstallmentPlanIds ) . ' )
						AND ip.deleted_by IS NULL ';

		return self::fetchInstallmentPlans( $strSql, $objDatabase );
	}

	public static function fetchInstallmentPlanByIdByCidByPropertyId( $intInstallmentPlanId, $intCid, $intLeaseId, $intPropertyId, $objDatabase ) {

		if( false == is_numeric( $intInstallmentPlanId ) ) return NULL;

		$strSql = ' SELECT
						ip.id,
						ip.name,
						ip.description,
						r.rate_amount,
						SUM ( charge_amount ) AS total_discount_amount
					FROM
						installment_plans ip
						LEFT JOIN scheduled_charges sc  ON ( sc.installment_plan_id = ip.id AND sc.cid = ip.cid AND sc.lease_id = ' . ( int ) $intLeaseId . '
															AND sc.property_id = ' . ( int ) $intPropertyId . ' AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL 
														)
						LEFT JOIN specials s ON ( ip.cid = s.cid AND ip.special_id = s.id )
						LEFT JOIN rates r ON ( ip.cid = r.cid AND r.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ip.special_id = r.ar_origin_reference_id )
					WHERE
						ip.id = ' . ( int ) $intInstallmentPlanId . '
						AND ip.cid = ' . ( int ) $intCid . '
					GROUP BY
						ip.id,
						ip.name,
						ip.description,
						r.rate_amount,
						charge_amount';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInstallmentsByInstallmentPlanIdByLeaseIntervalId( $intInstallmentPlanId, $intCid, $intLeaseIntervalId, $objDatabase ) {
		if( false == is_numeric( $intInstallmentPlanId ) ) return NULL;

		$strSql = ' SELECT
						i.name,
						i.id as installment_id,
						i.installment_plan_id,
						sc.id as scheduled_charge_id,
						i.charge_start_date,
                        i.charge_end_date
					FROM
						installments i
						JOIN scheduled_charges sc  ON ( sc.installment_id = i.id AND sc.cid = i.cid AND sc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL )
					WHERE
						i.installment_plan_id = ' . ( int ) $intInstallmentPlanId . '
						AND i.cid = ' . ( int ) $intCid . '
					ORDER BY
						i.charge_start_date ASC ';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInstallmentPlansByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ip.*
					FROM
						installment_plans ip
						JOIN lease_interval_installment_plans liip ON ( liip.cid = ip.cid AND liip.installment_plan_id = ip.id )
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND liip.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchInstallmentPlans( $strSql, $objDatabase );
	}

	public static function fetchInstallmentPlanByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					    p.id AS property_id,
					    foo.name AS NAME,
					    foo.id as installment_plan_id,
					    pp.key,
					    pp.value,
					    CASE WHEN ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND ppr.value = \'1\' ) THEN 1 ELSE 0 END AS is_student_semester_selection_enabled
					FROM
					    properties p
					    LEFT JOIN (
						    SELECT 
						        ip.id,
						        ip.cid,
						        ip.property_id,
						        ip.name AS NAME
						    FROM
						        lease_terms lt
						        JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
						        LEFT JOIN installment_plans ip ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id AND ip.lease_start_window_id = lsw.id )
						    WHERE
						        ip.cid = ' . ( int ) $intCid . '
						        AND lt.is_disabled = \'false\'
						        AND lt.deleted_on IS NULL
						        AND lsw.deleted_on IS NULL
						        AND lsw.is_active = \'true\'
						        AND ip.deleted_by IS NULL 
						    ) as foo ON ( foo.cid = p.cid AND foo.property_id = p.id )
						LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id  AND pp.key = \'' . trim( $strKey ) . '\' )
						LEFT JOIN property_preferences ppr ON ( ppr.cid = p.cid AND ppr.property_id = p.id AND ppr.key = \'ENABLE_SEMESTER_SELECTION\' )
					WHERE
					    p.cid = ' . ( int ) $intCid . '
					    AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomInstallmentPlansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ip.*,
						s.special_recipient_id,
						s.special_type_id,
						s.details AS special_details,
						ra.id AS rate_association_id,
						ra.ar_origin_id,
						lt.name AS lease_term_name,
						lsw.start_date AS window_start_date,
						lsw.end_date AS window_end_date
					FROM 
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
						LEFT JOIN installment_plans ip ON ( ip.cid = lt.cid AND ip.lease_term_id = lt.id AND ip.lease_start_window_id = lsw.id )
						LEFT JOIN specials s ON ( ip.cid = s.cid AND ip.special_id = s.id )
						LEFT JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.property_id = ip.property_id ) 
					WHERE 
						ip.cid= ' . ( int ) $intCid . '
						AND ip.property_id= ' . ( int ) $intPropertyId . '
						AND lt.deleted_on IS NULL
						AND lsw.deleted_on IS NULL
						AND ip.deleted_by IS NULL ';

		return parent::fetchInstallmentPlans( $strSql, $objDatabase );
	}

	public static function fetchInstallmentPlansByRemotePrimaryKeysByNamesByPropertyIdByCid( $arrstrRemotePrimaryKeys, $arrstrNames, $intPropertyId, $intCid, $objDatabase ) {

		$strSubSql = '';
		if( true == valArr( $arrstrRemotePrimaryKeys ) ) {
			$strSubSql = ' ip.remote_primary_key IN( \'' . implode( '\', \'', $arrstrRemotePrimaryKeys ) . '\' ) OR';
		}
		if( false == valArr( $arrstrNames ) ) return NULL;

		$strSql = 'SELECT
						ip.*,
						lt.name AS lease_term_name
					FROM
						installment_plans ip
						LEFT JOIN lease_terms lt ON ( lt.cid = ip.cid AND lt.id = ip.lease_term_id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = ip.cid AND lsw.id = ip.lease_start_window_id  AND lsw.lease_term_id = ip.lease_term_id )
					WHERE 
						ip.cid = ' . ( int ) $intCid . '
					AND ip.property_id = ' . ( int ) $intPropertyId . '
					AND ip.deleted_on IS NULL
					AND ( ' . $strSubSql . ' lower( ip.name ) IN( \'' . \Psi\CStringService::singleton()->strtolower( implode( '\', \'', $arrstrNames ) ) . '\' ) )
					AND ip.deleted_by IS NULL ';

		return self::fetchInstallmentPlans( $strSql, $objDatabase );
	}

	public static function fetchActiveInstallmentPlansByPropertyIdByLeaseTermIdsByCid( $intPropertyId, $arrintLeaseTermIds, $intCid, $objDatabase, $boolReturnCountOnly = false ) {

		if( false == valArr( $arrintLeaseTermIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;
		$strSelectParameters = ( false == $boolReturnCountOnly ) ? 'ip1.*' : 'count( ip1.id )';

		$strSql = ' SELECT
					' . $strSelectParameters . '
					FROM
						installment_plans ip1
						JOIN( SELECT 
								ip.cid,
								ip.property_id,
								ip.lease_term_id
							FROM 
								installment_plans ip
							WHERE
								ip.cid = ' . ( int ) $intCid . '
								AND ip.property_id = ' . ( int ) $intPropertyId . '
								AND ip.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )
								AND ip.is_published = TRUE
								AND ip.deleted_by IS NULL
							GROUP BY
								ip.cid,
								ip.property_id,
								ip.lease_term_id	
							HAVING count(ip.id) = 1
						) as sub ON ( sub.cid = ip1.cid AND sub.property_id = ip1.property_id AND sub.lease_term_id = ip1.lease_term_id )
						WHERE 
							ip1.cid = ' . ( int ) $intCid . '
							AND ip1.property_id = ' . ( int ) $intPropertyId . '
							AND ip1.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )
							AND ip1.is_published = TRUE
							AND ip1.deleted_by IS NULL ';

		if( false == $boolReturnCountOnly ) {
			return parent::fetchInstallmentPlans( $strSql, $objDatabase );
		} else {
			$arrmixResponse = fetchData( $strSql, $objDatabase );
			return $arrmixResponse[0]['count'];
		}

	}

}
?>
