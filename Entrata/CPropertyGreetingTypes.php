<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGreetingTypes
 * Do not add any new functions to this class.
 */

class CPropertyGreetingTypes extends CBasePropertyGreetingTypes {

	public static function fetchPropertyGreetingTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPropertyGreetingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPropertyGreetingType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPropertyGreetingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>