<?php

class CLeaseCustomerVehicle extends CBaseLeaseCustomerVehicle {

	protected $m_objClient;

	protected $m_intLeaseId;

	protected $m_strCustomerNameFull;
	protected $m_strVehicleType;
	protected $m_strCustomerPhoneNumber;
	protected $m_boolIsStateRequired;

	/**
	 * Get Functions
	 *
	 */

	public function getVehicleType() {
		return $this->m_strVehicleType;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function getCustomerPhoneNumber() {
		return $this->m_strCustomerPhoneNumber;
	}

	public function getIsStateRequired() {
		return $this->m_boolIsStateRequired;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['customer_name_full'] ) ) $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		if( true == isset( $arrmixValues['customer_phone_number'] ) ) $this->setCustomerPhoneNumber( $arrmixValues['customer_phone_number'] );
		if( true == isset( $arrmixValues['is_state_required'] ) ) $this->setCustomerPhoneNumber( $arrmixValues['is_state_required'] );
	}

	public function setVehicleType( $strVehicleType ) {
		$this->m_strVehicleType = $strVehicleType;
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function setCustomerPhoneNumber( $strCustomerPhoneNumber ) {
		$this->m_strCustomerPhoneNumber = $strCustomerPhoneNumber;
	}

	public function setNotes( $strNotes ) {
		parent::setNotes( CStrings::strTrimDef( strip_tags( $strNotes ), -1, NULL, true ) );
	}

	public function setModel( $strModel ) {
		parent::setModel( CStrings::strTrimDef( strip_tags( $strModel ), 50, NULL, true ) );
	}

	public function setColor( $strColor ) {
		parent::setColor( CStrings::strTrimDef( strip_tags( $strColor ), 20, NULL, true ) );
	}

	public function setPermitNumber( $strPermitNumber ) {
		$this->m_strPermitNumber = CStrings::strTrimDef( strip_tags( $strPermitNumber ), 100, NULL, true );
	}

	public function setLicensePlateNumber( $strLicensePlateNumber ) {
		$this->m_strLicensePlateNumber = CStrings::strTrimDef( strip_tags( $strLicensePlateNumber ), 20, NULL, true );
	}

	public function setMake( $strMake ) {
		parent::setMake( CStrings::strTrimDef( strip_tags( $strMake ), 50, NULL, true ) );
	}

	public function setIsStateRequired( $boolIsStateRequired ) {
		$this->m_boolIsStateRequired = $boolIsStateRequired;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Vehicle Request:  Id required - CLeaseCustomerVehicle::valId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Vehicle Request:  Management id required - CLeaseCustomerVehicle::valCid()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intLeaseId ) || 0 >= ( int ) $this->m_intLeaseId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Vehicle Request:  Management id required - CLeaseCustomerVehicle::valLeaseid()', E_USER_ERROR );
		}
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Customer is required' ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function valYear( $boolIsRequired = false, $intVehicleCount = 0, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == isset( $this->m_intYear ) ) {
			// Make sure that the year is greater then when they made cars

			$intValidDateYear = 2099;

			if( $intValidDateYear < $this->m_intYear || 1900 > $this->m_intYear ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_year_' . $intVehicleCount, __( 'Vehicle year is invalid for #{%d, 0}', [ $intVehicleCount ] ) ) );
			}
		} elseif( true == $boolIsRequired && 1 > strlen( trim( $this->getYear() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_year_' . $intVehicleCount, __( 'Vehicle year is required for #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valMake( $boolIsRequired = false, $intVehicleCount = 0, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && 1 > strlen( trim( $this->getMake() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_make_' . $intVehicleCount, __( 'Make is required for #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valModel( $boolIsRequired = false, $intVehicleCount = 0, $boolIsValidateRequired = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 1 > strlen( trim( $this->getModel() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_model_' . $intVehicleCount, __( 'Model is required for #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function valColor( $boolIsRequired = false, $intVehicleCount = 0, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && 1 > strlen( trim( $this->getColor() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_color_' . $intVehicleCount, __( 'Color is required for #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function valPermitNumber( $intVehicleCount = 0 ) {
		$boolIsValid = true;

		if( 0 < strlen( trim( $this->getPermitNumber() ) ) && 0 !== \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9-_# ]*$/', trim( $this->getPermitNumber() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_permit_number_' . $intVehicleCount, __( 'Enter valid Permit Number #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			return false;
		} elseif( 0 < strlen( trim( $this->getPermitNumber() ) ) && 100 != \Psi\CStringService::singleton()->strlen( trim( $this->getPermitNumber() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_permit_number_' . $intVehicleCount, __( 'Permit Number must be 100 digits #{%d, 0}', [ $intVehicleCount ] ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valLicensePlateNumber( $objClientDatabase, $boolIsRequired = true, $intVehicleCount = 0, $boolIsValidateRequired = true, $arrintDeletingVehicleIds = NULL, $boolValDuplicatePlateNumber = true, $boolIsStateField = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && 1 > strlen( trim( $this->getLicensePlateNumber() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'license_plate_number_' . $intVehicleCount, __( 'Plate # is a required field.' ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && true == $boolValDuplicatePlateNumber ) {
			$strWhereConditions = ' WHERE
									cid = ' . ( int ) $this->getCid() . '
									AND lease_id = ' . ( int ) $this->getLeaseId() . ' 
									AND customer_id = ' . ( int ) $this->getCustomerId();

			if( true == $boolIsStateField ) {
				$strWhereConditions .= ' AND state_code LIKE \'' . $this->getStateCode() . '\'';
			} else {
				$strWhereConditions .= ' AND country_code LIKE \'' . $this->getCountryCode() . '\'';
			}

			$strWhereConditions .= ' AND license_plate_number ILIKE \'' . addslashes( $this->getLicensePlateNumber() ) . '\'
									AND deleted_on IS NULL
									' . ( ( 0 < $this->getId() ) ? ' AND id <> ' . ( int ) $this->getId() : '' );

			if( true == valArr( $arrintDeletingVehicleIds ) ) {
				$strWhereConditions .= ' AND id NOT IN ( ' . implode( ',', $arrintDeletingVehicleIds ) . ' ) ';
			}

			$intLeaseCustomerVehiclesCount = CLeaseCustomerVehicles::fetchLeaseCustomerVehicleCount( $strWhereConditions, $objClientDatabase );

			if( 0 < $intLeaseCustomerVehiclesCount ) {
				$boolIsValid = false;
				$this->setLicensePlateNumber( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'license_plate_number_' . $intVehicleCount, __( 'Plate No. is already used.' ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valStateCode( $boolIsRequired = true, $intVehicleCount = 0, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && false == valStr( $this->getStateCode() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_state_code_' . $intVehicleCount, __( 'State is a required field.' ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode( $boolIsRequired = true, $intVehicleCount = 0, $boolIsValidateRequired = true ) {
		$boolIsValid = true;
		if( true == $boolIsRequired && false == valStr( $this->getCountryCode() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_country_code_' . $intVehicleCount, __( 'Country is a required field.' ), '', [ 'label' => __( 'Vehicle' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function hasValidData() {
		return valStr( $this->getLicensePlateNumber() ) || valStr( $this->getYear() ) || valStr( $this->getMake() ) || valStr( $this->getModel() ) || valStr( $this->getColor() ) || valStr( $this->getStateCode() || valStr( $this->getCountryCode() ) );
	}

	public function validate( $strAction, $objClientDatabase, $arrobjPropertyApplicationPreferences = [], $intVehicleCount = 0, $boolIsValidateRequired = true, $arrintDeletingVehicleIds = NULL, $boolIsStateField = true ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLicensePlateNumber( $objClientDatabase, ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_PLATE_NUMBER'] ) : true, $intVehicleCount, $boolIsValidateRequired, $arrintDeletingVehicleIds, false,  $boolIsStateField );
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valYear( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_YEAR'] ) : false, $intVehicleCount, $boolIsValidateRequired );
				if( true == $boolIsStateField ) {
					$boolIsValid &= $this->valStateCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount, $boolIsValidateRequired );
				}
				$boolIsValid &= $this->valCountryCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_LICENSE_COUNTRY'] ) : true, $intVehicleCount, $boolIsValidateRequired );
				$boolIsValid &= $this->valMake( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_MAKE'] ), $intVehicleCount, $boolIsValidateRequired );
				$boolIsValid &= $this->valModel( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_MODEL'] ), $intVehicleCount, $boolIsValidateRequired );
				$boolIsValid &= $this->valColor( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_COLOR'] ), $intVehicleCount, $boolIsValidateRequired );
				$boolIsValid &= $this->valPermitNumber( $intVehicleCount );
				$boolIsValid &= $this->valLeaseId();
				break;

			case VALIDATE_UPDATE:
				if( VALIDATE_UPDATE == $strAction ) {
					$boolIsValid &= $this->valId();
				}

				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLicensePlateNumber( $objClientDatabase, ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_PLATE_NUMBER'] ) : true, $intVehicleCount, true, NULL, false, $boolIsStateField );
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valYear( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_YEAR'] ) : false, $intVehicleCount );
				$boolIsValid &= $this->valMake( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_MAKE'] ), $intVehicleCount );
				$boolIsValid &= $this->valModel( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_MODEL'] ), $intVehicleCount );
				$boolIsValid &= $this->valColor( isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_COLOR'] ), $intVehicleCount );
				$boolIsValid &= $this->valPermitNumber( $intVehicleCount );
				if( true == $boolIsStateField ) {
					$boolIsValid &= $this->valStateCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount );
				}
				$boolIsValid &= $this->valCountryCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_LICENSE_COUNTRY'] ) : true, $intVehicleCount, $boolIsValidateRequired );

				$boolIsValid &= $this->valLeaseId();
				break;

			case 'validate_update_resident_vehicle':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCustomerId();
				if( true == is_null( $this->getDeletedOn() ) ) {
					$boolIsValid &= $this->valLicensePlateNumber( $objClientDatabase, true, $intVehicleCount, true, NULL, false, $boolIsStateField );
					$boolIsValid &= $this->valYear();
					$boolIsValid &= $this->valCountryCode();
				}
				break;

			case 'validate_insert_resident_portal20':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLicensePlateNumber( $objClientDatabase, true, 0, true, NULL, true, $boolIsStateField );
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valYear();
				if( true == $boolIsStateField ) {
					$boolIsValid &= $this->valStateCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount );
				} else {
					$boolIsValid &= $this->valCountryCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount );
				}
				break;

			case 'validate_update_resident_portal20':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLicensePlateNumber( $objClientDatabase, true, 0, true, NULL, true, $boolIsStateField );
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valYear();
				if( true == $boolIsStateField ) {
					$boolIsValid &= $this->valStateCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount );
				} else {
					$boolIsValid &= $this->valCountryCode( ( true == valId( $intVehicleCount ) ) ? isset( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_VEHICLES_STATE'] ) : true, $intVehicleCount );
				}
				break;

			case 'pre_insert':
				$boolIsValid &= $this->valYear();
				break;

			case 'validate_update_lease_modification_vehicle':
			case 'validate_delete_vehicle':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCustomerId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function fetchCountLeaseCustomerVehicleByCustomerIdByCidByCustomerVehicleTypeId( $intCustomerId, $intCid, $intCustomerVehicleTypeId, $objDatabase ) {

		$strSql = ' SELECT
							count( lcv.id )
						FROM
							lease_customer_vehicles lcv
						WHERE
							lcv.customer_id = ' . ( int ) $intCustomerId . '
							AND lcv.cid = ' . ( int ) $intCid . '
							AND lcv.customer_vehicle_type_id = ' . ( int ) $intCustomerVehicleTypeId;

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrintResponse ) ) ? $arrintResponse[0][count] : 0;
	}

}
?>
