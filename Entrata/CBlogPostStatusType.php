<?php

class CBlogPostStatusType extends CBaseBlogPostStatusType {

	const PUBLISHED_POST_STATUS_TYPE_ID             = NULL;
	const SCHEDULED_POST_STATUS_TYPE_ID             = 1;
	const DELETED_POST_STATUS_TYPE_ID               = 2;
	const PENDING_POST_STATUS_TYPE_ID               = 3;
	const REVISION_REQUIRED_POST_STATUS_TYPE_ID     = 4;
	const UNPUBLISHED_POST_STATUS_TYPE_ID           = 5;
	const DRAFTED_POST_STATUS_TYPE_ID               = 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>