<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntrataTabletModules
 * Do not add any new functions to this class.
 */

class CEntrataTabletModules extends CBaseEntrataTabletModules {

	public static function fetchEntrataTabletModules( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEntrataTabletModule', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEntrataTabletModule( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEntrataTabletModule', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedEntrataTabletModules( $objDatabase ) {
		return self::fetchEntrataTabletModules( 'SELECT * FROM entrata_tablet_modules WHERE is_published = 1 ORDER BY entrata_tablet_module_id NULLS FIRST, order_num DESC, id', $objDatabase );
	}
}
?>