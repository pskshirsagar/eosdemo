<?php

class CApPayeeType extends CBaseApPayeeType {

	const STANDARD				= 1;
	const OWNER					= 2;
	const INTERCOMPANY			= 3;

	// 	Use 'PROPERTY_SOLUTIONS' type for resident refunds invoices.
	const PROPERTY_SOLUTIONS	= 4;
	const EMPLOYEE				= 5;
	const CORPORATE				= 6;
	const RESIDENT				= 7;
	const LENDER				= 8;

	public static $c_arrintAllApPayeeTypes = [
		self::STANDARD,
		self::OWNER,
		self::INTERCOMPANY,
		self::PROPERTY_SOLUTIONS,
		self::LENDER
	];

	public static $c_arrstrFilterApPayeeTypes = [
		self::STANDARD		=> 'Standard',
		self::OWNER			=> 'Owner',
		self::INTERCOMPANY	=> 'Intercompany',
		self::RESIDENT		=> 'Resident',
		self::LENDER		=> 'Lender'
	];

	public static $c_arrstrApPayeeTypeNames = [
		self::STANDARD				=> 'Standard',
		self::OWNER					=> 'Owner',
		self::INTERCOMPANY 			=> 'Intercompany',
		self::PROPERTY_SOLUTIONS 	=> CONFIG_COMPANY_NAME,
		self::LENDER				=> 'Lender'
	];

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'AP_PAYEE_TYPE_STANDARD', 				self::STANDARD );
		$objSmarty->assign( 'AP_PAYEE_TYPE_OWNER', 					self::OWNER );
		$objSmarty->assign( 'AP_PAYEE_TYPE_INTERCOMPANY', 			self::INTERCOMPANY );
		$objSmarty->assign( 'AP_PAYEE_TYPE_PROPERTY_SOLUTIONS',		self::PROPERTY_SOLUTIONS );
		$objSmarty->assign( 'AP_PAYEE_TYPE_RESIDENT', 				self::RESIDENT );
		$objSmarty->assign( 'AP_PAYEE_TYPE_LENDER', 				self::LENDER );
	}

	public static function assignTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['AP_PAYEE_TYPE_STANDARD']             = self::STANDARD;
		$arrmixTemplateParameters['AP_PAYEE_TYPE_OWNER']                = self::OWNER;
		$arrmixTemplateParameters['AP_PAYEE_TYPE_INTERCOMPANY']         = self::INTERCOMPANY;
		$arrmixTemplateParameters['AP_PAYEE_TYPE_PROPERTY_SOLUTIONS']   = self::PROPERTY_SOLUTIONS;
		$arrmixTemplateParameters['AP_PAYEE_TYPE_RESIDENT']             = self::RESIDENT;
		$arrmixTemplateParameters['AP_PAYEE_TYPE_LENDER']               = self::LENDER;

		return $arrmixTemplateParameters;
	}

}
?>