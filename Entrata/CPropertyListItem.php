<?php

class CPropertyListItem extends CBasePropertyListItem {
	protected $m_intListItemId;

	protected $m_strName;
	protected $m_strDefaultCancellationReasonName;

	protected $m_intDefaultListItemId;

	protected $m_boolIsAssociatedWithLeaseProcesses;
	protected $m_boolShowInEntrata;
	protected $m_boolIsSystem;

	/**
	 * Set Functions
	 */

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
	}

	public function setListItemId( $intListItemId ) {
		$this->m_intListItemId = $intListItemId;
	}

	public function setDefaultListItemId( $intDefaultListItemId ) {
		$this->m_intDefaultListItemId = $intDefaultListItemId;
	}

	public function setIsAssociatedWithLeaseProcesses( $boolIsAssociatedWithLeaseProcesses ) {
		$this->m_boolIsAssociatedWithLeaseProcesses = $boolIsAssociatedWithLeaseProcesses;
	}

	public function setShowInEntrata( $boolShowInEntrata ) {
		$this->m_boolShowInEntrata = CStrings::strToBool( $boolShowInEntrata );
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->m_boolIsSystem = CStrings::strToBool( $boolIsSystem );
	}

	public function setDefaultCancellationReasonName( $strDefaultCancellationReasonName ) {
		$this->m_strDefaultCancellationReasonName = CStrings::strTrimDef( $strDefaultCancellationReasonName, 240, NULL, true );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function getIsAssociatedWithLeaseProcesses() {
		return $this->m_boolIsAssociatedWithLeaseProcesses;
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function getDefaultCancellationReasonName() {
		return $this->m_strDefaultCancellationReasonName;
	}

	public function getDefaultListItemId() {
		return $this->m_intDefaultListItemId;
	}

	public function getShowInEntrata() {
		return $this->m_boolShowInEntrata;
	}

	public function getListItemId() {
		return $this->m_intListItemId;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( isset( $arrstrValues['name'] ) && $boolDirectSet )
			$this->m_strName = trim( stripcslashes( $arrstrValues['name'] ) );
		elseif( isset( $arrstrValues['name'] ) )
			$this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['name'] ) : $arrstrValues['name'] );

		if( isset( $arrstrValues['is_associated_with_lease_processes'] ) && $boolDirectSet )
			$this->m_boolIsAssociatedWithLeaseProcesses = trim( stripcslashes( $arrstrValues['is_associated_with_lease_processes'] ) );
		elseif( isset( $arrstrValues['is_associated_with_lease_processes'] ) )
			$this->setIsAssociatedWithLeaseProcesses( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['is_associated_with_lease_processes'] ) : $arrstrValues['is_associated_with_lease_processes'] );

		if( isset( $arrstrValues['show_in_entrata'] ) && $boolDirectSet )
			$this->m_boolShowInEntrata = trim( stripcslashes( $arrstrValues['show_in_entrata'] ) );
		elseif( isset( $arrstrValues['show_in_entrata'] ) )
			$this->setShowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['show_in_entrata'] ) : $arrstrValues['show_in_entrata'] );

		if( true == isset( $arrstrValues['is_system'] ) )									$this->setIsSystem( $arrstrValues['is_system'] );
		if( true == isset( $arrstrValues['list_item_id'] ) )								$this->setListItemId( $arrstrValues['list_item_id'] );
		if( true == isset( $arrstrValues['default_list_item_id'] ) )						$this->setDefaultListItemId( $arrstrValues['default_list_item_id'] );
		if( true == isset( $arrstrValues['default_cancellation_reason_name'] ) )			$this->setDefaultCancellationReasonName( $arrstrValues['default_cancellation_reason_name'] );
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		switch( NULL ) {
			default:
				// Move out reason check
				$arrobjLeaseProcesses          = ( array ) \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByMoveOutReasonListItemIdByPropertyIdByCid( $this->getListItemId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
				$strPropertyMoveOutReasonName  = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemNameByListTypeIdByIdByCid( $this->getListTypeId(), $this->getListItemId(), $this->getCid(), $objDatabase );

				if( true == valArr( $arrobjLeaseProcesses ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property move out reason', $strPropertyMoveOutReasonName . ' Move-out reason is used for ' . \Psi\Libraries\UtilFunctions\count( $arrobjLeaseProcesses ) . ' leases and cannot be deleted from the property. If you do not want to use this Move-out Reason in the future, make it inactive.' ) );
					break;
				}

				// Transfer reason check
				$arrobjLeaseProcesses = ( array ) \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByTransferReasonListItemIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				if( true == valArr( $arrobjLeaseProcesses ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property transfer reason', 'Failed to delete the transfer reason, it is being associated with a schedule transfer.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsExistListItem = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				if( true == $boolIsExistListItem ) {
					$boolIsValid &= $this->valDependantInformation( $objDatabase );
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>
