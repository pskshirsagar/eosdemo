<?php

class CCandidateDetail extends CBaseCandidateDetail {

    public function valId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUsername() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUsername() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getPasswordEncrypted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_encrypted', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMaxLoginAttempts() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMaxLoginAttempts() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_login_attempts', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLoginErrorCount() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getLoginErrorCount() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_error_count', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRequiresPasswordChange() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getRequiresPasswordChange() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requires_password_change', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDontAllowLogin() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDontAllowLogin() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dont_allow_login', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDlNumberEncrypted() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDlNumberEncrypted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dl_number_encrypted', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDlStateCode() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDlStateCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dl_state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDlProvince() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDlProvince() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dl_province', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDigitalSignature() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDigitalSignature() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'digital_signature', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAcceptanceDatetime() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getAcceptanceDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acceptance_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRefusalDatetime() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getRefusalDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refusal_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStartDatetime() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getStartDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApprovalDatetime() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getApprovalDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approval_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApprovalIpAddress() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getApprovalIpAddress() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approval_ip_address', '' ) );
        // }

        return $boolIsValid;
    }

    public function valInquiryBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getInquiryBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inquiry_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valInquiryOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getInquiryOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inquiry_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valInquiryApprovedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        //  if( true == is_null( $this->getInquiryApprovedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inquiry_approved_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valInquiryDeniedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getInquiryDeniedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inquiry_denied_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
         		break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>