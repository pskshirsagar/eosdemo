<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicants
 * Do not add any new functions to this class.
 */

class CApplicants extends CBaseApplicants {

	const TABLE_ORIGINAL_APPLICATANTS 	= 'applicatants';
	const VIEW_APPLICANTS 				= 'view_applicants';

	public static function fetchApplicantsByIdsByCid( $arrintApplicantIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND a.cid = ' . ( int ) $intCid;
		return self::fetchApplicants( $strSql, $objDatabase );
 	}

	public static function fetchAssociatedOccupantsAndGuarantorsCountByApplicationIdByCustomerTypeIds( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
 		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

 		$strSql = ' SELECT
					COUNT ( CASE WHEN
							aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) THEN 1
							END ) as occupants,
					COUNT ( CASE WHEN
							aa.customer_type_id NOT IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) THEN 1
							END ) as guarantors
					FROM
					applicant_applications aa
					WHERE
					aa.application_id::int = ' . ( int ) $intApplicationId . '
					AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

 		$arrstrData = fetchData( $strSql, $objDatabase );

 		if( true == valArr( $arrstrData ) ) {
 			$arrstrResult['occupants'] = $arrstrData[0]['occupants'];
 			$arrstrResult['guarantors'] = $arrstrData[0]['guarantors'];

 			return $arrstrResult;
 		}
 		return 0;
 	}

 	public static function fetchAssociatedInfantsCountByApplicationIdByCustomerTypeIds( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $intNonResponsibleLimit = NULL, $boolIsReturnCountOnly = true ) {
 		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( false == $boolIsReturnCountOnly ) {

			$strSql = '	SELECT
							aa.applicant_id
						FROM
							applicant_applications aa
							JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
						WHERE
							aa.application_id::int = ' . ( int ) $intApplicationId . '
							AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . '
							AND ( EXTRACT( year from age( a.birth_date ) ) * 12 + EXTRACT( month from age( a.birth_date ) )  <= ' . ( int ) $intNonResponsibleLimit . ')  IS TRUE
							AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )';

			$arrstrData = fetchData( $strSql, $objDatabase );

			if( false === valArr( $arrstrData ) ) return 0;

			return array_column( $arrstrData, 'applicant_id' );
		}

 		$strSql = ' SELECT
						COUNT ( CASE WHEN
						EXTRACT( year from age( a.birth_date ) ) * 12 + EXTRACT( month from age( a.birth_date ) ) <= ' . ( int ) $intNonResponsibleLimit . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) THEN 1
						END ) as infants
					FROM
						applicant_applications aa
						JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
					WHERE
						aa.application_id::int = ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

 		$arrstrData = fetchData( $strSql, $objDatabase );

 		if( false === valArr( $arrstrData ) ) return 0;

		return $arrstrData[0];
 	}

	public static function fetchApplicantsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolLoadFromView = true, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( true == $boolLoadFromView ) {
			$strSql = 'SELECT
							va.*,
							aa.customer_type_id,
							aa.customer_relationship_id,
							cr.name AS customer_relationship_name,
							aa.application_form_id,
							aa.application_document_id,
							aa.lease_document_id,
							aa.application_step_id,
							aa.application_id,
							aa.id AS applicant_application_id,
							aa.deleted_on AS applicant_application_deleted_on,
							c.preferred_locale_code
						FROM
							applicant_applications aa
							JOIN view_applicants va ON ( aa.applicant_id = va.id AND aa.cid = va.cid )
							JOIN customer_relationships cr ON ( aa.cid = cr.cid AND aa.customer_relationship_id = cr.id )
							JOIN customers c ON ( c.cid = va.cid AND c.id = va.customer_id ) 
						WHERE
							aa.application_id =  ' . ( int ) $intApplicationId . '
							AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						ORDER BY
							aa.customer_type_id, va.name_first';

		} else {
			$strSql = 'SELECT
							a.*,
							aa.customer_type_id,
							aa.customer_relationship_id,
							aa.application_form_id,
							aa.application_document_id,
							aa.lease_document_id,
							aa.application_step_id,
							aa.application_id,
							aa.id AS applicant_application_id,
							aa.deleted_on AS applicant_application_deleted_on,
							c.preferred_locale_code,
							cpn.phone_number_type_id AS primary_phone_number_type_id,
                            cpn.phone_number AS primary_phone_number
						FROM
							applicant_applications aa
							JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
							JOIN customers c ON ( c.cid = a.cid AND c.id = a.customer_id ) 
							LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
						WHERE
							aa.application_id =  ' . ( int ) $intApplicationId . '
							AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						ORDER BY
							aa.customer_type_id,
							a.name_first';
		}

		$arrintApplicationIds = ( true == valId( $intApplicationId ) ) ? [ $intApplicationId ] : [];

		return self::fetchApplicants( $strSql, $objDatabase, true, $arrintApplicationIds );
	}

	public static function fetchScreenedApplicantsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
							a.*,
							aa.id as applicant_application_id,
							aa.customer_type_id
						FROM
							( SELECT * FROM applicant_applications WHERE application_id::int = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . ' ) as aa
							JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						WHERE
							a.id IN (
								SELECT
									DISTINCT( aa.applicant_id )
								FROM
									applicant_applications aa
									JOIN applicant_application_transmissions aat ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid )
								WHERE
									aat.application_id = ' . ( int ) $intApplicationId . '
									AND aat.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						)
						AND	a.cid = ' . ( int ) $intCid . '';

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchApplicantsByIdsByApplicationIdByCid( $arrintApplicantIds, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$arrintApplicantIds = array_map( '\intval', ( array ) $arrintApplicantIds );
		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
							view_applicants.*,
							aa.customer_type_id,
							aa.application_form_id,
							aa.application_document_id,
							aa.lease_document_id,
							aa.application_step_id,
							aa.customer_relationship_id,
							aa.id as applicant_application_id
					 FROM
					 		view_applicants
							JOIN applicant_applications AS aa ON ( aa.applicant_id = view_applicants.id AND aa.cid = view_applicants.cid ' . $strCheckDeletedAASql . ' )
					WHERE
							view_applicants.id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
					  AND	view_applicants.cid::int = ' . ( int ) $intCid . '
					  AND	aa.application_id::int = ' . ( int ) $intApplicationId;

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchApplicantByIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
							view_applicants.*,
							aa.customer_type_id,
							aa.application_form_id,
							aa.application_document_id,
							aa.lease_document_id,
							aa.application_step_id,
							aa.id as applicant_application_id,
							aa.customer_relationship_id,
							aa.completed_on,
							c.preferred_locale_code,
							aa.application_id,
							' . self::getCustomerAddressLogFields() . '
					 FROM
					 		view_applicants
					JOIN   applicant_applications AS aa ON ( aa.applicant_id = view_applicants.id AND aa.cid = view_applicants.cid ' . $strCheckDeletedAASql . ' )
					JOIN   customers AS c ON ( view_applicants.customer_id = c.id AND c.cid = view_applicants.cid )
					LEFT JOIN application_associations application_association_current ON application_association_current.cid = view_applicants.cid AND application_association_current.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND view_applicants.customer_id = application_association_current.customer_id AND application_association_current.application_id = aa.application_id
					LEFT JOIN customer_address_logs current_address ON view_applicants.cid = current_address.cid AND current_address.customer_id = view_applicants.customer_id AND current_address.address_type_id = ' . CAddressType::CURRENT . ' AND application_association_current.customer_data_type_reference_id = current_address.id
					LEFT JOIN application_associations application_association_previous ON application_association_previous.cid = view_applicants.cid AND application_association_previous.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND view_applicants.customer_id = application_association_previous.customer_id AND application_association_previous.application_id = aa.application_id
					LEFT JOIN customer_address_logs previous_address ON view_applicants.cid = previous_address.cid AND previous_address.customer_id = view_applicants.customer_id AND previous_address.address_type_id = ' . CAddressType::PREVIOUS . ' AND previous_address.id = application_association_previous.customer_data_type_reference_id
					WHERE
							view_applicants.id::int = ' . ( int ) $intApplicantId . '
					  AND	view_applicants.cid::int = ' . ( int ) $intCid . '
					  AND	aa.application_id::int = ' . ( int ) $intApplicationId . '
				 ORDER BY
							view_applicants.name_first limit 1';

		return self::fetchApplicant( $strSql, $objDatabase, [ $intApplicationId ], false );
	}

	public static function fetchSimpleApplicantsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						app.id as application_id,
						aa.customer_type_id as customer_type_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.cid = ' . ( int ) $intCid . ' AND a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( a.cid = ' . ( int ) $intCid . ' AND app.id = aa.application_id AND a.cid = app.cid AND app.lease_id = ' . ( int ) $intLeaseId . ' )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					ORDER BY aa.customer_type_id';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicationIdsByByCustomerTypeIdByCid( $arrintApplicationIds, $intCustomerTypeId, $intCid, $objDatabase, $strOrderName = '', $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$arrintApplicationIds = array_filter( $arrintApplicationIds );

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						appt.*,
						aa.id as applicant_application_id,
						app.property_id,
						appt.username,
						app.id as application_id,
						apptd.primary_phone_number_type_id,
						apptd.secondary_phone_number_type_id,
						apptd.sms_activated_on,
						apptd.sms_cancelled_on,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
					 	applicants appt
					 	JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					 	JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					 	JOIN applicant_details apptd ON ( apptd.applicant_id = appt.id AND apptd.cid = appt.cid )
					 	LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						appt.cid::int = ' . ( int ) $intCid . '
						AND	 aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
					  	AND	aa.customer_type_id::int = ' . ( int ) $intCustomerTypeId;
		if( 'name' == $strOrderName ) {
			$strSql .= ' ORDER BY appt.name_first, appt.name_last';
		}

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicationIdsByCustomerTypeIdsByCid( $arrintApplicationIds, $intCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$arrintApplicationIds = array_filter( $arrintApplicationIds );

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						appt.name_first, 
						appt.name_last,
						appt.username,
						appt.id,
						aa.application_id,
						aa.customer_type_id,
						aa.application_step_id,
						aa.completed_on
					FROM
						applicants appt
						JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						appt.cid::int = ' . ( int ) $intCid . '
						AND	 aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND	aa.customer_type_id IN ( ' . implode( ',', $intCustomerTypeIds ) . ' ) ';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicationIdsByUnitSpaceIdsByCid( $arrintApplicationIds, $arrintUnitSpaceIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;
		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$arrintUnitSpaceIds           = array_filter( $arrintUnitSpaceIds );
		$arrintApplicationIds         = array_filter( $arrintApplicationIds );

		$strCheckDeletedAASql 		  = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
					appt.*,
					aa.id as applicant_application_id,
					app.property_id,
					app.id as application_id,
					us.unit_number_cache,
                    cpn.phone_number AS primary_phone_number,
                    cpn.phone_number_type_id AS primary_phone_number_type_id
				FROM
					applicants appt
					JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					JOIN unit_spaces us ON ( us.application_lease_id = app.lease_id AND us.cid = aa.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
				WHERE appt.cid = ' . ( int ) $intCid . '
					AND aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
					AND us.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					AND aa.customer_type_id::int = ' . ( int ) CCustomerType::PRIMARY . '
				ORDER BY appt.name_first, appt.name_last';

		return  fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantByApplicationIdByCustomerTypeIdByCid( $intApplicationId, $intCustomerTypeId, $intCid, $objDatabase, $boolLoadFromView = true, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( true == $boolLoadFromView ) {
			$strSql = ' SELECT 
							va.*,
							aa.customer_relationship_id,
							aa.application_id,
							' . self::getCustomerAddressLogFields() . '
						FROM view_applicants va
							JOIN applicant_applications aa ON (va.cid = aa.cid and va.id = aa.applicant_id )
							LEFT JOIN application_associations application_association_current ON application_association_current.cid = va.cid AND application_association_current.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_current.customer_id AND application_association_current.application_id = aa.application_id
							LEFT JOIN customer_address_logs current_address ON va.cid = current_address.cid AND current_address.customer_id = va.customer_id AND current_address.address_type_id = ' . CAddressType::CURRENT . ' AND application_association_current.customer_data_type_reference_id = current_address.id
							LEFT JOIN application_associations application_association_previous ON application_association_previous.cid = va.cid AND application_association_previous.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_previous.customer_id AND application_association_previous.application_id = aa.application_id
							LEFT JOIN customer_address_logs previous_address ON va.cid = previous_address.cid AND previous_address.customer_id = va.customer_id AND previous_address.address_type_id = ' . CAddressType::PREVIOUS . ' AND previous_address.id = application_association_previous.customer_data_type_reference_id
						WHERE 
							aa.application_id = ' . ( int ) $intApplicationId . ' AND
							aa.cid = ' . ( int ) $intCid . ' AND
							aa.customer_type_id = ' . ( int ) $intCustomerTypeId . $strCheckDeletedAASql . ' 
						LIMIT 1';
			return self::fetchApplicant( $strSql, $objDatabase, [ $intApplicationId ], false );
		} else {
			 $strSql = 'SELECT
							a.*,
                            cpn.phone_number AS primary_phone_number,
                            cpn.phone_number_type_id AS primary_phone_number_type_id
						FROM
							applicants a
							JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
							LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
						WHERE
							aa.application_id::int = ' . ( int ) $intApplicationId . '
							AND aa.cid = ' . ( int ) $intCid . '
							AND aa.customer_type_id::int = ' . ( int ) $intCustomerTypeId . '
						LIMIT 1';
		}
		return self::fetchApplicant( $strSql, $objDatabase, [ $intApplicationId ] );
	}

	public static function fetchPreExistingDeletedApplicantByApplicationIdByUsernameByFirstAndlastNameByBirthDateByPhoneNumberByCid( $intApplicationId, $strUsername, $strNameFirst, $strNameLast, $strBirthDate, $strPrimaryPhoneNumber, $intCid, $objDatabase ) {

		$strPrimaryPhoneNumber = str_replace( [ '-', '(', ')', '*' ], '', $strPrimaryPhoneNumber );

		$strCondition = '( lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
						AND lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ) . '\' )  ';

		$strInnerCondition = '';
		if( false == is_null( $strUsername ) && 0 < strlen( trim( $strUsername ) ) ) {
			$strInnerCondition .= '(lower(a.username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\') ';
		}

		$strPhoneNumberJoin = 'LEFT JOIN';

		if( false == is_null( $strPrimaryPhoneNumber ) && 0 < strlen( trim( $strPrimaryPhoneNumber ) ) ) {
			$strInnerCondition .= ( ( '' != $strInnerCondition ) ? ' OR ' : '' ) . '( translate(trim(cpn.phone_number),\'()- \',\'\' ) = \'' . addslashes( $strPrimaryPhoneNumber ) . '\' )';
			$strPhoneNumberJoin = 'JOIN';
		}

		if( false == is_null( $strBirthDate ) && 0 < strlen( trim( $strBirthDate ) ) ) {
			$strInnerCondition .= ( ( '' != $strInnerCondition ) ? ' OR ' : '' ) . 'a.birth_date = \'' . ( string ) addslashes( $strBirthDate ) . '\'';
		}

		if( '' != $strInnerCondition ) {
			$strCondition .= ' AND ( ' . $strInnerCondition . ' ) ';
		}

		$strSql = 'SELECT a.*,
						  aa.id as applicant_application_id,
						  aa.customer_type_id,
						  cpn.phone_number_type_id,
						  cpn.phone_number
					FROM
						applicants a
						JOIN applicant_details ad ON ( a.cid = ad.cid AND a.id = ad.applicant_id )
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						' . $strPhoneNumberJoin . ' customer_phone_numbers cpn ON (a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND ( ' . $strCondition . ' )
						AND aa.deleted_on IS NOT NULL
						AND aa.deleted_by IS NOT NULL
					ORDER BY created_on DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchPreExistingApplicantCountByPropertyIdByUsernameByHistoricaldaysByCid( $strUsername, $intPropertyId, $intApplicantId, $intHistoricaldays, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND appt.id != ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT count( appt.id )
					FROM
						applicants appt
						JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
			 		WHERE
						appt.cid::integer = ' . ( int ) $intCid . '::integer
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . $strCondition . '
						AND ( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' )
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
			 			AND trim( both \' \' from lower(username) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) ) . '\'
			 			AND to_char( ca.created_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricaldays . '\' )::date';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPreExistingApplicantCountByApplicationIdByUsernameByCid( $intApplicationId, $strUsername, $intCid, $objDatabase, $intApplicantId = NULL, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND a.id != ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT COUNT( a.id )
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND lower(a.username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\'' . $strCondition;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPreExistingApplicantCountByApplicationIdByTaxNumberByCid( $intApplicationId, $strTaxNumber, $intCid, $objDatabase, $intApplicantId = NULL, $boolIncludeDeletedAA = false, $arrintExcludeOccupantTypes = NULL ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND a.id != ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql       = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strExcludeOccupantTypesSql = ( true == valArr( $arrintExcludeOccupantTypes ) ) ? ' AND aa.customer_type_id NOT IN (' . implode( ',', $arrintExcludeOccupantTypes ) . ') ' : '';

		$strSql = 'SELECT a.*,
                          cpn.phone_number AS primary_phone_number,
                          cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . $strExcludeOccupantTypesSql . ' )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . $strCondition;

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase );

		foreach( $arrobjApplicants as $intApplicant => $objApplicant ) {
			if( $objApplicant->getTaxNumber() != $strTaxNumber ) {
				unset( $arrobjApplicants[$intApplicant] );
			}
		}

		return \Psi\Libraries\UtilFunctions\count( $arrobjApplicants );
	}

	public static function fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByBirthDateByCid( $intApplicationId, $strNameFirst, $strNameLast, $strBirthDate, $intCid, $objDatabase, $intApplicantId = NULL, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND a.id != ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT COUNT( a.id )
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
						AND	lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ) . '\'
						AND a.birth_date = \'' . ( string ) addslashes( $strBirthDate ) . '\'' . $strCondition;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByPhoneNumberByCid( $intApplicationId, $strNameFirst, $strNameLast, $strPhoneNumber, $intCid, $objDatabase, $intApplicantId = NULL, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND a.id != ' . ( int ) $intApplicantId;
		}

		$strPhoneNumber			= preg_replace( '/[^0-9]+/', '', trim( $strPhoneNumber ) );
		$strCheckDeletedAASql	= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT COUNT( a.id )
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.deleted_by IS NULL )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
						AND	lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ) . '\'
						AND (
								 translate(trim(cpn.phone_number),\'()- \',\'\' )   = \'' . addslashes( $strPhoneNumber ) . '\'
							)' . $strCondition;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPreExistingApplicantCountWithPasswordByUsernameByCid( $strUsername, $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND appt.id != ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
							count(1)
					FROM
						applicants appt 
					WHERE
						appt.cid::integer = ' . ( int ) $intCid . '::integer 
						AND appt.password_encrypted IS NOT NULL
						AND trim( both \' \' from lower( appt.username ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) ) . '\'
						and exists ( SELECT 
										1 
									 FROM 
									    applicant_applications aa 
									 WHERE
									    aa.cid = appt.cid 
									    AND appt.id = aa.applicant_id
									    ' . $strCheckDeletedAASql . ' 
									    and exists ( SELECT 
														1
									                 FROM 	
									                    cached_applications ca 
									                 WHERE aa.application_id = ca.id 
									                    AND aa.cid = ca.cid 
									                    AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . $strCondition . ' 							 				 	
													)	
						)
					HAVING 
						COUNT( appt.id ) > 0 LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchSimpleApplicantByIdByCid( $intApplicantId, $intCid, $objDatabase, $boolLoadFromView = true ) {

		if( true == $boolLoadFromView ) {
			$strSql = 'SELECT 
              va.*,
              aa.application_id,
              ' . self::getCustomerAddressLogFields() . '
            FROM view_applicants va
              JOIN applicant_applications aa ON (va.cid = aa.cid and va.id = aa.applicant_id )
              LEFT JOIN application_associations application_association_current ON application_association_current.cid = va.cid AND application_association_current.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_current.customer_id AND application_association_current.application_id = aa.application_id
              LEFT JOIN customer_address_logs current_address ON va.cid = current_address.cid AND current_address.customer_id = va.customer_id AND current_address.address_type_id = ' . CAddressType::CURRENT . ' AND application_association_current.customer_data_type_reference_id = current_address.id
              LEFT JOIN application_associations application_association_previous ON application_association_previous.cid = va.cid AND application_association_previous.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_previous.customer_id AND application_association_previous.application_id = aa.application_id
              LEFT JOIN customer_address_logs previous_address ON va.cid = previous_address.cid AND previous_address.customer_id = va.customer_id AND previous_address.address_type_id = ' . CAddressType::PREVIOUS . ' AND previous_address.id = application_association_previous.customer_data_type_reference_id
           WHERE va.id = ' . ( int ) $intApplicantId . ' AND va.cid = ' . ( int ) $intCid . ' LIMIT 1';

		} else {
			$strSql = 'SELECT * FROM applicants WHERE id = ' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid;
		}

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantCountByIdByCid( $intApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(id)
					FROM
						applicants
					WHERE
						id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIsView = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strTable = ( true == $boolIsView ) ? 'applicants' : 'view_applicants';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (a.id) a.*
					FROM
						' . $strTable . ' a
						JOIN applicant_applications aa ON( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications ap ON( aa.application_id = ap.id AND aa.cid = ap.cid )
					WHERE
						ap.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND a.cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchInactiveApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					a.id,
					a.name_first,
					a.name_middle,
					a.name_last,
					aa.application_id,
					aa.id as applicant_application_id,
					aa.customer_type_id,
					aa.application_step_id,
					aa.customer_relationship_id,
					aa.completed_on,
					aa.primary_is_responsible,
					aa.deleted_on as applicant_application_deleted_on
				FROM applicant_applications AS aa
					JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
				WHERE aa.application_id = ' . ( int ) $intApplicationId . ' AND aa.cid = ' . ( int ) $intCid . ' AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) AND aa.deleted_on is NOT NULL';

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );

	}

	public static function fetchApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolLoadFromView = true, $boolLoadOnlyResponsibleApplicant = false, $boolIncludeDeletedAA = false, $intNonResponsibleLimit = NULL, $boolIsUnderAgeLimit = false, $boolLoadNewApplicantsOnly = false ) {

		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strNonResponsibleAgeLimitSql = '';
		$strWhereCondition = '';
		$strJoinCondition = '';

		if( true == $boolLoadNewApplicantsOnly ) {
			$strWhereCondition = ' AND aa.lease_status_type_id = ' . CLeaseStatusType::APPLICANT;

			$strJoinCondition = 'JOIN applications ap ON( ap.cid = aa.cid AND ap.id = aa.application_id )
								JOIN lease_customers lc ON( lc.cid = aa.cid AND lc.lease_id = ap.lease_id AND a.customer_id = lc.customer_id AND lc.is_ungrouped = 1 )';
		}

		if( true == valId( $intNonResponsibleLimit ) ) {
			$strNonResponsibleAgeLimitSql = ( true == $boolIsUnderAgeLimit ) ? '
				AND CASE a.birth_date
					WHEN
						NULL
					THEN
						NULL
					ELSE
						EXTRACT( year from age( a.birth_date ) ) * 12 + EXTRACT( month from age( a.birth_date ) )
				END < ' . $intNonResponsibleLimit : ' AND ( a.birth_date IS NULL OR ( EXTRACT( year from age( a.birth_date ) ) * 12 + EXTRACT( month from age( a.birth_date ) ) ) >= ' . $intNonResponsibleLimit . ' )';
		}

		$strSql = 'SELECT
						a.id,
						a.name_first,
						a.name_middle,
						a.name_last,
						a.name_last_matronymic,
						func_format_customer_name( a.name_first, a.name_last, a.company_name ) AS applicant_name_full,
						CASE a.birth_date
						WHEN NULL THEN NULL
							ELSE EXTRACT( year from age( a.birth_date ) ) * 12 + EXTRACT( month from age( a.birth_date ) )
						END as birth_months,
						aa.application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id,
						aa.application_step_id,
						aa.customer_relationship_id,
						cr.name AS customer_relationship_name,
						aa.completed_on,
						aa.primary_is_responsible,
						aa.deleted_on as applicant_application_deleted_on,
						a.name_last_matronymic
					FROM
						applicant_applications AS aa
						JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
						JOIN customer_relationships cr ON ( cr.cid = aa.cid AND cr.id = aa.customer_relationship_id )
						' . $strJoinCondition . '
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) ' .
						( ( true == $boolLoadOnlyResponsibleApplicant ) ? 'AND aa.primary_is_responsible = 1 ' : '' ) . $strCheckDeletedAASql . $strNonResponsibleAgeLimitSql . $strWhereCondition;

		$strSql = $strSql . ' ORDER BY aa.customer_type_id;';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixApplicants = [];

		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$arrmixApplicants[$arrmixValues['id']] = $arrmixValues;

				$objDataset->next();
			}
		}

		if( false == valArr( $arrmixApplicants ) ) return NULL;

		if( true == $boolLoadFromView ) {
			$strSql = 'SELECT *
					FROM view_applicants
					WHERE id IN ( ' . implode( ', ', array_keys( $arrmixApplicants ) ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY id';
		} else {
			$strSql = 'SELECT *
					FROM applicants
					WHERE id IN ( ' . implode( ', ', array_keys( $arrmixApplicants ) ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY id';
		}

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );

		$arrobjApplicants = rekeyObjects( 'Id', $arrobjApplicants );

		foreach( $arrobjApplicants as $objApplicant ) {
			if( true == array_key_exists( $objApplicant->getId(), $arrmixApplicants ) ) {
				$objApplicant->setApplicationId( $arrmixApplicants[$objApplicant->getId()]['application_id'] );
				$objApplicant->setApplicantApplicationId( $arrmixApplicants[$objApplicant->getId()]['applicant_application_id'] );
				$objApplicant->setCustomerTypeId( $arrmixApplicants[$objApplicant->getId()]['customer_type_id'] );
				$objApplicant->setApplicationStepId( $arrmixApplicants[$objApplicant->getId()]['application_step_id'] );
				$objApplicant->setCustomerRelationshipId( $arrmixApplicants[$objApplicant->getId()]['customer_relationship_id'] );
				$objApplicant->setCustomerRelationshipName( $arrmixApplicants[$objApplicant->getId()]['customer_relationship_name'] );
				$objApplicant->setCompletedOn( $arrmixApplicants[$objApplicant->getId()]['completed_on'] );
				$objApplicant->setPrimaryIsResponsible( $arrmixApplicants[$objApplicant->getId()]['primary_is_responsible'] );
				$objApplicant->setApplicantApplicationDeletedOn( $arrmixApplicants[$objApplicant->getId()]['applicant_application_deleted_on'] );
				$objApplicant->setBirthMonths( $arrmixApplicants[$objApplicant->getId()]['birth_months'] );
			}
		}

		return $arrobjApplicants;
	}

	public static function fetchPendingApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intRequiredApplicantAge, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $intLeaseIntervalId ) {

		$strApplicantAgeCondition	= ( false == is_null( $intRequiredApplicantAge ) && true == is_numeric( $intRequiredApplicantAge ) ) ? 'AND a.birth_date <= ( NOW() - INTERVAL \'' . ( int ) $intRequiredApplicantAge . ' YEAR\' )' : '';
		$strCheckDeletedAASql		= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						aa.application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id,
						aa.application_step_id,
						aa.completed_on,
						aa.primary_is_responsible
					FROM
						( SELECT * FROM applicant_applications aap WHERE aap.completed_on IS NULL AND aap.application_id = ' . ( int ) $intApplicationId . ' AND aap.cid = ' . ( int ) $intCid . ' AND aap.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )) AS aa,
						applicants a
					WHERE
						a.id = aa.applicant_id ' . $strApplicantAgeCondition . ' AND a.cid = aa.cid AND a.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixApplicants = [];

		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$arrmixApplicants[$arrmixValues['id']] = $arrmixValues;

				$objDataset->next();
			}
		}

		if( false == valArr( $arrmixApplicants ) ) return NULL;

		$strSql = 'SELECT *
					FROM view_applicants
					WHERE id IN ( ' . implode( ', ', array_keys( $arrmixApplicants ) ) . ' ) AND cid = ' . ( int ) $intCid;

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );

		$arrobjApplicants = rekeyObjects( 'Id', $arrobjApplicants );

		if( true == valArr( $arrobjApplicants ) ) {
			$arrintCustomerIds = array_keys( rekeyObjects( 'CustomerId', $arrobjApplicants ) );
			$arrmixEvents = ( array ) \Psi\Eos\Entrata\CEvents::createService()->fetchLatestEventByEventTypeIdByLeaseIntervalIdByCustomerIdsByCid( CEventType::INVITATION_EMAIL, $intLeaseIntervalId, $arrintCustomerIds, $intCid, $objDatabase );
		}

		foreach( $arrobjApplicants as $intApplicantId => $objApplicant ) {
			if( true == valArr( $arrmixEvents ) && true == valArr( $arrmixEvents[$objApplicant->getCustomerId()] ) ) {
				unset( $arrobjApplicants[$intApplicantId] );
				continue;
			}
			if( true == array_key_exists( $objApplicant->getId(), $arrmixApplicants ) ) {
				$objApplicant->setApplicationId( $arrmixApplicants[$objApplicant->getId()]['application_id'] );
				$objApplicant->setApplicantApplicationId( $arrmixApplicants[$objApplicant->getId()]['applicant_application_id'] );
				$objApplicant->setCustomerTypeId( $arrmixApplicants[$objApplicant->getId()]['customer_type_id'] );
				$objApplicant->setApplicationStepId( $arrmixApplicants[$objApplicant->getId()]['application_step_id'] );
				$objApplicant->setCompletedOn( $arrmixApplicants[$objApplicant->getId()]['completed_on'] );
				$objApplicant->setPrimaryIsResponsible( $arrmixApplicants[$objApplicant->getId()]['primary_is_responsible'] );
				$objApplicant->setApplicantApplicationDeletedOn( $arrmixApplicants[$objApplicant->getId()]['applicant_application_deleted_on'] );
			}
		}

		return $arrobjApplicants;
	}

	public static function fetchPendingLeaseApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intRequiredApplicantAge, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strApplicantAgeCondition 	= ( false == is_null( $intRequiredApplicantAge ) && true == is_numeric( $intRequiredApplicantAge ) ) ? 'AND a.birth_date <= ( NOW() - INTERVAL \'' . ( int ) $intRequiredApplicantAge . ' YEAR\' )' : '';
		$strCheckDeletedAASql 		= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						aa.application_id,
						aa.id AS applicant_application_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						aa.application_id = ' . ( int ) $intApplicationId . '
						AND a.cid = ' . ( int ) $intCid . '
						AND aa.completed_on IS NOT NULL
						AND aa.lease_generated_on IS NULL
						AND aa.lease_signed_on IS NULL
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )';

		$strSql .= $strApplicantAgeCondition . $strCheckDeletedAASql;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	// This is only used in screening.  If we can avoid the view here, it would be really great.

	public static function fetchUsApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeAllApplicants = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strUsApplicantSql = ( false == $boolIncludeAllApplicants ) ? ' AND	( ad.is_alien = 0 OR ad.is_alien IS NULL )' : '';

		$strSql = 'SELECT
						a.*,
						app.id as application_id,
						aa.customer_type_id,
						aa.application_step_id as application_step_id,
						aa.primary_is_responsible,
						aa.id as applicant_application_id,
						ad.is_alien as is_alien,
						cal.street_line1 AS current_street_line1,
						cal.street_line2 AS current_street_line2,
						cal.street_line3 AS current_street_line3,
						cal.city AS current_city,
						cal.state_code AS current_state_code,
						cal.postal_code AS current_postal_code,
						cal.country_code AS current_country_code,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_details ad ON ( a.id = ad.applicant_id AND a.cid = ad.cid )
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
						LEFT JOIN application_associations aas ON ( aas.cid = app.cid AND aas.application_id = app.id AND aas.customer_id = a.customer_id )
						LEFT JOIN customer_address_logs cal ON ( aas.cid = cal.cid AND aas.customer_data_type_reference_id = cal.id AND cal.address_type_id = ' . CAddressType::CURRENT . ' )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
			 		WHERE
		   				aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
		   				' . $strUsApplicantSql . '
						AND	aa.application_id = ' . ( int ) $intApplicationId . ' AND aa.cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantByUsernameByPasswordByCid( $strUsername, $strPasswordEncrypted, $intCid, $objDatabase ) {
		if ( true == is_null( trim( $strUsername ) ) ) return NULL;
		if ( true == is_null( trim( $strPasswordEncrypted ) ) ) return NULL;

	 $strSql = 'SELECT 	va.*,' . self::getCustomerAddressLogFields() . '
             FROM view_applicants va
              JOIN applicant_applications aa ON (va.cid = aa.cid and va.id = aa.applicant_id )
              LEFT JOIN application_associations application_association_current ON application_association_current.cid = va.cid AND application_association_current.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_current.customer_id AND application_association_current.application_id = aa.application_id LEFT JOIN customer_address_logs current_address ON va.cid = current_address.cid AND current_address.customer_id = va.customer_id AND current_address.address_type_id = ' . CAddressType::CURRENT . ' AND application_association_current.customer_data_type_reference_id = current_address.id
              LEFT JOIN application_associations application_association_previous ON application_association_previous.cid = va.cid AND application_association_previous.customer_data_type_id = ' . CCustomerDataType::CUSTOMER_ADDRESS . ' AND va.customer_id = application_association_previous.customer_id AND application_association_previous.application_id = aa.application_id LEFT JOIN customer_address_logs previous_address ON va.cid = previous_address.cid AND previous_address.customer_id = va.customer_id AND previous_address.address_type_id = ' . CAddressType::PREVIOUS . ' AND previous_address.id = application_association_previous.customer_data_type_reference_id WHERE lower( va.username ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' AND va.password_encrypted = \'' . trim( $strPasswordEncrypted ) . '\' AND va.cid = \'' . ( int ) $intCid . '\' ORDER BY va.updated_on DESC LIMIT 1;';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByQuoteIdByPropertyIdByPasswordByCid( $intQuoteId, $intPropertyId, $strPasswordEncrypted, $intCid, $objDatabase ) {
		if ( true == is_null( $intQuoteId ) ) return NULL;
		if ( true == is_null( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
					    applicants a
					    JOIN cached_applications ca ON( a.id = ca.primary_applicant_id AND a.cid = ca.cid )
    					JOIN quotes q ON( q.application_id = ca.id  AND q.cid = ca.cid )
    					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
					    q.id = ' . ( int ) $intQuoteId . '
					    AND q.cid = ' . ( int ) $intCid . '
					    AND q.expires_on > NOW()
						AND q.cancelled_by IS NULL
						AND q.cancelled_on IS NULL
					    AND ca.property_id = ' . ( int ) $intPropertyId .
					    ( ( true == valStr( $strPasswordEncrypted ) && false == is_null( trim( $strPasswordEncrypted ) ) ) ? ' AND a.password_encrypted = \'' . trim( $strPasswordEncrypted ) . '\'' : '' ) . '
					    AND a.username IS NOT NULL ';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByQuoteIdByPropertyIdsByPasswordByCid( $intQuoteId, $arrintPropertyIds, $strPasswordEncrypted, $intCid, $objDatabase ) {
		if ( true == is_null( $intQuoteId ) ) return NULL;
		if ( true == is_null( $intCid ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					    a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
					    applicants a
					    JOIN cached_applications ca ON( a.id = ca.primary_applicant_id AND a.cid = ca.cid )
    					JOIN quotes q ON( q.application_id = ca.id  AND q.cid = ca.cid )
    					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
					    q.id = ' . ( int ) $intQuoteId . '
					    AND q.cid = ' . ( int ) $intCid . '
					    AND q.expires_on > NOW()
						AND q.cancelled_by IS NULL
						AND q.cancelled_on IS NULL
					    AND ca.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
		          ( ( true == valStr( $strPasswordEncrypted ) && false == is_null( trim( $strPasswordEncrypted ) ) ) ? ' AND a.password_encrypted = \'' . trim( $strPasswordEncrypted ) . '\'' : '' ) . '
					    AND a.username IS NOT NULL ';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByUsernameByCid( $strUsername, $intCid, $objDatabase, $boolReturnData = false, $boolIncludeDeletedAA = false, $boolShowLeaseStatus = false ) {

		$strSubQuery = $strSelectFields = $strSubSelectFields = NULL;

		if ( true == is_null( trim( $strUsername ) ) ) return NULL;
		if ( true == is_null( $intCid ) ) return NULL;

		$strApplicantDatasql	= ( true == $boolReturnData ) ? 'a.name_first,a.name_last,a.username' : 'a.*';
		$strCheckDeletedAASql	= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( true == $boolShowLeaseStatus ) {
			$strSubQuery = 'LEFT JOIN cached_leases cl ON ( cl.cid = ca.cid AND cl.property_id = ca.property_id AND cl.id = ca.lease_id )' . // Dead
							'LEFT JOIN cached_leases cl1 ON ( cl.cid = cl1.cid AND cl.transfer_lease_id = cl1.id AND cl.transferred_on IS NULL )'; // Surviving

			$strSelectFields = 'apps.application_stage_id,
								apps.application_status_id,
								apps.lease_interval_type_id,
								apps.lease_status_type_id,
								apps.lease_status_type,
								apps.termination_list_type_id,
								apps.lease_id,
								apps.transfer_type,';

			$strSubSelectFields = ', cl.lease_status_type_id,
									cl.lease_status_type,
									cl.termination_list_type_id,
									cl.id AS lease_id,
									CASE WHEN cl.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' ) THEN
											CASE WHEN cl.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' AND cl1.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' THEN ' .
													CLease::TRANSFER_TYPE_UNIT_TRANSFER . ' -- notice scheduled transfer
												 WHEN cl1.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' THEN ' .
													CLease::TRANSFER_TYPE_RENEWAL_TRANSFER . ' -- Renewal shceduled_tansfer
											END
										WHEN cl.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintMoveinPendingLeaseStatusTypeIds ) . ' ) THEN
											CASE WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' AND cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' THEN ' .
													CLease::TRANSFER_TYPE_UNIT_TRANSFER . ' -- notice scheduled transfer
												WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' THEN ' .
													CLease::TRANSFER_TYPE_RENEWAL_TRANSFER . ' -- Renewal shceduled_tansfer
											END
									END AS transfer_type';
		}

		// is blanked field is used just to hide the records on screen.
		$strSql = 'SELECT
						' . $strApplicantDatasql . ',
						aa.application_id, ' .
						$strSelectFields . '
						p.property_name,
						p.id property_id,
						( ass.stage_name || \' \' || ass.status_name ) as application_status,
						aa.customer_type_id,
						( CASE

							WHEN lower( a.username ) LIKE \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '%\'

							THEN 1
							ELSE 0
						  END ) is_blanked
					FROM
						( SELECT DISTINCT ( aa.application_id ),
								ca.cid,
								ca.property_id,
								ca.application_stage_id,
								ca.application_status_id,
								li.lease_interval_type_id ' .
								$strSubSelectFields . '
							FROM
								applicant_applications aa
								JOIN cached_applications ca ON ( ca.id = aa.application_id AND ca.cid = aa.cid )
								JOIN lease_intervals li ON ( li.cid = ca.cid AND li.id = ca.lease_interval_id )
								JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid ) ' .
								$strSubQuery . '
							WHERE

								lower( username ) LIKE \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '%\'

								AND a.cid = ' . ( int ) $intCid . '
						) as apps
						JOIN applicant_applications aa ON ( aa.application_id = apps.application_id AND aa.cid = apps.cid )
						JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						JOIN properties p ON ( p.id = apps.property_id AND p.cid = apps.cid )
						JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = apps.lease_interval_type_id AND ass.application_stage_id = apps.application_stage_id AND ass.application_status_id = apps.application_status_id )
					WHERE
						aa.application_id IN ( apps.application_id )
						AND a.cid = ' . ( int ) $intCid . '
						AND ( aa.customer_type_id = ' . CCustomerType::PRIMARY . ' OR lower( a.username ) LIKE \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '%\' )
						 ' . $strCheckDeletedAASql . '
					ORDER BY a.id DESC LIMIT 10';

		return ( true == $boolReturnData ) ? fetchData( $strSql, $objDatabase ) : self::fetchApplicants( $strSql, $objDatabase );

	}

	public static function fetchApplicantsByFirstNameByLastNameByUsernameByCustomerTypesByCid( $strNameFirst, $strNameLast, $strUsername, $arrstrCustomerTypes, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCondition = '';
		if ( true == is_null( trim( $strUsername ) ) ) return NULL;
		if ( true == is_null( $intCid ) ) return NULL;
		if ( 0 < strlen( trim( $strNameFirst ) ) ) $strCondition = ' AND  lower (name_first) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strNameFirst ) ) ) . '\'';
		if ( 0 < strlen( trim( $strNameLast ) ) ) $strCondition .= ' AND  lower (name_last) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strNameLast ) ) ) . '\'';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql  = 'SELECT 
						ap.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id 
					FROM 
						applicants ap
						LEFT JOIN customer_phone_numbers cpn ON ( ap.cid = cpn.cid AND ap.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL ) 
						WHERE ap.id IN
						(
							SELECT MAX(a.id) as id
							FROM
								applicant_applications aa
							 	INNER JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
							WHERE
							 	lower(username) =\'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\'
							 	AND a.cid = \'' . ( int ) $intCid . '\'
							 	AND aa.customer_type_id IN ( ' . implode( ',', $arrstrCustomerTypes ) . ' ) ' . $strCondition . '
							 	' . $strCheckDeletedAASql . '
							 	GROUP BY a.username,a.name_first,a.name_last
						) AND ap.cid = ' . ( int ) $intCid . ' ORDER BY id DESC';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.id,
						a.name_first,
						a.name_last,
						a.username,
						a.tax_number_encrypted
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE
						aa.application_id = ' . ( int ) $intApplicationId . ' AND aa.cid = ' . ( int ) $intCid . '

					ORDER BY id';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchPrimaryApplicantByIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*, aa.completed_on as completed_on, ad.is_alien, aa.id as applicant_application_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
						JOIN applicant_details ad ON ( ad.applicant_id = aa.applicant_id AND ad.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
			 	   WHERE
			 			app.id = ' . ( int ) $intApplicationId . '
				 		AND a.cid = ' . ( int ) $intCid . '
				 		AND aa.customer_type_id IN ( ' . CCustomerType::PRIMARY . ' ) LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByUsernameByFirstNameByLastNameByHistoricaldaysByCid( $strNameFirst, $strNameLast, $strUsername, $intHistoricaldays, $intCid, $objDatabase, $arrintApplicationStageStatusIds = NULL, $boolIncludeDeletedAA = false ) {

 		$arrintApplicationStageStatusIds = ( false == is_null( $arrintApplicationStageStatusIds ) && true == valArr( $arrintApplicationStageStatusIds ) ) ? $arrintApplicationStageStatusIds : CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds;

		$strHistoricalDaysSql = '';

		if( false == is_null( $intHistoricaldays ) && 0 < $intHistoricaldays ) {
			$strHistoricalDaysSql = " AND to_char( COALESCE( ca.application_datetime, ca.created_on ), 'MM/DD/YYYY' )::date >= ( CURRENT_DATE - integer '" . ( int ) $intHistoricaldays . "' )::date ";
		}

		$strUsernamesql = ( 0 == strlen( $strUsername ) ) ? 'a.username IS NULL AND ' : ( ' lower(a.username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\' AND ' );

		$strNameLastsql = ( 0 == strlen( $strNameLast ) ) ? 'a.name_last IS NULL' : ( ' lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'' );

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*, ca.lease_interval_type_id
					FROM
						view_applicants a,
						applicant_applications aa,
						cached_applications ca
					WHERE

						' . $strUsernamesql . $strNameLastsql . '
					AND substr(lower(a.name_first), 1, 3) = \'' . addslashes( \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtolower( $strNameFirst ), 0, 3 ) ) . '\'
					AND a.cid = ' . ( int ) $intCid . '	AND aa.applicant_id = a.id AND aa.cid = a.cid
					AND aa.application_id = ca.id AND aa.cid = ca.cid ' . $strHistoricalDaysSql . '
					AND ( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					 ' . $strCheckDeletedAASql . '
					ORDER BY
						ca.lease_interval_type_id DESC, a.created_on DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantWithPasswordByUsernameByFirstNameByLastNameByHistoricalDaysByCid( $strNameFirst, $strNameLast, $strUsername, $intHistoricaldays, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strHistoricalDaysSql = '';

		if( false == is_null( $intHistoricaldays ) && 0 < $intHistoricaldays ) {
			$strHistoricalDaysSql = " AND to_char( COALESCE( ca.application_datetime, ca.created_on ), 'MM/DD/YYYY' )::date >= ( CURRENT_DATE - integer '" . ( int ) $intHistoricaldays . "' )::date ";
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
	 					a.*, ca.lease_interval_type_id
	 				FROM
	 					view_applicants a,
						applicant_applications aa,
						cached_applications ca
				   WHERE
						lower(a.username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\'
					 	AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
					 	AND	substr(lower(a.name_first), 1, 3) = \'' . addslashes( \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtolower( $strNameFirst ), 0, 3 ) ) . '\'
					 	AND	a.cid = ' . ( int ) $intCid . '
	 				 	AND	aa.applicant_id = a.id
			 			AND aa.cid = a.cid
	 				 	AND aa.application_id = ca.id AND aa.cid = ca.cid ' . $strHistoricalDaysSql . '
	 				 	AND
	 				 	 	CASE
								WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND ca.application_stage_id <> ' . CApplicationStage::PRE_APPLICATION . '
									THEN a.password_encrypted IS NOT NULL
									ELSE 1 = 1
							END
						 ' . $strCheckDeletedAASql . '
				   ORDER BY ca.lease_interval_type_id DESC, a.created_on DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
		                aa.is_lead,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
                        LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL ),
						applicant_applications aa 
					WHERE
						a.id = aa.applicant_id
						AND a.cid = aa.cid
						AND	aa.id = ' . ( int ) $intApplicantApplicationId . '
						AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicantsByIdsByCid( $arrintApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;
		$strSql = 'SELECT * FROM view_applicants WHERE id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicantsByIds( $arrintApplicantIds, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;
		$strSql = 'SELECT * FROM view_applicants WHERE id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) ';
		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchLeaseAgreementIssuedApplicantByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM applicants WHERE  cid = ' . ( int ) $intCid . ' AND  originating_customer_id = ' . ( int ) $intCustomerId . ' ORDER BY created_on DESC LIMIT 1';
		return self::fetchApplicant( $strSql, $objDatabase );
	}

	// This function only gets people who have their information completely filled out. (step = confirmation)

	public static function fetchApplicantsByApplicationIdByCustomerTypeIdsByApplicationStepIdByCid( $intApplicationId, $arrintCustomerTypeIds, $intApplicationStepId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						aa.application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id,
						aa.application_step_id,
						aa.completed_on,
						aa.primary_is_responsible,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						( SELECT * FROM applicant_applications WHERE application_id = ' . ( int ) $intApplicationId . ' AND application_step_id = ' . ( int ) $intApplicationStepId . ' AND customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) AND cid = ' . ( int ) $intCid . ') AS aa,
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.id = aa.applicant_id AND a.cid = aa.cid
						' . $strCheckDeletedAASql;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixApplicants = [];

		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$arrmixApplicants[$arrmixValues['id']] = $arrmixValues;

				$objDataset->next();
			}
		}

		if( false == valArr( $arrmixApplicants ) ) return NULL;

		$strSql = 'SELECT *
					FROM view_applicants
					WHERE id IN ( ' . implode( ', ', array_keys( $arrmixApplicants ) ) . ' ) AND cid = ' . ( int ) $intCid;

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );

		$arrobjApplicants = rekeyObjects( 'Id', $arrobjApplicants );

		foreach( $arrobjApplicants as $objApplicant ) {
			if( true == array_key_exists( $objApplicant->getId(), $arrmixApplicants ) ) {
				$objApplicant->setApplicationId( $arrmixApplicants[$objApplicant->getId()]['application_id'] );
				$objApplicant->setApplicantApplicationId( $arrmixApplicants[$objApplicant->getId()]['applicant_application_id'] );
				$objApplicant->setCustomerTypeId( $arrmixApplicants[$objApplicant->getId()]['customer_type_id'] );
				$objApplicant->setApplicationStepId( $arrmixApplicants[$objApplicant->getId()]['application_step_id'] );
				$objApplicant->setCompletedOn( $arrmixApplicants[$objApplicant->getId()]['completed_on'] );
				$objApplicant->setPrimaryIsResponsible( $arrmixApplicants[$objApplicant->getId()]['primary_is_responsible'] );
			}
		}

		return $arrobjApplicants;
	}

	public static function fetchApplicantsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( array_filter( $arrintApplicantApplicationIds ) ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						aa.application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id,
						aa.customer_relationship_id,
						aa.application_step_id,
						aa.completed_on,
						aa.primary_is_responsible,
						aa.send_consumer_report
					FROM
						applicant_applications aa,
						applicants a
					WHERE
						aa.applicant_id = a.id
						AND aa.cid = a.cid
						AND	a.cid = ' . ( int ) $intCid . '
					  	AND aa.id IN (' . implode( ',', $arrintApplicantApplicationIds ) . ')'
					  	. $strCheckDeletedAASql;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixApplicants = [];

		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$arrmixApplicants[$arrmixValues['id']] = $arrmixValues;

				$objDataset->next();
			}
		}

		if( false == valArr( $arrmixApplicants ) ) return NULL;

		$strSql = 'SELECT *
					FROM view_applicants
					WHERE id IN ( ' . implode( ', ', array_keys( $arrmixApplicants ) ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY id';

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase, true, array_keys( rekeyArray( 'application_id', $arrmixApplicants ) ) );

		$arrobjApplicants = rekeyObjects( 'Id', $arrobjApplicants );

		foreach( $arrobjApplicants as $objApplicant ) {
			if( true == array_key_exists( $objApplicant->getId(), $arrmixApplicants ) ) {
				$objApplicant->setApplicationId( $arrmixApplicants[$objApplicant->getId()]['application_id'] );
				$objApplicant->setApplicantApplicationId( $arrmixApplicants[$objApplicant->getId()]['applicant_application_id'] );
				$objApplicant->setCustomerTypeId( $arrmixApplicants[$objApplicant->getId()]['customer_type_id'] );
				$objApplicant->setApplicationStepId( $arrmixApplicants[$objApplicant->getId()]['application_step_id'] );
				$objApplicant->setCustomerRelationshipId( $arrmixApplicants[$objApplicant->getId()]['customer_relationship_id'] );
				$objApplicant->setCompletedOn( $arrmixApplicants[$objApplicant->getId()]['completed_on'] );
				$objApplicant->setPrimaryIsResponsible( $arrmixApplicants[$objApplicant->getId()]['primary_is_responsible'] );
				$objApplicant->setSendConsumerReport( $arrmixApplicants[$objApplicant->getId()]['send_consumer_report'] );
			}
		}
		return $arrobjApplicants;
	}

	public static function fetchMatchingApplicantsByApplicantInfoByCid( $strApplicantInfo, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( true == is_null( $strApplicantInfo ) || 0 == strlen( trim( $strApplicantInfo ) ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.id, a.cid,
						a.name_first,
						a.name_last,
						a.email_address,
						a.username,
						ct.name as customer_type,
						aa.customer_type_id,
						aa.id as applicant_application_id,
						ap.updated_on,
						ap.id as application_id,
						ap.property_id,
						( ass.stage_name || \' \' || ass.status_name ) as application_status
					FROM
						 applicants a
						 JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						 JOIN applications ap ON ( ap.id = aa.application_id AND ap.cid = aa.cid )
						 JOIN customer_types ct ON ( ct.id = aa.customer_type_id )
 					     JOIN lease_intervals li ON ( li.cid = ap.cid AND li.id = ap.lease_interval_id )
						 JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = li.lease_interval_type_id AND ass.application_stage_id = ap.application_stage_id AND ass.application_status_id = ap.application_status_id )
						 JOIN customer_phone_numbers cpn ON ( cpn.customer_id = a.customer_id AND cpn.cid = a.cid )
					WHERE
						 ap.property_id IS NOT NULL

					 AND a.cid = ' . ( int ) $intCid . '
					 AND (	regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' ) LIKE \'%' . addslashes( $strApplicantInfo ) . '%\' )
					 AND cpn.deleted_on IS NULL
					 LIMIT 10';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchLatestGuarantorIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND aa.customer_type_id = ' . CCustomerType::GUARANTOR . '
					   	AND aa.application_id = ' . ( int ) $intApplicationId . '
					   	AND aa.deleted_on IS NULL
					   	AND aa.deleted_by IS NULL
					ORDER BY aa.created_on DESC
					LIMIT 1';

		return parent::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchApplicantsByCidByIds( $intCid, $arrintApplicantIds, $objDatabase, $boolLoadFromView = false ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		if( false == $boolLoadFromView ) {
			$strSql = 'SELECT * FROM applicants WHERE id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND cid = ' . ( int ) $intCid;
		} else {
			$strSql = 'SELECT * FROM view_applicants WHERE id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND cid = ' . ( int ) $intCid;
		}

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByByPhoneNumbersCid( $arrintPhoneNumbers, $intCid, $objDatabase ) {

			if( false == valArr( $arrintPhoneNumbers ) ) return NULL;

			$strSql = 'SELECT
						phone_number, COUNT( phone_number ) AS count_phone_number,
							CASE WHEN COUNT( phone_number ) > 1 THEN 1
							ELSE 0 END as is_multiple_records
						FROM
							applicants
						WHERE cid = ' . ( int ) $intCid . ' AND
							phone_number IN	( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )
					GROUP BY phone_number';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByAllPhoneNumbersCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						phone_number, COUNT( phone_number ) AS count_phone_number,
							CASE WHEN COUNT( phone_number ) > 1 THEN 1
							ELSE 0 END as is_multiple_records
						FROM
							applicants

						WHERE cid = ' . ( int ) $intCid . '
					GROUP BY phone_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByMobileNumbersByCid( $arrintPhoneNumbers, $intCid, $objDatabase ) {

			if( false == valArr( $arrintPhoneNumbers ) ) return NULL;

			$strSql = 'SELECT
						mobile_number, COUNT( mobile_number ) AS count_mobile_number,
							CASE WHEN COUNT( mobile_number ) > 1 THEN 1
							ELSE 0 END as is_multiple_records
						FROM
							applicants
						WHERE cid = ' . ( int ) $intCid . ' AND
							mobile_number IN	( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )
					GROUP BY mobile_number';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByWorkNumbersByCid( $arrintPhoneNumbers, $intCid, $objDatabase ) {

			if( false == valArr( $arrintPhoneNumbers ) ) return NULL;

			$strSql = 'SELECT
						work_number, COUNT( work_number ) AS count_work_number,
							CASE WHEN COUNT( work_number ) > 1 THEN 1
							ELSE 0 END as is_multiple_records
						FROM
							applicants
						WHERE cid = ' . ( int ) $intCid . ' AND
							work_number IN	( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )
					GROUP BY work_number';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByCallNumberByCid( $intCallNumber, $intCid, $objDatabase ) {

			if( false == valId( $intCallNumber ) ) return NULL;

			$strSql = 'SELECT
						    array_to_string( array_agg( a.id ), \',\' ) as applicant_ids,
						    CASE WHEN COUNT( a.id ) > 1 THEN 1
							ELSE 0 END as is_multiple_records,
						    cpn.phone_number
						FROM
						    applicants a
						WHERE
						    a.cid = ' . ( int ) $intCid . '
						    AND ( length( regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' ) ) > 0 AND length( regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' ) ) < 15 AND regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' )::bigint = ( ' . $intCallNumber . ' ) )
						GROUP BY
						    cpn.phone_number';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantByEventTypeIdByDataReferenceIdByCid( $intEventTypeId, $intDataReferenceId, $intCid, $objDatabase ) {

			$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN events e ON ( a.cid = e.cid AND a.customer_id = e.customer_id AND e.event_type_id = ' . ( int ) $intEventTypeId . ' )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						e.cid = ' . ( int ) $intCid . '
						AND e.data_reference_id = ' . ( int ) $intDataReferenceId;

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicantByPhoneNumberByCid( $intPhoneNumber, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( true == is_null( $intPhoneNumber ) || false == valStr( $intPhoneNumber ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						aa.applicant_id,
						aa.application_id,
						app.property_id,
						app.lease_interval_id,
						app.application_datetime,
						app.updated_on,
						a.name_first,
						a.name_last
					FROM
						applicants AS a
						JOIN applicant_applications AS aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
						JOIN customer_phone_numbers cpn ON cpn.customer_id = a.customer_id
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND cpn.phone_number = \'' . addslashes( $intPhoneNumber ) . '\'
						AND cpn.deleted_on IS NULL
						LIMIT 1';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicantByPhoneNumbersByCid( $arrstrPhoneNumbers, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrstrPhoneNumbers ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						aa.applicant_id,
						aa.application_id,
						app.application_datetime,
						app.updated_on,
						a.name_first,
						a.name_last,
						cpn.phone_number,
						app.property_id
					FROM
						applicants AS a
						JOIN applicant_applications AS aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
						JOIN customer_phone_numbers cpn ON ( cpn.customer_id = a.customer_id AND cpn.cid = a.cid )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND cpn.deleted_on IS NULL
						AND cpn.phone_number IN ( \'' . implode( '\',\'', $arrstrPhoneNumbers ) . '\' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleAllApplicantByPhoneNumbersByCid( $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						aa.applicant_id,
						aa.application_id,
						app.application_datetime,
						app.updated_on,
						a.name_first,
						a.name_last,
						a.phone_number
					FROM
						applicants AS a
						JOIN applicant_applications AS aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE a.cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT COUNT ( a.id ) AS applicant_count
				 	 FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications ap ON ( ap.id = aa.application_id AND ap.cid = aa.cid )
					 WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrData[0]['applicant_count'] ) ) {
			return $arrstrData[0]['applicant_count'];
		}

		return NULL;
	}

	public static function fetchUnApprovedApplicantsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
					a.*,
					aa.application_id,
					aa.id as applicant_application_id,
					aa.customer_type_id,
                    cpn.phone_number AS primary_phone_number,
                    cpn.phone_number_type_id AS primary_phone_number_type_id
				  FROM
					applicants a
					JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) ' . $strCheckDeletedAASql . ' )
					LEFT JOIN applicant_application_transmissions AS aat ON ( aat.applicant_application_id = aa.id AND aat.cid = aa.cid )
					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
				  WHERE
					aa.application_id = ' . ( int ) $intApplicationId . '
					AND aa.cid = ' . ( int ) $intCid . '
					AND ( aat.transmission_response_type_id IS NULL OR aat.transmission_response_type_id not in(' . implode( ',', [ CTransmissionResponseType::APPROVED, CTransmissionResponseType::SUCCESSFUL, CTransmissionResponseType::DENIED ] ) . ' ) )';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantIdsByApplicantApplicationIdsByApplicationIdByCid( $arrintApplicantApplicationIds, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND deleted_on IS NULL' : '';

		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						array_to_string( array_agg("applicant_id"), \', \') AS applicant_ids
					FROM
						applicant_applications
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApplicantByApplicationIdByCustomerIdByCid( $intApplicationId, $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		// -> Use DISTINCT for fixing bad data, These bad data was arieses while issue in integration > AMSI call.
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON ( a.cid, a.customer_id ) a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND a.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . ' )
						JOIN applications apps ON ( apps.id = aa.application_id AND apps.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						apps.id = ' . ( int ) $intApplicationId . '
						AND a.customer_id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid . '
					ORDER BY
						a.cid,
						a.customer_id,
						a.id DESC';
		return self::fetchApplicant( $strSql, $objDatabase, [ $intApplicationId ] );
	}

	public static function fetchViewApplicantByApplicationIdByCustomerIdByCid( $intApplicationId, $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		// -> Use DISTINCT for fixing bad data, These bad data was arieses while issue in integration > AMSI call.
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON ( a.cid, a.customer_id ) a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						view_applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND a.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . ' )
						JOIN applications apps ON ( apps.id = aa.application_id AND apps.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						apps.id = ' . ( int ) $intApplicationId . '
						AND a.customer_id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid . '
					ORDER BY
						a.cid,
						a.customer_id,
						a.id DESC';

		return self::fetchApplicant( $strSql, $objDatabase, [ $intApplicationId ] );
	}

	public static function fetchApplicantByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT va.*
					FROM
						view_applicants va
						JOIN applicant_applications aa ON ( aa.cid = va.cid AND aa.applicant_id = va.id ' . $strCheckDeletedAASql . ' )
						JOIN applications ap ON ( ap.cid = aa.cid AND ap.id = aa.application_id )
					WHERE
						va.customer_Id = ' . ( int ) $intCustomerId . '
						AND va.cid = ' . ( int ) $intCid . '
						AND ap.lease_id = ' . ( int ) $intLeaseId . ' ORDER BY va.id DESC LIMIT 1 ';

		return self::fetchApplicant( $strSql, $objDatabase );

	}

	public static function fetchApplicantsByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT 
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.cid = a.cid AND aa.applicant_id = a.id ' . $strCheckDeletedAASql . ' )
						JOIN applications ap ON ( ap.cid = aa.cid AND ap.id = aa.application_id )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.customer_Id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid . '
						AND ap.lease_id = ' . ( int ) $intLeaseId . ' ORDER BY a.id DESC';

		return self::fetchApplicants( $strSql, $objDatabase );

	}

	public static function fetchExistingApplicantByUsernameByCid( $strUsername, $intCid, $objDatabase ) {

		$strSql = 'SELECT
	 					*
	 				FROM
	 					view_applicants
	  				WHERE
						cid = ' . ( int ) $intCid . '
						AND lower(username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\' ORDER BY id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantWithPasswordByUsernameByCid( $strUsername, $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCondition = '';

		if( false == is_null( $intApplicantId ) && true == is_numeric( $intApplicantId ) ) {
			$strCondition = ' AND appt.id = ' . ( int ) $intApplicantId;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT 
						appt.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants appt
						JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
			 		WHERE appt.cid::integer = ' . ( int ) $intCid . '::integer
			 			AND trim( both \' \' from lower( appt.username ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) ) . '\'
			 			AND appt.password_encrypted IS NOT NULL
			 			AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . $strCondition;

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchSkippedApplicantsByApplicationIdsByUnitSpaceIdByCid( $arrintApplicationIds, $intUnitSpaceId, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedAASql    	  = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlanSql  = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
		  				apl.id,
						COALESCE( apl.name_first::text || \' \', \' \' ) || COALESCE( apl.name_middle::TEXT || \' \',\' \' ) || apl.name_last applicant_name,
						aa.application_id,
						pu.property_floorplan_id as floor_id,
						us.id as space_id
					FROM
						applications ap
						JOIN applicant_applications aa ON ( aa.cid = ap.cid AND aa.application_id = ap.id ' . $strCheckDeletedAASql . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						LEFT JOIN application_floorplans af ON ( af.cid = ap.cid AND af.application_id = ap.id )
						LEFT JOIN property_floorplans pf ON ( pf.cid = ap.cid AND ( pf.id = ap.property_floorplan_id OR af.property_floorplan_id = pf.id OR ( pf.number_of_bathrooms = ap.desired_bathrooms AND pf.number_of_bedrooms = ap.desired_bedrooms ) ) ' . $strCheckDeletedFloorPlanSql . ' )
						JOIN property_units pu ON ( pu.cid = apl.cid AND ( pu.property_floorplan_id = ap.property_floorplan_id OR pu.property_floorplan_id = af.property_floorplan_id OR pu.property_floorplan_id = pf.id ) ' . $strCheckDeletedUnitsSql . ' )
						JOIN unit_spaces us ON ( us.cid = pu.cid AND us.property_unit_id = pu.id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND	aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND apl.cid::int = ' . ( int ) $intCid . '
						AND us.id = ' . ( int ) $intUnitSpaceId . '
					GROUP BY
						apl.id,
						apl.name_first,
						apl.name_last,
						apl.name_middle,
						aa.application_id,
						floor_id,
						space_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSkippedApplicantsByApplicationIdsByAddOnIdByCid( $arrintApplicationIds, $intAddOnId, $intCid, $objDatabase, $boolIncludeDeleted = false, $boolIncludeDeletedAddOns = false ) {

		$strCheckDeletedAASql   	= ( false == $boolIncludeDeleted ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedAddOnSql	= ( false == $boolIncludeDeletedAddOns ) ? ' AND ao.deleted_on IS NULL' : '';

		$strSql = 'SELECT
		  				apl.id,
						COALESCE( apl.name_first::text || \' \', \' \' ) || COALESCE( apl.name_middle::TEXT || \' \',\' \' ) || apl.name_last applicant_name,
						aa.application_id,
						aao.add_on_id
					FROM
						applications ap
						JOIN applicant_applications aa ON ( aa.cid = ap.cid AND aa.application_id = ap.id ' . $strCheckDeletedAASql . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN application_add_ons aao ON ( aao.cid = ap.cid AND aao.application_id = ap.id )
						JOIN add_ons ao ON ( ao.cid = ap.cid AND ao.id = aao.add_on_id ' . $strCheckDeletedAddOnSql . ' )
					WHERE
						aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND	aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND apl.cid::int = ' . ( int ) $intCid . '
						AND ao.id = ' . ( int ) $intAddOnId . '
					GROUP BY
						apl.id,
						apl.name_first,
						apl.name_last,
						apl.name_middle,
						aa.application_id,
						aao.add_on_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicantsCountByApplicationIdsByCustomerTypeIdsByCid( $arrintApplicationIds, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintCustomerTypeIds ) || false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						application_id, count(id) AS applicant_count
					FROM (
							SELECT aa.id,
								   aa.application_id,
								   MAX(aa.id) OVER( PARTITION BY applicant_id ) as max_application_id
							FROM applicants a
								JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
								JOIN applications app ON (app.id = aa.application_id AND app.cid = aa.cid )
							WHERE a.cid = ' . ( int ) $intCid . '
								  AND app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
								  AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
							ORDER BY aa.id DESC
						) as sub
					WHERE id = max_application_id
					GROUP BY application_id';

		$arrintApplicantsCount = ( array ) fetchData( $strSql, $objDatabase );

		return rekeyArray( 'application_id', $arrintApplicantsCount );
	}

	public static function fetchApplicantByNameFirstByNameLastByUsernameByCid( $strNameFirst, $strNameLast, $strUsername, $intCid, $objDatabase ) {
		if( true == is_null( $strUsername ) ) return NULL;

		$strSql = 'SELECT
	 					a.*
	 				FROM
	 					view_applicants a
				   WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ( a.email_address ILIKE E\'' . addslashes( trim( $strUsername ) ) . '\' OR a.username ILIKE E\'' . addslashes( trim( $strUsername ) ) . '\' )
						AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameLast ) ) ) . '\'
					 	AND	lower(a.name_first) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strNameFirst ) ) ) . '\' ';
		$strSql .= ' ORDER BY a.guest_remote_primary_key DESC,a.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByTaxNumberMaskedByCid( $strNameFirst, $strNameLast, $strTaxNumber, $intCid, $objDatabase ) {
		if( true == is_null( $strTaxNumber ) ) return NULL;

		$strSql = 'SELECT
	 					a.*
	 				FROM
	 					view_applicants a
				   WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.tax_number_masked ILIKE E\'%' . addslashes( \Psi\CStringService::singleton()->substr( $strTaxNumber, -4 ) ) . '\'
					 	AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameLast ) ) ) . '\'
					 	AND	lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameFirst ) ) ) . '\' ';
		$strSql .= ' ORDER BY a.guest_remote_primary_key DESC,a.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByBirthDateByCid( $strNameFirst, $strNameLast, $strBirthDate, $intCid, $objDatabase ) {
		if( true == is_null( $strBirthDate ) ) return NULL;

		$strSql = 'SELECT
	 					a.*
	 				FROM
	 					view_applicants a
				   WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.birth_date = \'' . trim( $strBirthDate ) . '\'
						AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameLast ) ) ) . '\'
					 	AND	lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameFirst ) ) ) . '\'
				   order by 
				        a.guest_remote_primary_key DESC,
                        a.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByPhoneNumberByCid( $strNameFirst, $strNameLast, $strPrimaryPhoneNumber, $strSecondaryPhoneNumber, $intCid, $objDatabase ) {
		if( '' == trim( $strPrimaryPhoneNumber ) && '' == trim( $strSecondaryPhoneNumber ) ) return NULL;

		$strPhoneNumberConditionalSql = '';

		$strPhoneNumberConditionalSql .= ( 0 < strlen( $strPrimaryPhoneNumber ) ) ? ( ' ( phone_number IS NOT NULL AND regexp_replace( phone_number, \'[^0-9]\', \'\', \'g\' ) = \'' . preg_replace( '/[^0-9]*/', '', $strPrimaryPhoneNumber ) . "' ) OR ( mobile_number IS NOT NULL AND regexp_replace( mobile_number, '[^0-9]', '', 'g' ) = '" . preg_replace( '/[^0-9]*/', '', $strPrimaryPhoneNumber ) . '\' ) OR ( work_number IS NOT NULL AND regexp_replace( work_number, \'[^0-9]\', \'\', \'g\' ) = \'' . preg_replace( '/[^0-9]*/', '', $strPrimaryPhoneNumber ) . '\' ) ' ) : '';
		$strPhoneNumberConditionalSql .= ( 0 < strlen( $strSecondaryPhoneNumber ) ) ? ( ( ( 0 < strlen( $strPhoneNumberConditionalSql ) ) ? ' OR ' : '' ) . ' ( phone_number IS NOT NULL AND regexp_replace( phone_number, \'[^0-9]\', \'\', \'g\' ) = \'' . preg_replace( '/[^0-9]*/', '', $strSecondaryPhoneNumber ) . "' ) OR ( mobile_number IS NOT NULL AND regexp_replace( mobile_number, '[^0-9]', '', 'g' ) = '" . preg_replace( '/[^0-9]*/', '', $strSecondaryPhoneNumber ) . '\' ) OR ( work_number IS NOT NULL AND regexp_replace( work_number, \'[^0-9]\', \'\', \'g\' ) = \'' . preg_replace( '/[^0-9]*/', '', $strSecondaryPhoneNumber ) . '\' ) ' ) : '';

		$strPhoneNumberConditionalSql = ' AND ( ' . $strPhoneNumberConditionalSql . ' ) ';

		$strSql = '	SELECT
						*
					FROM view_applicants
					WHERE cid = ' . ( int ) $intCid . '
						AND lower(name_first) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strNameFirst ) ) ) . '\'
					 	AND lower(name_last) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strNameLast ) ) ) . '\'' . $strPhoneNumberConditionalSql . '
					ORDER BY 
						guest_remote_primary_key DESC,
                        id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByAppRemotePrimaryKeyByCid( $strNameFirst, $strNameLast, $strAppRemotePrimaryKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND lower( a.app_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strAppRemotePrimaryKey ) ) . '\'
						AND lower( a.name_last ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
						AND	lower( a.name_first ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strNameFirst ) ) . '\' ';
		$strSql .= ' ORDER BY a.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByGuestRemotePrimaryKeyByCid( $strNameFirst, $strNameLast, $strGuestRemotePrimaryKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
					a.cid = ' . ( int ) $intCid . '
					AND lower( a.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\'
					AND lower( a.name_last ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
					AND	lower( a.name_first ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strNameFirst ) ) . '\' ';
		$strSql .= ' ORDER BY a.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchLeaseSignaturePendingApplicantsByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT (va.id),
						va.*,
						aa.customer_type_id,
						aa.id as applicant_application_id
					FROM
						view_applicants va
						JOIN applications a ON ( a.id = ' . ( int ) $intApplicationId . ' AND a.cid = va.cid )
						JOIN file_associations fa ON( va.id = fa.applicant_id AND a.id = fa.application_id AND fa.cid = va.cid)
						JOIN files f ON ( f.id = fa.file_id AND f.cid = fa.cid ) JOIN file_types ft ON ( f.file_type_id = ft.id AND ft.cid = f.cid AND ft.system_code = \'' . addslashes( CFileType::SYSTEM_CODE_LEASE_ADDENDUM ) . '\')
						JOIN file_signatures fs ON ( fa.id = fs.file_association_id AND fs.cid = fa.cid )
						JOIN applicant_applications aa ON ( va.id = aa.applicant_id AND aa.application_id = a.id AND va.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						va.id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND va.cid = ' . ( int ) $intCid . '
						AND fa.file_signed_on IS NULL
						AND fa.deleted_by IS NULL
						AND fa.deleted_on IS NULL
						AND fs.deleted_by IS NULL
						AND fs.deleted_on IS NULL';

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchApplicantIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = true ) {

		if( true == is_null( $intApplicationId ) || true == is_null( $intCid ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
				   		a.id
				   FROM
						applicants a
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND aa.cid = a.cid )
						JOIN applications apps ON ( aa.application_id = apps.id AND aa.cid = apps.cid )
				   WHERE
					   	aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		$arrintResponses = fetchData( $strSql, $objDatabase );

		$arrintApplicantIds = [];

		if( false == valArr( array_filter( $arrintResponses ) ) ) return NULL;

		foreach( $arrintResponses as $arrintResponse ) {
			if( true == array_key_exists( 'id', $arrintResponse ) && true == is_numeric( $arrintResponse['id'] ) ) {
				$arrintApplicantIds[$arrintResponse['id']] = $arrintResponse['id'];
			}
		}

		return $arrintApplicantIds;
	}

	public static function fetchApplicantsByCustomerIdsByCid( $arrintCustomersIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.customer_id IN ( ' . implode( ',', $arrintCustomersIds ) . ' )
						AND a.cid = ' . ( int ) $intCid;
		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsIdsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

	    $strSql = 'SELECT
						id
					FROM
						applicants
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchApplicantIdsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

	    $strSql = 'SELECT
						id
					FROM
						applicants
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

	    $arrintApplicantIds = ( array ) fetchData( $strSql, $objDatabase );
	    $arrintNewApplicantIds = [];
	    foreach( $arrintApplicantIds as $arrintApplicantId ) {
	        $arrintNewApplicantIds[] = $arrintApplicantId['id'];
	    }

	    return $arrintNewApplicantIds;
	}

	public static function fetchCustomApplicantsByCustomerIdsByCid( $arrintCustomersIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( a.cid, a.customer_id ) a.*
					FROM
						view_applicants a
						JOIN customers c ON ( c.cid = a.cid AND c.id = a.customer_id )
					WHERE
						a.customer_id IN ( ' . sqlIntImplode( $arrintCustomersIds ) . ' )
						AND a.cid = ' . ( int ) $intCid . '
						AND CASE
								WHEN
									c.email_address IS NOT NULL AND a.username IS NOT NULL
								THEN
									a.username = c.email_address
								ELSE
									TRUE
							END
					ORDER BY
						a.cid,
						a.customer_id,
						a.created_on ASC';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchPrimaryApplicantIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						a.id
					FROM
						applicants a
						JOIN leases l ON ( l.cid = a.cid AND l.primary_customer_id = a.customer_id AND l.id = ' . ( int ) $intLeaseId . ' AND l.cid = ' . ( int ) $intCid . ' )
					ORDER BY id DESC LIMIT 1 ';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		if( false == valArr( array_filter( $arrintResponses ) ) ) return NULL;

		return $arrintResponses[0]['id'];
	}

	public static function fetchApplicantsByUsernamesByApplicationIdByCid( $arrstrUsernames, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						aa.*, appt.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants appt
						JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						LOWER ( appt.username ) IN ( \'' . implode( "','", array_map( 'strtolower', $arrstrUsernames ) ) . '\' )
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase );

	}

	public static function fetchApplicantsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $strOrderName = '', $boolIncludeDeletedAA = false, $arrintCustomerTypeIds = [], $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedAASql		= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCustomerTypeCondition	= ( true == valArr( $arrintCustomerTypeIds ) ) ? ' AND aa.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' ) ' : '';

		$strSql = 'SELECT
						appt.*,
						appt.username,
						app.property_id,
						apptd.sms_activated_on,
						apptd.sms_cancelled_on,
						app.id as application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id,
						aa.customer_relationship_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
					 	applicants appt
					 	JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ' . $strCheckDeletedAASql . $strCustomerTypeCondition . ' )
					 	JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					 	JOIN applicant_details apptd ON ( apptd.applicant_id = appt.id AND apptd.cid = appt.cid )
					 	LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						appt.cid::int = ' . ( int ) $intCid . '
						AND	 aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';

		if( 'name' == $strOrderName )
			$strSql .= ' ORDER BY appt.name_first, appt.name_last';

		return self::fetchApplicants( $strSql, $objDatabase, $boolIsReturnKeyedArray, $arrintApplicationIds );
	}

	public static function fetchApplicantInfoByIdByCid( $intApplicantId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						tax_number_encrypted,
						identification_value,
						birth_date,
						tax_id_type_id
					FROM
						applicants
					WHERE
						id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchUnsubscribedApplicantsByIdsByCid( $arrintApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return false;

		$strSql = 'SELECT
						appt.*,
						a.property_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants appt
						JOIN scheduled_email_address_filters seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid )
						JOIN applicant_applications AS aa ON ( aa.cid = appt.cid AND aa.applicant_id = appt.id )
						JOIN applications AS a ON ( a.cid = aa.cid AND a.id = aa.application_id )
						LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						appt.cid = ' . ( int ) $intCid . '
						AND appt.id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicationIdByLeaseIdByCid( int $intApplicationId, int $intLeaseId, int $intCid, CDatabase $objDatabase, $intApplicationStatusId = \CApplicationStatus::STARTED ) {

		$strSql = 'SELECT a.name_first,
					       a.name_last,
					       a.name_last_matronymic,
					       COALESCE(lp1.move_in_date, lp.move_in_date) move_in_date,
					       cr.name as customer_relationship_name,
						   a.id as id,
						   a.customer_id,
						   aa.*
					FROM applicant_applications aa
					     JOIN customer_relationships cr On (aa.customer_relationship_id = cr.id AND aa.cid = cr.cid)
					     JOIN cached_applications cap ON (aa.cid = cap.cid ANd aa.application_id = cap.id AND cap.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND cap.application_status_id = ' . ( int ) $intApplicationStatusId . ' )
					     JOIN applicants a ON (aa.cid = a.cid AND aa.applicant_id = a.id)
					     JOIN lease_processes lp ON (a.cid = lp.cid AND cap.lease_id = lp.lease_id AND lp.customer_id IS NULL)
					     LEFT JOIN lease_processes lp1 ON (a.cid = lp1.cid ANd cap.lease_id = lp1.lease_id AND lp1.customer_id = a.customer_id)
					WHERE cap.id = ' . ( int ) $intApplicationId . '
					      AND cap.lease_id = ' . ( int ) $intLeaseId . '
					      AND aa.deleted_by IS NULL
					      AND cap.cid = ' . ( int ) $intCid . '
					ORDER BY lp1.move_in_date,
					         lp.move_in_date,
					         a.name_first,
					         a.name_last,
					         cr.name';

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchApplicantsByApplicationIdByPhoneNumberByCid( $intApplicationId, $strPhoneNumber, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valStr( $strPhoneNumber ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applicant_applications aa
						JOIN view_applicants va ON ( aa.applicant_id = va.id AND aa.cid = va.cid )
						JOIN customer_phone_numbers cpn ON cpn.customer_id = va.customer_id
					WHERE
						aa.deleted_on IS NULL
						AND cpn.phone_number = \'' . $strPhoneNumber . '\'
						AND cpn.deleted_on IS NULL
						AND aa.application_id =  ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase, true, [ $intApplicationId ] );
	}

	public static function fetchCustomApplicantsByLeaseIntervalTypeIdsByPropertyIdsByCid( $arrmixSearch, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $boolIncludeDeleted, $objDatabase, $boolIsIncludeDateCondition = true ) {
		$strCondition					= '';
		$arrstrSearchName				= [];
		$arrstrSearchParameters			= [];
		$arrstrApplicationStageStatus	= CApplicationStageStatus::$c_arrintProspectNonActiveApplicationStageStatusIds;

		if( false != valArr( $arrmixSearch ) ) {
			if( false != isset( $arrmixSearch['name'] ) && false != valStr( $arrmixSearch['name'] ) ) {
				$arrmixName = explode( ' ', $arrmixSearch['name'] );

				foreach( $arrmixName as $strName ) {
					$arrstrSearchName[] = 'lower( a.name_first ) LIKE lower( \'%' . addslashes( trim( $strName ) ) . '%\' )';
					$arrstrSearchName[] = 'lower( a.name_last ) LIKE lower( \'%' . addslashes( trim( $strName ) ) . '%\' )';
					$arrstrSearchName[] = 'lower( a.username ) LIKE lower( \'%' . addslashes( trim( $strName ) ) . '%\' )';
				}

				if( true == valArr( $arrstrSearchName ) ) {
					$arrstrSearchParameters[] = ' ( ' . implode( ' OR ', $arrstrSearchName ) . ' )';
				}
			}

			if( false != valStr( $arrmixSearch['applicationStatusId'] ) ) {
				$arrstrSearchParameters[] = 'ass.id  IN (' . $arrmixSearch['applicationStatusId'] . ') ';
				$arrstrApplicationStageStatus = array_diff( $arrstrApplicationStageStatus, explode( ',', $arrmixSearch['applicationStatusId'] ) );
			}

			if( false != valStr( $arrmixSearch['moveInDateFrom'] ) ) {
				$arrstrSearchParameters[] = 'to_char( ca.lease_start_date, \'MM/DD/YYYY\' ) >= \'' . $arrmixSearch['moveInDateFrom'] . '\' ';
			}

			if( false != valStr( $arrmixSearch['moveInDateTo'] ) ) {
				$arrstrSearchParameters[] = 'to_char( ca.lease_start_date, \'MM/DD/YYYY\' ) <= \'' . $arrmixSearch['moveInDateTo'] . '\' ';
			}

			if( false != valId( $arrmixSearch['leasingAgentId'] ) ) {
				$arrstrSearchParameters[] = 'ca.leasing_agent_id  = ' . $arrmixSearch['leasingAgentId'];
			}

			if( false != valStr( $arrmixSearch['createdOnFrom'] ) ) {
				$arrstrSearchParameters[] = 'to_char( ca.created_on, \'MM/DD/YYYY\' )::date >= \'' . $arrmixSearch['createdOnFrom'] . '\' ';
			}

			if( false != valStr( $arrmixSearch['createdOnTo'] ) ) {
				$arrstrSearchParameters[] = 'to_char( ca.created_on, \'MM/DD/YYYY\' )::date <= \'' . $arrmixSearch['createdOnTo'] . '\' ';
			}
		}

		if( true == valArr( $arrstrSearchParameters ) ) {
			$strCondition = ' AND ( ' . implode( ' AND ', $arrstrSearchParameters ) . ' )';
		}

		if( false != $boolIsIncludeDateCondition ) {
			$strDateCondition = ' AND ca.application_datetime >= ( now() - interval \'90 day\')::Date ';
		}

		$boolIncludeDeleted = ( false == $boolIncludeDeleted ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						a.id AS applicant_id,
						ca.id AS application_id,
						aa.id AS applicant_application_id,
						a.name_last,
						a.name_first,
						aa.customer_type_id,
						aa.customer_relationship_id,
						ca.leasing_agent_id,
						ca.created_on,
						ca.updated_on,
						ca.property_id,
						ca.application_datetime,
						ass.id as application_stage_status_id,
						ca.lease_start_date,
						ca.originating_lead_source_id,
						a.username
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id IN ( ' . implode( ',', CCustomerType::$c_arrintResponsibleCustomerTypeIds ) . ' )' . $boolIncludeDeleted . ' )
				 		JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid)
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id )
						JOIN application_display_details add ON ( add.cid = ca.cid AND add.application_id = ca.id )
					WHERE
						ca.cid = ' . ( int ) $intCid . ' ' . $strDateCondition . '
						AND ca.property_id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . implode( ',',  $arrintLeaseIntervalTypeIds ) . ' )
						AND add.is_lead IS TRUE
						AND ass.id NOT IN ( ' . implode( ',', $arrstrApplicationStageStatus ) . ' )' . $strCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.customer_id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByCidBySearchKeywords( $arrstrFilteredExplodedSearch, $intCid, $objDatabase, $boolIsSearchUsername = false ) {

		$strSql = '	SELECT
						id as applicant_id,( name_first|| \'  \' || name_last ) as applicant_name, username as email_address, phone_number
					FROM
						applicants
					WHERE
						cid = ' . ( int ) $intCid . ' AND name_last IS NOT NULL ';

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( false == empty( $strKeywordAdvancedSearch ) ) {
				if( true == $boolIsSearchUsername ) {
					$arrstrAdvancedSearchParameters[] = 'username ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'';
				} else {
					$arrstrAdvancedSearchParameters[] = 'name_first ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'';
					$arrstrAdvancedSearchParameters[] = 'name_last ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'';
				}
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql .= ' ORDER BY LOWER( name_first ) ASC
						LIMIT 50';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsWithPropertyIdByIdsByCid( $arrintApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						a.*,
						aa.application_id,
						ap.property_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						JOIN applications ap ON ( ap.id = aa.application_id AND ap.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) ';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsWithPropertyIdByIdsByCids( $arrstrApplicantIdsCids, $objDatabase, $boolIsReturnKeyedArray = true ) {

		if( false == valArr( $arrstrApplicantIdsCids ) ) {
			return NULL;
		}

		$arrintApplicationStatusIds = CApplicationStatus::$c_arrintActiveApplicationStatusIds;

		$strSql = 'SELECT
						DISTINCT ON( a.id, a.cid ) a.*,
						aa.application_id,
						ap.property_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						JOIN applications ap ON ( ap.id = aa.application_id AND ap.cid = aa.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						( a.id, a.cid ) IN ( ' . implode( ',', $arrstrApplicantIdsCids ) . ' ) 
						AND ap.application_status_id IN ( ' . implode( ',', $arrintApplicationStatusIds ) . ' )';

		return self::fetchApplicants( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCustomApplicantsByMobileNumber( $strMobileNumber, $objDatabase ) {
		$strSql = 'SELECT
						a.id,
						a.cid,
						a.customer_id
					FROM
						applicants a 
						JOIN customer_phone_numbers cpn ON ( a.customer_id = cpn.customer_id AND a.cid = cpn.cid AND cpn.phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' AND cpn.deleted_by IS NULL AND cpn.deleted_on IS NULL )
					WHERE
						cpn.phone_number IS NOT NULL
						AND ( regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\') LIKE \'%' . $strMobileNumber . '%\'
						OR regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\') LIKE \'%1' . $strMobileNumber . '%\' )
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByCidByPropertyIdBySearchKeywords( $intCid, $arrmixRequestData, $boolIncludeDeletedAA, $objDatabase ) {

		$arrstrFilteredExplodedSearch = CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );
		$arrintFilteredPropertyIds    = [];
		$arrintFilteredPropertyIds[]  = $arrmixRequestData['fast_lookup_filter']['property_id'];
		$arrmixPropertyData           = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByFieldNamesByCid( $arrmixRequestData['fast_lookup_filter']['property_id'], [ 'property_id' ], $intCid, $objDatabase );
		$intParentPropertyId          = $arrmixPropertyData['property_id'];
		if( true == isset( $intParentPropertyId ) ) {
			$arrintFilteredPropertyIds[] = $intParentPropertyId;
		}

		if( true == valArr( $arrstrFilteredExplodedSearch ) ) {

			$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

			$strSql = 'SELECT
							a.id,
							a.name_first,
							a.name_last,
							a.username,
							ap.application_completed_on ,
							ap.id as application_id
						FROM
							applicants a
							JOIN applicant_applications aa ON ( aa.cid = a.cid AND aa.applicant_id = a.id ' . $strCheckDeletedAASql . ' )
							JOIN cached_applications ap ON ( ap.cid = aa.cid AND ap.id = aa.application_id )
							JOIN application_stage_statuses ass ON ( ap.lease_interval_type_id = ass.lease_interval_type_id AND ap.application_stage_id = ass.application_stage_id AND ap.application_status_id = ass.application_status_id )
							JOIN lease_customers lc ON ( lc.cid = ap.cid AND lc.lease_id = ap.lease_id )
							JOIN wait_lists wl ON ( wl.cid = ap.cid AND wl.property_group_id = ap.property_id AND wl.is_active IS TRUE )
							JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintFilteredPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
							LEFT JOIN application_floorplans af ON ( af.cid = ap.cid AND af.application_id = ap.id )
						    JOIN property_floorplans pf ON ( pf.cid = ap.cid AND pf.deleted_on IS NULL AND ( CASE
				                                                                   WHEN ap.property_floorplan_id IS NOT NULL
																				   THEN pf.id = ap.property_floorplan_id
				                                                                   ELSE pf.id = af.property_floorplan_id
				                                                               END ) )
						WHERE
							ap.property_id IN ( ' . implode( ',', $arrintFilteredPropertyIds ) . ' )
							AND ass.id >= wl.application_stage_status_id
							AND ap.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ',' . CApplicationStatus::ON_HOLD . ' )
							AND ap.property_unit_id IS NULL
							AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
							AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
							AND ap.application_completed_on  IS NOT NULL
							AND a.cid = ' . ( int ) $intCid . '';

			$arrstrAdvancedSearchParameters = [];

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( false == empty( $strKeywordAdvancedSearch ) ) {
					$arrstrAdvancedSearchParameters[] = 'a.name_first  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'';
					$arrstrAdvancedSearchParameters[] = 'a.name_last   ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'';
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' GROUP BY
							ap.id,
							a.id,
							a.name_first,
							a.name_last,
							a.username,
							ap.application_completed_on
						 ORDER BY
							ap.application_completed_on  DESC
						 LIMIT 10 ';

			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchApplicantsByScreeningApplicationRequestIdByScreeningRecommendationTypeIdByCid( $intScreeningApplicationRequestId, $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						app.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants app						
						JOIN screening_applicant_results sar ON sar.applicant_id = app.id AND sar.cid = app.cid
						LEFT JOIN customer_phone_numbers cpn ON ( app.cid = cpn.cid AND app.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						sar.screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						AND sar.screening_recommendation_type_id = ' . ( int ) $intScreeningRecommendationTypeId . '
						AND sar.cid = ' . ( int ) $intCid;

		return parent::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByScreeningApplicationRequestIdByScreeningRecommendationTypeIdByCustomerTypeIdByCid( $intScreeningApplicationRequestId, $intScreeningRecommendationTypeId, $intCustomerTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						app.*,
						aa.customer_type_id,
						aa.customer_relationship_id,
						aa.id as applicant_application_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants app
						JOIN applicant_applications aa ON aa.applicant_id = app.id AND aa.cid = app.cid
						JOIN screening_applicant_results sar ON sar.applicant_id = app.id AND sar.cid = app.cid AND sar.application_id = aa.application_id
						LEFT JOIN customer_phone_numbers cpn ON ( app.cid = cpn.cid AND app.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						aa.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND sar.screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						AND sar.screening_recommendation_type_id = ' . ( int ) $intScreeningRecommendationTypeId . '
						AND aa.deleted_on IS NULL
					   	AND aa.deleted_by IS NULL
						AND sar.cid = ' . ( int ) $intCid;

		return parent::fetchApplicants( $strSql, $objDatabase );
	}

    public static function fetchApplicantsByApplicationIdByCutomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {
        $strSql = ' SELECT
                        a.*,
                        aa.customer_type_id,
                        aa.customer_relationship_id,
                        util_get_translated( \'name\', cr.name, cr.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS customer_relationship_name,
                        aa.application_form_id,
                        aa.application_document_id,
                        aa.lease_document_id,
                        aa.application_step_id,
                        aa.application_id,
                        aa.id AS applicant_application_id,
                        aa.deleted_on AS applicant_application_deleted_on,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
                    FROM 
                        applicants a 
                        JOIN applicant_applications aa ON ( aa.cid = a.cid AND aa.applicant_id = a.id )
                        JOIN customer_relationships cr ON ( aa.cid = cr.cid AND aa.customer_relationship_id = cr.id )
                        LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
                    WHERE
                        aa.cid = ' . ( int ) $intCid . '
                        AND aa.application_id =  ' . ( int ) $intApplicationId . '
                        AND aa.customer_type_id IN ( ' . sqlIntImplode( $arrintCustomerTypeIds ) . ' )
                    ORDER BY aa.customer_type_id';

        return self::fetchApplicants( $strSql, $objDatabase );
    }

	public static function fetchScreenedApplicantsByScreeningApplicationRequestIdByCid( $intScreeningApplicationRequestId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						aa.id as applicant_application_id,
						aa.customer_type_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON aa.applicant_id = a.id AND aa.cid = a.cid
						JOIN screening_applicant_results sar ON sar.applicant_id = a.id AND sar.cid = a.cid AND sar.application_id = aa.application_id
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ( sar.request_status_type_id <>' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ' OR sar.request_status_type_id IS NULL )
						AND sar.screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . $strCheckDeletedAASql;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicantsByPhoneNumbersByCid( $arrintPhoneNumbers, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPhoneNumbers ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						phone_number AS contact_number,
						CASE WHEN COUNT( id ) > 1 THEN 1 ELSE 0 END as is_multiple_records
					FROM 
						customer_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND phone_number IN ( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )
						AND deleted_on IS NULL
					GROUP BY
						phone_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantByApplicationIdByUsernameByFirstLastAndMiddleNameByBirthDateByTaxNumberByCid( $intApplicationId, $strUsername, $strNameFirst, $strNameLast, $strNameMiddle, $strBirthDate, $strTaxNumber, $intCid, $objDatabase ) {
		$strCondition = '( lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
						AND lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ) . '\' )  ';

		if( true == valStr( $strNameMiddle ) ) {
			$strCondition .= ' AND lower(a.name_middle) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameMiddle ) ) . '\'';
		}

		$strInnerCondition = '';
		if( false == is_null( $strUsername ) && 0 < strlen( trim( $strUsername ) ) ) {
			$strInnerCondition .= '(lower(a.username) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\') ';
		}

		if( '' != $strInnerCondition ) {
			$strCondition .= ' AND ( ' . $strInnerCondition . ' ) ';
		}

		if( false == is_null( $strBirthDate ) && 0 < strlen( trim( $strBirthDate ) ) ) {
			$strCondition .= ' AND a.birth_date = \'' . ( string ) addslashes( $strBirthDate ) . '\'';
		}

		$strSql = 'SELECT a.*,
						  aa.id as applicant_application_id,
						  aa.customer_type_id,
						  cpn.phone_number_type_id
						  -- ad.secondary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_details ad ON ( a.cid = ad.cid AND a.id = ad.applicant_id )
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = a.cid AND cpn.customer_id = a.customer_id AND cpn.is_primary is TRUE )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND ( ' . $strCondition . ' )';

		$arrobjApplicants = ( array ) self::fetchApplicants( $strSql, $objDatabase );

		foreach( $arrobjApplicants as $intApplicant => $objApplicant ) {
			if( $objApplicant->getTaxNumber() != $strTaxNumber ) {
				unset( $arrobjApplicants[$intApplicant] );
			}
		}

		return ( true == valArr( $arrobjApplicants ) ) ? current( $arrobjApplicants ) : NULL;
	}

	public static function fetchSimpleApplicantByCallNumberByCid( $strCallNumber, $intCid, $objDatabase ) {
		$strCallNumber = trim( preg_replace( '/[^0-9]/', '', $strCallNumber ) );

		// If the phone number starts with a 1, trim it off
		$strCallNumber = ( '1' == $strCallNumber[0] ) ? \Psi\CStringService::singleton()->substr( $strCallNumber, 1 ) : $strCallNumber;

		if ( 0 == ( strlen( trim( $strCallNumber ) ) ) ) return;

		$strSql = 'SELECT
						ap.name_first,
						ap.name_last
					FROM
						applicants ap 
						JOIN applicant_applications aa ON ( aa.applicant_id = ap.id AND aa.cid = ap.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = ap.customer_id AND cpn.cid = ap.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' ) like \'%' . $strCallNumber . '\' )
						AND cpn.deleted_on IS NULL
					ORDER BY
						aa.created_on DESC
					LIMIT 1';
		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicantsByPhoneNumbersByCid( $arrstrPhoneNumbers, $intCid, $objDatabase ) {

		$strSql	= '	SELECT
						Max( aa.applicant_id) AS applicant_id, 
						Max(aa.application_id) AS application_id, 
						Max(app.application_datetime) AS application_datetime, 
						Max(app.updated_on) AS updated_on, 
						Max(a.name_first) AS name_first, 
						Max(a.name_last) AS name_last, 
						cpn.phone_number, 
						Max(app.property_id) AS property_id, 
						CASE 
							WHEN count(cpn.phone_number) > 1 THEN 1 ELSE 0 
						END AS is_multiple_record 
					FROM
						applicants a
						JOIN customer_phone_numbers cpn ON a.customer_id = cpn.customer_id AND cpn.cid = a.cid
						JOIN applicant_applications AS aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid AND aa.deleted_on IS NULL )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND cpn.phone_number IN ( \'' . implode( '\',\'', $arrstrPhoneNumbers ) . '\' )
						AND cpn.deleted_by IS NULL
					GROUP BY
						cpn.phone_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantCountByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(id)
					FROM
						applicants
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
		return 0;
	}

	public static function fetchTotalApplicantCountHavingRequiredAgeByApplicationIdByCid( $intRequiredApplicantAge, $intApplicationId, $intCid, $objDatabase ) {

		$strApplicantAgeCondition	= ( false == is_null( $intRequiredApplicantAge ) && true == is_numeric( $intRequiredApplicantAge ) ) ? ' AND a.birth_date <= ( NOW() - INTERVAL \'' . ( int ) $intRequiredApplicantAge . ' YEAR\' )' : '';

		$strSql = 'SELECT
                        COUNT( a.id )
					FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND aa.deleted_on IS NULL )
					WHERE a.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . $strApplicantAgeCondition;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchScreenedApplicantsByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
							a.*,
							aa.id as applicant_application_id,
							aa.customer_type_id,
	                        cpn.phone_number AS primary_phone_number,
	                        cpn.phone_number_type_id AS primary_phone_number_type_id
						FROM
							applicants	a 						
							JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
							JOIN screening_applicant_results sar ON ( sar.cid = a.cid AND sar.applicant_id = a.id AND sar.application_id = aa.application_id )
							LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
						WHERE
							a.id IN( ' . implode( ',', $arrintApplicantIds ) . ' )
							AND aa.application_id = ' . ( int ) $intApplicationId . '
							AND	a.cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByCustomerIdByLeaseIdByApplicationIdByCid( $intCustomerId, $intLeaseId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
						JOIN applications app ON ( a.cid = app.cid AND app.id = aa.application_id )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.customer_id = ' . ( int ) $intCustomerId . '
						AND app.lease_id = ' . ( int ) $intLeaseId . ' 
						AND app.id = ' . ( int ) $intApplicationId;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantByApplicantIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase, $boolLoadAddresses = true ) {

		$strSql = 'SELECT
						a.*,
						app.property_id,
						app.id as application_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM applicants a
					JOIN applications app ON app.cid = a.cid AND app.id = ' . ( int ) $intApplicationId . '
					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE 
					a.cid = ' . ( int ) $intCid . '
					AND a.id = ' . ( int ) $intApplicantId;

		return self::fetchApplicant( $strSql, $objDatabase, [], $boolLoadAddresses );
	}

	public static function fetchCustomApplicantByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE customer_id = %d AND cid = %d ORDER BY app_remote_primary_key NULLS LAST, guest_remote_primary_key NULLS LAST LIMIT 1', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSimpleApplicantsByFullNamesByCid( $arrstrFullNames, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						aa.id
					FROM applicants a
					JOIN applicant_applications aa ON aa.applicant_id = a.id 
					WHERE a.name_first || a.name_last ~* \'(' . implode( '|', $arrstrFullNames ) . ')\'
						AND a.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantByApplicantDetailsByCid( $arrstrApplicantData, $intCid, $objDatabase ) {
		$strSubSql = '';

		$strNameFirst           = getArrayElementByKey( 'name_first', $arrstrApplicantData );
		$strNameLast            = getArrayElementByKey( 'name_last', $arrstrApplicantData );
		$strEmailAddress        = getArrayElementByKey( 'email_address', $arrstrApplicantData );
		$strBirthDate           = trim( getArrayElementByKey( 'birth_date', $arrstrApplicantData ) );
		$strPhoneNumber         = trim( getArrayElementByKey( 'phone_number', $arrstrApplicantData ) );

		if( true == valStr( $strBirthDate ) ) {
			$strSubSql = ' birth_date = \'' . trim( $strBirthDate ) . '\' ';
		}

		if( true == valStr( $strEmailAddress ) ) {
			$strSubSql .= ( ( true == valStr( $strSubSql ) ) ? ' OR ' : '' ) . ' ( email_address ILIKE E\'' . addslashes( $strEmailAddress ) . '\' OR username ILIKE E\'' . addslashes( trim( $strEmailAddress ) ) . '\' ) ';
		}

		if( true == valId( $strPhoneNumber ) ) {
			$strSubSql .= ( ( true == valStr( $strSubSql ) ) ? ' OR ' : '' ) . ' (translate(trim(phone_number),\'()- \',\'\' )   = \'' . addslashes( $strPhoneNumber ) . '\'
							 OR  translate(trim(mobile_number),\'()- \',\'\' )   = \'' . addslashes( $strPhoneNumber ) . '\'
							 OR  translate(trim(work_number),\'()- \',\'\' )   = \'' . addslashes( $strPhoneNumber ) . '\') ';
		}

		$strSubSql = ( true == valStr( $strSubSql ) ) ? substr_replace( $strSubSql, 'AND (', 0, 0 ) . ')' : '';

		$strSql = 'SELECT
						*
					FROM
						view_applicants
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lower(name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameLast ) ) ) . '\'
						AND	lower(name_first) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strNameFirst ) ) ) . '\'
						' . $strSubSql . '
							ORDER BY updated_on
							LIMIT 1;';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchCurrentCustomerIncomesByCustomerIds( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomersIncomeByCustomerIdsByIncomeTypeIdsByCid( $arrintCustomerIds, [ CIncomeType::CURRENT_EMPLOYER ], $intCid, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIds( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$arrobjCustomerIncome = \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase );

		if( true == valArr( $arrobjCustomerIncome ) ) {
			$arrobjCustomerIncome = rekeyObjects( 'CustomerId', $arrobjCustomerIncome, true );
		}

		return $arrobjCustomerIncome;

	}

	public static function fetchApplicantByCustomerIdByApplicationIdByCid( $intCustomerId, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valId( $intCustomerId ) ) return false;

		$strSql = ' SELECT
						va.*,
						aa.customer_relationship_id
				    FROM 
				        view_applicants va
				        JOIN applicant_applications aa ON ( va.cid = aa.cid and va.id = aa.applicant_id )
					WHERE
			             aa.application_id = ' . ( int ) $intApplicationId . '
			             AND aa.cid = ' . ( int ) $intCid . '
			             AND va.customer_id = ' . ( int ) $intCustomerId . '
		            LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByCustomerTypeIdsByApplicationIdByCid( $arrintCustomerTypeIds, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						va.*, 
						aa.customer_relationship_id, 
						aa.applicant_id, 
						aa.application_id
					FROM
					    view_applicants va
						JOIN applicant_applications aa ON ( va.cid = aa.cid and va.id = aa.applicant_id )
					WHERE
						aa.deleted_on IS NULL
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid . '
						AND va.parent_email_address IS NOT NULL';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicationIdByCustomerIdsByCid( $intApplicationId, $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valId( $intApplicationId ) ) return false;

		$strSql = ' SELECT
						va.*, aa.customer_relationship_id
				    FROM view_applicants va
				         JOIN applicant_applications aa ON (va.cid = aa.cid and va.id = aa.applicant_id )
					WHERE
			             aa.application_id = ' . ( int ) $intApplicationId . '
			             AND aa.cid = ' . ( int ) $intCid . '
						 AND va.customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' ) 
						 AND aa.deleted_on IS NULL';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantByMobileNumberByPropertyIdByCid( $intMobileNumber, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						appt.id,
						appt.cid,
						app.property_id,
						aa.application_id
					FROM
						applicants appt
						JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE
						appt.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND ( regexp_replace( appt.mobile_number, \'[^0-9]\', \'\', \'g\') = \'' . $intMobileNumber . '\' OR regexp_replace( appt.mobile_number, \'[^0-9]\', \'\', \'g\') = \'1' . $intMobileNumber . '\' )
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 *Over riding base class used functions
	 */

	public static function fetchApplicantByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicant( sprintf( 'SELECT * FROM view_applicants WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

// fetch applicants preffered locale if exists or fetch primary applicants preffered locale code.

	public static function fetchApplicantPrefferedLocaleCodeByApplicantIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT c.preferred_locale_code FROM applicant_applications aa
					JOIN applicants a ON ( aa.applicant_id = a.id and aa.cid = a.cid )
					JOIN customers c ON ( a.customer_id = c.id and a.cid = c.cid )
					JOIN applications ap ON ( aa.application_id = ap.id AND aa.cid = ap.cid )
					WHERE
					( aa.applicant_id = ' . ( int ) $intApplicantId . ' OR aa.applicant_id = ap.primary_applicant_id )
					AND aa.application_id = ' . ( int ) $intApplicationId . '
					AND aa.cid = ' . ( int ) $intCid . '
					AND c.preferred_locale_code IS NOT NULL
					ORDER BY 
					CASE aa.applicant_id
					WHEN ' . ( int ) $intApplicantId . ' THEN 1
					WHEN ap.primary_applicant_id THEN 2
					END
					LIMIT 1';
		$arrmixValues = fetchData( $strSql, $objDatabase );
		return $arrmixValues[0]['preferred_locale_code'];
	}

	public static function fetchApplicantsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByCurrentCompanyResidenceTypeIdByCid( $intCurrentCompanyResidenceTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE current_company_residence_type_id = %d AND cid = %d', ( int ) $intCurrentCompanyResidenceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByCurrentMoveOutReasonListItemIdByCid( $intCurrentMoveOutReasonListItemId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE current_move_out_reason_list_item_id = %d AND cid = %d', ( int ) $intCurrentMoveOutReasonListItemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByPreviousCompanyResidenceTypeIdByCid( $intPreviousCompanyResidenceTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE previous_company_residence_type_id = %d AND cid = %d', ( int ) $intPreviousCompanyResidenceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantsByPreviousMoveOutReasonListItemIdByCid( $intPreviousMoveOutReasonListItemId, $intCid, $objDatabase ) {
		return self::fetchApplicants( sprintf( 'SELECT * FROM view_applicants WHERE previous_move_out_reason_list_item_id = %d AND cid = %d', ( int ) $intPreviousMoveOutReasonListItemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicant( $strSql, $objDatabase, $arrintApplicationIds = [], $boolLoadAddresses = true ) {

		$objApplicant = parent::fetchObject( $strSql, 'CApplicant', $objDatabase );

		if( true == $boolLoadAddresses && true == valObj( $objApplicant, 'CApplicant' ) ) {
			$arrmixCustomerAddressData = ( array ) \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdsByApplicationIdsByCid( [ $objApplicant->getCustomerId() ], $objApplicant->getCid(), $objDatabase, $arrintApplicationIds );
			if( true == valArrKeyExists( $arrmixCustomerAddressData, 0 ) ) {
				$objApplicant->setValues( $arrmixCustomerAddressData[0] );
			}
		}

		return $objApplicant;
	}

	public static function fetchApplicants( $strSql, $objDatabase, $boolIsReturnKeyedArray = true, $arrintApplicationIds = [], $boolLoadAddresses = true ) {

		$arrobjApplicantsOriginal = $arrobjApplicants = parent::fetchObjects( $strSql, 'CApplicant', $objDatabase, $boolIsReturnKeyedArray );

		if( false == valArr( extractUniqueFieldValuesFromObjects( 'getCustomerId', $arrobjApplicants ) ) ) return $arrobjApplicants;

		$arrobjApplicants = rekeyObjects( 'CustomerId', $arrobjApplicants );

		if( true == valArr( $arrobjApplicants ) ) {

			$arrintCustomerIds = array_keys( $arrobjApplicants );

			if( true == valArr( $arrintCustomerIds ) && true == $boolLoadAddresses ) {
				if( empty( $arrintApplicationIds ) ) {
					$arrintApplicationIds = array_filter( array_values( array_map( function( $objApplicants ) { if( !empty( $objApplicants->getApplicationId() ) ) return $objApplicants->getApplicationId(); }, $arrobjApplicants ) ) );
				}
				$arrmixCustomerAddressData = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdsByApplicationIdsByCid( $arrintCustomerIds, $arrobjApplicants[$arrintCustomerIds[0]]->getCid(), $objDatabase, $arrintApplicationIds );
				$arrmixCustomerAddressData = rekeyArray( 'customer_id', $arrmixCustomerAddressData );

				foreach( $arrintCustomerIds as $intCustomerId ) {
					if( true == valObj( $arrobjApplicants[$intCustomerId], 'CApplicant' ) && true == valArr( $arrmixCustomerAddressData ) && true == array_key_exists( $intCustomerId, $arrmixCustomerAddressData ) ) {
						$arrobjApplicants[$intCustomerId]->setValues( $arrmixCustomerAddressData[$intCustomerId] );
					}
				}
			}
		}

		$arrobjApplicants = rekeyObjects( 'Id', $arrobjApplicants );

		if( \Psi\Libraries\UtilFunctions\count( $arrobjApplicants ) < \Psi\Libraries\UtilFunctions\count( $arrobjApplicantsOriginal ) ) {
			$arrobjApplicants = $arrobjApplicants + $arrobjApplicantsOriginal;
		}
		return $arrobjApplicants;

	}

	public static function fetchApplicantsByPropertyIdsByCids( $arrintPropertyIds, $arrintClientIds, $objClientDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintClientIds ) ) {
			return;
		}

		$arrintApplicationStageStatusIds = [
			CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::COMPLETED ],
			CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
			CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
		];

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON ( appt.id )
						appt.*,
						apps.lease_id,
						apps.lease_interval_id,
						apps.property_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants AS appt
						JOIN applicant_applications AS aa ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications AS apps ON ( apps.id = aa.application_id AND apps.cid = appt.cid )
						JOIN applicant_details ad ON ( ad.applicant_id = appt.id AND ad.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						apps.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND appt.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND (apps.application_stage_id,apps.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND appt.mobile_number IS NOT NULL
						AND ad.sms_activated_on IS NOT NULL
						AND ad.sms_cancelled_on IS NULL';

		return self::fetchApplicants( $strSql, $objClientDatabase );
	}

	public static function  fetchSimpleApplicantsByCustomerTypeIdsByApplicationIdByCid( $arrintCustomerTypeIds, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) || false == valId( $intApplicationId ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						a.id,
						a.name_first,
						a.name_last,
						aa.application_id,
						aa.id as applicant_application_id,
						aa.customer_type_id
					FROM
						applicant_applications AS aa
						JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' ) ';

		$strSql = $strSql . ' ORDER BY aa.customer_type_id;';

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByUsernameByApplicationIdByCustomerTypeIdByCid( $strNameFirst, $strNameLast, $strUsername, $intApplicationId, $intCustomerTypeId, $intCid, $objDatabase ) {
		if( false == valStr( $strUsername ) || false == valId( $intApplicationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
	 					a.*
	 				FROM
	 					view_applicants a 
	 					JOIN applicant_applications aa ON (aa.cid =  a.cid and aa.applicant_id =  a.id)
				   WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ( a.email_address ILIKE E\'' . addslashes( trim( $strUsername ) ) . '\' OR a.username ILIKE E\'' . addslashes( trim( $strUsername ) ) . '\' )
						AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strNameLast ) ) ) . '\'
					 	AND	lower(a.name_first) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strNameFirst ) ) ) . '\' 
						AND aa.application_id =' . ( int ) $intApplicationId . '
		                AND aa.customer_type_id = ' . ( int ) $intCustomerTypeId . '
		                LIMIT 1;';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchPrimaryApplicantByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						a.*,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM applicants a 
					JOIN applications app ON ( a.cid = app.cid AND a.id = app.primary_applicant_id) 
					JOIN applicant_applications aa ON ( app.cid = aa.cid AND aa.application_id = app.id and aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.applicant_id = app.primary_applicant_id )
					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE app.id =' . ( int ) $intApplicationId .
		          'AND app.cid = ' . ( int ) $intCid . ';';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicantContactDetailsByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cpn.phone_number AS mobile_number,
						a.id as applicant_id,
						a.customer_id,
						a.name_first,
						a.name_last,
						a.cid,
						a.customer_id,
						a.email_address,
						a.username
					FROM
						applicants AS a
						JOIN applicant_applications AS aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND aa.deleted_on IS NULL )
						LEFT JOIN customer_phone_numbers AS cpn ON ( a.customer_id = cpn.customer_id AND a.cid = cpn.cid AND cpn.phone_number_type_id = ' . ( int ) CPhoneNumberType::MOBILE . ' AND cpn.deleted_by IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND aa.application_id =  ' . ( int ) $intApplicationId . '
						AND a.id =  ' . ( int ) $intApplicantId;

		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( valArr( $arrstrData ) && valArr( $arrstrData[0] ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchApplicantsFromViewByCustomerIdsByCid( $arrintCustomersIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						view_applicants
					WHERE
						customer_id IN ( ' . implode( ',', $arrintCustomersIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchApplicants( $strSql, $objDatabase );
	}

	public static function getCustomerAddressLogFields() {

		return 'current_address.id as current_address_id,
              current_address.company_residence_type_id AS current_company_residence_type_id,
              current_address.move_out_reason_list_item_id AS current_move_out_reason_list_item_id,
              current_address.community_name AS current_community_name,
              current_address.reason_for_leaving AS current_reason_for_leaving,
              current_address.move_in_date AS current_move_in_date,
              current_address.move_out_date AS current_move_out_date,
              current_address.monthly_payment_amount AS current_monthly_payment_amount,
              current_address.payment_recipient AS current_payment_recipient,
              current_address.street_line1 AS current_street_line1,
              current_address.street_line2 AS current_street_line2,
              current_address.street_line3 AS current_street_line3,
              current_address.city AS current_city,
              current_address.state_code AS current_state_code,
              current_address.postal_code AS current_postal_code,
              current_address.country_code AS current_country_code,
              current_address.address_owner_type AS current_address_owner_type,
              current_address.is_non_us_address AS current_is_non_us_address,
              previous_address.id as previous_address_id,
              previous_address.customer_id as customer_id2,
              previous_address.company_residence_type_id AS previous_company_residence_type_id,
              previous_address.move_out_reason_list_item_id AS previous_move_out_reason_list_item_id,
              previous_address.community_name AS previous_community_name,
              previous_address.reason_for_leaving AS previous_reason_for_leaving,
              previous_address.move_in_date AS previous_move_in_date,
              previous_address.move_out_date AS previous_move_out_date,
              previous_address.monthly_payment_amount AS previous_monthly_payment_amount,
              previous_address.payment_recipient AS previous_payment_recipient,
              previous_address.street_line1 AS previous_street_line1,
              previous_address.street_line2 AS previous_street_line2,
              previous_address.street_line3 AS previous_street_line3,
              previous_address.city AS previous_city,
              previous_address.state_code AS previous_state_code,
              previous_address.postal_code AS previous_postal_code,
              previous_address.country_code AS previous_country_code,
              previous_address.address_owner_type AS previous_address_owner_type,
              previous_address.is_non_us_address AS previous_is_non_us_address,
              current_address.details::json -> \'_postal_addresses\' -> \'default\' ->>\'dependentLocality\' AS current_dependent_locality,
              previous_address.details::json -> \'_postal_addresses\' -> \'default\' ->>\'dependentLocality\' AS previous_dependent_locality,
              current_address.details::json -> \'_postal_addresses\' -> \'default\' ->>\'sortingCode\' AS current_sorting_code,
              previous_address.details::json -> \'_postal_addresses\' -> \'default\' ->>\'sortingCode\' AS previous_sorting_code,
              rank() OVER (PARTITION BY aa.cid, aa.applicant_id ORDER BY aa.application_id DESC, current_address.id DESC NULLS LAST, previous_address.id DESC NULLS LAST) AS rank';
	}

	public static function fetchApplicantsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						app.id as application_id,
						aa.customer_type_id as customer_type_id,
                        cpn.phone_number AS primary_phone_number,
                        cpn.phone_number_type_id AS primary_phone_number_type_id
					FROM
						applicants a
						JOIN applicant_applications aa ON ( a.cid = ' . ( int ) $intCid . ' AND a.id = aa.applicant_id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( a.cid = ' . ( int ) $intCid . ' AND app.id = aa.application_id AND a.cid = app.cid AND app.lease_id = ' . ( int ) $intLeaseId . ' )
						LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					ORDER BY aa.customer_type_id';

		return parent::fetchApplicants( $strSql, $objDatabase );
	}

	public static function fetchApplicantByNameFirstByNameLastByPhoneNumbersByCid( $strNameFirst, $strNameLast, $arrstrPhoneNumbers, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrPhoneNumbers ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						va.*
					FROM 
					view_applicants va
					JOIN customer_phone_numbers cpn ON cpn.cid = va.cid AND cpn.customer_id = va.customer_id AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL
					WHERE 
						va.cid = ' . ( int ) $intCid . '
						AND lower( va.name_first ) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strNameFirst ) ) ) . '\'
					 	AND lower( va.name_last ) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strNameLast ) ) ) . '\'
						AND cpn.phone_number IS NOT NULL AND regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\' ) IN ( ' . sqlstrImplode( $arrstrPhoneNumbers ) . ' )
					ORDER BY 
						va.guest_remote_primary_key DESC,
						va.id DESC LIMIT 1';

		return self::fetchApplicant( $strSql, $objDatabase );
	}

	public static function fetchApplicantNameEmailAddressByApplicantIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.name_first,
						a.name_last,
						a.email_address,
						aa.send_consumer_report
					FROM applicants a
					JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE 
					a.cid = ' . ( int ) $intCid . '
					AND a.id = ' . ( int ) $intApplicantId . '
					AND aa.application_id = ' . ( int ) $intApplicationId;

		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( valArr( $arrstrData ) && valArr( $arrstrData[0] ) ) ? $arrstrData[0] : NULL;
	}

}
?>
