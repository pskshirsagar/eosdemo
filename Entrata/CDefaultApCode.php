<?php

class CDefaultApCode extends CBaseDefaultApCode {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWipDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumptionDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultJobApCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToWip() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAutoAdded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReserved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>