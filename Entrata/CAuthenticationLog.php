<?php

class CAuthenticationLog extends CBaseAuthenticationLog {

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCustomerId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getApplicantId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCompanyUserId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getEmployeeId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getApPayeeId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valLoginDatetime() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getLoginDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLogoutDatetime() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getLogoutDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'logout_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getIpAddress() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    // other function

    public function calculateTimeSpent( $intLogOutTime, $intLoginTime ) {

		$diff	= abs( $intLogOutTime - $intLoginTime );
		$hours 	= floor( $diff / ( 60 * 60 ) );
		$mins 	= floor( ( $diff - ( $hours * 60 * 60 ) ) / ( 60 ) );
		$secs 	= floor( ( $diff - ( ( $hours * 60 * 60 ) + ( $mins * 60 ) ) ) );
		$result = sprintf( '%02d', $hours ) . ':' . sprintf( '%02d', $mins ) . ':' . sprintf( '%02d', $secs );

		return $result;
    }
}
?>