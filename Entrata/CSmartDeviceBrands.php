<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartDeviceBrands
 * Do not add any new functions to this class.
 */

class CSmartDeviceBrands extends CBaseSmartDeviceBrands {

	public static function fetchAllSmartDeviceBrands( $objDatabase ) {
		return self::fetchSmartDeviceBrands( 'SELECT * FROM smart_device_brands ORDER BY ' . $objDatabase->getCollateSort( 'order_num' ), $objDatabase );
	}

}
?>