<?php

class CEventLog extends CBaseEventLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPriorEventLogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventSubTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventResultId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAssociatedEventId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldStageId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewStageId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldStatusId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewStatusId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntegrationResultId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectivePeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOriginalPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarEventKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEndDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDoNotExport() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResident() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDeleted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPostMonthIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPostDateIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>