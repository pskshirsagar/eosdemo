<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallChunks
 * Do not add any new functions to this class.
 */

class CScheduledCallChunks extends CBaseScheduledCallChunks {

	public static function fetchScheduledCallChunksByScheduledCallIdOrderByOrderNumByCid( $intScheduledCallId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						scheduled_call_chunks
					WHERE
						scheduled_call_id = ' . ( int ) $intScheduledCallId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						order_num';

		return self::fetchScheduledCallChunks( $strSql, $objDatabase );
	}

	public static function fetchScheduledCallChunksByScheduledCallIdByIdByCid( $intScheduledCallId, $intScheduledCallChunkId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						scheduled_call_chunks
					WHERE
						scheduled_call_id = ' . ( int ) $intScheduledCallId . '
						AND id = ' . ( int ) $intScheduledCallChunkId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchScheduledCallChunk( $strSql, $objDatabase );
	}

	public static function fetchScheduledCallChunksByIdsByCid( $arrintScheduledCallChunks, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScheduledCallChunks ) ) return NULL;

		$strSql = 'SELECT * FROM
						scheduled_call_chunks
					WHERE
						id IN ( ' . implode( ',', $arrintScheduledCallChunks ) . ' )
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						order_num';

		return self::fetchScheduledCallChunks( $strSql, $objDatabase );
	}
}
?>