<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyHolidays
 * Do not add any new functions to this class.
 */

class CCompanyHolidays extends CBaseCompanyHolidays {

    public static function fetchCurrentYearCompanyHolidaysByCid( $intCid, $objDatabase ) {
    	$strSql = 'SELECT * FROM
    					company_holidays
    				WHERE
    					cid = ' . ( int ) $intCid . '
    					AND date >= ( NOW() - INTERVAL \'6 months\' )
    					AND property_id IS NULL
    				ORDER BY
    					date ASC';

    	return self::fetchCompanyHolidays( $strSql, $objDatabase );
    }

    public static function fetchUnassociatedCompanyHolidayIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT
						ch.id,
						' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', ch.name, ch.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'ch.name' ) . ' AS name,
    					EXTRACT ( YEAR FROM ch.date ) AS year 
					FROM
						company_holidays ch
						LEFT JOIN property_holidays ph ON ( ph.cid = ch.cid AND ph.property_id = ' . ( int ) $intPropertyId . ' AND ph.company_holiday_id = ch.id )
					WHERE
						ch.cid = ' . ( int ) $intCid . '
						AND date >= ( NOW() - INTERVAL \'6 months\' )
						AND ph.id IS NULL';

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchAssociatedCompanyHolidayIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT
					    ch.id,
					    ' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', ch.name, ch.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'ch.name' ) . ' AS name
					FROM
					    company_holidays ch
					    JOIN property_holidays ph ON ( ph.cid = ch.cid AND ph.property_id = ' . ( int ) $intPropertyId . ' AND ph.company_holiday_id = ch.id )
					WHERE
					    ch.cid = ' . ( int ) $intCid;

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchAssociatedCompanyHolidaysByDatesByPropertyIdByCid( $strBeginDate, $strEndDate, $intCid, $objDatabase, $intPropertyId = NULL ) {

		$strJoinCondition = ( true == valId( $intPropertyId ) ) ? 'AND ph.property_id = ' . ( int ) $intPropertyId . '' : '';

		$strSql = 'SELECT
					    ph.id,
                        ph.property_id,
                        ch.date
					FROM
					    company_holidays ch
					    JOIN property_holidays ph ON ( ph.cid = ch.cid ' . $strJoinCondition . ' AND ph.company_holiday_id = ch.id )
					WHERE
					    ch.cid = ' . ( int ) $intCid . '
					    AND ch.date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\'';

		return rekeyArray( 'date', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchAssociatedCompanyHolidayByIdByPropertyIdsByCid( $intCompanyHolidayId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ph.id,
						ph.property_id,
						ch.name
					FROM
						company_holidays ch
						JOIN property_holidays ph ON ( ph.cid = ch.cid AND ph.company_holiday_id = ch.id )
					WHERE
						ch.id = ' . ( int ) $intCompanyHolidayId . '
						AND ch.cid = ' . ( int ) $intCid . '
						AND ph.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>