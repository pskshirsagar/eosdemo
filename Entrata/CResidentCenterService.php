<?php

class CResidentCenterService extends CBaseResidentCenterService {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_intId )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_strName )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ));
        // }

        return $boolIsValid;
    }

    public function valUri() {
        $boolIsValid = true;

		// Validation example
        // if( false == isset( $this->m_strUri )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'uri', '' ));
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_strDescription )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ));
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_intIsPublished )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ));
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_intOrderNum )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>