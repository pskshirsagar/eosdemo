<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactTypes
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeContactTypes extends CBaseCompanyEmployeeContactTypes {

	public static function fetchCompanyEmployeeContactTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CCompanyEmployeeContactType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCompanyEmployeeContactType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CCompanyEmployeeContactType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }
}
?>