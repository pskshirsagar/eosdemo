<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantRequirementTypes
 * Do not add any new functions to this class.
 */

class CScreeningApplicantRequirementTypes extends CBaseScreeningApplicantRequirementTypes {

	public static function fetchScreeningApplicantRequirementTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningApplicantRequirementType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningApplicantRequirementType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningApplicantRequirementType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedScreeningApplicantRequirementTypes( $objDatabase ) {
		return self::fetchScreeningApplicantRequirementTypes( sprintf( 'SELECT * FROM screening_applicant_requirement_types WHERE is_published = true ORDER BY order_num', ( int ) $intCid ), $objDatabase );
	}

}
?>