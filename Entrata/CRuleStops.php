<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRuleStops
 * Do not add any new functions to this class.
 */

class CRuleStops extends CBaseRuleStops {

	public static function fetchRuleStopsByRouteRuleIdsByRouteIdByCid( $arrintRouteRulesIds, $intRouteId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintRouteRulesIds ) ) return NULL;

		$strSql = 'SELECT
						rs.*,
						cg.name AS company_group_name,
						ce.name_first AS company_user_name_first,
						ce.name_last AS company_user_name_last,
						cu.is_disabled AS company_user_is_disabled
					FROM
						rule_stops rs
						LEFT JOIN company_groups cg ON ( rs.cid = cg.cid AND rs.company_group_id = cg.id )
						LEFT JOIN company_users cu ON ( rs.cid = cu.cid AND rs.company_user_id = cu.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.route_rule_id IN ( ' . implode( ',', $arrintRouteRulesIds ) . ' )
						AND rs.route_id = ' . ( int ) $intRouteId . '
						AND rs.deleted_on IS NULL
					ORDER BY
						rs.order_num';

		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchCustomRuleStopsByRouteRuleIdByCid( $intRouteRuleId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM rule_stops WHERE cid = ' . ( int ) $intCid . ' AND route_rule_id = ' . ( int ) $intRouteRuleId . ' AND deleted_on IS NULL ORDER BY order_num';
		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchNonAdminRuleStopsByRouteIdByCid( $intRouteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						rs.*
					FROM
						rule_stops rs
						JOIN company_users cu ON ( rs.cid = cu.cid AND cu.id = rs.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.route_id = ' . ( int ) $intRouteId . '
						AND rs.deleted_by IS NULL
						AND cu.is_administrator != 1';

		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchRuleStopsByGroupIdsByCid( $arrintGroupIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						rs.*,
						r.name AS route_name
					FROM
						rule_stops rs
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.company_group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND rs.deleted_on IS NULL
						AND r.is_published = 1
					ORDER BY
						rs.order_num';

		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchActiveRuleStopsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						rs.*,
						r.id,
						r.name AS route_name
					FROM
						rule_stops rs
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id )
						JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND rs.deleted_on IS NULL
						AND r.is_published = 1
						AND cu.is_disabled = 0
					ORDER BY
						rs.order_num';

		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchActiveRuleStopsByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						rs.id,
						rs.company_user_id,
						r.name AS route_name,
						cu.username
					FROM
						rule_stops rs
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id )
						JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND rs.deleted_on IS NULL
						AND r.is_published = 1
						AND cu.is_disabled = 0
					ORDER BY
						rs.order_num';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchRuleStopsByRouteRuleIdsByCid( $arrintRouteRuleIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintRouteRuleIds ) ) return NULL;

		$strSql = 'SELECT * FROM rule_stops WHERE route_rule_id IN ( ' . implode( ',', $arrintRouteRuleIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL ORDER BY order_num';

		return self::fetchRuleStops( $strSql, $objClientDatabase );
	}

	public static function fetchFirstRuleStopByRouteIdByRouteRuleIdByCid( $intRouteId, $intRouteRuleId, $intCid, $objDatabase ) {

		$strSql = 'SELECT rs.* FROM
						rule_stops rs
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.route_id = ' . ( int ) $intRouteId . '
						AND rs.route_rule_id = ' . ( int ) $intRouteRuleId . '
						AND rs.deleted_on IS NULL
					ORDER BY
						rs.order_num
					LIMIT 1';

		return self::fetchRuleStop( $strSql, $objDatabase );
	}

	public static function fetchAllRuleStopsByRouteIdsByRouteRuleIdsByCid( $arrintRouteIds, $arrintRouteRuleIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( array_filter( $arrintRouteIds ) ) || false == valArr( array_filter( $arrintRouteRuleIds ) ) || false == valArr( array_filter( $arrintPropertyIds ) ) ) return;

		$strSql = 'SELECT
						rs.*,
						pr.property_id
					FROM
						rule_stops rs
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id AND r.is_published = 1 )
						JOIN property_routes pr ON ( pr.cid = r.cid AND pr.route_id = r.id AND pr.route_type_id = r.route_type_id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.route_id IN ( ' . implode( ', ', $arrintRouteIds ) . ' )
						AND rs.route_rule_id IN ( ' . implode( ', ', $arrintRouteRuleIds ) . ' )
						AND rs.deleted_on IS NULL
						AND pr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
					ORDER BY
						rs.route_id, rs.route_rule_id, rs.order_num';

		return self::fetchRuleStops( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchPreviousRuleStopByRuleStopIdByCid( $intRuleStopId, $intCid, $objDatabase ) {

		$strSql = 'SELECT rs.* FROM
						rule_stops rs
						-- loaded rule stop
						JOIN (
							SELECT
								rs1.cid,
								rs1.route_id,
								rs1.route_rule_id,
								rs1.order_num
							FROM
								rule_stops rs1
							WHERE
								rs1.id = ' . ( int ) $intRuleStopId . '
								AND rs1.cid = ' . ( int ) $intCid . '

						) lrs ON ( lrs.cid = rs.cid AND lrs.route_id = rs.route_id AND lrs.route_rule_id = rs.route_rule_id AND lrs.order_num > rs.order_num )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.deleted_on IS NULL
					ORDER BY
						rs.order_num DESC
					LIMIT 1';

		return self::fetchRuleStop( $strSql, $objDatabase );
	}

	public static function fetchNextRuleStopByRuleStopIdByCid( $intRuleStopId, $intCid, $objDatabase ) {

		$strSql = 'SELECT rs.* FROM
						rule_stops rs
						-- loaded rule stop
						JOIN (
							SELECT
								rs1.cid,
								rs1.route_id,
								rs1.route_rule_id,
								rs1.order_num
							FROM
								rule_stops rs1
							WHERE
								rs1.id = ' . ( int ) $intRuleStopId . '
								AND rs1.cid = ' . ( int ) $intCid . '

						) lrs ON ( lrs.cid = rs.cid AND lrs.route_id = rs.route_id AND lrs.route_rule_id = rs.route_rule_id AND lrs.order_num < rs.order_num )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.deleted_on IS NULL
					ORDER BY
						rs.order_num DESC
					LIMIT 1';

		return self::fetchRuleStop( $strSql, $objDatabase );
	}

}
?>