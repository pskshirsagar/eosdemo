<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueRateConstraints
 * Do not add any new functions to this class.
 */

class CRevenueRateConstraints extends CBaseRevenueRateConstraints {

    public static function fetchRevenueRateConstraintsByPropertyIdByRevenueRateConstraintTypeIdByCid( $intPropertyId, $arrintConstraintTypeIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintConstraintTypeIds ) ) return NULL;

    	$strSql = 'SELECT * FROM
    					revenue_rate_constraints
    				WHERE
    					property_id = ' . ( int ) $intPropertyId . '
    					AND cid =  ' . ( int ) $intCid . '
    					AND revenue_rate_constraint_type_id IN ( ' . implode( ',', $arrintConstraintTypeIds ) . ' )';

    	return self::fetchRevenueRateConstraints( $strSql, $objDatabase );
    }

    public static function fetchRevenueRateConstraintsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT * FROM
    					revenue_rate_constraints
    				WHERE
    					property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
    					AND cid =  ' . ( int ) $intCid . '
    				ORDER BY
    					revenue_rate_constraint_type_id';

    	return self::fetchRevenueRateConstraints( $strSql, $objDatabase );
    }

    public static function fetchEnabledRevenueRateConstraintsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intIsStudentProperty = 0 ) {
		if( 1 == $intIsStudentProperty ) {
			$strConstraintTypeIdCondition = 'AND revenue_rate_constraint_type_id > 12';
		} else {
			$strConstraintTypeIdCondition = 'AND revenue_rate_constraint_type_id <= 12';
		}
    	$strSql = 'SELECT * FROM
    					revenue_rate_constraints
    				WHERE
    					property_id =  ' . ( int ) $intPropertyId . '
    					AND cid =  ' . ( int ) $intCid . '
    					' . $strConstraintTypeIdCondition . '
    				ORDER BY
    					revenue_rate_constraint_type_id';

    	return self::fetchRevenueRateConstraints( $strSql, $objDatabase );
    }

    public static function fetchEnabledRevenueRateConstraintsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT * FROM
    					revenue_rate_constraints
    				WHERE
    					property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
    					AND cid =  ' . ( int ) $intCid . '
    					AND is_disabled = 0
    				ORDER BY
    					revenue_rate_constraint_type_id';

    	return self::fetchRevenueRateConstraints( $strSql, $objDatabase );
    }

	public static function fetchPropertyIdsRevenueRateConstraintsRecordsByPropertyIdByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT property_id FROM
    					revenue_rate_constraints
    				WHERE
    					property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
    					AND cid =  ' . ( int ) $intCid . '
    					AND is_disabled = 0
    				GROUP BY
    					property_id';

		return fetchData( $strSql, $objDatabase );

	}

}
?>