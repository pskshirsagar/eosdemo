<?php

class CMoveInSchedule extends CBaseMoveInSchedule {

	private $m_arrintLeaseStartWindowIds;
	private $m_arrintLeaseTermIds;
	private $m_strName;
	private $m_intOccupancyTypeId;
	private $m_intCalendarEventId;

	const SCHEDULE_TRIGGER_LEASE_APPROVED                  = 'lease_approved';
	const SCHEDULE_TRIGGER_UNIT_ASSIGNED_AND_LOCKED        = 'upon_unit_assignment';
	const SCHEDULE_TRIGGER_MANUALLY                        = 'manually';
	const CHECKLIST_ITEM_REQUIRED_COMPLETE_FOR_APPOINTMENT = 'CIC_REQ';
	const CHECKLIST_ITEM_NOT_REQUIRED_FOR_APPOINTMENT      = 'CIC_OPT';
	const CONVENTIONAL_MOVE_IN_SCHEDULE_NAME      = 'Conventional move in scheduler';
	/**
	 * CONVENTIONAL_MOVE_IN_SCHEDULE_START_DATE date is used for the Conventional move in scheduler weekly calendar
	 * to get start day as Sunday setting date as 01/04/1970
	 */

	const CONVENTIONAL_MOVE_IN_SCHEDULE_START_DATE = '01/04/1970';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledMoveInDate() {
		$boolIsValid = true;

		if( COccupancyType::STUDENT != $this->m_intOccupancyTypeId ) {
			return $boolIsValid;
		}

		if( false == valStr( $this->getScheduledMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Please select move-in date.' ) ) );

		} elseif( strtotime( $this->getScheduledMoveInDate() ) < strtotime( date( 'm/d/Y' ) ) && false == valId( $this->getDeletedBy() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date should not be in past.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAppointmentsCount() {
		$boolIsValid = true;

		if( ( false == valId( $this->getAppointmentCap() ) || $this->getAppointmentCap() <= 0 ) && $this->getUseAppointments() == 1 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please enter valid Appointment Count.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valScheduledMoveInStartTime() {
		$boolIsValid = true;
		if( false == valStr( $this->getScheduledMoveInStartTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_time', __( 'Move in start time is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valScheduledMoveInEndTime() {
		$boolIsValid = true;
		if( false == valStr( $this->getScheduledMoveInEndTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_time', __( 'Move in end time is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valScheduledMoveInTime( $boolIsValid = true ) {

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$boolIsValid = true;

		if( strtotime( $this->getScheduledMoveInStartTime() ) >= strtotime( $this->getScheduledMoveInEndTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_time', __( 'Start Time should be less than End Time.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMoveInScheduleDateWithOverlappedLeaseTerm( $objDatabase, $boolIsValid = true ) {

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == valArr( $this->getLeaseTermIds() ) && true == valArr( $this->getLeaseStartWindowIds() ) ) {
			$objMoveInSchedulerService = new CMoveInSchedulerService();
			$objMoveInSchedulerService->setCid( $this->getCid() );
			$objMoveInSchedulerService->setPropertyId( $this->getPropertyId() );
			$objMoveInSchedulerService->setLeaseStartWindowIds( $this->getLeaseStartWindowIds() );
			$objMoveInSchedulerService->setLeaseTermIds( $this->getLeaseTermIds() );
			$objMoveInSchedulerService->setDatabase( $objDatabase );

			$arrmixLeaseTermData = $objMoveInSchedulerService->loadMoveInScheduleGroupLeaseTerms( true );
			if( strtotime( $this->getScheduledMoveInDate() ) > $arrmixLeaseTermData['time']['min_end_date'] ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in dates must be set within the least terms included in the schedule.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;

	}

	public function valConventionalMoveInScheduleOverlappedWithExistingMoveInSchedule( $objDatabase, $boolIsValid = true ) {

		if( false == $boolIsValid || COccupancyType::CONVENTIONAL != $this->m_intOccupancyTypeId ) {
			return $boolIsValid;
		}

			$strWhere = ' WHERE cid = ' . $this->m_intCid . ' AND property_id = ' . ( int ) $this->m_intPropertyId . ' AND move_in_schedule_type_id = ' . ( int ) CMoveInScheduleType::PROPERTY . ' AND day_of_week = ' . ( int ) $this->m_intDayOfWeek . ' AND ( scheduled_move_in_start_time::time, scheduled_move_in_end_time::time ) OVERLAPS ( \'' . $this->m_strScheduledMoveInStartTime . '\'::time, \'' . $this->m_strScheduledMoveInEndTime . '\'::time' . ') AND deleted_on IS NULL';

			if( true == valId( $this->getId() ) ) {
				$strWhere .= ' AND id != ' . ( int ) $this->getId();
			}

			$intCount = ( int ) \Psi\Eos\Entrata\CMoveInSchedules::createService()->fetchMoveInScheduleCount( $strWhere, $objDatabase );

			if( 0 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'This schedule is overlapping with existing schedule.' ) ) );

				$boolIsValid = false;
			}

		return $boolIsValid;

	}

	public function valConventionalPropertyOfficeHours( $objDatabase, $boolIsLunchTime = false, $boolIsValid = true ) {

		if( false == $boolIsValid || COccupancyType::CONVENTIONAL != $this->m_intOccupancyTypeId ) {
			return $boolIsValid;
		}

		$boolAllowAppointmentsOutsideBusinessHrs = \Psi\Eos\Entrata\CMoveInScheduleGroups::createService()->fetchIsAllowAppointmentCreationOutsideOfBusinessHours( $this->getPropertyId(), $this->getCId(), $objDatabase );

		if( true == ( bool ) $boolAllowAppointmentsOutsideBusinessHrs ) return true;

		$intLastDayOfWeek = CPropertyHour::MAX_PROPERTY_HOUR_DAYS - 1;

		$arrintDays = \Psi\Eos\Entrata\CPropertyHours::createService()->fetchPropertyHourDaysByPropertyIdByOpenTimeCloseTimeByDayByCid( $this->m_intPropertyId, $this->m_strScheduledMoveInStartTime, $this->m_strScheduledMoveInEndTime, $this->m_intDayOfWeek, $this->m_intCid, $objDatabase, CPropertyHourType::OFFICE_HOURS, true, $boolIsLunchTime );

		if( false == valArr( $arrintDays ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'This Appointment should be in office hours.' ) ) );
			return false;
		}

		foreach( $arrintDays as $arrintDay ) {
			if( $intLastDayOfWeek < $arrintDay['day'] ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'This Appointment should be in office hours.' ) ) );
				return false;
			}
		}

		return true;

	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valScheduledMoveInDate();
				$boolIsValid &= $this->valScheduledMoveInStartTime();
				$boolIsValid &= $this->valScheduledMoveInEndTime();
				$boolIsValid &= $this->valScheduledMoveInTime( $boolIsValid );
				$boolIsValid &= $this->valAppointmentsCount();
				$boolIsValid &= $this->valMoveInScheduleDateWithOverlappedLeaseTerm( $objDatabase, $boolIsValid );
				$boolIsValid &= $this->valConventionalMoveInScheduleOverlappedWithExistingMoveInSchedule( $objDatabase, $boolIsValid );
				$boolIsValid &= $this->valConventionalPropertyOfficeHours( $objDatabase, false, $boolIsValid );
				$boolIsValid &= $this->valConventionalPropertyOfficeHours( $objDatabase, true, $boolIsValid );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createMoveInScheduleAssociation() {
		$objMoveInScheduleAssociation = new CMoveInScheduleAssociation();
		$objMoveInScheduleAssociation->setCid( $this->getCid() );
		$objMoveInScheduleAssociation->setPropertyId( $this->getPropertyId() );
		$objMoveInScheduleAssociation->setMoveInScheduleId( $this->getId() );
		$objMoveInScheduleAssociation->setUseAppointments( $this->getUseAppointments() );
		$objMoveInScheduleAssociation->setMoveInScheduleGroupId( $this->getMoveInScheduleGroupId() );

		return $objMoveInScheduleAssociation;
	}

	public function setLeaseTermIds( $arrintLeaseTermId ) {
		$this->m_arrintLeaseTermIds = $arrintLeaseTermId;
	}

	public function getLeaseTermIds() {
		return $this->m_arrintLeaseTermIds;
	}

	public function setLeaseStartWindowIds( $arrintLeaseStartWindowId ) {
		$this->m_arrintLeaseStartWindowIds = $arrintLeaseStartWindowId;
	}

	public function getLeaseStartWindowIds() {
		return $this->m_arrintLeaseStartWindowIds;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setMoveInScheduleTypeId( $intMoveInScheduleTypeId ) {
		$this->m_intMoveInScheduleTypeId = $intMoveInScheduleTypeId;
	}

	public function getMoveInScheduleTypeId() {
		return $this->m_intMoveInScheduleTypeId;
	}

	public function setMoveInScheduleName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getMoveInScheduleName() {
		return $this->m_strName;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function setCalendarEventId( $intCalendarEventId ) {
		$this->m_intCalendarEventId = $intCalendarEventId;
	}

	public function getCalendarEventId() {
		return $this->m_intCalendarEventId;
	}

}
?>
