<?php

class CClAdFloorplan extends CBaseClAdFloorplan {
	protected $m_fltMaxSquareFeet;
	protected $m_fltMinSquareFeet;
	protected $m_intMaxBedrooms;
	protected $m_fltMaxBathrooms;

    /**
     * Get Functions
     */

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['min_square_feet'] ) ) 		$this->setMinSquareFeet( $arrmixValues['min_square_feet'] );
    	if( true == isset( $arrmixValues['max_square_feet'] ) ) 		$this->setMaxSquareFeet( $arrmixValues['max_square_feet'] );
    	if( true == isset( $arrmixValues['max_bedrooms'] ) ) 			$this->setMaxBedrooms( $arrmixValues['max_bedrooms'] );
    	if( true == isset( $arrmixValues['max_bathrooms'] ) ) 			$this->setMaxBathrooms( $arrmixValues['max_bathrooms'] );
    	return;
    }

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClAdId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClAdSlideId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMinRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMinDeposit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxDeposit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }

	/**
	* Create Functions
	*
	*/

}
?>