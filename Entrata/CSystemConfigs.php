<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemConfigs
 * Do not add any new functions to this class.
 */

class CSystemConfigs extends CBaseSystemConfigs {

	public static function fetchSystemConfigByKey( $strKey, $objDatabase ) {
		$strSql = "SELECT * FROM system_configs WHERE key='" . trim( $strKey ) . "'";
		return self::fetchSystemConfig( $strSql, $objDatabase );
	}

	public static function fetchSystemConfigByKeys( $arrstrKeys, $objDatabase ) {
		$strSql = 'SELECT * FROM system_configs WHERE key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' )';
		return self::fetchSystemConfigs( $strSql, $objDatabase );
	}

}
?>