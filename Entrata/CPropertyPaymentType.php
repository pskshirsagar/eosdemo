<?php

class CPropertyPaymentType extends CBasePropertyPaymentType {

	// Platforms/Portals
	const PLATFORM_ENTRATA			= 1;
	const PLATFORM_RESIDENT_PORTAL	= 2;
	const PLATFORM_PROSPECT_PORTAL	= 3;

	protected $m_intArCodeCount;
	protected $m_intDefaultArCodeId;
	protected $m_strPaymentTypeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInEntrata() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInResidentPortal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInProspectPortal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCertifiedFunds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Get Functions
	*
	*/

	public function getArCodeCount() {
		return $this->m_intArCodeCount;
	}

	public function getDefaultArCodeId() {
		return $this->m_intDefaultArCodeId;
	}

	public function getPaymentTypeName() {
		return $this->m_strPaymentTypeName;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['ar_code_count'] ) )		$this->setArCodeCount( $arrmixValues['ar_code_count'] );
		if( true == isset( $arrmixValues['default_ar_code_id'] ) )	$this->setDefaultArCodeId( $arrmixValues['default_ar_code_id'] );
		if( true == isset( $arrmixValues['payment_type_name'] ) )	$this->setPaymentTypeName( $arrmixValues['payment_type_name'] );

		return;
	}

	public function setArCodeCount( $intArCodeCount ) {
		$this->m_intArCodeCount = $intArCodeCount;
	}

	public function setDefaultArCodeId( $intDefaultArCodeId ) {
		$this->m_intDefaultArCodeId = $intDefaultArCodeId;
	}

	public function setPaymentTypeName( $strPaymentTypeName ) {
		$this->m_strPaymentTypeName = $strPaymentTypeName;
	}

	/**
	 * Other Functions
	 *
	 */

	public static function getPlatformIdByProductId( $intProductId ) {
		$intPlatformId = NULL;

		switch( $intProductId ) {
			case CPsProduct::RESIDENT_PORTAL:
			case CPsProduct::MOBILE_PORTAL:
			case CPsProduct::API_SERVICES:
			case CPsProduct::FACEBOOK_INTEGRATION:
			case CPsProduct::RESIDENT_PAY:
			case CPsProduct::SERVICES:
				$intPlatformId = self::PLATFORM_RESIDENT_PORTAL;
				break;

			case CPsProduct::PROSPECT_PORTAL:
			case CPsProduct::ONLINE_APPLICATION:
			case CPsProduct::SITE_TABLET:
				$intPlatformId = self::PLATFORM_PROSPECT_PORTAL;
				break;

			case CPsProduct::ENTRATA:
			case CPsProduct::SEM_SERVICES:
			case CPsProduct::ENTRATA_PAAS:
				$intPlatformId = self::PLATFORM_ENTRATA;
				break;

			default:
				$intPlatformId = NULL;
				break;
		}

		return $intPlatformId;
	}

	public static function getWhiteListedProductIds() {
		return [
			CPsProduct::RESIDENT_PAY_DESKTOP
		];
	}

	public static function getPropertyPaymentTypeFilter( $arrboolPropertyPaymentTypePlatformIds = NULL, $boolIsCertifiedFunds = NULL ) {
		$arrmixPropertyPaymentTypeFilter = [
			'allow_in_entrata'			=> NULL,
			'allow_in_resident_portal'	=> NULL,
			'allow_in_prospect_portal'	=> NULL,
			'is_certified_funds'		=> NULL
		];

		foreach( ( array ) $arrboolPropertyPaymentTypePlatformIds as $intPropertyPaymentTypePlatformId => $boolValue ) {
			switch( $intPropertyPaymentTypePlatformId ) {
				case self::PLATFORM_ENTRATA:
					$arrmixPropertyPaymentTypeFilter['allow_in_entrata'] = $boolValue;
					break;

				case self::PLATFORM_RESIDENT_PORTAL;
					$arrmixPropertyPaymentTypeFilter['allow_in_resident_portal'] = $boolValue;
					break;

				case self::PLATFORM_PROSPECT_PORTAL;
					$arrmixPropertyPaymentTypeFilter['allow_in_prospect_portal'] = $boolValue;
					break;

				default:
					// unrecognized platform id
					break;
			}
		}

		$arrmixPropertyPaymentTypeFilter['is_certified_funds'] = $boolIsCertifiedFunds;

		return $arrmixPropertyPaymentTypeFilter;
	}

	public static function buildSqlFromPropertyPaymentTypeFilter( $arrmixPropertyPaymentTypeFilter, $strTableAlias = '' ) {
		if( true == valStr( $strTableAlias ) ) {
			$strTableAlias .= '.';
		}

		$arrstrConditions = [];

		foreach( $arrmixPropertyPaymentTypeFilter as $strColumn => $boolValue ) {
			if( NULL !== $boolValue ) {
				$arrstrConditions[] = sprintf( $strTableAlias . $strColumn . " = '%d'", $boolValue );
			}
		}

		return ( true == valArr( $arrstrConditions ) ) ? implode( ' AND ', $arrstrConditions ) : '';
	}

}
?>