<?php

class CCareerStatusType extends CBaseCareerStatusType {

	const OPEN      = 1;
	const SUSPENDED = 2;
	const ACCEPTED  = 3;
	const FILLED    = 4;
	const ABONDED   = 5;
	const ACCEPTING = 6;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// No Default case.
            	break;
        }

        return $boolIsValid;
    }
}
?>