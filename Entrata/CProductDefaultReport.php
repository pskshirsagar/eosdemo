<?php

class CProductDefaultReport extends CBaseProductDefaultReport {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultReportId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }
        return $boolIsValid;
    }

    public function deleteProductReport( $intDefaultreportId, $objAdminDatabase, $boolReturnSqlOnly = false ) {
    	$objDataset = $objAdminDatabase->createDataset();

    	$strSql = 'SELECT
						*
					FROM
						func_product_report_permission_delete(' . ( int ) $intDefaultreportId . ' );';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

    	if( false == $objDataset->execute( $strSql ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to product reports. The following error was reported.' ) );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

    		$objDataset->cleanup();

    		return false;
    	}
    	$objDataset->cleanup();

    	return true;
    }

    public function insertProductReport( $intDefaultreportId, $intPsProductId, $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

    	$objDataset = $objAdminDatabase->createDataset();
    	$strPsProductIdSql = ( true == $intPsProductId ) ? $intPsProductId : 'NULL';
    	$strSql = 'SELECT
						*
					FROM
						func_product_report_permission_insert(' . ( int ) $intDefaultreportId . ', '
    								. $strPsProductIdSql . ', '
    										. ( int ) $intCurrentUserId .
    										'); ';
		if( true == $boolReturnSqlOnly ) {
		    return $strSql;
		}
    	if( false == $objDataset->execute( $strSql ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to product reports. The following error was reported.' ) );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

    		$objDataset->cleanup();

    		return false;
    	}
    	$objDataset->cleanup();

    	return true;
    }

}
?>