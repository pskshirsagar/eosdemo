<?php

class CWebsiteProperty extends CBaseWebsiteProperty {

	/**
	 * Get Functions
	 */

	public function getUrl() {
		return $this->getJsonbFieldValue( 'SlugUrl', 'url' );
	}

	public function getTargetWebsiteId() {
		return $this->getJsonbFieldValue( 'SlugUrl', 'target_website_id' );
	}

	public function getJsonbFieldValue( $strMethodName, $strKey ) {
		$strMethodName = 'get' . $strMethodName;

		return ( false == is_null( $this->$strMethodName() ) && true == property_exists( $this->$strMethodName(), $strKey ) ? $this->$strMethodName()->$strKey : NULL );
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function fetchWebsite( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsites::createService()->fetchWebsiteByIdByCid( $this->m_intWebsiteId, $this->getCid(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

}
?>