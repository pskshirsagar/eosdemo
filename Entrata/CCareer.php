<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CCareer extends CBaseCareer {

	use Psi\Libraries\EosFoundation\TEosStoredObject;
	protected $m_strCareerStatusTypeName;
	protected $m_strCareerTypeName;
	protected $m_strStateCode;
	protected $m_strCity;
	protected $m_strPostalCode;
	protected $m_strPropertyName;

    /**
     * Get Functions
     */

    public function getCareerStatusTypeName() {
    	return $this->m_strCareerStatusTypeName;
    }

    public function getCareerTypeName() {
    	return $this->m_strCareerTypeName;
    }

    public function getStateCode() {
    	return $this->m_strStateCode;
    }

    public function getCity() {
    	return $this->m_strCity;
    }

    public function getPostalCode() {
    	return $this->m_strPostalCode;
    }

    public function getIsForProspectPortal() {
    	return $this->m_boolIsForProspectPortal;
    }

    public function getDeleteExistingApplicationDocument() {
    	return $this->m_boolDeleteExistingApplicationDocument;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    /**
     * Set Functions
     */

    public function setCareerStatusTypeName( $strCareerStatusTypeName ) {
    	$this->m_strCareerStatusTypeName = $strCareerStatusTypeName;
    }

    public function setCareerTypeName( $strCareerTypeName ) {
    	$this->m_strCareerTypeName = $strCareerTypeName;
    }

    public function setStateCode( $strStateCode ) {
    	$this->m_strStateCode = $strStateCode;
    }

    public function setCity( $strCity ) {
    	$this->m_strCity = $strCity;
    }

    public function setPostalCode( $strPostalCode ) {
    	$this->m_strPostalCode = $strPostalCode;
    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setIsForProspectPortal( $boolIsForProspectPortal ) {
    	$this->m_boolIsForProspectPortal = $boolIsForProspectPortal;
    }

    public function setDeleteExistingApplicationDocument( $boolDeleteExistingApplicationDocument ) {
    	$this->m_boolDeleteExistingApplicationDocument = $boolDeleteExistingApplicationDocument;
    }

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrstrValues['career_status_type_name'] ) ) 	$this->setCareerStatusTypeName( $arrstrValues['career_status_type_name'] );
    	if( true == isset( $arrstrValues['career_type_name'] ) ) 			$this->setCareerTypeName( $arrstrValues['career_type_name'] );
    	if( true == isset( $arrstrValues['state_code'] ) ) 				$this->setStateCode( $arrstrValues['state_code'] );
    	if( true == isset( $arrstrValues['postal_code'] ) ) 				$this->setPostalCode( $arrstrValues['postal_code'] );
    	if( true == isset( $arrstrValues['city'] ) ) 						$this->setCity( $arrstrValues['city'] );
    	if( true == isset( $arrstrValues['property_name'] ) )				$this->setPropertyName( $arrstrValues['property_name'] );
    	return;
    }

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCareerTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getCareerTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'career_type_id', __( 'Type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCareerStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCareerApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyDepartmentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valManagerEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRecruiterEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHiringManagerName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

 	public function valTitle() {
        $boolIsValid = true;

        if( true == is_null( $this->getTitle() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLegalTerms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMinCompensation() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxCompensation() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFaxNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSpecialRequirements() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCareerShiftType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationFilePath() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationFileName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valCareerAvailableOn() {

    	$boolIsValid = true;

        if( false == is_null( $this->getCareerAvailableOn() ) && 0 < strlen( $this->getCareerAvailableOn() ) && 1 !== CValidation::checkDate( $this->getCareerAvailableOn() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'career_available_on', __( 'Available On date must be valid and in mm/dd/yyyy form.' ) ) );
        }

        return $boolIsValid;
    }

 	public function valSelectionScheduledOn() {

        $boolIsValid = true;

         if( false == is_null( $this->getSelectionScheduledOn() ) && 0 < strlen( $this->getSelectionScheduledOn() ) && 1 !== CValidation::checkDate( $this->getSelectionScheduledOn() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'selection_scheduled_on', __( 'Selection scheduled on date must be valid and in mm/dd/yyyy form.' ) ) );
         }

        return $boolIsValid;
    }

	public function valListDatetime() {

        $boolIsValid = true;

         if( false == is_null( $this->getListDatetime() ) && 0 < strlen( $this->getListDatetime() ) && 1 !== CValidation::checkDate( $this->getListDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'list_datetime', __( 'List datetime must be valid and in mm/dd/yyyy form.' ) ) );
         }

        return $boolIsValid;
    }

	public function valHideDatetime() {

        $boolIsValid = true;

         if( false == is_null( $this->getHideDatetime() ) && 0 < strlen( $this->getHideDatetime() ) && 1 !== CValidation::checkDate( $this->getHideDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hide_datetime', __( 'Hide datetime must be valid and in mm/dd/yyyy form.' ) ) );
         }

        if( false == is_null( $this->getHideDatetime() ) && true == $boolIsValid && ( strtotime( $this->getListDatetime() ) > strtotime( $this->getHideDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hide_datetime', __( 'Hide datetime must be greater than list datetime.' ) ) );
		}

        return $boolIsValid;
    }

    public function valKeepLocationAnonymous() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valPropertyId();
            		$boolIsValid &= $this->valTitle();
            		$boolIsValid &= $this->valCareerTypeId();
            		$boolIsValid &= $this->valCareerAvailableOn();
            		$boolIsValid &= $this->valSelectionScheduledOn();
            		$boolIsValid &= $this->valListDatetime();
            		$boolIsValid &= $this->valHideDatetime();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// No Default case.
            	break;
        }

        return $boolIsValid;
    }

    public function validateCareer( $arrstrCareer ) {

    	$this->setValues( $arrstrCareer );

    	$boolIsValid = true;
		$arrstrErrorMessages = [];
		$strErrorMessages = '';

		if( true == is_null( $this->getPropertyId() ) ) {
			$arrstrErrorMessages[] = __( 'Property is required.' );
		}

		if( true == is_null( $this->getTitle() ) ) {
			$arrstrErrorMessages[] = __( 'Position title is required.' );
		}

		if( true == is_null( $this->getCareerTypeId() ) ) {
			$arrstrErrorMessages[] = __( 'Position type is required.' );
		}

		if( false == is_null( $this->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$arrstrErrorMessages[] = __( 'Email does not appear to be valid.' );
		}

		if( false == is_null( $this->getPhoneNumber() ) && ( false == \Psi\CStringService::singleton()->preg_match( '/^\(?\d{3}\)?[-\.\s]?\d{3}[-\.\s]?\d{4}$/', $this->getPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$arrstrErrorMessages[] = __( 'Phone number should be in {%h, 0, STANDARD} format', [ 'xxxxxxxxxx' ] );
		}

		$boolIsValid = true;
		if( false == is_null( $this->getFaxNumber() ) && ( false == \Psi\CStringService::singleton()->preg_match( '/^\(?\d{3}\)?[-\.\s]?\d{3}[-\.\s]?\d{4}$/', $this->getFaxNumber() ) ) ) {
			$boolIsValid = false;
			$arrstrErrorMessages[] = __( 'Fax number should be in {%h, 0, STANDARD} format', [ 'xxxxxxxxxx' ] );
		}

		if( false == is_null( $this->getCareerAvailableOn() ) && 0 < strlen( $this->getCareerAvailableOn() ) && 1 !== CValidation::checkDate( $this->getCareerAvailableOn() ) ) {
			$arrstrErrorMessages[] = __( '\'Available on date must be valid and in mm/dd/yyyy form.\'' );
		}

		if( false == is_null( $this->getSelectionScheduledOn() ) && 0 < strlen( $this->getSelectionScheduledOn() ) && 1 !== CValidation::checkDate( $this->getSelectionScheduledOn() ) ) {
			$arrstrErrorMessages[] = __( 'Selection scheduled on date must be valid and in mm/dd/yyyy form.' );
		}

		if( false == is_null( $this->getListDatetime() ) && 0 < strlen( $this->getListDatetime() ) && 1 !== CValidation::checkDate( $this->getListDatetime() ) ) {
			$arrstrErrorMessages[] = __( 'List datetime must be valid and in mm/dd/yyyy form.' );
		}

		$boolIsValid = true;
		if( false == is_null( $this->getHideDatetime() ) && 0 < strlen( $this->getHideDatetime() ) && 1 !== CValidation::checkDate( $this->getHideDatetime() ) ) {
			$boolIsValid = false;
			$arrstrErrorMessages[] = __( 'Hide datetime must be valid and in mm/dd/yyyy form.' );
		}

		if( false == is_null( $this->getHideDatetime() ) && true == $boolIsValid && ( strtotime( $this->getListDatetime() ) > strtotime( $this->getHideDatetime() ) ) ) {
			$arrstrErrorMessages[] = __( 'Hide datetime must be greater than list datetime.' );
		}

		if( false == is_null( $strErrorMessages ) ) {
			$strErrorMessages = implode( ' ', $arrstrErrorMessages );
		}

		return $strErrorMessages;
    }

    /**
     * Other Functions
     */

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$boolIsValid 	= true;
    	$boolIsInsert 	= false;

    	if( true == is_null( $this->getId() ) && false == is_numeric( $this->getId() ) ) {
    		$boolIsInsert = true;
    		$this->setId( $this->fetchNextId( $objDatabase ) );
    	}

	    if( true == $this->getDeleteExistingApplicationDocument() && 0 < strlen( $this->getApplicationFileName() ) ){
			    $this->setApplicationFilePath( NULL );
			    $this->setApplicationFileName( NULL );
	    }

    	if( false == empty( $_FILES['application_file']['name'] ) ) {
    		$this->setApplicationFileName( CFileUpload::cleanFilename( $_FILES['application_file']['name'] ) );
    		$this->setApplicationFilePath( 'career_application' . '/' . $this->getCid() . '/' . date( 'Y/m/d/his' ) . '/' . $this->getId() . '/' );
    	}

    	if( true == $boolIsValid && true == $boolIsInsert ) {
    		$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} elseif( true == $boolIsValid ) {
    		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

    	return $boolIsValid;
    }

    public function getFullApplicationFilePath() {
    	return getMountsPath( $this->getCid(), PATH_MOUNTS_CAREER_APPLICATION ) . $this->m_intId;
    }

    public function uploadFiles( $objDatabase ) {
    	$boolIsValid = true;

    	$strApplicationFilePath 	= $this->getFullApplicationFilePath();

    	if( false == CFileIo::isDirectory( $strApplicationFilePath ) ) {

    		if( false == CFileIo::recursiveMakeDir( $strApplicationFilePath ) ) {
			    trigger_error( __( 'Couldn\'t create directory({%s, 0})', [ $strApplicationFilePath ] ), E_USER_ERROR );
    			return false;
    		}
    	}

    	$objUpload = new CFileUpload();

    	$this->m_arrobjFileExtensions	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchAllFileExtensions( $objDatabase );

    	foreach( $this->m_arrobjFileExtensions as $objFileExtension ) {
    		$arrstrAllowedTypes[] = $objFileExtension->getExtension();
    	}

    	$objUpload->setAcceptableTypes( $arrstrAllowedTypes );

    	$objUpload->setOverwriteMode( CFileUpload::MODE_OVERWRITE );

    	if( false == empty( $_FILES['application_file']['name'] ) ) {

    		$strUploadedApplicationFile = $objUpload->upload( 'application_file', $strApplicationFilePath );

    		if( false == is_string( $strUploadedApplicationFile ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Attachment', __( 'Invalid file specified for application file.' ) ) );
    			$boolIsValid = false;
    		}
    	}

    	return $boolIsValid;
    }

    public function deleteApplicationDocument( $strApplicationDocumentUri ) {
    	$boolIsValid = true;

    	if( true == CFileIo::fileExists( $strApplicationDocumentUri ) ) {
    		if( false == CFileIo::deleteFile( $strApplicationDocumentUri ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to delete.' ) ) );
    			trigger_error( __( 'Failed to delete existing application document' ), E_USER_WARNING );
    		}
    	}

    	return $boolIsValid;
    }

    public function deleteApplicationDocumentDirectory( $strApplicationFilePath ) {
    	$boolIsValid = true;

    	if( true == CFileIo::isDirectory( $strApplicationFilePath ) ) {
    		if( false == CFileIo::recursiveRemoveDir( $strApplicationFilePath ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to delete.' ) ) );
    			trigger_error( __( 'Failed to delete existing application document directory' ), E_USER_WARNING );
    		}
    	}

    	return $boolIsValid;
    }

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return $this->getId() . '/' .$this->getApplicationFileName();

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
			default:
				return $this->getApplicationFilePath() . '' .$this->getApplicationFileName();

		}
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return 'career_application/';

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
			default:
				return CONFIG_OSG_BUCKET_CAREERS;

		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}
}
?>