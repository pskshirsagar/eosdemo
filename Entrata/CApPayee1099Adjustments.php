<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayee1099Adjustments
 * Do not add any new functions to this class.
 */

class CApPayee1099Adjustments extends CBaseApPayee1099Adjustments {

	public static function fetchApPayee1099AdjustmentsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apa.*,
						p.property_name
					FROM
						ap_payee_1099_adjustments apa
						JOIN properties p ON ( apa.cid = p.cid AND apa.property_id = p.id )
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
					ORDER BY
						LOWER( p.property_name ),
						apa.year ASC';

		return self::fetchApPayee1099Adjustments( $strSql, $objClientDatabase );
	}

	public static function fetchApPayee1099AdjustmentsByApPayeeIdByCidByLegalEntityId( $intApPayeeId, $intCid, $intApLegalEntityId, $objClientDatabase ) {

		if( false == valId( $intApLegalEntityId ) || false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apa.*,
						p.property_name
					FROM
						ap_payee_1099_adjustments apa
						JOIN properties p ON ( apa.cid = p.cid AND apa.property_id = p.id )
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apa.ap_legal_entity_id = ' . ( int ) $intApLegalEntityId . '
					ORDER BY
						LOWER( p.property_name ),
						apa.year ASC';

		return self::fetchApPayee1099Adjustments( $strSql, $objClientDatabase );
	}
}
?>