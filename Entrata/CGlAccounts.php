<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccounts
 * Do not add any new functions to this class.
 */
class CGlAccounts extends CBaseGlAccounts {

	public static function fetchGlAccountBySystemCodeByGlAccountUsageTypeIdByCid( $strSystemCode, $intGlAccountUsageTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\'  )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.system_code = \'' . $strSystemCode . '\'
						AND ga.gl_account_usage_type_id = ' . ( int ) $intGlAccountUsageTypeId . '
						AND ga.disabled_on IS NULL
					LIMIT 1';

		return self::fetchGlAccount( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.*,
						gat.gl_account_id as id,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name
					FROM
						gl_account_trees gat
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.disabled_by IS NULL
					    AND gat.is_default = 1
					ORDER BY
						gat.account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchAllGlAccountsBySystemCodeByCid( $strGlChartSystemCode, $intCid, $objClientDatabase, $boolAllowPostingToRestrictedGlAccount = false ) {

		if( is_null( $intCid ) || is_null( $strGlChartSystemCode ) ) {
			return NULL;
		}

		$strWhereCondition		= ( false == $boolAllowPostingToRestrictedGlAccount ) ? ' AND ga.restrict_for_gl = false' : NULL;

		$strSql = 'SELECT
						ga.*,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						glat.name AS gl_account_type_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
					ORDER BY
						gat.account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByGlChartId( $intCid, $intGlChartId, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*
					FROM
						gl_accounts ga
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_chart_id =' . ( int ) $intGlChartId . '
					ORDER BY
						ga.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleGlAccountsByCidByGlChartId( $intCid, $intGlChartId, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_chart_id =' . ( int ) $intGlChartId . '
					ORDER BY
						gat.formatted_account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByIdsByCid( $arrintIds, $intCid, $objClientDatabase, $boolJEAllowPostingToRestrictedGlAccounts = false ) {

		if( false == valArr( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) return NULL;
		$strWhereCondition = ( false == $boolJEAllowPostingToRestrictedGlAccounts ) ? ' AND ga.restrict_for_gl = false' : NULL;

		$strSql = 'SELECT
						ga.*,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ga.id IN ( ' . implode( ',', $arrintIds ) . ' )
					ORDER
						BY gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleGlAccountsByIdsByFieldNamesCid( $arrintGlAccountIds, $arrstrFieldNames, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlAccountIds ) || false == valArr( $arrstrFieldNames ) ) return NULL;

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
					WHERE
						ga.id IN ( ' . implode( ',', $arrintGlAccountIds ) . ' )
						AND ga.cid = ' . ( int ) $intCid . '
					ORDER BY
						gat.formatted_account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomGlAccountByIdByCid( $intCid, $intGlAccountId, $objClientDatabase ) {
		if( false == valId( $intGlAccountId ) ) return NULL;

		$strSql = 'SELECT
						ga.*,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						glat.name AS gl_account_type_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.id = ' . ( int ) $intGlAccountId . '
						AND ga.cid = ' . ( int ) $intCid;

		return self::fetchGlAccount( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountByCidByGlChartIdByAccountNumber( $intCid, $intGlChartId, $strAccountNumber, $objClientDatabase ) {
		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_chart_id = ' . ( int ) $intGlChartId . '
						AND gat.formatted_account_number = ' . ( string ) pg_escape_literal( $objClientDatabase->getHandle(), $strAccountNumber ) . '';
		return self::fetchGlAccount( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountsWithGlAccountTypeNameByCid( $intCid, $objClientDatabase, $boolShowDisabledGlAccounts = false ) {

		$strCondition = ( false == $boolShowDisabledGlAccounts ) ? 'AND ga.disabled_on IS NULL' : '';

		$strSql = 'SELECT
						ga.*,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						glat.name AS gl_account_type_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid AND gat.is_default = 1 )
						JOIN ( SELECT id, util_get_system_translated( \'name\', name, details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' )  AS name FROM gl_account_types OFFSET 0 ) AS glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
					ORDER BY
						ga.gl_account_type_id ASC,
						gat.formatted_account_number ASC';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						gat.gl_account_type_id,
						ga.gl_account_usage_type_id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
						util_get_system_translated( \'name\', glat.name, glat.details ) AS gl_account_type_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.is_default = 1
						AND ga.disabled_on IS NULL
					ORDER BY
						glat.name ASC,
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function has been moved to eos file kindly make changes there..

	public static function fetchActiveGlAccountsByCidByGlChartSystemCode( $intCid, $strGlChartSystemCode, $objClientDatabase, $boolAllowPostingToRestrictedGlAccounts = true, $boolJEAllowPostingToRestrictedGlAccounts = true, $boolIsFromBillbackListing = false ) {

		$strWhereCondition				= ( false == $boolAllowPostingToRestrictedGlAccounts ) ? ' AND ga.restrict_for_ap = false' : NULL;
		$strJeWhereCondition			= ( false == $boolJEAllowPostingToRestrictedGlAccounts && false == $boolIsFromBillbackListing ) ? ' AND ga.restrict_for_gl = false' : NULL;
		$strBillbackJeWhereCondition	= ( false == $boolIsFromBillbackListing ) ? ' AND ga.disabled_on IS NULL' : NULL;

		$strSql = 'SELECT
						ga.id,
						ga.ap_code_id,
						ga.gl_account_usage_type_id,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.formatted_account_number AS account_number,
						CONCAT( gat.formatted_account_number, \' : \', util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ) as gl_account_name,
						gat.gl_account_tree_id,
						gat.gl_group_id,
						gat.gl_tree_id,
						gat.gl_account_type_id,
						gat.gl_account_type_id as group_type_id,
						gat.disabled_by
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN gl_charts gc ON ( ga.cid = gc.cid AND ga.gl_chart_id = gc.id AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\' )
					WHERE
						gat.cid = ' . ( int ) $intCid .
						$strWhereCondition .
						$strJeWhereCondition .
						$strBillbackJeWhereCondition . '
					ORDER BY
						ga.gl_account_type_id,
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountByCidByGlAccountSystemCodeByGlChartSystemCode( $intCid, $strGlAccountSystemCode, $strGlChartSystemCode, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number as account_number
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.system_code = \'' . addslashes( $strGlAccountSystemCode ) . '\'
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
						AND ga.disabled_on IS NULL
						AND ( gc.is_disabled <> 1 OR gc.is_disabled IS NULL ) ';

		return self::fetchGlAccount( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByGlAccountGroupSystemCodesByGlChartSystemCode( $intCid, $arrstrGlGroupSystemCodes, $strGlChartSystemCode, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						glat.name AS gl_account_type_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
						JOIN gl_groups gg ON ( gat.gl_group_id = gg.id AND gat.cid = gg.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
						AND gg.system_code IN ( \'' . implode( '\', \'', $arrstrGlGroupSystemCodes ) . '\' )
						AND ga.disabled_on IS NULL
					ORDER BY
						ga.gl_account_type_id,
						gat.id,
						gat.formatted_account_number,
						ga.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByGlAccountTypeIdsByCid( $arrintGlAccountTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						gat.gl_account_type_id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						util_get_system_translated( \'name\', glat.name, glat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_type_name,
						gg.parent_gl_group_id
					FROM
						gl_account_trees gat
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_groups gg ON ( gat.gl_group_id = gg.id AND gat.cid = gg.cid )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
						AND gat.disabled_by IS NULL
						AND gat.is_default = 1
					ORDER BY
						glat.id,
						gat.formatted_account_number,
						gat.name';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchCustomActiveGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.gl_account_id as id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name
					FROM
						gl_account_trees gat
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_groups gg ON ( gat.gl_group_id = gg.id AND gat.cid = gg.cid )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.is_default = 1
						AND gat.disabled_by IS NULL
					ORDER BY
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByGlAccountTypeIdsByGlChartSystemCode( $intCid, $arrintGlAccountTypeIds, $strGlChartSystemCode, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						glat.name AS gl_account_type_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
						AND ga.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
						AND ga.disabled_on IS NULL
					ORDER BY
						gat.formatted_account_number,
						gat.id,
						ga.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	// This function returns all GlAccounts objects associated with all children of supplied GL group id
	// ( This functions fetchs all children GL groups recurresively and then fetches all associated GL accounts )

	public static function fetchGlAccountsRecursivelyByCidByGlTreeIdByGlGroupId( $intCid, $intGlTreeId, $intGlGroupId, $objClientDatabase ) {
		$strSql = 'WITH RECURSIVE
						recursive_gl_groups( id, cid, parent_gl_group_id, name, level )
						AS (
								SELECT
									gg1.id,
									gg1.cid,
									gg1.parent_gl_group_id,
									gg1.name,
									1 AS level
								FROM
									gl_groups gg1
								WHERE
									gg1.id = ' . ( int ) $intGlGroupId . '
									AND gg1.cid = ' . ( int ) $intCid . '
									AND gg1.gl_tree_id = ' . ( int ) $intGlTreeId . '

								UNION ALL

								SELECT
									gg2.id,
									gg2.cid,
									gg2.parent_gl_group_id,
									gg2.name,
									level + 1
								FROM
									gl_groups gg2,
									recursive_gl_groups rgg
								WHERE
									rgg.id = gg2.parent_gl_group_id
									AND rgg.cid = gg2.cid
									AND gg2.cid = ' . ( int ) $intCid . ' )

					SELECT
						ga.*
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
					WHERE
						gat.gl_group_id IN ( SELECT id FROM recursive_gl_groups )
						AND gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
					ORDER BY
						LOWER( gat.name )';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function getOrFetchSubmittedGlAccounts( $intGlTreeId, $intCid, $objDatabase, $arrintSubmittedGlAccountIds = NULL, $strGlAccountRangeStart = NULL, $strGlAccountRangeEnd = NULL ) {

		// when no gl accounts are selected but start & end range are selected, fetch those
		if( false == valArr( $arrintSubmittedGlAccountIds ) && false == is_null( $strGlAccountRangeStart ) && false == is_null( $strGlAccountRangeEnd ) ) {
			$arrintSubmittedGlAccountIds = CGlAccounts::fetchSimpleGlAccountIdsByAccountNumberRangeByCid( $strGlAccountRangeStart, $strGlAccountRangeEnd, $intCid, $objDatabase, $intGlTreeId );
		}

		return $arrintSubmittedGlAccountIds;
	}

	public static function fetchSimpleGlAccountIdsByAccountNumberRangeByCid( $strStartingAccountNumber, $strEndingAccountNumber, $intCid, $objClientDatabase, $intGlTreeId = NULL ) {

		if( false != is_null( $intGlTreeId ) ) {
			$strGlTreeCondition = 'AND gt.is_system = 1';
		} else {
			$strGlTreeCondition = 'AND gt.id = ' . ( int ) $intGlTreeId;
		}

		// We need to take min/max because, on Reporting Masks we can have multiple gl_account_tree records which have the same grouping_gl_account_id & formatted_account_number.
		$strSql = '
			WITH cte_gl_account_range AS (
				SELECT
					gat.grouping_gl_account_id,
					CASE WHEN gat.formatted_account_number = \'' . $strStartingAccountNumber . '\' THEN 1 END AS is_start,
					CASE WHEN gat.formatted_account_number = \'' . $strEndingAccountNumber . '\' THEN 1 END AS is_end,
					row_number() OVER ( ORDER BY gat.formatted_account_number, gat.gl_account_id ) AS row_num
				FROM
					gl_account_trees gat
					JOIN gl_trees gt ON gat.gl_tree_id = gt.id AND gat.cid = gt.cid ' . $strGlTreeCondition . '
				WHERE
					gat.cid = ' . ( int ) $intCid . '
				ORDER BY
					gat.formatted_account_number
			)

			SELECT
				grouping_gl_account_id
			FROM
				cte_gl_account_range
			WHERE
				row_num BETWEEN ( SELECT MIN( row_num ) FROM cte_gl_account_range WHERE is_start = 1 )
				AND ( SELECT MAX( row_num ) FROM cte_gl_account_range WHERE is_end = 1 )
			GROUP BY
				grouping_gl_account_id;';

		$arrintGlAccountIdsData = fetchData( $strSql, $objClientDatabase );

		return array_keys( rekeyArray( 'grouping_gl_account_id', $arrintGlAccountIdsData ) );
	}

	// This function has been moved to eos file kindly make changes there.

	public static function fetchAssociatedGlAccountsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase, $arrintGlAccountTypeIds = NULL, $objBudgetsFilter = NULL, $boolAllowPostingToRestrictedGlAccounts = false, $boolJEAllowPostingToRestrictedGlAccount = false, $boolIsSettingsTemplateProperty = false, $boolIsFromBillbackListing = false ) {

		if( true == $boolIsSettingsTemplateProperty ) {
			$strPropertyGroupIdsCondition = '';
		} else if( true == valArr( $arrintPropertyGroupIds ) ) {
			$strPropertyGroupIdsCondition = ' OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )';
		} else {
			return NULL;
		}

		$strLimitCondition			= NULL;
		$strWhereCondition			= ( false == $boolAllowPostingToRestrictedGlAccounts ) ? ' AND ga.restrict_for_ap = false' : NULL;
		$strJeWhereCondition		= ( false == $boolJEAllowPostingToRestrictedGlAccount && false == $boolIsFromBillbackListing ) ? ' AND ga.restrict_for_gl = false' : NULL;
		$strGlAccountTypeCondition	= ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND glat.id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : NULL;
		$strBillbackWhereCondition	= ( false == $boolIsFromBillbackListing )? ' AND ga.disabled_on IS NULL ': NULL;

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) && true == valStr( $objBudgetsFilter->getMinAccountNumber() ) && true == valStr( $objBudgetsFilter->getMaxAccountNumber() ) ) {

			$strWhereCondition .= " AND ( NULLIF ( regexp_replace ( gat.account_number, E'\\\\D', '', 'g' ), '' )::NUMERIC BETWEEN " . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMinAccountNumber() ) ] ) . '::NUMERIC AND ' . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMaxAccountNumber() ) ] ) . '::NUMERIC ) ';
		}

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) && true == valId( $objBudgetsFilter->getPropertyId() ) ) {
			$strWhereCondition .= ' AND ( pga.property_id = ' . $objBudgetsFilter->getPropertyId() . ' OR pga.property_id IS NULL )';
		}

		$strSql = 'SELECT
						ga.id,
						ga.id AS gl_account_id,
						ga.gl_account_type_id,
						ga.gl_account_usage_type_id,
						( gat.formatted_account_number || \' : \' || util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ) AS gl_account_name,
						gat.formatted_account_number as account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gt.account_number_pattern,
						gt.account_number_delimiter,
						gap.property_group_id,
						pga.property_id,
						glat.name AS gl_account_type_name,
						ga.ap_code_id,
						ga.disabled_on,
						gat.disabled_by,
						p.id As unassociated_property_id
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON ( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						LEFT JOIN properties p ON ( pga.cid = p.cid AND p.id = pga.property_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gat.gl_tree_id = gt.id AND gt.is_system = 1 )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.cid = ' . ( int ) $intCid .
						$strGlAccountTypeCondition .
						$strWhereCondition . '
						' . $strJeWhereCondition .
						$strBillbackWhereCondition . '
						AND ( gap.gl_account_id IS NULL ' . $strPropertyGroupIdsCondition . ' )
					ORDER BY
						gat.account_number, gat.name ASC';

		if( true == valObj( $objBudgetsFilter, CBudgetsFilter::class ) && true == valId( $objBudgetsFilter->getPageSize() ) ) {
			$strLimitCondition = ' OFFSET ' . ( int ) $objBudgetsFilter->getPageOffset() . ' LIMIT ' . ( int ) $objBudgetsFilter->getPageSize();
		}

		$strSql .= $strLimitCondition;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedGlAccountsByPropertyGroupIdsByCids( $arrintPropertyGroupIds, $arrintCids, $objClientDatabase, $arrintGlAccountTypeIds = NULL ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strGlAccountTypeCondition = ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND glat.id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : '';

		$strSql = 'SELECT
						DISTINCT ga.*,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						gap.property_group_id,
						pga.property_id,
						gap.gl_account_id,
						glat.name AS gl_account_type_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1)
						LEFT JOIN gl_account_properties gap ON( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ' . $strGlAccountTypeCondition . '
						AND ga.disabled_on IS NULL' . '
						AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
					ORDER BY
						glat.name ASC,
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByGlChartSystemCode( $intCid, $strGlChartSystemCode, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.id,
						ga.ap_code_id,
						ga.gl_account_usage_type_id,
						ga.disabled_by,
						ga.disabled_on,
						ga.restrict_for_ap,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						gat.gl_account_tree_id,
						gat.gl_group_id,
						gat.gl_tree_id,
						gat.gl_account_type_id,
						glat.name AS gl_account_type_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN gl_trees gt ON ( gat.cid = gt.cid  AND gat.gl_tree_id = gt.id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( gt.gl_chart_id = gc.id AND gt.cid = gc.cid )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
					ORDER BY
						ga.gl_account_type_id,
						gat.formatted_account_number,
						gat.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountByCidBySystemCode( $intCid, $strSystemCode, $objClientDatabase ) {
		$strSql = 'SELECT * FROM gl_accounts WHERE cid = ' . ( int ) $intCid . ' AND system_code = \'' . $strSystemCode . '\'';
		return self::fetchGlAccount( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByGlAccountUsageTypeIdsByCid( $arrintGlAccountUsageTypeIds, $intCid, $objClientDatabase, $boolFetchGlAccountProperties = false ) {

		if( false == valArr( $arrintGlAccountUsageTypeIds ) ) return NULL;

		$strColumn = $strJoin = '';

		if( true == $boolFetchGlAccountProperties ) {
			$strColumn = ', pga.property_id';

			$strJoin	= '
						LEFT JOIN gl_account_properties gap ON ( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )';
		}

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						ga.gl_account_usage_type_id,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.formatted_account_number AS account_number,
						ac.id AS ap_code_id,
						util_get_system_translated( \'name\', glat.name, glat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_type_name,
						ga.restrict_for_ap
						' . $strColumn . '
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
						JOIN ap_codes ac ON ( ga.cid = ac.cid AND ga.id = ac.item_gl_account_id )
						' . $strJoin . '
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
						AND gat.is_default = 1
						AND ga.disabled_on IS NULL
						AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . '
						' . $strWhereCondition . '
					ORDER BY
						gat.formatted_account_number,
						gat.gl_group_id,
						ga.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase, !$boolFetchGlAccountProperties );
	}

	public static function fetchAssociatedGlAccountsByGlAccountUsageTypeIdsByPropertyGroupIdsByCid( $arrintGlAccountUsageTypeIds, $arrintPropertyGroupIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPropertyGroupIds ) || false == valIntArr( $arrintGlAccountUsageTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ga.*,
						gat.id AS gl_tree_account_id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						gap.property_group_id,
						pga.property_id,
						ac.id AS ap_code_id,
						glat.name AS gl_account_type_name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON ( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN ap_codes ac ON ( ga.cid = ac.cid AND ga.id = ac.item_gl_account_id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
						AND ga.disabled_on IS NULL' . '
						AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
						AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . '
					ORDER BY
						glat.name ASC,
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedGlAccountsByPropertyGroupIdsByGlAccountUsageTypeIdsByCid( $arrintPropertyGroupIds, $arrintGlAccountUsageTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintGlAccountUsageTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						gap.property_group_id,
						pga.property_id,
						gap.gl_account_id
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						LEFT JOIN gl_account_properties gap ON ( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_association ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
					ORDER BY
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllGlAccountsByGlAccountUsageTypeIdsByCid( $arrintGlAccountUsageTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountUsageTypeIds ) ) return NULL;

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details ,\'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.gl_tree_id,
						util_get_system_translated( \'name\', glat.name, glat.details ,\'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_type_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
					WHERE
	 					gat.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
						AND gat.is_default = 1
						AND gat.disabled_by IS NULL
					ORDER BY
						glat.name,
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchAllGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						glat.name AS gl_account_type_name,
						glat.id as gl_account_type_id
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
					ORDER BY
						glat.name ASC,
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.*,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
					WHERE
						gt.cid = ' . ( int ) $intCid . '
					ORDER BY
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchMappedGlAccountsByGlAccountTreeIdsNotByGlAccountIdByCid( $arrintGlAccountTreeIds, $intGlAccountId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat_parent.gl_account_id AS id,
						gat_parent.gl_tree_id,
						util_get_translated( \'name\', gat_parent.name, gat_parent.details ) AS account_name,
						gat_parent.formatted_account_number AS account_number,
						gat.gl_tree_id
					FROM
						gl_account_trees gat_parent
						JOIN gl_account_trees gat ON ( gat_parent.cid = gat.cid AND gat_parent.gl_account_id = gat.gl_account_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat_parent.gl_account_id <> ' . ( int ) $intGlAccountId . '
						AND ( gat.gl_account_tree_id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' ) OR gat.id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' ) )
						AND gat_parent.is_default = 1
						AND gat_parent.disabled_by IS NULL
					ORDER BY
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveAssociatedGlAccountsCountByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase, $objBudgetsFilter = NULL ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strWhereCondition = NULL;

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) ) {
			$strWhereCondition = ' AND glat.id = ' . ( int ) $objBudgetsFilter->getGlAccountTypeId();

			if( true == valStr( $objBudgetsFilter->getMinAccountNumber() ) && true == valStr( $objBudgetsFilter->getMaxAccountNumber() ) ) {
				$strWhereCondition .= " AND ( NULLIF ( regexp_replace ( gat.account_number, E'\\\\D', '', 'g' ), '' )::NUMERIC BETWEEN " . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMinAccountNumber() ) ] ) . '::NUMERIC AND ' . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMaxAccountNumber() ) ] ) . '::NUMERIC ) ';
			}
		}

		$strSql = 'SELECT
						COUNT( DISTINCT gat.id )
					FROM
						gl_account_trees gat
						LEFT JOIN gl_account_properties gap ON ( gat.cid = gap.cid AND gap.gl_account_id = gat.gl_account_id )
						LEFT JOIN property_group_associations pga ON ( gat.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.disabled_by IS NULL
						AND gat.is_default = 1
						AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
						' . $strWhereCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchRequiredGlAccountsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( gl.id ) gl.id,
						gl.cid
					FROM
						gl_accounts gl
						LEFT JOIN ar_codes ac ON ( ac.cid = gl.cid AND gl.id IN( ac.credit_gl_account_id, ac.ap_gl_account_id ) )
					WHERE
						gl.cid = ' . ( int ) $intCid . '
						AND ac.id IS NULL;';

		return self::fetchGlAccounts( $strSql, $objDatabase );
	}

	public static function fetchSimpleActiveGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.disabled_on IS NULL
					ORDER BY
						gat.formatted_account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleActiveGlAccountsByCidByGlChartSystemCode( $intCid, $strGlChartSystemCode, $objClientDatabase, $intLastSyncOn = NULL ) {
		$strWhereSql = '';
		if( true == \valId( $intLastSyncOn ) ) {
			$strWhereSql .= ' AND ( gt.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR ga.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' ) ';
		}

		$strSql = 'SELECT
						ga.id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						glat.name AS gl_account_type_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
						AND ga.disabled_on IS NULL
						' . $strWhereSql . '
					ORDER BY
						ga.gl_account_type_id,
						gat.formatted_account_number,
						ga.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchEligibleDebitGlAccountsByArCodeTypeIdByCid( $intArCodeTypeId, $intCid, $objClientDatabase, $intArCodeId = NULL, $arrintGlAccountIds = [] ) {

		if( false == valId( $intArCodeTypeId ) ) return NULL;

		$strJoins			= '';
		$strConditions		= ( true == valArr( $arrintGlAccountIds ) ) ? 'AND ga.id IN( ' . implode( ', ', $arrintGlAccountIds ) . ' ) ' : '';

		if( true == valId( $intArCodeId ) ) {

			$strConditions 	.= ' AND (
										( gaut.id = ' . CGlAccountUsageType::PRE_PAYMENTS . ' AND ( ac.ar_code_type_id = ' . CArCodeType::PAYMENT . ' OR ac.is_payment_in_kind = true ) )
										OR (
											NOT EXISTS ( SELECT 1 FROM ar_transactions at WHERE at.cid = ac.cid AND at.ar_code_id = ac.id LIMIT 1 )
											AND NOT EXISTS ( SELECT 1 FROM property_ar_codes pac WHERE pac.cid = ac.cid AND pac.ar_code_id = ac.id LIMIT 1 )
										)
										OR ( ac.is_payment_in_kind = false AND gaut.id <> ' . CGlAccountUsageType::PRE_PAYMENTS . ' )
								)';

			$strJoins 		= 'JOIN ar_codes ac ON( gat.cid = ac.cid AND ac.id = ' . ( int ) $intArCodeId . ' )';
		}

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						ga.gl_account_usage_type_id,
						ga.gl_account_type_id,
						ga.default_gl_account_id,
						gat.formatted_account_number as account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id AND ' . ( int ) $intArCodeTypeId . ' = ANY( gaut.debit_ar_code_types ) )
						' . $strJoins . '
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						' . $strConditions . '
						AND gat.is_default = 1
						AND gat.disabled_by IS NULL
					ORDER BY
						gat.gl_account_type_id,
						gat.formatted_account_number,
						gat.name';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchEligibleCreditGlAccountsByArCodeTypeIdByArOriginIdByArTriggerIdByCid( $intArCodeTypeId, $intArOriginId, $intArTriggerId, $intCid, $objClientDatabase, $arrintGlAccountIds = [] ) {

		if( false == valId( $intArCodeTypeId ) || false == valId( $intArOriginId ) ) return NULL;

		$strConditions = ( true == valArr( $arrintGlAccountIds ) ) ? ' AND ga.id IN( ' . implode( ', ', $arrintGlAccountIds ) . ' ) ' : '';

		$strSql = 'SELECT
						ga.id,
						ga.gl_account_usage_type_id,
						ga.gl_account_type_id,
						ga.default_gl_account_id,
						gat.formatted_account_number as account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id AND ' . ( int ) $intArCodeTypeId . ' = ANY( gaut.credit_ar_code_types ) AND ' . ( int ) $intArOriginId . ' = ANY( gaut.credit_ar_origins ) )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						' . $strConditions . '
						AND CASE
								WHEN gaut.allow_null_triggers = true
								THEN ( gaut.credit_ar_trigger_ids IS NULL OR ' . ( int ) $intArTriggerId . ' = ANY( gaut.credit_ar_trigger_ids ) )
								ELSE ' . ( int ) $intArTriggerId . ' = ANY( gaut.credit_ar_trigger_ids )
							END
						AND gat.disabled_by IS NULL
						AND gat.is_default = 1
					ORDER BY
						gat.gl_account_type_id,
						gat.formatted_account_number,
						gat.name';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedGlAccountsByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT *	FROM gl_accounts WHERE cid = ' . ( int ) $intCid . ' AND remote_primary_key IS NOT NULL;';
		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsDetailsByGlAccountUsageTypeIdsByCid( $arrintGlAccountUsageTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountUsageTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ga.id,
						ga.cid,
						ga.gl_account_usage_type_id,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						gat.gl_group_id,
						ga.system_code,
						ac.id AS ap_code_id,
						glat.name AS gl_account_type_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND ga.cid = gat.cid )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.gl_chart_id = gc.id AND ga.cid = gc.cid )
						JOIN ap_codes ac ON ( ga.cid = ac.cid AND ga.id = ac.item_gl_account_id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
						AND gat.is_default = 1
						AND ga.disabled_on IS NULL
					ORDER BY
						gat.formatted_account_number,
						gat.gl_group_id,
						ga.id';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByIdsByRestrictForApByCid( $arrintIds, $boolIsRestrictedForAp, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintIds ) ) return NULL;

		$strRestrictCondition = 'AND restrict_for_ap = false';

		if( true == $boolIsRestrictedForAp ) {
			$strRestrictCondition = 'AND restrict_for_ap = true';
		}

		$strSql = 'SELECT
						*
					FROM
						gl_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintIds ) . ' )
						' . $strRestrictCondition;

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountWithGlAccountNameByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.id,
						gat.name,
						gat.formatted_account_number
					FROM
						gl_accounts ga
						LEFT JOIN gl_account_trees gat ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						ga.id = ' . ( int ) $intId . '
						AND ga.disabled_on IS NULL
						AND gat.cid = ' . ( int ) $intCid . '
						AND gat.disabled_by IS NULL
					    AND gat.is_default = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByBySearchKeywords( $intCid, $arrmixRequestData, $objDatabase ) {

		$arrstrExplodedSearch 	= explode( ' ', $arrmixRequestData['q'] );
		$boolIsDistribution 	= ( true == array_key_exists( 'is_distribution', $arrmixRequestData['fast_lookup_filter'] ) ) ? $arrmixRequestData['fast_lookup_filter']['is_distribution']: NULL;
		$boolIsContribution 	= ( true == array_key_exists( 'is_contribution', $arrmixRequestData['fast_lookup_filter'] ) ) ? $arrmixRequestData['fast_lookup_filter']['is_contribution'] : NULL;
		$intGlAccountUsageTypeId 	= ( true == array_key_exists( 'gl_account_usage_type_id', $arrmixRequestData['fast_lookup_filter'] ) ) ? $arrmixRequestData['fast_lookup_filter']['gl_account_usage_type_id']: NULL;

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );

		$strSql = 'SELECT
						DISTINCT ga.id,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id AND gat.is_default = 1 )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND	gat.disabled_by IS NULL';

		$arrstrAdvancedSearchParameters = [];

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( false == empty( $strKeywordAdvancedSearch ) ) {
				array_push( $arrstrAdvancedSearchParameters, 'gat.formatted_account_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				array_push( $arrstrAdvancedSearchParameters, 'gat.name	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		if( true == isset( $boolIsDistribution ) && true == $boolIsDistribution ) {
			$strSql .= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::OWNER_DRAWS;
		}

		if( true == isset( $boolIsContribution ) && true == $boolIsContribution ) {
			$strSql .= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::OWNER_CONTRIBUTIONS;
		}

		if( true == valId( $intGlAccountUsageTypeId ) ) {
			$strSql .= ' AND ga.gl_account_usage_type_id = ' . ( int ) $intGlAccountUsageTypeId;
		}

		$strSql .= ' ORDER BY gat.formatted_account_number, ga.id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlAccountsBySearchKeywordByCid( $arrmixRequestData, $intCid, $boolIsModulePermission, $objDatabase ) {

		$strSortOrder				= '';
		$strCondition				= '';
		$strWhereCondition			= '';
		$strSortOrderColumns		= '';
		$strSearchedString			= addslashes( trim( $arrmixRequestData['q'] ) );

		if( false == $boolIsModulePermission ) {
			$strWhereCondition = ' AND ga.restrict_for_ap IS FALSE';
		}

		if( true == is_numeric( $arrmixRequestData['fast_lookup_filter']['property_id'] ) ) {
			$strCondition = 'gap.property_group_id = ' . $arrmixRequestData['fast_lookup_filter']['property_id'];
		}

		if( true == valStr( $strSearchedString ) ) {

			$strSortOrderColumns	= ',CASE
											WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
												OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
											)
											THEN 1
											WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
											THEN 2
											ELSE 3
										END AS sort_order';
			$strSortOrder 			= ' sort_order,';

			$strWhereCondition		.= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';
		}

		if( false == valStr( $strCondition ) ) {
			return false;
		}

		$strSql = 'SELECT
						ga.id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						glat.name AS gl_account_type_name
						' . $strSortOrderColumns . '
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.cid = gc.cid AND ga.gl_chart_id = gc.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ga.disabled_on IS NULL' . '
						AND gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
						AND ( gap.gl_account_id IS NULL OR ' . $strCondition . ' )';

		$strSql .= ' ORDER BY ' . $strSortOrder . '
						gat.formatted_account_number,
						ga.gl_account_type_id,
						gat.gl_group_id,
						ga.id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlAccountsBySearchParametersByCid( $arrmixRequestData, $intCid, $objDatabase ) {

		$strCondition       = NULL;
		$strSearchedString  = $arrmixRequestData['q'];
		$strLookUpType		= $arrmixRequestData['fast_lookup_filter']['lookup_type'];
		$arrstrFilteredExplodedSearch	= [];
		$arrstrAdvancedSearchParameters	= [];

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $strSearchedString );

		if( CGlAccount::LOOKUP_TYPE_GL_ACCOUNTS_FOR_IC == $strLookUpType ) {
			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( false == empty( $strKeywordAdvancedSearch ) ) {
					array_push( $arrstrAdvancedSearchParameters, 'gat.formatted_account_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'gat.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strCondition .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql = 'SELECT
						ga.id,
						ga.gl_account_usage_type_id,
						util_get_translated( \'name\', gat.name, gat.details ,\'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::PENDING_REIMBURSEMENTS . '
						AND gat.is_default = 1
						AND ga.disabled_on IS NULL
					' . $strCondition . '
					ORDER BY
						gat.formatted_account_number,
						ga.id';

		} else {
			$intAssetTypeId					= ( int ) $arrmixRequestData['fast_lookup_filter']['asset_type_id'];
			$strAccountType					= $arrmixRequestData['fast_lookup_filter']['account_type'];
			$strSortOrder					= '';
			$strGlAccountType					= '';
			$strSortedColumn				= '';
			$strWhereCondition				= '';
			$strGlAccountUsageTypeJoinCondition	= '';
			$strGlAccountUsageTypeWhereCondition	= '';

			if( valId( $intAssetTypeId ) && valStr( $strAccountType ) ) {
				switch( $strAccountType ) {
					case 'consumption':
						if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

							$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::DEPRECIATION;
							$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types glat ON ( ga.gl_account_usage_type_id = glat.id )';
						}

						if( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $intAssetTypeId ) {
							$strGlAccountType					= ' AND glat1.id = ' . CGlAccountType::EXPENSES;
						}
						break;

					case 'debit':
						if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

							$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::FIXED_ASSET;
							$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types glat ON ( ga.gl_account_usage_type_id = glat.id )';
						} elseif( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $intAssetTypeId ) {

							$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::INVENTORY;
							$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types glat ON ( ga.gl_account_usage_type_id = glat.id )';

						} elseif( CAssetType::SUPPLIES_NOT_TRACKED == $intAssetTypeId || CAssetType::SERVICES_NOT_TRACKED == $intAssetTypeId ) {
							$strGlAccountType					= ' AND glat1.id != ' . CGlAccountType::INCOME;
							$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id != ' . CGlAccountUsageType::BANK_ACCOUNT;
							$strGlAccountUsageTypeWhereCondition	.= ' AND ga.system_code IS NULL';

						} elseif( CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED == $intAssetTypeId || CAssetType::INVENTORY_EXPENSED_AND_TRACKED == $intAssetTypeId ) {
							$strGlAccountType					= ' AND glat1.id = ' . CGlAccountType::EXPENSES;
						}
						break;

					case 'credit':
						if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

							$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::ACCUMULATED_DEPRECIATION;
							$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types glat ON ( ga.gl_account_usage_type_id = glat.id )';
						}
						break;

					default:
						// nothing to do
				}
			}

			if( true == valStr( $strSearchedString ) ) {
				$strSearchedString = addslashes( trim( $strSearchedString ) );
				$strSortedColumn	= ',CASE
											WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
												OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
											)
											THEN 1
											WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
											THEN 2
											ELSE 3
										END AS sort_order';
				$strSortOrder		= ' ORDER BY sort_order';

				$strWhereCondition	= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';

			}

			$strSql = 'SELECT
								DISTINCT ga.*,
								glat1.name AS gl_account_type_name,
								gat.formatted_account_number AS account_number,
								util_get_translated( \'name\', gat.name, gat.details ,\'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
								gat.gl_group_id,
								gat.gl_account_type_id
								' . $strSortedColumn . '
							FROM
								gl_accounts ga
								JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
								JOIN gl_trees glt ON (gat.cid = glt.cid AND gat.gl_tree_id = glt.id)
								JOIN gl_account_types glat1 ON ( gat.gl_account_type_id = glat1.id )
								' . $strGlAccountUsageTypeJoinCondition . '
								JOIN gl_charts gc ON (glt.gl_chart_id = gc.id AND glt.cid = gc.cid)
							WHERE
								gat.cid = ' . ( int ) $intCid . $strGlAccountType . '
								AND	gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
								' . $strGlAccountUsageTypeWhereCondition . $strWhereCondition . '
								AND	gat.disabled_by IS NULL
								AND gat.is_default=1' . $strSortOrder;
		}

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlAccountByExplodedSearchByFastLookupFilterByCid( $arrstrFilteredExplodedSearch, $arrmixFastLookupFilter, $intCid, $objDatabase ) {

		$strLookUpType		= $arrmixFastLookupFilter['lookup_type'];

		$arrstrAdvancedSearchParameters = [];

		if( CGlAccount::LOOKUP_TYPE_AP_PAYEES_GL_ACCOUNT == $strLookUpType ) {
			$boolIsDistribution 	= $arrmixFastLookupFilter['is_distribution'];
			$boolIsContribution 	= $arrmixFastLookupFilter['is_contribution'];
			$intGlAccountUsageTypeId 	= $arrmixFastLookupFilter['gl_account_usage_type_id'];

			$strSql = 'SELECT
						DISTINCT ga.id,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id AND gat.is_default = 1 )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND	gat.disabled_by IS NULL';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( false == empty( $strKeywordAdvancedSearch ) ) {
					array_push( $arrstrAdvancedSearchParameters, 'gat.formatted_account_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'gat.name	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			if( true == isset( $boolIsDistribution ) && true == $boolIsDistribution ) {
				$strSql .= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::OWNER_DRAWS;
			}

			if( true == isset( $boolIsContribution ) && true == $boolIsContribution ) {
				$strSql .= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::OWNER_CONTRIBUTIONS;
			}

			if( true == valId( $intGlAccountUsageTypeId ) ) {
				$strSql .= ' AND ga.gl_account_usage_type_id = ' . ( int ) $intGlAccountUsageTypeId;
			}

			$strSql .= ' ORDER BY gat.formatted_account_number, ga.id';
		}

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlAccountLinkedToPropertyByPropertyGroupIdsBySearchedStringByCid( $strSearchedString, $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsAllowPostingToRestrictedGlAccounts = false ) {

		$strSql						= '';
		$strSortOrder				= '';
		$strCondition				= '';
		$strWhereCondition			= '';
		$strSortOrderColumns		= '';

		if( false == $boolIsAllowPostingToRestrictedGlAccounts ) {
			$strWhereCondition = ' AND ga.restrict_for_ap IS FALSE';
		}

		if( true == valArr( $arrintPropertyGroupIds ) ) {
			$strCondition = 'gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )';
		}

		if( true == valStr( $strSearchedString ) ) {

			$strSortOrderColumns	= ',CASE
											WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
												OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
											)
											THEN 1
											WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
											THEN 2
											ELSE 3
										END AS sort_order';
			$strSortOrder 			= ' sort_order,';

			$strWhereCondition		.= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';
		}

		if( true == valStr( $strCondition ) ) {

			$strSql = 'SELECT
							ga.id,
							gat.formatted_account_number AS account_number,
							util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
							glat.name AS gl_account_type_name,
							pga.property_id
							' . $strSortOrderColumns . '
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						LEFT JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.cid = gc.cid AND ga.gl_chart_id = gc.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ga.disabled_on IS NULL' . '
						AND gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
						AND ( gap.gl_account_id IS NULL OR ' . $strCondition . ' )
						AND p.disabled_on IS NULL';
			$strSql .= ' ORDER BY ' . $strSortOrder . '
						gat.formatted_account_number,
						ga.gl_account_type_id,
						gat.gl_group_id,
						ga.id';

			$arrmixResultSets = ( array ) fetchData( $strSql, $objDatabase );

			return rekeyArray( 'id', $arrmixResultSets );

		}

		return [];
	}

	public static function fetchGlAccountsForJournalEntriesByPropertyGroupIdsBySearchedStringByCid( $strSearchedString, $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsAllowPostingToRestrictedGlAccounts = false ) {

		$strSortOrder						= '';
		$strWhereCondition					= '';
		$strSortOrderColumn					= '';
		$strSearchedString					= addslashes( trim( $strSearchedString ) );

		if( false == valIntArr( $arrintPropertyGroupIds ) ) return false;

		if( false == $boolIsAllowPostingToRestrictedGlAccounts ) {
			$strWhereCondition = ' AND ga.restrict_for_gl IS FALSE';
		}

		if( true == valIntArr( $arrintPropertyGroupIds ) ) {
			$strCondition = 'gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )';
		}

		if( true == valStr( $strSearchedString ) ) {

			$strSortOrderColumn = ',CASE
										WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
												OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
											)
										THEN 1
										WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
										THEN 2
											ELSE 3
									END AS sort_order';
			$strWhereCondition 	= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';
			$strSortOrder		= ' sort_order,';
		}

		if( true == valStr( $strCondition ) ) {

			$strSql	= 'SELECT
							DISTINCT ga.id,
							gat.formatted_account_number AS account_number,
							gat.name AS account_name,
							gat.gl_group_id,
							pga.property_id,
							glat.name AS gl_account_type_name'
					. $strSortOrderColumn . '
						FROM
 							gl_accounts ga
 							JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1)
 							LEFT JOIN gl_account_properties gap ON ( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
							LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
 							JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
 							JOIN gl_charts gc ON ( ga.cid = gc.cid AND ga.gl_chart_id = gc.id )
						WHERE
							ga.cid = ' . ( int ) $intCid . '
							AND ga.disabled_on IS NULL ' .
									$strWhereCondition . '
							AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
							AND gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
						ORDER BY
								' . $strSortOrder . '
							 gat.formatted_account_number, gat.name';

			return ( array ) fetchData( $strSql, $objDatabase );
		}

		return [];
	}

	public static function fetchGlAccountsByCidByPropertyIdByApPayeeIdBySearchKeywords( $intCid, $arrmixRequestData, $arrintPropertyIds, $objDatabase ) {

		$strIsPrimaryLocationRequired = '';

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );

		$intApPayeeId = NULL;
		$strSql		  = '';
		if( false == valArr( $arrintPropertyIds ) ) {
			return;
		}

		if( false == is_null( $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] ) ) {
			$intApPayeeId = $arrmixRequestData['fast_lookup_filter']['ap_payee_id'];
		}

		if( false == is_numeric( $intApPayeeId ) ) {
			return;
		}

		if( 0 == ( int ) $arrmixRequestData['fast_lookup_filter']['is_primary_location_required'] ) {
			$strIsPrimaryLocationRequired = ' AND apl.is_primary <> 1';
		}

		$strSql = 'SELECT
						apl.id,
						( CASE
							WHEN apl.vendor_code IS NOT NULL
							THEN apl.vendor_code || \' - \' || apl.location_name
							ELSE apl.location_name
						END ) AS location_name
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payee_property_groups appg ON ( apl.id = appg.ap_payee_location_id AND apl.cid = appg.cid )
						LEFT JOIN property_groups pg ON ( pg.cid = apl.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
					WHERE
						apl.cid = ' . ( int ) $intCid . $strIsPrimaryLocationRequired . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND disabled_by IS NULL ';

		$arrstrAdvancedSearchParameters = [];

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( false == empty( $strKeywordAdvancedSearch ) ) {
				array_push( $arrstrAdvancedSearchParameters, 'apl.location_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				array_push( $arrstrAdvancedSearchParameters, 'apl.vendor_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql .= ' GROUP BY
						apl.id,
						apl.location_name,
						apl.is_primary,
						apl.vendor_code
					ORDER BY
						apl.is_primary DESC,
						LOWER(apl.location_name) ASC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchGlAccountsByCidByPropertyGroupIdsBySearchKeywords( $intCid, $arrmixRequestData, $boolCheckModulePermission, $objDatabase ) {

		$strSql						= '';
		$strSortOrder				= '';
		$strCondition				= '';
		$strWhereCondition			= '';
		$strSortOrderColumns		= '';
		$strSearchedString			= addslashes( trim( $arrmixRequestData['q'] ) );
		$arrmixPropertyGroupIds		= [];

		if( false == $boolCheckModulePermission ) {
			$strWhereCondition = ' AND ga.restrict_for_ap IS FALSE';
		}

		if( true == is_numeric( $arrmixRequestData['fast_lookup_filter']['property_id'] ) ) {
			$strCondition           = 'gap.property_group_id IN ( NULL ) ';
			$arrmixPropertyGroupIds	= rekeyArray( 'property_group_id', ( array ) \Psi\Eos\Entrata\CPropertyGroupAssociations::createService()->fetchPropertyGroupAssociationsByCidByPropertyIds( $intCid, [ $arrmixRequestData['fast_lookup_filter']['property_id'] ], $objDatabase ) );

			if( true == valArr( $arrmixPropertyGroupIds ) ) {
				$strCondition = 'gap.property_group_id IN ( ' . implode( ',', array_keys( $arrmixPropertyGroupIds ) ) . ' ) ';
			}
		}

		if( true == valStr( $strSearchedString ) ) {

			$strSortOrderColumns	= ',CASE
											WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
												OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
												OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
											)
											THEN 1
											WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
											THEN 2
											ELSE 3
										END AS sort_order';
			$strSortOrder 			= ' sort_order,';

			$strWhereCondition		.= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';
		}

		if( true == valStr( $strCondition ) ) {

			$strSql = 'SELECT
						ga.id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						glat.name AS gl_account_type_name
						' . $strSortOrderColumns . '
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON( ga.cid = gap.cid AND gap.gl_account_id = ga.id )
						LEFT JOIN property_group_associations pga ON ( ga.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( ga.cid = gc.cid AND ga.gl_chart_id = gc.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ga.disabled_on IS NULL' . '
						AND gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
						AND ( gap.gl_account_id IS NULL OR ' . $strCondition . ' )';

			$strSql .= ' ORDER BY ' . $strSortOrder . '
						gat.formatted_account_number,
						ga.gl_account_type_id,
						gat.gl_group_id,
						ga.id';
		}
		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlAccountsWithGlAccountTypeIdsByCid( $arrintGlAccountTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ga.id,
						func_format_gl_account_number ( gat.account_number, gt.account_number_pattern, gt.account_number_delimiter ) || \': \' || gat.name AS name
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND ga.cid = gt.cid AND gat.gl_tree_id = gt.id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
						AND gat.disabled_by IS NULL
						AND gat.is_default = 1
					ORDER BY
						gat.formatted_account_number,
						gat.name,
						ga.gl_account_type_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByRemotePrimaryKeysByCid( $arrstrRemotePrimaryKeys, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrRemotePrimaryKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						gl_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND remote_primary_key IN ( \'' . implode( '\',\'', $arrstrRemotePrimaryKeys ) . '\' ) ';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchActiveCommercialExpenseTypeGlAccountsByCid( $intPropertyId, $intCid, $objClientDatabase, $arrstrAccountName, $arrintGlAccountIds ) {

		if( false === valId( $intPropertyId ) || false === valId( $intCid ) || false === valObj( $objClientDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == valArr( $arrstrAccountName ) ) {
			$strWhere = ' AND lower( gat.name ) IN ( ' . implode( ',', $arrstrAccountName ) . ' ) ';
		}

		$strWhereCondition = 'AND ga.id NOT IN (
											SELECT 
												pcb.gl_account_id
											FROM 
												property_cam_budgets pcb
												JOIN property_cam_periods pcp ON ( pcb.cid = pcp.cid AND pcb.property_id = pcp.property_id AND pcb.property_cam_period_id = pcp.id )
												JOIN cam_periods cp ON (pcp.cid = cp.cid AND pcp.cam_period_id = cp.id)
											WHERE 
												pcb.cid = ' . ( int ) $intCid . ' 
												AND pcb.property_id = ' . ( int ) $intPropertyId . ' 
												AND date_part( \'year\', cp.start_date ) = date_part( \'year\', CURRENT_DATE )
												AND pcb.gl_account_id IS NOT NULL  
												AND pcb.deleted_by IS NULL
										)';

		if( true === valArr( $arrintGlAccountIds ) ) {
			$strWhereCondition = 'AND ga.id IN (' . sqlIntImplode( $arrintGlAccountIds ) . ')';
		}

		$strSql = 'WITH RECURSIVE
						opexpense
						AS (
							SELECT 
								gg1.*
							FROM gl_groups gg1
							JOIN gl_trees gt ON (gt.id = gg1.gl_tree_id  AND gt.cid = gg1.cid )
							WHERE 
								gg1.system_code = \'' . CGlGroup::OPEXPENSE_SYSTEM_CODE . '\' AND
								gg1.cid = ' . ( int ) $intCid . ' AND
								gt.is_system = 1 AND
								gt.system_code = \'DEFAULT\'
							UNION ALL
							SELECT 
								gg2.*
							FROM gl_groups gg2,
								opexpense opexp
							WHERE opexp.id = gg2.parent_gl_group_id AND
								opexp.cid = gg2.cid AND
								gg2.cid = ' . ( int ) $intCid . ' )
						SELECT 
							DISTINCT ga.id,
							util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
							ga.gl_account_usage_type_id,
							gat.account_number,
							gg.id as parent_group_id
						FROM 
							gl_accounts ga
							JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.disabled_by IS NULL AND gat.is_default = 1 ' . $strWhere . ')
							JOIN opexpense gg ON ( gat.cid = gg.cid AND gat.gl_tree_id = gg.gl_tree_id AND gat.gl_group_id = gg.id )
						WHERE 
						ga.cid = ' . ( int ) $intCid . ' 
						AND ga.gl_account_type_id = ' . CGlAccountType::EXPENSES . ' 
						AND ga.gl_account_usage_type_id NOT IN ( ' . sqlIntImplode( CGlAccountUsageType::$c_arrintExcludedGlAccountUsageTypeIds ) . ' ) 
						' . $strWhereCondition . '
					ORDER BY 
						ga.gl_account_usage_type_id,
						account_name';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByGlAccountUsageTypeIdsByGlAccountTypeIdsByCid( $arrintGlAccountUsageTypeIds, $arrintGlAccountTypeIds, $intCid, $objClientDatabase, $boolExcludeExistsInCorporateArCodes = false, $intGlAccountId = NULL ) {

		if( false == valArr( $arrintGlAccountUsageTypeIds ) || false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strWhereSql = '';
		if( true === $boolExcludeExistsInCorporateArCodes ) {
			$strWhereSql .= ' AND NOT EXISTS ( SELECT id FROM ar_codes WHERE mode_type_id = ' . CModeType::CORPORATE . ' AND cid = ' . ( int ) $intCid . ' and credit_gl_account_id = gat.gl_account_id ) ';
		}
		if( true === valId( $intGlAccountId ) ) {
			$strWhereSql .= ' AND ga.id = ' . ( int ) $intGlAccountId;
		}

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						ga.gl_account_usage_type_id,
						gat.name AS account_name,
						gat.formatted_account_number AS account_number,
						ac.id AS ap_code_id,
						glat.name AS gl_account_type_name,
						gat.gl_account_type_id
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN ap_codes ac ON ( ga.cid = ac.cid AND ga.id = ac.item_gl_account_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						' . $strWhereSql . '
						AND ga.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
						AND ga.gl_account_usage_type_id IN ( ' . implode( ',', $arrintGlAccountUsageTypeIds ) . ' )
						AND gat.is_default = 1
						AND ga.disabled_on IS NULL
						AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . '
					ORDER BY
						glat.id,
						gl_account_type_name,
						gat.formatted_account_number';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsByCidByGlAccountTypeIds( $intCid, $arrintGlAccountTypeIds, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ga.*,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.formatted_account_number AS account_number,
						util_get_system_translated( \'name\', glat.name, glat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_type_name
					FROM
						gl_trees gt
						JOIN gl_account_trees gat ON ( gat.gl_tree_id = gt.id AND gt.cid = gat.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_accounts ga ON ( ga.id = gat.gl_account_id AND gat.cid = ga.cid )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
						AND ga.disabled_on IS NULL
					ORDER BY
						gat.formatted_account_number,
						gat.id,
						ga.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsDataByGlAccountTypeIdByCid( $intGlAccountTypeIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) && false == valIntArr( $intGlAccountTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.gl_account_id AS id,
						gat.gl_account_type_id,
						ga.gl_account_usage_type_id,
						gat.gl_group_id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						util_get_system_translated( \'name\', glat.name, glat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_type_name,
						util_get_translated( \'name\', gg.name, gg.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_group_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_groups gg ON ( gat.cid = gg.cid AND gat.gl_tree_id = gg.gl_tree_id AND gat.gl_group_id = gg.id ) 
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.is_default = 1
						AND glat.id IN  ( ' . sqlIntImplode( $intGlAccountTypeIds ) . ' )
						AND ga.disabled_on IS NULL
					ORDER BY
						glat.name ASC,
						gat.formatted_account_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchConsumptionGlAccountsByGlAccountTypeIdByCid( $intGlAccountTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ga.id,
						gat.name as gl_tree_name,
						gat.formatted_account_number AS account_number
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_trees glt ON (gat.cid = glt.cid AND gat.gl_tree_id = glt.id)
						JOIN gl_account_types gat1 ON ( gat.gl_account_type_id = gat1.id )
						JOIN gl_account_usage_types glat ON ( ga.gl_account_usage_type_id = glat.id )
						JOIN gl_charts gc ON (glt.gl_chart_id = gc.id AND glt.cid = gc.cid)
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat1.id = ' . ( int ) $intGlAccountTypeId . '
						AND	gc.system_code =  \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
						AND	gat.disabled_by IS NULL
						AND gat.is_default = 1';

		return self::fetchGlAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleActiveGlAccountsByFilterByCid( $strGlAccount, $arrintSelectedIds = [], $intCid, $objClientDatabase ) {
		if( false == valStr( $strGlAccount ) ) return;

		$strSqlGlAccount	= ' ( lower( gat.name ) ILIKE \'%' . strtolower( $strGlAccount ) . '%\'
								OR
								lower( gat.formatted_account_number ) ILIKE \'%' . strtolower( $strGlAccount ) . '%\' ) ';

		if( true == valIntArr( $arrintSelectedIds ) ) {
			$strSqlGlAccount = ' AND ( gat.gl_account_id IN ( ' . sqlIntImplode( $arrintSelectedIds ) . ' ) OR ' . $strSqlGlAccount . ' ) ';
		} else {
			$strSqlGlAccount = ' AND ( ' . $strSqlGlAccount . ' ) ';
		}

		$strSql = 'SELECT
						gat.formatted_account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						gat.gl_account_id as id
					FROM
						gl_account_trees gat
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.disabled_by IS NULL
						AND gat.is_default = 1
						' . $strSqlGlAccount . '
					ORDER BY
						gat.account_number, account_name
					LIMIT 50';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAdvancedBudgetGlAccountsDetailsByBudgetIdByCid( $intBudgetId, $intCid, $objClientDatabase ) {

		if( false == valId( $intBudgetId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT sub.gl_account_type_id,
						sum(sub.total_income) AS total_income,
						sum(sub.total_expense) AS total_expense,
						sum(sub.rental_income) AS rental_income, 
						sub.post_month
					FROM (
						SELECT ga.gl_account_type_id,
								CASE
									WHEN ga.gl_account_type_id = ' . CGlAccountType::INCOME . ' THEN sum( btgam.amount )
									ELSE 0
								END as total_income,
								CASE
									WHEN ga.gl_account_type_id = ' . CGlAccountType::EXPENSES . ' THEN sum(btgam.amount)
									ELSE 0
								END as total_expense,
								CASE
									WHEN ga.gl_account_type_id = ' . CGlAccountType::INCOME . ' AND ga.gl_account_usage_type_id in ( ' . CGlAccountUsageType::RENT . ',' . CGlAccountUsageType::SPECIALS_CONSESSIONS . ',' . CGlAccountUsageType::BAD_DBEBT . ',' . CGlAccountUsageType::DELINQUENT_RENT . ',' . CGlAccountUsageType::PREPAID_INCOME . ',' . CGlAccountUsageType::GAIN_LOSS_ON_LEAVE . ',' . CGlAccountUsageType::VACANCY_LOSS . ' ) THEN sum( btgam.amount )
									ELSE 0
								END AS rental_income, 
								btgam.post_month
							FROM budget_tab_gl_account_months btgam
								JOIN budget_tab_gl_accounts btga ON ( btgam.cid = btga.cid AND btgam.budget_tab_gl_account_id = btga.id )
								JOIN gl_account_trees gat ON ( btgam.cid = gat.cid AND btga.gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
								JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
							WHERE btgam.cid = ' . ( int ) $intCid . ' 
								AND btgam.budget_id =  ' . ( int ) $intBudgetId . '  
								AND ga.gl_account_type_id in ( ' . CGlAccountType::INCOME . ',' . CGlAccountType::EXPENSES . ' )
							GROUP BY ga.gl_account_type_id,
								ga.gl_account_usage_type_id, 
								btgam.post_month
						) AS sub
					GROUP BY 
						sub.gl_account_type_id, 
						sub.post_month';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>