<?php

class CSubsidyRequest extends CBaseSubsidyRequest {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRequestResponseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestEndpoint() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestPort() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestServiceName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFailed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>