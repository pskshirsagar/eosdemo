<?php

class CPropertyTransfer extends CBasePropertyTransfer {

	/**
	 * Create Functions
	 */

	public function createPropertyTransferProperties() {

		$objPropertyTransferProperty = new CPropertyTransferProperty();
		$objPropertyTransferProperty->setCid( $this->getCid() );
		$objPropertyTransferProperty->setPropertyTransferId( $this->getId() );

		return $objPropertyTransferProperty;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMigrationTokenId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyTransferStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransferName() {
		$boolIsValid = true;

		if( false == valStr( $this->getTransferName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_name', 'Transfer name is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStartOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_on', 'Scheduled on is required.' ) );
		} elseif( strtotime( date( 'm/d/Y', time() ) ) > strtotime( $this->getStartOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_on', 'Scheduled on date is not allowed from past.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valEmail() {
		$boolIsValid = true;

		if( false == valStr( CValidation::validateEmailAddress( $this->getEmail() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Email should be comma (,) separated valid email address(es).' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmail();
				$boolIsValid &= $this->valStartOn();
				$boolIsValid &= $this->valTransferName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>