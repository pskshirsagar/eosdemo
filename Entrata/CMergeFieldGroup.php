<?php

class CMergeFieldGroup extends CBaseMergeFieldGroup {

	const PROPERTY 						= 1;
    const LEASE_AND_UNIT 				= 2;
    const AMENITIES 					= 3;
    const CHARGES_AND_FEES 				= 4;
    const UTILITIES 					= 5;
    const MAINTENANCE 					= 6;
    const APPLICANTS_AND_OCCUPANTS 		= 7;
    const PETS 							= 8;
    const POLICIES 						= 9;
    const OTHERS 						= 10;
    const OPTIONS 						= 11;

    const LEASE_TERMS					= 12;
    const SPECIAL_PROVISIONS			= 15;
    const ATTACHMENTS					= 16;
    const LEAD_HAZARD					= 18;
    const BUY_OUT_AGREEMENT				= 20;
    const MISC_ADDENDA					= 21;
    const UTILITY_ADDENDUM				= 22;
    const RENT_CONCESSIONS				= 24;
    const RENTERS_INSUARANCE            = 25;
    const SECURITY_DEPOSIT				= 30;
	const SPECIAL_TERMS_PET_INFO		= 31;
    const SERVICE_BILLING				= 41;
    const SMOKE_FREE_ADDEN				= 48;
    const ELECTRIC_WATER_WASTWATER		= 50;
    const PARKING						= 47;
	const WASHER_DRYER					= 52;
    const SIGNATURE_AND_INITIALS		= 55;
	const SECURITY_DEPOSITE_MISC		= 35;
	const UTILITIES_INSURANCE_DISCOUNTS = 36;
	const RESIDENTS_OCCUPANTS			= 32;
	const STUDENT_HOUSING				= 44;
	const REPAYMENTS                    = 62;
	const WORK_ORDER					= 66;
	const SOCIAL_MEDIA					= 67;
	const TENANT_TECH					= 71;

	const EMPLOYEE						= 95;
	const VENDOR						= 96;
	const NEW_FROM_BLUEMOON             = 94;

	const AFFORDABLE                    = 100;
	const MILITARY                      = 110;

	const COMMERCIAL                    = 109;

	const GROUPS                        = 115;
	const AR_PAYMENT                    = 117;
	const RESIDENT_PORTAL               = 118;
	const CUSTOMER_CONTACTS             = 119;
	const LEASING_CENTER				= 120;
	const WECHAT                        = 129;

    public static $c_arrintLeaseExecutionMergeFieldGroups = [
        self::PROPERTY,
        self::LEASE_AND_UNIT,
        self::AMENITIES,
        self::CHARGES_AND_FEES,
        self::UTILITIES,
        self::MAINTENANCE,
        self::APPLICANTS_AND_OCCUPANTS,
        self::PETS,
        self::POLICIES,
        self::OTHERS,
        self::OPTIONS,
        self::REPAYMENTS
    ];

    protected $m_intInvalidDocumentMergeFieldsCount;

	protected $m_arrobjDocumentMergeFields;
	protected $m_arrobjMergeFieldSubGroups;
	protected $m_arrobjBlockDocumentMergeFields;
	protected $m_arrstrMappedBlockMergeFields;
	protected $m_arrstrCustomBlockMergeFields;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjDocumentMergeFields 		= [];
        $this->m_arrobjBlockDocumentMergeFields = [];
        $this->m_arrstrMappedBlockMergeFields 	= [];
        $this->m_arrstrCustomBlockMergeFields 	= [];

        return;
    }

    /**
     * Get Functions
     */
	public function getMergeFieldSubGroups() {
			return $this->m_arrobjMergeFieldSubGroups;
	}

	public function addMergeFieldSubGroup( $objMergeFieldSubGroup ) {
			$this->m_arrobjMergeFieldSubGroups[$objMergeFieldSubGroup->getOrderNum()] = $objMergeFieldSubGroup;
	}

    public function getDocumentMergeFields() {
    	return $this->m_arrobjDocumentMergeFields;
    }

	public function getBlockDocumentMergeFields() {
    	return $this->m_arrobjBlockDocumentMergeFields;
    }

    public function getInvalidDocumentMergeFieldsCount() {
    	return $this->m_intInvalidDocumentMergeFieldsCount;
    }

    /**
     * Set Functions
     */
	public function setMergeFieldSubGroups( $arrobjMergeFieldSubGroups ) {
		$this->m_arrobjMergeFieldSubGroups = $arrobjMergeFieldSubGroups;
	}

	public function setDocumentMergeFields( $arrobjDocumentMergeFields ) {
    	$this->m_arrobjDocumentMergeFields = $arrobjDocumentMergeFields;
    }

	public function setBlockDocumentMergeFields( $arrobjBlockDocumentMergeFields ) {
    	$this->m_arrobjBlockDocumentMergeFields = $arrobjBlockDocumentMergeFields;
    }

    public function getMappedBlockMergeFields() {
    	return $this->m_arrstrMappedBlockMergeFields;
    }

	public function getCustomBlockMergeFields() {
    	return $this->m_arrstrCustomBlockMergeFields;
    }

    public function setInvalidDocumentMergeFieldsCount( $intInvalidDocumentMergeFieldsCount ) {
    	$this->m_intInvalidDocumentMergeFieldsCount = $intInvalidDocumentMergeFieldsCount;
    }

    /**
     * Add Functions
     */

    public function addDocumentMergeField( $objDocumentMergeField ) {
    	$this->m_arrobjDocumentMergeFields[$objDocumentMergeField->getField()] = $objDocumentMergeField;
    }

	public function addBlockDocumentMergeField( $objBlockDocumentMergeField ) {
    	$this->m_arrobjBlockDocumentMergeFields[$objBlockDocumentMergeField->getField()] = $objBlockDocumentMergeField;
    }

    public function addMappedBlockMergeField( $strField, $arrstrMappedMergeFields ) {
    	$this->m_arrstrMappedBlockMergeFields[$strField] = $arrstrMappedMergeFields;
    }

	public function addCustomBlockMergeField( $strField, $arrstrCustomMergeFields ) {
    	$this->m_arrstrCustomBlockMergeFields[$strField] = $arrstrCustomMergeFields;
    }

    /**
     * Other Functions
     */

	public function validateDocumentMergeFields( $arrstrMappedMergeFields ) {

		$this->m_intDocumentMergeFieldErrorMsgsCount = 0;
		$boolIsValid = true;

		if( true == valArr( $this->getMergeFieldSubGroups() ) ) {
			foreach( $this->getMergeFieldSubGroups() as $objMergeFieldSubGroup ) {

				if( false == $objMergeFieldSubGroup->getDocumentMergeFields() && false == $objMergeFieldSubGroup->getBlockDocumentMergeFields() ) continue;

				if( true == valArr( $objMergeFieldSubGroup->getDocumentMergeFields() ) ) {
					$arrobjDocumentMergeFields = $objMergeFieldSubGroup->getDocumentMergeFields();

					foreach( $arrobjDocumentMergeFields as $objDocumentMergeField ) {
						if( false == CLeaseExecutionMergeFieldValidator::validate( $objDocumentMergeField, $arrstrMappedMergeFields ) ) {
							$this->m_intInvalidDocumentMergeFieldsCount += 1;
						}
					}
				}
			}
		}

		if( false == $this->getDocumentMergeFields() && false == $this->getBlockDocumentMergeFields() ) return true;
		if( true == valArr( $this->getDocumentMergeFields() ) ) {
			$arrobjDocumentMergeFields = $this->getDocumentMergeFields();

			foreach( $arrobjDocumentMergeFields as $objDocumentMergeField ) {
				if( false == CLeaseExecutionMergeFieldValidator::validate( $objDocumentMergeField, $arrstrMappedMergeFields ) ) {
					$this->m_intInvalidDocumentMergeFieldsCount += 1;
				}
			}
		}
	}

    /**
     * Validate Functions
     */

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Name', 'Name is required.' ) );
		}
		if( true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strSql = " WHERE name ILIKE '" . trim( addslashes( $this->getName() ) ) . "'" . $strSqlCondition;

			$intCount = CMergeFieldGroups::fetchMergeFieldGroupCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Name', 'Name already Exist.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valOrderNum( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getOrderNum() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Order_num', 'Order Number is required.' ) );
		}
		if( true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			if( true == is_null( $this->getTransmissionVendorId() ) ) {
				$strSql = ' WHERE  is_external = ' . $this->getIsExternal() . ' AND order_num = ' . $this->getOrderNum() . " AND ((details->>'transmission_vendor_id')::VARCHAR = '" . $this->getTransmissionVendorId() . "' OR details->>'transmission_vendor_id' IS NULL) " . $strSqlCondition;
			} else {
				$strSql = ' WHERE is_external = ' . $this->getIsExternal() . ' AND order_num = ' . $this->getOrderNum() . " AND (details->>'transmission_vendor_id')::VARCHAR = '" . $this->getTransmissionVendorId() . "'" . $strSqlCondition;
			}
			$intCount = CMergeFieldGroups::fetchMergeFieldGroupCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Order_num', 'Order Number already in use.' ) );
			}
		}
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valOrderNum( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valOrderNum( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			    $boolIsValid = true;
			    break;
        }

        return $boolIsValid;
    }

}
?>