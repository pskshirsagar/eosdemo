<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountItems
 * Do not add any new functions to this class.
 */

class CBudgetTabGlAccountItems extends CBaseBudgetTabGlAccountItems {

	public static function fetchBudgetTabGlAccountItemsByBudgetIdByBudgetTabIdByCid( $intBudgetId, $intBudgetTabId, $intCid, $objDatabase ) {

		if( false === valId( $intBudgetId ) || false === valId( $intBudgetTabId ) || false === valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						btgai.*
					FROM
						budget_tab_gl_account_items btgai
						JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgai.cid AND btga.id = btgai.budget_tab_gl_account_id AND btga.budget_id = btgai.budget_id )
					WHERE
						btgai.cid = ' . ( int ) $intCid . '
						AND btgai.budget_id = ' . ( int ) $intBudgetId . '
						AND btga.budget_tab_id = ' . ( int ) $intBudgetTabId;

		return self::fetchBudgetTabGlAccountItems( $strSql, $objDatabase );
	}

}
?>