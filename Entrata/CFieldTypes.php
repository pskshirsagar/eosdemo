<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldTypes
 * Do not add any new functions to this class.
 */

class CFieldTypes extends CBaseFieldTypes {

    public static function fetchFieldTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CFieldType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchFieldType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CFieldType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>