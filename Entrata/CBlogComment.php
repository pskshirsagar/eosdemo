<?php

class CBlogComment extends CBaseBlogComment {

	protected $m_strAuthorName;

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrstrValues['author_name'] ) ) {
			$this->setAuthorName( $arrstrValues['author_name'] );
		}
	}

	public function setName( $strAuthorName ) {
		parent::setName( utf8_encode( $strAuthorName ) );
	}

	public function setAuthorName( $strAuthorName ) {
		$this->m_strAuthorName = CStrings::strTrimDef( utf8_encode( $strAuthorName ), 256, NULL, true );
	}

	public function setBlogComment( $strBlogComment ) {
		parent::setBlogComment( utf8_encode( $strBlogComment ) );
	}

	public function getAuthorName() {
		return $this->m_strAuthorName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert_from_pp':
				$boolIsValid = true;

				if( true == is_null( $this->m_strName ) && false == valStr( $this->m_strName ) ) {
					if( 0 == strlen( $this->m_strName ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
						$boolIsValid = false;
					}
				} else if( $this->m_strName !== strip_tags( $this->m_strName ) ) {
					// Checking if name consist of any html tags
					// i18n localization will be handled later
					// Temporarily added error message, will change it later after discussion with SDM/TPM
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter valid name.' ) );
					$boolIsValid = false;
				}

				if( true == is_null( $this->m_strEmail ) && false == valStr( $this->m_strEmail ) ) {
					if( 0 == strlen( $this->m_strEmail ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Email is required.' ) );
						$boolIsValid = false;
					}
				} elseif( false == CValidation::validateEmailAddresses( $this->m_strEmail ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email is invalid.' ) );
					$boolIsValid = false;
				}

				if( true == is_null( $this->m_strBlogComment ) && false == valStr( $this->m_strBlogComment ) ) {
					if( 0 == strlen( $this->m_strBlogComment ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'blog_comment', 'Blog comment is required.' ) );
						$boolIsValid = false;
					}
				} else if( $this->m_strBlogComment !== strip_tags( $this->m_strBlogComment ) ) {
					// Checking if comment consist of any html tags
					// i18n localization will be handled later
					// Temporarily added error message, will change it later after discussion with SDM/TPM
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'blog_comment', 'Please enter valid comment.' ) );
					$boolIsValid = false;
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>