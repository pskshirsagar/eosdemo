<?php

class CSubsidyTaxCreditSetting extends CBaseSubsidyTaxCreditSetting {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentAssistanceArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// getter setter methods for fields on details field

	public function setIncludeAnticipatedMembers( $boolIncludeAnticipatedMembers ) {
		$this->setDetailsField( 'include_anticipated_members', $boolIncludeAnticipatedMembers );
	}

	public function getIncludeAnticipatedMembers() : bool {
		return ( bool ) $this->getDetailsField( 'include_anticipated_members' );
	}

	public function setIsVawaComplainceTracked( $boolIsVawaComplainceTracked ) {
		$this->setDetailsField( 'is_vawa_complaince_tracked', $boolIsVawaComplainceTracked );
	}

	public function getIsVawaComplainceTracked() : bool {
		return ( bool ) $this->getDetailsField( 'is_vawa_complaince_tracked' );
	}

}
?>