<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifiedFavorites
 * Do not add any new functions to this class.
 */

class CCustomerClassifiedFavorites extends CBaseCustomerClassifiedFavorites {

	public static function fetchCustomCustomerClassifiedFavoriteCount( $intCustomerClassifiedId, $intCustomerId, $intCid, $objEntrataDatabase ) {
		$strWhereClause = 'WHERE customer_classified_id = \'' . $intCustomerClassifiedId . ' \' AND cid = \'' . $intCid . ' \' AND customer_id = \'' . $intCustomerId . ' \' AND deleted_on IS NULL AND deleted_by IS NULL';
		return self::fetchCustomerClassifiedFavoriteCount( $strWhereClause, $objEntrataDatabase );
	}

	public static function fetchDeletedCustomerClassfiedFavoriteCount( $intCustomerClassifiedId, $intCustomerId, $intCid, $objEntrataDatabase ) {
		$strWhereClause = 'WHERE customer_classified_id = \'' . $intCustomerClassifiedId . ' \' AND cid = \'' . $intCid . ' \' AND customer_id = \'' . $intCustomerId . ' \' AND deleted_on IS NOT NULL AND deleted_by IS NOT NULL';
		return self::fetchCustomerClassifiedFavoriteCount( $strWhereClause, $objEntrataDatabase );
	}

}
?>