<?php

class CUnitOfMeasureConversion extends CBaseUnitOfMeasureConversion {

	protected $m_intApCodeId;

	/**
	 * Get Functions
	 */

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	/**
	 * Set Functions
	 */

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = $intApCodeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_code_id'] ) ) $this->setApCodeId( $arrmixValues['ap_code_id'] );
	}

	public function valDefaultUnitOfMeasureConversionId() {

		$boolIsValid = true;

		if( true == valId( $this->getDefaultUnitOfMeasureConversionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_unit_of_measure_conversion_id', __( 'Cannot delete default unit of measure conversion.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFromUnitOfMeasureId() {

		$boolIsValid = true;

		if( false == valId( $this->getFromUnitOfMeasureId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_unit_of_measure_id', __( 'Select Consumption unit of measure.' ) ) );
		}

		return $boolIsValid;
	}

	public function valToUnitOfMeasureId() {

		$boolIsValid = true;

		if( false == valId( $this->getToUnitOfMeasureId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_unit_of_measure_id', __( 'Select Purchasing unit of measure.' ) ) );
		}

		return $boolIsValid;
	}

	public function valConversionRate() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getConversionRate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conversion_rate', __( 'Enter conversion rate.' ) ) );
			return false;
		}

		if( true == is_numeric( $this->getConversionRate() ) && 0 >= $this->getConversionRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conversion_rate', __( 'Conversion rate should be greater than zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureConversion( $objClientDatabase, $arrobjUnitOfMeasures, $arrintUpdateUnitOfMeasureIds, $arrobjUnitOfMeasureConversions = [] ) {

		$boolIsValid 	= true;
		$strCondition	= '';

		if( true == valArr( $arrobjUnitOfMeasures )
				&& true == array_key_exists( $this->getFromUnitOfMeasureId(), $arrobjUnitOfMeasures )
				&& true == array_key_exists( $this->getToUnitOfMeasureId(), $arrobjUnitOfMeasures ) ) {

			$objFromUnitOfMeasure	= $arrobjUnitOfMeasures[$this->getFromUnitOfMeasureId()];
			$objToUnitOfMeasure		= $arrobjUnitOfMeasures[$this->getToUnitOfMeasureId()];

			if( $objFromUnitOfMeasure->getUnitOfMeasureTypeId() != $objToUnitOfMeasure->getUnitOfMeasureTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_unit_of_measure_id', __( 'Consumption unit of measure and Purchasing unit of measure does not belong to same unit of measure type.' ) ) );
				return false;
			}
		}

		// validating in newly added conversion in newly added rows
		$arrstrCompareFields = [ 'FromUnitOfMeasureId', 'ToUnitOfMeasureId', 'ConversionRate' ];

		foreach( $arrobjUnitOfMeasures as $objUnitOfMeasure ) {

			if( true == valArr( compareObjects( $objUnitOfMeasure, $this, $arrstrCompareFields ) ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_unit_of_measure_id', __( 'Consumption unit of measure and Purchasing unit of measure combination already exists.' ) ) );
				break;
			}
		}

		$arrintCombinationCounts = [];

		foreach( $arrobjUnitOfMeasureConversions as $intKey1 => $objUnitOfMeasureConversion1 ) {

			foreach( $arrobjUnitOfMeasureConversions as $intKey2 => $objUnitOfMeasureConversion2 ) {

				if( false == isset( $arrintCombinationCounts[$intKey1] ) ) {
					$arrintCombinationCounts[$intKey1] = 0;
				}

				if( ( $objUnitOfMeasureConversion1->getFromUnitOfMeasureId() == $objUnitOfMeasureConversion2->getFromUnitOfMeasureId()
					&& $objUnitOfMeasureConversion1->getToUnitOfMeasureId() == $objUnitOfMeasureConversion2->getToUnitOfMeasureId() )
					|| ( $objUnitOfMeasureConversion1->getFromUnitOfMeasureId() == $objUnitOfMeasureConversion2->getToUnitOfMeasureId()
					&& $objUnitOfMeasureConversion1->getToUnitOfMeasureId() == $objUnitOfMeasureConversion2->getFromUnitOfMeasureId() ) ) {

						$arrintCombinationCounts[$intKey1]++;
				}
			}
		}

		foreach( $arrobjUnitOfMeasureConversions as $intKey => $objUnitOfMeasureConversion ) {

			if( 2 == $arrintCombinationCounts[$intKey] ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_unit_of_measure_id', __( 'Consumption unit of measure and Purchasing unit of measure combination already exists.' ) ) );
				break;
			}
		}

		// validating in newly added and updated conversion in db
		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) return $boolIsValid;

		if( true == valIntArr( $arrintUpdateUnitOfMeasureIds ) ) {
			$strCondition = 'id NOT IN ( ' . implode( ',', $arrintUpdateUnitOfMeasureIds ) . ' ) AND ';
		}

		$strWhere = 'WHERE ' .
						$strCondition . '
						cid = ' . ( int ) $this->getCid() . '
						AND (
								( from_unit_of_measure_id = ' . ( int ) $this->getFromUnitOfMeasureId() . ' AND to_unit_of_measure_id = ' . ( int ) $this->getToUnitOfMeasureId() . ' )
								OR ( from_unit_of_measure_id = ' . ( int ) $this->getToUnitOfMeasureId() . ' AND to_unit_of_measure_id = ' . ( int ) $this->getFromUnitOfMeasureId() . ' )
							)';

		if( 0 < \Psi\Eos\Entrata\CUnitOfMeasureConversions::createService()->fetchUnitOfMeasureConversionCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_unit_of_measure_id', __( 'Consumption unit of measure and Purchasing unit of measure combination already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $arrobjUnitOfMeasures = [], $arrintUpdateUnitOfMeasureIds = [], $arrobjUnitOfMeasureConversions = [], $boolIsRequetFromApCatalogItem = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFromUnitOfMeasureId();
				if( false == $boolIsRequetFromApCatalogItem ) {
					$boolIsValid &= $this->valToUnitOfMeasureId();
				}
				$boolIsValid &= $this->valConversionRate();
				$boolIsValid &= $this->valUnitOfMeasureConversion( $objClientDatabase, $arrobjUnitOfMeasures, $arrintUpdateUnitOfMeasureIds, $arrobjUnitOfMeasureConversions );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDefaultUnitOfMeasureConversionId();
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>