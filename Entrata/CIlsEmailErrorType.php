<?php

class CIlsEmailErrorType extends CBaseIlsEmailErrorType {

	const UNKNOWN_PARSE_ERROR 			= 1;
	const NAME_PARSING_ERROR 			= 2;
	const EMAIL_ADDRESS_PARSING_ERROR 	= 3;
	const FIRST_NAME_MISSING 			= 4;
	const INVALID_FIRST_NAME 			= 5;
	const USER_NAME_EMAIL_MISSING 		= 6;
	const INVALID_EMAIL 				= 7;
	const EMAIL_ALREADY_EXISTS 			= 8;
	const PHONE_NUMBER_MISSING 			= 9;
	const INVALID_PHONE_NUMBER 			= 10;
	const DUPLICATE_GUEST_CARD 			= 11;
	const EMPTY_GUEST_CARD 				= 12;
	const INVALID_GUEST_CARD_FORMAT 	= 13;
	const DEDUPE_GUEST_CARD_FAILED 		= 14;
	const MISSMATCH_GUEST_CARD 			= 15;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>