<?php

class CGlReconciliationBankTransaction extends CBaseGlReconciliationBankTransaction {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlReconciliationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankCsvColumnMappingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCleared() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>