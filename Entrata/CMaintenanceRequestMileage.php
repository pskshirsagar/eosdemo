<?php

class CMaintenanceRequestMileage extends CBaseMaintenanceRequestMileage {

	protected $m_strStartTimeAmpm;
	protected $m_strEndTimeAmpm;
	protected $m_strEmployeeName;
	protected $m_fltTotalCharge;
	protected $m_fltTotalHour;
	protected $m_strUnitOfMeasure;

	/**
	 * Get Functions
	 */

	public function getTotalHour() {
		return $this->m_fltTotalHour;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );
		if( isset( $arrmixValues['start_time_ampm'] ) && $boolDirectSet ) {
			$this->m_strStartTimeAmpm = trim( $arrmixValues['start_time_ampm'] );
		} elseif( isset( $arrmixValues['start_time_ampm'] ) ) {
			$this->setStartTimeAmpm( $arrmixValues['start_time_ampm'] );
		}
		if( isset( $arrmixValues['end_time_ampm'] ) && $boolDirectSet ) {
			$this->m_strEndTimeAmpm = trim( $arrmixValues['end_time_ampm'] );
		} elseif( isset( $arrmixValues['end_time_ampm'] ) ) {
			$this->setEndTimeAmpm( $arrmixValues['end_time_ampm'] );
		}
		if( isset( $arrmixValues['employee_name'] ) && $boolDirectSet ) {
			$this->m_strEmployeeName = trim( $arrmixValues['employee_name'] );
		} elseif( isset( $arrmixValues['employee_name'] ) ) {
			$this->setEmployeeName( $arrmixValues['employee_name'] );
		}
		if( isset( $arrmixValues['total_charge'] ) && $boolDirectSet ) {
			$this->m_fltTotalCharge = trim( $arrmixValues['total_charge'] );
		} elseif( isset( $arrmixValues['total_charge'] ) ) {
			$this->setTotalCharge( $arrmixValues['total_charge'] );
		}
		if( isset( $arrmixValues['total_hour'] ) && $boolDirectSet ) {
			$this->m_fltTotalHour = trim( $arrmixValues['total_hour'] );
		} elseif( isset( $arrmixValues['total_hour'] ) ) {
			$this->setTotalHour( $arrmixValues['total_hour'] );
		}
	}

	public function setStartTimeAmpm( $strStartTimeAmpm ) {
		$this->m_strStartTimeAmpm = CStrings::strTrimDef( $strStartTimeAmpm, -1, NULL, true );
	}

	public function setEndTimeAmpm( $strEndTimeAmpm ) {
		$this->m_strEndTimeAmpm = CStrings::strTrimDef( $strEndTimeAmpm, -1, NULL, true );
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = CStrings::strTrimDef( $strEmployeeName, -1, NULL, true );
	}

	public function setTotalCharge( $fltTotalCharge ) {
		$this->m_fltTotalCharge = CStrings::strTrimDef( $fltTotalCharge, -1, NULL, true );
	}

	public function setTotalHour( $fltTotalHour ) {
		$this->m_fltTotalHour = CStrings::strTrimDef( $fltTotalHour, -1, NULL, true );
	}

	public function setUnitOfMeasure( $strUnitOfMeasure ) {
		$this->m_strUnitOfMeasure = CStrings::strTrimDef( $strUnitOfMeasure, -1, NULL, true );
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;

        // It is required
    	if( true == is_null( $this->getCompanyEmployeeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Please select company employee.' ) ) );
        }

        return $boolIsValid;
    }

    public function valVehicleId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getVehicleId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Please select vehicle.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        if( true == is_null( $this->getDescription() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Description details are required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valStartingMileage() {
        $boolIsValid = true;

        if( true == is_null( $this->getStartingMileage() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Starting Mileage is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valEndingMileage() {
        $boolIsValid = true;

        if( true == is_null( $this->getEndingMileage() ) || 0 == $this->getEndingMileage() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Ending Mileage is required.' ) ) );
        } elseif( $this->getEndingMileage() < $this->getStartingMileage() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Ending Mileage must be greater than Starting Mileage.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTotalMileage() {
        $boolIsValid = true;

        if( true == is_null( $this->getTotalMileage() ) || 0 == $this->getTotalMileage() ) {
        	$boolIsValid = false;
        	if( CPropertyPreference::MILES == $this->m_strUnitOfMeasure ) {
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Total Miles can not be zero.' ) ) );
	        } else {
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Total Kilometers can not be zero.' ) ) );

	        }
        }

        return $boolIsValid;
    }

    public function valMileageDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInvoiceDetailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCompanyEmployeeId();
        		$boolIsValid &= $this->valVehicleId();
        		$boolIsValid &= $this->valStartingMileage();
        		$boolIsValid &= $this->valEndingMileage();
        		$boolIsValid &= $this->valTotalMileage();
        		break;

        	case VALIDATE_INSERT_PARTIALLY:
        		$boolIsValid &= $this->valCompanyEmployeeId();
        		$boolIsValid &= $this->valVehicleId();
        		$boolIsValid &= $this->valTotalMileage();
        		break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valCompanyEmployeeId();
        		$boolIsValid &= $this->valVehicleId();
        		$boolIsValid &= $this->valStartingMileage();
        		$boolIsValid &= $this->valEndingMileage();
        		$boolIsValid &= $this->valTotalMileage();
        		break;

        	case VALIDATE_UPDATE_PARTIALLY:
        		$boolIsValid &= $this->valCompanyEmployeeId();
        		$boolIsValid &= $this->valVehicleId();
        		$boolIsValid &= $this->valTotalMileage();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>