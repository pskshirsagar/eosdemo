<?php

class CCompanyApplicationAddOnGroup extends CBaseCompanyApplicationAddOnGroup {

	protected $m_strTitle;

    public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {

	    if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( $arrmixValues['title'] );

	    parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

    public function setTitle( $strTitle ) {
    	$this->m_strTitle = CStrings::strTrimDef( $strTitle, 50, NULL, true );
    }

    public function getTitle() {
    	return $this->m_strTitle;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAddOnGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRequired() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>