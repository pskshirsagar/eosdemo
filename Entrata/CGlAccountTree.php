<?php

use Psi\Eos\Entrata\CApCodes;

class CGlAccountTree extends CBaseGlAccountTree {

	const LOOKUP_TYPE_DEBIT_GL_ACCOUNTS						= 'debit_gl_accounts';
	const LOOKUP_TYPE_CREDIT_GL_ACCOUNTS					= 'credit_gl_accounts';
	const HIDE_ACCOUNT_BALANCE_FROM_REPORTS					= 0;

	protected $m_boolRestrictForAp;
	protected $m_boolRestrictForGl;
	protected $m_boolIsGlAccountEditable;

	protected $m_intApCodeId;
	protected $m_intGlAccountTypeId;
	protected $m_intCashFlowTypeId;
	protected $m_intGlAccountUsageTypeId;
	protected $m_intDefaultGlAccountId;
	protected $m_intGlGroupGlAccountTypeId;

	protected $m_strGlTreeName;
	protected $m_strSystemName;
	protected $m_strChildGlAccountTreeId;
	protected $m_strChildGlAccountName;
	protected $m_strSystemAccountNumber;
	protected $m_strMasterAccountNumber;
	protected $m_strAccountNumberPattern;
	protected $m_strAccountNumberDelimiter;
	protected $m_strSystemAccountNumberPattern;
	protected $m_strSystemAccountNumberDelimiter;
	protected $m_strGroupLocation;
	protected $m_strChildGlType;

	/**
	 * Get Functions
	 */

	public function getRestrictForAp() {
		return $this->m_boolRestrictForAp;
	}

	public function getRestrictForGl() {
		return $this->m_boolRestrictForGl;
	}

	public function getDefaultGlAccountId() {
		return $this->m_intDefaultGlAccountId;
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getCashFlowTypeId() {
		return $this->m_intCashFlowTypeId;
	}

	public function getGlTreeName() {
		return $this->m_strGlTreeName;
	}

	public function getAccountNumberPattern() {
		return $this->m_strAccountNumberPattern;
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	public function getSystemAccountNumberPattern() {
		return $this->m_strSystemAccountNumberPattern;
	}

	public function getSystemAccountNumberDelimiter() {
		return $this->m_strSystemAccountNumberDelimiter;
	}

	public function getSystemName() {
		return $this->m_strSystemName;
	}

	public function getSystemAccountNumber() {
		return $this->m_strSystemAccountNumber;
	}

	public function getMasterAccountNumber() {
		return $this->m_strMasterAccountNumber;
	}

	public function getChildGlAccountTreeId() {
		return $this->m_strChildGlAccountTreeId;
	}

	public function getChildGlAccountName() {
		return $this->m_strChildGlAccountName;
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function getGroupLocation() {
		return $this->m_strGroupLocation;
	}

	public function getChildGlType() {
		return $this->m_strChildGlType;
	}

	public function getIsGlAccountEditable() {
		return $this->m_boolIsGlAccountEditable;
	}

	public function getGlAccountUsageTypeId() {
		return $this->m_intGlAccountUsageTypeId;
	}

	public function getGlGroupGlAccountTypeId() {
		return $this->m_intGlGroupGlAccountTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['gl_account_type_id'] ) && $boolDirectSet ) $this->m_strGlAccountTypeId = trim( stripcslashes( $arrmixValues['gl_account_type_id'] ) );
			elseif ( isset( $arrmixValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_account_type_id'] ) : $arrmixValues['gl_account_type_id'] );
		if( isset( $arrmixValues['default_gl_account_id'] ) && $boolDirectSet ) $this->m_strDefaultGlAccountId = trim( stripcslashes( $arrmixValues['default_gl_account_id'] ) );
			elseif ( isset( $arrmixValues['default_gl_account_id'] ) ) $this->setDefaultGlAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['default_gl_account_id'] ) : $arrmixValues['default_gl_account_id'] );
		if( isset( $arrmixValues['cash_flow_type_id'] ) && $boolDirectSet ) $this->m_intCashFlowTypeId = trim( $arrmixValues['cash_flow_type_id'] );
			elseif ( isset( $arrmixValues['cash_flow_type_id'] ) ) $this->setCashFlowTypeId( $arrmixValues['cash_flow_type_id'] );
		if( isset( $arrmixValues['gl_tree_name'] ) && $boolDirectSet ) $this->m_strGlTreeName = trim( stripcslashes( $arrmixValues['gl_tree_name'] ) );
			elseif ( isset( $arrmixValues['gl_tree_name'] ) ) $this->setGlTreeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_tree_name'] ) : $arrmixValues['gl_tree_name'] );
		if( isset( $arrmixValues['account_number_pattern'] ) && $boolDirectSet ) $this->m_strAccountNumberPattern = trim( stripcslashes( $arrmixValues['account_number_pattern'] ) );
			elseif ( isset( $arrmixValues['account_number_pattern'] ) ) $this->setAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number_pattern'] ) : $arrmixValues['account_number_pattern'] );
		if( isset( $arrmixValues['account_number_delimiter'] ) && $boolDirectSet ) $this->m_strAccountNumberDelimiter = trim( stripcslashes( $arrmixValues['account_number_delimiter'] ) );
			elseif ( isset( $arrmixValues['account_number_delimiter'] ) ) $this->setAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number_delimiter'] ) : $arrmixValues['account_number_delimiter'] );
		if( isset( $arrmixValues['system_account_number_pattern'] ) && $boolDirectSet ) $this->m_strSystemAccountNumberPattern = trim( stripcslashes( $arrmixValues['system_account_number_pattern'] ) );
			elseif ( isset( $arrmixValues['system_account_number_pattern'] ) ) $this->setSystemAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_account_number_pattern'] ) : $arrmixValues['system_account_number_pattern'] );
		if( isset( $arrmixValues['system_account_number_delimiter'] ) && $boolDirectSet ) $this->m_strSystemAccountNumberDelimiter = trim( stripcslashes( $arrmixValues['system_account_number_delimiter'] ) );
			elseif ( isset( $arrmixValues['system_account_number_delimiter'] ) ) $this->setSystemAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_account_number_delimiter'] ) : $arrmixValues['system_account_number_delimiter'] );
		if( isset( $arrmixValues['system_name'] ) && $boolDirectSet ) $this->m_strSystemName = trim( stripcslashes( $arrmixValues['system_name'] ) );
			elseif ( isset( $arrmixValues['system_name'] ) ) $this->setSystemName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_name'] ) : $arrmixValues['system_name'] );
		if( isset( $arrmixValues['system_account_number'] ) && $boolDirectSet ) $this->m_strSystemAccountNumber = trim( stripcslashes( $arrmixValues['system_account_number'] ) );
			elseif ( isset( $arrmixValues['system_account_number'] ) ) $this->setSystemAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_account_number'] ) : $arrmixValues['system_account_number'] );

		if( isset( $arrmixValues['master_account_number'] ) && $boolDirectSet ) $this->m_strMasterAccountNumber = trim( stripcslashes( $arrmixValues['master_account_number'] ) );
			elseif ( isset( $arrmixValues['master_account_number'] ) ) $this->setMasterAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['master_account_number'] ) : $arrmixValues['master_account_number'] );

		if( isset( $arrmixValues['child_gl_account_tree_id'] ) && $boolDirectSet ) $this->m_strChildGlAccountTreeId = trim( stripcslashes( $arrmixValues['child_gl_account_tree_id'] ) );
			elseif ( isset( $arrmixValues['child_gl_account_tree_id'] ) ) $this->setChildGlAccountTreeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['child_gl_account_tree_id'] ) : $arrmixValues['child_gl_account_tree_id'] );

		if( isset( $arrmixValues['child_gl_account_name'] ) && $boolDirectSet ) $this->m_strChildGlAccountName = trim( stripcslashes( $arrmixValues['child_gl_account_name'] ) );
			elseif ( isset( $arrmixValues['child_gl_account_name'] ) ) $this->setChildGlAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['child_gl_account_name'] ) : $arrmixValues['child_gl_account_name'] );

		if( isset( $arrmixValues['restrict_for_ap'] ) && $boolDirectSet ) $this->m_boolRestrictForAp = trim( stripcslashes( $arrmixValues['restrict_for_ap'] ) );
			elseif ( isset( $arrmixValues['restrict_for_ap'] ) ) $this->setRestrictForAp( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['restrict_for_ap'] ) : $arrmixValues['restrict_for_ap'] );

		if( isset( $arrmixValues['restrict_for_gl'] ) && $boolDirectSet ) $this->m_boolRestrictForGl = trim( stripcslashes( $arrmixValues['restrict_for_gl'] ) );
			elseif ( isset( $arrmixValues['restrict_for_gl'] ) ) $this->setRestrictForGl( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['restrict_for_gl'] ) : $arrmixValues['restrict_for_gl'] );

		if( isset( $arrmixValues['ap_code_id'] ) && $boolDirectSet ) $this->m_intApCodeId = trim( stripcslashes( $arrmixValues['ap_code_id'] ) );
			elseif ( isset( $arrmixValues['ap_code_id'] ) ) $this->setApCodeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ap_code_id'] ) : $arrmixValues['ap_code_id'] );

		if( isset( $arrmixValues['child_gl_type'] ) && $boolDirectSet ) $this->m_strGlTreeName = trim( stripcslashes( $arrmixValues['child_gl_type'] ) );
			elseif ( isset( $arrmixValues['child_gl_type'] ) ) $this->setChildGlType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['child_gl_type'] ) : $arrmixValues['child_gl_type'] );

		if( isset( $arrmixValues['group_location'] ) && $boolDirectSet ) $this->m_strGroupLocation = trim( stripcslashes( $arrmixValues['group_location'] ) );
			elseif ( isset( $arrmixValues['group_location'] ) ) $this->setGroupLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['group_location'] ) : $arrmixValues['group_location'] );

		if( isset( $arrmixValues['is_gl_account_editable'] ) && $boolDirectSet ) $this->m_boolIsGlAccountEditable = $arrmixValues['is_gl_account_editable'];
			elseif ( isset( $arrmixValues['is_gl_account_editable'] ) ) $this->setIsGlAccountEditable( $arrmixValues['is_gl_account_editable'] );

		if( isset( $arrmixValues['gl_account_usage_type_id'] ) && $boolDirectSet ) $this->m_intGlAccountUsageTypeId = trim( stripcslashes( $arrmixValues['gl_account_usage_type_id'] ) );
			elseif ( isset( $arrmixValues['gl_account_usage_type_id'] ) ) $this->setGlAccountUsageTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_account_usage_type_id'] ) : $arrmixValues['gl_account_usage_type_id'] );

		if( isset( $arrmixValues['gl_group_gl_account_type_id'] ) && $boolDirectSet ) $this->m_intGlGroupGlAccountTypeId = trim( stripcslashes( $arrmixValues['gl_group_gl_account_type_id'] ) );
			elseif ( isset( $arrmixValues['gl_group_gl_account_type_id'] ) ) $this->setGlGroupGlAccountTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_group_gl_account_type_id'] ) : $arrmixValues['gl_group_gl_account_type_id'] );

		return;
	}

	public function setRestrictForGl( $boolRestrictForGl ) {
		$this->m_boolRestrictForGl = CStrings::strToBool( $boolRestrictForGl );
	}

	public function setRestrictForAp( $boolRestrictForAp ) {
		$this->m_boolRestrictForAp = CStrings::strToBool( $boolRestrictForAp );
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = CStrings::strToIntDef( $intGlAccountTypeId, NULL, false );
	}

	public function setDefaultGlAccountId( $intDefaultGlAccountId ) {
		$this->m_intDefaultGlAccountId = CStrings::strToIntDef( $intDefaultGlAccountId, NULL, false );
	}

	public function setCashFlowTypeId( $intCashFlowTypeId ) {
		$this->m_intCashFlowTypeId = CStrings::strToIntDef( $intCashFlowTypeId, NULL, false );
	}

	public function setGlTreeName( $strName ) {
		$this->m_strGlTreeName = CStrings::strTrimDef( $strName, 50, NULL, true );
	}

	public function setAccountNumberPattern( $strAccountNumberPattern ) {
		$this->m_strAccountNumberPattern = CStrings::strTrimDef( $strAccountNumberPattern, 50, NULL, true );
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		$this->m_strAccountNumberDelimiter = CStrings::strTrimDef( $strAccountNumberDelimiter, 50, NULL, true );
	}

	public function setSystemAccountNumberPattern( $strSystemAccountNumberPattern ) {
		$this->m_strSystemAccountNumberPattern = CStrings::strTrimDef( $strSystemAccountNumberPattern, 50, NULL, true );
	}

	public function setSystemAccountNumberDelimiter( $strSystemAccountNumberDelimiter ) {
		$this->m_strSystemAccountNumberDelimiter = CStrings::strTrimDef( $strSystemAccountNumberDelimiter, 50, NULL, true );
	}

	public function setSystemName( $strSystemName ) {
		$this->m_strSystemName = CStrings::strTrimDef( $strSystemName, 50, NULL, true );
	}

	public function setSystemAccountNumber( $strSystemAccountNumber ) {
		$this->m_strSystemAccountNumber = CStrings::strTrimDef( $strSystemAccountNumber, 80, NULL, true );
	}

	public function setMasterAccountNumber( $strMasterAccountNumber ) {
		$this->m_strMasterAccountNumber = CStrings::strTrimDef( $strMasterAccountNumber, 80, NULL, true );
	}

	public function setChildGlAccountTreeId( $strChildGlAccountTreeId ) {
		$this->m_strChildGlAccountTreeId = $strChildGlAccountTreeId;
	}

	public function setChildGlAccountName( $strChildGlAccountName ) {
		$this->m_strChildGlAccountName = $strChildGlAccountName;
	}

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = $intApCodeId;
	}

	public function setGroupLocation( $strGroupLocation ) {
		$this->m_strGroupLocation = $strGroupLocation;
	}

	public function setChildGlType( $strChildGlType ) {
		$this->m_strChildGlType = $strChildGlType;
	}

	public function setIsGlAccountEditable( $boolIsGlAccountEditable ) {
		return $this->m_boolIsGlAccountEditable = $boolIsGlAccountEditable;
	}

	public function setGlAccountUsageTypeId( $intGlAccountUsageTypeId ) {
		$this->m_intGlAccountUsageTypeId = CStrings::strToIntDef( $intGlAccountUsageTypeId, NULL, false );
	}

	public function setGlGroupGlAccountTypeId( $intGlGroupGlAccountTypeId ) {
		$this->m_intGlGroupGlAccountTypeId = CStrings::strToIntDef( $intGlGroupGlAccountTypeId, NULL, false );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlGroupId() {

		if( true == valId( $this->getChildGlAccountTreeId() ) ) return true;

		$boolIsValid = true;

		if( false == valId( $this->getGlGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_group_id', __( 'Group location is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Gl account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		if( true == valId( $this->getChildGlAccountTreeId() ) ) return true;

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Account name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAccountNumber( $objClientDatabase, $arrobjGlAccountTrees, $boolIsBulkAccountMapping = NULL, $boolIsUpdate = false, $boolIsReassignGlAccount = false, $intCurrentGLAccountTreeId = NULL, $boolIsFromMaster = false ) {

		$boolIsValid					= true;
		$strAccountNumber				= trim( $this->getAccountNumber() );
		$strAccountNumberDelimiter		= trim( $this->getAccountNumberDelimiter() );
		$strFormattedAccountNumber		= '';

		$boolFlag						= false;
		$intCountAccountNumber			= 0;
		$intAccountNumberPatternCount	= 0;
		$arrintAccountNumberPattern		= explode( ',', str_replace( '[', '', str_replace( ']', '', str_replace( '][', ',', $this->getAccountNumberPattern() ) ) ) );
		$intAccountNumberLength			= strlen( $strAccountNumber );

		foreach( $arrintAccountNumberPattern as $intAccountNumberPattern ) {

			if( $intAccountNumberLength >= $intAccountNumberPattern ) {

				if( 0 != mb_strlen( trim( mb_substr( $strAccountNumber, $intAccountNumberPatternCount, $intAccountNumberPattern ) ) ) ) {
					$strFormattedAccountNumber .= mb_substr( $strAccountNumber, $intAccountNumberPatternCount, $intAccountNumberPattern ) . $strAccountNumberDelimiter;
				}
				$intAccountNumberPatternCount += $intAccountNumberPattern;
			} else {
				$strFormattedAccountNumber 		= $strAccountNumber;
				break;
			}
		}

		if( \Psi\CStringService::singleton()->substr( $strFormattedAccountNumber, -1 ) == $strAccountNumberDelimiter ) {
			$strFormattedAccountNumber = rtrim( str_replace( ' ', '', $strFormattedAccountNumber ), $strAccountNumberDelimiter );
		}

		$arrobjOriginalGlTrees = ( array ) CGlAccountTrees::fetchGlAccountTreesByCidByGlTreeIdByAccountNumber( $this->getCid(), $this->getGlTreeId(), $strAccountNumber, $objClientDatabase, $strFormattedAccountNumber );

		if( true == valId( $this->getChildGlAccountTreeId() ) ) {
			if( true == valArr( $arrobjOriginalGlTrees ) && false == array_key_exists( $this->getId(), $arrobjOriginalGlTrees ) && true == $boolIsUpdate && true == is_null( $this->getGlAccountTreeId() ) ) {
				$boolIsValid = false;
				if( true == $boolIsFromMaster ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists on {%s, 1}.', [ $strAccountNumber, $this->getGlTreeName() ] ) ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
				}
			}
			return $boolIsValid;
		}

		if( true == $boolIsBulkAccountMapping || true == is_null( $boolIsBulkAccountMapping ) ) {
			if( false == valStr( $strAccountNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( 'Account number is required.' ) ) );
			} elseif( false == ctype_alnum( str_replace( ' ', '', $strAccountNumber ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( 'Account number allow only [a-z] or [A-Z] or [1-9].' ) ) );
			} elseif( true == valArr( $arrobjOriginalGlTrees ) && false == array_key_exists( $this->getId(), $arrobjOriginalGlTrees ) && false == $boolIsUpdate ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
			} elseif( true == valArr( $arrobjOriginalGlTrees ) && false == array_key_exists( $this->getId(), $arrobjOriginalGlTrees ) && true == $boolIsUpdate && true == is_null( $this->getGlAccountTreeId() ) ) {
				$boolIsValid = false;
				if( true == $boolIsFromMaster ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists on {%s, 1}.', [ $strAccountNumber, $this->getGlTreeName() ] ) ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
				}
			} elseif( true == valArr( $arrobjOriginalGlTrees ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrobjOriginalGlTrees ) && true == array_key_exists( $this->getId(), $arrobjOriginalGlTrees )
			          && 1 == $arrobjOriginalGlTrees[$this->getId()]->getHideAccountBalanceFromReports()
			          && 0 == $this->getHideAccountBalanceFromReports()
			          && true == $boolIsUpdate
			          && true == is_null( $this->getGlAccountTreeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
			} elseif( $boolIsReassignGlAccount && ( $this->getId() == $intCurrentGLAccountTreeId || ( valArr( $arrobjOriginalGlTrees ) && !valId( $intCurrentGLAccountTreeId ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
			} else {

				if( true == valArr( $arrobjGlAccountTrees ) ) {

					foreach( $arrobjGlAccountTrees as $objGlAccountTree ) {
						if( $strFormattedAccountNumber == $objGlAccountTree->getFormattedAccountNumber() && $this->getId() != $objGlAccountTree->getId() ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} account number already exists.', [ $strAccountNumber ] ) ) );
							return;
						}
					}
				}

				foreach( $arrintAccountNumberPattern as $intAccountNumberPattern ) {
					$intCountAccountNumber = $intCountAccountNumber + $intAccountNumberPattern;
					if( $intAccountNumberLength <= $intCountAccountNumber ) {
						$boolFlag = true;
						break;
					}
				}

				if( false == $boolFlag ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( '{%s, 0} Account number should be in correct format.', [ $this->getGlTreeName() ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideIfZero() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {

		$boolIsValid = true;

		if( false == is_null( $this->getIsSystem() ) && 1 == $this->getIsSystem() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', __( 'System GL account cannot be deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondaryNumber( $objClientDatabase = NULL ) {

		if( true == valId( $this->getChildGlAccountTreeId() ) ) return true;

		$boolIsValid		= true;
		$intAccountNumber	= $this->getAccountNumber();
		$strName			= $this->getName();

		if( 0 < preg_match( '/[\s]/', $this->getSecondaryNumber() ) ) {

			$boolIsValid	= false;

			if( false == empty( $intAccountNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'External Id can not contain spaces for \'{%s, 0}\'.', [ $intAccountNumber ] ) ) );
			} elseif( false == empty( $strName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'External Id can not contain spaces for \'{%s, 0}\'.', [ $strName ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'External Id can not contain spaces.' ) ) );
			}

			return $boolIsValid;
		}

		if( true == valStr( $this->getSecondaryNumber() ) && true == valObj( $objClientDatabase, 'CDatabase' ) && true == $boolIsValid ) {
			// Check for existing secondary number with the same client and gl tree id
			$arrobjGlAccountTrees = ( array ) \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesNotByIdByGlTreeIdBySecondaryNumberByCid( $this->getId(), $this->getGlTreeId(), $this->getSecondaryNumber(), $this->getCid(), $objClientDatabase, $this->getGlAccountTreeId() );

			if( false == valArr( $arrobjGlAccountTrees ) ) {
				return $boolIsValid;
			}

			$boolIsValid = false;

			if( false == empty( $intAccountNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'Unique External Id required for \'{%s, 0}\' GL Account.', [ $intAccountNumber ] ) ) );
			} elseif( false == empty( $strName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'Unique External Id required for \'{%s, 0}\' GL Account.', [ $strName ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'Unique External Id required for each GL Account.' ) ) );
			}

		}

		return $boolIsValid;
	}

	public function valDisabledOn( $objClientDatabase ) {

		$boolIsValid = true;

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND auto_post_gpr_adjustment = 1
						AND ( ap_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR retained_earnings_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR market_rent_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR gain_to_lease_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR loss_to_lease_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR vacancy_loss_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR delinquent_rent_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR prepaid_rent_gl_account_id = ' . ( int ) $this->getGlAccountId() . ' )';

		if( 0 < CPropertyGlSettings::fetchPropertyGlSettingCount( $strWhere, $objClientDatabase ) || 0 < \Psi\Eos\Entrata\CUnitSpaceExclusions::createService()->fetchUnitSpaceExclusionCountForBreakOutExcludedUnitsByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'GL account can not be disabled, it is being used in \'Accounting Settings\' of property settings.' ) ) );
			$boolIsValid &= false;
		}

		if( 0 < CPropertyBankAccounts::fetchCountActivePropertyBankAccountsByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'GL account can not be disabled, it is being used in active bank account(s).' ) ) );
			$boolIsValid &= false;
		}

		$strErrorMessage = '';
		$strArCodeNames = Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeNamesByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase );

		if( valStr( $strArCodeNames ) ) {

			$strPropertiesNames = Psi\Eos\Entrata\CArCodes::createService()->fetchPropertiesNamesOfAssociatedArCodesByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase );

			if( valStr( $strPropertiesNames ) ) {
				$strErrorMessage    = 'This account is associated to the following charge codes: [ '. str_replace(',', ', ', str_replace('"', '', substr( $strArCodeNames, 1, -1 ) ) ) . ' ] in the property settings for propert(ies) [ ' . str_replace(',', ', ', str_replace('"', '', substr( $strPropertiesNames, 1, -1 ) ) ) . ' ]. Edit and associate a different GL account to these charge codes before disabling this account.';
			} else {
				$strErrorMessage    = 'This account is associated to the following charge codes: [ ' . str_replace(',', ', ', str_replace('"', '', substr( $strArCodeNames, 1, -1 ) ) ) . ' ] in your company settings. Edit and associate a different GL account to these charge codes before disabling this account.';
			}
		}

		if( valStr( $strErrorMessage ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( $strErrorMessage ) ) );
			$boolIsValid &= false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ( retained_earnings_gl_account_id = ' . ( int ) $this->getGlAccountId() . ' OR ap_gl_account_id = ' . ( int ) $this->getGlAccountId() . ' )';

		if( 0 < CGlSettings::fetchGlSettingCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'GL account can not be disabled, it is being used in company GL settings.' ) ) );
			$boolIsValid &= false;
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND gl_account_id = ' . ( int ) $this->getGlAccountId();

		if( 0 < \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchFeeTemplateCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'GL account can not be disabled, it is being used in management fee template(s).' ) ) );
			$boolIsValid &= false;
		}

		$arrmixApCodesData = ( array ) CApCodes::createService()->fetchApCodesByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase );

		foreach( $arrmixApCodesData as $arrmixApCodeData ) {
			if( 0 < $arrmixApCodeData['count'] ) {
				$strErrorMessage = 'GL account can not be disabled, it is being used in catalog item(s).';

				if( CApCodeType::JOB_COST == $arrmixApCodeData['ap_code_type_id'] ) {
					$strErrorMessage = 'This GL account is associated with the job code [ ' . $arrmixApCodeData['code_names'] . ' ] and cannot be disabled.';
				} elseif( CApCodeType::CATALOG_ITEMS == $arrmixApCodeData['ap_code_type_id'] ) {
					$strErrorMessage = 'This GL account is associated with the catalog item [ ' . $arrmixApCodeData['code_names'] . ' ] and cannot be disabled.';
				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( $strErrorMessage ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valEnabledOn( $objClientDatabase ) {

		$intDisabledGlGroupCount = $this->fetchCountDisabledGlGroup( $objClientDatabase );

		if( 0 < $intDisabledGlGroupCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'GL account cannot be enabled, it is associated with a disabled group.' ) ) );
			return false;
		}

		return true;
	}

	public function valGlAccountDependencies( $objClientDatabase ) {

		$intSetupTableCount			= 0;
		$intTransactionTableCount	= 0;

		$arrmixData = ( array ) CDataChanges::fetchTransacationsCountForGlAccountByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase, true );

		foreach( $arrmixData as $arrmixDatum ) {

			if( CDataChange::TABLE_TYPE_TRANSACTION == $arrmixDatum['table_type'] ) {
				$intTransactionTableCount = $intTransactionTableCount + $arrmixDatum['row_count'];
			} elseif( CDataChange::TABLE_TYPE_SETUP == $arrmixDatum['table_type'] ) {
				$intSetupTableCount = $intSetupTableCount + $arrmixDatum['row_count'];
			}
		}

		if( 0 < $intTransactionTableCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The GL account cannot be deleted because GL transactions exist for this account. If you do not want to use this account please disable it.' ) ) );
			return false;
		}

		if( 0 < $intSetupTableCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The GL account cannot be deleted because the account is currently being used in the system. Please update your configuration and retry.' ) ) );
			return false;
		}

		// Validate catalog items
		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ap_code_type_id <> ' . CApCodeType::GL_ACCOUNT . '
						AND ( item_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR ap_gl_account_id = ' . ( int ) $this->getGlAccountId() . '
								OR consumption_gl_account_id = ' . ( int ) $this->getGlAccountId() . ' ) ';

		if( 0 < \Psi\Eos\Entrata\CApCodes::createService()->fetchApCodeCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_on', __( 'The GL account cannot be disabled because it is being used in catalog item(s).' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $arrobjGlAccountTrees = NULL, $boolIsBulkAccountMapping = NULL, $intCurrentGLAccountTreeId = NULL, $boolIsFromMaster = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsUpdate = false;
				if( VALIDATE_UPDATE == $strAction ) {
					$boolIsUpdate = true;
				}

				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAccountNumber( $objClientDatabase, $arrobjGlAccountTrees, $boolIsBulkAccountMapping, $boolIsUpdate, false, NULL, $boolIsFromMaster );
				$boolIsValid &= $this->valGlGroupId();
				$boolIsValid &= $this->valSecondaryNumber( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valIsSystem();
				$boolIsValid &= $this->valGlAccountDependencies( $objClientDatabase );
				break;

			case 'enable_gl_account_update':
				$boolIsValid &= $this->valEnabledOn( $objClientDatabase );
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAccountNumber( $objClientDatabase, $arrobjGlAccountTrees, $boolIsBulkAccountMapping );
				$boolIsValid &= $this->valGlGroupId();
				$boolIsValid &= $this->valSecondaryNumber( $objClientDatabase );
				break;

			case 'disable_gl_account_update':
				$boolIsValid &= $this->valDisabledOn( $objClientDatabase );
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAccountNumber( $objClientDatabase, $arrobjGlAccountTrees, $boolIsBulkAccountMapping );
				$boolIsValid &= $this->valGlGroupId();
				$boolIsValid &= $this->valSecondaryNumber( $objClientDatabase );
				break;

			case 'enable_bulk_gl_account_update':
				$boolIsValid &= $this->valEnabledOn( $objClientDatabase );
				$boolIsValid &= $this->valGlGroupId();
				break;

			case 'disable_bulk_gl_account_update':
				$boolIsValid &= $this->valDisabledOn( $objClientDatabase );
				$boolIsValid &= $this->valGlGroupId();
				break;

			case 'reassign_child_gl_account':
				$boolIsValid &= $this->valGlAccountId();
				if( ! $boolIsValid ) {
					break;
				}
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAccountNumber( $objClientDatabase, $arrobjGlAccountTrees, $boolIsBulkAccountMapping, false, true, $intCurrentGLAccountTreeId );
				$boolIsValid &= $this->valGlGroupId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchActiveGlAccountTreesNotById( $objClientDatabase, $boolShowParents = true ) {
		return CGlAccountTrees::fetchActiveGlAccountTreesByGlAccountIdNotByIdByCid( $this->getGlAccountId(), $this->getId(), $this->getCid(), $objClientDatabase, $boolShowParents );
	}

	public function fetchGlAccountTreesNotById( $objClientDatabase ) {
		return CGlAccountTrees::fetchGlAccountTreesByGlAccountIdNotByIdByCid( $this->getGlAccountId(), $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchCountDisabledGlGroup( $objClientDatabase ) {

		$arrintData = CGlGroups::fetchCountDisabledGlGroupByGlAccountIdByCid( $this->getGlAccountId(), $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrintData ) ) return $arrintData[0]['count'];
		return 0;
	}

}
?>