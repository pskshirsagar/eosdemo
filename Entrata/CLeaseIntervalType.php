<?php

class CLeaseIntervalType extends CBaseLeaseIntervalType {

	const APPLICATION 			= 1;
	const MONTH_TO_MONTH 		= 2;
	const RENEWAL 				= 3;
	const LEASE_MODIFICATION	= 4;
	const TRANSFER 				= 5;
	const EXTENSION             = 6;

	public static $c_arrintRenewedLeaseIntervalTypes 				= [ self::RENEWAL, self::LEASE_MODIFICATION ];
	public static $c_arrintDocumentPacketLeaseIntervalTypes 		= [ self::APPLICATION, self::RENEWAL, self::TRANSFER ];
	public static $c_arrintRenewalTransferLeaseIntervalTypes 		= [ self::RENEWAL, self::TRANSFER ];
	public static $c_arrintAddOnLeaseIntervalTypes 					= [ self::APPLICATION, self::RENEWAL, self::TRANSFER, self::MONTH_TO_MONTH ];
	public static $c_arrintFutureLeaseIntervalTypes 				= [ self::RENEWAL, self::EXTENSION ];
	public static $c_arrintNonContractulaLeaseIntervalTypes 		= [ self::MONTH_TO_MONTH, self::EXTENSION ];

	// @FIXME: Typo in Corpororate
	public static $c_arrintCorpororateScreeningLeaseIntervalTypes 	= [ self::RENEWAL, self::LEASE_MODIFICATION, self::TRANSFER ];
	public static $c_arrintLeaseSigningLeaseIntervalTypes 			= [ self::APPLICATION, self::RENEWAL, self::LEASE_MODIFICATION, self::TRANSFER ];
	public static $c_arrintLeaseCustomerLeaseIntervalTypes 			= [ self::RENEWAL, self::TRANSFER, self::MONTH_TO_MONTH ];

	public static $c_arrintUtilityLeaseIntervalTypes 			= [ self::APPLICATION, self::MONTH_TO_MONTH, self::RENEWAL ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function loadCustomizedLeaseIntervalTypes() {

		$arrstrCustomizedLeaseIntervalTypes = [];

		$arrstrCustomizedLeaseIntervalTypes[self::APPLICATION] 			= __( 'Application' );
		$arrstrCustomizedLeaseIntervalTypes[self::MONTH_TO_MONTH]		= __( 'Month to Month' );
		$arrstrCustomizedLeaseIntervalTypes[self::RENEWAL] 				= __( 'Renewal' );
		$arrstrCustomizedLeaseIntervalTypes[self::LEASE_MODIFICATION]	= __( 'Lease Modification' );
		$arrstrCustomizedLeaseIntervalTypes[self::TRANSFER] 			= __( 'Transfer' );
        $arrstrCustomizedLeaseIntervalTypes[self::EXTENSION] 			= __( 'Extension' );

		return $arrstrCustomizedLeaseIntervalTypes;
	}

	public function getLeaseIntervalTypeNameByLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$strIntervalTypeName = NULL;

		switch( $intLeaseIntervalTypeId ) {

			case CLeaseIntervalType::APPLICATION:
				$strIntervalTypeName = __( 'Application' );
				break;

			case CLeaseIntervalType::MONTH_TO_MONTH:
				$strIntervalTypeName = __( 'Month to Month' );
				break;

			case CLeaseIntervalType::RENEWAL:
				$strIntervalTypeName = __( 'Renewal' );
				break;

			case CLeaseIntervalType::LEASE_MODIFICATION:
				$strIntervalTypeName = __( 'Lease Modification' );
				break;

			case CLeaseIntervalType::TRANSFER:
				$strIntervalTypeName = __( 'Transfer' );
				break;

			case CLeaseIntervalType::EXTENSION:
				$strIntervalTypeName = __( 'Extension' );
				break;

			default:
				// default case
				break;
		}

		return $strIntervalTypeName;
	}

	public function getLeaseIntervalTypeIdToStrArray() {
		return [
			self::APPLICATION			=> __( 'Application' ),
			self::MONTH_TO_MONTH 		=> __( 'Month to Month' ),
			self::RENEWAL 				=> __( 'Renewal' ),
			self::LEASE_MODIFICATION 	=> __( 'Lease Modification' ),
			self::TRANSFER 				=> __( 'Transfer' ),
			self::EXTENSION 			=> __( 'Extension' ),
		];
	}

	public static function getValidPolicyDcoumentLeaseIntervalTypeIds() {
		return [ self::APPLICATION, self::RENEWAL ];
	}

}
?>