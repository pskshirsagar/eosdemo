<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVotes
 * Do not add any new functions to this class.
 */

class CCustomerVotes extends CBaseCustomerVotes {

	public static function fetchCustomerVotesByCidByPropertyIdByCustomerId( $intCid, $intPropertyId, $intCustomerId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						customer_votes 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND customer_id = ' . ( int ) $intCustomerId;

		return parent::fetchCustomerVotes( $strSql, $objDatabase );
	}

}
?>