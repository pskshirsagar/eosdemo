<?php

class CArActions extends CBaseArActions {

    public static function fetchArActions( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CArAction', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchArAction( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CArAction', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>