<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportBatchTypes
 * Do not add any new functions to this class.
 */

class CApExportBatchTypes extends CBaseApExportBatchTypes {

	public static function fetchApExportBatchTypes( $strSql, $objClientDatabase ) {
		return self::fetchObjects( $strSql, 'CApExportBatchType', $objClientDatabase );
	}

	public static function fetchApExportBatchType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApExportBatchType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllApExportBatchTypes( $objClientDatabase ) {
		return self::fetchApExportBatchTypes( 'SELECT * FROM public.ap_export_batch_types WHERE is_published = 1 ORDER BY name ', $objClientDatabase );
	}

	public static function fetchApExportBatchTypesByIds( $arrintIds, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_export_batch_types
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchApExportBatchTypes( $strSql, $objClientDatabase );
	}

}
?>