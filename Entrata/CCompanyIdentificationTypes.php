<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyIdentificationTypes
 * Do not add any new functions to this class.
 */

class CCompanyIdentificationTypes extends CBaseCompanyIdentificationTypes {

	public static function fetchDuplicateCompanyIdentificationTypeByNameByCid( $strName, $intCompanyidentificationTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							*
					 FROM
					 		company_identification_types
					WHERE
							cid = ' . ( int ) $intCid;

		if( 0 < $intCompanyidentificationTypeId ) {
			$strSql .= ' AND id != ' . ( int ) $intCompanyidentificationTypeId;
		}

		$strSql .= ' AND name ILIKE	\'' . addslashes( trim( $strName ) ) . '\' LIMIT 1';

		return self::fetchCompanyIdentificationType( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyIdentificationTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolReturnCount = false ) {

		$strSql = 'SELECT
						cit.*
					FROM
						company_identification_types as cit
						JOIN property_identification_types as pit ON( cit.id = pit.company_identification_type_id AND cit.cid = pit.cid )
					WHERE
						cit.cid = ' . ( int ) $intCid . '
						AND pit.property_id = ' . ( int ) $intPropertyId . '
						AND cit.is_published = 1 Order By cit.system_code asc';

		if( false == $boolReturnCount ) {
			return self::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
		} else {

			$arrmixValues = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrmixValues ) ) return \Psi\Libraries\UtilFunctions\count( $arrmixValues );

			return 0;
		}
	}

	public static function fetchUnassociatedCompanyIdentificationTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cit.*
					FROM
						company_identification_types as cit
						LEFT OUTER JOIN property_identification_types as pit ON ( cit.id = pit.company_identification_type_id AND cit.cid = pit.cid AND pit.property_id = ' . ( int ) $intPropertyId . ' AND pit.cid = ' . ( int ) $intCid . ' )
					WHERE
						pit.id IS NULL
						AND	cit.cid = ' . ( int ) $intCid . '
						AND cit.is_published = 1';

		return self::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
	}

	public static function fetchCompanyIdentificationTypeBySystemCodeByCid( $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT
							*
					 FROM
					 		company_identification_types
					WHERE
							cid = ' . ( int ) $intCid . '
							AND system_code ILIKE \'' . addslashes( $strSystemCode ) . '\'
							LIMIT 1';

		return self::fetchCompanyIdentificationType( $strSql, $objDatabase );
	}

	public static function fetchCompanyIdentificationTypeByNameByCid( $strName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
							*
					 FROM
					 		company_identification_types
					WHERE
							cid = ' . ( int ) $intCid . '
							AND name ILIKE \'' . addslashes( $strName ) . '\'
							LIMIT 1';

		return self::fetchCompanyIdentificationType( $strSql, $objDatabase );
	}

	public static function fetchCompanyIdentificationTypeNameByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						name
					 FROM
					 	company_identification_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}

	public static function fetchActiveCompanyIdentificationTypesByNamesByCid( $arrstrCompanyIdentificationTypes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrCompanyIdentificationTypes ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_identification_types
					WHERE
						LOWER( name ) IN (\'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrstrCompanyIdentificationTypes ) ) . '\')
						AND cid = ' . ( int ) $intCid . '
						AND is_published =1';

		return self::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyIdentificationTypesByPropertyIdByCidByName( $intPropertyId, $intCid, $strName, $objDatabase, $boolReturnCount = false ) {

		$strSql = '	SELECT
						cit.*
				   	FROM
						company_identification_types as cit
						INNER JOIN property_identification_types as pit ON( cit.id = pit.company_identification_type_id AND cit.cid = pit.cid )
				   	WHERE
				   		cit.cid = ' . ( int ) $intCid . '
				   		AND pit.property_id = ' . ( int ) $intPropertyId . '
				   		AND cit.name = \'' . addslashes( $strName ) . '\'
				   		AND cit.is_published = 1 Order By cit.system_code asc';

		if( false == $boolReturnCount ) {
			return self::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
		} else {

			$arrmixValues = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrmixValues ) ) return \Psi\Libraries\UtilFunctions\count( $arrmixValues );

			return 0;
		}
	}

	public static function fetchCompanyIdentificationTypesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) )		return [];

		$strSql = 'SELECT * FROM company_identification_types WHERE id IN ( ' . implode( ',', $arrintIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyIdentificationTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return [];

		$strSql = 'SELECT
						cit.*,
						pit.property_id
					FROM
						company_identification_types as cit
						JOIN property_identification_types as pit ON( cit.id = pit.company_identification_type_id AND cit.cid = pit.cid )
					WHERE
						cit.cid = ' . ( int ) $intCid . '
						AND pit.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cit.is_published = 1 ORDER BY cit.system_code asc';

		$arrmixValues = fetchData( $strSql, $objDatabase );
		return $arrmixValues;
	}

	public static function fetchCompanyIdentificationTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						cit.*
					FROM
						company_identification_types as cit
						JOIN property_identification_types as pit ON( cit.cid = pit.cid AND cit.id = pit.company_identification_type_id )
				   	WHERE
				   		cit.cid = ' . ( int ) $intCid . '
				   		AND pit.property_id = ' . ( int ) $intPropertyId . '
				   		AND cit.is_published = 1';

		return parent::fetchCompanyIdentificationTypes( $strSql, $objDatabase );
	}

}
?>