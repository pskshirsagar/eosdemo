<?php

class CAncillaryPlanPromoDetail extends CBaseAncillaryPlanPromoDetail {

	protected $m_strLocation;
	protected $m_strVendorName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAncillaryVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalPlanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlanName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPromoTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPromoDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['location'] ) ) $this->setLocation( $arrmixValues['location'] );
		if( true == isset( $arrmixValues['vendor_name'] ) ) $this->setVendorName( $arrmixValues['vendor_name'] );

		return;
	}

	public function setLocation( $strLocation ) {
		return $this->m_strLocation = $strLocation;
	}

	public function setVendorName( $strVendorName ) {
		return $this->m_strVendorName = $strVendorName;
	}

	public function getLocation() {
		return $this->m_strLocation;
	}

	public function getVendorName() {
		return $this->m_strVendorName;
	}

}
?>