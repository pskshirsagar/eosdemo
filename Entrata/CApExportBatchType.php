<?php

class CApExportBatchType extends CBaseApExportBatchType {

	const QUICK_BOOKS                = 1;
	const SKYLINE                    = 2;
	const TIMBERLINE_PM              = 3;
	const PEOPLESOFT                 = 4;
	const MRI                        = 5;
	const JENARK                     = 6;
	const YARDI                      = 7;
	const ONESITE                    = 8;
	const AMSI                       = 9;
	const PEAK                       = 10;
	const JD_EDWARDS                 = 11;
	const YARDI_ETL_INVOICE_REGISTER = 12;
	const YARDI_ETL_FINPAYABLES      = 13;

	public static $c_arrintYardiExportFormatTypes   = [ self::YARDI, self::YARDI_ETL_INVOICE_REGISTER, self::YARDI_ETL_FINPAYABLES ];

	public function fetchCustomApExportBatchTypeNameById( $intApExportBatchTypeId ) {

		if( false == valId( $intApExportBatchTypeId ) ) {
			return NULL;
		}

		switch( $intApExportBatchTypeId ) {
			case CApExportBatchType::TIMBERLINE_PM:
				$strApExportType = 'TimberlinePm';
				break;

			case CApExportBatchType::PEOPLESOFT:
				$strApExportType = 'PeopleSoft';
				break;

			case CApExportBatchType::YARDI:
				$strApExportType = 'Yardi';
				break;

			case CApExportBatchType::YARDI_ETL_INVOICE_REGISTER:
				$strApExportType = 'YardiEtl_InvoiceRegister';
				break;

			case CApExportBatchType::YARDI_ETL_FINPAYABLES:
				$strApExportType = 'Yardi_ETL_FinPayables';
				break;

			default:
				$strApExportType = 'ApExport';
				break;
		}
		return $strApExportType;
	}

}
?>