<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferTemplates
 * Do not add any new functions to this class.
 */

class COfferTemplates extends CBaseOfferTemplates {

	public static function fetchOfferTemplatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolReturnArray = false, $intArCascadeId = NULL, $intOfferTemplateId = NULL ) {

		$strOfferTemplateIdCondition = NULL;
		if( true == valId( $intOfferTemplateId ) ) {
			$strOfferTemplateIdCondition = ' AND ot.id = ' . $intOfferTemplateId . ' ';
			$strUnitTypeCondition = NULL;
		} elseif( $intArCascadeId == CArCascade::UNIT_TYPE ) {
			$strUnitTypeCondition = ' AND ot.unit_type_ids IS NOT NULL';
		} else {
			$strUnitTypeCondition = ' AND ot.unit_type_ids IS NULL';
		}

		$strSql = 'SELECT
						ot.id AS "offer_templates.id",
						ot.name AS "offer_templates.name",
						ot.description AS "offer_templates.description",
						ot.lease_interval_type_id AS "offer_templates.lease_interval_type_id",
						ot.move_out_start_date AS "offer_templates.move_out_start_date",
						ot.move_out_end_date AS "offer_templates.move_out_end_date",
						ot.offer_template_type_id AS "offer_templates.offer_template_type_id",
						ot.occupancy_type_id AS "offer_templates.occupancy_type_id",
						ot.application_stage_status_id AS "offer_templates.application_stage_status_id",
						ot.unit_type_ids AS "offer_templates.unit_type_ids",
						ot.unit_type_ids AS "offer_templates.unit_types",
						ot.lease_start_window_ids AS "offer_templates.lease_start_window_ids",
						o.id AS "offers.id",
						oi.name AS "offers.name",
						o.days_offset AS "offers.days_offset",
						o.start_date AS "offers.start_date",
						o.end_date AS "offers.end_date",
						o.max_offer_count AS "offers.max_offer_count",
						r.rate_amount AS "offers.rate_amount",
						r.ar_code_id AS ar_code_id,
						at.name AS "offers.trigger_name",
						at.id AS ar_trigger_id,
						s.id AS "offers.ar_origin_reference_id",
						s.special_type_id AS "offers.special_type_id",
						s.quantity_remaining AS "offers.quantity_remaining",
						COALESCE( (
							SELECT o1.days_offset
							FROM offers o1
							WHERE o1.cid = o.cid AND
								  o1.offer_template_id = o.offer_template_id AND
								  o1.deleted_by IS NULL AND
								  o1.days_offset < o.days_offset
							ORDER BY o1.days_offset DESC
							LIMIT 1
						 ) + 1, 0) AS "offers.min_days_to_lease_start",
						oi.name AS "offer_items.name",
						oi.description AS "offer_items.description",
						oi.offer_id AS "offer_items.offer_id",
						oi.id AS "offer_items.id",
						s.name AS "offers.name",
						s.special_type_id AS "offer_items.special_type_id",
						s.special_type_id
					FROM
						offer_templates ot
						JOIN offers o ON ( ot.id = o.offer_template_id AND ot.cid = o.cid AND o.deleted_by IS NULL )
						LEFT JOIN offer_items oi ON ( o.id = oi.offer_id AND o.cid = oi.cid AND oi.deleted_by IS NULL )
						LEFT JOIN specials s ON ( oi.ar_origin_reference_id = s.id AND oi.cid = s.cid )
						LEFT JOIN special_types st ON ( st.id = s.special_type_id )
						LEFT JOIN rates r ON( r.cid = s.cid AND r.ar_origin_reference_id = s.id AND 0 != r.rate_amount )
                        LEFT JOIN ar_triggers at ON ( at.id = r.ar_trigger_id )
					WHERE
						ot.cid = ' . ( int ) $intCid . '
						AND ot.is_published = true
						AND ot.property_id = ' . ( int ) $intPropertyId . '
						' . $strUnitTypeCondition . '
						' . $strOfferTemplateIdCondition . '
						AND ot.deleted_by IS NULL
                    ORDER BY
						o.days_offset desc,o.id,oi.name';

		if( true === $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}
		return self::fetchOfferTemplates( $strSql, $objDatabase, false );
	}

	public static function fetchActiveOfferTemplatesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false === valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT ot.*
 					FROM offer_templates ot
 						JOIN offers o ON ( o.cid = ot.cid AND o.offer_template_id = ot.id )
 					WHERE ot.cid = ' . ( int ) $intCid . ' AND
 					      ot.is_published = true AND
 					      o.deleted_by IS NULL AND
 					      ot.deleted_by IS NULL AND
 					      ot.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )  AND
 					      ot.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '';

		return self::fetchOfferTemplates( $strSql, $objDatabase );
	}

	public static function fetchOfferTemplatePublishedByOfferTemplateTypeIdByLeaseIntervalTypeIdByArCascadeIdByPropertyIdByCid( $intLeaseIntervalTypeId, $intArCascadeId, $intPropertyId, $intCid, $objDatabase, $strMoveOutStartDate, $strMoveOutEndDate, $intOfferTemplateId = NULL, $strOfferTemplateType = NULL ) {

		$strCondition			= ' AND ot.offer_template_type_id IS NULL';
		$strUnitTypeCondition	= NULL;

		if( true == valStr( $strOfferTemplateType ) ) {
			$strCondition = ' CASE 
							  WHEN ' . ( int ) $intArCascadeId == CArCascade::PROPERTY . '
							  AND ot.offer_template_type_id IS NULL 
							  THEN ot.offer_template_type_id = \'' . $strOfferTemplateType . '\'
							  ELSE TRUE';
		}

		if( true == valId( $intOfferTemplateId ) ) {
			$strCondition .= ' AND ot.id <> ' . ( int ) $intOfferTemplateId;
		}

		if( true == valStr( $strMoveOutStartDate ) && true == valStr( $strMoveOutEndDate ) ) {
			$strCondition .= ' AND ( \'' . $strMoveOutStartDate . '\'::DATE BETWEEN  ot.move_out_start_date AND ot.move_out_end_date  OR \'' . $strMoveOutEndDate . '\'::DATE BETWEEN  ot.move_out_start_date AND ot.move_out_end_date )';
		}

		if( $intArCascadeId == CArCascade::UNIT_TYPE ) {
			$strUnitTypeCondition = ' AND ot.unit_type_ids IS NOT NULL';
		} else {
			$strUnitTypeCondition = ' AND ot.unit_type_ids IS NULL';
		}

		$strSql = 'SELECT 
 						count( DISTINCT ot.id)
 					FROM
 						offer_templates ot
                         JOIN offers o ON( ot.cid = o.cid AND ot.id = o.offer_template_id AND o.deleted_by IS NULL )
                         JOIN offer_items oi ON( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
                         JOIN specials s ON( s.cid = o.cid AND s.id = oi.ar_origin_reference_id )
                         JOIN rate_associations ra ON( oi.cid = ra.cid AND oi.ar_origin_reference_id = ra.ar_origin_reference_id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ')
 					WHERE
 						ot.cid = ' . ( int ) $intCid . '
 						AND ot.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
 						AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . '
 						AND ot.is_published IS TRUE
 						AND ot.deleted_by IS NULL
 						AND ot.deleted_on IS NULL
 						' . $strUnitTypeCondition . '
 						AND ra.property_id = ' . ( int ) $intPropertyId . '
 						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
 						AND ra.ar_cascade_reference_id = ot.property_id
 						AND s.deleted_by IS NULL' . $strCondition;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveOfferTemplateByPropertyIdByCidByArCascadeId( $intPropertyId, $intCid, $intArCascadeId, $strMoveOutStartDate, $strMoveOutEndDate, $arrintUnitTypeIds, $objDatabase, $intOfferTemplateId, $intLeaseIntervalTypeId ) {

		$strJoinCondition = $strParameters = $strDateCondition = $strUnitTypeCondition = NULL;

		if( true == valArr( $arrintUnitTypeIds ) ) {
			$strUnitTypeCondition = 'AND ot.unit_type_ids IN (  ARRAY [' . sqlIntImplode( $arrintUnitTypeIds ) . ']::INT[] )';
		}

		if( CArCascade::UNIT_TYPE == $intArCascadeId ) {
			$strJoinCondition = 'JOIN unit_types ut ON ( ut.cid = ra.cid AND ut.id = ra.ar_cascade_reference_id )';
			$strParameters = ', ut.name AS unit_type_name';
		}

		if( CSpecialRecipient::RENEWALS == $intLeaseIntervalTypeId ) {
			$strDateCondition = ' AND ( \'' . $strMoveOutStartDate . '\'::DATE BETWEEN ot.move_out_start_date AND ot.move_out_end_date
 						OR \'' . $strMoveOutEndDate . '\'::DATE BETWEEN ot.move_out_start_date AND ot.move_out_end_date )';
		}

		$strSql = ' SELECT
 						DISTINCT ON ( ot.cid, ot.id, ra.ar_cascade_reference_id ) ot.*,
 						ra.ar_cascade_reference_id AS unit_type_id
 						' . $strParameters . '
 					FROM
 						offer_templates ot
						JOIN offers o ON( ot.cid = o.cid AND ot.id = o.offer_template_id AND o.deleted_by IS NULL )
						JOIN offer_items oi ON( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
 						JOIN rate_associations ra ON ( ra.cid = o.cid AND ra.ar_origin_reference_id = oi.ar_origin_reference_id AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . ' )
 						' . $strJoinCondition . '
 					WHERE
 						ot.property_id = ' . ( int ) $intPropertyId . '
 						AND ot.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
 						' . $strUnitTypeCondition . '
 						AND ot.cid = ' . ( int ) $intCid . '
 						AND ot.deleted_by IS NULL
 						AND ot.deleted_on IS NULL
 						AND ot.move_out_start_date IS NOT NULL' . ( true == valId( $intOfferTemplateId ) ? ' AND ot.id <> ' . ( int ) $intOfferTemplateId : '' );

		return fetchData( $strSql, $objDatabase );
	}

}
?>
