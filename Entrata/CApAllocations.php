<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApAllocations
 * Do not add any new functions to this class.
 */

class CApAllocations extends CBaseApAllocations {

	public static function fetchNormalAndPrePaidApAllocationsByApDetailIdByCid( $intApDetailId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						aa.*
					FROM
						ap_details ad
						JOIN ap_allocations aa ON ( ad.cid = aa.cid AND ( ad.id = aa.charge_ap_detail_id OR ad.id = aa.credit_ap_detail_id ) )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ad.id = ' . ( int ) $intApDetailId . '
						AND aa.is_deleted = false';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	/*
     * @Todo change the function name to fetchApAllocationsByChargeApDetailIdsOrCreditApDetailIdsByIsDeletedByCid
	 */

	public static function fetchApAllocationsByCreditApDetailIdsByIsDeletedByCid( $arrintCreditApDetailIds, $boolIsDeleted, $intCid, $objClientDatabase, $boolIsCrossAllocation = false ) {

		$strWhereCondition = '';

		if( false == valArr( $arrintCreditApDetailIds ) ) {
			return NULL;
		}

		if( true == $boolIsCrossAllocation ) {
			$strWhereCondition = '( credit_ap_detail_id IN ( ' . implode( ',', $arrintCreditApDetailIds ) . ' ) OR charge_ap_detail_id IN ( ' . implode( ',', $arrintCreditApDetailIds ) . ' ) )';
		} else {
			$strWhereCondition = 'credit_ap_detail_id IN ( ' . implode( ',', $arrintCreditApDetailIds ) . ' )';
		}

		if( true == $boolIsDeleted ) {
			$strWhereCondition .= ' AND origin_ap_allocation_id IS NULL
									AND is_deleted = true ';
		} else {
			$strWhereCondition .= ' AND is_deleted = false ';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_allocations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ' . $strWhereCondition . '  
					ORDER BY
						charge_ap_detail_id
					ASC';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationsByLumpApHeaderIdsByIsDeletedByCid( $arrintLumpApHeaderIds, $boolIsDeleted, $intCid, $objClientDatabase ) {

		$strWhereCondition = '';

		if( false == valArr( $arrintLumpApHeaderIds ) ) {
			return NULL;
		}

		if( true == $boolIsDeleted ) {
			$strWhereCondition = ' AND origin_ap_allocation_id IS NULL
									AND is_deleted = true ';
		} else {
			$strWhereCondition = 'AND is_deleted = false ';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_allocations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lump_ap_header_id IN ( ' . implode( ',', $arrintLumpApHeaderIds ) . ' )'
						. $strWhereCondition . '  
					ORDER BY
						charge_ap_detail_id
					ASC';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationsForCreditMemoByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( aa.* )
					FROM
						ap_allocations aa
						JOIN ap_details ad ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( aa.charge_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) OR aa.credit_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) )
						AND ad.reversal_ap_detail_id IS NULL
						AND aa.is_deleted = false';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchPaymentApAllocationsForCreditMemoByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = '( SELECT
						aa.id,
						aa.charge_ap_detail_id,
						aa.credit_ap_detail_id,
						cah.id AS lump_ap_header_id
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						LEFT JOIN ap_allocations aa ON ( ad.cid = aa.cid AND ( ad.id = aa.charge_ap_detail_id OR ad.id = aa.credit_ap_detail_id ) )
						LEFT JOIN ap_details cad ON ( aa.cid = cad.cid AND aa.credit_ap_detail_id = cad.id )
						LEFT JOIN ap_headers cah ON ( cad.cid = cah.cid AND cad.ap_header_id = cah.id AND cah.reversal_ap_header_id IS NULL AND cah.gl_transaction_type_id = cad.gl_transaction_type_id AND cah.post_month = cad.post_month )
						LEFT JOIN ap_payments ap ON ( cah.cid = ap.cid AND cah.ap_payment_id = ap.id AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED . ' )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( aa.charge_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) OR aa.credit_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) )
						AND aa.is_deleted = false
						AND ah.reversal_ap_header_id IS NULL
						AND ah.ap_payment_id IS NOT NULL
					ORDER BY
						cah.id,
						aa.charge_ap_detail_id )

					UNION

					( SELECT
						aa.id,
						aa.charge_ap_detail_id,
						aa.credit_ap_detail_id,
						aa.lump_ap_header_id
					FROM
						ap_allocations aa
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( aa.charge_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) OR aa.credit_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' ) )
						AND aa.lump_ap_header_id IS NOT NULL
						AND aa.is_deleted = false
					ORDER BY
						aa.lump_ap_header_id,
						aa.charge_ap_detail_id )';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationsByApDetailIdByCid( $intApDetailId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						aa.id,
						aa.charge_ap_detail_id,
						aa.credit_ap_detail_id,
						aa.allocation_datetime,
						aa.post_date,
						aa.post_month,
						aa.allocation_amount,
						\'Charge header number : \' || COALESCE( ah_charge.header_number, \'\' ) || \' <br> Credit header number : \' || COALESCE( ah_credit.header_number, \'\' ) AS header_number,
						ap_credit.payment_number
					FROM
						ap_allocations aa
						LEFT JOIN ap_details ad_charge ON ( ad_charge.cid = aa.cid ANd ad_charge.id = aa.charge_ap_detail_id )
						LEFT JOIN ap_headers ah_charge ON ( ad_charge.cid = ah_charge.cid ANd ad_charge.ap_header_id = ah_charge.id )
						LEFT JOIN ap_details ad_credit ON ( ad_credit.cid = aa.cid ANd ad_credit.id = aa.credit_ap_detail_id )
						LEFT JOIN ap_headers ah_credit ON ( ad_credit.cid = ah_credit.cid ANd ad_credit.ap_header_id = ah_credit.id )
						LEFT JOIN ap_payments ap_credit ON ( ap_credit.cid = ah_credit.cid AND ap_credit.id = ah_credit.ap_payment_id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( aa.charge_ap_detail_id = ' . ( int ) $intApDetailId . ' OR aa.credit_ap_detail_id = ' . ( int ) $intApDetailId . ' )
						AND aa.is_deleted = false';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationsByLumpApHeaderIdsByApDetailIdsByCid( $arrintApHeaderIds, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) || false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_allocations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( charge_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
							  OR credit_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
							  OR lump_ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' ) )
					ORDER BY
						id';

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						aa.*
					FROM
						ap_allocations aa
						JOIN ap_details ad ON( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) )
						JOIN ap_headers ah ON( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id =' . ( int ) $intApPayeeId;

		return self::fetchApAllocations( $strSql, $objClientDatabase );
	}

	public static function fetchApAllocationCountByPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM ( sub.details_count ) AS details_count,
						SUM ( sub.summary_count ) AS summary_count,
						sub.ap_payment_id
					FROM
						(
							SELECT
								count ( DISTINCT aa.id ) AS details_count,
								count ( DISTINCT charge_ad.ap_header_id ) AS summary_count,
								ap.id AS ap_payment_id
							FROM
								ap_payments ap
								LEFT JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id )
								LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
								JOIN ap_allocations aa ON ( ad.cid = aa.cid AND ( ad.id = aa.charge_ap_detail_id OR ad.id = aa.credit_ap_detail_id OR aa.lump_ap_header_id = ah.id ) AND aa.is_deleted IS FALSE AND aa.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
								LEFT JOIN ap_details charge_ad ON ( charge_ad.cid = aa.cid AND charge_ad.id = aa.charge_ap_detail_id )
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
							GROUP BY
								ap.id,
								charge_ad.ap_header_id,
								ad.property_id
						) AS sub
					GROUP BY
						sub.ap_payment_id
					ORDER BY
						sub.ap_payment_id';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>
