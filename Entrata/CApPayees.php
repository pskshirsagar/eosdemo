<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayees
 * Do not add any new functions to this class.
 */

class CApPayees extends CBaseApPayees {

	public static function fetchIntegratedApPayeesByApPayeeTypeIdsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $arrintApPayeeTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN integration_databases id ON ( id.cid = ap.cid AND id.id = ap.integration_database_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ap.ap_payee_type_id IN ( ' . implode( ', ', $arrintApPayeeTypeIds ) . ' )
						AND ap.remote_primary_key IS NOT NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM ap_payees WHERE cid = ' . ( int ) $intCid . ' AND LOWER ( remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) trim( $strRemotePrimaryKey ) ) ) . '\'';
		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeByRemotePrimaryKeyByIntegrationDatabaseIdByCid( $strRemotePrimaryKey, $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND LOWER ( remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) trim( $strRemotePrimaryKey ) ) ) . '\'';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByRemotePrimaryKeysByIntegrationDatabaseIdByCid( $arrstrRemotePrimaryKeys, $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrstrRemotePrimaryKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND LOWER( remote_primary_key ) IN( \'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrstrRemotePrimaryKeys ) ) . '\')';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	/**
	 * @param boolean       $boolAdminUser
	 * @param integer       $intCid
	 * @param array         $arrintPermissionedPropertyGroupIds
	 * @param string        $strAlias
	 * @param boolean       $boolSelectLocations
	 */
	public static function getApPayeeLocationSqlByPropertyGroupIdsByCidByIsAdminUser( $boolAdminUser, $intCid, $arrintPermissionedPropertyGroupIds, string $strAlias, $boolSelectLocations = true, $boolIsPropertyFilterSelected ) {

		if( false == valArr( $arrintPermissionedPropertyGroupIds ) ) {
			throw new Exception( 'Error. You must pass permissioned property group IDs when calling this function' );
		}

		if( '' == $strAlias ) {
			throw new Exception( 'Error. You must specify table alias for ap payee location ID or ap payee ID when calling this method.' );
		}

		if( true == $boolSelectLocations ) {
			$strSelect  = 'ap_payee_location_id';
		} else {
			$strSelect = 'ap_payee_id';
		}

		if( true == $boolAdminUser && false == $boolIsPropertyFilterSelected ) {
			$strWhereSql = '';
		} else {

			if( false == valArr( $arrintPermissionedPropertyGroupIds ) ) {
				throw new Exception( 'Error. You must specify table alias for ap payee location ID or ap payee ID when calling this method.' );
			}

			/** @TODO change 100000000 to a constant, probably on CPropertyGroup but not sure what to call it */

			$strWhereSql = '
				AND ' . $strAlias . ' IN (
					SELECT
						' . $strSelect . '
					FROM
						ap_payee_property_groups appg
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_group_id = 100000000 + ' . ( int ) $intCid . '

					UNION ALL

					SELECT
						' . $strSelect . '
					FROM
						ap_payee_property_groups appg
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_group_id <> 100000000 + ' . ( int ) $intCid . '
						AND NOT EXISTS(
							SELECT
								cid, ap_payee_id
							FROM
								ap_payee_property_groups sub
							WHERE
								sub.cid = ' . ( int ) $intCid . '
								AND sub.property_group_id = 100000000 + ' . ( int ) $intCid . '
								AND sub.cid = appg.cid AND sub.ap_payee_id = appg.ap_payee_id
						)
						AND EXISTS (
							SELECT 1
							FROM
								property_group_associations pga
								JOIN property_groups pg ON pg.cid = pga.cid AND pg.id = pga.property_group_id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL
							WHERE
								pga.cid = appg.cid
								AND pga.property_group_id = appg.property_group_id
								AND pga.property_id IN( ' . implode( ',', $arrintPermissionedPropertyGroupIds ) . ' )
						)
					GROUP BY
						' . $strSelect . '
				)';
		}

		return $strWhereSql;
	}

	public static function fetchPaginatedApPayeesByApPayeeSearchByPropertyIdsByCid( $arrintPropertyIds, $objApPayeeFilter, $objPagination, $intCid, $objClientDatabase, $boolAdminUser, $boolReturnSql = false ) {

		if( false == $boolAdminUser && false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = '';
		$boolIsPropertyFilterSelected = false;

		$strOrderBy		= ' ap.ap_payee_status_type_id, ap.company_name ASC';

		if( true == valStr( $objApPayeeFilter->getSortBy() ) && true == in_array( $objApPayeeFilter->getSortBy(), [ 'company_name', 'ap_payee_status_type_id' ] ) ) {

			$strOrderBy = addslashes( $objApPayeeFilter->getSortBy() ) . ' ' . addslashes( $objApPayeeFilter->getSortDirection() ) . ', ap.company_name ' . addslashes( $objApPayeeFilter->getSortDirection() );

			if( 'company_name' == $objApPayeeFilter->getSortBy() ) {
				$strOrderBy = addslashes( $objApPayeeFilter->getSortBy() ) . ' ' . addslashes( $objApPayeeFilter->getSortDirection() );
			}
		}

		if( true == valStr( $objApPayeeFilter->getLocation() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_payee_locations apl WHERE apl.cid = ap.cid AND apl.ap_payee_id = ap.id
				AND apl.location_name ILIKE \'%' . addslashes( $objApPayeeFilter->getLocation() ) . '%\' ) ';
		}

		if( true == valStr( $objApPayeeFilter->getVendorCode() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_payee_locations apl WHERE apl.cid = ap.cid AND apl.ap_payee_id = ap.id
				AND apl.vendor_code ILIKE \'%' . addslashes( $objApPayeeFilter->getVendorCode() ) . '%\' ) ';
		}

		if( true == valStr( $objApPayeeFilter->getContactName() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_payee_contacts apc WHERE apc.cid = ap.cid AND apc.ap_payee_id = ap.id
				AND ( apc.name_first ILIKE \'%' . addslashes( $objApPayeeFilter->getContactName() ) . '%\' OR apc.name_last ILIKE \'%' . addslashes( $objApPayeeFilter->getContactName() ) . '%\' ) ) ';
		}

		if( true == valStr( $objApPayeeFilter->getAddress() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_payee_locations apl WHERE apl.cid = ap.cid AND apl.ap_payee_id = ap.id
				AND ( apl.street_line1 ILIKE \'%' . addslashes( $objApPayeeFilter->getAddress() ) . '%\'
						OR apl.street_line2 ILIKE \'%' . addslashes( $objApPayeeFilter->getAddress() ) . '%\'
						OR apl.city ILIKE \'%' . addslashes( $objApPayeeFilter->getAddress() ) . '%\'
						OR apl.state_code ILIKE \'%' . addslashes( $objApPayeeFilter->getAddress() ) . '%\'
						OR apl.postal_code ILIKE \'%' . addslashes( $objApPayeeFilter->getAddress() ) . '%\' ) ) ';
		}

		if( true == valStr( $objApPayeeFilter->getEmail() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_payee_contacts apc WHERE apc.cid = ap.cid AND apc.ap_payee_id = ap.id
				AND apc.email_address ILIKE \'%' . addslashes( $objApPayeeFilter->getEmail() ) . '%\' ) ';
		}

		if( true == valStr( $objApPayeeFilter->getSecondaryNumber() ) ) {
			$strWhereSql	.= ' AND ap.secondary_number ILIKE \'%' . addslashes( $objApPayeeFilter->getSecondaryNumber() ) . '%\' ';
		}

		if( true == valStr( $objApPayeeFilter->getTaxpayerIdNumberEncrypted() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_legal_entities ale WHERE ale.cid = ap.cid AND ale.ap_payee_id = ap.id
				AND ale.tax_number_encrypted = ' . $objApPayeeFilter->sqlTaxpayerIdNumberEncrypted() . ' AND ale.deleted_by IS NULL AND ale.deleted_on IS NULL ) ';
		}

		if( true == valStr( $objApPayeeFilter->getName() ) ) {
			$strWhereSql	.= ' AND EXISTS ( SELECT 1 FROM ap_legal_entities ale WHERE ale.cid = ap.cid AND ale.ap_payee_id = ap.id AND ale.deleted_by IS NULL AND ale.deleted_on IS NULL
				AND ( ap.company_name ILIKE \'%' . addslashes( $objApPayeeFilter->getName() ) . '%\' OR ale.entity_name ILIKE \'%' . addslashes( $objApPayeeFilter->getName() ) . '%\' ) ) ';
		}

		if( true == valArr( $objApPayeeFilter->getApPayeeTypeIds() ) ) {
			$strWhereSql .= ' AND ap.ap_payee_type_id IN ( ' . implode( ',', $objApPayeeFilter->getApPayeeTypeIds() ) . ' )';
		} else {
			$strWhereSql .= ' AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )';
		}

		if( true == valArr( $objApPayeeFilter->getApPayeeStatusTypeIds() ) ) {
			$strWhereSql .= ' AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $objApPayeeFilter->getApPayeeStatusTypeIds() ) . ' )';
		}

		if( true == valArr( $objApPayeeFilter->getPropertyGroupIds() ) ) {
			$arrintFilterPropertyGroupIds = $objApPayeeFilter->getPropertyGroupIds();
			$boolIsPropertyFilterSelected = true;
		} else {
			$arrintFilterPropertyGroupIds = $arrintPropertyIds;
		}

		$strWhereSql .= self::getApPayeeLocationSqlByPropertyGroupIdsByCidByIsAdminUser( $boolAdminUser, $intCid, $arrintFilterPropertyGroupIds, 'ap.id', false, $boolIsPropertyFilterSelected );

		$strSql = '
			SELECT DISTINCT
				ap.id,
				ap.ap_payee_status_type_id,
				ap.company_name,
				apl.phone_number,
				apl.email_address,
				apl.street_line1,
				apl.street_line2,
				apl.city,
				apl.state_code,
				apl.postal_code,
				apl.vendor_code,
				apl.country_code
			FROM
				ap_payees ap
				JOIN ap_payee_locations apl ON ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.is_primary = true
			WHERE
				ap.cid = ' . ( int ) $intCid . '
				' . $strWhereSql . '
			ORDER BY
				' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolReturnSql ) {
			return $strSql;
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalApPayeesCountByCidByApPayeeSearchByPropertyIds( $intCid, $arrintPropertyIds, $objApPayeeFilter, $objClientDatabase, $boolIsAdmin ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT count( * ) FROM ( ' . self::fetchPaginatedApPayeesByApPayeeSearchByPropertyIdsByCid( $arrintPropertyIds, $objApPayeeFilter, NULL, $intCid, $objClientDatabase, $boolIsAdmin, true ) . ' ) sub';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPaginatedVendorsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolIsRequestToDownload = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == is_null( $objScheduledEmailFilter->getPageNo() ) && false == is_null( $objScheduledEmailFilter->getCountPerPage() ) ) {
			$intOffset = ( 0 < $objScheduledEmailFilter->getPageNo() ) ? $objScheduledEmailFilter->getCountPerPage() * ( $objScheduledEmailFilter->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objScheduledEmailFilter->getCountPerPage();
		}

		$strWhere = '';
		if( 99 != $objScheduledEmailFilter->getStatusTypes() ) {
			$strWhere = ' AND ap.ap_payee_status_type_id IN( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
		}

		if( true == valId( $objScheduledEmailFilter->getIsExcludeRecipient() ) && 1 == $objScheduledEmailFilter->getIsExcludeRecipient() ) {
			$strWhere .= ' AND apc.email_address IS NOT NULL ';
		}

		$arrstrOrderBy = [
			'vendor_name'	=> 'ap.company_name',
			'vendor_code'	=> 'apl.vendor_code',
			'email_address'	=> 'apc.email_address',
			'vendor_status'	=> 'apst.name',
			'location_name'	=> 'apl.location_name',
			'contact_name'	=> 'contact_name'
		];

		$strSortBy			= ( true == isset( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) ? addslashes( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) : $objDatabase->getCollateSort( 'apl.location_name' ) );
		$strSortDirection	= ( false == is_null( $objScheduledEmailFilter->getSortDirection() ) ) ? $objScheduledEmailFilter->getSortDirection() : 'ASC';

		$strSql = 'SELECT
						DISTINCT ON( pga.property_id, ap.id, apl.id, apc.id, ' . $strSortBy . ' )
						ap.id,
						apcl.id AS ap_payee_contact_location_id,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apl.vendor_code,
						apl.location_name,
						apst.id AS vendor_status_id,
						apst.name AS vendor_status,
						apc.email_address,
						CONCAT( apc.name_last, \' \', apc.name_first ) AS contact_name,
						apc.name_first,
						apc.name_last,
						apc.id as recipient_id,
						p.property_name
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON ( apst.id = ap.ap_payee_status_type_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apl.id = apcl.ap_payee_location_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.id = apcl.ap_payee_contact_id AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
						LEFT JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ap.ap_payee_type_id IN( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL
						' . $strWhere;

		if( false == is_null( $strSortBy ) && false == is_null( $strSortDirection ) ) {
			$strSql .= ' ORDER BY ' . $strSortBy . ' ' . addslashes( $strSortDirection );
		}

		if( false == $boolIsRequestToDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleRecipientsByScheduledEmailFilterByCid( $objScheduledEmailFilter, $intCid, $objDatabase, $strSelectedFields ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valStr( $strSelectedFields ) ) return NULL;
		switch( $strSelectedFields ) {
			case 'recipients':
				$strSelectColumns = 'apcl.id AS recipients';
				break;

			case 'count':
				$strSelectColumns = 'COUNT ( apcl.id )';
				break;

			default:
				return NULL;
		}

		$strWhere = '';
		if( 99 != $objScheduledEmailFilter->getStatusTypes() ) {
			$strWhere = ' AND ap.ap_payee_status_type_id IN( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
		}

		if( true == valId( $objScheduledEmailFilter->getIsExcludeRecipient() ) && 1 == $objScheduledEmailFilter->getIsExcludeRecipient() ) {
			$strWhere .= ' AND apc.email_address IS NOT NULL ';
		}

		if( false == valStr( $strSelectColumns ) ) return NULL;

		$strSql = '	SELECT
						DISTINCT ON( pga.property_id, ap.id, apl.id, apc.id )
						' . $strSelectColumns . '
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON ( apst.id = ap.ap_payee_status_type_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apl.id = apcl.ap_payee_location_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.id = apcl.ap_payee_contact_id AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . $objScheduledEmailFilter->getProperties() . ' )
						AND ap.ap_payee_type_id IN( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL
						' . $strWhere;

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		switch( $strSelectedFields ) {
			case 'recipients':
				$arrmixAllRecipients = [];
				foreach( $arrmixResponseData as $arrmixReponse ) {
					$arrmixAllRecipients[] = $arrmixReponse['recipients'];
				}
				return $arrmixAllRecipients;

			case 'count':
				if( true == isset( $arrmixResponseData[0]['count'] ) )
					return $arrmixResponseData[0]['count'];

				return 0;

			default:
				return NULL;
		}

	}

	public static function fetchApPayeesByScheduledEmailFilterByCid( $objScheduledEmailFilter, $intCid, $objDatabase, $boolIsRestrictBlockedRecipients = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;
		$strJoin = '';

		if( 99 != $objScheduledEmailFilter->getStatusTypes() ) {
			$strWhere = ' AND ap.ap_payee_status_type_id IN( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
		}

		if( true == $boolIsRestrictBlockedRecipients ) {
			$strJoin = ' LEFT JOIN LATERAL(
											SELECT id FROM 
											(
												SELECT
													id,
													regexp_split_to_array ( blocked_recipients, \',\' ) AS blocked_recipients
												FROM
													scheduled_email_filters
												WHERE 
													id = ' . ( int ) $objScheduledEmailFilter->getId() . '
											 ) AS blocked_recipient_lists
											 WHERE
													apcl.id::TEXT = ANY( blocked_recipient_lists.blocked_recipients )
										) AS sef ON ( true )';

			$strWhere .= ' AND sef.id IS NULL AND apc.email_address IS NOT NULL';
		}

		$strSql = 'SELECT
						DISTINCT ON( pga.property_id, ap.id, apl.id, apc.id )
						ap.id,
						apcl.id AS ap_payee_contact_location_id,
						apc.id AS ap_payee_contact_id,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apst.name as ap_payee_status_type,
						ap.company_name,
						apc.email_address,
						apl.location_name,
						apc.name_last,
						apc.name_first,
						apc.phone_number,
						pga.property_id,
						apc.created_on,
						p.property_name
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON ( apst.id = ap.ap_payee_status_type_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apl.id = apcl.ap_payee_location_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.id = apcl.ap_payee_contact_id AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
						' . $strJoin . '
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . $objScheduledEmailFilter->getProperties() . ' )
						AND ap.ap_payee_type_id IN( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL
						' . $strWhere;

		return self::fetchApPayees( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeeWithContactDetailsByScheduedEmailFilterByIdByCid( $intApPayeeContactId, $objScheduledEmailFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		if( 99 != $objScheduledEmailFilter->getStatusTypes() ) {
			$strWhere = ' AND ap.ap_payee_status_type_id IN( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
		}

		$strSql = 'SELECT
						DISTINCT ON( pga.property_id, ap.id, apl.id, apc.id )
						ap.id,
						apc.id AS ap_payee_contact_id,
						apcl.id AS ap_payee_contact_location_id,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apst.name as ap_payee_status_type,
						ap.company_name,
						apc.email_address,
						apl.location_name,
						apc.name_last,
						apc.name_first,
						apc.phone_number,
						pga.property_id,
						apc.created_on,
						p.property_name
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON ( apst.id = ap.ap_payee_status_type_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apl.id = apcl.ap_payee_location_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.id = apcl.ap_payee_contact_id AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND apc.id = ' . ( int ) $intApPayeeContactId . '
						AND pga.property_id IN ( ' . $objScheduledEmailFilter->getProperties() . ' )
						AND ap.ap_payee_type_id IN( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL
						' . $strWhere;

		return self::fetchApPayee( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeesByApPayeeTypeIdByCid( $intApPayeeTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.*,
						CASE
							WHEN apl.payee_name IS NULL THEN
								ap.company_name
							ELSE
								apl.payee_name
						END AS payee_name
					FROM
						ap_payees ap
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id = ' . ( int ) $intApPayeeTypeId . '
					ORDER BY
						CASE
							WHEN apl.payee_name IS NULL THEN LOWER( ap.company_name )
							ELSE LOWER( apl.payee_name )
						END';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintIds = array_filter( ( array ) $arrintIds ) ) ) return NULL;
		return self::fetchApPayees( 'SELECT * FROM ap_payees WHERE id IN ( ' . implode( ',', $arrintIds ) . ' ) AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchApPayeesByCid( $intCid, $objClientDatabase ) {

		if( true == is_null( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
						ar.ap_payment_type_id
					FROM
						ap_payees ap
						LEFT JOIN ap_remittances ar ON ( ap.cid = ar.cid AND ap.default_ap_remittance_id = ar.id )
					WHERE
						ap.cid = ' . ( int ) $intCid;

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByCidByApPayeeTypeIdByApPayeeStatusTypeIds( $intCid, $intApPayeeTypeId, $arrintApPayeeStatusTypeIds, $objClientDatabase, $arrmixApPayeesInfo ) {
		$strCondition = '';

		if( false == valArr( $arrintApPayeeStatusTypeIds ) ) return NULL;

		if( false == valArr( $arrmixApPayeesInfo ) ) return NULL;

		if( true == valArr( $arrmixApPayeesInfo ) ) {
			$strCondition = 'AND ( apl.vendor_code IN ( \'' . trim( implode( '\',\'', array_map( 'addslashes', ( array ) $arrmixApPayeesInfo['vendor_code'] ) ) ) . '\' )
			 OR ( ap.company_name IN ( \'' . trim( implode( '\',\'', array_map( 'addslashes', ( array ) $arrmixApPayeesInfo['vendor_business_name'] ) ) ) . '\' ) ) )';
		}

		$strSql = 'SELECT
						ap.id,
						ap.cid,
						ap.company_name,
						ap.secondary_number
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id = ' . ( int ) $intApPayeeTypeId . '
						AND	ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )
						AND ap.company_name IS NOT NULL
						' . $strCondition . '
					GROUP BY
						ap.id,
						ap.cid,
						ap.company_name,
						ap.secondary_number
					ORDER BY
						LOWER( ap.company_name )';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeeByIdByCid( $intId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
					WHERE
						ap.id = ' . ( int ) $intId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchVendorsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ap.*,
						ap.company_name,
						apst.name as ap_payee_status_type
					FROM
						ap_payees ap
						LEFT JOIN ap_payee_property_groups appg ON ( ap.cid = appg.cid AND ap.id = appg.ap_payee_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ap.ap_payee_type_id IN (' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ')';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByPropertyIdsByApPayeeTypeIdsByApPayeeTypeIdsByCid( $arrintPropertyIds, $arrintApPayeeTypeIds, $intCid, $objClientDatabase, $boolIsActiveOnly = false, $boolIsForMrb = NULL ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintApPayeeTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ap.*,
						ap.company_name,
						apst.name as ap_payee_status_type
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON( ap.ap_payee_status_type_id = apst.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
						AND EXISTS (

								SELECT
									DISTINCT ON ( appg.cid, appg.ap_payee_id )
									appg.cid, appg.ap_payee_id
								FROM
									ap_payee_property_groups appg
									JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
									JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
								WHERE
									appg.cid = ' . ( int ) $intCid . '
									AND appg.ap_payee_id = ap.id
									AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						) ';

		if( true == $boolIsActiveOnly ) {
			$strSql .= ' AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE;
		}

		$strSql .= ' ORDER BY
						ap.company_name';

		if( true == $boolIsForMrb ) {
			return fetchData( $strSql, $objClientDatabase );
		} else {
			return self::fetchApPayees( $strSql, $objClientDatabase );
		}
	}

	public static function fetchPaginatedApPayeesPropertyNamesByPropertyidsByApPayeeIdByCid( $arrintPropertyIds, $intCid, $intApPayeeId, $objPagination, $objClientDatabase, $intApLegalEntityId = NULL, $arrintAllowedProperties = NULL ) {

		$strSql = 'SELECT
 						DISTINCT p.id,
 						p.property_name
 					FROM
 						ap_payee_property_groups appg
 						JOIN ap_legal_entities ale ON ( appg.cid = ale.cid AND appg.ap_payee_id = ale.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
 						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
 						JOIN properties p ON ( p.cid = pga.cid AND pga.property_id = p.id )
 					WHERE
 						appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
 						AND appg.cid = ' . ( int ) $intCid . '
 						AND p.is_disabled = 0
 						AND p.is_managerial = 0
 						AND ale.deleted_by IS NULL
 						AND ale.deleted_on IS NULL';

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND pga.property_id NOT IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( false == is_null( $intApLegalEntityId ) ) {
			$strSql .= ' AND ale.id = ' . ( int ) $intApLegalEntityId;
		}

		if( true == valArr( $arrintAllowedProperties ) ) {
			$strSql .= ' AND pga.property_id IN ( ' . implode( ',', $arrintAllowedProperties ) . ' )';
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesCountByPropertyidsByApPayeeIdByCid( $arrintPropertyIds, $intCid, $intApPayeeId, $objClientDatabase, $intApLegalEntityId = NULL, $arrintAllowedProperties = NULL ) {

		$strSql = 'SELECT
 						count( DISTINCT pga.property_id ) as count
 					FROM
 						ap_payee_property_groups appg
 						JOIN ap_legal_entities ale ON ( appg.cid = ale.cid AND appg.ap_payee_id = ale.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
 						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
 						JOIN properties p ON ( p.cid = pga.cid AND pga.property_id = p.id )
 					WHERE
 						appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
 						AND appg.cid = ' . ( int ) $intCid . '
 						AND p.is_disabled = 0
 						AND p.is_managerial = 0
 						AND ale.deleted_by IS NULL
 						AND ale.deleted_on IS NULL';

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND pga.property_id NOT IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( false == is_null( $intApLegalEntityId ) ) {
			$strSql .= ' AND ale.id = ' . ( int ) $intApLegalEntityId . '';
		}

		if( true == valArr( $arrintAllowedProperties ) ) {
			$strSql .= ' AND pga.property_id IN ( ' . implode( ',', $arrintAllowedProperties ) . ' )';
		}

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApPayeesByApPayeeTypeIdsByApPayeeStatusTypeIdsByPropertyIdsByCid( $arrintApPayeeTypeIds, $arrintApPayeeStatusTypeIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeTypeIds ) || false == valArr( $arrintApPayeeStatusTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ap.*
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( ap.cid = appg.cid AND ap.id = appg.ap_payee_id )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
						AND	ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )
						AND appg.property_group_id IN ( SELECT
															DISTINCT property_group_id
														FROM
															property_group_associations WHERE cid = ' . ( int ) $intCid . '
															AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeByIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase, $boolIsPrimary = false ) {

		$strFromClause		= '';
		$strWhereCondition	= '';

		if( true == $boolIsPrimary ) {
			$strFromClause		.= ' JOIN ap_payee_contacts apc ON ( apc.cid = apl.cid AND apc.ap_payee_id = ap.id )
									 JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id AND apcl.ap_payee_location_id = apl.id ) ';
			$strWhereCondition	.= ' AND apcl.is_primary = TRUE ';
		}

		$strSql = 'SELECT
						ap.*,
						apl.vendor_code,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.location_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id ) ' . $strFromClause . '
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intApPayeeId . '
						AND apl.id = ' . ( int ) $intApPayeeLocationId . $strWhereCondition;

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchWarrantyTypeApPayeeByIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						LEFT JOIN ap_payee_contacts apc ON ap.cid = apc.cid AND ap.id = apc.ap_payee_id
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intApPayeeId . '
						AND apc.is_warranty = TRUE';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByIdsByApPayeeStatusTypeIdsByCid( $arrintApPayeeIds, $arrintApPayeeStatusTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApPayeeStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND	ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByCidByFilter( $intCid, $objClientDatabase, $objApPayeeFilter = NULL ) {

		$arrstrConditons = [];

		$arrstrConditons[] = '1 = 1';

		if( true == valObj( $objApPayeeFilter, 'CApPayeeFilter' ) ) {

			if( false == is_null( $objApPayeeFilter->getPageName() ) ) {
				$strConditon = 'LOWER (company_name) ';
				$strConditon .= ( '#' == $objApPayeeFilter->getPageName() ) ? '!~ \'^[a-z A-Z]\'' : 'ILIKE \'' . $objApPayeeFilter->getPageName() . '%\'';
				$arrstrConditons[] = $strConditon;
			}

			if( true == valArr( $objApPayeeFilter->getApPayeeIds() ) ) {
				$arrstrConditons[] = 'id IN (' . implode( ',', $objApPayeeFilter->getApPayeeIds() ) . ')';
			}
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrConditons ) . '
					ORDER BY
						ap_payee_status_type_id,
						LOWER ( company_name ) ASC';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByApPayeeByAcccountNumberByCid( $strAccountNumber, $intCid, $objClientDatabase ) {

		$strJoin = 'LEFT JOIN';

		if( false == is_null( $strAccountNumber ) ) $strJoin = 'JOIN';

		$strSql = 'SELECT
						ap.*,
						apl.payee_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						' . $strJoin . ' ap_payee_accounts apa ON( apa.ap_payee_id = ap.id AND apa.cid = ap.cid )
					WHERE
						ap.cid= ' . ( int ) $intCid . '
						AND apa.is_disabled IS FALSE';

		if( false == is_null( $strAccountNumber ) )		$strSql .= ' AND upper( apa.account_number ) = upper( \'' . addslashes( $strAccountNumber ) . '\' )';

		$strSql .= ' ORDER BY
						payee_name';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
						CASE
							WHEN apl.payee_name IS NULL THEN
								ap.company_name
							ELSE
								apl.payee_name
						END AS payee_name,
						apl.id as ap_payee_location_id
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						LEFT JOIN ap_payee_accounts apa ON ( apa.cid = ap.cid AND apa.ap_payee_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintIds ) . ' )
					ORDER BY
						payee_name';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByIdsByCids( $arrintIds, $arrintCids, $objClientDatabase ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintCids ) ) return NULL;
		return self::fetchApPayees( 'SELECT *	FROM ap_payees WHERE id IN ( ' . implode( ',', $arrintIds ) . ' ) AND cid IN ( ' . implode( ',', $arrintCids ) . ' )', $objClientDatabase );
	}

	public static function fetchApPayeeNamesByApPayeeTypeIdsByCid( $arrintApPayeeTypeIds, $intCid, $objClientDatabase, $boolForMrb = false, $boolHideOwnerVendors = false ) {

		if( false == valArr( $arrintApPayeeTypeIds ) ) return 0;

		$strSelect 	= 'ap.id, ap.company_name AS company_name';
		$strJoins 	= 'ap_payees ap';
		$strOrderBy = 'LOWER( ap.company_name )';

		if( true == $boolForMrb ) {
			$strSelect	= 'DISTINCT LOWER( apl.payee_name ) AS payee_name, ' . $strSelect;
			$strJoins	= 'ap_payees ap
							LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )';
			$strOrderBy = 'LOWER( apl.payee_name )';
		}

		$strWhere 	= ( true == $boolHideOwnerVendors ) ? ' AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::PROPERTY_SOLUTIONS . ', ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::LENDER . ' )' : ' AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )';

		$strSql 	= 'SELECT
							' . $strSelect . '
						FROM
							' . $strJoins . '
						WHERE
							ap.cid = ' . ( int ) $intCid . $strWhere . '
						ORDER BY
							' . $strOrderBy;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByApPayeeTypeIdByCid( $intApPayeeTypeId, $intCid, $objClientDatabase, $intApPayeeId = NULL ) {

		$strWhereCondition = ' AND ( ap.ap_payee_type_id = ' . ( int ) ( ( true == valId( $intApPayeeTypeId ) ) ? $intApPayeeTypeId : CApPayeeType::STANDARD ) . ' OR ap.ap_payee_type_id = ' . ( int ) CApPayeeType::INTERCOMPANY . ' ) ';

		if( true == valId( $intApPayeeId ) ) {
			$strWhereCondition .= ' AND ap.id <>' . $intApPayeeId;
		}

		$strSql = 'SELECT
						ap.*,
						apl.city,
						apl.state_code,
						apl.location_name,
						apl.id as ap_payee_location_id,
						apl.vendor_code
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
					WHERE
						ap.cid =' . ( int ) $intCid
						. $strWhereCondition . '
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
					ORDER BY
						LOWER( ap.company_name )';

		return self::fetchApPayees( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveApPayeeByIdByApPayeeTypeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeTypeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeId ) || false == is_numeric( $intApPayeeTypeId ) || false == is_numeric( $intApPayeeLocationId ) ) return 0;

		$strSql = 'SELECT
						ap.*,
						apl.city,
						apl.state_code,
						apl.location_name,
						apl.id AS ap_payee_location_id,
						apl.vendor_code
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.id =' . ( int ) $intApPayeeId . '
						AND ap.ap_payee_type_id =' . ( int ) $intApPayeeTypeId . '
						AND apl.id =' . ( int ) $intApPayeeLocationId . '
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeeByManagementFeeIdByCid( $intManagementFeeId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intManagementFeeId ) ) return 0;

		$strSql = 'SELECT
						ap.*,
						apc.city,
						apc.state_code,
						apl.location_name,
						apl.vendor_code,
						ah.post_date,
						ah.post_month,
						ah.ap_routing_tag_id
					FROM
						ap_payees ap
						JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN fees f ON ( ad.cid = f.cid AND ad.fee_id = f.id )
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apcl.ap_payee_location_id = apl.id )
						JOIN ap_payee_contacts apc ON ( apc.cid = apcl.cid AND apc.id = apcl.ap_payee_contact_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND f.fee_type_id = ' . CFeeType::MANAGEMENT_FEES . '
						AND f.id = ' . ( int ) $intManagementFeeId;

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByIdsByApPayeeLocationsIdByCid( $arrintApPayeeIds, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApPayeeLocationIds ) ) return 0;

		$strSql = 'SELECT
						ap.*,
						apc.city,
						apc.state_code,
						apl.location_name,
						apl.vendor_code,
						apl.id AS ap_payee_location_id,
						apt.days,
						apt.system_code,
						apcl.ap_payee_contact_id,
						ale.receives_1099
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apcl.ap_payee_location_id = apl.id )
						JOIN ap_payee_contacts apc ON ( apc.cid = apcl.cid AND apc.id = apcl.ap_payee_contact_id )
						LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
						JOIN ap_payee_terms apt ON ( apt.cid = ap.cid AND ap.ap_payee_term_id = apt.id )
						JOIN ap_legal_entities ale ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id AND ale.id = apl.ap_legal_entity_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveApPayeesFor1099ByCidByFilter( $intCid, $objClientDatabase, $objApPayeeFilter = NULL, $boolIncludeAllApPayee = false, $boolIsFromPrint1099Forms = false, $boolIsFromeFile = false ) {

		$strStartDate					= NULL;
		$strEndDate						= NULL;
		$strPropertyIds					= NULL;
		$strOffsetLimit					= NULL;
		$strOrderBy						= '';
		$strOwnersJoin					= '';
		$strAmountsJoin					= '';
		$strCreditCardCondition			= '';
		$strOwnerPropertySelect			= '';
		$strOwnerPropertyEntityJoin		= '';
		$strOwnerPropertyRecordSelect	= '';
		$strOwnerCondition              = '';
		$strOwnerField                  = '';
		$strOuterFieldName				= ', SUM ( payment_records.payment_amount ) OVER ( PARTITION BY payment_records.property_id, payment_records.id, payment_records.ap_legal_entity_id ) AS payment_amount';
		$strFieldNames					= 'COALESCE ( apa.amount, 0 ) AS payment_amount';
		$strDistinctOnField				= 'DISTINCT ON ( rr.ap_legal_entity_id, rr.property_id ) ap_legal_entity_id,';
		$strDistinctOnForPrint          = ' DISTINCT ON( payment_records.ap_legal_entity_id, payment_records.property_id, payment_records.owner_id ) payment_records.ap_legal_entity_id,';
		$strAmountColumn				= ' SUM ( rr.payment_amount ) OVER ( PARTITION BY rr.property_id, rr.id, rr.ap_legal_entity_id ) AS payment_amount';

		$arrintPropertyIds				= [];
		$arrstrConditions				= [ '1 = 1' ];
		$arrstrPreviousBalanceConditions	= [ '1 = 1' ];

		$strPaymentRecordConditionForPrint = '';

		if( true == valObj( $objApPayeeFilter, 'CApPayeeFilter' ) ) {

			if( 0 == $objApPayeeFilter->getIsFromSaveAndPrint() && false == is_null( $objApPayeeFilter->getPageName() ) && true == is_null( $objApPayeeFilter->getApPayeesCount() ) && false == $boolIsFromeFile ) {

				if( false == $boolIncludeAllApPayee ) {
					if( '#' == $objApPayeeFilter->getPageName() ) {
						$strConditon = 'LOWER ( app.company_name ) !~ \'^[a-z A-Z]\'';
					} else {
						$strConditon = '( LOWER ( app.company_name ) <= \'' . $objApPayeeFilter->getPageName() . '%\' OR app.company_name ILIKE \'' . $objApPayeeFilter->getPageName() . '%\' )';
					}

					$arrstrConditions[]               = $strConditon;
					$arrstrPreviousBalanceConditions[] = $strConditon;
				}
			}

			if( true == valArr( $objApPayeeFilter->getApPayeeIds() ) ) {
				$arrstrConditions[] = 'app.id IN (' . implode( ',', $objApPayeeFilter->getApPayeeIds() ) . ')';
				$arrstrPreviousBalanceConditions[] = 'app.id IN (' . implode( ',', $objApPayeeFilter->getApPayeeIds() ) . ')';
			}

			if( true == $objApPayeeFilter->getIsFrom1099View() && true == valArr( $objApPayeeFilter->getApLegalEntityIds() ) ) {

				$arrstrConditions[] = 'ale.id IN (' . implode( ',', $objApPayeeFilter->getApLegalEntityIds() ) . ')';
				$arrstrPreviousBalanceConditions[] = 'ale.id IN (' . implode( ',', $objApPayeeFilter->getApLegalEntityIds() ) . ')';
			}

			if( true == valArr( $objApPayeeFilter->getPropertyIds() ) ) {
				$strPropertyIds		= 'AND apa.property_id IN (' . implode( ',', $objApPayeeFilter->getPropertyIds() ) . ')';
				$arrstrConditions[]	= 'ad.property_id IN (' . implode( ',', $objApPayeeFilter->getPropertyIds() ) . ')';
				$arrintPropertyIds	= ( array ) $objApPayeeFilter->getPropertyIds();
			}

			if( 1 == $objApPayeeFilter->getIsCurrentTaxYear() ) {
				$strStartDate	= date( 'm/d/Y', strtotime( '01/01/' . date( 'Y' ) ) );
				$strEndDate		= date( 'm/d/Y', strtotime( '12/31/' . date( 'Y' ) ) );
			} else {
				$strStartDate	= date( 'm/d/Y', strtotime( '01/01/' . date( 'Y', strtotime( '-1 year' ) ) ) );
				$strEndDate		= date( 'm/d/Y', strtotime( '12/31/' . date( 'Y', strtotime( '-1 year' ) ) ) );
			}

			if( false != valStr( $objApPayeeFilter->getStartDate() ) ) {
				$strStartDate = $objApPayeeFilter->getStartDate();
			}

			if( false != valStr( $objApPayeeFilter->getEndDate() ) ) {
				$strEndDate = $objApPayeeFilter->getEndDate();
			}

			// Check form type is 1099 NEC
			if( 1 == $objApPayeeFilter->getIsFromSaveAndPrint() ) {
				$arrstrConditions[] = 'ale.form_1099_type_id = ' . CForm1099Type::FORM_1099_TYPE_8;
			}

			if( true == $objApPayeeFilter->getIsFromSelectAll() ) {
				$strOffsetLimit = ' LIMIT 1000';
			} elseif( false == is_null( $objApPayeeFilter->getApPayeesCount() ) && false == $boolIncludeAllApPayee ) {
				$strOffsetLimit = ' OFFSET ' . ( int ) ( $objApPayeeFilter->getApPayeesCount() ) . ' LIMIT ' . ( int ) $objApPayeeFilter->getPageLimit();
			} elseif( false == valStr( $objApPayeeFilter->getPageName() ) && false == $boolIncludeAllApPayee && false == $boolIsFromeFile && false == $boolIsFromPrint1099Forms ) {
				$strOffsetLimit = ' OFFSET ' . ( int ) ( $objApPayeeFilter->getPageNo() * $objApPayeeFilter->getPageLimit() ) . ' LIMIT ' . ( int ) $objApPayeeFilter->getPageLimit();
			}

			$strAmountsJoin = ' JOIN ap_payee_1099_adjustments apa ON ( app.cid = apa.cid AND apa.ap_legal_entity_id = ale.id AND app.id = apa.ap_payee_id ' . $strPropertyIds . ' AND apa.year = \'' . $strStartDate . '\' )';

			if( true == $objApPayeeFilter->getIsFromPopupContinue() || true == $boolIsFromPrint1099Forms || true == $boolIsFromeFile || true == $objApPayeeFilter->getIsFrom1099View() ) {
				$strOwnerPropertySelect = ' o.owner_name, p.property_name, o.id as owner_id, ale.entity_name as entity_name, p.id as property_id,';
				$strOwnerPropertyRecordSelect	= ' payment_records.owner_name, payment_records.property_name, payment_records.owner_id , payment_records.entity_name,';
				$strOwnersJoin					= ' JOIN owners o ON ( p.cid = o.cid AND p.owner_id = o.id ) ';
				$strOwnerPropertyEntityJoin		= ' JOIN properties p ON ( p.cid = apa.cid AND p.id = apa.property_id )
													JOIN owners o ON ( p.cid = o.cid AND p.owner_id = o.id ) ';
				$strCreditCardCondition 		= ' AND ap.ap_payment_type_id <> ' . CApPaymentType::CREDIT_CARD;
				$strOwnerCondition				= ' AND rr.owner_id = cc.owner_id ';
				$strOwnerField					= 'rr.owner_name, rr.owner_id, rr.property_name, rr.entity_name,';
			} else {
				$strOwnerPropertySelect = '0 as property_id, ';
			}

			if( true == $objApPayeeFilter->getIsFrom1099View() ) {
				$strFieldNames			= 'apa.amount AS payment_amount';
				$strOuterFieldName		= ', SUM ( payment_records.payment_amount ) OVER ( PARTITION BY payment_records.owner_id, payment_records.id, payment_records.ap_legal_entity_id ) AS payment_amount';
				$strAmountsJoin			= ' JOIN ap_payee_1099_adjustments apa ON ( app.cid = apa.cid AND apa.ap_legal_entity_id = ale.id AND app.id = apa.ap_payee_id ' . $strPropertyIds . ' AND apa.year = \'' . $strStartDate . '\' )';
				$strDistinctOnField		= ' DISTINCT ON ( CASE
												WHEN cc.owner_count > 1 THEN ( rr.ap_legal_entity_id )::VARCHAR
												ELSE ( rr.ap_legal_entity_id, rr.property_id )::VARCHAR
												END ) ap_legal_entity_id,';
				$strAmountColumn		= ' CASE
												WHEN cc.owner_count > 1 THEN SUM ( rr.payment_amount ) OVER ( PARTITION BY rr.id, rr.ap_legal_entity_id, rr.owner_id )
												ELSE SUM ( rr.payment_amount ) OVER ( PARTITION BY rr.property_id, rr.id, rr.ap_legal_entity_id )
												END AS payment_amount';
				$strDistinctOnForPrint  = ' DISTINCT ON( payment_records.ap_legal_entity_id, payment_records.owner_id ) payment_records.ap_legal_entity_id,';
			}

			if( true == valStr( $objApPayeeFilter->getSortBy() ) && true == in_array( $objApPayeeFilter->getSortBy(), [ 'property_name', 'owner_name', 'entity_name', 'company_name', 'form_1099_type_id', 'form_1099_box_type_id', 'payment_amount' ] ) ) {
				$strOrderBy .= ' ORDER BY ';
				if( ( true == $objApPayeeFilter->getIsFromPopupContinue() || true == $boolIsFromPrint1099Forms || true == $boolIsFromeFile ) && false == in_array( $objApPayeeFilter->getSortBy(), [ 'property_name', 'owner_name' ] ) ) {
					if( true == $objApPayeeFilter->getIsLegalEntity() ) {
						$strOrderBy .= ' LOWER( owner_name ), ';
					} else {
						$strOrderBy .= ' LOWER( property_name ), ';
					}
				}

				$strOrderBy .= addslashes( $objApPayeeFilter->getSortBy() ) . ' ' . addslashes( $objApPayeeFilter->getSortDirection() ) . $strOffsetLimit;

			} else {
				$strOrderBy .= ' ORDER BY';
				if( true == $objApPayeeFilter->getIsFromPopupContinue() || true == $boolIsFromPrint1099Forms || true == $boolIsFromeFile ) {
					if( true == $objApPayeeFilter->getIsLegalEntity() ) {
						$strOrderBy .= ' LOWER( owner_name ), LOWER( company_name ) ' . $strOffsetLimit;
					} else {
						$strOrderBy .= ' LOWER( property_name ), LOWER( company_name ) ' . $strOffsetLimit;
					}
				} else {
					$strOrderBy .= ' ap_legal_entity_id,
							LOWER( company_name ) ' . $strOffsetLimit;
				}
			}

			// While printing forms need to check the combination of selected ap_payees and property_ids
			if( ( true == $boolIsFromPrint1099Forms || true == $boolIsFromeFile ) && true == valArr( $objApPayeeFilter->getApPayeePropertyIds() ) ) {
				if( true == $objApPayeeFilter->getIsLegalEntity() ) {
					$strPaymentRecordConditionForPrint = ' WHERE ( payment_records.id, payment_records.ap_legal_entity_id, payment_records.owner_id ) IN ( ( ' . implode( '),(', $objApPayeeFilter->getApPayeePropertyIds() ) . ' ) )';
				} else {
					$strPaymentRecordConditionForPrint = ' WHERE ( payment_records.id, payment_records.ap_legal_entity_id, payment_records.property_id ) IN ( ( ' . implode( '),(', $objApPayeeFilter->getApPayeePropertyIds() ) . ' ) )';
				}
			}

		}

		if( false == valIntArr( $arrintPropertyIds ) ) return NULL;
// @TODO - check if we need DISTINCT ON

		$strSql = 'WITH ';

		if( false == $boolIsFromPrint1099Forms ) {
			$strSql .= 'owner_prop_count AS ( 
					SELECT
						p.id,
						p.cid,
						p.owner_id,
						count ( p.id ) over ( PARTITION BY p.owner_id ) AS owner_count
					FROM
					    properties p
					    JOIN owners o ON ( p.cid = o.cid AND p.owner_id = o.id )
					WHERE
					    p.cid = ' . ( int ) $intCid . '
					    AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ),';

			$strOuterFieldName = ', payment_records.payment_amount';
			$strDistinctOnForPrint = 'payment_records.ap_legal_entity_id,';
		}

		$strSql .= ' review_1099_records AS ( SELECT
						' . $strDistinctOnForPrint . '
						payment_records.id,
						payment_records.cid,
						payment_records.company_name,
						payment_records.property_id,
						' . $strOwnerPropertyRecordSelect . '
						payment_records.ap_payee_type_id,
						payment_records.form_1099_box_type_id,
						payment_records.form_1099_type_id,
						payment_records.receives_1099' . $strOuterFieldName;

		$strSql .= ' FROM
						(
							(
								SELECT
									DISTINCT ale.id AS ap_legal_entity_id,
									app.id,
									app.cid,
									app.company_name,
									' . $strOwnerPropertySelect . ' 
									app.ap_payee_type_id,
									ale.form_1099_box_type_id,
									ale.form_1099_type_id,
									ale.receives_1099,
									SUM ( aa.allocation_amount ) OVER(PARTITION BY aa.property_id, ah_payments.ap_payee_id, ale.id  ) * - 1 AS payment_amount';

		$strSql .= ' FROM
									ap_payees app
									JOIN ap_headers ah ON ( app.cid = ah.cid AND app.id = ah.ap_payee_id AND ah.ap_payment_id IS NULL )
									JOIN ap_legal_entities ale ON ( ale.cid = app.cid AND ale.ap_payee_id = app.id )
									JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND ale.id = apl.ap_legal_entity_id )
									JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.is_1099 = true )
									JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
									' . $strOwnersJoin . '
									JOIN ap_allocations aa ON ( ad.cid = aa.cid AND ad.id = aa.charge_ap_detail_id )
									JOIN ap_details ad_payments ON ( ad.cid = ad_payments.cid AND aa.credit_ap_detail_id = ad_payments.id )
									JOIN ap_headers ah_payments ON ( ad.cid = ah_payments.cid AND ad_payments.ap_header_id = ah_payments.id )
									JOIN ap_payments ap ON ( ad.cid = ap.cid AND ah_payments.ap_payment_id = ap.id AND ap.payment_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\')
								WHERE
									app.cid = ' . ( int ) $intCid . '
									AND ale.receives_1099 = true
									AND app.ap_payee_type_id <> ' . CApPayeeType::PROPERTY_SOLUTIONS .
									' AND ah.ap_payee_location_id = apl.id ' . $strCreditCardCondition . '
									AND ' . implode( ' AND ', $arrstrConditions ) . '
									AND ale.deleted_by IS NULL
									AND ale.deleted_on IS NULL';

				$strSql .= ' )
							UNION ALL
								(
									SELECT
										DISTINCT ale.id AS ap_legal_entity_id,
										app.id,
										app.cid,
										app.company_name,
										' . $strOwnerPropertySelect . '
										app.ap_payee_type_id,
										ale.form_1099_box_type_id,
										ale.form_1099_type_id,
										ale.receives_1099,'
										. $strFieldNames;

					$strSql .= ' FROM
										ap_payees app
										JOIN ap_legal_entities ale ON ( app.cid = ale.cid AND ale.ap_payee_id = app.id )
										LEFT JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND ale.id = apl.ap_legal_entity_id )
										' . $strAmountsJoin . '
										' . $strOwnerPropertyEntityJoin . '
									WHERE
										app.cid = ' . ( int ) $intCid . '
										AND ale.receives_1099 = true
										AND app.ap_payee_type_id <> ' . CApPayeeType::PROPERTY_SOLUTIONS . '
										AND ' . implode( ' AND ', $arrstrPreviousBalanceConditions ) . '
										AND apa.property_id IS NOT NULL
										AND ale.deleted_by IS NULL
										AND ale.deleted_on IS NULL';

					$strSql .= ' AND apa.amount >= 1';

				$strSql .= '				)
						) AS payment_records
						' . $strPaymentRecordConditionForPrint;

			$strSql .= ' )';

			if( false == $boolIsFromPrint1099Forms ) {
				$strSql .= ' , cte_records AS( SELECT
										 ' . $strDistinctOnField . '
										rr.id,
										rr.cid,
										rr.company_name,
										rr.property_id,
										' . $strOwnerField . '
										rr.ap_payee_type_id,
										rr.form_1099_box_type_id,
										rr.form_1099_type_id,
										rr.receives_1099,
										cc.owner_count,
										' . $strAmountColumn . '
								FROM
									review_1099_records rr
									JOIN owner_prop_count cc ON rr.cid = cc.cid ' . $strOwnerCondition . ' AND rr.property_id = cc.id )
 								SELECT
										*
								FROM
									cte_records';
			} else {

				$strSql .= ' SELECT
									*
							FROM
								review_1099_records';
			}

			$strSql .= ' WHERE
								cid = ' . ( int ) $intCid .
								' AND payment_amount >= ' . ( int ) $objApPayeeFilter->getIRSReportingThreshold() . $strOrderBy;
		if( true == $objApPayeeFilter->getIsLegalEntity() || true == $objApPayeeFilter->getIsFromPopupContinue() || true == $boolIsFromPrint1099Forms || true == $boolIsFromeFile ) {

			return self::fetchApPayees( $strSql, $objClientDatabase, false );
		}
		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedApPayeesByIdsByCids( $arrintApPayeeTypeIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeTypeIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN integration_databases id ON ( id.cid = ap.cid AND id.id = ap.integration_database_id )
					WHERE
						ap.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ap.id IN ( ' . implode( ', ', $arrintApPayeeTypeIds ) . ' )
						AND ap.remote_primary_key IS NOT NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedApPayeesByApPayeeTypeIdByCid( $arrintApPayeeTypeIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN integration_databases id ON ( id.cid = ap.cid AND id.id = ap.integration_database_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . implode( ', ', $arrintApPayeeTypeIds ) . ' )
						AND ap.remote_primary_key IS NOT NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByCidByIsServiceProvider( $intCid, $boolIsServiceProvider = false, $objClientDatabase, $intApPayeeId = NULL, $intVendorCategoryTypeId = NULL ) {

		$strAndCondition = ( true == valId( $intApPayeeId ) ) ? ' AND id = ' . ( int ) $intApPayeeId : '';
		$strAndCondition .= ( NULL != $intVendorCategoryTypeId ) ? ' AND vendor_category_type_id = ' . ( int ) $intVendorCategoryTypeId : '';
		$strAndCondition .= ( false == $boolIsServiceProvider ) ? ' AND is_on_site = false' : 'AND is_on_site = true';

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id IN ( ' . ( int ) CApPayeeStatusType::ACTIVE . ',' . ( int ) CApPayeeStatusType::INACTIVE . ' )
						AND ap.vendor_id IS NULL' . $strAndCondition;

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByCompanyNamesByCid( $arrstrCompanyNames, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrCompanyNames ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND LOWER ( company_name ) IN ( ' . sqlStrImplode( $arrstrCompanyNames ) . ' )';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByPropertyIdsByUserIdByCid( $arrintPropertyIds, $intUserId, $intCid, $objClientDatabase, $intApAllocationId = NULL, $intApPayeeId = NULL, $objPagination = NULL ) {

		if( ( false == valId( $intCid ) || false == valId( $intUserId ) || false == valIntArr( $arrintPropertyIds ) ) && false == valId( $intApAllocationId ) ) return NULL;

		$strPropertyCondition		= 'AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		$strApFormulaJoinCondition	= NULL;

		if( true == valId( $intApAllocationId ) ) {
			$strApFormulaJoinCondition	= 'JOIN ap_formula_properties AS afp ON afp.cid = pga.cid AND afp.property_id = pga.property_id
											JOIN ap_formulas AS af ON afp.cid = af.cid AND afp.ap_formula_id = af.id';
			$strPropertyCondition		= ' AND af.id = ' . $intApAllocationId;
		}

		$strApIdCondition = '';
		if( true == valId( $intApPayeeId ) ) {
			$strApIdCondition		= ' AND ap.id = ' . ( int ) $intApPayeeId;
		}

		// vendor_code - ap.company_name, apl.location_name, apa.account_number
		$strDisplayNameColumn = 'concat_ws( \' - \', apl.vendor_code, concat_ws( \', \', ap.company_name, apl.location_name, apa.account_number ) )';

		$strSearchCondition = '';
		$strLimitOffset = '';
		if( $objPagination instanceof CPagination ) {

			if( valStr( $objPagination->getQuickSearch() ) ) {
				$strSearchCondition = ' AND ' . $strDisplayNameColumn . ' ilike \'%' . html_entity_decode( pg_escape_string( $objPagination->getQuickSearch() ) ) . '%\' ';
			}

			if( is_integer( $objPagination->getPageSize() ) && is_integer( $objPagination->getOffset() ) ) {
				$strLimitOffset = ' LIMIT ' . ( int ) $objPagination->getPageSize() . ' OFFSET ' . ( int ) $objPagination->getOffset();
			}

		}

		$arrintApPayeeTypeIds	= [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER, CApPayeeType::OWNER ];

		$strSql = '
			WITH cte_unlocked_ap_payee_locations AS (
				SELECT
					cid, ap_payee_id, ap_payee_location_id
				FROM
					ap_payee_property_groups appg
				WHERE
					cid = ' . ( int ) $intCid . '
					AND property_group_id = 100000000 + cid
			),
			cte_restricted_ap_payee_locations AS (
				SELECT
					appg.cid, appg.ap_payee_id, appg.ap_payee_location_id
				FROM
					ap_payee_property_groups appg
				WHERE
					appg.cid = ' . ( int ) $intCid . '
					AND appg.property_group_id <> 100000000 + appg.cid
					AND NOT EXISTS( SELECT 1 FROM cte_unlocked_ap_payee_locations cte WHERE cte.cid = appg.cid AND cte.ap_payee_id = appg.ap_payee_id AND cte.ap_payee_location_id = appg.ap_payee_location_id )
					AND EXISTS (
						SELECT 1
						FROM
							property_group_associations pga
							' . $strApFormulaJoinCondition . '
						WHERE
							pga.cid = appg.cid
							AND pga.property_group_id = appg.property_group_id
							' . $strPropertyCondition . '
					)
				GROUP BY
					appg.cid, appg.ap_payee_id, appg.ap_payee_location_id
			)
			SELECT
				ap.*,
				apl.city,
				apl.state_code,
				apl.vendor_code,
				apl.location_name,
				' . $strDisplayNameColumn . ' AS display_name,
				apl.street_line1 AS ap_payee_street_line1,
				apl.street_line2 AS ap_payee_street_line2,
				apl.city AS ap_payee_city,
				apl.state_code AS ap_payee_state_code,
				apl.postal_code AS ap_payee_postal_code,
				apl.default_ap_routing_tag_id,
				apl.id AS ap_payee_location_id,
				apa.id AS ap_payee_account_id,
				apa.default_ap_remittance_id,
				apa.default_property_id,
				ar.id AS location_default_ap_remittance_id,
				apa.account_number AS ap_payee_account_number,
				gat.account_number || \' : \' || gat.name AS gl_account_name,
				ale.receives_1099,
				apa.default_gl_account_id AS account_default_gl_account_id,
				COALESCE( apa.default_gl_account_id, ap.gl_account_id ) AS gl_account_id,
				apa.account_number IS NULL AS is_dummy_account
			FROM
				ap_payees AS ap
				JOIN ap_payee_locations AS apl ON ap.cid = apl.cid AND ap.id = apl.ap_payee_id
				JOIN ap_payee_accounts AS apa ON apl.cid = apa.cid AND apl.ap_payee_id = apa.ap_payee_id AND apl.id = apa.ap_payee_location_id
				JOIN ap_legal_entities AS ale ON apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id
				LEFT JOIN gl_account_trees AS gat ON ap.cid = gat.cid AND ap.gl_account_id = gat.gl_account_id AND gat.is_default = 1
				LEFT JOIN ap_remittances AS ar ON apl.cid = ar.cid AND apl.id = ar.ap_payee_location_id AND ar.is_default AND ar.is_published
			WHERE
				ap.cid = ' . ( int ) $intCid . '
				AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
				AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
				AND apl.disabled_by IS NULL
				AND apl.deleted_by IS NULL
				AND apl.deleted_on IS NULL
				AND ale.deleted_by IS NULL
				AND ale.deleted_on IS NULL
				' . $strSearchCondition . '
				' . $strApIdCondition . '
				AND apa.is_disabled = false
				AND apl.id IN ( 
					SELECT ap_payee_location_id FROM cte_unlocked_ap_payee_locations
					UNION ALL
					SELECT ap_payee_location_id FROM cte_restricted_ap_payee_locations
				)
			/* the group by does not seem needed
			GROUP BY
				apl.id, apl.cid,
				ap.id, ap.cid,
				apa.id, apa.cid,
				ar.id, ar.cid,
				gat.id, gat.cid,
				ale.receives_1099
			*/
			ORDER BY
				display_name
				' . ( string ) $strLimitOffset . ';';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByIdsByApPayeeTypeIdByApPayeeLocationIdsByCid( $arrintApPayeeIds, $intApPayeeTypeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == is_numeric( $intApPayeeTypeId ) || false == valArr( $arrintApPayeeLocationIds ) ) return 0;

		$strSql = 'SELECT
						ap.*,
						apc.city,
						apc.state_code,
						apl.location_name,
						apl.id AS ap_payee_location_id,
						apt.days,
						apt.system_code,
						apcl.ap_payee_contact_id,
						apl.vendor_code,
						ale.receives_1099
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apcl.ap_payee_location_id = apl.id )
						JOIN ap_payee_contacts apc ON ( apc.cid = apcl.cid AND apc.id = apcl.ap_payee_contact_id )
						JOIN ap_payee_terms apt ON ( apt.cid = ap.cid AND ap.ap_payee_term_id = apt.id )
						JOIN ap_legal_entities ale ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id AND ale.id = apl.ap_legal_entity_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ap.ap_payee_type_id =' . ( int ) $intApPayeeTypeId . '
						AND apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND apl.disabled_by IS NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchVendorInfoDetailsByIdByCidByApPayeeTermIdByGlAccountId( $intApPayeeId, $intCid, $intApPayeeTermId, $intGlAccountId, $objClientDatabase ) {

		$strAndCondition	= NULL;

		$strAndCondition .= ( true == is_numeric( $intGlAccountId ) ) ? ' AND gat.gl_account_id = ' . ( int ) $intGlAccountId : ' AND gat.gl_account_id IS NULL';
		$strAndCondition .= ( true == is_numeric( $intApPayeeTermId ) ) ? ' AND apt.id = ' . ( int ) $intApPayeeTermId : ' AND apt.id IS NULL';

		$strSql = 'SELECT
						DISTINCT apt.id,
						apt.name AS payment_term_name,
						gat.account_number || \' : \'|| gat.name  AS default_gl_account_name,
						vct.name AS service_provided_name
					FROM
						ap_payees ap
						LEFT JOIN ap_payee_terms apt ON ( apt.cid = ap.cid AND apt.id = ap.ap_payee_term_id )
						LEFT JOIN gl_account_trees gat ON (  gat.cid = ap.cid AND gat.gl_account_id = ap.gl_account_id AND gat.is_default = 1  )
						LEFT JOIN vendor_category_types vct ON ( vct.id = ap.vendor_category_type_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.id =' . ( int ) $intApPayeeId . '
						AND gat.disabled_by IS NULL
						AND apt.deleted_on IS NULL' . $strAndCondition;

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		return array_pop( $arrintResponse );
	}

	public static function fetchApPayeeQuickViewDetailsByIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.website_url,
						apl.street_line1,
						apl.street_line2,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.country_code,
						
						apc.name_first,
						apc.name_last,
						apc.phone_number,
						apc.fax_number,
						apc.email_address,
						apc.extension,
						apc.is_verified
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.is_primary AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apcl.ap_payee_location_id = apl.id AND apcl.is_primary )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = apcl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apc.id = apcl.ap_payee_contact_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intApPayeeId;

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		return array_pop( $arrintResponse );
	}

	public static function fetchActiveApPayeesByCidByPropertyId( $intCid, $intPropertyId, $arrintUnclaimedPropertyApPayeeTypeIds, $objDatabase ) {

		$strAndCondition = '';

		if( false == is_numeric( $intPropertyId ) ) return NULL;

		if( true == valArr( $arrintUnclaimedPropertyApPayeeTypeIds ) ) {
			$strAndCondition = ' AND ap.ap_payee_type_id IN( ' . implode( ',', $arrintUnclaimedPropertyApPayeeTypeIds ) . ' )
								 ORDER BY
									ap.company_name';
		}

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( ap.id = appg.ap_payee_id AND ap.cid = appg.cid )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND pga.property_id = ' . ( int ) $intPropertyId . $strAndCondition;

		return self::fetchApPayees( $strSql, $objDatabase );

	}

	public static function fetchActiveApPayeesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid =' . ( int ) $intCid . '
						AND ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND ap_payee_type_id = ' . ( int ) CApPayeeType::INTERCOMPANY;

		return self::fetchApPayees( $strSql, $objDatabase );

	}

	public static function fetchActiveApPayeesByApPayeeStatusTypeIdsByApPayeeTypeIdsByCid( $arrintApPayeeStatusTypeIds, $arrintApPayeeTypeIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApPayeeStatusTypeIds ) || false == valIntArr( $arrintApPayeeTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id IN ( ' . sqlIntImplode( $arrintApPayeeStatusTypeIds ) . ')
						AND ap.ap_payee_type_id IN ( ' . sqlIntImplode( $arrintApPayeeTypeIds ) . ') ';

		return self::fetchApPayees( $strSql, $objDatabase );
	}

	public static function fetchApPayeesDetailsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ap.id, ap.company_name
					FROM
						ap_payees ap
					WHERE
   						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ';

		return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchApPayeeByCidByApPayeeTypeId( $intCid, $intApPayeeTypeId, $objDatabase, $boolIsForRefund = false ) {

		$strSql = 'SELECT
						ap.id,
						ap.ap_payee_term_id,
   						apl.id AS ap_payee_location_id,
   						apc.id AS ap_payee_account_id,
   						CASE 
   							WHEN apc.account_number IS NOT NULL THEN apc.default_ap_remittance_id
   							ELSE apr.id
   						END as ap_remittance_id
					FROM
						ap_payees ap
   						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.is_primary = true )
   						JOIN ap_remittances apr ON ( ap.cid = apr.cid AND ap.id = apr.ap_payee_id AND apr.ap_payee_location_id = apl.id AND apr.is_default = true AND apr.is_published = true )
   						JOIN ap_payee_accounts apc ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id AND apl.id = apc.ap_payee_location_id )
					WHERE
   						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id = ' . ( int ) $intApPayeeTypeId . '
					LIMIT 1';

		if( true == $boolIsForRefund ) {
			return self::fetchApPayee( $strSql, $objDatabase );
		} else {
			return current( fetchData( $strSql, $objDatabase ) );
		}

	}

	public static function fetchApPayeeIdsAndComapanyNamesByCid( $intCid, $objDatabase, $strAlias = NULL ) {

		if( false == valId( $intCid ) ) return NULL;

		$strAlias = ( true == valStr( $strAlias ) ) ? ' AS ' . $strAlias : NULL;

		$strSql = 'SELECT id,
						  company_name ' . $strAlias . '
				   FROM ap_payees
				   WHERE cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeesIdsByApPayeeTypeIdsByCid( $arrintApPayeeTypeIds, $intCid, $objDatabase, $arrintApPayeeIds = [], $boolIsOnSite = NULL ) {

		$strAndCondition = '';
		if( false == valArr( $arrintApPayeeTypeIds ) ) return NULL;
		if( true == valArr( $arrintApPayeeIds ) ) $strAndCondition = ' AND id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )';
		if( false == is_null( $boolIsOnSite ) ) $strAndCondition .= ( false == $boolIsOnSite ) ? ' AND is_on_site = false' : 'AND is_on_site = true';

		$strSql = 'SELECT
						id,
						is_on_site,
						vendor_category_type_id,
						vendor_id,
						company_name
				   FROM
						ap_payees
				   WHERE cid = ' . ( int ) $intCid . '
				         AND ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
				   		 AND ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )' . $strAndCondition;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApPayeesByCidByFilterTypeByPropertyIdsByStatusIds( $arrmixParameters, $intCid, $objDatabase ) {

		$boolIncludeNullProperties  = getArrayElementByKey( 'include_null_properties', $arrmixParameters );
		$strFilterTypeBy            = getArrayElementByKey( 'filter_type_by', $arrmixParameters );
		$strSearchKeyword           = getArrayElementByKey( 'search_keyword', $arrmixParameters );
		$arrintStatusIds            = getArrayElementByKey( 'ap_payee_status_ids', $arrmixParameters );
		$arrintPropertyIds          = getArrayElementByKey( 'property_ids', $arrmixParameters );
		$arrintApPayeeTypeIds       = getArrayElementByKey( 'ap_payee_type_ids', $arrmixParameters );

		if( false == valStr( $strSearchKeyword ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrWhereSql = [];

		$strSearchKeyword = \Psi\CStringService::singleton()->strtolower( $strSearchKeyword );

		switch( $strFilterTypeBy ) {
			case 'name':
				$arrstrNameSql[]	= 'lower( ap.company_name ) LIKE \'%' . $strSearchKeyword . '%\'';
				$arrstrNameSql[]	= '( lower( ale.entity_name ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrWhereSql[]	= '( ' . implode( ' OR ', $arrstrNameSql ) . ' )';
				break;

			case 'location':
				$arrstrWhereSql[] 	= 'lower( apl.location_name ) LIKE \'%' . $strSearchKeyword . '%\'';
				break;

			case 'vendor_code':
				$arrstrWhereSql[] 	= 'lower( apl.vendor_code ) LIKE \'%' . $strSearchKeyword . '%\'';
				break;

			case 'address':
				$arrstrAddressSql[] = '( lower( apl.street_line1 ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrAddressSql[] = '( lower( apl.street_line2 ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrAddressSql[] = '( lower( apl.city ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrAddressSql[] = '( lower( apl.state_code ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrAddressSql[] = '( lower( apl.postal_code ) LIKE \'%' . $strSearchKeyword . '%\' )';

				$arrstrWhereSql[] 	= '( ' . implode( ' OR ', $arrstrAddressSql ) . ' )';
				break;

			case 'contact_name':
				$arrstrCompanySql[] = '( lower( apc.name_first ) LIKE \'%' . $strSearchKeyword . '%\' )';
				$arrstrCompanySql[] = '( lower( apc.name_last ) LIKE \'%' . $strSearchKeyword . '%\' )';

				$arrstrWhereSql[] 	= '( ' . implode( ' OR ', $arrstrCompanySql ) . ' )';
				break;

			case 'email':
				$arrstrWhereSql[] 	= 'lower( apc.email_address ) LIKE \'%' . $strSearchKeyword . '%\'';
				break;

			case 'secondary_number':
				$arrstrWhereSql[] 	= 'lower( ap.secondary_number ) LIKE \'%' . $strSearchKeyword . '%\'';
				break;

			default:
		}

		if( true == valArr( $arrintStatusIds ) && 'on' != $arrintStatusIds[0] && false == empty( $arrintStatusIds[0] ) ) {
			$arrstrWhereSql[] = 'ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintStatusIds ) . ' ) ';
		}

		if( true == valArr( $arrintPropertyIds ) && false == $boolIncludeNullProperties ) {
			$strPropertyWhereSql = 'pga.property_id = ANY ( ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) ';
		} else {
			$strPropertyWhereSql = ' ( ( pga.property_id IS NULL ) OR ( pga.property_id = ANY ( ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) ) )';
		}

		if( true == valArr( $arrintApPayeeTypeIds ) ) {
			$arrstrWhereSql[] = 'ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )';
		} else {
			$arrstrWhereSql[] = 'ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )';
		}

		$strSql = 'SELECT
						DISTINCT
						ap.id,
						ap.ap_payee_status_type_id,
						ap.company_name,
						lower( ap.company_name )
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_payee_contacts apc ON ( apl.cid = apc.cid AND ap.id = apc.ap_payee_id )
						JOIN ap_payee_contact_locations apcl ON ( ap.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id AND apc.id = apcl.ap_payee_contact_id )
						JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND ap.id = ale.ap_payee_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrWhereSql ) . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND EXISTS (
							SELECT
								1
							FROM
								ap_payee_property_groups appg
								JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
								JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
							WHERE
								appg.cid = ' . ( int ) $intCid . '
								AND appg.ap_payee_id = ap.id
								AND ' . $strPropertyWhereSql . '
						)
					ORDER BY
						ap.ap_payee_status_type_id,
						lower( ap.company_name ) ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleApPayeesByIdsByPayeeNameByCid( $arrintApPayeeIds, $strPayeeName, $intCid, $objDatabase ) {

		if( false == valStr( $strPayeeName ) ) return NULL;

		$strSql = 'SELECT
						id,
						cid,
						company_name
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_type_id IN  ( ' . CApPayeeType::STANDARD . ',' . CApPayeeType::INTERCOMPANY . ',' . CApPayeeType::LENDER . ' )
						AND ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

		if( true == valArr( $arrintApPayeeIds ) ) {
			$strSql .= ' AND ( company_name ILIKE \'%' . addslashes( $strPayeeName ) . '%\' OR id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) )';
		} else {
			$strSql .= ' AND company_name ILIKE \'%' . addslashes( $strPayeeName ) . '%\'';
		}

		$strSql .= 'ORDER BY company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleApPayeesByPropertyIdByPayeeNameByCid( $intPropertyId, $strPayeeName, $intCid, $objDatabase, $arrintApPayeeTypeIds = [], $strSelectFields = '' ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strSelectFields      = ( false == valStr( $strSelectFields ) ? 'ap.id, ap.company_name, pga.property_id' : $strSelectFields );
		$arrintApPayeeTypeIds = ( false == valArr( $arrintApPayeeTypeIds ) ? [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER ] : $arrintApPayeeTypeIds );

		$strSql = ' SELECT
						' . $strSelectFields . '
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' ) 
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

		if( true == valStr( $strPayeeName ) ) {
			$strSql .= ' AND company_name ILIKE \'%' . addslashes( $strPayeeName ) . '%\' ';
		}

		$strSql .= ' ORDER BY
    					ap.company_name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleActiveApPayeesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						cid,
						company_name
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_type_id IN  ( ' . CApPayeeType::STANDARD . ',' . CApPayeeType::INTERCOMPANY . ',' . CApPayeeType::LENDER . ',' . CApPayeeType::CORPORATE . ',' . CApPayeeType::RESIDENT . ',' . CApPayeeType::PROPERTY_SOLUTIONS . ',' . CApPayeeType::OWNER . ' )
						AND ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
					ORDER BY
						company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeNamesByApPayeeTypeIdsByCidByStatus( $arrintApPayeeTypeIds, $arrmixVendorStatusTypeId, $intCid, $objClientDatabase, $boolForMrb = false, $boolHideOwnerVendors = false ) {

		if( false == valArr( $arrintApPayeeTypeIds ) ) return 0;

		$strSelect 	= 'ap.id, ap.company_name AS company_name';
		$strJoins 	= 'ap_payees ap';
		$strOrderBy = 'LOWER( ap.company_name )';

		if( true == $boolForMrb ) {
			$strSelect	= 'DISTINCT LOWER( apl.payee_name ) AS payee_name, ' . $strSelect;
			$strJoins	= 'ap_payees ap
							LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )';
			$strOrderBy = 'LOWER( apl.payee_name )';
		}

		$strWhere 	= ( true == $boolHideOwnerVendors ) ? ' AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::PROPERTY_SOLUTIONS . ', ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::LENDER . ' )' : ' AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )';

		$strWhere 	.= ( false == empty ( $arrmixVendorStatusTypeId ) ) ? ' AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrmixVendorStatusTypeId ) . ' )' : ' ';

		$strSql 	= 'SELECT
							' . $strSelect . '
						FROM
							' . $strJoins . '
						WHERE
							ap.cid = ' . ( int ) $intCid . $strWhere . '
						ORDER BY
							' . $strOrderBy;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeNamesByPropertyGroupIdsByApPayeeTypeIdsByVendorStatusTypeIdsByCid( $arrintPropertyGroupIds, $arrintApPayeeTypeIds, $arrintVendorStatusTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintApPayeeTypeIds ) || false == valArr( $arrintVendorStatusTypeIds ) ) return NULL;

		$strSql 	= 'SELECT
							DISTINCT ap.id, 
							ap.company_name
						FROM
							ap_payees ap
							JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
							LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = pga.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
							AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintVendorStatusTypeIds ) . ' )
						ORDER BY
							ap.company_name';
		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleVendorsForReportFilter( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						CASE WHEN apst.id = ' . CApPayeeStatusType::LOCKED . ' THEN \'' . __( 'On Hold' ) . ' \' ELSE util_get_translated( \'name\', apst.name, apst.details ) END AS group,
						ap.id,
						ap.company_name AS name
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON apst.id = ap.ap_payee_status_type_id
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ',' . CApPayeeType::INTERCOMPANY . ' )
					ORDER BY
						apst.id,
						ap.company_name';

		return nestArrayByKeys( fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchApPayeeByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reference_id = ' . ( int ) $intReferenceId . '
						AND ap_payee_type_id = ' . CApPayeeType::RESIDENT;

		return self::fetchApPayee( $strSql, $objDatabase );
	}

	public static function fetchActiveApPayeesByIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;

		$arrintApPayeeTypeIds	= [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER ];

		$strSql = 'SELECT
						DISTINCT ( LOWER( ap.company_name ) ),
						ap.*,
					 	apl.city,
						apl.state_code,
						apl.vendor_code,
						apl.location_name,
						apl.default_ap_routing_tag_id,
						apl.id AS ap_payee_location_id,
						apa.id as ap_payee_account_id,
						apa.default_ap_remittance_id,
						apa.account_number AS ap_payee_account_number,
						gat.account_number || \' : \' || gat.name AS gl_account_name,
						ale.receives_1099,
						apa.default_gl_account_id AS account_default_gl_account_id,
						CASE
							WHEN apa.default_gl_account_id IS NULL THEN
								ap.gl_account_id
							ELSE
								apa.default_gl_account_id
						END AS gl_account_id
					FROM
						ap_payees ap
						LEFT JOIN gl_account_trees gat ON ( gat.cid = ap.cid AND gat.gl_account_id = ap.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
						JOIN ap_payee_locations apl ON ( ap.id = apl.ap_payee_id AND apl.cid = ap.cid AND apl.is_primary = true)
						JOIN ap_payee_accounts apa ON ( apa.cid = apl.cid AND apa.ap_payee_id = apl.ap_payee_id AND apa.ap_payee_location_id = apl.id )
						JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND ap.id = ale.ap_payee_id AND ale.id = apl.ap_legal_entity_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND apa.is_disabled = false
					ORDER BY
						LOWER( ap.company_name )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeByIdByCid( $intId, $intCid, $objClientDatabase ) {

		if( false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
                        ap.*,
                        apl.id as ap_payee_location_id,
						apl.ap_legal_entity_id,
                        apa.id as ap_payee_account_id,
        				apl.location_name,
        				apl.street_line1,
						apl.street_line2,
						apl.city,
						apl.state_code,
						apl.postal_code,
						ale.receives_1099,
						apr.id as locations_ap_remittance_id,
						apl.vendor_code
                    FROM
                        ap_payees ap
                        JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
                        JOIN ap_payee_accounts apa ON ( apa.cid = apl.cid AND apa.ap_payee_location_id = apl.id )
                        JOIN ap_legal_entities ale ON ( ale.cid = ap.cid AND ale.ap_payee_id = ap.id AND ale.id = apl.ap_legal_entity_id )
                        JOIN ap_remittances apr ON ( apr.cid = apl.cid AND apr.ap_payee_id = apl.ap_payee_id AND apr.ap_payee_location_id = apl.id )
                    WHERE
                        ap.cid = ' . ( int ) $intCid . '
                        AND ap.id = ' . ( int ) $intId . '
                        AND apl.is_primary = true
                        AND apr.is_default = true
                        AND ale.deleted_by IS NULL
                        AND ale.deleted_on IS NULL';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByFastLookUpTypeByCid( $strLookUpType, $intCid, $arrmixRequestData, $objDatabase ) {

		if( CApPayee::LOOKUP_TYPE_AP_CODE_VENDOR == $arrmixRequestData['fast_lookup_filter']['lookup_type'] ) {
			$strCompanyName = $arrmixRequestData['q'];

			$strSql = 'SELECT
						DISTINCT ap.id,
						ap.company_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid and ap.id = apl.ap_payee_id)
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.company_name IS NOT NULL
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

			if( true == valStr( $strCompanyName ) ) {
				$strSql .= ' AND ap.company_name ILIKE \'%' . addslashes( $strCompanyName ) . '%\'';
			}

			$strSql .= ' ORDER BY ap.company_name ASC';
		} elseif( CApPayee::LOOKUP_TYPE_AP_CODE_VENDORS_ASSOCIATED_WITH_CATALOG_ITEMS == $arrmixRequestData['fast_lookup_filter']['lookup_type'] ) {

			$strWhereSql    = '';
			$strCompanyName = $arrmixRequestData['q'];
			$strApPayeeId   = $arrmixRequestData['id'];

			if( true == valStr( $strCompanyName ) ) {
				$strWhereSql .= ' AND ap.company_name ILIKE \'%' . addslashes( $strCompanyName ) . '%\'';
			}

			if( true == valStr( $strApPayeeId ) ) {
				$strWhereSql .= ' AND ap.id = \'' . addslashes( $strApPayeeId ) . '\'';
			}

			$strSql = 'SELECT
						DISTINCT ap.id,
						ap.company_name
					FROM
                    	ap_payees ap
						JOIN ap_catalog_items aci ON ( aci.cid = ap.cid AND aci.ap_payee_id = ap.id )
						JOIN ap_codes ac ON ( ap.cid = ac.cid AND ac.id = aci.ap_code_id AND ac.is_disabled = FALSE )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND aci.deleted_on IS NULL
                        AND ac.deleted_on IS NULL
						' . $strWhereSql . '
					ORDER BY
						ap.company_name';
		} else {

			$arrmixFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );

			$strCondition = '';

			if( CApPayee::LOOKUP_TYPE_VENDORS_WITH_WARRANTY_CONTACTS == $strLookUpType ) {
				$strCondition = ' AND apc.id IS NOT NULL';
			} elseif( CApPayee::LOOKUP_TYPE_VENDORS_WITHOUT_WARRANTY_CONTACTS == $strLookUpType ) {
				$strCondition = ' AND apc.id IS NULL';
			}

			$strCondition .= ' AND ap.ap_payee_type_id <> ' . CApPayeeType::OWNER;

			if( false == is_null( $arrmixRequestData['fast_lookup_filter']['ap_payee_status_type'] ) ) {
				$arrintApPayeeStatusTypeIds = [ CApPayeeStatusType::ACTIVE ];
			} else {
				$arrintApPayeeStatusTypeIds	= [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ];
			}

			$intApPayeeId	= ( true == is_numeric( $arrmixRequestData['fast_lookup_filter']['po_header_ap_payee_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['po_header_ap_payee_id'] : NULL;
			$strOrderBy 	= ( true == is_numeric( $intApPayeeId ) ) ? ' apc.ap_payee_location_id' : ' LOWER( ap.company_name )';

			$strSql = 'SELECT
							DISTINCT ( ' . $strOrderBy . ' ),
							ap.*,
						 	apc.city,
							apc.state_code,
							apc.postal_code,
							apc.street_line1,
							apc.street_line2,
							apc.street_line3,
							apc.name_first,
							apc.name_last,
							apc.phone_number,
							apc.mobile_number,
							apc.email_address,
							apl.location_name,
							apl.id as ap_payee_location_id
						FROM
							ap_payees ap
							LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
							LEFT JOIN ap_payee_contacts apc ON ( ( ap.id = apc.ap_payee_id AND apc.cid = ap.cid ) AND apc.is_warranty = true )
							JOIN ap_payee_locations apl ON ( ap.id = apl.ap_payee_id AND apl.cid = ap.cid )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )
							AND apl.is_primary = true
							AND apl.disabled_by IS NULL ' . $strCondition;

			if( true == is_numeric( $intApPayeeId ) ) {
				$strSql .= ' AND ap.id = ' . ( int ) $intApPayeeId;
			}

			if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
				$arrstrAdvancedSearchParameters = [];

				foreach( $arrmixFilteredExplodedSearch as $strKeywordAdvancedSearch ) {

					if( '' != $strKeywordAdvancedSearch ) {
						array_push( $arrstrAdvancedSearchParameters, 'ap.company_name	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
						array_push( $arrstrAdvancedSearchParameters, 'apl.location_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					}
				}

				if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
					$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
				}
			}

			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		return	( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleApPayeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayee( sprintf( 'SELECT * FROM ap_payees WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ap.id AS ap_payee_id,
						ap.company_name AS vendor_name
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
					ORDER BY
						ap.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFundingSourceApPayeesByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						LEFT JOIN owners o ON ( o.cid = ap.cid AND o.ap_payee_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND( ap.ap_payee_type_id = ' . CApPayeeType::LENDER . ' OR ( ap.ap_payee_type_id = ' . CApPayeeType::OWNER . ' AND o.id IS NOT NULL AND o.disabled_by IS NULL ) )
					ORDER BY
						LOWER( ap.company_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeByCustomerIdByApPayeeTypeIdByCid( $intCustomerId, $intApPayeeTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayee( sprintf( 'SELECT * FROM ap_payees WHERE customer_id = %d AND ap_payee_type_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intApPayeeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByCustomerIdsByApPayeeTypeIdsByCid( $arrintCustomerIds, $arrintApPayeeTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
   						apl.id AS ap_payee_location_id,
   						apc.id AS ap_payee_account_id,
   						CASE 
   							WHEN apc.account_number IS NOT NULL THEN apc.default_ap_remittance_id
   							ELSE apr.id
   						END as ap_remittance_id
   					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.is_primary = true )
						JOIN ap_remittances apr ON ( ap.cid = apr.cid AND ap.id = apr.ap_payee_id AND apr.ap_payee_location_id = apl.id AND apr.is_published = true )
   						JOIN ap_payee_accounts apc ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id AND apl.id = apc.ap_payee_location_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN( ' . implode( ', ', $arrintApPayeeTypeIds ) . ' )
						AND ap.customer_id IN( ' . implode( ', ', $arrintCustomerIds ) . ' )';

		return self::fetchApPayees( $strSql, $objDatabase );
	}

	public static function fetchActiveApPayeesByJobIdByCid( $intJobId, $intCid, $objClientDatabase ) {

		if( false == valId( $intJobId ) )  return NULL;

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN job_funding_sources jfs ON ( jfs.cid = ap.cid AND ap.id = jfs.ap_payee_id )
					WHERE
						jfs.cid = ' . ( int ) $intCid . '
						AND jfs.job_id = ' . ( int ) $intJobId . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . ' 
					ORDER BY 
						ap.ap_payee_type_id,
						ap.company_name';

		return parent::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeNamesByPropertyGroupIdsByVendorStatusTypeIds( $arrintPropertyGroupIds, $arrintVendorStatusTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintVendorStatusTypeIds ) ) return NULL;

		$strSql 	= 'SELECT
							DISTINCT ap.id, 
							ap.company_name
						FROM
							ap_payees ap
							JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
							LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = pga.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintVendorStatusTypeIds ) . ' )
							AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						ORDER BY
							ap.company_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeBySecondaryNumberByCid( $strSecondaryNumber, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE cid = ' . ( int ) $intCid . '
						AND LOWER ( secondary_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) trim( $strSecondaryNumber ) ) ) . '\'';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAssociatedPropertiesByApPayeeTypeIdsByCid( $arrintPropertyIds, $arrintApPayeeTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ap.id,
						ap.company_name
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyIds ) . ' ] ) AS lp ON lp.property_id = pga.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
					ORDER BY
						ap.company_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeesByResidentIdsByCid( $arrintApPayeeContactIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApPayeeContactIds ) ) return NULL;

		$strSql = 'SELECT
					ap.*,
					apc.id as ap_payee_contact_id
				FROM
					ap_payees ap
					JOIN ap_payee_contacts apc ON ( apc.ap_payee_id = ap.id AND apc.cid = ap.cid )
				WHERE
					ap.cid =' . ( int ) $intCid . '
					AND apc.id IN ( ' . implode( ',', $arrintApPayeeContactIds ) . ' )
					';
		return self::fetchApPayees( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeeByResidentIdByCid( $intApPayeeContactId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					ap.*
				FROM
					ap_payees ap
					JOIN ap_payee_contacts apc ON ( apc.ap_payee_id = ap.id AND apc.cid = ap.cid )
				WHERE
					ap.cid =' . ( int ) $intCid . '
					AND apc.id = ' . ( int ) $intApPayeeContactId . '
					';
		return self::fetchApPayees( $strSql, $objDatabase, false );
	}

	public static function fetchVerifiedApPayeesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pga.property_id,
						CASE
							WHEN pga.property_id IS NOT NULL THEN 1
							ELSE 0
						END AS ap_payee
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.is_primary = true )
						JOIN ap_payee_contact_locations apcl ON ( ap.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id )
						JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND ap.id = ale.ap_payee_id )
						JOIN ap_remittances ar ON ( ar.ap_payee_id = ap.id  AND ar.cid = ap.cid )
						JOIN ap_payee_property_groups appg ON ( ap.id = appg.ap_payee_id AND ap.cid = appg.cid )
						JOIN property_group_associations pga ON ( ap.cid = pga.cid )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN company_ap_payment_type_associations capta ON ( capta.cid = ap.cid )
					WHERE 
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ', ' . CApPayeeType::RESIDENT . ' )
						AND ap.ap_payee_status_type_id = 1
						AND ar.is_published = true
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND ar.ap_payment_type_id IN ( ' . CApPaymentType::ACH . ' )
						AND ar.is_verified = true
						AND capta.ap_payment_type_id IN ( ' . CApPaymentType::BILL_PAY_ACH . ', ' . CApPaymentType::BILL_PAY_CHECK . ' )
						AND pga.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeesByIdsByCids( $arrintIds, $arrintCids, $objDatabase ) {

		if( false == ( $arrintCids = getIntValuesFromArr( $arrintCids ) ) || false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) return NULL;

		$strSql = 'SELECT
						id,
						cid
					FROM 
						ap_payees
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ')
						AND cid IN ( ' . sqlIntImplode( $arrintCids ) . ')
						AND ap_payee_type_id IN  ( ' . CApPayeeType::STANDARD . ',' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllApPayeesByIdsByPrimaryLocationByCid( $arrintApPayeeIds, $boolIsPrimary, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.country_code,
						apl.location_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apl.is_primary = ' . $boolIsPrimary . '::BOOLEAN
						AND apl.disabled_by IS NULL';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchAllApPayeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchApPayees( $strSql, $objDatabase );
	}

	public static function fetchVendorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON( pga.property_id, ap.id, apl.id, apc.id )
						ap.id,
						apcl.id AS ap_payee_contact_location_id,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apl.vendor_code,
						apl.location_name,
						apst.id AS vendor_status_id,
						apst.name AS vendor_status,
						apc.email_address,
						CONCAT( apc.name_last, \' \', apc.name_first ) AS contact_name,
						apc.name_first,
						apc.name_last,
						apc.id as recipient_id,
						p.property_name
					FROM
						ap_payees ap
						JOIN ap_payee_status_types apst ON ( apst.id = ap.ap_payee_status_type_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL AND apl.deleted_on IS NULL )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apl.id = apcl.ap_payee_location_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.id = apcl.ap_payee_contact_id AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = pg.id )
						LEFT JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id =' . ( int ) $intPropertyId . '
						AND ap.ap_payee_type_id IN( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ', ' . CApPayeeType::LENDER . ' )
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveApPayeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ap.id,
						ap.company_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_property_groups appg ON ( appg.cid = apl.cid AND appg.ap_payee_location_id = apl.id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
						JOIN ap_payee_accounts apa ON ( apa.cid = apl.cid AND apa.ap_payee_id = apl.ap_payee_id AND apa.ap_payee_location_id = apl.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::LENDER . ' )
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apa.is_disabled = false
					GROUP BY 
						ap.id,
						ap.company_name
					ORDER BY
						ap.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllApPayeesByApPayeeTypeIdsByPropertyIdsByCid( $arrintApPayeeTypeIds, $arrintPropertyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApPayeeTypeIds ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = ' SELECT
						DISTINCT ap.id,
						ap.company_name AS company_name
					FROM
						ap_payees ap
						JOIN properties p ON ( p.cid = ap.cid )
						JOIN load_properties(ARRAY [ ' . ( int ) $intCid . ' ]::INT [ ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ]::INT [ ], ARRAY [ 1 ]::INT [ ]) load_prop ON load_prop.property_id = p.id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND ap.id = appg.ap_payee_id )
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						JOIN property_group_associations pga ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . sqlIntImplode( $arrintApPayeeTypeIds ) . ' )
					ORDER BY
						ap.company_name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static  function fetchApPayeeByApRemittanceIdsByIdByCid( $arrintApRemittanceIds, $intId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApRemittanceIds ) || false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						AND default_ap_remittance_id IN ( ' . sqlIntImplode( $arrintApRemittanceIds ) . ' )';

		return self::fetchApPayee( $strSql, $objDatabase );
	}

	public static function fetchApPayeeNamesByApPayeeTypeIdsByPropertyIdsByCid( $arrintApPayeeTypeIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						ap.id,
						ap.company_name,
						NULL AS payee_name
					FROM
						ap_payees ap
						JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND pg.id = appg.property_group_id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
						AND pga.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					GROUP BY
						ap.id,
						ap.company_name
					ORDER BY
						ap.company_name ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeIdsByPropertyTypeIdsByCid( $arrintPropertyTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyTypeIds ) ) {
			return [];
		}

		$strSql = 'SELECT ARRAY(
					SELECT
						ap.id
					FROM
						ap_payees ap
						JOIN property_details pd ON (pd.cid = ap.cid AND pd.ap_payee_id = ap.id)
						JOIN properties p ON ( p.cid = pd.cid AND p.id = pd.property_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
                        AND p.property_type_id IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )
		            ) as ap_payee_ids';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_payee_accounts apa ON( apa.ap_payee_id = ap.id AND apa.cid = ap.cid )
					WHERE
						ap.cid= ' . ( int ) $intCid . '
						AND apa.id = ' . ( int ) $intApPayeeAccountId . '
						AND apa.is_disabled IS FALSE';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertyIdsExceptCurrentApPayeeLocationIdByIdByPropertyIdsByCid( $intId, $arrintPropertyIds, $intCid, $objDatabase, $intApPayeeLocationId ) {

		if( false == valId( $intId ) || false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) || false == valId( $intApPayeeLocationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pga.property_id
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_property_groups appg ON ( appg.cid = apl.cid AND appg.ap_payee_location_id = apl.id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
						JOIN ap_payee_accounts apa ON ( apa.cid = apl.cid AND apa.ap_payee_id = apl.ap_payee_id AND apa.ap_payee_location_id = apl.id )
						JOIN ap_contracts ac ON (ac.cid = apl.cid AND ac.ap_payee_id = apl.ap_payee_id and ac.ap_contract_status_id = 1)
                        JOIN jobs j ON (j.cid = ac.cid AND j.id = ac.job_id AND pga.cid = j.cid AND pga.property_id = j.property_id)
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intId . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::LENDER . ' )
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND apa.is_disabled = false
						AND apl.id <> ' . ( int ) $intApPayeeLocationId . '
			            AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY pga.property_id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeesByApPayeeCompanyNameByCid( $strApPayeeCompanyName, $intCid, $objClientDatabase ) {

		if( false == valStr( $strApPayeeCompanyName ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap.id,
						ap.company_name,
						array_to_string( array_agg( pga.property_id ), \', \' ) AS property_ids
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_property_groups appg ON ( appg.cid = apl.cid AND appg.ap_payee_location_id = apl.id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::LENDER . ' )
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND lower( ap.company_name ) ILIKE lower( \'' . addslashes( trim( $strApPayeeCompanyName ) ) . '\' )
						AND apl.disabled_by IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
					GROUP BY 
						ap.id,
						ap.company_name
					ORDER BY
						ap.company_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveStandardApPayeesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ap.company_name,
						ap.id,
					 	apl.location_name,
						apl.id AS ap_payee_location_id,
						apa.id as ap_payee_account_id,
						apa.account_number AS ap_payee_account_number,
						apa.id AS ap_payee_account_id
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_payee_accounts apa ON ( apl.cid = apa.cid AND apl.ap_payee_id = apa.ap_payee_id AND apl.id = apa.ap_payee_location_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id = ' . CApPayeeType::STANDARD . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL
						AND apl.deleted_by IS NULL
						AND apl.location_name IS NOT NULL
						AND apl.deleted_on IS NULL
						AND apa.is_disabled = false
						ORDER BY
							LOWER( ap.company_name )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomApPayeesByApPayeeTypeIdByCid( $intApPayeeTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ap_payees ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id = ' . ( int ) $intApPayeeTypeId . '
					ORDER BY
						ap.company_name';

		return self::fetchApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeesByPropertyGroupIdsByApHeaderTypeIdByCid( $arrintPropertyGroupIds, $intApHeaderTypeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valId( $intCid ) ) return NULL;

		if( CApHeaderType::PURCHASE_ORDER == $intApHeaderTypeId ) {
			$arrintApPayeeTypeIds = [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER ];
		} else {
			// CApHeaderType::INVOICE
			$arrintApPayeeTypeIds = [ CApPayeeType::STANDARD, CApPayeeType::OWNER, CApPayeeType::INTERCOMPANY, CApPayeeType::RESIDENT ];
		}

		$strSql = 'SELECT
						DISTINCT ON ( ap.company_name, ap.id )
						ap.company_name AS vendor_name,
						ap.id
					FROM
						ap_payees ap
						JOIN LATERAL (
							SELECT
								appg.ap_payee_id
							FROM
								property_groups pg
								JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )

								JOIN ap_payee_property_groups appg ON ( appg.cid = pg.cid )
								JOIN property_group_associations pga_2 ON ( pga_2.cid = appg.cid AND pga_2.property_group_id = appg.property_group_id )
							WHERE
								pg.cid = ' . ( int ) $intCid . '
								AND pg.id IN ( ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' )
								AND pg.deleted_on IS NULL
								AND pga.property_id = pga_2.property_id
								AND appg.ap_payee_id = ap.id
							GROUP BY
								1
						) sub_appg ON TRUE
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND ap.ap_payee_type_id IN ( ' . sqlIntImplode( $arrintApPayeeTypeIds ) . ' )
						AND ap.company_name IS NOT NULL
						AND EXISTS ( SELECT NULL FROM ap_headers ah WHERE ah.cid = ap.cid AND ah.ap_payee_id = ap.id AND ah.ap_header_type_id = ' . ( int ) $intApHeaderTypeId . ' AND ah.deleted_on IS NULL )
					ORDER BY
						1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsAssociatedPropertiesByApPayeeIdByLocationIdByAccountIdByCid( $intApPayeeId, $intApPayeeLocationId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) || false == valId( $intApPayeeAccountId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apa.default_property_id AS property_id,
						apa.id as account_id,
						apa.ap_payee_location_id,
						ap.id as ap_payee_id,
						apl.id as ap_payee_location_id,
						ba.bank_account_type_id,
						ba.reimbursed_property_id,
						pgs.is_ap_migration_mode,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month,
						p.property_name property_name
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl on ( ap.cid = apl.cid AND apl.ap_payee_id = ap.id  )
						JOIN ap_payee_accounts apa ON ( apa.cid = ap.cid AND apa.ap_payee_id = ap.id AND apa.ap_payee_location_id = apl.id )   
						JOIN property_gl_settings pgs ON ( apa.cid = pgs.cid AND apa.default_property_id = pgs.property_id )
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						LEFT JOIN bank_accounts ba ON ( pgs.cid = ba.cid AND pgs.ap_bank_account_id = ba.id )
					WHERE 
						ap.cid = ' . ( int ) $intCid . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apa.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND apa.id = ' . ( int ) $intApPayeeAccountId . ' ';
		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPayeeByApPayeeTypeIdByApPayeeLocationIdByCid( $intApPayeeTypeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeTypeId ) || false == is_numeric( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
						apl.city,
						apl.state_code,
						apl.location_name,
						apl.id AS ap_payee_location_id,
						apl.vendor_code
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND ap.ap_payee_type_id =' . ( int ) $intApPayeeTypeId . '
						AND apl.id =' . ( int ) $intApPayeeLocationId . '
						AND ap.ap_payee_status_type_id =' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND apl.disabled_by IS NULL';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchAllActiveApPayeesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) return NULL;

		$strSql = 'SELECT
						id
					FROM 
						ap_payees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
						ap.*,
						apl.city,
						apl.state_code,
						apl.location_name,
						apl.id AS ap_payee_location_id,
						apl.vendor_code
					FROM
						ap_payees ap
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
					WHERE
						ap.cid =' . ( int ) $intCid . '
						AND apl.id =' . ( int ) $intApPayeeLocationId . '
						AND apl.disabled_by IS NULL';

		return self::fetchApPayee( $strSql, $objClientDatabase );
	}

	public static function fetchPendingBillbackApPayeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ap.company_name,
						apl.vendor_code,
						apl.location_name,
						apl.id AS ap_payee_location_id
					FROM
						ap_payees ap
						JOIN ap_headers ah ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND ah.ap_payee_location_id = apl.id )
						LEFT JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ad.property_id = ' . ( int ) $intPropertyId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND NOT ah.is_template
						AND ah.transaction_amount_due = 0
						AND adr.id IS NULL
						AND ad.ap_transaction_type_id = ' . CApTransactionType::STANDARD . '
						AND ad.reimbursement_method_id = ' . CReimbursementMethod::BILLBACK . '
					GROUP BY
						apl.id,
						ap.company_name,
						apl.vendor_code,
						apl.location_name
					ORDER BY
						LOWER( ap.company_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllApPayeesByIdsByApLegalEntityIdsByCid( $arrintApPayeeIds, $arrintApLegalEntityIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'WITH cte_ap_legal_entity AS(
									SELECT
										ap.*,
										apl.is_primary,
										apl.street_line1,
										apl.street_line2,
										apl.street_line3,
										apl.city,
										apl.state_code,
										apl.postal_code,
										apl.country_code,
										apl.location_name,
										ale.id AS ap_legal_entity_id,
										count ( apl.id ) OVER ( PARTITION BY apl.ap_legal_entity_id, apl.ap_payee_id ) AS temp_ap_locations
									FROM
										ap_payees ap
										JOIN ap_legal_entities ale ON ( ap.cid = ap.cid AND ap.id = ale.ap_payee_id )
										LEFT JOIN ap_payee_locations apl ON ( ale.cid = apl.cid AND ale.ap_payee_id = apl.ap_payee_id AND apl.is_primary = TRUE AND apl.disabled_by IS NULL )
									WHERE
										ap.cid =' . ( int ) $intCid . '
										AND ap.id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
										AND ale.id IN ( ' . implode( ',', $arrintApLegalEntityIds ) . ' )
										 )

									SELECT
									    *
									FROM
										cte_ap_legal_entity cte
									WHERE
										CASE
											WHEN cte.temp_ap_locations > 1 AND TRUE = cte.is_primary THEN
                                        		 cte.is_primary
                                                 AND length ( cte.state_code ) > 0
                                           WHEN cte.temp_ap_locations > 1 AND false = cte.is_primary AND length ( cte.state_code ) > 0 THEN
												length ( cte.state_code ) > 0
                                           ELSE
	                                           1=1                                          
										END
										ORDER BY cte.is_primary DESC
										';

		return self::fetchApPayees( $strSql, $objClientDatabase, false );
	}

}
?>
