<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCheckComponents
 * Do not add any new functions to this class.
 */

class CCheckComponents extends CBaseCheckComponents {

	public static function fetchSimpleCheckComponentsByCheckTemplateIdByCid( $intCheckTemplateId, $intCid, $objClientDatabase ) {
		return self::fetchCheckComponents( 'SELECT * FROM check_components WHERE check_template_id = ' . ( int ) $intCheckTemplateId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY check_component_type_id', $objClientDatabase );
	}

	public static function fetchVisibleCheckComponentsByCheckTemplateIdByCid( $intCheckTemplateId, $intCid, $objClientDatabase ) {
		return self::fetchCheckComponents( 'SELECT * FROM check_components WHERE check_template_id = ' . ( int ) $intCheckTemplateId . ' AND cid = ' . ( int ) $intCid . ' AND is_visible = 1 ORDER BY check_component_type_id', $objClientDatabase );
	}

	public static function fetchVisibleCheckComponentsByCheckTemplateIdsByCid( $arrintCheckTemplateIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintCheckTemplateIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						check_components
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_visible = 1
						AND check_template_id IN ( ' . implode( ',', $arrintCheckTemplateIds ) . ' )
					ORDER BY
						check_component_type_id';

		return self::fetchCheckComponents( $strSql, $objClientDatabase );
	}

	public static function fetchCheckComponentsByCheckTemplateIdByCheckComponentTypeIds( $intCheckTemplateId, $arrintCheckComponentTypeIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintCheckComponentTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						check_components
					WHERE
						cid = ' . ( int ) $intCid . '
						AND check_template_id = ' . ( int ) $intCheckTemplateId . '
						AND check_component_type_id IN ( ' . implode( ',', $arrintCheckComponentTypeIds ) . ' ) 
					ORDER BY
						check_component_type_id';
		return self::fetchCheckComponents( $strSql, $objClientDatabase );
	}

}

?>