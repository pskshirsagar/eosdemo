<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestFrequencyTypes
 * Do not add any new functions to this class.
 */

class CGuestFrequencyTypes extends CBaseGuestFrequencyTypes {

	public static function fetchGuestFrequencyTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGuestFrequencyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGuestFrequencyType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGuestFrequencyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedGuestFrequencyTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM guest_frequency_types WHERE is_published = 1';
		return self::fetchGuestFrequencyTypes( $strSql, $objDatabase );
	}

}
?>