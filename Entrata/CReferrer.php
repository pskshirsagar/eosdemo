<?php

class CReferrer extends CBaseReferrer {

	protected $m_arrintPropertyIds;
	protected $m_objReferrer;
	protected $m_intReferrerContactCustomerId;

	const ACTIVE_REFERRER			= 1;
	const ARCHIVED_REFERRER			= 2;
	const ACTIVE_ARCHIVED_REFERRER	= 3;

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getReferrerContactCustomerId() {
		return $this->m_intReferrerContactCustomerId;
	}

	public function setReferrerContactCustomerId( $intReferrerContactCustomerId ) {
		$this->m_intReferrerContactCustomerId = $intReferrerContactCustomerId;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) && true == valArr( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_ids'] ) ) $this->setPropertyIds( $arrmixValues['property_ids'] );
		if( true == isset( $arrmixValues['reference_id'] ) ) $this->setReferrerContactCustomerId( $arrmixValues['reference_id'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferrerTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intReferrerTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer_type_id', __( 'Referrer type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name is required.' ) ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( 1 > strlen( $this->m_strNameFirst ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name must have at least one letter.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );

		}

		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmail() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strEmail ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', __( 'Email is required.' ), 504 ) );
		}
		if( false == CValidation::validateEmailAddresses( $this->m_strEmail ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', __( 'Please enter a valid email.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhone() {
		$boolIsValid = true;
		$strPhoneNumber   = $this->m_strPhone;
		$objPhoneNumber = $this->createPhoneNumber( $strPhoneNumber );

		$intPhoneLength	= strlen( $objPhoneNumber->getNumber() );

		if( 0 == $intPhoneLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone', __( 'Phone number is required.' ), '' ) );
		} elseif( true == valStr( $objPhoneNumber->getNumber() ) && 0 < $intPhoneLength && false == $objPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ), 504 ) );
		}

		return $boolIsValid;
	}

	/**
	 * This function is used for email or phone number validation.
	 * Skip phone or email validation for Resident referrer.
	 */
	public function valEmailOrPhone( $objDatabase ) {

		$boolIsValid = true;
		if( false == empty( $objDatabase ) ) {
			$objDefaultReferrerType = \Psi\Eos\Entrata\CReferrerTypes::createService()->fetchReferrerTypeByDefaultReferrerTypeIdByCid( CDefaultReferrerType::RESIDENT, $this->getCid(), $objDatabase );
			if( true == valObj( $objDefaultReferrerType, 'CReferrerType' ) && $objDefaultReferrerType->getId() == $this->m_intReferrerTypeId ) {
				return $boolIsValid;
			}
		}

		if( false == empty( $this->m_strEmail ) && false == $this->valEmail() ) {
			$boolIsValid = false;
		}

		if( false == empty( $this->m_strPhone ) && false == $this->valPhone() ) {
			$boolIsValid = false;
		}
		if( true == empty( $this->m_strEmail ) && true == empty( $this->m_strPhone ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email or phone', __( 'Email or Phone is required.' ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function validatePhoneNumber( $strNumber ) {
		if( true == preg_match( '/\-{3,}/', $strNumber ) || 1 !== preg_match( '/^[\(]{0,1}(\d{1,3})[\)]?[\-)]?[\s]{0,}(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?[\s]?(\d*)$/', $strNumber ) ) {
			return 'Phone Number not in valid format.';
		}

		return 1;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDuplicateReferrer( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNameFirst ) || false == valStr( $this->m_strNameLast ) || false == valId( $this->m_intReferrerTypeId ) || ( false == valStr( $this->m_strEmail ) && false == valStr( $this->m_strPhone ) ) ) {
			return true;
		}

		$objReferrer = \Psi\Eos\Entrata\CReferrers::createService()->fetchDuplicateReferrerByCid( $this->getCid(), $objDatabase, $this->m_intId, $this->m_strNameFirst, $this->m_strNameLast, $this->m_intReferrerTypeId, $this->m_strEmail, $this->m_strPhone );

		if( true == valObj( $objReferrer, 'CReferrer' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone', __( 'The referrer you are trying to add already exists.' ), 504 ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case 'validate_insert_or_update':
				$boolIsValid &= $this->valReferrerTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case VALIDATE_REFRRER_TYPE_CUSTOMER:
				$boolIsValid &= $this->valReferrertypeCustomer( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

	public function valReferrertypeCustomer( $objDatabase ) {
		$boolIsValid = true;

		if( true == valObj( \Psi\Eos\Entrata\CReferrers::createService()->fetchReferrersByIdByCustomerIdByReferrerTypeIdByCid( $this->getId(), $this->getCustomerId(), $this->getReferrerTypeId(), $this->getCid(), $objDatabase ), 'CReferrer' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer_type_id', __( 'Same company and referrer type already exist.' ), NULL ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

}
?>