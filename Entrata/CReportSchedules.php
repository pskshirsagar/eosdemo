<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportSchedules
 * Do not add any new functions to this class.
 */

class CReportSchedules extends CBaseReportSchedules {

	public static function fetchReportScheduleDetailsByReportIdByCid( $intReportId, $intCid, $objDatabase, $intReportFilterId = NULL ) {
		$strReportFilterIdCondition = ( false == is_null( $intReportFilterId ) ? 'AND rf.id = ' . ( int ) $intReportFilterId : '' );

		$strSql = '
			SELECT
				r.name,
				r.report_type_id,
				rv.major,
				rv.minor,
				rv.id AS report_version_id,
				rv.is_latest,
				rv.expiration,
				rf.id,
				rf.name AS filter_set_name,
				rf.filters,
				rs.id as report_schedule_id,
				rs.download_options,
				rs.report_schedule_type_id,
				rs.company_user_id AS created_by,
				st.id as schedule_task_id,
				st.last_run_on,
				st.schedule_details,
				st.schedule_type_id
			FROM
				reports r
				JOIN report_versions rv ON ( r.cid = rv.cid AND r.id = rv.report_id )
				JOIN report_filters rf ON ( rf.cid = r.cid AND rf.report_id = r.id AND COALESCE( rv.definition ->> \'is_published\', \'true\' ) = \'true\' )
				JOIN report_schedules rs ON ( rs.cid = rf.cid AND rs.report_filter_id = rf.id AND rv.id = rs.report_version_id )
				JOIN scheduled_tasks st ON ( st.cid = rs.cid AND st.id = rs.scheduled_task_id )
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.id = ' . ( int ) $intReportId . '
				' . $strReportFilterIdCondition . '
				AND rs.deleted_by IS NULL
				AND rf.deleted_by IS NULL
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
			ORDER BY
				rs.id';

		return fetchData( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchMyReportScheduleDetailsByCidByCompanyUserId( $intCompanyUserId, $intCid, $arrstrReportNameSearch, $objDatabase, $boolRecipientReportSchedules = false, $boolIsOrderByFilterName = false, $boolIsCountOnly = false, $strMigrateReportsAccessedIn = '' ) {

		$strSelectFieldsSql = $strJoinSql = $strRemoveDuplicateSchedulesSql = $strCompanyUserSql = $strSelectCreatedBy = $strPublicSharedSql = $strMigratedSql = '';
		$strSortBy = $boolIsOrderByFilterName ? 'schedule_report_list.filter_set_name' : 'schedule_report_list.report_name';

		if( NULL != $intCompanyUserId ) {
			if( true == $boolRecipientReportSchedules ) {

				$strRemoveDuplicateSchedulesSql = ' DISTINCT ON ( st.id ) ';
				$strSelectFieldsSql = ' rsr.reference_id AS report_schedule_recipient,
					cu.id AS company_user_id,
					cg.id AS company_group_id, ';

				$strJoinSql = ' JOIN report_schedule_recipients rsr ON ( rsr.cid  = rs.cid AND rsr.report_schedule_id = rs.id )
							JOIN report_schedule_recipient_types rsrt ON ( rsrt.id = rsr.report_schedule_recipient_type_id )
							LEFT JOIN company_groups cg ON ( cg.cid = rsr.cid AND cg.id = rsr.reference_id)
							LEFT JOIN company_user_groups cug ON ( cug.cid = rsr.cid AND cug.company_group_id = cg.id )
							LEFT JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = COALESCE( cug.company_user_id, rsr.reference_id ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id ) ';

				$strReportSchedulesSql = ' AND COALESCE ( cug.company_user_id, rsr.reference_id ) = ' . ( int ) $intCompanyUserId;

			} else {
				$strReportSchedulesSql = ' AND rs.company_user_id = ' . ( int ) $intCompanyUserId;
			}
		} else {
			$strCompanyUserSql = 'JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.created_by )
									LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )';
			$strSelectCreatedBy = 'ce.name_first,
									ce.name_last,';
			$strPublicSharedSql = ' AND ( rf.is_public = TRUE OR exists ( 
				SELECT 
					1
				FROM 
					shared_filters sf 
				WHERE 
					sf.cid = ' . ( int ) $intCid . '
					AND sf.report_filter_id = rf.id
					AND sf.filter_type_id = ' . CFilterType::FILTER_TYPE_REPORT . ' )
			)';

			$strMigratedSql = ' AND COALESCE( ( rs.download_options->>\'is_migrated\')::BOOLEAN, FALSE ) = FALSE ';
			if( $boolIsCountOnly || valStr( $strMigrateReportsAccessedIn ) ) {
				$strMigratedSql .= ' AND COALESCE( ( rs.download_options->>\'failed_to_migration\')::BOOLEAN, FALSE ) = FALSE ';
			}
		}

		$strSql = ' SELECT
						schedule_report_list.*,
						CASE WHEN day_diff < 31 THEN 1 ELSE 0 END AS thirty,
						CASE WHEN day_diff < 61 THEN 1 ELSE 0 END AS sixty,
						CASE WHEN day_diff < 91 THEN 1 ELSE 0 END AS ninety,
						CASE WHEN day_diff < 366 THEN 1 ELSE 0 END AS one_year,
						1 AS all_records
					FROM
						(
						SELECT
							' . $strRemoveDuplicateSchedulesSql . '
							r.name,
							r.id AS report_id,
							util_get_translated( \'title\', r.title, r.details ) AS report_name,
							r.report_type_id,
							rv.id AS report_version_id,
							rv.major,
							rv.minor,
							COALESCE( util_get_translated( \'title\', m.title, m.details ), \'' . __( 'Client-specific' ) . '\' ) AS category,
							rv.is_latest,
							rv.expiration,
							rv.definition,
							rf.id,
							rf.name AS filter_set_name,
							rf.filters,
							rs.id AS report_schedule_id,
							rs.download_options,
							rs.report_schedule_type_id,
							rs.company_user_id AS created_by,
							' . $strSelectCreatedBy . '
							rs.created_on,
							' . $strSelectFieldsSql . '
							st.id AS schedule_task_id,
							st.last_run_on,
							st.schedule_details,
							st.schedule_type_id,
							DATE_PART( \'DAY\', CURRENT_TIMESTAMP - st.last_run_on ) as day_diff
						FROM
							reports AS r
							JOIN report_versions AS rv ON r.cid = rv.cid AND r.id = rv.report_id AND COALESCE( rv.definition ->> \'is_published\', \'true\' ) = \'true\'
							JOIN report_filters AS rf ON rf.cid = r.cid AND rf.report_id = r.id
							JOIN report_schedules AS rs ON rs.cid = rf.cid AND rs.report_filter_id = rf.id AND rv.id = rs.report_version_id
							JOIN scheduled_tasks AS st ON st.cid = rs.cid AND st.id = rs.scheduled_task_id
							LEFT JOIN default_reports AS dr ON dr.id = r.default_report_id
							LEFT JOIN default_report_groups AS drg ON drg.id = dr.default_report_group_id
							LEFT JOIN modules AS m ON m.id = drg.parent_module_id
							' . $strJoinSql . '
							' . $strCompanyUserSql . '
						WHERE
							r.cid = ' . ( int ) $intCid . '
							' . $strPublicSharedSql . '
							AND COALESCE( m.id, 0 ) < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
							' . $strReportSchedulesSql . '
							' . $strMigratedSql . '
							AND rs.deleted_by IS NULL
							AND rf.deleted_by IS NULL
							AND COALESCE( rv.expiration, NOW() ) >= NOW() ';

		if( true == isset( $arrstrReportNameSearch ) ) {
			$arrstrAdvancedSearchParameters[] = '( r.title || \' \' || rv.major ||\'.\'|| rv.minor ) ILIKE E\'%' . addslashes( trim( $arrstrReportNameSearch ) ) . '%\' OR ( util_get_translated( \'title\', r.title, r.details ) || \' \' || rv.major ||\'.\'|| rv.minor ) ILIKE E\'%' . addslashes( trim( $arrstrReportNameSearch ) ) . '%\'';
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( '', $arrstrAdvancedSearchParameters ) . ' ) ';
		}

		$strSql .= ') AS schedule_report_list
			ORDER BY
			' . $strSortBy;

		$strSelect = 'schedule_report_details.*';
		$strWhereCondition = '';
		if( $boolIsCountOnly ) {
			$strSelect = '
				COALESCE( SUM( schedule_report_details.thirty ), 0 ) AS thirty,
				COALESCE( SUM( schedule_report_details.sixty ), 0 ) AS sixty,
				COALESCE( SUM( schedule_report_details.ninety ), 0 ) AS ninety,
				COALESCE( SUM( schedule_report_details.one_year ), 0 ) AS one_year,
				COALESCE( SUM( schedule_report_details.all_records ), 0 ) AS all_records';
		}
		if( valStr( $strMigrateReportsAccessedIn ) ) {
			$strWhereCondition = 'WHERE schedule_report_details.' . $strMigrateReportsAccessedIn . ' = 1';
		}
		$strUpdatedSql = '
			SELECT
				' . $strSelect . '
			FROM
				( ' . $strSql . ' ) AS schedule_report_details
			' . $strWhereCondition . '
			;';
		return fetchData( $strUpdatedSql, $objDatabase ) ?: [];
	}

	public static function fetchMyReportInstancesScheduleDetailsByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT rs.id AS report_schedule_id,
						ri.id AS report_instance_id,
						ri.report_version_id,
						util_get_translated( \'name\', ri.name, ri.details ) AS report_instance_name,
						ri.report_id,
						ri.module_id,
						r.name,
						util_get_translated( \'title\', r.title, r.details ) AS report_name,
						r.report_type_id,
						COALESCE( util_get_translated( \'title\', m.title, m.details ), \'' . __( 'Client-specific' ) . '\' ) AS category,
						ri.filters,
						rs.download_options,
						rs.report_schedule_type_id,
						rs.company_user_id AS created_by,
						rs.created_on,
						st.id AS schedule_task_id,
						st.last_run_on,
						st.schedule_details,
						st.schedule_type_id,
						CASE 
							WHEN ce.id IS NOT NULL THEN concat( ce.name_first, \' \', ce.name_last )
							ELSE cu_owner.username
						END AS owner_name
					FROM
						report_new_instances ri
						JOIN reports r ON r.cid = ri.cid AND r.id = ri.report_id
						JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id AND COALESCE( ( rv.definition ->> \'is_published\' )::BOOLEAN, TRUE )
						JOIN report_schedules rs ON rs.cid = ri.cid AND rs.report_new_instance_id = ri.id
						JOIN scheduled_tasks st ON st.cid = rs.cid AND st.id = rs.scheduled_task_id
						LEFT JOIN default_reports dr ON dr.id = r.default_report_id
						LEFT JOIN default_report_groups drg ON ( drg.id = dr.default_report_group_id )
						LEFT JOIN modules m ON ( m.id = drg.parent_module_id )
						LEFT JOIN report_schedule_recipients rsr ON ( rsr.cid  = rs.cid AND rsr.report_schedule_id = rs.id )
						LEFT JOIN report_schedule_recipient_types rsrt ON ( rsrt.id = rsr.report_schedule_recipient_type_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rsr.cid AND cg.id = rsr.reference_id)
						LEFT JOIN company_user_groups cug ON ( cug.cid = rsr.cid AND cug.company_group_id = cg.id )
						LEFT JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = COALESCE( cug.company_user_id, rsr.reference_id ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_users cu_owner ON ( cu_owner.cid = rs.cid AND cu_owner.id = rs.company_user_id AND cu_owner.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu_owner.cid AND ce.id = cu_owner.company_employee_id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND COALESCE( m.id, 0 ) < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						AND ( rs.company_user_id IN (  ' . implode( ', ',  $arrintCompanyUserIds ) . ') OR rsr.reference_id IN (  ' . implode( ', ',  $arrintCompanyUserIds ) . ') )
						AND rs.deleted_by IS NULL
						AND ri.deleted_by IS NULL
						AND COALESCE( rv.expiration, NOW() ) >= NOW()
					ORDER BY
						report_instance_name,
						rs.id';

		return fetchData( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchScheduleDetailsLastRunOnRecipientsByReportGroupIdByCId( $intReportGroupId, $intCid, $objDatabase, $boolIsNewReportSystem = false, $boolIsCountOnly = false, $strMigrateReportsAccessedIn = '' ) {

		if( true == $boolIsNewReportSystem ) {
			$strJoinTableNameSql = ' report_new_groups rg';
			$strReportGroupFieldSql = 'rs.report_new_group_id';
		} else {
			$strJoinTableNameSql = ' report_groups rg ';
			$strReportGroupFieldSql = 'rs.report_group_id';
		}

		$strGroupIdSql = $strSelectCreatedBy = $strCompanyUserSql = $strMigratedSql = '';

		if( '' !== $intReportGroupId ) {
			$strGroupIdSql = ' AND rg.id = ' . ( int ) $intReportGroupId . ' ';
		} else {
			$strGroupIdSql = ' AND rg.report_group_type_id = ' . CReportGroupType::PACKET . ' ';
			$strCompanyUserSql = 'LEFT JOIN company_users cu ON ( cu.cid = rg.cid AND cu.id = rg.created_by )
									LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )';
			$strSelectCreatedBy = 'ce.name_first,
									ce.name_last,';

			$strMigratedSql = ' AND COALESCE( ( rs.download_options->>\'is_migrated\')::BOOLEAN, FALSE ) = FALSE ';
			if( $boolIsCountOnly || valStr( $strMigrateReportsAccessedIn ) ) {
				$strMigratedSql .= ' AND COALESCE( ( rs.download_options->>\'failed_to_migration\')::BOOLEAN, FALSE ) = FALSE ';
			}
		}

		$strReportInstancesWhere = '';
		if( $boolIsCountOnly || valStr( $strMigrateReportsAccessedIn ) ) {
			$strReportInstancesWhere = 'AND report_instances.total > 0';
		}

		$strSql = '
			SELECT
				report_packets.*,
				CASE WHEN day_diff < 31 THEN 1 ELSE 0 END AS thirty,
				CASE WHEN day_diff < 61 THEN 1 ELSE 0 END AS sixty,
				CASE WHEN day_diff < 91 THEN 1 ELSE 0 END AS ninety,
				CASE WHEN day_diff < 366 THEN 1 ELSE 0 END AS one_year,
				1 AS all_records
			FROM
				(
				SELECT
						rg.id AS packet_id,
						rg.company_user_id,
						util_get_translated( \'name\', rg.name, rg.details ) AS packet_name,
						st.last_run_on,
						st.schedule_details,
						st.schedule_type_id,
						st.event_sub_type_id,
						st.id AS schedule_task_id,
						rs.id AS report_schedule_id,
						rs.download_options,
						rst.id AS packet_type_id,
						rst.name as packet_type,
						' . $strSelectCreatedBy . '
						sct.name AS scheduled_type,
						CASE
							WHEN rst.id = ' . CReportScheduleType::MANUAL . ' THEN report_history.day_diff
							ELSE DATE_PART( \'DAY\', CURRENT_TIMESTAMP - st.last_run_on )
						END AS day_diff,
						report_instances.total as report_instances_total
					FROM
						' . $strJoinTableNameSql . '
						JOIN report_schedules rs ON ( rs.cid = rg.cid AND ' . $strReportGroupFieldSql . ' = rg.id AND strpos ( rs.download_options::TEXT, \'"is_internal": true\' )::INTEGER = 0 )
						LEFT JOIN scheduled_tasks st ON ( st.cid = rs.cid AND st.id = rs.scheduled_task_id )
						LEFT JOIN schedule_types sct ON ( st.schedule_type_id = sct.id )
						' . $strCompanyUserSql . '
						LEFT JOIN report_schedule_types rst ON ( rs.report_schedule_type_id = rst.id )
						LEFT JOIN LATERAL (
							SELECT
								COUNT(ri.id) AS total
							FROM
								report_instances AS ri
								JOIN report_versions AS rv ON ri.report_version_id = rv.id AND ri.cid = rv.cid
							WHERE
								ri.cid = rg.cid
								AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, true) = true
								AND COALESCE( rv.expiration, NOW() ) >= NOW()
								AND ri.report_group_id = rg.id
								AND ri.deleted_by IS NULL
						) AS report_instances ON TRUE
						LEFT JOIN LATERAL(
							SELECT
								DATE_PART( \'DAY\', CURRENT_TIMESTAMP - rh.created_on ) AS day_diff
							FROM
								report_histories AS rh
							WHERE
								rh.cid = rs.cid
								AND rh.report_schedule_id = rs.id
							ORDER BY
								rh.created_by DESC
							LIMIT 1
						) AS report_history on true
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						' . $strGroupIdSql . '
						' . $strMigratedSql . '
						AND rs.deleted_by IS NULL
						AND rg.deleted_by IS NULL
						' . $strReportInstancesWhere . '
					ORDER BY
						rg.name
				) AS report_packets';

		$strSelect = '
			report_packet_details.*';
		$strWhereCondition = '';
		if( $boolIsCountOnly ) {
			$strSelect = '
				COALESCE( SUM( report_packet_details.thirty ), 0 ) AS thirty,
				COALESCE( SUM( report_packet_details.sixty ), 0 ) AS sixty,
				COALESCE( SUM( report_packet_details.ninety ), 0 ) AS ninety,
				COALESCE( SUM( report_packet_details.one_year ), 0 ) AS one_year,
				COALESCE( SUM( report_packet_details.all_records ), 0 ) AS all_records';
		}
		if( valStr( $strMigrateReportsAccessedIn ) ) {
			$strWhereCondition = 'WHERE report_packet_details.' . $strMigrateReportsAccessedIn . ' = 1';
		}
		$strUpdatedSql = '
			SELECT
				' . $strSelect . '
			FROM
				( ' . $strSql . ' ) AS report_packet_details
			' . $strWhereCondition . '
			;';

		return fetchData( $strUpdatedSql, $objDatabase );
	}

	public static function fetchReportScheduleByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						report_schedules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND ( strpos ( download_options::TEXT, \'"is_internal": true\' )::INTEGER = 0 ) 
						AND report_group_id = ' . ( int ) $intReportGroupId;

		return self::fetchReportSchedule( $strSql, $objDatabase );
	}

	public static function fetchReportNameByReportScheduleIdByCid( $intReportScheduleId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						r.name
					FROM
						report_schedules rs
						JOIN report_filters rf ON ( rf.cid = rs.cid AND rf.id = rs.report_filter_id )
						JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.id = ' . ( int ) $intReportScheduleId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportNameByReportScheduleIdsByCid( $arrintReportScheduleIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintReportScheduleIds ) ) return [];
		$strSql = 'SELECT
						rs.*
					FROM
						report_schedules rs
						JOIN report_filters rf ON ( rf.cid = rs.cid AND rf.id = rs.report_filter_id )
						JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.id IN ( ' . implode( ', ',  $arrintReportScheduleIds ) . ')';

		return self::fetchReportSchedules( $strSql, $objDatabase );
	}

	public static function fetchReportSchedulesByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rs.*
					FROM
						report_schedules rs
						JOIN scheduled_tasks st ON st.cid = rs.cid AND st.id = rs.scheduled_task_id
					WHERE
						st.cid = ' . ( int ) $intCid . '
						AND st.id = ' . ( int ) $intScheduledTaskId;

		return self::fetchReportSchedule( $strSql, $objDatabase );
	}

	public static function fetchReportSchedulesOrPacketSchedulesByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rs.id
					FROM
						report_schedules rs
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.report_filter_id = ' . ( int ) $intReportFilterId . '
						AND rs.deleted_by IS NULL
						AND rs.deleted_on IS NULL
					UNION
					SELECT
					rs.id
					FROM
						report_schedules rs
						JOIN report_instances ri ON ( rs.cid=ri.cid AND rs.report_group_id=ri.report_group_id )
						JOIN report_filters rf ON ( ri.cid=rf.cid AND ri.report_filter_id=rf.id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND ri.report_filter_id = ' . ( int ) $intReportFilterId . '
						AND ri.deleted_by IS NULL
						AND ri.deleted_on IS NULL
					UNION
					SELECT
						rf.id
					FROM
						report_filters rf
						JOIN report_instances ri ON( rf.cid=ri.cid AND rf.report_id = ri.report_id )
						JOIN report_groups rg ON ( rg.cid = ri.cid AND ri.report_group_id = rg.id and rg.report_group_type_id = ' . CReportGroupType::PACKET . ' )
					WHERE
						rf.cid = ' . ( int ) $intCid . '
						AND rf.id = ' . ( int ) $intReportFilterId . '
						AND ri.deleted_by IS NULL
						AND rg.deleted_by IS NULL
						AND rf.id = ri.report_filter_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportSchedulesByEventSubTypeIdsByCid( $arrintEventSubTypeIds, $intCid, CDatabase $objDatabase ) {

		if( false == valArr( $arrintEventSubTypeIds ) || false == is_int( $intCid ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				rs.*
			FROM
				report_schedules rs
				JOIN scheduled_tasks st ON st.cid = rs.cid AND st.id = rs.scheduled_task_id
			WHERE
				rs.cid = ' . ( int ) $intCid . '
				AND rs.deleted_by IS NULL
				AND ( st.end_on IS NULL OR st.end_on > NOW() )
				AND st.event_sub_type_id IN( ' . implode( ',', $arrintEventSubTypeIds ) . ' )';

		return self::fetchReportSchedules( $strSql, $objDatabase );
	}

	public static function fetchReportsByCidBySearchKeywords( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
					(
						SELECT
							DISTINCT r.id AS report_id,
							( util_get_translated( \'title\', r.title, r.details ) || \' \' || rv.major ||\'.\'|| rv.minor ) AS report_name,
							r.name,
							rv.id AS report_version_id
						FROM
							reports r
							JOIN report_filters rf ON ( rf.cid = r.cid AND rf.report_id = r.id )
							JOIN report_versions rv ON ( r.cid = rv.cid AND r.id = rv.report_id AND rv.definition ->> \'is_published\' = \'true\' )
							JOIN report_schedules rs ON ( rs.cid = rf.cid AND rs.report_filter_id = rf.id AND rv.id = rs.report_version_id )
						WHERE
							r.cid = ' . ( int ) $intCid . '
							AND rs.company_user_id = ' . ( int ) $intCompanyUserId . '
							AND rs.deleted_by IS NULL
							AND rf.deleted_by IS NULL

						UNION

						SELECT
							DISTINCT r.id AS report_id,
							( util_get_translated( \'title\', r.title, r.details ) || \' \' || rv.major ||\'.\'|| rv.minor ) AS report_name,
							r.name,
							rv.id AS report_version_id
						FROM
							reports r
							JOIN report_filters rf ON ( rf.cid = r.cid AND rf.report_id = r.id )
							JOIN report_versions rv ON ( r.cid = rv.cid AND r.id = rv.report_id AND rv.definition ->> \'is_published\' = \'true\' )
							JOIN report_schedules rs ON ( rs.cid = rf.cid AND rs.report_filter_id = rf.id AND rv.id = rs.report_version_id )
							JOIN report_schedule_recipients rsr ON ( rsr.cid = rs.cid AND rsr.report_schedule_id = rs.id )
							JOIN report_schedule_recipient_types rsrt ON ( rsrt.id = rsr.report_schedule_recipient_type_id )
							LEFT JOIN company_groups cg ON ( cg.cid = rsr.cid AND cg.id = rsr.reference_id )
							LEFT JOIN company_user_groups cug ON ( cug.cid = rsr.cid AND cug.company_group_id = cg.id )
							LEFT JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = COALESCE ( cug.company_user_id, rsr.reference_id ) AND cu.company_user_type_id IN (  ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						WHERE
							r.cid = ' . ( int ) $intCid . '
							AND COALESCE ( cug.company_user_id, rsr.reference_id ) = ' . ( int ) $intCompanyUserId . '
							AND rs.deleted_by IS NULL
							AND rf.deleted_by IS NULL
					) AS report_names
					ORDER BY
						report_name';

		return fetchData( $strSql, $objDatabase )?: [];
	}

	public static function fetchReportInternalScheduleByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						report_schedules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND ( strpos ( download_options::TEXT, \'"is_internal": true\' )::INTEGER <> 0 )
						AND report_group_id = ' . ( int ) $intReportGroupId;

		return self::fetchReportSchedule( $strSql, $objDatabase );
	}

	public static function fetchReportSchedulesByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase, $boolIsForCount = false ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return [];

		if( true == $boolIsForCount ) {
			$strSelectFieldSql = ' COUNT( rs.id ) AS schedule_count, ';
			$strGroupByFieldSql = '';
		} else {
			$strSelectFieldSql = 'rs.id AS report_schedule_id,
								rf.name AS filter_name,
								util_get_translated( \'title\', r.title, r.details ) AS report_name, ';
			$strGroupByFieldSql = ', rs.id, rf.name, r.title, r.details';
		}

		$strSql = ' SELECT 
						' . $strSelectFieldSql . '
						rs.company_user_id
					FROM
						reports r
						JOIN report_versions rv ON ( r.cid = rv.cid AND r.id = rv.report_id AND rv.definition ->> \'is_published\' = \'true\' )
						JOIN report_filters rf ON ( rf.cid = r.cid AND rf.report_id = r.id )
						JOIN report_schedules rs ON ( rs.cid = rf.cid AND rs.report_filter_id = rf.id AND rv.id = rs.report_version_id )
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.company_user_id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
						AND rs.deleted_by IS NULL
					GROUP BY
						rs.company_user_id '
						. $strGroupByFieldSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportPacketsByReportNewGroupIdByCId( $arrintReportNewGroupIds, $intCid, $objDatabase,$arrmixSearchCriteria = [] ) {
		if( false == valArr( $arrintReportNewGroupIds ) ) {
			return [];
		}
		$strReportTitleCondition = $strReportScheduleTypeCondition = $strReportUserCondition = $strReportRecipientCondition = $strPrivatePacketCondition = '';
		if( valArr( $arrmixSearchCriteria ) ) {

			$strPrivatePacketCondition = 'AND ( util.util_to_int(rng.details->>\'module_id\') IS NULL OR util.util_to_int( rng.details->>\'module_id\') = ' . $arrmixSearchCriteria['public_packet_id'] . ')';
			if( valStr( $arrmixSearchCriteria['show_private_packets'] ) ) {
					if( $arrmixSearchCriteria['is_admin'] ) {
						$strPrivatePacketCondition = 'AND util.util_to_int( rng.details->>\'module_id\' ) = ' . $arrmixSearchCriteria['private_packet_id'];
					} else {
						$strPrivatePacketCondition = 'AND util.util_to_int( rng.details->>\'module_id\' ) = ' . $arrmixSearchCriteria['private_packet_id'] . ' AND rng.created_by = ' . $arrmixSearchCriteria['company_user_id'];
					}
			}

			if( valStr( $arrmixSearchCriteria['keywords'] ) ) {
				$strReportTitleCondition = ' AND ( LOWER( rng.name ) LIKE \'%' . \Psi\CStringService::singleton()->strtolower( $arrmixSearchCriteria['keywords'] ) . '%\' )';
			}
			if( valArr( $arrmixSearchCriteria['packet_status'] ) && 'all' != current( $arrmixSearchCriteria['packet_status'] ) ) {
				$strReportScheduleTypeCondition = ' AND ( rs.report_schedule_type_id IN (' . implode( ',', $arrmixSearchCriteria['packet_status'] ) . ') )';
			}
			if( valArr( $arrmixSearchCriteria['packet_user'] ) && 'all' != current( $arrmixSearchCriteria['packet_user'] ) ) {
				$strReportUserCondition = ' AND ( cu.id IN (' . implode( ',', $arrmixSearchCriteria['packet_user'] ) . ') )';
			}
			if( valArr( $arrmixSearchCriteria['packet_recipient'] ) && 'all' != current( $arrmixSearchCriteria['packet_recipient'] ) ) {
				$strReportRecipientCondition = 'JOIN report_schedule_recipients AS rsr ON rsr.report_schedule_id = rs.id AND ( rsr.reference_id IN (' . implode( ',', $arrmixSearchCriteria['packet_recipient'] ) . ') )';
			}
		}

		$strSql = ' SELECT 
						report_packet_list.* 
					FROM
						(
						SELECT
							DISTINCT ON ( rng.id, rni.id )
							rng.id AS packet_id,
							rni.report_id,
							r.name,
							r.report_type_id,
							rni.id AS report_new_instance_id,
							rni.module_id,
							rs.report_schedule_type_id,
							rng.report_group_type_id,
							cu.id AS company_user_id,
							util_get_translated( \'name\', rng.name, rng.details ) AS packet_name,
							rni.report_version_id,
							( rv.major || \'.\' || rv.minor ) AS version_no,
							CASE
								WHEN ce.id IS NOT NULL THEN ( ce.name_first || \' \' || ce.name_last )
								ELSE \'' . __( 'Entrata User' ) . '\'
							END  AS created_by,
							st.id AS schedule_task_id,
							st.last_run_on,
							rs.id AS report_schedule_id,
							rs.download_options,
							sch.scheduled_data,
							rngi.id AS report_new_group_instance_id,
							util_get_translated( \'name\', rni.name, rni.details ) AS report_name,
							CASE
								WHEN st.schedule_type_id = ' . CScheduleType::EVENT_TRIGGER . ' and rst.id IN ( ' . CReportScheduleType::PERIOD_CLOSE . ', ' . CReportScheduleType::PERIOD_ADVANCE . ' ) THEN (
									CASE
										WHEN st.event_sub_type_id = ' . CEventSubType::AR_PERIOD_ADVANCE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'AR Advance' ) . '\'
										WHEN st.event_sub_type_id = ' . CEventSubType::AP_PERIOD_ADVANCE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'AP Advance' ) . '\'
										WHEN st.event_sub_type_id = ' . CEventSubType::GL_PERIOD_ADVANCE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'GL Advance' ) . '\'
										WHEN st.event_sub_type_id = ' . CEventSubType::AR_PERIOD_CLOSE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'AR Close' ) . '\'
										WHEN st.event_sub_type_id = ' . CEventSubType::AP_PERIOD_CLOSE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'AP Close' ) . '\'
										WHEN st.event_sub_type_id = ' . CEventSubType::GL_PERIOD_CLOSE . ' THEN \'' . __( 'Event Trigger - ' ) . '\' || \'' . __( 'GL Close' ) . '\'
									END )
								ELSE util_get_translated( \'name\', rst.name, rst.details )
							END AS schedule_type
						FROM
							report_new_groups AS rng
							JOIN report_new_group_instances AS rngi ON rngi.cid = rng.cid AND rngi.report_new_group_id = rng.id
							JOIN report_new_instances AS rni ON rni.cid = rngi.cid AND rni.id = rngi.report_new_instance_id
							JOIN reports r ON r.cid = rni.cid AND r.id = rni.report_id
							JOIN report_versions AS rv ON rv.cid = rni.cid AND rv.id = rni.report_version_id
							JOIN report_schedules AS rs ON rs.cid = rng.cid AND rs.report_new_group_id = rng.id
							' . $strReportRecipientCondition . '
							LEFT JOIN LATERAL 
							(
								SELECT
									JSON_BUILD_OBJECT( \'id\', schedules.id, \'is_internal\', schedules.is_internal ) AS scheduled_data
								FROM
								(
									SELECT
										rep_sch.id,
										CASE
											WHEN STRPOS( rep_sch.download_options::TEXT, \'"is_internal": true\' )::INTEGER > 0 THEN TRUE
										END AS is_internal,
										ROW_NUMBER( ) OVER ( PARTITION BY rep_sch.report_new_group_id
									ORDER BY
										rep_sch.id DESC ) AS latest
									FROM
										report_schedules rep_sch
									WHERE
										rep_sch.cid = rng.cid
										AND rep_sch.report_new_group_id = rng.id
										AND rep_sch.deleted_on IS NULL
								) AS schedules
								WHERE
									schedules.latest = 1
							) sch ON TRUE
							LEFT JOIN scheduled_tasks AS st ON st.cid = rs.cid AND st.id = rs.scheduled_task_id
							LEFT JOIN schedule_types AS sct ON st.schedule_type_id = sct.id
							LEFT JOIN report_schedule_types AS rst ON rs.report_schedule_type_id = rst.id
							LEFT JOIN company_users AS cu ON cu.cid = rng.cid AND cu.id = COALESCE( rng.company_user_id, rng.created_by )
							LEFT JOIN company_employees AS ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
						WHERE
							rng.cid = ' . ( int ) $intCid . '
							' . $strPrivatePacketCondition . '
							AND rng.id IN ( ' . implode( ',', $arrintReportNewGroupIds ) . ' )
							AND rs.download_options ->> \'is_internal\'::TEXT IS NULL
							' . $strReportTitleCondition . '
							' . $strReportScheduleTypeCondition . '
							' . $strReportUserCondition . '
							AND rs.deleted_by IS NULL
							AND rni.deleted_by IS NULL
							AND rngi.deleted_by IS NULL
						) AS report_packet_list
					ORDER BY
						report_packet_list.packet_name,
						report_packet_list.report_new_group_instance_id';

	// Adding report_new_group_instance_id in order by clause so that this listing should show instances in order in which user added those.
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportScheduleByReportNewGroupIdByCid( $intReportNewGroupId, $intCid, $objDatabase, $boolIsInternal = false ) {

		if( true == $boolIsInternal ) {
			$strDownloadOptionsCondition = 'AND ( strpos ( download_options::TEXT, \'"is_internal": true\' )::INTEGER <> 0 )';
		} else {
			$strDownloadOptionsCondition = 'AND ( strpos ( download_options::TEXT, \'"is_internal": true\' )::INTEGER = 0 ) ';
		}

		$strSql = 'SELECT
						*
					FROM
						report_schedules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						' . $strDownloadOptionsCondition . '
						AND report_new_group_id = ' . ( int ) $intReportNewGroupId;

		return self::fetchReportSchedule( $strSql, $objDatabase );
	}

	public static function fetchReportScheduleForUpdate( $intCid, $intId, CDatabase $objDatabase ) {
		return self::fetchReportSchedule( 'SELECT * FROM report_schedules rs WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId . ' FOR UPDATE', $objDatabase );
	}

	public static function fetchReportNewScheduleByReportInstanceIdsByCid( array $arrintReportNewInstanceId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				rs.id
			FROM
				report_new_instances ri
				JOIN report_schedules rs ON rs.cid = ri.cid AND rs.report_new_instance_id = ri.id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id IN ( ' . implode( ', ', $arrintReportNewInstanceId ) . ' )
				AND ri.deleted_by IS NULL
				AND rs.deleted_by IS NULL';

		return self::fetchReportSchedules( $strSql, $objDatabase );

	}

	public static function fetchCompanyUsersWithReportSchedulesByCid( $intCid, $objDatabase, $boolIsPSIUser, $intCompanyUserId, $boolIsNewSystem ) {

		if( $boolIsNewSystem ) {
			$strPSIUserConditionSql = !$boolIsPSIUser ? ' AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA : ' AND cu.company_user_type_id IN( ' . implode( ',', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )';

			$strSql = '
				SELECT
					DISTINCT id,
					created_by
				FROM
				(
					SELECT
						cu.id,
						CASE
							WHEN ce_owner.id IS NOT NULL THEN ce_owner.name_first || \' \' || ce_owner.name_last
							ELSE cu.username
						END AS created_by
					FROM
						report_schedules rs
						JOIN report_new_instances ri ON ri.cid = rs.cid AND ri.id = rs.report_new_instance_id
						JOIN company_users cu ON cu.cid = rs.cid AND cu.id = rs.created_by
 						LEFT JOIN company_employees ce_owner ON ce_owner.cid = cu.cid AND ce_owner.id = cu.company_employee_id
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND cu.id <> ' . ( int ) $intCompanyUserId . $strPSIUserConditionSql . '

					UNION

					SELECT
						cu.id,
						CASE
						  WHEN ce_recipient.id IS NOT NULL THEN ce_recipient.name_first || \' \' || ce_recipient.name_last
						END AS created_by
					FROM
						report_schedules rs
						JOIN report_schedule_recipients rsr ON rsr.cid = rs.cid AND rsr.report_schedule_id = rs.id
						LEFT JOIN company_users cu ON cu.cid = rsr.cid AND cu.id = rsr.reference_id
						LEFT JOIN company_employees ce_recipient ON ce_recipient.cid = cu.cid AND ce_recipient.id = cu.company_employee_id
					WHERE
					rs.cid = ' . ( int ) $intCid . $strPSIUserConditionSql . '
				) user_data
				WHERE
					id IS NOT NULL
				ORDER BY
					created_by';
		} else {
			$strPSIUserConditionSql = !$boolIsPSIUser ? ' AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA : ' AND cu.company_user_type_id IN( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )';

			$strSql = '
				SELECT
					DISTINCT
					cu.id,
					CASE
						WHEN ce.id IS NOT NULL THEN ce.name_first || \' \' || ce.name_last
						ELSE cu.username
					END AS created_by
				FROM
					report_schedules rs
					JOIN company_users cu ON cu.cid = rs.cid AND cu.id = rs.created_by
					LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
				WHERE
					rs.cid = ' . ( int ) $intCid . '
					' . $strPSIUserConditionSql . '
					AND cu.id <> ' . ( int ) $intCompanyUserId . '
				ORDER BY
					created_by';
		}

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrmixCompanyUsers = extractArrayKeyValuePairs( $arrmixData, 'id', 'created_by' );

		return $arrmixCompanyUsers;
	}

	public static function fetchReportPacketsWithExpiredInstances( $objDatabase ) {
		$strSql = 'SELECT
						packets_details.*
						FROM (
							SELECT
								DISTINCT ON ( rs.id, rs.cid )
								rs.*,
								COUNT(rni.id) OVER (PARTITION BY rng.id) AS total_instances,
								COUNT(CASE WHEN (COALESCE( rv.expiration, NOW() ) < NOW() OR rngi.deleted_by IS NOT NULL OR rni.deleted_by IS NOT NULL OR (rv.definition ->> \'is_published\' = \'false\') ) THEN rni.id ELSE NULL END) OVER (PARTITION BY rng.id) AS expired_instances
							FROM
								report_schedules AS rs
							    JOIN report_new_groups AS rng ON rs.cid = rng.cid AND rs.report_new_group_id = rng.id
							    JOIN report_new_group_instances AS rngi ON rngi.cid = rng.cid AND rngi.report_new_group_id = rng.id
								JOIN report_new_instances AS rni ON rni.cid = rngi.cid AND rni.id = rngi.report_new_instance_id
							    JOIN reports r ON r.cid = rni.cid AND r.id = rni.report_id
							    JOIN report_versions AS rv ON rv.cid = rni.cid AND rv.id = rni.report_version_id
							WHERE
								rng.report_group_type_id = ' . CReportGroupType::PACKET . '
								AND rs.deleted_by IS NULL
								AND rng.deleted_by IS NULL
							UNION
							SELECT
								DISTINCT ON ( rs.id, rs.cid )
								rs.*,
								COUNT(ri.id) OVER (PARTITION BY rg.id) AS total_instances,
								COUNT(CASE WHEN (COALESCE( rv.expiration, NOW() ) < NOW() OR ri.deleted_by IS NOT NULL OR (rv.definition ->> \'is_published\' = \'false\') ) THEN ri.id ELSE NULL END) OVER (PARTITION BY rg.id) AS expired_instances
							FROM
								report_schedules AS rs
							    JOIN report_groups AS rg ON rs.cid = rg.cid AND rs.report_group_id = rg.id
								JOIN report_instances AS ri ON ri.cid = rg.cid AND ri.report_group_id = rg.id
							    JOIN reports r ON r.cid = ri.cid AND r.id = ri.report_id
							    JOIN report_versions AS rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
							WHERE
								rg.report_group_type_id = ' . CReportGroupType::PACKET . '
								AND rs.deleted_by IS NULL
								AND rg.deleted_by IS NULL
						) AS packets_details
						WHERE
							packets_details.total_instances = packets_details.expired_instances
					';

		return self::fetchReportSchedules( $strSql, $objDatabase ) ?: [];

	}

}
