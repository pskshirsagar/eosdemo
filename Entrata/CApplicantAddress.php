<?php

class CApplicantAddress extends CBaseApplicantAddress {

	protected $m_arrobjPropertyApplicationPreferences;

 	/**
 	 * Set Functions
 	 */

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = ( 0 == strlen( $strStreetLine1 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) ) ) );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = ( 0 == strlen( $strStreetLine2 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) ) ) );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = ( 0 == strlen( $strStreetLine3 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) ) ) );
	}

	public function setCity( $strCity ) {
		$this->m_strCity = ( 0 == strlen( $strCity ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCity, 50, NULL, true ) ) ) );
	}

	public function setCounty( $strCounty ) {
		$this->m_strCounty = ( 0 == strlen( $strCounty ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCounty, 50, NULL, true ) ) ) );
	}

	public function setPropertyApplicationPreferences( $arrobjPropertyApplicationPreferences ) {

		$this->m_arrobjPropertyApplicationPreferences = $arrobjPropertyApplicationPreferences;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is require.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApplicantId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', __( 'Applicant id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAddressTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTimeZoneId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valAddress( $boolIsValidateRequiredField ) {

		$boolIsValid = true;
		$strSectionName = '';

		if( 0 == strlen( $this->m_strStreetLine1 ) && 0 == strlen( $this->m_strStreetLine2 ) ) {
			$boolIsValid = false;
			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} elseif( 0 < $this->getIsNonUsAddress() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Current address is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Current street is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		if( 0 == $this->getIsNonUsAddress() ) {

			if( 0 == strlen( $this->m_strCity ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'Current city is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField );

			if( 0 == strlen( $this->m_strStateCode ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'Current state/province code is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCounty() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {
		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName )  : 'Current';
		if( 0 == strlen( $this->getPostalCode() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		} elseif( false == CApplicationUtils::validateZipCode( $this->getPostalCode(), NULL ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code must be 5 to 10 characters.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		} elseif( false == CValidation::validateMilitaryPostalCode( $this->getPostalCode(), $this->getStateCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} military state zip code must start with 0 or 9.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $intCustomerTypeId,  $strAction, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valAddressTypeId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valAddressTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'new_applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valAddress( $boolIsValidateRequiredField );
				break;

			case 'new_secondary_applicant_pre_screening':
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valAddress( $boolIsValidateRequiredField );
				break;

			case 'update_additional_info':
				if( CAddressType::CURRENT == $this->getAddressTypeId() ) {
					$boolIsValid &= $this->valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField );
				}

				if( CAddressType::PREVIOUS == $this->getAddressTypeId() && $intCustomerTypeId != CCustomerType::GUARANTOR ) {
					$boolIsValid &= $this->valPreviousAddressInformation( $intCustomerTypeId, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField );
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField ) {

		$boolIsValid = true;
		$strSectionName = '';

		if( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
			return true;
		}

		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue();
		}

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_BLOCK_NON_US_RESIDENT', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_BLOCK_NON_US_RESIDENT']->getValue() ) ) {
			$boolIsValid &= $this->valBlockNonUsResident( CApplicationStep::ADDITIONAL_INFO );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddress( $strSectionName, 'addl_info', $boolIsValidateRequiredField );

		}

		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current';
		if( false == is_null( $this->getPostalCode() ) && false == CApplicationUtils::validateZipCode( $this->getPostalCode(), NULL ) ) {
			// ELSE CONDITION IS ADDED TO ENSURE THAT THE ZIP CODE IS VALIDATED ( IF INPUT IS PROVIDED ) EVEN WHEN CURRENT ADDRESS IS NOT MANDATORY
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_postal_code', __( '{%s,0} address zip code must be 5 to 10 characters.', [ $strSectionNameToDisplay ] ), [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( true == is_null( $this->getPostalCode() ) && true == $boolIsValidateRequiredField && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) && 0 == ( int ) $this->getIsNonUsAddress() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( false == CValidation::validateMilitaryPostalCode( $this->getPostalCode(),  $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_postal_code', __( 'Military state zip code must start with 0 or 9.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_RESIDENCE_TYPE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressResidenceType( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_REASON_FOR_LEAVING', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressReasonForLeaving( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_REASON_FOR_LEAVING']->getPropertyId(), $objDatabase, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
		} else {
			$boolIsValid &= $this->valCurrentAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
		} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MONTHLY_PAYMENT_AMOUNT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMonthlyPaymentAmount( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MONTHLY_PAYMENT_RECIPIENT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMonthlyPaymentRecipient( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_OWNED_OR_RENTED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_OWNED_OR_RENTED']->getValue() ) {
			$boolIsValid &= $this->valCurrentAddressIsOwner( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_COMMUNITY_NAME', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressCommunityName( $strSectionName, $boolIsValidateRequiredField );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressInformation( $intCustomerTypeId, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField = true ) {
		$boolIsValid = true;
		$strSectionName = '';
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous';

		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue();
		}

		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerType::GUARANTOR != $intCustomerTypeId ) {

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {

						$boolIsValid &= $this->valPreviousAddress( $strSectionName, 'addl_info', $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_RESIDENCE_TYPE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressResidenceType( $strSectionName, $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_REASON_FOR_LEAVING', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressReasonForLeaving( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_REASON_FOR_LEAVING']->getPropertyId(), $objDatabase, $strSectionName, $boolIsValidateRequiredField );
					}

					if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
					} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
					}

					if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
					} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MONTHLY_PAYMENT_AMOUNT', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressMonthlyPaymentAmount( $strSectionName, $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MONTHLY_PAYMENT_RECIPIENT', $arrobjPropertyApplicationPreferences ) ) {

						$boolIsValid &= $this->valPreviousAddressMonthlyPaymentRecipient( $strSectionName, $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_OWNED_OR_RENTED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_OWNED_OR_RENTED']->getValue() ) {
						$boolIsValid &= $this->valPreviousAddressIsOwner( $strSectionName, $boolIsValidateRequiredField );
					}

					if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_COMMUNITY_NAME', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valPreviousAddressCommunityName( $strSectionName, $boolIsValidateRequiredField );
					}

					if( false == is_null( $this->getPostalCode() ) && false == CApplicationUtils::validateZipCode( $this->getPostalCode(), NULL ) ) {
						// ELSE CONDITION IS ADDED TO ENSURE THAT THE ZIP CODE IS VALIDATED ( IF INPUT IS PROVIDED ) EVEN WHEN PREVIOUS ADDRESS IS NOT MANDATORY
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} address zip code must be 5 to 10 characters.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					} elseif( true == is_null( $this->getPostalCode() ) && true == $boolIsValidateRequiredField && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && 0 == ( int ) $this->getIsNonUsAddress() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}

					if( false == CValidation::validateMilitaryPostalCode( $this->getPostalCode(), $this->getStateCode() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( 'Military state zip code must start with 0 or 9.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}

		} elseif( $intCustomerTypeId != CCustomerType::GUARANTOR || ( CCustomerType::GUARANTOR == $intCustomerTypeId && true == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {

				// ELSE CONDITION IS ADDED TO ENSURE THAT THE ZIP CODE / MOVE-IN / MOVE-OUT DATE IS VALIDATED ( IF INPUT IS PROVIDED ) EVEN WHEN FIELD IS NOT MANDATORY
				if( false == is_null( $this->getPostalCode() ) && false == CApplicationUtils::validateZipCode( $this->getPostalCode(), NULL ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} address zip code must be 5 to 10 characters.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} elseif( true == is_null( $this->getPostalCode() ) && true == $boolIsValidateRequiredField && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && 0 == ( int ) $this->getIsNonUsAddress() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
				} elseif( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
				}

				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
				}

		}

				return $boolIsValid;
	}

	public function valBlockNonUsResident( $intApplicationStepId = NULL ) {

		$boolIsValid = true;

		if( false == is_null( $intApplicationStepId ) && CApplicationStep::BASIC_INFO == $intApplicationStepId ) {
			if( true == $this->getIsAlien() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_alien', __( 'U.S. citizen is required.' ), '' ) );
			}
		} elseif( false == is_null( $intApplicationStepId ) && CApplicationStep::ADDITIONAL_INFO == $intApplicationStepId ) {
			if( true == $this->getIsNonUsAddress() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_non_us_address', __( 'A current U.S. address is required. Please contact the property staff for assistance with your application.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddress( $strSectionName = NULL, $strStepName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 'addl_info' == $strStepName ) {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsNonUsAddress;
		} else {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsAlien;
		}

		if( 0 == strlen( $this->m_strStreetLine1 ) && 0 == strlen( $this->m_strStreetLine2 ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;
			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line2', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} elseif( 0 < $this->getIsNonUsAddress() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( 'Current address is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line2', __( 'Current address is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line3', __( 'Current address is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( 'Current street is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line2', __( 'Current street is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		if( false == $boolIsNonUsAddress ) {

			if( 0 == strlen( $this->m_strCity ) && true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_city', __( 'Current city is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valCurrentPostalCode( $strSectionName, $boolIsValidateRequiredField );

			if( 0 == strlen( $this->m_strStateCode ) && true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_state_code', __( 'Current state/province code is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCurrentPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPostalCode() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName )  : 'Current';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveInDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current address';

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveInDate() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveInDate() ) && 1 != CValidation::checkDate( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date must be in mm/dd/yyyy form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveOutDate() ) && true == $boolIsValidateRequiredField ) {

			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) && true == $boolIsValidateRequiredField ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( 'Current address move-out date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveOutDate() ) && 1 != CValidation::checkDate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current address';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( '{%s,0} move-out date must be in mm/dd/yyyy form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && 0 != strlen( $this->getMoveInDate() && 0 != strlen( $this->getMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getMoveOutDate() ) ) <= ( strtotime( $this->getMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentAmount( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( ( 0 == strlen( $this->getMonthlyPaymentAmount() ) || 0 > $this->getMonthlyPaymentAmount() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( 'Current address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentRecipient( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPaymentRecipient() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( 'Current address monthly payment recipient is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressResidenceType( $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCompanyResidenceTypeId() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( '{%s,0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( 'Current address residence type is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid 			= true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getReasonForLeaving() ) ) ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( '{%s,0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( 'Current address reason for leaving is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressIsOwner( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( true == is_null( $this->getIsOwner() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_is_owner', __( '{%s,0} owned/rented is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_is_owner', __( 'Current address owned/rented is required..' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valCurrentAddressCommunityName( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCommunityName() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( '{%s,0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( 'Current address community name is required..' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valFaxNumber( $boolCanBeNull = false, $strSectionName = NULL ) {
		$boolIsValid = true;

		$intLength 		= strlen( $this->m_strFaxNumber );
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Fax';

		if( false == $boolCanBeNull ) {
			if( true == is_null( $this->m_strFaxNumber ) || 0 == $intLength ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( '{%s,0} number is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( '{%s,0} number must be between 10 and 15 characters.', [ $strSectionName ] ), 504, [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;

		} elseif( true == isset( $this->m_strFaxNumber ) && false == ( CValidation::validateFullPhoneNumber( $this->m_strFaxNumber, false ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Valid {%s,0} number is required.', [ $strSectionName ] ), 504, [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousAddressResidenceType( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCompanyResidenceTypeId() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( '{%s,0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( 'Previous Address residence type is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddress( $strSectionName = NULL, $strStepName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 'addl_info' == $strStepName ) {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsNonUsAddress;
		} else {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsAlien;
		}

		if( 0 == strlen( $this->m_strStreetLine1 ) && 0 == strlen( $this->m_strStreetLine2 ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line2', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} elseif( 0 < $this->getIsNonUsAddress() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line2', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line3', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous street is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line2', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		if( false == $boolIsNonUsAddress ) {

			if( 0 == strlen( $this->m_strCity ) && true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( 'Previous city is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valPreviousPostalCode( $strSectionName, $boolIsValidateRequiredField );

			if( 0 == strlen( $this->m_strStateCode ) && true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( 'Previous state/province code is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveInDate( $boolIsRequired = false,  $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous address';

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveInDate() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous address move-in date required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveInDate() ) && 1 !== CValidation::checkDate( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date must be in mm/dd/yyyy form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous address';

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveOutDate() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( 'Previous address move-out date is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveOutDate() ) && 1 !== CValidation::checkDate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date must be in mm/dd/yyyy form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( strtotime( $this->getMoveOutDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && 0 != strlen( $this->getMoveInDate() && 0 != strlen( $this->getMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getMoveOutDate() ) ) <= ( strtotime( $this->getMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentAmount( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( ( ( 0 == strlen( $this->getMonthlyPaymentAmount() ) || 0 > $this->getMonthlyPaymentAmount() ) ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( 'Previous address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentRecipient( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPaymentRecipient() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( 'Previous address monthly payment recipient is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPostalCode() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( false == CValidation::validatePostalCode( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->strtolower( $strSectionName ) : 'previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( 'Invalid {%s,0} zip/postal code.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid 			= true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getReasonForLeaving() ) ) ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( '{%s,0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( 'Previous address reason for leaving is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressIsOwner( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( true == is_null( $this->getIsOwner() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_is_owner', __( '{%s,0} owned/rented is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_is_owner', __( 'Previous address owned/rented is required..' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valPreviousAddressCommunityName( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCommunityName() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( '{%s,0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( 'Previous address community name is required..' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function isBlankData() {

		$boolIsBlank = ( bool ) ( false == valStr( $this->getStreetLine1() ) && false == valStr( $this->getStreetLine2() ) && ( ( true == $this->getIsNonUsAddress() && false == valStr( $this->getStreetLine3() ) ) || ( false == $this->getIsNonUsAddress() && false == valStr( $this->getCity() ) && false == valStr( $this->getStateCode() ) && false == valStr( $this->getPostalCode() ) ) ) );

		switch( $this->getAddressTypeId() ) {
			case CAddressType::CURRENT:
			 	$boolIsBlank &= ( bool ) ( false == valStr( $this->getCompanyResidenceTypeId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCommunityName() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveInDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMonthlyPaymentAmount() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getPaymentRecipient() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutReasonListItemId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getIsOwner() ) );
				if( 0 == ( int ) $this->getMoveOutReasonListItemId() ) {
					$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				}
				break;

			case CAddressType::PREVIOUS:
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCompanyResidenceTypeId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCommunityName() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveInDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMonthlyPaymentAmount() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getPaymentRecipient() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutReasonListItemId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getIsOwner() ) );
				if( 0 == ( int ) $this->getMoveOutReasonListItemId() ) {
					$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				}
				break;

			default:
				// #code...
				break;
		}
		return  $boolIsBlank;
	}

}
?>