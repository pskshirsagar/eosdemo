<?php

class CBudgetGlAccountMonthNote extends CBaseBudgetGlAccountMonthNote {

	protected $m_strCompanyEmployeeFullName;
	protected $m_objBudgetGlAccountMonthNote;

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrValues['company_employee_full_name'] ) ) $this->setCompanyEmployeeFullName( $arrValues['company_employee_full_name'] );
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetGlAccountMonthNoteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetGlAccountMonthId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoteDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_variance_note':
				$boolIsValid = true;

				if( true == is_null( $this->m_objBudgetGlAccountMonthNote->getNote() ) ) {
					$boolIsValid = false;
					$this->m_objBudgetGlAccountMonthNote->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
				}

				return $boolIsValid;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>