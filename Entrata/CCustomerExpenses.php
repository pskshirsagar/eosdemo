<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExpenses
 * Do not add any new functions to this class.
 */

class CCustomerExpenses extends CBaseCustomerExpenses {

	public static function fetchCustomerExpenseByIdByCid( $intExpenseId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ce.*,
						ce.id AS expense_id,
						( c.name_last || \', \' || c.name_first ) AS household_member_name,
						et.name AS expense_name,
						COALESCE ( ce.institution_street_line1,
							COALESCE ( ce.institution_street_line2,
								COALESCE ( ce.institution_city,
									COALESCE ( ce.institution_state_code,
										COALESCE ( ce.institution_postal_code, NULL )
									)
								)
							)
						) AS address
					FROM
						customer_expenses ce
						JOIN expense_types et ON ( et.id = ce.expense_type_id )
						JOIN customers c ON ( ce.cid = c.cid AND ce.customer_id = c.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id = ' . ( int ) $intExpenseId;

		return self::fetchCustomerExpense( $strSql, $objDatabase );
	}

	public static function fetchCustomerExpensesByStatusByLeaseIdByCid( $strStatus, $intLeaseId, $intCid, $objDatabase ) {

		$strWhereCondition = '';

		if( $strStatus == CCustomerExpense::CURRENT || $strStatus == '' ) {
			$strWhereCondition = 'AND
				( DATE (to_char(ce.expense_effective_date, \'YYYY-MM-DD\')) <= NOW() AND (DATE (to_char(ce.date_ended, \'YYYY-MM-DD\')) >= NOW() OR DATE (to_char(ce.date_ended, \'YYYY-MM-DD\')) IS NULL)
				)';
		}

		if( $strStatus == CCustomerExpense::PAST ) {
			$strWhereCondition = 'AND ( DATE (to_char(ce.date_ended, \'YYYY-MM-DD\')) < NOW() )';
		}

		if( $strStatus == CCustomerExpense::FUTURE ) {
			$strWhereCondition = 'AND ( DATE (to_char(ce.expense_effective_date, \'YYYY-MM-DD\')) > NOW() )';
		}

		$strSql = ' SELECT
						ce.*,
						ce.id AS expense_id,
						et.name AS expense_name,
						( c.name_last || \', \' || c.name_first ) AS household_member_name
					FROM
						customer_expenses ce
						JOIN expense_types et ON ( et.id = ce.expense_type_id )
						JOIN lease_customers lc ON ( ce.cid = lc.cid AND ce.customer_id = lc.customer_id )
						JOIN customers c ON ( lc.cid = c.cid AND lc.customer_id = c.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND lc.lease_id = ' . ( int ) $intLeaseId . '
						' . $strWhereCondition . '
						AND ce.deleted_on IS NULL
						AND ce.deleted_by IS NULL
					ORDER BY
						ce.expense_effective_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentCustomerExpensesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $strDesiredSubsidyCertificationEffectiveDate = 'NOW()', $strPossibleSubsidyCertificationEffectiveThroughDate = NULL ) {

		$strDesiredSubsidyCertificationEffectiveDate = ( false == valStr( $strDesiredSubsidyCertificationEffectiveDate ) ) ? 'NOW()' : $strDesiredSubsidyCertificationEffectiveDate;
		$strPossibleSubsidyCertificationEffectiveThroughDate = ( true == valStr( $strPossibleSubsidyCertificationEffectiveThroughDate ) ) ? '\'' . $strPossibleSubsidyCertificationEffectiveThroughDate . '\'::date' : '\'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date + INTERVAL \'1 YEAR -1 DAY\'';

		$strWhereCondition = 'AND
								(
									DATE ( to_char( ce.expense_effective_date, \'YYYY-MM-DD\' ) ) <= ' . $strPossibleSubsidyCertificationEffectiveThroughDate . ' 
									AND
									(
										DATE ( to_char( ce.date_ended, \'YYYY-MM-DD\') ) >= \'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date 
										OR
										DATE ( to_char( ce.date_ended, \'YYYY-MM-DD\') ) IS NULL
									)
								)';

		$strSql = '	SELECT
						ce.*
					FROM
						customer_expenses ce
						JOIN customers c ON ( ce.customer_id = c.id AND ce.cid = c.cid )
						JOIN applicants a ON ( c.id = a.customer_id AND c.cid = a.cid )
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE
						aa.application_id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND aa.cid =' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ce.deleted_by IS NULL
						AND ce.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerExpensesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolIsShowCategorizedInfo = false ) {

		$strCaseStatement = '';
		$strOrderBy = ' ORDER BY ce.expense_effective_date';
		if( true == $boolIsShowCategorizedInfo ) {
			$strOrderBy = ' ORDER BY subq.status, subq.expense_effective_date';
			$strCaseStatement = ' ,CASE
							        WHEN ce.date_ended < CURRENT_DATE THEN \'' . __( 'Past' ) . '\'
							        WHEN ce.expense_effective_date > CURRENT_DATE THEN \'' . __( 'Future' ) . '\'
							        ELSE \'' . __( 'Current' ) . '\'
                                END as status';
		}

		$strSql = ' SELECT
						ce.*,
						et.name AS expense_name ' . $strCaseStatement . '
					FROM
						customer_expenses ce
						JOIN expense_types et ON ( et.id = ce.expense_type_id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.customer_id = ' . ( int ) $intCustomerId . '
						AND ce.deleted_on IS NULL
						AND ce.deleted_by IS NULL';

		$strSql = ( true == $boolIsShowCategorizedInfo ) ? 'SELECT * FROM ( ' . $strSql . ' ) as subq ' . $strOrderBy : $strSql . $strOrderBy;

		return self::fetchCustomerExpenses( $strSql, $objDatabase );
	}

	public static function fetchCustomerExpenseByCustomerIdByExpenseTypeIdByAmountByCid( $intCustomerId, $intExpenseTypeId, $intAmount, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						customer_expenses
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND expense_type_id = ' . ( int ) $intExpenseTypeId . '
						AND amount = ' . ( int ) $intAmount . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						LIMIT 1;';

		return self::fetchCustomerExpense( $strSql, $objDatabase );
	}

	public static function fetchCustomerExpensesCountByCustomerIdByIncomeIdByCid( $intCustomerIncomeId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						count( id )
					FROM
						customer_expenses ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ' . ( int ) $intCustomerIncomeId . ' = ANY( ce.enabled_customer_income_ids )
						AND ce.deleted_on IS NULL
						AND ce.deleted_by IS NULL
					';
		$arrintExpensesCount = fetchData( $strSql, $objDatabase );
		return $arrintExpensesCount[0]['count'];
	}

	public static function fetchWorkCareCodeDataByCustomerIdsByCid( array $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = '	SELECT
						ci.customer_id,
						array_agg( DISTINCT ce.expense_type_id ) AS expense_types,
						count( DISTINCT ce.expense_type_id ) AS total_expense_types
					FROM
						customer_incomes ci
						JOIN income_types it ON ( ci.income_type_id = it.id )
						JOIN income_type_groups itg ON ( it.income_type_group_id = itg.id )
						LEFT JOIN customer_expenses ce ON ( ci.cid = ce.cid )
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND ci.customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND ci.deleted_on IS NULL
						AND ci.deleted_by IS NULL
						AND itg.is_subsidy = TRUE
						AND itg.is_published = TRUE
						AND itg.id = ' . CIncomeTypeGroup::EMPLOYMENT . '
						AND ce.expense_type_id IN ( ' . CExpenseType::CHILD_CARE_WORK . ', ' . CExpenseType::DISABILITY . ' )
						AND ce.deleted_by IS NULL
						AND ce.deleted_on IS NULL
						AND ce.enabled_customer_income_ids IS NOT NULL
						AND ci.id = ANY( ce.enabled_customer_income_ids )
						AND ( ( DATE( to_char( ci.date_started, \'YYYY-MM-DD\' ) ) IS NULL AND DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) ) IS NULL )
							OR
							(
								DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) ) <= NOW()
								AND ( DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) >= NOW() OR DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) IS NULL )
							)
							OR
							(
								DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) )  IS NULL
								AND DATE( to_char( ci.date_started, \'YYYY-MM-DD\' ) ) <= NOW()
								AND ( DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) >= NOW() OR DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) IS NULL )
							) )
						AND
							( DATE ( to_char( ce.expense_effective_date, \'YYYY-MM-DD\' ) ) <= NOW() AND ( DATE ( to_char( ce.date_ended, \'YYYY-MM-DD\' ) ) >= NOW() OR DATE ( to_char( ce.date_ended, \'YYYY-MM-DD\' ) ) IS NULL )
							)
					GROUP BY ci.customer_id';

		$arrmixWorkCareCodeData = fetchData( $strSql, $objDatabase );
		$arrmixWorkCareCodeData = rekeyArray( 'customer_id', $arrmixWorkCareCodeData, NULL, false );

		$arrmixWorkCareCodes = [];
			foreach( $arrintCustomerIds as $intCustomerId ) {
				$arrmixWorkCareCodes[$intCustomerId] = '';
				if( isset( $arrmixWorkCareCodeData[$intCustomerId] ) && 2 == $arrmixWorkCareCodeData[$intCustomerId]['total_expense_types'] ) {
					$arrmixWorkCareCodes[$intCustomerId] = 'CH';
				} elseif( isset( $arrmixWorkCareCodeData[$intCustomerId] ) && 1 == $arrmixWorkCareCodeData[$intCustomerId]['total_expense_types'] ) {
					$arrmixWorkCareCodes[$intCustomerId] = 'C';
					if( CExpenseType::DISABILITY == $arrmixWorkCareCodeData[$intCustomerId]['expense_types'][1] ) {
						$arrmixWorkCareCodes[$intCustomerId] = 'H';
					}
				}
			}

		return $arrmixWorkCareCodes;

	}

	public static function fetchCustomerExpensesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = ' SELECT
						ce.*
					FROM
						customer_expenses ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND ce.deleted_on IS NULL
						AND ce.deleted_by IS NULL
					ORDER BY
						ce.id';

		return self::fetchCustomerExpenses( $strSql, $objDatabase );
	}

}
?>