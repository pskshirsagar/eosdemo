<?php

class CDataExportLog extends CBaseDataExportLog {

	protected $m_strDataExportStatusName;

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrValues['data_export_status_name'] ) ) $this->setDataExportStatusName( $arrValues['data_export_status_name'] );
    }

    public function setDataExportStatusName( $strDataExportStatusName ) {
    	$this->m_strDataExportStatusName = $strDataExportStatusName;
    }

    public function getDataExportStatusName() {
    	return $this->m_strDataExportStatusName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataExportScheduleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataExportStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }
}
?>