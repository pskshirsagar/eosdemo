<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryUnitCdmpStatusTypes
 * Do not add any new functions to this class.
 */

class CMilitaryUnitCdmpStatusTypes extends CBaseMilitaryUnitCdmpStatusTypes {

	public static function fetchMilitaryUnitCdmpStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMilitaryUnitCdmpStatusType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMilitaryUnitCdmpStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMilitaryUnitCdmpStatusType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMilitaryUnitCdmpStatusTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM military_unit_cdmp_status_types';
		return self::fetchMilitaryUnitCdmpStatusTypes( $strSql, $objDatabase );
	}

}
?>