<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAdministrativeMenuTypes
 * Do not add any new functions to this class.
 */

class CAdministrativeMenuTypes extends CBaseAdministrativeMenuTypes {

	public static function fetchAdministrativeMenuTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAdministrativeMenuType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAdministrativeMenuType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAdministrativeMenuType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>