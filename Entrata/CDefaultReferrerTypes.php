<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReferrerTypes
 * Do not add any new functions to this class.
 */

class CDefaultReferrerTypes extends CBaseDefaultReferrerTypes {

	public static function fetchDefaultReferrerTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultReferrerType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultReferrerType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultReferrerType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>