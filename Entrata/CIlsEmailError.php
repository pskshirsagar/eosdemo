<?php

class CIlsEmailError extends CBaseIlsEmailError {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIlsEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIlsEmailErrorTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valErrorDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valErrorDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>