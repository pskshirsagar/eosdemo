<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetDetails
 * Do not add any new functions to this class.
 */

class CAssetDetails extends CBaseAssetDetails {

	public static function fetchAssetDetailsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						asd.*
					FROM
						asset_details asd
						JOIN assets a ON ( asd.cid = a.cid AND asd.asset_id = a.id )
					WHERE
						asd.cid = ' . ( int ) $intCid . '
						AND asd.asset_number IS NOT NULL';

		return self::fetchAssetDetails( $strSql, $objClientDatabase );
	}

	public static function fetchAssetDetailByAssetIdByCid( $intAssetId, $intCid, $objClientDatabase ) {

		if( false == valId( $intAssetId ) ) {
			return;
		}

		$strSql = 'SELECT
						asd.*,
						ac.name AS asset_condition,
						CASE
							WHEN asd.maintenance_location_id IS NULL AND asd.unit_space_id IS NULL THEN
								al.name
							WHEN asd.maintenance_location_id IS NOT NULL AND asd.unit_space_id IS NULL THEN
								ml.name
							ELSE
								concat_ws( \' \', concat_ws ( \'-\', us.building_name, us.unit_number, us.space_number ), ml.name )
						END AS maintenance_location,
						at.transaction_amount as cost,
						ad.ap_header_id,
						dc.is_depreciable,
						dc.id AS depreciation_category_id
					FROM
						asset_details asd
						JOIN assets a ON ( asd.cid = a.cid AND asd.asset_id = a.id )
						JOIN ap_codes apc ON ( a.cid = apc.cid AND a.ap_code_id = apc.id )
						LEFT JOIN unit_spaces us ON ( asd.cid = us.cid AND asd.unit_space_id = us.id )
						LEFT JOIN asset_conditions ac ON ( asd.cid = ac.cid AND asd.asset_condition_id = ac.id )
						LEFT JOIN maintenance_locations ml ON ( asd.cid = ml.cid AND asd.maintenance_location_id = ml.id )
						LEFT JOIN asset_transactions at ON ( asd.cid = at.cid AND asd.asset_id = at.asset_id )
						LEFT JOIN ap_details ad ON ( ad.cid = at.cid AND ad.id = at.invoice_ap_detail_id )
						LEFT JOIN asset_locations al ON ( a.cid = al.cid AND a.asset_location_id = al.id )
						LEFT JOIN depreciation_categories dc ON ( dc.cid = asd.cid AND dc.id = apc.depreciation_category_id )
					WHERE
						asd.cid = ' . ( int ) $intCid . '
						AND asd.asset_id = ' . ( int ) $intAssetId . '
					ORDER BY
						at.created_on';

		return self::fetchAssetDetail( $strSql, $objClientDatabase );
	}

	public static function fetchAssetDetailsCountByPropertyIdsByAssetListsFilterByCid( $arrintPropertyIds, $objAssetListsFilter, $intCid, $objClientDatabase, $arrintDepreciationCategoryIds = [], $arrintAssetTypeIds = [], $arrintAssetStatusesIds = [], $boolIsSearch = false, $boolIsReset = false ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $intCid, $arrintPropertyIds, $objAssetListsFilter, $arrintDepreciationCategoryIds, $arrintAssetTypeIds, $arrintAssetStatusesIds, $boolIsSearch, $boolIsReset );

		$strSql = $arrstrSqlCondition['temporary_table'];

		unset( $arrstrSqlCondition['temporary_table'] );

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSql .= 'SELECT
						COUNT( tas.id )
					FROM
						tmp_asset_list AS tas
						LEFT JOIN unit_spaces us ON ( tas.cid = us.cid AND tas.unit_space_id = us.id ) 
						LEFT JOIN depreciation_categories dc ON ( dc.cid = tas.cid AND dc.id = tas.depreciation_category_id ) 
						LEFT JOIN asset_locations al ON ( tas.cid = al.cid AND tas.asset_location_id = al.id ) 
						LEFT JOIN maintenance_locations ml ON ( tas.cid = ml.cid AND tas.maintenance_location_id = ml.id ) 
						LEFT JOIN asset_transactions at ON ( tas.cid = at.cid AND tas.asset_id = at.asset_id )
					WHERE
						tas.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	/**
	 * @param       $arrintPropertyIds
	 * @param       $objAssetListsFilter
	 * @param array $arrintDepreciationCategoryIds
	 * @param array $arrintAssetTypeIds
	 * @param array $arrintAssetStatusesIds
	 * @param bool  $boolIsSearch
	 * @param bool  $boolIsReset
	 * @return array
	 */

	public static function fetchSearchCriteria( $intCid, $arrintPropertyIds, $objAssetListsFilter, $arrintDepreciationCategoryIds = [], $arrintAssetTypeIds = [], $arrintAssetStatusesIds = [], $boolIsSearch = false, $boolIsReset = false ) {

		$arrstrSqlCondition = [];
		// create a temporary table for asset_list to prepare the data and add custom indexes
		$arrstrSqlCondition['temporary_table'] = self::createTemporaryTableAssetList( $intCid, $arrintPropertyIds, $objAssetListsFilter );

		$arrintApCodeIds = [];
		$arrintUnitSpaceIds = [];

		// from fixed assets listing filter
		if( true == valId( $objAssetListsFilter->getApCodeId() ) ) {
			$arrintApCodeIds[] = $objAssetListsFilter->getApCodeId();
		}

		if( true == valId( $objAssetListsFilter->getUnitSpaceIds() ) ) {
			$arrintUnitSpaceIds[] = $objAssetListsFilter->getUnitSpaceIds();
		}

		// from bulk transfer fixed assets
		$arrintApCodeIds = array_merge( $arrintApCodeIds, ( array ) $objAssetListsFilter->getApCodeIds() );
		if( true == valIntArr( $arrintApCodeIds ) ) {
			$arrstrSqlCondition[] = 'tas.ap_code_id IN ( ' . sqlIntImplode( $arrintApCodeIds ) . ' )';
		}

		if( true == valStr( $objAssetListsFilter->getSerialNumber() ) ) {
			$arrstrSqlCondition[] = 'tas.serial_number = \'' . addslashes( $objAssetListsFilter->getSerialNumber() ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getAssetNumber() ) ) {
			$arrstrSqlCondition[] = 'tas.asset_number = \'' . addslashes( $objAssetListsFilter->getAssetNumber() ) . '\'';
		}

		$arrintUnitSpaceIds = array_unique( array_merge( $arrintUnitSpaceIds, ( array ) $objAssetListsFilter->getUnitSpaceIds() ) );

		if( true == valIntArr( $arrintUnitSpaceIds ) ) {
			$arrstrSqlCondition[] = 'tas.unit_space_id IN ( ' . sqlIntImplode( $arrintUnitSpaceIds ) . '  )';
		}

		if( true == valStr( $objAssetListsFilter->getPurchaseDateFrom() ) && true == CValidation::validateDate( $objAssetListsFilter->getPurchaseDateFrom() ) ) {
			$arrstrSqlCondition[] = 'tas.purchased_on >= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getPurchaseDateFrom() ) ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getPurchaseDateTo() ) && true == CValidation::validateDate( $objAssetListsFilter->getPurchaseDateTo() ) ) {
			$arrstrSqlCondition[] = 'tas.purchased_on <= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getPurchaseDateTo() ) ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getPlacedInServiceDateFrom() ) && true == CValidation::validateDate( $objAssetListsFilter->getPlacedInServiceDateFrom() ) ) {
			$arrstrSqlCondition[] = 'tas.placed_in_service_on >= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getPlacedInServiceDateFrom() ) ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getPlacedInServiceDateTo() ) && true == CValidation::validateDate( $objAssetListsFilter->getPlacedInServiceDateTo() ) ) {
			$arrstrSqlCondition[] = 'tas.placed_in_service_on <= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getPlacedInServiceDateTo() ) ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getWarrantyExpirationDateFrom() ) && true == CValidation::validateDate( $objAssetListsFilter->getWarrantyExpirationDateFrom() ) ) {
			$arrstrSqlCondition[] = 'tas.warranty_end_date >= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getWarrantyExpirationDateFrom() ) ) . '\'';
		}

		if( true == valStr( $objAssetListsFilter->getWarrantyExpirationDateTo() ) && true == CValidation::validateDate( $objAssetListsFilter->getWarrantyExpirationDateTo() ) ) {
			$arrstrSqlCondition[] = 'tas.warranty_end_date <= \'' . date( 'Y-m-d', strtotime( $objAssetListsFilter->getWarrantyExpirationDateTo() ) ) . '\'';
		}

		if( true == $boolIsSearch && true == valArr( $objAssetListsFilter->getAssetStatusIds() ) && \Psi\Libraries\UtilFunctions\count( CAssetDetail::$c_arrstrAssetStatuses ) >= \Psi\Libraries\UtilFunctions\count( $objAssetListsFilter->getAssetStatusIds() ) ) {
			$arrstrServiceConditions = [];

			if( true == in_array( CAssetDetail::NOT_IN_SERVICE, $objAssetListsFilter->getAssetStatusIds() ) ) {
				$arrstrServiceConditions[] = '( tas.placed_in_service_on IS NULL AND tas.purchased_by IS NOT NULL AND tas.retired_by IS NULL AND tas.returned_by IS NULL )';
			}
			if( true == in_array( CAssetDetail::IN_SERVICE, $objAssetListsFilter->getAssetStatusIds() ) ) {
				$arrstrServiceConditions[] = ' ( tas.placed_in_service_on IS NOT NULL AND tas.retired_by IS NULL AND tas.returned_by IS NULL )';
			}
			if( true == in_array( CAssetDetail::RETIRED, $objAssetListsFilter->getAssetStatusIds() ) ) {
				$arrstrServiceConditions[] = '( tas.retired_on IS NOT NULL )';
			}
			if( true == in_array( CAssetDetail::RETURNED, $objAssetListsFilter->getAssetStatusIds() ) ) {
				$arrstrServiceConditions[] = '( tas.returned_on IS NOT NULL )';
			}
			if( true == valArr( $arrstrServiceConditions ) ) {
				$arrstrSqlCondition[] = ' ( ' . implode( ' OR ', $arrstrServiceConditions ) . ' ) ';
			}
		} elseif( ( false == $boolIsSearch && true == valArr( $arrintAssetStatusesIds ) ) || ( true == $boolIsReset && true == valArr( $arrintAssetStatusesIds ) ) ) {
			$arrstrServiceConditions = [];

			if( true == in_array( CAssetDetail::NOT_IN_SERVICE, $arrintAssetStatusesIds ) ) {
				$arrstrServiceConditions[] = '( tas.placed_in_service_on IS NULL AND tas.purchased_by IS NOT NULL AND tas.retired_by IS NULL AND tas.returned_by IS NULL )';
			}
			if( true == in_array( CAssetDetail::IN_SERVICE, $arrintAssetStatusesIds ) ) {
				$arrstrServiceConditions[] = '( tas.placed_in_service_on IS NOT NULL AND tas.retired_by IS NULL AND tas.returned_by IS NULL )';
			}
			if( true == in_array( CAssetDetail::RETIRED, $arrintAssetStatusesIds ) ) {
				$arrstrServiceConditions[] = '( tas.retired_on IS NOT NULL )';
			}
			if( true == in_array( CAssetDetail::RETURNED, $arrintAssetStatusesIds ) ) {
				$arrstrServiceConditions[] = '( tas.returned_on IS NOT NULL )';
			}
			if( true == valArr( $arrstrServiceConditions ) ) {
				$arrstrSqlCondition[] = ' ( ' . implode( ' OR ', $arrstrServiceConditions ) . ' ) ';
			}
		}

		if( ( true == $boolIsSearch && true == valArr( $objAssetListsFilter->getAssetTypeIds() ) ) || ( true == valArr( $objAssetListsFilter->getAssetTypeIds() ) && false == is_null( $boolIsScroll ) ) ) {
			$arrstrAssetTypeConditions = [];
			if( true == in_array( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, $objAssetListsFilter->getAssetTypeIds() ) ) {
				$arrstrAssetTypeConditions[] = 'tas.asset_type_id = 1';
			}

			if( ( true == in_array( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, $objAssetListsFilter->getAssetTypeIds() ) && CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == \Psi\Libraries\UtilFunctions\count( $objAssetListsFilter->getAssetTypeIds() ) )
			    || \Psi\Libraries\UtilFunctions\count( CAssetType::$c_arrintAssetTypeIds ) == \Psi\Libraries\UtilFunctions\count( $objAssetListsFilter->getAssetTypeIds() ) ) {
				if( true == valArr( $objAssetListsFilter->getDepreciationCategoryIds() ) ) {
					$arrstrAssetTypeConditions[] = ' dc.id IN ( ' . sqlIntImplode( $objAssetListsFilter->getDepreciationCategoryIds() ) . ' ) ';
				}
			} elseif( true == valArr( $objAssetListsFilter->getDepreciationCategoryIds() ) ) {
				$arrstrAssetTypeConditions[] = ' dc.id IN ( ' . sqlIntImplode( $objAssetListsFilter->getDepreciationCategoryIds() ) . ' ) ';
			}

			if( true == valArr( $arrstrAssetTypeConditions ) ) {
				$arrstrTempAssetTypeConditions = $arrstrAssetTypeConditions;
				$arrstrAssetTypeConditions = [];
				$arrstrAssetTypeConditions[] = ' ( ' . implode( ' OR ', [ ' ( ' . implode( ' AND ', $arrstrTempAssetTypeConditions ) . ' )' ] ) . ' ) ';
			}

			 if( true == in_array( CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED, $objAssetListsFilter->getAssetTypeIds() ) ) {
				 $arrstrAssetTypeConditions[] = 'tas.asset_type_id = 2';
			 }
			$arrstrSqlCondition[] = ' ( ' . implode( ' OR ', $arrstrAssetTypeConditions ) . ' ) ';
		} elseif( ( 0 == \Psi\Libraries\UtilFunctions\count( $objAssetListsFilter->getAssetTypeIds() ) && true == valArr( $objAssetListsFilter->getDepreciationCategoryIds() ) ) || ( true == valArr( $objAssetListsFilter->getDepreciationCategoryIds() ) && false == is_null( $boolIsScroll ) ) ) {
			$arrstrSqlCondition[] = ' dc.id IN ( ' . sqlIntImplode( $objAssetListsFilter->getDepreciationCategoryIds() ) . ' ) ';
		} elseif( true == valArr( $arrintAssetTypeIds ) && ( false == $boolIsSearch || true == $boolIsReset ) ) {
			$arrstrAssetTypeConditions = [];
			$boolAssetTypeCapitalized = in_array( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, $arrintAssetTypeIds );
			$boolAssetTypeExpensed = in_array( CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED, $arrintAssetTypeIds );
			if( true == $boolAssetTypeCapitalized && false == $boolAssetTypeExpensed ) {
				$intAssetTypeCount = 1;
			}
			if( true == $boolAssetTypeCapitalized ) {
				$arrstrAssetTypeConditions[] = 'tas.asset_type_id = 1';
			}
			if( ( true == $boolAssetTypeCapitalized && $intAssetTypeCount == \Psi\Libraries\UtilFunctions\count( $arrintAssetTypeIds ) ) || \Psi\Libraries\UtilFunctions\count( CAssetType::$c_arrintAssetTypeIds ) == \Psi\Libraries\UtilFunctions\count( $arrintAssetTypeIds ) ) {
				$arrstrAssetTypeConditions[] = ' dc.id IN ( ' . sqlIntImplode( $arrintDepreciationCategoryIds ) . ' ) ';
			} else {
				$arrstrAssetTypeConditions[] = ' dc.id IN ( ' . sqlIntImplode( $arrintAssetTypeIds ) . ' ) ';
			}
			$arrstrTempAssetTypeConditions = $arrstrAssetTypeConditions;
			$arrstrAssetTypeConditions = [];
			$arrstrAssetTypeConditions[] = ' ( ' . implode( ' OR ', [ ' ( ' . implode( ' AND ', $arrstrTempAssetTypeConditions ) . ' )' ] ) . ' ) ';
			if( true == $boolAssetTypeExpensed ) {
				$arrstrAssetTypeConditions[] = 'tas.asset_type_id = 2';
			}
			$arrstrSqlCondition[] = ' ( ' . implode( ' OR ', $arrstrAssetTypeConditions ) . ' ) ';
		}

		if( true == valId( $objAssetListsFilter->getMaintenanceLocationId() )
			&& true == valId( $objAssetListsFilter->getAssetLocationId() ) ) {

			$arrstrSqlCondition[] = '( tas.maintenance_location_id = ' . $objAssetListsFilter->getMaintenanceLocationId() . ' OR tas.asset_location_id = ' . $objAssetListsFilter->getAssetLocationId() . ' )';
		} elseif( true == valId( $objAssetListsFilter->getMaintenanceLocationId() ) ) {

			$arrstrSqlCondition[] = 'tas.maintenance_location_id = ' . $objAssetListsFilter->getMaintenanceLocationId();
		} elseif( true == valId( $objAssetListsFilter->getAssetLocationId() ) ) {

			$arrstrSqlCondition[] = 'tas.asset_location_id = ' . $objAssetListsFilter->getAssetLocationId();
		}

		if( true == valIntArr( $objAssetListsFilter->getApCodeCategoryId() ) ) {
			$arrstrSqlCondition[] = ' tas.ap_code_category_id IN ( ' . sqlIntImplode( $objAssetListsFilter->getApCodeCategoryId() ) . ' ) ';
		}

		return $arrstrSqlCondition;
	}

	public static function createTemporaryTableAssetList( $intCid, $arrintPropertyIds, $objAssetListsFilter ) {

		$arrintSqlPropertyIds = [];
		if( true == valArr( $objAssetListsFilter->getPropertyIds() ) ) {
			$arrintSqlPropertyIds = $objAssetListsFilter->getPropertyIds();
		} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
			$arrintSqlPropertyIds = $arrintPropertyIds;
		}

		$strLoadPropertyJoin = '';

		if( true == valId( $objAssetListsFilter->getPropertyId() ) ) {
			$arrintSqlPropertyIds[] = $objAssetListsFilter->getPropertyId();
		}

		$strLoadPropertiesJoin = '';
		$strPropertiesSelectClause = 'NULL AS property_id, NULL AS property_name,';
		if( true == valIntArr( $arrintSqlPropertyIds ) ) {
			$strPropertiesSelectClause = 'p.id AS property_id, p.property_name,';
			$strLoadPropertiesJoin = 'JOIN load_properties( ARRAY[' . ( int ) $intCid . ']::int[], ARRAY[' . sqlIntImplode( $arrintSqlPropertyIds ) . ']::int[], ARRAY[' . CPsProduct::ENTRATA . ']::int[] ) AS lp ON( lp.cid = a.cid AND a.property_id = lp.property_id )
						JOIN properties p ON ( lp.cid = p.cid AND lp.property_id = p.id AND a.cid = p.cid AND lp.property_id = p.id )';
		}

		$strSql = 'DROP TABLE IF EXISTS tmp_asset_list;
					CREATE TEMP TABLE
                        tmp_asset_list AS (
						SELECT
						  asd.*,
						  ac.name AS ap_code_name,
						  acc.name AS category_name,' . $strPropertiesSelectClause . '
						  a.asset_location_id,
						  ac.asset_type_id,
						  a.ap_code_id AS ap_code_id,
						  acc.id AS ap_code_category_id,
						  a.serial_number
						FROM
						  assets AS a ' . $strLoadPropertiesJoin . ' ' . $strLoadPropertyJoin . '
						  JOIN asset_details AS asd ON ( asd.cid = a.cid AND asd.asset_id = a.id )
						  JOIN ap_codes AS ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id )
						  JOIN ap_code_categories AS acc ON ( acc.cid = ac.cid AND acc.id = ac.ap_code_category_id )
						WHERE
						  a.cid = ' . ( int ) $intCid . '
						  AND a.deleted_by IS NULL
						);
						CREATE INDEX idx_tbr_data_cte ON tmp_asset_list USING btree(ap_code_name, depreciation_category_id, unit_space_id, asset_location_id, ap_code_category_id, asset_type_id );
						ANALYZE tmp_asset_list ( ap_code_name, depreciation_category_id, unit_space_id, asset_location_id, ap_code_category_id, asset_type_id );';

		return $strSql;
	}

	public static function fetchIsRetiredAssetDetailCount( $intAssetId, $intCid, $objClientDatabase ) {

		$strSql = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND asset_id = ' . ( int ) $intAssetId . '
						AND retired_on IS NULL
						AND retired_by IS NULL';

		return self::fetchAssetDetailCount( $strSql, $objClientDatabase );

	}

	public static function fetchAssetDetailsByDepreciationCategoryIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						a.serial_number,
						asd.*
					FROM
						asset_details asd
						JOIN assets a ON ( asd.cid = a.cid AND asd.asset_id = a.id )
					WHERE
						asd.cid = ' . ( int ) $intCid . '
						AND asd.depreciation_category_id = ' . ( int ) $intId;

		 return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssetDetailsByPropertyIdsByAssetListsFilterByCid( $arrintPropertyIds, $objAssetListsFilter, $intCid, $objClientDatabase ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $intCid, $arrintPropertyIds, $objAssetListsFilter, [], [], [], true );

		$strSql = $arrstrSqlCondition['temporary_table'];

		unset( $arrstrSqlCondition['temporary_table'] );

		$strOrderBy = 'tas.ap_code_name';
		$strSortDirection = 'ASC';

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$arrstrSortBy = [
			'ap_code_name' => 'tas.ap_code_name',
			'asset_number' => 'tas.asset_number',
			'depreciation_category_name' => 'dc.name',
			'property_name' => 'tas.property_name',
			'asset_location_name' => 'asset_location_name',
			'purchased_on' => 'tas.purchased_on',
			'placed_in_service_on' => 'tas.placed_in_service_on',
			'retired_on' => 'tas.retired_on',
			'cost' => 'at.transaction_amount::float'
		];

		if( false == is_null( $strByColumnName = getArrayElementByKey( $objAssetListsFilter->getSortBy(), $arrstrSortBy ) ) && true == valStr( $objAssetListsFilter->getSortDirection() ) ) {
			$strSortDirection = $objAssetListsFilter->getSortDirection();
			$strOrderBy       = addslashes( $strByColumnName );
		}

		$strSql .= 'SELECT
						tas.*,
						dc.name AS depreciation_category_name,
						CASE
							WHEN tas.maintenance_location_id IS NULL AND tas.unit_space_id IS NULL THEN
								al.name
							WHEN tas.maintenance_location_id IS NOT NULL AND tas.unit_space_id IS NULL THEN
								ml.name
							ELSE
								concat_ws( \' \', concat_ws ( \'-\', us.building_name, us.unit_number, us.space_number ), ml.name )
						END AS asset_location_name,
						tas.property_name,
						at.transaction_amount as cost
					FROM
						tmp_asset_list AS tas
						LEFT JOIN unit_spaces AS us ON ( tas.cid = us.cid AND tas.unit_space_id = us.id )
						LEFT JOIN depreciation_categories AS dc ON ( dc.cid = tas.cid AND dc.id = tas.depreciation_category_id )
						LEFT JOIN asset_locations AS al ON ( tas.cid = al.cid AND tas.asset_location_id = al.id )
						LEFT JOIN maintenance_locations AS ml ON ( tas.cid = ml.cid AND tas.maintenance_location_id = ml.id )
						LEFT JOIN asset_transactions AS at ON ( tas.cid = at.cid AND tas.asset_id = at.asset_id )
					WHERE
						tas.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition . '
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		return self::fetchAssetDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnRetireAssetDetailByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						asset_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND asset_id = ' . ( int ) $intAssetId;

		return self::fetchAssetDetail( $strSql, $objDatabase );
	}

	public static function fetchFixedAssetsCountByFixedAssetListFilterByCid( $objAssetListsFilter, $intCid, $objClientDatabase ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $intCid, [], $objAssetListsFilter, [], [], [], true );

		$strSql = $arrstrSqlCondition['temporary_table'];

		unset( $arrstrSqlCondition['temporary_table'] );

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSql .= 'SELECT
						COUNT( tas.id )
					FROM
						tmp_asset_list tas
						JOIN assets a ON ( tas.cid = a.cid AND tas.asset_id = a.id )
						JOIN properties p ON ( a.cid = p.cid AND a.property_id = p.id )
						JOIN ap_codes ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id )
						JOIN ap_code_categories acc ON ( acc.cid = ac.cid AND acc.id = ac.ap_code_category_id )
						LEFT JOIN asset_locations al ON ( a.cid = al.cid AND a.asset_location_id = al.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchFixedAssetsByFixedAssetListFilterByCid( $objAssetListsFilter, $intCid, $objClientDatabase, $objPagination = NULL ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $intCid, [], $objAssetListsFilter, [], [], [], true );

		$strSql = $arrstrSqlCondition['temporary_table'];

		unset( $arrstrSqlCondition['temporary_table'] );

		$strOrderBy = 'tas.ap_code_name';
		$strSortDirection = 'ASC';

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$arrstrSortBy = [
			'ap_code_name' => 'tas.ap_code_name',
			'asset_number' => 'tas.asset_number',
			'category_name' => 'tas.category_name',
			'purchased_on' => 'tas.purchased_on',
			'cost' => 'at.transaction_amount::float',
			'serial_number' => 'tas.serial_number'
		];

		if( false == is_null( $strByColumnName = getArrayElementByKey( $objAssetListsFilter->getSortBy(), $arrstrSortBy ) ) && true == valStr( $objAssetListsFilter->getSortDirection() ) ) {
			$strSortDirection = $objAssetListsFilter->getSortDirection();
			$strOrderBy       = addslashes( $strByColumnName );
		}

		$strSql .= 'SELECT
						tas.*,
						at.transaction_amount as cost
					FROM
						tmp_asset_list tas
						LEFT JOIN asset_locations al ON ( tas.cid = al.cid AND tas.asset_location_id = al.id )
						LEFT JOIN asset_transactions at ON ( tas.cid = at.cid AND tas.asset_id = at.asset_id )
					WHERE
						tas.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition . '
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchAssetDetails( $strSql, $objClientDatabase );
	}

}
?>