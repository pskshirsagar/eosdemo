<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAlertTypes
 * Do not add any new functions to this class.
 */

class CAlertTypes extends CBaseAlertTypes {

    public static function fetchAlertTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CAlertType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAlertType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CAlertType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>