<?php

class CPropertyGlBalance extends CBasePropertyGlBalance {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCashBeginningBalance() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccrualBegginningBalance() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImportedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>