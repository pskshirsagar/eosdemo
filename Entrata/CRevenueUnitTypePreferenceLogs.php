<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypePreferenceLogs
 * Do not add any new functions to this class.
 */

class CRevenueUnitTypePreferenceLogs extends CBaseRevenueUnitTypePreferenceLogs {

	public static function fetchRevenueUnitTypePreferencesLogValueByKeyIdsByUnitTypeIdByPropertyIdByCid( $strSettingKey, $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strJoin    = '';
		$strSelect  = '';
		if( 'PRICING_VACANCY_SUSPENSION_CHARGE_CODE' == $strSettingKey ) {
			$strJoin = 'JOIN ar_codes ar ON ar.id = rutpl.new_value::integer';
			$strSelect = 'ar.name as charge_code_name, ';
		}

		$strSql = ' SELECT 
                        DISTINCT rutpl.id, rutpl.new_value, psk.key, psk.label, cu.username, rutpl.created_on, rutpl.created_by, ' . $strSelect . ' 
                        CASE rutpl.new_value
                            WHEN \'0\' THEN \'Off\'
                            WHEN \'0.5\' THEN \'Conservative\'
                            WHEN \'1\' THEN \'Moderate\'
                            WHEN \'1.5\' THEN \'Aggressive\'
                        END AS log_value,
                        CASE psk.key
                       WHEN \'PRICING_ENABLE_VACANCY_SUSPENSION\'
				        THEN 
				            CASE rutpl.new_value
				             WHEN \'0\' THEN \'No\'
                             WHEN \'1\' THEN \'Yes\'
				            END
				        END AS vss_log_value
					FROM 
						revenue_unit_type_preference_logs rutpl
					JOIN property_setting_keys psk on psk.id = rutpl.property_setting_key_id
					JOIN company_users cu ON cu.id = rutpl.created_by ' . $strJoin . ' 
					WHERE 
						rutpl.property_id = ' . ( int ) $intPropertyId . '
					     AND rutpl.unit_type_id  = ' . ( int ) $intUnitTypeId . '
	    			     AND rutpl.cid::integer = ' . ( int ) $intCid . '
	    			     AND psk.key = \'' . $strSettingKey . '\'
		            ORDER BY rutpl.id DESC 
		            LIMIT 10';
		return fetchData( $strSql, $objDatabase );
	}

}
?>