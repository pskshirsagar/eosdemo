<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderLogTypes
 * Do not add any new functions to this class.
 */

class CGlHeaderLogTypes extends CBaseGlHeaderLogTypes {

	public static function fetchGlHeaderLogType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlHeaderLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>