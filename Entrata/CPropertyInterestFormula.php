<?php

class CPropertyInterestFormula extends CBasePropertyInterestFormula {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCodeId() {
        $boolIsValid = true;

		if( true == is_null( $this->getArCodeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required.', NULL ) );
			$boolIsValid = false;
		}

        return $boolIsValid;
    }

    public function valArTriggerId() {
        $boolIsValid = true;

        if( true == is_null( $this->getArTriggerId() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', 'Pay interest to tenants at is required.', NULL ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valInterestQualificationDays() {
      $boolIsValid = true;

		if( false == valStr( $this->getInterestQualificationDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest is available to tenants after is required.' ) );
		} elseif( true == valStr( $this->getInterestQualificationDays() ) ) {
			if( false == is_numeric( $this->getInterestQualificationDays() ) && 0 >= $this->getInterestQualificationDays() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest is available to tenants after should be numeric.' ) );
			} elseif( 0 >= $this->getInterestQualificationDays() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest is available to tenants after should be greater than zero .' ) );
			} elseif( !( ( string ) round( $this->getInterestQualificationDays() ) == $this->getInterestQualificationDays() && false == \Psi\CStringService::singleton()->strpos( $this->getInterestQualificationDays(), '.' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest is available to tenants after should not be decimal.' ) );
			}
		}

		return $boolIsValid;
    }

	public function valDelinquencyInterestArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getArCodeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Delinquency Interest Charge Code is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valInterestAccruesAfter() {
		$boolIsValid = true;

		if( false == valStr( $this->getInterestQualificationDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest Accrues After is required.' ) );
		} elseif( true == valStr( $this->getInterestQualificationDays() ) ) {
			if( false == is_numeric( $this->getInterestQualificationDays() ) && 0 >= $this->getInterestQualificationDays() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest Accrues After should be numeric.' ) );
			} elseif( 0 >= $this->getInterestQualificationDays() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest Accrues After should be greater than zero .' ) );
			} elseif( !( ( string ) round( $this->getInterestQualificationDays() ) == $this->getInterestQualificationDays() && false == \Psi\CStringService::singleton()->strpos( $this->getInterestQualificationDays(), '.' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_qualification_days', 'Interest Accrues After should not be decimal.' ) );
			}
		}

		return $boolIsValid;
	}

    public function valInterestPeriodForfeitDays() {
    	$boolIsValid = true;
    	if( false == valStr( $this->getInterestPeriodForfeitDays() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_period_forfeit_days', 'Tenants forfeit interest after is required.' ) );
    	} elseif( true == valStr( $this->getInterestPeriodForfeitDays() ) ) {
    		if( false == is_numeric( $this->getInterestPeriodForfeitDays() ) && 0 >= $this->getInterestPeriodForfeitDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_period_forfeit_days', 'Tenants forfeit interest after should be numeric.' ) );
    		} elseif( 0 >= $this->getInterestPeriodForfeitDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_period_forfeit_days', 'Tenants forfeit interest after should be greater than zero .' ) );
    		} elseif( !( ( string ) round( $this->getInterestPeriodForfeitDays() ) == $this->getInterestPeriodForfeitDays() && false == \Psi\CStringService::singleton()->strpos( $this->getInterestPeriodForfeitDays(), '.' ) ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_period_forfeit_days', 'Tenants forfeit interest after should not be decimal.' ) );
    		} elseif( 365 < $this->getInterestPeriodForfeitDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_period_forfeit_days', 'Tenants forfeit interest after cannot be more than 365 days.' ) );
    		}
    	}
    	return $boolIsValid;
    }

    public function valInterestPaymentGraceDays() {

    	$boolIsValid = true;
    	if( false == valStr( $this->getInterestPaymentGraceDays() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_payment_grace_days', 'Automatically pay interest to Tenants within is required.' ) );
    	} elseif( true == valStr( $this->getInterestPaymentGraceDays() ) ) {
    		if( false == is_numeric( $this->getInterestPaymentGraceDays() ) && 0 >= $this->getInterestPaymentGraceDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_payment_grace_days', 'Automatically pay interest to Tenants within should be numeric.' ) );
    		} elseif( 0 >= $this->getInterestPaymentGraceDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_payment_grace_days', 'Automatically pay interest to Tenants within should be greater than zero .' ) );
    		} elseif( !( ( string ) round( $this->getInterestPaymentGraceDays() ) == $this->getInterestPaymentGraceDays() && false == \Psi\CStringService::singleton()->strpos( $this->getInterestPaymentGraceDays(), '.' ) ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_payment_grace_days', 'Automatically pay interest to Tenants within should not be decimal.' ) );
    		} elseif( 365 < $this->getInterestPaymentGraceDays() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_payment_grace_days', 'Automatically pay interest to Tenants within cannot be more than 365 days.' ) );
    		}
    	}
    	return $boolIsValid;

    }

    public function valDenyInterestForLeaseViolations() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGenerateRefundCheck() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterestPercent() {
        $boolIsValid = true;
    	if( false == valStr( $this->getInterestPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_percent', ' Interest rate is required.' ) );
		} elseif( true == valStr( $this->getInterestPercent() ) ) {
			if( false == is_numeric( $this->getInterestPercent() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_percent', 'Interest rate should be valid number.' ) );
				$boolIsValid = false;
			} elseif( 0 >= $this->getInterestPercent() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_percent', 'Interest rate should be greater than zero .' ) );
			} elseif( 6 < strlen( \Psi\CStringService::singleton()->substr( strrchr( $this->getInterestPercent(), '.' ), 1 ) ) ) {
				$boolIsValid = false;
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_percent', 'Interest rate should be up to 4 decimals .' ) );
			}
		}
        return $boolIsValid;
    }

    public function valCalculateInterest() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;
        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		if( true == $this->getCalculateInterest() ) {
        			$boolIsValid &= $this->valArCodeId();
        			$boolIsValid &= $this->valArTriggerId();
        			$boolIsValid &= $this->valInterestQualificationDays();
        			// As discussed, we are commenting this code for future reference.
        			// $boolIsValid &= $this->valInterestPaymentGraceDays();
        			// $boolIsValid &= $this->valInterestPeriodForfeitDays();
        			$boolIsValid &= $this->valInterestPercent();
        		}
				break;

        	case VALIDATE_DELETE:
        		break;

		    case 'VALIDATE_COMMERCIAL_INSERT':
			    $boolIsValid &= $this->valInterestAccruesAfter();
			    $boolIsValid &= $this->valInterestPercent();
			    $boolIsValid &= $this->valArCodeId();
			    break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>