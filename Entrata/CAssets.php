<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssets
 * Do not add any new functions to this class.
 */

class CAssets extends CBaseAssets {

	public static function fetchAssetByPropertyIdByApCodeIdByLocationIdByCid( $intPropertyId, $intApCodeId, $intAssetLocationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intApCodeId ) || false == valId( $intAssetLocationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						assets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id =' . ( int ) $intPropertyId . '
						AND ap_code_id = ' . ( int ) $intApCodeId . '
						AND asset_location_id = ' . ( int ) $intAssetLocationId;

		return self::fetchAsset( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedInventoryDetailsByPropertyIdsByAssetListsFilterByCid( $arrintPropertyIds, $objPagination, $objAssetListsFilter, $intCid, $objClientDatabase, $boolIsReviewItems = false ) {

		$strSqlCondition		= '';
		$arrstrSqlCondition = self::fetchSearchCriteria( $arrintPropertyIds, $objAssetListsFilter, $boolIsReviewItems );
		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSortDirection = 'ASC';
		$strOrderBy       = 'ac.item_number';

		$arrstrSortBy = [
			'ap_code_number' => 'ac.item_number',
			'ap_code_name' => 'ac.name',
			'ap_code_category_name' => 'acc.name',
			'property_name' => 'p.property_name',
			'asset_location_name' => 'al.name',
			'unit_of_measure_name' => 'uom.name',
			'qty_on_hand' => 'qty_on_hand',
			'extended_cost' => 'extended_cost'
		];

		if( false == is_null( $strByColumnName = getArrayElementByKey( $objAssetListsFilter->getSortBy(), $arrstrSortBy ) ) && true == valStr( $objAssetListsFilter->getSortDirection() ) ) {
			$strSortDirection = $objAssetListsFilter->getSortDirection();
			$strOrderBy       = addslashes( $strByColumnName );
		}

		$strSql = 'SELECT
						a.*,
						ac.item_number as ap_code_number,
						ac.name as ap_code_name,
						ac.item_number,
						ac.unit_of_measure_id,
						p.property_name,
						al.name as asset_location_name,
						acc.name as ap_code_category_name,
						uom.name as unit_of_measure_name,'
						. CUnitOfMeasureConversionsLibrary::getSelectClauseForQuantityOnHand() . ','
		                . CUnitOfMeasureConversionsLibrary::getSelectClauseForExtendedCost() . ','
		                . CUnitOfMeasureConversionsLibrary::getSelectClauseForQuantityOnOrder() . ','
		                . CUnitOfMeasureConversionsLibrary::getSelectClauseForApHeaderIds() . '
					FROM
						assets a
						JOIN ap_codes ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id and ac.asset_type_id IN( ' . sqlIntImplode( CAssetType::$c_arrintInventoryAssetTypeIds ) . ' ) )
						JOIN properties p ON ( a.cid = p.cid AND a.property_id = p.id )
						JOIN asset_locations al ON ( a.cid = al.cid and a.asset_location_id = al.id )
						JOIN ap_code_categories acc ON ( ac.cid = acc.cid and ac.ap_code_category_id = acc.id )
						JOIN unit_of_measures uom ON ( ac.cid = uom.cid and ac.unit_of_measure_id = uom.id )
						JOIN property_preferences pf ON ( p.cid = pf.cid and p.id = pf.property_id and pf.key = ' . ' \'TRACK_INVENTORY_QUANTITIES\' ' . ' and pf.value = ' . ' \'1\' ' . ' ) '
						. CUnitOfMeasureConversionsLibrary::getJoinSqlInventoryQuantityOnHand( $intCid )
						. CUnitOfMeasureConversionsLibrary::getJoinSqlInventoryExtendedCost( $intCid ) . '
						LEFT ' . CUnitOfMeasureConversionsLibrary::getJoinSqlQuantityOnOrder( $intCid ) . '
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.is_active IS true
						' . $strSqlCondition . '
						ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( false == $objAssetListsFilter->getShowZeroQnt() ) {
			$strSql = 'DROP TABLE
							IF EXISTS tmp_inventory_list;
							CREATE TEMP TABLE
							 tmp_inventory_list AS (' . $strSql . '
							 );
							CREATE INDEX idx_tbr_data_til ON tmp_inventory_list USING btree(qty_on_hand );
							ANALYZE tmp_inventory_list ( qty_on_hand );';

			$strSql .= 'SELECT * FROM tmp_inventory_list WHERE qty_on_hand > 0;';
		}

		$strSql = CUnitOfMeasureConversionsLibrary::getTempReceiveItemTable( $intCid ) . $strSql;

		return self::fetchAssets( $strSql, $objClientDatabase );
	}

	public static function fetchInventoryDetailsCountByPropertyIdsByAssetListsFilterByCid( $arrintPropertyIds, $objAssetListsFilter, $intCid, $objClientDatabase ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $arrintPropertyIds, $objAssetListsFilter );

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSql = 'SELECT
						COUNT( a.id )
					FROM
						assets a
						JOIN ap_codes ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id and ac.asset_type_id IN ( ' . sqlIntImplode( CAssetType::$c_arrintInventoryAssetTypeIds ) . ' ) )
						JOIN properties p ON ( a.cid = p.cid AND a.property_id = p.id )
						JOIN asset_locations al ON ( a.cid = al.cid and a.asset_location_id = al.id )
						JOIN ap_code_categories acc ON ( ac.cid = acc.cid and ac.ap_code_category_id = acc.id )
						JOIN unit_of_measures uom ON ( ac.cid = uom.cid and ac.unit_of_measure_id = uom.id )
						JOIN property_preferences pf ON ( p.cid = pf.cid and p.id = pf.property_id and pf.key = ' . ' \'TRACK_INVENTORY_QUANTITIES\' ' . ' and pf.value = ' . ' \'1\' ' . ' )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.is_active IS true
						' . $strSqlCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchSearchCriteria( $arrintPropertyIds, $objAssetListsFilter, $boolIsReviewItems = false ) {

		$arrstrSqlCondition = [];
		if( true == valId( $objAssetListsFilter->getInventoryApCodeId() ) ) {
			$arrstrSqlCondition[] = 'a.ap_code_id = ' . $objAssetListsFilter->getInventoryApCodeId();
		}

		if( true == valId( $objAssetListsFilter->getAssetLocationId() ) ) {
			$arrstrSqlCondition[] = 'a.asset_location_id = ' . $objAssetListsFilter->getAssetLocationId();
		}

		if( true == valId( $objAssetListsFilter->getApCodeCategoryId() ) ) {
			$arrstrSqlCondition[] = 'acc.id = ' . $objAssetListsFilter->getApCodeCategoryId();
		}

		if( true == valArr( $objAssetListsFilter->getPropertyIds() ) ) {
			$arrstrSqlCondition[] = ' a.property_id IN( ' . implode( ',', $objAssetListsFilter->getPropertyIds() ) . ' )';
		} elseif( true == valId( $intPropertyId = $objAssetListsFilter->getPropertyId() ) ) {
			$arrstrSqlCondition[] = 'a.property_id = ' . $intPropertyId;
		} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
			$arrstrSqlCondition[] = ' a.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( true == valArr( $objAssetListsFilter->getAssetTypeIds() ) ) {
			$arrstrAssetTypeConditions = [];
			if( true == in_array( CAssetType::INVENTORY_EXPENSED_AND_TRACKED, $objAssetListsFilter->getAssetTypeIds() ) ) {
				$arrstrAssetTypeConditions[] = 'ac.asset_type_id = ' . CAssetType::INVENTORY_EXPENSED_AND_TRACKED;
			}
			if( true == in_array( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED, $objAssetListsFilter->getAssetTypeIds() ) ) {
				$arrstrAssetTypeConditions[] = 'ac.asset_type_id = ' . CAssetType::INVENTORY_INVENTORIED_AND_TRACKED;
			}
			if( true == valArr( $arrstrAssetTypeConditions ) ) {
				$arrstrSqlCondition[] = ' ( ' . implode( ' OR ', $arrstrAssetTypeConditions ) . ' ) ';
			}
		}

		if( true == $boolIsReviewItems && true == ( $arrintAssetIds = getIntValuesFromArr( array_keys( array_filter( $objAssetListsFilter->getQtyToTransfer() ) ) ) ) ) {
			$arrstrSqlCondition[] = 'a.id IN ( ' . sqlIntImplode( $arrintAssetIds ) . ' )';
		} else {
			// search criteria for inventory bulk transfer
			if( true == valIntArr( $intInventoryApCodeIds = $objAssetListsFilter->getInventoryApCodeIds() ) ) {
				$arrstrSqlCondition[] = 'a.ap_code_id IN ( ' . sqlIntImplode( $intInventoryApCodeIds ) . ' )';
			}

			if( true == valIntArr( $intApCodeCategoryIds = $objAssetListsFilter->getApCodeCategoryIds() ) ) {
				$arrstrSqlCondition[] = 'acc.id IN ( ' . sqlIntImplode( $intApCodeCategoryIds ) . ' )';
			}
		}

		return $arrstrSqlCondition;
	}

	public static function fetchAssetWithApCodeByIdByCid( $intId, $intCid, $objClientDatabase ) {

		if( false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						a.*,
						ac.name as ap_code_name,
						ac.item_number as ap_code_number,
						lp.property_type_id,
						lp.is_student_property
					FROM
						assets a
						JOIN ap_codes ac ON ( ac.cid = a.cid AND ac.id = a.ap_code_id )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::int[], ARRAY[]::int[] ) lp ON ( a.cid = lp.cid AND a.property_id = lp.property_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id = ' . ( int ) $intId;

		return self::fetchAsset( $strSql, $objClientDatabase );
	}

	public static function fetchAssetsByPropertyIdsByAssetLocationIdsByApCodeIdsByCid( $arrintPropertyIds, $arrintAssetLocationIds, $arrintApCodeIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) || false == ( $arrintAssetLocationIds = getIntValuesFromArr( $arrintAssetLocationIds ) ) || false == ( $arrintApCodeIds = getIntValuesFromArr( $arrintApCodeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						assets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND asset_location_id IN ( ' . sqlIntImplode( $arrintAssetLocationIds ) . ')
						AND ap_code_id IN ( ' . sqlIntImplode( $arrintApCodeIds ) . ') ';

		return self::fetchAssets( $strSql, $objClientDatabase );
	}

	public static function fetchAssetsByPropertyIdByUnitSpaceIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objClientDatabase, $intLocationId = NULL, $intAssetId = NULL ) {

		$strSql = 'SELECT
						DISTINCT a.id,
						a.cid,
						a.serial_number,
						ac.name,
						ad.asset_number,
						ad.warranty_end_date
					FROM
						assets a
						JOIN asset_details ad ON ( a.cid = ad.cid AND a.id = ad.asset_id )
						JOIN ap_codes ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND ad.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND ad.retired_on IS NULL
						AND ad.retired_by IS NULL
						AND ac.asset_type_id IN ( ' . CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED . ', ' . CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED . ' )';

		if( true == valId( $intLocationId ) ) {
			$strSql .= ' AND ad.maintenance_location_id = ' . ( int ) $intLocationId;
			if( true == valId( $intAssetId ) ) {
				$strSql .= ' OR a.id = ' . ( int ) $intAssetId;
			}
		}
		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssetsBySerialNumbersByCid( $arrstrSerialNumbers, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrSerialNumbers ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						assets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND LOWER( serial_number ) IN ( ' . sqlStrImplode( array_filter( $arrstrSerialNumbers ) ) . ' )';

		return self::fetchAssets( $strSql, $objClientDatabase );
	}

	public static function fetchAssetDetailsByFastLookUpTypeByFilterData( $arrmixRequestData, $intCid, $arrintPropertyIds, $objDatabase, $boolCanAccessStandardPo = false, $boolCanAccessCatalogPo = false, $intUserId = NULL, $boolIsAdministrator = false ) {

		$strLookUpType 					= $arrmixRequestData['fast_lookup_filter']['lookup_type'];
		if( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_header_sub_type'] ) ) {
			$intApHeaderSubTypeId 			= $arrmixRequestData['fast_lookup_filter']['ap_header_sub_type'];
		}

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );

		if( ( CAsset::LOOKUP_TYPE_PURCHASE_ORDERS_CREATED_BY == $strLookUpType || CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_CREATED_BY == $strLookUpType ) ) {
			$arrstrAdvancedSearchParameters = [];

			$strSql = 'SELECT
					cu.id,
					cu.cid,
					cu.company_employee_id,
					COALESCE( ce.name_first, NULL ) AS name_first,
					COALESCE( ce.name_last, NULL ) AS name_last,
					cu.username,
					cu.is_administrator,
					cu.is_disabled
				FROM
					company_users cu
					LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ( ce.cid = cu.cid AND cu.default_company_user_id IS NULL ) )
				WHERE
					cu.cid = ' . ( int ) $intCid . '
					AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					AND cu.default_company_user_id IS NULL';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ce.name_first  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'ce.name_last ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'cu.username ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		} elseif( ( CAsset::LOOKUP_TYPE_PURCHASE_ORDERS_HEADER_NUMBER == $strLookUpType ) ) {

			$arrstrAdvancedSearchParameters = [];

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ah.header_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			$strSqlPart = '';

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSqlPart .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql = 'SELECT
				ah.header_number,
				ah.po_ap_header_ids[1] AS id
			FROM
				ap_headers ah
			WHERE
				ah.cid = ' . ( int ) $intCid . '
				AND ah.po_ap_header_ids[1] IS NOT NULL
				AND ah.is_template = false'
				. $strSqlPart . '
			 ORDER BY ah.header_number DESC ';

		} elseif( CAsset::LOOKUP_TYPE_ADD_PURCHASE_ORDER_VENDOR == $strLookUpType ) {

			$intApPayeeId					= NULL;
			$intPropertyId					= $arrmixRequestData['fast_lookup_filter']['property_id'];
			$strOrderBy						= NULL;
			$arrintApPayeeTypeIds			= [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER ];
			$arrintApPayeeStatusTypeIds		= [ CApPayeeStatusType::ACTIVE ];
			$arrstrAdvancedSearchParameters = [];

			if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_header_ap_payee_id'] ) && true == valId( $arrmixRequestData['fast_lookup_filter']['po_header_ap_payee_id'] ) ) {
				$intApPayeeId	= $arrmixRequestData['fast_lookup_filter']['po_header_ap_payee_id'];
			}

			$strOrderBy = ' LOWER( ap.company_name )';

			if( true == is_numeric( $intApPayeeId ) ) {
				$strOrderBy = ' apc.ap_payee_location_id';
			}

			if( false == valArr( $arrintPropertyIds ) ) {
				return;
			}

			$strWhereSql = '';

			if( false == is_null( $intPropertyId ) && true == is_numeric( $intPropertyId ) ) {
				$strWhereSql = ' AND pga.property_id = ' . $intPropertyId;
			} else {
				$strWhereSql .= ' AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			$strSql = 'SELECT
				DISTINCT ( ' . $strOrderBy . ' ),
				ap.*,
			 	apc.city,
				apc.state_code,
				apl.location_name,
				apl.id AS ap_payee_location_id,
				gat.formatted_account_number || \' : \' || gat.name AS gl_account_name
			FROM
				ap_payees ap
				JOIN gl_trees gt ON ( gt.cid = ap.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
				JOIN gl_account_trees gat ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
				JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND ap.cid = ga.cid AND ap.gl_account_id = ga.id )
				LEFT JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
				JOIN ap_payee_locations apl ON ( ap.id = apl.ap_payee_id AND apl.cid = ap.cid )
				LEFT JOIN ap_payee_contacts apc ON ( ap.id = apc.ap_payee_id AND apc.cid = ap.cid AND apc.ap_payee_location_id = apl.id )
			WHERE
				ap.cid = ' . ( int ) $intCid . '
				AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrintApPayeeTypeIds ) . ' )
				AND ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )
				AND apl.disabled_by IS NULL
				AND EXISTS (
					SELECT
						NULL
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
						JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, false, true ) . ' ) AS gvcup ON ( pga.cid = gvcup.cid AND pga.property_id = gvcup.id )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.cid = appg.cid
						AND apl.id = appg.ap_payee_location_id'
									. $strWhereSql . '
				)';

			if( true == is_numeric( $intApPayeeId ) ) {
				$strSql .= ' AND ap.id = ' . ( int ) $intApPayeeId;
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ap.company_name  	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'apl.location_name 	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'apc.city 			ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'apc.state_code 		ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY ' . $strOrderBy;

		} elseif( CAsset::LOOKUP_TYPE_ADD_PURCHASE_ORDER_PROPERTY == $strLookUpType ) {

			$intApBatchId					= ( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_batch_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['ap_batch_id'] : '';
			$intVendorApPayeeId				= ( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] : '';
			$intVendorApPayeeLocationId		= ( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_payee_location_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['ap_payee_location_id'] : '';
			$arrstrAdvancedSearchParameters	= [];

			$strApPayeeCondition = '';
			if( true == valId( $intVendorApPayeeId ) && true == valId( $intVendorApPayeeLocationId ) ) {
				$strApPayeeCondition = ' AND appg.ap_payee_id = ' . ( int ) $intVendorApPayeeId . ' AND appg.ap_payee_location_id = ' . ( int ) $intVendorApPayeeLocationId;
			}

			$strSql = 'SELECT
					DISTINCT gvcup.id,
					CASE
						WHEN gvcup.lookup_code IS NULL THEN gvcup.property_name
						ELSE gvcup.lookup_code || \' - \' || gvcup.property_name
					END AS property_name,
					LOWER ( gvcup.property_name ) lower_property_name,
					\'property\' AS line_item_type,
					pg.order_num AS order_num,
					gvcup.ap_post_month
				FROM
					ap_payees ap
					JOIN ap_payee_property_groups appg ON ( ap.cid = appg.cid AND ap.id = appg.ap_payee_id )
					JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
					JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, false, true ) . ' ) AS gvcup ON ( pga.cid = gvcup.cid AND pga.property_id = gvcup.id )
				WHERE
					gvcup.cid = ' . ( int ) $intCid . '
					AND gvcup.is_disabled = 0' . $strApPayeeCondition;

			// overwrite sql if vendor not selected in bulk invoices
			if( true == is_numeric( $intApBatchId ) && false == is_numeric( $intVendorApPayeeId ) && true == valArr( $arrintPropertyIds ) ) {
				$strSql = ' SELECT
						p.id,
						p.property_name,
						LOWER ( p.property_name ) lower_property_name,
						\'property\' AS line_item_type,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
					WHERE
						p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND p.is_disabled = 0
						AND p.cid = ' . ( int ) $intCid;
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, '( property_name	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\' OR lookup_code	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\' )' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY
					lower_property_name ASC, order_num';

		} elseif( CAsset::LOOKUP_TYPE_AP_CODES == $strLookUpType ) {

			$intApPayeeId	= ( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] : '';
			$intPropertyId	= ( true == isset( $arrmixRequestData['fast_lookup_filter']['property_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['property_id'] : '';

			$strSqlPart						= NULL;
			$arrstrAdvancedSearchParameters	= [];

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ac.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSqlPart .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			if( true == valId( $intPropertyId ) ) {
				$strSqlPart .= ' AND ( gap.gl_account_id IS NULL OR gap.property_id = ' . $intPropertyId . ' ) ';
			}

			$strSql = '
				SELECT * FROM (
					SELECT
						DISTINCT ac.*,
						CASE
							WHEN cs.vendor_unit_cost IS NULL OR cs.vendor_unit_cost = 0 THEN COALESCE( ac.default_amount, 0::numeric)
							ELSE COALESCE( cs.vendor_unit_cost, 0::numeric)
						END AS unit_cost,
						uom.name AS unit_of_measure_name,
						gat.name AS account_name,
						gat.formatted_account_number as account_number,
						gat.gl_account_id
					FROM
						ap_codes ac
						JOIN gl_accounts ga ON ( ac.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN asset_types at ON ( at.id = ac.asset_type_id )
						JOIN ap_catalog_items cs ON ( ac.cid = cs.cid AND ac.id = cs.ap_code_id )
						JOIN catalog_sku_property_groups cspg ON ( cs.cid = cspg.cid AND cs.id = cspg.ap_catalog_item_id )
						JOIN property_group_associations pga ON ( cspg.cid = pga.cid AND cspg.property_group_id = pga.property_group_id )
						LEFT JOIN gl_account_properties gap ON ( ac.cid = gap.cid AND ( ac.item_gl_account_id = gap.gl_account_id OR ac.consumption_gl_account_id = gap.gl_account_id ) )
						JOIN unit_of_measures uom ON ( ac.cid = uom.cid AND ac.unit_of_measure_id = uom.id )
						JOIN gl_account_trees gat ON ( ac.cid = gat.cid AND ac.item_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						ac.cid = ' . ( int ) $intCid . '
						AND ac.deleted_by IS NULL
						AND ac.is_disabled = false
						AND ga.disabled_by IS NULL'
									. $strSqlPart . '
						AND cs.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '

					UNION

					SELECT
						DISTINCT ac.*,
						COALESCE ( ac.default_amount, 0::numeric ) AS unit_cost,
						uom.name AS unit_of_measure_name,
						gat.name AS account_name,
						gat.formatted_account_number as account_number,
						gat.gl_account_id
					FROM
						ap_codes ac
						JOIN asset_types at ON ( at.id = ac.asset_type_id )
						LEFT JOIN ap_catalog_items cs ON ( ac.cid = cs.cid AND ac.id = cs.ap_code_id )
						JOIN unit_of_measures uom ON ( ac.cid = uom.cid AND ac.unit_of_measure_id = uom.id )
						JOIN gl_account_trees gat ON ( ac.cid = gat.cid AND ac.item_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						ac.cid = ' . ( int ) $intCid . '
						AND ac.deleted_by IS NULL
						AND ac.is_disabled = false
						AND cs.id IS NULL
					) AS sub
				ORDER BY
					sub.name,
					sub.ap_code_category_id';

		} elseif( CAsset::LOOKUP_TYPE_UNIT_OF_MEASURES == $strLookUpType ) {

			$intUnitOfMeasureId				= ( true == isset( $arrmixRequestData['fast_lookup_filter']['unit_of_measure_id'] ) ) ? $arrmixRequestData['fast_lookup_filter']['unit_of_measure_id'] : '';
			$strSqlPart						= NULL;
			$arrstrAdvancedSearchParameters	= [];

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'uom.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSqlPart .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql = 'SELECT
					uom.id,
					uom.name
				FROM
					unit_of_measures uom
				WHERE uom.id IN (
									SELECT
										CASE WHEN ac.unit_of_measure_id = uomc.to_unit_of_measure_id THEN
											uomc.from_unit_of_measure_id
										WHEN ac.unit_of_measure_id = uomc.from_unit_of_measure_id THEN
											uomc.to_unit_of_measure_id
										END  AS unit_of_measure_id
									FROM
										unit_of_measure_conversions uomc
										JOIN ap_codes ac ON ( ( uomc.to_unit_of_measure_id = ac.unit_of_measure_id OR uomc.from_unit_of_measure_id = ac.unit_of_measure_id ) AND uomc.cid = ac.cid )
									WHERE
										ac.cid = ' . ( int ) $intCid . '
										AND ac.unit_of_measure_id = ' . ( int ) $intUnitOfMeasureId . '

									UNION

									SELECT
										ac.unit_of_measure_id
									FROM
										ap_codes ac
									WHERE
										ac.cid = ' . ( int ) $intCid . '
										AND ac.unit_of_measure_id = ' . ( int ) $intUnitOfMeasureId . '
								)
				 	AND uom.cid = ' . ( int ) $intCid . '
				 ORDER BY
					uom.id';

		} elseif( CAsset::LOOKUP_TYPE_INVENTORY_ADJUSTMENT_PROPERTIES == $strLookUpType ) {

			$arrstrAdvancedSearchParameters	= [];

			$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, true );

			// check TRACK_INVENTORY_QUANTITIES setting from proerpty preferences for selected company
			$strSql .= ' AND p.id IN (
						SELECT
							property_id
						FROM
							property_preferences
						WHERE
							cid = ' . ( int ) $intCid . '
							AND key = \'' . CPropertyPreference::TRACK_INVENTORY_QUANTITIES . '\'
							AND value = \'1\'
					)';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'property_name 	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

		} elseif( CAsset::LOOKUP_TYPE_FIXED_ASSETS_PROPERTIES == $strLookUpType ) {

			$arrstrAdvancedSearchParameters	= [];

			$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, true, false, [], true );

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'property_name 	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY property_name';

		} elseif( CAsset::LOOKUP_TYPE_INVENTORY_AP_CODE == $strLookUpType ) {

			$arrstrAdvancedSearchParameters	= [];

			$strSql = 'SELECT
					DISTINCT ac.*,
					COALESCE ( ac.default_amount, 0::numeric ) AS unit_cost,
					uom.name AS base_unit_name,
					acc.name AS category_name,
					( SELECT count(ac1.name) FROM ap_codes ac1 WHERE lower( ac.name ) = lower( ac1.name ) AND ac.cid = ac1.cid AND ac.deleted_by IS NULL AND ac1.deleted_by IS NULL ) AS duplicate_name_count
				FROM
					ap_codes ac
					LEFT JOIN unit_of_measures uom ON ( uom.cid = ac.cid AND uom.id = ac.unit_of_measure_id )
					JOIN ap_code_categories acc ON ( acc.id = ac.ap_code_category_id AND acc.cid = ac.cid )
				WHERE
					ac.cid = ' . ( int ) $intCid . '
					AND ac.deleted_by IS NULL
					AND ac.is_disabled IS FALSE
					AND ac.asset_type_id IN ( ' . sqlIntImplode( CAssetType::$c_arrintInventoryAssetTypeIds ) . ' )
					AND acc.deleted_by IS NULL';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ac.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY ac.name DESC';

		} elseif( CAsset::LOOKUP_TYPE_FIXED_ASSETS_AP_CODE == $strLookUpType ) {

			$arrstrAdvancedSearchParameters	= [];

			$strSql = 'SELECT
					DISTINCT ac.*,
					COALESCE ( ac.default_amount, 0::numeric ) AS unit_cost,
					uom.name AS base_unit_name,
					acc.name AS category_name,
					( SELECT count(ac1.name) FROM ap_codes ac1 WHERE lower( ac.name ) = lower( ac1.name ) AND ac.cid = ac1.cid AND ac.deleted_by IS NULL AND ac1.deleted_by IS NULL ) AS duplicate_name_count
				FROM
					ap_codes ac
					LEFT JOIN unit_of_measures uom ON ( uom.cid = ac.cid AND uom.id = ac.unit_of_measure_id )
					JOIN ap_code_categories acc ON ( acc.id = ac.ap_code_category_id AND acc.cid = ac.cid )
				WHERE
					ac.cid = ' . ( int ) $intCid . '
					AND ac.deleted_by IS NULL
					AND ac.is_disabled IS FALSE
					AND ac.asset_type_id IN( ' . ( int ) CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED . ', ' . ( int ) CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED . ' ) 
					AND acc.deleted_by IS NULL';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ac.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY ac.name';

		} elseif( true == valArr( $arrstrFilteredExplodedSearch ) ) {

			$intHiddenApPayeeId			= NULL;
			$strPoNumber				= '';
			$strPayeeName				= '';
			$strPoCreatedBy				= '';
			$strPostDate				= '';
			$strPostDateTo				= '';
			$strPostDateFrom			= '';
			$arrintPoPropertyIds		= [];
			$arrintPoStatusTypeIds		= [];
			$arrintVendorLocationIds	= [];
			$arrintApHeaderSubTypeIds	= [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER ];

			if( false == $boolCanAccessStandardPo ) {

				$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
				unset( $arrintApHeaderSubTypeIds[CApHeaderSubType::STANDARD_PURCHASE_ORDER] );
				$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
			}

			if( false == $boolCanAccessCatalogPo ) {

				$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
				unset( $arrintApHeaderSubTypeIds[CApHeaderSubType::CATALOG_PURCHASE_ORDER] );
				$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
			}

			$arrintApPhysicalStatusTypeIds	= [ CApPhysicalStatusType::NOT_ORDERED, CApPhysicalStatusType::BACK_ORDERED, CApPhysicalStatusType::ORDERED, CApPhysicalStatusType::PARTIALLY_RECEIVED, CApPhysicalStatusType::RECEIVED, CApPhysicalStatusType::RETURNED, CApPhysicalStatusType::CANCELLED, CApPhysicalStatusType::ON_HOLD ];
			$arrintApFinancialStatusTypeIds	= [ CApFinancialStatusType::PENDING, CApFinancialStatusType::APPROVED, CApFinancialStatusType::CANCELLED, CApFinancialStatusType::CLOSED, CApFinancialStatusType::PARTIALLY_INVOICED, CApFinancialStatusType::REJECTED ];

			if( true == isset( $intApHeaderSubTypeId ) && true == valId( $intApHeaderSubTypeId ) ) {

				$arrintApHeaderSubTypeIds		= [ $intApHeaderSubTypeId ];
				$arrintApFinancialStatusTypeIds = [ CApFinancialStatusType::APPROVED ];
			}

			if( false == valArr( $arrintApHeaderSubTypeIds ) ) {
				return;
			}

			$strSql = ' SELECT
					DISTINCT( ah.* ),
					( SELECT
							SUM( COALESCE( ad.transaction_amount, 0::numeric ) )
						FROM
							ap_details ad
						WHERE
							ad.ap_header_id = ah.id
							AND ad.cid = ah.cid
							AND ad.cid = ' . ( int ) $intCid . '
							AND ad.deleted_by IS NULL
							AND ad.deleted_on IS NULL
					) AS transaction_amount,
					ap.company_name,
					apl.phone_number,
					apl.street_line1,
					apl.street_line2,
					apl.city,
					apl.state_code,
					apl.postal_code,
					apl.location_name,
					CASE
						WHEN fa.ap_header_id IS NOT NULL THEN
							1
						ELSE
							0
					END AS is_attachment
				FROM
					ap_headers ah
					JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
					LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id AND apl.ap_payee_id = ap.id )
					LEFT JOIN file_associations fa ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id AND fa.deleted_by IS NULL
														AND fa.id = ( SELECT MAX( id ) FROM file_associations WHERE cid = fa.cid AND ap_header_id = fa.ap_header_id ) )
					LEFT JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
				WHERE
					ah.cid = ' . ( int ) $intCid . '
					AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
					AND ah.ap_financial_status_type_id IN ( ' . implode( ',', $arrintApFinancialStatusTypeIds ) . ' )
					AND ah.is_template = false
					AND ah.deleted_by IS NULL
					AND ah.deleted_on IS NULL
					AND ad.deleted_by IS NULL
					AND ad.deleted_on IS NULL ';

			if( CAsset::LOOKUP_TYPE_VENDOR_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType ) {

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['vendor_po_status_type_ids'] ) ) {
					$arrintPoStatusTypeIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['vendor_po_status_type_ids'] );
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['vendor_po_property_id'] ) ) {
					$arrintPoPropertyIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['vendor_po_property_id'] );
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['vendor_ap_payee_id'] ) ) {
					$intHiddenApPayeeId = $arrmixRequestData['fast_lookup_filter']['vendor_ap_payee_id'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_header_date_from'] ) ) {
					$strPostDateFrom = $arrmixRequestData['fast_lookup_filter']['po_header_date_from'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_header_date_to'] ) ) {
					$strPostDateTo = $arrmixRequestData['fast_lookup_filter']['po_header_date_to'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['vendor_locations'] ) ) {
					$arrintVendorLocationIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['vendor_locations'] );
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['vendor_po_created_by'] ) ) {
					$strPoCreatedBy = $arrmixRequestData['fast_lookup_filter']['vendor_po_created_by'];
				}

				if( true == valArr( array_filter( $arrintVendorLocationIds ) ) && 'on' != $arrintVendorLocationIds[0] ) {
					$strSql .= ' AND ah.ap_payee_location_id IN ( ' . implode( ',', array_filter( $arrintVendorLocationIds ) ) . ' )';
				}

				if( true == is_numeric( $intHiddenApPayeeId ) ) {
					$strSql .= ' AND ah.ap_payee_id = ' . ( int ) $intHiddenApPayeeId;
				}

				if( !empty( $strPostDateFrom ) && 'From' != trim( $strPostDateFrom ) && true == CValidation::validateDate( $strPostDateFrom ) ) {
					$strSql .= ' AND ah.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $strPostDateFrom ) ) . '\'';
				}

				if( !empty( $strPostDateTo ) && 'To' != trim( $strPostDateTo ) && true == CValidation::validateDate( $strPostDateTo ) ) {
					$strSql .= ' AND ah.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $strPostDateTo ) ) . '\'';
				}

			} elseif( CAsset::LOOKUP_TYPE_JOB_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType || CAsset::LOOKUP_TYPE_JOB_PURCHASE_ORDERS_VENDOR == $strLookUpType ) {
				if( true == isset( $arrmixRequestData['fast_lookup_filter']['job_id'] ) ) {
					$strSql .= ' AND jp.job_id = ' . ( int ) $arrmixRequestData['fast_lookup_filter']['job_id'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['job_cost_code_ids'] ) && 'on' != $arrmixRequestData['fast_lookup_filter']['job_cost_code_ids'] ) {
					$strSql .= ' AND ad.ap_code_id IN ( ' . $arrmixRequestData['fast_lookup_filter']['job_cost_code_ids'] . ' ) ';
				}

				$arrintApHeaderSubTypeIds = [ CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ];
			} else {

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_financial_status_type_id'] ) ) {
					$arrintApFinancialStatusTypeIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['ap_financial_status_type_id'] );
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_physical_status_type_ids'] ) && false == is_null( $arrmixRequestData['fast_lookup_filter']['ap_physical_status_type_ids'] ) ) {
					$arrintApPhysicalStatusTypeIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['ap_physical_status_type_ids'] );
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['is_bulk_receive'] ) && false == is_null( $arrmixRequestData['fast_lookup_filter']['is_bulk_receive'] ) && true == $arrmixRequestData['fast_lookup_filter']['is_bulk_receive'] ) {

					$arrintPoStatusTypeIds = [
						CApFinancialStatusType::APPROVED,
						CApFinancialStatusType::PARTIALLY_INVOICED,
						CApPhysicalStatusType::BACK_ORDERED,
						CApPhysicalStatusType::ON_HOLD,
						CApPhysicalStatusType::ORDERED,
						CApPhysicalStatusType::PARTIALLY_RECEIVED,
					];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_property_id'] ) && false == is_null( $arrmixRequestData['fast_lookup_filter']['po_property_id'] ) ) {
					$arrintPoPropertyIds = explode( ',', $arrmixRequestData['fast_lookup_filter']['po_property_id'] );
				}

				if( CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType || CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_VENDOR == $strLookUpType ) {

					if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_date_from'] ) ) {
						$strPostDateFrom = $arrmixRequestData['fast_lookup_filter']['po_date_from'];
					}

					if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_date_to'] ) ) {
						$strPostDateTo = $arrmixRequestData['fast_lookup_filter']['po_date_to'];
					}

					$strSql .= ' AND ap.ap_payee_status_type_id = 1
				 AND ah.ap_financial_status_type_id = ' . CApFinancialStatusType::PENDING;

				} else {

					if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_header_date'] ) ) {
						$strPostDate = $arrmixRequestData['fast_lookup_filter']['po_header_date'];
					}
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['header_number'] ) ) {
					$strPoNumber = $arrmixRequestData['fast_lookup_filter']['header_number'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['payee_name'] ) ) {
					$strPayeeName = $arrmixRequestData['fast_lookup_filter']['payee_name'];
				}

				if( true == isset( $arrmixRequestData['fast_lookup_filter']['po_created_by'] ) ) {
					$strPoCreatedBy = $arrmixRequestData['fast_lookup_filter']['po_created_by'];
				}

				if( !empty( $strPostDate ) && true == CValidation::validateDate( $strPostDate ) ) {
					$strSql .= ' AND ah.transaction_datetime = \'' . date( 'Y-m-d', strtotime( $strPostDate ) ) . '\'';
				}

				if( !empty( $strPostDateFrom ) && 'From' != trim( $strPostDateFrom ) && true == CValidation::validateDate( $strPostDateFrom ) ) {
					$strSql .= ' AND ah.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $strPostDateFrom ) ) . '\'';
				}

				if( !empty( $strPostDateTo ) && 'To' != trim( $strPostDateTo ) && true == CValidation::validateDate( $strPostDateTo ) ) {
					$strSql .= ' AND ah.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $strPostDateTo ) ) . '\'';
				}

			}

			if( true == valIntArr( $arrintApFinancialStatusTypeIds ) && 'on' != $arrintApFinancialStatusTypeIds[0] ) {
				$strSql .= ' AND ah.ap_financial_status_type_id IN ( ' . implode( ',', $arrintApFinancialStatusTypeIds ) . ' )';
			}

			if( true == valIntArr( $arrintApHeaderSubTypeIds ) ) {
				$strSql .= ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )';
			}

			if( true == valIntArr( $arrintApPhysicalStatusTypeIds ) && 'on' != $arrintApPhysicalStatusTypeIds[0] ) {
				$strSql .= ' AND ah.ap_physical_status_type_id IN ( ' . implode( ',', $arrintApPhysicalStatusTypeIds ) . ' )';
			}

			if( true == valIntArr( $arrintPoPropertyIds ) && 'on' != $arrintPoPropertyIds[0] ) {
				$strSql .= ' AND ad.property_id IN ( ' . implode( ',', $arrintPoPropertyIds ) . ' )';
			} elseif( true == valIntArr( $arrintPropertyIds ) ) {
				$strSql .= ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			if( !empty( $strPoCreatedBy ) ) {
				$strSql .= ' AND ah.created_by = ' . ( int ) $strPoCreatedBy;
			}

			$arrstrAdvancedSearchParameters = [];

			if( CAsset::LOOKUP_TYPE_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType || CAsset::LOOKUP_TYPE_VENDOR_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType || CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType || CAsset::LOOKUP_TYPE_JOB_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType ) {

				if( '' != $strPayeeName ) {
					$strSql .= ' AND ap.company_name ILIKE \'%' . addslashes( trim( $strPayeeName ) ) . '%\' ';
				}

				foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
					if( '' != $strKeywordAdvancedSearch ) {
						array_push( $arrstrAdvancedSearchParameters, 'ah.header_number  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					}
				}
			} elseif( CAsset::LOOKUP_TYPE_PURCHASE_ORDERS_VENDOR == $strLookUpType || CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_VENDOR == $strLookUpType || CAsset::LOOKUP_TYPE_JOB_PURCHASE_ORDERS_VENDOR == $strLookUpType ) {

				if( '' != $strPoNumber ) {
					$strSql .= ' AND ah.header_number ILIKE \'%' . addslashes( trim( $strPoNumber ) ) . '%\' ';

				}

				foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
					if( '' != $strKeywordAdvancedSearch ) {
						array_push( $arrstrAdvancedSearchParameters, 'ap.company_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
						array_push( $arrstrAdvancedSearchParameters, 'apl.vendor_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );

					}
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY ah.id DESC';
		}

		$arrstrNewSearchResults	= [];

		$arrstrSearchResults			= ( array ) fetchData( $strSql, $objDatabase );

		foreach( $arrstrSearchResults as $intKey => $arrmixSearchResult ) {
			if( true == valArr( $arrmixSearchResult ) ) {
				foreach( $arrmixSearchResult as $strSearchKey => $strSearchValue ) {
					// remove control characters from string to avoid json error
					$arrstrSearchResults[$intKey][$strSearchKey] = preg_replace( '/[^(\x20-\x7F)]*/', '', $strSearchValue );
				}
			}
		}

		if( CAsset::LOOKUP_TYPE_PURCHASE_ORDERS_VENDOR == $strLookUpType || CAsset::LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_VENDOR == $strLookUpType || CAsset::LOOKUP_TYPE_JOB_PURCHASE_ORDERS_VENDOR == $strLookUpType ) {
			$strSearchResult = '';

			foreach( $arrstrSearchResults as $intIndex => $arrstrSearchResult ) {
				if( $strSearchResult == $arrstrSearchResult['ap_payee_id'] ) {
					unset( $arrstrSearchResults[$intIndex] );
				} else {
					$strSearchResult = $arrstrSearchResult['ap_payee_id'];
				}
			}

			$arrstrSearchResults = array_values( ( array ) $arrstrSearchResults );
		}

		// Merge invoice allocations in to the properties dropdown.
		if( CAsset::LOOKUP_TYPE_ADD_PURCHASE_ORDER_PROPERTY == $strLookUpType ) {

			$intApPayeeId					= NULL;
			$intApPayeeLocationId			= NULL;
			$strApPayeeCondition			= NULL;
			$arrintPropertyIds				= [];
			$arrstrNewSearchResults	= $arrstrSearchResults;
			$arrstrAdvancedSearchParameters	= [];

			if( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] ) && true == valId( $arrmixRequestData['fast_lookup_filter']['ap_payee_id'] ) ) {
				$intApPayeeId = $arrmixRequestData['fast_lookup_filter']['ap_payee_id'];
			}

			if( true == isset( $arrmixRequestData['fast_lookup_filter']['ap_payee_location_id'] ) && true == valId( $arrmixRequestData['fast_lookup_filter']['ap_payee_location_id'] ) ) {
				$intApPayeeLocationId = $arrmixRequestData['fast_lookup_filter']['ap_payee_location_id'];
			}

			if( true == valArr( $arrintPropertyIds ) ) {
				$arrintPropertyIds = $arrintPropertyIds;
			} else {
				foreach( $arrstrSearchResults as $arrstrProperty ) {
					$arrintPropertyIds[] = $arrstrProperty['id'];
				}
			}

			$arrintApFormulaTypeIds = [ CApFormulaType::PERCENT, CApFormulaType::NO_OF_UNITS, CApFormulaType::NO_OF_SPACES ];

			if( true == valId( $intApPayeeId ) && true == valId( $intApPayeeLocationId ) ) {
				$strApPayeeCondition = ' AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . ' AND appg.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId;
			}

			if( true == valArr( $arrintPropertyIds ) ) {
				$strPropertyIdCondition = ' AND afp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';
			}

			$strSql = 'SELECT
					DISTINCT af.id,
					af.is_gl_account_associated,
					af.name AS property_name,
					\'allocation\' AS line_item_type
				FROM
					ap_formulas af
					JOIN ap_formula_properties afp ON ( af.id = afp.ap_formula_id AND af.cid = afp.cid )
					JOIN property_group_associations pga ON ( afp.cid = pga.cid AND afp.property_id = pga.property_id )
					JOIN ap_payee_property_groups appg ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
					JOIN property_groups pg ON ( pg.cid = af.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, false, true ) . ' ) AS gvcup ON ( pga.cid = gvcup.cid AND pga.property_id = gvcup.id )
				WHERE
					af.ap_formula_type_id IN (' . implode( ',', $arrintApFormulaTypeIds ) . ')
					AND af.cid = ' . ( int ) $intCid . '
					' . $strApPayeeCondition . ' 
					' . $strPropertyIdCondition . '
					AND af.id NOT IN (
										SELECT
											DISTINCT af.id
										FROM
											ap_formulas af
											JOIN ap_formula_properties afp ON ( af.id = afp.ap_formula_id AND af.cid = afp.cid )
											JOIN property_group_associations pga ON ( afp.cid = pga.cid AND afp.property_id = pga.property_id )
											JOIN ap_payee_property_groups appg ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
											JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
											JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intUserId, $boolIsAdministrator, true, false, true ) . ' ) AS gvcup ON ( pga.cid = gvcup.cid AND pga.property_id = gvcup.id )
										WHERE
											afp.cid = ' . ( int ) $intCid . '
											AND af.ap_formula_type_id IN ( ' . implode( ', ', $arrintApFormulaTypeIds ) . ' )
											AND ( pga.property_id IS NULL
													OR gvcup.id IS NULL )
									)';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'af.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= 'ORDER BY af.name ASC';
			$arrstrSearchResults	= [];

			$arrstrSearchResults	= fetchData( $strSql, $objDatabase );
			$arrstrNewSearchResults	= array_merge( $arrstrNewSearchResults, $arrstrSearchResults );
		} else {
			$arrstrNewSearchResults	= $arrstrSearchResults;
		}

		return $arrstrNewSearchResults;
	}

	public static function fetchAllFixedAssetsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		if( false == valId( $intUnitSpaceId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = ' SELECT
						a.id,
						CASE WHEN asd.asset_number IS NOT NULL THEN
						ac.name || \' (\' || asd.asset_number || \')\'
						ELSE
						ac.name
						END AS name
					FROM
						assets a
						JOIN asset_details asd ON ( asd.cid = a.cid AND asd.asset_id = a.id )
						JOIN ap_codes ac ON ( a.cid = ac.cid AND a.ap_code_id = ac.id )
						JOIN unit_spaces us ON ( us.cid = a.cid AND us.id = asd.unit_space_id )
					WHERE
						asd.cid = ' . ( int ) $intCid . '
						AND asd.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND ac.asset_type_id = ' . CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED . '
						AND asd.retired_on IS NULL
						AND asd.retired_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveAssetByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						a.*,
						lp.property_type_id,
						lp.is_student_property
					FROM
						assets a
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::int[], ARRAY[]::int[] ) lp ON ( a.cid = lp.cid AND a.property_id = lp.property_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id  = ' . ( int ) $intId . '
						AND a.is_active IS TRUE';

		return self::fetchAsset( $strSql, $objClientDatabase );
	}

	public static function fetchAllUnRetiredAssetsByUnitSpaceIdByMaintenanceLocationIdByCidByAssetId( $intUnitSpaceId, $intMaintenanceLocationId, $intCid, $intAssetId, $objClientDatabase ) {

		$strWhereClause = '';

		if( true == valId( $intMaintenanceLocationId ) ) {
			$strWhereClause .= ' AND ad.maintenance_location_id = ' . ( int ) $intMaintenanceLocationId;
		}

		if( true == valId( $intUnitSpaceId ) ) {
			$strWhereClause .= ' AND ad.unit_space_id = ' . ( int ) $intUnitSpaceId;
		}
		$strSql = 'SELECT
						a.*,
						ac.name as ap_code_name
					FROM
						assets a
						JOIN asset_details ad ON ( a.cid = ad.cid AND a.id = ad.asset_id )
						JOIN ap_codes ac ON ( ac.cid = a.cid AND ac.id = a.ap_code_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ad.retired_on IS NULL
						AND ad.retired_by IS NULL
						AND ac.asset_type_id IN ( ' . sqlIntImplode( CAssetType::$c_arrintFixedAssetTypeIds ) . ' )
						ANd a.id != ' . ( int ) $intAssetId
						. $strWhereClause;

		return self::fetchAssets( $strSql, $objClientDatabase );
	}

	public static function fetchActiveAssetsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					*
					FROM 
						assets
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ' ) 
						AND cid = ' . ( int ) $intCid . '
						AND is_active IS TRUE';

		return self::fetchAssets( $strSql, $objDatabase );
	}

	public static function createTempReceiveItemTable( $intCid ) {

		return 'DROP TABLE IF EXISTS received_items; 
				CREATE TEMP TABLE  received_items AS ( 
													SELECT
                                                        at.cid,
                                                        at.po_ap_detail_id,
                                                        SUM ( at.display_quantity ) AS received_quantity
													FROM
                                                        asset_transactions at
                                                        JOIN ap_details ad ON ( at.cid = ad.cid AND at.po_ap_detail_id = ad.id )
                                                        JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . ' )
													WHERE
                                                        at.cid = ' . ( int ) $intCid . '
                                                        AND at.asset_transaction_type_id = ' . CAssetTransactionType::RECEIVE_INVENTORY . '
                                                        AND at.asset_transaction_id IS NULL
													GROUP BY
                                                        at.cid,
                                                        at.po_ap_detail_id
                                                    ); 
				ANALYZE received_items ( cid,po_ap_detail_id,received_quantity  );';

	}

	public static function getInventoryQuantityOnHand( $intCid ) {

		return 'JOIN LATERAL
							(
								SELECT
		                             at.cid,
		                             at.asset_id,
		                             SUM ( at.cached_remaining ) AS qty_on_hand
                                FROM
                                     asset_transactions at
                                WHERE
                                    at.cid = ' . ( int ) $intCid . '
                                    AND at.asset_transaction_type_id IN ( ' . sqlIntImplode( array_keys( CAssetTransactionType::createService()->getReceivedAssetTransactionTypes() ) ) . ' )
                                    AND a.cid = at.cid
                                    AND a.id = at.asset_id
                                GROUP BY
                                    at.cid,
                                    at.asset_id
                            ) AS quantity_on_hand ON TRUE ';

	}

	public static function getInventoryExtendedCost( $intCid ) {

		return 'JOIN LATERAL
							(
								SELECT
		                             at.cid,
                                     at.asset_id,
                                     SUM ( COALESCE ( ( ' . \Psi\Eos\Entrata\CAssetTransactions::createService()->getSelectSqlConsumptionUnitCost() . ' ) * at.cached_remaining, 0::NUMERIC ) ) as extended_cost
                                FROM
                                     asset_transactions at
                                WHERE
                                    at.cid = ' . ( int ) $intCid . '
                                    AND at.asset_transaction_type_id IN ( ' . sqlIntImplode( array_keys( CAssetTransactionType::createService()->getReceivedAssetTransactionTypes() ) ) . ' )
                                    AND a.cid = at.cid
                                    AND a.id = at.asset_id
                                GROUP BY
                                    at.cid,
                                    at.asset_id
                            ) AS extended_cost ON TRUE ';

	}

	// To use this function you need to call first createTempReceiveItemTable function then only you can call getQuantityOnOrder.

	public static function getQuantityOnOrder( $intCid ) {

		return 'JOIN LATERAL (
                                SELECT
                                    array_to_string( array_agg( DISTINCT( ah.id ) ), \',\') AS ap_header_ids,
                                    ad.cid,
                                    ad.property_id,
                                    asset_inner.id AS asset_id,
                                    sum( ad.quantity_ordered - COALESCE ( tri.received_quantity, 0 ) ) AS qty_on_order
                                FROM
                                    ap_headers ah
                                    JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ad.ap_code_id = a.ap_code_id)
                                    LEFT JOIN received_items tri ON ( ad.cid = tri.cid AND ad.id = tri.po_ap_detail_id )
                                    JOIN ap_financial_status_types afst ON ( afst.id = ah.ap_financial_status_type_id )
                                    JOIN ap_physical_status_types apst ON ( apst.id = ah.ap_physical_status_type_id )
                                    JOIN ap_payees ap ON ( ah.ap_payee_id = ap.id AND ah.cid = ap.cid )
                                    JOIN assets asset_inner ON ( ad.cid = asset_inner.cid AND ad.ap_code_id = asset_inner.ap_code_id )
                                WHERE
                                    ah.cid = ' . ( int ) $intCid . '
                                    AND ( ad.quantity_ordered - COALESCE ( tri.received_quantity, 0 ) ) <> 0
                                    AND ah.approved_by IS NOT NULL
                                    AND ah.deleted_by IS NULL
                                Group by
                                    ad.cid,
                                    ad.property_id,
                                    asset_inner.id
                                ) AS quantity_on_order ON ( quantity_on_order.cid = a.cid AND quantity_on_order.asset_id = a.id AND quantity_on_order.property_id = a.property_id) ';

	}

}
?>