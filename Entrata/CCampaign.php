<?php

use Psi\Eos\Entrata\CCampaigns;

class CCampaign extends CBaseCampaign {

	protected $m_intDraftedBy;

	protected $m_strUsername;
	protected $m_strDraftedOn;
	protected $m_strEmailSubject;
	protected $m_strFromEmailAddress;
	protected $m_strReplyToEmailAddress;
	protected $m_strSubject;
	protected $m_strPreheader;

	protected $m_arrstrCcEmailAddresses;
	protected $m_arrstrBccEmailAddresses;

	const CONTENT_VIEW_IN_BROWSER	= 3;
	const CONTENT_UPLOAD_FILES		= 4;

	/**
	 * Set Functions
	 */

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setSubject( $strSubject ) {
		$this->m_strSubject = $strSubject;
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->m_strFromEmailAddress = $strFromEmailAddress;
	}

	public function setReplyToEmailAddress( $strReplyToEmailAddress ) {
		$this->m_strReplyToEmailAddress = $strReplyToEmailAddress;
	}

	public function setCcEmailAddresses( $arrstrCcEmailAddresses ) {
		$this->m_arrstrCcEmailAddresses = $arrstrCcEmailAddresses;
	}

	public function setBccEmailAddresses( $arrstrBccEmailAddresses ) {
		$this->m_arrstrBccEmailAddresses = $arrstrBccEmailAddresses;
	}

	public function setDraftedOn( $strDraftedOn ) {
		$this->m_strDraftedOn = $strDraftedOn;
	}

	public function setPreheader( $strPreheader ) {
		$this->m_strPreheader = $strPreheader;
	}

	public function setDraftedBy( $intDraftedBy ) {
		$this->m_intDraftedBy = $intDraftedBy;
	}

	/**
	 * Get Functions
	 */

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function getReplyToEmailAddress() {
		return $this->m_strReplyToEmailAddress;
	}

	public function getCcEmailAddresses() {
		return $this->m_arrstrCcEmailAddresses;
	}

	public function getBccEmailAddresses() {
		return $this->m_arrstrBccEmailAddresses;
	}

	public function getDraftedOn() {
		return $this->m_strDraftedOn;
	}

	public function getDraftedBy() {
		return $this->m_intDraftedBy;
	}

	public function getPreheader() {
		return $this->m_strPreheader;
	}

	/**
	 * Create Functions
	 */

	public function createCampaignTarget() {
		$objCampaignTarget = new CCampaignTarget();

		$objCampaignTarget->setCid( $this->getCid() );
		$objCampaignTarget->setPropertyId( $this->getPropertyId() );
		$objCampaignTarget->setCampaignId( $this->getId() );

		return $objCampaignTarget;
	}

	/**
	 * Other Functions
	 */
	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['username'] ) && $boolDirectSet ) {
			$this->m_strUsername = trim( $arrmixValues['username'] );
		} elseif( isset( $arrmixValues['username'] ) ) {
			$this->setUsername( $arrmixValues['username'] );
        }

		if( isset( $arrmixValues['email_subject'] ) && $boolDirectSet ) {
			$this->m_strEmailSubject = trim( $arrmixValues['email_subject'] );
		} elseif( isset( $arrmixValues['email_subject'] ) ) {
			$this->setEmailSubject( $arrmixValues['email_subject'] );
		}

		if( isset( $arrmixValues['subject'] ) && $boolDirectSet ) {
			$this->m_strSubject = trim( $arrmixValues['subject'] );
		} elseif( isset( $arrmixValues['subject'] ) ) {
			$this->setSubject( $arrmixValues['subject'] );
		}

		if( isset( $arrmixValues['from_email_address'] ) && $boolDirectSet ) {
			$this->m_strFromEmailAddress = trim( $arrmixValues['from_email_address'] );
		} elseif( isset( $arrmixValues['from_email_address'] ) ) {
			$this->setFromEmailAddress( $arrmixValues['from_email_address'] );
        }

		if( isset( $arrmixValues['bcc_email_addresses'] ) && $boolDirectSet ) {
			$this->m_arrstrBccEmailAddresses = trim( $arrmixValues['bcc_email_addresses'] );
		} elseif( isset( $arrmixValues['bcc_email_addresses'] ) ) {
			$this->setBccEmailAddresses( $arrmixValues['bcc_email_addresses'] );
        }

		if( true == isset( $arrmixValues['preheader'] ) ) {
			$this->setPreheader( $arrmixValues['preheader'] );
		}
	}

	public function encodeEmailDraft() {

		$arrmixEmailDraft = [];

		$this->setEmailDraft( NULL );

		if( true == isset( $this->m_strFromEmailAddress ) || true == isset( $this->m_strSubject ) || true == isset( $this->m_arrstrCcEmailAddresses ) || true == isset( $this->m_arrstrBccEmailAddresses ) ) {
			$arrmixEmailDraft['from_email_address'] 	= isset( $this->m_strFromEmailAddress ) ? $this->m_strFromEmailAddress:'';
			$arrmixEmailDraft['email_subject'] 			= isset( $this->m_strSubject ) ? ( $this->m_strSubject ) : '';

			$arrmixEmailDraft['cc_email_addresses'] 	= isset( $this->m_arrstrCcEmailAddresses ) ? $this->m_arrstrCcEmailAddresses:'';
			$arrmixEmailDraft['bcc_email_addresses']	= isset( $this->m_arrstrBccEmailAddresses ) ? $this->m_arrstrBccEmailAddresses:'';

			$arrmixEmailDraft['drafted_by']				= isset( $this->m_intDraftedBy ) ? $this->m_intDraftedBy : '';
			$arrmixEmailDraft['drafted_on']				= isset( $this->m_strDraftedOn ) ? $this->m_strDraftedOn : date( 'Y-m-d H:i:s' );
			$arrmixEmailDraft['preheader']				= isset( $this->m_strPreheader ) ? $this->m_strPreheader : '';

			$this->setEmailDraft( json_encode( $arrmixEmailDraft ) );
		}

	}

	public function decodeEmailDraft() {

		if( true == valStr( $this->getEmailDraft() ) ) {
			$strEmailDraft = $this->getEmailDraft();
			$intEmailAddressPosition = \Psi\CStringService::singleton()->stripos( $strEmailDraft, 'email_subject' ) - 1;
			$intCcEmailAddressPosition = \Psi\CStringService::singleton()->stripos( $strEmailDraft, 'cc_email_addresses' ) - 1;
			$strEmailDraftWithoutSubject = \Psi\CStringService::singleton()->substr_replace( $strEmailDraft, '', $intEmailAddressPosition, ( $intCcEmailAddressPosition - $intEmailAddressPosition ) );
			$arrmixEmailDraft = json_decode( $strEmailDraftWithoutSubject, true );

			if( true == valStr( $arrmixEmailDraft['from_email_address'] ) ) $this->setFromEmailAddress( $arrmixEmailDraft['from_email_address'] );
			if( true == valStr( $arrmixEmailDraft['reply_to_email_address'] ) ) $this->setReplyToEmailAddress( $arrmixEmailDraft['reply_to_email_address'] );
			if( true == valStr( $arrmixEmailDraft['email_subject'] ) ) $this->setEmailSubject( $this->getSubject() );

			if( true == valStr( $arrmixEmailDraft['cc_email_addresses'] ) ) $this->setCcEmailAddresses( $arrmixEmailDraft['cc_email_addresses'] );
			if( true == valStr( $arrmixEmailDraft['bcc_email_addresses'] ) ) $this->setBccEmailAddresses( $arrmixEmailDraft['bcc_email_addresses'] );

			if( true == valStr( $arrmixEmailDraft['drafted_by'] ) ) $this->setDraftedBy( $arrmixEmailDraft['drafted_by'] );
			if( true == valStr( $arrmixEmailDraft['drafted_on'] ) ) $this->setDraftedOn( $arrmixEmailDraft['drafted_on'] );
			if( true == valStr( $arrmixEmailDraft['preheader'] ) ) {
				$this->setPreheader( $arrmixEmailDraft['preheader'] );
			}

		}
	}

	public function getSystemMessageTemplateSlotImagesByCampaign( $objDatabase ) {

		if( false == valStr( $this->getId() ) ) return false;

		$arrstrSystemMessageTemplateSlotImages = [];

		$arrstrSystemMessageEmailTemplateSlotName	= ( array ) \Psi\Eos\Entrata\CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $objDatabase );
		$arrmixSystemMessageTemplateSlotImages		= ( array ) \Psi\Eos\Entrata\CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesByCampaignIdByCid( $this->getId(), $this->getCid(),  $objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageEmailTemplateSlotName ) ) {
				$strKey = $arrstrSystemMessageEmailTemplateSlotName[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = [
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target']
				];
			}
		}

		return $arrstrSystemMessageTemplateSlotImages;
	}

	/**
	 * Validate Functions
	 */

	public function valIls( $intInternetListingServiceId, $objDatabase ) {

		$boolIsValid = true;

		$objInternetListingService = \Psi\Eos\Entrata\CInternetListingServices::createService()->fetchInternetListingServiceById( $intInternetListingServiceId, $objDatabase );

		if( true == valObj( $objInternetListingService, 'CInternetListingService' ) &&
			( true == $objInternetListingService->getIsIlsApi()
				|| true == $objInternetListingService->getIsGuestCardApi()
				|| ( true == $objInternetListingService->getIsGuestCardEnabled() && true == $objInternetListingService->getIsPublished() )
				|| true == in_array( $objInternetListingService->getId(), CInternetListingService::$c_arrintNoTrackerEmailIlsIds ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internet_listing_service_id', __( 'Cannot enable tracking emails with the associated ILS' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valConflictCampaignName( $objDatabase, $objCompanyUser ) {
		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Valid database object not provided.', E_USER_ERROR );
		}

		if( 0 == mb_strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name is required.' ) ) );
		} elseif( false == is_null( $this->getName() ) && 2 > mb_strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name must have atleast 2 characters.' ) ) );
		} elseif( false == is_null( $this->getName() ) && 50 < mb_strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name cannot be more than 50 characters long.' ) ) );
		} elseif( false == is_null( $this->getName() ) && ( true == preg_match( '/"/', $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Do not use double quotes (") in the list name.' ) ) );
		} else {

			$intConflictScheduledEmailFilterCount = ( int ) CCampaigns::createService()->fetchCampaignCountByNameByCidByCompanyUser( $this->getName(), $this->getCid(), $objCompanyUser, $objDatabase );

			if( 0 < $intConflictScheduledEmailFilterCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'This list name is already in use! Please enter another list name.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valReuseCampaignName( $objDatabase, $objCompanyUser ) {
		$boolIsValid = true;

		$intConflictScheduledEmailFilterCount = ( int ) CCampaigns::createService()->fetchCampaignCountByNameByCidByCompanyUser( $this->getName(), $this->getCid(), $objCompanyUser, $objDatabase );

		if( 0 < $intConflictScheduledEmailFilterCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'This list is already in use. Please check in Drafts or Saved uploaded lists.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intInternetListingServiceId = NULL, $objDatabase = NULL,  $objCompanyUser = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valConflictCampaignName( $objDatabase, $objCompanyUser );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_ils':
				$boolIsValid = $this->valIls( $intInternetListingServiceId, $objDatabase );
				break;

			case 'validate_reuse_insert':
				$boolIsValid = $this->valReuseCampaignName( $objDatabase, $objCompanyUser );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->encodeEmailDraft();
		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->encodeEmailDraft();
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setCampaignId( $this->getId() );

		return $objFileAssociation;
	}

	public function getEmailAttachmentsByEmailCampaign( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;

		$arrmixFileAttachments = [];

		$arrobjFiles = ( array ) CFiles::fetchFilesByCampaignIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );

		$intCounter = 1;

		if( true == valArr( $arrobjFiles ) ) {

			foreach( $arrobjFiles as $objFile ) {
				$strKey = 'file' . $intCounter;
				$arrmixFileAttachments[$strKey] = [
					'id' 	=> $objFile->getId(),
					'title' => $objFile->getTitle()
				];

				$intCounter++;
			}
		}

		return $arrmixFileAttachments;
	}

}
?>