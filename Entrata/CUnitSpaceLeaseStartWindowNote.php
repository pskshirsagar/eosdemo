<?php

class CUnitSpaceLeaseStartWindowNote extends CBaseUnitSpaceLeaseStartWindowNote {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getCid() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getUnitSpaceId() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getLeaseTermId() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getLeaseStartWindowId() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getNote() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseTermId();
				$boolIsValid &= $this->valLeaseStartWindowId();
				$boolIsValid &= $this->valNote();
				break;
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>