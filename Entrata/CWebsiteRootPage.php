<?php

class CWebsiteRootPage extends CBaseWebsiteRootPage {

	public static $c_arrstrValidExtentions = [ 'xml'  => 'Xml',
		'html' => 'Html',
		'txt'  => 'Html',
		'css'  => 'CSS',
		'kml'  => 'Kml'
	];

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;

		if( true == is_null( $this->getWebsiteId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_id', __( 'Website id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPageName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPageName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'page_name', __( 'Page name is required.' ) ) );
		} else {

			if( false == CValidation::validateFileName( $this->getPageName() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'page_name', __( 'Valid page name is required.' ) ) );
				$boolIsValid &= false;
			}

			if( true == $boolIsValid ) {

				$arrstrFileParts = explode( '.', $this->getPageName() );
				if( false == array_key_exists( \Psi\CStringService::singleton()->strtolower( $arrstrFileParts[\Psi\Libraries\UtilFunctions\count( $arrstrFileParts ) - 1] ), CWebsiteRootPage::$c_arrstrValidExtentions ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'page_name', __( 'Valid extention is required.' ) ) );
					$boolIsValid &= false;
				}
			}

			if( true == $boolIsValid ) {
				if( 'index.html' == $this->getPageName() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'page_name', __( 'Not a valid page name.' ) ) );
					$boolIsValid &= false;
				}
			}

			if( true == $boolIsValid ) {

				$strWhereConditions = ' WHERE
											cid = ' . ( int ) $this->getCid() . '
											AND deleted_on IS NULL
											AND LOWER( page_name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getPageName() ) ) . '\'
											AND website_id = ' . ( int ) $this->getWebsiteId() .
											( ( 0 < $this->getId() ) ? ' AND id <> ' . $this->getId() : '' );

				if( 0 < \Psi\Eos\Entrata\CWebsiteRootPages::createService()->fetchWebsiteRootPageCount( $strWhereConditions, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'page_name', __( 'Page name already in use.' ) ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valWebsiteId();
				$boolIsValid &= $this->valPageName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function loadPageContents() {
		$arrstrFileParts 	= explode( '.', $this->getPageName() );
		$strExtention 		= $arrstrFileParts[\Psi\Libraries\UtilFunctions\count( $arrstrFileParts ) - 1];

		header( 'Cache-Control: no-cache' );

		switch( $strExtention ) {

			case 'txt':
				header( 'Content-Type: text/plain' );
				break;

			case 'html':
				header( 'Content-Type: text/html' );
				break;

			case 'xml':
				header( 'Content-Type: text/xml' );
				break;

			case 'css':
				header( 'Content-Type: text/css' );
				break;

			default:
				// default case
				break;
		}

		print $this->getPageContents();
	}

}
?>