<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOfferStatuses
 * Do not add any new functions to this class.
 */

class CScreeningConditionOfferStatuses extends CBaseScreeningConditionOfferStatuses {

	public static function fetchScreeningConditionOfferStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningConditionOfferStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningConditionOfferStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningConditionOfferStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>