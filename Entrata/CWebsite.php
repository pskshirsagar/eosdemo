<?php

use Psi\Eos\Entrata\CWebsiteNotifications;
use Psi\Eos\Entrata\CWebsiteAds;
use Psi\Eos\Entrata\CTemplateSlotImages;
use Psi\Eos\Entrata\CWebsiteTemplates;
use Psi\Eos\Entrata\CWebsitePreferences;
use Psi\Eos\Entrata\CWebsiteDomainsCertificates;
use Psi\Eos\Entrata\CBlogComments;
use Psi\Eos\Entrata\CBlogPosts;
use Psi\Eos\Entrata\CWebsiteWrappers;
use Psi\Eos\Entrata\CWebsiteLocales;
use Psi\Eos\Entrata\CWebsiteProperties;
use Psi\Eos\Entrata\CWebsiteDocuments;
use Psi\Eos\Entrata\CWebsiteDomains;
use Psi\Eos\Entrata\CWebsiteCanonicalUrls;
use Psi\Eos\Entrata\CBlogTags;
use Psi\Eos\Entrata\CWebsiteWidgets;
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CProperties;
use Psi\Eos\Entrata\CDocuments;
use Psi\Libraries\UtilHash\CHash;

class CWebsite extends CBaseWebsite {

	const VALIDATE_UPDATE_SUBDOMAIN = 'update_subdomain';

	protected $m_arrobjProperties;
	protected $m_arrobjWebsitePreferences;
	protected $m_arrobjWebsiteNotifications;
	protected $m_arrobjPropertyAddresses;
	protected $m_arrobjWebsiteDocuments;

	protected $m_arrintPropertyIds;

	protected $m_objClient;
	protected $m_objWebsiteTemplate;
	protected $m_objSourceWebsite;

	protected $m_strWebsiteDomain;
	protected $m_strWebsiteOldSubDomain;
	protected $m_strWebsiteOldFullDomain;
	protected $m_strWebsiteCurrentFullDomain;
	protected $m_strWebsitePropertyName;
	protected $m_strTemplateName;
	protected $m_strDeprecatedOn;
	protected $m_strBuildingName;

	protected $m_intPropertyId;
	protected $m_intWebsiteTemplateTypeId;
	protected $m_intWebsitePropertiesCount;

	protected $m_boolTemplateIsResponsive;
	protected $m_boolWebsiteDomainIsSecure = false;

	protected $m_intResidentPortalService;

	/**
	 * Set Functions
	 */

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setResidentPortalService( $intResidentPortalService ) {
		$this->m_intResidentPortalService = $intResidentPortalService;
	}

	public function setPropertyAddresses( $arrobjPropertyAddresses ) {
		$this->m_arrobjPropertyAddresses = $arrobjPropertyAddresses;
	}

	public function setWebsiteDocuments( $arrobjWebsiteDocuments ) {
		$this->m_arrobjWebsiteDocuments = $arrobjWebsiteDocuments;
	}

	public function setWebsiteOldSubDomain( $strWebsiteOldSubDomain ) {
		$this->m_strWebsiteOldSubDomain = $strWebsiteOldSubDomain;
	}

	public function setWebsiteOldFullDomain( $strWebsiteOldFullDomain ) {
		$this->m_strWebsiteOldFullDomain = $strWebsiteOldFullDomain;
	}

	public function setWebsiteCurrentFullDomain( $strWebsiteCurrentFullDomain ) {
		$this->m_strWebsiteCurrentFullDomain = $strWebsiteCurrentFullDomain;
	}

	public function setWebsiteTemplateTypeId( $intWebsiteTemplateTypeId ) {
		$this->m_intWebsiteTemplateTypeId = $intWebsiteTemplateTypeId;
	}

	public function setWebsitePropertyName( $strWebsitePropertyName ) {
		$this->m_strWebsitePropertyName = $strWebsitePropertyName;
	}

	public function setWebsiteTags( $strWebsiteTags ) {
		$this->m_strWebsiteTags = $strWebsiteTags;
	}

	public function setTemplateName( $strTemplateName ) {
		$this->m_strTemplateName = $strTemplateName;
	}

	public function setTemplateIsResponsive( $boolTemplateIsResponsive ) {
		$this->m_boolTemplateIsResponsive = $boolTemplateIsResponsive;
	}

	public function setDeprecatedOn( $strDeprecatedOn ) {
		$this->m_strDeprecatedOn = $strDeprecatedOn;
	}

	public function setWebsiteDomainIsSecure( $boolWebsiteDomainIsSecure ) {
	   $this->m_boolWebsiteDomainIsSecure = CStrings::strToBool( $boolWebsiteDomainIsSecure );
	}

	public function setWebsitePropertiesCount( $intWebsitePropertiesCount ) {
		$this->m_intWebsitePropertiesCount = $intWebsitePropertiesCount;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['property_id'] ) ) 	$this->setPropertyId( $arrstrValues['property_id'] );
		if( true == isset( $arrstrValues['website_domain'] ) )	$this->setWebsiteDomain( $arrstrValues['website_domain'] );
		if( true == isset( $arrstrValues['website_template_type_id'] ) )	$this->setWebsiteTemplateTypeId( $arrstrValues['website_template_type_id'] );
		if( true == isset( $arrstrValues['property_name'] ) )	$this->setWebsitePropertyName( $arrstrValues['property_name'] );
		if( true == isset( $arrstrValues['website_tags'] ) )	$this->setWebsiteTags( $arrstrValues['website_tags'] );
		if( true == isset( $arrstrValues['template_name'] ) )	$this->setTemplateName( $arrstrValues['template_name'] );
		if( true == isset( $arrstrValues['is_responsive'] ) )	$this->setTemplateIsResponsive( $arrstrValues['is_responsive'] );
		if( true == isset( $arrstrValues['deprecated_on'] ) )	$this->setDeprecatedOn( $arrstrValues['deprecated_on'] );
		if( true == isset( $arrstrValues['is_secure'] ) )	    $this->setWebsiteDomainIsSecure( $arrstrValues['is_secure'] );
		if( true == isset( $arrstrValues['properties_count'] ) )	$this->setWebsitePropertiesCount( $arrstrValues['properties_count'] );
		if( true == isset( $arrstrValues['building_name'] ) )	$this->setBuildingName( $arrstrValues['building_name'] );
		if( true == isset( $arrstrValues['resident_portal_service'] ) )	$this->setResidentPortalService( $arrstrValues['resident_portal_service'] );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setWebsiteDomain( $strWebsiteDomain ) {
		$this->m_strWebsiteDomain = $strWebsiteDomain;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setWebsitePreferences( $arrobjWebsitePreferences ) {
		$this->m_arrobjWebsitePreferences = $arrobjWebsitePreferences;
	}

	public function setWebsiteTemplate( $objWebsiteTemplate ) {
		$this->m_objWebsiteTemplate = $objWebsiteTemplate;
	}

	public function setSubDomain( $strSubDomain, $strDomainExtension = NULL ) {
		parent::setSubDomain( \Psi\CStringService::singleton()->strtolower( trim( $strSubDomain . $strDomainExtension ) ) );
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setSourceWebsite( $objSourceWebsite ) {
		$this->m_objSourceWebsite = $objSourceWebsite;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getResidentPortalService() {
		return $this->m_intResidentPortalService;
	}

	public function getWebsiteDomain() {
		return $this->m_strWebsiteDomain;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getWebsitePreferences() {
		return $this->m_arrobjWebsitePreferences;
	}

	public function getWebsiteTemplate() {
		return $this->m_objWebsiteTemplate;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getWebsiteTemplateTypeId() {
		return $this->m_intWebsiteTemplateTypeId;
	}

	public function getWebsitePropertyName() {
		return $this->m_strWebsitePropertyName;
	}

	public function getWebsiteTags() {
		return $this->m_strWebsiteTags;
	}

	public function getTemplateName() {
		return $this->m_strTemplateName;
	}

	public function getTemplateIsResponsive() {
		return $this->m_boolTemplateIsResponsive;
	}

	public function getDeprecatedOn() {
		return $this->m_strDeprecatedOn;
	}

	public function getWebsiteDomainIsSecure() {
		return $this->m_boolWebsiteDomainIsSecure;
	}

	public function getWebsitePropertiesCount() {
		return $this->m_intWebsitePropertiesCount;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getWebsiteOldSubDomain() {
		return $this->m_strWebsiteOldSubDomain;
	}

	public function getOrFetchClient( $objDatabase ) {
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objDatabase );
		}

		return $this->m_objClient;
	}

	public function getOrFetchWebsiteNotifications( $objDatabase ) {
		if( false == valObj( $this->m_arrobjWebsiteNotifications, 'CWebsiteNotifications' ) ) {
			$this->m_arrobjWebsiteNotifications = $this->fetchWebsiteNotifications( $objDatabase );
		}

		return $this->m_arrobjWebsiteNotifications;
	}

	public function getOrFetchPrimaryWebsiteDomain( $objDatabase ) {

		if( false == valObj( $this->m_objWebsiteDomain, 'CWebsiteDomain' ) ) {
			$this->m_objWebsiteDomain = $this->fetchPrimaryWebsiteDomain( $objDatabase );
		}

		return $this->m_objWebsiteDomain;
	}

	public function getOrFetchWebsiteTemplate( $objDatabase ) {
		if( false == valObj( $this->m_objWebsiteTemplate, 'CWebsiteTemplate' ) ) {
			$this->m_objWebsiteTemplate = $this->fetchWebsiteTemplate( $objDatabase );
		}

		return $this->m_objWebsiteTemplate;
	}

	public function getOrFetchCachedWebsiteTemplate( $objDatabase ) {

		if( false == valObj( $this->m_objWebsiteTemplate, 'CWebsiteTemplate' ) ) {

			$this->m_objWebsiteTemplate = CCache::fetchObject( 'CWebsiteTemplate_' . $this->getCid() . '_' . $this->getId() . '_' . $this->getWebsiteTemplateId() );

			if( false === $this->m_objWebsiteTemplate || false == valObj( $this->m_objWebsiteTemplate, 'CWebsiteTemplate' ) ) {

				$this->m_objWebsiteTemplate = $this->fetchWebsiteTemplate( $objDatabase );
				CCache::storeObject( 'CWebsiteTemplate_' . $this->getCid() . '_' . $this->getId() . '_' . $this->getWebsiteTemplateId(), $this->m_objWebsiteTemplate, $intSpecificLifetime = 3600, $arrstrTags = [] );
			}
		}

		return $this->m_objWebsiteTemplate;
	}

	public function getSupportedLocaleCodes( $objDatabase ) {
		$arrstrSupportedLocaleCodes = CCache::fetchObject( 'supported_locale_codes_cid_' . $this->getCId() . '_website_' . $this->getId() );
		if( false == valArr( $arrstrSupportedLocaleCodes ) ) {
			$arrstrSupportedLocaleCodes = [];
			$arrobjPropertyProducts = \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPropertyProductsByProductIdByProductOptionIdByCid( CPsProduct::ENTRATA, CPsProductOption::ENTRATA_INTERNATIONAL, $this->getCid(), $objDatabase );
			if( true == valArr( $arrobjPropertyProducts ) ) {
				$arrobjSupportedLocaleCodes = ( array ) CWebsiteLocales::createService()->fetchCustomWebsiteLocalesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				if( true == valArr( $arrobjSupportedLocaleCodes ) ) {
					foreach( $arrobjSupportedLocaleCodes as $objSupportedLocaleCode ) {
						$arrstrSupportedLocaleCodes[] = $objSupportedLocaleCode->getLocaleCode();
					}

				}
			}
			CCache::storeObject( 'supported_locale_codes_cid_' . $this->getCId() . '_website_' . $this->getId(), $arrstrSupportedLocaleCodes, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		return $arrstrSupportedLocaleCodes;
	}

	public function getSourceWebsite() {
		return $this->m_objSourceWebsite;
	}

	/**
	 * Create Functions
	 */

	public function createPropertyWrapper() {
		$objPropertyWrapper = new CPropertyWrapper();
		$objPropertyWrapper->setCid( $this->m_intCid );
		$objPropertyWrapper->setWrapperCache( 'text/html' );
		return $objPropertyWrapper;
	}

	public function createWebsiteBlog( $objClient ) {

		$objWebsiteBlog = new CWebsiteBlog();

		$strInitHandle = \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z]/', '', $objClient->getCompanyName() ), 0, 3 ) . '_' . \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9]/', '', $this->getSubDomain() ), 0, 6 ) . '_' . $this->getId() );

		$objWebsiteBlog->setCid( $this->m_intCid );
		$objWebsiteBlog->setWebsiteId( $this->m_intId );
		$objWebsiteBlog->setInitHandle( $strInitHandle );
		$objWebsiteBlog->setUsername( $strInitHandle );
		$objWebsiteBlog->setPassword( \Psi\CStringService::singleton()->substr( CHash::createService()->hashGeneric( $strInitHandle ), 0, 8 ) );
		$objWebsiteBlog->setPassPhrase( \Psi\CStringService::singleton()->substr( CHash::createService()->hashGeneric( $this->getName() ), 0, 8 ) );

		return $objWebsiteBlog;
	}

	public function createWebsiteDocument() {

		$objWebsiteDocument = new CWebsiteDocument();

		$objWebsiteDocument->setWebsiteId( $this->m_intId );
		$objWebsiteDocument->setCid( $this->m_intCid );

		return $objWebsiteDocument;
	}

	public function createWebsiteDomain() {
		$objWebsiteDomain = new CWebsiteDomain();

		$objWebsiteDomain->setCid( $this->getCid() );
		$objWebsiteDomain->setWebsiteId( $this->getId() );

		return $objWebsiteDomain;
	}

	public function createWebsiteDomainsCertificate() {
		$objWebsiteDomainCertificate = new CWebsiteDomainsCertificate();

		$objWebsiteDomainCertificate->setCid( $this->getCid() );
		$objWebsiteDomainCertificate->setWebsiteId( $this->getId() );

		return $objWebsiteDomainCertificate;
	}

	public function createWebsiteCertificate() {
		$objWebsiteCertificate = new CWebsiteCertificate();

		$objWebsiteCertificate->setCid( $this->getCid() );
		$objWebsiteCertificate->setWebsiteId( $this->getId() );

		return $objWebsiteCertificate;
	}

	public function createWebsitePreference() {
		$objWebsitePreference = new CWebsitePreference();

		$objWebsitePreference->setCid( $this->getCid() );
		$objWebsitePreference->setWebsiteId( $this->getId() );

		return $objWebsitePreference;
	}

	public function createWebsiteProperty() {
		$objWebsiteProperty = new CWebsiteProperty();

		$objWebsiteProperty->setCid( $this->getCid() );
		$objWebsiteProperty->setWebsiteId( $this->getId() );

		return $objWebsiteProperty;
	}

	public function createWebsitePropertyBuilding() {
		$objWebsitePropertyBuilding = new CWebsitePropertyBuilding();

		$objWebsitePropertyBuilding->setCid( $this->getCid() );
		$objWebsitePropertyBuilding->setWebsiteId( $this->getId() );

		return $objWebsitePropertyBuilding;
	}

	public function createWebsiteNotification() {
		$objWebsiteNotification = new CWebsiteNotification();

		$objWebsiteNotification->setCid( $this->m_intCid );
		$objWebsiteNotification->setWebsiteId( $this->m_intId );

		return $objWebsiteNotification;
	}

	public function createWebsiteAmenity() {
		$objWebsiteAmenity = new CWebsiteAmenity();

		$objWebsiteAmenity->setWebsiteId( $this->getId() );
		$objWebsiteAmenity->setCid( $this->getCid() );

		return $objWebsiteAmenity;
	}

	public function createWebsiteMedia() {
		$objWebsiteMedia = new CWebsiteMedia();

		$objWebsiteMedia->setCid( $this->m_intCid );
		$objWebsiteMedia->setWebsiteId( $this->getId() );

		return $objWebsiteMedia;
	}

	public function createWebsiteWrapper() {
		$objWebsiteWrapper = new CWebsiteWrapper();

		$objWebsiteWrapper->setCid( $this->m_intCid );
		$objWebsiteWrapper->setWebsiteId( $this->getId() );
		$objWebsiteWrapper->setWrapperCache( 'text/html' );

		return $objWebsiteWrapper;
	}

	public function createClientPrivacyItem() {
		$objClientPrivacyItem = new CClientPrivacyItem();

		$objClientPrivacyItem->setCid( $this->m_intCid );
		$objClientPrivacyItem->setWebsiteId( $this->getId() );

		return $objClientPrivacyItem;
	}

	public function createWebsiteRootPage() {
		$objWebsiteRootPage = new CWebsiteRootPage();

		$objWebsiteRootPage->setCid( $this->m_intCid );
		$objWebsiteRootPage->setWebsiteId( $this->getId() );

		return $objWebsiteRootPage;
	}

	public function createDirective( $objClientDatabase ) {
		$objDirective = new CDirective();

		$objDirective->setCid( $this->getCid() );
		$objDirective->setDirectiveTypeId( CDirectiveType::WEBSITE_SUB_DOMAIN );
		$objDirective->setWebsiteId( $this->getId() );
		$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getSubDomain() ) );
		$objDirective->setClusterId( $objClientDatabase->getClusterId() );
		$objDirective->setDatabaseId( $objClientDatabase->getId() );

		return $objDirective;
	}

	public function createWebsiteCanonicalUrl() {
		$objWebsiteCanonicalUrl = new CWebsiteCanonicalUrl();

		$objWebsiteCanonicalUrl->setCid( $this->getCid() );
		$objWebsiteCanonicalUrl->setWebsiteId( $this->getId() );

		return $objWebsiteCanonicalUrl;
	}

	public function createWebsitePost() {
		$objBlogPost = new CBlogPost();

		$objBlogPost->setCid( $this->getCid() );
		$objBlogPost->setWebsiteId( $this->getId() );

		return $objBlogPost;
	}

	public function createWebsiteBlogTag() {
		$objBlogTag = new CBlogTag();

		$objBlogTag->setWebsiteId( $this->getId() );
		$objBlogTag->setCid( $this->getCid() );

		return $objBlogTag;
	}

	public function createWebsiteBlogPostTag() {
		$objBlogPostTag = new CBlogPostTag();

		$objBlogPostTag->setWebsiteId( $this->getId() );
		$objBlogPostTag->setCid( $this->getCid() );

		return $objBlogPostTag;
	}

	public function createWebsiteBlogComment() {
		$objWebsiteBlogComment = new CBlogComment();

		$objWebsiteBlogComment->setCid( $this->getCid() );

		return $objWebsiteBlogComment;
	}

	public function createWebsiteBlogPostMedia() {
		$objBlogPostTag = new CBlogPostMedia();

		$objBlogPostTag->setWebsiteId( $this->getId() );
		$objBlogPostTag->setCid( $this->getCid() );

		return $objBlogPostTag;
	}

	public function createWebsiteTagAssociation() {

		$objWebsiteTagAssociation = new CWebsiteTagAssociation();
		$objWebsiteTagAssociation->setCid( $this->getCid() );
		$objWebsiteTagAssociation->setWebsiteId( $this->getId() );

		return $objWebsiteTagAssociation;
	}

	public function createWebsiteAd() {
		$objWebsiteAd = new CWebsiteAd();

		$objWebsiteAd->setCid( $this->getCid() );
		$objWebsiteAd->setWebsiteId( $this->getId() );

		return $objWebsiteAd;
	}

	public function createWebsiteWebsiteAdRun() {
		$objWebsiteAdRun = new CWebsiteAdRun();

		$objWebsiteAdRun->setCid( $this->getCid() );
		$objWebsiteAdRun->setWebsiteId( $this->getId() );

		return $objWebsiteAdRun;
	}

	public function createWebsiteActivityLogs() {

		$objWebsiteActivityLog = new CWebsiteActivityLog();
		$objWebsiteActivityLog->setCid( $this->getCid() );
		$objWebsiteActivityLog->setWebsiteId( $this->getId() );

		return $objWebsiteActivityLog;
	}

	public function createWebsiteLocale() {

		$objWebsiteLocale = new CWebsiteLocale();
		$objWebsiteLocale->setCid( $this->getCid() );
		$objWebsiteLocale->setWebsiteId( $this->getId() );

		return $objWebsiteLocale;
	}

	public function createMlv3Library( $objClient, $objCompanyUser, $objDatabase ) {

		$objMlv3Library = new CMlv3Library();
		$objMlv3Library->setDatabase( $objDatabase );
		$objMlv3Library->setCompanyUser( $objCompanyUser );
		$objMlv3Library->setClient( $objClient );
		$objMlv3Library->setMarketingMediaTypeId( CMarketingMediaType::WEBSITE_MEDIA );

		return $objMlv3Library;
	}

	public function createWebsitePage() {

		$objWebsitePage = new CWebsitePage();

		$objWebsitePage->setCid( $this->m_intCid );
		$objWebsitePage->setWebsiteId( $this->m_intId );

		return $objWebsitePage;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyWrapperById( $intPropertyWrapperId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyWrappers::createService()->fetchPropertyWrapperByIdByCid( $intPropertyWrapperId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteWidgetByWidgetNamespaceBySupportedTypeId( $strWidgetNamespace, $intSupportedTypeId, $objDatabase ) {
		return CWebsiteWidgets::createService()->fetchWebsiteWidgetsByWidgetNamespaceBySupportedTypeIdByWebsiteIdByCid( $strWidgetNamespace, $intSupportedTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostsWithCommentCountByCidByWebsiteId( $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostsWithCommentCountByCidByWebsiteId( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteBlogByWebsiteIdByCid( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteBlogs::createService()->fetchWebsiteBlogByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePostsCountByStatusTypes( $objDatabase, $objCompanyUser = NULL, $boolIsDesignatedApprover = false ) {
		return CBlogPosts::createService()->fetchWebsitePostsCountByCidByWebsiteIdByStatusTypes( $this->getCid(), $this->getId(), $objDatabase, $objCompanyUser, $boolIsDesignatedApprover );
	}

	public function fetchPaginatedWebsiteCommentsCountByWebsiteIdByCid( $intCommentType, $objDatabase, $arrmixFilterData = [] ) {
		return CBlogComments::createService()->fetchPaginatedWebsiteCommentsCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $intCommentType, $objDatabase, $arrmixFilterData );
	}

	public function fetchPaginatedBlogPosts( $intPageNo, $intPageSize, $boolIsDisabled, $objDatabase, $intBlogPostStatusTypeId = NULL, $arrmixFilterData = NULL, $boolShowCount = false, $objCompanyUser = NULL, $boolIsDesignatedApprover = false ) {
		return CBlogPosts::createService()->fetchPaginatedBlogPostsByWebsiteIdByCid( $this->getId(), $this->getCid(), $intPageNo, $intPageSize, $boolIsDisabled, $objDatabase, $intBlogPostStatusTypeId, $arrmixFilterData, $boolShowCount, $objCompanyUser, $boolIsDesignatedApprover );
	}

	public function fetchBlogPostTagsByCidByWebsiteId( $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostTagsByCidByWebsiteId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchBlogPostAuthorsByCidByWebsiteId( $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostAuthorsByCidByWebsiteId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchBlogPostAuthorsByCidByWebsiteIdByBlogPostId( $intBlogPostId, $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostAuthorsByCidByWebsiteIdByBlogPostId( $this->getCid(), $this->getId(), $intBlogPostId, $objDatabase );
	}

	public function fetchBlogTagByCidByWebsiteId( $strQuickSearch, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagByCidByWebsiteId( $this->getId(), $this->getCid(), $strQuickSearch, $objDatabase );
	}

	public function fetchBlogTagsByTagNames( $strTagNames, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagsByCidByWebsiteIdByTagNames( $this->getCid(), $this->getId(), $strTagNames, $objDatabase );
	}

	public function fetchBlogCommentsByBlogPostIdByCommentTypeIds( $intBlogPostId, $objDatabase, $intBlogCommentTypeIds ) {
		return CBlogComments::createService()->fetchBlogCommentsByCidByWebsiteIdByBlogPostIdByCommentTypeIds( $this->getCid(), $this->getId(), $intBlogPostId, $objDatabase, $intBlogCommentTypeIds );
	}

	public function fetchBlogPostsByPostId( $intBlogPostId, $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostsByPostId( $intBlogPostId, $this->getCid(), $objDatabase );
	}

	public function fetchBlogTagsByBlogPostId( $intBlogPostId, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogPostTagsByBlogPostId( $intBlogPostId, $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostTagsByBlogPostId( $intBlogPostId, $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogPostTags::createService()->fetchBlogPostTagsByBlogPostId( $intBlogPostId, $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostByIdByCid( $intBlogPostId, $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostByIdByCid( $intBlogPostId, $this->getCid(), $objDatabase );
	}

	public function fetchPostContentByPostIds( $arrintPostIds, $objDatabase ) {
		return CBlogPosts::createService()->fetchPostContentByPostIdsByWebsiteIdByCid( $arrintPostIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogCommentsByStatus( $intPageNo, $intPageSize,$objDatabase, $intIsDisabled, $arrmixFilterData ) {
		return CBlogComments::createService()->fetchBlogCommentsByCidByWebsiteIdByStatus( $this->getId(), $this->getCid(), $intPageNo, $intPageSize, $objDatabase, $intIsDisabled, $arrmixFilterData );
	}

	public function fetchBlogCommentsByBlogCommentParentId( $intBlogCommentParentId, $objDatabase, $arrmixFilterData = [],  $boolIsFetchData = true ) {
		return CBlogComments::createService()->fetchBlogCommentsByBlogCommentParentIdByCidByWebsiteIdByPostId( $intBlogCommentParentId, $this->getCid(), $this->getId(), $objDatabase, $arrmixFilterData, $boolIsFetchData );
	}

	public function fetchBlogCommentById( $intId, $objDatabase ) {
		return CBlogComments::createService()->fetchBlogCommentByIdByCid( $intId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteWidgetsBySupportedTypeId( $intSupportedTypeId, $objDatabase ) {
		return CWebsiteWidgets::createService()->fetchActiveWebsiteWidgetsBySupportedTypeIdByWebsiteIdByCid( $intSupportedTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPmEnabledProperties( $objDatabase, $arrintPropertyIds = NULL ) {
		return CProperties::createService()->fetchPmEnabledPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintPropertyIds );
	}

	public function fetchTemplateSlotImagesByWebsiteTemplateSlotIdsByWebsiteId( $arrintWebsiteTemplateSlotIds, $objDatabase ) {
		return CTemplateSlotImages::createService()->fetchTemplateSlotImagesByWebsiteTemplateSlotIdsByWebsiteIdByCid( $arrintWebsiteTemplateSlotIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaStockFilesByTemplateSlotImageIdsByWebsiteId( $arrintTemplateSlotImageIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaStockFilesByTemplateSlotImageIdsByWebsiteIdByCid( $arrintTemplateSlotImageIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDocumentsOrderByOrderNum( $objDatabase ) {
		return CWebsiteDocuments::createService()->fetchWebsiteDocumentsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDocumentByName( $strDocumentName, $objDatabase ) {
		return CDocuments::createService()->fetchDocumentByWebsiteIdByDocumentNameByCid( $this->getId(), $strDocumentName, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDocumentBySeoUri( $strSeoUri, $objDatabase ) {
		return CDocuments::createService()->fetchDocumentByWebsiteIdBySeoUriByCid( $this->getId(), $strSeoUri, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDocumentsCountBySeoUri( $strSeoUri, $objDatabase ) {
		return CWebsiteDocuments::createService()->fetchWebsiteDocumentsCountBySeoUriByWebsiteIdByCid( $strSeoUri, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegratedResidentPortalPropertiesByIds( $arrintPropertyIds, $objClientDatabase ) {
		return CProperties::createService()->fetcIntegratedResidentPortalPropertiesByIdsByWebsiteIdByCid( $arrintPropertyIds, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPopulatedWebsiteTemplateSlotsByWebsiteTemplate( $objWebsiteTemplate, $objProperty, $objClientDatabase, $arrobjWebsiteTemplateSlots = NULL ) {
		return \Psi\Eos\Entrata\CWebsiteTemplateSlots::createService()->fetchPopulatedWebsiteTemplateSlotsByWebsiteByWebsiteTemplate( $this, $objWebsiteTemplate, $objProperty, $objClientDatabase, $arrobjWebsiteTemplateSlots );
	}

	public function fetchCompanyMediaFilesByTemplateSlotImageIds( $arrintTemplateSlotImageIds, $objDatabase, $boolCheckCid = false ) {
		if( false == valArr( $arrintTemplateSlotImageIds ) ) return NULL;
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByTemplateSlotImageIdsByCid( $arrintTemplateSlotImageIds, $this->getCid(), $objDatabase, $boolCheckCid );
	}

	public function fetchDocumentById( $intDocumentId, $objDatabase ) {
		return CDocuments::createService()->fetchDocumentByIdByWebsiteIdByCid( $intDocumentId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDocumentByTitle( $strTitle, $objDatabase ) {
		return CDocuments::createService()->fetchDocumentByTitleByWebsiteIdByCid( $strTitle, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedDocuments( $objDatabase ) {
		return CDocuments::createService()->fetchPublishedDocumentsByWebsiteIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMobilePortalDocuments( $objDatabase ) {
		return CDocuments::createService()->fetchPublishedMobileWebsiteDocumentsByWebsiteIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchProperties( $objClientDatabase ) {
		return CProperties::createService()->fetchPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchParentChildProperties( $objClientDatabase ) {
		return CProperties::createService()->fetchParentChildPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPublishedProperties( $objClientDatabase ) {
		return CProperties::createService()->fetchPublishedPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchMinMaxRent( $objClientDatabase ) {
		return CProperties::createService()->fetchMinMaxRentByWebsiteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchMinMaxBedBath( $objClientDatabase ) {
		return CProperties::createService()->fetchMinMaxBedBathByWebsiteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertiesByIdsByCompanyUserId( $arrintPropertyIds, $intCompanyUserId, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		return CProperties::createService()->fetchPropertiesByCompanyUserIdByPropertyIdsByCid( $intCompanyUserId, $arrintPropertyIds, $this->getCid(), $objDatabase, $boolShowEnabled = NULL, $boolIsFeatured = NULL, $boolSortParentPropertyFirst = false, $intPageNo, $intPageSize );
	}

	public function fetchPropertyKeyValues( $objAdminDatabase, $objClientDatabase ) {

		return \Psi\Eos\Entrata\CPropertyKeyValues::createService()->fetchPropertyKeyValuesByWebsiteId( $this->getId(), $this->getCid(), $objAdminDatabase, $objClientDatabase );
	}

	public function fetchWebsiteDomains( $objDatabase ) {
		return CWebsiteDomains::createService()->fetchAllWebsiteDomainsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAndPropertyDomains( $objDatabase ) {
		return CWebsiteDomains::createService()->fetchWebsiteAndAssociatedPropertyDomainsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDomainsByPropertyId( $intPropertyId, $objDatabase ) {

		return CWebsiteDomains::createService()->fetchAllWebsiteDomainsByWebsiteIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDomain( $intWebsiteDomainId, $objDatabase ) {

		return CWebsiteDomains::createService()->fetchWebsiteDomainByIdByWebsiteIdByCid( $intWebsiteDomainId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryWebsiteDomain( $objDatabase ) {

		return CWebsiteDomains::createService()->fetchPrimaryWebsiteDomainByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePreferenceByKey( $strKey, $objDatabase ) {
		return CWebsitePreferences::createService()->fetchWebsitePreferenceByWebsiteIdAndKeyByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePreferencesByKeys( $arrstrKeys, $objDatabase ) {
		return CWebsitePreferences::createService()->fetchWebsitePreferencesByWebsiteIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase );
	}

    public function fetchViewWebsitePreferencesByKeys( $arrstrKeys, $intWebsiteTemplateId, $objDatabase ) {
        return CWebsitePreferences::createService()->fetchViewWebsitePreferencesByWebsiteIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $intWebsiteTemplateId, $objDatabase );
    }

	public function fetchWebsitePreferencesForVideoSliderByKeys( $arrstrKeys, $objDatabase ) {
		return CWebsitePreferences::createService()->fetchWebsitePreferencesForVideoSliderByWebsiteIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyCount( $objDatabase ) {

		$strSql = 'SELECT count( cwp.id ) FROM website_properties cwp, properties cp WHERE cwp.property_id = cp.id AND cwp.cid = cp.cid AND cp.is_disabled = 0 AND cwp.website_id =' . ( int ) $this->m_intId . ' AND cwp.cid = ' . ( int ) $this->getCid();

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrData[0]['count'] ) ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

	public function fetchWebsitePreferences( $objDatabase ) {

		return CWebsitePreferences::createService()->fetchWebsitePreferencesByWebsiteIdKeyedByKeyByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteProperties( $objDatabase ) {

		return CWebsiteProperties::createService()->fetchWebsitePropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllWebsiteProperties( $objDatabase ) {

		return CWebsiteProperties::createService()->fetchAllWebsitePropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllWebsitePropertyBuildings( $objDatabase ) {

		return \Psi\Eos\Entrata\CWebsitePropertyBuildings::createService()->fetchWebsitePropertyBuildingsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePropertiesByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		return CWebsiteProperties::createService()->fetchWebsitePropertiesByWebsiteIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchHasMerchantAccountByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return false;

		$strWhere = ' WHERE property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $this->getCid() . ' AND ar_code_id IS NULL';
		$intCount = \Psi\Eos\Entrata\CPropertyMerchantAccounts::createService()->fetchPropertyMerchantAccountCount( $strWhere, $objDatabase );

		return true == isset( $intCount ) && 0 < $intCount;
	}

	public function fetchCachedHasMerchantAccountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return false;

		sort( $arrintPropertyIds );

		$strPropertyIds = implode( '_', $arrintPropertyIds );

		$arrmixData = CCache::fetchObject( 'Has_Merchant_Accounts_' . $strPropertyIds );

		if( false === $arrmixData || true == is_null( $arrmixData ) ) {
			$strWhere = ' WHERE property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $this->getCid() . ' AND ar_code_id IS NULL';
			$intCount = \Psi\Eos\Entrata\CPropertyMerchantAccounts::createService()->fetchPropertyMerchantAccountCount( $strWhere, $objDatabase );

			$boolHasMerchantAccounts = false;
			if( true == isset( $intCount ) && 0 < $intCount ) {
				$boolHasMerchantAccounts = true;
			}

			$arrmixData[$strPropertyIds] = $boolHasMerchantAccounts;

			CCache::storeObject( 'Has_Merchant_Accounts_' . $strPropertyIds, $arrmixData, $intSpecificLifetime = 900, [] );
		}

		return $arrmixData;
	}

	public function fetchClient( $objDatabase ) {

		return \Psi\Eos\Entrata\CClients::createService()->fetchSimpleClientById( $this->getCid(), $objDatabase );
	}

	public function fetchDirective( $objDatabase ) {

		return \Psi\Eos\Connect\CDirectives::createService()->fetchDirectiveByCid( $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAddresses( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPublishedPropertyAddressesByPropertyIdsAddressTypeIdByCid( $arrintPropertyIds, CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchProspectPortalPropertyIds( $objDatabase ) {
		return CProperties::createService()->fetchProspectPortalPropertyIdsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchResidentPortalPropertyIds( $objDatabase ) {

		return CProperties::createService()->fetchResidentPortalPropertyIdsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProspectPortalProperties( $objDatabase ) {

		return CProperties::createService()->fetchProspectPortalPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProspectPortalPropertiesByPropertyTypeId( $intPropertyTypeId, $objDatabase ) {

		return CProperties::createService()->fetchProspectPortalPropertiesByWebsiteIdByPropertyTypeIdByCid( $this->getId(), $intPropertyTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchProspectPortalPropertyById( $intPropertyId, $objClientDatabase ) {

		return CProperties::createService()->fetchProspectPortalPropertyByIdByWebsiteIdByCid( $intPropertyId, $this->m_intId, $this->getCid(), $objClientDatabase );
	}

	public function fetchUnassociatedProperties( $arrobjPermissionedProperties, $intCompanyUserId, $objDatabase, $boolIsParentOnly = false ) {
		if( false == valArr( $arrobjPermissionedProperties ) ) return NULL;

		$arrobjAssociatedProperties = $this->fetchProperties( $objDatabase );

		if( false == valArr( $arrobjAssociatedProperties ) ) {
			return $arrobjPermissionedProperties;
		}

		$arrintUnassociatedPropertyIds = array_diff( array_keys( $arrobjPermissionedProperties ), array_keys( $arrobjAssociatedProperties ) );

		if( false == $boolIsParentOnly ) {
			return CProperties::createService()->fetchParentPropertiesByCompanyUserIdByPropertyIdsByCid( $intCompanyUserId, $arrintUnassociatedPropertyIds, $this->getCid(), $objDatabase );
		} else {
			return CProperties::createService()->fetchPropertiesByCompanyUserIdByPropertyIdsByCid( $intCompanyUserId, $arrintUnassociatedPropertyIds, $this->getCid(), $objDatabase );
		}
	}

	public function fetchUnassociatedPropertiesByCompanyUserId( $intCompanyUserId, $objDatabase, $boolIsParentOnly = true, $boolIsAdministrator = false ) {
		return CProperties::createService()->fetchUnAssociatedPropertiesByWebsiteIdByCompanyUserIdByCid( $this->getId(), $intCompanyUserId, $this->getCid(), $objDatabase, $boolIsParentOnly, $boolIsAdministrator );
	}

	public function fetchWebsiteTemplate( $objDatabase ) {
		return CWebsiteTemplates::createService()->fetchWebsiteTemplateById( $this->getWebsiteTemplateId(), $objDatabase );
	}

	public function fetchWebsiteTemplateWithWebsitesCount( $objDatabase ) {
		return CWebsiteTemplates::createService()->fetchWebsiteTemplateWithWebsiteCountById( $this->getWebsiteTemplateId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyById( $intPropertyId, $objDatabase ) {

		return CProperties::createService()->fetchPropertyByWebsiteIdByIdByCid( $this->m_intId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteBlog( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteBlogs::createService()->fetchwebsiteblogbywebsiteidbycid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteBlogs( $objDatabase ) {

		return \Psi\Eos\Entrata\CWebsiteBlogs::createService()->fetchWebsiteBlogsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteTabletButtons( $objDatabase ) {

		return \Psi\Eos\Entrata\CTabletButtons::createService()->fetchCustomTabletButtonsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchTabletButtonById( $intId, $objDatabase ) {

		return \Psi\Eos\Entrata\CTabletButtons::createService()->fetchCustomTabletButtonByIdByCid( $intId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePreferencesKeyedByKey( $objDatabase ) {

		$arrobjWebsitePreferences			= CWebsitePreferences::createService()->fetchWebsitePreferencesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjRekeyedWebsitePreferences	= rekeyObjects( 'Key', $arrobjWebsitePreferences );

		return $arrobjRekeyedWebsitePreferences;
	}

	public function fetchCachedWebsitePreferencesKeyedByKey( $objDatabase ) {
		$arrobjWebsitePreferences = [];
		$arrstrWebsitePreferences = CWebsitePreferences::createService()->fetchCustomWebsitePreferencesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrstrWebsitePreferences ) ) {
			$objWebsitePreference = $this->createWebsitePreference();

			foreach( $arrstrWebsitePreferences as $strKey => $arrstrWebsitePreference ) {
				$objWebsitePreferenceCloned = clone $objWebsitePreference;
				$objWebsitePreferenceCloned->setKey( $arrstrWebsitePreference['key'] );
				$objWebsitePreferenceCloned->setValue( $arrstrWebsitePreference['value'] );
				$arrobjWebsitePreferences[$arrstrWebsitePreference['key']] = $objWebsitePreferenceCloned;
			}
		}

		return $arrobjWebsitePreferences;
	}

	public function fetchWebsitePreferencesByKey( $strPreferenceKey, $objDatabase ) {
		$arrobjWebsitePreferences = [];
		$arrstrWebsitePreferences = CWebsitePreferences::createService()->fetchWebsitePreferencesByWebsiteIdByKeyByCid( $this->getId(), $strPreferenceKey, $this->getCid(), $objDatabase );

		if( true == valArr( $arrstrWebsitePreferences ) ) {

			$objWebsitePreference 		= $this->createWebsitePreference();
			$arrstrWebsitePreference 	= $arrstrWebsitePreferences[0];

			$objWebsitePreference->setKey( $arrstrWebsitePreference['key'] );
			$objWebsitePreference->setValue( $arrstrWebsitePreference['value'] );
			$arrobjWebsitePreferences[$arrstrWebsitePreference['key']] = $objWebsitePreference;
		}

		return $arrobjWebsitePreferences;
	}

	public function fetchTemplateSlotImages( $objClientDatabase ) {

		return CTemplateSlotImages::createService()->fetchTemplateSlotImagesByWebsiteTemplateIdByWebsiteIdByCid( $this->getWebsiteTemplateId(), $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchOldTemplateSlotImages( $intWebsiteTemplateId, $objClientDatabase ) {

		return CTemplateSlotImages::createService()->fetchTemplateSlotImagesByWebsiteTemplateIdByWebsiteIdByCid( $intWebsiteTemplateId, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchTemplateSlotImagesBySlotTypeId( $intSlotTypeId, $objClientDatabase ) {

		return CTemplateSlotImages::createService()->fetchTemplateSlotImagesByWebsiteTemplateIdByWebsiteIdByCidBySlotTypeId( $this->getWebsiteTemplateId(), $this->getId(), $this->getCid(), $intSlotTypeId, $objClientDatabase );
	}

	public function fetchWebsiteNotifications( $objDatabase ) {
		return CWebsiteNotifications::createService()->fetchCustomWebsiteNotificationsByWebsiteIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchCachedWebsiteNotifications( $objDatabase ) {
		return CWebsiteNotifications::createService()->fetchCachedWebsiteNotificationsByWebsiteIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchWebsiteNotificationByKey( $strKey, $objDatabase ) {
		return CWebsiteNotifications::createService()->fetchWebsiteNotificationByWebsiteIdByKeyByCid( $this->m_intId, $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteNotificationByKeys( $arrstrWebsiteNotificationKeys, $objDatabase ) {
		return CWebsiteNotifications::createService()->fetchWebsiteNotificationByWebsiteIdByKeysByCid( $this->m_intId, $arrstrWebsiteNotificationKeys, $this->getCid(), $objDatabase );
	}

    public function fetchViewWebsiteNotificationByKeys( $arrstrWebsiteNotificationKeys, $intWebsiteTemplateId, $objDatabase ) {
        return CWebsiteNotifications::createService()->fetchViewWebsiteNotificationByWebsiteIdByKeysByCid( $this->m_intId, $arrstrWebsiteNotificationKeys, $this->getCid(), $intWebsiteTemplateId, $objDatabase );
    }

	public function fetchPropertyByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CProperties::createService()->fetchPropertyByWebsiteIdByRemotePrimaryKeyByCid( $this->m_intId, $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchAssociatedWebsiteAmenities( $objDatabase ) {

		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchAssociatedWebsiteAmenitiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCommunityWebsiteAmenities( $objDatabase ) {

		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchCommunityAmenityFiltersByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFilteredAmenitiesByAmenityTypeIdByPropertyIds( $intAmenityTypeId, $arrintPropertyIds, $objDatabase ) {

		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchFilteredAmenitiesByAmenityTypeIdByWebsiteIdByPropertyIdsByCid( $intAmenityTypeId, $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchApartmentWebsiteAmenities( $objDatabase ) {

		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchApartmentAmenityFiltersByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAmenitiesByAmenityFilterIds( $arrintAmenityFilterIds, $objDatabase ) {
		if( false == valArr( $arrintAmenityFilterIds ) ) return NULL;

		return \Psi\Eos\Entrata\CWebsiteAmenities::createService()->fetchWebsiteAmenitiesByWebsiteIdByAmenityFilterIdsByCid( $this->getId(), $arrintAmenityFilterIds, $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePropertyDomainsCount( $objDatabase ) {

		return CWebsiteDomains::createService()->fetchWebsitePropertyDomainCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByWebsiteIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIdsKeyedByMediaTypeId( $arrintMediaTypeIds, $objDatabase ) {
		if( false == valArr( $arrintMediaTypeIds ) ) return NULL;

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByWebsiteIdByMediaTypeIdsKeyedByMediaTypeIdByCid( $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteMediaByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteMedias::createService()->fetchWebsiteMediaByWebsiteIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeId( $intMarketingMediaSubTypeId, $objDatabase, $boolIsSingleAssociation = false ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::WEBSITE_MEDIA, $intMarketingMediaSubTypeId, $this->getCid(), $objDatabase, $boolIsSingleAssociation );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase, $boolIsSingleAssociation = false ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdsByCid( $this->getId(), CMarketingMediaType::WEBSITE_MEDIA, $arrintMarketingMediaSubTypeIds, $this->getCid(), $objDatabase, $boolIsSingleAssociation );
	}

	public function fetchPropertyByPropertyNameByCity( $strPropertyName, $strCity, $objDatabase ) {
		return CProperties::createService()->fetchPropertyByWebsiteIdByPropertyNameByCityByCid( $this->getId(), $strPropertyName, $strCity, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBySeoPropertyName( $strSeoPropertyName, $objDatabase ) {
		return CProperties::createService()->fetchPropertyByWebsiteIdBySeoPropertyNameByCid( $this->getId(), $strSeoPropertyName, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteWrappers( $objDatabase ) {
		return CWebsiteWrappers::createService()->fetchWebsiteWrappersByWebsiteIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchWebsiteWrapperById( $intWebsiteWrapperId, $objDatabase ) {
		return CWebsiteWrappers::createService()->fetchWebsiteWrapperByWebsiteIdByIdByCid( $this->getId(), $intWebsiteWrapperId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteWrapperByWebsiteWrapperTypeId( $intWebsiteWrapperTypeId, $objDatabase ) {

		return CWebsiteWrappers::createService()->fetchWebsiteWrapperByWebsiteWrapperTypeIdByWebsiteIdByCid( $intWebsiteWrapperTypeId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchProspectPortalWebsitePropertiesCount( $objDatabase ) {
		return CWebsiteProperties::createService()->fetchProspectPortalWebsitePropertiesCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function getOrFetchProspectPortalWebsitePropertiesCount( $objDatabase ) {
		$intPropertyIdsCount = CCache::fetchObject( 'Website_PropertyIds_Count_' . $this->getCid() . '_' . $this->getId() );

		if( true == valId( $intPropertyIdsCount ) || ( 'integer' == gettype( $intPropertyIdsCount ) && 0 == $intPropertyIdsCount ) ) {
			return $intPropertyIdsCount;
		}
		$intPropertyIdsCount = CWebsiteProperties::createService()->fetchProspectPortalWebsitePropertiesCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		CCache::storeObject( 'Website_PropertyIds_Count_' . $this->getCid() . '_' . $this->getId(), $intPropertyIdsCount, $intSpecificLifetime = 1800, [] );

		return $intPropertyIdsCount;
	}

	public function fetchSingleRandomFeaturedPropertyByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		return CProperties::createService()->fetchSingleRandomFeaturedPropertyByWebsiteIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchFeaturedPropertiesByWebsiteId( $objDatabase, $strOrderBy = '' ) {

		return \CProperties::createService()->fetchFeaturedPropertiesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase, $strOrderBy );
	}

	public function fetchCorporatePropertiesByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		return CProperties::createService()->fetchCorporatePropertiesByWebsiteIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchRandomFeaturedPropertiesByPropertyIdsByLimit( $arrintPropertyIds, $objDatabase, $intLimit = NULL, $boolRandom = false, $strOrderByField = NULL ) {

		return CProperties::createService()->fetchRandomFeaturedPropertiesByWebsiteIdByPropertyIdsByLimitByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase, $intLimit, $boolRandom, $strOrderByField );
	}

	public function fetchWebsiteDocumentByDocumentId( $intDocumentId, $objDatabase ) {
		return CWebsiteDocuments::createService()->fetchWebsiteDocumentByWebsiteIdByDocumentIdByCid( $this->getId(), $intDocumentId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteRootPages( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteRootPages::createService()->fetchCustomWebsiteRootPagesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteRootPageByPageName( $strPageName, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteRootPages::createService()->fetchWebsiteRootPageByWebsiteIdByCidByPageName( $this->getId(), $this->getCid(), $strPageName, $objDatabase );
	}

	public function fetchDirectiveByDirectiveTypeId( $intDirectiveTypeId, $objDatabase ) {
		return \Psi\Eos\Connect\CDirectives::createService()->fetchDirectiveByWebsiteIdByCidByDirectiveTypeId( $this->getId(), $this->getCid(), $intDirectiveTypeId, $objDatabase );
	}

	public function fetchCompetingDirectiveCount( $objConnectDatabase ) {
		return \Psi\Eos\Connect\CDirectives::createService()->fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByWebsiteId( $this->getSubDomain(), CDirectiveType::WEBSITE_SUB_DOMAIN, $this->getId(), $objConnectDatabase );
	}

	public function fetchWebsiteSettingLogsByWebsiteSettingKeyId( $intWebsiteSettingKeyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteSettingLogs::createService()->fetchWebsiteSettingLogsByWebsiteSettingKeyIdByWebsiteIdByCid( $intWebsiteSettingKeyId, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		// It is required
		if( NULL == $this->m_intCid ) {
			trigger_error( __( 'A Client Id is required - {%s, 0}', [ 'CWebsite' ] ), E_USER_ERROR );
			$boolIsValid = false;
		}

		// Must be greater than zero
		if( 1 > $this->m_intCid ) {
			trigger_error( __( 'A Client Id has to be greater than zero - {%s, 0}', [ 'CWebsite' ] ), E_USER_ERROR );
			$boolIsValid = false;
		}

		// No Errors
		return $boolIsValid;
	}

	public function valWebsiteTemplateId() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_intWebsiteTemplateId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_template_id', __( 'Look and feel design is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > $this->m_intWebsiteTemplateId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_template_id', __( 'Look and feel design is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSubDomain( $objClientDatabase, $objConnectDatabase, $strAction = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strSubDomain ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Subdomain is required.' ) ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		if( true == \Psi\CStringService::singleton()->strstr( $this->m_strSubDomain, '\\' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Invalid characters in subdomain' ) ) );
			return false;
		}

		if( true == preg_match( '/\//', $this->m_strSubDomain ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Invalid characters in subdomain' ) ) );
			return false;
		}

		if( true == \Psi\CStringService::singleton()->strstr( $this->m_strSubDomain, '\\' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Invalid characters in subdomain' ) ) );
			return false;
		}

		if( true == preg_match( '/\'/', $this->m_strSubDomain ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Invalid characters in subdomain' ) ) );
			return false;
		}

		// Check for the http:// in the beginning of the domain and strip it if it is there
		$strHttp = 'http:';
		$strSlash = '/';

		$this->m_strSubDomain = str_replace( $strHttp, '', $this->m_strSubDomain );
		$this->m_strSubDomain = str_replace( $strSlash, '', $this->m_strSubDomain );

		// Sub domain can't have the word split in it.
		if( true == is_null( $strAction ) && VALIDATE_UPDATE !== $strAction ) {
			if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $this->m_strSubDomain ), 'split' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'The sub domain cannot contain the word \'split\'.' ) ) );
				$boolIsValid = false;
				return $boolIsValid;
			}
		}

		// Must not have any invalid characters. Check to make sure that there are no invalid characters
		if( false == preg_match( '/^([a-z0-9]+-)*[a-z0-9]+$/', $this->m_strSubDomain ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Invalid characters in subdomain' ) ) );
			$boolIsValid = false;
		}

		// It has to be not in the reserved list in the database
		if( 0 != \Psi\Eos\Entrata\CReservedSubdomains::createService()->fetchReservedSubdomainRowCount( $this->m_strSubDomain, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', __( 'Sub domain entered is already in use.' ) ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && 0 < $this->fetchCompetingDirectiveCount( $objConnectDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'The sub domain \'' . $this->getSubDomain() . '\' has already been taken.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		// It is required

		if( true == is_null( $this->m_strName ) || 0 >= strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Website name is required.' ) ) );
			$boolIsValid = false;

		} elseif( \Psi\CStringService::singleton()->strlen( $this->m_strName ) > 50 ) {
			// It has to be <= 50 long
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Website name cannot be longer than 50 characters.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		// It has to be <= 50 long
		if( \Psi\CStringService::singleton()->strlen( $this->m_strName ) > 50 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Website title cannot be longer than 50 characters' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valKeywords() {
		$boolIsValid = true;
		// Must be <= 4096
		if( \Psi\CStringService::singleton()->strlen( $this->m_strKeywords ) > 4096 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keywords', __( 'Website keywords cannot be longer than 4096 characters' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCaption() {
		$boolIsValid = true;
		// It has to be <= 50 long
		if( \Psi\CStringService::singleton()->strlen( $this->m_strCaption ) > 50 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'caption', __( 'Website caption cannot be longer than 50 characters' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		// Must be <= 320
		if( \Psi\CStringService::singleton()->strlen( $this->m_strDescription ) > 320 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Website description cannot be longer than 320 characters' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCompanyLogo() {
		$boolIsValid = true;
		// Must be <= 240
		if( \Psi\CStringService::singleton()->strlen( $this->m_strCompanyLogo ) > 50 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_logo', __( 'Website logo text cannot be longer than 50 characters' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSingularWebsiteTemplate( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == isset( $objClientDatabase ) ) {

			$objWebsiteTemplate 						= $this->fetchWebsiteTemplate( $objClientDatabase );
			$intProspectPortalWebsitePropertiesCount 	= $this->fetchProspectPortalWebsitePropertiesCount( $objClientDatabase );

			if( 1 < $intProspectPortalWebsitePropertiesCount && 1 == $objWebsiteTemplate->getIsSingular() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_template_id', __( 'This website design cannot be applied to a website having multiple Prospect Portal properties.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase, $objConnectDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valWebsiteTemplateId();
				$boolIsValid &= $this->valSubDomain( $objClientDatabase, $objConnectDatabase );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valKeywords();
				$boolIsValid &= $this->valCaption();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valCompanyLogo();
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valWebsiteTemplateId();
				$boolIsValid &= $this->valSubDomain( $objClientDatabase, $objConnectDatabase, VALIDATE_UPDATE );
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valKeywords();
				$boolIsValid &= $this->valCaption();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valCompanyLogo();
				$boolIsValid &= $this->valSingularWebsiteTemplate( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case self::VALIDATE_UPDATE_SUBDOMAIN:
				$boolIsValid &= $this->valSubDomain( $objClientDatabase, $objConnectDatabase );
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * DML Functions
	 */

	public function insert( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL ) {

		$this->validateDatabases( $objClientDatabase, $arrobjConnectDatabases );

		$strSql = parent::insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) return true;

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) return false;

		$objDirective = $this->createDirective( $objClientDatabase );
		$objDirective->fetchNextId( $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			if( false == $objDirective->insert( $intCurrentUserId, $objConnectDatabase ) ) return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL ) {

		$this->validateDatabases( $objClientDatabase, $arrobjConnectDatabases );

		$strSql = parent::update( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) return true;

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {

			$objDirective = $this->fetchDirectiveByDirectiveTypeId( CDirectiveType::WEBSITE_SUB_DOMAIN, $objConnectDatabase );
			if( true == valObj( $objDirective, 'CDirective' ) ) {
				$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getSubDomain() ) );
			}

			if( false == valObj( $objDirective, 'CDirective' ) || false == $objDirective->update( $intCurrentUserId, $objConnectDatabase ) ) {
				return false;
			}
		}

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) return false;

		return true;
	}

	public function delete( $intUserId, $objClientDatabase, $arrobjConnectDatabases = NULL ) {

		$this->validateDatabases( $objClientDatabase, $arrobjConnectDatabases );

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( true == $this->update( $intUserId, $objClientDatabase, $arrobjConnectDatabases ) ) {
			return true;
		}
	}

	public function validateDatabases( $objClientDatabase, $arrobjConnectDatabases ) {
		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valArr( $arrobjConnectDatabases ) ) {
			trigger_error( __( 'Failed to load database object' ), E_USER_ERROR );
			exit;
		}

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			if( false == valObj( $objConnectDatabase, 'CDatabase' ) || CDatabaseType::CONNECT != $objConnectDatabase->getDatabaseTypeId() ) {
				trigger_error( __( 'Failed to load database object' ), E_USER_ERROR );
				exit;
			}
		}
	}

	public function addDNSEntry() {

		// Include the necessary files

		// Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();
		$boolAddDNSEntry = $objManageDNSLibrary->addDNSEntry( $this );

		if( false == $boolAddDNSEntry ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to add DNS entry.' ) ) );
		}

		return $boolAddDNSEntry;
	}

	public function updateDNSEntry( $strOldDomain ) {

		// Include the necessary files

		// Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();

		$objDevDnsDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::DNS, CDatabaseUserType::PS_PROPERTYMANAGER );
		$intRecordCount = CRecords::fetchRecordCount( ' WHERE name ILIKE \'' . addslashes( $strOldDomain . CONFIG_PROSPECTPORTAL_DOMAIN_POSTFIX ) . '\'', $objDevDnsDatabase->createDataset() );

		if( 0 == $intRecordCount ) {
			$boolUpdateDNSEntry = $objManageDNSLibrary->addDNSEntry( $this );
		} else {
			$boolUpdateDNSEntry = $objManageDNSLibrary->updateDNSEntry( $this, $strOldDomain, $objDevDnsDatabase );
		}

		$objDevDnsDatabase->close();

		if( false == $boolUpdateDNSEntry ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update DNS Entry.' ) ) );
		}

		return $boolUpdateDNSEntry;
	}

	public function deleteDNSEntry() {

		// Include the necessary files

		// Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();
		$boolDeleteDNSEntry = $objManageDNSLibrary->deleteDNSEntry( $this );

		if( false == $boolDeleteDNSEntry ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to delete DNS Entry.' ) ) );
		}

		return $boolDeleteDNSEntry;
	}

	public function canCustomerLogin( $objDatabase ) {

		if( 0 >= ( int ) $this->getId() ) {
			trigger_error( __( 'Invalid Website Request: Website id not present - {%s, 0}', [ 'CWebsite::canCustomerLogin()' ] ), E_USER_ERROR );
		}

		// This function returns a count of the number of properties
		// associated with this website allow the resident with the id to login
		$intPropertyCount = CProperties::createService()->fetchPropertyCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		return 0 < $intPropertyCount;
	}

	public function fetchAllWebsiteCanonicalUrls( $strQuickSearch, $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchCustomWebsiteCanonicalUrlsByWebsiteIdByCid( $this->getId(), $this->getCid(), $strQuickSearch, $objDatabase );
	}

	public function fetchWebsiteCanonicalUrlsUrls( $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchWebsiteCanonicalUrlUrlsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCanonicalUrlsCanonicalUrls( $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchWebsiteCanonicalUrlCanonicalUrlsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedWebsiteCanonicalUrls( $strQuickSearch, $intPageNo, $intPageSize, $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchPaginatedWebsiteCanonicalUrlsByWebsiteIdByCid( $this->getId(), $this->getCid(), $strQuickSearch, $intPageNo, $intPageSize, $objDatabase );
	}

	public function fetchPaginatedWebsiteCanonicalUrlsCount( $strQuickSearch, $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchPaginatedWebsiteCanonicalUrlsCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $strQuickSearch, $objDatabase );
	}

	public function fetchWebsiteCanonicalUrlCanonicalsByWebsiteIdByCid( $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchWebsiteCanonicalUrlCanonicalsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCanonicalUrlByDomainName( $strDomainName, $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchCanonicalUrlByDomainNameByWebsiteIdByCid( $strDomainName, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCanonicalUrlById( $intWebsiteCanonicalUrlId, $objDatabase ) {
		return CWebsiteCanonicalUrls::createService()->fetchCustomWebsiteCanonicalUrlByIdByWebsiteIdByCid( $intWebsiteCanonicalUrlId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCanonicalUrlsByIds( $arrintWebsiteCanonicalUrlIds, $objDatabase ) {
		if( false == valArr( $arrintWebsiteCanonicalUrlIds ) ) return NULL;

		return CWebsiteCanonicalUrls::createService()->fetchWebsiteCanonicalUrlsByIdsByWebsiteIdByCid( $arrintWebsiteCanonicalUrlIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDomainsCertificate( $intWebsiteDomainId, $objDatabase ) {
		return CWebsiteDomainsCertificates::createService()->fetchWebsiteDomainsCertificateByWebsiteIdByDomainIdByCid( $this->getId(), $intWebsiteDomainId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCertificate( $intCertificateId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteCertificates::createService()->fetchWebsiteCertificateByCertificateIdByCid( $intCertificateId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCertificateByAuthUrl( $strAuthUrl, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteCertificates::createService()->fetchWebsiteCertificateByAuthUrlIdByCid( $strAuthUrl, $this->getCid(), $objDatabase );
	}

	public function fetchActiveWebsiteDomainsCertificates( $objDatabase ) {
		return CWebsiteDomainsCertificates::createService()->fetchWebsiteDomainsCertificatesByWebsiteIdByIsActiveByCid( $this->getId(), $boolIsActive = true, $this->getCid(), $objDatabase );
	}

	public function fetchActiveWebsiteDomainsCertificateByDomainId( $intDomainId, $objDatabase ) {
		return CWebsiteDomainsCertificates::createService()->fetchWebsiteDomainsCertificateByWebsiteIdByDomainIdByIsActiveByCid( $this->getId(), $intDomainId, $boolIsActive = true, $this->getCid(), $objDatabase );
	}

	/**
	 * Entrata Powered Blog (Prospect Portal) Functions
	 */

	public function fetchBlogPostsByWordpressPostId( $intWpid, $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostsByCidByWebsiteIdByWordpressPostId( $this->getCid(), $this->getId(), $intWpid, $objDatabase );
	}

	public function fetchEntrataPoweredBlogPosts( $objDatabase, $intPageNumber = 1, $boolIsAllPostsCount = false ) {
		return CBlogPosts::createService()->fetchEntrataPoweredBlogPostsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intPageNumber, $boolIsAllPostsCount );
	}

	public function fetchEntrataPoweredBlogPostsCount( $objDatabase ) {
		return CBlogPosts::createService()->fetchEntrataPoweredBlogPostsCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogTags( $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchRecentBlogPosts( $objDatabase ) {
		return CBlogPosts::createService()->fetchRecentBlogPostsByWebsiteIdByCid( $this->getId(), $this->getCid(), $intLimit = 10, $objDatabase );
	}

	public function fetchEntrataPoweredBlogPostById( $intId, $objDatabase, $boolIsHttpReferrerEntrata = false ) {
		return CBlogPosts::createService()->fetchEntrataPoweredBlogPostByIdByWebsiteIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase, $boolIsHttpReferrerEntrata );
	}

	public function fetchEntrataPoweredBlogPostByUrl( $strUrl, $objDatabase, $boolIsHttpReferrerEntrata = false ) {
		return CBlogPosts::createService()->fetchEntrataPoweredBlogPostByUrlByWebsiteIdByCid( $strUrl, $this->getId(), $this->getCid(), $objDatabase, $boolIsHttpReferrerEntrata );
	}

	public function fetchBlogPostTagsByPostId( $intPostId, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogPostTagsByPostIdByWebsiteIdByCid( $intPostId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostsByTagId( $intTagId, $objDatabase, $intPageNumber = 1, $boolIsAllPostsCount = false ) {
		return CBlogPosts::createService()->fetchBlogPostsByTagIdByWebsiteIdByCid( $intTagId, $this->getId(), $this->getCid(), $objDatabase, $intPageNumber, $boolIsAllPostsCount );
	}

	public function fetchBlogPostsByTagUrl( $strTagUrl, $objDatabase, $intPageNumber = 1, $boolIsAllPostsCount = false ) {
		return CBlogPosts::createService()->fetchBlogPostsByTagUrlByWebsiteIdByCid( $strTagUrl, $this->getId(), $this->getCid(), $objDatabase, $intPageNumber, $boolIsAllPostsCount );
	}

	public function fetchBlogTagById( $intId, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagByIdByWebsiteIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostTagsByTagId( $intId, $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogPostTags::createService()->fetchBlogPostTagByBlogTagIdByWebsiteIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogTagByUrl( $strUrl, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagByUrlByWebsiteIdByCid( $strUrl, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostsByWordpressTagId( $intWtid, $objDatabase ) {
		return CBlogTags::createService()->fetchBlogTagByWebsiteIdByCidByWordpressTagId( $this->getId(), $this->getCid(), $intWtid, $objDatabase );
	}

	public function fetchWebsiteAds( $objDatabase ) {
		return CWebsiteAds::createService()->fetchWebsiteActiveAdsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAdsCount( $objDatabase ) {
		return CWebsiteAds::createService()->fetchWebsiteActiveAdsCountByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAdsWithMarketingMedia( $objDatabase ) {
		return CWebsiteAds::createService()->fetchWebsiteActiveAdsWithMarketingMediaByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAd( $intWebsiteAdId, $objDatabase ) {
		return CWebsiteAds::createService()->fetchWebsiteAdByIdByWebsiteIdByCid( $intWebsiteAdId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteAdRunById( $intId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteAdRuns::createService()->fetchWebsiteAdRunByIdByWebsiteIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveWebsiteAdRuns( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteAdRuns::createService()->fetchActiveWebsiteAdRunsByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteLocales( $objDatabase ) {
		return CWebsiteLocales::createService()->fetchWebsiteLocalesByWebsiteIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogPostIdByFilterLanguageLocalCodes( $arrmixFilterData, $objDatabase ) {
		return CBlogPosts::createService()->fetchBlogPostIdsByCidByWebsiteIdByFilterLanguageLocalCodes( $this->getCid(), $this->getId(), $arrmixFilterData, $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::WEBSITE_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase );
	}

}
?>