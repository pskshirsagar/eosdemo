<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplates
 * Do not add any new functions to this class.
 */

class CFeeTemplates extends CBaseFeeTemplates {

	public static function fetchFeeTemplatesByPropertyIdsByFeeTypeIdCid( $arrintPropertyIds, $intFeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '
					SELECT
						fee_template_data.*
					FROM
						(
						SELECT
							DISTINCT ft.id,
							ft.cid,
							ft.name,
							CASE
								WHEN 1 < COUNT( ftpg.property_group_id ) OVER( PARTITION BY ftpg.fee_template_id ) THEN \'Multiple\'
								ELSE p.property_name
							END AS property_name,
							COUNT( ftpg.property_group_id ) OVER ( PARTITION BY ftpg.fee_template_id ) AS property_count,
							ft.is_disabled
						FROM
							fee_templates ft
							LEFT JOIN fee_template_property_groups ftpg ON ( ftpg.fee_template_id = ft.id AND ft.cid = ftpg.cid AND ftpg.deleted_by IS NULL )
							LEFT JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
							LEFT JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
							LEFT JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
						WHERE
							ft.cid = ' . ( int ) $intCid . '
							AND ( ftpg.property_group_id IS NULL OR p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							AND ft.fee_type_id = ' . ( int ) $intFeeTypeId . '
							AND NOT ft.is_disabled
						UNION
						SELECT
							DISTINCT ft.id,
							ft.cid,
							ft.name,
							NULL AS property_name,
							0 AS property_count,
							ft.is_disabled
						FROM
							fee_templates ft
						WHERE
							ft.cid = ' . ( int ) $intCid . '
							AND ft.is_disabled = true
					) AS fee_template_data
					ORDER BY
					    fee_template_data.name';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplateByIdByCid( $intFeeTemplateId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ft.*,
						apl.ap_payee_id
					FROM
						fee_templates AS ft
						LEFT JOIN ap_payee_locations AS apl ON ( ft.ap_payee_location_id = apl.id AND apl.cid = ft.cid )
					WHERE
						ft.cid = ' . ( int ) $intCid . '
						AND ft.id = ' . ( int ) $intFeeTemplateId;

		return self::fetchFeeTemplate( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatesByIdsByCid( $arrintFeeTemplateIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintFeeTemplateIds ) ) return NULL;

		$strSql = 'SELECT
						ft.*,
						apl.ap_payee_id,
						art.name AS ap_routing_tag_name
					FROM
						fee_templates AS ft
						JOIN ap_payee_locations AS apl ON ( ft.ap_payee_location_id = apl.id AND apl.cid = ft.cid )
						LEFT JOIN ap_routing_tags AS art ON ( ft.cid = art.cid AND ft.ap_routing_tag_id = art.id )
					WHERE
						ft.cid = ' . ( int ) $intCid . '
						AND ft.id IN ( ' . implode( ',', $arrintFeeTemplateIds ) . ' )';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatesCountByGlAccountIdNotByGLAccountUsageTypeIdByCid( $intApGlAccountId, $intGlAccountUsageTypeId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApGlAccountId ) || false == is_numeric( $intGlAccountUsageTypeId ) ) return 0;

		$strSql = 'SELECT
						COUNT( ft.id )
					FROM
						fee_templates ft
						JOIN gl_accounts ga ON ( ft.cid = ga.cid AND ft.gl_account_id = ga.id )
						JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )
					WHERE
						ft.gl_account_id = ' . ( int ) $intApGlAccountId . '
						AND gaut.id <> ' . ( int ) $intGlAccountUsageTypeId . '
						AND ft.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchActiveFeeTemplatesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeLocationId ) ) return 0;

		$strSql = 'SELECT
						*
					FROM
						fee_templates
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND NOT is_disabled';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

	public static function fetchOwnerFeeTemplatesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ft.*,
						CASE
							WHEN 1 < sub_pft.property_count THEN \'Multiple Properties\'
							ELSE p.property_name
						END AS property_name,
						sub_pft.property_count,
						fct.name AS fee_calculation_type_name
					FROM
						fee_templates ft
						JOIN fee_calculation_types fct ON ( fct.id = ft.fee_calculation_type_id )
						LEFT JOIN fee_template_property_groups ftpg ON ( ftpg.fee_template_id = ft.id AND ft.cid = ftpg.cid )
						LEFT JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						LEFT JOIN properties p ON ( p.id = pga.property_id AND pga.cid = p.cid )
						LEFT JOIN LATERAL (
							SELECT
								COUNT( DISTINCT pga.property_id ) AS property_count 
							FROM
								fee_template_property_groups ftpg
								JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
								JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
							WHERE
								ftpg.cid = ' . ( int ) $intCid . '
								AND ftpg.fee_template_id = ft.id
								AND ftpg.cid = p.cid
						) sub_pft ON TRUE 
					WHERE
						ft.cid = ' . ( int ) $intCid . '
						AND fee_type_id = ' . CFeeType::OWNER_DISTRIBUTIONS . '
					ORDER BY
						ft.name';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplateByCidByFeeId( $intCid, $intFeeId, $objClientDatabase ) {

		if( false == valId( $intFeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						fee_templates ft
						JOIN fees f ON ( ft.cid = f.cid AND ft.id = f.fee_template_id ) 
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.id = ' . ( int ) $intFeeId;

		return self::fetchFeeTemplate( $strSql, $objClientDatabase );
	}

	public static function fetchActiveFeeTemplatesByFeeTypeIdByCid( $arrintFeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $arrintFeeTypeId ) ) return NULL;

		$strSql = 'SELECT
						ft.*,
						apl.ap_payee_id
					FROM
						fee_templates AS ft
						JOIN ap_payee_locations AS apl ON ( ft.ap_payee_location_id = apl.id AND apl.cid = ft.cid )
					WHERE
						ft.cid =' . ( int ) $intCid . '
						AND ft.fee_type_id = ' . ( int ) $arrintFeeTypeId . '
						AND NOT ft.is_disabled';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

	public static function fetchActiveFeeTemplatesByIdsByPropertyIdFeeTypeIdByCid( $arrintFeeTemplateIds, $intPropertyId, $arrintFeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $arrintFeeTypeId ) || false == valArr( $arrintFeeTemplateIds ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ft.*,
						apl.ap_payee_id
					FROM
						fee_templates AS ft
						LEFT JOIN fee_template_property_groups ftpg ON ( ftpg.fee_template_id = ft.id AND ft.cid = ftpg.cid AND ftpg.deleted_by IS NULL )
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN ap_payee_locations AS apl ON ( ft.ap_payee_location_id = apl.id AND apl.cid = ft.cid )
					WHERE
						ft.cid =' . ( int ) $intCid . '
						AND ft.id IN ( ' . implode( ',', $arrintFeeTemplateIds ) . ' )
						AND ft.fee_type_id = ' . ( int ) $arrintFeeTypeId . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ftpg.deleted_by IS NULL
						AND ftpg.deleted_on IS NULL
						AND NOT ft.is_disabled';

		return self::fetchFeeTemplates( $strSql, $objClientDatabase );
	}

}
?>