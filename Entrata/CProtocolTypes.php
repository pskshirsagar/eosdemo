<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProtocolTypes
 * Do not add any new functions to this class.
 */

class CProtocolTypes extends CBaseProtocolTypes {

	public static function fetchProtocolTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CProtocolType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchProtocolType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CProtocolType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>