<?php

class CWebsiteCertificate extends CBaseWebsiteCertificate {

	const TYPE_LETS_ENCRYPT = 2;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setPublicKeyEncrypted( $strPublicKey ) {
		if( true == valStr( $strPublicKey ) ) {
			$this->setPublicKey( Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPublicKey, CConfig::get( 'sodium_key_ssl_certificate' ) ) );
		}
	}

	public function getPublicKeyDecrypted() {
		if( false == valStr( $this->getPublicKey() ) ) {
			return NULL;
		}
		return Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getPublicKey(), CConfig::get( 'sodium_key_ssl_certificate' ), [ 'legacy_secret_key' => CConfig::get( 'key_encrypt_decrypt_file' ) ] );
	}

	public function setPrivateKeyEncrypted( $strPrivateKey ) {
		if( true == valStr( $strPrivateKey ) ) {
			$this->setPrivateKey( Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPrivateKey, CConfig::get( 'sodium_key_ssl_certificate' ) ) );
		}
	}

	public function getPrivateKeyDecrypted() {
		if( false == valStr( $this->getPrivateKey() ) ) {
			return NULL;
		}
		return Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getPrivateKey(), CConfig::get( 'sodium_key_ssl_certificate' ), [ 'legacy_secret_key' => CConfig::get( 'key_encrypt_decrypt_file' ) ] );
	}

}
?>