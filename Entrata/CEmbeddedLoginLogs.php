<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmbeddedLoginLogs
 * Do not add any new functions to this class.
 */

class CEmbeddedLoginLogs extends CBaseEmbeddedLoginLogs {

	public static function fetchEmbeddedLoginLogByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'SELECT * FROM
						 embedded_login_logs
					WHERE
						 cid = ' . ( int ) $intCid . '
						 AND property_id = ' . ( int ) $intPropertyId . '
					LIMIT 1';

		return self::fetchEmbeddedLoginLog( $strSql, $objDatabase );
	}
}
?>