<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxIdAssociations
 * Do not add any new functions to this class.
 */

class CTaxIdAssociations extends CBaseTaxIdAssociations {

	public static function fetchTaxIdAssociationByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchTaxIdAssociation( sprintf( 'SELECT * FROM tax_id_associations WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchTaxIdAssociationByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
			return self::fetchTaxIdAssociation( sprintf( 'SELECT * FROM tax_id_associations WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

}
?>