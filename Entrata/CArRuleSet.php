<?php

class CArRuleSet extends CBaseArRuleSet {

	protected $m_intAssignedPropertiesCount;

	public function setAssignedPropertiesCount( $intAssignedPropertiesCount ) {
		return $this->m_intAssignedPropertiesCount = $intAssignedPropertiesCount;
	}

	public function getAssignedPropertiesCount() {
		return $this->m_intAssignedPropertiesCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['assigned_properties_count'] ) ) $this->setAssignedPropertiesCount( $arrmixValues['assigned_properties_count'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArRuleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Rule Name is required.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {
			$intConfilictingArRuleSetCount = \Psi\Eos\Entrata\CArRuleSets::createService()->fetchConflictingNameCount( $this, $objDatabase );
			if( 0 < $intConfilictingArRuleSetCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Rule name is already in use. Enter a unique rule name.' ) ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createArRuleSetProperty() {
		$objArRuleSetProperty = new CArRuleSetProperty();
		$objArRuleSetProperty->setCid( $this->getCid() );
		$objArRuleSetProperty->setArRuleTypeId( $this->getArRuleTypeId() );
		$objArRuleSetProperty->setArRuleSetId( $this->getId() );

		return $objArRuleSetProperty;
	}

}
?>