<?php

class CDefaultReport extends CBaseDefaultReport {

	const AVAILABILITY							= 15;
	const BUILDINGS_AND_UNITS					= 21;
	const INSURANCE_POLICY_DETAILS				= 106;
	const LEAD_DATA								= 134;
	const RESIDENT_AGED_RECEIVABLES				= 191;
	const RESIDENT_DEPOSIT_AUDIT				= 195;
	const RENTABLE_ITEMS						= 200;
	const GL_DETAILS							= 307;
	const RESIDENT_DATA							= 329;
	const RENT_ROLL								= 380;
	const MARKET_RENT_SCHEDULE					= 507;
	const RECURRING_CHARGES_FORECAST			= 192;
	const SCREENING_BILLING_DETAILS				= 275;
	const LEAD_DETAILS                          = 278;
	const INCOME_STATEMENT_BUDGET_VS_ACTUALS    = 100;

	// Implementation Verification Reports
	public static $c_arrintImplementationVerficationReportNames = [
		self::AVAILABILITY,
		self::BUILDINGS_AND_UNITS,
		self::INSURANCE_POLICY_DETAILS,
		self::LEAD_DATA,
		self::RESIDENT_AGED_RECEIVABLES,
		self::RESIDENT_DEPOSIT_AUDIT,
		self::RENTABLE_ITEMS,
		self::GL_DETAILS,
		self::RESIDENT_DATA,
		self::RENT_ROLL,
		self::MARKET_RENT_SCHEDULE,
		self::RECURRING_CHARGES_FORECAST
	];

	/*
	 * Get Functions
	 */

	/*
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	/*
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportTypeId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intReportTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Report Type', 'Report Type is required..</br>' ) );
		}
		return $boolIsValid;
	}

	public function valDefaultReportGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle( $strAction, $objDatabase ) {
		$boolIsValid = true;
		if( true == empty( $this->m_strTitle ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Name is required.</br>' ) );
		} else {
			$strWhere = '
					WHERE
						title ILIKE \'' . addslashes( $this->m_strTitle ) . '\' ';

			if( VALIDATE_UPDATE == $strAction ) {
				$strWhere .= ' AND id != ' . $this->m_intId;
			}

			if( false == is_null( $objDatabase ) ) {
				$intExistingReportsCount = \Psi\Eos\Entrata\CDefaultReports::createService()->fetchDefaultReportCount( $strWhere, $objDatabase );
				if( 0 < $intExistingReportsCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Report with this title is already exists.' ) );
				}

			}
		}

		if( true == empty( $this->m_intDefaultReportGroupId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Parent', 'Parent is required.</br>' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == isset( $this->m_strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAllowedForApi() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle( $strAction, $objDatabase );
				$boolIsValid &= $this->valReportTypeId();
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_UPDATE_DESCRIPTION:
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
