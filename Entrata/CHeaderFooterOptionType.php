<?php

class CHeaderFooterOptionType extends CBaseHeaderFooterOptionType {

	private $m_arrstrHeaderFooterOptionTypes;

	const HEADER_FOOTER_OPTION_LOGO				= 1;
	const HEADER_FOOTER_OPTION_HEADER			= 2;
	const HEADER_FOOTER_OPTION_FOOTER			= 3;
	const HEADER_FOOTER_OPTION_HEADER_FOOTER	= 4;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getHeaderFooterOptionTypes() {
		if( isset( $this->m_arrstrHeaderFooterOptionTypes ) ) {
			return $this->m_arrstrHeaderFooterOptionTypes;
		}

		$this->m_arrstrHeaderFooterOptionTypes = [
			self::HEADER_FOOTER_OPTION_LOGO 			=> __( 'Property Logo Only' ),
			self::HEADER_FOOTER_OPTION_HEADER 			=> __( 'Property Header Only' ),
			self::HEADER_FOOTER_OPTION_FOOTER 			=> __( 'Property Footer Only' ),
			self::HEADER_FOOTER_OPTION_HEADER_FOOTER 	=> __( 'Property Header & Footer' )
		];

		return $this->m_arrstrHeaderFooterOptionTypes;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>