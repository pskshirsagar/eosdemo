<?php

class CMediaType extends CBaseMediaType {

	const OVERVIEW								= 1;
	const PHOTO 								= 2;
	const TYPE_360 								= 3;
	const FLOORPLAN 							= 4;
	const APPLICATION 							= 5;
	const TEMPLATE_IMAGE 						= 6;
	const PAGE_IMAGE 							= 7;
	const TEMPLATE_SWF 							= 8;
	const PAGE_SWF 								= 9;
	const COMPANY_PREFERENCES_DEFAULT_IMAGE		= 10;
	const VIDEO 								= 11;
	const CUSTOMER_DEFAULT_IMAGE				= 12;
	const SITE_PLAN								= 13;
	const LOGO									= 14;
	const SCHEDULED_CALL						= 15;
	const DIRECTIONS_MAP						= 16;
	const PORTFOLIO_LOGO						= 17;
	const TESTIMONIAL_PHOTO						= 18;
	const WEBSITE_MUSIC							= 19;
	const WEBSITE_FAVICON						= 20;
	const COMPANY_AREA_MAP_OVERLAY_PHOTO		= 22;
	const FLOORPLAN_3D							= 23;
	const SPLASH_IMAGE							= 24;
	const GREETINGS_AUDIO_RECORDINGS			= 25;
	const CALL_LOG_RECORDINGS					= 26;
	const CRAIGSLIST_ADVERTISEMENT_PHOTO		= 27;
	const FLOOR									= 29;
	const COMPANY_TAB_IMAGE						= 31;
	const CRAIGSLIST_LOGO						= 34;
	const MESSAGE_CENTER						= 35;
	const CHARITY_LOGO							= 36;
	const UTILITY_INVOICE_RESIDENT_MESSAGE		= 37;
	const RESIDENT_CLUB_IMAGE					= 38;
	const COMPANY_AREA_DEFAULT_IMAGE			= 39;
	const COMPANY_AREA_SMALL_IMAGE				= 40;
	const COMPANY_AREA_LARGE_IMAGE				= 41;
	const COMPANY_AREA_SEARCH_RESULT_IMAGE		= 42;
	const CRAIGSLIST_CUSTOM_HEADER				= 43;
	const CLUB_IMAGE							= 44;
	const COMPANY_EVENT_IMAGE					= 45;
	const CRAIGSLIST_PROPERTY_LOGO				= 46;
	const LEASING_BINDER_RESOURCES				= 47;
	const LEASING_BINDER_POLICIES				= 48;
	const LEASING_BINDER_FAQ					= 49;
	const SITE_TABLET_LANDSCAPE					= 51;
	const SITE_TABLET_LANDSCAPE_HEADER			= 52;
	const SITE_TABLET_POTRAIT					= 53;
	const SITE_TABLET_POTRAIT_HEADER			= 54;
	const SITE_PLAN_PDF 						= 55;
	const LOBBY_DISPLAY_LOGO					= 56;
	const LOBBY_DISPLAY_SLIDE					= 57;
	const LOBBY_DISPLAY_SKYSCRAPPER				= 58;
	const LOBBY_DISPLAY_STOCK_SLIDE				= 60;
	const LOBBY_DISPLAY_STOCK_SKYSCRAPPER		= 61;
	const BUILDING								= 62;
	const FLOORPLAN_PDF 						= 63;
	const CRAIGSLIST_POST_CUSTOM_IMAGES			= 64;
	const FLOOR_CITY_VIEW 						= 65;
	const UNIT_FLOORPLAN 						= 67;
	const ADD_ON_CATEGORY 						= 68;
	const PROPERTY_EMAIL_HEADER_IMAGE			= 70;
	const PROPERTY_EMAIL_FOOTER_IMAGE			= 71;
	const PROPERTY_SEARCH_MARKER_IMAGE			= 72;
	const MAP_AND_DIRECTIONS_CONTENT_IMAGE		= 73;
	const CUSTOM_AD_IMAGE						= 74;
	const GOOGLE_AND_MATTERPORT_TOUR_360 		= 75;
	const SITE_TABLET_LOGO						= 77;
	const BLOG_MEDIA							= 78;
	const MARKETING_SPACE_IMAGE					= 79;
    const VIDEO_SLIDER_HTML_POSTER_IMAGE		= 80;
	const WIDGET_MEDIA						    = 81;
	const SHIPPING_VENDOR_ICON					= 82;
	const SITETABLET_ASSET_LIBRARY_PDF			= 83;
	const SITETABLET_ASSET_LIBRARY_PHOTO		= 84;
	const REPUTATION_ADVISOR_POSTING_TOOL		= 85;
	const COMPANY_CONCIERGE_SERVICE_IMAGE	    = 87;
	const ANNOUNCEMENT_IMAGE					= 88;
	const GUEST_IMAGE							= 89;

	public static function assignSmartyVideosAndVirtualToursMediaConstants( $objSmarty ) {

		$objSmarty->assign( 'MEDIA_TYPE_360', self::TYPE_360 );
		$objSmarty->assign( 'MEDIA_TYPE_VIDEO', self::VIDEO );
		self::assignSmartyConstants( $objSmarty );
	}

}
?>