<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserWebsites
 * Do not add any new functions to this class.
 */

class CCompanyUserWebsites extends CBaseCompanyUserWebsites {

	public static function fetchCompanyUserWebsitesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchCompanyUserWebsites( sprintf( 'SELECT * FROM company_user_websites WHERE cid = ' . ( int ) $intCid . ' AND company_user_id = %d', ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchCompanyUserWebsiteByWebsiteIdCompanyUserIdByCid( $intWebsiteId, $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						company_user_websites
					WHERE
						website_id = ' . ( int ) $intWebsiteId . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserWebsite( $strSql, $objDatabase );
	}

}
?>