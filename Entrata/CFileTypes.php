<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTypes
 * Do not add any new functions to this class.
 */

class CFileTypes extends CBaseFileTypes {

	public static function fetchFileTypeBySystemCodeByCid( $strSystemCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT *	FROM file_types	WHERE cid = ' . ( int ) $intCid . '	AND system_code = \'' . addslashes( $strSystemCode ) . '\' LIMIT 1 ';
		return self::fetchFileType( $strSql, $objDatabase );
	}

	public static function fetchFileTypeByNameByCid( $strAttachmentTypeName, $intCid, $objDatabase ) {
		$strSql = 'SELECT *	FROM file_types	WHERE cid = ' . ( int ) $intCid . '	AND name = \'' . addslashes( $strAttachmentTypeName ) . '\' LIMIT 1 ';
		return self::fetchFileType( $strSql, $objDatabase );
	}

	public static function fetchFileTypesBySystemCodesByCid( $arrstrSystemCodes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrSystemCodes ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						file_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
					ORDER BY 
						id';

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomFileTypesByCid( $intCid, $objDatabase, $intIsHidden = NULL, $boolIsReturnArray = false, $strExcludingFileType = NULL ) {

		$strSql = 'SELECT 
						*
					FROM 
						file_types 
					WHERE 
						cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL ';

		if( false == is_null( $intIsHidden ) ) {
			$strSql .= ' AND is_hidden = ' . ( int ) $intIsHidden;
		}

		if( true == valStr( $strExcludingFileType ) ) {
			$strSql .= ' AND ( system_code <> \'' . $strExcludingFileType . '\' OR system_code IS NULL )';
		}

		$strSql .= ' ORDER BY ' . $objDatabase->getCollateSort( 'name', true );

		if( true == $boolIsReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchConflictingNameCount( $objConflictingFileType, $objDatabase ) {

		$intId = ( false == is_numeric( $objConflictingFileType->getId() ) ) ? 0 : $objConflictingFileType->getId();

		$strWhereSql = ' WHERE
							cid = ' . ( int ) $objConflictingFileType->getCid() . '
							AND id <> ' . ( int ) $intId .
		               ' AND name = \'' . ( string ) addslashes( $objConflictingFileType->getName() ) . '\'
							AND deleted_by IS NULL
						LIMIT 1';

		return self::fetchFileTypeCount( $strWhereSql, $objDatabase );
	}

	public static function fetchOrderedFileTypesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						file_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND ( ( is_system = 0 ) OR ( is_system = 1 AND is_published = 1 ) )
						AND is_hidden = 0
					ORDER BY
						is_system, ' . $objDatabase->getCollateSort( 'name', true );

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedLeaseFileTypesByCid( $intCid, $objDatabase ) {
		return self::fetchFileTypes( sprintf( 'SELECT * FROM file_types WHERE cid = %d AND is_published = 1 AND deleted_on IS NULL', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileTypeIdBySystemCodeByCid( $strSystemCode, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						id
					FROM
						file_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND system_code = ' . pg_escape_literal( $objDatabase->getHandle(), $strSystemCode ) . '
					LIMIT 1 ';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchFileTypeBySystemCodeByCids( $strSystemCode, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return false;
		$strSql = 'SELECT *	FROM file_types	WHERE cid in ( ' . implode( ',', $arrintCids ) . ' ) AND system_code = \'' . addslashes( $strSystemCode ) . '\' ';
		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypesByCustomFilterByCid( $strWhereSql, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM file_types WHERE ' . $strWhereSql . ' AND is_published = 1 AND is_hidden = 0 AND deleted_on IS NULL AND cid = ' . ( int ) $intCid . ' ORDER BY ' . $objDatabase->getCollateSort( 'name', true );
		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypesByIdsByCid( $arrintFileTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileTypeIds ) ) return NULL;
		$strSql = 'SELECT * FROM file_types WHERE id IN ( ' . implode( ',', $arrintFileTypeIds ) . ') AND cid = ' . ( int ) $intCid;
		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypeNamesByIdsByCid( $arrintFileTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileTypeIds ) ) return NULL;

		$strSql = 'SELECT string_agg( name , \', \')  file_type_names FROM  file_types  WHERE id IN ( ' . implode( ',', $arrintFileTypeIds ) . ') AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileTypesByIdsByCids( $arrintFileTypeIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintFileTypeIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						id,
						name
					FROM
						file_types
					WHERE
						id IN ( ' . implode( ',', $arrintFileTypeIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchDocumentStorageFileTypesByCustomFilterByCid( $strWhereSql, $intCid, $objDatabase ) {
		$strSql = 'SELECT id, name, system_code, entity_type_ids, file_type_group_id FROM file_types WHERE ' . $strWhereSql . ' AND is_published = 1 AND deleted_on IS NULL AND cid = ' . ( int ) $intCid . ' ORDER BY ' . $objDatabase->getCollateSort( 'name', true );
		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomFileTypesByIdsByCid( $arrintFileTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileTypeIds ) ) return NULL;

		$strSql = 'SELECT id, name FROM file_types WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintFileTypeIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileTypeByIdBySystemCodesByCid( $intFileTypeId, $arrstrSystemCodes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrSystemCodes ) ) return NULL;

		$strSql = 'SELECT * FROM 
						file_types
					WHERE 
						id = ' . ( int ) $intFileTypeId . '
						AND is_published = 1 
						AND deleted_on IS NULL
						AND system_code IN ( \'' . implode( "', '", $arrstrSystemCodes ) . '\' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileType( $strSql, $objDatabase );
	}

	public static function fetchFileTypeBySystemCodeById( $intFileTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT *	FROM file_types	WHERE cid = ' . ( int ) $intCid . '	AND id = ' . ( int ) $intFileTypeId . ' LIMIT 1 ';
		return self::fetchFileType( $strSql, $objDatabase );
	}

	public static function fetchCustomFileTypesForFileExportAndBackupByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						file_types
					WHERE 
						( ' . CEntityType::RESIDENT . ' = ANY( entity_type_ids ) OR ' . CEntityType::LEAD . ' = ANY( entity_type_ids ) )
						AND ( ( system_code IN ( \'' . implode( '\',\'', CFileType::$c_arrstrDocStorageFileTypes ) . '\' ) ) OR ( is_system = 0 AND system_code IS NULL ) )
					    AND is_published = 1 
					    AND deleted_on IS NULL 
					    AND cid = ' . ( int ) $intCid . ' 
					ORDER BY ' . $objDatabase->getCollateSort( 'name', true );

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypesSystemCodeByIdsByCid( $arrintFileTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFileTypeIds ) ) return NULL;
		$strSql = 'SELECT id, system_code, name FROM file_types WHERE id IN ( ' . implode( ',', $arrintFileTypeIds ) . ') AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomFileTypeBySystemCodeByCid( $strSystemCode, $intCid, $objDatabase ) {
		if( false == valStr( $strSystemCode ) ) return NULL;

		// FixMe: Entire additional TICs generation logic needs be get refactored.
		$arrstrExcludedFileTypeSystemCodes = [ CFileType::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC, CFileType::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC ];

		$strSql = '
					SELECT
						ft.id AS file_type_id,
						CASE
							WHEN ft.system_code = \'CA2_TIC\' THEN REPLACE ( REPLACE ( INITCAP ( ft.name ), \'Ctcac_Tic\', \'CTCAC TIC\' ), \'_\', \' \')
							WHEN ft.system_code = \'CA_TIC\' THEN REPLACE ( REPLACE ( INITCAP ( ft.name ), \'Calhfa_Tic\', \'CalHFA TIC\' ), \'_\', \' \')
							ELSE REPLACE ( REPLACE ( INITCAP ( ft.name ), \'Tic\', \'TIC\' ), \'_\', \' \')
						END AS file_type_name
					FROM
						file_types ft
					WHERE
						CID = ' . ( int ) $intCid . '
						AND system_code LIKE \'%' . addslashes( $strSystemCode ) . '%\'
						AND ft.system_code NOT IN ( ' . sqlStrImplode( $arrstrExcludedFileTypeSystemCodes ) . ' )
						AND ft.is_published = 1
						AND ft.deleted_on IS NULL
					ORDER BY
						' . $objDatabase->getCollateSort( 'ft.name', true );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileTypesBySystemCode( $strSystemCode, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						file_types
					WHERE
						system_code = \'' . $strSystemCode . '\'
					ORDER BY
						id';

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypeBySystemCodesBySubsidyCertificationIdByCid( $arrstrSystemCodes, $strEffectiveThroughDate, $intCid, $objDatabase, $boolIsHudLease = true ) {

		$strRemainingDaysCondition = 'DATE_PART( \'day\', ( \'' . $strEffectiveThroughDate . '\'::timestamp + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )';

		$strSql = 'SELECT
						ft.*
					FROM
					    file_types ft
					WHERE
						ft.system_code IN ( ' . sqlStrImplode( $arrstrSystemCodes ) . ' )
						AND CASE
							WHEN ( true = ' . $boolIsHudLease . ' ) THEN
								CASE
									WHEN ( 100 < ' . $strRemainingDaysCondition . ' )
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE ] ) . '
									WHEN ( 70 < ' . $strRemainingDaysCondition . ' )
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' 
									ELSE ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE ] ) . '
								END
							ELSE
								CASE
									WHEN ( 100 < ' . $strRemainingDaysCondition . ' )
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE ] ) . '
									WHEN ( 70 < ' . $strRemainingDaysCondition . ' )
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE ] ) . '
									WHEN ( 50 < ' . $strRemainingDaysCondition . ' ) 
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE ] ) . '
									WHEN ( 50 > ' . $strRemainingDaysCondition . ' ) 
									THEN ft.system_code = ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ] ) . '
								END
							END
						AND ft.deleted_on IS NULL
						AND ft.cid = ' . ( int ) $intCid;

		return self::fetchFileType( $strSql, $objDatabase );
	}

	public static function fetchFileTypesByEntityTypeIdByCid( $intEntityTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intEntityTypeId ) ) return NULL;
		$strSql = 'SELECT 
						* 
					FROM 
						file_types 
					WHERE 
						entity_type_ids     = \'{ ' . $intEntityTypeId . ' }\' 
						AND is_published    = 1
						AND is_hidden       = 0
						AND cid             = ' . ( int ) $intCid . '
					ORDER BY ' . $objDatabase->getCollateSort( 'name', true );

		return self::fetchFileTypes( $strSql, $objDatabase );
	}

	public static function fetchFileTypeIdsBySystemCodesByCid( $arrstrSystemCodes, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrSystemCodes ) ) return [];

		$strSql = 'SELECT
						system_code,
						id
					FROM
						file_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		$arrstrFileTypeIds = [];

		if( true == valArr( $arrmixResult ) ) {
			foreach( $arrmixResult as $arrmixResultRow ) {
				$arrstrFileTypeIds[$arrmixResultRow['system_code']] = $arrmixResultRow['id'];
			}
		}

		return $arrstrFileTypeIds;

	}

	public static function fetchOnlyResidentPortalUploadFileTypesByCid( $intCid, $objDatabase, $boolIsTenant = false, $boolIsReservationHub = false ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSqlWhere = '';
		if( true == $boolIsTenant ) {
			$strSqlWhere = ' OR ft.system_code = \'' . CFileType::SYSTEM_CODE_PERCENT_RENT_SALE . '\'';
		}

		if( true == $boolIsReservationHub ) {
			$strSqlWhere = ' OR ft.system_code = \'' . CFileType::SYSTEM_CODE_CUSTOMER_INVOICE . '\'';
		}

		$strSql = 'SELECT
					id, name AS title
				FROM
					file_types ft
				WHERE
					cid = ' . ( int ) $intCid . '
					AND deleted_by IS NULL
					AND is_published = 1
					AND is_hidden = 0
					AND ( ft.details -> \'can_upload_resident_portal\' = \'true\' ' . $strSqlWhere . ' )
				ORDER BY
					is_system, ' . $objDatabase->getCollateSort( 'name', true );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileTypesByCanUploadResidentPortalByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id, 
						name
					FROM
						file_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_system <> 1
						AND is_published = 1
						AND deleted_on IS NULL
						AND details->>\'can_upload_resident_portal\' = \'true\'
					ORDER BY ' . $objDatabase->getCollateSort( 'name', true );

		return fetchData( $strSql, $objDatabase );
	}

}
?>