<?php
class CCustomerExpense extends CBaseCustomerExpense {

	const HOURLY								= 15;
	const WEEKLY								= 3;
	const MONTHLY								= 4;
	const ANNUALY								= 6;
	const AGE_LIMIT_FOR_MEDICAL					= 62;
	const AGE_LIMIT_FOR_CHILD_CARE				= 13;
	const AGE_LIMIT_FOR_DISABILITY				= 18;
	const AMOUNT_LIMIT_FOR_FULL_TIME_STUDENT	= 480;

	const FUTURE			= 1;
	const CURRENT			= 2;
	const PAST				= 3;

	public static $c_arrstrAllExpenseStatusTypes = [
		self::FUTURE	=> 'Future',
		self::CURRENT	=> 'Current',
		self::PAST		=> 'Past'
	];

	protected $m_strAddress;
	protected $m_strExpenseTypeName;
	protected $m_strHouseholdMemberName;
	protected $m_strExpenseBeneficiaries;
	protected $m_strStatus;
	protected $m_strAction;

	protected $m_intFrequencyDetailId;
	protected $m_intHourPerWeek;
	protected $m_intWeekPerYear;

	protected $m_fltHourlyExpense;
	protected $m_fltExpenseAmount;
	protected $m_intApplicationId;

	protected $m_boolIsPureAffordableLease;

	/**
	 * Set Functions
	 */

	public function setExpenseTypeName( $strExpenseName ) {
		$this->m_strExpenseTypeName = $strExpenseName;
	}

	public function setHouseholdMemberName( $strHouseholdMemberName ) {
		$this->m_strHouseholdMemberName = $strHouseholdMemberName;
	}

	public function setAddress( $strAddress ) {
		$this->m_strAddress = $strAddress;
	}

	public function setFrequencyDetailId( $intFrequencyDetailId ) {
		$this->m_intFrequencyDetailId = $intFrequencyDetailId;
	}

	public function setHourlyExpense( $fltHourlyExpense ) {
		$this->m_fltHourlyExpense = $fltHourlyExpense;
	}

	public function setHourPerWeek( $intHourPerWeek ) {
		$this->m_intHourPerWeek = $intHourPerWeek;
	}

	public function setWeekPerYear( $intWeekPerYear ) {
		$this->m_intWeekPerYear = $intWeekPerYear;
	}

	public function setExpenseAmount( $fltExpenseAmount ) {
		$this->m_fltExpenseAmount = $fltExpenseAmount;
	}

	public function setExpenseBeneficiaries( $strExpenseBeneficiaries ) {
		$this->m_strExpenseBeneficiaries = $strExpenseBeneficiaries;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		$arrmixDetailsKeys = [ 'frequency_detail_id', 'hourly_expense', 'hour_per_week', 'week_per_year', 'expense_amount', 'expense_beneficiaries' ];

		if( true == isset( $arrmixValues['expense_name'] ) )			$this->setExpenseTypeName( $arrmixValues['expense_name'] );
		if( true == isset( $arrmixValues['household_member_name'] ) )	$this->setHouseholdMemberName( $arrmixValues['household_member_name'] );
		if( true == isset( $arrmixValues['address'] ) )					$this->setAddress( $arrmixValues['address'] );
		if( true == isset( $arrmixValues['status'] ) )                  $this->setStatus( $arrmixValues['status'] );

		$arrmixTempDetail = [];
		foreach( $arrmixDetailsKeys as $intKeyDetail ) {
			if( true == isset( $arrmixValues[$intKeyDetail] ) ) {
				$arrmixTempDetail[$intKeyDetail] = $arrmixValues[$intKeyDetail];
			}
		}

		if( true == valId( $this->getId() ) && true == valStr( json_encode( $this->getDetailsField( [] ) ) ) && true == valStr( json_encode( $arrmixValues['details'] ) ) ) {
			$arrmixValues = ( array ) ( $this->getDetailsField( [] ) );
		} else if( true == valStr( json_encode( $this->getDetailsField( [] ) ) ) && true == valStr( $arrmixValues['details'] ) ) {
			$arrmixValues = ( array ) ( json_decode( $this->getDetailsField( [] ) ) );
		}

		$this->setJsonData( $arrmixValues );
	}

	public function setJsonData( $arrmixValues ) {

		if( true == isset( $arrmixValues['frequency_detail_id'] ) )		$this->setFrequencyDetailId( $arrmixValues['frequency_detail_id'] );
		if( true == isset( $arrmixValues['hourly_expense'] ) )			$this->setHourlyExpense( $arrmixValues['hourly_expense'] );
		if( true == isset( $arrmixValues['hour_per_week'] ) )			$this->setHourPerWeek( $arrmixValues['hour_per_week'] );
		if( true == isset( $arrmixValues['week_per_year'] ) )			$this->setWeekPerYear( $arrmixValues['week_per_year'] );
		if( true == isset( $arrmixValues['expense_amount'] ) )			$this->setExpenseAmount( $arrmixValues['expense_amount'] );
		if( true == isset( $arrmixValues['expense_beneficiaries'] ) )	$this->setExpenseBeneficiaries( $arrmixValues['expense_beneficiaries'] );
	}

	public function setIsPureAffordableLease( $boolIsPureAffordableLease ) {
		$this->m_boolIsPureAffordableLease = $boolIsPureAffordableLease;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	/**
	 * Get Functions
	 */

	public function getExpenseTypeName() {
		return $this->m_strExpenseTypeName;
	}

	public function getHouseholdMemberName() {
		return $this->m_strHouseholdMemberName;
	}

	public function getAddress() {
		return $this->m_strAddress;
	}

	public function getFrequencyDetailId() {
		return $this->m_intFrequencyDetailId;
	}

	public function getHourlyExpense() {
		return $this->m_fltHourlyExpense;
	}

	public function getHourPerWeek() {
		return $this->m_intHourPerWeek;
	}

	public function getWeekPerYear() {
		return $this->m_intWeekPerYear;
	}

	public function getExpenseAmount() {
		return $this->m_fltExpenseAmount;
	}

	public function getExpenseBeneficiaries() {
		return $this->m_strExpenseBeneficiaries;
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerExpenseLog = CCustomerExpenseLogs::fetchRecentCustomerExpenseLogByCustomerIdByIdByCid( $this->getCustomerId(), $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerExpenseLog, 'CCustomerIncomeLog' ) ) {
			return $objCustomerExpenseLog;
		}

		return NULL;
	}

	public function getIsPureAffordableLease() {
		return $this->m_boolIsPureAffordableLease;
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Household member is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExpenseTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getExpenseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_type_id', 'Expense type is required.' ) );
		} elseif( CExpenseType::CHILD_CARE_WORK == $this->getExpenseTypeId() ) {
			$boolIsValid = $this->valEnabledCustomerIncomeIds();
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( false == valStr( $this->getFrequencyDetailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'Frequency of expense is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEnabledCustomerIncomeIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getEnabledCustomerIncomeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'enabled_customer_income_ids', 'Child care allows member to work at is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExpenseAmount() {
		$boolIsValid = true;

		$intFrequency = intval( $this->getFrequencyDetailId() );

		if( 0 == $intFrequency && false == valStr( $this->getExpenseAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_amount', 'Annual expense amount is required.' ) );
		}

		switch( $intFrequency ) {

			case self::HOURLY:
				$boolIsValid &= $this->valHourlyExpense();
				break;

			case self::WEEKLY:
				$boolIsValid &= $this->valExpenseByFrequency( $strMsg = 'Weekly' );
				break;

			case self::MONTHLY:
				$boolIsValid &= $this->valExpenseByFrequency( $strMsg = 'Monthly' );
				break;

			case self::ANNUALY:
				$boolIsValid &= $this->valExpenseByFrequency( $strMsg = 'Annual' );
				break;

			default:
				return $boolIsValid;
				break;
		}
		return $boolIsValid;
	}

	public function valHourlyExpense() {
		$boolIsValid = true;

		if( true == is_null( $this->getHourlyExpense() ) || false == valStr( $this->getHourlyExpense() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_expense', 'Hourly expense is required.' ) );

		} elseif( ( false === is_numeric( $this->getHourlyExpense() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_expense', 'Hourly expense is not valid.' ) );
		}

		if( true == is_null( $this->getHourPerWeek() ) || false == valStr( $this->getHourPerWeek() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hour_per_week', 'Hours per week is required.' ) );

		} elseif( ( false === is_numeric( $this->getHourPerWeek() ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hour_per_week', 'Hours per week is not valid.' ) );
		}

		if( true == is_null( $this->getWeekPerYear() ) || false == valStr( $this->getWeekPerYear() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_per_year', 'Weeks per year is required.' ) );

		} elseif( ( false === is_numeric( $this->getWeekPerYear() ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_per_year', 'Weeks per year is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valExpenseByFrequency( $strMsg ) {

		$boolIsValid = true;

		if( true === is_null( $this->getExpenseAmount() ) || false == valStr( $this->getExpenseAmount() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_amount', $strMsg . ' expense amount is required.' ) );

		} elseif( ( false == is_numeric( $this->getExpenseAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_amount', $strMsg . ' expense amount is not valid.' ) );

		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactEmail() {
		$boolIsValid = true;

		if( true == valStr( $this->getContactEmail() ) && false == filter_var( $this->getContactEmail(), FILTER_VALIDATE_EMAIL ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email', 'Email address does not appear to be valid.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAmount( $objDatabase ) {

		$boolIsValid = true;

		if( $this->getExpenseTypeId() == CExpenseType::CHILD_CARE_WORK && true == valArr( $this->getEnabledCustomerIncomeIds() ) ) {
			$intAnnualIncome		= ( int ) \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchAnnualIncomeByIdsByCustomerIdByCid( $this->getEnabledCustomerIncomeIds(), $this->getCid(), $objDatabase );

			$boolIsFullTimeStudent	= ( true == in_array( \CSubsidySpecialStatusType::FULL_TIME_STUDENT, array_column( \Psi\Eos\Entrata\CCustomerSubsidySpecialStatusTypes::createService()->fetchCustomerSubsidySpecialStatusTypesByCustomerIdByCId( $this->getExpenseBeneficiaries(), $this->getCid(), $objDatabase ), 'subsidy_special_status_type_id' ) ) ) ? true : false;

			if( $intAnnualIncome < $this->getAmount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_amount', 'Total annual expense amount should be less than total amount of employment income ($' . $intAnnualIncome . ').' ) );
				$boolIsValid &= false;
			} elseif( true == $boolIsFullTimeStudent && $this::AMOUNT_LIMIT_FOR_FULL_TIME_STUDENT < $this->getAmount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expense_amount', 'Total annual expense amount should be less than $' . $this::AMOUNT_LIMIT_FOR_FULL_TIME_STUDENT . ', as member to work is a full time student .' ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valExpenseEffectiveDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getExpenseEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_effective', 'Effective date is required.' ) );
		} elseif( false === CValidation::checkISODateFormat( $this->getExpenseEffectiveDate(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_effective', 'Effective date must be in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valExpenseBeneficiaries() {

		$boolIsValid = true;

		if( CExpenseType::DISABILITY == $this->getExpenseTypeId() && false == valArr( $this->getExpenseBeneficiaries() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'enabled_customer_income_ids', 'Disability expense allows member to work is required.' ) );
		} elseif( ( CExpenseType::CHILD_CARE_SCHOOL == $this->getExpenseTypeId() || CExpenseType::CHILD_CARE_WORK == $this->getExpenseTypeId() ) && false == valStr( $this->getExpenseBeneficiaries() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'enabled_customer_income_ids', 'Child care allows which member to work is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDateEnded( $strEditContext, $strExpenseStatus ) {
		$boolIsValid = true;

			if( true == is_null( $this->getDateEnded() ) && 'past' == $strExpenseStatus ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', 'End date is required.' ) );
			} elseif( false == valStr( $this->getDateEnded() ) && 'end_expense' == $strEditContext ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', 'End date is required.' ) );
			} elseif( true == valStr( $this->getDateEnded() ) ) {
				if( false === CValidation::checkISODateFormat( $this->getDateEnded(), $boolFormat = true ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', 'End date must be in mm/dd/yyyy format.' ) );
				} elseif( true == valStr( $this->getExpenseEffectiveDate() ) && strtotime( $this->getDateEnded() ) < strtotime( $this->getExpenseEffectiveDate() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', 'End date must be greater than or equal to effective date.' ) );
				}
			}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $strExpenseStatus,  $objDatabase = NULL ) {
		$boolIsValid = true;
		switch( $strAction ) {

			case 'add_expense':
				$boolIsValid &= $this->valExpenseEffectiveDate();
				$boolIsValid &= $this->valDateEnded( $strAction, $strExpenseStatus );
				$boolIsValid &= $this->valExpenseBeneficiaries();
				$boolIsValid &= $this->valExpenseTypeId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valExpenseAmount();
				$boolIsValid &= $this->valAmount( $objDatabase );
				$boolIsValid &= $this->valContactEmail();
				break;

			case 'incr_decr_expense':
				$boolIsValid &= $this->valExpenseEffectiveDate();
				$boolIsValid &= $this->valDateEnded( $strAction, $strExpenseStatus );
				$boolIsValid &= $this->valExpenseTypeId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valExpenseAmount();
				$boolIsValid &= $this->valAmount( $objDatabase );
				break;

			case 'edit_expense':
				$boolIsValid &= $this->valContactEmail();
				break;

			case 'end_expense':
				$boolIsValid &= $this->valDateEnded( $strAction, $strExpenseStatus );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objPreviousCustomerExpense = NULL ) {

		$boolIsValid = true;
		if( true == $this->getIsPureAffordableLease() && true == valStr( $this->getAction() ) ) {
			$boolIsValid = $this->insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerExpense );
		}

		if( true == $boolIsValid ) {
			if( true == is_null( $this->getId() ) ) {
				return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			} else {
				return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		return $boolIsValid;

	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;
		if( true == $this->getIsPureAffordableLease() && true == valStr( $this->getAction() ) ) {
			$boolIsValid = $this->insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase );
		}

		if( true == $boolIsValid ) {
			if( true == $boolIsHardDelete ) {
				return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			} else {
				$this->setDeletedBy( $intCurrentUserId );
				$this->setDeletedOn( 'NOW()' );

				return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		return $boolIsValid;

	}

	public function insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerExpense = NULL ) {

		$arrobjEvents                    = [];
		$arrstrActions = $this->checkExpenseActionType( $objPreviousCustomerExpense );
		if( false == valArr( $arrstrActions ) ) {
			return true;
		}
		$objAffordableLibrary = $this->prepareAffordableLibrary( $intCurrentUserId, $objDatabase );
		foreach( $arrstrActions as $strAction ) {
			$arrobjEvents[] = $objAffordableLibrary->createEventAndApplicantApplicationChanges( CEventType::EXPENSE_CHANGE, $objPreviousCustomerExpense, $strAction );
		}
		return $objAffordableLibrary->insertEventsAndApplicantApplicationChanges( $arrobjEvents );

	}

	public function checkExpenseActionType( $objPreviousCustomerExpense = NULL ) {

		if( 'add_expense' == $this->m_strAction ) {
			$this->m_strAction = ( false == valId( $this->getId() ) ) ? 'add_expense' : 'edit_expense';
		}

		$arrstrActions = [];
		if( true == in_array( $this->m_strAction, [ 'delete_expense', 'end_expense', 'add_expense' ] ) ) {
			$arrstrActions[]        = $this->m_strAction;
		} else {
			if( $objPreviousCustomerExpense->getAmount() != $this->getAmount() ) {
				$arrstrActions[]    = 'edit_expense_amount';
			}
			if( $objPreviousCustomerExpense->getExpenseTypeId() != $this->getExpenseTypeId() ) {
				$arrstrActions[]    = 'edit_expense_type';
			}
			if( $objPreviousCustomerExpense->getFrequencyId() != $this->getFrequencyDetailId() ) {
				$arrstrActions[]    = 'edit_expense_frequency';
			}
			if( date( 'm/d/y', strtotime( $objPreviousCustomerExpense->getExpenseEffectiveDate() ) ) != date( 'm/d/y', strtotime( $this->getExpenseEffectiveDate() ) ) ) {
				$arrstrActions[] = 'edit_expense_effective_date';
			}
			if( 1 > \Psi\Libraries\UtilFunctions\count( $arrstrActions ) && false == in_array( $arrstrActions, [ 'edit_expense_amount', 'edit_expense_type', 'edit_expense_frequency', 'edit_expense_effective_date' ] ) ) {
				return false;
			}
		}

		return $arrstrActions;
	}

	public function prepareAffordableLibrary( $intCurrentUserId, $objDatabase ) {

		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objDatabase );
		$objApplicant   = CApplicants::fetchApplicantByLeaseIdByCustomerIdByCid( $objApplication->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objDatabase );
		$objLease       = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objApplication->getLeaseId(), $this->getCid(), $objDatabase );

		require_once( PATH_LIBRARY_AFFORDABLE . 'CAffordableLibrary.class.php' );
		$objAffordableLibrary = new CAffordableLibrary();
		$objAffordableLibrary->setDatabase( $objDatabase );
		$objAffordableLibrary->setApplicantId( $objApplicant->getId() );
		$objAffordableLibrary->setApplication( $objApplication );
		$objAffordableLibrary->setCompanyUser( $objCompanyUser );
		$objAffordableLibrary->setCustomerExpense( $this );
		$objAffordableLibrary->setLease( $objLease );

		return $objAffordableLibrary;
	}

}
?>
