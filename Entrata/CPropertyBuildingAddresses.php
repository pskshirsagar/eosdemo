<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBuildingAddresses
 * Do not add any new functions to this class.
 */

class CPropertyBuildingAddresses extends CBasePropertyBuildingAddresses {

	public static function fetchPropertyBuildingAddressByAddressTypeIdByPropertyBuildingIdByCid( $intAddressTypeId, $intPropertyBuildingId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_building_addresses
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_building_id = ' . ( int ) $intPropertyBuildingId . '
						AND address_type_id = ' . ( int ) $intAddressTypeId . '
					LIMIT 1';

		return parent::fetchPropertyBuildingAddress( $strSql, $objDatabase );
	}

	public static function fetchPropertyBuildingAddressByIdByPropertyBuildingIdByCid( $intId, $intPropertyBuildingId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_building_addresses
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_building_id = ' . ( int ) $intPropertyBuildingId . '
						AND id = ' . ( int ) $intId;

		return parent::fetchPropertyBuildingAddress( $strSql, $objDatabase );
	}
}
?>