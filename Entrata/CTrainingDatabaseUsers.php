<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTrainingDatabaseUsers
 * Do not add any new functions to this class.
 */

class CTrainingDatabaseUsers extends CBaseTrainingDatabaseUsers {

	public static function fetchTrainingDatabaseUsersByTrainingDatabaseIdsByCid( $arrintTrainingDatabaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintTrainingDatabaseIds ) ) return NULL;

		$strSql = 'SELECT 
						tdu.*,
						cu.id,
						ce.name_first,
						ce.name_last
					FROM 
						training_database_users tdu
						JOIN company_users cu ON ( tdu.cid = cu.cid AND tdu.company_user_id = cu.id )
						JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE 
						tdu.training_database_id IN ( ' . implode( ',', $arrintTrainingDatabaseIds ) . ' )
						AND tdu.cid = ' . ( int ) $intCid . '
						AND tdu.deleted_by IS NULL
						AND tdu.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveTrainingDatabaseUsersByTrainingDatabaseIdByCid( $intTrainingDatabaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM 
						training_database_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND training_database_id = ' . ( int ) $intTrainingDatabaseId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchTrainingDatabaseUsers( $strSql, $objDatabase );
	}

}
?>