<?php

class CRateAdjustmentUnit extends CBaseRateAdjustmentUnit {

	const PERCENTAGE 	= 1;
	const DOLLARS 		= 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>