<?php

class CRuleConditionType extends CBaseRuleConditionType {

	const OVER_BUDGET											= 1;
	const UNDER_BUDGET											= 2;
	const TOTAL_GREATER_THAN									= 3;
	const TOTAL_LESS_THAN										= 4;
	const TOTAL_BETWEEN											= 5;
	const SPECIFIC_GL_ACCOUNTS									= 6;
	const SPECIFIC_COMPANY_USERS								= 7;
	const INVOICE_LINKED_TO_APPROVED_PO							= 8;
	const INVOICES_TOTAL_VARIES_FROM_PO_TOTAL					= 9;
	const OVER_BUDGET_BY_PERCENT								= 10;
	const UNDER_BUDGET_BY_PERCENT								= 11;
	const INVOICE_TOTAL_VARIES_FROM_PO_TOTAL_BY_PERCENT			= 12;
	const SPECIFIC_VENDORS										= 13;
	const SPECIFIC_BANK_ACCOUNTS								= 14;
	const VENDOR_ACCESS_STATUS									= 15;
	const JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH	= 18;
	const FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN				= 19;
	const FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN					= 20;
	const FINANCIAL_MOVE_OUT_BALANCE_BETWEEN					= 21;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN		= 22;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN			= 23;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN			= 24;
	const SPECIFIC_ROUTING_TAGS									= 25;

	public static $c_arrintRuleConditionTypeIdsWithoutDetails = [
		CRuleConditionType::OVER_BUDGET,
		CRuleConditionType::UNDER_BUDGET,
		CRuleConditionType::OVER_BUDGET_BY_PERCENT,
		CRuleConditionType::UNDER_BUDGET_BY_PERCENT,
		CRuleConditionType::TOTAL_GREATER_THAN,
		CRuleConditionType::TOTAL_LESS_THAN,
		CRuleConditionType::INVOICES_TOTAL_VARIES_FROM_PO_TOTAL,
		CRuleConditionType::INVOICE_TOTAL_VARIES_FROM_PO_TOTAL_BY_PERCENT,
		CRuleConditionType::INVOICE_LINKED_TO_APPROVED_PO,
		CRuleConditionType::TOTAL_BETWEEN,
		CRuleConditionType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH,
		CRuleConditionType::FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN,
		CRuleConditionType::FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN,
		CRuleConditionType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN,
		CRuleConditionType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN,
		CRuleConditionType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN,
		CRuleConditionType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN
	];

	public static $c_arrintRuleConditionTypeIdsWithDetails = [
		CRuleConditionType::SPECIFIC_GL_ACCOUNTS,
		CRuleConditionType::SPECIFIC_COMPANY_USERS,
		CRuleConditionType::SPECIFIC_VENDORS,
		CRuleConditionType::SPECIFIC_BANK_ACCOUNTS,
		CRuleConditionType::VENDOR_ACCESS_STATUS,
		CRuleConditionType::SPECIFIC_ROUTING_TAGS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				// Default case
				break;
		}

		return $boolIsValid;
	}

}
?>