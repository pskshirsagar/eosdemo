<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMilitaryDetails
 * Do not add any new functions to this class.
 */

class CCustomerMilitaryDetails extends CBaseCustomerMilitaryDetails {

	public static function fetchCustomerMilitaryDetailByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cmd.*,
						CASE WHEN cmd.military_status_id = ' . CMilitaryStatus::ACTIVE_DUTY_MILITARY . ' AND cmd.military_component_id <> ' . CMilitaryComponent::FOREIGNMILITARY . ' THEN mr.name ELSE NULL END as military_rank_name,
    					CASE WHEN cmd.military_status_id = ' . CMilitaryStatus::ACTIVE_DUTY_MILITARY . ' AND cmd.military_component_id <> ' . CMilitaryComponent::FOREIGNMILITARY . 'THEN mpgr.insignia_uri ELSE NULL END as insignia_uri
					FROM
						customer_military_details cmd
						LEFT JOIN military_ranks mr ON ( cmd.military_rank_id = mr.id )
						LEFT JOIN military_pay_grade_ranks mpgr ON ( mpgr.military_rank_id = cmd.military_rank_id AND mpgr.military_pay_grade_id = cmd.military_pay_grade_id )
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	customer_id = ' . ( int ) $intCustomerId;

		return parent::fetchCustomerMilitaryDetail( $strSql, $objDatabase );
	}

	public static function fetchExistingCustomerMilitaryDetailByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                       cmd.customer_id
				   FROM
				       customer_military_details cmd
				   WHERE
					   cmd.cid = ' . ( int ) $intCid . '
					   AND cmd.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		if( false == valId( $intApplicationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cmd.*,aa.customer_type_id
					FROM
						applicants app
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND app.id = aa.applicant_id )
						JOIN customer_military_details cmd ON ( app.cid = cmd.cid AND app.customer_id = cmd.customer_id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
					ORDER BY
						aa.customer_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cmd.id,
						cmd.customer_id,
						ms.name AS military_status_name,
						cmd.duty_phone_number,
						cmd.duty_address_street1,
						cmd.duty_address_street2,
						cmd.duty_address_city,
						s.name AS state_name,
						cmd.duty_address_state_code,
						cmd.duty_address_postal_code,
						cmd.duty_email_address,
						cmd.duty_phone_number,
						cmd.change_in_duty_date,
						cmd.report_date,
						cmd.arrival_date,
						cmd.enlistment_date,
						cmd.rank_effective_date,
						cmd.unit_id_code,
						cmd.is_warrant_candidate,
						cmd.commanding_officer,
						cmd.is_promotable,
						cmd.military_dependent_status_type_id,
						cmd.military_dependent_relationship_id,
						cmd.military_component_id,
						cmd.military_assistance_type_id,
						cmd.military_rank_id,
						cmd.military_pay_grade_id,
						cmd.last_assignment,
						cmd.end_of_service_date,
						cmd.is_deployed,
						cmd.deployed_location,
						cmd.deployment_start_date,
						cmd.deployment_end_date,
						cmd.eligibility_date,
						mr.name AS military_rank_name,
						mpg.name AS military_pay_grade,
						cmd.is_active_reservist,
						mnmt.name AS military_non_military,
						mc.name AS military_components,
						aa.customer_type_id
					FROM
						customer_military_details cmd LEFT JOIN military_statuses ms
						ON cmd.military_status_id = ms.id LEFT JOIN states s
						ON cmd.duty_address_state_code = s.code LEFT JOIN military_ranks mr
						ON cmd.military_rank_id = mr.id LEFT JOIN military_pay_grades mpg
						ON cmd.military_pay_grade_id = mpg.id LEFT JOIN military_non_military_types mnmt
						ON cmd.military_non_military_type_id = mnmt.id LEFT JOIN military_components mc
						ON cmd.military_component_id = mc.id LEFT JOIN applicants a
						ON cmd.customer_id = a.customer_id LEFT JOIN applicant_applications aa
						ON aa.applicant_id = a.id
					WHERE
						cmd.cid = a.cid AND aa.cid = cmd.cid AND cmd.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND cmd.cid = ' . ( int ) $intCid;

		return parent::fetchCustomerMilitaryDetails( $strSql, $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByIdsByCid( $arrintCustomerMilitaryDetailIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerMilitaryDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                       cmd.*
				   FROM
				       customer_military_details cmd
				   WHERE
					   cmd.cid = ' . ( int ) $intCid . '
					   AND cmd.id IN ( ' . implode( ',', $arrintCustomerMilitaryDetailIds ) . ' ) ';

		return parent::fetchCustomerMilitaryDetails( $strSql, $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase, $arrintMilitaryStatusIds = [], $arrintCustomerTypeIds = [] ) {

		// @FIXME Do we need to exclude is_ungrouped.

		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cmd.*,
						c.name_first,
						c.name_last,
						lc.lease_id
					FROM
						customer_military_details cmd
						JOIN lease_customers lc ON ( cmd.cid = lc.cid AND cmd.customer_id = lc.customer_id )
						JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
					WHERE
						cmd.cid = ' . ( int ) $intCid . '
						AND lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		if( true == valArr( $arrintMilitaryStatusIds ) ) {
			$strSql .= 'AND cmd.military_status_id IN ( ' . implode( ',', $arrintMilitaryStatusIds ) . ' )';
		}

		if( true == valArr( $arrintCustomerTypeIds ) ) {
			$strSql .= 'AND lc.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )';
		}

		return parent::fetchCustomerMilitaryDetails( $strSql, $objDatabase );
	}

	public static function fetchCustomerMilitaryAssistanceTypeByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						mat.name AS military_assistance_type
					FROM
						customer_military_details cmd
						JOIN military_assistance_types mat ON ( mat.id = cmd.military_assistance_type_id )
					WHERE
						cmd.customer_id = ' . ( int ) $intCustomerId . '
						AND cmd.cid = ' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, 'military_assistance_type', $objDatabase );
	}

	public static function fetchFutureMilitaryInstallationByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COALESCE( mi.name, cmd.future_installation_other ) AS future_military_installation
					FROM
						customer_military_details cmd
						LEFT JOIN military_installations mi ON ( mi.id = cmd.future_military_installation_id )
					WHERE
						cmd.customer_id = ' . ( int ) $intCustomerId . '
						AND ( cmd.future_military_installation_id IS NOT NULL OR cmd.future_installation_other IS NOT NULL )
						AND cmd.future_report_date IS NOT NULL
						AND cmd.cid = ' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, 'future_military_installation', $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
				   FROM 
						customer_military_details cmd
				   WHERE
						cmd.cid = ' . ( int ) $intCid . ' 
						AND cmd.customer_id in ( ' . implode( ',', $arrintCustomerIds ) . ' ) ';

		return parent::fetchCustomerMilitaryDetails( $strSql, $objDatabase );
	}

}
?>
