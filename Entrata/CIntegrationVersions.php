<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationVersions
 * Do not add any new functions to this class.
 */

class CIntegrationVersions extends CBaseIntegrationVersions {

	public static function fetchAllIntegrationVersions( $objDatabase ) {
		$strSql = 'SELECT * FROM integration_versions';
		return self::fetchIntegrationVersions( $strSql, $objDatabase );
	}

	public static function fetchIntegrationVersionsByNameByIntegrationClientTypeId( $strIntegrationVersion, $intIntegrationClientTypeId, $objDatabase ) {
		$strSql = 'SELECT *
						FROM integration_versions
						WHERE integration_client_type_id = ' . ( int ) $intIntegrationClientTypeId . '
						AND name = \'' . ( string ) $strIntegrationVersion . '\'';

		return parent::fetchIntegrationVersion( $strSql, $objDatabase );
	}

}
?>