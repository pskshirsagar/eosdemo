<?php

class CScreeningDocumentExportRequestStatusType extends CBaseScreeningDocumentExportRequestStatusType {

	const SCREENING_DOCUMENT_EXPORT_REQUEST_STATUS_TYPE_IN_PROGRESS	= 1;
	const SCREENING_DOCUMENT_EXPORT_REQUEST_STATUS_TYPE_COMPLETED	= 2;
	const SCREENING_DOCUMENT_EXPORT_REQUEST_STATUS_TYPE_CANCELLED	= 3;
	const SCREENING_DOCUMENT_EXPORT_REQUEST_STATUS_TYPE_ERROR		= 4;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>