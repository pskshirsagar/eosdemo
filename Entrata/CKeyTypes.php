<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CKeyTypes
 * Do not add any new functions to this class.
 */

class CKeyTypes extends CBaseKeyTypes {

	public static function fetchKeyTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CKeyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchKeyType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CKeyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>