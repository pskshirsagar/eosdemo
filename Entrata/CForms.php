<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForms
 * Do not add any new functions to this class.
 */

class CForms extends CBaseForms {

	public static function fetchPaginatedFormsByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $objFormsFilter, $objDatabase ) {

		$arrstrWhere = [];

		$intOffset 	= 0;
		$intLimit	= NULL;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = $intPageSize;
 		}

		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		if( true == isset( $objFormsFilter ) ) {
			if( false == is_null( $objFormsFilter->getFromDate() ) && 0 < strlen( $objFormsFilter->getFromDate() ) )    	$arrstrWhere[] = 'f.created_on >= \'' . date( 'Y-m-d', strtotime( $objFormsFilter->getFromDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objFormsFilter->getToDate() ) && 0 < strlen( $objFormsFilter->getToDate() ) )      		$arrstrWhere[] = 'f.created_on <= \'' . date( 'Y-m-d', strtotime( $objFormsFilter->getToDate() ) ) . ' 23:59:59\'';

			if( false == is_null( $objFormsFilter->getNameFirst() ) && 0 < strlen( $objFormsFilter->getNameFirst() ) ) {
				$arrstrWhere[] = ' ( cc.name_first ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameFirst() ) . '%\' OR ce.name_first ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameFirst() ) . '%\' ) ';
			}

			if( false == is_null( $objFormsFilter->getNameMiddle() ) && 0 < strlen( $objFormsFilter->getNameMiddle() ) ) {
				$arrstrWhere[] = ' ( cc.name_middle ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameMiddle() ) . '%\' OR ce.name_middle ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameMiddle() ) . '%\' ) ';
			}

			if( false == is_null( $objFormsFilter->getNameLast() ) && 0 < strlen( $objFormsFilter->getNameLast() ) ) {
				$arrstrWhere[] = ' ( cc.name_last ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameLast() ) . '%\' OR ce.name_last ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameLast() ) . '%\' ) ';
			}
		}

		$strSql = 'SELECT
				 		DISTINCT ON ( created_on, f.id ) f.*
				  	FROM
				 		forms f';

				if( true == isset( $objFormsFilter ) && ( 0 < strlen( $objFormsFilter->getNameFirst() ) || 0 < strlen( $objFormsFilter->getNameMiddle() ) || 0 < strlen( $objFormsFilter->getNameLast() ) ) ) {
					$strSql .= ' LEFT OUTER JOIN customers cc ON f.customer_id = cc.id AND f.cid = cc.cid ';
					$strSql .= ' LEFT OUTER JOIN ap_payees ap ON f.ap_payee_id = ap.id AND f.cid = ap.cid';
					$strSql .= ' LEFT OUTER JOIN company_employees ce ON f.company_employee_id = ce.id AND f.cid = ce.cid';
				}

				$strSql .= ' JOIN documents d ON ( d.id = f.document_id AND d.cid = f.cid AND d.cid = ' . ( int ) $intCid . ' )
				  			 JOIN document_types dt ON ( dt.id = d.document_type_id AND dt.id = ' . CDocumentType::FORM . ' )';

				if( true == isset( $objFormsFilter ) && false == is_null( $objFormsFilter->getDocumentSubTypeId() ) && 0 < $objFormsFilter->getDocumentSubTypeId() ) {
					$strSql .= ' JOIN document_sub_types dst ON ( dst.id = d.document_sub_type_id AND dst.id = ' . ( int ) $objFormsFilter->getDocumentSubTypeId() . ' ) ';
				} else {
					$strSql .= ' JOIN document_sub_types dst ON ( dst.id = d.document_sub_type_id AND dst.id IN ( ' . CDocumentSubType::CUSTOM_FORM . ', ' . CDocumentSubType::LEASE_AGREEMENT . ', ' . CDocumentSubType::RESIDENT_PORTAL_SURVEY_FORM . ', ' . CDocumentSubType::RESIDENT_PORTAL_SURVEY_FORM . ', ' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ', ' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ', ' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ' ) ) ';
				}

				// $strSql .= ' JOIN leases cl ON ( cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ) ';

				$strSql .= ' WHERE
								f.cid = ' . ( int ) $intCid;

				if( true == valArr( $arrstrWhere ) ) {
					$strSql .= ' AND ';
					$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
				}

		$strSql .= ' ORDER BY created_on DESC, f.id DESC ';

		if( 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( false == is_null( $intLimit ) ) {
    		$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchForms( $strSql, $objDatabase );
	}

	public static function fetchPaginatedFormsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objFormsFilter, $objDatabase ) {

		$arrstrWhere = [];

		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		if( true == isset( $objFormsFilter ) ) {
			if( false == is_null( $objFormsFilter->getFromDate() ) && 0 < strlen( $objFormsFilter->getFromDate() ) )    	$arrstrWhere[] = 'f.created_on >= \'' . date( 'Y-m-d', strtotime( $objFormsFilter->getFromDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objFormsFilter->getToDate() ) && 0 < strlen( $objFormsFilter->getToDate() ) )      		$arrstrWhere[] = 'f.created_on <= \'' . date( 'Y-m-d', strtotime( $objFormsFilter->getToDate() ) ) . ' 23:59:59\'';

			if( false == is_null( $objFormsFilter->getNameFirst() ) && 0 < strlen( $objFormsFilter->getNameFirst() ) ) {
				$arrstrWhere[] = ' ( cc.name_first ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameFirst() ) . '%\'  OR ce.name_first ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameFirst() ) . '%\' ) ';
			}

			if( false == is_null( $objFormsFilter->getNameMiddle() ) && 0 < strlen( $objFormsFilter->getNameMiddle() ) ) {
				$arrstrWhere[] = ' ( cc.name_middle ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameMiddle() ) . '%\' OR ce.name_middle ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameMiddle() ) . '%\' ) ';
			}

			if( false == is_null( $objFormsFilter->getNameLast() ) && 0 < strlen( $objFormsFilter->getNameLast() ) ) {
				$arrstrWhere[] = ' ( cc.name_last ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameLast() ) . '%\' OR ce.name_last ILIKE E\'%' . addslashes( $objFormsFilter->sqlNameLast() ) . '%\' ) ';
			}
		}

		$strSql = 'SELECT
				 		count( DISTINCT  f.id )
				  	FROM
				 		forms f';

				if( true == isset( $objFormsFilter ) && ( 0 < strlen( $objFormsFilter->getNameFirst() ) || 0 < strlen( $objFormsFilter->getNameMiddle() ) || 0 < strlen( $objFormsFilter->getNameLast() ) ) ) {
					$strSql .= ' LEFT OUTER JOIN customers cc ON f.customer_id = cc.id AND cc.cid = f.cid ';
					$strSql .= ' LEFT OUTER JOIN ap_payees ap ON f.ap_payee_id = ap.id AND f.cid = ap.cid';
					$strSql .= ' LEFT OUTER JOIN company_employees ce ON f.company_employee_id = ce.id AND f.cid = ce.cid';
				}

				$strSql .= ' JOIN documents d ON ( d.id = f.document_id AND d.cid = f.cid AND d.cid = ' . ( int ) $intCid . ' )
				  		     JOIN document_types dt ON ( dt.id = d.document_type_id AND dt.id = ' . CDocumentType::FORM . ' )';

				if( true == isset( $objFormsFilter ) && false == is_null( $objFormsFilter->getDocumentSubTypeId() ) && 0 < $objFormsFilter->getDocumentSubTypeId() ) {
					$strSql .= ' JOIN document_sub_types dst ON ( dst.id = d.document_sub_type_id AND dst.id = ' . ( int ) $objFormsFilter->getDocumentSubTypeId() . ' ) ';
				} else {
					$strSql .= ' JOIN document_sub_types dst ON ( dst.id = d.document_sub_type_id AND dst.id IN ( ' . CDocumentSubType::CUSTOM_FORM . ', ' . CDocumentSubType::LEASE_AGREEMENT . ', ' . CDocumentSubType::RESIDENT_PORTAL_SURVEY_FORM . ', ' . CDocumentSubType::RESIDENT_PORTAL_SURVEY_FORM . ', ' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ', ' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ', ' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ' ) ) ';
				}

				// $strSql .= ' JOIN leases cl ON ( cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ) ';

				$strSql .= ' WHERE
								f.cid = ' . ( int ) $intCid;

				if( true == valArr( $arrstrWhere ) ) {
					$strSql .= ' AND ';
					$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
				}
		$arrintResponse = fetchData( $strSql, $objDatabase );

    	if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

    public static function fetchFormsByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $intCid, $objDatabase ) {
	    if( false == \valArr( array_filter( $arrintMaintenanceRequestIds ) ) || true == is_null( $intCid ) ) return NULL;

    	$strSql = 'SELECT
    					*
    				FROM
    					forms
    				WHERE
    					cid = ' . ( int ) $intCid . '
    				 AND
    					maintenance_request_id IN ( ' . implode( ',',  $arrintMaintenanceRequestIds ) . ' )';
    	return self::fetchForms( $strSql, $objDatabase );
    }

	public static function fetchFormsByDocumentIdsByPropertyIdByCustomerIdByCid( $arrintDocumentIds, $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) && false == valArr( $arrintPropertyIds ) && false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						* 
						FROM 
							forms 
						WHERE 
							document_id IN( ' . implode( ',', $arrintDocumentIds ) . ' ) 
						AND 
							cid = ' . ( int ) $intCid . '
						AND 
							property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND
							customer_id = ' . ( int ) $intCustomerId;
		return self::fetchForms( $strSql, $objDatabase );
	}

    public static function fetchFilteredFormsByDocumentIdByCid( $intDocumentId, $intCid, $objGraphFilter, $objDatabase ) {

		if( true == isset( $objGraphFilter ) && true == is_null( $objGraphFilter->getPropertyIds() ) && 0 == strlen( $objGraphFilter->getPropertyIds() ) ) {
			return NULL;
		}

		$arrstrWhere = [];

		if( true == isset( $objGraphFilter ) ) {
			if( false == is_null( $objGraphFilter->getStartDate() ) && 0 < strlen( $objGraphFilter->getStartDate() ) )
				$arrstrWhere[] = 'to_char( created_on,\'mm/dd/yyyy\') >= \'' . addslashes( $objGraphFilter->getStartDate() ) . '\'';
			if( false == is_null( $objGraphFilter->getEndDate() ) && 0 < strlen( $objGraphFilter->getEndDate() ) )
				$arrstrWhere[] = 'to_char( created_on,\'mm/dd/yyyy\') <= \'' . addslashes( $objGraphFilter->getEndDate() ) . '\'';
			if( false == is_null( $objGraphFilter->getPropertyIds() ) && 0 < strlen( $objGraphFilter->getPropertyIds() ) )
				$arrstrWhere[] = 'property_id IN ( ' . $objGraphFilter->getPropertyIds() . ' )';
		}

		$strSql = 'SELECT
						*
    				FROM
    					forms
    				WHERE
    					document_id = ' . ( int ) $intDocumentId . '
    					AND cid = ' . ( int ) $intCid;

    		if( true == valArr( $arrstrWhere ) ) {
				$strSql .= ' AND ';
				$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
			}

		return self::fetchForms( $strSql, $objDatabase );
	}

	public static function fetchFormsByDocumentIdsByApplicantIdByCid( $arrintDocumentIds, $intApplicantId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) || true == is_null( $intApplicantId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM forms WHERE document_id IN( ' . implode( ',', $arrintDocumentIds ) . ' ) AND applicant_id = ' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchForms( $strSql, $objDatabase );
	}

	public static function fetchCustomFormsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {

		if( true == is_null( $intApplicantId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						f.*,
						d.title as document_title
					FROM
						forms f,
						documents d
					WHERE
						f.document_id = d.id
						AND f.cid = d.cid
						AND f.applicant_id = ' . ( int ) $intApplicantId . '
						AND f.cid = ' . ( int ) $intCid;

		return self::fetchForms( $strSql, $objDatabase );
	}

}
?>