<?php

class CAncillaryCustomerOrder extends CBaseAncillaryCustomerOrder {

	const DIRECT_ENERGY	= 1;
	const BOUNCE_ENERGY	= 2;
	const ACH = 'ach';
	const CREDIT_CARD = 'creditcard';

	protected $m_intTypeId;
	protected $m_strPlanId;
	protected $m_strProductId;
	protected $m_strAncillaryPlanPromoDetailId;

	protected $m_strServiceAddr1;
	protected $m_strServiceAddr2;
	protected $m_strServiceCity;
	protected $m_strServiceState;
	protected $m_strServiceZip;
	protected $m_strFirstName;
	protected $m_strLastName;
	protected $m_strEmail;
	protected $m_strPhoneNumber;
	protected $m_boolAgreesToTerms;
	protected $m_strBillAddr1;
	protected $m_strBillAddr2;
	protected $m_strBillCity;
	protected $m_strBillState;
	protected $m_strBillZip;

	protected $m_boolIsOrderValid;
	protected $m_strUtilityAccountNumber;
	protected $m_strEsiId;

	protected $m_boolIsAutoPay;
	protected $m_strPaymentType;
	protected $m_strCCHolderName;
	protected $m_strCCNum;
	protected $m_strCCExpMonth;
	protected $m_strCCExpYear;
	protected $m_strCCSecCode;
	protected $m_strAccountType;
	protected $m_strAcctName;
	protected $m_strBankName;
	protected $m_strRoutingNum;
	protected $m_strAcctNum;
	protected $m_strAcctNumConfirm;

	public function setTypeId( $intTypeId ) {
		$this->m_intTypeId = CStrings::strToIntDef( $intTypeId, NULL, false );
	}

	public function setPlanId( $strPlanId ) {
		$this->m_strPlanId = CStrings::strToIntDef( $strPlanId, NULL, false );
	}

	public function setProductId( $strProductId ) {
		$this->m_strProductId = CStrings::strToIntDef( $strProductId, NULL, false );
	}

	public function setAncillaryPlanPromoDetailId( $strAncillaryPlanPromoDetailId ) {
		$this->m_strAncillaryPlanPromoDetailId = CStrings::strToIntDef( $strAncillaryPlanPromoDetailId, NULL, false );
	}

	public function setServiceAddr1( $strServiceAddr1 ) {
		$this->m_strServiceAddr1 = CStrings::strTrimDef( $strServiceAddr1, 100, NULL, true );
	}

	public function setServiceAddr2( $strServiceAddr2 ) {
		$this->m_strServiceAddr2 = CStrings::strTrimDef( $strServiceAddr2, 100, NULL, true );
	}

	public function setServiceCity( $strServiceCity ) {
		$this->m_strServiceCity = CStrings::strTrimDef( $strServiceCity, 100, NULL, true );
	}

	public function setServiceState( $strServiceState ) {
		$this->m_strServiceState = CStrings::strTrimDef( $strServiceState, 100, NULL, true );
	}

	public function setServiceZip( $strServiceZip ) {
		$this->m_strServiceZip = CStrings::strTrimDef( $strServiceZip, 100, NULL, true );
	}

	public function setFirstName( $strFirstName ) {
		$this->m_strFirstName = CStrings::strTrimDef( $strFirstName, 100, NULL, true );
	}

	public function setLastName( $strLastName ) {
		$this->m_strLastName = CStrings::strTrimDef( $strLastName, 100, NULL, true );
	}

	public function setEmail( $strEmail ) {
		$this->m_strEmail = CStrings::strTrimDef( $strEmail, 100, NULL, true );
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = CStrings::strTrimDef( $strPhoneNumber, 100, NULL, true );
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		$this->m_boolAgreesToTerms = CStrings::strTrimDef( $boolAgreesToTerms, 100, NULL, true );
	}

	public function setBillAddr1( $strBillAddr1 ) {
		$this->m_strBillAddr1 = CStrings::strTrimDef( $strBillAddr1, 100, NULL, true );
	}

	public function setBillAddr2( $strBillAddr2 ) {
		$this->m_strBillAddr2 = CStrings::strTrimDef( $strBillAddr2, 100, NULL, true );
	}

	public function setBillCity( $strBillCity ) {
		$this->m_strBillCity = CStrings::strTrimDef( $strBillCity, 100, NULL, true );
	}

	public function setBillState( $strBillState ) {
		$this->m_strBillState = CStrings::strTrimDef( $strBillState, 100, NULL, true );
	}

	public function setBillZip( $strBillZip ) {
		$this->m_strBillZip = CStrings::strTrimDef( $strBillZip, 100, NULL, true );
	}

	public function setIsOrderValid( $boolIsOrderValid ) {
		$this->m_boolIsOrderValid = CStrings::strTrimDef( $boolIsOrderValid, 100, NULL, true );
	}

	public function setUtilityAccountNumber( $strUtilityAccountNumber ) {
		$this->m_strUtilityAccountNumber = CStrings::strTrimDef( $strUtilityAccountNumber, 100, NULL, true );
	}

	public function setEsiId( $strEsiId ) {
		$this->m_strEsiId = CStrings::strTrimDef( $strEsiId, 100, NULL, true );
	}

	public function setIsAutoPay( $boolIsAutoPay ) {
		$this->m_boolIsAutoPay = CStrings::strTrimDef( $boolIsAutoPay, 100, NULL, true );
	}

	public function setPaymentType( $strPaymentType ) {
		$this->m_strPaymentType = CStrings::strTrimDef( $strPaymentType, 100, NULL, true );
	}

	public function setCCHolderName( $strCCHolderName ) {
		$this->m_strCCHolderName = CStrings::strTrimDef( $strCCHolderName, 100, NULL, true );
	}

	public function setCCNum( $strCCNum ) {
		$this->m_strCCNum = CStrings::strTrimDef( $strCCNum, 100, NULL, true );
	}

	public function setCCExpMonth( $strCCExpMonth ) {
		$this->m_strCCExpMonth = CStrings::strTrimDef( $strCCExpMonth, 100, NULL, true );
	}

	public function setCCExpYear( $strCCExpYear ) {
		$this->m_strCCExpYear = CStrings::strTrimDef( $strCCExpYear, 100, NULL, true );
	}

	public function setCCSecCode( $strCCSecCode ) {
		$this->m_strCCSecCode = CStrings::strTrimDef( $strCCSecCode, 100, NULL, true );
	}

	public function setAccountType( $strAccountType ) {
		$this->m_strAccountType = CStrings::strToIntDef( $strAccountType, NULL, false );
	}

	public function setAcctName( $strAcctName ) {
		$this->m_strAcctName = CStrings::strTrimDef( $strAcctName, 100, NULL, true );
	}

	public function setBankName( $strBankName ) {
		$this->m_strBankName = CStrings::strTrimDef( $strBankName, 100, NULL, true );
	}

	public function setRoutingNum( $strRoutingNum ) {
		$this->m_strRoutingNum = CStrings::strTrimDef( $strRoutingNum, 100, NULL, true );
	}

	public function setAcctNum( $strAcctNum ) {
		$this->m_strAcctNum = CStrings::strTrimDef( $strAcctNum, 100, NULL, true );
	}

	public function setAcctNumConfirm( $strAcctNumConfirm ) {
		$this->m_strAcctNumConfirm = CStrings::strTrimDef( $strAcctNumConfirm, 100, NULL, true );
	}

	public function getTypeId() {
		return $this->m_intTypeId;
	}

	public function getPlanId() {
		return $this->m_strPlanId;
	}

	public function getProductId() {
		return $this->m_strProductId;
	}

	public function getAncillaryPlanPromoDetailId() {
		return $this->m_strAncillaryPlanPromoDetailId;
	}

	public function getServiceAddr1() {
		return $this->m_strServiceAddr1;
	}

	public function getServiceAddr2() {
		return $this->m_strServiceAddr2;
	}

	public function getServiceCity() {
		return $this->m_strServiceCity;
	}

	public function getServiceState() {
		return $this->m_strServiceState;
	}

	public function getServiceZip() {
		return $this->m_strServiceZip;
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function getEmail() {
		return $this->m_strEmail;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	public function getBillAddr1() {
		return $this->m_strBillAddr1;
	}

	public function getBillAddr2() {
		return $this->m_strBillAddr2;
	}

	public function getBillCity() {
		return $this->m_strBillCity;
	}

	public function getBillState() {
		return $this->m_strBillState;
	}

	public function getBillZip() {
		return $this->m_strBillZip;
	}

	public function getIsOrderValid() {
		return $this->m_boolIsOrderValid;
	}

	public function getUtilityAccountNumber() {
		return $this->m_strUtilityAccountNumber;
	}

	public function getEsiId() {
		return $this->m_strEsiId;
	}

	public function getIsAutoPay() {
		return $this->m_boolIsAutoPay;
	}

	public function getPaymentType() {
		return $this->m_strPaymentType;
	}

	public function getCCHolderName() {
		return $this->m_strCCHolderName;
	}

	public function getCCNum() {
		return $this->m_strCCNum;
	}

	public function getCCExpMonth() {
		return $this->m_strCCExpMonth;
	}

	public function getCCExpYear() {
		return $this->m_strCCExpYear;
	}

	public function getCCSecCode() {
		return $this->m_strCCSecCode;
	}

	public function getAccountType() {
		return $this->m_strAccountType;
	}

	public function getAcctName() {
		return $this->m_strAcctName;
	}

	public function getBankName() {
		return $this->m_strBankName;
	}

	public function getRoutingNum() {
		return $this->m_strRoutingNum;
	}

	public function getAcctNum() {
		return $this->m_strAcctNum;
	}

	public function getAcctNumConfirm() {
		return $this->m_strAcctNumConfirm;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['type_id'] ) && $boolDirectSet ) {
			$this->setTypeId( trim( $arrmixValues['type_id'] ) );
		} elseif( isset( $arrmixValues['type_id'] ) ) {
			$this->setTypeId( $arrmixValues['type_id'] );
		}

		if( isset( $arrmixValues['vendor_id'] ) && $boolDirectSet ) {
			$this->setAncillaryVendorId( trim( $arrmixValues['vendor_id'] ) );
		} elseif( isset( $arrmixValues['vendor_id'] ) ) {
			$this->setAncillaryVendorId( $arrmixValues['vendor_id'] );
		}

		if( isset( $arrmixValues['plan_id'] ) && $boolDirectSet ) {
			$this->setPlanId( trim( $arrmixValues['plan_id'] ) );
		} elseif( isset( $arrmixValues['plan_id'] ) ) {
			$this->setPlanId( $arrmixValues['plan_id'] );
		}

		if( isset( $arrmixValues['product_id'] ) && $boolDirectSet ) {
			$this->setProductId( trim( $arrmixValues['product_id'] ) );
		} elseif( isset( $arrmixValues['product_id'] ) ) {
			$this->setProductId( $arrmixValues['product_id'] );
		}

		if( isset( $arrmixValues['ancillary_plan_promo_detail_id'] ) && $boolDirectSet ) {
			$this->setProductId( trim( $arrmixValues['ancillary_plan_promo_detail_id'] ) );
		} elseif( isset( $arrmixValues['ancillary_plan_promo_detail_id'] ) ) {
			$this->setProductId( $arrmixValues['ancillary_plan_promo_detail_id'] );
		}

		if( isset( $arrmixValues['service_addr1'] ) && $boolDirectSet ) {
			$this->setServiceAddr1( trim( stripcslashes( $arrmixValues['service_addr1'] ) ) );
		} elseif( isset( $arrmixValues['service_addr1'] ) ) {
			$this->setServiceAddr1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['service_addr1'] ) : $arrmixValues['service_addr1'] );
		}

		if( isset( $arrmixValues['service_addr2'] ) && $boolDirectSet ) {
			$this->setServiceAddr2( trim( stripcslashes( $arrmixValues['service_addr2'] ) ) );
		} elseif( isset( $arrmixValues['service_addr2'] ) ) {
			$this->setServiceAddr2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['service_addr2'] ) : $arrmixValues['service_addr2'] );
		}

		if( isset( $arrmixValues['service_city'] ) && $boolDirectSet ) {
			$this->setServiceCity( trim( stripcslashes( $arrmixValues['service_city'] ) ) );
		} elseif( isset( $arrmixValues['service_city'] ) ) {
			$this->setServiceCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['service_city'] ) : $arrmixValues['service_city'] );
		}

		if( isset( $arrmixValues['service_state'] ) && $boolDirectSet ) {
			$this->setServiceState( trim( stripcslashes( $arrmixValues['service_state'] ) ) );
		} elseif( isset( $arrmixValues['service_state'] ) ) {
			$this->setServiceState( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['service_state'] ) : $arrmixValues['service_state'] );
		}

		if( isset( $arrmixValues['service_zip'] ) && $boolDirectSet ) {
			$this->setServiceZip( trim( stripcslashes( $arrmixValues['service_zip'] ) ) );
		} elseif( isset( $arrmixValues['service_zip'] ) ) {
			$this->setServiceZip( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['service_zip'] ) : $arrmixValues['service_zip'] );
		}

		if( isset( $arrmixValues['f_name'] ) && $boolDirectSet ) {
			$this->setFirstName( trim( stripcslashes( $arrmixValues['f_name'] ) ) );
		} elseif( isset( $arrmixValues['f_name'] ) ) {
			$this->setFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['f_name'] ) : $arrmixValues['f_name'] );
		}

		if( isset( $arrmixValues['l_name'] ) && $boolDirectSet ) {
			$this->setLastName( trim( stripcslashes( $arrmixValues['l_name'] ) ) );
		} elseif( isset( $arrmixValues['l_name'] ) ) {
			$this->setLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['l_name'] ) : $arrmixValues['l_name'] );
		}

		if( isset( $arrmixValues['email'] ) && $boolDirectSet ) {
			$this->setEmail( trim( stripcslashes( $arrmixValues['email'] ) ) );
		} elseif( isset( $arrmixValues['email'] ) ) {
			$this->setEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email'] ) : $arrmixValues['email'] );
		}

		if( isset( $arrmixValues['day_phone'] ) && $boolDirectSet ) {
			$this->setPhoneNumber( trim( stripcslashes( $arrmixValues['day_phone'] ) ) );
		} elseif( isset( $arrmixValues['day_phone'] ) ) {
			$this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['day_phone'] ) : $arrmixValues['day_phone'] );
		}

		if( isset( $arrmixValues['agrees_to_terms'] ) && $boolDirectSet ) {
			$this->setAgreesToTerms( trim( stripcslashes( $arrmixValues['agrees_to_terms'] ) ) );
		} elseif( isset( $arrmixValues['agrees_to_terms'] ) ) {
			$this->setAgreesToTerms( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['agrees_to_terms'] ) : $arrmixValues['agrees_to_terms'] );
		}

		if( isset( $arrmixValues['bill_addr1'] ) && $boolDirectSet ) {
			$this->setBillAddr1( trim( stripcslashes( $arrmixValues['bill_addr1'] ) ) );
		} elseif( isset( $arrmixValues['bill_addr1'] ) ) {
			$this->setBillAddr1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_addr1'] ) : $arrmixValues['bill_addr1'] );
		}

		if( isset( $arrmixValues['bill_addr2'] ) && $boolDirectSet ) {
			$this->setBillAddr2( trim( stripcslashes( $arrmixValues['bill_addr2'] ) ) );
		} elseif( isset( $arrmixValues['bill_addr2'] ) ) {
			$this->setBillAddr2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_addr2'] ) : $arrmixValues['bill_addr2'] );
		}

		if( isset( $arrmixValues['bill_city'] ) && $boolDirectSet ) {
			$this->setBillCity( trim( stripcslashes( $arrmixValues['bill_city'] ) ) );
		} elseif( isset( $arrmixValues['bill_city'] ) ) {
			$this->setBillCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_city'] ) : $arrmixValues['bill_city'] );
		}

		if( isset( $arrmixValues['bill_state'] ) && $boolDirectSet ) {
			$this->setBillState( trim( stripcslashes( $arrmixValues['bill_state'] ) ) );
		} elseif( isset( $arrmixValues['bill_state'] ) ) {
			$this->setBillState( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_state'] ) : $arrmixValues['bill_state'] );
		}

		if( isset( $arrmixValues['bill_zip'] ) && $boolDirectSet ) {
			$this->setBillZip( trim( stripcslashes( $arrmixValues['bill_zip'] ) ) );
		} elseif( isset( $arrmixValues['bill_zip'] ) ) {
			$this->setBillZip( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_zip'] ) : $arrmixValues['bill_zip'] );
		}

		if( isset( $arrmixValues['order_id'] ) && $boolDirectSet ) {
			$this->setRemotePrimaryKey( trim( stripcslashes( $arrmixValues['order_id'] ) ) );
		} elseif( isset( $arrmixValues['order_id'] ) ) {
			$this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['order_id'] ) : $arrmixValues['order_id'] );
		}

		if( isset( $arrmixValues['order_valid'] ) && $boolDirectSet ) {
			$this->setIsOrderValid( trim( stripcslashes( $arrmixValues['order_valid'] ) ) );
		} elseif( isset( $arrmixValues['order_valid'] ) ) {
			$this->setIsOrderValid( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['order_valid'] ) : $arrmixValues['order_valid'] );
		}

		if( isset( $arrmixValues['utility_account_number'] ) && $boolDirectSet ) {
			$this->setUtilityAccountNumber( trim( stripcslashes( $arrmixValues['utility_account_number'] ) ) );
		} elseif( isset( $arrmixValues['utility_account_number'] ) ) {
			$this->setUtilityAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_account_number'] ) : $arrmixValues['utility_account_number'] );
		}

		if( isset( $arrmixValues['esiid'] ) && $boolDirectSet ) {
			$this->setEsiId( trim( stripcslashes( $arrmixValues['esiid'] ) ) );
		} elseif( isset( $arrmixValues['esiid'] ) ) {
			$this->setEsiId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['esiid'] ) : $arrmixValues['esiid'] );
		}

		if( isset( $arrmixValues['auto_pay'] ) && $boolDirectSet ) {
			$this->setIsAutoPay( trim( stripcslashes( $arrmixValues['auto_pay'] ) ) );
		} elseif( isset( $arrmixValues['auto_pay'] ) ) {
			$this->setIsAutoPay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['auto_pay'] ) : $arrmixValues['auto_pay'] );
		}

		if( isset( $arrmixValues['payment_type'] ) && $boolDirectSet ) {
			$this->setPaymentType( trim( stripcslashes( $arrmixValues['payment_type'] ) ) );
		} elseif( isset( $arrmixValues['payment_type'] ) ) {
			$this->setPaymentType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['payment_type'] ) : $arrmixValues['payment_type'] );
		}

		if( isset( $arrmixValues['cc_holder_name'] ) && $boolDirectSet ) {
			$this->setCCHolderName( trim( stripcslashes( $arrmixValues['cc_holder_name'] ) ) );
		} elseif( isset( $arrmixValues['cc_holder_name'] ) ) {
			$this->setCCHolderName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_holder_name'] ) : $arrmixValues['cc_holder_name'] );
		}

		if( isset( $arrmixValues['cc_num'] ) && $boolDirectSet ) {
			$this->setCCNum( trim( stripcslashes( $arrmixValues['cc_num'] ) ) );
		} elseif( isset( $arrmixValues['cc_num'] ) ) {
			$this->setCCNum( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_num'] ) : $arrmixValues['cc_num'] );
		}

		if( isset( $arrmixValues['cc_exp_month'] ) && $boolDirectSet ) {
			$this->setCCExpMonth( trim( stripcslashes( $arrmixValues['cc_exp_month'] ) ) );
		} elseif( isset( $arrmixValues['cc_exp_month'] ) ) {
			$this->setCCExpMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_exp_month'] ) : $arrmixValues['cc_exp_month'] );
		}

		if( isset( $arrmixValues['cc_exp_year'] ) && $boolDirectSet ) {
			$this->setCCExpYear( trim( stripcslashes( $arrmixValues['cc_exp_year'] ) ) );
		} elseif( isset( $arrmixValues['cc_exp_year'] ) ) {
			$this->setCCExpYear( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_exp_year'] ) : $arrmixValues['cc_exp_year'] );
		}

		if( isset( $arrmixValues['cc_sec_code'] ) && $boolDirectSet ) {
			$this->setCCSecCode( trim( stripcslashes( $arrmixValues['cc_sec_code'] ) ) );
		} elseif( isset( $arrmixValues['cc_sec_code'] ) ) {
			$this->setCCSecCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_sec_code'] ) : $arrmixValues['cc_sec_code'] );
		}

		if( isset( $arrmixValues['acct_type'] ) && $boolDirectSet ) {
			$this->setAccountType( trim( stripcslashes( $arrmixValues['acct_type'] ) ) );
		} elseif( isset( $arrmixValues['acct_type'] ) ) {
			$this->setAccountType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['acct_type'] ) : $arrmixValues['acct_type'] );
		}

		if( isset( $arrmixValues['acct_name'] ) && $boolDirectSet ) {
			$this->setAcctName( trim( stripcslashes( $arrmixValues['acct_name'] ) ) );
		} elseif( isset( $arrmixValues['acct_name'] ) ) {
			$this->setAcctName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['acct_name'] ) : $arrmixValues['acct_name'] );
		}

		if( isset( $arrmixValues['bank_name'] ) && $boolDirectSet ) {
			$this->setBankName( trim( stripcslashes( $arrmixValues['bank_name'] ) ) );
		} elseif( isset( $arrmixValues['bank_name'] ) ) {
			$this->setBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bank_name'] ) : $arrmixValues['bank_name'] );
		}

		if( isset( $arrmixValues['routing_num'] ) && $boolDirectSet ) {
			$this->setRoutingNum( trim( stripcslashes( $arrmixValues['routing_num'] ) ) );
		} elseif( isset( $arrmixValues['routing_num'] ) ) {
			$this->setRoutingNum( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['routing_num'] ) : $arrmixValues['routing_num'] );
		}

		if( isset( $arrmixValues['acct_num'] ) && $boolDirectSet ) {
			$this->setAcctNum( trim( stripcslashes( $arrmixValues['acct_num'] ) ) );
		} elseif( isset( $arrmixValues['acct_num'] ) ) {
			$this->setAcctNum( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['acct_num'] ) : $arrmixValues['acct_num'] );
		}

		if( isset( $arrmixValues['acct_num_confirm'] ) && $boolDirectSet ) {
			$this->setAcctNumConfirm( trim( stripcslashes( $arrmixValues['acct_num_confirm'] ) ) );
		} elseif( isset( $arrmixValues['acct_num_confirm'] ) ) {
			$this->setAcctNumConfirm( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['acct_num_confirm'] ) : $arrmixValues['acct_num_confirm'] );
		}

		return;
	}

	public function valTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type_id', 'A type id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valAncillaryVendorId() {
		$boolIsValid = true;
		if( false == valId( $this->getAncillaryVendorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_id', 'A vendor id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valPlanId() {
		$boolIsValid = true;
		if( false == valStr( $this->getPlanId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'plan_id', 'A plan id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		if( false == valStr( $this->getProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_id', 'A product id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valId() {
		$boolIsValid = true;
		if( false == valId( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'A id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_id', 'A client id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		if( false == valId( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'A customer id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		if( false == valId( $this->getLeaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', 'A lease id is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valToken() {
		$boolIsValid = true;
		if( false == valStr( $this->getToken() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		if( false == valStr( $this->getRemotePrimaryKey() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', 'Remote primary key is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valConfirmationNumber() {
		$boolIsValid = true;
		if( false == valStr( $this->getConfirmationNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirmation_number', 'Confirmation number is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valServiceAddr1() {
		$boolIsValid = true;
		if( false == valStr( $this->getServiceAddr1() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_addr1', 'Service Address1 is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valServiceAddr2( $boolIsRequired = false ) {
		$boolIsValid = true;
		if( true == $boolIsRequired && false == valStr( $this->getServiceAddr2() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_addr2', 'Service Address2 is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valServiceCity() {
		$boolIsValid = true;
		if( false == valStr( $this->getServiceCity() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_city', 'Service city is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valServiceState() {
		$boolIsValid = true;
		if( false == valStr( $this->getServiceState() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_state', 'Service city is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valServiceZip( $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getServiceZip() ) || false == CValidation::validatePostalCode( $this->getServiceZip(), $strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_zip',  'Enter valid zip code.' ) );
		}
		return $boolIsValid;
	}

	public function valFirstName() {
		$boolIsValid = true;
		if( false == valStr( $this->getFirstName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'f_name', 'First name is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valLastName() {
		$boolIsValid = true;
		if( false == valStr( $this->getLastName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'l_name', 'Last name is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valEmail() {
		$boolIsValid = true;
		if( false == valStr( $this->getEmail() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Email is required.' ) );
			$boolIsValid &= false;
		} elseif( false == CValidation::validateEmailAddresses( $this->getEmail() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Email address does not appear to be valid.' ) );
				return $boolIsValid;
		}
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of formats available internationally.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */

	public function valPhoneNumber() {
		$boolIsValid = true;
		if( false == valStr( $this->getPhoneNumber() ) || 1 !== CValidation::checkPhoneNumber( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day_phone', 'PhoneNumber is required.' ) );
			$boolIsValid &= false;
		} elseif( 1 != $this->checkPhoneNumberFullValidation( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day_phone', 'Enter valid phone number.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valAgreesToTerms() {
		$boolIsValid = true;
		if( false == $this->getAgreesToTerms() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agrees_to_terms', 'Agreeing to terms is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBillAddr1() {
		$boolIsValid = true;
		if( false == valStr( $this->getBillAddr1() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_addr1', 'Bill Address1 is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBillAddr2( $boolIsRequired = false ) {
		$boolIsValid = true;
		if( true == $boolIsRequired && false == valStr( $this->getBillAddr2() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_addr2', 'Bill Address2 is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBillCity() {
		$boolIsValid = true;
		if( false == valStr( $this->getBillCity() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_city', 'Bill city is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBillState() {
		$boolIsValid = true;
		if( false == valStr( $this->getBillState() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_state', 'Bill city is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBillZip( $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getBillZip() ) || false == CValidation::validatePostalCode( $this->getBillZip(), $strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_zip',  'Enter valid zip code.' ) );
		}
		return $boolIsValid;
	}

	public function valIsOrderValid() {
		$boolIsValid = true;
		if( false == $this->getIsOrderValid() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_valid', 'Invalid order.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valUtilityAccountNumber() {
		$boolIsValid = true;
		if( false == valStr( $this->getUtilityAccountNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_account_number', 'Utility account number is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valEsiId() {
		$boolIsValid = true;
		if( false == valStr( $this->getEsiId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'esiid', 'Esiid is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valAutoPay() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getIsAutoPay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auto_pay', 'Payment method is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valPaymentType() {
		$boolIsValid = true;
		if( false == valStr( $this->getPaymentType() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type', 'Payment type is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valCCHolderName() {
		$boolIsValid = true;
		if( false == valStr( $this->getCCHolderName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_name', 'Name on account is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valCCNum() {
		$boolIsValid = true;
		if( false == valStr( $this->getCCNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_num', 'CC number is required.' ) );
			$boolIsValid &= false;
		}
		if( 0 !== preg_match( '/[^0-9]/', $this->getCCNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_num', 'CC number must be a number.' ) );
			return false;
		}
		return $boolIsValid;
	}

	public function valCCExpMonth() {
		$boolIsValid = true;
		if( false == valStr( $this->getCCExpMonth() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_month', 'CC expiry month is required.' ) );
			$boolIsValid &= false;
		}
		if( true == valStr( $this->getCCExpMonth() ) && true == valStr( $this->getCCExpYear() ) && $this->getCCExpYear() == date( 'Y' ) && $this->getCCExpMonth() < date( 'm' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_month', 'CC expiry month is invalid or expired.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valCCExpYear() {
		$boolIsValid = true;
		if( false == valStr( $this->getCCExpYear() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_year', 'CC expiry year is required.' ) );
			$boolIsValid &= false;
		}
		if( true == valStr( $this->getCCExpYear() ) && $this->getCCExpYear() < date( 'Y' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_year', 'CC expiry year is invalid or expired.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valCCSecCode() {
		$boolIsValid = true;
		if( false == valStr( $this->getCCSecCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_sec_code', 'CC security code is required.' ) );
			$boolIsValid &= false;
		}
		if( 0 !== preg_match( '/[^0-9]/', $this->getCCSecCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_sec_code', 'CC security code must be a number.' ) );
			return false;
		}
		return $boolIsValid;
	}

	public function valAccountType() {
		$boolIsValid = true;
		if( false == valStr( $this->getAccountType() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_type', 'Account type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valAcctName() {
		$boolIsValid = true;
		if( false == valStr( $this->getAcctName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_name', 'Account name is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valBankName() {
		$boolIsValid = true;
		if( false == valStr( $this->getBankName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_name', 'Bank name is required.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valRoutingNum() {
		$boolIsValid = true;

		if( false == valStr( $this->getRoutingNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_num', 'Routing number is required.' ) );
			return false;
		}
		if( 0 !== preg_match( '/[^0-9]/', $this->getRoutingNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_num', 'Routing number must be a number.' ) );
			return false;
		}
		if( 9 != strlen( $this->getRoutingNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_num', 'Routing number must be 9 digits long.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valAcctNum() {
		$boolIsValid = true;

		if( false == valStr( $this->getAcctNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_num', 'Account number is required.' ) );
			$boolIsValid &= false;
		}
		if( 0 !== preg_match( '/[^0-9]/', $this->getRoutingNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_num', 'Account number must be a number.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valAcctNumConfirm() {
		$boolIsValid = true;
		if( false == valStr( $this->getAcctNumConfirm() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_num_confirm', 'Account number confirm is required.' ) );
			$boolIsValid &= false;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->getRoutingNum() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_num_confirm', 'Account number must be a number.' ) );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && $this->getAcctNum() != $this->getAcctNumConfirm() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acct_num_confirm', 'Account number confirm is invalid.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function checkPhoneNumberFullValidation( $strNumber ) {
		if( true == preg_match( '/\-{3,}/', $strNumber ) || 1 !== preg_match( '/^[\(]{0,1}(\d{1,3})[\)]?[\-)]?[\s]{0,}(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?[\s]?(\d*)$/', $strNumber ) ) {
			return 'Phone number not in valid format.';
		}

		return 1;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'view_plans':
				$boolIsValid &= $this->valTypeId();
				break;

			case 'view_product_details':
				$boolIsValid &= $this->valAncillaryVendorId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				break;

			case 'view_plans':
				$boolIsValid &= $this->valTypeId();
				break;

			case 'valiate_sign_up':
				$boolIsValid &= $this->valTypeId();
				$boolIsValid &= $this->valAncillaryVendorId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				break;

			case 'validate_service_address':
				$boolIsValid &= $this->valTypeId();
				$boolIsValid &= $this->valAncillaryVendorId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				$boolIsValid &= $this->valServiceAddr1();
				$boolIsValid &= $this->valServiceZip();
				break;

			case 'terms':
				$boolIsValid &= $this->valAgreesToTerms();
				break;

			case 'sign_up':
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valServiceAddr1();
				$boolIsValid &= $this->valServiceAddr2( false );
				$boolIsValid &= $this->valServiceCity();
				$boolIsValid &= $this->valServiceState();
				$boolIsValid &= $this->valServiceZip();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmail();
				break;

			case 'view_terms':
				$boolIsValid &= $this->valTypeId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valServiceAddr1();
				$boolIsValid &= $this->valServiceAddr2( false );
				$boolIsValid &= $this->valServiceCity();
				$boolIsValid &= $this->valServiceState();
				$boolIsValid &= $this->valServiceZip();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmail();
				$boolIsValid &= $this->valIsOrderValid();
				break;

			case 'view_billing_information':
				$boolIsValid &= $this->valTypeId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valServiceAddr1();
				$boolIsValid &= $this->valServiceAddr2( false );
				$boolIsValid &= $this->valServiceCity();
				$boolIsValid &= $this->valServiceState();
				$boolIsValid &= $this->valServiceZip();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmail();
				$boolIsValid &= $this->valIsOrderValid();
				break;

			case 'billing_information':
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valBillAddr1();
				$boolIsValid &= $this->valBillAddr2( false );
				$boolIsValid &= $this->valBillCity();
				$boolIsValid &= $this->valBillState();
				$boolIsValid &= $this->valBillZip();
				break;

			case 'payment_information':
				$boolIsValid &= $this->valAutoPay();
				if( self::BOUNCE_ENERGY == $this->getAncillaryVendorId() ) {
					$boolIsValid &= $this->valPaymentType();
					if( true == $boolIsValid && self::CREDIT_CARD == $this->getPaymentType() ) {
						$boolIsValid &= $this->valCCHolderName();
						$boolIsValid &= $this->valCCNum();
						$boolIsValid &= $this->valCCExpMonth();
						$boolIsValid &= $this->valCCExpYear();
						$boolIsValid &= $this->valCCSecCode();
					}
					if( true == $boolIsValid && self::ACH == $this->getPaymentType() ) {
						$boolIsValid &= $this->valAccountType();
						$boolIsValid &= $this->valAcctName();
						$boolIsValid &= $this->valBankName();
						$boolIsValid &= $this->valRoutingNum();
						$boolIsValid &= $this->valAcctNum();
						$boolIsValid &= $this->valAcctNumConfirm();
					}
				}
				break;

			case 'setup':
				$boolIsValid &= $this->valTypeId();
				$boolIsValid &= $this->valPlanId();
				$boolIsValid &= $this->valProductId();
				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valServiceAddr1();
				$boolIsValid &= $this->valServiceAddr2( false );
				$boolIsValid &= $this->valServiceCity();
				$boolIsValid &= $this->valServiceState();
				$boolIsValid &= $this->valServiceZip();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmail();
				$boolIsValid &= $this->valBillAddr1();
				$boolIsValid &= $this->valBillAddr2( false );
				$boolIsValid &= $this->valBillCity();
				$boolIsValid &= $this->valBillState();
				$boolIsValid &= $this->valBillZip();
				if( self::BOUNCE_ENERGY == $this->getAncillaryVendorId() && true == valId( $this->getIsAutoPay() ) ) {
					$boolIsValid &= $this->valPaymentType();
					if( true == $boolIsValid && self::CREDIT_CARD == $this->getPaymentType() ) {
						$boolIsValid &= $this->valCCHolderName();
						$boolIsValid &= $this->valCCNum();
						$boolIsValid &= $this->valCCExpMonth();
						$boolIsValid &= $this->valCCExpYear();
						$boolIsValid &= $this->valCCSecCode();
					}
					if( true == $boolIsValid && self::ACH == $this->getPaymentType() ) {
						$boolIsValid &= $this->valAccountType();
						$boolIsValid &= $this->valAcctName();
						$boolIsValid &= $this->valBankName();
						$boolIsValid &= $this->valRoutingNum();
						$boolIsValid &= $this->valAcctNum();
						$boolIsValid &= $this->valAcctNumConfirm();
					}
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}
		return $boolIsValid;
	}

}
?>