<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageTypes
 * Do not add any new functions to this class.
 */

class CPackageTypes extends CBasePackageTypes {

	public static function fetchPackageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPackageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPackageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPackageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPackageTypes( $objDatabase ) {
        $strSql = 'SELECT * FROM package_types WHERE is_published = 1 ORDER BY order_num';
	    return self::fetchPackageTypes( $strSql, $objDatabase );
    }

    public static function fetchPackageTypeById( $intPackageTypeId, $objDataBase ) {
    	$strSql = 'SELECT * FROM package_types WHERE id = ' . ( int ) $intPackageTypeId;
    	return self::fetchPackageType( $strSql, $objDataBase );
    }
}
?>