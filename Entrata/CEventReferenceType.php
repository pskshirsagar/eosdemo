<?php

class CEventReferenceType extends CBaseEventReferenceType {

	const VENDOR  						= 5;
	const PACKAGE						= 6;
	const CRAIGSLIST					= 7;
	const LEASE_CUSTOMER				= 8;
	const CONTACT_SUBMISSION			= 9;
	const INSPECTION					= 10;
	const UNIT_SPACE					= 11;
	const PERIOD						= 12;
	const CHORE							= 13;
	const WORK_ORDER					= 14;
	const JOB							= 15;
	const CONTRACT						= 16;
	const FIXED_ASSETS					= 17;
	const PROPERTY_UNIT					= 18;
	const DRAW_REQUEST					= 20;
	const BANK_ACCOUNT					= 21;
	const JOB_GROUP						= 22;
	const BUDGET_CHANGE_ORDER			= 23;
	const BUDGET_ADJUSTMENT	 			= 24;
	const OWNER							= 25;
	const VIOLATION						= 26;
	const GROUP_CONTRACT_UNIT_SPACE 	= 27;
	const JOURNAL_ENTRY_TEMPLATE 		= 28;
	const INVOICE                       = 29;
	const PURCHASE_ORDER         		= 30;
	const ADVANCED_BUDGET_WORKSHEET     = 31;
}
?>