<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnitTypes
 * Do not add any new functions to this class.
 */

class CJobUnitTypes extends CBaseJobUnitTypes {

	public static function fetchJobUnitTypesByJobIdByCid( $intJobId, $intCid, $objDatabase, $boolIsUnitsData = true ) {
		$strSelectFields = '';
		$strJoins = '';

		if( false == valId( $intJobId ) ) return NULL;

		if( true == $boolIsUnitsData ) {
			$strSelectFields = ',COALESCE( SUM( jutp.unit_count ) OVER ( PARTITION BY jutp.cid, jutp.job_id, jutp.unit_type_id ), 0 ) AS total_unit_count,
						COALESCE( COUNT( ju.id ), 0 ) AS total_assigned_units';

			$strJoins = 'LEFT JOIN job_unit_type_phases jutp ON ( jutp.cid = jut.cid AND jutp.job_id = jut.job_id AND jutp.unit_type_id = jut.unit_type_id )
						LEFT JOIN job_units ju ON ( ju.cid = jut.cid AND ju.job_id = jut.job_id AND ju.unit_type_id = jut.unit_type_id )';
		}

		$strSql = 'SELECT
						DISTINCT jut.*,
						ut.name AS unit_type_name' . $strSelectFields . '
					FROM
						job_unit_types jut
						JOIN unit_types ut ON ( ut.cid = jut.cid AND ut.id = jut.unit_type_id ) ' . $strJoins . '
					WHERE
						jut.cid = ' . ( int ) $intCid . '
						AND jut.job_id = ' . ( int ) $intJobId . '
					GROUP BY
						jut.id,
						jut.cid,
						jutp.id,
						jutp.cid,
						ut.name';

		return self::fetchJobUnitTypes( $strSql, $objDatabase );
	}

}
?>