<?php

class CSubsidyProgramType extends CBaseSubsidyProgramType {

	const HUD_SECTION_8             = 1;
	const HUD_RENT_SUPPLEMENT       = 2;
	const HUD_RAP                   = 3;
	const HUD_SECTION_236           = 4;
	const HUD_BMIR                  = 5;
	const HUD_811_PRA_DEMO          = 6;
	const HUD_SECTION_202_PRAC      = 7;
	const HUD_SECTION_811_PRAC      = 8;
	const HUD_SECTION_202_162_PAC   = 9;
	const HOME_HIGH_HOME            = 10;
	const HOME_LOW_HOME             = 11;
	const LIHTC_40_60               = 12;
	const LIHTC_20_50               = 13;
	const LIHTC_15_40               = 14;
	const LIHTC_HTC                 = 15;
	const LIHTC_EXCHANGE            = 16;
	const LIHTC_TCAP                = 17;
	const LIHTC_SHTF                = 18;
	const LIHTC_NSP                 = 19;
	const LIHTC_OTHER               = 20;

	public static $c_arrintHudSubsidyProgramType = [
		self::HUD_SECTION_8,
		self::HUD_RENT_SUPPLEMENT,
		self::HUD_RAP,
		self::HUD_BMIR,
		self::HUD_811_PRA_DEMO,
		self::HUD_SECTION_202_PRAC,
		self::HUD_SECTION_811_PRAC,
		self::HUD_SECTION_202_162_PAC
	];

	public static $c_arrintTaxCreditSubsidyProgramType = [
		self::LIHTC_40_60,
		self::LIHTC_20_50,
		self::LIHTC_15_40,
		self::LIHTC_HTC,
		self::LIHTC_EXCHANGE,
		self::LIHTC_TCAP,
		self::LIHTC_SHTF,
		self::LIHTC_NSP,
		self::LIHTC_OTHER
	];

	public static $c_arrintHomeSubsidyProgramType = [
		self::HOME_HIGH_HOME,
		self::HOME_LOW_HOME
	];

	public static $c_arrintOperatingRentSubsidyProgramTypes	= [
		self::HUD_SECTION_202_PRAC,
		self::HUD_SECTION_811_PRAC
	];
	public static $c_arrintContractUnitCountSubsidyContractTypes	= [ self::HUD_SECTION_8, self::HUD_SECTION_202_162_PAC, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC, self::HUD_RENT_SUPPLEMENT, self::HUD_RAP, self::HUD_811_PRA_DEMO ];
	public static $c_arrintContractNumberSubsidyContractTypes		= [ self::HUD_SECTION_8, self::HUD_SECTION_202_162_PAC, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get subsidy rent labels by subsidy program type
	 * @param $intSubsidyProgramTypeId
	 * @return array
	 */

	public static function getRentLabelsBySubsidyProgramType( $intSubsidyProgramTypeId ) {
		$strTotalRent 	= 'Gross Rent';
		$strSubsidyRent = 'Contract Rent';

		if( self::HUD_SECTION_236 == $intSubsidyProgramTypeId ) {
			$strTotalRent 	= 'Market Rent';
			$strSubsidyRent = 'Basic Rent';
		} elseif( self::HUD_BMIR == $intSubsidyProgramTypeId ) {
			$strTotalRent 	= '110% BMIR Rent';
			$strSubsidyRent = 'BMIR Rent';
		} elseif( true === in_array( $intSubsidyProgramTypeId, self::$c_arrintOperatingRentSubsidyProgramTypes ) ) {
			$strTotalRent 	= 'Operating Rent';
		}

		return [ $strTotalRent, $strSubsidyRent ];
	}

}
?>