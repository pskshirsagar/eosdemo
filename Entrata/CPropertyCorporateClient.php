<?php

class CPropertyCorporateClient extends CBasePropertyCorporateClient {

	public function valId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCid() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		// }

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getPropertyId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCorporateClientId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCorporateClientId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corporate_client_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getUpdatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getUpdatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCreatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCreatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>