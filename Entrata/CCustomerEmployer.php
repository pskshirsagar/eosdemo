<?php

class CCustomerEmployer extends CBaseCustomerEmployer {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
            $boolIsValid = false;
            trigger_error( 'Invalid Customer Employer Request:  Id required - CCustomerEmployer::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Employer Request:  Management Id required - CCustomerEmployer::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Employer Request:  Customer Id required - CCustomerEmployer::valCustomerId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valIncomeTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intIncomeTypeId ) || 0 >= ( int ) $this->m_intIncomeTypeId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Employer Request:  Employer Type Id required - CCustomerEmployer::valIncomeTypeId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valEmployersName() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strEmployersName ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employers_name', '' ) );
        // }

        return $boolIsValid;
    }

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
    public function checkPhoneNumberFullValidation( $strNumber ) {
        if( true == preg_match( '/\-{3,}/', $strNumber ) || 1 !== preg_match( '/^[\(]{0,1}(\d{1,3})[\)]?[\-)]?[\s]{0,}(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?[\s]?(\d*)$/', $strNumber ) ) {
            return 'Phone number not in valid format.';
        }

        return 1;
    }

    public function valEmployersPhoneNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( false == is_null( $this->getEmployersPhoneNumber() ) && 1 != $this->checkPhoneNumberFullValidation( $this->getEmployersPhoneNumber() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employers_phone_number', 'Enter valid company phone number.', 504 ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valStreetLine1() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine1 )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', '' ));
        // }

        return $boolIsValid;
    }

    public function valStreetLine2() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine2 )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', '' ));
        // }

        return $boolIsValid;
    }

    public function valStreetLine3() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine3 ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line3', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strCity ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStateCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valProvince() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strProvince ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == $boolIsValid && ( 0 == strlen( trim( $this->getPostalCode() ) ) || ( false == is_null( $this->getPostalCode() ) && false == CValidation::validatePostalCode( $this->getPostalCode(), $this->getCountryCode() ) ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employers_postal_code',  'Enter valid company zip code.' ) );
        }

        return $boolIsValid;
    }

    public function valCountryCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strCountryCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSupervisorsName() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strSupervisorsName ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'supervisors_name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSupervisorsPhoneNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strSupervisorsPhoneNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'supervisors_phone_number', '' ) );
        // }

        if( false == is_null( $this->getSupervisorsPhoneNumber() ) && 1 != CCustomerValidator::checkPhoneNumberFullValidation( $this->getSupervisorsPhoneNumber() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'supervisors_phone_number', 'Enter valid supervisors phone number.' ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valPosition() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strPosition ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'position', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGrossSalary() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( 0 > ( float ) $this->getGrossSalary() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_gross_salary', 'Salary must be greater than zero.' ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valDateStarted() {
        $boolIsValid = true;

        if( true == valStr( $this->getDateStarted() ) ) {
            $mixValidatedDateStarted = CValidation::checkDateFormat( $this->getDateStarted(), $boolFormat = true );
            if( false === $mixValidatedDateStarted ) {
                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_started', 'Start date must be in mm/dd/yyyy format.' ) );
            } else {
                $this->setDateStarted( date( 'm/d/Y', strtotime( $mixValidatedDateStarted ) ) );
            }
        }

        return $boolIsValid;
    }

    public function valDateTerminated() {

        $boolIsValid = true;
        $boolIsValid &= $this->valDateStarted();

        if( true == valStr( $this->getDateTerminated() ) ) {
            $mixValidatedDateTerminated = CValidation::checkDateFormat( $this->getDateTerminated(), $boolFormat = true );
            if( false === $mixValidatedDateTerminated ) {
                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_terminated', 'End date must be in mm/dd/yyyy format.' ) );
            } else {
                $this->setDateTerminated( date( 'm/d/Y', strtotime( $mixValidatedDateTerminated ) ) );
            }

            if( true == valStr( $this->getDateStarted() ) && true == $boolIsValid
                && ( strtotime( $this->getDateStarted() ) > strtotime( $this->getDateTerminated() ) ) ) {

                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_terminated', 'End date must be greater than or equal to start date.' ) );
            }
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valDateTerminated();
                $boolIsValid &= $this->valEmployersPhoneNumber();
                $boolIsValid &= $this->valSupervisorsPhoneNumber();
                $boolIsValid &= $this->valGrossSalary();
                $boolIsValid &= $this->valPostalCode();
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valDateTerminated();
                $boolIsValid &= $this->valEmployersPhoneNumber();
				$boolIsValid &= $this->valSupervisorsPhoneNumber();
                $boolIsValid &= $this->valGrossSalary();
                $boolIsValid &= $this->valPostalCode();
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function mapCorrespondingApplicantIncomeData( $objApplicantIncome ) {

    	$this->setCid( $objApplicantIncome->getCid() );
		$this->setIncomeTypeId( $objApplicantIncome->getIncomeTypeId() );
		$this->setEmployersName( $objApplicantIncome->getPayerName() );
		$this->setEmployersEmail( $objApplicantIncome->getContactEmail() );
		$this->setEmployersPhoneNumber( $objApplicantIncome->getContactPhoneNumber() );
		$this->setStreetLine1( $objApplicantIncome->getStreetLine1() );
		$this->setStreetLine2( $objApplicantIncome->getStreetLine2() );
		$this->setStreetLine3( $objApplicantIncome->getStreetLine3() );
		$this->setCity( $objApplicantIncome->getCity() );
		$this->setStateCode( $objApplicantIncome->getStateCode() );
		$this->setProvince( $objApplicantIncome->getProvince() );
		$this->setPostalCode( $objApplicantIncome->getPostalCode() );
		$this->setCountryCode( $objApplicantIncome->getCountryCode() );
		$this->setSupervisorsName( $objApplicantIncome->getContactName() );
		$this->setSupervisorsPhoneNumber( $objApplicantIncome->getContactPhoneNumber() );
		$this->setPosition( $objApplicantIncome->getPosition() );
		$this->setGrossSalary( $objApplicantIncome->getAmount() );
		$this->setDateStarted( $objApplicantIncome->getDateStarted() );
		$this->setDateTerminated( $objApplicantIncome->getDateStopped() );

    	return true;
    }

}
?>