<?php
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CUnitSpaces;

class CApplicationDuplicate extends CBaseApplicationDuplicate {

	protected $m_intApplicationId;

	protected $m_objClient;
	protected $m_objProperty;
	protected $m_objApplication;
	protected $m_objPropertyUnit;
	protected $m_objPropertyFloorplan;
	protected $m_objUnitSpace;
	protected $m_objApplicant;

	protected $m_intLastContactDays;

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Company property is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valLeaseCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAssignedEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeadSourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSemAdGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSemKeywordId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSemSourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCallId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;

        if( true == is_null( $this->getNotes() ) && true == $this->getIsTextViewed() && true == $this->getIsSendText() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', __( 'Message is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valContactDatetime() {
         $boolIsValid = true;

        if( true == is_null( $this->getContactDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_datetime', __( 'Contact date time is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valFollowUpOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowOnCalendar() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredRentMin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredRentMax() {
        $boolIsValid = true;

        if( false == is_null( $this->getDesiredRentMax() ) && 0 < $this->getDesiredRentMax() && $this->getDesiredRentMax() < $this->getDesiredRentMin() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_rent_max', __( 'Desired maximum rent can\'t be less than the desired minimum rent.' ) ) );
    	}

        return $boolIsValid;
    }

    public function valTermMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseStartDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredBedrooms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredBathrooms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsWebInitiated() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsTextViewed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSendText() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valContactAcceptedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valContactAcceptedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valContactDatetime();
            	$boolIsValid &= $this->valDesiredRentMax();
            	break;

            case 'sms_insert':
            case 'sms_update':
            	$boolIsValid &= $this->valNotes();
            	break;

            case 'resident_work_add_lead':
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Get Functions
	 */

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getApplicationId() {
        return $this->m_intApplicationId;
    }

   	public function getLastContactDays() {
    	return $this->m_intLastContactDays;
    }

	/**
	 * Set Functions
	 */

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['application_id'] ) ) 			$this->setApplicationId( $arrmixValues['application_id'] );
        if( true == isset( $arrmixValues['last_contact_days'] ) ) 		$this->setLastContactDays( $arrmixValues['last_contact_days'] );

        return;
    }

	public function setLastContactDays( $intLastContactDays ) {
		$this->m_intLastContactDays = $intLastContactDays;
	}

    /**
     * Get or Fetch Functions
     */

    public function getOrFetchProperty( $objDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objDatabase );
		}
	}

	public function getOrFetchApplication( $objDatabase ) {

		if( true == valObj( $this->m_objApplication, 'CApplication' ) ) {
			return $this->m_objApplication;
		} else {
			return $this->fetchApplication( $objDatabase );
		}
	}

	public function getOrFetchApplicant( $objDatabase ) {

		if( true == valObj( $this->m_objApplicant, 'CApplicant' ) ) {
			return $this->m_objApplicant;
		} else {
			return $this->fetchApplicant( $objDatabase );
		}
	}

	/**
	 * Fetch Functions
	 */

	public function fetchProperty( $objDatabase ) {
    	$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );
    	return $this->m_objProperty;
	}

	public function fetchApplication( $objDatabase ) {
    	$this->m_objApplication = CApplications::fetchApplicationByPropertyIdByApplicantApplicationIdByCid( $this->m_intPropertyId, $this->m_intApplicantApplicationId, $this->m_intCid, $objDatabase );

    	return $this->m_objApplication;
	}

	public function fetchUnitSpace( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) ) return NULL;

        $this->m_objUnitSpace = CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );

        return $this->m_objUnitSpace;
    }

    public function fetchPropertyFloorplan( $objDatabase ) {
		if( true == is_null( $this->getPropertyFloorplanId() ) || false == is_numeric( $this->getPropertyFloorplanId() ) ) return NULL;

        $this->m_objPropertyFloorplan = CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByCid( $this->getPropertyFloorplanId(), $this->getCid(), $objDatabase );

        return $this->m_objPropertyFloorplan;
    }

	public function fetchPropertyUnit( $objDatabase ) {
		if( true == is_null( $this->getPropertyUnitId() ) || false == is_numeric( $this->getPropertyUnitId() ) ) return NULL;

        $this->m_objPropertyUnit = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );

        return $this->m_objPropertyUnit;
    }

    public function fetchApplicant( $objDatabase ) {
    	$this->m_objApplicant = CApplicants::fetchApplicantByApplicantApplicationIdByCid( $this->getApplicantApplicationId(), $this->getCid(), $objDatabase );

    	return $this->m_objApplicant;
	}

	public function fetchPropertyLeadSource( $objDatabase ) {
    	if( true == is_null( $this->m_intLeadSourceId ) ) return NULL;

    	return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByLeadSourceIdByPropertyIdByCid( $this->m_intLeadSourceId, $this->m_intPropertyId, $this->getCid(), $objDatabase );
    }

    public function fetchApplicantApplicationById( $objDatabase ) {
    	return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByIdByCid( $this->getApplicantApplicationId(), $this->getCid(), $objDatabase );
	}

    public function fetchCall( $objDatabase ) {
    	if( true == is_null( $this->getCallId() ) ) return NULL;

    	return CCalls::fetchCallById( $this->getCallId(), $objDatabase );
	}

	public function fetchPropertyLeasingAgentByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentByCompanyEmployeeIdByPropertyIdByCid( $intCompanyEmployeeId, $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

    /**
     * Other Functions
     */

    public function loadApplication() {

		if( false == is_null( $this->getPropertyFloorplanId() ) )
			$this->m_objApplication->setPropertyFloorplanId( $this->getPropertyFloorplanId() );

		if( false == is_null( $this->getPropertyUnitId() ) )
			$this->m_objApplication->setPropertyUnitId( $this->getPropertyUnitId() );

		if( false == is_null( $this->getUnitSpaceId() ) )
			$this->m_objApplication->setUnitSpaceId( $this->getUnitSpaceId() );

		if( false == is_null( $this->getLeadSourceId() ) )
			$this->m_objApplication->setLeadSourceId( $this->getLeadSourceId() );

		if( false == is_null( $this->getTermMonth() ) )
			$this->m_objApplication->setTermMonth( $this->getTermMonth() );

		if( false == is_null( $this->getLeaseStartDate() ) )
			$this->m_objApplication->setLeaseStartDate( $this->getLeaseStartDate() );

		if( false == is_null( $this->getDesiredRentMax() ) && 0 < $this->getDesiredRentMax() )
			$this->m_objApplication->setDesiredRentMax( $this->getDesiredRentMax() );

		if( false == is_null( $this->getDesiredRentMin() ) && 0 < $this->getDesiredRentMin() )
			$this->m_objApplication->setDesiredRentMin( $this->getDesiredRentMin() );

		if( false == is_null( $this->getDesiredBedrooms() ) )
			$this->m_objApplication->setDesiredBedrooms( $this->getDesiredBedrooms() );

		if( false == is_null( $this->getDesiredBathrooms() ) )
			$this->m_objApplication->setDesiredBathrooms( $this->getDesiredBathrooms() );

		return $this->m_objApplication;
    }

    public function export( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {

    	$this->getOrFetchProperty( $objDatabase );
    	$this->getOrFetchApplication( $objDatabase );

    	if( false == valObj( $this->m_objApplication, 'CApplication' ) ) {
    		trigger_error( 'Application Duplicate [Id] ' . $this->getId() . ' is not associated with application.', E_USER_WARNING );
    		return false;
    	}

    	$this->loadApplication();

    	// Throw an error if Company Property is not integrated or the application has already been exported.
    	if( true == is_null( $this->m_objProperty ) || true == is_null( $this->m_objProperty->getRemotePrimaryKey() ) ) return true;

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );
		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		// We need to dedupe the guest cards on AMSI side, for that we need to call the modify guest card service.
		// If modify guest card service is there then we call the same else send guest card.

		$intIntegrationServiceId = CIntegrationService::SEND_GUEST_CARD;

		if( CIntegrationClientType::AMSI == $objIntegrationDatabase->getIntegrationClientTypeId() ) {

			$objIntegrationClient = $objIntegrationDatabase->fetchIntegrationClientByIntegrationServiceId( CIntegrationService::MODIFY_GUEST_CARD, $objDatabase );

			if( true == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) {
				$intIntegrationServiceId = CIntegrationService::MODIFY_GUEST_CARD;
			}
		}

	    if( CIntegrationService::SEND_GUEST_CARD == $intIntegrationServiceId && true == valStr( $this->m_objApplication->getRemotePrimaryKey() ) ) {
		    return true;
	    }

    	if( CLeaseIntervalType::APPLICATION == $this->m_intLeaseIntervalTypeId ) {
    		$objGenericWorker = CIntegrationFactory::createWorker( $this->getProperty()->getId(), $this->m_intCid, $intIntegrationServiceId, $intCompanyUserId, $objDatabase );
    	} else {
    		trigger_error( 'No Lease Interval type set when export attempt was made.', E_USER_WARNING );
    		return false;
    	}

		$objGenericWorker->setApplicationDuplicate( $this );
		$objGenericWorker->setApplication( $this->m_objApplication );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		return $objGenericWorker->process();
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolExport = true ) {
    	if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) ) return false;
    	if( true == $boolExport ) $this->export( $intCurrentUserId, $objDatabase );
    	return true;
    }

}
?>