<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyHours
 * Do not add any new functions to this class.
 */

class CPropertyHours extends CBasePropertyHours {

	public static function fetchMaintenancePropertyHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE property_id = %d AND cid = %d AND property_hour_type_id = %d', ( int ) $intPropertyId, ( int ) $intCid, ( int ) CPropertyHourType::MAINTENANCE_HOURS ), $objDatabase );
	}

	public static function fetchMaintenancePropertyHoursByPropertyIdByCidByDays( $intPropertyId, $intCid, $arrintDays, $objDatabase ) {
		$strSql = 'SELECT * FROM property_hours WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND property_hour_type_id = ' . CPropertyHourType::MAINTENANCE_HOURS . ' AND day IN (' . implode( ',', $arrintDays ) . ')';
		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchPropertyHoursWithLunchHoursByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ph.*,
						phs.open_time AS lunch_start_time,
						phs.close_time AS lunch_end_time
					FROM
						property_hours ph
						LEFT JOIN property_hours phs on ( ph.cid = phs.cid AND ph.property_id = phs.property_id AND ph.day = phs.day - ' . CPropertyHour::LUNCH_HOUR_INTERVAL . ' AND phs.is_old_format <> true )
					WHERE
						ph.day < ' . CPropertyHour::MAX_PROPERTY_HOUR_DAYS . '
						AND ph.is_old_format <> true
						AND ph.cid = ' . ( int ) $intCid . '
						AND ph.property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND ph.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchOfficePropertyHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE property_id = %d AND cid = %d AND property_hour_type_id = %d', ( int ) $intPropertyId, ( int ) $intCid, ( int ) CPropertyHourType::OFFICE_HOURS ), $objDatabase );
	}

	public static function fetchCustomOfficePropertyHoursByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		return self::fetchPropertyHours( 'SELECT * FROM property_hours WHERE is_old_format <> true AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )  AND property_hour_type_id = ' . ( int ) CPropertyHourType::OFFICE_HOURS . ' AND cid::int = ' . ( int ) $intCid . '::int ORDER BY day', $objDatabase );
	}

	public static function fetchCustomOfficePropertyHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( 'SELECT * FROM property_hours WHERE is_old_format <> true AND day < 8 AND property_id::int = ' . ( int ) $intPropertyId . '::int AND property_hour_type_id = ' . ( int ) CPropertyHourType::OFFICE_HOURS . ' AND cid::int = ' . ( int ) $intCid . '::int ORDER BY day', $objDatabase );
	}

	public static function fetchOldFormatPropertyOfficeHourByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyOfficeHour( 'SELECT * FROM property_hours WHERE is_old_format = true AND property_id::int = ' . ( int ) $intPropertyId . '::int AND cid::int = ' . ( int ) $intCid . '::int LIMIT 1', $objDatabase );
	}

	public static function fetchPropertyHoursByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT 
						* 
					FROM 
						property_hours 
					WHERE 
						is_old_format <> true 
					AND day < 8
					AND property_id::int = ' . ( int ) $intPropertyId . '::int 
					AND property_hour_type_id = ' . ( int ) $intPropertyHourType . '::int 
					AND cid::int = ' . ( int ) $intCid . '::int 
					ORDER BY day';

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchPropertyHoursWithLunchHoursByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT 
					* 
					FROM 
						property_hours 
					WHERE 
						is_old_format <> true
					AND property_id::int = ' . ( int ) $intPropertyId . '::int 
					AND property_hour_type_id = ' . ( int ) $intPropertyHourType . '::int
					AND cid::int = ' . ( int ) $intCid . '::int 
					ORDER BY day';

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchOldFormatPropertyHourByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = '	SELECT
					*
					FROM
						property_hours
					WHERE
						is_old_format = true
					AND property_id::int = ' . ( int ) $intPropertyId . '::int
					AND property_hour_type_id = ' . ( int ) $intPropertyHourType . '::int
					AND cid::int = ' . ( int ) $intCid . '::int
					LIMIT 1';

		return self::fetchPropertyHour( $strSql, $objDatabase );
	}

	public static function fetchPropertyHoursKeyedByPropertyHourTypeByPropertyIdByDayNumberByCid( $intPropertyHourType, $intPropertyId, $intDayNumber, $intCid, $objDatabase ) {
		if( true == is_null( $intDayNumber ) ) return NULL;

		$strSql = '	SELECT 
						*
					FROM 
						property_hours
					WHERE 
						is_old_format <> true
					AND property_id::int = ' . ( int ) $intPropertyId . '::int
					AND property_hour_type_id = ' . ( int ) $intPropertyHourType . '::int
					AND day = ' . ( int ) $intDayNumber . '
					AND cid::int = ' . ( int ) $intCid . '::int
					LIMIT 1';

		return self::fetchPropertyHour( $strSql, $objDatabase );
	}

	public static function fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIdByCid( $intPropertyHourTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$arrobjPropertyHours = self::fetchPropertyHours( 'SELECT * FROM property_hours WHERE is_old_format <> true AND property_hour_type_id::int = ' . ( int ) $intPropertyHourTypeId . '::int AND property_id::int = ' . ( int ) $intPropertyId . '::int AND cid::int = ' . ( int ) $intCid . '::int ORDER BY day', $objDatabase );

		$arrobjRekeyedPropertyHours = [];
		$arrstrOfficeHourDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $objPropertyHour ) {
				if( true == isset ( $arrstrOfficeHourDays[$objPropertyHour->getDay()] ) ) {
					$arrobjRekeyedPropertyHours[$arrstrOfficeHourDays[$objPropertyHour->getDay()]] = $objPropertyHour;
				}
			}
		}

		return $arrobjRekeyedPropertyHours;
	}

	public static function fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIdsByCid( $intPropertyHourTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						property_hours
					WHERE
						is_old_format <> true
						AND property_hour_type_id = ' . ( int ) $intPropertyHourTypeId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY day';

		$arrobjPropertyHours = self::fetchPropertyHours( $strSql, $objDatabase );

		$arrobjRekeyedPropertyHours = [];
		$arrstrOfficeHourDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $objPropertyHour ) {
				if( true == isset ( $arrstrOfficeHourDays[$objPropertyHour->getDay()] ) ) {
					$arrobjRekeyedPropertyHours[$objPropertyHour->getPropertyId()][$arrstrOfficeHourDays[$objPropertyHour->getDay()]] = $objPropertyHour;
				}
			}
		}

		return $arrobjRekeyedPropertyHours;
	}

	public static function fetchPropertyOfficeHoursKeyedByWeekdayByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$arrobjPropertyHours = self::fetchPropertyHours( 'SELECT * FROM property_hours WHERE is_old_format <> true AND property_id::int = ' . ( int ) $intPropertyId . '::int AND cid::int = ' . ( int ) $intCid . '::int ORDER BY day', $objDatabase );

		$arrobjRekeyedPropertyOfficeHours = [];
		$arrstrOfficeHourDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $objPropertyHour ) {
				if( true == isset ( $arrstrOfficeHourDays[$objPropertyHour->getDay()] ) ) {
					$arrobjRekeyedPropertyOfficeHours[$arrstrOfficeHourDays[$objPropertyHour->getDay()]] = $objPropertyHour;
				}
			}
		}

		return $arrobjRekeyedPropertyOfficeHours;
	}

	public static function fetchCustomPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIdByCid( $intPropertyHourTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_hours
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_hour_type_id = ' . ( int ) $intPropertyHourTypeId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_old_format <> true
					ORDER BY day';
		$arrobjPropertyHours = self::fetchPropertyHours( $strSql, $objDatabase );

		$arrobjRekeyedPropertyOfficeHours = [];
		$arrstrOfficeHourDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $objPropertyHour ) {
				if( true == isset ( $arrstrOfficeHourDays[$objPropertyHour->getDay()] ) ) {
					$arrobjRekeyedPropertyOfficeHours[$arrstrOfficeHourDays[$objPropertyHour->getDay()]] = $objPropertyHour;
				}
			}
		}

		return $arrobjRekeyedPropertyOfficeHours;
	}

	public static function fetchPropertyHoursKeyedByPropertyIdByDayNumberByCid( $intPropertyId, $intDayNumber, $intCid, $objDatabase ) {
		if( true == is_null( $intDayNumber ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						property_hours
					WHERE
						is_old_format <> true
						AND property_id::int = ' . ( int ) $intPropertyId . '::int
						AND day = ' . ( int ) $intDayNumber . '
						AND cid::int = ' . ( int ) $intCid . '::int
					LIMIT 1';

		return self::fetchPropertyOfficeHour( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyHoursDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
					    pho.id,
					    pho.property_id,
					    pho.office_hours,
					    pho.open_time,
					    pho.close_time,
					    pho.by_appointment_only
					FROM
					    property_hours pho
					WHERE
					    ( pho.office_hours IS NULL
					    OR pho.by_appointment_only::INT = 0
					    OR ( pho.open_time IS NULL
					    AND pho.close_time IS NULL ) )
					    AND pho.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					    AND cid::int = ' . ( int ) $intCid . '
					    AND pho.property_hour_type_id = ' . ( int ) CPropertyHourType::OFFICE_HOURS . '
					GROUP BY
					    pho.property_id,
					    pho.office_hours,
					    pho.open_time,
					    pho.close_time,
					    pho.id,
					    pho.by_appointment_only
					HAVING
					    pho.open_time IS NOT NULL OR pho.close_time IS NOT NULL OR pho.office_hours IS NOT NULL OR pho.by_appointment_only::INT=1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyOfficeHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT 
						* 
					FROM 
						property_hours 
					WHERE 
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . ' 
						AND property_hour_type_id = ' . ( int ) CPropertyHourType::OFFICE_HOURS . '
						AND day IS NOT NULL 
						AND open_time IS NOT NULL 
						AND close_time IS NOT NULL 
						AND is_published = TRUE';

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyHoursKeyedByPropertyIdByDayNumberByCid( $intPropertyId, $intDayNumber, $intCid, $objDatabase ) {
		if( true == is_null( $intDayNumber ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						property_hours
					WHERE
						is_old_format <> true
						AND property_id::int = ' . ( int ) $intPropertyId . '::int
						AND day = ' . ( int ) $intDayNumber . '
						AND cid::int = ' . ( int ) $intCid . '::int
						AND is_published = TRUE
					LIMIT 1';

		return self::fetchPropertyOfficeHour( $strSql, $objDatabase );
	}

	public static function fetchOfficePropertyHoursByPropertyIdByCidByDay( $intPropertyId, $intCid, $intDay, $objDatabase ) {
		$strSql = 'SELECT * FROM property_hours WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . ' AND day = ' . ( int ) $intDay;
		return self::fetchPropertyHour( $strSql, $objDatabase );
	}

	public  static function fetchPropertyHoursByPropertyIdsByHoursTypeIdByCid( $arrintPropertyIds, $intHoursTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = '	SELECT 
						* 
					FROM 
						property_hours 
					WHERE
					    cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND property_hour_type_id = ' . ( int ) $intHoursTypeId . '
						AND day IS NOT NULL 
						AND open_time IS NOT NULL 
						AND close_time IS NOT NULL 
						AND is_published = TRUE';

		return self::fetchPropertyHours( $strSql, $objDatabase );

	}

	public static function fetchPropertyOfficeHoursKeyedByWeekdayByTypeIdByPropertyIdByCid( $arrintPropertyHourTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						property_hours
					WHERE
						is_old_format <> true
						AND property_hour_type_id IN ( ' . implode( ',', $arrintPropertyHourTypeId ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
					ORDER BY day';

		$arrobjPropertyHours = self::fetchPropertyHours( $strSql, $objDatabase );

		$arrobjRekeyedPropertyOfficeHours = [];
		$arrstrOfficeHourDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $objPropertyHour ) {
				if( true == isset ( $arrstrOfficeHourDays[$objPropertyHour->getDay()] ) ) {
					$arrobjRekeyedPropertyOfficeHours[$arrstrOfficeHourDays[$objPropertyHour->getDay()]] = $objPropertyHour;
				}
			}
		}

		return $arrobjRekeyedPropertyOfficeHours;
	}

	public static function fetchCustomPropertyHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						property_hours
					WHERE
						day IS NOT NULL
						AND open_time IS NOT NULL
						AND close_time IS NOT NULL
						AND open_time <> close_time
						AND property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND open_time <> close_time
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	public static function fetchPropertyHoursWithLunchHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ph.*,
						phs.open_time AS lunch_start_time,
						phs.close_time AS lunch_end_time
					FROM
						property_hours ph
						LEFT JOIN property_hours phs on ( ph.cid = phs.cid AND ph.property_id = phs.property_id AND ph.day = phs.day - ' . CPropertyHour::LUNCH_HOUR_INTERVAL . ' AND phs.is_old_format <> true )
					WHERE
						ph.day < ' . CPropertyHour::MAX_PROPERTY_HOUR_DAYS . '
						AND ph.is_old_format <> true
						AND ph.cid = ' . ( int ) $intCid . '
						AND ph.property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND ph.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyHours( $strSql, $objDatabase );
	}

	/**
	 * @return CPropertyHour
	 */
	public static function fetchPropertyOfficeHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyHour::class, $objDatabase );
	}

	/**
	 * @return CPropertyHour[]
	 */
	public static function fetchPropertyOfficeHours( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyHour::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertyOfficeHoursWithLunchHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ph.id,
						ph.cid,
						ph.property_id,
						ph.open_time,
						ph.close_time,
						ph.day,
						ph.property_hour_type_id,
						phs.open_time AS lunch_start_time,
						phs.close_time AS lunch_end_time
					FROM
						property_hours ph
						LEFT JOIN property_hours phs on ( ph.cid = phs.cid AND ph.property_id = phs.property_id AND ph.day = phs.day - ' . CPropertyHour::LUNCH_HOUR_INTERVAL . ' AND phs.is_old_format <> true )
					WHERE
						ph.day < ' . CPropertyHour::MAX_PROPERTY_HOUR_DAYS . '
						AND ph.is_old_format <> true
						AND ph.cid = ' . ( int ) $intCid . '
						AND ph.property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND ph.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY ph.day';

		return fetchData( $strSql, $objDatabase );
	}

}
?>