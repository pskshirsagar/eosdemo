<?php
use Psi\Libraries\Container\TContainerized;

class CCustomerConciergeService extends CBaseCustomerConciergeService {

	use TContainerized;

	protected $m_objClient;
	protected $m_objProperty;

	/**
	 *
	 * Get Functions
	 */

	public function getCustomerNameFirst() {
		if( false == valStr( $this->getCustomerNameFirstEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCustomerNameFirstEncrypted(), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getCustomerNameLast() {
		if( false == valStr( $this->getCustomerNameLastEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCustomerNameLastEncrypted(), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getCustomerPhoneNumber() {
		if( false == valStr( $this->getCustomerPhoneNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCustomerPhoneNumberEncrypted(), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getCustomerEmailAddress() {
		if( false == valStr( $this->getCustomerEmailAddressEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCustomerEmailAddressEncrypted(), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getDatetimeSubmitted() {
		return $this->m_strDatetimeSubmitted;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	/**
	 *
	 * Set Functions
	 */

	public function setCustomerNameFirst( $strCustomerNameFirst ) {

		if( true == valStr( $strCustomerNameFirst ) ) {
			$this->setCustomerNameFirstEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CStrings::strTrimDef( $strCustomerNameFirst, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setCustomerNameLast( $strCustomerNameLast ) {

		if( true == valStr( $strCustomerNameLast ) ) {
			$this->setCustomerNameLastEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CStrings::strTrimDef( $strCustomerNameLast, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setCustomerPhoneNumber( $strCustomerPhoneNumber ) {

		if( true == valStr( $strCustomerPhoneNumber ) ) {
			$this->setCustomerPhoneNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CStrings::strTrimDef( $strCustomerPhoneNumber, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {

		if( true == valStr( $strCustomerEmailAddress ) ) {
			$this->setCustomerEmailAddressEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CStrings::strTrimDef( $strCustomerEmailAddress, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setDatetimeSubmitted( $strDatetimeSubmitted ) {
		$this->m_strDatetimeSubmitted = $strDatetimeSubmitted;
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = $intDeletedBy;
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = $strDeletedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['customer_name_first'] ) )		$this->setCustomerNameFirst( $arrmixValues['customer_name_first'] );
		if( isset( $arrmixValues['customer_name_last'] ) )		$this->setCustomerNameLast( $arrmixValues['customer_name_last'] );
		if( isset( $arrmixValues['customer_phone_number'] ) )	$this->setCustomerPhoneNumber( $arrmixValues['customer_phone_number'] );
		if( isset( $arrmixValues['customer_email_address'] ) )	$this->setCustomerEmailAddress( $arrmixValues['customer_email_address'] );
		if( isset( $arrmixValues['datetime_submitted'] ) )		$this->setDatetimeSubmitted( $arrmixValues['datetime_submitted'] );
		if( isset( $arrmixValues['deleted_by'] ) )				$this->setDeletedBy( $arrmixValues['deleted_by'] );
		if( isset( $arrmixValues['deleted_on'] ) )				$this->setDeletedOn( $arrmixValues['deleted_on'] );

		return;
	}

	/**
	 *
	 * Fetch Functions
	 */

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );
		return $this->m_objProperty;
	}

	/**
	 *
	 * Get or Fetch Functions
	 */

	public function getOrFetchClient( $objDatabase ) {

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient;
		} else {
			return $this->fetchClient( $objDatabase );
		}
	}

	public function getOrFetchProperty( $objDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objDatabase );
		}
	}

	/**
	 *
	 * Validation Functions
	 */

	public function valCompanyConciergeServiceId() {
		$boolIsValid = true;
		if( $this->m_intCompanyConciergeServiceId == NULL ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'company_concierge_service_id', __( 'Please select a service' ) ) );
			$boolIsValid = false;
		}

		if( $this->m_intCompanyConciergeServiceId < 1 ) {
			trigger_error( __( 'A Company Concierge Id has to be greater than zero - CCustomerConciergeService' ), E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( $this->m_intCid == NULL ) {
			trigger_error( __( 'A Client Id is required - CCustomerConciergeService' ), E_USER_ERROR );
			return false;
		}

		if( $this->m_intCid < 1 ) {
			trigger_error( __( 'A Client Id has to be greater than zero - CCustomerConciergeService' ), E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( $this->m_intPropertyId == NULL ) {
			trigger_error( __( 'A Property Id has to be greater than zero - CCustomerConciergeService' ), E_USER_ERROR );
			return false;
		}

		if( $this->m_intPropertyId < 1 ) {
			trigger_error( __( 'A Property Id has to be greater than zero - CCustomerConciergeService' ), E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerNameFirst() {
		$boolIsValid = true;
		$strCustomerNameFirst = $this->getCustomerNameFirst();

		if( false == valStr( $strCustomerNameFirst ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'customer_name_first', __( 'Please provide a first name' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerNameLast() {
		$boolIsValid = true;
		$strCustomerNameLast = $this->getCustomerNameLast();

		if( false == valStr( $strCustomerNameLast ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'customer_name_last', __( 'Please provide a last name' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerPhoneNumber( $objPhoneNumber = NULL ) {
		$boolIsValid = true;

		if( false == $objPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_phone_number', __( 'A valid phone number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerEmailAddress() {
		$boolIsValid = true;
		$strCustomerEmailAddress = $this->getCustomerEmailAddress();

		if( false == isset( $strCustomerEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', __( 'Email address is required.' ) ) );
		} elseif( false == is_null( $strCustomerEmailAddress ) ) {

			if( false == CValidation::validateEmailAddresses( $strCustomerEmailAddress ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', __( 'A valid email address is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valConciergeSpecifics() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strConciergeSpecifics ) || strlen( $this->m_strConciergeSpecifics ) <= 0 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'company_concierge_service_id', __( 'You must choose a concierge service.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDatetimeSubmitted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatetimeCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCustomerNameFirst();
				$boolIsValid &= $this->valCustomerNameLast();
				$boolIsValid &= $this->valCustomerEmailAddress();
				$boolIsValid &= $this->valConciergeSpecifics();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 *
	 * Other Functions
	 */

	public function populateCustomerInformation( $objCustomer, $strCustomerPhoneNumber = '' ) {

		$this->setCustomerId( $objCustomer->getId() );
		$this->setCustomerNameFirst( $objCustomer->getNameFirst() );
		$this->setCustomerNameLast( $objCustomer->getNameLast() );
		$this->setCustomerEmailAddress( $objCustomer->getUsername() );
		$this->setCustomerPhoneNumber( $strCustomerPhoneNumber );
	}

	public function sendConciergeServiceRequestEmails( $intCompanyConciergeServiceId, $objDatabase, $objEmailDatabase, $boolIsEmailTranslated = false ) {

		$arrstrToEmailAddresses		= [];

		$objCompanyConciergeService = CCompanyConciergeServices::fetchCompanyConciergeServiceByIdByCid( $intCompanyConciergeServiceId, $this->getCid(), $objDatabase );
		$objClient		= $this->getOrFetchClient( $objDatabase );
		$objProperty	= $this->getOrFetchProperty( $objDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation						= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
			$objConciergeServicCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'CONCIERGE_SERVICE_CUSTOM_TEXT', $objDatabase );
			$arrobjPropertyPreferences 							= $objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_CONCIERGE_NOTIFICATION_EMAIL' ],  $objDatabase );
			$arrobjPropertyPreferences 							= rekeyObjects( 'Key', $arrobjPropertyPreferences );
			if( true == $boolIsEmailTranslated ) {
				$strPropertyPreferredLocaleCode = ( ( true == valStr( $objProperty->getLocaleCode() ) ) ? $objProperty->getLocaleCode() : \CLocale::DEFAULT_LOCALE );
				CLocaleContainer::createService()->setPreferredLocaleCode( $strPropertyPreferredLocaleCode, $objDatabase, $objProperty );
			}
		}

		if( true == valObj( $objClient, 'CClient' ) ) {

			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$objSmarty->assign_by_ref( 'customer_concierge_service', $this );
		$objSmarty->assign_by_ref( 'property', $objProperty );
		$objSmarty->assign_by_ref( 'company_concierge_service', $objCompanyConciergeService );
		$objSmarty->assign_by_ref( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'concierge_service_custom_text_property_notification', $objConciergeServicCustomTextPropertyNotification );

		$objSmarty->assign( 'base_uri',					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri',		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlEmailOutput		= $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/customer_concierge_services/customer_concierge_service.tpl' );

		$objSmarty->assign( 'is_resident', true );

		if( true == $boolIsEmailTranslated ) {
			$strCustomerPreferredLocaleCode = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerPreferredLocaleCodeByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
			if( false == valStr( $strCustomerPreferredLocaleCode ) ) {
				$strCustomerPreferredLocaleCode = $strPropertyPreferredLocaleCode;

			}
			$strPreferredLocaleCode = ( ( true == valStr( $strCustomerPreferredLocaleCode ) ) ? $strCustomerPreferredLocaleCode : \CLocale::DEFAULT_LOCALE );
			CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objDatabase, $objProperty );
		}

		$strResidentHtmlEmailContent = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/customer_concierge_services/customer_concierge_service.tpl' );

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setHtmlContent( addslashes( $strHtmlEmailOutput ) );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::CONCIERGE_SERVICE );
		$objSystemEmail->setSubject( $objProperty->getPropertyName() . ' - E-concierge Service Received' );

		$objSystemEmail->setCid( $this->m_intCid );
		$objSystemEmail->setPropertyId( $this->m_intPropertyId );

		$arrstrManagerEmailAddresses		 = $objProperty->fetchConciergeServiceNotificationEmails( $objDatabase );
		$arrstrServiceProviderEmailAddresses = CEmail::createEmailArrayFromString( $objCompanyConciergeService->getEmailAddress() );

		if( false == is_null( $this->getCustomerEmailAddress() ) ) {

			if( false == is_null( $this->getCustomerNameFirst() ) && false == is_null( $this->getCustomerNameLast() ) ) {
				$arrstrToEmailAddressesCustomer[] = $this->getCustomerNameFirst() . ' ' . $this->getCustomerNameLast() . ' < ' . $this->getCustomerEmailAddress() . ' > ';
			} else {
				$arrstrToEmailAddressesCustomer[] = $this->getCustomerEmailAddress();
			}
		}

		if( true == valArr( $arrstrManagerEmailAddresses ) ) {

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrManagerEmailAddresses ) ) {
				$arrstrManagerEmailAddresses = array_unique( $arrstrManagerEmailAddresses );
			}

			foreach( $arrstrManagerEmailAddresses as $strManagerEmailAddress ) {
				$arrstrToEmailAddresses[] = $strManagerEmailAddress;
			}
		}

		if( true == isset ( $arrstrServiceProviderEmailAddresses ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrServiceProviderEmailAddresses ) ) {
			foreach( $arrstrServiceProviderEmailAddresses as $strServiceProviderManagerEmailAddress ) {
				$arrstrToEmailAddressesServiceProvider[] = $strServiceProviderManagerEmailAddress;
			}
		}

		$arrobjSystemEmails = [];

		if( true == valArr( $arrstrToEmailAddresses ) ) {

			foreach( $arrstrToEmailAddresses as $strEmailAddress ) {
				$objManagerSystemEmail = clone $objSystemEmail;
				$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );

				if( true == valArr( $arrobjPropertyPreferences ) ) {

					if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
						$objManagerSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
					}
				}
				$arrobjSystemEmails[] = $objManagerSystemEmail;
			}
		}

		foreach( $arrstrToEmailAddressesCustomer as $strEmailAddress ) {
			$objCustomerSystemEmail = clone $objSystemEmail;
			$objCustomerSystemEmail->setToEmailAddress( $strEmailAddress );

			if( 0 < strlen( $strResidentHtmlEmailContent ) ) {
				$objCustomerSystemEmail->setHtmlContent( $strResidentHtmlEmailContent );
			}

			if( true == valArr( $arrobjPropertyPreferences ) ) {

				if( true == array_key_exists( 'FROM_CONCIERGE_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() ) ) {
					$objCustomerSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() );
				}
			}

			array_push( $arrobjSystemEmails, $objCustomerSystemEmail );
		}

		foreach( $arrstrToEmailAddressesServiceProvider as $strEmailAddress ) {
			$objServiceProviderSystemEmail = clone $objSystemEmail;
			$objServiceProviderSystemEmail->setToEmailAddress( $strEmailAddress );

			if( true == valArr( $arrobjPropertyPreferences ) ) {

				if( true == array_key_exists( 'FROM_CONCIERGE_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() ) ) {
					$objServiceProviderSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() );
				}
			}

			array_push( $arrobjSystemEmails, $objServiceProviderSystemEmail );
		}

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) )	continue;
				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					continue;
				}
			}
		}

		return true;
	}

	public function sendConciergeServiceRequestCancelledEmails( $intCompanyConciergeServiceId, $objDatabase, $objEmailDatabase ) {

		$arrstrToEmailAddresses		= [];

		$objCompanyConciergeService = CCompanyConciergeServices::fetchCompanyConciergeServiceByIdByCid( $intCompanyConciergeServiceId, $this->getCid(), $objDatabase );
		$objClient		= $this->getOrFetchClient( $objDatabase );
		$objProperty	= $this->getOrFetchProperty( $objDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation						= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
			$objConciergeServicCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'CONCIERGE_SERVICE_CUSTOM_TEXT', $objDatabase );
			$arrobjPropertyPreferences 							= $objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_CONCIERGE_NOTIFICATION_EMAIL' ],  $objDatabase );
			$arrobjPropertyPreferences 							= rekeyObjects( 'Key', $arrobjPropertyPreferences );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$objSmarty->assign_by_ref( 'customer_concierge_service', $this );
		$objSmarty->assign_by_ref( 'property', $objProperty );
		$objSmarty->assign_by_ref( 'company_concierge_service', $objCompanyConciergeService );
		$objSmarty->assign_by_ref( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'concierge_service_custom_text_property_notification', $objConciergeServicCustomTextPropertyNotification );

		$objSmarty->assign( 'base_uri',					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri',		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlEmailOutput		= $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/customer_concierge_services/customer_concierge_service_cancelled_notification_email.tpl' );

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setHtmlContent( addslashes( $strHtmlEmailOutput ) );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::CONCIERGE_SERVICE );
		$objSystemEmail->setSubject( $objProperty->getPropertyName() . __( '{%s, 0}, - Concierge service request cancelled' ) );
		$objSystemEmail->setCid( $this->m_intCid );
		$objSystemEmail->setPropertyId( $this->m_intPropertyId );

		$arrstrManagerEmailAddresses		 = $objProperty->fetchConciergeServiceNotificationEmails( $objDatabase );
		$arrstrServiceProviderEmailAddresses = CEmail::createEmailArrayFromString( $objCompanyConciergeService->getEmailAddress() );

		if( true == valArr( $arrstrManagerEmailAddresses ) ) {

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrManagerEmailAddresses ) ) {
				$arrstrManagerEmailAddresses = array_unique( $arrstrManagerEmailAddresses );
			}

			foreach( $arrstrManagerEmailAddresses as $strManagerEmailAddress ) {
				$arrstrToEmailAddresses[] = $strManagerEmailAddress;
			}
		}

		if( true == isset ( $arrstrServiceProviderEmailAddresses ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrServiceProviderEmailAddresses ) ) {
			foreach( $arrstrServiceProviderEmailAddresses as $strServiceProviderManagerEmailAddress ) {
				$arrstrToEmailAddressesServiceProvider[] = $strServiceProviderManagerEmailAddress;
			}
		}

		$arrobjSystemEmails = [];

		if( true == valArr( $arrstrToEmailAddresses ) ) {

			foreach( $arrstrToEmailAddresses as $strEmailAddress ) {
				$objManagerSystemEmail = clone $objSystemEmail;
				$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );

				if( true == valArr( $arrobjPropertyPreferences ) ) {

					if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
						$objManagerSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
					}
				}
				$arrobjSystemEmails[] = $objManagerSystemEmail;
			}
		}

		foreach( $arrstrToEmailAddressesServiceProvider as $strEmailAddress ) {
			$objServiceProviderSystemEmail = clone $objSystemEmail;
			$objServiceProviderSystemEmail->setToEmailAddress( $strEmailAddress );

			if( true == valArr( $arrobjPropertyPreferences ) ) {

				if( true == array_key_exists( 'FROM_CONCIERGE_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() ) ) {
					$objServiceProviderSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_CONCIERGE_NOTIFICATION_EMAIL']->getValue() );
				}
			}

			array_push( $arrobjSystemEmails, $objServiceProviderSystemEmail );
		}

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) )	continue;
				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					continue;
				}
			}
		}

		return true;
	}

}
?>