<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRelationships
 * Do not add any new functions to this class.
 */

class CCustomerRelationships extends CBaseCustomerRelationships {

	public static function fetchCustomerRelationshipByIdByOccupancyTypeIdByPropertyIdByCid( $intId, $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase, $boolIsDefaultCustomerRelationship = false ) {

		if( false == valId( $intPropertyId ) || false == valId( $intId ) ) return NULL;

		$strDefaultCustomerRelationshipSql = ( true == $boolIsDefaultCustomerRelationship ) ? ' AND cr.default_customer_relationship_id = ' . ( int ) $intId : '';

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.id = ' . ( int ) $intId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						' . $strDefaultCustomerRelationshipSql;

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );

	}

	public static function fetchCustomerRelationshipByNameByOccupancyTypeIdByPropertyIdByCid( $strName, $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strName ) || false == valId( $intPropertyId ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.name = \'' . $strName . '\'
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND is_system = true';

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );

	}

 	public static function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false ) {

		if( false == valId( $intPropertyId ) ) return NULL;

	    $boolIsPropertyTypeMilitary = false;
		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';
		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';

	    $arrintSelectedPropertyTypeIds = CPropertyTypes::fetchPropertyTypeIdsByPropertyIdsByCid( [ $intPropertyId ], $intCid, $objDatabase );

	    if( true == valArr( $arrintSelectedPropertyTypeIds ) && CPropertyType::MILITARY == $arrintSelectedPropertyTypeIds[0]['id'] ) {
		    $boolIsPropertyTypeMilitary = true;
	    }

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = ( true == $boolIsPropertyTypeMilitary ) ? [ COccupancyType::CONVENTIONAL, COccupancyType::MILITARY ] : [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						cr.*,
						pcrg.occupancy_type_id
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
						' . $strCondition . '
					ORDER BY
						cr.customer_relationship_group_id,
						cr.is_system DESC,
						cr.customer_type_id,
						cr.id';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );

	}

	public static function fetchCustomerRelationshipsDataByOccupancyTypeIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false, $arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ] ) {

		if( true == is_null( $intPropertyId ) ) return NULL;

		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';

		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';

		$strSql = 'SELECT
					cr.*,pcrg.occupancy_type_id
				FROM
					customer_relationships cr
 					JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
			   	WHERE
					cr.cid = ' . ( int ) $intCid . '
					AND pcrg.property_id = ' . ( int ) $intPropertyId . '
					AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
					' . $strCondition . '
				ORDER BY
					cr.is_system DESC,
					pcrg.occupancy_type_id,
					cr.customer_type_id ASC';

	    $arrmixCustomerRelationships = fetchData( $strSql, $objDatabase );

	    $arrmixCustomerRelationshipsKeyedByOccupancyType = [];

		if( true == valArr( $arrmixCustomerRelationships ) ) {
			foreach( $arrmixCustomerRelationships as $arrmixCustomerRelationship ) {
				if( COccupancyType::CONVENTIONAL == $arrmixCustomerRelationship['occupancy_type_id'] ) {
					$arrmixCustomerRelationshipsKeyedByOccupancyType[COccupancyType::CONVENTIONAL][$arrmixCustomerRelationship['id']] = $arrmixCustomerRelationship;
				} elseif( COccupancyType::AFFORDABLE == $arrmixCustomerRelationship['occupancy_type_id'] ) {
					$arrmixCustomerRelationshipsKeyedByOccupancyType[COccupancyType::AFFORDABLE][$arrmixCustomerRelationship['id']] = $arrmixCustomerRelationship;
				} elseif( COccupancyType::MILITARY == $arrmixCustomerRelationship['occupancy_type_id'] ) {
					$arrmixCustomerRelationshipsKeyedByOccupancyType[COccupancyType::MILITARY][$arrmixCustomerRelationship['id']] = $arrmixCustomerRelationship;
				}
			}
		}

	    return $arrmixCustomerRelationshipsKeyedByOccupancyType;

	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCustomerTypeIdsByCid( $arrintOccupancyTypeIds, $intPropertyId, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false, $boolIsSystem = false ) {

		if( false == valId( $intPropertyId ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';
		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';
		$strCondition .= ( true == $boolIsSystem ) ? ' AND cr.is_system = true ' : '';

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						util_get_system_translated( \'name\', cr.name, cr.details ) AS name,
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
						' . $strCondition . '
					ORDER BY
						cr.is_system DESC,
						cr.customer_type_id DESC,
						cr.name ASC';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );

	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByIdsByCid( $arrintOccupancyTypeIds, $intPropertyId, $arrintIds, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false ) {

		if( false == valArr( $arrintIds ) || ( true == valArr( $arrintIds ) && false == valArr( array_filter( $arrintIds ) ) ) ) return NULL;

		$boolIsPropertyTypeMilitary = false;
		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';
		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';

		$arrintSelectedPropertyTypeIds = CPropertyTypes::fetchPropertyTypeIdsByPropertyIdsByCid( [ $intPropertyId ], $intCid, $objDatabase );

		if( true == valArr( $arrintSelectedPropertyTypeIds ) && CPropertyType::MILITARY == $arrintSelectedPropertyTypeIds[0]['id'] ) {
			$boolIsPropertyTypeMilitary = true;
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = ( true == $boolIsPropertyTypeMilitary ) ? [ COccupancyType::CONVENTIONAL, COccupancyType::MILITARY ] : [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.id IN (' . implode( ',', $arrintIds ) . ' )
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
						' . $strCondition . '
					ORDER BY
						cr.is_system DESC,
						cr.customer_type_id,
						cr.is_default,
						cr.id';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipNameByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						name
					FROM
						customer_relationships
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}

	public static function fetchCustomerRelationshipsCountByOccupancyTypeIdsByPropertyIdByCustomerTypeIdsByCid( $arrintOccupancyTypeIds, $intPropertyId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						count(cr.id)
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' ) ';

		$arrintPublishedCustomerRelationshipsCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintPublishedCustomerRelationshipsCount[0]['count'] ) ) {
			return $arrintPublishedCustomerRelationshipsCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchOrderedCustomerRelationshipsByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerRelationshipGroupId ) || false == is_numeric( $intCustomerRelationshipGroupId ) ) return NULL;

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
					WHERE
						cr.customer_relationship_group_id = ' . ( int ) $intCustomerRelationshipGroupId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						cr.is_system DESC,
						cr.customer_type_id,
						cr.id';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $intOccupancyTypeId, $intPropertyId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.id AS customer_relationship_id
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
					LIMIT 1';

		return parent::fetchColumn( $strSql, 'customer_relationship_id', $objDatabase );

	}

	public static function fetchDefaultCustomerRelationshipByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $intOccupancyTypeId, $intPropertyId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerTypeId ) || true == is_null( $intPropertyId ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.is_default = true';

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );

	}

	public static function fetchDefaultCustomerRelationshipIdByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $intOccupancyTypeId, $intPropertyId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerTypeId ) || true == is_null( $intPropertyId ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.id
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.is_default = true';

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );

	}

	public static function fetchDefaultCustomerRelationshipsByOccupancyTypIdsByPropertyIdsByCustomerTypeIdByCid( $arrintOccupancyTypeIds, $arrintPropertyIds, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerTypeId ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						cr.id,
						pcrg.property_id
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
						AND cr.is_default = true';

		$arrintCustomerRelationships = fetchData( $strSql, $objDatabase );

		foreach( $arrintCustomerRelationships as $arrintCustomerRelationship ) {
			$arrintCustomerRelationshipIds[$arrintCustomerRelationship['property_id']] = $arrintCustomerRelationship['id'];
		}

		return $arrintCustomerRelationshipIds;
	}

	public static function fetchDefaultCustomerRelationshipsByOccupancyTypeIdByPropertyIdByCidRekeyedByCustomerTypeId( $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intPropertyId ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.is_default = true';

		$arrobjDefaultCustomerRelationships = parent::fetchCustomerRelationships( $strSql, $objDatabase );
		return $arrobjDefaultCustomerRelationships;
	}

	public static function fetchSpouseCustomerRelationshipByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $intOccupancyTypeId, $intPropertyId, $intCustomerTypeId, $intCid, $objDatabase, $boolIsChildCheck = false,  $boolIsSystem = true ) {

		if( false == valStr( $intCustomerTypeId ) || true == is_null( $intPropertyId ) ) return NULL;

		if( true == $boolIsChildCheck ) {
			$strRelationshipName = 'child';
		} else {
			$strRelationshipName = 'spouse';
		}

		$strSubSQL = ( true == $boolIsSystem ) ? ' AND cr.is_system = true' : '';

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND lower( cr.name ) ILIKE E\'' . $strRelationshipName . '\'
						' . $strSubSQL . '
					LIMIT 1';

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );
	}

	public static function fetchSpouseCustomerRelationshipByRelationshipNameByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( $strRelationShipName, $intOccupancyTypeId, $intPropertyId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND lower( cr.name ) ILIKE E\'' . $strRelationShipName . '\'
					LIMIT 1';

			return parent::fetchCustomerRelationship( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintCustomerRelationshipIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return CCustomerRelationship[]
	 */
	public static function fetchCustomerRelationshipsByIdsByCid( $arrintCustomerRelationshipIds, $intCid, $objDatabase, $boolIsShowInEntrata = false ) {

		if( false == valArr( $arrintCustomerRelationshipIds ) ) return NULL;

		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND show_internally = true ' : '';

		$strSql = 'SELECT
					*
				FROM
					customer_relationships
				WHERE
					cid = ' . ( int ) $intCid . '
					' . $strCondition . '
					AND id IN (' . implode( ',', $arrintCustomerRelationshipIds ) . ' )';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase ) ?: [];
	}

    public static function fetchCustomerRelationshipsByOccupancyTypeIdByPropertyIdByIdsByCustomerTypeIdByCid( $arrintOccupancyTypeId = [], $intPropertyId, $arrintIds, $arrintCustomerTypeIds, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false ) {

		if( true == is_null( $intPropertyId ) || false == valArr( $arrintCustomerTypeIds ) || false == valArr( array_filter( $arrintIds ) ) || ( true == valArr( $arrintIds ) && false == valArr( array_filter( $arrintIds ) ) ) ) return NULL;

		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';

		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';

		$arrintOccupancyTypeId = [ COccupancyType::CONVENTIONAL, COccupancyType::AFFORDABLE, COccupancyType::MILITARY ];

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
                        cr.cid = ' . ( int ) $intCid . '
                        AND pcrg.property_id = ' . ( int ) $intPropertyId . '
                        AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeId ) . ' )
                        AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND cr.id IN (' . implode( ',', $arrintIds ) . ' )
						' . $strCondition . '
				ORDER BY
					cr.is_system DESC,
					cr.customer_type_id ASC';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );

	}

	public static function fetchCustomerTypeCountByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cr.customer_type_id,
					count(cr.customer_type_id)
				FROM
					customer_relationships cr
					WHERE
					cr.cid = ' . ( int ) $intCid . '
					AND cr.customer_relationship_group_id = ' . ( int ) $intCustomerRelationshipGroupId . '
				GROUP BY cr.customer_type_id';

		$arrintPublishedCustomerRelationshipsCount = fetchData( $strSql, $objDatabase );

		return $arrintPublishedCustomerRelationshipsCount;
	}

	public static function fetchCustomerRelationshipsByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						customer_relationships
					WHERE
						customer_relationship_group_id = ' . ( int ) $intCustomerRelationshipGroupId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						customer_type_id';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipsByCustomerRelationshipGroupIdByCidRekeyedByCustomerTypeId( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					*
				FROM
					customer_relationships
					WHERE
						customer_relationship_group_id = ' . ( int ) $intCustomerRelationshipGroupId . '
					AND cid = ' . ( int ) $intCid . '
					AND is_default = true';

		$arrobjDefaultCustomerRelationships = parent::fetchCustomerRelationships( $strSql, $objDatabase );
		$arrobjDefaultCustomerRelationships = ( array ) rekeyObjects( 'CustomerTypeId', $arrobjDefaultCustomerRelationships );

		return $arrobjDefaultCustomerRelationships;
	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdByPropertyIdByCid( $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						cr.*
					FROM
						customer_relationships cr
					    JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND cr.customer_relationship_group_id = pcrg.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.is_default = true';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCustomerRelationshipsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND cr.show_internally = true
						AND cr.show_externally = true
					ORDER BY
						cr.is_system DESC,
						pcrg.occupancy_type_id,
						cr.customer_type_id ASC';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );

	}

	public static function fetchDefaultCustomerRelationshipsByOccupancyTypeIdsByPropertyIdsByCustomerTypeIdsByCid( $arrintOccupancyTypeIds, $arrintPropertyIds, $arrintCustomerTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintOccupancyTypeIds ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						cr.id,
						pcrg.property_id,
						cr.customer_type_id
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND pcrg.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
						AND is_default = true';

		$arrintCustomerRelationships = fetchData( $strSql, $objDatabase );

		foreach( $arrintCustomerRelationships as $arrintCustomerRelationship ) {
			$arrintCustomerRelationshipIds[$arrintCustomerRelationship['property_id']][$arrintCustomerRelationship['customer_type_id']] = $arrintCustomerRelationship['id'];
		}

		return $arrintCustomerRelationshipIds;
	}

	public static function fetchCustomerRelationshipsByIdByCustomerTypeIdByCid( $intCustomerRelationshipId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerRelationshipId ) || true == is_null( $intCustomerTypeId ) ) return NULL;

		$strSql = 'SELECT
    					cr.id
    				FROM
    					customer_relationships cr
    				WHERE
    					cr.cid = ' . ( int ) $intCid . '
    					AND cr.id = ' . ( int ) $intCustomerRelationshipId . '
    					AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemCustomerRelationshipsByCustomerTypeIdByCid( $intCustomerTypeId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerTypeId ) ) return NULL;

		$strSql = 'SELECT
    					cr.id,
    					cr.name
    				FROM
    					customer_relationships cr
    				WHERE
    					cr.cid = ' . ( int ) $intCid . '
    					AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
    					AND is_system = true';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdsByCid( $arrintOccupancyTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		$strSql = 'SELECT
						pcrg.property_id
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomerRelationshipsByCustomerRelationshipGroupIdsByCustomerTypeIdByCid( $arrintCustomerRelationshipGroupId, $intCustomerTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerRelationshipGroupId ) || true == is_null( $intCustomerTypeId ) ) return NULL;

		$strSql = 'SELECT
    					cr.id,
    					cr.is_default,
    					cr.customer_relationship_group_id
    				FROM
    					customer_relationships cr
    				WHERE
    					cr.cid = ' . ( int ) $intCid . '
    					AND cr.customer_relationship_group_id IN ( ' . implode( ',', $arrintCustomerRelationshipGroupId ) . ' )
    					AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId;

		$arrintCustomerRelationships = fetchData( $strSql, $objDatabase );

		return $arrintCustomerRelationships;
	}

	public static function fetchDefaultCustomerRelationshipIdByCustomerRelationshipIdByCid( $intCustomerRelationshipId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerRelationshipId ) ) return NULL;

		$strSql = 'SELECT
						cr.default_customer_relationship_id
					FROM
						customer_relationships cr
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.id = ' . ( int ) $intCustomerRelationshipId;

		return parent::fetchColumn( $strSql, 'default_customer_relationship_id', $objDatabase );

	}

	public static function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCustomerTypeIdsByCompanyApplicationIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $arrintCustomerTypeIds, $intCompanyApplicationId, $intCid, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false ) {
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && false == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$arrintOccupancyTypeIds = [ COccupancyType::CONVENTIONAL ];
		}

		if( false == valId( $intPropertyId ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strCondition = ( true == $boolIsShowInEntrata ) ? ' AND cr.show_internally = true ' : '';
		$strCondition .= ( true == $boolIsShowInWebsite ) ? ' AND cr.show_externally = true ' : '';

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          cr.*
					      FROM
					          customer_relationships cr
					          JOIN property_applications pa ON pa.cid = cr.cid
					      WHERE
					          pa.property_id = ' . ( int ) $intPropertyId . '
					          AND pa.company_application_id = ' . ( int ) $intCompanyApplicationId . '
					          AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
					          AND pa.customer_relationships @> ARRAY [ cr.id ]
					          ' . $strCondition . '
					      UNION ALL
					      SELECT
					          cr.*
					      FROM
					          customer_relationships cr
					          JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					      WHERE
					          cr.cid = ' . ( int ) $intCid . '
					          AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
					          AND pcrg.property_id = ' . ( int ) $intPropertyId . '
					          AND pcrg.occupancy_type_id IN (' . implode( ',', $arrintOccupancyTypeIds ) . ' )
					          AND cr.customer_type_id NOT IN (
					                                           SELECT
					                                               cr3.customer_type_id
					                                           FROM
					                                               customer_relationships cr3
					                                               JOIN property_applications pa ON pa.cid = cr3.cid
					                                           WHERE
					                                               pa.property_id = ' . ( int ) $intPropertyId . '
					                                               AND pa.company_application_id = ' . ( int ) $intCompanyApplicationId . '
					                                               AND pa.customer_relationships @> ARRAY [ cr3.id ]
					          )
					          ' . $strCondition . '
					      ORDER BY
					          is_system DESC,
					          customer_type_id ASC
					    ) AS customer_relationships';

		return parent::fetchCustomerRelationships( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipIdsByNamesByCustomerTypeIdByOccupancyTypeIdByPropertyIdByCid( $arrstrNames, $intCustomerTypeId, $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrNames ) || false == valId( $intPropertyId ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.id
					FROM
						customer_relationships cr
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
				   	WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND ( cr.name LIKE \'' . $arrstrNames[0] . '\'  OR cr.name LIKE \'' . $arrstrNames[1] . '\'  OR cr.name LIKE \'' . $arrstrNames[2] . '\'  )
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND is_system = true';

		$arrintCustomerRelationships = fetchData( $strSql, $objDatabase );

		foreach( $arrintCustomerRelationships as $arrintCustomerRelationship ) {
			$arrintCustomerRelationshipIds[$arrintCustomerRelationship['id']] = $arrintCustomerRelationship['id'];
		}

		return $arrintCustomerRelationshipIds;
	}

	public static function fetchCustomerRelationshipByNameByCustomerTypeIdByOccupancyTypeIdByPropertyIdByCid( $strName, $intCustomerTypeId, $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						cr.*
					FROM 
						customer_relationships cr
						JOIN customer_relationship_groups crg ON ( crg.cid = cr.cid AND crg.id = cr.customer_relationship_group_id )
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = crg.cid AND pcrg.customer_relationship_group_id = crg.id AND pcrg.occupancy_type_id = crg.occupancy_type_id )
					WHERE 
						cr.cid = ' . ( int ) $intCid . '
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.customer_type_id = ' . ( int ) $intCustomerTypeId . '
						AND crg.is_published = true
						AND cr.name ILIKE \'' . $strName . '\'
					ORDER BY 
						pcrg.property_id,
						cr.customer_relationship_group_id, 
						cr.is_default DESC
						LIMIT 1';

		return parent::fetchCustomerRelationship( $strSql, $objDatabase );
	}

	public static function fetchActiveCustomerRelationshipsByOccupancyTypeIdByPropertyIdByCustomerTypeIdsByCid( $intOccupancyTypeId, $intPropertyId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
						cr.*
					FROM
						customer_relationships cr
 						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = cr.cid AND pcrg.customer_relationship_group_id = cr.customer_relationship_group_id )
					WHERE
						cr.cid = ' . ( int ) $intCid . '
						AND cr.customer_type_id IN (' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND cr.show_internally = true
					ORDER BY
						cr.is_system DESC,
						cr.customer_type_id ASC,
						cr.name ASC';

		return parent::fetchCustomerRelationShips( $strSql, $objDatabase );
	}

}
?>