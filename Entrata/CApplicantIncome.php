<?php

class CApplicantIncome extends CBaseApplicantIncome {

	protected $m_boolIsDelete;
	protected $m_strIncomeName;
	protected $m_boolIncomeTypeSelected;
	protected $m_intIncomeTypeCount = NULL;
	protected $m_intApplicationId;
	protected $m_intPropertyId;
	protected $m_intCurrentApplicationStepId;

	public function __construct() {
        parent::__construct();
        $this->m_boolAllowDifferentialUpdate = true;
        return;
    }

    /**
     * Get Functions
     */

	public function getIsDelete() {
		return $this->m_boolIsDelete;
    }

	public function getIncomeName() {
		return $this->m_strIncomeName;
    }

    public function getDefaultSectionName() {
    	return 'Payer\'s';
    }

    public function getMonthlyAmount() {

		$fltAmount = $this->getAmount();

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
				$fltAmount *= 30;
				break;

			case CFrequency::WEEKLY:
				$fltAmount = ( $fltAmount * 30 ) / 7;
				break;

			case CFrequency::QUARTERLY:
				$fltAmount = $fltAmount / 3;
				break;

			case CFrequency::YEARLY:
				$fltAmount = $fltAmount / 12;
				break;

			default:
				// default case
				break;
		}

		return $fltAmount;
    }

    public function getIncomeTypeSelected() {
    	return $this->m_boolIncomeTypeSelected;
    }

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getIncomeTypeCount() {
		return $this->m_intIncomeTypeCount;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCurrentApplicationStepId() {
		return $this->m_intCurrentApplicationStepId;
	}

    /**
     * Set Functions
     */

    public function setStreetLine1( $strStreetLine1 ) {
        $this->m_strStreetLine1 = ( 0 == strlen( $strStreetLine1 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) ) ) );
    }

    public function setStreetLine2( $strStreetLine2 ) {
        $this->m_strStreetLine2 = ( 0 == strlen( $strStreetLine2 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) ) ) );
    }

    public function setStreetLine3( $strStreetLine3 ) {
        $this->m_strStreetLine3 = ( 0 == strlen( $strStreetLine3 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) ) ) );
    }

    public function setCity( $strCity ) {
        $this->m_strCity = ( 0 == strlen( $strCity ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCity, 50, NULL, true ) ) ) );
    }

    public function setContactName( $strContactName ) {
        $this->m_strContactName = ( 0 == strlen( $strContactName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strContactName, 100, NULL, true ) ) ) );
    }

    public function setPayerName( $strPayerName ) {
        $this->m_strPayerName = ( 0 == strlen( $strPayerName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPayerName, 50, NULL, true ) ) ) );
    }

    public function setPayeeName( $strPayeeName ) {
        $this->m_strPayeeName = ( 0 == strlen( $strPayeeName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPayeeName, 50, NULL, true ) ) ) );
    }

    public function setPosition( $strPosition ) {
        $this->m_strPosition = ( 0 == strlen( $strPosition ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPosition, 50, NULL, true ) ) ) );
    }

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
    }

	public function setIncomeName( $strIncomeName ) {
		$this->m_strIncomeName = $strIncomeName;
    }

    public function setIncomeTypeSelected( $boolIncomeTypeSelected ) {
		$this->m_boolIncomeTypeSelected = $boolIncomeTypeSelected;
    }

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setIncomeTypeCount( $intIncomeTypeCount ) {
		$this->m_intIncomeTypeCount = $intIncomeTypeCount;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = $intCurrentApplicationStepId;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
   		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrstrValues['is_delete'] ) ) 		$this->setIsDelete( $arrstrValues['is_delete'] );
        if( true == isset( $arrstrValues['income_name'] ) ) 	$this->setIncomeName( $arrstrValues['income_name'] );
        if( true == isset( $arrstrValues['income_select'] ) )	$this->setIncomeTypeSelected( $arrstrValues['income_select'] );

        return;
    }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction, $objIncomeType, $arrobjPropertyApplicationPreferences = NULL, $arrstrIncomeTypeCustomFields = NULL, $intIsAlien = NULL, $arrobjPropertyPreferences = NULL ) {
        $boolIsValid = true;

        $objApplicantIncomeValidator = new CApplicantIncomeValidator();
        $objApplicantIncomeValidator->setApplicantIncome( $this );

        $boolIsValid &= $objApplicantIncomeValidator->validate( $strAction, $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $intIsAlien, $arrobjPropertyPreferences );

        return $boolIsValid;
    }

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		$arrstrResponse = fetchData( 'SELECT * FROM applicant_incomes WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
		$this->setSerializedOriginalValues( serialize( $arrstrResponse[0] ) );

		if( false == $boolReturnSqlOnly ) {
			$boolIsValid  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			return $boolIsValid;
		}

		$strSql  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly );
		$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return $strSql;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( false == $boolReturnSqlOnly ) {
			$boolIsValid  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_INSERT );
			$boolIsValid &= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			return $boolIsValid;
		}

		$strSql  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_INSERT );
		$strSql .= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $strSql;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( false == $boolReturnSqlOnly ) {
			$boolIsValid = $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_DELETE );
			$boolIsValid &= parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			return $boolIsValid;
		}

		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_DELETE );
		$strSql .= parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $strSql;
	}

	public function insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getApplicationId() ) ) {
			return ( false == $boolReturnSqlOnly ) ? true : NULL;
		}

		$this->unSerializeAndSetOriginalValues();

		$arrobjApplicantApplicationChanges = [];

		if( CIncomeType::CURRENT_EMPLOYER == $this->getIncomeTypeId() ) {

			if( $this->getAmount() != $this->getOriginalValueByFieldName( 'amount' ) || $this->getFrequencyId() != $this->getOriginalValueByFieldName( 'frequency_id' ) ) {
				$strOriginalValue = number_format( $this->getOriginalValueByFieldName( 'amount' ), 2, '.', ',' ) . ' ( ' . CFrequency::frequencyIdToStr( $this->getOriginalValueByFieldName( 'frequency_id' ) ) . ' ) ';
				$strNewValue = number_format( $this->getAmount(), 2, '.', ',' ) . ' ( ' . CFrequency::frequencyIdToStr( $this->getFrequencyId() ) . ' ) ';

				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::CURRENT_EMPLOYER_SALARY, $strOriginalValue, $strNewValue );
			}

			if( $this->getDateStarted() != $this->getOriginalValueByFieldName( 'date_started' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYMENT_START_DATE, $this->getOriginalValueByFieldName( 'date_started' ), $this->getDateStarted() );
			}

			if( $this->getContactName() != $this->getOriginalValueByFieldName( 'contact_name' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYER_CONTACT_NAME, $this->getOriginalValueByFieldName( 'contact_name' ), $this->getContactName() );
			}

			if( $this->getContactPhoneNumber() != $this->getOriginalValueByFieldName( 'contact_phone_number' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYER_PHONE_NUMBER, $this->getOriginalValueByFieldName( 'contact_phone_number' ), $this->getContactPhoneNumber() );
			}
		}

		$boolIsValid = true;
		$strSql 	 = '';

		if( true == valArr( $arrobjApplicantApplicationChanges ) ) {
			return CApplicantApplicationChanges::bulkInsert( $arrobjApplicantApplicationChanges, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return ( false == $boolReturnSqlOnly ) ? $boolIsValid : $strSql;
	}

	public function createApplicantApplicationChange( $intApplicantApplicationChangeTypeId, $strOldValue, $strNewValue ) {

		$objApplicantApplicationChange = new CApplicantApplicationChange();
		$objApplicantApplicationChange->setCid( $this->getCid() );
		$objApplicantApplicationChange->setApplicationId( $this->getApplicationId() );
		$objApplicantApplicationChange->setApplicantId( $this->getApplicantId() );
		$objApplicantApplicationChange->setApplicantApplicationChangeTypeId( $intApplicantApplicationChangeTypeId );
		$objApplicantApplicationChange->setOldValue( $strOldValue );
		$objApplicantApplicationChange->setNewValue( $strNewValue );

		return $objApplicantApplicationChange;
	}

	public function mapCorrespondingCustomerEmployerData( $objCustomerEmployer ) {

		$this->setCid( $objCustomerEmployer->getCid() );
		$this->setIncomeTypeId( CIncomeType::CURRENT_EMPLOYER );
		$this->setPayerName( $objCustomerEmployer->getEmployersName() );
		$this->setPayerEmail( $objCustomerEmployer->getEmployersEmail() );
		$this->setPayerPhoneNumber( $objCustomerEmployer->getEmployersPhoneNumber() );
		$this->setStreetLine1( $objCustomerEmployer->getStreetLine1() );
		$this->setStreetLine2( $objCustomerEmployer->getStreetLine2() );
		$this->setStreetLine3( $objCustomerEmployer->getStreetLine3() );
		$this->setCity( $objCustomerEmployer->getCity() );
		$this->setStateCode( $objCustomerEmployer->getStateCode() );
		$this->setProvince( $objCustomerEmployer->getProvince() );
		$this->setPostalCode( $objCustomerEmployer->getPostalCode() );
		$this->setCountryCode( $objCustomerEmployer->getCountryCode() );
		$this->setContactName( $objCustomerEmployer->getSupervisorsName() );
		$this->setContactPhoneNumber( $objCustomerEmployer->getSupervisorsPhoneNumber() );
		$this->setContactEmail( $objCustomerEmployer->getEmployersEmail() );
		$this->setPosition( $objCustomerEmployer->getPosition() );
		$this->setAmount( $objCustomerEmployer->getGrossSalary() );
		$this->setDateStarted( $objCustomerEmployer->getDateStarted() );
		$this->setDateStopped( $objCustomerEmployer->getDateTerminated() );

		return true;
	}

}
?>