<?php

class CSubsidyStudentExceptionType extends CBaseSubsidyStudentExceptionType {

	const EXCEPTION_CODE_NOT_APPLICABLE		= 'not_applicable';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExceptionCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getStudentExceptionType( $strStudentException ) {

		switch( \Psi\CStringService::singleton()->strtolower( $strStudentException ) ) {
			case 'all married':
			case 'married and entitled to file joint tax return':
				$strStudentExceptionType = 'all_married';
				break;

			case 'single parent':
			case 'single parent with dependent child':
				$strStudentExceptionType = 'single_parent';
				break;

			case 'welfare':
			case 'receiving tanf assistance':
				$strStudentExceptionType = 'welfare';
				break;

			case 'job training':
			case 'job training program':
				$strStudentExceptionType = 'job_training';
				break;

			case 'foster care':
			case 'previously in foster care':
				$strStudentExceptionType = 'foster_care';
				break;

			default:
				$strStudentExceptionType = NULL;
				break;
		}

		return $strStudentExceptionType;
	}

}
?>