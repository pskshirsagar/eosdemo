<?php

class CCompanyEmployeeReliever extends CBaseCompanyEmployeeReliever {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_intCompanyUserId;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues );
	    if( isset( $arrmixValues['name_first'] ) && $boolDirectSet ) {
		    $this->m_strNameFirst = trim( stripcslashes( $arrmixValues['name_first'] ) );
	    } elseif( isset( $arrmixValues['name_first'] ) ) {
		    $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_first'] ) : $arrmixValues['name_first'] );
	    }
	    if( isset( $arrmixValues['name_last'] ) && $boolDirectSet ) {
		    $this->m_strNameLast = trim( stripcslashes( $arrmixValues['name_last'] ) );
	    } elseif( isset( $arrmixValues['name_last'] ) ) {
		    $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_last'] ) : $arrmixValues['name_last'] );
	    }
	    if( isset( $arrmixValues['company_user_id'] ) && $boolDirectSet ) {
		    $this->m_intCompanyUserId = trim( stripcslashes( $arrmixValues['company_user_id'] ) );
	    } elseif( isset( $arrmixValues['company_user_id'] ) ) {
		    $this->setCompanyUserId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_user_id'] ) : $arrmixValues['company_user_id'] );
	    }
        return;
    }

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_reliever_id', __( 'Company employee reliever record not found.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getCompanyEmployeeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', __( 'Company Employee is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valRelieverCompanyEmployeeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getRelieverCompanyEmployeeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reliever_employee_id', __( 'Reliever is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valBeginDatetime() {

        $boolIsValid = true;

        if( true == is_null( $this->getBeginDatetime() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', __( 'From date is required.' ) ) );
        }

        if( false == is_null( $this->getBeginDatetime() ) && strtotime( $this->getBeginDatetime() ) < strtotime( date( 'Y-m-d' ) ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', __( 'From date should be greater or same as current date.' ) ) );
        }

        return $boolIsValid;
    }

    public function valEndDatetime() {

        $boolIsValid = true;

        if( true == is_null( $this->getEndDatetime() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', __( 'To date is required.' ) ) );
        }

        if( false == is_null( $this->getBeginDatetime() ) && false == is_null( $this->getEndDatetime() ) && strtotime( $this->getBeginDatetime() ) > strtotime( $this->getEndDatetime() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', __( 'To date should not be greater than From date.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCompanyEmployeeId();
            	$boolIsValid &= $this->valRelieverCompanyEmployeeId();
            	$boolIsValid &= $this->valBeginDatetime();
            	$boolIsValid &= $this->valEndDatetime();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
          	    break;

            default:
        }

        return $boolIsValid;
    }

    /**
     * Get
     */

 	public function getNameFirst() {
        return $this->m_strNameFirst;
    }

    public function getNameLast() {
    	return $this->m_strNameLast;
    }

    public function getCompanyUserId() {
    	return $this->m_intCompanyUserId;
    }

    /**
     * Set
     */

  	public function setNameFirst( $strNameFirst ) {
        $this->m_strNameFirst = CStrings::strTrimDef( $strNameFirst, 50, NULL, true );
    }

	public function setNameLast( $strNameLast ) {
        $this->m_strNameLast = CStrings::strTrimDef( $strNameLast, 50, NULL, true );
    }

    public function setCompanyUserId( $intCompanyUserId ) {
    	$this->m_intCompanyUserId = $intCompanyUserId;
    }

}
?>