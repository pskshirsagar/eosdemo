<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationSyncTypes
 * Do not add any new functions to this class.
 */

class CIntegrationSyncTypes extends CBaseIntegrationSyncTypes {

	public static function fetchIntegrationSyncTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIntegrationSyncType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIntegrationSyncType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIntegrationSyncType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

 	public static function fetchAllIntegrationSyncTypes( $objDatabase ) {
		return self::fetchIntegrationSyncTypes( 'SELECT * FROM integration_sync_types', $objDatabase );
	}
}
?>