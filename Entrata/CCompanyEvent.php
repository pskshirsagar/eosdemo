<?php

class CCompanyEvent extends CBaseCompanyEvent {

	protected $m_arrobjPropertyEvents;
	protected $m_arrintRequestedPropertyAssociations;
	protected $m_strPropertyName;
	protected $m_strLookupCode;
	protected $m_strCompanyEventType;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strCompanyMediaFileName;
	protected $m_strUserName;

	protected $m_boolIsAttendingEvent;
	protected $m_intAttendeesCount;
	protected $m_intCustomerEventId;
	protected $m_intPropertyId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAttendingEvent = false;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyEventsOnFailure() {
		$arrobjFailedPropertyEvents = [];
		if( true == valArr( $this->m_arrintRequestedPropertyAssociations ) ) {
			foreach( $this->m_arrintRequestedPropertyAssociations as $intPropertyId ) {
				$objFailedPropertyEvent = new CPropertyEvent();
				$objFailedPropertyEvent->setPropertyId( $intPropertyId );
				$objFailedPropertyEvent->setCid( $this->getCid() );
				$arrobjFailedPropertyEvents[] = $objFailedPropertyEvent;
			}
		}
		return $arrobjFailedPropertyEvents;
	}

	public function getRequestedPropertyAssociations() {
		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function getCompanyEventType() {
		return $this->m_strCompanyEventType;
	}

	public function getIsAttendingEvent() {
		return $this->m_boolIsAttendingEvent;
	}

	public function getAttendeesCount() {
		return $this->m_intAttendeesCount;
	}

	public function getCustomerEventId() {
		return $this->m_intCustomerEventId;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCompanyMediaFileName() {
		return $this->m_strCompanyMediaFileName;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	/**
	 * Set Functions
	 */

	public function setIsAttendingEvent( $boolIsAttendingEvent ) {
		$this->m_boolIsAttendingEvent = $boolIsAttendingEvent;
	}

	public function setAttendeesCount( $intAttendeesCount ) {
		$this->m_intAttendeesCount = $intAttendeesCount;
	}

	public function setCustomerEventId( $intCustomerEventId ) {
		$this->m_intCustomerEventId = $intCustomerEventId;
	}

	public function setPropertyEvents( $arrobjPropertyEvents ) {
		if( true == valArr( $arrobjPropertyEvents ) && false == empty( $arrobjPropertyEvents ) ) {
			$this->m_arrobjPropertyEvents = $arrobjPropertyEvents;
		}
	}

	public function setRequestedPropertyAssociations( $arrstrRequestProperty ) {
		$this->m_arrintRequestedPropertyAssociations = [];
		if( true == isset( $arrstrRequestProperty ) && true == valArr( $arrstrRequestProperty ) ) {
			foreach( $arrstrRequestProperty as $intPropertyId ) {
				$this->m_arrintRequestedPropertyAssociations[] = $intPropertyId;
			}
		}
		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setLookupCode( $strLookupCode ) {
		$this->m_strLookupCode = $strLookupCode;
	}

	public function setCompanyEventType( $strCompanyEventType ) {
		$this->m_strCompanyEventType = $strCompanyEventType;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst 	= $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast 	= $strNameLast;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId 	= $intPropertyId;
	}

	public function setCompanyMediaFileName( $strCompanyMediaFileName ) {
		$this->m_strCompanyMediaFileName = $strCompanyMediaFileName;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	 public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['property_name'] ) ) 			$this->setPropertyName( $arrstrValues['property_name'] );
		if( true == isset( $arrstrValues['lookup_code'] ) ) 			$this->setLookupCode( $arrstrValues['lookup_code'] );
		if( true == isset( $arrstrValues['company_event_type'] ) ) 		$this->setCompanyEventType( $arrstrValues['company_event_type'] );
		if( true == isset( $arrstrValues['is_attending_event'] ) ) 		$this->setIsAttendingEvent( $arrstrValues['is_attending_event'] );
		if( true == isset( $arrstrValues['attendees_count'] ) ) 		$this->setAttendeesCount( $arrstrValues['attendees_count'] );
		if( true == isset( $arrstrValues['customer_event_id'] ) ) 		$this->setCustomerEventId( $arrstrValues['customer_event_id'] );
		if( true == isset( $arrstrValues['name_first'] ) )				$this->setNameFirst( $arrstrValues['name_first'] );
		if( true == isset( $arrstrValues['name_last'] ) )				$this->setNameLast( $arrstrValues['name_last'] );
		if( true == isset( $arrstrValues['property_id'] ) )				$this->setPropertyId( $arrstrValues['property_id'] );
		if( true == isset( $arrstrValues['company_media_file_name'] ) )	$this->setCompanyMediaFileName( $arrstrValues['company_media_file_name'] );
		if( true == isset( $arrstrValues['username'] ) )				$this->setUserName( $arrstrValues['username'] );
	 }

	/**
	 * Create Functions
	 */

	public function createCompanyEventResponse() {
		$objCompanyEventResponse = new CCompanyEventResponse();
		$objCompanyEventResponse->setCid( $this->getCid() );
		$objCompanyEventResponse->setResponseDatetime( 'NOW()' );

		return $objCompanyEventResponse;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyEvents( $objDatabase ) {
		return CPropertyEvents::fetchPropertyEventsByCompanyEventIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEventResponsesByPropertyId( $intPropertyId, $objDatabase, $intApproved = 0 ) {
		return CCompanyEventResponses::fetchCompanyEventResponsesByCompanyEventIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->m_intCid, $objDatabase, $intApproved );
	}

	public function fetchCompanyEventResponseByIdByPropertyId( $intCompanyEventResponseId, $intPropertyId, $objDatabase ) {
		return CCompanyEventResponses::fetchCompanyEventResponseByIdByCompanyEventIdByPropertyIdByCid( $intCompanyEventResponseId, $this->getId(), $intPropertyId, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerEvents( $objDatabase ) {
		return CCustomerEvents::fetchCustomerEventsByCompanyEventIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerEventByCustomerId( $intCustomerId, $objDatabase ) {
		return CCustomerEvents::fetchCustomerEventByCompanyEventIdByCustomerIdByCid( $this->getId(), $intCustomerId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaType( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByCompanyEventIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchEventClubMemberIds( $intPropertyId, $objDatabase ) {
		return CCustomerClubs::fetchSimpleCustomerClubByClubIdByCidByFieldNamesByPropertyIds( $this->getClubId(), $this->getCid(), [ 'lc.customer_id' ], [ $intPropertyId ], $objDatabase );
	}

	public function fetchCalendarEventByPropertyId( $intPropertyEventId, $objDatabase ) {
		$arrobjCalendarEventTypes = ( array ) \Psi\Eos\Entrata\CCalendarEventTypes::createService()->fetchCalendarEventTypesByDefaultCalendarEventTypeIdByCid( \CDefaultCalendarEventType::COMMUNITY, $this->getCid(), $objDatabase );

		$objCalendarEvent = \Psi\Eos\Entrata\CCalendarEvents::createService()->fetchCalendarEventByCalendarEventTypeByReferenceIdByCid( current( array_keys( $arrobjCalendarEventTypes ) ), $intPropertyEventId, $this->getCid(), $objDatabase );

		return $objCalendarEvent;
	}

	public function fetchAttendeesByCid( $intPropertyId, $intCid, $objDatabase, $boolIsResidentPortal = false ) {
	 $strSql = '
					SELECT
						c.*, ce.id as customer_event_id ';
		if( true == $boolIsResidentPortal ) {
			$strSql .= ', cs.value as is_opt_out_of_apartmates ';
		}
		$strSql .= 'FROM
						lease_customers as lc
						JOIN leases as l ON l.id = lc.lease_id AND lc.cid = l.cid
						JOIN customers as c ON c.id = lc.customer_id AND c.cid = lc.cid';

		if( true == $boolIsResidentPortal ) {
			$strSql .= ' LEFT JOIN customer_settings cs ON c.id = cs.customer_id AND cs.key = \'opt_out_of_apartmates\' AND c.cid =
	   cs.cid';
		}

		$strSql .= '
						LEFT JOIN customer_events ce ON c.id = ce.customer_id AND c.cid =
	   ce.cid AND ce.company_event_id = ' . ( int ) $this->getId() . '
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND	l.property_id = ' . ( int ) $intPropertyId . '
						AND ( c.id IN ( ce.customer_id ) OR c.id = ' . ( int ) $this->getCustomerId() . ' )';
		$strSql .= '
					ORDER BY
						lc.lease_status_type_id';

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomers( $strSql, $objDatabase );
	}

	public function fetchAttendeesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsResidentPortal = false, $objCustomerCommunityFilter = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereCondition	= NULL;

		if( true == valObj( $objCustomerCommunityFilter, 'CCustomerCommunityFilter' ) && true == valStr( $objCustomerCommunityFilter->getResidentName() ) ) {
			$arrstrExplodedResidentNames = explode( ' ', trim( $objCustomerCommunityFilter->getResidentName() ) );

			if( true == valArr( $arrstrExplodedResidentNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrExplodedResidentNames ) ) {
				$arrstrOrParameters = [];
				foreach( $arrstrExplodedResidentNames as $strExplodedResidentName ) {
					if( true === strrchr( $strExplodedResidentName, '\'' ) ) {
						$strFilteredExplodedResidentName = $strExplodedResidentName;
					} else {
						$strFilteredExplodedResidentName = '\' || \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strExplodedResidentName ) ) . '\' || \'';
					}
					array_push( $arrstrOrParameters, $strFilteredExplodedResidentName );
				}

				$arrstrOrParameters = array_filter( $arrstrOrParameters );

				if( true == valArr( $arrstrOrParameters ) ) {
					$strWhereCondition = 'AND ( c.name_first ILIKE E\'%' . implode( '%\' OR c.name_first ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
					$strWhereCondition .= ' AND ( c.name_last ILIKE E\'%' . implode( '%\' OR c.name_last ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
				}
			} else {
				$strWhereCondition = 'AND ( lower( c.name_first ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' OR lower( c.name_last ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' )';
			}
		}

	 $strSql = '
					SELECT
						c.*, ce.id as customer_event_id, ce.created_on as customer_event_created_on,
						cpn.phone_number ';
		if( true == $boolIsResidentPortal ) {
			$strSql .= ', cs.value as is_opt_out_of_apartmates ';
		}
		$strSql .= 'FROM
						lease_customers as lc
						JOIN leases as l ON l.id = lc.lease_id AND lc.cid = l.cid
						JOIN customers as c ON c.id = lc.customer_id AND c.cid = lc.cid
						LEFT JOIN customer_phone_numbers cpn ON c.id = cpn.customer_id AND c.cid = cpn.cid AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL AND cpn.deleted_on IS NULL';

		if( true == $boolIsResidentPortal ) {
			$strSql .= ' LEFT JOIN customer_settings cs ON c.id = cs.customer_id AND cs.key = \'opt_out_of_apartmates\' AND c.cid =
	   cs.cid';
		}

		$strSql .= '
						LEFT JOIN customer_events ce ON c.id = ce.customer_id AND c.cid =
	   ce.cid AND ce.company_event_id = ' . ( int ) $this->getId() . '
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND	l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( c.id IN ( ce.customer_id ) OR c.id = ' . ( int ) $this->getCustomerId() . ' )' . $strWhereCondition;
		$strSql .= '
					ORDER BY
						lc.lease_status_type_id';

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomers( $strSql, $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$objCompanyEventValidator = new CCompanyEventValidator();

		$objCompanyEventValidator->setobjCompanyEvent( $this );

		return $objCompanyEventValidator->validate( $strAction );
	}

	/**
	 * Other Functions
	 */

	public function addRequestedPropertyAssociation( $intPropertyId ) {
		if( true == is_numeric( $intPropertyId ) && 0 < $intPropertyId ) {
			$this->m_arrintRequestedPropertyAssociations[] = $intPropertyId;
		}
	}

	public function addPropertyEvent( $objPropertyEvent ) {
		if( true == valObj( $objPropertyEvent, 'CPropertyEvent' ) ) {
			$this->m_arrobjPropertyEvents[$objPropertyEvent->getId()] = $objPropertyEvent;
		}
	}

	public function addNewPropertyEvents( $objDatabase, $intCompanyUserId ) {
		if( false == valArr( $this->m_arrintRequestedPropertyAssociations ) || true == empty( $this->m_arrintRequestedPropertyAssociations ) ) {
			trigger_error( 'Invalid Company Event Request: Attempt made to add property events without associations - CCompanyEvent::addNewPropertyEvents()', E_USER_ERROR );
		}

		foreach( $this->m_arrintRequestedPropertyAssociations as $intPropertyId ) {
			$objNewPropertyEvent = new CPropertyEvent();
			$objNewPropertyEvent->setCompanyEventId( $this->getId() );
			$objNewPropertyEvent->setPropertyId( $intPropertyId );
			$objNewPropertyEvent->setCid( $this->getCid() );

			if( false == $objNewPropertyEvent->insertOrUpdate( $objDatabase, $intCompanyUserId ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to associate all properties.' ) );
				return false;
			}
			$this->addPropertyEvent( $objNewPropertyEvent );
		}
		return true;
	}

	public function convertTime12To24( $strTime, $boolIsFromResidentPortal = true ) {

		$arrmixTime = explode( ':', $strTime );

		if( true == $boolIsFromResidentPortal ) {
			$arrmixAMPMTime = explode( ' ', $arrmixTime[1] );
		} else {
			$arrmixAMPMTime[0] = \Psi\CStringService::singleton()->substr( $arrmixTime[1], 0, 2 );
			$arrmixAMPMTime[1] = ( 'AM' == \Psi\CStringService::singleton()->strtoupper( trim( \Psi\CStringService::singleton()->substr( $arrmixTime[1], 2, 4 ) ) ) ) ? 'AM' : 'PM';
		}

		$intHour	= 0;
		$intMinute	= 0;

		if( true == valArr( $arrmixTime ) && false == valArr( $arrmixAMPMTime ) ) {

			$intMinute	= $arrmixTime[1];
			$intHour	= $arrmixTime[0];

			if( 'PM' == $arrmixTime[2] ) {

				if( $intHour != 12 ) {

					$intHour = 12 + $intHour;
				} elseif( $intMinute == 0 ) {

					$intHour = 12;
				}
			} elseif( 12 == $intHour ) {
					$intHour = 00;
			}
		} else {
			$intHour	= $arrmixTime[0];
			$intMinute	= $arrmixAMPMTime[0];

			if( 'PM' == $arrmixAMPMTime[1] ) {

				if( $intHour != 12 ) {
					$intHour = 12 + $intHour;
				} elseif( $intMinute == 0 ) {
					$intHour = 12;
				}
			} elseif( 12 == $intHour ) {
					$intHour = 00;
			}
		}
		$arrmixTime[0]	= $intHour;
		$arrmixTime[1]	= $intMinute;

		return $arrmixTime;
	}

	public function getTimeZoneName() {
		if( true == valId( $this->m_intPropertyId ) ) {
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $this->m_objDatabase );
			if( false == valObj( $objProperty, 'CProperty' ) ) {
				echo '<p class="alert error slim"><i class="action-alert"></i>A problem occurred. Please try again</p>';
				exit;
			}

			$objTimeZone = CTimeZones::fetchTimeZoneByPropertyIdByCid( $this->m_intPropertyId, $this->m_intCid, $this->m_objDatabase );
			$strPropertyTimezone = ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : NULL;
		} else {
			$strPropertyTimezone = CTimeZone::DEFAULT_TIME_ZONE_NAME;
		}
		return $strPropertyTimezone;
	}

}
?>