<?php

class CDataExportFrequencyType extends CBaseDataExportFrequencyType {

	const SCHEDULED_EXPORT_FREQUENCY_TYPE_DAILY 	= 1;
	const SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKLY 	= 2;
	const SCHEDULED_EXPORT_FREQUENCY_TYPE_MONTHLY 	= 3;
	const SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKDAY 	= 4;
	const SCHEDULED_EXPORT_FREQUENCY_TYPE_YEARLY 	= 5;
	const SCHEDULED_EXPORT_FREQUENCY_TYPE_ONCE	 	= 6;

	public static $c_arrintRecurringDataExportFrequencyTypes		= [ self::SCHEDULED_EXPORT_FREQUENCY_TYPE_DAILY, self::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKLY, self::SCHEDULED_EXPORT_FREQUENCY_TYPE_MONTHLY, self::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKDAY, self::SCHEDULED_EXPORT_FREQUENCY_TYPE_YEARLY ];

	public static $c_arrstrAllExportFrequencyTypes = [
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_DAILY		=> 'Daily',
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKLY	=> 'Weekly',
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_MONTHLY	=> 'Monthly',
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKDAY	=> 'Weekday',
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_YEARLY	=> 'Yearly',
		self::SCHEDULED_EXPORT_FREQUENCY_TYPE_ONCE		=> 'Once'
	];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }

}
?>