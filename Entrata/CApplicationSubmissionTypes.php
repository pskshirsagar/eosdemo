<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSubmissionTypes
 * Do not add any new functions to this class.
 */

class CApplicationSubmissionTypes extends CBaseApplicationSubmissionTypes {

	public static function fetchApplicationSubmissionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationSubmissionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationSubmissionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationSubmissionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>