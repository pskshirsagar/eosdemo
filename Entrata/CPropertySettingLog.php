<?php

class CPropertySettingLog extends CBasePropertySettingLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertySettingKeyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAction() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewText() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public static function createCustomPropertySettingLog( $objProperty, $intCurrentUserId, $arrstrPropertySettingLogs, $arrmixNewPropertySettings, $arrmixOldPropertySettings = NULL, $arrobjPropertySettingKeys = NULL ) {
		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$arrobjPropertySettingLogs  = [];
		$strPropertyLogAction 		= 'INSERT';
		$intPropertyId 				= $objProperty->getId();
		$intCid 					= $objProperty->getCid();
		$objClientDatabase 			= $objProperty->getDatabase();

		if( true == valArr( $arrstrPropertySettingLogs ) ) {
			if( false == valArr( $arrobjPropertySettingKeys ) ) {
				$arrobjPropertySettingKeys = \Psi\Eos\Entrata\CPropertySettingkeys::createService()->fetchPropertySettingKeysByKeys( array_keys( $arrstrPropertySettingLogs ), $objClientDatabase );
			}

			foreach( $arrstrPropertySettingLogs as $strKey => $arrstrKeyInfo ) {
				$strOldKeyValue				= '';
				$strKeyValue 				= '';
				$strNewKeyText 				= '';

				$boolIsSetOldPropertySetting = ( true == valArr( $arrmixOldPropertySettings ) && true == isset( $arrmixOldPropertySettings[$strKey]['value'] ) ) ? true : false;
				$boolIsSetLogPropertySetting = ( true == valArr( $arrstrPropertySettingLogs ) && true == isset( $arrstrPropertySettingLogs[$strKey]['value'] ) ) ? true : false;
				$boolIsSetNewPropertySetting = ( true == valArr( $arrmixNewPropertySettings ) && true == isset( $arrmixNewPropertySettings[$strKey]['value'] ) ) ? true : false;

				if( true == $boolIsSetOldPropertySetting ) {
					$strOldKeyValue = $arrmixOldPropertySettings[$strKey]['value'];

				}

				if( true == $boolIsSetLogPropertySetting && true == $boolIsSetNewPropertySetting ) {
					$strNewKeyText = $arrstrPropertySettingLogs[$strKey]['value'];
				} elseif( true == $boolIsSetNewPropertySetting ) {
					$strNewKeyText = $arrmixNewPropertySettings[$strKey]['value'];
				}

				if( true == $boolIsSetNewPropertySetting ) {
					$strKeyValue = $arrmixNewPropertySettings[$strKey]['value'];
				} elseif( true == $boolIsSetLogPropertySetting ) {
					$strKeyValue = $arrstrPropertySettingLogs[$strKey]['value'];
				}

				$intPropertySettingKeyId = $arrobjPropertySettingKeys[$strKey]->getId();

				if( trim( $strOldKeyValue ) != trim( $strKeyValue ) ) {
					if( true == $boolIsSetOldPropertySetting ) {
						$strPropertyLogAction = 'UPDATE';
					}

					$objPropertySettingLog = new CPropertySettingLog();
					$objPropertySettingLog->setCid( $intCid );
					$objPropertySettingLog->setPropertyId( $intPropertyId );
					$objPropertySettingLog->setPropertySettingKeyId( $intPropertySettingKeyId );
					$objPropertySettingLog->setAction( $strPropertyLogAction );
					$objPropertySettingLog->setNewValue( $strKeyValue );
					$objPropertySettingLog->setNewText( $strNewKeyText );
					$arrobjPropertySettingLogs[] = $objPropertySettingLog;
				}
			}

			if( true == valArr( $arrobjPropertySettingLogs ) && false == \Psi\Eos\Entrata\CPropertySettingLogs::createService()->bulkInsert( $arrobjPropertySettingLogs, $intCurrentUserId, $objClientDatabase ) ) {
				return false;
			}
		}
		return true;
	}

}
?>