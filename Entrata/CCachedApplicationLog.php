<?php

class CCachedApplicationLog extends CBaseCachedApplicationLog {

	const IS_IGNORED_FALSE = 'false';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPrimaryApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectivePeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOriginalPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeadSourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInternetListingServiceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeasingAgentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldApplicationStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationStepId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLateFeeFormulaId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRenewalOfferId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyContactResultId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCraigslistPostId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClAdId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTrafficCookieId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCancellationListTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCancellationListItemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseTermId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumberCache() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameMiddle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPrimaryPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredRentMin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredRentMax() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseStartDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseEndDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredBedrooms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredBathrooms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredPets() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDesiredOccupants() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTermMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsOpeningLog() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPostMonthIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEffectiveDateIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMuteFollowupsUntil() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBlueMoonApplicationSentOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningApprovedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationApprovedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationApprovedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseGeneratedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseCompletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseApprovedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseApprovedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCancelledOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

    	if( CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::APPROVED == $this->getApplicationStatusId() && 11 == $this->getCreatedBy() ) {
    		trigger_error( 'Lease approved record updated from Prospect Portal. Please report the issue to Chirayu Tailor', E_USER_WARNING );
    	}
    }

}
?>