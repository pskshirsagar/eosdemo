<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgets
 * Do not add any new functions to this class.
 */

class CBudgets extends CBaseBudgets {

	public static function fetchPaginatedBudgetsByCid( $intCid, $objPagination, $objClientDatabase, $objBudgetsFilter = NULL ) {

		$strCondition	= '';
		$strOrderBy		= 'b.updated_on DESC, b.id DESC';
		$arrstrSortBy	= [ 'property_name', 'name', 'fiscal_year', 'start_date', 'end_date', 'amount' ];

		if( true == valStr( $objBudgetsFilter->getSortBy() ) && true == in_array( $objBudgetsFilter->getSortBy(), $arrstrSortBy ) ) {
			$strOrderBy = $objBudgetsFilter->getSortBy() . ' ' . $objBudgetsFilter->getSortDirection();
		}

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) ) {

			if( true == valStr( $objBudgetsFilter->getBudgetName() ) ) {
				$strCondition .= ' AND b.name ILIKE \'%' . addslashes( $objBudgetsFilter->getBudgetName() ) . '%\' ';
			}

			if( true == valArr( $objBudgetsFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND b.property_id IN ( 
															SELECT 
																pga.property_id 
															FROM 
																properties AS p
																JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
															WHERE 
																p.cid = ' . ( int ) $intCid . '
																AND pg.id IN ( ' . implode( ',', $objBudgetsFilter->getPropertyIds() ) . ' )
																AND pg.deleted_by IS NULL
																AND pg.deleted_on IS NULL
														)';
			}

			if( true == valArr( $objBudgetsFilter->getFiscalYears() ) ) {
				$strCondition .= ' AND b.fiscal_year IN ( ' . implode( ',', $objBudgetsFilter->getFiscalYears() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetStatusTypeIds() ) ) {
				$strCondition .= ' AND b.budget_status_type_id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetStatusTypeIds() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetIds() ) ) {
				$strCondition .= ' AND b.id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetIds() ) . ' )';
			}
		}

		$strSql = 'SELECT
						p.property_name,
						p.id AS property_id,
						b.id,
						b.budget_status_type_id,
						b.start_date,
						b.end_date,
						b.fiscal_year,
						b.name,
						b.created_on,
						b.updated_on,
						b.is_default,
						COALESCE ( SUM( bgam.amount ), 0 ) AS amount
					FROM
						properties p
						JOIN budgets b ON ( p.cid = b.cid AND p.id = b.property_id )
						LEFT JOIN budget_gl_account_months bgam ON ( b.cid = bgam.cid AND b.id = bgam.budget_id )
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND b.deleted_on IS NULL ' .
						$strCondition . '
					GROUP BY
						p.property_name,
						p.id,
						b.id,
						b.budget_status_type_id,
						b.fiscal_year,
						b.start_date,
						b.end_date,
						b.name,
						b.created_on,
						b.updated_on,
						b.is_default
					ORDER BY ' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetsCountByCid( $intCid, $objClientDatabase, $objBudgetsFilter = NULL ) {

		$strCondition = '';

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) ) {

			if( true == valStr( $objBudgetsFilter->getBudgetName() ) ) {
				$strCondition .= ' AND b.name ILIKE \'%' . addslashes( $objBudgetsFilter->getBudgetName() ) . '%\' ';
			}

			if( true == valArr( $objBudgetsFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND b.property_id IN ( 
															SELECT 
																pga.property_id 
															FROM 
																properties AS p
																JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
															WHERE 
																p.cid = ' . ( int ) $intCid . '
																AND pg.id IN ( ' . implode( ',', $objBudgetsFilter->getPropertyIds() ) . ' )
																AND pg.deleted_by IS NULL
																AND pg.deleted_on IS NULL
														)';
			}

			if( true == valArr( $objBudgetsFilter->getFiscalYears() ) ) {
				$strCondition .= ' AND b.fiscal_year IN ( ' . implode( ',', $objBudgetsFilter->getFiscalYears() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetStatusTypeIds() ) ) {
				$strCondition .= ' AND b.budget_status_type_id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetStatusTypeIds() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetIds() ) ) {
				$strCondition .= ' AND b.id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetIds() ) . ' )';
			}
		}

		$strSql = 'SELECT
						COUNT( DISTINCT( b.id ) )
					FROM
						properties p
						JOIN budgets b ON ( p.cid = b.cid AND p.id = b.property_id )
						-- LEFT JOIN budget_gl_account_months bgam ON ( b.cid = bgam.cid AND b.id = bgam.budget_id ) -- do we need this join
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND b.deleted_on IS NULL 
						AND ( b.is_advanced_budget IS FALSE OR( b.is_advanced_budget IS TRUE AND b.completed_datetime IS NOT NULL  ) ) ' .
						$strCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchBudgetByIdByCid( $intBudgetId, $intCid, $objClientDatabase ) {
		if( false == valId( $intBudgetId ) ) return NULL;

		$strSql = 'SELECT
						p.property_name,
						b.*
					FROM
						budgets b
						JOIN properties p ON ( b.cid = p.cid AND b.property_id = p.id )
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND b.id = ' . ( int ) $intBudgetId;

		return self::fetchBudget( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetsByIdsByCid( $arrintBudgetIds, $intCid, $objClientDatabase ) {
		$strSql = ' SELECT
						b.*
					FROM
						budgets b
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND	b.id IN ( ' . implode( ',', $arrintBudgetIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetsByPropertyIdByBudgetStatusTypeIdByCid( $intPropertyId, $intBudgetStatusTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.property_name,
						b.*
					FROM
						budgets b
						JOIN properties p ON ( b.cid = p.cid AND b.property_id = p.id )
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND	b.property_id = ' . ( int ) $intPropertyId . '
						AND	b.budget_status_type_id = ' . ( int ) $intBudgetStatusTypeId;

		return self::fetchBudgets( $strSql, $objClientDatabase );
	}

	public static function fetchFiscalYearByCid( $intCid, $objClientDatabase ) {

		$strSql = '	SELECT
						DISTINCT fiscal_year AS name
					FROM
						budgets
					WHERE
						cid = ' . ( int ) $intCid . '
					ORDER BY
						name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetsByPropertyIdsByBudgetStatusTypeIdByCid( $arrintPropertyIds = [], $arrintBudgetStatusTypeIds = [], $boolIsBudgetStatusDefault = false, $intCid, $objClientDatabase ) {

		if( ( ( false == valArr( $arrintBudgetStatusTypeIds ) ) && false == $boolIsBudgetStatusDefault ) || false == valArr( $arrintPropertyIds ) )	return NULL;

			$strSql = ' SELECT
							p.id as property_id,
							p.property_name,
							b.id,
							b.name,
							b.is_default
						FROM
							budgets b
							JOIN properties p ON ( b.cid = p.cid AND b.property_id = p.id )
							JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ']::INTEGER[] ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 )
						WHERE
							b.cid = ' . ( int ) $intCid . ' ';
			if( false == $boolIsBudgetStatusDefault && true == valArr( $arrintBudgetStatusTypeIds ) ) {
				$strSql .= ' AND b.budget_status_type_id IN( ' . implode( ',', $arrintBudgetStatusTypeIds ) . ')';
			} else {
				$strSql .= ' AND b.is_default = 1 ';
			}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetsByPropertyIdsByBudgetIdsByCid( $arrintPropertyIds = [], $arrintBudgetIds = [], $intCid, $objClientDatabase ) {

	    if( false == valArr( $arrintBudgetIds ) || false == valArr( $arrintPropertyIds ) )	return NULL;

	    $strSql = ' SELECT
							p.id as property_id,
							p.property_name,
							b.id,
							b.name,
							b.is_default
						FROM
							budgets b
							JOIN properties p ON ( b.cid = p.cid AND b.property_id = p.id )
						WHERE
							b.cid = ' . ( int ) $intCid . '
							AND	b.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND b.id IN( ' . implode( ',', $arrintBudgetIds ) . ')';

	    return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultBudgetsByPropertyIdByFiscalYearByCid( $intPropertyId, $intFiscalYear, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) )	return NULL;

		$strSql = 'SELECT
						b.*
					FROM
						budgets b
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND	b.property_id = ' . ( int ) $intPropertyId . '
						AND b.is_default = 1 
						AND b.fiscal_year = ' . ( int ) $intFiscalYear;

		return self::fetchBudgets( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetIdsByPropertyIdsByBudgetStatusTypeIdByFiscalYearByCid( $arrintPropertyGroupIds = [], $boolIsDefaultBudget = false, $arrintBudgetStatusTypeIds = [], $arrintBudgetIds = [], $intFiscalYear, $intCid, $objClientDatabase, $strPropertyBudgetIdAlias ) {

		if( ( ( false == valArr( $arrintBudgetStatusTypeIds ) && false == valArr( $arrintBudgetIds ) ) && false == $intFiscalYear ) || false == valArr( $arrintPropertyGroupIds ) )	return NULL;

		$strPropertyBudgetIdAlias = ( true == valStr( $strPropertyBudgetIdAlias ) ) ? $strPropertyBudgetIdAlias : 'budget_id';

		$strSql = '	SELECT
						b.id
					FROM
						budgets b
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ']::INTEGER[] ) lp ON ( b.cid = lp.cid AND b.property_id = lp.property_id AND lp.is_disabled = 0 )
					WHERE
						b.cid = ' . ( int ) $intCid . ' ';
		if( true == valArr( $arrintBudgetStatusTypeIds ) ) {
			if( true == $boolIsDefaultBudget ) {
				$strSql .= ' AND b.is_default = 1 ';
			} else {
				$strSql .= ' AND b.budget_status_type_id IN ( ' . implode( ', ', $arrintBudgetStatusTypeIds ) . ' )';
			}
		} else {
			$strSql .= ' AND b.is_default = 1 ';
		}
		$strSql .= ' AND b.fiscal_year = ' . ( int ) $intFiscalYear .
				' AND b.id IN ( ' . implode( ', ', $arrintBudgetIds ) . ' ) ';
		$strSql .= ' ORDER BY
						b.name';
		$arrintBudgetIdsData = ( array ) fetchData( $strSql, $objClientDatabase );
		if( true == valArr( $arrintBudgetIdsData ) ) {
			$intBudgetIdData = call_user_func_array( 'array_merge', $arrintBudgetIdsData )['id'];
			return ( int ) $intBudgetIdData . ' AS ' . $strPropertyBudgetIdAlias;
		}
		return;
	}

	public static function createTempTableBudgetsByPropertyIdsByCid( $intCid, $objClientDatabase, $objBudgetsFilter = NULL, $boolIsViewConfidentialAdvancedBudgetTab = false ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_budgets_listing\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strJoin		 = '';
		$strCondition	 = '';
		$strSubCondition = '';
		$strOrderBy		 = 'b.updated_on DESC, b.id DESC';
		$arrstrSortBy	 = [ 'property_name', 'name', 'fiscal_year', 'start_date', 'end_date', 'amount', 'is_advanced_budget', 'workbook_name' ];

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) ) {

			if( true == valStr( $objBudgetsFilter->getSortBy() ) && true == in_array( $objBudgetsFilter->getSortBy(), $arrstrSortBy ) ) {
				$strOrderBy = $objBudgetsFilter->getSortBy() . ' ' . $objBudgetsFilter->getSortDirection() . ' NULLS LAST ';
			}

			if( true == valStr( $objBudgetsFilter->getBudgetName() ) ) {
				$strCondition .= ' AND b.name ILIKE \'%' . addslashes( $objBudgetsFilter->getBudgetName() ) . '%\' ';
			}

			if( true == valArr( $objBudgetsFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND b.property_id IN ( 
															SELECT 
																pga.property_id 
															FROM 
																properties AS p
																JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
															WHERE 
																p.cid = ' . ( int ) $intCid . '
																AND pg.id IN ( ' . implode( ',', $objBudgetsFilter->getPropertyIds() ) . ' ) 
																AND pg.deleted_by IS NULL
																AND pg.deleted_on IS NULL
														)';
			}

			if( true == valArr( $objBudgetsFilter->getFiscalYears() ) ) {
				$strCondition .= ' AND b.fiscal_year IN ( ' . implode( ',', $objBudgetsFilter->getFiscalYears() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetStatusTypeIds() ) ) {
				$strCondition .= ' AND b.budget_status_type_id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetStatusTypeIds() ) . ' )';
			}

			if( true == valArr( $objBudgetsFilter->getBudgetIds() ) ) {
				$strCondition .= ' AND b.id IN ( ' . implode( ',', $objBudgetsFilter->getBudgetIds() ) . ' )';
			}
		}

		$strWorkbookName = ' NULL ';

		if( true == $boolIsViewConfidentialAdvancedBudgetTab ) {
			$strSubCondition .= ' OR ( b.is_advanced_budget IS TRUE AND b.completed_datetime IS NOT NULL AND pp.id IS NOT NULL )';
			$strJoin = ' LEFT JOIN budget_workbooks bw ON ( bw.cid = b.cid AND bw.id = b.budget_workbook_id ) 
						 LEFT JOIN property_products pp ON ( pp.cid = b.cid AND pp.property_id = b.property_id AND pp.ps_product_id = ' . CPsProduct::ADVANCED_BUDGETING . ' )';
			$strWorkbookName = ' COALESCE( bw.name, NULL ) ';
		}

		$strSql = 'DROP TABLE IF EXISTS pg_temp.temp_budgets_listing;
				CREATE TEMP TABLE temp_budgets_listing AS (
					SELECT
						p.property_name,
						p.id AS property_id,
						b.id,
						b.budget_status_type_id,
						b.start_date,
						b.end_date,
						b.fiscal_year,
						b.name,
						b.created_on,
						b.updated_on,
						b.is_default,
						COALESCE ( SUM( bgam.amount ), 0 ) AS amount,
						b.is_advanced_budget,
						' . $strWorkbookName . ' AS workbook_name
					FROM
						properties p
						JOIN budgets b ON ( p.cid = b.cid AND p.id = b.property_id )
						LEFT JOIN budget_gl_account_months bgam ON ( b.cid = bgam.cid AND b.id = bgam.budget_id )
						' . $strJoin . '
					WHERE
						b.cid = ' . ( int ) $intCid . '
						AND b.deleted_on IS NULL
						AND ( b.is_advanced_budget IS FALSE ' . $strSubCondition . ' )
						 ' . $strCondition . '
					GROUP BY
						p.id,
						p.cid,
						b.id,
						b.cid,
						workbook_name
					ORDER BY ' . $strOrderBy . ');
				ANALYZE pg_temp.temp_budgets_listing;';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedBudgetsDetailsByCid( $intCid, $objClientDatabase, $objBudgetsFilter = NULL, $objPagination = NULL, $boolIsViewConfidentialAdvancedBudgetTab = false ) {

		if( false == valId( $intCid ) ) return NULL;

		self::createTempTableBudgetsByPropertyIdsByCid( $intCid, $objClientDatabase, $objBudgetsFilter, $boolIsViewConfidentialAdvancedBudgetTab );

		$strSql = 'SELECT * FROM pg_temp.temp_budgets_listing';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchBudgetsDetailsCountByCid( $intCid, $objClientDatabase, $objBudgetsFilter = NULL, $boolIsViewConfidentialAdvancedBudgetTab = false ) {

		self::createTempTableBudgetsByPropertyIdsByCid( $intCid, $objClientDatabase, $objBudgetsFilter, $boolIsViewConfidentialAdvancedBudgetTab );

		$strSql = 'SELECT count( id ) FROM pg_temp.temp_budgets_listing';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCustomBudgetsByCid( $intCid, $objBudgetFilter, $objClientDatabase ) {

		$strCondition = '';

		if( true == valObj( $objBudgetFilter, 'CBudgetsFilter' ) ) {

			if( true == valArr( $objBudgetFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND b.property_id IN ( ' . implode( ',', $objBudgetFilter->getPropertyIds() ) . ' )';
			}

			if( true == valArr( $objBudgetFilter->getFiscalYears() ) ) {
				$strCondition .= ' AND b.fiscal_year IN ( ' . implode( ',', $objBudgetFilter->getFiscalYears() ) . ' )';
			}

			if( true == valArr( $objBudgetFilter->getBudgetStatusTypeIds() ) ) {
				$strCondition .= ' AND b.budget_status_type_id IN ( ' . implode( ',', $objBudgetFilter->getBudgetStatusTypeIds() ) . ' )';
			}

			if( true == valArr( $objBudgetFilter->getBudgetIds() ) ) {
				$strCondition .= ' AND b.id IN ( ' . implode( ',', $objBudgetFilter->getBudgetIds() ) . ' )';
			}
		}

		$strSql = 'SELECT
						b.*,
						gat.gl_account_type_id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						bgam.id AS budget_gl_account_month_id,
						bgam.property_id,
						bgam.gl_account_id,
						bgam.post_month,
						bgam.amount,
						bgam.gl_dimension_id
					FROM
						budgets b
						LEFT JOIN budget_gl_account_months bgam ON ( b.cid = bgam.cid AND b.id = bgam.budget_id )
						LEFT JOIN gl_account_trees gat ON ( gat.cid = bgam.cid AND gat.gl_account_id = bgam.gl_account_id AND gat.is_default = 1 )
					WHERE
						b.cid = ' . ( int ) $intCid .
		          $strCondition;

		$strSql .= ' ORDER BY
		gat.formatted_account_number,bgam.id';

		$arrmixBudgetDetails = fetchData( $strSql, $objClientDatabase );
		if( false != valArr( $arrmixBudgetDetails ) ) {
			foreach( $arrmixBudgetDetails as $intIndex => $arrmixBudget ) {
				$arrmixBudgetDetails[$intIndex]['budget_id'] = $arrmixBudget['id'];
				unset( $arrmixBudgetDetails[$intIndex]['id'] );
			}
		}
		return $arrmixBudgetDetails;

	}

	public static function fetchLastYearActualAndBudgetDataByBudgetIdByPropertyIdByFiscalYearByBudgetTabIdByCid( $intBudgetId, $intPropertyId, $intFiscalYear, $intBudgetTabId, $intCid, $objClientDatabse ) {

		if( false == valId( $intBudgetId ) || false == valId( $intPropertyId ) || false == valId( $intFiscalYear ) || false == valId( $intCid ) || false == valId( $intBudgetTabId ) ) return NULL;

		$strSql = '
			DROP TABLE IF EXISTS prior_default_budgets;
			CREATE TEMPORARY TABLE prior_default_budgets AS (
				( SELECT id,
					cid,
					property_id,
					fiscal_year
				FROM
					budgets
				WHERE
					cid = ' . ( int ) $intCid . '
					AND is_default = 1
					AND property_id = ' . ( int ) $intPropertyId . '
					AND fiscal_year >= ' . ( ( int ) $intFiscalYear - 3 ) . '
					AND fiscal_year < ' . ( int ) $intFiscalYear . '
				GROUP BY
					cid,
					id ORDER BY fiscal_year DESC )

				UNION ALL

				( SELECT max(id) as id,
					cid ,
					property_id,
					fiscal_year
				FROM
					budgets
				WHERE
					cid = ' . ( int ) $intCid . '
					AND ( property_id, fiscal_year ) NOT IN (
						SELECT
							property_id,
							fiscal_year
						FROM budgets
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_default = 1
							AND property_id = ' . ( int ) $intPropertyId . '
							AND fiscal_year >= ' . ( ( int ) $intFiscalYear - 3 ) . '
							AND fiscal_year < ' . ( int ) $intFiscalYear . '
						GROUP BY cid,
							fiscal_year,
							property_id
					)
					AND is_default = 0
					AND property_id = ' . ( int ) $intPropertyId . '
					AND fiscal_year >= ' . ( ( int ) $intFiscalYear - 3 ) . '
					AND fiscal_year < ' . ( int ) $intFiscalYear . '
				GROUP BY
					cid,
					fiscal_year,
					property_id
				ORDER BY fiscal_year DESC
				)
			);

			( SELECT
					bgam.gl_account_id,
					\'budget\' as data_type,
					SUM( COALESCE( bgam.amount, 0 ) ) as amount,
					pdb.fiscal_year
				FROM prior_default_budgets pdb
					JOIN budget_gl_account_months bgam ON ( bgam.cid = pdb.cid AND pdb.id = bgam.budget_id )
					JOIN budget_tab_gl_accounts btga ON ( btga.cid = pdb.cid AND btga.budget_id = ' . ( int ) $intBudgetId . ' AND bgam.gl_account_id = btga.gl_account_id )
				WHERE
					pdb.cid = ' . ( int ) $intCid . '
					AND btga.budget_tab_id = ' . ( int ) $intBudgetTabId . '
				GROUP BY
					pdb.fiscal_year,
					pdb.property_id,
					bgam.gl_account_id, btga.adjustment_percent ORDER BY pdb.fiscal_year DESC )';

		return fetchData( $strSql, $objClientDatabse );
	}

}
?>