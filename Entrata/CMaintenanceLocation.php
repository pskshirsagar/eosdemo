<?php

class CMaintenanceLocation extends CBaseMaintenanceLocation {

	const SYSTEM_CODE_UNIT_WIDE = 'UW';
	const DEFAULT_MAINTENANCE_LOCATION = 'DF';
	const PROPERTY_WIDE = 'PW';
	const LOOKUP_TYPE_LOCATIONS_ON_PROPERTIES		= 'locations_on_properties';
	const LOOKUP_TYPE_LOCATIONS_WITHIN_UNIT			= 'locations_within_unit';

	protected $m_strRemotePrimaryKey;
	protected $m_strMaintenanceLocationTypeName;
	protected $m_intPropertyUnitMaintenanceLocationId;
	protected $m_intIntegrationClientStatusTypeId;

	protected $m_boolShowOnResidentPortalPropertyLevel = false;

	public static $c_arrintIntegratedClientTypes = [
		CIntegrationClientType::AMSI,
		CIntegrationClientType::YARDI,
		CIntegrationClientType::MRI,
		CIntegrationClientType::TIMBERLINE,
		CIntegrationClientType::REAL_PAGE,
		CIntegrationClientType::JENARK,
		CIntegrationClientType::REAL_PAGE_API,
		CIntegrationClientType::YARDI_RPORTAL,
	];

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, true, $boolDirectSet );

    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( trim( stripcslashes( $arrmixValues['remote_primary_key'] ) ) );
    	if( true == isset( $arrmixValues['maintenance_location_type_name'] ) ) $this->setMaintenanceLocationTypeName( trim( stripcslashes( $arrmixValues['maintenance_location_type_name'] ) ) );
	    if( true == isset( $arrmixValues['property_unit_maintenance_location_id'] ) ) $this->setPropertyUnitMaintenanceLocationId( trim( stripcslashes( $arrmixValues['property_unit_maintenance_location_id'] ) ) );
	    if( true == isset( $arrmixValues['show_on_resident_portal_property_level'] ) ) $this->setShowOnResidentPortalPropertyLevel( $arrmixValues['show_on_resident_portal_property_level'] );
	    if( true == isset( $arrmixValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrmixValues['integration_client_status_type_id'] );

    	return;
    }

    /**
     * Create Functions
     */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenanceLocationName( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		$objMaintenanceLocation = \Psi\Eos\Entrata\CMaintenanceLocations::createService()->fetchMaintenanceLocation( sprintf( 'SELECT name, details FROM maintenance_locations WHERE id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
		$strMaintenanceLocation = NULL;
		if( true == valObj( $objMaintenanceLocation, CMaintenanceLocation::class ) ) {
			$strMaintenanceLocation = $objMaintenanceLocation->getName();
		}

		return $strMaintenanceLocation;
	}

    public function createPropertyMaintenanceLocation() {

    	$objPropertyMaintenanceLocation = new CPropertyMaintenanceLocation();
    	$objPropertyMaintenanceLocation->setCid( $this->m_intCid );
    	$objPropertyMaintenanceLocation->setMaintenanceLocationId( $this->m_intId );

    	return $objPropertyMaintenanceLocation;
    }

    /**
     * Getters and Setters
     */
	public function setMaintenanceLocationTypeName( $strMaintenanceLocationTypeName ) {
		$this->m_strMaintenanceLocationTypeName = CStrings::strTrimDef( $strMaintenanceLocationTypeName, 50, NULL, true );
    }

    public function getMaintenanceLocationTypeName() {
        return $this->m_strMaintenanceLocationTypeName;
    }

    public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

    /**
     * Validation Functions
     */

	public function valMaintenanceLocationMaintenanceRequests( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestsByMaintenanceLocationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjMaintenanceRequests ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance request(s) exists for the maintenance location.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationMaintenanceTemplates( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenanceLocationId( $this->getCid(), $this->getId(), $objDatabase );

			if( true == valArr( $arrobjMaintenanceTemplates ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance template(s) exists for the maintenance location.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationInspectionChecklists( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$arrobjInspectionFormLocationProblem = \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByMaintenanceLocationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjInspectionFormLocationProblem ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Inspection checklist(s) exists for the maintenance location.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationJobCosting( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$arrobjJobCostings = \Psi\Eos\Entrata\CJobGroupLocations::createService()->fetchJobGroupLocationsByMaintenanceLocationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjJobCostings ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance location is being used by Job Costing and cannot be deleted.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valToggleMaintenanceLocationMaintenanceTemplates( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) && 0 == $this->getIsPublished() ) {
			$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenanceLocationId( $this->getCid(), $this->getId(), $objDatabase );

			if( true == valArr( $arrobjMaintenanceTemplates ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance template(s) exists for the maintenance location.' ) ) );
			}
		}

		return $boolIsValid;
	}

    public function valName( $objDatabase ) {

        $boolIsValid = true;

   		$objMaintenanceLocation = \Psi\Eos\Entrata\CMaintenanceLocations::createService()->fetchMaintenanceLocationByCidByName( $this->m_intCid, $this->getName(), $objDatabase );

    	if( true == is_object( $objMaintenanceLocation ) && true == valObj( $objMaintenanceLocation, 'CMaintenanceLocation' ) && $objMaintenanceLocation->getId() != $this->getId() ) {
			$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Location {%s, 0} already exists.', [ $objMaintenanceLocation->getName() ] ) ) );
		} elseif( true == is_null( $this->getName() ) ) {
    		$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

        return $boolIsValid;
    }

    public function valMaintenanceLocationTypeId() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getMaintenanceLocationTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_type_id', __( 'Location Type is required. ' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valMaintenanceLocationTypeChange( $objDatabase ) {
        $boolIsValid = true;

        if( false == is_null( $this->getMaintenanceLocationTypeId() ) ) {

            if( CMaintenanceLocationType::PROPERTY == $this->getMaintenanceLocationTypeId() ) {
                $intInspectionFormLocationCount = \Psi\Eos\Entrata\CInspectionFormLocations::createService()->fetchInspectionFormLocationCountByMaintenanceLocationIdByMaintenanceLocationTypeIdByCid( $this->getId(), CMaintenanceLocationType::UNIT, $this->m_intCid, $objDatabase );
                if( 0 < $intInspectionFormLocationCount ) {
                    $boolIsValid = false;
                    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Inspection template(s) exists with unit type maintenance location.' ) ) );
                }

                $intPropertyUnitMaintenanceLocationCount = \Psi\Eos\Entrata\CPropertyUnitMaintenanceLocations::createService()->fetchActivePropertyUnitMaintenanceLocationCountByMaintenanceLocationIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
                if( 0 < $intPropertyUnitMaintenanceLocationCount ) {
                    $boolIsValid = false;
                    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Location exist on unit type setup.' ) ) );
                }
            } else {
                $intInspectionFormLocationCount = \Psi\Eos\Entrata\CInspectionFormLocations::createService()->fetchInspectionFormLocationCountByMaintenanceLocationIdByMaintenanceLocationTypeIdByCid( $this->getId(), CMaintenanceLocationType::PROPERTY, $this->m_intCid, $objDatabase );
                if( 0 < $intInspectionFormLocationCount ) {
                    $boolIsValid = false;
                    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Inspection template exists with property type maintenance location.' ) ) );
                }
            }
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valMaintenanceLocationTypeId();
            	break;

            case 'maintenance_location_toggle':
            	$boolIsValid &= $this->valToggleMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valMaintenanceLocationMaintenanceRequests( $objDatabase );
            	$boolIsValid &= $this->valMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	$boolIsValid &= $this->valMaintenanceLocationInspectionChecklists( $objDatabase );
	            $boolIsValid &= $this->valMaintenanceLocationJobCosting( $objDatabase );
            	break;

            case 'validate_delete_new':
            	$boolIsValid &= $this->valMaintenanceLocationMaintenanceRequests( $objDatabase );
            	$boolIsValid &= $this->valMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	// $boolIsValid &= $this->valMaintenanceLocationInspectionChecklists( $objDatabase );
            	break;

            case 'validate_update_new':
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valMaintenanceLocationTypeId();
            	$boolIsValid &= $this->valMaintenanceLocationTypeChange( $objDatabase );
            	$boolIsValid &= $this->valToggleMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
    	$this->m_strRemotePrimaryKey = $strRemotePrimaryKey;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getRemotePrimaryKey() {
    	return $this->m_strRemotePrimaryKey;
    }

    public function setPropertyUnitMaintenanceLocationId( $intPropertyUnitMaintenanceLocationId ) {
		$this->m_intPropertyUnitMaintenanceLocationId = $intPropertyUnitMaintenanceLocationId;
    }

	public function setShowOnResidentPortalPropertyLevel( $boolShowOnResidentPortalPropertyLevel ) {
		$this->m_boolShowOnResidentPortalPropertyLevel = $boolShowOnResidentPortalPropertyLevel;
	}

	public function getPropertyUnitMaintenanceLocationId() {
		return $this->m_intPropertyUnitMaintenanceLocationId;
	}

	public function getShowOnResidentPortalPropertyLevel() {
		return $this->m_boolShowOnResidentPortalPropertyLevel;
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		return $this->m_intIntegrationClientStatusTypeId = $intIntegrationClientStatusTypeId;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == is_null( $this->getName() ) ) {
			$this->setName( $this->replaceBreakTag( $this->getName() ) );
		}
		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == is_null( $this->getName() ) ) {
			$this->setName( $this->replaceBreakTag( $this->getName() ) );
		}
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function replaceBreakTag( $strLocationName ) {
		$strLocationName = preg_replace( '/<br\W*?>/', ' ', $strLocationName );
		return $strLocationName;
	}

}
?>