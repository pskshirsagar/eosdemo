<?php

class CDepreciationCategory extends CBaseDepreciationCategory {

	public function valDepreciationGlAccountId( $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( false == valId( $this->getDepreciationGlAccountId() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'depreciation_gl_account_id', __( 'Depreciation expense GL acct is required.' ) ) );
			return false;
		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getDepreciationGlAccountId() . '
							AND disabled_by IS NOT NULL
							AND disabled_on IS NOT NULL';

			if( 0 != CGlAccounts::fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'depreciation_gl_account_id', __( 'Depreciation expense GL account has be disabled.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valAccumDepreciationGlAccountId( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getAccumDepreciationGlAccountId() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_depreciation_category_id', __( 'Accumulated depreciation GL acct is required.' ) ) );
			return false;
		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getAccumDepreciationGlAccountId() . '
							AND disabled_by IS NOT NULL
							AND disabled_on IS NOT NULL';

			if( 0 != CGlAccounts::fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_depreciation_category_id', __( 'Accumulated depreciation GL account has be disabled.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valTaxRecoveryPeriodId() {
		$boolIsValid = true;

		if( false == valId( $this->getTaxRecoveryPeriodId() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_recovery_period_id', __( 'Default tax recovery period is required.' ) ) );
			return false;
		}
		return $boolIsValid;
	}

	public function valName( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			return false;
		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

				$strWhere = 'WHERE
								cid = ' . ( int ) $this->getCid() . '
								AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
								AND id <> ' . ( int ) $this->getId() . '
								AND deleted_by IS NULL';

			if( 0 != \Psi\Eos\Entrata\CDepreciationCategories::createService()->fetchDepreciationCategoryCount( $strWhere, $objClientDatabase ) ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "&nbspDepreciation category name '{%s, 0}' already exists .&nbsp", [ $this->getName() ] ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valUsefulLifeMonths() {

		if( true == is_null( $this->getUsefulLifeMonths() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'useful_life_months', __( 'Default useful life is required.' ) ) );
			return false;
		}

		if( false == is_numeric( $this->getUsefulLifeMonths() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'useful_life_months', __( 'Please enter a numeric value in default useful life.' ) ) );
			return false;
		}

		if( mb_strlen( $this->getUsefulLifeMonths() ) > 3 ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'useful_life_months', __( 'Please enter less then three digits in default useful life.' ) ) );
			return false;
		}

		if( true == $this->getIsDepreciable() && $this->getUsefulLifeMonths() <= 0 ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'useful_life_months', __( 'Please enter a valid number greater then zero in default useful life.' ) ) );
			return false;
		}

		return true;
	}

	public function valApCodes( $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return false;
		}

		$arrobjApCodes		= rekeyObjects( 'Name', ( array ) \Psi\Eos\Entrata\CApCodes::createService()->fetchApCodesByDepreciationCategoryIdByCid( $this->getId(), $this->getCid(), $objClientDatabase ) );
		$arrmixAssetDetails = ( array ) \Psi\Eos\Entrata\CAssetDetails::createService()->fetchAssetDetailsByDepreciationCategoryIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjApCodes ) ) {

			$strApCodeNames 	= implode( ', ', array_keys( $arrobjApCodes ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Can not delete the depreciation category since it is associated with following catalog items :{%s, 0}', [ $strApCodeNames ] ) ) );
			return false;
		} elseif( true == valArr( $arrmixAssetDetails ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Can not delete the depreciation category since it is associated with asset details.' ) ) );
			return false;
		}
		return $boolIsValid;

	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				// no need to validate the GL account if category is non depreciable
				if( true == $this->getIsDepreciable() ) {
					$boolIsValid &= $this->valAccumDepreciationGlAccountId( $objClientDatabase );
					$boolIsValid &= $this->valDepreciationGlAccountId( $objClientDatabase );
				}
				$boolIsValid &= $this->valUsefulLifeMonths();
				$boolIsValid &= $this->valTaxRecoveryPeriodId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valApCodes( $objClientDatabase );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>