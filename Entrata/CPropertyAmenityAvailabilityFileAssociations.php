<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAmenityAvailabilityFileAssociations
 * Do not add any new functions to this class.
 */

class CPropertyAmenityAvailabilityFileAssociations extends CBasePropertyAmenityAvailabilityFileAssociations {

	public static function fetchPropertyAmenityAvailabilityFileAssociationByPropertyAmenityAvailabilityIdByCid( $intPropertyAmenityAvailabilityId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						property_amenity_availability_file_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_amenity_availability_id = ' . ( int ) $intPropertyAmenityAvailabilityId;

		return parent::fetchPropertyAmenityAvailabilityFileAssociation( $strSql, $objDatabase );
	}
}
?>