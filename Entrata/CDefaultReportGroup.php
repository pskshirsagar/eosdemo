<?php

class CDefaultReportGroup extends CBaseDefaultReportGroup {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportGroupTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valParentModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $strAction, $objDatabase ) {
		$boolIsValid = true;
		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} else {

			if( true == empty( $this->m_intParentModuleId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Parent', 'Parent is required.' ) );
				return;
			}

			$strWhere = '
					WHERE
						name ILIKE \'' . $this->m_strName . '\'
						AND parent_module_id = ' . $this->m_intParentModuleId;

			if( VALIDATE_UPDATE == $strAction ) {
				$strWhere .= ' AND id != ' . $this->m_intId;
			}

			if( false == is_null( $objDatabase ) ) {
				$intExistingReportGroupCount = \Psi\Eos\Entrata\CDefaultReportGroups::createService()->fetchDefaultReportGroupCount( $strWhere, $objDatabase );

				if( 0 < $intExistingReportGroupCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Report Group with this title is already exists.' ) );
				}
			}
		}
		return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPositionNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName( $strAction, $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$intCurrentUserId;

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '
					VALUES ( ' .
					$strId . ', ' .
					$this->sqlReportGroupTypeId() . ', ' .
					$this->sqlParentModuleId() . ', ' .
					$this->sqlName() . ', ' .
					$this->sqlDescription() . ', ' .
					$this->sqlPositionNumber() . ', ' .
					$this->sqlIsInternal() . ', ' .
					$this->sqlIsPublished() . ', ' .
					$this->sqlIsDisabled() . ', ' .
					$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$intCurrentUserId;

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' report_group_type_id = ' . $this->sqlReportGroupTypeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlReportGroupTypeId() ) != $this->getOriginalValueByFieldName( 'report_group_type_id' ) ) {
			$arrstrOriginalValueChanges['report_group_type_id'] = $this->sqlReportGroupTypeId();
			$strSql .= ' report_group_type_id = ' . $this->sqlReportGroupTypeId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlParentModuleId() ) != $this->getOriginalValueByFieldName( 'parent_module_id' ) ) {
			$arrstrOriginalValueChanges['parent_module_id'] = $this->sqlParentModuleId();
			$strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' name = ' . $this->sqlName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName( 'name' ) ) {
			$arrstrOriginalValueChanges['name'] = $this->sqlName();
			$strSql .= ' name = ' . $this->sqlName() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' description = ' . $this->sqlDescription() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName( 'description' ) ) {
			$arrstrOriginalValueChanges['description'] = $this->sqlDescription();
			$strSql .= ' description = ' . $this->sqlDescription() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' position_number = ' . $this->sqlPositionNumber() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlPositionNumber() ) != $this->getOriginalValueByFieldName( 'position_number' ) ) {
			$arrstrOriginalValueChanges['position_number'] = $this->sqlPositionNumber();
			$strSql .= ' position_number = ' . $this->sqlPositionNumber() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_internal = ' . $this->sqlIsInternal() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsInternal() ) != $this->getOriginalValueByFieldName( 'is_internal' ) ) {
			$arrstrOriginalValueChanges['is_internal'] = $this->sqlIsInternal();
			$strSql .= ' is_internal = ' . $this->sqlIsInternal() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
			$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsDisabled() ) != $this->getOriginalValueByFieldName( 'is_disabled' ) ) {
			$arrstrOriginalValueChanges['is_disabled'] = $this->sqlIsDisabled();
			$strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' details = ' . $this->sqlDetails();
		} elseif( CStrings::reverseSqlFormat( $this->sqlDetails() ) != $this->getOriginalValueByFieldName( 'details' ) ) {
			$arrstrOriginalValueChanges['details'] = $this->sqlDetails();
			$strSql .= ' details = ' . $this->sqlDetails();
			$boolUpdate = true;
		}
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';
		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}
?>