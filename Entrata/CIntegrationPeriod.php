<?php

class CIntegrationPeriod extends CBaseIntegrationPeriod {

	const NIGHTLY 			= 1;
	const EVERY_TWO_DAYS 	= 2;
	const WEEKLY 			= 3;
	const HOURLY 			= 4;
	const EVERY_FIVE_MINUTES	= 5;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>