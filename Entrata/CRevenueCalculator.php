<?php

class CRevenueCalculator extends CBaseRevenueCalculator {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCalculatedDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( '' == $this->getName() ) {
					$this->addErrorMsg( new Error( 'Please Enter Version Name' ) );
					return false;
				}
				$strWhere = 'WHERE name = \'' . $this->getName() . '\' AND cid = ' . $this->getCid() . ' AND property_id = ' . $this->getPropertyId();
				if( 0 < ( int ) CRevenueCalculators::fetchRevenueCalculatorCount( $strWhere, $objDatabase ) ) {
					$this->addErrorMsg( new Error( 'Version Name is duplicate.' ) );
					return false;
				}

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>