<?php

class CApplicantSubsidyDetail extends CBaseApplicantSubsidyDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCitizenshipTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyEthnicityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidySsnExceptionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyDependentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAlienRegistrationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTracsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHudSpecifyGender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPartTimeStudent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPoliceOrSecurityOfficer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>