<?php

class CCommercialClauseKey extends CBaseCommercialClauseKey {

	public static $c_arrstrCommercialClauseKeys = [ 'TERM', 'LEASE_DATE','TERM_COMMENCEMENT', 'RENT_COMMENCEMENT', 'RENT_STEPS', 'RENEWAL_OPTIONS', 'SECURITY_DEPOSIT', 'HOLD_OVER', 'LATE_FEE_FORMULA', 'FIRST_RENT_DUE', 'PERCENTAGE_RENT', 'OTHER_FEES','SECURITY_DEPOSIT_REFUND_DATE', 'CAM_POOL', 'LOOKUP_CODE', 'TENANT', 'SUITE', 'ADDRESS_TYPE','GUARANTOR_REFERENCE', 'CONTACT_REFERENCE', 'WAIVE_LATE_FEES', 'USE_RENT_STEP_FOR_SALES', 'COVERAGE_DETAIL', 'PERCENT_SALES', 'AR_CODE_ID', 'INTEREST_PERCENT', 'INTEREST_QUALIFICATION_DAYS', 'RATE_INTERVAL_OFFSET', 'USE_ANNUAL_RENT_PERCENT_INCREASE', 'DELINQUENCY_POLICY', 'SECURITY_DEPOSIT_CHARGE_CODE', 'TERM_EXPIRATION', 'COMMITMENT_DATE', 'CERTIFICATE_OF_OCCUPANCY_DATE' ];
	public static $c_arrstrAddTenantCommercialClauseKeys = [ 'LOOKUP_CODE', 'TENANT', 'SUITE', 'ADDRESS_TYPE', 'TERM', 'LEASE_DATE','TERM_COMMENCEMENT', 'RENT_COMMENCEMENT', 'TERM_EXPIRATION', 'LATE_FEE_FORMULA', 'COMMITMENT_DATE', 'CERTIFICATE_OF_OCCUPANCY_DATE' ];
	public static $c_arrstrNonMandatoryCommercialClauseKeys = [ 'SECURITY_DEPOSIT', 'SECURITY_DEPOSIT_REFUND_DATE','HOLD_OVER', 'CERTIFICATE_OF_OCCUPANCY_DATE', 'COMMITMENT_DATE' ];
	public static $c_arrstrExcludedCommercialClauseKeys = [ 'ADDRESS_TYPE', 'CAM_POOL', 'CONTACT_REFERENCE', 'COVERAGE_DETAIL', 'GUARANTOR_REFERENCE' ];

	public function valKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commercial_clause_key', __( 'Key is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLabel( $objDatabase = NULL ) {

		if( false === valStr( $this->getLabel() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commercial_clause_label', __( 'Label is required.' ) ) );
			return false;
		}
		$intDefaultCommercialClauseKeyCount = 0;
		$intCommercialClauseKeyCount = \Psi\Eos\Entrata\CCommercialClauseKeys::createService()->fetchDuplicateCommercialClauseKeysCountByLabelByIdByCid( $this->getLabel(), $this->getId(), $this->getCid(), $objDatabase );
		if( 'CUSTOM_CLAUSE_KEY' == $this->getKey() ) {
			$arrobjDefaultCommercialClauseKeys = \Psi\Eos\Entrata\CDefaultCommercialClauseKeys::createService()->fetchAllDefaultCommercialClauseKeys( $objDatabase );
			foreach( $arrobjDefaultCommercialClauseKeys As $objDefaultCommercialClauseKey ) {
				$intDefaultCommercialClauseKeyCount += ( $objDefaultCommercialClauseKey->getLabel() == $this->getLabel() ) ? 1 : 0;
			}
		}
		if( 0 < $intCommercialClauseKeyCount || 0 < $intDefaultCommercialClauseKeyCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'label', __( '{%s, 0} already exists.', [ $this->getLabel() ] ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valLabel( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
			$this->setUpdatedBy( $intUserId );
			$this->setUpdatedOn( 'NOW()' );
			return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>
