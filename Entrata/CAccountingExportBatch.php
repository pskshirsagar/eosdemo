<?php

class CAccountingExportBatch extends CBaseAccountingExportBatch {

	const GL_EXPORT_BOOK_TYPE_ACCRUAL = 1;
	const GL_EXPORT_BOOK_TYPE_CASH    = 2;
	const GL_EXPORT_BOOK_TYPE_BOTH    = 3;
	const FILE_EXTENSION_CSV          = 'csv';
	const FILE_EXTENSION_XLS          = 'xlsx';
	const FILE_EXTENSION_TXT          = 'txt';
	const FILE_EXTENSION_IIF          = 'iif';
	const CONNECTION_TYPE_SFTP        = 2;
	const CONNECTION_TYPE_FTP         = 1;
	const AR_TRANSACTIONS             = 1;
	const AP_TRANSACTIONS             = 2;
	const JOURNAL_ENTRIES             = 3;

	const ACCOUNTING_EXPORT_BATCH_STATUS_ACTIVE = 1;
	const ACCOUNTING_EXPORT_BATCH_STATUS_DELETED = 2;
	const SETTING_NAME_TRANSMISSION_VENDOR = 'Transmission vendor';
	const SETTING_NAME_INCLUDE_ORIGINATING_CURRENCY = 'Include Originating currency';
	const PERIOD_POST_MONTH = 'post_month';
	const PERIOD_DATE_RANGE = 'date_range';
	const COUNTRY_FORMAT_THREE_DIGIT_COUNTRY_CODE = 1;
	const COUNTRY_FORMAT_TWO_DIGIT_COUNTRY_CODE   = 2;
	const COUNTRY_FORMAT_COUNTRY_NAME             = 3;

	const MAX_REATTEMPT_LIMIT	= 10;

	const QUICK_BOOK_CLASS_UNIT_NUMBER = 1;
	const QUICK_BOOK_CLASS_BLDG_UNIT_NUMBER = 2;

	const GL_EXPORT_STRING_BOOK_TYPE_CASH    = 'Cash';
	const GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL = 'Accrual';

	protected $m_strFileName;
	protected $m_strFileExtension;
	protected $m_strFileContent;
	protected $m_strAmount;
	protected $m_strUsername;
	protected $m_arrobjTransmissions;
	protected $m_arrstrFileContents;
	protected $m_strFilePath;
	protected $m_arrstrFileContent;
	protected $m_arrstrFileNames;
	protected $m_strPropertyName;
	protected $m_intLeaseCustomerId;
	protected $m_intPropertyId;
	protected $m_strMoveInDate;
	protected $m_intTransmissionTypeId;

	private $m_objSyncEncryption;

	public $m_objExportsFileLibrary;

	public function __construct() {
		parent::__construct();

		$this->setSyncEncryption( CSyncEncryption::createService() );
	}

	/**
	 * @return mixed
	 */
	protected function getSyncEncryption() {
		return $this->m_objSyncEncryption;
	}

	/**
	 * @param \CSyncEncryption $objSyncEncryption
	 */
	protected function setSyncEncryption( CSyncEncryption $objSyncEncryption ) {
		$this->m_objSyncEncryption = $objSyncEncryption;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getTransmissionTypeId() {
		return $this->m_intTransmissionTypeId;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setTransmissionTypeId( $intTransmissionTypeId ) {
		$this->m_intTransmissionTypeId = $intTransmissionTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( isset( $arrmixValues['username'] ) && $boolDirectSet ) {
			$this->m_strAmount = trim( $arrmixValues['username'] );
		} elseif( isset( $arrmixValues['username'] ) ) {
			$this->setUsername( $arrmixValues['username'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) {
			$this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['move_in_date'] ) ) {
			$this->setMoveInDate( $arrmixValues['move_in_date'] );
		}
		if( true == isset( $arrmixValues['transmission_type_id'] ) ) {
			$this->setTransmissionTypeId( $arrmixValues['transmission_type_id'] );
		}
	}

	/**
	 * Set Functions
	 */

	public function setarrobjTransmissions( $arrobjTransmissions ) {
		$this->m_arrobjTransmissions = $arrobjTransmissions;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	/**
	 * Get Functions
	 */

	public function getarrobjTransmissions() {
		return $this->m_arrobjTransmissions;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intGlExportBatchTypeId = NULL, $intSummarizationMethod = NULL ) {

		if( 2 == $intSummarizationMethod ) {
			return \Psi\Eos\Entrata\CAccountingExportBatchDetails::createService()->fetchCustomAccountingExportBatchDetailsWithPostMonthSummaryByAccountingExportBatchIdByGlExportBookTypeIdByCid( $this->getId(), $this->getCid(), $intGlExportBookTypeId, $objClientDatabase, $intGlExportBatchTypeId, $this->getGlTreeId() );
		}
		return \Psi\Eos\Entrata\CAccountingExportBatchDetails::createService()->fetchCustomAccountingExportBatchDetailsByAccountingExportBatchIdByAccountingExportBookTypeIdByCid( $this->getId(), $this->getCid(), $intGlExportBookTypeId, $objClientDatabase, $intGlExportBatchTypeId, $this->getGlTreeId() );

	}

	// this function will be for GL export only

	public function fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intGlExportBatchTypeId = NULL, $boolIncludeDeletedUnits = false ) {

		switch( $intGlExportBatchTypeId ) {

			case CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM:
				$strSql = $this->buildDetailsSqlForTimberLineFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_QUICK_BOOKS:
				$strSql = $this->buildDetailsSqlForQuickBooksFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_SKYLINE:
				$strSql = $this->buildDetailsSqlForSkylineFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_MRI:
				$strSql = $this->buildDetailsSqlForMriFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_JENARK:
				$strSql = $this->buildDetailsSqlForJenarkFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI:
			case CTransmissionVendor::GL_EXPORT_YARDI_SIPP:
				$strSql = $this->buildDetailsSqlForYardiFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_ONESITE:
				$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
				$strSql = $this->buildDetailsSqlForOnesiteFormat( $intGlExportBookTypeId, $strCheckDeletedUnitsSql );
				break;

			case CTransmissionVendor::GL_EXPORT_AMSI:
				$strSql = $this->buildDetailsSqlForAmsiFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_JD_EDWARDS:
				$strSql = $this->buildDetailsSqlForJdEdwardsFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_ORACLE:
				$strSql = $this->buildDetailsSqlForOracleFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_GREAT_PLAINS:
				$strSql = $this->buildDetailsSqlForGreatPlainsFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_ETL:
			case CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS:
				$strSql = $this->buildDetailsSqlForYardiEtlFormat( $intGlExportBookTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S:
				$strSql = $this->buildDetailsSqlForYardiSevenSFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX:
				$strSql = $this->buildDetailsSqlForYardiSevenSMxFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL:
				$strSql = $this->buildDetailsSqlForYardiSevenSInternationalFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_ETL_MR:
				$strSql = $this->buildDetailsSqlForYardiEtlMrFormat( $intGlExportBookTypeId );
				break;

			default:
				/*** DO nothing **/
				break;
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	private function getCommonSelectFields( $intGlExportTypeId = NULL, $intGlExportBookTypeId = NULL ) {
		$strTempSelect = '';
		if( false != valId( $intGlExportBookTypeId ) && ( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId || SELF::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) ) {
			$strTempSelect = ',gd.accrual_gl_account_id';
		}
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strTempSelect .= ',gat.account_number, gat.name AS account_name, gat.secondary_number, gat.id as gl_account_id';
		}
		return ( ( false == valId( $intGlExportTypeId ) ) ? ' DISTINCT ' : ' ' ) . ' gd.id, gd.property_id, gd.gl_header_id, gd.amount, gd.cash_gl_account_id' . $strTempSelect .
				',gd.lease_id,
    			gh.transaction_datetime, gh.post_month,gh.post_date as transaction_post_date,
    			p.property_name, p.lookup_code, gh.header_number,
    			CASE
      						WHEN ( to_char( gh.post_date, \'mm/yyyy\' ) = to_char( gh.post_month, \'mm/yyyy\' ) )
      						THEN gh.post_date
      						ELSE gh.post_month
    					END AS post_date';
	}

	private function getCommonJoins( $intGlExportBookTypeId ) {
		$strJoins = ' gl_headers gh
    			JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gd.cid = gh.cid )
    			JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
    			JOIN properties p ON ( gd.property_id = p.id and gd.cid = p.cid )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strJoins .= ' JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = ';
			$strJoins .= ( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) ? 'gd.accrual_gl_account_id )' : 'gd.cash_gl_account_id )';
		}

		return $strJoins;
	}

	private function getCommonWhereClause( $intGlExportBookTypeId = NULL ) {
		$strWhere = '';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strWhere = ' AND gat.gl_tree_id = ' . ( int ) $this->getGlTreeId();
		}
		return 'gh.cid = ' . ( int ) $this->getCid() . '
				AND gh.is_template = FALSE
				AND aea.accounting_export_batch_id = ' . $this->getId() . $strWhere . '
		        AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';
	}

	private function getCommonOrderBy() {
		return ' gd.property_id, gh.post_date';
	}

	public function buildDetailsSqlForTimberLineFormat( $intGlExportBookTypeId ) {
		$strSelect = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM,  $intGlExportBookTypeId );
		$strJoins  = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect .= ', gh.reference, gh.memo, pp.value, pp1.value as gl_account_prefix,
										CASE
									      WHEN ( gd.gl_transaction_type_id IN ( ' . CGlTransactionType::GL_GJ . ',' . CGlTransactionType::DATA_FIX_GENERAL . ' ) ) THEN gh.id
									      ELSE gh.reference_id
									    END AS transaction_id';
		$strJoins .= 'JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'TIMBERLINE_PM_JOURNAL_CODE\' AND pp.value IS NOT NULL )
							 	 LEFT JOIN  property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key = \'TIMBERLINE_PM_GL_ACCOUNT_PREFIX\' )
								 LEFT JOIN property_preferences pp2 ON ( p.cid = pp2.cid AND p.id = pp2.property_id AND pp2.key = \'TIMBERLINE_PM_EXCLUDE_AP_TRANSACTION\' )
								 JOIN gl_transaction_types gtt ON ( gd.gl_transaction_type_id = gtt.id )';

		$strWhere .= 'AND CASE
								      	 WHEN pp2.value = \'1\' THEN gtt.gl_ledger_type_id NOT IN (' . CGlLedgerType::AP . ')
								    	ELSE true
			    					END';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		 return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForQuickBooksFormat( $intGlExportBookTypeId ) {
		$strSelect = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_QUICK_BOOKS,  $intGlExportBookTypeId );
		$strJoins  = $this->getCommonJoins( $intGlExportBookTypeId );
		$strJoins .= ' LEFT JOIN ar_deposits ad ON (gh.reference_id = ad.id AND ad.cid = gh.cid)
					LEFT JOIN
						(
						  SELECT
						  		invApHeader.header_number,
						         invAd.description,
						         invApHeader.cid,
						         invApheader.id,
						         invapheader.ap_payee_id,
						         invAd.id as ap_detail_id
						  FROM ap_headers invApHeader
						       JOIN ap_details invAd ON (invAd.cid = invApHeader.cid AND invApHeader.id = invAd.ap_header_id)
						  WHERE invApHeader.cid = ' . ( int ) $this->getCid() . '
						) invoices ON ( invoices.cid = gd.cid AND invoices.id = gh.reference_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . ' AND invoices.ap_detail_id = gd.ap_detail_id )
					LEFT JOIN
						(
						  SELECT paymentAa.cid,
								paymentAa.id,
								invoice.header_number AS invoice_number,
								paymentApHeader.ap_payee_id,
								paymentApHeader.gl_transaction_type_id,
								ap.payment_number,
								paymentAd.description,
								paymentAa.cash_credit_gl_account_id,
								paymentAa.accrual_credit_gl_account_id
						  FROM ap_allocations paymentAa
								JOIN ap_details paymentAd ON (paymentAa.cid = paymentAd.cid AND paymentAa.credit_ap_detail_id = paymentAd.id)
								JOIN ap_headers paymentApHeader ON (paymentApHeader.cid = paymentAd.cid AND paymentApHeader.id = paymentAd.ap_header_id)
								JOIN ap_payments ap ON (ap.cid = paymentApHeader.cid AND ap.id = paymentApHeader.ap_payment_id)
								JOIN ap_headers invoice ON (invoice.cid = paymentapheader.cid AND invoice.id::text = paymentapheader.header_number)
						WHERE paymentApHeader.cid = ' . ( int ) $this->getCid() . '
						) payments ON (payments.cid = gh.cid AND payments.id = gh.reference_id)
					LEFT JOIN ap_payees app ON (invoices.cid = gh.cid AND app.id = invoices.ap_payee_id)
					LEFT JOIN ap_payees app2 ON (payments.cid = gh.cid AND app2.id = payments.ap_payee_id)
					LEFT JOIN property_preferences pf ON ( pf.cid = gd.cid AND pf.property_id = gd.property_id AND pf.key = \'QUICK_BOOK_CLASS\')
					LEFT JOIN property_units pu ON ( pu.cid = gd.cid AND pu.id = gd.property_unit_id )
					LEFT JOIN property_buildings pb ON ( pb.cid =  gd.cid AND pb.id = gd.property_building_id )
					LEFT JOIN ar_deposit_transactions adt ON ( ad.cid = adt.cid AND ad.id = adt.ar_deposit_id AND adt.property_id = gd.property_id )
					LEFT JOIN leases l ON ( l.cid = gd.cid AND l.id = COALESCE(gd.lease_id, adt.lease_id) AND gd.property_id = l.property_id )
					LEFT JOIN unit_spaces us ON ( l.cid = us.cid AND l.unit_space_id = us.id )';
		$strWhere = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = '';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy = ' ORDER BY gd.gl_header_id, gat.name';
		}
		$strSelect .= ',gh.reference, gh.memo, gh.gl_transaction_type_id, ad.deposit_number,
		COALESCE(invoices.header_number, payments.invoice_number) as invoice_number,
		COALESCE(app.company_name, app2.company_name) as vendor_name,
		payments.payment_number, invoices.description as line_description,
		payments.description as payment_line_description, pf.value as quick_book_class,
		COALESCE(pu.unit_number, us.unit_number_cache) as unit_number, pb.building_name';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
		$strSelect .= ', DENSE_RANK() OVER ( PARTITION BY payments.payment_number ORDER BY gat.account_number DESC ) AS rank';
		}
		$strSelect .= ', payments.cash_credit_gl_account_id,
		payments.accrual_credit_gl_account_id';

		$strSubSql = ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . $strOrderBy;
		return 'SELECT subDetails.* FROM ( ' . $strSubSql . ' ) As subDetails';
	}

	public function buildDetailsSqlForSkylineFormat( $intGlExportBookTypeId ) {
		$strSelect = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_SKYLINE,  $intGlExportBookTypeId );
		$strJoins  = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect .= ',gh.reference,gh.memo';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForMriFormat( $intGlExportBookTypeId ) {
		$strSelect = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_MRI,  $intGlExportBookTypeId );
		$strJoins  = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();

		$strSelect .= ', gh.reference, gh.memo,pp.value as ref,
										 pp1.value as entity_id,
										 pp2.value as mri_source,
										 pp3.value as site_id,
										 pp4.value as include_originating_currency,
										 gh.reference as memo_mri';
		$strJoins .= 'LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id  AND pp.key = \'MRI_REFERENCE\' AND pp.value IS NOT NULL )
								 LEFT JOIN property_preferences pp2 ON ( p.cid = pp2.cid AND p.id = pp2.property_id  AND pp2.key = \'MRI_SOURCE\' AND pp2.value IS NOT NULL )
							 	 LEFT JOIN property_preferences pp3 ON (p.cid = pp3.cid AND p.id = pp3.property_id AND pp3.key = \'MRI_SITE_ID\' AND pp3.value IS NOT NULL )
								 LEFT JOIN property_preferences pp4 ON (p.cid = pp4.cid AND p.id = pp4.property_id AND pp4.key = \'MRI_INCLUDE_ORIGINATING_CURRENCY\' AND pp4.value IS NOT NULL )
								 JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id  AND pp1.key = \'MRI_ENTITY_ID\' AND pp1.value IS NOT NULL )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}

		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForJenarkFormat( $intGlExportBookTypeId ) {
		$strSelect = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_JENARK,  $intGlExportBookTypeId );
		$strJoins  = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();

		$strSelect .= ',gh.reference, gh.memo,
		pp.value as ref, pp1.value as entity_id,
		CASE
											WHEN gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' THEN \'Journal Entry \' || gh.header_number
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_PAYMENT . ', ' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) AND l.property_unit_id IS NOT NULL ) THEN gh.reference_id || CASE WHEN pb.building_name IS NOT NULL THEN \' \' || pb.building_name ELSE \'\' END || \'-\' || pu.unit_number || CASE WHEN us.space_number IS NOT NULL THEN \'-\' || us.space_number ELSE \'\' END
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_PAYMENT . ', ' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) AND l.property_unit_id IS NULL ) THEN gh.reference_id::TEXT
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ' ) AND l.property_unit_id IS NOT NULL ) THEN aa.credit_ar_transaction_id || CASE WHEN pb.building_name IS NOT NULL THEN \' \' || pb.building_name ELSE \'\' END || \'-\' || pu.unit_number || CASE WHEN us.space_number IS NOT NULL THEN \'-\' || us.space_number ELSE \'\' END
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ' ) AND l.property_unit_id IS NULL ) THEN aa.credit_ar_transaction_id::TEXT
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ', ' . CGlTransactionType::AP_CHARGE . ', ' . CGlTransactionType::AP_PAYMENT . ', ' . CGlTransactionType::AP_ALLOCATION . ', ' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::AR_DEPOSIT_HELD . ', ' . CGlTransactionType::DATA_FIX_AP_HEADERS . ' , ' . CGlTransactionType::DATA_FIX_AP_ALLOCATIONS . ' ) ) THEN gh.reference
											WHEN ( gh.gl_transaction_type_id IN ( ' . CGlTransactionType::DATA_FIX_AR_DEPOSITS . ', ' . CGlTransactionType::DATA_FIX_GENERAL . ' ) ) THEN gh.memo
										 END
										AS jenark_memo';
		$strJoins .= 'LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id  AND pp.key = \'JENARK_REFERENCE\' AND pp.value IS NOT NULL)
		  				 		 LEFT JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id  AND pp1.key = \'JENARK_ENTITY_ID\' AND pp1.value IS NOT NULL)
								 LEFT JOIN ar_deposits ad ON ( ad.cid = gh.cid AND ad.id = gh.reference_id AND ad.gl_transaction_type_id = gh.gl_transaction_type_id )
								 LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
								 LEFT JOIN ar_codes ac ON ( ac.cid = art.cid AND ac.id = art.ar_code_id )
								 LEFT JOIN leases l ON ( gh.lease_id = l.id AND gh.cid = l.cid )
								 LEFT JOIN property_units pu ON ( l.property_unit_id = pu.id AND l.cid = pu.cid )
								 LEFT JOIN property_buildings pb ON ( pu.property_building_id = pb.id AND pu.cid = pb.cid AND pb.deleted_on IS NULL )
								 LEFT JOIN unit_spaces us ON ( pu.id = us.property_unit_id AND pu.cid = us.cid )
								 LEFT JOIN ar_allocations aa ON ( gh.reference_id = aa.id AND gh.cid = aa.cid )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ',gat.name';
		}

		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;

	}

	public function buildDetailsSqlForYardiFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId ) {
		$strSelect  = $this->getCommonSelectFields( $intGlExportBatchTypeId,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect  .= ',DENSE_RANK() over( ORDER BY gh.post_date,gd.property_id ASC ) as record_sequence, ad.description,
						gh.reference, gh.memo, p.remote_primary_key';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSelect .= ' ,gat.formatted_account_number as account_number_delimiter';
			$strOrderBy .= ',gat.name';
		}
		$strSelect .= ', aes.frequency_id,gd.gl_transaction_type_id,gd.lease_id, c.name_first,c.name_last,pu.unit_number,
										 ad.description,ah.header_number AS invoice_id, 
										 CASE
		                            		WHEN ( gd.memo IS NOT NULL) THEN gd.memo
											WHEN ( gh.memo IS NOT NULL) THEN gh.memo
											WHEN ( gh.reference IS NOT NULL) THEN gh.reference
		                   	    		 END AS memo_yardi,ap.company_name,ah.ap_payee_id, ah.lease_customer_id,
									     c1.name_first as c1_name_first,
									     c1.name_last as c1_name_last,
										gh.post_date AS postdate';
		if( CTransmissionVendor::GL_EXPORT_YARDI == $intGlExportBatchTypeId ) {
			$strSelect .= ',pp.value';
		}
		$strJoins   .= 'LEFT JOIN ap_details ad ON (ad.cid = gd.cid AND ad.id = gd.ap_detail_id)
                                LEFT JOIN ap_headers ah ON (ah.cid = ad.cid AND ah.id = ad.ap_header_id)
								LEFT JOIN leases l ON ( gd.cid = l.cid AND l.id=gd.lease_id )
								LEFT JOIN customers c ON ( l.cid = c.cid AND c.id = l.primary_customer_id )
								LEFT JOIN lease_customers lc ON ( lc.cid = ah.cid AND lc.id=ah.lease_customer_id )
								LEFT JOIN customers c1 ON ( c1.cid = lc.cid AND  c1.id = lc.customer_id )
								LEFT JOIN property_units pu ON ( l.cid = pu.cid AND pu.id = l.property_unit_id )
                                JOIN accounting_export_batches aeb ON ( aea.cid = aeb.cid AND aea.accounting_export_batch_id = aeb.id )
                                LEFT JOIN accounting_export_schedulers aes ON ( aes.cid = aeb.cid AND aeb.accounting_export_scheduler_id =  aes.id  )
								LEFT JOIN ap_payees ap ON ( ah.ap_payee_id = ap.id AND ah.cid = ap.cid)';
		if( CTransmissionVendor::GL_EXPORT_YARDI == $intGlExportBatchTypeId ) {
			$strJoins .= 'JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'YARDI_PROPERTY_GL_BU\' AND pp.value IS NOT NULL )';
		}

		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForOnesiteFormat( $intGlExportBookTypeId, $strCheckDeletedUnitsSql ) {
		$strSelect  = 'DISTINCT ON(  gd.property_id,
						 gh.post_date,
						 gh.id,gd.amount,
						 gd.id ) gd.id, gh.id as gl_headers_id,';
		$strSelect  .= $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_ONESITE,  $intGlExportBookTypeId );
		$strSelect  .= ',gh.reference,ap.payment_number,
							gtt.id as transaction_type_id,
							gd.reference_id,
							aa.gl_transaction_type_id as ap_allocation_transaction_type_id,
							ah.header_number as ap_header_number,
							CASE
							WHEN ( gd.ap_detail_id IS NOT NULL) THEN ah.header_memo
							WHEN ( gh.reference_id = at.id AND gh.cid = at.cid AND gh.gl_transaction_type_id = ' . CGlTransactionType::AR_CHARGE . ' ) THEN at.memo
							ELSE gh.memo
							END AS memo,
							CASE
							WHEN ( gd.ap_detail_id IS NOT NULL) THEN ad.description
							ELSE gd.memo
							END AS description,
							gh.reference AS ref, pp.value as journal, pp1.value as location, gtt.name as transaction_type, gh.post_date AS postdate,
							ac.name || \'-\' || c.name_last || \',\' || c.name_first || \'-\' || pu.unit_number AS memo_onsite';
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strJoins   .= ' JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'ONE_SITE_JOURNAL_CODE\' AND pp.value IS NOT NULL )
								 LEFT JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key = \'ONE_SITE_PROPERTY_GL_BU\' AND pp1.value IS NOT NULL )
								 LEFT JOIN ar_transactions at ON ( gh.reference_id = at.id AND gh.cid = at.cid AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
								 LEFT JOIN gl_transaction_types gtt ON ( gd.gl_transaction_type_id = gtt.id )
								 LEFT JOIN leases l ON ( at.lease_id = l.id AND at.cid = l.cid )
								 LEFT JOIN lease_customers lc ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
								 LEFT JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
								 LEFT JOIN ar_codes ac ON ( ac.cid = at.cid AND ac.id = at.ar_code_id )
								 LEFT JOIN ap_headers ah ON ( ah.cid = gd.cid AND gd.reference_id = ah.id AND ah.gl_transaction_type_id = 2 )
								 LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND gd.ap_detail_id = ad.id AND ah.id = ad.ap_header_id )
								 LEFT JOIN ap_allocations aa ON ( aa.cid = gd.cid AND aa.id = gd.reference_id )
								 LEFT JOIN ap_details ad1 ON ( aa.cid = ad1.cid AND aa.credit_ap_detail_id = ad1.id )
								 LEFT JOIN ap_headers ah1 ON ( ah1.cid = ad1.cid AND ah1.id = ad1.ap_header_id )
								 LEFT JOIN ap_payments ap ON ( ah1.cid = ap.cid AND ah1.ap_payment_id = ap.id )
								 LEFT JOIN property_units pu ON ( l.cid = pu.cid AND l.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' ) ';
		$strOrderBy .= ',gh.id,gd.amount';
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;

	}

	public function buildDetailsSqlForAmsiFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_AMSI,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect .= ',gh.reference,gh.memo,pp1.value as entity_id';
		$strJoins .= 'JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id  AND pp1.key = \'AMSI_ENTITY_ID\' AND pp1.value IS NOT NULL )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForJdEdwardsFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_JD_EDWARDS,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect .= ',gh.reference,gh.memo,pp.value as jde_number, CASE
		                            		WHEN ( gd.memo IS NOT NULL) THEN gd.memo
											WHEN ( gh.memo IS NOT NULL) THEN gh.memo
											WHEN ( gh.reference IS NOT NULL) THEN gh.reference
		                   	    		 END AS memo_jd_edwards';
		$strJoins .= 'JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id  AND pp.key = \'JDE_NUMBER\' AND pp.value IS NOT NULL)';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForOracleFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_ORACLE,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();

		$strSelect  .= ',gh.reference, gh.memo,ac.name as ar_code_name';
		$strJoins   .= 'LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
		    					LEFT JOIN ar_codes ac ON ( ac.cid = art.cid AND ac.id = art.ar_code_id )';
		$strWhere   .= 'AND gd.amount <> 0';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForGreatPlainsFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();

		$strSelect .= ',ad.deposit_number,ad.transaction_datetime as deposite_date,ad.post_month as deposit_post_month,gh.reference_id,
			CASE
         WHEN gh.header_number IS NOT NULL THEN \'GJ#\' || gh.header_number || \' - \' || gh.reference
         WHEN gtty.id IN ( ' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ' ) THEN gh.reference
         ELSE COALESCE(\'Check #\' || ap_ref.payment_number, gh.reference, \'Check #\' || ap.payment_number)
         END AS reference,
         ac.name,gh.memo';

		$strJoins .= 'JOIN gl_transaction_types gtty ON (gh.gl_transaction_type_id = gtty.id)
								LEFT JOIN ar_deposits ad ON ( gd.cid = ad.cid AND gd.reference_id = ad.id AND gh.gl_transaction_type_id = ad.gl_transaction_type_id )
								LEFT JOIN ar_allocations ara ON (ara.cid = gh.cid AND ara.id = gh.reference_id AND gh.gl_transaction_type_id IN (' . CGlTransactionType:: AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ',' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ',' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ') )
								LEFT JOIN ar_transactions art ON (gh.cid = art.cid AND (gh.reference_id = art.id OR art.id = ara.credit_ar_transaction_id) AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType:: AR_CHARGE . ', ' . CGlTransactionType::AR_PAYMENT . ', ' . CGlTransactionType::AR_DEPOSIT_HELD . ',' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) )
								LEFT JOIN ar_codes ac ON (ac.cid = art.cid AND ac.id = art.ar_code_id)
								LEFT JOIN ap_allocations aa ON (gh.cid = aa.cid AND gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN (' . CGlTransactionType:: AP_ALLOCATION . ', ' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ' ))
								LEFT JOIN ap_details ad2 ON (aa.cid = ad2.cid AND aa.credit_ap_detail_id = ad2.id)
								
								LEFT JOIN ap_headers ah ON (ad2.cid = ah.cid AND ad2.ap_header_id = ah.id AND ad2.gl_transaction_type_id = ah.gl_transaction_type_id AND ad2.post_month = ah.post_month)
								LEFT JOIN ap_headers ah1 ON (ah1.cid = aa.cid AND ah1.id = aa.lump_ap_header_id)
								
								LEFT JOIN ap_payments ap_ref ON (ap_ref.cid = ah.cid AND ap_ref.id = ah.ap_payment_id)
								LEFT JOIN ap_payments ap ON (ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id)';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForYardiEtlFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_YARDI_ETL,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = 'gd.id ASC';
		$arrintGlTransactionTypesForVendorPayments = array_diff( CGlTransactionType::$c_arrintApGlTransactionTypeIds, [ CGlTransactionType::AP_CHARGE ] );
		$arrintApCheckPaymentTypes = [ CApPaymentType::CHECK, CApPaymentType::ACH, CApPaymentType::WRITTEN_CHECK, CApPaymentType::BILL_PAY_CHECK ];

		$strSelect .= ',gd.gl_transaction_type_id,
							gh.reference AS yardi_reference,
							cl.name_first,
							cl.name_last,
							cl.unit_number_cache,
							p.id as property_id,
							ad.description,
							ah.lease_customer_id,
							ah3.ap_payee_id,
							cust.name_first AS c1_name_first,
							cust.name_last AS c1_name_last,
							app.company_name,
							CASE
								WHEN ( gd.memo IS NOT NULL ) THEN gd.memo
								WHEN ( gh.memo IS NOT NULL ) THEN gh.memo
								WHEN ( gh.reference IS NOT NULL ) THEN gh.reference
							END AS memo_yardi,
							ar.name as charge_code_name,
			CASE
					WHEN gh.header_number IS NOT NULL THEN \'GJ#\' || COALESCE(gh.header_number, 0) || \' - \' || COALESCE(gh.reference, \'\')
					WHEN gtty.id IN (' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ') THEN gh.reference
					WHEN ap_ref.ap_payment_type_id IN (' . implode( ', ', $arrintApCheckPaymentTypes ) . ') OR ap.ap_payment_type_id IN ( ' . implode( ', ', $arrintApCheckPaymentTypes ) . ' ) THEN COALESCE(\'Check #\' || ap_ref.payment_number, gh.reference, \'Check #\' || ap.payment_number)
					ELSE COALESCE(\'Payment #\' || ap_ref.payment_number, gh.reference, \'Payment #\' || ap.payment_number)
					END AS reference';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSelect .= ',gat.formatted_account_number || \': \' || gat.name AS gl_account';
		}
		$strSelect .= ',( CASE
						WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' ) THEN ard.deposit_memo
						WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_ALLOCATION . ' ) THEN art1.memo
						WHEN art.id IS NOT NULL THEN ac.name
						WHEN gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' THEN gd.memo
						WHEN ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::BANK_ACCOUNT . ' ) AND gh.gl_transaction_type_id IN ( ' . implode( ', ', $arrintGlTransactionTypesForVendorPayments ) . ' ) THEN NULL
						ELSE COALESCE( ad1.description, ad.description, ah.header_memo, gh.memo )
					END ) || \' \' ::TEXT AS detail_memo,
			 CASE
						WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AP_CHARGE . ', ' . CGlTransactionType::AP_PAYMENT . ', ' . CGlTransactionType::AP_ALLOCATION . ', ' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ' )
						THEN ( CASE
								WHEN lc.id IS NOT NULL THEN func_format_customer_name( cust.name_first, cust.name_last, cust.company_name )
								ELSE app.company_name
							END )
						ELSE func_format_customer_name( cl.name_first, cl.name_last, cl.company_name )
					END AS resident,gh.memo';

		$strJoins .= 'LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id )
					LEFT JOIN ar_codes ar ON ( ar.cid = gd.cid AND ar.id = art.ar_code_id )
					LEFT JOIN ap_allocations aa ON (gh.cid = aa.cid AND gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN (' . CGlAccountType::INCOME . ', ' . CGlAccountType::EXPENSES . '))
					LEFT JOIN ap_headers ah1 ON (ah1.cid = aa.cid AND ah1.id = aa.lump_ap_header_id)
					LEFT JOIN ap_details ad ON (aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id)
					JOIN gl_transaction_types gtty ON (gh.gl_transaction_type_id = gtty.id)
					LEFT JOIN ap_headers ah ON (ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month)
					LEFT JOIN ap_payments ap_ref ON (ap_ref.cid = ah.cid AND ap_ref.id = ah.ap_payment_id)
					LEFT JOIN ap_payments ap ON (ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id)
					LEFT JOIN ar_allocations ara ON ( ara.cid = gh.cid AND ara.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ' ) )
					LEFT JOIN ar_transactions art1 ON ( gh.cid = art1.cid AND art1.id = COALESCE ( ara.charge_ar_transaction_id, art.id ) )
					LEFT JOIN ar_transactions art2 ON ( gh.cid = art2.cid AND ( gh.reference_id = art2.id OR art2.id = ara.credit_ar_transaction_id ) AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_PAYMENT . ', ' . CGlTransactionType::AR_DEPOSIT_HELD . ', ' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) )
					LEFT JOIN ar_deposits ard ON ( gh.cid = ard.cid AND gh.reference_id = ard.id AND gh.gl_transaction_type_id = ard.gl_transaction_type_id )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			$strJoins .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ( ga.id = gd.accrual_gl_account_id OR ga.id = gd.cash_gl_account_id ) )';
		} else {
			if( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) {
				$strJoins .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.accrual_gl_account_id )';
			} else {
				$strJoins .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.cash_gl_account_id )';
			}
		}
		$strJoins .= ' LEFT JOIN ap_details ad1 ON ( ad1.cid = COALESCE( gd.cid, aa.cid ) AND ad1.id = COALESCE( gd.ap_detail_id, aa.charge_ap_detail_id ) )
					LEFT JOIN ar_codes ac ON ( ac.cid = art2.cid AND ac.id = art2.ar_code_id )
					LEFT JOIN ap_headers ah2 ON ( ad1.cid = ah2.cid AND ad1.ap_header_id = ah.id AND ad1.gl_transaction_type_id = ah2.gl_transaction_type_id AND ad1.post_month = ah2.post_month )
					LEFT JOIN lease_customers lc ON ( lc.cid = COALESCE ( ah2.cid,ah.cid ) AND lc.id = COALESCE ( ah2.lease_customer_id,ah.lease_customer_id ) )
					LEFT JOIN customers cust ON ( cust.cid = lc.cid AND cust.id = lc.customer_id )
					LEFT JOIN ap_details ad3 ON ( ad3.cid = gd.cid AND ad3.id = gd.ap_detail_id )
					LEFT JOIN ap_headers ah3 ON ( ah3.cid = ad3.cid AND ah3.id = ad3.ap_header_id )
					LEFT JOIN ap_payees app ON ( app.cid = ah3.cid AND app.id = ah3.ap_payee_id )
					LEFT JOIN cached_leases cl ON ( cl.cid = gd.cid AND cl.id = gd.lease_id )';
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForYardiSevenSFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId ) {
		$strSelect  = $this->getCommonSelectFields( $intGlExportBatchTypeId,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strWhere .= ' AND gd.amount != 0 ';
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect  .= ',DENSE_RANK() over( ORDER BY gh.post_month,gh.post_date ASC ) as record_sequence, ad.description,gh.reference,gh.memo, gtt.description as transaction_type_description';
		$strJoins   .= 'LEFT JOIN ap_details ad ON (ad.cid = gd.cid AND ad.id = gd.ap_detail_id)
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )';

		$strOrderBy .= ',gh.post_month';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForYardiSevenSMxFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId ) {
		$strSelect  = $this->getCommonSelectFields( $intGlExportBatchTypeId,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect  .= ',DENSE_RANK() over( ORDER BY gh.post_month,gh.post_date ASC ) as record_sequence, 
						ad.description,
						gh.reference,
						gh.memo, 
						gtt.description as transaction_type_description, 
						gh.reference_id, 
						gh.cid,
						gtt.id as gl_transaction_type_id';
		$strJoins   .= 'LEFT JOIN ap_details ad ON (ad.cid = gd.cid AND ad.id = gd.ap_detail_id)
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )';
		$strOrderBy .= ',gh.post_month';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		$strTempSql = ' WITH main_sql AS 
							( SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy . ' ),
							subTe AS (
								SELECT te.external_property_lookup_code,
								teac.ar_code_id,
								teac.cid,
								te.property_id
							FROM 
								taxable_entity_ar_codes teac
								JOIN taxable_entities te ON (teac.cid = te.cid AND teac.taxable_entity_id = te.id)
							WHERE 
								teac.cid = ' . ( int ) $this->getCid() . '
								AND teac.transmission_type_id = ' . CTransmissionType::GL_EXPORT . ' ) ';
		if( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) {
			$strAccountSql = 'ms.accrual_gl_account_id,';
		} elseif( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
			$strAccountSql = ' ms.cash_gl_account_id,';
		} else {
			$strAccountSql = ' ms.cash_gl_account_id, ms.accrual_gl_account_id,';
		}
		$strSql = ' SELECT 
						ms.id,
						ROW_NUMBER() OVER( PARTITION BY ms.id ORDER BY ms.id DESC ) as id_sequence,
						ms.property_id,
						ms.gl_header_id,
						' . $strAccountSql . '
						ms.lease_id,
						ms.transaction_datetime,
						ms.post_month,
						ms.post_date as transaction_post_date,
						ms.property_name,
						ms.lookup_code,
						ms.header_number,
						ms.post_date,
						ms.record_sequence,
						ms.description,
						ms.reference,
						ms.memo,
						ms.transaction_type_description as transaction_type_description,
						CASE
						 WHEN ms.gl_transaction_type_id IN ( ' . \CGlTransactionType::AR_PAYMENT . ', ' . \CGlTransactionType::AR_DEPOSIT . ' ) AND aa.allocation_amount != 0 AND ms.amount > 0 THEN aa.allocation_amount * - 1
						 WHEN ms.gl_transaction_type_id IN ( ' . \CGlTransactionType::AR_PAYMENT . ', ' . \CGlTransactionType::AR_DEPOSIT . ' ) AND aa.allocation_amount != 0 AND ms.amount < 0 THEN aa.allocation_amount
						 ELSE ms.amount
						END as amount,
						subTe.external_property_lookup_code
					FROM 
						main_sql as ms
						LEFT JOIN ar_deposit_transactions adt ON ( adt.cid = ms.cid AND ms.property_id = adt.property_id AND adt.ar_deposit_id = ms.reference_id )
						LEFT JOIN ar_transactions paymentat ON ( ms.cid = paymentat.cid AND ms.property_id = paymentat.property_id AND ( ( ms.lease_id = paymentat.lease_id AND  ms.reference_id = paymentat.id ) OR ( paymentat.cid = adt.cid AND paymentat.id =
						adt.ar_transaction_id ) ) and paymentat.ar_payment_id IS NOT NULL )
						LEFT JOIN ar_allocations aa ON ( aa.cid = ms.cid AND ms.property_id = aa.property_id AND ( ( ms.lease_id = aa.lease_id AND aa.id = ms.reference_id ) OR ( aa.cid = paymentat.cid AND paymentat.id =
						aa.credit_ar_transaction_id ) ) )
						LEFT JOIN ar_transactions chargeat ON ( chargeat.cid = ms.cid AND ms.property_id = chargeat.property_id AND chargeat.ar_payment_id IS NULL AND ( ( chargeat.id = ms.reference_id AND ms.lease_id = chargeat.lease_id ) OR ( aa.cid = chargeat.cid AND
						chargeat.id = aa.charge_ar_transaction_id ) ) )
						LEFT JOIN ar_transactions creditat ON ( creditat.cid = ms.cid AND ms.property_id = creditat.property_id AND
						( (aa.cid = creditat.cid AND creditat.id = aa.credit_ar_transaction_id ) ) AND creditat.ar_payment_id IS NULL AND creditat.is_reversal IS FALSE AND creditat.is_deleted IS FALSE AND creditat.transaction_amount < 0 )
						LEFT JOIN ar_codes ac ON ( ac.cid = COALESCE( creditat.cid, chargeat.cid ) AND ac.id = COALESCE( creditat.ar_code_id,chargeat.ar_code_id ) )
						LEFT JOIN subTe ON ( ac.id = subTe.ar_code_id AND ac.cid = subTe.cid AND chargeat.property_id = subTe.property_id )
					WHERE 
						ms.cid = ' . ( int ) $this->getCid() . '
					ORDER BY ms.id DESC';

		return $strTempSql . $strSql;
	}

	public function buildDetailsSqlForYardiSevenSInternationalFormat( $intGlExportBookTypeId, $intGlExportBatchTypeId ) {
		$strSelect  = $this->getCommonSelectFields( $intGlExportBatchTypeId,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
		$strSelect  .= ',DENSE_RANK() over( ORDER BY gh.post_month,gh.post_date ASC ) as record_sequence, ad.description,gh.reference,gh.memo, gtt.description as transaction_type_description, c.name_full';
		$strJoins   .= 'LEFT JOIN ap_details ad ON (ad.cid = gd.cid AND ad.id = gd.ap_detail_id)
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )
						LEFT JOIN leases l ON (gh.cid = l.cid AND l.id = gh.lease_id AND l.occupancy_type_id = ' . COccupancyType::OTHER_INCOME . ' AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArGlTransactionTypeIds ) . ' ) )
						LEFT JOIN lease_customers lc ON (lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						LEFT JOIN customers c ON (c.cid = lc.cid AND c.id = lc.customer_id)';
		$strOrderBy .= ',gh.post_month';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strOrderBy .= ', gat.name';
		}
		return ' SELECT ' . $strSelect . ' FROM ' . $strJoins . ' WHERE ' . $strWhere . ' ORDER BY ' . $strOrderBy;
	}

	public function buildDetailsSqlForYardiEtlMrFormat( $intGlExportBookTypeId ) {
		$strSelect  = $this->getCommonSelectFields( CTransmissionVendor::GL_EXPORT_YARDI_ETL_MR,  $intGlExportBookTypeId );
		$strJoins   = $this->getCommonJoins( $intGlExportBookTypeId );
		$strWhere   = $this->getCommonWhereClause( $intGlExportBookTypeId );
		$strOrderBy = $this->getCommonOrderBy();
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;
		if( true == is_null( $this->getTransmissionVendorId() ) ) {
			$boolIsValid = false;
			$this->m_arrstrMissingRequiredFields[] = __( 'Export batch type' );
		}
		return $boolIsValid;
	}

	public function valAccountingExportSchedulerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountingExportFileFormatTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlChartId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSuccess() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileNamePrefix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileNameSuffix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastAttemptedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReattemptCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedThrough() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonthRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valAccountingExportBatchTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->valTransmissionVendorId() ) || 0 >= ( int ) $this->valTransmissionVendorId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_expor_batch_type_id', __( 'Export type is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function export( $intGlExportFormatTypeId, $intGlExportBookTypeId, $intCompanyUserId, $objClientDatabase, $arrmixGlExportFileInput = [] ) {

		$boolIsBookMethodToBoth = isset( $arrmixGlExportFileInput['book_method_to_both'] ) ? $arrmixGlExportFileInput['book_method_to_both'] : 1; // yardi
		$boolIsIncludeHeader    = isset( $arrmixGlExportFileInput['include_header'] ) ? $arrmixGlExportFileInput['include_header'] : NULL; // Jenark , yardi
		$boolIsFromDownload     = isset( $arrmixGlExportFileInput['is_from_download'] ) ? $arrmixGlExportFileInput['is_from_download'] : false; // people_soft, mri, yardi
		$objGlDetailsFilter     = isset( $arrmixGlExportFileInput['gl_details_filter'] ) ? $arrmixGlExportFileInput['gl_details_filter'] : NULL; // people soft
		$intSummarizationMethod = isset( $arrmixGlExportFileInput['summarization_method'] ) ? $arrmixGlExportFileInput['summarization_method'] : 1; // mri
		$arrmixConnectionData   = isset( $arrmixGlExportFileInput['connection_data'] ) ? $arrmixGlExportFileInput['connection_data'] : NULL; // all except people soft
		$arrmixExportData       = isset( $arrmixGlExportFileInput['export_data'] ) ? $arrmixGlExportFileInput['export_data'] : NULL; // all except people soft
		$arrmixExportedData     = [];

		$this->m_objExportsFileLibrary = new CExportsFileLibrary( CTransmissionType::GL_EXPORT );
		switch( $this->getTransmissionVendorId() ) {

			case CTransmissionVendor::GL_EXPORT_QUICK_BOOKS:
				$arrmixExportedData = $this->exportInQuickBooksFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_SKYLINE:
				$arrmixExportedData = $this->exportInSkylineFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM:
				$arrmixExportedData = $this->exportInTimberlinePmFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $intSummarizationMethod );
				break;

			case CTransmissionVendor::GL_EXPORT_PEOPLESOFT:
				if( false == $this->exportInPeopleSoftFormat( $objClientDatabase, $boolIsFromDownload, $intCompanyUserId, $objGlDetailsFilter ) ) {
					return false;
				}
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI:
				$arrmixExportedData = $this->exportInYardiFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $boolIsFromDownload, $boolIsBookMethodToBoth, $boolIsIncludeHeader, $intCompanyUserId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_PEAK:
				$arrmixExportedData = $this->exportInPeakFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_ORACLE:
				$arrmixExportedData = $this->exportInOracleFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_ETL:
			case CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS:
				$arrmixExportedData = $this->exportInYardiEtlFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $this->getTransmissionVendorId() );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX:
			case CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL:
				$arrmixExportedData = $this->exportInYardiGlExportSevenSFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $this->getTransmissionVendorId() );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_ETL_MR:
				$arrmixExportedData = $this->exportInYardiEtlMrFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI_CA:
				$arrmixExportedData = $this->exportInYardiCaFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData );
				break;

			default:
				trigger_error( 'Unexpected gl export batch type.', E_USER_ERROR );
				break;
		}
		return $arrmixExportedData;
	}

	public function exportInQuickBooksFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrmixAccountingExportBatchDetails = \Psi\Eos\Entrata\CGlDetails::createService()->buildSummarySqlForQuickBooksFormat( $this->getId(), $this->getCid(), $objClientDatabase, $intGlExportBookTypeId, $this->getGlTreeId() );
			if( false == valArr( $arrmixAccountingExportBatchDetails ) ) {
				return false;
			}
			$arrmixCashGlDetails    = [];
			$arrmixAccrualGlDetails = [];
			$arrintGlAccountId = [];
			$arrobjAccountDetails = [];
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixAccountingExportBatchDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixAccountingExportBatchDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixAccountingExportBatchDetails;
				}
			}
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$strPostdate = NULL;
				$intIndex = 0;
				$this->m_strFileContent = '';
				$this->m_strFileContent .= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!ENDTRNS\n";
				$arrmixDepositTypeCashTransactions = [];
				$arrmixPaymentTypeCashTransactions = [];
				$boolIsTransactionsAdded = false;
				foreach( $arrmixCashGlDetails as $arrmixAccountingExportBatchDetail ) {
					if( 0 == $arrmixAccountingExportBatchDetail['amount'] ) continue;
					if( false != valStr( $arrmixAccountingExportBatchDetail['deposit_number'] ) ) {
						$arrmixDepositTypeCashTransactions[] = $arrmixAccountingExportBatchDetail;
						continue;
					}
					if( false != valStr( $arrmixAccountingExportBatchDetail['payment_number'] ) ) {
						$arrmixPaymentTypeCashTransactions[] = $arrmixAccountingExportBatchDetail;
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']];
						$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];

					if( NULL != $strPostdate && false != valStr( $strPostdate ) && $arrmixAccountingExportBatchDetail['post_date'] != $strPostdate ) {
						$this->m_strFileContent .= "ENDTRNS\n \n";
						$intIndex = 0;
					}
					if( 0 == $intIndex ) {
						$this->m_strFileContent .= "TRNS\t";
					} else {
						$this->m_strFileContent .= "SPL\t";
					}

					$strMemo = NULL;
					$strDocNum = NULL;
					if( false != valStr( $arrmixAccountingExportBatchDetail['invoice_number'] ) ) {
						$strDocNum = $arrmixAccountingExportBatchDetail['invoice_number'];
						$strMemo = $arrmixAccountingExportBatchDetail['customer_name'] ?? $arrmixAccountingExportBatchDetail['vendor_name'];
						$strMemo .= ' ' . $arrmixAccountingExportBatchDetail['invoice_number'];
						if( false != $arrmixAccountingExportBatchDetail['line_description'] ) {
							$strMemo .= '-' . $arrmixAccountingExportBatchDetail['line_description'];
						}
					} elseif( false != valStr( $arrmixAccountingExportBatchDetail['header_number'] ) ) {
						$strDocNum = 'JE#' . $arrmixAccountingExportBatchDetail['header_number'];
						$strMemo = '';
					} else {
						$strDocNum = 'ENTRATA AR [ImportDate ' . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['transaction_datetime'] ) ) . ']';
						$strMemo = $arrmixAccountingExportBatchDetail['account_name'];
					}

					$strMemo = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
					$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
					$strPostdate = $arrmixAccountingExportBatchDetail['post_date'];
					$boolIsTransactionsAdded = true;
					$intIndex++;
				}

				if( false == valArr( $arrmixDepositTypeCashTransactions ) && false == valArr( $arrmixPaymentTypeCashTransactions ) ) {
					$this->m_strFileContent .= "ENDTRNS\n";
				}
				$arrmixDepositCashTransactions = rekeyArray( 'deposit_number', $arrmixDepositTypeCashTransactions, false, true );
				// reset the memory
				$arrmixAccountingExportBatchDetail = NULL;
				$arrmixDepositTypeCashTransactions = NULL;
				// render the deposit type of transactions
				if( false != valArr( $arrmixDepositCashTransactions ) ) {
					if( false != $boolIsTransactionsAdded ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					$boolIsTransactionsAdded = true;
					foreach( $arrmixDepositCashTransactions as $arrmixDepositTypeCashTransactions ) {
						$strDepositNumber       = NULL;
						$intIndex               = 0;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixDepositTypeCashTransactions as $arrmixAccountingExportBatchDetail ) {
							if( NULL != $strDepositNumber && false != valStr( $strDepositNumber ) && $arrmixAccountingExportBatchDetail['deposit_number'] != $strDepositNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex               = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}

							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']];
								$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
							}
							$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];
							$strDocNum              = 'Deposit #' . $arrmixAccountingExportBatchDetail['deposit_number'];
							$strMemo                = '';
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
							$strDepositNumber       = $arrmixAccountingExportBatchDetail['deposit_number'];
							$intIndex++;
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}

				$arrmixPaymentCashTransactions = rekeyArray( 'payment_number', $arrmixPaymentTypeCashTransactions, false, true );
				// reset the memory
				$arrmixPaymentTypeCashTransactions = NULL;
				// render the payment type of transactions
				if( false != valArr( $arrmixPaymentCashTransactions ) ) {
					if( false != $boolIsTransactionsAdded && false == valArr( $arrmixDepositCashTransactions ) ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					foreach( $arrmixPaymentCashTransactions as $arrmixPaymentTypeCashTransactions ) {
						$strPaymentNumber       = NULL;
						$intIndex               = 0;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixPaymentTypeCashTransactions as $arrmixAccountingExportBatchDetail ) {
							if( NULL != $strPaymentNumber && false != valStr( $strPaymentNumber ) && $arrmixAccountingExportBatchDetail['payment_number'] != $strPaymentNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex               = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}

							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['cash_gl_account_id']];
								$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
							}

							$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];
							if( false != valStr( $arrmixAccountingExportBatchDetail['payment_number'] ) ) {
								$strDocNum = 'AP Payment #' . $arrmixAccountingExportBatchDetail['payment_number'];
								$strMemo   = $arrmixAccountingExportBatchDetail['customer_name'] ?? $arrmixAccountingExportBatchDetail['vendor_name'];
								if( false != valStr( $arrmixAccountingExportBatchDetail['invoice_number'] ) ) {
									$strMemo .= ' ' . implode( ',', array_unique( explode( ',', $arrmixAccountingExportBatchDetail['invoice_number'] ) ) );
								}
								if( false != valStr( $arrmixAccountingExportBatchDetail['payment_line_description'] ) ) {
									$strMemo .= '-' . $arrmixAccountingExportBatchDetail['payment_line_description'];
								}
							}
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
							$strPaymentNumber       = $arrmixAccountingExportBatchDetail['payment_number'];
							$intIndex++;
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;

			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$strPostdate = NULL;
				$intIndex = 0;
				$this->m_strFileContent = '';
				$this->m_strFileContent .= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!ENDTRNS\n";
				$arrmixDepositTypeAccrualTransactions = [];
				$arrmixPaymentTypeAccrualTransactions = [];
				$boolIsTransactionsAdded = false;
				foreach( $arrmixAccrualGlDetails as $arrmixAccountingExportBatchDetail ) {
					if( 0 == $arrmixAccountingExportBatchDetail['amount'] ) continue;
					if( false != valStr( $arrmixAccountingExportBatchDetail['deposit_number'] ) ) {
						$arrmixDepositTypeAccrualTransactions[] = $arrmixAccountingExportBatchDetail;
						continue;
					}

					if( false != valStr( $arrmixAccountingExportBatchDetail['payment_number'] ) ) {
						$arrmixPaymentTypeAccrualTransactions[] = $arrmixAccountingExportBatchDetail;
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']];
						$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];

					if( NULL != $strPostdate && false != valStr( $strPostdate ) && $arrmixAccountingExportBatchDetail['post_date'] != $strPostdate ) {
						$this->m_strFileContent .= "ENDTRNS\n \n";
						$intIndex = 0;
					}
					if( 0 == $intIndex ) {
						$this->m_strFileContent .= "TRNS\t";
					} else {
						$this->m_strFileContent .= "SPL\t";
					}

					$strMemo = NULL;
					$strDocNum = NULL;
					if( false != valStr( $arrmixAccountingExportBatchDetail['invoice_number'] ) ) {
						$strDocNum = $arrmixAccountingExportBatchDetail['invoice_number'];
						$strMemo = $arrmixAccountingExportBatchDetail['customer_name'] ?? $arrmixAccountingExportBatchDetail['vendor_name'];
						if( false != $arrmixAccountingExportBatchDetail['line_description'] ) {
							$strMemo .= '-' . $arrmixAccountingExportBatchDetail['line_description'];
						}
					} elseif( false != valStr( $arrmixAccountingExportBatchDetail['header_number'] ) ) {
						$strDocNum = 'JE#' . $arrmixAccountingExportBatchDetail['header_number'];
						$strMemo = '';
					} else {
						$strDocNum = 'ENTRATA AR [ImportDate ' . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['transaction_datetime'] ) ) . ']';
						$strMemo = $arrmixAccountingExportBatchDetail['account_name'];
					}
					$strMemo = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
					$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
					$strPostdate = $arrmixAccountingExportBatchDetail['post_date'];
					$boolIsTransactionsAdded = true;
					$intIndex++;
				}
				if( false == valArr( $arrmixDepositTypeAccrualTransactions ) && false == valArr( $arrmixPaymentTypeAccrualTransactions ) ) {
					$this->m_strFileContent .= "ENDTRNS\n";
				}

				$arrmixDepositAccrualTransactions = rekeyArray( 'deposit_number', $arrmixDepositTypeAccrualTransactions, false, true );

				// reset the memory
				$arrmixAccountingExportBatchDetail = NULL;
				$arrmixDepositTypeAccrualTransactions = NULL;
				// render the deposit type of transactions
				if( false != valArr( $arrmixDepositAccrualTransactions ) ) {
					if( false != $boolIsTransactionsAdded ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					$boolIsTransactionsAdded = true;
					foreach( $arrmixDepositAccrualTransactions as $arrmixDepositTypeAccrualTransactions ) {
						$strDepositNumber = NULL;
						$intIndex = 0;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixDepositTypeAccrualTransactions as $arrmixAccountingExportBatchDetail ) {
							if( NULL != $strDepositNumber && false != valStr( $strDepositNumber ) && $arrmixAccountingExportBatchDetail['deposit_number'] != $strDepositNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']];
								$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
							}
							$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];
							$strDocNum = 'Deposit #' . $arrmixAccountingExportBatchDetail['deposit_number'];
							$strMemo = '';
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
							$strDepositNumber = $arrmixAccountingExportBatchDetail['deposit_number'];
							$intIndex++;
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}
				}

				$arrmixPaymentAccrualTransactions = rekeyArray( 'payment_number', $arrmixPaymentTypeAccrualTransactions, false, true );
				// reset the memory
				$arrmixAccountingExportBatchDetail = NULL;
				$arrmixPaymentTypeAccrualTransactions = NULL;
				// render the deposit type of transactions
				if( false != valArr( $arrmixPaymentAccrualTransactions ) ) {
					if( false != $boolIsTransactionsAdded && false == valArr( $arrmixDepositAccrualTransactions ) ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					foreach( $arrmixPaymentAccrualTransactions as $arrmixPaymentTypeAccrualTransactions ) {
						$strPaymentNumber = NULL;
						$intIndex = 0;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixPaymentTypeAccrualTransactions as $arrmixAccountingExportBatchDetail ) {
							if( NULL != $strPaymentNumber && false != valStr( $strPaymentNumber ) && $arrmixAccountingExportBatchDetail['payment_number'] != $strPaymentNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}

							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixAccountingExportBatchDetail['accrual_gl_account_id']];
								$arrmixAccountingExportBatchDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixAccountingExportBatchDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixAccountingExportBatchDetail['account_name'] = $objGlAccount->getName();
							}
							$strAccountNumber = ( true == valStr( $arrmixAccountingExportBatchDetail['secondary_number'] ) ) ?$arrmixAccountingExportBatchDetail['secondary_number'] : $arrmixAccountingExportBatchDetail['account_number'];
							if( false != valStr( $arrmixAccountingExportBatchDetail['payment_number'] ) ) {
								$strDocNum = 'AP Payment #' . $arrmixAccountingExportBatchDetail['payment_number'];
								$strMemo   = $arrmixAccountingExportBatchDetail['customer_name'] ?? $arrmixAccountingExportBatchDetail['vendor_name'];
								if( false != valStr( $arrmixAccountingExportBatchDetail['invoice_number'] ) ) {
									$strMemo .= ' ' . implode( ',', array_unique( explode( ',', $arrmixAccountingExportBatchDetail['invoice_number'] ) ) );
								}
								if( false != valStr( $arrmixAccountingExportBatchDetail['payment_line_description'] ) ) {
									$strMemo .= '-' . $arrmixAccountingExportBatchDetail['payment_line_description'];
								}
							}
							$strMemo = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixAccountingExportBatchDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . "\t" . $arrmixAccountingExportBatchDetail['amount'] . "\t" . $strDocNum . "\t" . $strMemo . "\t\n";
							$strPaymentNumber = $arrmixAccountingExportBatchDetail['payment_number'];
							$intIndex++;
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}
				}

				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		} else {

			$arrmixGlDetails = ( array ) $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_QUICK_BOOKS );
			if( false == valArr( $arrmixGlDetails ) ) {
				return false;
			}
			$arrmixCashGlDetails    = [];
			$arrmixAccrualGlDetails = [];
			$arrintGlAccountId = [];
			$arrobjAccountDetails = [];
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			$boolIsTransactionsAdded = false;
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$intGlHeaderId = 0;
				$this->m_strFileContent = '';
				$this->m_strFileContent .= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!ENDTRNS\n";
				$arrmixDepositTypeCashTransactions = [];
				$arrmixPaymentTypeCashTransactions = [];
				$intIndex = 0;
				foreach( $arrmixCashGlDetails as $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) continue;
					if( false != valStr( $arrmixGlDetail['deposit_number'] ) ) {
						$arrmixDepositTypeCashTransactions[] = $arrmixGlDetail;
						continue;
					}

					if( false != valStr( $arrmixGlDetail['payment_number'] ) ) {
						$arrmixPaymentTypeCashTransactions[] = $arrmixGlDetail;
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}

					if( $arrmixGlDetail['gl_header_id'] != $intGlHeaderId && $intGlHeaderId != 0 ) {
						$this->m_strFileContent .= "ENDTRNS\n \n";
						$intIndex = 0;
					}

					if( 0 == $intIndex ) {
						$this->m_strFileContent .= "TRNS\t";
					} else {
						$this->m_strFileContent .= "SPL\t";
					}

					$strMemo = $arrmixGlDetail['memo'];

					if( false != valStr( $arrmixGlDetail['invoice_number'] ) ) {
						$strMemo = $arrmixGlDetail['customer_name'] ?? $arrmixGlDetail['vendor_name'];
						if( false != $arrmixGlDetail['line_description'] ) {
							$strMemo .= '-' . $arrmixGlDetail['line_description'];
						}
					}
					$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );
					$strMemo = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $arrmixGlDetail['amount'] ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $strMemo . "\n";

					$intGlHeaderId = $arrmixGlDetail['gl_header_id'];
					$intIndex++;
					$boolIsTransactionsAdded = true;

				}
				if( false == valArr( $arrmixDepositTypeCashTransactions ) && false == valArr( $arrmixPaymentTypeCashTransactions ) ) {
					$this->m_strFileContent .= "ENDTRNS\n";
				}
				$arrmixDepositCashTransactions = rekeyArray( 'deposit_number', $arrmixDepositTypeCashTransactions, false, true );

				// resetting the memory
				$arrmixDepositTypeCashTransactions = NULL;
				if( false != valArr( $arrmixDepositCashTransactions ) ) {
					if( false != $boolIsTransactionsAdded ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					$boolIsTransactionsAdded = true;
					foreach( $arrmixDepositCashTransactions as $arrmixDepositTypeCashTransactions ) {
						$strReference           = NULL;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixDepositTypeCashTransactions as $intIndex => $arrmixGlDetail ) {
							if( $strReference != NULL && $arrmixGlDetail['reference'] != $strReference ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex               = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
							$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );
							$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $arrmixGlDetail['amount'] ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $arrmixGlDetail['memo'] . "\n";
							$strReference           = $arrmixGlDetail['reference'];
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}

				$arrmixPaymentCashTransactions = rekeyArray( 'payment_number', $arrmixPaymentTypeCashTransactions, false, true );
				// resetting the memory
				$arrmixPaymentTypeCashTransactions = NULL;
				if( false != valArr( $arrmixPaymentCashTransactions ) ) {
					if( false != $boolIsTransactionsAdded && false == valArr( $arrmixDepositCashTransactions ) ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					foreach( $arrmixPaymentCashTransactions as $arrmixPaymentTypeCashTransactions ) {
						$strPaymentNumber           = NULL;
						$intRank                    = NULL;
						$this->m_strFileContent .= "\n";
						$arrmixPaymentsWithRank = rekeyArray( 'rank', $arrmixPaymentTypeCashTransactions, false, true );
						foreach( $arrmixPaymentTypeCashTransactions as $intIndex => $arrmixGlDetail ) {
							$fltAmount = 0;
							$strPaymentDescription = NULL;
							if( $arrmixGlDetail['payment_number'] == $strPaymentNumber && $intRank == $arrmixGlDetail['rank'] && $arrmixGlDetail['cash_gl_account_id'] == $arrmixGlDetail['cash_credit_gl_account_id'] ) {
								continue;
							}

							$strPaymentDescription = $arrmixGlDetail['payment_line_description'];
							if( false != isset( $arrmixPaymentTypeCashTransactions[$intIndex + 1] ) && $arrmixGlDetail['payment_number'] == $arrmixPaymentTypeCashTransactions[$intIndex + 1]['payment_number'] && $arrmixPaymentTypeCashTransactions[$intIndex + 1]['rank'] == $arrmixGlDetail['rank'] && $arrmixGlDetail['cash_gl_account_id'] == $arrmixGlDetail['cash_credit_gl_account_id'] ) {
								$strPaymentDescription = NULL;
								foreach( $arrmixPaymentsWithRank[$arrmixGlDetail['rank']] as $arrmixPaymentWithRank ) {
									if( $arrmixPaymentWithRank['cash_gl_account_id'] == $arrmixPaymentWithRank['cash_credit_gl_account_id'] ) {
										$fltAmount += $arrmixPaymentWithRank['amount'];
									}

								}
							} else {
								$fltAmount = $arrmixGlDetail['amount'];
							}
							if( NULL != $strPaymentNumber && false != valStr( $strPaymentNumber ) && $arrmixGlDetail['payment_number'] != $strPaymentNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex = 0;
							}

							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}
							$strMemo = $arrmixGlDetail['memo'];
							if( false != valStr( $arrmixGlDetail['vendor_name'] ) ) {
								$strMemo .= ' ' . $arrmixGlDetail['vendor_name'];
							}

							if( false != valStr( $arrmixGlDetail['invoice_number'] ) ) {
								$strMemo .= ' ' . $arrmixGlDetail['invoice_number'];
							}
							if( false != valStr( $strPaymentDescription ) ) {
								$strMemo .= '-' . $strPaymentDescription;
							}

							$strMemo                = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
							$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );
							$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $fltAmount ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $strMemo . "\n";
							$strPaymentNumber           = $arrmixGlDetail['payment_number'];
							$intRank                    = $arrmixGlDetail['rank'];
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}

				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$intGlHeaderId = 0;
				$this->m_strFileContent = '';
				$this->m_strFileContent .= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
				$this->m_strFileContent .= "!ENDTRNS\n";
				$arrmixDepositTypeAccrualTransactions = [];
				$arrmixPaymentTypeAccrualTransactions = [];
				$intIndex = 0;
				$boolIsTransactionsAdded = false;
				foreach( $arrmixAccrualGlDetails as $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) continue;
					if( false != valStr( $arrmixGlDetail['deposit_number'] ) ) {
						$arrmixDepositTypeAccrualTransactions[] = $arrmixGlDetail;
						continue;
					}

					if( false != valStr( $arrmixGlDetail['payment_number'] ) ) {
						$arrmixPaymentTypeAccrualTransactions[] = $arrmixGlDetail;
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}

					if( $arrmixGlDetail['gl_header_id'] != $intGlHeaderId && $intGlHeaderId != 0 ) {
						$this->m_strFileContent .= "ENDTRNS\n \n";
						$intIndex = 0;
					}

					if( 0 == $intIndex ) {
						$this->m_strFileContent .= "TRNS\t";
					} else {
						$this->m_strFileContent .= "SPL\t";
					}

					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$strMemo = $arrmixGlDetail['memo'];

					if( false != valStr( $arrmixGlDetail['invoice_number'] ) ) {
						$strMemo = $arrmixGlDetail['customer_name'] ?? $arrmixGlDetail['vendor_name'];
						if( false != $arrmixGlDetail['line_description'] ) {
							$strMemo .= '-' . $arrmixGlDetail['line_description'];
						}
						if( false != valStr( $arrmixGlDetail['payment_line_description'] ) ) {
							$strMemo .= '-' . $arrmixGlDetail['payment_line_description'];
						}
					}
					$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );

					$strMemo = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );

					$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $arrmixGlDetail['amount'] ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $strMemo . "\n";

					$intGlHeaderId = $arrmixGlDetail['gl_header_id'];
					$intIndex++;
					$boolIsTransactionsAdded = true;
				}
				if( false == valArr( $arrmixDepositTypeCashTransactions ) && false == valArr( $arrmixPaymentTypeCashTransactions ) ) {
					$this->m_strFileContent .= "ENDTRNS\n";
				}
				$arrmixDepositAccrualTransactions = rekeyArray( 'deposit_number', $arrmixDepositTypeAccrualTransactions, false, true );

				// resetting the memory
				$arrmixDepositTypeAccrualTransactions = NULL;
				if( false != valArr( $arrmixDepositAccrualTransactions ) ) {
					if( false != $boolIsTransactionsAdded ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					$boolIsTransactionsAdded = true;
					foreach( $arrmixDepositAccrualTransactions as $arrmixDepositTypeAccrualTransactions ) {
						$strReference           = NULL;
						$this->m_strFileContent .= "\n";
						foreach( $arrmixDepositTypeAccrualTransactions as $intIndex => $arrmixGlDetail ) {
							if( $strReference != NULL && $arrmixGlDetail['reference'] != $strReference ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex               = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
							$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );
							$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $arrmixGlDetail['amount'] ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $arrmixGlDetail['memo'] . "\n";
							$strReference           = $arrmixGlDetail['reference'];
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}

				$arrmixPaymentAccrualTransactions = rekeyArray( 'payment_number', $arrmixPaymentTypeAccrualTransactions, false, true );
				// resetting the memory
				$arrmixPaymentTypeAccrualTransactions = NULL;
				if( false != valArr( $arrmixPaymentAccrualTransactions ) ) {
					if( false != $boolIsTransactionsAdded && false == valArr( $arrmixDepositAccrualTransactions ) ) {
						$this->m_strFileContent .= "ENDTRNS\n";
					}
					foreach( $arrmixPaymentAccrualTransactions as $arrmixPaymentTypeAccrualTransactions ) {
						$strPaymentNumber           = NULL;
						$intRank                    = NULL;
						$this->m_strFileContent .= "\n";
						$arrmixPaymentsWithRank = rekeyArray( 'rank', $arrmixPaymentTypeAccrualTransactions, false, true );

						foreach( $arrmixPaymentTypeAccrualTransactions as $intIndex => $arrmixGlDetail ) {

							$fltAmount = 0;
							$strPaymentDescription = NULL;
							if( $arrmixGlDetail['payment_number'] == $strPaymentNumber && $intRank == $arrmixGlDetail['rank'] && $arrmixGlDetail['accrual_gl_account_id'] == $arrmixGlDetail['accrual_credit_gl_account_id'] ) {
								continue;
							}
							$strPaymentDescription = $arrmixGlDetail['payment_line_description'];
							if( false != isset( $arrmixPaymentTypeAccrualTransactions[$intIndex + 1] ) && $arrmixGlDetail['payment_number'] == $arrmixPaymentTypeAccrualTransactions[$intIndex + 1]['payment_number'] && $arrmixPaymentTypeAccrualTransactions[$intIndex + 1]['rank'] == $arrmixGlDetail['rank'] && $arrmixGlDetail['accrual_gl_account_id'] == $arrmixGlDetail['accrual_credit_gl_account_id'] ) {
								$strPaymentDescription = NULL;
								foreach( $arrmixPaymentsWithRank[$arrmixGlDetail['rank']] as $arrmixPaymentWithRank ) {
									if( $arrmixPaymentWithRank['accrual_gl_account_id'] == $arrmixPaymentWithRank['accrual_credit_gl_account_id'] ) {
										$fltAmount += $arrmixPaymentWithRank['amount'];
									}
								}
							} else {
								$fltAmount = $arrmixGlDetail['amount'];
							}
							if( NULL != $strPaymentNumber && false != valStr( $strPaymentNumber ) && $arrmixGlDetail['payment_number'] != $strPaymentNumber ) {
								$this->m_strFileContent .= "ENDTRNS\n \n";
								$intIndex = 0;
							}
							if( 0 == $intIndex ) {
								$this->m_strFileContent .= "TRNS\t";
							} else {
								$this->m_strFileContent .= "SPL\t";
							}
							$strMemo = $arrmixGlDetail['memo'];
							if( false != valStr( $arrmixGlDetail['vendor_name'] ) ) {
								$strMemo .= ' ' . $arrmixGlDetail['vendor_name'];
							}
							if( false != valStr( $arrmixGlDetail['invoice_number'] ) ) {
								$strMemo .= ' ' . $arrmixGlDetail['invoice_number'];
							}
							if( false != valStr( $strPaymentDescription ) ) {
								$strMemo .= '-' . $strPaymentDescription;
							}
							$strMemo                = \Psi\CStringService::singleton()->substr( trim( $strMemo ), 0, 4000 );
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
							$strQuickBookClass = $this->renderQuickBookClass( $arrmixGlDetail['quick_book_class'], $arrmixGlDetail['unit_number'], $arrmixGlDetail['building_name'] );
							$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
							$this->m_strFileContent .= "\t GENERAL JOURNAL \t" . date( 'm/d/Y', strtotime( $arrmixGlDetail['post_date'] ) ) . "\t" . $strAccountNumber . "\t" . $strQuickBookClass . "\t" . str_replace( ',', ' ', $fltAmount ) . "\t" . str_replace( ',', ' ', $arrmixGlDetail['reference'] ) . "\t" . $strMemo . "\n";
							$strPaymentNumber           = $arrmixGlDetail['payment_number'];
							$intRank                    = $arrmixGlDetail['rank'];
						}
						$this->m_strFileContent .= "ENDTRNS\n";
					}

				}

				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		}
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d', strtotime( $this->getBatchDatetime() ) ) . '_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_IIF;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d', strtotime( $this->getBatchDatetime() ) ) . '_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_IIF;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d', strtotime( $this->getBatchDatetime() ) ) . '_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_IIF;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension']     = self::FILE_EXTENSION_IIF;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod]      = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_IIF );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != isset( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_IIF );
			}
		}
		return;
	}

	public function exportInSkylineFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		$arrintGlAccountId      = [];
		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {

			$arrobjGlExportBatchDetails = ( array ) $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_SKYLINE );

			if( false == valArr( $arrobjGlExportBatchDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrobjGlExportBatchDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() && 0 != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() && 0 != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrobjGlExportBatchDetails;
				} else {
					$arrobjAccrualGlDetails = $arrobjGlExportBatchDetails;
				}
			}
			$intPropertiesCount = \Psi\Libraries\UtilFunctions\count( rekeyObjects( 'PropertyId', $arrobjGlExportBatchDetails ) );
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_strFileContent = '';
				$strLookupCode 			= '';
				$intPropertyId			= NULL;
				foreach( $arrobjCashGlDetails as $objGlExportBatchDetail ) {

					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) || false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) ) {
						$objGlAccount = ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) ? $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] : $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
						$objGlExportBatchDetail->setAccountName( $objGlAccount->getName() );
					}
					$strAccountNumber = ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();

					$this->m_strFileContent .= 'GLDET' . ',' . $objGlExportBatchDetail->getLookupCode() . ',' . date( 'Ymd', strtotime( $this->getExportedThrough() ) ) . ',' . date( 'Ym', strtotime( $objGlExportBatchDetail->getPostMonth() ) ) . ',' . $strAccountNumber . ',' . date( 'Ymd', strtotime( $objGlExportBatchDetail->getTransactionDatetime() ) ) . ',' . str_replace( ',', ' ', $objGlExportBatchDetail->getAccountName() ) . ',' . $objGlExportBatchDetail->getAmount();

					$this->m_strFileContent .= ',C';
					$this->m_strFileContent .= ',' . $objGlExportBatchDetail->getTransactionNumber() . "\r\n";

					if( 1 == $intPropertiesCount && false == valStr( $strLookupCode ) ) {
						$strLookupCode = $objGlExportBatchDetail->getLookupCode();
						$intPropertyId = $objGlExportBatchDetail->getPropertyId();
					}
				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_strFileContent = '';
				$strLookupCode 			= '';
				$intPropertyId			= NULL;
				foreach( $arrobjAccrualGlDetails as $objGlExportBatchDetail ) {

					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) {
						$objGlAccount = $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
						$objGlExportBatchDetail->setAccountName( $objGlAccount->getName() );
					}
					$strAccountNumber = ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();

					$this->m_strFileContent .= 'GLDET' . ',' . $objGlExportBatchDetail->getLookupCode() . ',' . date( 'Ymd', strtotime( $this->getExportedThrough() ) ) . ',' . date( 'Ym', strtotime( $objGlExportBatchDetail->getPostMonth() ) ) . ',' . $strAccountNumber . ',' . date( 'Ymd', strtotime( $objGlExportBatchDetail->getTransactionDatetime() ) ) . ',' . str_replace( ',', ' ', $objGlExportBatchDetail->getAccountName() ) . ',' . $objGlExportBatchDetail->getAmount();

					$this->m_strFileContent .= ',A';
					$this->m_strFileContent .= ',' . $objGlExportBatchDetail->getTransactionNumber() . "\r\n";

					if( 1 == $intPropertiesCount && false == valStr( $strLookupCode ) ) {
						$strLookupCode = $objGlExportBatchDetail->getLookupCode();
						$intPropertyId = $objGlExportBatchDetail->getPropertyId();
					}
				}
				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}

		} else {
			$arrmixGlDetails = ( array ) $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_SKYLINE );
			if( false == valArr( $arrmixGlDetails ) ) {
				return false;
			}
			$intPropertiesCount = \Psi\Libraries\UtilFunctions\count( rekeyArray( 'property_id', $arrmixGlDetails ) );
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$intRecordId = 1;
				$this->m_strFileContent = '';
				$strLookupCode 			= '';
				$intPropertyId			= NULL;
				foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {

					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$this->m_strFileContent .= 'GLDET' . ',' . $arrmixGlDetail['lookup_code'] . ',' . date( 'Ymd', strtotime( $this->getExportedThrough() ) ) . ',' . date( 'Ym', strtotime( $arrmixGlDetail['post_month'] ) ) . ',' . $strAccountNumber . ',' . date( 'Ymd', strtotime( $arrmixGlDetail['post_date'] ) ) . ',' . str_replace( ',', ' ', $arrmixGlDetail['account_name'] ) . ',' . $arrmixGlDetail['amount'];
					$this->m_strFileContent .= ',C';
					$this->m_strFileContent .= ',' . $intRecordId++ . "\r\n";

					if( 1 == $intPropertiesCount && false == valStr( $strLookupCode ) ) {
						$strLookupCode = $arrmixGlDetail['lookup_code'];
						$intPropertyId = $arrmixGlDetail['property_id'];
					}
				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$intRecordId = 1;
				$this->m_strFileContent = '';
				$strLookupCode 			= '';
				$intPropertyId			= NULL;
				foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {

					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$this->m_strFileContent .= 'GLDET' . ',' . $arrmixGlDetail['lookup_code'] . ',' . date( 'Ymd', strtotime( $this->getExportedThrough() ) ) . ',' . date( 'Ym', strtotime( $arrmixGlDetail['post_month'] ) ) . ',' . $strAccountNumber . ',' . date( 'Ymd', strtotime( $arrmixGlDetail['post_date'] ) ) . ',' . str_replace( ',', ' ', $arrmixGlDetail['account_name'] ) . ',' . $arrmixGlDetail['amount'];
					$this->m_strFileContent .= ',A';
					$this->m_strFileContent .= ',' . $intRecordId++ . "\r\n";

					if( 1 == $intPropertiesCount && false == valStr( $strLookupCode ) ) {
						$strLookupCode = $arrmixGlDetail['lookup_code'];
						$intPropertyId = $arrmixGlDetail['property_id'];
					}
				}
				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		}
		$this->m_strFileExtension = ( \Psi\CStringService::singleton()->strlen( $this->getId() ) < 3 ) ? \Psi\CStringService::singleton()->str_pad( $this->getId(), 3, '0', STR_PAD_LEFT ) : \Psi\CStringService::singleton()->substr( $this->getId(), -3 );
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) || false != valArr( $arrobjCashGlDetails ) ) {
				if( 1 == $intPropertiesCount ) {
					$this->m_arrstrFileNames['Cash'] = ( false == is_null( $strLookupCode ) ) ? 'EQ' . $strLookupCode  : 'EQ' . $intPropertyId;
				} else {
					$this->m_arrstrFileNames['Cash'] = 'EQ' . $this->getId();
				}
				$this->m_arrstrFileNames['Cash'] .= '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . $this->m_strFileExtension;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) || false != valArr( $arrobjAccrualGlDetails ) ) {
				if( 1 == $intPropertiesCount ) {
					$this->m_arrstrFileNames['Accrual'] = ( false == is_null( $strLookupCode ) ) ? 'EQ' . $strLookupCode  : 'EQ' . $intPropertyId;
				} else {
					$this->m_arrstrFileNames['Accrual'] = 'EQ' . $this->getId();
				}
				$this->m_arrstrFileNames['Accrual'] .= '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . $this->m_strFileExtension;
			}
		} else {
			if( 1 == $intPropertiesCount ) {
				$this->m_strFileName = ( false == is_null( $strLookupCode ) ) ? 'EQ' . $strLookupCode  : 'EQ' . $intPropertyId;
			} else {
				$this->m_strFileName = 'EQ' . $this->getId();
			}
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . $this->m_strFileExtension;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension']     = $this->m_strFileExtension;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod]      = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( $this->m_strFileExtension );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( $this->m_strFileExtension );
			}
		}
		return;
	}

	public function exportInPeopleSoftFormat( $objClientDatabase, $boolIsFromDownload, $intCompanyUserId, $objGlDetailsFilter ) {

		$objCompanyTransmissionVendor = \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByTransmissionTypeIdByTransmissionVendorIdByCid( CTransmissionType::FINANCIAL, CTransmissionVendor::PEOPLESOFT, $this->getCid(), $objClientDatabase );

		if( false == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) || false == valStr( $objCompanyTransmissionVendor->getRequestUrl() ) ) {
			return false;
		}

		if( true == $boolIsFromDownload ) {
			$strSql = 'SELECT
	    					aebd.id,
							aebd.property_id,
							amount,
							aebd.post_month,
	    					aebd.transaction_datetime AS post_date,
							gat.name AS account_name,
	    					gat.secondary_number,
	    					gat.account_number,
							pp.value,
	    					aebd.memo AS description,
	    					aebd.accrual_gl_account_id,
	    					aebd.cash_gl_account_id
	    				FROM
	    					accounting_export_batch_details aebd
	    					JOIN gl_accounts ga ON ( aebd.cid = ga.cid
	    											AND ga.id = aebd.accrual_gl_account_id  )
		    				JOIN gl_account_trees gat ON ( ga.cid = gat.cid
	    													AND gat.gl_account_id = ga.id  )
	    	    	 		JOIN property_preferences pp ON ( pp.property_id = aebd.property_id AND pp.cid = aebd.cid  AND pp.key = \'PEOPLESOFT_PROPERTY_GL_BU\')
	    	   			WHERE
	    					aebd.cid = ' . ( int ) $this->getCid() . '
	    					AND pp.value IS NOT NULL
	    					AND aebd.accounting_export_batch_id = ' . ( int ) $this->getId() . '
	    					ORDER BY property_id, transaction_datetime, aebd.post_month, gat.name';
			$arrmixGlExportBatchDetails = fetchData( $strSql, $objClientDatabase );
		} else {
			$arrmixGlExportBatchDetails = ( array ) \Psi\Eos\Entrata\CGlDetails::createService()->fetchUnBatchedSummarizedGlDetailsByGlDetailsFilterForPeopleSoft( $objGlDetailsFilter, $objClientDatabase );
		}

		if( false == valArr( $arrmixGlExportBatchDetails ) ) return false;

		$intPreviousPropertyId = 0;
		$intPropertiesCount = 0;
		$boolIsValid = true;

		$strJournalId = 'PSI';
		$intLengthOfGlExportBatchId = 10 - \Psi\CStringService::singleton()->strlen( $this->getId() );

		if( 0 < $intLengthOfGlExportBatchId ) {
			$strJournalId = \Psi\CStringService::singleton()->str_pad( $strJournalId, $intLengthOfGlExportBatchId, '0', STR_PAD_RIGHT );
		}

		$strJournalId .= $this->getId();

		$arrmixParameters = [
			'url' => $objCompanyTransmissionVendor->getRequestUrl() . 'JOURNAL_LOAD_TEMPLATE.1.wsdl',
			'connection_timeout' => 10,
			'execution_timeout' => 30,
			'ssl_verify_peer' => false,
			'ssl_verify_host' => false,
			'request_method' => 'POST',
			'ssl_version' => 3
		];

		$objExternalRequest = new CExternalRequest();
		$objExternalRequest->setParameters( $arrmixParameters );

		foreach( $arrmixGlExportBatchDetails as $arrmixGlExportBatchDetail ) {
			if( $intPreviousPropertyId != $arrmixGlExportBatchDetail['property_id'] ) {
				$intPropertiesCount++;
				$intNumber = 1;
				$strBusinessUnit = trim( $arrmixGlExportBatchDetail['value'] );
			}

			$strAccount = ( true == valStr( $arrmixGlExportBatchDetail['secondary_number'] ) ) ?  $arrmixGlExportBatchDetail['secondary_number'] : $arrmixGlExportBatchDetail['account_number'];

			if( 0 != $arrmixGlExportBatchDetail['amount'] ) {
				$strPostString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:jour="/JOURNAL_LOAD_TEMPLATE.VERSION_1">
	    						<soapenv:Header/>
								<soapenv:Body>
								<jour:JOURNAL_LOAD_TEMPLATE>
	    							<jour:MsgData>
	    							<jour:Transaction>
	    						   		<jour:JGEN_ACCT_ENTRY class="R">
											<jour:BUSINESS_UNIT IsChanged="Y">' . $strBusinessUnit . '</jour:BUSINESS_UNIT>
		    								<jour:TRANSACTION_ID IsChanged="Y">' . $this->getId() . '</jour:TRANSACTION_ID>
		                  					<jour:TRANSACTION_LINE IsChanged="Y">' . $intNumber . '</jour:TRANSACTION_LINE>
		                  					<jour:LEDGER_GROUP IsChanged="Y">ACTUALS</jour:LEDGER_GROUP>
		                  					<jour:LEDGER IsChanged="Y">ACTUALS</jour:LEDGER>
		                  					<jour:ACCOUNTING_DT IsChanged="Y">' . date( 'Y/m/d', strtotime( $arrmixGlExportBatchDetail['post_date'] ) ) . '</jour:ACCOUNTING_DT>
			                  				<jour:APPL_JRNL_ID IsChanged="Y">GENERIC</jour:APPL_JRNL_ID>
			                 				<jour:BUSINESS_UNIT_GL IsChanged="Y">' . $strBusinessUnit . '</jour:BUSINESS_UNIT_GL>
			                  				<jour:FISCAL_YEAR IsChanged="Y"></jour:FISCAL_YEAR>
			                   	 			<jour:ACCOUNTING_PERIOD IsChanged="Y"></jour:ACCOUNTING_PERIOD>
			                  				<jour:JOURNAL_ID IsChanged="Y">' . $strJournalId . '</jour:JOURNAL_ID>
			                  				<jour:JOURNAL_DATE IsChanged="Y">' . date( 'Y/m/d', strtotime( $arrmixGlExportBatchDetail['post_date'] ) ) . '</jour:JOURNAL_DATE>
			                 				<jour:JOURNAL_LINE IsChanged="Y">' . $intNumber . '</jour:JOURNAL_LINE>
			                  				<jour:ACCOUNT IsChanged="Y">' . $strAccount . '</jour:ACCOUNT>
			                  				<jour:ALTACCT IsChanged="Y"></jour:ALTACCT>
			                  				<jour:DEPTID IsChanged="Y"></jour:DEPTID>
			                 				<jour:OPERATING_UNIT IsChanged="Y"></jour:OPERATING_UNIT>
						                  	<jour:PRODUCT IsChanged="Y"></jour:PRODUCT>
		                  					<jour:FUND_CODE IsChanged="Y"></jour:FUND_CODE>
		                  					<jour:CLASS_FLD IsChanged="Y"></jour:CLASS_FLD>
		                  					<jour:PROGRAM_CODE IsChanged="Y"></jour:PROGRAM_CODE>
		                  					<jour:BUDGET_REF IsChanged="Y"></jour:BUDGET_REF>
		                  					<jour:AFFILIATE IsChanged="Y"></jour:AFFILIATE>
		                  					<jour:AFFILIATE_INTRA1 IsChanged="Y"></jour:AFFILIATE_INTRA1>
						                  	<jour:AFFILIATE_INTRA2 IsChanged="Y"></jour:AFFILIATE_INTRA2>
						                  	<jour:CHARTFIELD1 IsChanged="Y"></jour:CHARTFIELD1>
						                  	<jour:CHARTFIELD2 IsChanged="Y"></jour:CHARTFIELD2>
						                  	<jour:CHARTFIELD3 IsChanged="Y"></jour:CHARTFIELD3>
						                  	<jour:PROJECT_ID IsChanged="Y"></jour:PROJECT_ID>
						                   	<jour:CURRENCY_CD IsChanged="Y">USD</jour:CURRENCY_CD>
						                  	<jour:STATISTICS_CODE IsChanged="Y"></jour:STATISTICS_CODE>
						                 	<jour:FOREIGN_CURRENCY IsChanged="Y">USD</jour:FOREIGN_CURRENCY>
						                  	<jour:RT_TYPE IsChanged="Y"></jour:RT_TYPE>
						                  	<jour:RATE_MULT IsChanged="Y"></jour:RATE_MULT>
		                  					<jour:RATE_DIV IsChanged="Y"></jour:RATE_DIV>
		                   					<jour:MONETARY_AMOUNT IsChanged="Y">' . $arrmixGlExportBatchDetail['amount'] . '</jour:MONETARY_AMOUNT>
		                  					<jour:FOREIGN_AMOUNT IsChanged="Y">' . $arrmixGlExportBatchDetail['amount'] . '</jour:FOREIGN_AMOUNT>
		                  					<jour:STATISTIC_AMOUNT IsChanged="Y"></jour:STATISTIC_AMOUNT>
		                  					<jour:MOVEMENT_FLAG IsChanged="Y"></jour:MOVEMENT_FLAG>
		                  					<jour:DOC_TYPE IsChanged="Y"></jour:DOC_TYPE>
		                  					<jour:DOC_SEQ_NBR IsChanged="Y"></jour:DOC_SEQ_NBR>
		                  					<jour:DOC_SEQ_DATE IsChanged="Y"></jour:DOC_SEQ_DATE>
						                  	<jour:JRNL_LN_REF IsChanged="Y">' . $strJournalId . '</jour:JRNL_LN_REF>
						                  	<jour:LINE_DESCR IsChanged="Y">' . \Psi\CStringService::singleton()->htmlspecialchars( $arrmixGlExportBatchDetail['description'] ) . '</jour:LINE_DESCR>
						                  	<jour:IU_SYS_TRAN_CD IsChanged="Y"></jour:IU_SYS_TRAN_CD>
						                  	<jour:IU_TRAN_CD IsChanged="Y"></jour:IU_TRAN_CD>
						                  	<jour:IU_ANCHOR_FLG IsChanged="Y"></jour:IU_ANCHOR_FLG>
						                  	<jour:GL_DISTRIB_STATUS IsChanged="Y">N</jour:GL_DISTRIB_STATUS>
						                  	<jour:PROCESS_INSTANCE IsChanged="Y"></jour:PROCESS_INSTANCE>
					               		</jour:JGEN_ACCT_ENTRY>
					               		<jour:PSCAMA class="R"></jour:PSCAMA>
									</jour:Transaction>
	    						</jour:MsgData>
							</jour:JOURNAL_LOAD_TEMPLATE>
						</soapenv:Body>
					</soapenv:Envelope>';

				$arrmixHeaders = [
					'Content-Type: text/xml; charset=utf-8',
					'Accept: text/xml',
					'Cache-Control: no-cache',
					'Pragma: no-cache',
					'SOAPAction: "JOURNAL_LOAD_TEMPLATE"',
					'Content-length: ' . strlen( $strPostString )
				];

				$objExternalRequest->setRequestData( $strPostString );
				$objExternalRequest->setHeaderInfo( $arrmixHeaders );
				$strResult = $objExternalRequest->execute( $boolGetResponse = true );

				$objTransmission = new CTransmission();
				$objTransmission->setCid( $this->getCid() );
				$objTransmission->setPropertyId( $arrmixGlExportBatchDetail['property_id'] );
				$objTransmission->setTransmissionTypeId( $objCompanyTransmissionVendor->getTransmissionTypeId() );
				$objTransmission->setTransmissionVendorId( $objCompanyTransmissionVendor->getTransmissionVendorId() );
				$objTransmission->setCompanyTransmissionVendorId( $objCompanyTransmissionVendor->getId() );

				$strResult = ( true == valStr( $strResult ) ) ? serialize( $strResult ) : NULL;
				$objTransmission->setRequestContent( serialize( $strPostString ) );
				$objTransmission->setResponseContent( $strResult );
				$objTransmission->setTransmissionDatetime( date( 'm/d/Y H:i:s' ) );

				if( true == valStr( $strResult ) ) {
					$objTransmission->setIsFailed( 1 );
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '{%s,0}', $strResult ), NULL ) );
					$boolIsValid &= false;
				}

				if( false == $objTransmission->insert( $intCompanyUserId, $objClientDatabase ) ) {
					if( true == valStr( $objTransmission->getPropertyId() ) ) {
						$strErrorMessage = __( 'Property [ID: {%d, 0, nots}] : Failed to insert record in transmission for peoplesoft.', [ $objTransmission->getPropertyId() ] );
					} else {
						$strErrorMessage = __( 'client [ID: {%d, 0, nots}] : Failed to insert record in transmission for peoplesoft.', [ $this->getCid() ] );
					}

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
				}

				if( 1 == $objTransmission->getIsFailed() ) {
					continue;
				}
			}

			$intPreviousPropertyId = $arrmixGlExportBatchDetail['property_id'];
			$intNumber++;

		}

		unset( $objExternalRequest );

		return true;
	}

	public function exportInTimberlinePmFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $intSummarizationMethod ) {

		$arrintGlAccountId      = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];

		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrobjGlExportSummarizedBatchDetails = $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM, $intSummarizationMethod );
			if( false == valArr( $arrobjGlExportSummarizedBatchDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrobjGlExportSummarizedBatchDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() && 0 != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() && 0 != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrobjGlExportSummarizedBatchDetails;
				} else {
					$arrobjAccrualGlDetails = $arrobjGlExportSummarizedBatchDetails;
				}
			}
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrobjCashGlDetails as $objGlExportBatchDetail ) {
						if( 0 == $objGlExportBatchDetail->getAmount() ) continue;
						if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) {
							$objGlAccount                       = $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()];
							$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
							$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
							$objGlExportBatchDetail->setAccountName( $objGlAccount->getName() );
						}
						$strAccountNumber = ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();
						$strGlAccountPrefix = ( true == valStr( $objGlExportBatchDetail->getGlAccountPrefix() ) ) ? $objGlExportBatchDetail->getGlAccountPrefix() : '';
						$strAccountNumber = $strGlAccountPrefix . \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 15 );

						$this->m_strFileContent .= 'GLT' . ',' . $strAccountNumber . ',' . date( 'm-d-Y', strtotime( $objGlExportBatchDetail->getTransactionDatetime() ) ) . ',' . $objGlExportBatchDetail->getJournalCode() . ',' . 'ENT' . ',' . NULL . ',' . \Psi\CStringService::singleton()->substr( str_replace( ',', ' ', $objGlExportBatchDetail->getAccountName() ), 0, 30 );
						$this->m_strFileContent .= ( 0 < $objGlExportBatchDetail->getAmount() ) ? ',' . $objGlExportBatchDetail->getAmount() . ',0.00,' . 2 : ',0.00,' . abs( $objGlExportBatchDetail->getAmount() ) . ',' . 2;
						$this->m_strFileContent .= "\r\n";
				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrobjAccrualGlDetails as $objGlExportBatchDetail ) {
					if( 0 == $objGlExportBatchDetail->getAmount() ) continue;
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
						$objGlExportBatchDetail->setAccountName( $objGlAccount->getName() );
					}
					$strAccountNumber = ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();
					$strGlAccountPrefix = ( true == valStr( $objGlExportBatchDetail->getGlAccountPrefix() ) ) ? $objGlExportBatchDetail->getGlAccountPrefix() : '';
					$strAccountNumber = $strGlAccountPrefix . \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 15 );

					$this->m_strFileContent .= 'GLT' . ',' . $strAccountNumber . ',' . date( 'm-d-Y', strtotime( $objGlExportBatchDetail->getTransactionDatetime() ) ) . ',' . $objGlExportBatchDetail->getJournalCode() . ',' . 'ENT' . ',' . NULL . ',' . \Psi\CStringService::singleton()->substr( str_replace( ',', ' ', $objGlExportBatchDetail->getAccountName() ), 0, 30 );
					$this->m_strFileContent .= ( 0 < $objGlExportBatchDetail->getAmount() ) ? ',' . $objGlExportBatchDetail->getAmount() . ',0.00,' . 1 : ',0.00,' . abs( $objGlExportBatchDetail->getAmount() ) . ',' . 1;
					$this->m_strFileContent .= "\r\n";
				}
				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		} else {
			$arrmixGlDetails = $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM );

			if( false == valArr( $arrmixGlDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) continue;
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['AccountName'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
					$strGlAccountPrefix = ( true == valStr( $arrmixGlDetail['gl_account_prefix'] ) ) ? $arrmixGlDetail['gl_account_prefix'] : '';
					$strAccountNumber = $strGlAccountPrefix . \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 15 );

					$this->m_strFileContent .= 'GLT' . ',' . $strAccountNumber . ',' . date( 'm-d-Y', strtotime( $arrmixGlDetail['post_date'] ) ) . ',' . $arrmixGlDetail['value'] . ',ENT,' . $arrmixGlDetail['transaction_id'] . ',' . \Psi\CStringService::singleton()->substr( str_replace( ',', ' ',  $arrmixGlDetail['reference'] ), 0, 30 );
					$this->m_strFileContent .= ( 0 < $arrmixGlDetail['amount'] ) ? ',' . $arrmixGlDetail['amount'] . ',0.00,' . 2 : ',0.00,' . abs( $arrmixGlDetail['amount'] ) . ',' . 2;
					$this->m_strFileContent .= "\r\n";
				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {

					if( 0 == $arrmixGlDetail['amount'] ) continue;
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['AccountName'] = $objGlAccount->getName();
					}
					$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
					$strGlAccountPrefix = ( true == valStr( $arrmixGlDetail['gl_account_prefix'] ) ) ? $arrmixGlDetail['gl_account_prefix'] : '';
					$strAccountNumber = $strGlAccountPrefix . \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 15 );

					$this->m_strFileContent .= 'GLT' . ',' . $strAccountNumber . ',' . date( 'm-d-Y', strtotime( $arrmixGlDetail['post_date'] ) ) . ',' . $arrmixGlDetail['value'] . ',ENT,' . $arrmixGlDetail['transaction_id'] . ',' . \Psi\CStringService::singleton()->substr( str_replace( ',', ' ',  $arrmixGlDetail['reference'] ), 0, 30 );
					$this->m_strFileContent .= ( 0 < $arrmixGlDetail['amount'] ) ? ',' . $arrmixGlDetail['amount'] . ',0.00,' . 1 : ',0.00,' . abs( $arrmixGlDetail['amount'] ) . ',' . 1;
					$this->m_strFileContent .= "\r\n";
				}
				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		}
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) || false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_timberline_pm_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_TXT;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) || false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_timberline_pm_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_TXT;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_timberline_pm_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_TXT;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension']     = self::FILE_EXTENSION_TXT;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;

		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_TXT );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_TXT );
			}
		}
		return;
	}

	public function exportInYardiFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $boolIsFromDownload, $boolIsBookMethodToBoth, $boolIsIncludeHeader, $intCompanyUserId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		$this->m_strFileContent	= '';
		$boolIsSummary = false;

		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrmixGlDetails = ( array ) $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_YARDI );
			$boolIsSummary = true;
		} else {
			$arrmixGlDetails = ( array ) $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_YARDI );
		}

		if( false == valArr( $arrmixGlDetails ) ) return false;

		if( false == $boolIsFromDownload ) {
			if( true == $boolIsIncludeHeader ) {
				$this->m_arrstrFileContents[] = [ 'Trans type', 'Trans No', 'Person code', 'Name', 'Trans Date', 'Post Month', 'Reference', 'Notes', 'Property', 'Amount', 'Account', 'Accrual Acct', 'Retention Acct', 'Book', 'Remarks', 'Flags', 'Field17', 'Field18', 'Field19', 'Field20', 'Segment1', 'Segment2', 'Segment3', 'Segment4' ];
				$this->m_strFileContent	.= "Trans type,Trans No,Person code,Name,Trans Date,Post Month,Reference,Notes,Property,Amount,Account,Accrual Acct,Retention Acct,Book,Remarks,Flags,Field17,Field18,Field19,Field20,Segment1,Segment2,Segment3,Segment4\n";
			}
			$boolIsValid = $this->exportApiFormat( $arrmixGlDetails, $intCompanyUserId, $objClientDatabase );
		} else {
			$boolIsValid = $this->exportFileFormat( $arrmixGlDetails, $intGlExportBookTypeId, $boolIsBookMethodToBoth, $boolIsSummary, $arrmixConnectionData, $objClientDatabase, $arrmixExportData, $boolIsIncludeHeader );
		}

		return $boolIsValid;
	}

	public function exportInPeakFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		$arrintGlAccountId      = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrobjGlExportBatchDetails = $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_PEAK );

			if( false == valArr( $arrobjGlExportBatchDetails ) ) {
				return false;
			}

			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrobjGlExportBatchDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrobjGlExportBatchDetails;
				} else {
					$arrobjAccrualGlDetails = $arrobjGlExportBatchDetails;
				}
			}
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrobjCashGlDetails as $objGlExportBatchDetail ) {
					if( 0 == $objGlExportBatchDetail->getAmount() ) continue;
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) || false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) ? $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] : $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
					}
					if( \Psi\CStringService::singleton()->strpos( $objGlExportBatchDetail->getAccountNumber(), $objGlExportBatchDetail->getAccountNumberDelimiter() ) !== false ) {
						$strAccountNumber	= ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : \Psi\CStringService::singleton()->substr( $objGlExportBatchDetail->getAccountNumber(), 0, \Psi\CStringService::singleton()->strpos( $objGlExportBatchDetail->getAccountNumber(), $objGlExportBatchDetail->getAccountNumberDelimiter() ) );
					} else {
						$strAccountNumber	= ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();
					}

					$intDebitAmount		= ( 0 < $objGlExportBatchDetail->getAmount() ) ? $objGlExportBatchDetail->getAmount() : NULL;
					$intCreditAmount 	= ( 0 > $objGlExportBatchDetail->getAmount() ) ? abs( $objGlExportBatchDetail->getAmount() ) : NULL;
					$strRemark			= $objGlExportBatchDetail->getMemo();

					$this->m_strFileContent .= $strAccountNumber . "\t" . $intDebitAmount . "\t" . $intCreditAmount . "\t\t\t" . $strRemark;
					$this->m_strFileContent .= "\n";
				}
				$this->m_arrstrFileContents['Cash'] = $this->m_strFileContent;
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_strFileContent = '';
				foreach( $arrobjAccrualGlDetails as $objGlExportBatchDetail ) {
					if( 0 == $objGlExportBatchDetail->getAmount() ) continue;
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objGlExportBatchDetail->setSecondaryNumber( $objGlAccount->getSecondaryNumber() );
					}
					if( \Psi\CStringService::singleton()->strpos( $objGlExportBatchDetail->getAccountNumber(), $objGlExportBatchDetail->getAccountNumberDelimiter() ) !== false ) {
						$strAccountNumber	= ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : \Psi\CStringService::singleton()->substr( $objGlExportBatchDetail->getAccountNumber(), 0, \Psi\CStringService::singleton()->strpos( $objGlExportBatchDetail->getAccountNumber(), $objGlExportBatchDetail->getAccountNumberDelimiter() ) );
					} else {
						$strAccountNumber	= ( true == valStr( $objGlExportBatchDetail->getSecondaryNumber() ) ) ? $objGlExportBatchDetail->getSecondaryNumber() : $objGlExportBatchDetail->getAccountNumber();
					}

					$intDebitAmount		= ( 0 < $objGlExportBatchDetail->getAmount() ) ? $objGlExportBatchDetail->getAmount() : NULL;
					$intCreditAmount 	= ( 0 > $objGlExportBatchDetail->getAmount() ) ? abs( $objGlExportBatchDetail->getAmount() ) : NULL;
					$strRemark			= $objGlExportBatchDetail->getMemo();

					$this->m_strFileContent .= $strAccountNumber . "\t" . $intDebitAmount . "\t" . $intCreditAmount . "\t\t\t" . $strRemark;
					$this->m_strFileContent .= "\n";
				}
				$this->m_arrstrFileContents['Accrual'] = $this->m_strFileContent;
			}
		}
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_peak_pm_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_XLS;
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_peak_pm_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_XLS;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_peak_pm_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_XLS;
		}
		if( false != $arrmixExportData['show_ftp'] ) {

			$arrmixExportData['file_extension']        = self::FILE_EXTENSION_XLS;
			$arrmixExportData['worksheet_name']        = 'Sheet1';
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_XLS );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != isset( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_XLS );
			}
		}
		return;
	}

	public function exportInOracleFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		$arrmixGlDetails = $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, CTransmissionVendor::GL_EXPORT_ORACLE );
		if( false == valArr( $arrmixGlDetails ) ) return false;

		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];
		$arrintGlAccountId = [];
		$arrobjAccountDetails = [];
		if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
					$arrmixCashGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
				}
				if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
					$arrmixAccrualGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
				}
			}
			$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
			$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
		} else {
			if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
				$arrmixCashGlDetails = $arrmixGlDetails;
			} else {
				$arrmixAccrualGlDetails = $arrmixGlDetails;
			}
		}
		if( false != valArr( $arrmixCashGlDetails ) ) {
			$intTransNumber		= NULL;
			$fltTotalCreditAmount   = NULL;
			$fltTotalDebitAmount    = NULL;
			foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
					$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
					$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
					$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
				}
				$intTransNumber					= $arrmixGlDetail['id'];
				$fltCreditAmount				= NULL;
				$fltDebitAmount					= NULL;

				$strPropertyName				= $arrmixGlDetail['property_name'];
				$strReference					= $arrmixGlDetail['reference'];
				$strArCodeName					= $arrmixGlDetail['ar_code_name'];
				$strTransPostMonth				= date( '01-M-Y', strtotime( $arrmixGlDetail['post_month'] ) );
				$strCurrentDate					= date( 'd-M-Y' );
				$strAccountNumber				= ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number']  : $arrmixGlDetail['account_number'];

				$strPropPropertyNamePostMonth	= 'PROP-' . $strPropertyName . '-' . date( 'm/Y', strtotime( $arrmixGlDetail['post_month'] ) );

				$intSegment5 = '00000';
				if( '221001' == $strAccountNumber ) {
					$intSegment5 = '60084';
				} elseif( false != in_array( $arrmixGlDetail['lookup_code'], [ '00382','00393','01517','01605' ] ) && false != in_array( \Psi\CStringService::singleton()->substr( $strAccountNumber,0, 1 ), [ 4, 5 ] ) ) {
					$intSegment5 = '90100';
				} elseif( '512406' == $strAccountNumber ) {
					$intSegment5 = '61021';
				}

				if( $arrmixGlDetail['amount'] < 0 ) {
					$fltCreditAmount = abs( $arrmixGlDetail['amount'] );
					$fltTotalCreditAmount += $fltCreditAmount;

				} else {
					$fltDebitAmount = $arrmixGlDetail['amount'];
					$fltTotalDebitAmount += $fltDebitAmount;
				}

				$this->m_arrstrFileContents['Cash'][] = [ 'NEW', '1', $strTransPostMonth, 'USD', $strCurrentDate, '1010', 'A', 'Income', 'ENTRATA', $arrmixGlDetail['lookup_code'], '000', '0', $strAccountNumber, $intSegment5, $fltDebitAmount, $fltCreditAmount, $strPropPropertyNamePostMonth, 'TRANSFER TO GL', $strPropPropertyNamePostMonth, 'TRANSFER TO GL', $strReference, $intTransNumber, $strArCodeName, $arrmixGlDetail['lookup_code'], '1' ];
			}
			$arrstrHeaderFileContents = [ 'TOTAL', \Psi\Libraries\UtilFunctions\count( $arrmixCashGlDetails ), $strCurrentDate, $fltTotalDebitAmount, $fltTotalCreditAmount ];
			array_unshift( $this->m_arrstrFileContents['Cash'], $arrstrHeaderFileContents );
		}
		if( false != valArr( $arrmixAccrualGlDetails ) ) {
			$intTransNumber		= NULL;
			$fltTotalCreditAmount   = NULL;
			$fltTotalDebitAmount    = NULL;
			foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
					$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
					$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
					$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
				}
				$intTransNumber					= $arrmixGlDetail['id'];
				$fltCreditAmount				= NULL;
				$fltDebitAmount					= NULL;

				$strPropertyName				= $arrmixGlDetail['property_name'];
				$strReference					= $arrmixGlDetail['reference'];
				$strArCodeName					= $arrmixGlDetail['ar_code_name'];
				$strTransPostMonth				= date( '01-M-Y', strtotime( $arrmixGlDetail['post_month'] ) );
				$strCurrentDate					= date( 'd-M-Y' );
				$strAccountNumber				= ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number']  : $arrmixGlDetail['account_number'];

				$strPropPropertyNamePostMonth	= 'PROP-' . $strPropertyName . '-' . date( 'm/Y', strtotime( $arrmixGlDetail['post_month'] ) );

				$intSegment5 = '00000';
				if( '221001' == $strAccountNumber ) {
					$intSegment5 = '60084';
				} elseif( false != in_array( $arrmixGlDetail['lookup_code'], [ '00382','00393','01517','01605' ] ) && false != in_array( \Psi\CStringService::singleton()->substr( $strAccountNumber,0, 1 ), [ 4, 5 ] ) ) {
					$intSegment5 = '90100';
				} elseif( '512406' == $strAccountNumber ) {
					$intSegment5 = '61021';
				}
				if( $arrmixGlDetail['amount'] < 0 ) {
					$fltCreditAmount = abs( $arrmixGlDetail['amount'] );
					$fltTotalCreditAmount += $fltCreditAmount;

				} else {
					$fltDebitAmount = $arrmixGlDetail['amount'];
					$fltTotalDebitAmount += $fltDebitAmount;
				}

				$this->m_arrstrFileContents['Accrual'][] = [ 'NEW', '1', $strTransPostMonth, 'USD', $strCurrentDate, '1010', 'A', 'Income', 'ENTRATA', $arrmixGlDetail['lookup_code'], '000', '0', $strAccountNumber, $intSegment5, $fltDebitAmount, $fltCreditAmount, $strPropPropertyNamePostMonth, 'TRANSFER TO GL', $strPropPropertyNamePostMonth, 'TRANSFER TO GL', $strReference, $intTransNumber, $strArCodeName, $arrmixGlDetail['lookup_code'], '1' ];
			}
			$arrstrHeaderFileContents = [ 'TOTAL', \Psi\Libraries\UtilFunctions\count( $arrmixAccrualGlDetails ), $strCurrentDate, $fltTotalDebitAmount, $fltTotalCreditAmount ];
			array_unshift( $this->m_arrstrFileContents['Accrual'], $arrstrHeaderFileContents );
		}

		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_oracle_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_oracle_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_CSV;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_oracle_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_CSV;
		}

		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_CSV );
			}
		}

		return true;
	}

	public function exportFileFormat( $arrmixGlDetails, $intGlExportBookTypeId, $boolIsBookMethodToBoth, $boolIsSummary, $arrmixConnectionData, $objClientDatabase, $arrmixExportData, $boolIsIncludeHeader ) {

		$arrintGlAccountId      = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];
		if( false != $boolIsSummary ) {
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() && 0 != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() && 0 != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrmixGlDetails;
				} else {
					$arrobjAccrualGlDetails = $arrmixGlDetails;
				}
			}
			$intTransCount  = 0;
			$strPreviousPostMonth = NULL;
			$intPreviousPropertyId = 0;
			if( false != valArr( $arrobjCashGlDetails ) ) {
				if( true == $boolIsIncludeHeader ) {
					$this->m_arrstrFileContents['Cash'][] = [ 'Trans type', 'Trans No', 'Person code', 'Name', 'Trans Date', 'Post Month', 'Reference', 'Notes', 'Property', 'Amount', 'Account', 'Accrual Acct', 'Retention Acct', 'Book', 'Remarks', 'Flags', 'Field17', 'Field18', 'Field19', 'Field20', 'Segment1', 'Segment2', 'Segment3', 'Segment4' ];
				}
				foreach( $arrobjCashGlDetails as $objGlExportBatchDetail ) {

					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) || false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) ? $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] : $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$arrmixGlDetail['gl_account_id']    = $objGlAccount->getId();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}
					if( ( $intPreviousPropertyId !== $objGlExportBatchDetail->getPropertyId() ) || ( $strPreviousPostMonth !== $objGlExportBatchDetail->getPostMonth() ) ) {
						$intPreviousPropertyId = $objGlExportBatchDetail->getPropertyId();
						$strPreviousPostMonth  = $objGlExportBatchDetail->getPostMonth();
						$intTransCount++;
					}
					$strPropertyGlBuCode = $objGlExportBatchDetail->getPropertyGlBu();
					$fltAmount           = $objGlExportBatchDetail->getAmount();
					$strNotes            = valStr( $objGlExportBatchDetail->getFrequencyId() ) ? CFrequency::frequencyIdToStr( $objGlExportBatchDetail->getFrequencyId() ) . ' Export' : 'GL Export';
					$strAccountNumber    = $objGlExportBatchDetail->getAccountNumber();

					$intBookTypeId = 1000;
					if( true != $boolIsBookMethodToBoth ) {
						$intBookTypeId = 0;
					}

					$this->m_arrstrFileContents['Cash'][] = [ 'J', $intTransCount, ' ', ' ', date( 'm/t/Y', strtotime( $objGlExportBatchDetail->getPostMonth() ) ), date( 'M-y', strtotime( $objGlExportBatchDetail->getPostMonth() ) ), ' ', $strNotes, $strPropertyGlBuCode, $fltAmount, $strAccountNumber, ' ', ' ', $intBookTypeId, 'Summary Only' ];
				}
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				if( true == $boolIsIncludeHeader ) {
					$this->m_arrstrFileContents['Accrual'][] = [ 'Trans type', 'Trans No', 'Person code', 'Name', 'Trans Date', 'Post Month', 'Reference', 'Notes', 'Property', 'Amount', 'Account', 'Accrual Acct', 'Retention Acct', 'Book', 'Remarks', 'Flags', 'Field17', 'Field18', 'Field19', 'Field20', 'Segment1', 'Segment2', 'Segment3', 'Segment4' ];
				}
				foreach( $arrobjAccrualGlDetails as $objGlExportBatchDetail ) {

					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) || false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) ? $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] : $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$arrmixGlDetail['gl_account_id']    = $objGlAccount->getId();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}
					if( ( $intPreviousPropertyId !== $objGlExportBatchDetail->getPropertyId() ) || ( $strPreviousPostMonth !== $objGlExportBatchDetail->getPostMonth() ) ) {
						$intPreviousPropertyId = $objGlExportBatchDetail->getPropertyId();
						$strPreviousPostMonth  = $objGlExportBatchDetail->getPostMonth();
						$intTransCount++;
					}
					$strPropertyGlBuCode = $objGlExportBatchDetail->getPropertyGlBu();
					$fltAmount           = $objGlExportBatchDetail->getAmount();
					$strNotes            = valStr( $objGlExportBatchDetail->getFrequencyId() ) ? CFrequency::frequencyIdToStr( $objGlExportBatchDetail->getFrequencyId() ) . ' Export' : 'GL Export';
					$strAccountNumber    = $objGlExportBatchDetail->getAccountNumber();

					$intBookTypeId = 1000;
					if( true != $boolIsBookMethodToBoth ) {
						$intBookTypeId = 1;
					}

					$this->m_arrstrFileContents['Accrual'][] = [ 'J', $intTransCount, ' ', ' ', date( 'm/t/Y', strtotime( $objGlExportBatchDetail->getPostMonth() ) ), date( 'M-y', strtotime( $objGlExportBatchDetail->getPostMonth() ) ), ' ', $strNotes, $strPropertyGlBuCode, $fltAmount, $strAccountNumber, ' ', ' ', $intBookTypeId, 'Summary Only' ];
				}
			}
		} else {
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			if( false != valArr( $arrmixCashGlDetails ) ) {
				if( true == $boolIsIncludeHeader ) {
					$this->m_arrstrFileContents['Cash'][] = [ 'Trans type', 'Trans No', 'Person code', 'Name', 'Trans Date', 'Post Month', 'Reference', 'Notes', 'Property', 'Amount', 'Account', 'Accrual Acct', 'Retention Acct', 'Book', 'Remarks', 'Flags', 'Field17', 'Field18', 'Field19', 'Field20', 'Segment1', 'Segment2', 'Segment3', 'Segment4' ];
				}
				foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) || false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) ? $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] : $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['gl_account_id']    = $objGlAccount->getId();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}
					$intTransNumber    = $arrmixGlDetail['id'];
					$strTransPostMonth = date( 'm/01/Y', strtotime( $arrmixGlDetail['post_month'] ) );
					$strNotes          = 'GL Export';
					if( valStr( $arrmixGlDetail['frequency_id'] ) ) {
						if( in_array( $arrmixGlDetail['frequency_id'], CFrequency::$c_arrintScheduledBasicFrequencies ) ) {
							$strNotes = CFrequency::frequencyIdToStr( $arrmixGlDetail['frequency_id'] ) . ' Export';
						} else {
							$strNotes = 'Scheduled Export';
						}
					}
					$strPropertyGlBuCode = $arrmixGlDetail['value'];
					$fltAmount           = $arrmixGlDetail['amount'];
					$strAccountNumber    = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$intBookTypeId = 1000;
					if( true != $boolIsBookMethodToBoth ) {
						$intBookTypeId = 0;
					}

					if( false != valStr( $arrmixGlDetail['gl_transaction_type_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
						$strRemarks = $arrmixGlDetail['reference'];
					} elseif( false != valStr( $arrmixGlDetail['lease_id'] ) ) {
						$strRemarks = $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
						if( false != valStr( $arrmixGlDetail['unit_number'] ) ) {
							$strRemarks .= ' Unit # ' . $arrmixGlDetail['unit_number'];
						}
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} elseif( false != valStr( $arrmixGlDetail['lease_customer_id'] ) ) {
						$strRemarks = $arrmixGlDetail['c1_name_first'] . ' ' . $arrmixGlDetail['c1_name_last'] . '-' . $arrmixGlDetail['reference'];
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} elseif( false != valStr( $arrmixGlDetail['ap_payee_id'] ) ) {
						$strRemarks = $arrmixGlDetail['company_name'] . '-' . $arrmixGlDetail['reference'];
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} else {
						$strRemarks = $arrmixGlDetail['memo_yardi'];
					}

					$this->m_arrstrFileContents['Cash'][] = [ 'J', $arrmixGlDetail['record_sequence'], ' ', ' ', date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_post_date'] ) ), $strTransPostMonth, $intTransNumber, $strNotes, $strPropertyGlBuCode, $fltAmount, $strAccountNumber, ' ', ' ', $intBookTypeId, $strRemarks, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' ];
				}
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				if( true == $boolIsIncludeHeader ) {
					$this->m_arrstrFileContents['Accrual'][] = [ 'Trans type', 'Trans No', 'Person code', 'Name', 'Trans Date', 'Post Month', 'Reference', 'Notes', 'Property', 'Amount', 'Account', 'Accrual Acct', 'Retention Acct', 'Book', 'Remarks', 'Flags', 'Field17', 'Field18', 'Field19', 'Field20', 'Segment1', 'Segment2', 'Segment3', 'Segment4' ];
				}
				foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) || false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) ? $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] : $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['gl_account_id']    = $objGlAccount->getId();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}

					$intTransNumber    = $arrmixGlDetail['id'];
					$strTransPostMonth = date( 'm/01/Y', strtotime( $arrmixGlDetail['post_month'] ) );
					$strNotes          = 'GL Export';
					if( valStr( $arrmixGlDetail['frequency_id'] ) ) {
						if( in_array( $arrmixGlDetail['frequency_id'], CFrequency::$c_arrintScheduledBasicFrequencies ) ) {
							$strNotes = CFrequency::frequencyIdToStr( $arrmixGlDetail['frequency_id'] ) . ' Export';
						} else {
							$strNotes = 'Scheduled Export';
						}
					}
					$strPropertyGlBuCode = $arrmixGlDetail['value'];
					$fltAmount           = $arrmixGlDetail['amount'];
					$strAccountNumber    = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];

					$intBookTypeId = 1000;
					if( true != $boolIsBookMethodToBoth ) {
						$intBookTypeId = 1;
					}

					if( false != valStr( $arrmixGlDetail['gl_transaction_type_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
						$strRemarks = $arrmixGlDetail['reference'];
					} elseif( false != valStr( $arrmixGlDetail['lease_id'] ) ) {
						$strRemarks = $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
						if( false != valStr( $arrmixGlDetail['unit_number'] ) ) {
							$strRemarks .= ' Unit # ' . $arrmixGlDetail['unit_number'];
						}
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} elseif( false != valStr( $arrmixGlDetail['lease_customer_id'] ) ) {
						$strRemarks = $arrmixGlDetail['c1_name_first'] . ' ' . $arrmixGlDetail['c1_name_last'] . '-' . $arrmixGlDetail['reference'];
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} elseif( false != valStr( $arrmixGlDetail['ap_payee_id'] ) ) {
						$strRemarks = $arrmixGlDetail['company_name'] . '-' . $arrmixGlDetail['reference'];
						if( false != valStr( $arrmixGlDetail['description'] ) ) {
							$strRemarks .= ' - ' . $arrmixGlDetail['description'];
						}
					} else {
						$strRemarks = $arrmixGlDetail['memo_yardi'];
					}

					$this->m_arrstrFileContents['Accrual'][] = [ 'J', $arrmixGlDetail['record_sequence'], ' ', ' ', date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_post_date'] ) ), $strTransPostMonth, $intTransNumber, $strNotes, $strPropertyGlBuCode, $fltAmount, $strAccountNumber, ' ', ' ', $intBookTypeId, $strRemarks, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' ];
				}
			}
		}

		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) || false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_pm_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) || false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_pm_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_CSV;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_pm_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_CSV;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_CSV );
			}
		}
		return true;
	}

	public function exportApiFormat( $arrmixGlDetails, $intCompanyUserId, $objClientDatabase ) {

		$objCompanyTransmissionVendor = \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByTransmissionTypeIdByTransmissionVendorIdByCid( CTransmissionType::FINANCIAL, CTransmissionVendor::YARDI, $this->getCid(), $objClientDatabase );

		if( false == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) || false == valStr( $objCompanyTransmissionVendor->getRequestUrl() ) ) {
			return false;
		}

		$strRequestUrl 		= $objCompanyTransmissionVendor->getRequestUrl();
		$strUsername 		= $objCompanyTransmissionVendor->getUsername();
		$strPassword 		= $objCompanyTransmissionVendor->getPassword();
		$strDatabaseName	= $objCompanyTransmissionVendor->getVendorCode();

		try {
			$arrmixSoapParams 	= [ 'trace' => 1, 'exceptions' => 1, 'keep_alive' => false ];
			$objSoapClient 	= new CPsiSoapClient( $strRequestUrl, $arrmixSoapParams );

		} catch( SoapFault $objSoapFault ) {

			$objTransmission = $objCompanyTransmissionVendor->createTransmission();
			$objTransmission->setResponseContent( serialize( $objSoapFault->faultstring ) );
			$objTransmission->setResponse( 'Failed to create the soap client : ' . $objSoapFault->faultstring );
			$objTransmission->setIsFailed( 1 );

			$objTransmission->insert( $intCompanyUserId, $objClientDatabase );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'WSDL could not be consumed: {%s,0}', [ $objSoapFault->faultstring ] ) ) );
			return false;
		}

		// Custom WSDL
		$objSoapClient->__setLocation( $strRequestUrl );

		$arrmixGlTransactions	= [];

		foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
			if( 0 == $arrmixGlDetail['amount'] ) continue;
			$arrmixGlTransactions[] = $arrmixGlDetail;
		}

		$strGlExportBatchId				= NULL;
		$arrmixChunkedGlTransactions	= array_chunk( $arrmixGlTransactions, 25, true );
		$intLastBatchId 				= \Psi\Libraries\UtilFunctions\count( $arrmixChunkedGlTransactions ) - 1;

		foreach( $arrmixChunkedGlTransactions as $intBatchId => $arrmixGlTransactions ) {

			$arrobjStdGlTransactions 	= [];

			$objSoapRequest = new stdClass();
			$objSoapRequest->authInfo = new stdClass();

			$objSoapRequest->authInfo->username = $this->getSyncEncryption()->encryptText( $strUsername );
			$objSoapRequest->authInfo->password = $this->getSyncEncryption()->encryptText( $strPassword );
			$objSoapRequest->authInfo->dbName   = $this->getSyncEncryption()->encryptText( $strDatabaseName );

			$objSoapRequest->glTransactionsRequests 	= new stdClass();

			$boolIsLastBatch = ( $intBatchId == $intLastBatchId ) ? '1' : '0';

			foreach( $arrmixGlTransactions as $arrmixGlDetail ) {

				$intTransNumber			= $arrmixGlDetail['id'];
				$strTransPostMonth		= date( 'm/01/Y', strtotime( $arrmixGlDetail['post_month'] ) );
				$strNotes				= str_replace( ',', ' ',  $arrmixGlDetail['account_name'] );
				$strPropertyGlBuCode	= $arrmixGlDetail['value'];
				$fltAmount				= $arrmixGlDetail['amount'];
				$strRemarks 			= str_replace( ',', ' ',  $arrmixGlDetail['memo_yardi'] );

				$strAccountNumber = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number']  : $arrmixGlDetail['account_number'];

				$intBookTypeId = 1000;

				$objStdGlTransaction = new stdClass();

				$objStdGlTransaction->transactionType 			= 'J';
				$objStdGlTransaction->transactionNumber 		= '';
				$objStdGlTransaction->glExportBatchId			= $strGlExportBatchId;
				$objStdGlTransaction->personCode 				= '';
				$objStdGlTransaction->name 						= '';
				$objStdGlTransaction->transactionDate 			= date( 'm/d/Y', strtotime( $arrmixGlDetail['postdate'] ) );
				$objStdGlTransaction->postMonth 				= $strTransPostMonth;
				$objStdGlTransaction->reference 				= $intTransNumber;
				$objStdGlTransaction->notes 					= $strNotes;
				$objStdGlTransaction->propertyId 				= $strPropertyGlBuCode;
				$objStdGlTransaction->amount 					= $fltAmount;
				$objStdGlTransaction->accountNumber 			= $strAccountNumber;
				$objStdGlTransaction->accrualAccountNumber		= '';
				$objStdGlTransaction->retentionAccountNumber 	= '';
				$objStdGlTransaction->bookNumber 				= $intBookTypeId;
				$objStdGlTransaction->description 				= $strRemarks;
				$objStdGlTransaction->hOffsetAcct 				= '';
				$objStdGlTransaction->hParent1 					= '';
				$objStdGlTransaction->hParent2 					= '';
				$objStdGlTransaction->iType 					= '';
				$objStdGlTransaction->hPerson 					= '';
				$objStdGlTransaction->uRef 						= '';
				$objStdGlTransaction->hUnit 					= '';
				$objStdGlTransaction->isLastBatch				= $boolIsLastBatch;

				$arrobjStdGlTransactions[] = $objStdGlTransaction;
			}

			$objSoapRequest->glTransactionsRequests->sendGLTransactionsRequest 	= $arrobjStdGlTransactions;

			$boolIsSoapFailed 	= false;
			$strResponseContent	= '';
			$objTransmission	= $objCompanyTransmissionVendor->createTransmission();

			try {
				$objReturnValue = $objSoapClient->sendGLTransactions( $objSoapRequest );

			} catch( SoapFault $objSoapFault ) {
				$boolIsSoapFailed			= true;
				$strSoapFaultErrorMessage 	= $objSoapFault->faultstring;
			}

			if( true == $boolIsSoapFailed ) {
				$objTransmission->setResponse( 'Failed to process the request : ' . $strSoapFaultErrorMessage );
				$objTransmission->setIsFailed( 1 );
				$strResponseContent = $strSoapFaultErrorMessage;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Unable to process request: {%s, 0}', [ $strSoapFaultErrorMessage ] ) ) );
			} else {

				if( false == isset( $objReturnValue->sendGLTransactionsResult->resultInfo->successful ) || 1 != $objReturnValue->sendGLTransactionsResult->resultInfo->successful ) {
					$objTransmission->setIsFailed( 1 );
					$objTransmission->setResponse( $objReturnValue->sendGLTransactionsResult->resultInfo->messages->string );
				}

				$strResponseContent = $objReturnValue->sendGLTransactionsResult;
			}

			$objTransmission->setResponseContent( serialize( $strResponseContent ) );
			$objTransmission->setRequestContent( serialize( $objSoapRequest ) );

			$objTransmission->insert( $intCompanyUserId, $objClientDatabase );

			if( false == isset( $objReturnValue->sendGLTransactionsResult->resultInfo->successful ) || 1 != $objReturnValue->sendGLTransactionsResult->resultInfo->successful ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objReturnValue->sendGLTransactionsResult->resultInfo->messages->string, NULL ) );
				return false;
			}

			if( true == isset( $objReturnValue->sendGLTransactionsResult->glExportBatchId ) && true == valStr( $objReturnValue->sendGLTransactionsResult->glExportBatchId ) ) {
				$strGlExportBatchId = trim( $objReturnValue->sendGLTransactionsResult->glExportBatchId );
			}
		}

		return true;

	}

	public function download( $strFileExtension = NULL, $strWorkSheetName = 'Sheet1', $strDelimiter = ',', $strEnclosure = '"' ) {

		switch( $strFileExtension ) {
			case self::FILE_EXTENSION_CSV:
				$strFiveMbMemoryLimit = 5 * 1024 * 1024;
				$objFileResource      = CFileIo::fileOpen( 'php://temp/maxmemory:' . $strFiveMbMemoryLimit, 'w' );
				// setting default file encoding to UTF-8

				if( CONFIG_CLOUD_ID == CCloud::CHINA_ID ) {
					fprintf( $objFileResource, chr( 0xEF ) . chr( 0xBB ) . chr( 0xBF ) );
				}
				foreach( $this->m_arrstrFileContents as $arrstrFileContent ) {

					CFileIo::filePutCSV( $objFileResource, $arrstrFileContent, $strDelimiter, $strEnclosure );
				}
				rewind( $objFileResource );
				$strCsvContent = stream_get_contents( $objFileResource );
				CFileIo::fileClose( $objFileResource );
				$strCsvContent = \Psi\CStringService::singleton()->substr( $strCsvContent, 0, \Psi\CStringService::singleton()->strlen( $strCsvContent ) - \Psi\CStringService::singleton()->strlen( PHP_EOL ) );
				header( 'Pragma: public' );
				header( 'Expires: 0' );
				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Cache-Control: private', false );
				header( 'Content-type: application/csv' );
				header( 'Content-Disposition: attachment; filename="' . $this->m_strFileName . '"' );
				header( 'Content-Transfer-Encoding: binary' );
				print $strCsvContent;
				exit;
				break;

			case self::FILE_EXTENSION_XLS:
				ob_end_clean();
				$objExcelBook     = CExcelBookFactory::createExcelBook( true );
				$strWorkSheetName = $strWorkSheetName;
				$objWorkSheet     = $objExcelBook->addSheet( $strWorkSheetName );
				$strColumnFormat  = $objExcelBook->addFormat();
				$strColumnFormat->horizontalAlign( ExcelFormat::ALIGNH_RIGHT );
				$strColumnFormat->numberFormat( ExcelFormat::NUMFORMAT_GENERAL );
				$arrstrRowValues = explode( "\n", $this->m_strFileContent );
				$intRow          = 0;
				foreach( $arrstrRowValues as $strRowValue ) {
					$arrstrColumnValues = explode( "\t", $strRowValue );
					$intColumn          = 0;

					foreach( $arrstrColumnValues as $strColumnValue ) {
						if( false != is_numeric( $strColumnValue ) ) {
							$strColumnValue = ( float ) $strColumnValue;
						}
						$objWorkSheet->write( $intRow, $intColumn, $strColumnValue, $strColumnFormat );
						$intColumn++;
					}
					$intRow++;
				}
				header( 'Pragma: public' );
				header( 'Expires: 0' );
				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Cache-Control: private', false );
				header( 'Content-type: application/vnd.ms-excel' );
				header( 'Content-Disposition: attachment; filename="' . $this->m_strFileName . '"' );
				header( 'Content-Transfer-Encoding: binary' );
				$objExcelBook->save( 'php://output' );
				exit;
				break;

			default:
				header( 'Pragma: public' );
				header( 'Expires: 0' );
				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Cache-Control: private', false );
				header( 'Content-type: text/plain' );
				header( 'Content-Disposition: attachment; filename="' . $this->m_strFileName . '"' );
				header( 'Content-Transfer-Encoding: binary' );
				print $this->m_strFileContent;
				exit;
				break;
		}
	}

	private function downloadAsZip( $strFileExtension = NULL, $strDelimiter = ',', $strEnclosure = '"' ) {
		$strTempDir = CConfig::get( 'cache_directory' );
		if( false == CFileIo::isDirectory( $strTempDir . 'GlExport/' ) ) {
			if( false == CFileIo::recursiveMakeDir( $strTempDir . 'GlExport/' ) ) {
				trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

				return false;
			}
		}
		switch( $strFileExtension ) {

			case self::FILE_EXTENSION_XLS:
				foreach( $this->m_arrstrFileContents as $strFileType => $strFileContent ) {

					$objExcelBook     = CExcelBookFactory::createExcelBook( true );
					$strWorkSheetName = 'Sheet1';
					$objWorkSheet     = $objExcelBook->addSheet( $strWorkSheetName );

					$strColumnFormat = $objExcelBook->addFormat();
					$strColumnFormat->horizontalAlign( ExcelFormat::ALIGNH_RIGHT );
					$strColumnFormat->numberFormat( ExcelFormat::NUMFORMAT_GENERAL );

					$strFilePath = CONFIG_CACHE_DIRECTORY . $this->m_arrstrFileNames[$strFileType];

					$arrstrFiles[]   = $strFilePath;
					$arrstrRowValues = explode( "\n", $strFileContent );
					$intRow          = 0;
					foreach( $arrstrRowValues as $intKey => $arrmixFileContents ) {
						$arrstrRowValuesTemp = explode( "\n", $arrmixFileContents );
						$intColumn           = 0;
						foreach( $arrstrRowValuesTemp as $strRowValue ) {
							$arrstrColumnValues = explode( "\t", $strRowValue );

							foreach( $arrstrColumnValues as $strColumnValue ) {
								if( false != is_numeric( $strColumnValue ) ) {
									$strColumnValue = ( float ) $strColumnValue;
								}
								$objWorkSheet->write( $intRow, $intColumn, $strColumnValue, $strColumnFormat );
								$intColumn++;
							}
							$intRow++;
						}
					}
					$objExcelBook->save( $strFilePath );
				}
				break;

			case self::FILE_EXTENSION_CSV:
				foreach( $this->m_arrstrFileContents as $intKey => $arrmixFileContents ) {
					// Temporary File path
					if( ( false != isset( $arrmixFileContents['Accrual'] ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFileContents['Accrual'] ) ) || ( false != isset( $arrmixFileContents['Cash'] ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFileContents['Cash'] ) ) ) {
						foreach( $arrmixFileContents as $strAccountingMethod => $arrmixAccountingMethodwiseContents ) {
							$strCacheFilename    = $this->m_arrstrFileNames[$intKey][$strAccountingMethod];
							$strFileDownloadPath = 'GlExport' . DIRECTORY_SEPARATOR . $strCacheFilename;
							// Check if the cache directory exists. If it doesn't create it.
							if( !is_dir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
								if( false == CFileIo::recursiveMakeDir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
									trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

									return false;
								}
							}
							$arrstrFiles[]   = $strTempDir . $strFileDownloadPath;
							$objFileResource = CFileIo::fileOpen( $strTempDir . $strFileDownloadPath, 'w' );
							// setting default file encoding to UTF-8
							if( CONFIG_CLOUD_ID == CCloud::CHINA_ID ) {
								fprintf( $objFileResource, chr( 0xEF ) . chr( 0xBB ) . chr( 0xBF ) );
							}
							foreach( $arrmixAccountingMethodwiseContents as $strFileContent ) {
								CFileIo::filePutCSV( $objFileResource, $strFileContent, $strDelimiter, $strEnclosure );
							}
						}

					} else {
						$strCacheFilename = $this->m_arrstrFileNames[$intKey];

						$strFileDownloadPath = 'GlExport' . DIRECTORY_SEPARATOR . $strCacheFilename;
						// Check if the cache directory exists. If it doesn't create it.
						if( !is_dir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
							if( false == CFileIo::recursiveMakeDir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
								trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

								return false;
							}
						}
						$arrstrFiles[]   = $strTempDir . $strFileDownloadPath;
						$objFileResource = CFileIo::fileOpen( $strTempDir . $strFileDownloadPath, 'w' );
						// setting default file encoding to UTF-8
						if( CONFIG_CLOUD_ID == CCloud::CHINA_ID ) {
							fprintf( $objFileResource, chr( 0xEF ) . chr( 0xBB ) . chr( 0xBF ) );
						}
						foreach( $arrmixFileContents as $strFileContent ) {
							CFileIo::filePutCSV( $objFileResource, $strFileContent, $strDelimiter );
						}
					}

				}
				break;

			case self::FILE_EXTENSION_TXT:
			case self::FILE_EXTENSION_IIF:
			default:

				foreach( $this->m_arrstrFileContents as $intKey => $arrmixFileContents ) {
					$strCacheFilename = $this->m_arrstrFileNames[$intKey];
					// Temporary File path
					$strFileDownloadPath = 'GlExport' . DIRECTORY_SEPARATOR . $strCacheFilename;

					// Check if the cache directory exists. If it doesn't create it.
					if( !is_dir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
						if( false == CFileIo::recursiveMakeDir( $strTempDir . 'GlExport' . DIRECTORY_SEPARATOR ) ) {
							trigger_error( 'Invalid Directory Creation: report caching directory could not be created.', E_USER_WARNING );

							return false;
						}
					}
					$arrstrFiles[] = $strTempDir . $strFileDownloadPath;
					CFileIO::filePutContents( $strTempDir . $strFileDownloadPath, $arrmixFileContents );
				}

		}

		// Create a zip file.
		$strZipFileName = $strTempDir . 'GlExport/' . 'Gl_export' . date( '_m_d_y_His', mktime() ) . '.zip';
		$objZipArchive  = new ZipArchive();
		$objZipArchive->open( $strZipFileName, ZipArchive::CREATE );

		// Loop through all files that needs to be zipped.
		foreach( $arrstrFiles as $strFile ) {
			if( true == valFile( $strFile ) ) {
				$strLocalFileName = basename( $strFile );
				$objZipArchive->addFile( $strFile, $strLocalFileName );
			} else {
				trigger_error( 'Not a valid file to zip.', E_USER_WARNING );

				return false;
			}
		}
		$objZipArchive->close();
		ob_end_clean();
		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-type:application/zip' );
		header( 'Content-Disposition: attachment; filename=' . basename( $strZipFileName ) );
		header( 'Content-Length: ' . filesize( $strZipFileName ) );
		header( 'Content-Transfer-Encoding:binary' );

		// Disable output compression as it creates problem for large file downloads.
		if( ini_get( 'zlib.output_compression' ) ) {
			ini_set( 'zlib.output_compression', 'Off' );
		}
		$resFilePointer = CFileIo::fileOpen( $strZipFileName, 'rb' );
		while( !feof( $resFilePointer ) ) {
			print ( fread( $resFilePointer, 1024 * 8 ) );
			ob_flush();
			flush();
		}
		fclose( $resFilePointer );
		unlink( $strZipFileName );
		if( false != valArr( $arrstrFiles ) ) {
			foreach( $arrstrFiles as $strFile ) {
				unlink( $strFile );
			}
		}
		exit;
	}

	public function exportInYardiEtlFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $intTransmissionVendorId ) {

		$arrintGlAccountId      = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];

		$strFileName = '_yardi_etl_export_batch_no_';
		if( CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS == $intTransmissionVendorId ) {
			$strFileName = '_yardi_etl_gs_export_batch_no_';
		}
		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrobjAccountingExportBatchDetails = ( array ) $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intTransmissionVendorId );

			if( false == valArr( $arrobjAccountingExportBatchDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrobjAccountingExportBatchDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() && 0 != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() && 0 != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[] = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrobjAccountingExportBatchDetails;
				} else {
					$arrobjAccrualGlDetails = $arrobjAccountingExportBatchDetails;
				}
			}
			$arrstrHeaders        = [ 'FinJournals' ];
			$arrstrFileColumnsName = [
				'TRANNUM', 'DATE',
				'PROPERTY', 'ACCOUNT', 'POSTMONTH', 'BOOKNUM', 'AMOUNT', 'REMARK', 'REF', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2',
				'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4', 'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'TAXPOINTDATE',
				'DESC', 'DOCUMENTSEQUENCENUMBER', 'TRANAMOUNT', 'DETILEDTRANAMOUNT', 'LEGALENTITYID', 'BASECURRENCYID', 'TRANCURRENCYID', 'EXCHANGERATE',
				'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE', 'DETAILNOTES', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3',
				'SEGMENT4', 'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'TAXAMOUNT1', 'TAXAMOUNT2', 'DETAILVATTRANTYPEID',
				'DETAILVATRATEID', 'VATRATETYEID', 'VATRATEID', 'DISPLAYTYPE', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'NOTES2', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3',
				'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8'
			];
			$intTranNum  = 0;
			$strPostMonth = NULL;
			$strFileIndex = 1;
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = $arrstrHeaders;
				$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = $arrstrFileColumnsName;

				foreach( $arrobjCashGlDetails as $objAccountingExportBatchDetail ) {
					if( 0 == $objAccountingExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && ( false != isset( $arrobjAccountDetails[$objAccountingExportBatchDetail->getCashGlAccountId()] ) || false != isset( $arrobjAccountDetails[$objAccountingExportBatchDetail->getAccrualGlAccountId()] ) ) ) {
						$objGlAccount                       = ( false != isset( $arrobjAccountDetails[$objAccountingExportBatchDetail->getCashGlAccountId()] ) ) ? $arrobjAccountDetails[$objAccountingExportBatchDetail->getCashGlAccountId()] : $arrobjAccountDetails[$objAccountingExportBatchDetail->getAccrualGlAccountId()];
						$objAccountingExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objAccountingExportBatchDetail->setAccountName( $objGlAccount->getName() );
					}
					if( $objAccountingExportBatchDetail->getPostMonth() != $strPostMonth ) {
						$intTranNum++;
					}
					$strDescription        = $objAccountingExportBatchDetail->getAccountName();
					$strDate               = date( 'm/1/Y', strtotime( $objAccountingExportBatchDetail->getPostMonth() ) );
					$strPropertyLookUpCode = $objAccountingExportBatchDetail->getLookUpCode();
					$strAccountNumber      = $objAccountingExportBatchDetail->getAccountNumber();
					$strPostMonth          = $objAccountingExportBatchDetail->getPostMonth();
					$intBookNum            = 1000;
					$fltAmount             = $objAccountingExportBatchDetail->getAmount();
					$strRemark             = 'Entrata';
					$strReference          = 'SUMMARYENTRY ' . date( 'mY', strtotime( $objAccountingExportBatchDetail->getPostMonth() ) );
					if( CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS == $intTransmissionVendorId ) {
						$strReference = 'Entrata';
						$strDescription = ' ';
					}

					$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = [
						$intTranNum, $strDate, $strPropertyLookUpCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount,
						$strRemark, $strReference, '', '', '', '', '', '', '', '', '', $strDescription, '', '', '', '', '', '', '', '', '',
						'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}

				$this->m_arrstrFileNames[$strFileIndex . '_cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strFileName . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$strFileIndex . '_cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $this->m_arrstrFileNames[$strFileIndex . '_cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames[$strFileIndex . '_cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$strFileIndex = 1;
				$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = $arrstrHeaders;
				$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = $arrstrFileColumnsName;

				foreach( $arrobjAccrualGlDetails as $objAccountingExportBatchDetail ) {
					if( 0 == $objAccountingExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objAccountingExportBatchDetail->getAccrualGlAccountId()] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$objAccountingExportBatchDetail->getAccrualGlAccountId()];
						$objAccountingExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
						$objAccountingExportBatchDetail->setAccountName( $objGlAccount->getName() );
					}
					if( $objAccountingExportBatchDetail->getPostMonth() != $strPostMonth ) {
						$intTranNum++;
					}
					$strDescription        = $objAccountingExportBatchDetail->getAccountName();
					$strDate               = date( 'm/1/Y', strtotime( $objAccountingExportBatchDetail->getPostMonth() ) );
					$strPropertyLookUpCode = $objAccountingExportBatchDetail->getLookUpCode();
					$strAccountNumber      = $objAccountingExportBatchDetail->getAccountNumber();
					$strPostMonth          = $objAccountingExportBatchDetail->getPostMonth();
					$intBookNum            = 1000;
					$fltAmount             = $objAccountingExportBatchDetail->getAmount();
					$strRemark             = 'Entrata';
					$strReference          = 'SUMMARYENTRY ' . date( 'mY', strtotime( $objAccountingExportBatchDetail->getPostMonth() ) );
					if( CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS == $intTransmissionVendorId ) {
						$strReference = 'Entrata';
						$strDescription = ' ';
					}
					$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = [
						$intTranNum, $strDate, $strPropertyLookUpCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount,
						$strRemark, $strReference, '', '', '', '', '', '', '', '', '', $strDescription, '', '', '', '', '', '', '', '', '',
						'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}

				$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strFileName . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$strFileIndex]['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $this->m_arrstrFileNames[$strFileIndex]['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames[$strFileIndex . '_accrual'] .= '.' . self::FILE_EXTENSION_CSV;
			}
		} else {
			$strFileIndex = NULL;
			$arrmixGlDetails       = $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intTransmissionVendorId );

			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[] = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			if( false == valArr( $arrmixCashGlDetails ) && false == valArr( $arrmixAccrualGlDetails ) ) {
				return false;
			}
			$arrstrFileName        = [ 'FinJournals' ];
			$arrstrFileColumnsName = [
				'TRANNUM', 'DATE',
				'PROPERTY', 'ACCOUNT', 'POSTMONTH', 'BOOKNUM', 'AMOUNT', 'REMARK', 'REF', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2',
				'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4', 'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'TAXPOINTDATE',
				'DESC', 'DOCUMENTSEQUENCENUMBER', 'TRANAMOUNT', 'DETILEDTRANAMOUNT', 'LEGALENTITYID', 'BASECURRENCYID', 'TRANCURRENCYID', 'EXCHANGERATE',
				'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE', 'DETAILNOTES', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3',
				'SEGMENT4', 'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'TAXAMOUNT1', 'TAXAMOUNT2', 'DETAILVATTRANTYPEID',
				'DETAILVATRATEID', 'VATRATETYEID', 'VATRATEID', 'DISPLAYTYPE', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'NOTES2', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3',
				'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8'
			];
			if( false != valArr( $arrmixCashGlDetails ) ) {
				foreach( $arrmixCashGlDetails as $arrmixKeyedGlDetail ) {
					$arrmixPropertyLookupCodes[$arrmixKeyedGlDetail['lookup_code']][]                                                 = $arrmixKeyedGlDetail['lookup_code'];
					$arrmixKeyedPropertyPostMonthGlDetails[$arrmixKeyedGlDetail['lookup_code']][$arrmixKeyedGlDetail['post_month']][] = $arrmixKeyedGlDetail;
				}

				foreach( $arrmixKeyedPropertyPostMonthGlDetails as $intKeyPropertyLookupCode => $arrmixKeyedPostMonthGlDetails ) {
					foreach( $arrmixKeyedPostMonthGlDetails as $intKeyPostMonth => $arrmixKeyedGlDetails ) {
							$strFileIndex = $intKeyPropertyLookupCode . '_' . date( 'mY', strtotime( $intKeyPostMonth ) );
							$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = $arrstrFileName;
							$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = $arrstrFileColumnsName;
						foreach( $arrmixKeyedGlDetails as $arrmixGlDetail ) {
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
								if( false != valStr( $intKeyPropertyLookupCode ) ) {
									$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $intKeyPropertyLookupCode . '_' . date( 'mY', strtotime( $intKeyPostMonth ) ) . '_' . date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_etl_export_batch_no_' . $this->getId();
								} else {
									$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $arrmixGlDetail['property_id'] . '_' . date( 'mY', strtotime( $intKeyPostMonth ) ) . '_' . date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_etl_export_batch_no_' . $this->getId();
								}
							$this->m_arrstrFileNames[$strFileIndex . '_cash'] .= '_cash';
							if( false != valStr( $this->getFileNamePrefix() ) ) {
								$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$strFileIndex . '_cash'];
							}
							if( false != valStr( $this->getFileNameSuffix() ) ) {
								$this->m_arrstrFileNames[$strFileIndex . '_cash'] = $this->m_arrstrFileNames[$strFileIndex . '_cash'] . '_' . $this->getFileNameSuffix();
							}
							$this->m_arrstrFileNames[$strFileIndex . '_cash'] .= '.' . self::FILE_EXTENSION_CSV;
							if( 0 == $arrmixGlDetail['amount'] ) {
								continue;
							}
							$strDetailNotes         = NULL;
							$fltAmount              = $arrmixGlDetail['amount'];
							$strRemark              = 'Entrata';
							$intBookNum             = 1000;
							$strReference           = \Psi\CStringService::singleton()->substr( $arrmixGlDetail['reference'], 0, 25 );
							$strPostMonth           = date( 'm/d/Y', strtotime( $arrmixGlDetail['post_month'] ) );
							$strLookupCode          = $arrmixGlDetail['lookup_code'];
							$strAccountName         = $arrmixGlDetail['account_name']; // Descriptionl
							$strAccountNumber       = $arrmixGlDetail['account_number'];
							$intTransactionId       = 1;
							$strTransactionDateTime = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
							if( CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS == $intTransmissionVendorId ) {
								$strReference = 'Entrata';
								$strAccountName = ' ';
							}
							if( false != valStr( $arrmixGlDetail['gl_transaction_type_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
								$strDetailNotes = $arrmixGlDetail['reference'];
							} elseif( false != valStr( $arrmixGlDetail['lease_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
								if( false != valStr( $arrmixGlDetail['unit_number_cache'] ) ) {
									$strDetailNotes .= ' Unit # ' . $arrmixGlDetail['unit_number_cache'];
								}
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} elseif( false != valStr( $arrmixGlDetail['lease_customer_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['c1_name_first'] . ' ' . $arrmixGlDetail['c1_name_last'] . '-' . $arrmixGlDetail['yardi_reference'];
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} elseif( false != valStr( $arrmixGlDetail['ap_payee_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['company_name'] . '-' . $arrmixGlDetail['yardi_reference'];
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} else {
								$strDetailNotes = $arrmixGlDetail['memo_yardi'];
							}
							$strDetailNotes                                       = \Psi\CStringService::singleton()->preg_replace( '/[\r\n]/', ' ', \Psi\CStringService::singleton()->substr( $strDetailNotes, 0, 255 ) );
							$strDetailNotes                                       = \Psi\CStringService::singleton()->preg_replace( '/["]/', '', \Psi\CStringService::singleton()->substr( $strDetailNotes, 0, 255 ) );
							$this->m_arrstrFileContents[$strFileIndex . '_cash'][] = [
								$intTransactionId, $strTransactionDateTime, $strLookupCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount, $strRemark,
								$strReference, '', '', '', '', '', '', '', '', '', $strAccountName, '', '', '', '', '', '', '', '', '', '', '', '', '', $strDetailNotes, '', '', '', '', '', '', '',
								'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
							];
						}
					}
				}
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$arrmixPropertyLookupCodes = [];
				$arrmixKeyedPropertyPostMonthGlDetails = [];
				foreach( $arrmixAccrualGlDetails as $arrmixKeyedGlDetail ) {
					$arrmixPropertyLookupCodes[$arrmixKeyedGlDetail['lookup_code']][]                                                 = $arrmixKeyedGlDetail['lookup_code'];
					$arrmixKeyedPropertyPostMonthGlDetails[$arrmixKeyedGlDetail['lookup_code']][$arrmixKeyedGlDetail['post_month']][] = $arrmixKeyedGlDetail;
				}

				foreach( $arrmixKeyedPropertyPostMonthGlDetails as $intKeyPropertyLookupCode => $arrmixKeyedPostMonthGlDetails ) {
					foreach( $arrmixKeyedPostMonthGlDetails as $intKeyPostMonth => $arrmixKeyedGlDetails ) {
							$strFileIndex = $intKeyPropertyLookupCode . '_' . date( 'mY', strtotime( $intKeyPostMonth ) );
							$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = $arrstrFileName;
							$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = $arrstrFileColumnsName;
						foreach( $arrmixKeyedGlDetails as $arrmixGlDetail ) {
							if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
								$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
								$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
								$arrmixGlDetail['account_name'] = $objGlAccount->getName();
							}
								if( false != valStr( $intKeyPropertyLookupCode ) ) {
									$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $intKeyPropertyLookupCode . '_' . date( 'mY', strtotime( $intKeyPostMonth ) ) . '_' . date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strFileName . $this->getId();
								} else {
									$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $arrmixGlDetail['property_id'] . '_' . date( 'mY', strtotime( $intKeyPostMonth ) ) . '_' . date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strFileName . $this->getId();
								}
							$this->m_arrstrFileNames[$strFileIndex . '_accrual'] .= '_accrual';
							if( false != valStr( $this->getFileNamePrefix() ) ) {
								$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$strFileIndex . '_accrual'];
							}
							if( false != valStr( $this->getFileNameSuffix() ) ) {
								$this->m_arrstrFileNames[$strFileIndex . '_accrual'] = $this->m_arrstrFileNames[$strFileIndex . '_accrual'] . '_' . $this->getFileNameSuffix();
							}
							$this->m_arrstrFileNames[$strFileIndex . '_accrual'] .= '.' . self::FILE_EXTENSION_CSV;
							if( 0 == $arrmixGlDetail['amount'] ) {
								continue;
							}
							$strDetailNotes         = NULL;
							$fltAmount              = $arrmixGlDetail['amount'];
							$strRemark              = 'Entrata';
							$intBookNum             = 1000;
							$strReference           = \Psi\CStringService::singleton()->substr( $arrmixGlDetail['reference'], 0, 25 );
							$strPostMonth           = date( 'm/d/Y', strtotime( $arrmixGlDetail['post_month'] ) );
							$strLookupCode          = $arrmixGlDetail['lookup_code'];
							$strAccountName         = $arrmixGlDetail['account_name']; // Descriptionl
							$strAccountNumber       = $arrmixGlDetail['account_number'];
							$intTransactionId       = 1;
							$strTransactionDateTime = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
							if( CTransmissionVendor::GL_EXPORT_YARDI_ETL_GS == $intTransmissionVendorId ) {
								$strReference = 'Entrata';
								$strAccountName = ' ';
							}
							if( false != valStr( $arrmixGlDetail['gl_transaction_type_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
								$strDetailNotes = $arrmixGlDetail['reference'];
							} elseif( false != valStr( $arrmixGlDetail['lease_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
								if( false != valStr( $arrmixGlDetail['unit_number_cache'] ) ) {
									$strDetailNotes .= ' Unit # ' . $arrmixGlDetail['unit_number_cache'];
								}
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} elseif( false != valStr( $arrmixGlDetail['lease_customer_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['c1_name_first'] . ' ' . $arrmixGlDetail['c1_name_last'] . '-' . $arrmixGlDetail['yardi_reference'];
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} elseif( false != valStr( $arrmixGlDetail['ap_payee_id'] ) ) {
								$strDetailNotes = $arrmixGlDetail['company_name'] . '-' . $arrmixGlDetail['yardi_reference'];
								if( false != valStr( $arrmixGlDetail['detail_memo'] ) ) {
									$strDetailNotes .= ' - ' . $arrmixGlDetail['detail_memo'];
								}
							} else {
								$strDetailNotes = $arrmixGlDetail['memo_yardi'];
							}
							$strDetailNotes                                       = \Psi\CStringService::singleton()->preg_replace( '/[\r\n]/', ' ', \Psi\CStringService::singleton()->substr( $strDetailNotes, 0, 255 ) );
							$strDetailNotes                                       = \Psi\CStringService::singleton()->preg_replace( '/["]/', '', \Psi\CStringService::singleton()->substr( $strDetailNotes, 0, 255 ) );
							$this->m_arrstrFileContents[$strFileIndex . '_accrual'][] = [
								$intTransactionId, $strTransactionDateTime, $strLookupCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount, $strRemark,
								$strReference, '', '', '', '', '', '', '', '', '', $strAccountName, '', '', '', '', '', '', '', '', '', '', '', '', '', $strDetailNotes, '', '', '', '', '', '', '',
								'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
							];
						}
					}
				}
			}
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			$arrmixExportData['file_content']   = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( false != valArr( $this->m_arrstrFileNames, 2 ) || false != valArr( $this->m_arrstrFileNames[$strFileIndex], 2 ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName        = ( false != isset( $this->m_arrstrFileNames[$strFileIndex . '_cash'] ) ) ? $this->m_arrstrFileNames[$strFileIndex . '_cash'] : $this->m_arrstrFileNames[$strFileIndex . '_accrual'];
				$this->m_arrstrFileContents = ( false != isset( $this->m_arrstrFileContents[$strFileIndex . '_cash'] ) ) ? $this->m_arrstrFileContents[$strFileIndex . '_cash'] : $this->m_arrstrFileContents[$strFileIndex . '_accrual'];

				return $this->download( self::FILE_EXTENSION_CSV );
			}
		}
		return true;

	}

	public function exportInYardiEtlMrFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {
		$arrmixGlDetails = $this->fetchGlDetailsForYardiEtlMrFormatByBookTypeId( $intGlExportBookTypeId, $objClientDatabase );

		if( false == valArr( $arrmixGlDetails ) ) {
			return false;
		}
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];
		$arrintGlAccountId = [];
		$arrobjAccountDetails = [];
		if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
					$arrmixCashGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
				}
				if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
					$arrmixAccrualGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
				}
			}
			$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
			$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
		} else {
			if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
				$arrmixCashGlDetails = $arrmixGlDetails;
			} else {
				$arrmixAccrualGlDetails = $arrmixGlDetails;
			}
		}
		$intFileCount              = 0;
		if( false != valArr( $arrmixCashGlDetails ) ) {
			$arrintDepositeIds         = CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds;
			$arrmixGlDetailsByProperty = rekeyArray( 'property_id', $arrmixCashGlDetails, false, true );

			foreach( $arrmixGlDetailsByProperty as $intPropertyId => $arrmixTempGlDetails ) {

				$strPropertyName = $arrmixTempGlDetails[0]['property_name'];
				$this->m_arrstrFileContents[$intPropertyId . '_cash'][0] = [ 'FinJournals' ];
				$this->m_arrstrFileContents[$intPropertyId . '_cash'][1] = [
					'TRANNUM', 'DATE',
					'PROPERTY', 'ACCOUNT', 'POSTMONTH', 'BOOKNUM', 'AMOUNT', 'REMARK', 'REF', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2',
					'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4', 'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'TAXPOINTDATE',
					'DESC', 'DOCUMENTSEQUENCENUMBER', 'TRANAMOUNT', 'DETILEDTRANAMOUNT', 'LEGALENTITYID', 'BASECURRENCYID', 'TRANCURRENCYID', 'EXCHANGERATE',
					'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE', 'DETAILNOTES', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3',
					'SEGMENT4', 'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'TAXAMOUNT1', 'TAXAMOUNT2', 'DETAILVATTRANTYPEID',
					'DETAILVATRATEID', 'VATRATETYEID', 'VATRATEID', 'DISPLAYTYPE', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'NOTES2', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3',
					'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8'
				];

				foreach( $arrmixTempGlDetails as $arrmixGlDetail ) {

					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}
					$strAccountNumber       = $arrmixGlDetail['account_number'];
					$strAccountName         = $arrmixGlDetail['account_name']; // Descriptionl
					$strTransactionDateTime = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
					$strLookupCode          = $arrmixGlDetail['lookup_code'];
					$strPostMonth           = date( 'm/d/Y', strtotime( $arrmixGlDetail['post_month'] ) );

					$intBookNum = 1000;
					$fltAmount  = $arrmixGlDetail['amount'];

					if( false != in_array( $arrmixGlDetail['gl_transaction_type_id'], $arrintDepositeIds ) ) {
						$strReference = $arrmixGlDetail['reference'];
						$strRemark    = 'Entrata - Deposit#' . $arrmixGlDetail['deposit_number'];
					} else {
						$strReference = 'CHGADJ';
						$strRemark    = 'Entrata';
					}

					$intTransactionId = $arrmixGlDetail['gl_header_id'];
					$strDetailNotes = '';
					if( $arrmixGlDetail['gl_transaction_type_id'] == CGlTransactionType::GL_GJ ) {
						$strDetailNotes = $arrmixGlDetail['detail_memo'];
					} elseif( false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
						$strDetailNotes = 'Deposit #' . $arrmixGlDetail['deposit_number'];
					} else {

						if( false != valStr( $arrmixGlDetail['resident'] ) ) {
							$strDetailNotes .= $arrmixGlDetail['resident'];
						}
						if( false != valStr( $arrmixGlDetail['building_name'] ) || false != valStr( $arrmixGlDetail['unit_number'] ) ) {
							$strDetailNotes .= ' Unit#' . $arrmixGlDetail['building_name'];
							if( false != valStr( $arrmixGlDetail['building_name'] ) ) {
								$strDetailNotes .= '-';
							}
							$strDetailNotes .= $arrmixGlDetail['unit_number'];
						}
						if( false != valStr( $arrmixGlDetail['payment_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], [ CGlTransactionType::AP_PAYMENT, CGlTransactionType::AP_ALLOCATION, CGlTransactionType::AP_ALLOCATION_REVERSAL ] ) ) {
							$strDetailNotes .= '(Payment ID ' . $arrmixGlDetail['payment_id'] . ')';
						}

					}
					$strNote = str_replace( ',', '', trim( $strDetailNotes ) );

					$strPattern = '/(?<=[!@#$%^&*().?":{}|<>\/-])\s+|\s+(?=[!@#$%^&*().?":{}|<>\/-])/';
					$strNote = preg_replace( $strPattern, '', $strNote );
					$strNote = preg_replace( '/\s\s+/', ' ', $strNote );

					$strDetailNotes = \Psi\CStringService::singleton()->substr( $strNote, 0, 255 );
					$this->m_arrstrFileContents[$intPropertyId . '_cash'][] = [
						$intTransactionId, $strTransactionDateTime, $strLookupCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount, $strRemark,
						$strReference, '', '', '', '', '', '', '', '', '', $strAccountName, '', '', '', '', '', '', '', '', '', '', '', '', '', $strDetailNotes, '', '', '', '', '', '', '',
						'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}
				$this->m_arrstrFileNames[$intPropertyId . '_cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_' . $strPropertyName . '_yardi_etl_mr_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames[$intPropertyId . '_cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$intPropertyId . '_cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames[$intPropertyId . '_cash'] = $this->m_arrstrFileNames[$intPropertyId . '_cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames[$intPropertyId . '_cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			$intFileCount++;
		}
		if( false != valArr( $arrmixAccrualGlDetails ) ) {
			$arrintDepositeIds         = CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds;
			$arrmixGlDetailsByProperty = rekeyArray( 'property_id', $arrmixAccrualGlDetails, false, true );

			foreach( $arrmixGlDetailsByProperty as $intPropertyId => $arrmixTempGlDetails ) {

				$strPropertyName = $arrmixTempGlDetails[0]['property_name'];
				$this->m_arrstrFileContents[$intPropertyId . '_accrual'][0] = [ 'FinJournals' ];
				$this->m_arrstrFileContents[$intPropertyId . '_accrual'][1] = [
					'TRANNUM', 'DATE',
					'PROPERTY', 'ACCOUNT', 'POSTMONTH', 'BOOKNUM', 'AMOUNT', 'REMARK', 'REF', 'USERDEFINEDFIELD1', 'USERDEFINEDFIELD2',
					'USERDEFINEDFIELD3', 'USERDEFINEDFIELD4', 'USERDEFINEDFIELD5', 'USERDEFINEDFIELD6', 'USERDEFINEDFIELD7', 'USERDEFINEDFIELD8', 'TAXPOINTDATE',
					'DESC', 'DOCUMENTSEQUENCENUMBER', 'TRANAMOUNT', 'DETILEDTRANAMOUNT', 'LEGALENTITYID', 'BASECURRENCYID', 'TRANCURRENCYID', 'EXCHANGERATE',
					'EXCHANGERATEDATE', 'EXCHANGERATE2', 'EXCHANGERATEDATE2', 'AMOUNT2', 'FROMDATE', 'TODATE', 'DETAILNOTES', 'SEGMENT1', 'SEGMENT2', 'SEGMENT3',
					'SEGMENT4', 'SEGMENT5', 'SEGMENT6', 'SEGMENT7', 'SEGMENT8', 'SEGMENT9', 'SEGMENT10', 'SEGMENT11', 'SEGMENT12', 'TAXAMOUNT1', 'TAXAMOUNT2', 'DETAILVATTRANTYPEID',
					'DETAILVATRATEID', 'VATRATETYEID', 'VATRATEID', 'DISPLAYTYPE', 'JOB', 'CATEGORY', 'CONTRACT', 'COSTCODE', 'NOTES2', 'DETAILFIELD1', 'DETAILFIELD2', 'DETAILFIELD3',
					'DETAILFIELD4', 'DETAILFIELD5', 'DETAILFIELD6', 'DETAILFIELD7', 'DETAILFIELD8'
				];

				foreach( $arrmixTempGlDetails as $arrmixGlDetail ) {
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['account_name'] = $objGlAccount->getName();
					}
					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}

					$strAccountNumber       = $arrmixGlDetail['account_number'];
					$strAccountName         = $arrmixGlDetail['account_name']; // Descriptionl
					$strTransactionDateTime = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
					$strLookupCode          = $arrmixGlDetail['lookup_code'];
					$strPostMonth           = date( 'm/d/Y', strtotime( $arrmixGlDetail['post_month'] ) );

					$intBookNum = 1000;
					$fltAmount  = $arrmixGlDetail['amount'];

					if( false != in_array( $arrmixGlDetail['gl_transaction_type_id'], $arrintDepositeIds ) ) {
						$strReference = $arrmixGlDetail['reference'];
						$strRemark    = 'Entrata - Deposit#' . $arrmixGlDetail['deposit_number'];
					} else {
						$strReference = 'CHGADJ';
						$strRemark    = 'Entrata';
					}

					$intTransactionId = $arrmixGlDetail['gl_header_id'];
					$strDetailNotes = '';
					if( $arrmixGlDetail['gl_transaction_type_id'] == CGlTransactionType::GL_GJ ) {
						$strDetailNotes = $arrmixGlDetail['detail_memo'];
					} elseif( false != in_array( $arrmixGlDetail['gl_transaction_type_id'], CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) ) {
						$strDetailNotes = 'Deposit #' . $arrmixGlDetail['deposit_number'];
					} else {

						if( false != valStr( $arrmixGlDetail['resident'] ) ) {
							$strDetailNotes .= $arrmixGlDetail['resident'];
						}
						if( false != valStr( $arrmixGlDetail['building_name'] ) || false != valStr( $arrmixGlDetail['unit_number'] ) ) {
							$strDetailNotes .= ' Unit#' . $arrmixGlDetail['building_name'];
							if( false != valStr( $arrmixGlDetail['building_name'] ) ) {
								$strDetailNotes .= '-';
							}
							$strDetailNotes .= $arrmixGlDetail['unit_number'];
						}
						if( false != valStr( $arrmixGlDetail['payment_id'] ) && false != in_array( $arrmixGlDetail['gl_transaction_type_id'], [ CGlTransactionType::AP_PAYMENT, CGlTransactionType::AP_ALLOCATION, CGlTransactionType::AP_ALLOCATION_REVERSAL ] ) ) {
							$strDetailNotes .= '(Payment ID ' . $arrmixGlDetail['payment_id'] . ')';
						}

					}
					$strNote = str_replace( ',', '', trim( $strDetailNotes ) );

					$strPattern = '/(?<=[!@#$%^&*().?":{}|<>\/-])\s+|\s+(?=[!@#$%^&*().?":{}|<>\/-])/';
					$strNote = preg_replace( $strPattern, '', $strNote );
					$strNote = preg_replace( '/\s\s+/', ' ', $strNote );

					$strDetailNotes = \Psi\CStringService::singleton()->substr( $strNote, 0, 255 );
					$this->m_arrstrFileContents[$intPropertyId . '_accrual'][] = [
						$intTransactionId, $strTransactionDateTime, $strLookupCode, $strAccountNumber, $strPostMonth, $intBookNum, $fltAmount, $strRemark,
						$strReference, '', '', '', '', '', '', '', '', '', $strAccountName, '', '', '', '', '', '', '', '', '', '', '', '', '', $strDetailNotes, '', '', '', '', '', '', '',
						'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
					];
				}
				$this->m_arrstrFileNames[$intPropertyId . '_accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_' . $strPropertyName . '_yardi_etl_mr_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames[$intPropertyId . '_accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames[$intPropertyId . '_accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames[$intPropertyId . '_accrual'] = $this->m_arrstrFileNames[$intPropertyId . '_accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames[$intPropertyId . '_accrual'] .= '.' . self::FILE_EXTENSION_CSV;
				$intFileCount++;
			}
		}

		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName = ( false != isset( $this->m_arrstrFileNames[$intPropertyId . '_cash'] ) ) ? $this->m_arrstrFileNames[$intPropertyId . '_cash'] : $this->m_arrstrFileNames[$intPropertyId . '_accrual'];
				$this->m_arrstrFileContents = ( false != isset( $this->m_arrstrFileContents[$intPropertyId . '_cash'] ) ) ? $this->m_arrstrFileContents[$intPropertyId . '_cash'] : $this->m_arrstrFileContents[$intPropertyId . '_accrual'];
				return $this->download( self::FILE_EXTENSION_CSV );
			}
		}

	}

	public function exportInYardiGlExportSevenSFormat( $intGlExportFormatTypeId, $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData, $intTransmissionVendorId ) {

		$arrintGlAccountId      = [];
		$arrobjAccountDetails   = [];
		$arrobjCashGlDetails    = [];
		$arrobjAccrualGlDetails = [];
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];

		$strExportTypeWiseFileName = '_Yardi_GL_Export_7S_';
		if( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL == $intTransmissionVendorId ) {
			$strExportTypeWiseFileName = '_Yardi_GL_Export_7S_International_';
		} elseif( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX == $intTransmissionVendorId ) {
			$strExportTypeWiseFileName = '_Yardi_GL_Export_7S_MX_';
		}
		if( $intGlExportFormatTypeId == CAccountingExportFormatType::SUMMARY ) {
			$arrobjGlExportBatchDetails = $this->fetchGlExportBatchDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intTransmissionVendorId );
			if( false == valArr( $arrobjGlExportBatchDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrobjGlExportBatchDetails as $objAccountingExportBatchDetail ) {
					if( NULL != $objAccountingExportBatchDetail->getCashGlAccountId() && 0 != $objAccountingExportBatchDetail->getCashGlAccountId() ) {
						$arrobjCashGlDetails[]                                                    = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getCashGlAccountId()] = $objAccountingExportBatchDetail->getCashGlAccountId();
					}
					if( NULL != $objAccountingExportBatchDetail->getAccrualGlAccountId() && 0 != $objAccountingExportBatchDetail->getAccrualGlAccountId() ) {
						$arrobjAccrualGlDetails[]                                                    = $objAccountingExportBatchDetail;
						$arrintGlAccountId[$objAccountingExportBatchDetail->getAccrualGlAccountId()] = $objAccountingExportBatchDetail->getAccrualGlAccountId();
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrobjCashGlDetails = $arrobjGlExportBatchDetails;
				} else {
					$arrobjAccrualGlDetails = $arrobjGlExportBatchDetails;
				}
			}
			if( false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileContents['Cash'][] = [ 'record_type', 'source', 'source_transaction_id', 'record_sequence', 'property_code', 'post_month', 'post_date', 'gl_account', 'gl_books', 'amount', 'detail_notes' ];
				if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
					$this->m_arrstrFileContents['Cash'][0][] = 'transaction_type';
				}
				$strGlBook = 'Cash';
				foreach( $arrobjCashGlDetails as $objGlExportBatchDetail ) {
					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()] ) ) {
						$objGlAccount = $arrobjAccountDetails[$objGlExportBatchDetail->getCashGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
					}
					$strAccountNumber                     = $objGlExportBatchDetail->getAccountNumber();
					$strLookupCode                        = $objGlExportBatchDetail->getLookupCode();
					$intAmount                            = $objGlExportBatchDetail->getAmount();
					$strPostMonth                         = $objGlExportBatchDetail->getPostMonth();
					$this->m_arrstrFileContents['Cash'][] = [ 'JOURNAL', 'Entrata', '', '', $strLookupCode, $strPostMonth, '', $strAccountNumber, $strGlBook, $intAmount, '' ];
				}
			}
			if( false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_arrstrFileContents['Accrual'][] = [ 'record_type', 'source', 'source_transaction_id', 'record_sequence', 'property_code', 'post_month', 'post_date', 'gl_account', 'gl_books', 'amount', 'detail_notes' ];
				if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
					$this->m_arrstrFileContents['Accrual'][0][] = 'transaction_type';
				}
				$strGlBook = 'Accrual';
				foreach( $arrobjAccrualGlDetails as $objGlExportBatchDetail ) {
					if( 0 == $objGlExportBatchDetail->getAmount() ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()] ) ) {
						$objGlAccount = $arrobjAccountDetails[$objGlExportBatchDetail->getAccrualGlAccountId()];
						$objGlExportBatchDetail->setAccountNumber( $objGlAccount->getAccountNumber() );
					}
					$strAccountNumber                        = $objGlExportBatchDetail->getAccountNumber();
					$strLookupCode                           = $objGlExportBatchDetail->getLookupCode();
					$intAmount                               = $objGlExportBatchDetail->getAmount();
					$strPostMonth                            = $objGlExportBatchDetail->getPostMonth();
					$this->m_arrstrFileContents['Accrual'][] = [ 'JOURNAL', 'Entrata', '', '', $strLookupCode, $strPostMonth, '', $strAccountNumber, $strGlBook, $intAmount, '' ];
				}
			}
		} else {
			$arrmixGlDetails = $this->fetchGlDetailsByGlExportBookTypeId( $intGlExportBookTypeId, $objClientDatabase, $intTransmissionVendorId );
			if( false == valArr( $arrmixGlDetails ) ) {
				return false;
			}
			if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
					if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
						$arrmixCashGlDetails[]                                    = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
					}
					if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
						$arrmixAccrualGlDetails[]                                    = $arrmixGlDetail;
						$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
					}
				}
				$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
				$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
			} else {
				if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$arrmixCashGlDetails = $arrmixGlDetails;
				} else {
					$arrmixAccrualGlDetails = $arrmixGlDetails;
				}
			}
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$this->m_arrstrFileContents['Cash'][] = [ 'record_type', 'source', 'source_transaction_id', 'record_sequence', 'property_code', 'post_month', 'post_date', 'gl_account', 'gl_books', 'amount', 'detail_notes' ];
				if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
					$this->m_arrstrFileContents['Cash'][0][] = 'transaction_type';
				}
				$strGlBook = 'Cash';
				foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}
					$strAmount     = $arrmixGlDetail['amount'];
					$strPostDate   = $arrmixGlDetail['transaction_post_date'];
					$strPostMonth  = $arrmixGlDetail['post_month'];
					$strLookupCode = $arrmixGlDetail['lookup_code'];
					$intSourceTransactionId       = $arrmixGlDetail['id'];
					if( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX == $intTransmissionVendorId ) {
						if( false != valStr( $arrmixGlDetail['external_property_lookup_code'] ) ) {
						$strLookupCode = $arrmixGlDetail['external_property_lookup_code'];
						}
						if( false != valId( $arrmixGlDetail['id_sequence'] ) ) {
							$intSourceTransactionId .= '000' . $arrmixGlDetail['id_sequence'];
						}
					}
					$strRecordSequence      = $arrmixGlDetail['record_sequence'];
					$strAccountNumber       = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
					if( false != valStr( $arrmixGlDetail['description'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['description'];
					} elseif( false != valStr( $arrmixGlDetail['memo'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['memo'];
					} else {
						$strDetailsNotes = $arrmixGlDetail['reference'];
					}
					if( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL == $intTransmissionVendorId && false != valStr( $arrmixGlDetail['name_full'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['name_full'];
					}
					$strTransactionTypeDescription = $arrmixGlDetail['transaction_type_description'];
					if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
						$this->m_arrstrFileContents['Cash'][] = [ 'JOURNAL', 'Entrata', $intSourceTransactionId, $strRecordSequence, $strLookupCode, $strPostMonth, $strPostDate, $strAccountNumber, $strGlBook, $strAmount, $strDetailsNotes, $strTransactionTypeDescription ];
					} else {
						$this->m_arrstrFileContents['Cash'][] = [ 'JOURNAL', 'Entrata', $intSourceTransactionId, $strRecordSequence, $strLookupCode, $strPostMonth, $strPostDate, $strAccountNumber, $strGlBook, $strAmount, $strDetailsNotes ];
					}
				}
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$this->m_arrstrFileContents['Accrual'][] = [ 'record_type', 'source', 'source_transaction_id', 'record_sequence', 'property_code', 'post_month', 'post_date', 'gl_account', 'gl_books', 'amount', 'detail_notes' ];
				if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
					$this->m_arrstrFileContents['Accrual'][0][] = 'transaction_type';
				}
				$strGlBook = 'Accrual';

				foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {
					if( 0 == $arrmixGlDetail['amount'] ) {
						continue;
					}
					if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
						$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
						$arrmixGlDetail['account_number']   = $objGlAccount->getAccountNumber();
						$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
						$arrmixGlDetail['account_name']     = $objGlAccount->getName();
					}
					$strAmount     = $arrmixGlDetail['amount'];
					$strPostDate   = $arrmixGlDetail['transaction_post_date'];
					$strPostMonth  = $arrmixGlDetail['post_month'];
					$strLookupCode = $arrmixGlDetail['lookup_code'];
					$intSourceTransactionId       = $arrmixGlDetail['id'];
					if( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX == $intTransmissionVendorId ) {
						if( false != valStr( $arrmixGlDetail['external_property_lookup_code'] ) ) {
						$strLookupCode = $arrmixGlDetail['external_property_lookup_code'];
						}
						if( false != valId( $arrmixGlDetail['id_sequence'] ) ) {
							$intSourceTransactionId .= '000' . $arrmixGlDetail['id_sequence'];
						}
					}
					$strRecordSequence      = $arrmixGlDetail['record_sequence'];
					$strAccountNumber       = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
					if( false != valStr( $arrmixGlDetail['description'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['description'];
					} elseif( false != valStr( $arrmixGlDetail['memo'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['memo'];
					} else {
						$strDetailsNotes = $arrmixGlDetail['reference'];
					}
					if( CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL == $intTransmissionVendorId && false != valStr( $arrmixGlDetail['name_full'] ) ) {
						$strDetailsNotes = $arrmixGlDetail['name_full'];
					}
					$strTransactionTypeDescription = $arrmixGlDetail['transaction_type_description'];
					if( false != in_array( $intTransmissionVendorId, [ CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, CTransmissionVendor::GL_EXPORT_YARDI_GL_EXPORT_7S_MX ] ) ) {
						$this->m_arrstrFileContents['Accrual'][] = [ 'JOURNAL', 'Entrata', $intSourceTransactionId, $strRecordSequence, $strLookupCode, $strPostMonth, $strPostDate, $strAccountNumber, $strGlBook, $strAmount, $strDetailsNotes, $strTransactionTypeDescription ];
					} else {
						$this->m_arrstrFileContents['Accrual'][] = [ 'JOURNAL', 'Entrata', $intSourceTransactionId, $strRecordSequence, $strLookupCode, $strPostMonth, $strPostDate, $strAccountNumber, $strGlBook, $strAmount, $strDetailsNotes ];
					}
				}
			}
		}
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) || false != valArr( $arrobjCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strExportTypeWiseFileName . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) || false != valArr( $arrobjAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strExportTypeWiseFileName . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_CSV;
			}
		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . $strExportTypeWiseFileName . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_CSV;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			$arrmixExportData['worksheet_name'] = 'Sheet1';
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}

			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId && 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_CSV );
			}
		}

		return;

	}

	public function fetchGlDetailsForYardiEtlMrFormatByBookTypeId( $intGlExportBookTypeId, $objClientDatabase ) {
		$arrintGlTransactionTypesForVendorPayments = array_diff( CGlTransactionType::$c_arrintApGlTransactionTypeIds, [ CGlTransactionType::AP_CHARGE ] );
		$arrintApCheckPaymentTypes                 = [ CApPaymentType::CHECK, CApPaymentType::ACH, CApPaymentType::WRITTEN_CHECK, CApPaymentType::BILL_PAY_CHECK ];
		$arrintTempApTransactionTypes              = [ CGlTransactionType::AP_PAYMENT, CGlTransactionType::AP_ALLOCATION, CGlTransactionType::AP_ALLOCATION_REVERSAL ];
		$strSql                                    = 'SELECT gd.id,
			gd.property_id,
			gd.gl_header_id,
			gd.amount,
			gat.account_number,
			gat.name AS account_name,
			gh.transaction_datetime,
			gh.post_month,
			p.lookup_code,
			p.property_name,
			CASE
				WHEN gh.header_number IS NOT NULL THEN \'GJ#\' || COALESCE(gh.header_number, 0) || \' - \' || COALESCE(gh.reference, \'\')
				WHEN gtty.id IN (' . CGlTransactionType::AP_ALLOCATION_REVERSAL . ') THEN gh.reference
				WHEN ap_ref.ap_payment_type_id IN (' . implode( ', ', $arrintApCheckPaymentTypes ) . ') OR ap.ap_payment_type_id IN ( ' . implode( ', ', $arrintApCheckPaymentTypes ) . ' ) THEN COALESCE(\'Check #\' || ap_ref.payment_number, gh.reference, \'Check #\' || ap.payment_number)
				WHEN gtty.id IN ( ' . implode( ', ', CGlTransactionType::$c_arrintDepositeGlTransactionTypeIds ) . ' ) THEN CONCAT( \'deposit [\', ard.deposit_number,\']\' )
				ELSE COALESCE(\'Payment #\' || ap_ref.payment_number, gh.reference, \'Payment #\' || ap.payment_number)
			END AS reference,
			gat.formatted_account_number || \': \' || gat.name AS gl_account,
			( CASE
				WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' ) THEN ard.deposit_memo
				WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_ALLOCATION . ' ) THEN art1.memo
				WHEN art.id IS NOT NULL THEN ac.name
				WHEN gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' THEN gd.memo
				WHEN ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::BANK_ACCOUNT . ' ) AND gh.gl_transaction_type_id IN ( ' . implode( ', ', $arrintGlTransactionTypesForVendorPayments ) . ' ) THEN NULL
				ELSE COALESCE( ad1.description, ad.description, ah.header_memo, gh.memo )
			END ) || \' \' ::TEXT AS detail_memo,
			CASE
				WHEN gh.gl_transaction_type_id IN ( ' . implode( ',', $arrintTempApTransactionTypes ) . ' )
				THEN ( CASE
						WHEN lc.id IS NOT NULL THEN func_format_customer_name( cust.name_first, cust.name_last, cust.company_name )
						ELSE app.company_name
				      END )
				WHEN gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AP_CHARGE . ' )
				THEN app.company_name
				ELSE func_format_customer_name( cl.name_first, cl.name_last, cl.company_name )
			END AS resident,
			gh.gl_transaction_type_id,
	        ard.deposit_number,
	        cl.building_name,
	        cl.unit_number_cache as unit_number,
	        ap_ref.id as payment_id,
	        gh.memo,
			CASE
				WHEN ( to_char( gh.post_date, \'mm/yyyy\' ) = to_char( gh.post_month, \'mm/yyyy\' ) )
				THEN gh.post_date
				ELSE gh.post_month
			END AS post_date,
			gd.accrual_gl_account_id,
			gd.cash_gl_account_id
		FROM
            gl_headers gh
            JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gd.cid = gh.cid AND gd.reclass_gl_detail_id IS NULL )
            JOIN accounting_export_associations aea ON ( aea.gl_detail_id = gd.id AND aea.cid = gd.cid )
            JOIN properties p ON ( gd.property_id = p.id and gd.cid = p.cid )
            
            JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = ';
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$strSql .= ' gd.cash_gl_account_id OR gat.gl_account_id = gd.accrual_gl_account_id )';
			} else {
				$strSql .= ( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) ? 'gd.accrual_gl_account_id )' : 'gd.cash_gl_account_id )';
			}
		$strSql .= ' LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id )
            LEFT JOIN ap_allocations aa ON (gh.cid = aa.cid AND gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN (' . CGlAccountType::INCOME . ', ' . CGlAccountType::EXPENSES . '))
            LEFT JOIN ap_headers ah1 ON (ah1.cid = aa.cid AND ah1.id = aa.lump_ap_header_id)
            LEFT JOIN ap_details ad ON (aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id)
            JOIN gl_transaction_types gtty ON (gh.gl_transaction_type_id = gtty.id)
            LEFT JOIN ap_headers ah ON (ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month)
            LEFT JOIN ap_payments ap_ref ON (ap_ref.cid = ah.cid AND ap_ref.id = ah.ap_payment_id)
            LEFT JOIN ap_payments ap ON (ap.cid = ah1.cid AND ap.id = ah1.ap_payment_id)
            LEFT JOIN ar_allocations ara ON ( ara.cid = gh.cid AND ara.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ', ' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ' ) )
            LEFT JOIN ar_transactions art1 ON ( gh.cid = art1.cid AND art1.id = COALESCE ( ara.charge_ar_transaction_id, art.id ) )
            LEFT JOIN ar_transactions art2 ON ( gh.cid = art2.cid AND ( gh.reference_id = art2.id OR art2.id = ara.credit_ar_transaction_id ) AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_CHARGE . ', ' . CGlTransactionType::AR_PAYMENT . ', ' . CGlTransactionType::AR_DEPOSIT_HELD . ', ' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) )
            LEFT JOIN ar_deposits ard ON ( gh.cid = ard.cid AND gh.reference_id = ard.id AND gh.gl_transaction_type_id = ard.gl_transaction_type_id )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			$strSql .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ( ga.id = gd.accrual_gl_account_id or ga.id = gd.cash_gl_account_id ) )';
		} else {
			if( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) {
				$strSql .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.accrual_gl_account_id )';
			} else {
				$strSql .= ' JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.cash_gl_account_id )';
			}
		}
		$strSql .= ' LEFT JOIN ap_details ad1 ON ( ad1.cid = COALESCE( gd.cid, aa.cid ) AND ad1.id = COALESCE( gd.ap_detail_id, aa.charge_ap_detail_id ) )
            LEFT JOIN ar_codes ac ON ( ac.cid = art2.cid AND ac.id = art2.ar_code_id )
            LEFT JOIN ap_headers ah2 ON ( ad1.cid = ah2.cid AND ad1.ap_header_id = ah2.id AND ad1.gl_transaction_type_id = ah2.gl_transaction_type_id AND ad1.post_month = ah2.post_month )
            LEFT JOIN lease_customers lc ON ( lc.cid = COALESCE ( ah2.cid,ah.cid ) AND lc.id = COALESCE ( ah2.lease_customer_id,ah.lease_customer_id ) )
            LEFT JOIN customers cust ON ( cust.cid = lc.cid AND cust.id = lc.customer_id )
            LEFT JOIN ap_payees app ON ( app.cid = COALESCE( ah.cid, ah2.cid ) AND app.id = COALESCE( ah.ap_payee_id, ah2.ap_payee_id ) )
            LEFT JOIN cached_leases cl ON ( cl.cid = gd.cid AND cl.id = COALESCE( gd.lease_id, lc.lease_id ) )
            ';
		$strSql .= ' WHERE
                        gh.cid = ' . ( int ) $this->getCid() . '
                        AND gh.is_template = false
                        AND gh.reclass_gl_header_id IS NULL
                        AND aea.accounting_export_batch_id = ' . ( int ) $this->getId() . '
                        AND gat.gl_tree_id = ' . ( int ) $this->getGlTreeId() . '
                        AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
                        AND gh.is_template is FALSE ';

		$strSql .= ' ORDER BY
						gd.gl_header_id,
						abs( gd.amount ),
                        gd.id';

		return $arrmixGlDetails = fetchData( $strSql, $objClientDatabase );

	}

	public function exportInYardiCaFormat( $intGlExportBookTypeId, $objClientDatabase, $arrmixConnectionData, $arrmixExportData ) {

		$arrmixGlDetails = $this->fetchGlDetailsForYardiCaFormatByBookTypeId( $intGlExportBookTypeId, $objClientDatabase );

		if( false == valArr( $arrmixGlDetails ) ) {
			return false;
		}
		$arrmixCashGlDetails    = [];
		$arrmixAccrualGlDetails = [];
		$arrintGlAccountId = [];
		$arrobjAccountDetails = [];
		if( SELF::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			foreach( $arrmixGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != isset( $arrmixGlDetail['cash_gl_account_id'] ) ) {
					$arrmixCashGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['cash_gl_account_id']] = $arrmixGlDetail['cash_gl_account_id'];
				}
				if( false != isset( $arrmixGlDetail['accrual_gl_account_id'] ) ) {
					$arrmixAccrualGlDetails[] = $arrmixGlDetail;
					$arrintGlAccountId[$arrmixGlDetail['accrual_gl_account_id']] = $arrmixGlDetail['accrual_gl_account_id'];
				}
			}
			$arrobjAccountDetails = \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchGlAccountTreesByGlTreeIdByGlAccountIdsByCid( $this->getGlTreeId(), $arrintGlAccountId, $this->m_intCid, $objClientDatabase );
			$arrobjAccountDetails = rekeyObjects( 'GlAccountId', $arrobjAccountDetails );
		} else {
			if( SELF::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
				$arrmixCashGlDetails = $arrmixGlDetails;
			} else {
				$arrmixAccrualGlDetails = $arrmixGlDetails;
			}
		}
		if( false != valArr( $arrmixCashGlDetails ) ) {
			foreach( $arrmixCashGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']] ) ) {
					$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['cash_gl_account_id']];
					$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
					$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
				}
				$strDescription       = NULL;
				$fltAmount            = $arrmixGlDetail['amount'];
				$strPostMonth         = date( 'm/Y', strtotime( $arrmixGlDetail['post_month'] ) );
				$strPropertyGlBu      = $arrmixGlDetail['property_gl_bu'];
				$strAccountNumber     = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
				$strTransactionDate   = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
				$strYardiCaReference  = $arrmixGlDetail['yardi_ca_reference'];
				$intTransactionNumber = $arrmixGlDetail['transaction_id'];

				if( CGlTransactionType::AP_CHARGE == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = $arrmixGlDetail['memo_yardi'] . ' - ' . $arrmixGlDetail['vendor_name'];
				} elseif( CGlTransactionType::GL_GJ == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = 'JE #' . $arrmixGlDetail['header_number'] . ' - ' . $arrmixGlDetail['memo_yardi'];
				} elseif( CGlTransactionType::AP_ALLOCATION == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = 'Check #' . $arrmixGlDetail['payment_number'] . ' - ' . $arrmixGlDetail['payee_name'];
				} elseif( false != valStr( $arrmixGlDetail['charge_code_name'] ) ) {
					$strDescription = $arrmixGlDetail['charge_code_name'] . ' - ' . $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
				} else {
					$strDescription = $arrmixGlDetail['memo_yardi'];
				}
				$this->m_arrstrFileContents['Cash'][] = [
					'J',
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $intTransactionNumber, -8 ), 8, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 8 ),
					\Psi\CStringService::singleton()->str_pad( ' ', 40 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strTransactionDate, 0, 10 ), 10, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strPostMonth, 0, 7 ), 7, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strYardiCaReference, 0, 24 ), 24, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 120 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strPropertyGlBu, 0, 8 ), 8, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $fltAmount, 0, 15 ), 15, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 9 ), 9, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 9 ),
					\Psi\CStringService::singleton()->str_pad( ' ', 9 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( '1000', 0, 14 ), 14, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strDescription, 0, 36 ), 36, ' ', STR_PAD_RIGHT )
				];
			}
		}
		if( false != valArr( $arrmixAccrualGlDetails ) ) {
			foreach( $arrmixAccrualGlDetails as $intIndex => $arrmixGlDetail ) {
				if( false != valArr( $arrobjAccountDetails ) && false != isset( $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']] ) ) {
					$objGlAccount                       = $arrobjAccountDetails[$arrmixGlDetail['accrual_gl_account_id']];
					$arrmixGlDetail['account_number'] = $objGlAccount->getAccountNumber();
					$arrmixGlDetail['secondary_number'] = $objGlAccount->getSecondaryNumber();
				}
				$strDescription       = NULL;
				$fltAmount            = $arrmixGlDetail['amount'];
				$strPostMonth         = date( 'm/Y', strtotime( $arrmixGlDetail['post_month'] ) );
				$strPropertyGlBu      = $arrmixGlDetail['property_gl_bu'];
				$strAccountNumber     = ( true == valStr( $arrmixGlDetail['secondary_number'] ) ) ? $arrmixGlDetail['secondary_number'] : $arrmixGlDetail['account_number'];
				$strTransactionDate   = date( 'm/d/Y', strtotime( $arrmixGlDetail['transaction_datetime'] ) );
				$strYardiCaReference  = $arrmixGlDetail['yardi_ca_reference'];
				$intTransactionNumber = $arrmixGlDetail['transaction_id'];

				if( CGlTransactionType::AP_CHARGE == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = $arrmixGlDetail['memo_yardi'] . ' - ' . $arrmixGlDetail['vendor_name'];
				} elseif( CGlTransactionType::GL_GJ == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = 'JE #' . $arrmixGlDetail['header_number'] . ' - ' . $arrmixGlDetail['memo_yardi'];
				} elseif( CGlTransactionType::AP_ALLOCATION == $arrmixGlDetail['gl_transaction_type_id'] ) {
					$strDescription = 'Check #' . $arrmixGlDetail['payment_number'] . ' - ' . $arrmixGlDetail['payee_name'];
				} elseif( false != valStr( $arrmixGlDetail['charge_code_name'] ) ) {
					$strDescription = $arrmixGlDetail['charge_code_name'] . ' - ' . $arrmixGlDetail['name_first'] . ' ' . $arrmixGlDetail['name_last'];
				} else {
					$strDescription = $arrmixGlDetail['memo_yardi'];
				}
				$this->m_arrstrFileContents['Accrual'][] = [
					'J',
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $intTransactionNumber, -8 ), 8, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 8 ),
					\Psi\CStringService::singleton()->str_pad( ' ', 40 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strTransactionDate, 0, 10 ), 10, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strPostMonth, 0, 7 ), 7, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strYardiCaReference, 0, 24 ), 24, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 120 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strPropertyGlBu, 0, 8 ), 8, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $fltAmount, 0, 15 ), 15, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strAccountNumber, 0, 9 ), 9, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( ' ', 9 ),
					\Psi\CStringService::singleton()->str_pad( ' ', 9 ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( '1000', 0, 14 ), 14, ' ', STR_PAD_LEFT ),
					\Psi\CStringService::singleton()->str_pad( \Psi\CStringService::singleton()->substr( $strDescription, 0, 36 ), 36, ' ', STR_PAD_RIGHT )
				];
			}
		}

		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			if( false != valArr( $arrmixCashGlDetails ) ) {
				$this->m_arrstrFileNames['Cash'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_ca_export_batch_no_' . $this->getId() . '_cash';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Cash'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Cash'] = $this->m_arrstrFileNames['Cash'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Cash'] .= '.' . self::FILE_EXTENSION_CSV;
			}
			if( false != valArr( $arrmixAccrualGlDetails ) ) {
				$this->m_arrstrFileNames['Accrual'] = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_ca_export_batch_no_' . $this->getId() . '_accrual';
				if( false != valStr( $this->getFileNamePrefix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->getFileNamePrefix() . '_' . $this->m_arrstrFileNames['Accrual'];
				}
				if( false != valStr( $this->getFileNameSuffix() ) ) {
					$this->m_arrstrFileNames['Accrual'] = $this->m_arrstrFileNames['Accrual'] . '_' . $this->getFileNameSuffix();
				}
				$this->m_arrstrFileNames['Accrual'] .= '.' . self::FILE_EXTENSION_CSV;
			}

		} else {
			$this->m_strFileName = date( 'Y_m_d_His', strtotime( $this->getBatchDatetime() ) ) . '_yardi_ca_export_batch_no_' . $this->getId();
			if( false != valStr( $this->getFileNamePrefix() ) ) {
				$this->m_strFileName = $this->getFileNamePrefix() . '_' . $this->m_strFileName;
			}
			if( false != valStr( $this->getFileNameSuffix() ) ) {
				$this->m_strFileName = $this->m_strFileName . '_' . $this->getFileNameSuffix();
			}
			$this->m_strFileName .= '.' . self::FILE_EXTENSION_CSV;
		}
		if( false != $arrmixExportData['show_ftp'] ) {
			$arrmixExportData['file_extension'] = self::FILE_EXTENSION_CSV;
			if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
				$arrmixExportData['file_name'] = $this->m_arrstrFileNames;
			} else {
				$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_ACCRUAL;
				if( self::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
					$strBookTypeMethod = self::GL_EXPORT_STRING_BOOK_TYPE_CASH;
				}
				$arrmixExportData['file_name'][$strBookTypeMethod] = $this->m_strFileName;
			}
			$arrmixExportData['file_content']       = $this->m_arrstrFileContents;
			$arrmixExportData['has_multiple_files'] = true;
			if( true == $arrmixExportData['is_zip_files'] ) {
				$arrmixExportData['zip_upload'] = true;
			}
			$boolIsUploadSuccess = $this->m_objExportsFileLibrary->uploadFileToServer( $arrmixExportData, $arrmixConnectionData, $objClientDatabase->getCid() );
			$this->setMessage( ( false != $boolIsUploadSuccess ) ? $this->m_objExportsFileLibrary->getSuccessMsg() : $this->m_objExportsFileLibrary->getLogMessage() );
			return $boolIsUploadSuccess;
		} else {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrFileContents ) ) {
				$this->downloadAsZip( self::FILE_EXTENSION_CSV );
			} else {
				$this->m_strFileName        = ( false != valStr( $this->m_strFileName ) ) ? $this->m_strFileName : current( $this->m_arrstrFileNames );
				$this->m_arrstrFileContents = ( false != valArr( $this->m_arrstrFileContents['Cash'] ) ) ? $this->m_arrstrFileContents['Cash'] : $this->m_arrstrFileContents['Accrual'];
				$this->download( self::FILE_EXTENSION_CSV );
			}
		}
		return true;
	}

	public function fetchGlDetailsForYardiCaFormatByBookTypeId( $intGlExportBookTypeId, $objClientDatabase ) {
		$strGlAccountCondition = '';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			$strGlAccountCondition = ' gd.cash_gl_account_id OR gat.gl_account_id = gd.accrual_gl_account_id ';
		} else {
			$strGlAccountCondition = ( self::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) ? 'gd.accrual_gl_account_id ' : 'gd.cash_gl_account_id ';
		}
		$strSql = 'SELECT
						CASE 
							WHEN gd.reference_id IS NULL 
								THEN gh.id 
							ELSE gd.reference_id 
						END AS transaction_id,
						gd.amount,
						gh.transaction_datetime,
						gh.header_number,
						gh.post_month,
						gh.reference,
						CASE
							WHEN ( to_char ( gh.post_date, \'mm/yyyy\' ) = to_char ( gh.post_month, \'mm/yyyy\' ) ) THEN gh.post_date
							ELSE gh.post_month
						END AS post_date,
						COALESCE( gd.memo, gh.memo, gh.reference ) as memo_yardi,
						gh.gl_transaction_type_id,
						l.name_first,
						l.name_last,
						pp.value as property_gl_bu,
						pp2.value as yardi_ca_reference,
						app.company_name AS vendor_name,
						ar.name as charge_code_name,
						ap.payee_name,
						ap.payment_number,
						gd.cash_gl_account_id,
						gd.accrual_gl_account_id';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSql .= ',gat.account_number,
						gat.secondary_number';
		}

		$strSql .= ' FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gd.cid = gh.cid AND gd.reclass_gl_detail_id IS NULL )
						JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
						JOIN properties p ON ( gd.property_id = p.id AND gd.cid = p.cid )
						JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'YARDI_CA_PROPERTY_GL_BU\' ) 
						JOIN property_preferences pp2 ON ( p.cid = pp.cid AND p.id = pp2.property_id AND pp2.key = \'YARDI_CA_REFERENCE\' )
						LEFT JOIN cached_leases l ON ( gd.cid = l.cid AND l.id = gd.lease_id )
						LEFT JOIN ap_allocations aa ON ( aa.cid = gd.cid AND aa.id = gd.reference_id )
						LEFT JOIN ap_details ad ON ( ad.cid = gd.cid AND ( ad.id = gd.ap_detail_id OR aa.credit_ap_detail_id = ad.id ) )
						LEFT JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						LEFT JOIN ap_payees app ON ( app.cid = ah.cid AND app.id = ah.ap_payee_id )
						LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id )
						LEFT JOIN ar_codes ar ON ( ar.cid = gd.cid AND ar.id = art.ar_code_id )
						LEFT JOIN ap_payments ap ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSql .= 'LEFT JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND ( gat.gl_account_id = ' . $strGlAccountCondition . ') )';
		}
		$strSql .= 'WHERE
						gh.cid = ' . ( int ) $this->getCid() . '
						AND gh.is_template = false
						AND gh.reclass_gl_header_id IS NULL
						AND aea.accounting_export_batch_id = ' . ( int ) $this->getId() . '
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
						AND gh.is_template is FALSE
						AND gd.amount <> 0';
		if( self::GL_EXPORT_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSql .= 'AND gat.gl_tree_id = ' . ( int ) $this->getGlTreeId();
		}
		$strSql .= ' ORDER BY
						gd.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function validatePropertyPreferencesSettings( $intTranasmissionVendorId, $arrintPropertyIds, $intCid, $objClientDatabase, $intExportMethod = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return [ false ];
		}
		$arrstrKeyMessages = [];
		$arrstrErrorMessages = [];
		switch( $intTranasmissionVendorId ) {
			case CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM:
				$arrstrKeyMessages = [ 'TIMBERLINE_PM_JOURNAL_CODE' ];
				break;

			case CTransmissionVendor::GL_EXPORT_ONESITE:
				$arrstrKeyMessages = [ 'ONE_SITE_JOURNAL_CODE', 'ONE_SITE_PROPERTY_GL_BU' ];
				break;

			case CTransmissionVendor::GL_EXPORT_MRI:
				$arrstrKeyMessages = [ 'MRI_ENTITY_ID' ];
				break;

			case CTransmissionVendor::GL_EXPORT_AMSI:
				$arrstrKeyMessages = [ 'AMSI_ENTITY_ID' ];
				break;

			case CTransmissionVendor::GL_EXPORT_JENARK:
				$arrstrKeyMessages = [ 'JENARK_ENTITY_ID' ];
				break;

			case CTransmissionVendor::GL_EXPORT_YARDI:
				$arrstrKeyMessages = [ 'YARDI_PROPERTY_GL_BU' ];
				break;

			default:
				// default
				break;
		}

		$arrstrPropertyNames   = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyNamesWithoutPropertySettingsByKeysByPropertyIdByCid( $arrintPropertyIds, $arrstrKeyMessages, $intCid, $objClientDatabase );

		if( CTransmissionVendor::GL_EXPORT_MRI == $intTranasmissionVendorId ) {
			// if 2 or more properties are selected, check if the include originating currency setting is consistent for all properties
			if( ( 1 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) ) {
				$arrstrPropertyNamesWithoutIncludeOriginatingCurrency = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyNamesWithoutPropertySettingsByKeysByPropertyIdByCid( $arrintPropertyIds, [ 'MRI_INCLUDE_ORIGINATING_CURRENCY' ], $intCid, $objClientDatabase );
				if( true == valArr( $arrstrPropertyNamesWithoutIncludeOriginatingCurrency ) && \Psi\Libraries\UtilFunctions\count( $arrstrPropertyNamesWithoutIncludeOriginatingCurrency ) != \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
					return [ false, $arrstrPropertyNames, self::SETTING_NAME_INCLUDE_ORIGINATING_CURRENCY ];
				}
			}
		}

		if( false != valId( $intExportMethod ) && CAccountingExportMethodType::API == $intExportMethod && false != in_array( $intTranasmissionVendorId, [ CTransmissionVendor::GL_EXPORT_MRI, CTransmissionVendor::GL_EXPORT_YARDI ] ) ) {
			$arrobjPropertyTransmissionVendors = \Psi\Eos\Entrata\CPropertyTransmissionVendors::createService()->fetchAllPropertyTransmissionVendorsByPropertyIdsByTransmissionVendorIdByCid( $arrintPropertyIds, CTransmissionVendor::GL_EXPORT_MRI, $intCid, $objClientDatabase );
			if( \Psi\Libraries\UtilFunctions\count( $arrobjPropertyTransmissionVendors ) < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
				$arrstrErrorMessages['entity id'] = $arrstrPropertyNames;
				return [ false, $arrstrPropertyNames, self::SETTING_NAME_TRANSMISSION_VENDOR ];
			}
		}

		// if all properties have changed their export type then only show the error message
		if( \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) == \Psi\Libraries\UtilFunctions\count( $arrstrPropertyNames ) && false != valArr( $arrstrPropertyNames ) ) {
			return [ false, $arrstrPropertyNames ];
		}

		return [ true ];

	}

	/**** Validation Fo AP Exports *****/

	public function validateExportConfigurations( $intTransmisionTypeId ) {
		$boolIsValid = true;

		switch( $intTransmisionTypeId ) {
			case CTransmissionType::AP_EXPORT:
				$boolIsValid &= $this->valPeriod();
				$boolIsValid &= $this->valConnection();
				$boolIsValid &= $this->valTransmissionVendorId();
				$this->addRequiredErrorMsg();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valPeriod() {
		$boolIsValid = true;
		if( 0 == $this->getIncludeAllUnexportedTransactions() ) {
			if( self::PERIOD_POST_MONTH == $this->getPeriodType() ) {

				$arrintExplodedPostMonthDate = explode( '/', $this->getPostMonth() );
				if( 2 == \Psi\Libraries\UtilFunctions\count( $arrintExplodedPostMonthDate ) ) {
					$strPostMonthDate = $arrintExplodedPostMonthDate[0] . '/1/' . $arrintExplodedPostMonthDate[1];
				}
				if( false == valStr( $this->getPostMonth() ) ) {
					$boolIsValid = false;
					$this->m_arrstrMissingRequiredFields[] = __( 'Post Month' );
				} elseif( false == preg_match( '/^[0-9]{2}\/[0-9]{4}$/', $this->getPostMonth() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
				} elseif( false == is_null( $strPostMonthDate ) && false == CValidation::validateDate( $strPostMonthDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is invalid.' ) ) );
				}
			} else {
				$strStartDate   = $this->getStartDate();
				$strEndDate     = $this->getEndDate();
				if( false == valStr( $strStartDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is required.' ) ) );
				}
				if( false == valStr( $strEndDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is required.' ) ) );
				}
				if( true == $boolIsValid && strtotime( $strStartDate ) > strtotime( $strEndDate ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'End Date should be greater than Start Date.' ) ) );
					$boolIsValid &= false;
				}
			}
		}
		return $boolIsValid;
	}

	public function valConnection() {
		$boolIsValid = true;
		if( true == $this->getClientFtp() ) {
			$strHostName    = str_replace( 'sftp://', '', $this->getHost() );
			$strUserName    = $this->getDetailsField( 'user_name' );
			$strPassword    = $this->getPassword();
			$strPort        = $this->getPort();
			$strFilePath    = $this->getDetailsField( 'file_path' );

			if( false == valStr( $strHostName ) ) {
				$boolIsValid = false;
				$this->m_arrstrMissingRequiredFields[] = __( 'Host name' );
			}
			if( false != valStr( $strHostName ) && false == filter_var( $strHostName, FILTER_VALIDATE_IP ) && false == filter_var( gethostbyname( $strHostName ), FILTER_VALIDATE_IP ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host_ip', __( 'Host name is not valid.' ) ) );
			}

			if( false == valStr( $strUserName ) ) {
				$boolIsValid = false;
				$this->m_arrstrMissingRequiredFields[] = __( 'Username' );
			}
			if( false == valStr( $strPassword ) ) {
				$boolIsValid = false;
				$this->m_arrstrMissingRequiredFields[] = __( 'Password' );
			}

			if( '' != $strPort && false == is_numeric( $strPort ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port is not valid. ' ) ) );
			}
			if( false != valStr( $strFilePath ) && false == preg_match( '/^[\w\/\\\]*$/', $strFilePath ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', __( 'File path cannot contain special characters except {%s, 0}', [ '_ \ /' ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function addRequiredErrorMsg() {
		if( false != valArr( $this->m_arrstrMissingRequiredFields ) ) {
			if( 1 == \Psi\Libraries\UtilFunctions\count( $this->m_arrstrMissingRequiredFields ) ) {
				$strMessage = __( '{%s,0 } is required.', [ implode( ', ', $this->m_arrstrMissingRequiredFields ) ] );
			} else {
				$strMessage = __( '{%s,0 } are required.', [ implode( ', ', $this->m_arrstrMissingRequiredFields ) ] );
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strMessage ) );
		}
		return NULL;
	}

	private function renderQuickBookClass( $intQuickBookClass, $strUnitNumber, $strBuildingName ) {
		$strQuickBookClass = '';
		if( self::QUICK_BOOK_CLASS_UNIT_NUMBER == $intQuickBookClass ) {
			$strQuickBookClass = $strUnitNumber;
		} elseif( self::QUICK_BOOK_CLASS_BLDG_UNIT_NUMBER == $intQuickBookClass ) {
			$strQuickBookClass = $strBuildingName;

			if( false != valStr( $strUnitNumber ) ) {
				if( false != valStr( $strQuickBookClass ) ) {
					$strQuickBookClass .= '-';
				}
				$strQuickBookClass .= $strUnitNumber;
			}
		}
		return $strQuickBookClass;

	}

	/***** End ************************/

}
?>
