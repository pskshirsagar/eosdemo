<?php

class CCachedLeaseLog extends CBaseCachedLeaseLog {

	const IS_IGNORED_FALSE = '0';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectivePeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReversalCachedLeaseLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldLeaseStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldTerminationListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTerminationListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldLeaseIntervalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActiveLeaseIntervalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuildingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumberCache() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisplayNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoticeDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMultiSlot() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReversal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostMonthIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEffectiveDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>