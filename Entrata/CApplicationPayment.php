<?php

class CApplicationPayment extends CBaseApplicationPayment {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
           	    $boolIsValid = true;
        	    break;
        }

        return $boolIsValid;
    }
}
?>