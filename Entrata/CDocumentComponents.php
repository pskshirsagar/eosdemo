<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentComponents
 * Do not add any new functions to this class.
 */

class CDocumentComponents extends CBaseDocumentComponents {

	public static function fetchCustomDocumentComponentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_components WHERE document_id = ' . ( int ) $intDocumentId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY order_num ASC';
		return self::fetchDocumentComponents( $strSql, $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentIdByComponentTypesByCid( $intDocumentId, $arrstrComponentTypes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrComponentTypes ) ) return NULL;

		$strSql = 'SELECT * FROM
					 	document_components
					WHERE
						document_id = ' . ( int ) $intDocumentId . '
						AND cid = ' . ( int ) $intCid . '
						AND component_type IN ( \'' . implode( "','", $arrstrComponentTypes ) . '\')
					ORDER BY
						created_on ASC';

		return self::fetchDocumentComponents( $strSql, $objDatabase );
	}

	public static function fetchDocumentComponentByDocumentIdByDocumentComponentIdByCid( $intDocumentId, $intDocumentComponentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
					 	document_components
					WHERE
						document_id = ' . ( int ) $intDocumentId . '
						AND cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intDocumentComponentId;

		return self::fetchDocumentComponent( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentComponentCountByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		$strWhereSql = ' WHERE cid = ' . ( int ) $intCid . ' AND document_id = ' . ( int ) $intDocumentId;
		return self::fetchDocumentComponentCount( $strWhereSql, $objDatabase );
	}

	public static function fetchDocumentComponentsByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT * FROM
					 	document_components
					WHERE
						cid = ' . ( int ) $intCid . ' AND
						document_id IN ( \'' . implode( "','", $arrintDocumentIds ) . '\')
					ORDER BY
						id ASC';

		return self::fetchDocumentComponents( $strSql, $objDatabase );
	}

}
?>