<?php

class CPropertyUnitUpdate extends CBasePropertyUnitUpdate {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitIdCurrent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitIdNew() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitChanged() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumberOld() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumberNew() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSpaceNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumberPatternId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>