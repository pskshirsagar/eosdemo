<?php

class CJobGroupLocation extends CBaseJobGroupLocation {

	protected $m_intPropertyId;
	protected $m_strName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActualStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Date field should not be empty.' ) );
		} elseif( false == CValidation::validateDate( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Date should be in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valActualCompletionDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getCompletedBy() ) ) {

			if( true == is_null( $this->getActualCompletionDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Date field should not be empty.' ) );
			} elseif( false == CValidation::validateDate( $this->getActualCompletionDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Date should be in mm/dd/yyyy format.' ) );
			}
			if( ( false == is_null( $this->getActualStartDate() ) && false == is_null( $this->getActualCompletionDate() ) && strtotime( $this->getActualStartDate() ) > strtotime( $this->getActualCompletionDate() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Actual Completion Date cannot be earlier than Actual Start Date. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDatesByJobsStartDateAndEndDate( $objJob ) {
		$boolIsValid = true;

		if( false == is_null( $this->getActualStartDate() ) && ( strtotime( $this->getActualStartDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date cannot be earlier than Job Start Date. ' ) );
		}

		if( false == is_null( $this->getActualStartDate() ) && ( strtotime( $this->getActualStartDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date cannot be greater than Job Planned Completion date. ' ) );
		}

		if( false == is_null( $this->getActualCompletionDate() ) && ( strtotime( $this->getActualCompletionDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Actual Completion Date cannot be greater than Job Completion Date. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objJob = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valActualStartDate();
				$boolIsValid &= $this->valActualCompletionDate();
				$boolIsValid &= $this->valDatesByJobsStartDateAndEndDate( $objJob );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */
	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getName() {
		return $this->m_strName;
	}

	/**
	 * Set Functions
	 */
	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	/**
	 * Other Functions
	 */
	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) )			$this->setName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['property_id'] ) )		$this->setPropertyId( $arrmixValues['property_id'] );

		return;
	}

}
?>