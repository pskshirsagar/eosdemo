<?php

class CCustomerDataNote extends CBaseCustomerDataNote {

	protected $m_strCompanyUserName;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['company_user_name'] ) ) $this->setCompanyUserName( $arrmixValues['company_user_name'] );
	}

	public function setCompanyUserName( $strCompanyUserName ) {
		$this->m_strCompanyUserName = $strCompanyUserName;
	}

	/**
	 * Get Funtions
	 */

	public function getCompanyUserName() {
		return $this->m_strCompanyUserName;
	}

	/**
	 * Validation Funtions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {

		$boolIsValid = true;

		if( false == valStr( $this->getNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNoteCreatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoteCreatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert':
				$boolIsValid = $this->valNote();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setNoteCreatedBy( $intCurrentUserId );
		$this->setNoteCreatedOn( 'NOW()' );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

	}
}
?>