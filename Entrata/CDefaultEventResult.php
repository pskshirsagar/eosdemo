<?php

class CDefaultEventResult extends CBaseDefaultEventResult {

	const CANCELLED							= 1;
	const COMPLETED							= 2;
	const RESCHEDULED						= 3;
	const MISSED							= 4;
	const LISTENED_TO						= 21;
	const LEAD_CREATED						= 22;
	const VOICEMAIL_MISSED					= 23;
	const RESPONSE_SENT						= 31;
	const NO_RESPONSE_SENT					= 32;
	const INTERESTED						= 41;
	const NOT_INTERESTED					= 42;
	const UNQUALIFIED						= 43;
	const TOUR_SCHEDULED					= 44;
	const LEAD_OUTREACH_NO_RESPONSE			= 45;
	const RENEWAL_OUTREACH_NO_RESPONSE		= 51;
	const PLANS_TO_RENEW					= 52;
	const NOT_HAPPY_WITH_OFFER				= 53;
	const UNDECIDED							= 54;
	const NO_PLANS_TO_RENEW					= 55;
	const RENEWED							= 56;
	const COLLECTIONS_RESPONSE_NO_RESPONSE	= 57;
	const UNABLE_TO_PAY						= 58;
	const WILL_PARTIAL_PAY					= 59;
	const WILL_PAY_IN_FULL					= 60;
	const PARTIALLY_PAID					= 61;
	const PAID_IN_FULL						= 62;
	const UNKNOWN							= 63;
	const FULFILLED							= 65;
	const CHAT_MISSED_OFFLINE               = 66;
	const CHAT_MISSED_NO_RESPONSE           = 67;
	const CHECK_IN                          = 83;
	const CHECK_OUT                         = 84;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResident() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsContactType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsScreeningType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCloseType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

}
?>