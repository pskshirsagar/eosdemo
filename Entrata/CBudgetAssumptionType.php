<?php

class CBudgetAssumptionType extends CBaseBudgetAssumptionType {

	const CUSTOM 					= 1;
	const AVERAGE_MARKET_RENT		= 2;
	const AVERAGE_BUDGETED_RENT		= 3;
	const LEASE_EXPIRATIONS			= 4;
	const RENEWALS					= 5;
	const OCCUPANCY					= 6;
	const SKIPS_AND_EVICTIONS		= 7;
	const MOVE_INS					= 8;
	const TOTAL_UNITS 				= 9;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>