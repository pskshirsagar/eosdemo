<?php

class CAssetType extends CBaseAssetType {

	const FIXED_ASSET_CAPITALIZED_AND_TRACKED = 1;
	const FIXED_ASSET_EXPENSED_AND_TRACKED    = 2;
	const INVENTORY_INVENTORIED_AND_TRACKED   = 3;
	const SUPPLIES_NOT_TRACKED                = 4;
	const SERVICES_NOT_TRACKED                = 5;
	const INVENTORY_EXPENSED_AND_TRACKED      = 6;

	public static $c_arrintInventoryAssetTypeIds = [ CAssetType::INVENTORY_INVENTORIED_AND_TRACKED, CAssetType::INVENTORY_EXPENSED_AND_TRACKED ];

	public static $c_arrintFixedAssetTypeIds = [ CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED ];

	public static $c_arrintAssetTypeIds = [
		CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED => 'Fixed Asset (Capitalized & Tracked)',
		CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED => 'Fixed Asset (Expensed & Tracked)'
	];

	public static $c_arrintAssetTypes = [
		'FIXED_ASSET_CAPITALIZED_AND_TRACKED' => self::FIXED_ASSET_CAPITALIZED_AND_TRACKED,
		'FIXED_ASSET_EXPENSED_AND_TRACKED' => self::FIXED_ASSET_EXPENSED_AND_TRACKED,
		'INVENTORY_INVENTORIED_AND_TRACKED' => self::INVENTORY_INVENTORIED_AND_TRACKED,
		'SUPPLIES_NOT_TRACKED' => self::SUPPLIES_NOT_TRACKED,
		'SERVICES_NOT_TRACKED' => self::SERVICES_NOT_TRACKED,
		'INVENTORY_EXPENSED_AND_TRACKED' => self::INVENTORY_EXPENSED_AND_TRACKED
	];

	public static $c_arrstrAssetTypesHavingEachUnitOfMeasure = [
		CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED => 'Fixed Asset (Capitalized & Tracked)',
		CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED => 'Fixed Asset (Expensed & Tracked)',
		CAssetType::SERVICES_NOT_TRACKED => 'Services (Not Tracked)'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'FIXED_ASSET_CAPITALIZED_AND_TRACKED', self::FIXED_ASSET_CAPITALIZED_AND_TRACKED );
		$objSmarty->assign( 'FIXED_ASSET_EXPENSED_AND_TRACKED',    self::FIXED_ASSET_EXPENSED_AND_TRACKED );
		$objSmarty->assign( 'INVENTORY_INVENTORIED_AND_TRACKED',   self::INVENTORY_INVENTORIED_AND_TRACKED );
		$objSmarty->assign( 'SUPPLIES_NOT_TRACKED',                self::SUPPLIES_NOT_TRACKED );
		$objSmarty->assign( 'SERVICES_NOT_TRACKED',                self::SERVICES_NOT_TRACKED );
		$objSmarty->assign( 'INVENTORY_EXPENSED_AND_TRACKED',      self::INVENTORY_EXPENSED_AND_TRACKED );
	}

	public static function assignTemplateConstants( $arrmixTemplateParameters = [] ) {

		$arrmixTemplateParameters['FIXED_ASSET_CAPITALIZED_AND_TRACKED'] = self::FIXED_ASSET_CAPITALIZED_AND_TRACKED;
		$arrmixTemplateParameters['FIXED_ASSET_EXPENSED_AND_TRACKED']    = self::FIXED_ASSET_EXPENSED_AND_TRACKED;
		$arrmixTemplateParameters['INVENTORY_INVENTORIED_AND_TRACKED']   = self::INVENTORY_INVENTORIED_AND_TRACKED;
		$arrmixTemplateParameters['SUPPLIES_NOT_TRACKED']                = self::SUPPLIES_NOT_TRACKED;
		$arrmixTemplateParameters['SERVICES_NOT_TRACKED']                = self::SERVICES_NOT_TRACKED;
		$arrmixTemplateParameters['INVENTORY_EXPENSED_AND_TRACKED']      = self::INVENTORY_EXPENSED_AND_TRACKED;

		return $arrmixTemplateParameters;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>