<?php

class CBudgetGlAccountActivityTarget extends CBaseBudgetGlAccountActivityTarget {

	const GL_ACCOUNT          = 1;
	const GL_ACCOUNT_MONTH    = 2;
	const ITEM_NAME           = 3;
	const ITEM_PRICE          = 4;
	const ITEM_MONTH_QUANTITY = 5;
	const ASSUMPTION_NAME     = 6;
	const ASSUMPTION_MONTH    = 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>