<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyHolidays
 * Do not add any new functions to this class.
 */

class CPropertyHolidays extends CBasePropertyHolidays {

	public static function fetchPropertyHolidaysByPropertyIdByCidWithCompanyHolidayDate( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ph.*,
						ch.date AS company_holiday_date
					FROM
						property_holidays AS ph
						LEFT JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ph.property_id =' . ( int ) $intPropertyId . '
						AND ph.cid =' . ( int ) $intCid;

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchPropertyHolidaysByCompanyHolidayDateByCid( $strCompanyHolidayDate, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ph.*,
						ch.date
					FROM
						property_holidays AS ph
						LEFT JOIN company_holidays AS ch ON ( ph.cid = ch.cid AND ph.company_holiday_id = ch.id )
					WHERE
						ph.cid =' . ( int ) $intCid . '
						AND ch.date = \'' . $strCompanyHolidayDate . '\'';

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchAssociatedPropertyHolidaysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ph.*,
						' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', ch.name, ch.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'ch.name' ) . ' AS name
					FROM
						property_holidays AS ph
						JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ph.property_id =' . ( int ) $intPropertyId . '
						AND ph.cid =' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyHolidaysByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ph.*,
						ch.name company_holiday_name,
						ch.date as company_holiday_date
					FROM
						property_holidays AS ph
						JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ph.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ph.cid =' . ( int ) $intCid . '
						AND date_part( \'YEAR\', ch.date ) = date_part( \'YEAR\', current_date )
						AND ph.is_published =1
						AND ch.is_published = 1';

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchCommonPropertyHolidaysByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (ph.company_holiday_id)
						ph.id,
						ph.company_holiday_id,
						ch.name company_holiday_name
					FROM
						property_holidays AS ph
						JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ph.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ph.cid =' . ( int ) $intCid . '
						AND ph.is_published =1
						AND ch.is_published = 1';

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchPropertyHolidaysBetweenDateTimeRangeByPropertyIdsByCid( $strStartDate, $strEndDate, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = "SELECT
						ph.*,
						ch.name AS company_holiday_name,
						ch.description AS company_holiday_description,
						ch.date AS company_holiday_date,
						CASE WHEN EXTRACT ( dow from ch.date ) > 0 THEN EXTRACT ( dow from ch.date ) ELSE 7 END AS day
					FROM
						property_holidays AS ph
						JOIN properties AS p ON ( p.id = ph.property_id AND p.cid = ph.cid )
						JOIN time_zones AS tz ON ( tz.id = p.time_zone_id )
						JOIN company_holidays AS ch ON (
							ch.id = ph.company_holiday_id
							AND ch.cid = ph.cid
							AND (
								( ( ch.date || ' ' ||
									CASE
										WHEN ph.open_time IS NOT NULL THEN ph.open_time
										ELSE '00:00:00'
									END
									|| ' ' || tz.abbr
								)::timestamptz,
								( ch.date || ' ' ||
									CASE
										WHEN ph.close_time IS NOT NULL THEN ph.close_time
										ELSE '23:59:59'
									END
									|| ' ' || tz.abbr
								)::timestamptz )
								OVERLAPS
								( ( '" . date( 'c', strtotime( $strStartDate ) ) . "' )::timestamptz,
								( '" . date( 'c', strtotime( $strEndDate ) ) . "' )::timestamptz )
							)
						)
					WHERE
						ph.property_id IN ( " . implode( ', ', $arrintPropertyIds ) . ' )
						AND ph.cid = ' . ( int ) $intCid;

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchPropertyHolidayByIdByPropertyIdByCidWithCompanyHolidayDate( $intId, $intPropertyId, $intDaysInterval, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ph.*,
						ch.date AS company_holiday_date
					FROM
						property_holidays AS ph
						LEFT JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ph.id = ' . ( int ) $intId . '
						AND ph.property_id = ' . ( int ) $intPropertyId . '
						AND ph.cid = ' . ( int ) $intCid . '
						AND DATE_TRUNC ( \'day\', ch.date + INTERVAL \'' . $intDaysInterval . ' days\' ) = DATE_TRUNC ( \'day\', NOW() )';

		return self::fetchPropertyHoliday( $strSql, $objDatabase );

	}

	public static function fetchPropertyHolidayAvailabilityByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT 
						ph.open_time,
						ph.close_time,
                        ch.date
					FROM 
						property_holidays AS ph
                        JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE 
						ph.property_id = ' . ( int ) $intPropertyId . '
						AND ph.cid = ' . ( int ) $intCid . '
						AND ph.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyHolidaysByPropertyIdByCidWithCompanyHolidayDate( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ph.*,
						ch.date AS company_holiday_date
					FROM
						property_holidays AS ph
						LEFT JOIN company_holidays AS ch ON ( ph.company_holiday_id = ch.id AND ph.cid = ch.cid )
					WHERE
						ch.date IS NOT NULL
						AND ph.company_holiday_id IS NOT NULL
						AND ph.property_id = ' . ( int ) $intPropertyId . '
						AND ph.cid = ' . ( int ) $intCid;

		return self::fetchPropertyHolidays( $strSql, $objDatabase );
	}

	public static function fetchLatestUpdatedPropertyHolidays( $objClientDatabase ) {
		$strSql = 'SELECT
						ph.*,
						ch.date as company_holiday_date
					FROM 
						property_holidays ph 
						LEFT JOIN company_holidays ch ON ph.company_holiday_id = ch.id AND ph.cid = ch.cid 
					WHERE 
						ph.updated_on >= NOW() - INTERVAL \'1 day\'';

		return CPropertyHolidays::fetchPropertyHolidays( $strSql, $objClientDatabase );
	}

	/**
	 * Overridden Functions.
	 */

	public static function bulkInsert( $arrobjPropertyHolidays, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false, $boolIsReturnIds = false, $objVoipDatabase = NULL ) {

		if( false == ( $strSql = parent::bulkInsert( $arrobjPropertyHolidays, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly, $boolIsReturnIds ) ) ) {
			return false;
		}

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', 'Voip Database object not found.' ) );
			return false;
		}

		foreach( $arrobjPropertyHolidays as $objPropertyHoliday ) {
			$arrobjPropertyCallHolidays[] = $objPropertyHoliday->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		}

		if( true == valArr( $arrobjPropertyCallHolidays ) && false == CPropertyCallHolidays::bulkInsert( $arrobjPropertyCallHolidays, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;
	}

	public static function bulkUpdate( $arrobjPropertyHolidays, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		if( false == ( $strSql = parent::bulkUpdate( $arrobjPropertyHolidays, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly ) ) ) {
			return false;
		}

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', 'Voip Database object not found.' ) );
			return false;
		}

		foreach( $arrobjPropertyHolidays as $objPropertyHoliday ) {
			$arrobjPropertyCallHolidays[] = $objPropertyHoliday->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		}

		if( true == valArr( $arrobjPropertyCallHolidays ) && false == CPropertyCallHolidays::bulkUpdate( $arrobjPropertyCallHolidays, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;
	}

	public static function bulkDelete( $arrobjPropertyHolidays, $objDatabase, $boolIsReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		if( false == ( $strSql = parent::bulkDelete( $arrobjPropertyHolidays, $objDatabase, $boolIsReturnSqlOnly ) ) ) {
			return false;
		}

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', 'Voip Database object not found.' ) );
			return false;
		}

		foreach( $arrobjPropertyHolidays as $objPropertyHoliday ) {
			$arrobjPropertyCallHolidays[] = $objPropertyHoliday->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		}

		if( true == valArr( $arrobjPropertyCallHolidays ) && false == CPropertyCallHolidays::bulkDelete( $arrobjPropertyCallHolidays, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;
	}

}
?>