<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStages
 * Do not add any new functions to this class.
 */

class CApplicationStages extends CBaseApplicationStages {

	public static function fetchApplicationStages( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationStage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationStage( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationStage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>