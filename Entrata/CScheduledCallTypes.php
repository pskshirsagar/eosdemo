<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallTypes
 * Do not add any new functions to this class.
 */

class CScheduledCallTypes extends CBaseScheduledCallTypes {

	public static function fetchScheduledCallTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScheduledCallType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScheduledCallType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledCallType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllScheduledCallTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM scheduled_call_types ORDER BY order_num, LOWER( name )';

		return self::fetchScheduledCallTypes( $strSql, $objDatabase );
	}

}
?>