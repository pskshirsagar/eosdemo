<?php

class CTaxIdType extends CBaseTaxIdType {

	protected $m_strCountryName;

	const SSN = 1;
	const EIN = 2;
	const SIN = 3;
	const RFC = 4;
	const NIR = 5;
	const PPS = 6;
	const NIF = 7;
	const CIN = 8;
	const NINO = 9;

	const CITIZENSHIP_CITIZEN = 'citizen';
    const CITIZENSHIP_EITHER = 'either';
    const CITIZENSHIP_VISITOR = 'visitor';

	public static $c_arrstrMixTaxIdTypes 				= [ self::SSN, self::NIR, self::CIN, self::NINO ];
	public static $c_arrstrNonSpanishTaxIdTypes 		= [ self::SSN, self::EIN, self::SIN, self::RFC, self::NIR, self::PPS, self::CIN, self::NINO ];
	public static $c_arrstrSpanishTaxIdTypes 			= [ self::NIF ];
	public static $c_arrintDefinesCitizenshipValues     = [ self::CITIZENSHIP_CITIZEN, self::CITIZENSHIP_EITHER, self::CITIZENSHIP_VISITOR ];

	public function setCountryName( $strCountryName ) {
		$this->m_strCountryName = $strCountryName;
	}

	public function getCountryName() {
		return $this->m_strCountryName;
	}

	const SSN_TAX_TYPE_VALIDATION_STRING = '/[^a-z0-9]/i';
	const OTHER_TAX_TYPE_VALIDATION_STRING = '/[^a-z0-9\/\x5c-]/i';
	const NIF_TYPE_VALIDATION_STRING = '/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/';
	const CIN_TYPE_VALIDATION_STRING = '/(^\d{17}([a-zA-Z0-9]{1})$)|(^\d{14}([a-zA-Z0-9]{1})$)/';
	const NINO_TYPE_VALIDATION_STRING = '/(^([a-zA-Z]{2}\d{6}[a-zA-Z]{1})$)/';

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAbbreviations() {
		return [
			self::SSN   => __( 'SSN' ),
			self::EIN   => __( 'EIN' ),
			self::SIN   => __( 'SIN' ),
			self::RFC   => __( 'RFC' ),
			self::NIR   => __( 'NIR' ),
			self::PPS   => __( 'PPS' ),
			self::NIF   => __( 'NIF' ),
			self::CIN   => __( 'CIN' ),
			self::NINO   => __( 'NINO' )
		];
	}

	public function getTaxIdTypeIdMaskFormat() {
		return [
			self::SSN   => '*****XXXX',
			self::EIN   => '***********XXXX',
			self::SIN   => 'XXXX***********',
			self::RFC   => '',
			self::NIR   => 'XXXXX****',
			self::PPS   => '*****XXXX',
			self::NIF   => '*****XXXX',
			self::CIN   => 'XXXX**************',
			self::NINO   => '*****XXXX'
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAbbreviation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaskFormat() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOrganzation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['country_name'] ) )
			$this->setCountryName( $arrmixValues['country_name'] );
	}

	public function getDefinesCitizenship() {
	    return $this->getDetailsField( 'defines_citizenship' );
    }

    public function setDefinesCitizenship( string $strDefinesCitizenship ) {
       if( !in_array( $strDefinesCitizenship, self::$c_arrintDefinesCitizenshipValues ) ) return false;
       return $this->setDetailsField( 'defines_citizenship', $strDefinesCitizenship );
    }

}
?>