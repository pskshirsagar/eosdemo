<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAlerts
 * Do not add any new functions to this class.
 */

class CCustomerAlerts extends CBaseCustomerAlerts {

	public static function doesTableExist( $objDatabase ) {
		$strSql = 'SELECT EXISTS (
					   SELECT 1
					   FROM   information_schema.tables 
					   WHERE  table_schema = \'public\'
					   AND    table_name = \'customer_alerts\'
					   )';

		return self::fetchCustomerAlerts( $strSql, $objDatabase );

	}

	public static function fetchCustomerAlertById( $intId, $objDatabase ) {

		$strSql = 'SELECT * FROM customer_alerts ca
				   WHERE ca.id = ' . ( int ) $intId;

		return self::fetchCustomerAlert( $strSql, $objDatabase );

	}

	public static function fetchCustomerAlertsByIds( $arrintIds, $objDatabase ) {

		$strSql = 'SELECT * FROM customer_alerts ca
				   WHERE ca.id IN( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchCustomerAlerts( $strSql, $objDatabase );

	}

	public static function fetchCustomerAlertsByCidByPropertyIdByCustomerId( $intCid, $intPropertyId, $intCustomerId, $objDatabase, $intNumberOfDays = NULL, $boolCustomerIsTenant = false, $arrmixPropertyPreferencesKeys = NULL ) {

		if( false == valArr( $arrmixPropertyPreferencesKeys ) ) return NULL;

		$strSql = 'SELECT 
						ca.id as id,
						ca.updated_on as date,
						cat.name as alert_type,
						ca.reference_id as reference_id,
						ca.priority as priority,
						util_get_translated( \'action_text\', ca.action_text, ca.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) as action_text,
						ca.customer_alert_type_id as customer_alert_type_id,
						ca.updated_on as updated_on,
						ca.dismissed_on,
						util_get_translated( \'alert_title\', ca.alert_title, ca.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) as alert_title,
						util_get_translated( \'alert_description\', ca.alert_description, ca.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) as alert_description,
						fa.file_id,
						util_get_translated( \'name\', cat.name, cat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as alert_type_name, 
						util_get_translated( \'description\', cat.description, cat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as alert_type_description, 
						cat.details as alert_type_details
					FROM customer_alerts ca
						JOIN customer_alert_types cat ON( cat.id = ca.customer_alert_type_id )
						LEFT JOIN file_associations fa ON ( fa.id = ca.reference_id )
					WHERE 
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.customer_id = ' . ( int ) $intCustomerId . '
						AND cat.id NOT IN( ' . CCustomerAlertType::ANNOUNCEMENT . ', ' . CCustomerAlertType::MESSAGE_CENTER . ' ) ';

		$boolCustomerIsTenantNotSet = true;
		if( true === $boolCustomerIsTenant ) {
			$boolCustomerIsTenantNotSet = false;
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::ROOMMATE_PAYMENT . ', ' . CCustomerAlertType::RATING_AND_REVIEW . ' )';
		}

		if( false == $arrmixPropertyPreferencesKeys['my_apartment_enabled'] || false == $arrmixPropertyPreferencesKeys['SHOW_RENEWAL_OFFERS_IN_RESIDENT_PORTAL'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::RENEWAL_OFFER . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['resident_pay_enabled'] ) {
			if( true == $boolCustomerIsTenantNotSet ) {
				$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::LATE_RENT . ', ' . CCustomerAlertType::SCHEDULED_CHARGE . ', ' . CCustomerAlertType::AR_PAYMENT_RETURN . ', ' . CCustomerAlertType::SCHEDULED_PAYMENT_RETURN . ', ' . CCustomerAlertType::PAUSED_SCHEDULED_PAYMENT . ', ' . CCustomerAlertType::ROOMMATE_PAYMENT . ', ' . CCustomerAlertType::RELINK_PAYMENT_ACCOUNT . ' ) ';
			} else {
				$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::LATE_RENT . ', ' . CCustomerAlertType::SCHEDULED_CHARGE . ', ' . CCustomerAlertType::AR_PAYMENT_RETURN . ', ' . CCustomerAlertType::SCHEDULED_PAYMENT_RETURN . ', ' . CCustomerAlertType::PAUSED_SCHEDULED_PAYMENT . ' ) ';
			}
		}

		if( false == $arrmixPropertyPreferencesKeys['documents_enabled'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::NEW_DOCUMENT_REQUESTED . ', ' . CCustomerAlertType::SIGN_LEASE . ', ' . CCustomerAlertType::ESIGN . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['inspection_enabled'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::INSPECTION . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_in_scheduler'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::SCHEDULED_MOVE_IN . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_in_checklist'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::MOVE_IN_CHECKLIST . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_out_checklist'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::MOVE_OUT_CHECKLIST . ' ) ';
		}

		if( true == $boolCustomerIsTenantNotSet
		    && true == $arrmixPropertyPreferencesKeys['HIDE_RATINGS_AND_REVIEWS_POP_UP'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::RATING_AND_REVIEW . ' ) ';
		}

		$strSql .= ' AND ca.deleted_by IS NULL 
					AND ca.deleted_on IS NULL 
					ORDER BY ca.priority, ca.updated_on DESC;';

		return self::fetchCustomerAlerts( $strSql, $objDatabase );
	}

	public static function fetchCustomerAlertsCountByCidByPropertyIdByCustomerId( $intCid, $intPropertyId, $intCustomerId, $objDatabase, $intNumberOfDays = NULL, $boolIsSkipLateRentAlert = false, $boolCustomerIsTenant = false, $arrmixPropertyPreferencesKeys = NULL ) {

		$strSql = 'SELECT  
						COUNT(*)
					FROM 
						customer_alerts ca
						JOIN customer_alert_types cat ON( cat.id = ca.customer_alert_type_id )
						LEFT JOIN file_associations fa ON ( fa.id = ca.reference_id AND
					       ca.customer_alert_type_id = ' . CCustomerAlertType::ESIGN . ' AND fa.require_sign = 1 AND
					       fa.file_signed_on IS NULL )
					WHERE 
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.deleted_on IS NULL
						AND ca.dismissed_on IS NULL
						AND ca.dismissed_by IS NULL
						AND cat.id NOT IN( ' . CCustomerAlertType::ANNOUNCEMENT . ', ' . CCustomerAlertType::MESSAGE_CENTER . ' ) ';

		$boolCustomerIsTenantNotSet = true;
		if( true === $boolCustomerIsTenant ) {
			$boolCustomerIsTenantNotSet = false;
			$strSql                     .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::ROOMMATE_PAYMENT . ', ' . CCustomerAlertType::RATING_AND_REVIEW . ' )';
		}

		if( false == is_null( $intNumberOfDays ) ) {
			$strSql .= ' AND ca.updated_on > ( NOW() - INTERVAL \'' . $intNumberOfDays . ' days\' )';
		}

		if( false == $arrmixPropertyPreferencesKeys['resident_pay_enabled'] ) {
			if( true == $boolCustomerIsTenantNotSet ) {
				$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::LATE_RENT . ', ' . CCustomerAlertType::SCHEDULED_CHARGE . ', ' . CCustomerAlertType::AR_PAYMENT_RETURN . ', ' . CCustomerAlertType::SCHEDULED_PAYMENT_RETURN . ', ' . CCustomerAlertType::PAUSED_SCHEDULED_PAYMENT . ', ' . CCustomerAlertType::ROOMMATE_PAYMENT . ', ' . CCustomerAlertType::RELINK_PAYMENT_ACCOUNT . ' ) ';
			} else {
				$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::LATE_RENT . ', ' . CCustomerAlertType::SCHEDULED_CHARGE . ', ' . CCustomerAlertType::AR_PAYMENT_RETURN . ', ' . CCustomerAlertType::SCHEDULED_PAYMENT_RETURN . ', ' . CCustomerAlertType::PAUSED_SCHEDULED_PAYMENT . ' ) ';
			}
		}

		if( false == $arrmixPropertyPreferencesKeys['my_apartment_enabled'] || false == $arrmixPropertyPreferencesKeys['SHOW_RENEWAL_OFFERS_IN_RESIDENT_PORTAL'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::RENEWAL_OFFER . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['documents_enabled'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::NEW_DOCUMENT_REQUESTED . ', ' . CCustomerAlertType::SIGN_LEASE . ', ' . CCustomerAlertType::ESIGN . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['inspection_enabled'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::INSPECTION . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_in_scheduler'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::SCHEDULED_MOVE_IN . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_in_checklist'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::MOVE_IN_CHECKLIST . ' ) ';
		}

		if( false == $arrmixPropertyPreferencesKeys['move_out_checklist'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::MOVE_OUT_CHECKLIST . ' ) ';
		}

		if( true == $boolCustomerIsTenantNotSet
		    && true == $arrmixPropertyPreferencesKeys['HIDE_RATINGS_AND_REVIEWS_POP_UP'] ) {
			$strSql .= ' AND cat.id NOT IN ( ' . CCustomerAlertType::RATING_AND_REVIEW . ' ) ';
		}

		$arrintCount = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
		return 0;
	}

	public static function deleteAnnouncementCustomerAlerts( $objDatabase ) {

		$strSql = 'DELETE FROM customer_alerts ca
					WHERE ca.customer_alert_type_id = ' . CCustomerAlertType::ANNOUNCEMENT . '
					AND ca.reference_id NOT IN (
						SELECT
							a.id
						FROM
							announcements a
						WHERE
							a.announcement_type_id IN ( ' . CAnnouncementType::PROPERTY . ',' . CAnnouncementType::COMPANY . ' )
							AND
								CASE
									WHEN ( a.frequency_id IS NULL OR a.frequency_id = ' . CFrequency::DAILY . ' )
										THEN a.date_start <= NOW()::date AND ( a.date_end >= NOW()::date OR a.date_end IS NULL )
									WHEN( a.frequency_id = ' . CFrequency::ONCE . ' )
										THEN a.date_start IS NOT NULL AND NOW()::date = a.date_start
									WHEN( a.frequency_id = ' . CFrequency::WEEKLY . ' )
										THEN date_part( \'dow\', NOW() ) = date_part( \'dow\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
									WHEN(a.frequency_id = ' . CFrequency::MONTHLY . ')
										THEN date_part( \'day\', NOW() ) = date_part( \'day\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
									ELSE TRUE
								END
							AND a.is_published = 1
					)';

		return fetchData( $strSql, $objDatabase );

	}

	public static function deleteDismissedCustomerAlerts( $objDatabase ) {

		$strSql = 'DELETE FROM customer_alerts ca
					WHERE ca.dismissed_on IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function markAsDeletedCustomerAlertsByCustomerAlertTypeId( $intCustomerAlertTypeId, $intUserId, $objDatabase, $strInterval = '1 month' ) {

		$strSql = 'UPDATE
						customer_alerts ca
					SET
						ca.deleted_on = NOW(),
						ca.deleted_by = ' . ( int ) $intUserId . '
					WHERE 
						ca.customer_alert_type_id = ' . ( int ) $intCustomerAlertTypeId . '
						AND ca.created_on < ( NOW() - INTERVAL \'' . $strInterval . '\' )
						';

		return fetchData( $strSql, $objDatabase );
	}

	public static function markAsDeletedCustomerAlertsByCustomerIdByCustomerAlertTypeId( $intCustomerAlertTypeId, $intUserId, $objDatabase, $strInterval = '1 month' ) {

		$strSql = 'UPDATE
						customer_alerts
					SET
						deleted_on = NOW(),
						deleted_by = ' . ( int ) $intUserId . ',
						dismissed_on = NOW(),
						dismissed_by = ' . ( int ) $intUserId . ',
						updated_on = NOW(),
						updated_by = ' . ( int ) $intUserId . '
					WHERE 
						customer_alert_type_id = ' . ( int ) $intCustomerAlertTypeId . '
						AND customer_id = ' . ( int ) $intUserId . '
						AND created_on < ( NOW() - INTERVAL \'' . $strInterval . '\' )
					';

		return fetchData( $strSql, $objDatabase );
	}

	public static function dismissCustomerAlertsByCustomerIdByCustomerAlertTypeId( $intCustomerAlertTypeId, $intCustomerId, $objDatabase ) {

		$strSql = 'UPDATE
						customer_alerts
					SET
						dismissed_on = NOW(),
						dismissed_by = ' . ( int ) $intCustomerId . ',
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCustomerId . '
				   WHERE customer_alert_type_id = ' . ( int ) $intCustomerAlertTypeId . '
						AND customer_id = ' . ( int ) $intCustomerId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function deleteExpiredCustomerAlertsByCustomerAlertTypeId( $intCustomerAlertTypeId, $objDatabase, $strInterval = '3 years' ) {

		$strSql = 'DELETE FROM 
						customer_alerts ca
          			WHERE ca.customer_alert_type_id = ' . ( int ) $intCustomerAlertTypeId . '
          			AND ca.created_on < ( NOW() - INTERVAL \'' . $strInterval . '\' )
          ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function dismissCustomerAlertsByIds( $arrintIds, $intCustomerId, $objDatabase ) {

		$strSql = 'UPDATE
						customer_alerts
					SET
						dismissed_on = NOW(),
						dismissed_by = ' . ( int ) $intCustomerId . ',
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCustomerId . '
				   WHERE id IN( ' . implode( ',', $arrintIds ) . ' )
				   AND customer_id = ' . ( int ) $intCustomerId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomerAlertsCountByCidByPropertyIdByCustomerIdByAlertTypeId( $intCid, $intPropertyId, $intCustomerId, $objDatabase, $intNumberOfDays = NULL, $intCustomerAlertTypeId ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) || false == valId( $intNumberOfDays ) || false == valId( $intCustomerAlertTypeId ) ) {
			return 0;
		}

		$strSql = 'SELECT  
						COUNT(*)
					FROM 
						customer_alerts ca
						JOIN customer_alert_types cat ON( cat.id = ca.customer_alert_type_id )
					WHERE 
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.dismissed_on IS NULL
						AND cat.id = ' . ( int ) $intCustomerAlertTypeId . ' 
						AND cat.id <> ' . CCustomerAlertType::MESSAGE_CENTER;

		if( false == is_null( $intNumberOfDays ) ) {
			$strSql .= ' AND ca.updated_on > ( NOW() - INTERVAL \'' . $intNumberOfDays . ' days\' )';
		}

		$arrintCount = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
		return 0;
	}

}

?>