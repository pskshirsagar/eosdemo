<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifiedAttachments
 * Do not add any new functions to this class.
 */

class CCustomerClassifiedAttachments extends CBaseCustomerClassifiedAttachments {

	public static function fetchClassifiedAttachmentsByClassifiedIdByCid( $intClassifiedId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						customer_classified_attachments
					WHERE
						customer_classified_id = ' . ( int ) $intClassifiedId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND cid = ' . ( int ) $intCid;

		return parent::fetchCustomerClassifiedAttachments( $strSql, $objDatabase );
	}

	public static function fetchClassifiedAttachmentsByClassifiedIdsByCid( $arrintClassifiedId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintClassifiedId ) || false == valId( $intCid ) ) return NULL;
		$strSql = 'SELECT
						cls.*,
						CASE
							WHEN cls.is_default_image = true
							THEN 1
							ELSE 0
						END AS classified_default_image
					FROM
						customer_classified_attachments cls
					WHERE
						customer_classified_id IN ( ' . implode( ', ', $arrintClassifiedId ) . ' )
						AND cid = ' . ( int ) $intCid . ' 
						AND cls.deleted_on IS NULL
						AND cls.deleted_by IS NULL
					ORDER BY classified_default_image DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchSimpleCustomerClassifiedAttachmentByIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == valId( $intId ) || false == valId( $intCid ) ) return NULL;
		$strSql = 'SELECT
						id,
						file_path,
						file_name
					FROM
						customer_classified_attachments
					WHERE
						id = ' . ( int ) $intId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND cid = ' . ( int ) $intCid;

		$arrmixCustomerClassifiedAttachments = ( array ) fetchData( $strSql, $objDatabase );
		return array_pop( $arrmixCustomerClassifiedAttachments );
	}

	public static function fetchSimpleClassifiedAttachmentsByCustomerClassifiedIdByCid( $intCustomerClassifiedId, $intCid, $objDatabase ) {
		if( false == valId( $intCustomerClassifiedId ) || false == valId( $intCid ) ) return NULL;
		$strSql = 'SELECT
						cls.id,
						cls.note,
						CASE
							WHEN cls.is_default_image = true
							THEN 1
							ELSE 0
						END AS classified_default_image
					FROM
						customer_classified_attachments cls
					WHERE
						customer_classified_id = ' . ( int ) $intCustomerClassifiedId . '
						AND cid = ' . ( int ) $intCid . ' 
						AND cls.deleted_on IS NULL
						AND cls.deleted_by IS NULL
					ORDER BY classified_default_image DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchCustomerClassifiedAttachmentsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		return CBaseCustomerClassifiedAttachments::fetchCustomerClassifiedAttachments( sprintf( 'SELECT * FROM customer_classified_attachments WHERE id IN ( %s ) AND cid = %d', implode( $arrintIds, ',' ), ( int ) $intCid ), $objDatabase );
	}

}
?>