<?php

class CBudgetDataSourceSubType extends CBaseBudgetDataSourceSubType {

	const CURRENT_YEAR_BUDGET = 1;
	const CURRENT_YEAR_ACTUAL = 2;
	const PRIOR_YEAR_ACTUAL   = 3;

	public static $c_arrintBudgetDataSourceSubTypes = [
		self::CURRENT_YEAR_BUDGET,
		self::CURRENT_YEAR_ACTUAL,
		self::PRIOR_YEAR_ACTUAL
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>