<?php

class CPropertyBuildingSubsidyDetail extends CBasePropertyBuildingSubsidyDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuildingIdentificationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstYearLevelPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComplianceStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExtendedUseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>