<?php

class CDocumentPreference extends CBaseDocumentPreference {

	const LEASE_USAGE_LEASE_INTERVAL_TYPE_APPLICATION = 'LEASE_USAGE_LEASE_INTERVAL_TYPE_APPLICATION';
	const LEASE_USAGE_LEASE_INTERVAL_TYPE_RENEWAL		= 'LEASE_USAGE_LEASE_INTERVAL_TYPE_RENEWAL';
	const LEASE_USAGE_LEASE_INTERVAL_TYPE_TRANSFER	= 'LEASE_USAGE_LEASE_INTERVAL_TYPE_TRANSFER';

	public static $c_arrstrLeaseUsagePreferences = [ 'LEASE_USAGE_LEASE_INTERVAL_TYPE_APPLICATION', 'LEASE_USAGE_LEASE_INTERVAL_TYPE_RENEWAL', 'LEASE_USAGE_LEASE_INTERVAL_TYPE_TRANSFER' ];

	public function valId() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCid() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		// }

		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getDocumentId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getKey() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', '' ) );
		// }

		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getValue() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsSecure() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getIsSecure() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_secure', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getIsPublished() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getUpdatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getUpdatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCreatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getCreatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>