<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmailTemplateAttachments
 * Do not add any new functions to this class.
 */

class CEmailTemplateAttachments extends CBaseEmailTemplateAttachments {

	public static function fetchEmailTemplateAttachmentsByEmailTemplateIdsByCid( $arrintEmailTemplateIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintEmailTemplateIds ) ) {
			return;
		}

		$strSql = ' SELECT 
						eta.*
					FROM 
						email_template_attachments eta
					WHERE 
						eta.cid = ' . ( int ) $intCid . '
						AND eta.email_template_id IN ( ' . implode( ',', $arrintEmailTemplateIds ) . ' ) ';

		return self::fetchEmailTemplateAttachments( $strSql, $objDatabase );
	}

}
?>