<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestTypes
 * Do not add any new functions to this class.
 */

class CGuestTypes extends CBaseGuestTypes {

	public static function fetchGuestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGuestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGuestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGuestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllGuestTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM guest_types';

		return self::fetchGuestTypes( $strSql, $objDatabase );
	}

}
?>