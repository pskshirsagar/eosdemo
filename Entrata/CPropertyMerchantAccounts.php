<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMerchantAccounts
 * Do not add any new functions to this class.
 */

class CPropertyMerchantAccounts extends CBasePropertyMerchantAccounts {

	public static function fetchCustomPropertyMerchantAccountsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolGetDisabledData = false, $boolIsGetCommercialAccount = false ) {
        if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
        	return NULL;
        }

		$strField = $strJoin = '';
		if( true == $boolIsGetCommercialAccount ) {
			$strField = ', ma.commercial_merchant_account_id, ma.merchant_processing_type_id';
			$strJoin = ' JOIN merchant_accounts AS ma ON( pma.cid = ma.cid and pma.company_merchant_account_id = ma.id )';
		}
		$strSql = 'SELECT
						pma.*' . $strField . '
					 FROM
						 property_merchant_accounts AS pma ';

		$strSql .= trim( $strJoin );
		$strSql .= ' WHERE property_id = ' . ( int ) $intPropertyId . ' AND pma.cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL AND disabled_on IS NULL';
		}

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );
    }

    public static function fetchCustomPropertyMerchantAccountsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
    	return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM property_merchant_accounts WHERE company_merchant_account_id = %d AND cid = %d AND disabled_by IS NULL AND disabled_on IS NULL', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
    }

	public static function fetchPropertyMerchantAccountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolGetDisabledData = false, $boolIsGetCommercialAccount = false ) {
		if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}
		$strField = $strJoin = '';
		if( true == $boolIsGetCommercialAccount ) {
			$strField = ', ma.commercial_merchant_account_id';
			$strJoin = ' JOIN merchant_accounts AS ma ON( pma.cid = ma.cid and pma.company_merchant_account_id = ma.id )';
		}
		$strSql = 'SELECT
						pma.*' . $strField . '
					 FROM
						 property_merchant_accounts AS pma ';
		$strSql .= trim( $strJoin );
		$strSql .= 'WHERE
						 property_id =' . ( int ) $intPropertyId . '
						 AND ar_code_id IS NULL
						 AND pma.cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL AND disabled_on IS NULL';
		}
		return self::fetchPropertyMerchantAccount( $strSql, $objDatabase );
	}

	public static function fetchArCodeMerchantAccountIdsByPropertyIdByMerchantAccountIdsByCid( $intPropertyId, $arrintMerchantAccountIds, $intCid, $objDatabase ) {
		if( false == is_numeric( $intPropertyId ) || false == valArr( $arrintMerchantAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
		                  company_merchant_account_id, ar_code_id
		           FROM
		                  property_merchant_accounts
				   WHERE
				          company_merchant_account_id IN ( ' . implode( ',', $arrintMerchantAccountIds ) . ' )
				   AND
				          property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . '
				   AND
				          disabled_by IS NULL
				   AND
				          disabled_on IS NULL';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrFinalReturnData = [];

		foreach( $arrstrData as $arrstrSubData ) {
			$intKey = ( true == is_null( $arrstrSubData['ar_code_id'] ) ) ? - 1 : $arrstrSubData['ar_code_id'];
			$arrstrFinalReturnData[$intKey] = $arrstrSubData['company_merchant_account_id'];
		}

		return $arrstrFinalReturnData;
	}

	public static function fetchMerchantAccountIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrmixCompanyMerchantAccountsData = fetchData( 'SELECT DISTINCT ( company_merchant_account_id ) FROM property_merchant_accounts WHERE cid = ' . ( int ) $intCid . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND disabled_by IS NULL AND disabled_on IS NULL ', $objDatabase );
		$arrintCompanyMerchantAccountIds = [];

		if( true == valArr( $arrmixCompanyMerchantAccountsData ) ) {
			foreach( $arrmixCompanyMerchantAccountsData as $arrintCompanyMerchantAccountRow ) {
				$arrintCompanyMerchantAccountIds[$arrintCompanyMerchantAccountRow['company_merchant_account_id']] = $arrintCompanyMerchantAccountRow['company_merchant_account_id'];
			}
		}

 		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintCompanyMerchantAccountIds ) ) {
			return $arrintCompanyMerchantAccountIds;
	    }

 		return NULL;
	}

	public static function fetchDefaultCompanyMerchantAccountIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM property_merchant_accounts WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND ar_code_id IS NULL AND disabled_by IS NULL AND disabled_on IS NULL LIMIT 1';
        $objPropertyMerchantAccount = self::fetchPropertyMerchantAccount( $strSql, $objDatabase );

        if( true == valObj( $objPropertyMerchantAccount, 'CPropertyMerchantAccount' ) ) {
        	return ( int ) $objPropertyMerchantAccount->getCompanyMerchantAccountId();
        } else {
        	return NULL;
        }
	}

	public static function fetchCompanyMerchantAccountIdsByPropertyIdByArCodeIdsByCid( $intPropertyId, $arrintArCodeIds, $intCid, $objDatabase ) {

		$arrintArCodeIds = array_filter( $arrintArCodeIds );
		if( false == valId( $intPropertyId ) || false == valArr( $arrintArCodeIds ) ) {
			return NULL;
		}

    	$strSql = 'SELECT
    					pma.company_merchant_account_id
    				FROM
    					property_merchant_accounts pma
    				WHERE
    					pma.property_id = ' . ( int ) $intPropertyId . '
    					AND pma.cid = ' . ( int ) $intCid . '
    					AND ( pma.ar_code_id IS NULL OR pma.ar_code_id IN ( ' . implode( ',', $arrintArCodeIds ) . ' ))
    					AND pma.disabled_by IS NULL
    					AND pma.disabled_on IS NULL ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		$arrintCompanyMerchantAccountIds = [];

		foreach( $arrstrData as $arrintCompanyMerchantAccountData ) {
			$arrintCompanyMerchantAccountIds[$arrintCompanyMerchantAccountData['company_merchant_account_id']] = $arrintCompanyMerchantAccountData['company_merchant_account_id'];
		}

		return $arrintCompanyMerchantAccountIds;
	}

	public static function fetchCompanyMerchantAccountIdByLeaseIdByArCodeIdsByCid( $intLeaseId, $arrintArCodeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintArCodeIds ) ) {
			return [];
		}

		if( false == isset ( $intLeaseId ) || false == is_numeric( $intLeaseId ) ) {
			return NULL;
		}

    	$strSql = 'SELECT
    					pma.company_merchant_account_id
    				 FROM
    					property_merchant_accounts pma,
    					leases cl
    			    WHERE
    					cl.property_id = pma.property_id AND cl.cid = pma.cid
    					AND cl.id = ' . ( int ) $intLeaseId . '
    					AND pma.cid = ' . ( int ) $intCid . '
    					AND ( pma.ar_code_id IS NULL OR pma.ar_code_id IN ( ' . implode( ',', $arrintArCodeIds ) . ' ))
    					AND pma.disabled_by IS NULL
    					AND pma.disabled_on IS NULL
    				LIMIT 1';

		$arrmixCompanyMerchantAccountsData = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrmixCompanyMerchantAccountsData[0]['company_merchant_account_id'] ) ) {
			return $arrmixCompanyMerchantAccountsData[0]['company_merchant_account_id'];
		}

		return NULL;
	}

	public static function fetchCompanyMerchantAccountIdByPropertyIdByArCodeIdByCid( $intPropertyId, $intArCodeId, $intCid, $objClientDatabase ) {

		if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT pma.company_merchant_account_id FROM
					property_merchant_accounts pma
					WHERE pma.property_id = ' . ( int ) $intPropertyId . '
						AND pma.cid = ' . ( int ) $intCid . '
						AND pma.ar_code_id IS NULL OR pma.ar_code_id = ' . ( int ) $intArCodeId . '
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
					LIMIT 1';

		$arrmixCompanyMerchantAccountsData = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrmixCompanyMerchantAccountsData[0]['company_merchant_account_id'] ) ) {
			return $arrmixCompanyMerchantAccountsData[0]['company_merchant_account_id'];
		}

    	return NULL;
	}

	public static function fetchPropertiesByCidByMerchantAccountId( $objCompanyMerchantAccount, $objDatabase ) {

		$intCid 	= ( false == is_numeric( $objCompanyMerchantAccount->getCid() ) ) ? 0 : $objCompanyMerchantAccount->getCid();
		$intCompanyMerchantAccountId = ( false == is_numeric( $objCompanyMerchantAccount->getId() ) ) ? 0 : $objCompanyMerchantAccount->getId();

		$strSql = 'SELECT
							cp.property_name
						FROM
							properties cp,
							property_merchant_accounts pma
						WHERE
							pma.company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
							AND pma.property_id = cp.id AND pma.cid = cp.cid
							AND pma.cid = ' . ( int ) $intCid . '
							AND pma.ar_code_id IS NULL
							AND pma.disabled_by IS NULL
							AND pma.disabled_on IS NULL ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByPropertyIdsByNULLArCodeIdByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolGetDisabledData = false ) {

		$strSql = 'SELECT
							*
						FROM
							property_merchant_accounts
						WHERE
							property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND ar_code_id IS NULL ';

		if( false == $boolGetDisabledData ) {
			$strSql .= 'AND disabled_by IS NULL
						AND disabled_on IS NULL ';
		}

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolGetDisabledData = false, $boolExcludeDefaultAccount = false ) {

		$strSql = 'SELECT * FROM property_merchant_accounts WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL
						AND disabled_on IS NULL ';
		}

		if( true == $boolExcludeDefaultAccount ) {
			$strSql .= ' AND ar_code_id IS NOT NULL ';
		}

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsWithCommercialMerchantAccountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolGetDisabledData = false ) {
		if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pma.company_merchant_account_id,
						ma.commercial_merchant_account_id
					 FROM
						 property_merchant_accounts AS pma
						 JOIN merchant_accounts AS ma ON( pma.cid = ma.cid and pma.company_merchant_account_id = ma.id )
					 WHERE
						 property_id =' . ( int ) $intPropertyId . '
						 AND pma.cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL
						 AND disabled_on IS NULL ';
		}

		return rekeyArray( 'company_merchant_account_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchCustomPropertyMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolGetDisabledData = false ) {

		$strSql = 'SELECT
						property_id,
						company_merchant_account_id
					FROM
						property_merchant_accounts
					WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL
						AND disabled_on IS NULL ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByPropertyIdWithMerchantAccountSameAsDefaultMerchantAccount( $intArCodeId, $intPropertyId, $intCid, $objDatabase ) {

		return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE property_id = %d AND cid = %d AND ar_code_id IS NOT NULL AND company_merchant_account_id = %d AND disabled_by IS NULL AND disabled_on IS NULL ', 'property_merchant_accounts', ( int ) $intPropertyId, ( int ) $intCid, ( int ) $intArCodeId ), $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByOldAndNewArCodeIdByCid( $intOldArCodeId, $intNewArCodeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pma1.*
					FROM
						property_merchant_accounts pma1
					WHERE
						pma1.cid = ' . ( int ) $intCid . '
						AND pma1.ar_code_id = ' . ( int ) $intNewArCodeId . '
						AND EXISTS (
										SELECT
											1
										FROM
											property_merchant_accounts pma2
										WHERE
											pma2.cid = ' . ( int ) $intCid . '
											AND pma2.ar_code_id = ' . ( int ) $intOldArCodeId . '
											AND pma1.property_id = pma2.property_id
									)';

		return self::fetchPropertyMerchantAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyMerchantAccountCountByPropertyIdsByCid( $strPropertyIds, $intCid, $objClientDatabase ) {

				$strSql = 'SELECT
							    pma.company_merchant_account_id, count( id )
							FROM
							    property_merchant_accounts pma
							WHERE
							    pma.cid = ' . ( int ) $intCid . '
							    AND pma.property_id IN ( ' . $strPropertyIds . ' )
							    AND pma.ar_code_id IS NULL
							GROUP BY
							    pma.company_merchant_account_id';

				return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyMerchantAccountsByPropertyIdByArCodeIdsByCid( $intPropertyId, $arrintArCodeIds, $intCid, $objDatabase ) {
		$arrintArCodeIds = array_filter( $arrintArCodeIds );
		if( false == valArr( $arrintArCodeIds ) || false == valId( $intPropertyId ) ) {
			return [];
		}

		$strSql = 'SELECT
						pma.*
					FROM
						property_merchant_accounts pma
					WHERE
						pma.property_id = ' . ( int ) $intPropertyId . '
						AND pma.cid = ' . ( int ) $intCid . '
						AND ( pma.ar_code_id IS NULL OR pma.ar_code_id IN ( ' . implode( ',', $arrintArCodeIds ) . ' ) )
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL ';

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );
	}

	public static function fetchCustomMerchantAccountIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						company_merchant_account_id
					FROM
						property_merchant_accounts
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND disabled_by IS NULL AND disabled_on IS NULL';

		$arrstrMerchantAccountIdsData = fetchData( $strSql, $objDatabase );

		$arrintMerchantAccountIds = [];

		foreach( $arrstrMerchantAccountIdsData as $arrintMerchantAccountIdData ) {
			$arrintMerchantAccountIds[$arrintMerchantAccountIdData['company_merchant_account_id']] = $arrintMerchantAccountIdData['company_merchant_account_id'];
		}

		return $arrintMerchantAccountIds;
	}

	public static function fetchBankAccountsAssocoatedWithMerchantAccountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ba.id as account_number,
					    ba.account_name,
					    pba.property_id,
					    pba.id,
					    pma.id as merchant_account_id,
					    ba.is_credit_card_account
					FROM
						bank_accounts ba
					    JOIN property_bank_accounts pba ON ( ba.cid = pba.cid AND ba.id = pba.bank_account_id AND pba.cid = ' . ( int ) $intCid . ' AND pba.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')  )
					    LEFT JOIN property_merchant_accounts pma ON( pba.property_id = pma.property_id AND pma.cid = pba.cid AND pma.ar_code_id IS NOT NULL )
					WHERE
						is_disabled = 0
					    AND ba.check_bank_name IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    property_merchant_accounts
					WHERE
					    disabled_on IS NULL
					    AND disabled_by IS NULL
					    AND CID = ' . ( int ) $intCid;

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByCompanyMerchantAccountIdByArCodeIdByPropertyIdByCid( $intCompanyMerchantAccountId, $intArCodeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    property_merchant_accounts
					WHERE
					    disabled_by IS NULL
					    AND disabled_on IS NULL
					    AND company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
					    AND ar_code_id = ' . ( int ) $intArCodeId . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyMerchantAccount( $strSql, $objDatabase );
	}

	public static function fetchPropertyMerchantAccountByCompanyMerchantAccountIdByPropertyIdByCid( $intCompanyMerchantAccountId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
					     *
					 FROM
					     property_merchant_accounts
					 WHERE
					     company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
					     AND property_id = ' . ( int ) $intPropertyId . '
					     AND cid =' . ( int ) $intCid . '
					     AND ar_code_id IS NULL';

		return self::fetchPropertyMerchantAccount( $strSql, $objDatabase );
	}

	public static function fetchDefaultPropertyMerchantAccountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM property_merchant_accounts WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND ar_code_id IS NULL AND disabled_by IS NULL AND disabled_on IS NULL LIMIT 1';
		return self::fetchPropertyMerchantAccount( $strSql, $objDatabase );

	}

	public static function fetchCompanyMerchantAccountByPropertyIdByArCodeIdByCid( $intPropertyId, $intArCodeId, $intCid, $objDatabase ) {

		if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pma.*
                    FROM
						property_merchant_accounts pma
					WHERE pma.property_id = ' . ( int ) $intPropertyId . '
						AND pma.cid = ' . ( int ) $intCid . '
						AND pma.ar_code_id = ' . ( int ) $intArCodeId . '
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
					LIMIT 1';

		return self::fetchPropertyMerchantAccount( $strSql, $objDatabase );
	}

	public static function fetchDistinctCompanyMerchantAccountIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolGetDisabledData = false ) {
		$strSql = 'SELECT
						DISTINCT ( company_merchant_account_id )
					FROM
						property_merchant_accounts
					WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid;

		if( false == $boolGetDisabledData ) {
			$strSql .= ' AND disabled_by IS NULL
						AND disabled_on IS NULL ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );
		$arrintCompanyMerchantAccountIds = [];
		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrintCompanyMerchantAccountData ) {
				$arrintCompanyMerchantAccountIds[$arrintCompanyMerchantAccountData['company_merchant_account_id']] = $arrintCompanyMerchantAccountData['company_merchant_account_id'];
			}
		}
		return $arrintCompanyMerchantAccountIds;
	}

	public static function fetchPropertyMerchantAccountDetailsByArCodeIdsByPropertyIdByCid( $arrintArCodeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArCodeIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						property_merchant_accounts
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND ar_code_id IN( ' . implode( ',', $arrintArCodeIds ) . ' )';

		return self::fetchPropertyMerchantAccounts( $strSql, $objDatabase );

	}

}
?>