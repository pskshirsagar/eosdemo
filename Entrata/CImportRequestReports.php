<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportRequestReports
 * Do not add any new functions to this class.
 */

class CImportRequestReports extends CBaseImportRequestReports {

	public static function fetchImportRequestReportsByImportRequestIdByPropertyIdByCid( $intImportRequestId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						import_request_report.*
					FROM
						(
						SELECT
							DISTINCT ON ( r.default_report_id ) r.default_report_id,
							irr.*,
							r.name,
							r.title,
							COALESCE( ce.name_first, NULL ) AS name_first,
							COALESCE( ce.name_last, NULL ) AS name_last
						FROM
							import_request_reports irr
							JOIN reports r ON ( irr.cid = r.cid AND irr.report_id = r.id )
							LEFT JOIN company_users cu ON ( cu.cid = r.cid AND cu.id = irr.verified_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT OUTER JOIN company_employees ce ON( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
 						WHERE
							irr.cid = ' . ( int ) $intCid . '
							AND irr.property_id = ' . ( int ) $intPropertyId . '
							AND irr.import_request_id = ' . ( int ) $intImportRequestId . '
						) AS import_request_report
					ORDER BY
						import_request_report.import_entity_report_id;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchImportRequestReportsByReportIdByPropertyIdByCid( $intReportId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						import_request_reports
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND report_id = ' . ( int ) $intReportId . ';';

		return self::fetchImportRequestReports( $strSql, $objDatabase );
	}

	public static function fetchNonVerifiedImportRequestReportsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						import_request_reports
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND verified_by IS NULL
						AND verified_on IS NULL;';

		return self::fetchImportRequestReports( $strSql, $objDatabase );
	}

}
?>