<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForcedPlacedPolicies
 * Do not add any new functions to this class.
 */

class CForcedPlacedPolicies extends CBaseForcedPlacedPolicies {

	public static function fetchForcePlacedPolicyByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						force_placed_policies fpp
					WHERE
						fpp.lease_id = ' . ( int ) $intLeaseId . '
						AND fpp.customer_id = ' . ( int ) $intCustomerId . '
						AND fpp.cid = ' . ( int ) $intCid . '
						AND fpp.policy_end_date IS NULL';

		return self::fetchForcePlacedPolicy( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByPolicyTypeIdsByCids( $arrintInsurancePolicyTypeIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintInsurancePolicyTypeIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
                        fpp.*
                    FROM
                        forced_placed_policies fpp
                        JOIN resident_insurance_policies rip ON ( rip.cid = fpp.cid AND rip.id = fpp.resident_insurance_policy_id AND rip.insurance_policy_status_type_id = ' . CInsurancePolicyStatusType::ACTIVE . ' AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypeIds ) . ' ) )
                        JOIN insurance_policy_customers ipc ON ( ipc.cid = fpp.cid AND ipc.property_id = fpp.property_id AND ipc.lease_id = fpp.lease_id )
                        JOIN resident_insurance_policies rip1 ON ( rip1.cid = ipc.cid AND rip1.property_id = ipc.property_id AND rip1.id = ipc.resident_insurance_policy_id AND rip1.id <> fpp.resident_insurance_policy_id AND rip1.insurance_policy_status_type_id IN ( ' . sqlIntImplode( CInsurancePolicyStatusType::$c_arrintActiveAndCancelPendingInsurancePolicyStatusTypeIds ) . ' ) AND rip1.effective_date::date <= CURRENT_DATE )
                    WHERE
                        fpp.is_disabled = FALSE
                        AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchForcePlacedPolicyByLeaseIdsByCustomerIdsByCids( $arrintLeaseIds, $arrintCustomerIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						force_placed_policies fpp
					WHERE
						fpp.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND fpp.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND fpp.policy_end_date IS NULL';

		return self::fetchForcePlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPolicyByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						forced_placed_policies
					WHERE
						resident_insurance_policy_id IS NULL
						AND is_disabled = false
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchForcedPlacedPolicy( $strSql, $objDatabase );
	}

	public static function fetchCustomForcedPlacedPoliciesToRenewByPolicyTypeIdsByCids( $arrintInsurancePolicyTypeIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintInsurancePolicyTypeIds ) ) return NULL;
		$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];

		$strSql = 'SELECT
						*
					FROM
					(
						SELECT
							row_number ( ) OVER ( PARTITION BY li.lease_id
						ORDER BY
							li.id DESC ) AS latest_lease_interval,
							rip.id AS resident_insurance_policy_id,
							rip.policy_number,
							cl.active_lease_interval_id,
							rip.insurance_policy_type_id,
							fpp.*,
							c.email_address,
							c.name_first,
							c.name_last
						FROM
							forced_placed_policies fpp
							JOIN resident_insurance_policies rip ON ( fpp.resident_insurance_policy_id = rip.id AND fpp.cid = rip.cid )
							JOIN cached_leases cl ON ( cl.cid = fpp.cid AND cl.id = fpp.lease_id )
							JOIN lease_intervals li ON ( li.cid = cl.cid AND li.lease_id = cl.id AND li.lease_status_type_id IN ( ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ' ) )
							JOIN customers c ON ( c.id = fpp.customer_id AND c.cid = fpp.cid )
						WHERE
							rip.end_date IS NULL
							AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypeIds ) . ' )
							AND fpp.is_disabled = FALSE
							AND fpp.policy_effective_date::DATE <= CURRENT_DATE::DATE
							AND fpp.enrolled_on IS NOT NULL
							AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
							AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
							AND (
									CASE
										WHEN li.lease_end_date IS NULL THEN TRUE
										ELSE li.lease_end_date::DATE > ( date_trunc ( \'month\', CURRENT_DATE ) + INTERVAL \'1 month - 1 day\' )::DATE
									END
								)
					) AS sub_query
					WHERE
					latest_lease_interval = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByResidentInsurancePolicyIdsByCids( $arrintResidentInsurancePolicyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintResidentInsurancePolicyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						forced_placed_policies
					WHERE
						resident_insurance_policy_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public function fetchAllForcedPlacedPolicyByPolicyTypeIdsByLeaseIdByCid( $arrintInsurancePolicyTypeIds, $intLeaseId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintInsurancePolicyTypeIds ) ) return NULL;

		$strSql = 'SELECT
						fpp.*
					FROM
						forced_placed_policies fpp
						LEFT JOIN resident_insurance_policies rip ON ( fpp.resident_insurance_policy_id = rip.id AND fpp.property_id = rip.property_id AND fpp.cid = rip.cid )
					WHERE
						fpp.is_disabled = false
						AND fpp.lease_id = ' . ( int ) $intLeaseId . '
						AND ( rip.id IS NULL OR ( rip.id = fpp.resident_insurance_policy_id AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypeIds ) . ' ) ) )
						AND fpp.cid = ' . ( int ) $intCid;

		return self::fetchForcedPlacedPolicy( $strSql, $objDatabase );
	}

	public static function fetchMoveOutForcedPlacedPoliciesByPolicyTypeIdsByCids( $arrintInsurancePolicyTypeIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintInsurancePolicyTypeIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						fpp.*
					FROM
						forced_placed_policies fpp
						JOIN resident_insurance_policies rip ON ( rip.cid = fpp.cid AND rip.id = fpp.resident_insurance_policy_id )
						JOIN lease_customers lc ON ( lc.cid = fpp.cid AND lc.lease_id = fpp.lease_id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::PAST . ' , ' . CLeaseStatusType::CANCELLED . ' ) )
						LEFT JOIN lease_customers lc1 ON ( lc.cid = lc1.cid AND lc.lease_id = lc1.lease_id AND lc1.customer_type_id = ' . CCustomerType::PRIMARY . ' AND lc1.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ' )
					WHERE
						fpp.is_disabled = FALSE
						AND rip.insurance_policy_status_type_id = ' . CInsurancePolicyStatusType::ACTIVE . '
						AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypeIds ) . ' )
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND lc1.id IS NULL';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchActiveForcedPlacedPolicyByResidentInsurancePolicyIdByCid( $intResidentInsuancePolicyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						forced_placed_policies
					WHERE
						is_disabled = false
						AND resident_insurance_policy_id = ' . ( int ) $intResidentInsuancePolicyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchForcedPlacedPolicy( $strSql, $objDatabase );
	}

	public static function fetchActiveForcedPlacedPoliciesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase, $boolIsBlanketProgram = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSubSql = ( true == $boolIsBlanketProgram ) ? ' AND rip.insurance_policy_type_id IN ( ' . implode( ',', [ CInsurancePolicyType::BLANKET_BASIC, CInsurancePolicyType::BLANKET_ENHANCED ] ) . ' ) ':'AND rip.insurance_policy_type_id = ' . CInsurancePolicyType::FORCED_PLACED_BASIC;

		$strSql = 'SELECT
                        fpp.*
                    FROM
                       forced_placed_policies fpp
					   LEFT JOIN resident_insurance_policies rip ON ( fpp.cid = rip.cid AND fpp.property_id = rip.property_id AND rip.id = fpp.resident_insurance_policy_id )
                    WHERE
                        fpp.is_disabled = FALSE
						' . $strSubSql . '
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
                        AND fpp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchActiveMasterPoliciesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intPropertyId ) || true == is_null( $intCid ) ) return NULL;

		$strSql = 'SELECT
                        fpp.*
                    FROM
                       forced_placed_policies fpp
					   LEFT JOIN resident_insurance_policies rip ON ( fpp.cid = rip.cid AND fpp.property_id = rip.property_id AND rip.id = fpp.resident_insurance_policy_id )
                    WHERE
                        fpp.is_disabled = FALSE
						AND rip.property_id = ' . ( int ) $intPropertyId . '
						AND rip.cid = ' . ( int ) $intCid;

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchLeaseTransferedBlanketPoliciesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
                        fpp.*
                    FROM
                       forced_placed_policies fpp
					   JOIN lease_processes lp ON ( lp.cid = fpp.cid AND lp.lease_id = fpp.lease_id AND lp.customer_id = fpp.customer_id )
					WHERE
                        fpp.is_disabled = FALSE
                        AND lp.transfer_lease_id IS NOT NULL
                        AND lp.transferred_on IS NOT NULL
                        AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
                        AND fpp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByIdsByCids( $arrintIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						forced_placed_policies
					WHERE
						 id IN (' . implode( ',', $arrintIds ) . ' )
					 	 AND cid IN (' . implode( ',', $arrintCids ) . ' )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						lc.lease_status_type_id,
						fpp.*
					FROM
						forced_placed_policies fpp
						JOIN lease_customers lc ON ( lc.cid = fpp.cid AND lc.lease_id = fpp.lease_id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
					WHERE
						fpp.resident_insurance_policy_id IS NULL
						AND fpp.is_disabled = FALSE
						AND fpp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND fpp.policy_effective_date::DATE <= CURRENT_DATE::DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveMasterPoliciesAndFirstNotices( $arrintTestCids, $objDatabase ) {

		if( false == valArr( $arrintTestCids ) ) return NULL;

		$strSql = 'SELECT
						rip.insurance_policy_type_id,
						fpp.*
					FROM
						forced_placed_policies fpp
						JOIN clients c ON( c.id = fpp.cid AND c.company_status_type_id != ' . CCompanyStatusType::CLIENT . ' )
						LEFT JOIN resident_insurance_policies rip ON ( rip.cid = fpp.cid AND rip.property_id = fpp.property_id AND rip.id = fpp.resident_insurance_policy_id )
					WHERE
						fpp.is_disabled = false
						AND fpp.cid NOT IN( ' . implode( ',', $arrintTestCids ) . ' )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchFutureForcedPlacedPoliciesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						lc.lease_status_type_id,
						fpp.*
					FROM
						forced_placed_policies fpp
						JOIN lease_customers lc ON ( lc.cid = fpp.cid AND lc.lease_id = fpp.lease_id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
					WHERE
						fpp.is_disabled = FALSE
						AND fpp.resident_insurance_policy_id IS NOT NULL
						AND fpp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND fpp.policy_effective_date::DATE > CURRENT_DATE::DATE';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByArTransactionIdsByPropertyIdByCid( $arrintArTransactionIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArTransactionIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT *
							 FROM
								  forced_placed_policies
						 WHERE
						 CID = ' . ( int ) $intCid . '
						 AND property_id = ' . ( int ) $intPropertyId . '
						 AND ar_transaction_id IN ( ' . implode( ',', $arrintArTransactionIds ) . ' )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchNonRenewedActivePoliciesByCids( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						rip.insurance_policy_type_id,
						fpp.*
					FROM
						forced_placed_policies fpp
						JOIN resident_insurance_policies rip ON ( fpp.resident_insurance_policy_id = rip.id AND fpp.property_id = rip.property_id AND fpp.cid = rip.cid )
					WHERE
						fpp.policy_end_date < CURRENT_DATE
						AND fpp.is_disabled = false
						AND fpp.cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchActiveForcedPlacedPolicyByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						forced_placed_policies
					WHERE
						is_disabled = false
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchForcedPlacedPolicy( $strSql, $objDatabase );
	}

	public static function fetchCustomForcedPlacedPoliciesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.insurance_policy_type_id,
					    fpp.*
					FROM
					    forced_placed_policies fpp
					    LEFT JOIN resident_insurance_policies rip ON ( fpp.resident_insurance_policy_id = rip.id AND fpp.property_id = rip.property_id AND fpp.cid = rip.cid )
					WHERE
					    fpp.is_disabled = FALSE
					    AND fpp.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					    AND fpp.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' ) ';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchMasterPoliciesByPolicyTypeIdsByPropertyPreferenceByCids( $arrintInsurancePolicyTypeIds, $arrstrPreferenceKeys, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintInsurancePolicyTypeIds ) || false == valArr( $arrstrPreferenceKeys ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
					   rip.insurance_policy_type_id,
                       fpp.*
                   FROM
                       forced_placed_policies fpp
                       JOIN resident_insurance_policies rip ON ( rip.cid = fpp.cid AND rip.id = fpp.resident_insurance_policy_id AND rip.insurance_policy_type_id IN ( ' . sqlIntImplode( $arrintInsurancePolicyTypeIds ) . ' ) )
                       JOIN insurance_policy_customers ipc ON ( ipc.cid = fpp.cid AND ipc.property_id = fpp.property_id AND ipc.lease_id = fpp.lease_id AND rip.id = ipc.resident_insurance_policy_id )
                       LEFT JOIN property_preferences pp on( pp.cid = fpp.cid and fpp.property_id = pp.property_id and pp.key IN( ' . sqlStrImplode( $arrstrPreferenceKeys ) . ' ) )
                   WHERE
                       fpp.is_disabled = FALSE
                       AND ( ( insurance_policy_type_id IN( ' . CInsurancePolicyType::FORCED_PLACED_BASIC . ' ) AND (  pp.key IS NULL OR pp.key != \'' . CPropertyPreference::REQUIRE_FORCE_PLACED_POLICIES . '\' ) ) OR
    	                    ( insurance_policy_type_id IN ( ' . sqlIntImplode( CInsurancePolicyType::$c_arrintBlanketPolicyTypes ) . ' ) AND (  pp.key IS NULL OR pp.key != \'' . CPropertyPreference::REQUIRE_BLANKET_POLICIES . '\' ) ) )
                       AND fpp.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' ) ';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

	public static function fetchActiveForcedPlacedPolicyByLeaseIdsByCids( $arrintLeaseIdsByCids, $objDatabase ) {

		if( false == valArr( $arrintLeaseIdsByCids ) ) {
			return [];
		}

		$strSql = 'CREATE TEMP TABLE temp_active_mp_policy_leases( cid, lease_id) ON COMMIT DROP AS ( values ' . sqlIntMultiImplode( $arrintLeaseIdsByCids ) . ' );';

		$strSql .= 'SELECT
						fpp.*
					FROM
						forced_placed_policies fpp
					WHERE
						is_disabled = false
						AND (lease_id, cid) IN ( SELECT lease_id, cid FROM temp_active_mp_policy_leases )';

		return self::fetchForcedPlacedPolicies( $strSql, $objDatabase );
	}

}

?>