<?php

class CFileEvent extends CBaseFileEvent {

	const FILE_VERIFICATION_STATUS_UPLOADED = 'UPLOADED';
	const FILE_VERIFICATION_STATUS_RE_UPLOADED = 'RE_UPLOADED';
	const FILE_VERIFICATION_STATUS_PENDING  = 'PENDING';
	const FILE_VERIFICATION_STATUS_VERIFIED = 'VERIFIED';
	const FILE_VERIFICATION_STATUS_REJECTED = 'REJECTED';
	const FILE_VERIFICATION_STATUS_INCOMPLETE = 'INCOMPLETE';
	const FILE_VERIFICATION_STATUS_APPROVED = 'APPROVED';
	const FILE_VERIFICATION_STATUS_EXPIRED = 'EXPIRED';

	protected $m_strEventTypeName;
	protected $m_strCompanyUserName;

    public function setEventTypeName( $strEventTypeName ) {
    	$this->m_strEventTypeName = $strEventTypeName;
    }

    public function getEventTypeName() {
    	return $this->m_strEventTypeName;
    }

    public function setCompanyUserName( $strCompanyUserName ) {
    	$this->m_strCompanyUserName = $strCompanyUserName;
    }

    public function getCompanyUserName() {
    	return $this->m_strCompanyUserName;
    }

    public function getEventDescriptionList() {
    	$arrstrEventDescription = json_decode( $this->getEventDescription() );
		return ( array ) $arrstrEventDescription;
    }

	public function getEventDisplay( $arrmixCustomVariables = NULL ) {

		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES, false );

		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		$strDisplayDataKey = 'display_data';

		if( true == valArr( $arrstrDetails[$strDisplayDataKey] ) && \Psi\Libraries\UtilStrings\CStrings::createService()->endsWith( $arrstrDetails[$strDisplayDataKey]['_template'], '.tpl' ) ) {
			$objSmarty->assign( 'event', $this );
			$objSmarty->assign( 'details', $arrstrDetails );
			$objSmarty->assign( 'full_path', PATH_INTERFACES_ENTRATA );
			$objSmarty->assign( 'custom_variables', $arrmixCustomVariables );
			return $objSmarty->fetch( PATH_INTERFACES_ENTRATA . $arrstrDetails[$strDisplayDataKey]['_template'] );
		} else {
			return $this->m_strEventDescription;
		}
	}

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false );

    	if( isset( $arrstrValues['event_type_name'] ) && $boolDirectSet ) {
    		$this->m_strEventTypeName = trim( $arrstrValues['event_type_name'] );
    	} elseif( isset( $arrstrValues['event_type_name'] ) ) {
    		$this->setEventTypeName( $arrstrValues['event_type_name'] );
    	}

    	if( isset( $arrstrValues['company_user_name'] ) && $boolDirectSet ) {
    		$this->m_strCompanyUserName = trim( $arrstrValues['company_user_name'] );
    	} elseif( isset( $arrstrValues['company_user_name'] ) ) {
    		$this->setCompanyUserName( $arrstrValues['company_user_name'] );
    	}

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>