<?php

class CFieldValue extends CBaseFieldValue {

	protected $m_strValue;

	public function getValue() {
		return $this->m_strValue;
	}

	public function setValue( $strValue ) {
		$this->m_strValue = $strValue;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFieldTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFieldId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFieldOptionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>