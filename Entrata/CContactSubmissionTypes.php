<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactSubmissionTypes
 * Do not add any new functions to this class.
 */

class CContactSubmissionTypes extends CBaseContactSubmissionTypes {

	public static function fetchContactSubmissionTypesByCid( $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						*
					FROM
						contact_submission_types cst

					WHERE
						cst.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchContactSubmissionTypes( $strSql, $objDatabase );
	}

	public static function fetchContactSubmissionTypesByNameByCid( $strName, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM contact_submission_types cst WHERE cst.name = \'' . $strName . '\' AND cst.cid = ' . ( int ) $intCid;

		return self::fetchContactSubmissionTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedContactSubmissionTypesByCid( $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						*
					FROM
						contact_submission_types cst

					WHERE
						cst.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_published = 1';

		return self::fetchContactSubmissionTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedContactSubmissionTypesDefaultIdByCid( $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						cst.id,
						util_get_translated( \'name\', cst.name, cst.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name,
						dcst.id as default_contact_type_id
					FROM
						contact_submission_types cst
						LEFT JOIN default_contact_submission_types dcst ON cst.name = dcst.name

					WHERE
						cst.cid = ' . ( int ) $intCid . '
						AND cst.deleted_by IS NULL
						AND cst.deleted_on IS NULL
						AND cst.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchSimplePublishedContactSubmissionTypesByCid( $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						id, name
					FROM
						contact_submission_types cst

					WHERE
						cst.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchContactSubmissionTypesNameByIdCid( $intId, $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						name
					FROM
						contact_submission_types cst

					WHERE
						cst.id = ' . ( int ) $intId . '
						AND cst.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_published = 1';

		$arrstrName = fetchData( $strSql, $objDatabase );
		return ( true == isset ( $arrstrName[0]['name'] ) ) ? $arrstrName[0]['name'] : '';
	}

	public static function fetchContactSubmissionTypeIdByNameByCid( $strName, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						id
					FROM
						contact_submission_types cst

					WHERE
						cst.name = \'' . $strName . '\'
						AND cst.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_published = 1
						limit 1 ';

		$arrstrName = fetchData( $strSql, $objDatabase );
		return ( true == isset ( $arrstrName[0]['id'] ) ) ? $arrstrName[0]['id'] : '';
	}

}
?>