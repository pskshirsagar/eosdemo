<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApprovalPreferences
 * Do not add any new functions to this class.
 */

class CApprovalPreferences extends CBaseApprovalPreferences {

	public static function fetchApprovalPreferenceByRouteTypeIdByCid( $intRouteTypeId, $intCid, $objDatabase ) {

		$objApprovalPreference = self::fetchApprovalPreference( sprintf( 'SELECT * FROM approval_preferences WHERE route_type_id = %d AND cid = %d', ( int ) $intRouteTypeId, ( int ) $intCid ), $objDatabase );

		if( false == valObj( $objApprovalPreference, 'CApprovalPreference' ) ) {
			$objApprovalPreference = new CApprovalPreference();
			$objApprovalPreference->setCid( $intCid );
		}

		return $objApprovalPreference;
	}

	public static function fetchApprovalPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchApprovalPreferences( sprintf( 'SELECT * FROM approval_preferences WHERE cid = %d ORDER BY route_type_id', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIsAdvancedRoutingApprovalPreferenceByRouteTypeIdByCid( $intRouteTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						is_enabled
					FROM
						approval_preferences
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND route_type_id = ' . ( int ) $intRouteTypeId;

		return ( 't' == self::fetchColumn( $strSql, 'is_enabled', $objDatabase ) );
	}

	public static function fetchSimpleIsAdvancedRoutingApprovalPreferencesByRouteTypeIdsByCid( $arrintRouteTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						route_type_id,
						is_enabled
					FROM
						approval_preferences
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND is_enabled = true
						AND route_type_id IN ( ' . implode( ',', $arrintRouteTypeIds ) . ' )';

		$arrboolRoutingPreferences = [];
		$arrmixApprovalPreferences = fetchData( $strSql, $objDatabase );
		foreach( $arrmixApprovalPreferences as $arrmixApprovalPreference ) {
			$arrboolRoutingPreferences[$arrmixApprovalPreference['route_type_id']] = $arrmixApprovalPreference['is_enabled'];
		}

		return $arrboolRoutingPreferences;
	}

	public static function fetchAdvancedRoutingApprovalPreferencesByRouteTypeId( $intRouteTypeId, $objDatabase ) {

		$strSql = 'SELECT 
						*
					FROM 
						approval_preferences
					WHERE 
						route_type_id = ' . ( int ) $intRouteTypeId . '
						AND is_enabled
						AND send_notification_emails
					ORDER BY
						cid';

		return self::fetchApprovalPreferences( $strSql, $objDatabase );
	}

	public static function fetchCustomApprovalPreferenceDetailsByRouteTypeIdByPropertyIdsByCid( int $intRouteTypeId, array $arrintPropertyIds, int $intCid, CDatabase $objDatabase ) {

		if( false == valIntArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						ap.cid,
						ap.is_enabled,
						pr.property_id,
						CASE WHEN r.id IS NOT NULL THEN 1 ELSE 0 END AS has_route
					FROM
						approval_preferences ap 
						LEFT JOIN property_routes pr ON ( pr.cid = ap.cid AND pr.route_type_id = ' . ( int ) $intRouteTypeId . ' AND pr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						LEFT JOIN routes r ON ( r.cid = pr.cid AND r.id = pr.route_id AND r.is_published = 1 )
						LEFT JOIN property_gl_settings pgs ON ( pr.cid = pr.cid AND pr.id = pgs.property_id AND pgs.activate_standard_posting = true )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND ap.is_enabled = true';

		return fetchData( $strSql, $objDatabase );
	}

}
?>