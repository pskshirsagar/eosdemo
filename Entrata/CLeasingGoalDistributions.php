<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoalDistributions
 * Do not add any new functions to this class.
 */

class CLeasingGoalDistributions extends CBaseLeasingGoalDistributions {

	public static function fetchPropertyLeasingGoalDistributionsByGoalIdByWindowIdsByPropertyIdByCid( $intGoalId, $arrintSelectedLeaseStartWindowIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intGoalId ) || false == valArr( $arrintSelectedLeaseStartWindowIds ) ) return [];

		$strSql = 'SELECT
					    sub.goal_date,
					    sub.term_month,
					    sub.month_year,
					    sub.is_renewal,
					    sub.total_count,
					    sub.id,
					    sub.cid,
					    sub.property_id,
					    ( date_trunc ( \'MONTH\', sub.goal_date::DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE <= CURRENT_DATE as is_disabled,
					    CASE
					      WHEN ( date_trunc ( \'MONTH\', CURRENT_DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE >= sub.goal_date::DATE THEN COALESCE( li.approved_interval_count, 0 )::VARCHAR
					      ELSE \'N/A\'
					    END AS actual_count
					FROM
						(
							SELECT
								CASE
								    WHEN to_char ( DATE ( lg.pre_lease_start_date ), \'mm/YYYY\' ) = to_char ( DATE ( lgd.goal_date ), \'mm/YYYY\' ) THEN to_char ( DATE ( lg.pre_lease_start_date ), \'mm/dd/YYYY\' )
								    WHEN to_char ( DATE ( lg.pre_lease_end_date ), \'mm/YYYY\' ) = to_char ( DATE ( lgd.goal_date ), \'mm/YYYY\' ) THEN to_char ( DATE ( lg.pre_lease_end_date ), \'mm/dd/YYYY\' )
								    ELSE to_char ( DATE ( lgd.goal_date ), \'mm/01/YYYY\' )
								END AS goal_date,
							    to_char ( DATE ( lgd.goal_date ), \'mm/01/YYYY\' ) AS term_month,
							    to_char ( DATE ( lgd.goal_date ), \'mm/YYYY\' ) AS month_year,
							    CASE WHEN lgd.is_renewal IS true THEN 1 ELSE 0 END AS is_renewal,
							    sum ( lgd.goal_count ) AS total_count,
							    lg.id,
							    lg.cid,
							    lg.property_id,
							    lg.pre_lease_start_date,
							    lg.pre_lease_end_date
							FROM
							    leasing_goals lg
							    JOIN leasing_goal_distributions lgd ON ( lgd.cid = lg.cid AND lgd.leasing_goal_id = lg.id )
							WHERE
							    lg.cid = ' . ( int ) $intCid . '
							    AND lg.id = ' . ( int ) $intGoalId . '
							    AND lg.property_id = ' . ( int ) $intPropertyId . '
							    AND lg.deleted_by IS NULL
								AND lg.deleted_on IS NULL
							GROUP BY
							    term_month,
							    month_year,
							    lgd.is_renewal,
							    lg.id,
							    lg.cid,
							    lg.property_id
						) sub
						LEFT JOIN LATERAL 
						(
							SELECT
								li.cid,
								li.property_id,
								li.lease_interval_type_id,
								sum( li.approved_interval_count ) AS approved_interval_count
							FROM(
							      SELECT
									li.cid,
									li.property_id,
									li.lease_interval_type_id,
									CASE
										WHEN lc.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND a.unit_space_id IS NULL AND 0 < a.desired_space_configuration_id THEN 
										(
											SELECT
												sc.unit_space_count
											FROM
												space_configurations sc
											WHERE
												sc.cid = a.cid
												AND sc.id = a.desired_space_configuration_id
												AND sc.deleted_by IS NULL 
												AND sc.deleted_on IS NULL
										)
										WHEN lc.lease_status_type_id >= ' . CLeaseStatusType::FUTURE . ' AND a.unit_space_id IS NOT NULL THEN 
										(
											SELECT
												COUNT( * ) as unit_space_count
											FROM
												lease_unit_spaces lus
											WHERE
												lus.cid = li.cid                    
												AND lus.property_id = li.property_id
												AND lus.unit_space_id IS NOT NULL
												AND lus.lease_id = li.lease_id
												AND lus.deleted_by IS NULL 
												AND lus.deleted_on IS NULL
											GROUP BY
												lus.lease_id
										)               
										ELSE 1
									END AS approved_interval_count
							      FROM
							          lease_intervals li
							          JOIN lease_customers lc ON ( lc.cid = li.cid AND lc.property_id = li.property_id AND lc.lease_id = li.lease_id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
							          JOIN applications a ON ( a.cid = li.cid AND a.property_id = li.property_id AND a.lease_interval_id = li.id AND a.lease_id = li.lease_id )
							      WHERE
							          li.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::APPLICATION . ' )
							          AND lc.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ' )
							          AND li.lease_start_window_id IN ( ' . implode( ', ', $arrintSelectedLeaseStartWindowIds ) . ' )
							          AND a.lease_approved_on IS NOT NULL
							          AND li.lease_id IS NOT NULL
							          AND li.cid = sub.cid
							          AND li.property_id = sub.property_id
							          AND a.lease_approved_on <= ( date_trunc ( \'MONTH\', CURRENT_DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE 
							          AND CASE
							                WHEN sub.goal_date::DATE = sub.pre_lease_start_date THEN
							                    ( a.lease_approved_on >= sub.goal_date::DATE AND a.lease_approved_on <= ( date_trunc ( \'MONTH\', sub.goal_date::DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE )
							                WHEN sub.goal_date::DATE = sub.pre_lease_end_date THEN
							                    ( a.lease_approved_on >= sub.term_month::DATE AND a.lease_approved_on <= sub.goal_date::DATE )
							                ELSE
							                    ( a.lease_approved_on >= sub.goal_date::DATE AND a.lease_approved_on <= ( date_trunc ( \'MONTH\', sub.goal_date::DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE )
							              END
							          AND CASE
							                WHEN ( sub.is_renewal::BOOLEAN IS TRUE ) THEN li.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
							                WHEN ( sub.is_renewal::BOOLEAN IS FALSE ) THEN li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
							                ELSE TRUE
							              END
								) AS li
								GROUP BY
								  li.cid,
								  li.property_id,
								  li.lease_interval_type_id
						) li ON TRUE
					GROUP BY
						sub.goal_date,
						sub.term_month,
						sub.month_year,
						sub.is_renewal,
						sub.total_count,
						sub.id,
						sub.cid,
						sub.property_id,
						li.approved_interval_count
					ORDER BY
						to_timestamp ( to_char ( DATE ( sub.goal_date ), \'mm/01/YYYY\' ), \'mm/01/YYYY\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeasingGoalDistributionsByStartDateByEndDateByGoalIdByTypeByPropertyIdByCid( $strStartDate, $strEndDate, $intLeasingGoalId, $boolIsRenewal, $intPropertyId, $intCid, $objDatabase, $intPropertyFloorPlanId = NULL ) {

		$strCondition = '';
		if( false == is_null( $intPropertyFloorPlanId ) ) {
			$strCondition = ' AND lgd.property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId;
		}

		$strSql = 'SELECT
					    *
					FROM
					    leasing_goals lg
					    JOIN leasing_goal_distributions lgd ON ( lgd.cid = lg.cid AND lgd.leasing_goal_id = lg.id )
					WHERE
					    lg.cid = ' . ( int ) $intCid . '
					    AND lg.id = ' . ( int ) $intLeasingGoalId . '
					    AND lg.property_id = ' . ( int ) $intPropertyId . $strCondition . '
					    AND lgd.is_renewal = CAST ( ' . ( int ) $boolIsRenewal . ' AS BOOLEAN )
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )
					    AND lg.deleted_by IS NULL
						AND lg.deleted_on IS NULL';

		return self::fetchLeasingGoalDistributions( $strSql, $objDatabase );
	}

	public static function deleteLeasingGoalsByLeasingGoalIdByStartDateByEndDateByIsRenewalByCid( $intLeasingGoalId, $strStartDate, $strEndDate, $boolIsRenewal, $intCid, $objDatabase, $boolIsReturnSqlOnly = false, $intPropertyFloorPlanId = NULL ) {

		if( false == valId( $intLeasingGoalId ) ) return NULL;

		$strAndCondition = ( false == is_null( $intPropertyFloorPlanId ) ) ? ' AND property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId : ' AND property_floorplan_id IS NULL';

		$strSql = 'DELETE
					FROM
						leasing_goal_distributions 
					WHERE
					    cid = ' . ( int ) $intCid . '    
					    AND leasing_goal_id = ' . ( int ) $intLeasingGoalId . '
					    AND is_renewal = CAST ( ' . ( int ) $boolIsRenewal . ' AS BOOLEAN ) '
						. $strAndCondition . '
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )';

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		fetchData( $strSql, $objDatabase );
	}

	public static function buildMonthlyLeasingGoalDistributionDetailsByCidByPropertyIdByLeasingGoalId( $intCid, $intPropertyId, $intLeasingGoalId ) {
		$strSql = ' DROP TABLE IF EXISTS leasing_goal_monthly_details;
					create temp table leasing_goal_monthly_details as(
						SELECT
							MAX(lgd.goal_date ) as max_date, 
							MIN(lgd.goal_date ) as min_date, 
							util_get_translated( \'name\', lg.name, lg.details ) As goal_name,
							lgd.cid,
							lg.property_id,
							lgd.leasing_goal_id,
							lg.pre_lease_start_date,
							lg.pre_lease_end_date,
							sum(lgd.goal_count) as monthly_goal_count,
							to_char( DATE ( lgd.goal_date ), \'Mon\' ) AS month,
							to_char( DATE ( lgd.goal_date ), \'YYYY\' ) AS year                
						FROM
							leasing_goals lg 
							JOIN leasing_goal_distributions lgd ON ( lg.cid = lgd.cid AND lg.id = lgd.leasing_goal_id )
					    WHERE
							lg.cid = ' . ( int ) $intCid . '
							AND lg.property_id = ' . ( int ) $intPropertyId . ' 
							AND lgd.leasing_goal_id = ' . ( int ) $intLeasingGoalId . '
							AND lg.deleted_by IS NULL
						GROUP BY
							lgd.leasing_goal_id,
							lgd.cid,
							lg.property_id,
							year,
							month,
							lg.pre_lease_start_date,
							lg.pre_lease_end_date,
							lg.name,
							lg.details         
					);
					DROP TABLE IF EXISTS leasing_goals_details_temp;
					create temp table leasing_goals_details_temp AS (
						SELECT 
							sub.goal_date,
							sub.cid,
							sub.property_id,
							sub.leasing_goal_id,
							sub.goal_name,
							sub.max_date,
							sub.min_date,
							sub.pre_lease_start_date,
							sub.pre_lease_end_date,
							sub.pre_max_date,
							CASE WHEN sub.total_goal_count = 0 THEN 0 ELSE 
								round((SUM( sub.goal_count )OVER( ORDER BY sub.goal_date asc rows between unbounded
									preceding and current row )*100.0)/sub.total_goal_count )
							END as leasing_goal_percentage
						FROM(
							SELECT
								DISTINCT lgd.goal_date,
								lgt.cid,
								lgt.property_id,
								lgt.leasing_goal_id,
								SUM( lgd.goal_count ) as goal_count,
								total.total_goal_count,
								lgt.goal_name,
								lgt.max_date,
								lgt.min_date,
								lgt.pre_lease_start_date,
								lgt.pre_lease_end_date,
								lag( lgt.max_date ) OVER ( PARTITION BY lgt.cid, lgt.property_id, lgt.leasing_goal_id ORDER BY lgd.goal_date ) as pre_max_date
							FROM
								leasing_goal_monthly_details lgt
								JOIN (
									SELECT
										SUM( lgt.monthly_goal_count ) as total_goal_count,
										lgt.cid,
										lgt.property_id,
										lgt.leasing_goal_id
									FROM
										leasing_goal_monthly_details lgt
									GROUP BY
										lgt.cid,
										lgt.property_id,
										lgt.leasing_goal_id
								) as total ON (lgt.cid = total.cid AND lgt.property_id = total.property_id AND lgt.leasing_goal_id = total.leasing_goal_id )
								JOIN leasing_goal_distributions lgd ON ( lgt.cid = lgd.cid AND lgt.leasing_goal_id = lgd.leasing_goal_id AND to_char( DATE ( lgd.goal_date ), \'Mon\' ) = lgt.month AND to_char( DATE ( lgd.goal_date ), \'YYYY\' ) = lgt.year )		
							GROUP BY
								lgt.cid,
								lgt.property_id,
								lgt.leasing_goal_id,
								lgt.goal_name,
								lgd.goal_date,
								lgt.max_date,
								lgt.min_date,
								total.total_goal_count,
								lgt.pre_lease_start_date,
								lgt.pre_lease_end_date,
								lgt.month, 
								lgt.year
							ORDER BY
								lgd.goal_date  
						) as sub
					);';

		return $strSql;
	}

	public static function fetchLeasingGoalsDistributionsVsActualsByFilterByCid( $arrmixLeasingGoalsFilter, $intCid, $objDatabase ) {

		if( CFrequency::WEEKLY == $arrmixLeasingGoalsFilter['frequency_id'] ) {
			$strTempTableSql = \Psi\Eos\Entrata\CLeasingGoalDistributions::createService()->buildWeeklyLeasingGoalDistributionDetailsByCidByPropertyIdByLeasingGoalId( $intCid, $arrmixLeasingGoalsFilter['property_id'], $arrmixLeasingGoalsFilter['leasing_goal_id'] );
			$strSelectSql    = ' sub2.min_date AS goal_date, to_char( DATE ( sub2.min_date ), \'Mon DD\' ) AS date_label, ( COALESCE( sub2.week_end_date, sub2.pre_lease_end_date + \'1 day\'::INTERVAL ) - \'1 day\'::INTERVAL ) AS week_end_date, ';
			$strWhereSql     = ' sub2.goal_actuals_rank = 1
                                 AND sub2.max_date = sub2.goal_date; ';
		} else {
			$strTempTableSql = self::buildMonthlyLeasingGoalDistributionDetailsByCidByPropertyIdByLeasingGoalId( $intCid, $arrmixLeasingGoalsFilter['property_id'], $arrmixLeasingGoalsFilter['leasing_goal_id'] );
			$strSelectSql    = ' sub2.goal_date, sub2.date_label, ';
			$strWhereSql     = ' sub2.goal_actuals_rank = 1; ';
		}

		fetchData( $strTempTableSql, $objDatabase );

		$strGoalActualsSql = 'SELECT 
						sub2.goal_name,
						' . $strSelectSql . '
						sub2.leasing_goal_id,
						sub2.leasing_goal_percentage,
						sub2.goal_actauls_percentage                    
					FROM (
						SELECT 
							sub1.goal_name,
							sub1.goal_date,
							sub1.min_date,
							sub1.max_date,
							LEAD( min_date ) OVER ( ORDER BY min_date ) AS week_end_date,
							sub1.pre_lease_end_date,
							sub1.date_label,
							sub1.leasing_goal_id,
							sub1.leasing_goal_percentage,
							COALESCE( sub1.goal_actauls_percentage, 0 ) as goal_actauls_percentage,
							dense_rank() OVER ( PARTITION BY sub1.goal_date ORDER BY sub1.goal_actauls_percentage desc ) AS goal_actuals_rank 
						FROM (
							SELECT
								DISTINCT lgt.goal_name,
								lgt.goal_date,
								to_char( DATE ( lgt.goal_date ), \'Mon DD\' ) AS date_label,
								lgt.leasing_goal_id,
								lgt.property_id,
								lgt.cid,
								lgt.leasing_goal_percentage,
								lgt.max_date,
								lgt.min_date,
								lgt.pre_lease_start_date,
								lgt.pre_lease_end_date,
								lgt.pre_max_date,
								round((SUM(goal_actuals.unit_space_count)OVER( ORDER BY lgt.goal_date, goal_actuals.lease_approved_on asc rows between unbounded
								preceding and current row )*100.0)/' . ( int ) $arrmixLeasingGoalsFilter['total_unit_spaces_count'] . ' ) as goal_actauls_percentage	
							FROM
								leasing_goals_details_temp lgt
								LEFT JOIN LATERAL (
									SELECT
										DISTINCT ca.id as application_id,
										ca.lease_approved_on::DATE,
										lgt.goal_date,
										 CASE
											WHEN ca.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NULL AND 0 < ca.desired_space_configuration_id THEN 
											(
											    SELECT
											        sc.unit_space_count
											    FROM
											        space_configurations sc
											    WHERE
											        sc.cid = ca.cid
											        AND sc.id = ca.desired_space_configuration_id
											        AND sc.deleted_by IS NULL 
											        AND sc.deleted_on IS NULL
											)
											WHEN ca.lease_status_type_id >= ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NOT NULL THEN
											(
											  SELECT
											      COUNT( * ) as unit_space_count
											  FROM
											      lease_unit_spaces lus
											  WHERE
											      lus.cid = ca.cid                    
											      AND lus.property_id = ca.property_id
											      AND lus.unit_space_id IS NOT NULL
											      AND lus.lease_id = ca.lease_id
											      AND lus.deleted_by IS NULL 
											      AND lus.deleted_on IS NULL
											  GROUP BY
											      lus.lease_id
											)
											ELSE 1              
										END AS unit_space_count
									FROM  
										cached_applications ca
									WHERE
										ca.cid = lgt.cid 
										AND ca.property_id = lgt.property_id 
										AND ca.lease_approved_on IS NOT NULL 
										AND ca.lease_start_window_id IN( ' . implode( ',', $arrmixLeasingGoalsFilter['goal_window_ids'] ) . ' )
										AND ( CASE WHEN 
												ca.lease_approved_on::DATE BETWEEN ( lgt.min_date + INTERVAL \'1 day\' ) AND lgt.max_date THEN
												ca.lease_approved_on::DATE = lgt.goal_date
											WHEN lgt.pre_lease_start_date = lgt.min_date AND lgt.min_date = lgt.goal_date THEN
												ca.lease_approved_on::DATE <= lgt.min_date
											WHEN lgt.min_date = lgt.goal_date THEN
												ca.lease_approved_on::DATE BETWEEN ( lgt.pre_max_date + INTERVAL \'1 day\' ) AND lgt.min_date
										END )
										AND ca.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ' )
										AND ca.lease_interval_type_id IN( ' . CLeaseIntervalType::APPLICATION . ',' . CLeaseIntervalType::RENEWAL . ' ) 
									ORDER BY
										lgt.goal_date,
										ca.lease_approved_on::DATE
								) AS goal_actuals ON TRUE 
							) as sub1 
						) as sub2
					WHERE ' . $strWhereSql;

		$arrmixLeasingGoalsVsActualsResult['goal_actuals'] = fetchData( $strGoalActualsSql, $objDatabase );

		if( true == array_key_exists( 'previous_year_lease_term_ids', $arrmixLeasingGoalsFilter ) && true == valStr( $arrmixLeasingGoalsFilter['previous_year_lease_term_ids'] ) ) {
			$arrmixLeasingGoalsVsActualsResult['previous_actuals'] = self::fetchPreviousYearActualsDataByLeasingGoalsFilter( $arrmixLeasingGoalsFilter, $objDatabase );
		}

		return $arrmixLeasingGoalsVsActualsResult;
	}

	public static function fetchPreviousYearActualsDataByLeasingGoalsFilter( $arrmixLeasingGoalsFilter, $objDatabase ) {

		$strPreviousActualsSql = 'SELECT 
								sub2.goal_date,
								sub2.leasing_goal_id,
								array_to_string( array_agg( DISTINCT sub2.prev_year_window_id ORDER BY sub2.prev_year_window_id ), \',\' ) as prev_year_window_ids,
								array_to_string( array_agg( sub2.previous_actuals_percentage ORDER BY sub2.prev_year_window_id ), \',\' ) as previous_actuals_percentage,
								array_to_string( array_agg( sub2.lease_term_name ), \'|\' ) as lease_term_names,
								array_to_string( array_agg( sub2.start_date ), \'|\' ) as start_dates,
								array_to_string( array_agg( sub2.end_date ), \'|\' ) as end_dates
							FROM (
								SELECT  
									DISTINCT sub1.goal_date,
									sub1.leasing_goal_id,
									COALESCE( sub1.previous_actuals_percentage, 0 ) as previous_actuals_percentage,
									sub1.lease_term_name,
									sub1.start_date,
									sub1.end_date,
									sub1.prev_year_window_id,
									dense_rank() OVER ( PARTITION BY sub1.goal_date,sub1.prev_year_window_id  ORDER BY sub1.previous_actuals_percentage desc ) AS prev_actuals_rank   
								FROM ( 
									SELECT
										DISTINCT lgt.goal_date,
										lgt.leasing_goal_id,
										COALESCE( previous_actuals.prev_year_window_id, 0 ) as prev_year_window_id,	
										COALESCE( previous_actuals.lease_term_name, \'NULL\' ) as lease_term_name,
										previous_actuals.start_date,
										previous_actuals.end_date,
										round(( SUM( previous_actuals.unit_space_count )OVER( PARTITION BY previous_actuals.prev_year_window_id ORDER BY lgt.goal_date, previous_actuals.lease_approved_on, previous_actuals.prev_year_window_id 
											asc rows between unbounded	preceding and current row )*100.0)/' . ( int ) $arrmixLeasingGoalsFilter['total_unit_spaces_count'] . ' ) as previous_actuals_percentage    
									FROM
										leasing_goals_details_temp lgt
										LEFT JOIN LATERAL ( 
											SELECT 
												DISTINCT ca.id as prev_app_id,
												lgt.goal_date,
												CASE
													WHEN ca.id IS NOT NULL AND ca.unit_space_id IS NULL AND 0 < ca.desired_space_configuration_id THEN 
													(
														SELECT
															sc.unit_space_count
														FROM
															space_configurations sc
														WHERE
															sc.cid = ca.cid
															AND sc.id = ca.desired_space_configuration_id
															AND sc.deleted_by IS NULL
															AND sc.deleted_on IS NULL
													)
													WHEN ca.id IS NOT NULL AND ca.unit_space_id IS NOT NULL THEN
													(
														SELECT
															count ( * ) AS unit_space_count
														FROM
															lease_unit_spaces lus
														WHERE
															lus.cid = ca.cid
															AND lus.property_id = ca.property_id
															AND lus.unit_space_id IS NOT NULL
															AND lus.lease_id = ca.lease_id
															AND lus.deleted_by IS NULL
															AND lus.deleted_on IS NULL
														GROUP BY
															lus.lease_id
													)
													WHEN ca.id IS NOT NULL THEN 1
												ELSE 0
												END AS unit_space_count,
												ca.lease_approved_on::DATE,
												util_get_translated( \'name\', lt.name, lt.details ) AS lease_term_name,
												CASE 
													WHEN lt.is_renewal = true AND lt.is_prospect = false THEN 
														lsw.renewal_start_date
													ELSE
														lsw.start_date
													END AS start_date,
												lsw.end_date,
												COALESCE( lsw.id, 0 ) as prev_year_window_id	
											FROM  
												lease_terms lt
												JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id AND lsw.property_id = lgt.property_id )
												LEFT JOIN cached_applications ca ON ( ca.cid = lsw.cid AND ca.property_id = lsw.property_id AND ca.lease_start_window_id = lsw.id  
													AND ca.lease_approved_on IS NOT NULL AND ca.lease_start_window_id IN( ' . $arrmixLeasingGoalsFilter['previous_year_lease_term_ids'] . ' )               
													AND ( CASE WHEN 
															ca.lease_approved_on::DATE BETWEEN ( ( lgt.min_date - INTERVAL \'1 year\' ) + INTERVAL \'1 day\' ) AND ( lgt.max_date - INTERVAL \'1 year\' ) THEN -- inclusive start and end dates
															ca.lease_approved_on::DATE = ( lgt.goal_date - INTERVAL \'1 year\' )
														WHEN lgt.pre_lease_start_date = lgt.min_date AND lgt.min_date = lgt.goal_date THEN
															ca.lease_approved_on::DATE <= ( lgt.min_date - INTERVAL \'1 year\' )
														WHEN lgt.min_date = lgt.goal_date THEN
															ca.lease_approved_on::DATE BETWEEN ( ( lgt.pre_max_date - INTERVAL \'1 year\' ) + INTERVAL \'1 day\' ) AND ( lgt.min_date - INTERVAL \'1 year\' ) 
													END ) )
											WHERE 
												lt.cid = lgt.cid AND
												lsw.id IN( ' . $arrmixLeasingGoalsFilter['previous_year_lease_term_ids'] . ' )
											ORDER BY
												lgt.goal_date      
										) as previous_actuals ON TRUE	
									ORDER BY 
										lgt.goal_date              
								) as sub1  
								ORDER BY 
									sub1.goal_date
							) as sub2
							WHERE  
								sub2.prev_actuals_rank = 1
							GROUP BY
								sub2.goal_date,
								sub2.leasing_goal_id;';

		$arrmixPrevYearActualsData = fetchData( $strPreviousActualsSql, $objDatabase );

		return $arrmixPrevYearActualsData;
	}

}
?>