<?php

class CScheduledApHeaderExportBatch extends CBaseScheduledApHeaderExportBatch {

	const CONNECTION_TYPE_FTP  = 1;
	const CONNECTION_TYPE_SFTP = 2;

	protected $m_strExportTypeName;
	protected $m_strFileFormatName;
	protected $m_strBankAccountName;
	protected $m_strClientNumber;
	protected $m_strRequestUrl;
	protected $m_strPort;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderExportBatchTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getApHeaderExportBatchTypeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_export_batch_type_id', __( 'Bank Name is required. ' ) ) );
		}
		return $boolIsValid;
	}

	public function valApHeaderExportFileFormatTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;
		if( false == valArr( $this->getBankAccountIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_ids', __( 'Bank Name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCompanyTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;
		if( true == is_null( $this->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Repeat interval is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeekDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastPostedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledStartOn() {
		$boolIsValid = true;
		if( true == is_null( $this->getScheduledStartOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'Start date is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valScheduledEndOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valApHeaderExportBatchTypeId();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valFrequencyInterval();
				$boolIsValid &= $this->valScheduledStartOn();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFrequencyInterval();
				$boolIsValid &= $this->valScheduledStartOn();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( isset( $arrmixValues['export_type_name'] ) ) $this->setExportTypeName( $arrmixValues['export_type_name'] );
		if( isset( $arrmixValues['file_format_name'] ) ) $this->setFileFormatName( $arrmixValues['file_format_name'] );
		if( isset( $arrmixValues['bank_account_name'] ) ) $this->setBankAccountName( $arrmixValues['bank_account_name'] );
		if( isset( $arrmixValues['client_number'] ) ) $this->setClientNumber( $arrmixValues['client_number'] );
		if( isset( $arrmixValues['port'] ) ) $this->setPort( $arrmixValues['port'] );
		if( isset( $arrmixValues['request_url'] ) ) $this->setRequestUrl( $arrmixValues['request_url'] );
		if( isset( $arrmixValues['username_encrypted'] ) ) $this->setUsername( $arrmixValues['username_encrypted'] );
		if( isset( $arrmixValues['password_encrypted'] ) ) $this->setPassword( $arrmixValues['password_encrypted'] );
		if( isset( $arrmixValues['email_addresses'] ) ) $this->setEmailAddresses( $arrmixValues['email_addresses'] );
	}

	public function setExportTypeName( $strExportTypeName ) {
		$this->m_strExportTypeName = $strExportTypeName;
	}

	public function getExportTypeName() {
		return $this->m_strExportTypeName;
	}

	public function setFileFormatName( $strFileFormatName ) {
		$this->m_strFileFormatName = $strFileFormatName;
	}

	public function getFileFormatName() {
		return $this->m_strFileFormatName;
	}

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = $strBankAccountName;
	}

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function calculateScriptStartDate() {

		$boolTakeLastDayOfMonth		= false;
		$boolTakeNextDayFromMonth	= false;
		$strScriptStartDate			= NULL;
		$strPostDate				= $this->getScheduledStartOn();
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= ( true == valStr( $this->getMonthDays() ) ) ? explode( ',', $this->getMonthDays() ) : [];
		$arrmixDayNames				= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];
		$arrmixRelativeFormats		= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];

		if( false == is_numeric( $intFrequencyInterval ) || false == $this->valScheduledStartOn() ) {
			return NULL;
		}

		if( strtotime( $strPostDate ) <= strtotime( 'today' ) ) {
			$strPostDate = date( 'm/d/Y', strtotime( ' + 1 days ' ) );
		}

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::YEARLY:
				$strScriptStartDate = $strPostDate;
				break;

			case CFrequency::WEEKLY:
				if( true == is_numeric( $this->getWeekDays() ) ) {
					$objStartDatetime	= new DateTime( $strPostDate );
					$objStartDatetime->modify( $arrmixDayNames[$this->getWeekDays()] );
					$strScriptStartDate	= $objStartDatetime->format( 'm/d/Y' );

					// if script start date is passed then calculate next day date
					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objStartDatetime->modify( 'next ' . $arrmixDayNames[$this->getWeekDays()] );
						$strScriptStartDate = $objStartDatetime->format( 'm/d/Y' );
					}
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {
					$intNextPostDay	= $arrintDaysOfMonth[0];
					$intDayKey		= array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey, $arrintDaysOfMonth ) ) {
							$intNextPostDay = $arrintDaysOfMonth[$intDayKey];
						}
						$strScriptStartDate 			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
						$strScriptStartDate 			= date( 'm/d/Y', strtotime( $strScriptStartDate ) );
						$boolTakeNextDayFromMonth		= true;
					} else {

						$arrstrPostDate = explode( '/', $strPostDate );
						$intDay = $arrstrPostDate[1];

						foreach( $arrintDaysOfMonth as $intDate ) {
							if( $intDate > $intDay ) {
								$intNextPostDay				= $intDate;
								$boolTakeNextDayFromMonth	= true;
								break;
							}
						}

						$strScriptStartDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

						if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
							$objNextPostMonthDatetime = new DateTime( $strScriptStartDate );
							$objNextPostMonthDatetime->modify( $intFrequencyInterval . ' month' );
							$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
						}
					}

					if( false == $boolTakeNextDayFromMonth && ( 0 == $intNextPostDay || ( false == $intDayKey && false != array_search( 0, $arrintDaysOfMonth ) ) ) ) {
						$boolTakeLastDayOfMonth = true;
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				if( false == is_numeric( $this->getWeekDays() ) ) {
					return NULL;
				}

				$strPostDate 		= date( $strPostDate );
				$strDayName 		= $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber	= $arrmixRelativeFormats[$intFrequencyInterval];

				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of this month' );
				$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );

				if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
					$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
					$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				}
				break;

			default:
		}// switch

		$arrstrNextPostDate = explode( '/', $strScriptStartDate );
		if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
			$boolTakeLastDayOfMonth = true;
		}

		if( true == $boolTakeLastDayOfMonth ) {

			$objDateTimeCurrentPostDate = new DateTime( $strPostDate );
			$objDateTimeCurrentPostDate->modify( 'last day of this month' );
			$strScriptStartDate = $objDateTimeCurrentPostDate->format( 'm/d/Y' );
		}

		return date( 'm/d/Y', strtotime( $strScriptStartDate ) );
	}

	public function calculateEndDate( $objFrequency ) {

		if( false == is_numeric( $this->getNumberOfOccurrences() ) && 0 == $this->getNumberOfOccurrences() ) {
			return NULL;
		}

		$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() ) );
		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
			case CFrequency::YEARLY:
			case CFrequency::WEEKLY:
				$intNumberOfIntervalUnits	= ( $this->getNumberOfOccurrences() - 1 ) * $this->getFrequencyInterval();
				$strIntervalLabel			= $objFrequency->getIntervalFrequencyLabelPlural();
				if( 0 < $intNumberOfIntervalUnits ) {
					$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() . ' +' . $intNumberOfIntervalUnits . ' ' . $strIntervalLabel ) );
				}
				break;

			case CFrequency::MONTHLY:
			case CFrequency::WEEKDAY_OF_MONTH:

				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getWeekDays() ) ) {
					return NULL;
				}

				if( CFrequency::MONTHLY == $this->getFrequencyId() && false == valStr( $this->getMonthDays() ) ) {
					return NULL;
				}

				$strScriptStartDate		= $this->getNextPostDate();
				$intNumberOfOccurrences	= $this->getNumberOfOccurrences();

				for( $intCounter = 2; $intCounter <= $intNumberOfOccurrences; $intCounter++ ) {

					$strEndDate = $this->calculateNextPostDate( $objFrequency );
					$this->setNextPostDate( $strEndDate );
				}
				$this->setNextPostDate( $strScriptStartDate );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strEndDate ) );
	}

	public function calculateNextPostDate( $objFrequency ) {

		if( false == is_numeric( $this->getFrequencyInterval() ) ) {
			return NULL;
		}

		$intNextPostDay				= NULL;
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= explode( ',', $this->getMonthDays() );
		$strPostDate				= $this->getNextPostDate();

		$arrmixRelativeFormats	= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];
		$arrmixDayNames			= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::WEEKLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' ' . $objFrequency->getIntervalFrequencyLabelPlural() ) );
				break;

			case CFrequency::YEARLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' ' . $objFrequency->getIntervalFrequencyLabelPlural() ) );

				if( date( 'm', strtotime( $strPostDate ) ) != date( 'm', strtotime( $strNextPostDate ) ) ) {

					$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
					$objDateTimeNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {

					$intDayKey = array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey + 1, $arrintDaysOfMonth ) ) {

							$intNextPostDay = $arrintDaysOfMonth[$intDayKey + 1];

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								}

							} else {

								$strNextPostDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

								$arrstrNextPostDate = explode( '/', $strNextPostDate );

								if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {

									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/01/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( 'last day of this month' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							}
						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

						}
					} else {

						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintDaysOfMonth ) ) {

							$intNextPostDay = current( $arrintDaysOfMonth );

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

							} else {

								$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
								$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
							}

						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
						}
					}
				}

				$objPostDate = new DateTime( $strPostDate );
				$objPostDate->modify( 'first day of this month' );

				$objNextPostDate = new DateTime( $strNextPostDate );
				$objNextPostDate->modify( 'first day of this month' );

				$objInterval = date_diff( $objPostDate, $objNextPostDate );

				if( $objInterval->format( '%m' ) != $intFrequencyInterval && $objInterval->format( '%m' ) != 0 ) {

					$intNextPostDay		= current( $arrintDaysOfMonth );
					$objNextPostDate	= new DateTime( $strNextPostDate );
					$objNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate	= $objNextPostDate->format( 'm/d/Y' );

					if( 0 != $intNextPostDay ) {

						$strNextPostDateCopy = $strNextPostDate;

						$strNextPostDate	= date( 'm', strtotime( $strNextPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strNextPostDate ) );
						$arrstrNextPostDate	= explode( '/', $strNextPostDate );

						if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
							$strNextPostDate = $strNextPostDateCopy;
						}
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				$strDayName = $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber = $arrmixRelativeFormats[$intFrequencyInterval];
				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
				$strNextPostDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strNextPostDate ) );
	}

	public function setClientNumber( $strClientNumber ) {
		$this->m_strClientNumber = $strClientNumber;
	}

	public function getClientNumber() {
		return $this->m_strClientNumber;
	}

	public function setPort( $strPort ) {
		$this->m_strPort = $strPort;
	}

	public function getPort() {
		return $this->m_strPort;
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->m_strRequestUrl = $strRequestUrl;
	}

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsernameEncrypted = $strUsername;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPasswordEncrypted = $strPassword;
	}

	public function getBankAccountIds() {
		// TODO: this hack is because this is NOT NULL filed and if only '{}' is there in this field then CStrings::strToArrIntDef() gives the empty value with 0th index.
		if( false != valArr( $this->m_arrintBankAccountIds ) ) {
			$this->m_arrintBankAccountIds = array_filter( $this->m_arrintBankAccountIds );
		}
		return $this->m_arrintBankAccountIds;
	}

	public function setEmailAddresses( $strEmailAddresses ) {
		$this->m_strEmailAddresses = $strEmailAddresses;
	}

	public function getEmailAddresses() {
		return $this->m_strEmailAddresses;
	}

}
?>