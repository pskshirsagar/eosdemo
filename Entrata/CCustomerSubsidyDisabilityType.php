<?php

class CCustomerSubsidyDisabilityType extends CBaseCustomerSubsidyDisabilityType {

	protected $m_boolIsDelete;

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function setIsDelete( $boolIsDelete ) {
		return $this->m_boolIsDelete = CStrings::strToBool( $boolIsDelete );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyDisabilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>