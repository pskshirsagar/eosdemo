<?php

class CPetType extends CBasePetType {

	protected $m_intPetRateAssociationId;

	// Get functions

	public function getPetRateAssociationId() {
		return $this->m_intPetRateAssociationId;
	}

	// Set functions

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['pet_rate_association_id'] ) && $boolDirectSet ) {
			$this->m_intPetRateAssociationId = trim( $arrmixValues['pet_rate_association_id'] );
		} elseif( true == isset( $arrmixValues['pet_rate_association_id'] ) ) {
			$this->setPetRateAssociationId( $arrmixValues['pet_rate_association_id'] );
		}
	}

	public function setPetRateAssociationId( $intPetRateAssociationId ) {
		$this->m_intPetRateAssociationId = $intPetRateAssociationId;
	}

	public function createRateAssociation() {

		$objRateAssociation = new CRateAssociation();
		$objRateAssociation->setCid( $this->getCid() );
		$objRateAssociation->setArOriginId( CArOrigin::PET );
		$objRateAssociation->setIsOptional( true );
		$objRateAssociation->setArOriginReferenceId( $this->getId() );
		$objRateAssociation->setIsFeatured( $this->getIsIconVisible() );

		return $objRateAssociation;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Other functions

	public function deletePetType( $intPropertyId, $intCurrentUserId, $objDatabase ) {
		if( 0 >= ( int ) $intPropertyId ) return;

		/**
		 *	Comment: DELETE PET TYPE
		 *	1. Delete rates function
		 *	2. Hard delete rate associations
		 *	3. NO DELETE ON PET TYPE JUST DELETE RATE ASSOCIATIONS
		 */

		// Rates Criteria

		$objRatesCriteria	= new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $intPropertyId );
		$objRatesCriteria->setArCascadeId( CArCascade::PROPERTY );

		$objRatesCriteria->setArOriginId( CArOrigin::PET );
		$objRatesCriteria->setArOriginReferenceId( $this->getId() );

		$objRatesLibrary	= new CRatesLibrary( $objRatesCriteria );

		// Rate Associations
		$arrobjPetRateAssociations = ( array ) \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( CArCascade::PROPERTY, CArOrigin::PET, $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				foreach( $arrobjPetRateAssociations as $objPetRateAssociation ) {

					if( false == $objPetRateAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}
		}

		return $boolIsValid;
	}

}
?>