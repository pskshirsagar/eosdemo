<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CLeaseUnitSpace extends CBaseLeaseUnitSpace {

    protected $m_intMaxSlotNumber;
	protected $m_intPropertyUnitId;
	protected $m_intPrimaryCustomerId;
	protected $m_intUnitSpaceStatusTypeId;

    protected $m_strSpaceNumber;
    protected $m_strAvailableOn;
	protected $m_strUnitNumberCache;

    /**
	* Set/Get Functions
	*
	*/

    public function setMaxSlotNumber( $intMaxSlotNumber ) {
        $this->m_intMaxSlotNumber = CStrings::strToIntDef( $intMaxSlotNumber, NULL, false );
    }

    public function getMaxSlotNumber() {
        return $this->m_intMaxSlotNumber;
    }

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function setAvailableOn( $strAvailableOn ) {
		$this->m_strAvailableOn = CStrings::strTrimDef( $strAvailableOn, -1, NULL, true );
	}

	public function getAvailableOn() {
		return $this->m_strAvailableOn;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = CStrings::strToIntDef( $intPropertyUnitId, NULL, false );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->m_intPrimaryCustomerId = CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->m_strSpaceNumber = CStrings::strTrimDef( $strSpaceNumber, 50, NULL, true );
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->m_intUnitSpaceStatusTypeId = CStrings::strToIntDef( $intUnitSpaceStatusTypeId, NULL, false );
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['max_slot_number'] ) && $boolDirectSet ) {
			$this->m_intMaxSlotNumber = trim( $arrmixValues['max_slot_number'] );
	    } elseif( isset( $arrmixValues['max_slot_number'] ) ) {
			$this->setMaxSlotNumber( $arrmixValues['max_slot_number'] );
	    }

		if( isset( $arrmixValues['space_number'] ) && $boolDirectSet ) {
			$this->m_strSpaceNumber = trim( stripcslashes( $arrmixValues['space_number'] ) );
		} elseif( isset( $arrmixValues['space_number'] ) ) {
			$this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['space_number'] ) : $arrmixValues['space_number'] );
		}

		if( isset( $arrmixValues['available_on'] ) && $boolDirectSet ) {
			$this->m_strAvailableOn = trim( $arrmixValues['available_on'] );
		} elseif( isset( $arrmixValues['available_on'] ) ) {
			$this->setAvailableOn( $arrmixValues['available_on'] );
		}

		if( isset( $arrmixValues['property_unit_id'] ) && $boolDirectSet ) {
			$this->m_intPropertyUnitId = trim( $arrmixValues['property_unit_id'] );
		} else if( isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}

		if( isset( $arrmixValues['primary_customer_id'] ) && $boolDirectSet ) {
			$this->m_intPrimaryCustomerId = trim( $arrmixValues['primary_customer_id'] );
		} else if( isset( $arrmixValues['primary_customer_id'] ) ) {
			$this->setPrimaryCustomerId( $arrmixValues['primary_customer_id'] );
		}

		if( isset( $arrmixValues['unit_number_cache'] ) && $boolDirectSet ) {
			$this->m_strUnitNumberCache = trim( stripcslashes( $arrmixValues['unit_number_cache'] ) );
		} elseif( isset( $arrmixValues['unit_number_cache'] ) ) {
			$this->setUnitNumberCache( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_number_cache'] ) : $arrmixValues['unit_number_cache'] );
		}

		if( isset( $arrmixValues['unit_space_status_type_id'] ) && $boolDirectSet ) {
			$this->m_intUnitSpaceStatusTypeId = trim( $arrmixValues['unit_space_status_type_id'] );
		} else if( isset( $arrmixValues['unit_space_status_type_id'] ) ) {
			$this->setUnitSpaceStatusTypeId( $arrmixValues['unit_space_status_type_id'] );
		}
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valUnitSpaceId( $objLease, $objDatabase ) {
		$boolIsValid   = true;
		$strMoveInDate = $objLease->getMoveInDate();

		$objUnitSpace = CUnitSpaces::createService()->fetchUnitSpaceByPropertyUnitIdByIdByCid( $objLease->getPropertyUnitId(), $this->getUnitSpaceId(), $objLease->getCid(), $objDatabase );

		if( true == in_array( $objLease->getLeaseStatusTypeId(), [ CLeaseStatusType::FUTURE,  CLeaseStatusType::APPLICANT ] ) ) {
			$arrintLeaseStartWindowIds = ( array ) CLeaseStartWindows::fetchAssociatedLeaseStartWindowIdsByIdByPropertyIdByCid( $objLease->getLeaseStartWindowId(), $objLease->getPropertyId(), $objLease->getCid(), $objDatabase );

			if( false == valArr( $arrintLeaseStartWindowIds ) ) {
				return true;
			}

			$arrintLeaseStartWindowIds = array_keys( rekeyArray( 'id', $arrintLeaseStartWindowIds ) );

			$arrintLeaseIdsCount = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseCountByUnitSpaceIdByLeaseStartWindowIdsByPropertyIdByCid( $this->getUnitSpaceId(), $arrintLeaseStartWindowIds, $objLease->getPropertyId(), $objLease->getCid(), $objDatabase );
			if( 1 <= $arrintLeaseIdsCount[0]['lease_id_count'] ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space', __( 'Unit Space is already assigned to some other lease.' ) ) );
				$boolIsValid = false;
			}
		} else {

			$strMoveInDate = date( 'm/d/Y' );
			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && true == valId( $objUnitSpace->getUnitSpaceStatusTypeId() ) && false == $objUnitSpace->valUnitSpaceStatusTypeId( $objDatabase, false ) ) {
				$this->addErrorMsgs( $objUnitSpace->getErrorMsgs() );
				$boolIsValid = false;
			}
		}

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && strtotime( $objUnitSpace->getAvailableOn() ) > strtotime( $strMoveInDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be greater than or equal to unit availability date - ( {%t, 0, DATE_NUMERIC_STANDARD} )', [ $objUnitSpace->getAvailableOn() ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function valMoveInDate( $objLease ) {
        $boolIsValid = true;

        if( false == valStr( $this->getMoveInDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move in date should not be blank .' ) ) );
        }

       	if( strtotime( $this->getMoveInDate() ) < strtotime( $objLease->getMoveInDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'You cannot move in an unit space before the move in date on the lease .' ) ) );
        }

        if( true == valStr( $objLease->getMoveOutDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( $objLease->getMoveOutDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'You cannot move in an unit space after the move out date on the lease .' ) ) );
        }

        return $boolIsValid;
    }

    public function valMoveOutDate() {
        $boolIsValid = true;

        if( false == valStr( $this->getMoveOutDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move out date should not be blank.' ) ) );
        }

       	if( strtotime( $this->getMoveOutDate() ) < strtotime( $this->getMoveInDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'You cannot move out an unit space before the move in date on the lease.' ) ) );
        }
        return $boolIsValid;
    }

    public function valStartsWithLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndsWithLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPrimary() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objLease, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valMoveInDate( $objLease );
        		$boolIsValid &= $this->valUnitSpaceId( $objLease, $objDatabase );
            	break;

        	case VALIDATE_UPDATE:
        		if( CLeaseStatusType::CURRENT === $objLease->getLeaseStatusTypeId() || CLeaseStatusType::NOTICE === $objLease->getLeaseStatusTypeId() ) {
        			$boolIsValid &= $this->valMoveOutDate( $objLease );
        		}
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>