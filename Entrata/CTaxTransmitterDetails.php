<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxTransmitterDetails
 * Do not add any new functions to this class.
 */

class CTaxTransmitterDetails extends CBaseTaxTransmitterDetails {

	public static function fetchPaginatedTaxTransmitterDetailsByCid( $intCid, $objPagination, $objClientDatabase ) {

		$strSql = ' SELECT
						* 
					FROM
						tax_transmitter_details
					WHERE
						cid = ' . ( int ) $intCid;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchTaxTransmitterDetails( $strSql, $objClientDatabase );
	}

	public static function fetchTaxTransmitterDetailsCountByCid( $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						COUNT( id )
					FROM
						tax_transmitter_details
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

}
?>