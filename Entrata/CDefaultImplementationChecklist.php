<?php

class CDefaultImplementationChecklist extends CBaseDefaultImplementationChecklist {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>