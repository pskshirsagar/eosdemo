<?php

class CNotification extends CBaseNotification {

	protected $m_intReadCount;
	protected $m_intTotalPostCount;
	protected $m_strViewedOn;
	protected $m_strCompanyUserName;

	public function __construct() {
		parent::__construct();

		$this->m_intReadCount      = 0;
		$this->m_intTotalPostCount = 0;
		$this->m_strViewedOn       = '';

		return;
	}

	public function setReadCount( $intReadCount ) {
		$this->m_intReadCount = CStrings::strToIntDef( $intReadCount, NULL, false );
	}

	public function setTotalPostCount( $intTotalPostCount ) {
		$this->m_intTotalPostCount = CStrings::strToIntDef( $intTotalPostCount, NULL, false );
	}

	public function setViewedOn( $strViewedOn ) {
		$this->m_strViewedOn = $strViewedOn;
	}

	public function setCompanyUserName( $strCompanyUserName ) {
		$this->m_strCompanyUserName = $strCompanyUserName;
	}

	public function getReadCount() {
		return $this->m_intReadCount;
	}

	public function getTotalPostCount() {
		return $this->m_intTotalPostCount;
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function getCompanyUserName() {
		return $this->m_strCompanyUserName;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['read_count'] ) ) {
			$this->setReadCount( $arrValues['read_count'] );
		}
		if( true == isset( $arrValues['total_post_count'] ) ) {
			$this->setTotalPostCount( $arrValues['total_post_count'] );
		}
		if( true == isset( $arrValues['viewed_on'] ) ) {
			$this->setViewedOn( $arrValues['viewed_on'] );
		}
		if( true == isset( $arrValues['company_user_name'] ) ) {
			$this->setCompanyUserName( $arrValues['company_user_name'] );
		}
	}

	public function fetchRecipientProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CNotificationProperties::createService()->fetchNotificationPropertiesByNotificationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchRecipientCompanyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CNotificationCompanyGroups::createService()->fetchNotificationCompanyGroupsByNotificationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valNotificationTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intNotificationTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notification_type_id', __( 'Message type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valModuleId() {
		return true;
	}

	public function valTitle() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strTitle ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Message is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Select Dates Posted from.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPastStartDate() {

		$boolIsValid = true;
		if( strtotime( $this->m_strStartDate ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Posted date can not be less than todays date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Select Dates Posted to.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartEndDates() {
		$boolIsValid = true;
		if( true == valStr( $this->m_strStartDate ) && true == valStr( $this->m_strEndDate ) && 1 == compareDates( $this->m_strStartDate, $this->m_strEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date_end_date', __( 'Dates Posted from should be less than Dates Posted to.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNotificationTypeId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valPastStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valStartEndDates();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				break;

			case 'validate_update_current_notification':
				$boolIsValid &= $this->valNotificationTypeId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valStartEndDates();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				break;

			case 'validate_reorder':
			case 'delete_active_message':
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}

?>