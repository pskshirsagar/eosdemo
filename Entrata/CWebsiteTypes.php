<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWebsiteTypes
 * Do not add any new functions to this class.
 */

class CWebsiteTypes extends CBaseWebsiteTypes {

	public static function fetchWebsiteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CWebsiteType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchWebsiteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CWebsiteType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>