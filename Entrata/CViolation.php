<?php

class CViolation extends CBaseViolation {

	protected $m_intPropertyId;
	protected $m_intViolationFileId;
	protected $m_strViolationNote;

	public function createViolationNote() {
		$objViolationNote = new CViolationNote();
		$objViolationNote->setCid( $this->getCid() );
		$objViolationNote->setViolationId( $this->getId() );

		return $objViolationNote;
	}

	public function createViolationNotice() {
		$objViolationNotice = new CViolationNotice();
		$objViolationNotice->setCid( $this->getCid() );
		$objViolationNotice->setViolationId( $this->getId() );

		return $objViolationNotice;
	}

	/*
     * Setter functions
	 */

	public function setViolationNote( $strViolationNote ) {
		$this->m_strViolationNote = $strViolationNote;
	}

	public function setViolationFileId( $intViolationFileId ) {
		$this->m_intViolationFileId = $intViolationFileId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/*
     * Getter functions
	 */

	public function getViolationNote() {
		return $this->m_strViolationNote;
	}

	public function getViolationFileId() {
		return $this->m_intViolationFileId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['violation_note'] ) ) {
			$this->setViolationNote( $arrmixValues['violation_note'] );
		}
		if( true == isset( $arrmixValues['violation_file_id'] ) ) {
			$this->setViolationFileId( $arrmixValues['violation_file_id'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViolationTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViolationTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getViolationTemplateId() ) && true == is_null( $this->getViolationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'violation_type_id', 'Violation type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getViolationTemplateId() ) && true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Violation name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViolationDatetime() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strViolationDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'violation_datetime', 'Date created and time required.' ) );
		} else if( true == valStr( $this->m_strViolationDatetime ) && 1 !== CValidation::checkDateTime( $this->m_strViolationDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'violation_datetime', 'Violation date and time must be in correct date-time form.' ) );
		}
		return $boolIsValid;
	}

	public function valResolvedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valViolationTypeId();
				$boolIsValid &= $this->valViolationDatetime();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function emailSubmitViolationNotice( $objCustomer = NULL, $intPropertyId, $intDocumentId, $intCompanyUserId, $objObjectStorageGateway, $objEmailDatabase, $objDatabase, $strEmailContent ) {
		$strEmailSubject = __( 'Violation Notice' );
		$intSystemEmailType = CSystemEmailType::EMAIL_VIOLATION_NOTICE;

		if( true == valStr( $strEmailContent ) ) {
			$strHtmlEmailContent = $strEmailContent;
		} else {
			$strHtmlEmailContent = __( 'Please find the attachment.' );
		}

		$arrintDocumentMergeFieldsCount = ( array ) \CDocumentMergeFields::fetchDocumentMergeFieldsByDocumentIdsByCid( [ $intDocumentId ], $this->getCid(), $objDatabase, true );

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y' ) );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setSubject( $strEmailSubject );

		if( true == valObj( $objCustomer, 'CCustomer' ) && true == valStr( $objCustomer->getEmailAddress() ) ) {
			$objSystemEmail->setToEmailAddress( $objCustomer->getEmailAddress() );
		} else {
			$objSystemEmail->setToEmailAddress( $objCustomer->getUsername() );
		}

		$objSystemEmail->setPropertyId( $intPropertyId );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailTypeId( $intSystemEmailType );
		$objSystemEmail->setCustomerId( $this->getCustomerId() );
		$objSystemEmail->setLeaseId( $this->getLeaseId() );
		$objSystemEmail->setHtmlContent( $strHtmlEmailContent );
		if( true == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			$objSystemEmail->setObjectStorageGateway( $objObjectStorageGateway );
		} else {
			$objSystemEmail->setDocumentManager( $objObjectStorageGateway );
		}

		$objDocument = \Psi\Eos\Entrata\CDocuments::createService()->fetchDocumentByIdByCid( $intDocumentId, $this->getCid(), $this->m_objDatabase );
		$objSystemEmail->setClientDatabase( $objDatabase );
		if( true == valArr( $arrintDocumentMergeFieldsCount ) && false == valId( $arrintDocumentMergeFieldsCount[0] ) && true == valObj( $objDocument, 'CDocument' ) ) {
			$strAttachmentFilePath = $this->getViolationNoticeAttachmentPath( $objDocument, $objObjectStorageGateway );
			if( true == valStr( $strAttachmentFilePath ) ) {
				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setDefaults();
				if( true == $objEmailAttachment->setEmailAttachmentFile( '', $objEmailDatabase, $strAttachmentFilePath ) ) {
					$objEmailAttachment->setFilePath( pathinfo( $strAttachmentFilePath )['dirname'] );
					$objSystemEmail->addEmailAttachment( $objEmailAttachment );
				}
			}
		}

		if( false == is_null( $objSystemEmail ) && false == $objSystemEmail->insert( $intCompanyUserId, $objEmailDatabase ) ) {
			return false;
		}

		return true;
	}

	public function getViolationNoticeAttachmentPath( $objDocument, $objObjectStorageGateway ) {
		if( true == valObj( $objDocument, 'CDocument' ) && false == is_null( $objObjectStorageGateway ) ) {
			$arrmixFileOptions = [
				'cid'        => $this->getCid(),
				'objectId'   => $objDocument->getId(),
				'outputFile' => 'temp'
			];

			$arrmixGatewayRequest = $objDocument->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( $arrmixFileOptions );
			$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );
			$strAttachmentFilePath = $objObjectStorageGatewayResponse['outputFile'];

			if( true == CFileIo::fileExists( $strAttachmentFilePath ) ) {
				return $strAttachmentFilePath;
			}
		}

		return false;
	}

	public function createViolationEvent( $intEventSubTypeId, $objCompanyUser = NULL, $objDatabase ) {
		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::VIOLATION_MANAGER, $intEventSubTypeId );
		$objEvent->setCid( $this->getCid() );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
			$intUserId = $objCompanyUser->getId();

			if( false == is_null( $objCompanyUser->getCompanyEmployeeId() ) ) {
				$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objCompanyUser->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objEvent->setCompanyEmployee( $objCompanyEmployee );
				}
			}
		} else {
			$intUserId = SYSTEM_USER_ID;
			$objEvent->setCompanyUser( NULL );
		}

		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setCustomerId( $this->getCustomerId() );
		$objEvent->setLeaseId( $this->getLeaseId() );
		$objEvent->setDataReferenceId( $this->getId() );
        if( CEventSubType::VIOLATION_DESCRIPTION_UPDATED == $intEventSubTypeId ) {
            $objEvent->setNotes( $this->getDescription() );
        } else {
            $objEvent->setNotes( NULL );
        }
		$objEvent->setReference( $this );

		$objEventLibrary->buildEventDescription( $this );

		if( false == $objEventLibrary->insertEvent( $intUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			$objDatabase->rollback();
			return false;
		}
		return true;
	}

}
?>