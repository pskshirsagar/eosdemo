<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStatuses
 * Do not add any new functions to this class.
 */

class CApplicationStatuses extends CBaseApplicationStatuses {

	public static function fetchApplicationStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>