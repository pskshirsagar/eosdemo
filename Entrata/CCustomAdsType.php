<?php

class CCustomAdsType extends CBaseCustomAdsType {

	const AD				= 1;
	const INFORMATION 		= 2;
	const TWEET 			= 3;

	public static $c_arrintCustomAdsTypes = [
		'ad'				=> CCustomAdsType::AD,
		'information'		=> CCustomAdsType::INFORMATION,
		'tweet'				=> CCustomAdsType::TWEET
	];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>