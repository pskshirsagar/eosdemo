<?php

class CArPaymentBatch extends CBaseArPaymentBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFormData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function updateArPaymentBatchFields( $intArPaymentBatchId, $intCid, $objDatabase, $intCompanyUserId = NULL, $boolClearBatchOpenOn = false ) {

		$strOpenedOn = ( true == $boolClearBatchOpenOn ) ? 'NULL' : 'NOW()';
		$strSqlJoin = ( false == is_null( $intCompanyUserId ) ) ? ', updated_by = ' . ( int ) $intCompanyUserId . '': '';
		$strSql = 'UPDATE
						ar_payment_batches
					SET
						opened_on = ' . $strOpenedOn . $strSqlJoin . '
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intArPaymentBatchId;

		return fetchData( $strSql, $objDatabase );
	}
}
?>