<?php

class CWebsiteAd extends CBaseWebsiteAd {

	protected $m_strSpecialMainText;
	protected $m_strSpecialStartDate;
	protected $m_strSpecialEndDate;
	protected $m_strWebsiteAdTypeName;
	protected $m_strMarketingMediaAssociationReference;

	const ALT_TEXT_LENGTH = 240;

	public function getWebsiteAdTypeName() {
		return $this->m_strWebsiteAdTypeName;
	}

	public function getMarketingMediaAssociationReference() {
		return $this->m_strMarketingMediaAssociationReference;
	}

	public function getSpecialMainText() {
		return $this->m_strSpecialMainText;
	}

	public function getSpecialStartDate() {
		return $this->m_strSpecialStartDate;
	}

	public function getSpecialEndDate() {
		return $this->m_strSpecialEndDate;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['name'] ) )					            $this->setWebsiteAdTypeName( $arrstrValues['name'] );
		if( true == isset( $arrstrValues['special_main_text'] ) )					$this->setSpecialMainText( $arrstrValues['special_main_text'] );
		if( true == isset( $arrstrValues['marketing_storage_type_reference'] ) )	$this->setMarketingMediaAssociationReference( $arrstrValues['marketing_storage_type_reference'] );
		if( true == isset( $arrstrValues['special_start_date'] ) )					$this->setSpecialStartDate( $arrstrValues['special_start_date'] );
		if( true == isset( $arrstrValues['special_end_date'] ) )					$this->setSpecialEndDate( $arrstrValues['special_end_date'] );
	}

	public function valTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Ad name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLinkURL() {
		$boolIsValid = true;

		if( true == valStr( $this->getLinkUrl() ) && false == CValidation::checkUrl( $this->getLinkUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', __( "A valid url is required. Url must start with \'http://\' , \'https://\' or \'/\'." ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Validate Alt Text to check for long text if it is not empty
	 * @return bool
	 */
	public function valAltText() {
		$boolIsValid = true;

		if( true == valStr( $this->getAltText() ) && self::ALT_TEXT_LENGTH < strlen( $this->getAltText() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_text', __( 'Alt Text is Too Long.' ) ) );
		}

		return $boolIsValid;
	}

	public function checkForSameTitle( $objDatabase ) {
		$boolIsValid = true;

		$intWebsiteAdCount = \Psi\Eos\Entrata\CWebsiteAds::createService()->fetchWebsiteAdByTitleByWebsiteIdByCid( $this->getTitle(), $this->getWebsiteId(), $this->getCid(), $objDatabase );
		if( 0 < $intWebsiteAdCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', __( 'Ad with same name already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function setWebsiteAdTypeName( $strWebsiteAdTypeName ) {
		$this->m_strWebsiteAdTypeName = $strWebsiteAdTypeName;
	}

	public function setMarketingMediaAssociationReference( $strMarketingMediaAssociationReference ) {
		$this->m_strMarketingMediaAssociationReference = $strMarketingMediaAssociationReference;
	}

	public function setSpecialMainText( $strSpecialMainText ) {
		$this->m_strSpecialMainText = $strSpecialMainText;
	}

	public function setSpecialStartDate( $strSpecialStartDate ) {
		$this->m_strSpecialStartDate = $strSpecialStartDate;
	}

	public function setSpecialEndDate( $strSpecialEndDate ) {
		$this->m_strSpecialEndDate = $strSpecialEndDate;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valLinkURL();
				$boolIsValid &= $this->checkForSameTitle( $objDatabase );
				$boolIsValid &= $this->valAltText();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valLinkURL();
				$boolIsValid &= $this->valAltText();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>