<?php

use Psi\Eos\Entrata\CScheduledEmailFilters;
use Psi\Eos\Entrata\CScheduledEmailAddressFilters;
use Psi\Eos\Entrata\CSystemMessageTemplateCustomTexts;
use Psi\Eos\Entrata\CSystemMessageTemplateSlotImages;
use Psi\Eos\Entrata\CSystemMessageTemplateSlots;
use Psi\Eos\Entrata\CSystemMessageTemplates;

class CScheduledEmail extends CBaseScheduledEmail {

	protected $m_arrobjScheduledEmailAddressFilters;

	protected $m_arrintCreatorPermissionedPropertyIds;

	protected $m_objScheduledEmailType;
	protected $m_objScheduledEmailFilter;
	protected $m_objClient;
	protected $m_objCustomer;
	protected $m_objCompanyEmployee;
	protected $m_objApplicant;
	protected $m_objLeaseCustomer;
	protected $m_objApPayee;

	protected $m_boolIsWeekly;
	protected $m_boolIsEmailScheduled;
	protected $m_boolIsEditPermission;
	protected $m_boolIsPublished;

	protected $m_intScheduledEmailId;
	protected $m_intEmailType;
	protected $m_intBlockedRecipientsCount;
	protected $m_intNumberOfOpenEmailsCount;
	protected $m_intNumberOfDeliveredEmails;
	protected $m_intScheduledEmailTransmissionId;
	protected $m_intScheduledEmailFilterIsShared;
	protected $m_arrintScheduledEmailFilterIsShares;

	protected $m_strScheduledEmailFilterName;
	protected $m_arrstrScheduledEmailFilterNames;
	protected $m_strSendHour;
	protected $m_strFrequencyName;
	protected $m_strNextSendOn;
	protected $m_strSendDate;
	protected $m_strScheduledEmailFilterDeletedOn;
	protected $m_strScheduledEmailEventTypeName;
	protected $m_strScheduledEmailDeliveryType;
	protected $m_strScheduledEmailCreatedByUser;
	protected $m_strListCreatedByUser;
	protected $m_strUserName;
	protected $m_strLeaseStatusTypes;

	const ONCE		= 1;
	const RECURRING	= 2;
	const AUTOMATED	= 3;

	const MAX_ATTEMPT_LIMIT = 3;

	const CONTENT_POSTAL_ADDRESS_FORMAT 		= 1;
	const CONTENT_UNSUBSCRIBE_FORMAT			= 2;
	const CONTENT_VIEW_IN_BROWSER				= 3;
	const CONTENT_UPLOAD_FILES					= 4;
	const CONTENT_PERSONALIZED					= 5;
	const CONTENT_PROPERTY_OR_COMPANY_LOGO		= 6;

	public function __construct() {
		parent::__construct();

		$this->m_intEmailType = self::ONCE;
		$this->m_intNumberOfDeliveredEmails = 0;
		$this->m_intNumberOfOpenEmailsCount = 0;
		$this->m_boolIsEditPermission = 1;

		return;
	}

	public function setDefaults() {

		$this->m_boolIsEmailScheduled = false;
	}

	/**
	 * Create Functions
	 */

	public function createScheduledEmailTransmission() {

		$objScheduledEmailTransmission = new CScheduledEmailTransmission();
		$objScheduledEmailTransmission->setCid( $this->getCid() );
		$objScheduledEmailTransmission->setScheduledEmailId( $this->getId() );

		return $objScheduledEmailTransmission;
	}

	public function createSystemEmail() {

		$objSystemEmail	= new CSystemEmail();
		$objSystemEmail->setScheduledEmailId( $this->getId() );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setFromEmailAddress( $this->getFromEmailAddress() );
		$objSystemEmail->setSubject( $this->getSubject() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::MESSAGE_CENTER_EMAIL );
		$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y' ) );

		return $objSystemEmail;
	}

	public function createEmailTransmission( $objScheduledEmailFilter, $objScheduledEmailTransmission ) {
		$objEmailTransmission = new CEmailTransmission();
		$objEmailTransmission->setCid( $this->getCid() );
		$objEmailTransmission->setScheduledEmailId( $this->getId() );
		$objEmailTransmission->setScheduledEmailTransmissionId( $objScheduledEmailTransmission->getId() );

		switch( $objScheduledEmailFilter->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$objEmailTransmission->setRecipientTypeId( CSystemEmailRecipientType::RESIDENT );
				break;

			case CScheduledEmailType::PROSPECTS:
				$objEmailTransmission->setRecipientTypeId( CSystemEmailRecipientType::LEAD );
				break;

			case CScheduledEmailType::EMPLOYEES:
				$objEmailTransmission->setRecipientTypeId( CSystemEmailRecipientType::COMPANY_EMPLOYEE );
				break;

			case CScheduledEmailType::VENDORS:
				$objEmailTransmission->setRecipientTypeId( CSystemEmailRecipientType::VENDORS );
				break;

			default:
		}

		$objEmailTransmission->setTemplateId( $this->getSystemMessageTemplateId() );
		$objEmailTransmission->setDeliveryTypeId( $this->getScheduledEmailDeliveryTypeId() );
		$objEmailTransmission->setSubject( $this->getSubject() );
		$objEmailTransmission->setScheduledEmailFilterName( $objScheduledEmailFilter->getName() );
		$objEmailTransmission->setScheduledEmailCreatedBy( $this->getEmailOwnerId() );

		return $objEmailTransmission;
	}

 	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setScheduledEmailId( $this->getId() );

		return $objFileAssociation;
	}

	public function sendCopyToBccEmails( $strHtmlContent, $objClientDatabase, $objEmailDatabase, $strToEmailAddresses = NULL, $arrobjFiles = [], $arrmixRecipientMergeFields = [], $objStorageGatewayObject = NULL ) {

		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*VIEW_IN_BROWSER_LINK*', '', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( 'Is this email not displaying correctly?', '', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '<code>VIEW_IN_BROWSER_LINK</code>', '', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*UNSUBSCRIBE*', '', $strHtmlContent );

		if( NULL == $strToEmailAddresses ) {

			$strHtmlNote = '<div style="background-color: #EECFC3;border:1px solid #BD3E0F;color: #BD3E0F; padding:5px; font-family:Arial, Helvetica, sans-serif; margin-top:10px; text-align: center"><p>' . __( 'This email is representative of what the intended recipients will see.
						In this copy, merge fields have been replaced according to first recipient of property.
						Actual merged information may vary accordingly.' ) . '<p/></div><br />';

			if( true == valArr( $arrmixRecipientMergeFields ) ) {
				foreach( $arrmixRecipientMergeFields as $strKey => $strValue ) {
					$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( $strKey, $strValue, $strHtmlContent );
				}
			}

			$strHtmlContent = $strHtmlNote . $strHtmlContent;
		}

		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*LOGO*', '<div style="width:260px;height:70px;background-color:#EAEAEA;font-size:30px;color:white;text-align:center;margin-bottom:5px;padding: 20px;font-weight:bold;">Logo<br/> will display here</div>', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*PROPERTY_EMAIL_HEADER_IMAGE*', '<div style="background-color:#eaeaea;color: white;font-size: 30px;font-weight: bold;height: 40px;margin-top: 10px;max-width: 680px;padding: 40px;text-align: center;">Property Header</div>', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*PROPERTY_EMAIL_FOOTER_IMAGE*', '<div style="background-color:#eaeaea;color: white;font-size: 30px;font-weight: bold;height: 40px;margin-bottom: 10px;max-width: 680px;padding: 40px;text-align: center;">Property Footer</div>', $strHtmlContent );
		$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( '*EQUAL_HOUSING_OPPORTUNITY_LOGO*', '', $strHtmlContent );

		$arrobjEmailAttachments = [];
		$arrmixFilePath			= [];
		$objSystemEmail 		= $this->createSystemEmail();
		$strFromEmailAddress	= CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		if( true == valObj( $this->m_objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_objCompanyEmployee->getPropertyId(), $this->getCid(), $objClientDatabase );

			if( true == valObj( $objProperty, 'CProperty' ) ) {
				$strFromEmailAddress = convertNonAsciiCharactersToAscii( $objProperty->getPropertyName() ) . ' <' . convertNonAsciiCharactersToAscii( $strFromEmailAddress ) . '>';
			}
		}

		if( true == valObj( $this->m_objApplicant, 'CApplicant' ) ) {
			$strFromEmailAddress = convertNonAsciiCharactersToAscii( $this->m_objApplicant->getPropertyName() ) . ' <' . convertNonAsciiCharactersToAscii( $strFromEmailAddress ) . '>';
		}

		if( true == valObj( $this->m_objLeaseCustomer, 'CLeaseCustomer' ) ) {
			$strFromEmailAddress = convertNonAsciiCharactersToAscii( $this->m_objLeaseCustomer->getPropertyName() ) . ' <' . convertNonAsciiCharactersToAscii( $strFromEmailAddress ) . '>';
		}

		$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );
		$objSystemEmail->setHtmlContent( $strHtmlContent );

		if( false == valArr( $arrobjFiles ) ) {
			$arrobjFiles = $this->fetchFiles( $objClientDatabase );
		}

		if( true == valArr( $arrobjFiles ) && false == is_null( $objStorageGatewayObject ) ) {
			foreach( $arrobjFiles as $objFile ) {
				$arrstrFileInfo = [];
				$arrmixRequest  = $objFile->fetchStoredObject( $objClientDatabase )->createGatewayRequest( [ 'checkExists' => true ] );

				$objObjectStorageGatewayResponse = $objStorageGatewayObject->getObject( $arrmixRequest );
				if( false == $objObjectStorageGatewayResponse['isExists'] ) {
					continue;
				}

				$arrmixRequest = $objFile->fetchStoredObject( $objClientDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
				$objObjectStorageGatewayResponse = $objStorageGatewayObject->getObject( $arrmixRequest );

				$arrstrFileInfo = pathinfo( $objObjectStorageGatewayResponse['outputFile'] );

				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setTitle( $objFile->getTitle() );
				$objEmailAttachment->setFilePath( $arrstrFileInfo['dirname'] );
				$objEmailAttachment->setFileName( $arrstrFileInfo['basename'] );
				$arrobjEmailAttachments[] = $objEmailAttachment;
			}
		}

		if( true == is_null( $strToEmailAddresses ) ) {
			$arrstrToEmailAddresses		= ( false == is_null( $this->getTestEmailAddress() ) ) ? explode( ',', trim( $this->getTestEmailAddress() ) ) : NULL;
		} else {
			$arrstrToEmailAddresses 	= explode( ',', trim( $strToEmailAddresses ) );
		}

		switch( NULL ) {
			default:
				$boolIsSent = true;

				if( false == valArr( $arrstrToEmailAddresses ) ) {
					break;
				}

				$arrstrSentEmailAddresses = [];
				foreach( $arrstrToEmailAddresses as $strToEmailAddress ) {

					$strToEmailAddress = trim( $strToEmailAddress );

					if( false == CValidation::validateEmailAddresses( $strToEmailAddress ) || true == in_array( $strToEmailAddress, $arrstrSentEmailAddresses ) ) {
						continue;
					}
					$objCloneSystemEmail = clone $objSystemEmail;

					if( true == valArr( $arrobjEmailAttachments ) ) {
						foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
							if( true == valObj( $objSystemEmail, CSystemEmail::class ) ) {
								$objCloneSystemEmail->addEmailAttachment( $objEmailAttachment );
							}
						}
					}

					$objCloneSystemEmail->setToEmailAddress( $strToEmailAddress );

					if( false == $objCloneSystemEmail->validate( VALIDATE_INSERT ) || false == $objCloneSystemEmail->insert( 1, $objEmailDatabase ) ) {
						$boolIsSent &= false;
						break 1;
					}

					if( false == in_array( $strToEmailAddress, $arrstrSentEmailAddresses ) ) {
						$arrstrSentEmailAddresses[] = $strToEmailAddress;
					}

					if( false == in_array( $strToEmailAddress, $arrstrSentEmailAddresses ) ) {
						$arrstrSentEmailAddresses[] = $strToEmailAddress;
					}
				}

		}

		return;
	}

	/**
	 * Get Functions
	 */

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function getEmailType() {
		return $this->m_intEmailType;
	}

	public function getScheduledEmailType() {
		return $this->m_objScheduledEmailType;
	}

	public function getIsWeekly() {
		return $this->m_boolIsWeekly;
	}

	public function getCustomerStautsTypes() {
		return $this->m_strLeaseStatusTypes;
	}

	public function getSendHour() {
		return $this->m_strSendHour;
	}

	public function getScheduledEmailFilterName() {
		return $this->m_strScheduledEmailFilterName;
	}

	public function getScheduledEmailFilterNames() {
		return $this->m_arrstrScheduledEmailFilterNames;
	}

	public function getBlockedRecipientsCount() {
		return $this->m_intBlockedRecipientsCount;
	}

	public function getFrequencyName() {
		return $this->m_strFrequencyName;
	}

	public function getNextSendOn() {
		return $this->m_strNextSendOn;
	}

	public function getSendDate() {
		return $this->m_strSendDate;
	}

	public function getScheduledEmailFilterDeletedOn() {
		return $this->m_strScheduledEmailFilterDeletedOn;
	}

	public function getScheduledEmailEventTypeName() {
		return $this->m_strScheduledEmailEventTypeName;
	}

	public function getScheduledEmailDeliveryType() {
		return $this->m_strScheduledEmailDeliveryType;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getCompanyEmployee() {
		return $this->m_objCompanyEmployee;
	}

	public function getLeaseCustomer() {
		return $this->m_objLeaseCustomer;
	}

	public function getApPayee() {
		return $this->m_objApPayee;
	}

	public function getNumberOfOpenEmails() {
		return $this->m_intNumberOfOpenEmailsCount;
	}

	public function getNumberOfDeliveredEmails() {
		return $this->m_intNumberOfDeliveredEmails;
	}

	public function getScheduledEmailTransmissionId() {
		return $this->m_intScheduledEmailTransmissionId;
	}

	public function getIsEmailScheduled() {
		return $this->m_boolIsEmailScheduled;
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function getIsEditPermission() {
		return $this->m_boolIsEditPermission;
	}

	public function getScheduledEmailCreatedByUser() {
		return $this->m_strScheduledEmailCreatedByUser;
	}

	public function getListCreatedByUser() {
		return $this->m_strListCreatedByUser;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getScheduledEmailFilterIsShared() {
		return $this->m_intScheduledEmailFilterIsShared;
	}

	public function getScheduledEmailFilterIsShares() {
		return $this->m_arrintScheduledEmailFilterIsShares;
	}

	public function getCreatorPermissionedPropertyIds( $objDatabase ) {
		if( false == valArr( $this->m_arrintCreatorPermissionedPropertyIds ) ) {
			$this->m_arrintCreatorPermissionedPropertyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByCidByCompanyUserIdBySessionData( $this->getCid(), $this->getEmailOwnerId(), NULL, $objDatabase );
		}

		return $this->m_arrintCreatorPermissionedPropertyIds;
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchScheduledEmailAddressFilters( $objDatabase ) {
		if( false == valArr( $this->m_arrobjScheduledEmailAddressFilters ) ) {
			$this->m_arrobjScheduledEmailAddressFilters = $this->fetchScheduledEmailAddressFilters( $objDatabase );
		}

		return $this->m_arrobjScheduledEmailAddressFilters;
	}

	public function getOrFetchScheduledEmailType( $objDatabase ) {

		if( false == valObj( $this->m_objScheduledEmailType, 'CScheduledEmailType' ) ) {
			$this->fetchScheduledEmailType( $objDatabase );
		}

		return $this->m_objScheduledEmailType;
	}

	public function getOrFetchClient( $objDatabase ) {
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objDatabase );
		}

		return $this->m_objClient;
	}

	public function getOrFetchScheduledEmailFilter( $objDatabase, $boolIncludeDeleted = false ) {

		if( false == valObj( $this->m_objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) {
			if( true == $boolIncludeDeleted ) {
				$this->m_objScheduledEmailFilter = $this->fetchScheduledEmailFilterForDeletedMessage( $objDatabase );
			} else {
				$this->m_objScheduledEmailFilter = $this->fetchScheduledEmailFilter( $objDatabase );
			}
		}

		$this->m_arrobjScheduledEmailAddressFilters = $this->getOrFetchScheduledEmailAddressFilters( $objDatabase );

		$this->m_objScheduledEmailFilter->setScheduledEmailEventTypeId( $this->m_intScheduledEmailEventTypeId );
		$this->m_objScheduledEmailFilter->setIsAfterEvent( $this->m_intIsAfterEvent );
		$this->m_objScheduledEmailFilter->setDaysFromEvent( $this->m_intDaysFromEvent );
		$this->m_objScheduledEmailFilter->getOrFetchUnsubscribedRecipientIds( $objDatabase );
		$this->m_objScheduledEmailFilter->setScheduledEmailId( $this->getId() );
		$this->m_objScheduledEmailFilter->fetchPropertyPreference( $objDatabase );

		if( true == valArr( $this->m_arrobjScheduledEmailAddressFilters ) ) {

			switch( $this->m_objScheduledEmailFilter->getScheduledEmailTypeId() ) {

				case CScheduledEmailType::RESIDENTS:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'CustomerId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				case CScheduledEmailType::PROSPECTS:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'ApplicantId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				case CScheduledEmailType::EMPLOYEES:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'CompanyEmployeeId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				default:

			}
		}

		return $this->m_objScheduledEmailFilter;
	}

	public function hydrateScheduledEmailFilter( $objScheduledEmailFilter, $objDatabase ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) {
			return;
		}

		$this->m_arrobjScheduledEmailAddressFilters = $this->getOrFetchScheduledEmailAddressFilters( $objDatabase );

		$objScheduledEmailFilter->setScheduledEmailEventTypeId( $this->m_intScheduledEmailEventTypeId );
		$objScheduledEmailFilter->setIsAfterEvent( $this->m_intIsAfterEvent );
		$objScheduledEmailFilter->setDaysFromEvent( $this->m_intDaysFromEvent );
		$objScheduledEmailFilter->getOrFetchUnsubscribedRecipientIds( $objDatabase );
		$objScheduledEmailFilter->setLastSentOn( $this->getLastSentOn() );
		$objScheduledEmailFilter->setConfirmedOn( $this->getConfirmedOn() );

		if( true == valArr( $this->m_arrobjScheduledEmailAddressFilters ) ) {

			switch( $objScheduledEmailFilter->getScheduledEmailTypeId() ) {

				case CScheduledEmailType::RESIDENTS:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'CustomerId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				case CScheduledEmailType::PROSPECTS:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'ApplicantId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				case CScheduledEmailType::EMPLOYEES:
					$this->m_arrobjScheduledEmailAddressFilters = rekeyObjects( 'CompanyEmployeeId', $this->m_arrobjScheduledEmailAddressFilters );
					break;

				default:
			}
		}

		return;
	}

	public function getOrFetchBlockedRecipientsCount( $objDatabase ) {
		if( true == is_null( $this->m_intBlockedRecipientsCount ) ) {
			$this->m_intBlockedRecipientsCount = $this->fetchBlockedRecipientsCount( $objDatabase );
		}

		return $this->m_intBlockedRecipientsCount;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['send_hour'] ) ) {
			$this->setSendHour( $arrmixValues['send_hour'] );
		}

		if( true == isset( $arrmixValues['lease_status_types'] ) ) {
			$this->setCustomerStautsTypes( $arrmixValues['lease_status_types'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_filter_name'] ) ) {
			$this->setScheduledEmailFilterName( $arrmixValues['scheduled_email_filter_name'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_id'] ) ) {
			$this->setScheduledEmailId( $arrmixValues['scheduled_email_id'] );
		}

		if( true == isset( $arrmixValues['email_type'] ) ) {
			$this->setEmailType( $arrmixValues['email_type'] );
		}

		if( true == isset( $arrmixValues['frequency_name'] ) ) {
			$this->setFrequencyName( $arrmixValues['frequency_name'] );
		}

		if( true == isset( $arrmixValues['next_send_on'] ) ) {
			$this->setNextSendOn( $arrmixValues['next_send_on'] );
		}

		if( true == isset( $arrmixValues['send_date'] ) ) {
			$this->setSendDate( $arrmixValues['send_date'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_filter_deleted_on'] ) ) {
			$this->setScheduledEmailFilterDeletedOn( $arrmixValues['scheduled_email_filter_deleted_on'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_event_type_name'] ) ) {
			$this->setScheduledEmailEventTypeName( $arrmixValues['scheduled_email_event_type_name'] );
		}

		if( true == isset( $arrmixValues['blocked_recipients_count'] ) ) {
			$this->setBlockedRecipientsCount( $arrmixValues['blocked_recipients_count'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_delivery_type'] ) ) {
			$this->setScheduledEmailDeliveryType( $arrmixValues['scheduled_email_delivery_type'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_transmission_id'] ) ) {
			$this->setScheduledEmailTransmissionId( $arrmixValues['scheduled_email_transmission_id'] );
		}

		if( true == isset( $arrmixValues['is_edit_permission'] ) ) {
			$this->setIsEditPermission( $arrmixValues['is_edit_permission'] );
		}

		if( true == isset( $arrmixValues['is_published'] ) ) {
			$this->setIsPublished( $arrmixValues['is_published'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_created_by_user'] ) ) {
			$this->setScheduledEmailCreatedByUser( $arrmixValues['scheduled_email_created_by_user'] );
		}

		if( true == isset( $arrmixValues['list_created_by_user'] ) ) {
			$this->setListCreatedByUser( $arrmixValues['list_created_by_user'] );
		}

		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setUserName( $arrmixValues['username'] );
		}

		if( true == isset( $arrmixValues['scheduled_email_filter_is_shared'] ) ) {
			$this->setScheduledEmailFilterIsShared( $arrmixValues['scheduled_email_filter_is_shared'] );
		}

		if( true == isset( $arrmixValues['preheader'] ) ) {
			$this->setPreheader( $arrmixValues['preheader'] );
		}

		return;
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->m_intScheduledEmailId = $intScheduledEmailId;
	}

	public function setEmailType( $intEmailType ) {
		$this->m_intEmailType = $intEmailType;
	}

	public function setScheduledEmailFilterName( $strScheduledEmailFilterName ) {
		$this->m_strScheduledEmailFilterName = $strScheduledEmailFilterName;
	}

	public function setScheduledEmailFilterNames( $arrstrScheduledEmailFilterNames ) {
		$this->m_arrstrScheduledEmailFilterNames = $arrstrScheduledEmailFilterNames;
	}

	public function setCustomerStautsTypes( $strCustomerStautsTypes ) {
		$this->m_strLeaseStatusTypes = $strCustomerStautsTypes;
	}

	public function setScheduledEmailType( $objScheduledEmailType ) {
		$this->m_objScheduledEmailType = $objScheduledEmailType;
	}

	public function setIsWeekly( $boolIsWeekly ) {
		$this->m_boolIsWeekly = $boolIsWeekly;
	}

	public function setSendHour( $strSendHour ) {
		$this->m_strSendHour = $strSendHour;
	}

	public function setFrequencyName( $strFrequencyName ) {
		$this->m_strFrequencyName = $strFrequencyName;
	}

	public function setNextSendOn( $strNextSendOn ) {
		$this->m_strNextSendOn = $strNextSendOn;
	}

	public function setSendDate( $strSendDate ) {
		$this->m_strSendDate = $strSendDate;
	}

	public function setScheduledEmailFilterDeletedOn( $strScheduledEmailFilterDeletedOn ) {
		$this->m_strScheduledEmailFilterDeletedOn = $strScheduledEmailFilterDeletedOn;
	}

	public function setScheduledEmailEventTypeName( $strScheduledEmailEventTypeName ) {
		$this->m_strScheduledEmailEventTypeName = $strScheduledEmailEventTypeName;
	}

	public function setBlockedRecipientsCount( $intBlockedRecipientsCount ) {
		$this->m_intBlockedRecipientsCount = $intBlockedRecipientsCount;
	}

	public function setScheduledEmailDeliveryType( $strScheduledEmailDeliveryType ) {
		$this->m_strScheduledEmailDeliveryType = $strScheduledEmailDeliveryType;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setCompanyEmployee( $objCompanyEmployee ) {
		$this->m_objCompanyEmployee = $objCompanyEmployee;
	}

	public function setLeaseCustomer( $objLeaseCustomer ) {
		$this->m_objLeaseCustomer = $objLeaseCustomer;
	}

	public function setApPayee( $objApPayee ) {
		$this->m_objApPayee = $objApPayee;
	}

	public function setNumberOfOpenEmails( $intNumberOfOpenEmailsCount ) {
		$this->m_intNumberOfOpenEmailsCount = $intNumberOfOpenEmailsCount;
	}

	public function setNumberOfDeliveredEmails( $intNumberOfDeliveredEmails ) {
		$this->m_intNumberOfDeliveredEmails = $intNumberOfDeliveredEmails;
	}

	public function setScheduledEmailTransmissionId( $intScheduledEmailTransmissionId ) {
		$this->m_intScheduledEmailTransmissionId = $intScheduledEmailTransmissionId;
	}

	public function setIsEmailScheduled( $boolIsEmailScheduled ) {
		$this->m_boolIsEmailScheduled = $boolIsEmailScheduled;
	}

	public function setIsPublished( $boolIsPublished ) {
		return $this->m_boolIsPublished = $boolIsPublished;
	}

	public function setIsEditPermission( $boolIsEditPermission ) {
		$this->m_boolIsEditPermission = $boolIsEditPermission;
	}

	public function setScheduledEmailCreatedByUser( $strScheduledEmailCreatedByUser ) {
		$this->m_strScheduledEmailCreatedByUser = $strScheduledEmailCreatedByUser;
	}

	public function setListCreatedByUser( $strListCreatedByUser ) {
		return $this->m_strListCreatedByUser = $strListCreatedByUser;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setScheduledEmailFilterIsShared( $intIsShared ) {
		$this->m_intScheduledEmailFilterIsShared = $intIsShared;
	}

	public function setScheduledEmailFilterIsShares( $arrintScheduledEmailFilterIsShares ) {
		$this->m_arrintScheduledEmailFilterIsShares = $arrintScheduledEmailFilterIsShares;
	}

	/**
	 * Validate Functions
	 */

	public function valScheduledEmailTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledEmailTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_type_id', __( 'Please select the type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( 1 != $this->getSendOnLastDay() && true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Please select the Frequency.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSendHour() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStartDatetime() {

		$boolIsValid = true;
		$strStartDateValidationMsg = __( 'Start Date must be greater than {%t,0, DATETIME_NUMERIC_SHORT_PRECISE_NOTZ}', [ CInternationalDateTime::create( time(), $this->m_strMachineTimeZoneName ) ] );

		if( 0 == strlen( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start date is required.' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 1 !== CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getStartDatetime(), 0, 10 ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start Date must be in mm/dd/yyyy form.' ) ) );

		} elseif( false == is_null( $this->getId() ) && false == is_null( $this->getScheduledEmailFilterIds() ) && ( strtotime( $this->getStartDatetime() ) + 3600 * $this->getSendHour() ) < time() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strStartDateValidationMsg ) );

		} elseif( true == is_null( $this->getId() ) && ( strtotime( $this->getStartDatetime() ) + 3600 * $this->getSendHour() ) < time() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strStartDateValidationMsg ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && 1 == CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) && strtotime( $this->getEndDatetime() ) < strtotime( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start Date can not be greater then End Date' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::WEEKLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 week', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 week after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::MONTHLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 month', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 month after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::QUARTERLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+3 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 3 months after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::SEMI_ANNUALLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+6 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 6 months after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::YEARLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 year', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 year after Start date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledStartDateTime() {

		$boolIsValid = true;
		$strSentTimeIsRequiredMsg = __( 'Send time is required.' );

		$arrintScheduledEmailEventTypeIds = [
			CScheduledEmailEventType::LEAD_CREATED_DATE,
			CScheduledEmailEventType::RESIDENT_CREATED_DATE
		];

		if( 0 == strlen( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			if( CScheduledEmailDeliveryType::ONCE == $this->getScheduledEmailDeliveryTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strSentTimeIsRequiredMsg ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strSentTimeIsRequiredMsg ) );
			}
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 7 == \Psi\CStringService::singleton()->strlen( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please enter valid date.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 10 == \Psi\CStringService::singleton()->strlen( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please enter valid time.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && ( 7 != \Psi\CStringService::singleton()->strlen( $this->getStartDatetime() ) || 10 != \Psi\CStringService::singleton()->strlen( $this->getStartDatetime() ) ) && 18 > \Psi\CStringService::singleton()->strlen( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please enter valid date and time.' ) ) );
		} elseif( 0 < strlen( $this->getEndDatetime() ) && 1 == CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) && strtotime( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) < strtotime( \Psi\CStringService::singleton()->substr( $this->getStartDatetime(), 0, 10 ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start date can not be greater than end date' ) ) );
		} elseif( false == is_null( $this->getStartDatetime() ) && strtotime( $this->getStartDatetime() ) < strtotime( date( 'm/d/Y H:i:s' ) ) && ( false == in_array( $this->getScheduledEmailEventTypeId(), $arrintScheduledEmailEventTypeIds ) ) ) {
			if( CScheduledEmailDeliveryType::AUTOMATED != $this->getScheduledEmailDeliveryTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please select a future date and time to send your message' ) ) );
			}
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y H:i:s' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date must be greater than {%t, 0}', [ date( 'm/d/Y h:ia' ) ] ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::WEEKLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 week', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 week after Start date.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::MONTHLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 month', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 month after Start date.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::QUARTERLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+3 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 3 months after Start date.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::YEARLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 year', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 year after Start date.' ) ) );
		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::SEMI_ANNUALLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+6 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 6 months after Start date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledEndDateTime() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getEndDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End date is required.' ) ) );
		} elseif( 0 < strlen( $this->getEndDatetime() ) && 18 > \Psi\CStringService::singleton()->strlen( $this->getEndDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please enter valid date time.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledDaysFromEvent() {
		$boolIsValid = true;
		$strNumericDaysValidationMsg = __( 'Number of Days should be numeric.' );

		if( self::AUTOMATED == $this->getScheduledEmailDeliveryTypeId() ) {
			if( true == is_null( $this->getDaysFromEvent() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', $strNumericDaysValidationMsg ) );
			} elseif( false == preg_match( '/^[0-9]*$/', $this->getDaysFromEvent() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', $strNumericDaysValidationMsg ) );
			} elseif( 365 < $this->getDaysFromEvent() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', __( 'Number of days should be less than or equal to 365.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valEndDatetime() {

		$boolIsValid = true;

		if( 0 < strlen( $this->getEndDatetime() ) && 1 !== CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date must be in mm/dd/yyyy form.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledEmailFilterIds() {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledEmailFilterIds() ) || 0 == $this->getScheduledEmailFilterIds() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_filter_id', __( 'Recipient list is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledEmailEventTypeId() {
		$boolIsValid = true;

		if( self::AUTOMATED == $this->getEmailType() && true == is_null( $this->getScheduledEmailEventTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_event_type_id', __( 'Event Name is Required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailTemplateId() {
			$boolIsValid = true;

			if( true == is_null( $this->getSystemMessageTemplateId() ) || 0 == $this->getSystemMessageTemplateId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sytem_message_template_id', __( 'Template selection is required.' ) ) );
			}

			return $boolIsValid;
	}

 	public function valSubject() {
	 	$boolIsValid = true;

	 	if( 0 == strlen( trim( $this->getSubject() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', __( 'Subject is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDaysFromEvent() {
		$boolIsValid = true;
		$strNumericDaysValidationMsg = __( 'Number of Days should be numeric.' );

		if( self::AUTOMATED == $this->getEmailType() ) {
			if( true == is_null( $this->getDaysFromEvent() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', $strNumericDaysValidationMsg ) );
			} elseif( false == preg_match( '/^[0-9]*$/', $this->getDaysFromEvent() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', $strNumericDaysValidationMsg ) );
			} elseif( 365 < $this->getDaysFromEvent() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_from_event', __( 'Number of days should be less than or equal to 365.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valEmailType() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailType() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_type', __( 'Please Select the Type of Message.' ) ) );
		}
		return $boolIsValid;
	}

	public function valTestEmailAddress() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getTestEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_email_address', __( 'Please enter valid test email address.' ) ) );
		}

		return $boolIsValid;

	}

	public function valBccEmailAddress() {
		$boolIsValid = true;

		if( 0 < strlen( $this->getTestEmailAddress() ) ) {
			if( true == \Psi\CStringService::singleton()->strpos( $this->getTestEmailAddress(), ';' ) || true == \Psi\CStringService::singleton()->strpos( $this->getTestEmailAddress(), ':' ) || true == \Psi\CStringService::singleton()->strpos( $this->getTestEmailAddress(), ' ' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_email_address', __( 'Separate email addresses with a comma ",".' ) ) );
			} else {
				$arrstrEmailAddresses = explode( ',', str_replace( ';', ',', $this->getTestEmailAddress() ) );

				if( true == valArr( $arrstrEmailAddresses ) ) {
					foreach( $arrstrEmailAddresses as $strEmailAddress ) {
						if( false == CValidation::validateEmailAddresses( $strEmailAddress ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_email_address', __( 'Enter valid BCC email address(es).' ) ) );
							return false;
						}
					}
				}
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valMessageCenterStartDatetime() {

		$boolIsValid = true;
		$strStartDateTime = '';
		$strEndDateGreaterThanMsg = __( 'End Date must be greater than {%t,0, DATE_NUMERIC_STANDARD}', [ CInternationalDateTime::create( time(), $this->m_strMachineTimeZoneName ) ] );

		if( false == is_null( $this->getStartDatetime() ) ) {
			if( true == \Psi\CStringService::singleton()->strstr( $this->getStartDatetime(), ':' ) ) {
				$strStartDateTime = $this->getStartDatetime();
			} else {
				$strStartDateTime = $this->getStartDatetime() . ' ' . $this->getSendHour();
			}
		}

		if( false == valStr( $strStartDateTime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start date is required.' ) ) );

		} elseif( self::ONCE == $this->getEmailType() && false == is_null( $strStartDateTime ) && ( strtotime( $strStartDateTime ) < ( strtotime( date( 'm/d/Y H:i' ) ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please select a future date to send your message' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && 1 == CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) && strtotime( $this->getEndDatetime() ) < strtotime( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Start Date can not be greater then End Date' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::DAILY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::WEEKLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::WEEKLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 week', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 week after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::MONTHLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::MONTHLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 month', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 month after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::QUARTERLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::QUARTERLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+3 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 3 months after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::SEMI_ANNUALLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::SEMI_ANNUALLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+6 months', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 6 months after Start date.' ) ) );

		} elseif( 0 < strlen( $this->getEndDatetime() ) && CFrequency::YEARLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', $strEndDateGreaterThanMsg ) );

		} elseif( 0 < strlen( $this->getStartDatetime() ) && 0 < strlen( $this->getEndDatetime() ) && CFrequency::YEARLY == $this->getFrequencyId() && strtotime( $this->getEndDatetime() ) < strtotime( '+1 year', strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'End Date should be at least 1 year after Start date.' ) ) );

		} elseif( self::RECURRING == $this->getScheduledEmailDeliveryTypeId() && false == is_null( $strStartDateTime ) && ( strtotime( $strStartDateTime ) < ( strtotime( date( 'm/d/Y H:i' ) ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_datetime', __( 'Please select a future date and time to send your message' ) ) );
		}

		return $boolIsValid;
	}

	public function valMessageCenterEndDatetime() {

		$boolIsValid = true;

		if( true == is_null( $this->getEndDatetime() ) && 0 == strlen( $this->getEndDatetime() ) && ( CFrequency::ONCE != $this->getFrequencyId() && self::AUTOMATED != $this->getEmailType() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', __( 'End date is required.' ) ) );
		} elseif( 0 < strlen( $this->getEndDatetime() ) && 1 !== CValidation::checkDate( \Psi\CStringService::singleton()->substr( $this->getEndDatetime(), 0, 10 ) ) && CFrequency::ONCE != $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', __( 'End Date must be in mm/dd/yyyy form.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valScheduledEmailTypeId();
				$boolIsValid &= $this->valStartDatetime();
				$boolIsValid &= $this->valEndDatetime();
				$boolIsValid &= $this->valFrequencyId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= ( false == is_null( $this->getScheduledEmailFilterIds() ) ) ? $this->valScheduledEmailFilterIds() : true;
				$boolIsValid &= $this->valFrequencyId();
				break;

			case 'create_scheduled_email':
				$boolIsValid &= $this->valSubject();
				break;

			case 'message_center':
				$boolIsValid &= $this->valMessageCenterStartDatetime();
				$boolIsValid &= $this->valMessageCenterEndDatetime();
			case 'resuse_scheduled_email':
				$boolIsValid &= $this->valScheduledEmailFilterIds();
				$boolIsValid &= $this->valScheduledEmailEventTypeId();
				$boolIsValid &= $this->valEmailTemplateId();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valDaysFromEvent();
				$boolIsValid &= $this->valEmailType();
				break;

			case 'send_test_email':
				$boolIsValid &= $this->valTestEmailAddress();
				$boolIsValid &= $this->valSubject();
				break;

			case 'draft':
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valEmailTemplateId();
				break;

			case 'once':
				$boolIsValid &= $this->valScheduledStartDateTime();
				break;

			case 'recurring':
				$boolIsValid &= $this->valScheduledStartDateTime();
				$boolIsValid &= $this->valScheduledEndDateTime();
				break;

			case 'automated':
				$boolIsValid &= $this->valScheduledDaysFromEvent();
				$boolIsValid &= $this->valScheduledStartDateTime();
				break;

			case 'save_email':
				$boolIsValid &= $this->valScheduledEmailFilterIds();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valEmailTemplateId();
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchScheduledEmailType( $objDatabase ) {
		$this->m_objScheduledEmailType = \Psi\Eos\Entrata\CScheduledEmailTypes::createService()->fetchScheduledEmailTypeById( $this->getScheduledEmailTypeId(), $objDatabase );

		return $this->m_objScheduledEmailType;
	}

	public function fetchScheduledEmailAddressFilters( $objDatabase ) {
		return CScheduledEmailAddressFilters::createService()->fetchCustomScheduledEmailAddressFiltersByScheduledEmailIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSystemMessageTemplateCustomTextByKey( $strKey, $objDatabase ) {

		return CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextByScheduledEmailIdBySystemMessageTemplateIdByKeyByCid( $this->getId(), $this->getSystemMessageTemplateId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchPopulatedSystemMessageTemplateSlotsBySystemMessageTemplate( $objEmailTemplate, $objDatabase ) {

		return CSystemMessageTemplateSlots::createService()->fetchPopulatedSystemMessageTemplateSlotsByScheduledEmailBySystemMessageTemplate( $this, $objEmailTemplate, $objDatabase );
	}

	public function fetchSystemMessageTemplateCustomTexts( $objDatabase ) {
		return CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsByScheduledEmailIdBySystemMessageTemplateIdByCid( $this->getId(), $this->getSystemMessageTemplateId(), $this->getCid(), $objDatabase );
	}

	public function fetchSystemMessageTemplateSlotImageBySystemMessageTemplateSlotId( $intSystemMessageTemplateSlotId, $objDatabase ) {
		return CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImageByScheduledEmailIdBySystemMessageTemplateIdBySystemMessageTemplateSlotIdByCid( $this->getId(), $this->getSystemMessageTemplateId(), $intSystemMessageTemplateSlotId, $this->getCid(), $objDatabase );
	}

	public function fetchSystemMessageTemplateSlotImages( $objDatabase ) {
		return CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesByScheduledEmailIdBySystemMessageTemplateIdByCid( $this->getId(), $this->getSystemMessageTemplateId(), $this->getCid(), $objDatabase );
	}

	public function fetchClient( $objDatabase ) {

		return \Psi\Eos\Entrata\CClients::createService()->fetchSimpleClientById( $this->getCid(), $objDatabase );
	}

	public function fetchScheduledEmailFilter( $objDatabase ) {
		return CScheduledEmailFilters::createService()->fetchScheduledEmailFilterByScheduledEmailFilterIdByCid( $this->getScheduledEmailFilterIds(), $this->getCid(), $objDatabase );
	}

	public function fetchScheduledEmailFilterForDeletedMessage( $objDatabase ) {
		return CScheduledEmailFilters::createService()->fetchScheduledEmailFilterForDeletedMessageByScheduledEmailFilterIdByCid( $this->getScheduledEmailFilterIds(), $this->getCid(), $objDatabase );
	}

	public function fetchFile( $objDatabase ) {
		return CFiles::fetchFileByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );
	}

	public function fetchFiles( $objDatabase ) {
		return CFiles::fetchFilesByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );
	}

	public function fetchFileByFileTypeSystemCode( $strFileTypeSystemCode, $objDatabase ) {
		return CFiles::fetchFileByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), $strFileTypeSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchBlockedRecipientsCount( $objDatabase ) {
		return CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFiltersCountByScheduledEmailIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEmailTemplate( $objDatabase ) {
		return CSystemMessageTemplates::createService()->fetchSystemMessageTemplateById( $this->getSystemMessageTemplateId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function findNextLastSendOn() {

		$intIncreamentLastSentOn = 0;

		$intTodayTimeStamp 	= strtotime( date( 'm/d/Y' ) );

		$intIncreamentLastSentOn = strtotime( date( 'm/d/Y', strtotime( $this->getStartDatetime() ) ) );

		$arrintScheduledEmailEventTypeIds = [
			CScheduledEmailEventType::LEAD_CREATED_DATE,
			CScheduledEmailEventType::RESIDENT_CREATED_DATE
		];

		while( $intIncreamentLastSentOn <= $intTodayTimeStamp ) {

			$intNextLastSentOn = $intIncreamentLastSentOn;

			switch( $this->getFrequencyId() ) {
				case NULL:
					return NULL;

				case CFrequency::ONCE:
					$intNextLastSentOn = $intTodayTimeStamp;
					break 2;

				case CFrequency::DAILY:
				case CFrequency::LEASE_EXPIRATION:
				case CFrequency::MOVE_IN:
					$intIncreamentLastSentOn = strtotime( '+1 day', $intIncreamentLastSentOn );
					break;

				case CFrequency::WEEKLY:
					$intIncreamentLastSentOn = strtotime( '+1 week', $intIncreamentLastSentOn );
					break;

				case CFrequency::MONTHLY:
					$intIncreamentLastSentOn = strtotime( '+1 month', $intIncreamentLastSentOn );
					break;

				case CFrequency::QUARTERLY:
					$intIncreamentLastSentOn = strtotime( '+3 months', $intIncreamentLastSentOn );
					break;

				case CFrequency::SEMI_ANNUALLY:
					$intIncreamentLastSentOn = strtotime( '+6 months', $intIncreamentLastSentOn );
					break;

				case CFrequency::YEARLY:
					$intIncreamentLastSentOn = strtotime( '+1 year', $intIncreamentLastSentOn );
					break;

				default:
					trigger_error( __( 'Frequency unrecognized.' ), E_USER_ERROR );
					exit;
			}
		}

		if( true == in_array( $this->getScheduledEmailEventTypeId(), $arrintScheduledEmailEventTypeIds ) ) {
			return NULL;
		}

		if( CFrequency::MONTHLY == $this->getFrequencyId() && $intNextLastSentOn < $intTodayTimeStamp ) {
			$intNextLastSentOn = $intTodayTimeStamp;
		}

		$intNextLastSentOnMinutes = date( 'i', strtotime( $this->getStartDatetime() ) ) * 60;
		$intNextLastSentOnHour	  = date( 'H', strtotime( $this->getStartDatetime() ) ) * 3600;

		return $intNextLastSentOn + $intNextLastSentOnHour + $intNextLastSentOnMinutes;
	}

	public function getIsEditable() {

		$boolIsEditable = true;

		$strSendNowStartDate = strtotime( date( 'm/d/Y H:i', strtotime( $this->getStartDatetime() ) ) );

		if( true == valArr( $this->getErrorMsgs() ) )						return $boolIsEditable = true;
		if( true == $this->getIsDisabled() && 0 < $this->getIsDisabled() )	return $boolIsEditable = false;

		if( CScheduledEmail::AUTOMATED == $this->getScheduledEmailDeliveryTypeId() && false == is_null( $this->getConfirmedOn() ) ) {
			if( 0 < $this->getIsDisabled() ) {
				$boolIsEditable = false;
			}

			return $boolIsEditable;
		}

		switch( $this->getFrequencyId() ) {
			case CFrequency::ONCE:
				if( false == is_null( $this->getLastSentOn() ) && false == is_null( $this->getConfirmedOn() ) ) {
					$boolIsEditable &= false;
				} elseif( true == is_null( $this->getLastSentOn() ) && false == is_null( $this->getConfirmedOn() ) ) {
					if( strtotime( date( 'm/d/Y H:i' ) ) == $strSendNowStartDate ) {
						$boolIsEditable &= false;
					} elseif( strtotime( date( 'm/d/Y H:i' ) ) > $strSendNowStartDate ) {
						$boolIsEditable &= false;
					}
				}

				return $boolIsEditable;
				break;

			case CFrequency::DAILY:
				$strFrequencyDate = '+1 day';
				break;

			case CFrequency::WEEKLY:
				$strFrequencyDate = '+1 week';
				break;

			case CFrequency::MONTHLY:
				$strFrequencyDate = '+1 month';
				break;

			case CFrequency::QUARTERLY:
				$strFrequencyDate = '+3 month';
				break;

			case CFrequency::SEMI_ANNUALLY:
				$strFrequencyDate = '+6 month';
				break;

			case CFrequency::YEARLY:
				$strFrequencyDate = '+1 year';
				break;

			default:
		}

		if( 0 < strlen( $this->getStartDatetime() ) ) {

			$strStartDateTime 	= strtotime( date( 'm/d/Y H:i', strtotime( $this->getStartDatetime() ) ) );
			$strEndDateTime 	= strtotime( date( 'm/d/Y H:i', strtotime( $this->getEndDatetime() ) ) );
			$strCurrentDateTime = strtotime( date( 'm/d/Y H:i' ) );

			if( false == is_null( $this->getLastSentOn() ) && false == is_null( $this->getConfirmedOn() ) ) {

				// Expired
				if( $strCurrentDateTime > $strEndDateTime ) {
					$boolIsEditable &= false;
				}

				// Active But Will Not Send
				$strNextSendOn = date( 'm/d/Y H:i', strtotime( $strFrequencyDate, strtotime( $this->getLastSentOn() ) ) );

				if( $strCurrentDateTime < $strEndDateTime && ( $strNextSendOn > $strEndDateTime ) ) {
					$boolIsEditable &= false;
				}
			}

			if( $strStartDateTime == $strEndDateTime && ( $strCurrentDateTime > $strEndDateTime ) && ( CFrequency::DAILY == $this->getFrequencyId() ) ) {
				$boolIsEditable &= false;
			}
		}

		return $boolIsEditable;
	}

	public function getIsCancelable() {

		$boolIsCancelable = true;

		$strSendNowStartDate = strtotime( date( 'm/d/Y H:i', strtotime( $this->getStartDatetime() ) ) );

		// Already disabled
		if( false == is_null( $this->getDeletedBy() ) && false == is_null( $this->getDeletedOn() ) ) {
			return $boolIsCancelable = false;
		}

		if( true == $this->getIsDisabled() && 0 < $this->getIsDisabled() && ( false == is_null( $this->getConfirmedOn() ) && self::AUTOMATED == $this->getEmailType() ) ) {
			return $boolIsCancelable = false;
		}

		if( false == is_null( $this->getScheduledEmailEventTypeId() ) && 0 < $this->getScheduledEmailEventTypeId() && false == is_null( $this->getConfirmedOn() ) ) {
			if( 0 < $this->getIsDisabled() ) {
				return $boolIsCancelable = false;
			}
		}

		switch( $this->getFrequencyId() ) {
			case CFrequency::ONCE:

				// Already sent and disable Msg
				if( false == is_null( $this->getLastSentOn() ) || ( true == $this->getIsDisabled() && 0 < $this->getIsDisabled() ) ) {
					$boolIsCancelable &= false;
				}

				return $boolIsCancelable;
				break;

			case CFrequency::DAILY:
				$strFrequencyDate = '+1 day';
				break;

			case CFrequency::WEEKLY:
				$strFrequencyDate = '+1 week';
				break;

			case CFrequency::MONTHLY:
				$strFrequencyDate = '+1 month';
				break;

			case CFrequency::QUARTERLY:
				$strFrequencyDate = '+3 month';
				break;

			case CFrequency::SEMI_ANNUALLY:
				$strFrequencyDate = '+6 month';
				break;

			case CFrequency::YEARLY:
				$strFrequencyDate = '+1 year';
				break;

			default:
		}

		if( false == is_null( $this->getStartDatetime() ) && false == is_null( $this->getEndDatetime() ) ) {
			$strStartDateTime 	= strtotime( date( 'm/d/Y H:i', strtotime( $this->getStartDatetime() ) ) );
			$strEndDateTime 	= strtotime( date( 'm/d/Y H:i', strtotime( $this->getEndDatetime() ) ) );
			$strCurrentDateTime = strtotime( date( 'm/d/Y H:i' ) );

			if( false == is_null( $this->getLastSentOn() ) ) {

				// Expired message
				if( $strCurrentDateTime > $strEndDateTime ) {
					$boolIsCancelable &= true;
				}

				// Active for now but will expire before next send date
				$strNextSendOn = date( 'm/d/Y H:i', strtotime( $strFrequencyDate, strtotime( $this->getLastSentOn() ) ) );

				if( $strCurrentDateTime < $strEndDateTime && ( $strEndDateTime < $strNextSendOn ) ) {
					$boolIsCancelable &= false;
				}
			}

		}

		return $boolIsCancelable;
	}

	public function loadEmailEventStatistics( $objEmailDatabase ) {

		$objEmailEventStatistics = new CEmailEventStatistics();
		$strLastSentOn = date( 'Y-m-d', strtotime( $this->getLastSentOn() ) );

		$strSql = 'SELECT
						COUNT( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) AS delivered,
						COUNT( CASE WHEN ee.processed_on IS NOT NULL OR ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL OR ee.bounced_on IS NOT NULL THEN ee.id END ) AS processed,
						COUNT( ee.opened_on ) AS opened,
						COUNT( ee.clicked_on ) AS clicked,
						COUNT( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NOT NULL THEN ee.bounced_on END ) AS bounced,
						COUNT( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NULL THEN ee.deferred_on END ) AS deferred,
						COUNT( ee.spammed_on ) AS spam
					FROM
						email_events AS ee
						JOIN system_emails se ON ( se.id = ee.system_email_id AND se.cid = ee.cid )
					WHERE
						ee.cid = ' . ( int ) $this->getCid() . '
						AND se.scheduled_email_id = ' . ( int ) $this->getId() . '
						AND se.scheduled_email_transmission_id IS NULL
					GROUP BY se.scheduled_email_id';

		$arrmixResponse = fetchData( $strSql, $objEmailDatabase );

		$objEmailEventStatistics->setValues( ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse[0] : [] );

		return $objEmailEventStatistics;
	}

	public function loadEmailEventStatisticsByTransmissionId( $intScheduledEmailTransmissionId, $objEmailDatabase ) {

		$objEmailEventStatistics = new CEmailEventStatistics();
		$strLastSentOn = date( 'Y-m-d', strtotime( $this->getLastSentOn() ) );

		$strSql = 'SELECT
						COUNT( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) AS delivered,
						COUNT( CASE WHEN ee.processed_on IS NOT NULL OR ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL OR ee.bounced_on IS NOT NULL THEN ee.id END ) AS processed,
						COUNT( ee.opened_on ) AS opened,
						COUNT( ee.clicked_on ) AS clicked,
						COUNT( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NOT NULL THEN ee.bounced_on END ) AS bounced,
						COUNT( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NULL THEN ee.deferred_on END ) AS deferred,
						COUNT( ee.spammed_on ) AS spam
					FROM
						email_events AS ee
						JOIN system_emails se ON ( se.id = ee.system_email_id AND se.cid = ee.cid )
					WHERE
						ee.cid = ' . ( int ) $this->getCid() . '
						AND se.scheduled_email_id = ' . ( int ) $this->getId() . '
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
					GROUP BY se.scheduled_email_transmission_id';

		$arrmixResponse = fetchData( $strSql, $objEmailDatabase );

		$objEmailEventStatistics->setValues( ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse[0] : [] );

		return $objEmailEventStatistics;
	}

	public function getSystemMessageTemplateSlotImagesByScheduleEmail( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;
		$arrstrSystemMessageTemplateSlotImages = [];

		$arrstrSystemMessageTemplateSlotName 	= ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $objDatabase );
		$arrmixSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesByScheduledEmailIdByCid( $this->getId(), $this->getCid(),  $objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageTemplateSlotName ) ) {
				$strKey = $arrstrSystemMessageTemplateSlotName[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = [
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target']
				];
			}
		}
		return $arrstrSystemMessageTemplateSlotImages;
	}

	public function getEmailTemplate() {
		return CSystemMessageTemplates::createService()->fetchSystemMessageTemplateById( $this->getSystemMessageTemplateId(), $this->m_objDatabase );
	}

	public function getSystemMessageTemplateCustomTexts() {
		return CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsByScheduledEmailIdBySystemMessageTemplateIdByCid( $this->getId(), $this->getSystemMessageTemplateId(), $this->getCid(), $this->m_objDatabase );
	}

	public function getSystemMessageTemplateSlotImages() {
		$arrstrSystemMessageTemplateSlotImages = [];

		$arrstrSystemMessageTemplateSlotName 	= ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $this->m_objDatabase );
		$arrmixSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesByScheduledEmailIdByCid( $this->getId(), $this->getCid(),  $this->m_objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageTemplateSlotName ) ) {
				$strKey = $arrstrSystemMessageTemplateSlotName[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = [
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target']
				];
			}
		}
		return $arrstrSystemMessageTemplateSlotImages;
	}

	public function getScheduledEmailAttachments() {
		return ( array ) CFiles::fetchFilesByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $this->m_objDatabase );
	}

	public function getEmailAttachmentsByScheduleEmail( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;

		$arrmixFileAttachments = [];

		$arrobjFiles = ( array ) CFiles::fetchFilesByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );

		$intCounter = 1;
		foreach( $arrobjFiles as $objFile ) {
			$strKey = 'file' . $intCounter;
			$arrmixFileAttachments[$strKey] = [
				'id' => $objFile->getId(),
				'title' => $objFile->getTitle()
			];
			$intCounter++;
		}

		return $arrmixFileAttachments;
	}

	public function setRequiredParameters( $objSenderRecipient ) {
		$arrmixRequiredParameters = [];

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$arrmixRequiredParameters = [
					'lease_customer_id' => $objSenderRecipient->getId(),
					'lease_id' => $objSenderRecipient->getLeaseId(),
					'property_id' => $objSenderRecipient->getPropertyId(),
					'company_user_id' => $this->getEmailOwnerId(),
					'customer_id' => $objSenderRecipient->getCustomerId()
				];
				break;

			case CScheduledEmailType::PROSPECTS:
				$arrmixRequiredParameters = [
					'applicant_application_id' => $objSenderRecipient->getApplicantApplicationId(),
					'application_id' => $objSenderRecipient->getId(),
					'property_id' => $objSenderRecipient->getPropertyId(),
					'company_user_id' => $this->getEmailOwnerId()
				];
				break;

			case CScheduledEmailType::EMPLOYEES:
				$arrmixRequiredParameters = [ 'property_id' => $objSenderRecipient->getPropertyId(), 'company_user_id' => $this->getEmailOwnerId(), 'company_employee_id' => $objSenderRecipient->getId() ];
				break;

			case CScheduledEmailType::VENDORS:
				$arrmixRequiredParameters = [ 'property_id' => $objSenderRecipient->getPropertyId(), 'company_user_id' => $this->getEmailOwnerId(), 'ap_payee_id' => $objSenderRecipient->getId(), 'ap_payee_contact_id' => $objSenderRecipient->getApPayeeContactId() ];
				break;

			default:
				//
		}

		return $arrmixRequiredParameters;
	}

	public function setSystemEmailRecipientDetailsByScheduleEmailType( $objSystemEmailRecipient, $objSenderRecipient ) {

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::RESIDENT );
				$objSystemEmailRecipient->setReferenceNumber( $objSenderRecipient->getCustomerId() );
				$objSystemEmailRecipient->setEmailAddress( $objSenderRecipient->getCustomerEmailAddress() );
				break;

			case CScheduledEmailType::PROSPECTS:
				$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::LEAD );
				$objSystemEmailRecipient->setReferenceNumber( $objSenderRecipient->getApplicantId() );
				$objSystemEmailRecipient->setEmailAddress( $objSenderRecipient->getEmailAddress() );
				break;

			case CScheduledEmailType::EMPLOYEES:
				$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::COMPANY_EMPLOYEE );
				$objSystemEmailRecipient->setReferenceNumber( $objSenderRecipient->getId() );
				$objSystemEmailRecipient->setEmailAddress( $objSenderRecipient->getEmailAddress() );
				break;

			case CScheduledEmailType::VENDORS:
				$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::VENDORS );
				$objSystemEmailRecipient->setReferenceNumber( $objSenderRecipient->getId() );
				$objSystemEmailRecipient->setEmailAddress( $objSenderRecipient->getEmailAddress() );
				break;

			default:
				//
		}

		return $objSystemEmailRecipient;
	}

	public function getFromAndReplyToEmailAddressesOfMessage( $arrmixPropertyDetails, $objPropertyPrimaryEmailAddress, $objCompanyUser, $arrmixPropertyPreference = [] ) {
		$arrmixFromAndReplyToEmailAddresses = [];

		$arrmixFromAndReplyToEmailAddresses['from_email_address'] = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;
		$strPropertyName = '';

		if( false == valArr( $arrmixPropertyDetails ) ) {
			return $arrmixFromAndReplyToEmailAddresses;
		}

		$strPropertyName = convertNonAsciiCharactersToAscii( $arrmixPropertyDetails['property_name'] );

		// Check for Email Relay property setting and implement this setting for leads/residents outgoing email
		if( true == array_key_exists( 'MESSAGE_CENTER_EMAIL_RELAY', $arrmixPropertyPreference ) && 1 == $arrmixPropertyPreference['MESSAGE_CENTER_EMAIL_RELAY'] ) {

			// For 0zone company, send resident emails also as per email relay format
			$arrmixFromAndReplyToEmailAddresses['from_email_address'] = ( true == valStr( $arrmixPropertyDetails['email_relay_address'] ) ) ? $arrmixPropertyDetails['email_relay_address'] : \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $strPropertyName ) ) . '@' . CConfig::get( 'email_relay_domain' );

			if( CScheduledEmailType::RESIDENTS == $this->getScheduledEmailTypeId() ) {
				$arrmixFromAndReplyToEmailAddresses['reply_to_email_address'] = 'Message-ID: ' . \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $strPropertyName ) ) . '-' . $arrmixPropertyDetails['id'] . '-LC-CU' . $objCompanyUser->getId() . '@' . CConfig::get( 'email_relay_domain' );
			} elseif( CScheduledEmailType::PROSPECTS == $this->getScheduledEmailTypeId() ) {
				$arrmixFromAndReplyToEmailAddresses['reply_to_email_address'] = 'Message-ID: ' . \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $strPropertyName ) ) . '-' . $arrmixPropertyDetails['id'] . '-CU' . $objCompanyUser->getId() . '@' . CConfig::get( 'email_relay_domain' );
			}

			return $arrmixFromAndReplyToEmailAddresses;
		}

		if( true == array_key_exists( 'FROM_MESSAGE_CENTER_DEFAULT_EMAIL', $arrmixPropertyPreference ) && true == CValidation:: validateEmailAddresses( $arrmixPropertyPreference['FROM_MESSAGE_CENTER_DEFAULT_EMAIL'] ) ) {
			$arrmixFromAndReplyToEmailAddresses['from_email_address'] = $arrmixPropertyPreference['FROM_MESSAGE_CENTER_DEFAULT_EMAIL'];
		} elseif( true == valObj( $objPropertyPrimaryEmailAddress, 'CPropertyEmailAddress' ) && true == CValidation:: validateEmailAddresses( $objPropertyPrimaryEmailAddress->getEmailAddress() ) ) {
			$arrmixFromAndReplyToEmailAddresses['from_email_address'] = $objPropertyPrimaryEmailAddress->getEmailAddress();
		}
		$strPropertyName = preg_replace( '/\@/', '', $strPropertyName );
		$arrmixFromAndReplyToEmailAddresses['from_email_address'] = $strPropertyName . ' <' . convertNonAsciiCharactersToAscii( $arrmixFromAndReplyToEmailAddresses['from_email_address'] ) . '>';

		return $arrmixFromAndReplyToEmailAddresses;
	}

	public function getBlockedRecipientId( $objRecipient ) {
		$strBlockCustomer = '';

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$strBlockCustomer = $objRecipient->getCustomerId() . '_' . $objRecipient->getPropertyId() . '_' . ( int ) $objRecipient->getPropertyUnitId();
				break;

			case CScheduledEmailType::PROSPECTS:
				$strBlockCustomer = $objRecipient->getApplicantId();
				break;

			case CScheduledEmailType::EMPLOYEES:
				$strBlockCustomer = $objRecipient->getId();
				break;

			case CScheduledEmailType::VENDORS:
				$strBlockCustomer = $objRecipient->getApPayeeContactLocationId();
				break;

			default:
				//
		}

		return $strBlockCustomer;
	}

	public function getMessageCenterOwnerPermission( $objCompanyUser ) {
		$boolIsEmailOwnerPermission = true;

		if( 1 == $this->getDashboardResidentEmails() ) {
			return true;
		}

		if( false == $objCompanyUser->getIsAdministrator() ) {
			$arrintModuleIds       = [ CModule::MESSAGE_CENTER_RECURRING_EMAIL, CModule::MESSAGE_CENTER_AUTOMATED_EMAIL, CModule::MESSAGE_CENTER_EMAIL_ADMIN, CModule::MESSAGE_CENTER_USE_RESIDENT_LIST, CModule::MESSAGE_CENTER_USE_PROSPECT_LIST, CModule::MESSAGE_CENTER_USE_EMPLOYEE_LIST, CModule::MESSAGE_CENTER_USE_VENDOR_LIST ];
			$arrmixIsModuleAllowed = $objCompanyUser->checkModulePermissionsByModuleIds( $arrintModuleIds, $this->m_objDatabase );
			$arrstrMessages         = [];

			if( false == array_key_exists( CModule::MESSAGE_CENTER_EMAIL_ADMIN, $arrmixIsModuleAllowed ) && true == valId( $this->getScheduledEmailDeliveryTypeId() ) ) {

				if( $this->getScheduledEmailDeliveryTypeId() == CScheduledEmailDeliveryType::RECURRING && false == array_key_exists( CModule::MESSAGE_CENTER_RECURRING_EMAIL, $arrmixIsModuleAllowed ) ) {
					$boolIsEmailOwnerPermission = false;
					$arrstrMessages[] 			= __( "email owner doesn't have recurring email type permission." );
				}

				if( $this->getScheduledEmailDeliveryTypeId() == CScheduledEmailDeliveryType::AUTOMATED && false == array_key_exists( CModule::MESSAGE_CENTER_AUTOMATED_EMAIL, $arrmixIsModuleAllowed ) ) {
					$boolIsEmailOwnerPermission = false;
					$arrstrMessages[] 			= __( "email owner doesn't have automated email type permission." );
				}
			}

			switch( $this->getScheduledEmailTypeId() ) {

				case CScheduledEmailType::RESIDENTS:
					if( false == array_key_exists( CModule::MESSAGE_CENTER_USE_RESIDENT_LIST, $arrmixIsModuleAllowed ) ) {
						$boolIsEmailOwnerPermission = false;
						$arrstrMessages[] 			= __( "email owner doesn't have permissions to send to the residents recipient list type." );
					}
					break;

				case CScheduledEmailType::PROSPECTS:
					if( false == array_key_exists( CModule::MESSAGE_CENTER_USE_PROSPECT_LIST, $arrmixIsModuleAllowed ) ) {
						$boolIsEmailOwnerPermission = false;
						$arrstrMessages[] 			= __( "email owner doesn't have permissions to send to the prospects recipient list type." );
					}
					break;

				case CScheduledEmailType::EMPLOYEES:
					if( false == array_key_exists( CModule::MESSAGE_CENTER_USE_EMPLOYEE_LIST, $arrmixIsModuleAllowed ) ) {
						$boolIsEmailOwnerPermission = false;
						$arrstrMessages[] 			= __( "email owner doesn't have permissions to send to the employees recipient list type." );
					}
					break;

				case CScheduledEmailType::VENDORS:
					if( false == array_key_exists( CModule::MESSAGE_CENTER_USE_VENDOR_LIST, $arrmixIsModuleAllowed ) ) {
						$boolIsEmailOwnerPermission = false;
						$arrstrMessages[] 			= __( "email owner doesn't have permissions to send to the vendors recipient list type." );
					}
					break;

				default:
			}
		}

		if( false == $boolIsEmailOwnerPermission ) {
			$this->addErrorMsgs( $arrstrMessages );
		}

		return $boolIsEmailOwnerPermission;
	}

	public function getEnabledProperties( $arrobjScheduledEmailFilters ) {
		$strProperties = '';

		foreach( $arrobjScheduledEmailFilters as $objScheduledEmailFilter ) {
			$strProperties .= ( false == is_null( $objScheduledEmailFilter->getProperties() ) ) ? trim( $objScheduledEmailFilter->getProperties(), ',' ) . ',' : '';
		}

		$arrintPropertyIds = ( true == valStr( $strProperties ) ) ? explode( ',', trim( $strProperties, ',' ) ) : [];
		$arrobjProperties  = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByPsProductIdByCid( $arrintPropertyIds, CPsProduct::MESSAGE_CENTER, $this->getCid(), $this->m_objDatabase );

		return $arrobjProperties;
	}

	public function getRecipientsReGroupedArray( $arrobjRecipients ) {
		$arrintCustomerIds 				= [];
		$arrobjRecipientsByPropertyId 	= [];
		$arrintPropertyIds 				= [];
		$strMethodName 					= $this->getRecipientMethodName();

		foreach( $arrobjRecipients as $objRecipient ) {

			$strToEmailAddress = '';
			if( CScheduledEmailType::RESIDENTS == $this->getScheduledEmailTypeId() ) {
				$strToEmailAddress = $objRecipient->getCustomerEmailAddress();
			} else {
				$strToEmailAddress = $objRecipient->getEmailAddress();
			}

			if( false == CValidation:: validateEmailAddresses( $strToEmailAddress ) ) {
				continue;
			}

			$arrobjRecipientsByPropertyId[$objRecipient->getPropertyId()][$objRecipient->$strMethodName()] = $objRecipient;
			$arrintPropertyIds[$objRecipient->getPropertyId()] = $objRecipient->getPropertyId();

			if( CScheduledEmailType::RESIDENTS == $this->getScheduledEmailTypeId() ) {
				$arrintCustomerIds[$objRecipient->getCustomerId()]	= $objRecipient->getCustomerId();
			}
		}

		return [ $arrobjRecipientsByPropertyId, $arrintPropertyIds, $arrintCustomerIds ];
	}

	public function getRecipientMethodName() {
		$strMethodName = '';
		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$strMethodName = 'getCustomerId';
				break;

			case CScheduledEmailType::PROSPECTS:
				$strMethodName = 'getApplicantId';
				break;

			case CScheduledEmailType::EMPLOYEES:
				$strMethodName = 'getId';
				break;

			case CScheduledEmailType::VENDORS:
				$strMethodName = 'getApPayeeContactLocationId';
				break;

			default:
				//
		}

		return $strMethodName;
	}

}
?>