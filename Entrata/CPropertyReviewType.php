<?php

class CPropertyReviewType extends CBasePropertyReviewType {

	protected $m_strUserName;
	protected $m_strReviewTypeDisplayName;
	protected $m_strReviewSource;

	/**
	 * Get Functions
	 *
	 */

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getReviewTypeDisplayName() {
		return $this->m_strReviewTypeDisplayName;
	}

	public function getReviewSource() {
		return $this->m_strReviewSource;
	}

	/**
	 * Set Function
	 *
	 */

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setReviewTypeDisplayName( $strReviewTypeDisplayName ) {
		$this->m_strReviewTypeDisplayName = $strReviewTypeDisplayName;
	}

	public function setReviewSource( $strReviewSource ) {
		$this->m_strReviewSource = $strReviewSource;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['display_name'] ) ) {
			$this->setReviewTypeDisplayName( $arrmixValues['display_name'] );
		}

		if( true == isset( $arrmixValues['source'] ) ) {
			$this->setReviewSource( $arrmixValues['source'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalListingRemoteKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>