<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGroupPermissions
 * Do not add any new functions to this class.
 */

class CDefaultGroupPermissions extends CBaseDefaultGroupPermissions {

	public static function fetchDefaultGroupPermissionsByGroupSystemCode( $strGroupSystemCode, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    default_group_permissions
					WHERE
					    group_system_code = \'' . $strGroupSystemCode . '\'';

		return parent::fetchDefaultGroupPermissions( $strSql, $objDatabase );
	}

	public static function updateSequence( $objClientDatabase ) {

		$strSql = 'SELECT setval(\'public.default_group_permissions_id_seq\', (
																				SELECT 
																					MAX(id)
																				FROM
																					public.default_group_permissions
		) );';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

}
?>