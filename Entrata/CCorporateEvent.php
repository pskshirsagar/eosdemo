<?php

class CCorporateEvent extends CBaseCorporateEvent {

	protected $m_intScreeningDecisionTypeId;
	protected $m_intScreeningRecommendationTypeId;

	protected $m_boolIsDecisionAutoProcessed;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorporateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventHandle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDeleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->m_intScreeningDecisionTypeId = $intScreeningDecisionTypeId;
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->m_intScreeningRecommendationTypeId = $intScreeningRecommendationTypeId;
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function setIsDecisionAutoProcessed( $boolIsDecisionAutoProcessed ) {
		$this->m_boolIsDecisionAutoProcessed = $boolIsDecisionAutoProcessed;
	}

	public function getIsDecisionAutoProcessed() {
		return $this->m_boolIsDecisionAutoProcessed;
	}

}
?>