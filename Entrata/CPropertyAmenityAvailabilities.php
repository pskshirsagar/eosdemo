<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAmenityAvailabilities
 * Do not add any new functions to this class.
 */

class CPropertyAmenityAvailabilities extends CBasePropertyAmenityAvailabilities {

	public static function fetchPropertyAmenityAvailabilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::ADD_ONS ] );
		$objRateLogsFilter->setPropertyRatesOnly( true );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = ' SELECT
						a.name,
						paa.id,
						ao.id as ar_origin_reference_id,
						paa.rate_association_id,
						ra.cid,
						ra.property_id,
						paa.availability,
						pr.rate_amount,
						a.id as amenity_id,
						paa.charge_resident_automatically,
						paa.require_payment_immediately,
						paa.require_property_approval,
						paa.max_reservation_time_limit,
						ra.is_published,
						paa.is_reserved_by_day,
						paa.rp_description,
						paa.amenity_availability,
						paa.is_reserved_by_hours_by_day,
						paa.hide_amenity,
						(
							SELECT
								COALESCE(COUNT(par.id), 0) AS counts
							FROM
								property_add_on_reservations par
								JOIN property_amenity_availabilities paa ON(par.rate_association_id = paa.rate_association_id AND par.cid = paa.cid)
							 WHERE
								par.cid = ' . ( int ) $intCid . '
								AND par.property_id = ra.property_id
								AND par.amenity_reservation_status_type_id IN ( ' . CAddOnReservationStatusType::PENDING . ', ' . CAddOnReservationStatusType::APPROVED . ' )
								AND par.reservation_start >= CURRENT_DATE
								AND par.rate_association_id = ra.id
						) AS reservations_count,
						paa.is_future_allowed,
						paa.reservable_within_days,
						(   
							SELECT
						        ras.marketing_media_association_id
						    FROM
						        rate_associations ras
						    WHERE
						        ras.cid = ' . ( int ) $intCid . '
								AND ras.property_id = ' . ( int ) $intPropertyId . '
								AND ras.ar_cascade_id = ' . CArCascade::PROPERTY . '
								AND ras.ar_cascade_reference_id =' . ( int ) $intPropertyId . '
								AND ras.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
								AND ras.ar_origin_reference_id = a.id
						    LIMIT 1 
						) as marketing_media_association_id
					FROM
						rate_associations AS ra
						JOIN add_ons AS ao ON( ao.id = ra.ar_origin_reference_id AND ao.cid = ra.cid )
						JOIN add_on_groups AS aog ON( aog.id = ao.add_on_group_id AND aog.cid = ra.cid )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id = aog.property_id OR aoc.property_id IS NULL ) )
						JOIN property_amenity_availabilities paa ON(ra.id = paa.rate_association_id AND ra.cid = paa.cid)
						JOIN amenities a ON( a.id = paa.amenity_id AND a.cid = ra.cid )
						LEFT JOIN prospect_rates pr ON( ao.id = pr.ar_origin_reference_id AND ao.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
						AND aog.add_on_type_id = ' . CAddOnType::RESERVATIONS . '
						AND aoc.default_add_on_category_id = ' . CDefaultAddOnCategory::RESERVATIONS . '
					ORDER BY
						ra.id';

		return parent::fetchObjects( $strSql, 'CPropertyAmenityAvailability', $objDatabase );
	}

	public static function fetchUnusedPropertyAmenitiesByPropertyId( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.id as rate_association_id,
						ra.property_id,
						ra.cid,
						a.id as amenity_id,
						a.name,
						ra.description,
						ra.is_published
					FROM
						rate_associations AS ra
						JOIN amenities a ON(a.id = ra.ar_origin_reference_id AND ra.cid = a.cid)
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND a.id NOT IN( SELECT amenity_id FROM property_amenity_availabilities WHERE property_id = ' . ( int ) $intPropertyId . 'AND cid = ' . ( int ) $intCid . ' )
					GROUP BY
						ra.id,
						ra.property_id,
						ra.cid,
						a.id,
						a.name,
						ra.description,
						ra.is_published';

		return parent::fetchObjects( $strSql, 'CPropertyAmenityAvailability', $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityDetailsByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						paa.*,
						a.name
					FROM
						property_amenity_availabilities paa
						JOIN amenities a ON( a.id = paa.amenity_id AND a.cid = paa.cid )
					WHERE
						paa.cid = ' . ( int ) $intCid . '
						AND paa.id = ' . ( int ) $intId;

		return parent::fetchPropertyAmenityAvailability( $strSql, $objDatabase );
	}

	public static function fetchAmenityBlockDaysByRateAssociationIdByPropertyIdByCid( $intRateAssociationId, $intPropertyId, $intCid, $objDatabase ) {

		$strMinDate = date( 'Y-m-d' );
		$strMaxDate = date( 'Y-m-d', strtotime( '+13 months', strtotime( $strMinDate ) ) );

		$strSql = 'WITH times AS
					(
						SELECT
							i AS "time",
							EXTRACT ( HOUR FROM i )::INTEGER AS hour,
							trim( to_char ( i, \'day\' ) )::text AS days
						FROM
							generate_series( \'' . $strMinDate . ' 00:00:00\'::timestamp,
											\'' . $strMaxDate . ' 00:00:00\'::timestamp - INTERVAL \'1 hour\',
											\'1 hour\'
											) i
					)
					SELECT
						times.time,
						CASE
							WHEN ( 1 = ( paa.amenity_availability->times.days->times.hour )::TEXT::INTEGER AND par.id IS NOT NULL )
							THEN 0
							WHEN ( 0 = ( paa.amenity_availability->times.days->times.hour )::TEXT::INTEGER AND par.id IS NOT NULL )
							THEN 0
							WHEN ( 1 = ( paa.amenity_availability->times.days->times.hour )::TEXT::INTEGER AND par.id IS NULL )
							THEN 0
							ELSE 1
						END AS available,
						par.reservation_start
					FROM
						times
					CROSS JOIN
						property_amenity_availabilities paa
					LEFT JOIN
						property_add_on_reservations par
						ON par.rate_association_id = paa.rate_association_id
						AND par.cid = paa.cid
						AND par.reservation_start <= times.time
						AND par.reservation_end > times.time
						AND par.amenity_reservation_status_type_id NOT IN( ' . CAddOnReservationStatusType::CANCELED . ', ' . CAddOnReservationStatusType::REJECTED . ' )
					WHERE
						paa.rate_association_id = ' . ( int ) $intRateAssociationId . '
						AND paa.property_id = ' . ( int ) $intPropertyId . '
						AND paa.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAmenityAvailabilitiesByPropertyIdByAmenityIdByDateByCid( $intPropertyId, $intRateAssociationId, $strDate, $intCid, $strPropertyTimeZone, $objDatabase, $boolIsYearDateRange = false, $strSplitTime = '1 hour' ) {

		if( true == $boolIsYearDateRange ) {
			$intWeekDay = date( 'w' );
			$strDate = date( 'm/d/Y', strtotime( '-' . $intWeekDay . ' days' ) );
			$strEndDate = date( 'm/d/Y', strtotime( '+6 months' ) );
		} else {
			$strEndDate = $strDate;
		}

		$strSql = 'WITH times AS
					(
						SELECT
							i AS "time",
							EXTRACT ( HOUR FROM i )::INTEGER AS hour,
							trim( to_char ( i, \'day\' ) )::text AS days
						FROM
							generate_series( \'' . $strDate . ' 00:00:00\'::timestamp,
											\'' . $strEndDate . '  00:00:00\'::timestamp + INTERVAL \'1 day\' - INTERVAL \'' . $strSplitTime . '\',
											\'' . $strSplitTime . '\'
											) i
					)
					SELECT
						DISTINCT
						times.time,
						CASE
						WHEN times.time < to_char(now() AT TIME ZONE \'' . $strPropertyTimeZone . '\', \'mm/dd/YYYY HH12:00:00 AM\')::timestamp + INTERVAL \'' . $strSplitTime . '\'
						THEN 0
						WHEN 1 <> ( paa.amenity_availability->times.days->times.hour )::TEXT::INTEGER
						AND par.id IS NULL
						THEN 1
						ELSE 0
						END AS available,
						CASE
 							WHEN ( times.time >=( ( par.reservation_start::timestamp AT time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' ) AT time zone \'' . $strPropertyTimeZone . '\' ) AND times.time <= ( ( par.reservation_end::timestamp AT time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' ) AT time zone \'' . $strPropertyTimeZone . '\' )  ) THEN 1
							ELSE 0
						END AS reserved,
						paa.quantity,
						par.id as reservation_id
					FROM
						times
					CROSS JOIN
						property_amenity_availabilities paa
						JOIN rate_associations ra ON( ra.id = paa.rate_association_id AND paa.cid = ra.cid )
					LEFT JOIN
						property_add_on_reservations par
						ON( par.rate_association_id = paa.rate_association_id
						AND par.cid = paa.cid
						AND ( ( par.reservation_start::timestamp AT time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' ) AT time zone \'' . $strPropertyTimeZone . '\' ) <= times.time
						AND ( ( par.reservation_end::timestamp AT time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' ) AT time zone \'' . $strPropertyTimeZone . '\' ) > times.time
						AND par.amenity_reservation_status_type_id NOT IN( ' . CAddOnReservationStatusType::CANCELED . ', ' . CAddOnReservationStatusType::REJECTED . ' ) )
					WHERE
						paa.rate_association_id = ' . ( int ) $intRateAssociationId . '
						AND paa.cid = ' . ( int ) $intCid . '
						AND paa.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						times.time';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAmenityAvailabilitiesByPropertyIdByAmenityIdsByDateByCid( $intPropertyId, $intRateAssociationIds, $strDate, $intCid, $strPropertyTimeZone, $objDatabase, $boolIsYearDateRange = false ) {
		if( false == valArr( $intRateAssociationIds ) ) return NULL;
		if( true == $boolIsYearDateRange ) {
			$intWeekDay = date( 'w' );
			$strDate = date( 'm/d/Y', strtotime( '-' . $intWeekDay . ' days' ) );
			$strEndDate = date( 'm/d/Y', strtotime( '+6 months' ) );
		} else {
			$strEndDate = $strDate;
		}

		$strSql = 'DROP TABLE IF EXISTS times;';

		$strSql .= 'CREATE temp TABLE times AS (
						SELECT
								i AS "time", i AS "day",
								EXTRACT ( HOUR FROM i )::INTEGER AS hour,
								trim( to_char ( i, \'day\' ) )::text AS days
							FROM
								generate_series( \'' . $strDate . ' 00:00:00\'::timestamp,
									\'' . $strEndDate . '  00:00:00\'::timestamp + INTERVAL \'1 day\' - INTERVAL \'1 hour\',
									\'1 hour\'
								) i
						);';

		$strSql .= 'CREATE INDEX times_time ON times( time );';

		$strSql .= 'WITH rer AS (
							SELECT
								ra.id as rate_association_id,
								paor.id as reservation_id,
								paor.reserved_by_day,
								paor.reservation_start,
								paor.reservation_end,
								paa.quantity,
								paor.reservation_start::timestamp at time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' AS reservation_start_time,
								paor.reservation_end::timestamp at time zone \'' . CTimeZone::DEFAULT_TIME_ZONE_NAME . '\' AS reservation_end_time
							FROM
								rate_associations ra
							JOIN property_amenity_availabilities paa ON
								paa.cid = ra.cid
								and paa.rate_association_id = ra.id
							LEFT JOIN property_add_on_reservations paor ON
								paor.rate_association_id = paa.rate_association_id
								and paor.cid = paa.cid
								and paor.amenity_reservation_status_type_id NOT IN( ' . CAddOnReservationStatusType::CANCELED . ', ' . CAddOnReservationStatusType::REJECTED . ' )
							WHERE
								ra.cid = ' . ( int ) $intCid . '
								AND paa.property_id = ' . ( int ) $intPropertyId . '
								AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
								AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
								AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
								AND ra.id IN ( ' . implode( ',', $intRateAssociationIds ) . ')
						 )
						
					SELECT
						t.time,
						rer.*
					FROM
						times t
					     JOIN rer ON t.time BETWEEN rer.reservation_start_time AND rer.reservation_end_time
					WHERE
						t.time >= NOW() - INTERVAL \'1 day\' + INTERVAL \'1 hour\'
						ORDER BY t.time;
					    ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						count( paa.id )
					FROM
						rate_associations AS ra
						JOIN add_ons ao ON( ao.id = ra.ar_origin_reference_id AND ao.cid = ra.cid )
						JOIN property_amenity_availabilities paa ON(ra.id = paa.rate_association_id AND ra.cid = paa.cid)
						JOIN amenities a ON(a.id = paa.amenity_id AND a.cid = paa.cid)
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintResponse ) ) ? $arrintResponse[0]['count'] : NULL;
	}

	public static function fetchPropertyAmenityAvailabilitiesBlackoutHolidaysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return parent::fetchPropertyAmenityAvailabilities( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE property_id = %d AND cid = %d AND blackout_holidays IS NOT NULL', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArOriginReferenceIdByAmenityIdByPropertyIdByCid( $intAmenityId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intAmenityId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
					   ra.ar_origin_reference_id as ar_origin_reference_id
				   FROM
					   property_amenity_availabilities paa
					   JOIN rate_associations ra ON ( ra.id = paa.rate_association_id AND ra.ar_cascade_reference_id = paa.property_id AND ra.cid = paa.cid )
					 WHERE
					   paa.amenity_id = ' . ( int ) $intAmenityId . '
					   AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
					   AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
					   AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
					   AND ra.cid = ' . ( int ) $intCid;
		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintResponse ) ) ? $arrintResponse[0]['ar_origin_reference_id'] : 0;

	}

	public static function fetchPropertyAmenityAvailabilityByAmenityIdByPropertyIdByCid( $intAmenityId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailability( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE amenity_id = %d AND property_id = %d AND cid = %d', ( int ) $intAmenityId, ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityByRateAssociationIdByPropertyIdByCid( $intRateAssociationId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailability( sprintf( 'SELECT * FROM property_amenity_availabilities WHERE rate_association_id = %d AND property_id = %d AND cid = %d', ( int ) $intRateAssociationId, ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilitiesByIdByPropertyIdByCid( $intAmenityAbilityId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intAmenityAbilityId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						paa.id,
						paa.amenity_availability,
						is_reserved_by_day,
						is_reserved_by_hours_by_day
					FROM
						property_amenity_availabilities paa
					WHERE
						paa.id = ' . ( int ) $intAmenityAbilityId . '
						AND paa.cid = ' . ( int ) $intCid . '
						AND paa.property_id = ' . ( int ) $intPropertyId . ' LIMIT 1';

		$arrmixResponse = ( array ) fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse[0] : '';
	}

	public static function fetchPropertyAmenityAvailabilitiesBlackoutHolidaysByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_amenity_availabilities
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND cid = ' . ( int ) $intCid . '
						AND blackout_holidays IS NOT NULL';

		return self::fetchObjects( $strSql, 'CPropertyAmenityAvailability', $objDatabase );
	}

}?>