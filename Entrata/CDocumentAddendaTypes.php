<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAddendaTypes
 * Do not add any new functions to this class.
 */

class CDocumentAddendaTypes extends CBaseDocumentAddendaTypes {

	public static function fetchDocumentAddendaTypesByDocumentAddendaTypeIdById( $intDocumentAddendaTypeId, $intId, $objDatabase ) {
		$strSql = 'SELECT * FROM document_addenda_types WHERE document_addenda_type_id =' . ( int ) $intDocumentAddendaTypeId . ' AND id <> ' . ( int ) $intId;

		return self::fetchDocumentAddendaTypes( $strSql, $objDatabase );
	}
}
?>