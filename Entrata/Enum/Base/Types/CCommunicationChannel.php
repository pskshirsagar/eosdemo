<?php

class CCommunicationChannel {

	const EMAIL = 'email';
	const PHONE = 'phone';
	const SMS_MARKETING = 'sms_marketing';
	const SMS_TRANSACTIONAL = 'sms_transactional';
	const POSTAL_MAIL = 'postal_mail';
	const CUSTOMER_LOCKDOWN = 'customer_lockdown';
}
?>