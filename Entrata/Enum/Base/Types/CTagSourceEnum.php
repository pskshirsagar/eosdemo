<?php

class CTagSourceEnum {

	const COMMERCIAL             = 'COMMERCIAL';
	const COMMERCIAL_LEASE_TYPES = 'COMMERCIAL_LEASE_TYPES';
	const TENANT_HEALTH          = 'TENANT_HEALTH';
	const USER_DEFINED           = 'USER_DEFINED';
	const EVENT_TYPES            = 'EVENT_TYPES';
}
?>