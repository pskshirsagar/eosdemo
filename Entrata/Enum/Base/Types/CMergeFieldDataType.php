<?php

class CMergeFieldDataType {

	const BOOLEAN = 'boolean';
	const CURRENCY = 'currency';
	const DATE = 'date';
	const DATETIME = 'datetime';
	const EMAIL = 'email';
	const HYPERLINK = 'hyperlink';
	const NUMERIC = 'numeric';
	const PERCENT = 'percent';
	const PHONE = 'phone';
	const TAXID_MASKED = 'taxid_masked';
	const TEXT = 'text';
	const POSTAL_CODE = 'postal_code';

}
?>