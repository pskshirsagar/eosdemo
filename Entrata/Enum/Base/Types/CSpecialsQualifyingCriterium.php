<?php

class CSpecialsQualifyingCriterium {

	const MOVE_IN = 'move_in';
	const APPLICATION_COMPLETED = 'application_completed';
	const RENEWAL_LEASE_START_DATE = 'renewal_lease_start_date';
	const RENEWAL_OFFER_COMPLETED = 'renewal_offer_completed';

}
?>