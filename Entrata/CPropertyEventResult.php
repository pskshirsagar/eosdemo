<?php

class CPropertyEventResult extends CBasePropertyEventResult {

	protected $m_strName;
	protected $m_intEventResultTypeId;
	protected $m_strDefaultEventResultName;
	protected $m_boolIsSystem;

    public function setDefaults() {
    	$this->m_strName 			= NULL;

	    return;
	}

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
        if( true == isset( $arrmixValues['name'] ) )		$this->setName( $arrmixValues['name'] );
        if( true == isset( $arrmixValues['event_result_type_id'] ) )	$this->setEventResultTypeId( $arrmixValues['event_result_type_id'] );
        if( true == isset( $arrmixValues['default_event_result_name'] ) )	$this->setDefaultEventResultName( $arrmixValues['default_event_result_name'] );
        if( true == isset( $arrmixValues['is_system'] ) )	$this->setIsSystem( $arrmixValues['is_system'] );
	    if( true == isset( $arrmixValues['event_type_id'] ) )	$this->setEventTypeId( $arrmixValues['event_type_id'] );

        return;
    }

   public function setDefaultEventResultName( $strDefaultEventResultName ) {
   	$this->m_strDefaultEventResultName = $strDefaultEventResultName;
   }

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setEventResultTypeId( $intEventResultTypeId ) {
    	$this->m_intEventResultTypeId = $intEventResultTypeId;
    }

    public function setIsSystem( $boolIsSystem ) {
    	$this->m_boolIsSystem = $boolIsSystem;
    }

	public function setEventTypeId( $intEventTypeId ) {
		$this->m_intEventTypeId = $intEventTypeId;
	}

    /**
     * Get Functions
     */

    public function getName() {
    	return $this->m_strName;
    }

    public function getIsSystem() {
        return $this->m_boolIsSystem;
    }

    public function getEventResultTypeId() {
    	return $this->m_intEventResultTypeId;
    }

    public function getDefaultEventResultName() {
    	return $this->m_strDefaultEventResultName;
    }

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valEventResultId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEventResultId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_result_id', __( 'Event result is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valEventResultId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>