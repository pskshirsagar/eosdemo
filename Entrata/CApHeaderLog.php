<?php

class CApHeaderLog extends CBaseApHeaderLog {

	const ACTION_IMPORTED							= 'Imported';
	const ACTION_CREATED							= 'Created';
	const ACTION_EDITED								= 'Edited';
	const ACTION_POSTED								= 'Approved and Posted';
	const ACTION_UNPOSTED							= 'Unposted';
	const ACTION_APPROVED							= 'Approved';
	const ACTION_UNAPPROVED							= 'Unapproved';
	const ACTION_CLOSED								= 'Closed';
	const ACTION_OPENED                             = 'Opened';
	const ACTION_DELETED							= 'Deleted';
	const ACTION_REVERSED							= 'Reversed';
	const ACTION_PAID								= 'Paid';
	const ACTION_VOIDED								= 'Voided';
	const ACTION_FINAL_APPROVAL						= 'Approved for Payment';
	const ACTION_FINAL_APPROVAL_PAYMENT				= 'Final Approval for Payment';
	const ACTION_FINAL_APPROVAL_PO					= 'Final Approval';
	const ACTION_RETENTION_RELEASED				    = 'Retention Released';
	const ACTION_RETURNED_TO_PREVIOUS				= 'Returned To Previous';
	const ACTION_RETURNED_TO_BEGINNING				= 'Returned To Beginning';
	const ACTION_ITEMS_RECEIVED						= 'Items Received';
	const ACTION_ITEMS_RETURNED						= 'Items Returned';
	const ACTION_ITEMS_STATUS_UPDATED				= 'Items Status Updated';
	const ACTION_MOVED_TO_UNCLAIMED_PROPERTY		= 'Moved to Unclaimed Property';
	const ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY	= 'Removed from Unclaimed Property';
	const ACTION_ATTACHMENT_ADDED					= 'Attachment added';
	const ACTION_ATTACHMENT_REMOVED					= 'Attachment removed';
	const ACTION_URL_ADDED							= 'URL added';
	const ACTION_URL_REMOVED						= 'URL removed';
	const ACTION_REJECTED							= 'Rejected';
	const ACTION_RE_SUBMITTED						= 'Re-Submitted';
	const ACTION_ITEMS_CANCELLED					= 'Items Cancelled';
	const ACTION_ENABLED							= 'Enabled';
	const ACTION_DISABLED							= 'Disabled';
	const ACTION_VENDOR_1099_SETTING_UPDATED		= 'Vendor/Location 1099 setting updated';
	const ACTION_RESUBMITTED						= 'Resubmitted';
	const ACTION_ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS	= 'Allow Multiple Invoices for Standard PO Line Items';

	protected $m_intLocationsApRemittanceId;
	protected $m_intScheduledApTransactionHeaderId;
	protected $m_intScheduledApTransactionHeaderNumber;

	protected $m_fltTransactionAmount;

	protected $m_strCity;
	protected $m_strCountry;
	protected $m_strRemittanceName;
	protected $m_strRemittanceType;
	protected $m_strProvince;
	protected $m_strStateCode;
	protected $m_strPayeeName;
	protected $m_strPostalCode;
	protected $m_strApPayeeTerm;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strRefundCustomerNames;
	protected $m_strCompanyEmployeeFullName;

	public static $c_arrstrApHeaderLogActions = [
		self::ACTION_IMPORTED							=> self::ACTION_IMPORTED,
		self::ACTION_CREATED							=> self::ACTION_CREATED,
		self::ACTION_EDITED								=> self::ACTION_EDITED,
		self::ACTION_POSTED								=> self::ACTION_POSTED,
		self::ACTION_UNPOSTED							=> self::ACTION_UNPOSTED,
		self::ACTION_APPROVED							=> self::ACTION_APPROVED,
		self::ACTION_UNAPPROVED							=> self::ACTION_UNAPPROVED,
		self::ACTION_DELETED							=> self::ACTION_DELETED,
		self::ACTION_REVERSED							=> self::ACTION_REVERSED,
		self::ACTION_PAID								=> self::ACTION_PAID,
		self::ACTION_VOIDED								=> self::ACTION_VOIDED,
		self::ACTION_FINAL_APPROVAL						=> self::ACTION_FINAL_APPROVAL,
		self::ACTION_FINAL_APPROVAL_PAYMENT				=> self::ACTION_FINAL_APPROVAL_PAYMENT,
		self::ACTION_FINAL_APPROVAL_PO					=> self::ACTION_FINAL_APPROVAL_PO,
		self::ACTION_RETURNED_TO_PREVIOUS				=> self::ACTION_RETURNED_TO_PREVIOUS,
		self::ACTION_RETURNED_TO_BEGINNING				=> self::ACTION_RETURNED_TO_BEGINNING,
		self::ACTION_MOVED_TO_UNCLAIMED_PROPERTY		=> self::ACTION_MOVED_TO_UNCLAIMED_PROPERTY,
		self::ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY	=> self::ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY,
		self::ACTION_RETENTION_RELEASED					=> self::ACTION_RETENTION_RELEASED,
		self::ACTION_ATTACHMENT_ADDED					=> self::ACTION_ATTACHMENT_ADDED,
		self::ACTION_ATTACHMENT_REMOVED					=> self::ACTION_ATTACHMENT_REMOVED,
		self::ACTION_REJECTED							=> self::ACTION_REJECTED,
		self::ACTION_URL_ADDED							=> self::ACTION_URL_ADDED,
		self::ACTION_URL_REMOVED						=> self::ACTION_URL_REMOVED,
		self::ACTION_RE_SUBMITTED						=> self::ACTION_RE_SUBMITTED,
		self::ACTION_RESUBMITTED						=> self::ACTION_RESUBMITTED,
		self::ACTION_ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS	=> self::ACTION_ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS
	];

	public function mapApHeaderLog( $objApHeader ) {

		$this->setCid( $objApHeader->getCid() );
		$this->setApHeaderId( $objApHeader->getId() );
		$this->setApHeaderTypeId( $objApHeader->getApHeaderTypeId() );
		$this->setApHeaderSubTypeId( $objApHeader->getApHeaderSubTypeId() );
		$this->setApHeaderModeId( $objApHeader->getApHeaderModeId() );
		$this->setApPhysicalStatusTypeId( $objApHeader->getApPhysicalStatusTypeId() );
		$this->setApFinancialStatusTypeId( $objApHeader->getApFinancialStatusTypeId() );
		$this->setApPayeeId( $objApHeader->getApPayeeId() );
		$this->setApPayeeLocationId( $objApHeader->getApPayeeLocationId() );
		$this->setApPayeeAccountId( $objApHeader->getApPayeeAccountId() );
		$this->setApRemittanceId( $objApHeader->getApRemittanceId() );
		$this->setApPayeeTermId( $objApHeader->getApPayeeTermId() );
		$this->setGlTransactionTypeId( $objApHeader->getGlTransactionTypeId() );
// 		$this->setInterCompanyTypeId( $objApHeader->getInterCompanyTypeId() );
// 		$this->setInterCompanyStageId( $objApHeader->getInterCompanyStageId() );
		$this->setBulkBankAccountId( $objApHeader->getBulkBankAccountId() );
		$this->setBulkPropertyId( $objApHeader->getBulkPropertyId() );
		$this->setBulkApFormulaId( $objApHeader->getBulkApFormulaId() );
// 		$this->setBulkApCodeId( $objApHeader->getBulkApCodeId() );
		$this->setBulkUnitNumberId( $objApHeader->getBulkUnitNumberId() );
		$this->setBulkCompanyDepartmentId( $objApHeader->getBulkCompanyDepartmentId() );
		$this->setBulkGlDimensionId( $objApHeader->getBulkGlDimensionId() );
// 		$this->setBulkJobPhaseApHeaderId( $objApHeader->getBulkJobPhaseApHeaderId() );
// 		$this->setBulkContractApHeaderId( $objApHeader->getBulkContractApHeaderId() );
		$this->setBudgetChangeOrderId( $objApHeader->getBudgetChangeOrderId() );

		if( true == valIntArr( $objApHeader->getPoApHeaderIds() ) ) {
			$this->setPoApHeaderIds( $objApHeader->getPoApHeaderIds() );
		}
		$this->setReversalApHeaderId( $objApHeader->getReversalApHeaderId() );
		$this->setPaymentApHeaderId( $objApHeader->getPaymentApHeaderId() );
		$this->setScheduledApHeaderId( $objApHeader->getScheduledApHeaderId() );
		$this->setTemplateApHeaderId( $objApHeader->getTemplateApHeaderId() );
		$this->setReimbursementApHeaderId( $objApHeader->getReimbursementApHeaderId() );
		$this->setFrequencyId( $objApHeader->getFrequencyId() );
		$this->setLeaseCustomerId( $objApHeader->getLeaseCustomerId() );
		$this->setRefundArTransactionId( $objApHeader->getRefundArTransactionId() );
		$this->setApBatchId( $objApHeader->getApBatchId() );
		$this->setAccountingExportBatchId( $objApHeader->getAccountingExportBatchId() );
		$this->setApPaymentId( $objApHeader->getApPaymentId() );
		$this->setComplianceJobId( $objApHeader->getComplianceJobId() );
		$this->setApRoutingTagId( $objApHeader->getApRoutingTagId() );
		$this->setRemotePrimaryKey( $objApHeader->getRemotePrimaryKey() );
		$this->setTemplateName( $objApHeader->getTemplateName() );
		$this->setTransactionDatetime( $objApHeader->getTransactionDatetime() );
		$this->setPostDate( $objApHeader->getPostDate() );
		$this->setDueDate( $objApHeader->getDueDate() );
		$this->setPostMonth( $objApHeader->getPostMonth() );
		$this->setScheduledPaymentDate( $objApHeader->getScheduledPaymentDate() );
		$this->setStartDate( $objApHeader->getStartDate() );
		$this->setEndDate( $objApHeader->getEndDate() );
		$this->setFrequencyInterval( $objApHeader->getFrequencyInterval() );
		$this->setNumberOfOccurrences( $objApHeader->getNumberOfOccurrences() );
		$this->setDayOfWeek( $objApHeader->getDayOfWeek() );
		$this->setDaysOfMonth( $objApHeader->getDaysOfMonth() );
		$this->setLastPostedDate( $objApHeader->getLastPostedDate() );
		$this->setNextPostDate( $objApHeader->getNextPostDate() );
		$this->setHeaderNumber( $objApHeader->getHeaderNumber() );
		$this->setAccountNumber( $objApHeader->getAccountNumber() );
		$this->setHeaderMemo( $objApHeader->getHeaderMemo() );
		$this->setApprovalNote( $objApHeader->getApprovalNote() );
		$this->setControlTotal( $objApHeader->getControlTotal() );
		$this->setPreApprovalAmount( $objApHeader->getPreApprovalAmount() );
		$this->setTransactionAmount( $objApHeader->getTransactionAmount() );
		$this->setTransactionAmountDue( $objApHeader->getTransactionAmountDue() );
		$this->setTaxAmount( $objApHeader->getTaxAmount() );
		$this->setDiscountAmount( $objApHeader->getDiscountAmount() );
		$this->setShippingAmount( $objApHeader->getShippingAmount() );
		$this->setBulkIsConfidential( $objApHeader->getBulkIsConfidential() );
		$this->setBulkIs1099( $objApHeader->getBulkIs1099() );
		$this->setAutoCreatePo( $objApHeader->getAutoCreatePo() );
		$this->setAutoApprovePo( $objApHeader->getAutoApprovePo() );
		$this->setAutoCreateInvoice( $objApHeader->getAutoCreateInvoice() );
		$this->setAutoApproveInvoice( $objApHeader->getAutoApproveInvoice() );
		$this->setAutoPostInvoice( $objApHeader->getAutoPostInvoice() );
		$this->setPayWithSingleCheck( $objApHeader->getPayWithSingleCheck() );
		$this->setIsDeleted( $objApHeader->getIsDeleted() );
		$this->setIsReversed( $objApHeader->getIsReversed() );
		$this->setIsPrimary( $objApHeader->getIsPrimary() );
		$this->setIsOnHold( $objApHeader->getIsOnHold() );
		$this->setIsTemplate( $objApHeader->getIsTemplate() );
		$this->setIsReimbursement( $objApHeader->getIsReimbursement() );
		$this->setIsBatching( $objApHeader->getIsBatching() );
		$this->setIsTemporary( $objApHeader->getIsTemporary() );
		$this->setIsPosted( $objApHeader->getIsPosted() );
		$this->setIsInitialImport( $objApHeader->getIsInitialImport() );
		$this->setIsDisabled( $objApHeader->getIsDisabled() );
		$this->setPostedOn( $objApHeader->getPostedOn() );
		$this->setImportedOn( $objApHeader->getImportedOn() );
		$this->setApprovedBy( $objApHeader->getApprovedBy() );
		$this->setApprovedOn( $objApHeader->getApprovedOn() );
		$this->setDeletedBy( $objApHeader->getDeletedBy() );
		$this->setDeletedOn( $objApHeader->getDeletedOn() );
		$this->setUpdatedOn( $objApHeader->getUpdatedOn() );
		$this->setCreatedOn( $objApHeader->getCreatedOn() );
	}

	/**
	 * Get Functions
	 */

	public function getScheduledApTransactionHeaderId() {
		return $this->m_intScheduledApTransactionHeaderId;
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function getApPayeeTerm() {
		return $this->m_strApPayeeTerm;
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function getFormattedPostMonth() {

		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function getCountry() {
		return $this->m_strCountry;
	}

	public function getRemittanceName() {
		return $this->m_strRemittanceName;
	}

	public function getRemittanceType() {
		return $this->m_strRemittanceType;
	}

	public function getScheduledApTransactionHeaderNumber() {
		return $this->m_intScheduledApTransactionHeaderNumber;
	}

	public function getRefundCustomerNames() {
		return $this->m_strRefundCustomerNames;
	}

	public function getLocationsApRemittanceId() {
		return $this->m_intLocationsApRemittanceId;
	}

	/**
	 * Set Functions
	 */

	public function setScheduledApTransactionHeaderId( $intScheduledApTransactionHeaderId ) {
		$this->m_intScheduledApTransactionHeaderId = $intScheduledApTransactionHeaderId;
	}

	public function setPayeeName( $strPayeeName ) {
		$this->m_strPayeeName = CStrings::strTrimDef( $strPayeeName, 100, NULL, true );
	}

	public function setApPayeeTerm( $strApPayeeTerm ) {
		$this->m_strApPayeeTerm = $strApPayeeTerm;
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->m_fltTransactionAmount = CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 );
	}

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setStreetLine1( $strStreetline1 ) {
		$this->m_strStreetLine1 = $strStreetline1;
	}

	public function setStreetLine2( $strStreetline2 ) {
		$this->m_strStreetLine2 = $strStreetline2;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setProvince( $strProvince ) {
		$this->m_strProvince = $strProvince;
	}

	public function setCountry( $strCountry ) {
		$this->m_strCountry = $strCountry;
	}

	public function setRemittanceName( $strRemittanceName ) {
		$this->m_strRemittanceName = $strRemittanceName;
	}

	public function setRemittanceType( $strRemittanceType ) {
		$this->m_strRemittanceType = $strRemittanceType;
	}

	public function setScheduledApTransactionHeaderNumber( $intScheduledApTransactionHeaderNumber ) {
		$this->m_intScheduledApTransactionHeaderNumber = CStrings::strToIntDef( $intScheduledApTransactionHeaderNumber, NULL, false );
	}

	public function setRefundCustomerNames( $strRefundCustomerNames ) {
		$this->m_strRefundCustomerNames = $strRefundCustomerNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['scheduled_ap_transaction_header_id'] ) ) $this->setScheduledApTransactionHeaderId( $arrmixValues['scheduled_ap_transaction_header_id'] );
		if( true == isset( $arrmixValues['payee_name'] ) ) $this->setPayeeName( $arrmixValues['payee_name'] );
		if( true == isset( $arrmixValues['ap_payee_term'] ) ) $this->setApPayeeTerm( $arrmixValues['ap_payee_term'] );
		if( true == isset( $arrmixValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrmixValues['transaction_amount'] );
		if( true == isset( $arrmixValues['company_employee_full_name'] ) ) $this->setCompanyEmployeeFullName( $arrmixValues['company_employee_full_name'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['province'] ) ) $this->setProvince( $arrmixValues['province'] );
		if( true == isset( $arrmixValues['country'] ) ) $this->setCountry( $arrmixValues['country'] );
		if( true == isset( $arrmixValues['refund_customer_names'] ) ) $this->setRefundCustomerNames( $arrmixValues['refund_customer_names'] );
		if( true == isset( $arrmixValues['remittance_name'] ) ) $this->setRemittanceName( $arrmixValues['remittance_name'] );
		if( true == isset( $arrmixValues['remittance_type'] ) ) $this->setRemittanceType( $arrmixValues['remittance_type'] );

		if( true == isset( $arrmixValues['scheduled_ap_transaction_header_number'] ) && true == $boolDirectSet ) {

			$this->m_intScheduledApTransactionHeaderNumber = trim( $arrmixValues['scheduled_ap_transaction_header_number'] );
		} elseif( true == isset( $arrmixValues['scheduled_ap_transaction_header_number'] ) ) {

			$this->setScheduledApTransactionHeaderNumber( $arrmixValues['scheduled_ap_transaction_header_number'] );
		}

		return;
	}

	public function setLocationsApRemittanceId( $intLocationsApRemittanceId ) {
		$this->m_intLocationsApRemittanceId = CStrings::strToIntDef( $intLocationsApRemittanceId, NULL, false );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTransactionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPoHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRefundArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledPaymentDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHeaderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHeaderMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valControlTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsManagerial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiresApproval() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayWithSingleCheck() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOnHold() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBatching() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTemporary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPosted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsInitialImport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRetentionAmountReleased() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions.
	 */

	public function fetchApDetails( $objClientDatabase ) {
		return CApDetails::fetchApDetailsByApHeaderLogIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApDetailLogsForItemReturn( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CApDetailLogs::createService()->fetchApDetailLogsForItemReturnByApHeaderLogIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Other functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getApHeaderLogAction( $strApHeaderLogAction ) {
		$strTranslatedApHeaderLogAction = NULL;

		switch( $strApHeaderLogAction ) {

			case self::ACTION_IMPORTED:
				$strTranslatedApHeaderLogAction = __( 'Imported' );
				break;

			case self::ACTION_CREATED:
				$strTranslatedApHeaderLogAction = __( 'Created' );
				break;

			case self::ACTION_EDITED:
				$strTranslatedApHeaderLogAction = __( 'Edited' );
				break;

			case self::ACTION_POSTED:
				$strTranslatedApHeaderLogAction = __( 'Approved and Posted' );
				break;

			case self::ACTION_UNPOSTED:
				$strTranslatedApHeaderLogAction = __( 'Unposted' );
				break;

			case self::ACTION_APPROVED:
				$strTranslatedApHeaderLogAction = __( 'Approved' );
				break;

			case self::ACTION_UNAPPROVED:
				$strTranslatedApHeaderLogAction = __( 'Unapproved' );
				break;

			case self::ACTION_CLOSED:
				$strTranslatedApHeaderLogAction = __( 'Closed' );
				break;

			case self::ACTION_OPENED:
				$strTranslatedApHeaderLogAction = __( 'Opened' );
				break;

			case self::ACTION_DELETED:
				$strTranslatedApHeaderLogAction = __( 'Deleted' );
				break;

			case self::ACTION_REVERSED:
				$strTranslatedApHeaderLogAction = __( 'Reversed' );
				break;

			case self::ACTION_PAID:
				$strTranslatedApHeaderLogAction = __( 'Paid' );
				break;

			case self::ACTION_VOIDED:
				$strTranslatedApHeaderLogAction = __( 'Voided' );
				break;

			case self::ACTION_FINAL_APPROVAL:
				$strTranslatedApHeaderLogAction = __( 'Approved for Payment' );
				break;

			case self::ACTION_FINAL_APPROVAL_PAYMENT:
				$strTranslatedApHeaderLogAction = __( 'Final Approval for Payment' );
				break;

			case self::ACTION_FINAL_APPROVAL_PO:
				$strTranslatedApHeaderLogAction = __( 'Final Approval ' );
				break;

			case self::ACTION_RETENTION_RELEASED:
				$strTranslatedApHeaderLogAction = __( 'Retention Released' );
				break;

			case self::ACTION_RETURNED_TO_PREVIOUS:
				$strTranslatedApHeaderLogAction = __( 'Returned To Previous' );
				break;

			case self::ACTION_RETURNED_TO_BEGINNING:
				$strTranslatedApHeaderLogAction = __( 'Returned To Beginning' );
				break;

			case self::ACTION_ITEMS_RECEIVED:
				$strTranslatedApHeaderLogAction = __( 'Items Received' );
				break;

			case self::ACTION_ITEMS_RETURNED:
				$strTranslatedApHeaderLogAction = __( 'Items Returned' );
				break;

			case self::ACTION_ITEMS_STATUS_UPDATED:
				$strTranslatedApHeaderLogAction = __( 'Items Status Updated' );
				break;

			case self::ACTION_MOVED_TO_UNCLAIMED_PROPERTY:
				$strTranslatedApHeaderLogAction = __( 'Moved to Unclaimed Property' );
				break;

			case self::ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY:
				$strTranslatedApHeaderLogAction = __( 'Removed from Unclaimed Property' );
				break;

			case self::ACTION_ATTACHMENT_ADDED:
				$strTranslatedApHeaderLogAction = __( 'Attachment added' );
				break;

			case self::ACTION_ATTACHMENT_REMOVED:
				$strTranslatedApHeaderLogAction = __( 'Attachment removed' );
				break;

			case self::ACTION_URL_ADDED:
				$strTranslatedApHeaderLogAction = __( 'URL added' );
				break;

			case self::ACTION_URL_REMOVED:
				$strTranslatedApHeaderLogAction = __( 'URL removed' );
				break;

			case self::ACTION_REJECTED:
				$strTranslatedApHeaderLogAction = __( 'Rejected' );
				break;

			case self::ACTION_RE_SUBMITTED:
				$strTranslatedApHeaderLogAction = __( 'Re-Submitted' );
				break;

			case self::ACTION_ITEMS_CANCELLED:
				$strTranslatedApHeaderLogAction = __( 'Items Cancelled' );
				break;

			case self::ACTION_ENABLED:
				$strTranslatedApHeaderLogAction = __( 'Enabled' );
				break;

			case self::ACTION_DISABLED:
				$strTranslatedApHeaderLogAction = __( 'Disabled' );
				break;

			case self::ACTION_VENDOR_1099_SETTING_UPDATED:
				$strTranslatedApHeaderLogAction = __( 'Vendor/Location 1099 setting updated' );
				break;

			case self::ACTION_ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS:
				$strTranslatedApHeaderLogAction = __( 'Allow Multiple Invoices for Standard PO Line Items' );
				break;

			case self::ACTION_RESUBMITTED:
				$strTranslatedApHeaderLogAction = __( 'Resubmitted' );
				break;

			default:
					$strTranslatedApHeaderLogAction = NULL;
		}

		return $strTranslatedApHeaderLogAction;
	}

	public static function assignSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_IMPORTED',						self::ACTION_IMPORTED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_CREATED',							self::ACTION_CREATED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_EDITED',							self::ACTION_EDITED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_POSTED',							self::ACTION_POSTED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_UNPOSTED',						self::ACTION_UNPOSTED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_APPROVED',						self::ACTION_APPROVED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_UNAPPROVED',						self::ACTION_UNAPPROVED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_DELETED',							self::ACTION_DELETED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_REVERSED',						self::ACTION_REVERSED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_PAID',							self::ACTION_PAID );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_VOIDED',							self::ACTION_VOIDED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_FINAL_APPROVAL',					self::ACTION_FINAL_APPROVAL );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_FINAL_APPROVAL_PAYMENT',			self::ACTION_FINAL_APPROVAL_PAYMENT );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_FINAL_APPROVAL_PO',				self::ACTION_FINAL_APPROVAL_PO );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_RETENTION_RELEASED',				self::ACTION_RETENTION_RELEASED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_RETURNED_TO_PREVIOUS',			self::ACTION_RETURNED_TO_PREVIOUS );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_RETURNED_TO_BEGINNING',			self::ACTION_RETURNED_TO_BEGINNING );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_ITEMS_RECEIVED',					self::ACTION_ITEMS_RECEIVED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_ITEMS_RETURNED',					self::ACTION_ITEMS_RETURNED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_ITEMS_STATUS_UPDATED',			self::ACTION_ITEMS_STATUS_UPDATED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_MOVED_TO_UNCLAIMED_PROPERTY',		self::ACTION_MOVED_TO_UNCLAIMED_PROPERTY );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY',	self::ACTION_REMOVED_FROM_UNCLAIMED_PROPERTY );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_ATTACHMENT_ADDED',				self::ACTION_ATTACHMENT_ADDED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_ATTACHMENT_REMOVED',				self::ACTION_ATTACHMENT_REMOVED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_URL_ADDED',						self::ACTION_URL_ADDED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_URL_REMOVED',						self::ACTION_URL_REMOVED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_REJECTED',						self::ACTION_REJECTED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_RE_SUBMITTED',					self::ACTION_RE_SUBMITTED );
		$objSmarty->assign( 'AP_HEADER_LOG_ACTION_RESUBMITTED',						self::ACTION_RESUBMITTED );
	}

	public function bulkUpdateApHeaderLogs( $arrobjApHeaderLogs, $arrmixFieldsToUpdate, $objDatabase ) {

		$strSql = '';

		foreach( $arrobjApHeaderLogs as $objApHeaderLog ) {
			$arrstrFieldValues = $objApHeaderLog->toArray();

			// Prepare the UPDATE sql
			$strSql .= 'UPDATE ap_header_logs SET ';

			// Loop on each field
			foreach( $arrmixFieldsToUpdate as $strFieldToUpdate ) {
				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strFieldToUpdate ) ) );
				$strSql .= ' ' . $strFieldToUpdate . ' = ' . ( true == is_null( $arrstrFieldValues[$strFieldToUpdate] ) ? 'NULL' : $objApHeaderLog->$strSqlFunctionName() ) . ', ';
			}

			// Trim the ending comma
			$strSql = trim( $strSql, ', ' );
			$strSql .= ' WHERE cid = ' . $arrstrFieldValues['cid'] . ' AND id = ' . $arrstrFieldValues['id'];
			$strSql .= ';' . PHP_EOL;

		}

		return fetchData( $strSql, $objDatabase );
	}

}
?>