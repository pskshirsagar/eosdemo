<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOffers
 * Do not add any new functions to this class.
 */

class CScreeningConditionOffers extends CBaseScreeningConditionOffers {

	public static function fetchPublishedScreeningConditionOfferByScreeningApplicationRequestIdByCid( $intScreeningApplicationRequestId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sco.*,
						COALESCE( app.name_first, ce.name_first ) as name_first,
						COALESCE( app.name_last, ce.name_last ) as name_last
					FROM
						screening_condition_offers sco
						LEFT JOIN applicants app ON app.customer_id = sco.offer_decision_by AND app.cid = sco.cid
						LEFT JOIN company_users cu ON cu.id = sco.offer_decision_by AND cu.cid = sco.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						LEFT JOIN company_employees ce ON ce.id = cu.company_employee_id AND ce.cid = sco.cid
					WHERE
						sco.screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						AND sco.cid = ' . ( int ) $intCid . '
						AND sco.is_published = 1 ORDER BY id DESC LIMIT 1';

		return parent::fetchScreeningConditionOffer( $strSql, $objDatabase );
	}

	public static function fetchScreeningOffersApplicationsCountByDashboardFilterByCompanyUserByCid( $objDashboardFilter, $objCompanyUser, $intCid, $objDatabase, $intMaxExecutionTimeOut = 0, $boolIsDashboardAuditReport = false  ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return;
		}

		$strWhere = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' WHERE screening_data.priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strWhere .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )
						OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		if( $boolIsDashboardAuditReport ) {
			$strSql .= 'SELECT 
						COUNT( * ) AS approvals_screening,
						property_id,
						property_name,
						COALESCE( MAX(priority), 1 ) AS priority';
		} else {
			$strSql .= 'SELECT
                        COUNT ( * ),
                        COALESCE ( max ( priority ), 1 ) AS priority';
		}

		$strSql .= '
					FROM
                        (
                            SELECT
                            DISTINCT ( ca.id ) AS cnt,
                            CASE
	                            WHEN scos.id =  ' . CScreeningConditionOfferStatus::ACCEPTED . ' THEN 
                                CASE
                                    WHEN ( select count(id) from screening_application_condition_sets where cid = ca.cid
                                            AND application_id = ca.id 
                                            AND satisfied_by IS NULL AND satisfied_on IS NULL
                                            AND is_active = 1 ) = 0 
                                    THEN 1
                                    ELSE 0
                                END      
                            ELSE 
                                1
                            END as satisfy_condition,
							CASE
                                WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
                                    3
                                WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                                    3
                                WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN 
                                    2
                                WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                                    2
                                ELSE
                                    1
                                END as priority,
                                p.id AS property_id,
                                p.property_name
							FROM
								cached_applications ca
                                JOIN screening_application_requests sar ON( sar.application_id = ca.id AND sar.cid = ca.cid )
			                    JOIN application_status_types ast ON( ast.id = ca.application_status_id)
			                    JOIN properties p ON( p.cid = ca.cid AND p.id = ca.property_id )
							    JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.is_disabled = 0 AND lp.property_id = ca.property_id
							    JOIN screening_condition_offers sco ON ( sco.cid = sar.cid AND sco.screening_application_request_id = sar.id AND sco.is_published = 1 )
						        JOIN screening_condition_offer_statuses scos ON ( scos.id = sco.screening_condition_offer_status_id )
						        LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
							    JOIN screening_recommendation_types srt ON( srt.id = sar.screening_recommendation_type_id )
							    JOIN applicant_applications aa ON( aa.cid = ca.cid AND ca.id = aa.application_id AND aa.deleted_on IS NULL AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
							    JOIN applicants appt ON( appt.cid = aa.cid AND appt.id = aa.applicant_id )
							    LEFT JOIN company_users cu ON ( sar.created_by = cu.id AND cu.cid = sar.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id = 2 )
							    LEFT JOIN company_employees ce ON( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
							    LEFT JOIN dashboard_priorities dp ON ( dp.cid = ca.cid )
                            WHERE
					            sar.cid =  ' . ( int ) $intCid . '
					            AND CASE 
                                WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN 
                                    ca.application_stage_id = ' . CApplicationStage::LEASE . '
                                AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                                ELSE         
                                    ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
                                AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                                END
                                AND CASE WHEN pp.id <> 0 AND pp.id IS NOT NULL AND sco.screening_condition_offer_status_id = ' . CScreeningConditionOfferStatus::ACCEPTED . ' THEN
		                            (   SELECT count(id) 
		                                FROM screening_application_condition_sets 
	                                        WHERE cid = ca.cid
                                            AND application_id = ca.id 
                                            AND satisfied_by IS NULL AND satisfied_on IS NULL
                                            AND is_active = 1 ) = 1
                                  ELSE TRUE  
		                        END                                    
					            AND sar.application_type_id = ' . CScreeningApplicationType::TENANT . '
			                    AND sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . $strWhere . ' ) AS screening_data ' . $strPriorityWhere;
		if( $boolIsDashboardAuditReport ) {
			$strSql .= '
					GROUP BY
						property_id,
						property_name';
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedScreeningOfferedApplicationsByDashboardFilterByCompanyUserByCid( $objDashboardFilter, $objCompanyUser, $intCid, $intOffset, $intPageLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strLeasingAgentCondition = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition .= ' AND ( ce.id IS NULL OR ce.id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' WHERE priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strSql = 'SELECT 
		            *
					FROM (
							SELECT
                                    DISTINCT ON( ca.id )
									ca.id as application_id,
									ca.cid,
									func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name,
								    COALESCE ( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache ) AS unit_number,
									COALESCE( ce.name_first || \' - \' || ce.name_last ) as agent_name,
									ca.application_completed_on  AS app_completed_on,
									ca.application_datetime,
									ca.lease_id,
									ca.application_status_id,
									ca.application_stage_id,
									ast.name as application_status,
									ca.lease_interval_type_id,
									p.property_name,
									p.id as property_id,
									pp.id AS screen_after_lease_completed,
									sar.screening_recommendation_type_id as recommendation_id,
									srt.name as recommendation,
									scos.id as offer_status_id,
									scos.name as offer_status,
									sco.expired_on,
									 CASE
                                        WHEN scos.id =  ' . CScreeningConditionOfferStatus::ACCEPTED . ' THEN 
                                        CASE
                                            WHEN ( select count(id) from screening_application_condition_sets where cid = ca.cid
                                                    AND application_id = ca.id 
                                                    AND satisfied_by IS NULL AND satisfied_on IS NULL
                                            AND is_active = 1 ) = 0 
                                            THEN 1
                                            ELSE 0
                                        END      
                                    ELSE 
                                        1
                                    END as satisfy_condition,									
									CASE 
						              WHEN pp.id IS NOT NULL THEN
						                ( SELECT
						                                  f.id
						                              FROM
						                                  files f
						                                  JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' AND f.countersigned_by IS NULL )
						                                  JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.deleted_on IS NULL AND fa.lease_id IS NOT NULL ) 
						                              WHERE
						                                  f.cid = ' . ( int ) $intCid . '
						                                  AND f.property_id = ca.property_id
						                                  AND fa.applicant_id = appt.id
						                                  AND fa.customer_id IS NOT NULL
						                                  AND fa.approved_by IS NULL
						                                  AND ( fa.application_id IS NOT NULL OR f.require_countersign = 1 )
						                                  ORDER BY f.id DESC LIMIT 1 )
						              END AS file_id,
                                    CASE
								    WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
								    3
								    WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
								    3
								    WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN 
								    2
								    WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
								    2
								    ELSE
								    1
                                    END as priority,
                                    CASE 
              	                        WHEN scos.id = ' . ( int ) CScreeningConditionOfferStatus::ACCEPTED . ' AND (ca.lease_type_id = ' . ( int ) CLeaseType::STANDARD . ' OR ca.lease_type_id IS NULL) AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . ' THEN
                	                        CASE
						                       WHEN
						                          ( SELECT
						                            count(aa_temp.id)
						                          FROM
						                              applicant_applications aa_temp
						                              JOIN screening_package_customer_types_mappings spctm ON ( aa_temp.cid = spctm.cid and spctm.customer_relationship_id = aa_temp.customer_relationship_id )
						                              LEFT JOIN screening_applicant_results sar ON ( aa_temp.cid = sar.cid AND sar.applicant_id = aa_temp.applicant_id AND sar.application_id = aa_temp.application_id AND aa_temp.deleted_on IS NULL )
						                          WHERE
						                              aa_temp.application_id = aa.application_id
						                              AND aa_temp.cid = aa.cid
						                              AND spctm.property_id = p.id
						                              AND spctm.lease_type_id = ' . ( int ) CLeaseType::STANDARD . '
						                              AND spctm.is_active = 1
						                              AND aa_temp.deleted_on IS NULL
						                              AND sar.id IS NULL
						                              AND spctm.screening_applicant_requirement_type_id = ' . ( int ) CScreeningApplicantRequirementType::REQUIRED . ') > 0 THEN 1 
						                       ELSE 0 
						                    END
						                ELSE 0
					                END AS is_screening_required
              
							FROM
                                cached_applications ca
				--LEFT
			     JOIN screening_application_requests sar ON( sar.application_id = ca.id AND sar.cid = ca.cid )
			     JOIN application_status_types ast ON( ast.id = ca.application_status_id)
			     JOIN properties p ON( p.cid = ca.cid AND p.id = ca.property_id )
			     JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
			     JOIN screening_condition_offers sco ON ( sco.cid = sar.cid AND sco.screening_application_request_id = sar.id AND sco.is_published = 1 )
		         JOIN screening_condition_offer_statuses scos ON ( scos.id = sco.screening_condition_offer_status_id )
		         LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
			     JOIN screening_recommendation_types srt ON( srt.id = sar.screening_recommendation_type_id )
			     JOIN applicant_applications aa ON( aa.cid = ca.cid AND ca.id = aa.application_id AND aa.deleted_on IS NULL AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
			     JOIN applicants appt ON( appt.cid = aa.cid AND appt.id = aa.applicant_id )
			     LEFT JOIN company_users cu ON ( sar.created_by = cu.id AND cu.cid = sar.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id = 2 )
			     LEFT JOIN company_employees ce ON( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
			     LEFT JOIN dashboard_priorities dp ON ( dp.cid = ca.cid )
                WHERE
		            sar.cid =  ' . ( int ) $intCid . '
		            AND CASE 
                                WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN 
                                    ca.application_stage_id = ' . CApplicationStage::LEASE . '
                                AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                                ELSE         
                                    ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
                                AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                        END
                    AND CASE WHEN pp.id <> 0 AND pp.id IS NOT NULL AND sco.screening_condition_offer_status_id = ' . CScreeningConditionOfferStatus::ACCEPTED . ' THEN
                        (   SELECT count(id) 
                            FROM screening_application_condition_sets 
                                WHERE cid = ca.cid
                                AND application_id = ca.id 
                                AND satisfied_by IS NULL AND satisfied_on IS NULL
                                AND is_active = 1 ) = 1
                      ELSE TRUE  
                    END    
		            AND sar.application_type_id = ' . CScreeningApplicationType::TENANT . '
                    AND sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '
		             ' . $strLeasingAgentCondition . '
            ) AS screening_data ' . $strPriorityWhere . '
			ORDER BY
				' . $strSortBy . '
				OFFSET ' . ( int ) $intOffset . '
				LIMIT ' . ( int ) $intPageLimit;
		return fetchData( $strSql, $objDatabase );
	}

}
?>