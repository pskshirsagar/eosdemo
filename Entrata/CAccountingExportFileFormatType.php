<?php

class CAccountingExportFileFormatType extends CBaseAccountingExportFileFormatType {

	const LEGACY_WF_ARP_INBOUND         = 1;
	const STANDARD_ARP_INBOUND          = 2;
	const BANK_OF_AMERICA_SB            = 3;
	const BANK_OF_AMERICA_NATIONAL      = 4;
	const BANK_OF_AMERICA_IDS           = 5;
	const JP_MORGAN_CHASE               = 6;
	const JP_MORGAN_CHASE_ARP           = 7;
	const FROST_BANK                    = 8;
	const FROST_BANK_ANSI               = 9;
	const WELLS_FARGO_ASCII             = 10;
	const JP_MORGAN_CHASE_CP            = 11;
	const US_BANK_CSV                   = 12;
	const US_BANK_TXT                   = 13;
	const JP_MORGAN_CHASE_PCL           = 15;
	const CNB_CNZ                       = 16;
	const CNB_TXT                       = 17;
	const IBERIA_AUTOMATED              = 18;
	const IBERIA_MANUAL                 = 19;
	const CHARGE_EXPORT                 = 20;
	const CREDIT_EXPORT                 = 21;
	const PAYMENT_EXPORT                = 22;
	const CITI_BANK_CSV                 = 23;
	const CITI_BANK_TXT                 = 24;
	const HSBC_US                       = 25;
	const HSBC_CANADA                   = 26;
	const MIDFIRST_BANK_CSV             = 27;
	const MIDFIRST_BANK_TXT             = 28;
	const US_BANK_ARP_STANDARD          = 29;
	const YARDI_ETL_FINPAYABLE_EXPENSE  = 30;
	const YARDI_ETL_FINPAYABLE_STANDARD = 31;
	const FIRST_COMMONWEALTH_TXT        = 32;
	const FIRST_COMMONWEALTH_CSV        = 33;
	const JP_MORGAN_CHASE_CSV           = 34;
	const POSPAY_EXTRACT_CUSTOM         = 35;
	const POSPAY_NO_ECHECKS             = 36;
	const CAPITAL_ONE_TXT               = 37;
	const CAPITAL_ONE_CSV               = 38;

	const ACCOUNTING_EXPORT_FILE_FORMAT_CSV = 1;
	const ACCOUNTING_EXPORT_FILE_FORMAT_TXT = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static $c_arrintApFileFormatTypes = [
		CTransmissionVendor::AP_EXPORT_YARDI_ETL_FINPAYABLES => [ self::YARDI_ETL_FINPAYABLE_EXPENSE, self::YARDI_ETL_FINPAYABLE_STANDARD ]
	];

	public function valTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getAccountingExportFileFormatTypeNameById( $intAccountingExportFileFormatTypeId ) {

		$strAccountingExportFileFormatTypeName = NULL;
		switch( $intAccountingExportFileFormatTypeId ) {
			case self::STANDARD_ARP_INBOUND:
				$strAccountingExportFileFormatTypeName = __( 'Standard ARP Inbound' );
				break;

			case self::LEGACY_WF_ARP_INBOUND:
				$strAccountingExportFileFormatTypeName = __( 'Legacy WF ARP Inbound' );
				break;

			case self::BANK_OF_AMERICA_SB:
				$strAccountingExportFileFormatTypeName = __( 'Bank  Of America SB' );
				break;

			case self::BANK_OF_AMERICA_NATIONAL:
				$strAccountingExportFileFormatTypeName = __( 'Bank  Of America National' );
				break;

			case self::BANK_OF_AMERICA_IDS:
				$strAccountingExportFileFormatTypeName = __( 'Bank  Of America IDS' );
				break;

			case self::JP_MORGAN_CHASE:
				$strAccountingExportFileFormatTypeName = __( 'JP Morgan Chase' );
				break;

			case self::JP_MORGAN_CHASE_ARP:
				$strAccountingExportFileFormatTypeName = __( 'JP Morgan Chase-ARP' );
				break;

			case self::FROST_BANK:
				$strAccountingExportFileFormatTypeName = __( 'Frost Bank' );
				break;

			case self::FROST_BANK_ANSI:
				$strAccountingExportFileFormatTypeName = __( 'Frost Bank - ANSI' );
				break;

			case self::WELLS_FARGO_ASCII:
				$strAccountingExportFileFormatTypeName = __( 'Wells Fargo ASCII' );
				break;

			case self::JP_MORGAN_CHASE_CP:
				$strAccountingExportFileFormatTypeName = __( 'JP Morgan Chase CP' );
				break;

			case self::US_BANK_CSV:
				$strAccountingExportFileFormatTypeName = __( 'US Bank CSV' );
				break;

			case self::US_BANK_TXT:
				$strAccountingExportFileFormatTypeName = __( 'US Bank TXT' );
				break;

			case self::JP_MORGAN_CHASE_PCL:
				$strAccountingExportFileFormatTypeName = __( 'JP Morgan Chase PCL' );
				break;

			case self::CNB_CNZ:
				$strAccountingExportFileFormatTypeName = __( 'CNB CNZ' );
				break;

			case self::CNB_TXT:
				$strAccountingExportFileFormatTypeName = __( 'CNB TXT' );
				break;

			case self::IBERIA_AUTOMATED:
				$strAccountingExportFileFormatTypeName = __( 'Automated' );
				break;

			case self::IBERIA_MANUAL:
				$strAccountingExportFileFormatTypeName = __( 'Manual' );
				break;

			case self::CHARGE_EXPORT:
				$strAccountingExportFileFormatTypeName = __( 'Charge Export' );
				break;

			case self::CREDIT_EXPORT:
				$strAccountingExportFileFormatTypeName = __( 'Credit Export' );
				break;

			case self::PAYMENT_EXPORT:
				$strAccountingExportFileFormatTypeName = __( 'Payment Export' );
				break;

			case self::CITI_BANK_CSV:
				$strAccountingExportFileFormatTypeName = __( 'CitiBank CSV' );
				break;

			case self::CITI_BANK_TXT:
				$strAccountingExportFileFormatTypeName = __( 'CitiBank TXT' );
				break;

			case self::HSBC_US:
				$strAccountingExportFileFormatTypeName = __( 'HSBC (US)' );
				break;

			case self::HSBC_CANADA:
				$strAccountingExportFileFormatTypeName = __( 'HSBC (Canada)' );
				break;

			case self::MIDFIRST_BANK_CSV:
				$strAccountingExportFileFormatTypeName = __( 'MidFirst CSV' );
				break;

			case self::MIDFIRST_BANK_TXT:
				$strAccountingExportFileFormatTypeName = __( 'MidFirst TXT' );
				break;

			case self::YARDI_ETL_FINPAYABLE_EXPENSE:
				$strAccountingExportFileFormatTypeName = __( 'Expense' );
				break;

			case self::YARDI_ETL_FINPAYABLE_STANDARD:
				$strAccountingExportFileFormatTypeName = __( 'Standard' );
				break;

			case self::CAPITAL_ONE_TXT:
				$strAccountingExportFileFormatTypeName = __( 'Capital One TXT' );
				break;

			case self::CAPITAL_ONE_CSV:
				$strAccountingExportFileFormatTypeName = __( 'Intellix/DFT Payee PP' );
				break;

			default:
				/** * do nothing*/
				break;
		}

		return $strAccountingExportFileFormatTypeName;
	}

}

?>