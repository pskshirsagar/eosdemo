<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationReasonTypes
 * Do not add any new functions to this class.
 */

class CSubsidyCertificationReasonTypes extends CBaseSubsidyCertificationReasonTypes {

	public static function fetchSubsidyCertificationReasonTypesBySubsidyCertificationTypeId( $intSubsidyCertificationTypeId, $objDatabase ) {
		return self::fetchSubsidyCertificationReasonTypes( sprintf( 'SELECT * FROM subsidy_certification_reason_types WHERE subsidy_certification_type_id = %d AND is_published = true', ( int ) $intSubsidyCertificationTypeId ), $objDatabase );
	}

	public static function fetchSubsidyCertificationReasonTypeCodeByName( $strMoveOutReasonListItemName, $objDatabase ) {

		$strSql = 'SELECT
						code
					FROM
						subsidy_certification_reason_types
					WHERE
						name ILIKE E\'' . addslashes( trim( $strMoveOutReasonListItemName ) ) . '\'';

		return self::fetchColumn( $strSql, 'code', $objDatabase );
	}

}
?>