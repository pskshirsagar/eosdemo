<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserTypes
 * Do not add any new functions to this class.
 */

class CCompanyUserTypes extends CBaseCompanyUserTypes {

	public static function fetchCompanyUserTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCompanyUserType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCompanyUserType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCompanyUserType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedCompanyUserTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM 
						company_user_types
					WHERE
						is_published = TRUE';

		return self::fetchCompanyUserTypes( $strSql, $objDatabase );
	}

}
?>