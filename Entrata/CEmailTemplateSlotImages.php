<?php

class CEmailTemplateSlotImages extends CBaseEmailTemplateSlotImages {

	public static function fetchEmailTemplateSlotImagesByEmailTemplateSlotIdsByScheduledEmailIdByCid( $arrobjSystemMessageTemplateSlotIds, $intScheduledEmailId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrobjEmailTemplateSlotIds ) ) return NULL;
		$strSql = 'SELECT * FROM email_template_slot_images WHERE system_message_template_slot_id IN ( ' . implode( ', ', $arrobjSystemMessageTemplateSlotIds ) . ' ) AND scheduled_email_id = ' . ( int ) $intScheduledEmailId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchEmailTemplateSlotImages( $strSql, $objClientDatabase );
	}

	public static function fetchEmailTemplateSlotImageByScheduledEmailIdByEmailTemplateIdByEmailTemplateSlotIdByCid( $intScheduledEmailId, $intSystemMessageTemplateId, $intSystemMessageTemplateSlotId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_slot_images WHERE system_message_template_id = ' . ( int ) $intSystemMessageTemplateId . ' AND scheduled_email_id = ' . ( int ) $intScheduledEmailId . ' AND system_message_template_slot_id = ' . ( int ) $intSystemMessageTemplateSlotId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchEmailTemplateSlotImage( $strSql, $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByScheduledEmailIdByEmailTemplateIdByCid( $intScheduledEmailId, $intEmailTemplateId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						email_template_slot_images
					WHERE
						scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND system_message_template_id = ' . ( int ) $intEmailTemplateId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1';

		return self::fetchEmailTemplateSlotImages( $strSql, $objDatabase );
	}

	// MC2.0

	public static function fetchSimpleEmailTemplateSlotImagesByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    etsi.company_media_file_id,
					    etsi.system_message_template_slot_id,
					    cmf.fullsize_uri
					FROM
					    email_template_slot_images etsi
					    JOIN scheduled_emails se ON ( se.id = etsi.scheduled_email_id AND se.system_message_template_id = etsi.system_message_template_id AND se.cid = etsi.cid )
					    JOIN company_media_files cmf ON ( cmf.id = etsi.company_media_file_id AND cmf.cid = etsi.cid )
					WHERE
					    se.id = ' . ( int ) $intScheduledEmailId . '
					    AND se.cid = ' . ( int ) $intCid . '
					   	AND etsi.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						email_template_slot_images
					WHERE
						scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchEmailTemplateSlotImages( $strSql, $objDatabase );
	}

	public static function fetchSimpleEmailTemplateSlotImagesByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    etsi.company_media_file_id,
					    etsi.system_message_template_slot_id,
					    cmf.fullsize_uri
					FROM
					    email_template_slot_images etsi
					    JOIN campaigns c ON ( c.id = etsi.campaign_id AND c.system_message_template_id = etsi.system_message_template_id AND c.cid = etsi.cid )
					    JOIN company_media_files cmf ON ( cmf.id = etsi.company_media_file_id AND cmf.cid = etsi.cid )
					WHERE
					    c.id = ' . ( int ) $intCampaignId . '
					    AND c.cid = ' . ( int ) $intCid . '
					   	AND etsi.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	// event scheduling

	public static function fetchSimpleEmailTemplateSlotImagesByScheduledTaskEmailIdByCid( $intScheduledTaskEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    etsi.company_media_file_id,
					    etsi.system_message_template_slot_id,
					    cmf.fullsize_uri
					FROM
					    email_template_slot_images etsi
					    JOIN scheduled_task_emails ste ON ( ste.id = etsi.scheduled_task_email_id AND ste.system_message_template_id = etsi.system_message_template_id AND ste.cid = etsi.cid )
					    JOIN company_media_files cmf ON ( cmf.id = etsi.company_media_file_id AND cmf.cid = etsi.cid )
					WHERE
					    ste.id = ' . ( int ) $intScheduledTaskEmailId . '
					    AND ste.cid = ' . ( int ) $intCid . '
					   	AND etsi.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}
}
?>