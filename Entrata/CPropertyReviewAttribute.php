<?php

class CPropertyReviewAttribute extends CBasePropertyReviewAttribute {

	protected $m_strAttributeName;
	protected $m_intValue;

	/**
	 * Set Functions
	 *
	 **/

	public function setAttributeName( $strAttributeName ) {
		$this->m_strAttributeName = CStrings::strTrimDef( $strAttributeName, 256, NULL, true );
	}

	public function setValue( $intValue ) {
		$this->m_intValue = CStrings::strToIntDef( $intValue, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['attribute_name'] ) ) $this->setAttributeName( $arrmixValues['attribute_name'] );

		return;
	}

	/**
	 * Get Functions
	 *
	 **/

	public function getAttributeName() {
		return $this->m_strAttributeName;
	}

	public function getValue() {
		return $this->m_intValue;
	}

	/**
	 * Validate Functions
	 *
	 **/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 1 > ( $this->m_intCid = ( int ) $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'company id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 1 > ( $this->m_intPropertyId = ( int ) $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyReviewAttributeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intTestimonialAttributeId ) || 1 > ( $this->m_intTestimonialAttributeId = ( int ) $this->m_intTestimonialAttributeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'testimonial_attribute_id', 'Testimonial attribute id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTestimonialAttributeId();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>