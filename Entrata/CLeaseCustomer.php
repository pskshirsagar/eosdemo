<?php

use Psi\Eos\Entrata\CWebsites;
use Psi\Libraries\UtilHash\CHash;

class CLeaseCustomer extends CBaseLeaseCustomer {

	protected $m_strCustomerNameFull;
	protected $m_strCustomerCompanyName;
	protected $m_strCustomerNameFirst;
	protected $m_strCustomerNameMiddle;
	protected $m_strCustomerNameLast;
	protected $m_strCustomerNameLastMatronymic;
	protected $m_strCustomerGender;
	protected $m_strCustomerEmailAddress;
	protected $m_strEmailAddress;
	protected $m_strCustomerTypeName;
	protected $m_strCustomerRelationshipName;
	protected $m_strCustomerBirthDate;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strIntervalDatetime;
	protected $m_strLeaseRenewalDate;
	protected $m_strPropertyUnitNumber;
	protected $m_strFloorplanName;
	protected $m_strSpaceNumber;
	protected $m_strBuildingName;
	protected $m_strPropertyName;
	protected $m_strLeaseStatusType;
	protected $m_strCustomerPhoneNumber;
	protected $m_strCustomerPortalUsername;
	protected $m_strCustomerSecondaryNumber;
	protected $m_strUnitNumberCache;
	protected $m_strPrimaryApplicantName;

	protected $m_strCustomerPrimaryPhoneNumber;
	protected $m_intIntegrationDatabaseId;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intPopertyBuildingId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_intMoveInFormId;
	protected $m_intMoveOutFormId;
	protected $m_intIsResidentPortalAccount;
	protected $m_intApplicantId;
	protected $m_intMoneyGramAccountId;
	protected $m_intApplicantApplicationId;
	protected $m_intIsSyncApplicationAndLease;
	protected $m_intMaintenanceRequestId;
	protected $m_intMoveOutReasonListItemId;
	protected $m_intLeaseAssociationId;
	protected $m_intLeaseIntervalId;
    protected $m_intRequireScreening;
	protected $m_intPrimaryCustomerId;
	protected $m_intReference;
	protected $m_intOccupancyTypeId;
	protected $m_intApplicationId;
	protected $m_intIsNewLeaseCustomer;

	// This will be used to change the default Event status in case of Skipped, Evict and Transfer events
	protected $m_intEventTypeId;

	protected $m_fltLeaseBalance;
	protected $m_fltDelinquencyAmount;
	protected $m_fltPreGraceDaysDelinquencyAmount;
	protected $m_fltBalance;
	protected $m_fltGuaranteePercent;

	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strOccupantMoveOutDate;
	protected $m_strNoticeDate;

	protected $m_strCompanyIdentificationType;
	protected $m_strIdentificationValue;
	protected $m_strNamePrefix;
	protected $m_strNameMaiden;
	protected $m_strNameSuffix;
	protected $m_strPrimaryStreetLine1;
	protected $m_strPrimaryStreetLine2;
	protected $m_strPrimaryStreetLine3;
	protected $m_strPrimaryCity;
	protected $m_strPrimaryStateCode;
	protected $m_strPrimaryPostalCode;
	protected $m_strPrimaryCountryCode;
	protected $m_strWorkNumber;
	protected $m_strFaxNumber;
	protected $m_strCustomerMobileNumber;
	protected $m_strBalanceDate;
	protected $m_strCustomerCreatedOn;
	protected $m_strOldUnitNumberCache;
	protected $m_strPreferredLocaleCode;
	protected $m_strLeaseSubStatus;

    protected $m_arrstrMergeFields;
	protected $m_arrmixRequiredParameters;

	protected $m_intCustomerFilterTypeId;
	protected $m_intCustomerContactId;
	protected $m_intOrganizationContractId;
	protected $m_intPhoneNumberTypeId;

	protected $m_boolIsIndividualTransfer;

	public function __construct() {
        parent::__construct();

        $this->m_intIsSyncApplicationAndLease = true;
		$this->m_boolIsIndividualTransfer = false;

        return;
    }

    /**
     * Create Functions
     */

    public function createFileAssociation() {

    	$objFileAssociation = new CFileAssociation();

    	$objFileAssociation->setCid( $this->getCid() );
    	$objFileAssociation->setLeaseId( $this->getLeaseId() );
    	$objFileAssociation->setCustomerId( $this->getCustomerId() );

    	return $objFileAssociation;
    }

	public function createLeaseProcess() {

		$objLeaseProcess = new CLeaseProcess();
		$objLeaseProcess->setCid( $this->getCid() );
		$objLeaseProcess->setLeaseId( $this->getLeaseId() );
		$objLeaseProcess->setCustomerId( $this->getCustomerId() );
		$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );

		return $objLeaseProcess;
	}

    /**
     * get Functions
     */

    public function getPhoneNumberTypeId() {
    	return $this->m_intPhoneNumberTypeId;
    }

    public function getPaymentAllowanceTypeId() {
        return $this->m_intPaymentAllowanceTypeId;
    }

    public function getCustomerNameFull() {

        return $this->m_strCustomerNameFull;
    }

    public function getLeaseSubStatus() {
        return $this->m_strLeaseSubStatus;
    }

    public function getCustomerCompanyName() {
    	return $this->m_strCustomerCompanyName;
    }

	public function getCustomerNameFirst() {
        return $this->m_strCustomerNameFirst;
    }

	public function getCustomerNameMiddle() {
        return $this->m_strCustomerNameMiddle;
    }

	public function getCustomerNameLast() {
        return $this->m_strCustomerNameLast;
    }

	public function getCustomerNameLastMatronymic() {
		return $this->m_strCustomerNameLastMatronymic;
	}

	public function getCustomerBirthDate() {
        return $this->m_strCustomerBirthDate;
    }

    public function getCustomerGender() {
        return $this->m_strCustomerGender;
    }

 	public function getIntegrationDatabaseId() {
        return $this->m_intIntegrationDatabaseId;
    }

	public function getCustomerEmailAddress() {
        return $this->m_strCustomerEmailAddress;
    }

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCustomerTypeName() {
        return $this->m_strCustomerTypeName;
    }

	public function getCustomerRelationshipName() {
        return $this->m_strCustomerRelationshipName;
    }

    public function getLeaseStartDate() {
    	return $this->m_strLeaseStartDate;
    }

	public function getLeaseEndDate() {
        return $this->m_strLeaseEndDate;
    }

    public function getLeaseRenewalDate() {
    	return $this->m_strLeaseRenewalDate;
    }

	public function getPropertyUnitNumber() {
        return $this->m_strPropertyUnitNumber;
    }

	public function getFloorplanName() {
        return $this->m_strFloorplanName;
    }

	public function getSpaceNumber() {
        return $this->m_strSpaceNumber;
    }

	public function getLeaseBalance() {
        return $this->m_fltLeaseBalance;
    }

	public function getPopertyBuildingId() {
        return $this->m_intPopertyBuildingId;
    }

	public function getBuildingName() {
        return $this->m_strBuildingName;
    }

	public function getPropertyName() {
        return $this->m_strPropertyName;
    }

	public function getLeaseStatusType() {
        return $this->m_strLeaseStatusType;
    }

	public function getCustomerPhoneNumber() {
		return $this->m_strCustomerPhoneNumber;
    }

	public function getCustomerSecondaryNumber() {
		return $this->m_strCustomerSecondaryNumber;
    }

	public function getCustomerMobileNumber() {
        return $this->m_strCustomerMobileNumber;
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

	public function getIsResidentPortalAccount() {
		return $this->m_intIsResidentPortalAccount;
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getOldUnitNumberCache() {
		return $this->m_strOldUnitNumberCache;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function getIntervalDateTime() {
		return $this->m_strIntervalDatetime;
	}

    public function getLongDescription( $boolIsFromResidentSystem = false, $strIsFrom=NULL, $boolIsHideUnitAndBuildingNumber=false ) {

    	$strNumberReference = NULL;
    	if( false == $boolIsHideUnitAndBuildingNumber ) {
       		$strNumberReference = ( true == is_null( $this->getPropertyUnitNumber() ) ) ? $this->getId() : $this->getPropertyUnitNumber();
    	}

       	$strDescription = ( false == valStr( $strIsFrom ) ) ? CLeaseStatusType::getLeaseStatusTypeNameByLeaseStatusTypeId( $this->getLeaseStatusTypeId() ) : $this->getLeaseStatusTypeName( $boolIsFromResidentSystem );
       	$strDescription .= ' : ';
    	if( false == $boolIsHideUnitAndBuildingNumber && true == valStr( $this->getBuildingName() ) ) $strDescription .= $this->getBuildingName() . ' - ';
    	$strDescription .= $strNumberReference . ' ' . $this->getPropertyName();
    	return \Psi\Libraries\UtilStrings\CStrings::createService()->truncate( $strDescription, 35 );
    }

    public function getPaymentAllowanceTypeName() {
    	return CPaymentAllowanceType::getPaymentAllowanceTypeNameByPaymentAllowanceTypeId( $this->getPaymentAllowanceTypeId() );
    }

    public function getMoveInDate() {
		return $this->m_strMoveInDate;
    }

    public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
    }

    public function getOccupantMoveOutDate() {
    	return $this->m_strOccupantMoveOutDate;
    }

    public function getNoticeDate() {
    	return $this->m_strNoticeDate;
    }

    public function getMoveInFormId() {
    	return $this->m_intMoveInFormId;
    }

    public function getMoveOutFormId() {
    	return $this->m_intMoveOutFormId;
    }

    public function getMoveOutReasonListItemId() {
    	return $this->m_intMoveOutReasonListItemId;
    }

    public function getDelinquencyAmount() {
    	return $this->m_fltDelinquencyAmount;
    }

    public function getPreGraceDaysDelinquencyAmount() {
    	return $this->m_fltPreGraceDaysDelinquencyAmount;
    }

    public function getCustomerPortalUsername() {
    	return $this->m_strCustomerPortalUsername;
    }

    public function getEventTypeId() {
    	return $this->m_intEventTypeId;
    }

	public function getPropertyUnitId() {
    	return $this->m_intPropertyUnitId;
    }

    public function getUnitSpaceId() {
    	return $this->m_intUnitSpaceId;
    }

    public function getBalance() {
    	return $this->m_fltBalance;
    }

    public function getBalanceDate() {
    	return $this->m_strBalanceDate;
    }

    public function getCustomerCreatedOn() {
    	return $this->m_strCustomerCreatedOn;
    }

    public function getCompanyIdentificationType() {
    	return $this->m_strCompanyIdentificationType;
    }

    public function getIdentificationValue() {
    	return $this->m_strIdentificationValue;
    }

    public function getNamePrefix() {
    	return $this->m_strNamePrefix;
    }

    public function getNameSuffix() {
    	return $this->m_strNameSuffix;
    }

    public function getNameMaiden() {
    	return $this->m_strNameMaiden;
    }

    public function getPrimaryStreetLine1() {
    	return $this->m_strPrimaryStreetLine1;
    }

    public function getPrimaryStreetLine2() {
    	return $this->m_strPrimaryStreetLine2;
    }

    public function getPrimaryStreetLine3() {
    	return $this->m_strPrimaryStreetLine3;
    }

    public function getPrimaryCity() {
    	return $this->m_strPrimaryCity;
    }

    public function getPrimaryStateCode() {
    	return $this->m_strPrimaryStateCode;
    }

    public function getPrimaryPostalCode() {
    	return $this->m_strPrimaryPostalCode;
    }

    public function getPrimaryCountryCode() {
    	return $this->m_strPrimaryCountryCode;
    }

    public function getWorkNumber() {
    	return $this->m_strWorkNumber;
    }

    public function getFaxNumber() {
    	return $this->m_strFaxNumber;
    }

    public function getCustomerTypeIdToStr() {
    	return CCustomerType::createService()->customerTypeIdToStr( $this->getCustomerTypeId() );
    }

    public function getApplicantId() {
    	return $this->m_intApplicantId;
    }

	public function getMoneyGramAccountId() {
		return $this->m_intMoneyGramAccountId;
	}

	public function getIsSyncApplicationAndLease() {
		return $this->m_intIsSyncApplicationAndLease;
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function getLeaseAssociationId() {
		return $this->m_intLeaseAssociationId;
	}

    public function getMergeFields() {
        return $this->m_arrstrMergeFields;
    }

    public function getMergeFieldByKey( $strKey ) {
        if( true == array_key_exists( $strKey, $this->m_arrstrMergeFields ) ) {
            return $this->m_arrstrMergeFields[$strKey];
        }

        return NULL;
    }

    public function getRequireScreening() {
    	return $this->m_intRequireScreening;
    }

	public function getPrimaryApplicantName() {
		return $this->m_strPrimaryApplicantName;
	}

	public function getReference() {
		return $this->m_intReference;
	}

	public function getGuaranteePercent() {
		return $this->m_fltGuaranteePercent;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getPreferredLocaleCode() {
    	return $this->m_strPreferredLocaleCode;
	}

	public function getApplicationId() {
    	return $this->m_intApplicationId;
	}

	public function getCustomerFilterTypeId() {
		return $this->m_intCustomerFilterTypeId;
	}

	public function getCustomerContactId() {
		return $this->m_intCustomerContactId;
	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getIsNewLeaseCustomer() {
		return $this->m_intIsNewLeaseCustomer;
	}

	public function getCustomerPrimaryPhoneNumber() {
		return $this->m_strCustomerPrimaryPhoneNumber;
	}

	public function getOrganizationContractId() {
    	return $this->m_intOrganizationContractId;
	}

	public function getIsIndividualTransfer() {
		return $this->m_boolIsIndividualTransfer;
	}

	/**
	 * Set Functions
	 */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['customer_name_full'] ) ) $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
        if( true == isset( $arrmixValues['lease_sub_status'] ) ) $this->setLeaseSubStatus( $arrmixValues['lease_sub_status'] );
		if( true == isset( $arrmixValues['primary_applicant_name'] ) ) $this->setPrimaryApplicantName( $arrmixValues['primary_applicant_name'] );
		if( true == isset( $arrmixValues['customer_company_name'] ) ) $this->setCustomerCompanyName( $arrmixValues['customer_company_name'] );
		if( true == isset( $arrmixValues['customer_name_first'] ) ) $this->setCustomerNameFirst( $arrmixValues['customer_name_first'] );
		if( true == isset( $arrmixValues['customer_name_middle'] ) ) $this->setCustomerNameMiddle( $arrmixValues['customer_name_middle'] );
		if( true == isset( $arrmixValues['customer_name_last'] ) ) $this->setCustomerNameLast( $arrmixValues['customer_name_last'] );
		if( true == isset( $arrmixValues['customer_name_last_matronymic'] ) ) $this->setCustomerNameLastMatronymic( $arrmixValues['customer_name_last_matronymic'] );
		if( true == isset( $arrmixValues['customer_birth_date'] ) ) $this->setCustomerBirthDate( $arrmixValues['customer_birth_date'] );
		if( true == isset( $arrmixValues['customer_gender'] ) ) $this->setCustomerGender( $arrmixValues['customer_gender'] );
		if( true == isset( $arrmixValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrmixValues['integration_database_id'] );
		if( true == isset( $arrmixValues['customer_email_address'] ) ) $this->setCustomerEmailAddress( $arrmixValues['customer_email_address'] );
		if( true == isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['customer_phone_number'] ) ) $this->setCustomerPhoneNumber( $arrmixValues['customer_phone_number'] );
	    if( true == isset( $arrmixValues['phone_number_type_id'] ) ) $this->setPhoneNumberTypeId( $arrmixValues['phone_number_type_id'] );
		if( true == isset( $arrmixValues['customer_secondary_number'] ) ) $this->setCustomerSecondaryNumber( $arrmixValues['customer_secondary_number'] );
		if( true == isset( $arrmixValues['customer_mobile_number'] ) ) $this->setCustomerMobileNumber( $arrmixValues['customer_mobile_number'] );
		if( true == isset( $arrmixValues['customer_type_name'] ) ) $this->setCustomerTypeName( $arrmixValues['customer_type_name'] );
		if( true == isset( $arrmixValues['customer_relationship_name'] ) ) $this->setCustomerRelationshipName( $arrmixValues['customer_relationship_name'] );
		if( true == isset( $arrmixValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		if( true == isset( $arrmixValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		if( true == isset( $arrmixValues['lease_renewal_date'] ) ) $this->setLeaseRenewalDate( $arrmixValues['lease_renewal_date'] );
		if( true == isset( $arrmixValues['lease_status_type'] ) ) $this->setLeaseStatusType( $arrmixValues['lease_status_type'] );
		if( true == isset( $arrmixValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrmixValues['maintenance_request_id'] );
	    if( true == isset( $arrmixValues['lease_association_id'] ) ) $this->setLeaseAssociationId( $arrmixValues['lease_association_id'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setPropertyUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['floorplan_name'] ) ) $this->setFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['lease_balance'] ) ) $this->setLeaseBalance( $arrmixValues['lease_balance'] );
		if( true == isset( $arrmixValues['property_building_id'] ) ) $this->setPopertyBuildingId( $arrmixValues['property_building_id'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['space_number'] ) ) $this->setSpaceNumber( $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrmixValues['payment_allowance_type_id'] );
		if( true == isset( $arrmixValues['move_in_date'] ) ) $this->setMoveInDate( $arrmixValues['move_in_date'] );
		if( true == isset( $arrmixValues['move_in_form_id'] ) ) $this->setMoveInFormId( $arrmixValues['move_in_form_id'] );
		if( true == isset( $arrmixValues['move_out_date'] ) ) $this->setMoveOutDate( $arrmixValues['move_out_date'] );
	    if( true == isset( $arrmixValues['notice_date'] ) ) $this->setNoticeDate( $arrmixValues['notice_date'] );
		if( true == isset( $arrmixValues['occupant_move_out_date'] ) ) $this->setOccupantMoveOutDate( $arrmixValues['occupant_move_out_date'] );
		if( true == isset( $arrmixValues['move_out_reason_list_item_id'] ) ) $this->setMoveOutReasonListItemId( $arrmixValues['move_out_reason_list_item_id'] );
		if( true == isset( $arrmixValues['move_out_form_id'] ) ) $this->setMoveOutFormId( $arrmixValues['move_out_form_id'] );
		if( true == isset( $arrmixValues['delinquency_amount'] ) ) $this->setDelinquencyAmount( $arrmixValues['delinquency_amount'] );
		if( true == isset( $arrmixValues['pre_grace_days_delinquency_amount'] ) ) $this->setPreGraceDaysDelinquencyAmount( $arrmixValues['pre_grace_days_delinquency_amount'] );
		if( true == isset( $arrmixValues['username'] ) ) $this->setCustomerPortalUsername( $arrmixValues['username'] );
		if( true == isset( $arrmixValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		if( true == isset( $arrmixValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		if( true == isset( $arrmixValues['balance'] ) ) $this->setBalance( $arrmixValues['balance'] );
		if( true == isset( $arrmixValues['balance_date'] ) ) $this->setBalanceDate( $arrmixValues['balance_date'] );
		if( true == isset( $arrmixValues['customer_created_on'] ) ) $this->setCustomerCreatedOn( $arrmixValues['customer_created_on'] );

		if( true == isset( $arrmixValues['company_identification_type'] ) ) $this->setCompanyIdentificationType( $arrmixValues['company_identification_type'] );
		if( true == isset( $arrmixValues['identification_value'] ) ) $this->setIdentificationValue( $arrmixValues['identification_value'] );
		if( true == isset( $arrmixValues['name_prefix'] ) ) $this->setNamePrefix( $arrmixValues['name_prefix'] );
		if( true == isset( $arrmixValues['name_suffix'] ) ) $this->setNameSuffix( $arrmixValues['name_suffix'] );
		if( true == isset( $arrmixValues['name_maiden'] ) ) $this->setNameMaiden( $arrmixValues['name_maiden'] );
		if( true == isset( $arrmixValues['primary_street_line1'] ) ) $this->setPrimaryStreetLine1( $arrmixValues['primary_street_line1'] );
		if( true == isset( $arrmixValues['primary_street_line2'] ) ) $this->setPrimaryStreetLine2( $arrmixValues['primary_street_line2'] );
		if( true == isset( $arrmixValues['primary_street_line3'] ) ) $this->setPrimaryStreetLine3( $arrmixValues['primary_street_line3'] );
		if( true == isset( $arrmixValues['primary_city'] ) ) $this->setPrimaryCity( $arrmixValues['primary_city'] );
		if( true == isset( $arrmixValues['primary_state_code'] ) ) $this->setPrimaryStateCode( $arrmixValues['primary_state_code'] );
		if( true == isset( $arrmixValues['primary_postal_code'] ) ) $this->setPrimaryPostalCode( $arrmixValues['primary_postal_code'] );
		if( true == isset( $arrmixValues['primary_country_code'] ) ) $this->setPrimaryCountryCode( $arrmixValues['primary_country_code'] );
		if( true == isset( $arrmixValues['work_number'] ) ) $this->setWorkNumber( $arrmixValues['work_number'] );
		if( true == isset( $arrmixValues['fax_number'] ) ) $this->setFaxNumber( $arrmixValues['fax_number'] );
		if( true == isset( $arrmixValues['is_resident_portal_account'] ) ) $this->setIsResidentPortalAccount( $arrmixValues['is_resident_portal_account'] );

		if( true == isset( $arrmixValues['guarantee_percent'] ) ) $this->setGuaranteePercent( $arrmixValues['guarantee_percent'] );
		if( true == isset( $arrmixValues['reference'] ) ) $this->setReference( $arrmixValues['reference'] );
		if( true == isset( $arrmixValues['applicant_id'] ) ) $this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['require_screening'] ) ) $this->setRequireScreening( $arrmixValues['require_screening'] );
		if( true == isset( $arrmixValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrmixValues['primary_customer_id'] );
	    if( true == isset( $arrmixValues['preferred_locale_code'] ) ) $this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );
		if( true == isset( $arrmixValues['application_id'] ) ) $this->setApplicationId( $arrmixValues['application_id'] );
	    if( true == isset( $arrmixValues['is_new_lease_customer'] ) ) $this->setRequireScreening( $arrmixValues['is_new_lease_customer'] );
		if( true == isset( $arrmixValues['customer_filter_type'] ) ) {
			$this->setCustomerFilterTypeId( $arrmixValues['customer_filter_type'] );
		}
		if( true == isset( $arrmixValues['cust_id'] ) ) {
			$this->setCustomerContactId( $arrmixValues['cust_id'] );
		}
	    if( true == isset( $arrmixValues['customer_primary_phone_number'] ) ) {
		    $this->setCustomerPrimaryPhoneNumber( $arrmixValues['customer_primary_phone_number'] );
	    }
	    if( true == isset( $arrmixValues['is_individual_transfer'] ) ) {
		    $this->setIsIndividualTransfer( $arrmixValues['is_individual_transfer'] );
	    }
	    $this->setOrganizationContractId( $arrmixValues['organization_contract_id'] ?? NULL );
    }

	public function setPhoneNumberTypeId( $intPhoneNumberTypeId ) {
		$this->m_intPhoneNumberTypeId = $intPhoneNumberTypeId;
	}

	public function setCustomerFilterTypeId( $intCustomerFilterTypeId ) {
		$this->m_intCustomerFilterTypeId = $intCustomerFilterTypeId;
	}

	public function setCustomerContactId( $intCustomerContactId ) {
		$this->m_intCustomerContactId = $intCustomerContactId;
	}

	public function setApplicationId( $strApplicationId ) {
		$this->m_intApplicationId = $strApplicationId;
	}

    public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
        $this->m_intPaymentAllowanceTypeId = $intPaymentAllowanceTypeId;
    }

	public function setLeaseStatusType( $strLeaseStatusType ) {
        $this->m_strLeaseStatusType = $strLeaseStatusType;
    }

	public function setCustomerMobileNumber( $strCustomerMobileNumber ) {
        $this->m_strCustomerMobileNumber = $strCustomerMobileNumber;
    }

    public function setCustomerNameFull( $strCustomerNameFull ) {
        $this->m_strCustomerNameFull = $strCustomerNameFull;
    }

    public function setLeaseSubStatus( $strLeaseSubStatus ) {
        $this->m_strLeaseSubStatus = $strLeaseSubStatus;
    }

    public function setCustomerCompanyName( $strCustomerCompanyName ) {
    	$this->m_strCustomerCompanyName = $strCustomerCompanyName;
    }

	public function setCustomerNameFirst( $strCustomerNameFirst ) {
        $this->m_strCustomerNameFirst = $strCustomerNameFirst;
    }

	public function setCustomerNameMiddle( $strCustomerNameMiddle ) {
        $this->m_strCustomerNameMiddle = $strCustomerNameMiddle;
    }

	public function setCustomerNameLast( $strCustomerNameLast ) {
		$this->m_strCustomerNameLast = $strCustomerNameLast;
	}

	public function setCustomerNameLastMatronymic( $strCustomerNameLastMatronymic ) {
		$this->m_strCustomerNameLastMatronymic = $strCustomerNameLastMatronymic;
	}

	public function setCustomerBirthDate( $strCustomerBirthDate ) {
        $this->m_strCustomerBirthDate = $strCustomerBirthDate;
    }

	public function setCustomerGender( $strGender ) {
        $this->m_strCustomerGender = $strGender;
    }

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
        $this->m_intIntegrationDatabaseId = $intIntegrationDatabaseId;
    }

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
        $this->m_strCustomerEmailAddress = $strCustomerEmailAddress;
    }

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setCustomerPhoneNumber( $strCustomerPhoneNumber ) {
        $this->m_strCustomerPhoneNumber = $strCustomerPhoneNumber;
    }

	public function setCustomerSecondaryNumber( $strCustomerSecondaryNumber ) {
        $this->m_strCustomerSecondaryNumber = $strCustomerSecondaryNumber;
    }

	public function setCustomerTypeName( $strCustomerTypeName ) {
        $this->m_strCustomerTypeName = $strCustomerTypeName;
    }

	public function setCustomerRelationshipName( $strCustomerRelationshipName ) {
        $this->m_strCustomerRelationshipName = $strCustomerRelationshipName;
    }

    public function setLeaseStartDate( $strLeaseStartDate ) {
    	$this->m_strLeaseStartDate = $strLeaseStartDate;
    }

    public function setLeaseEndDate( $strLeaseEndDate ) {
        $this->m_strLeaseEndDate = $strLeaseEndDate;
    }

    public function setLeaseRenewalDate( $strLeaseRenewalDate ) {
    	$this->m_strLeaseRenewalDate = $strLeaseRenewalDate;
    }

	public function setPropertyUnitNumber( $strPropertUnitNumber ) {
        $this->m_strPropertyUnitNumber = $strPropertUnitNumber;
    }

	public function setFloorplanName( $strFloorplanName ) {
        $this->m_strFloorplanName = $strFloorplanName;
    }

	public function setSpaceNumber( $strSpaceNumber ) {
        $this->m_strSpaceNumber = $strSpaceNumber;
    }

	public function setLeaseBalance( $fltLeaseBalance ) {
        $this->m_fltLeaseBalance = $fltLeaseBalance;
    }

	public function setPopertyBuildingId( $intPopertyBuildingId ) {
        $this->m_intPopertyBuildingId = $intPopertyBuildingId;
    }

	public function setBuildingName( $strBuildingName ) {
        $this->m_strBuildingName = $strBuildingName;
    }

	public function setPropertyName( $strPropertyName ) {
        $this->m_strPropertyName = $strPropertyName;
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = $intPropertyId;
    }

  	public function setMoveInDate( $strMoveInDate ) {
		return $this->m_strMoveInDate = $strMoveInDate;
    }

	public function setMoveOutDate( $strMoveOutDate ) {
		return $this->m_strMoveOutDate = $strMoveOutDate;
    }

    public function setOccupantMoveOutDate( $strOccupantMoveOutDate ) {
    	return $this->m_strOccupantMoveOutDate = $strOccupantMoveOutDate;
    }

    public function setMoveOutReasonListItemId( $intMoveOutReasonListItemId ) {
    	return $this->m_intMoveOutReasonListItemId = $intMoveOutReasonListItemId;
    }

    public function setNoticeDate( $strNoticeDate ) {
    	return $this->m_strNoticeDate = $strNoticeDate;
    }

    public function setMoveInFormId( $intMoveInFormId ) {
    	return $this->m_intMoveInFormId = $intMoveInFormId;
    }

    public function setMoveOutFormId( $intMoveOutFormId ) {
    	return $this->m_intMoveOutFormId = $intMoveOutFormId;
    }

    public function setDelinquencyAmount( $fltDelinquencyAmount ) {
    	return $this->m_fltDelinquencyAmount = $fltDelinquencyAmount;
    }

    public function setPreGraceDaysDelinquencyAmount( $fltPreGraceDaysDelinquencyAmount ) {
    	return $this->m_fltPreGraceDaysDelinquencyAmount = $fltPreGraceDaysDelinquencyAmount;
    }

    public function setCustomerPortalUsername( $strCustomerPortalUsername ) {
    	return $this->m_strCustomerPortalUsername = $strCustomerPortalUsername;
    }

    public function setEventTypeId( $intEventTypeId ) {
    	$this->m_intEventTypeId = $intEventTypeId;
    }

    public function setPropertyUnitId( $intPropertyUnitId ) {
    	return $this->m_intPropertyUnitId = $intPropertyUnitId;
    }

    public function setUnitSpaceId( $intUnitSpaceId ) {
    	return $this->m_intUnitSpaceId = $intUnitSpaceId;
    }

    public function setBalance( $fltBalance ) {
    	$this->m_fltBalance = $fltBalance;
    }

    public function setBalanceDate( $strBalanceDate ) {
    	return $this->m_strBalanceDate = $strBalanceDate;
    }

    public function setCustomerCreatedOn( $strCustomerCreatedOn ) {
    	return $this->m_strCustomerCreatedOn = $strCustomerCreatedOn;
    }

    public function setCompanyIdentificationType( $strCompanyIdentificationType ) {
    	$this->m_strCompanyIdentificationType = $strCompanyIdentificationType;
    }

    public function setIdentificationValue( $strIdentificationValue ) {
    	$this->m_strIdentificationValue = CStrings::strTrimDef( $strIdentificationValue, 240, NULL, true );
    }

    public function setNamePrefix( $strNamePrefix ) {
    	$this->m_strNamePrefix = $strNamePrefix;
    }

    public function setNameSuffix( $strNameSuffix ) {
    	$this->m_strNameSuffix = $strNameSuffix;
    }

    public function setNameMaiden( $strNameMaiden ) {
    	$this->m_strNameMaiden = $strNameMaiden;
    }

    public function setPrimaryStreetLine1( $strPrimaryStreetLine1 ) {
    	$this->m_strPrimaryStreetLine1 = $strPrimaryStreetLine1;
    }

    public function setPrimaryStreetLine2( $strPrimaryStreetLine2 ) {
    	$this->m_strPrimaryStreetLine2 = $strPrimaryStreetLine2;
    }

    public function setPrimaryStreetLine3( $strPrimaryStreetLine3 ) {
    	$this->m_strPrimaryStreetLine3 = $strPrimaryStreetLine3;
    }

    public function setPrimaryCity( $strPrimaryCity ) {
    	$this->m_strPrimaryCity = $strPrimaryCity;
    }

    public function setPrimaryStateCode( $strPrimaryStateCode ) {
    	$this->m_strPrimaryStateCode = $strPrimaryStateCode;
    }

    public function setPrimaryPostalCode( $strPrimaryPostalCode ) {
    	$this->m_strPrimaryPostalCode = $strPrimaryPostalCode;
    }

    public function setPrimaryCountryCode( $strPrimaryCountryCode ) {
    	$this->m_strPrimaryCountryCode = $strPrimaryCountryCode;
    }

    public function setWorkNumber( $strWorkNumber ) {
    	$this->m_strWorkNumber = $strWorkNumber;
    }

    public function setFaxNumber( $strFaxNumber ) {
    	$this->m_strFaxNumber = $strFaxNumber;
    }

	public function setIsResidentPortalAccount( $intIsResidentPortalAccount ) {
		return $this->m_intIsResidentPortalAccount = $intIsResidentPortalAccount;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->m_intPrimaryCustomerId = $intPrimaryCustomerId;
	}

	public function setIntervalDatetime( $strIntervalDatetime ) {
		$this->m_strIntervalDatetime = $strIntervalDatetime;
	}

	public function setMoneyGramAccountId( $intMoneyGramAccountId ) {
		$this->m_intMoneyGramAccountId = $intMoneyGramAccountId;
	}

	public function setIsSyncApplicationAndLease( $intIsSyncApplicationAndLease ) {
		$this->m_intIsSyncApplicationAndLease = $intIsSyncApplicationAndLease;
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->m_intApplicantApplicationId = $intApplicantApplicationId;
	}

	public function setoldUnitNumberCache( $strOldUnitNumberCache ) {
		$this->m_strOldUnitNumberCache = $strOldUnitNumberCache;
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->m_intMaintenanceRequestId = $intMaintenanceRequestId;
	}

	public function setLeaseAssociationId( $intLeaseAssociationId ) {
		$this->m_intLeaseAssociationId = $intLeaseAssociationId;
	}

	public function setMergeFields( $arrstrMergeFields ) {
		foreach( $arrstrMergeFields as $strKey => $strValue ) {
			$this->setMergeFieldByKey( $strKey, $strValue );
		}
	}

	public function setMergeFieldByKey( $strKey, $strValue ) {
		$this->m_arrstrMergeFields[$strKey] = $strValue;
	}

	public function setRequireScreening( $intRequireScreening ) {
		$this->m_intRequireScreening = CStrings::strToIntDef( $intRequireScreening, NULL, false );
	}

	public function setPrimaryApplicantName( $strPrimaryApplicantName ) {
		$this->m_strPrimaryApplicantName = $strPrimaryApplicantName;
	}

	public function setReference( $intReference ) {
		$this->m_intReference = $intReference;
	}

	public function setGuaranteePercent( $fltGuaranteePercent ) {
			$this->m_fltGuaranteePercent = $fltGuaranteePercent;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = CStrings::strToIntDef( $intOccupancyTypeId, NULL, false );
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
    	$this->m_strPreferredLocaleCode = $strPreferredLocaleCode;
	}

	public function setRequiredParameters( $arrmixParameters ) {
		$this->m_arrmixRequiredParameters = $arrmixParameters;
	}

	public function setIsNewLeaseCustomer( $intIsNewLeaseCustomer ) {
		$this->m_intIsNewLeaseCustomer = $intIsNewLeaseCustomer;
	}

	public function setCustomerPrimaryPhoneNumber( $strCustomerPrimaryPhoneNumber ) {
		$this->m_strCustomerPrimaryPhoneNumber = $strCustomerPrimaryPhoneNumber;
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
    	$this->m_intOrganizationContractId = $intOrganizationContractId;
	}

	public function setIsIndividualTransfer( $boolIsIndividualTransfer ) {
		$this->m_boolIsIndividualTransfer = $boolIsIndividualTransfer;
	}

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
            $boolIsValid = false;
            trigger_error( 'Invalid Company Lease Customer Request:  Id required - CLeaseCustomer::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Customer Request:  Management Id required - CLeaseCustomer::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intLeaseId ) || 0 >= ( int ) $this->m_intLeaseId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Customer Request:  Company Lease Id required - CLeaseCustomer::valLeaseId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

	public function valGuaranteePercent() {

		$boolIsValid = true;
		if( false == valStr( $this->getGuaranteePercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_guarantor_percentage', __( 'Guarantee Percentage is required.' ) ) ] );
		}
		return $boolIsValid;
	}

    public function valCustomerId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Customer Request:  Customer Id required - CLeaseCustomer::valCustomerId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valLeaseStatusTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intLeaseStatusTypeId ) || 0 >= ( int ) $this->m_intLeaseStatusTypeId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Customer Request:  Customer Status Type Id required - CLeaseCustomer::valLeaseStatusTypeId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valOccupantMoveOutDate( $objDatabase = NULL ) {

    	$boolIsValid = true;
    	$strErrormsg = '';

        if( true == valStr( $this->getOccupantMoveOutDate() ) ) {
        	$mixValidatedMoveOutDate = CValidation::checkISODateFormat( $this->getOccupantMoveOutDate(), true );

           	if( false === $mixValidatedMoveOutDate ) {
        		$boolIsValid = false;
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupant_move_out_date', __( 'Move-out date is not valid' ) ) ] );
        	} else {

        		$this->setOccupantMoveOutDate( $mixValidatedMoveOutDate );
        		$objLease 			= $this->fetchLease( $objDatabase );
				$arrstrLeaseDates 	= \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseDatesByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

        		if( true == valStr( $this->getMoveInDate() ) ) {
        			$strMinimumDate = ( strtotime( $this->getMoveInDate() ) > strtotime( $arrstrLeaseDates['lease_start_date'] ) ) ? $this->getMoveInDate() : $arrstrLeaseDates['lease_start_date'];
        		} else {
        			$strMinimumDate = ( strtotime( $objLease->getMoveInDate() ) > strtotime( $strLeaseStartDate ) ) ? $objLease->getMoveInDate() : $strLeaseStartDate;
        		}

        		if( strtotime( $this->getOccupantMoveOutDate() ) < strtotime( $strMinimumDate ) ) {
        			$boolIsValid = false;
        			$strErrormsg .= __( 'Move out date must be greater than or equal to Lease Start Date and Move-in Date.' );
        		}

        		if( true == valStr( $objLease->getLeaseEndDate() ) && strtotime( $this->getOccupantMoveOutDate() ) > strtotime( $objLease->getLeaseEndDate() ) ) {
        			$boolIsValid = false;
        			$strErrormsg .= __( 'Move out date must be less than or equal to Lease End Date.' );
        		}

        		if( false == $boolIsValid ) {
        			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupant_move_out_date', $strErrormsg ) ] );
        		}
        	}
        } else {
        	$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupant_move_out_date', __( 'Move-out date is required.' ) ) ] );

        }

        return $boolIsValid;
    }

	public function valCustomerRelationshipId( $objDatabase ) {

		if( false == valStr( $this->getCustomerRelationshipId() ) ) {
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Relationship is required. Enter a relationship for all occupants on the lease.' ) ) ] );
			return false;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {

			// this is the temporary fix to avoid inserting bad data of customer relationships with respect to customer type.
    		$arrmixCustomerRelationships = CCustomerRelationships::fetchCustomerRelationshipsByIdByCustomerTypeIdByCid( $this->getCustomerRelationshipId(), $this->getCustomerTypeId(), $this->getCid(), $objDatabase );
    		if( false == valArr( $arrmixCustomerRelationships ) ) {
    			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Wrong customer relationship got set for selected customer type.' ) ) ] );
    			return false;
    		}
		}

    	return true;
    }

    public function valRefundPercent( $arrmixDataToValidate = NULL ) {
		$boolIsValid = true;
    	if( CRefundMethod::MULTIPLE_REFUNDS == getArrayElementByKey( 'refund_method_id', $arrmixDataToValidate ) && true == $this->getIncludeOnFmoStatement() && 0 >= ( float ) $this->getRefundPercent() ) {
    		$boolIsValid = false;
    		$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'refund_percent', __( 'Refund percent must be greater than 0 for selected resident(s)' ) ) ] );
    	}
    	return $boolIsValid;
    }

	public function valEmailAddressExist( $objDatabase ) {
		$boolIsValid = true;
		$intCustomerId = NULL;

		if( COccupancyType::COMMERCIAL === $this->getOccupancyTypeId() ) {
			if( true == valStr( $this->getCustomerCreatedOn() ) ) {
				$intCustomerId = $this->getCustomerId();
			}
			$intCustomersCount = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersCountByEmailAddressByLeaseIdByCid( $this->getCustomerEmailAddress(), $this->getLeaseId(), $this->getCid(), $objDatabase, $intCustomerId );
			if( $intCustomersCount > 0 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'count', __( 'A user with this email already exists.' ) ) );
			}
		}
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL, $arrmixDataToValidate = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
			case 'pre_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				break;

            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valLeaseStatusTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId( $objDatabase );
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseStatusTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId( $objDatabase );
				$boolIsValid &= $this->valRefundPercent( $arrmixDataToValidate );
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            case 'application_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseStatusTypeId();
            case 'guest_card_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseStatusTypeId();
            	break;

            case 'validate_occupant_moveout_date':
            	 $boolIsValid &= $this->valOccupantMoveOutDate( $objDatabase );
				break;

			case 'validate_commercial_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valLeaseStatusTypeId();
				$boolIsValid &= $this->valEmailAddressExist( $objDatabase );
				break;

			case 'primary_resident_correction':
				$boolIsValid &= $this->valCustomerRelationshipId( $objDatabase );
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function getLeaseStatusTypeName( $boolIsFromResidentSystem=false ) {

    	$strStatusTypeName = NULL;

    	switch( $this->getLeaseStatusTypeId() ) {
    		case CLeaseStatusType::APPLICANT:
				$strStatusTypeName = __( 'Applicant' );
				break;

    		case CLeaseStatusType::CANCELLED:
				$strStatusTypeName = __( 'Cancelled' );
				break;

			case CLeaseStatusType::FUTURE:
				$strStatusTypeName = __( 'Future' );
				break;

    		case CLeaseStatusType::CURRENT:
    			$strStatusTypeName = __( 'Current' );

    			if( CCustomerType::RESPONSIBLE == $this->getCustomerTypeId() ) {
					$strStatusTypeName = __( 'Co-Applicant' );
					if( COccupancyType::COMMERCIAL == $this->getOccupancyTypeId() ) {
						$strStatusTypeName = __( 'Associated Lease' );
					} elseif( true == $boolIsFromResidentSystem ) {
						$strStatusTypeName = __( 'Roommate' );
    				}
    			} elseif( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
    				$strStatusTypeName = __( 'Primary' );
    			} elseif( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) {
    				$strStatusTypeName = __( 'Guarantor' );
    			} elseif( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {
    				$strStatusTypeName = __( 'Occupant' );
    			}
				break;

    		case CLeaseStatusType::NOTICE:
				$strStatusTypeName = __( 'On Notice' );
				break;

    		case CLeaseStatusType::PAST:
				$strStatusTypeName = __( 'Past' );
				break;

    		default:
    			// default case
    			break;
    	}

    	return $strStatusTypeName;
    }

    public function getDefaultCustomerTypeId() {

    	$intCustomerTypeId = NULL;

		switch( $this->getLeaseStatusTypeId() ) {

    		case CLeaseStatusType::CURRENT:
    		case CLeaseStatusType::NOTICE:
    		case CLeaseStatusType::APPLICANT:
    		case CLeaseStatusType::PAST:

    			if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
					$intCustomerTypeId = CCustomerType::PRIMARY;
    			} elseif( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) {
					$intCustomerTypeId = CCustomerType::GUARANTOR;
    			} else {
    				$intCustomerTypeId = CCustomerType::RESPONSIBLE;
    			}
				break;

    		default:
				$intCustomerTypeId = CCustomerType::RESPONSIBLE;
				break;
    	}

    	return $intCustomerTypeId;
    }

    /**
     * Fetch Functions
     */

    public function fetchCustomer( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
    }

    public function fetchLease( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeaseByIdByCid( $this->m_intLeaseId, $this->m_intCid, $objDatabase );
    }

	public function fetchLeasesCountByCid( $objDatabase ) {
		$strSql = 'SELECT count(lease_id) FROM lease_customers where customer_id = ' . $this->m_intCustomerId . 'AND cid = ' . $this->m_intCid;
		$arrintLeasesCount = fetchData( $strSql, $objDatabase );
		return $arrintLeasesCount[0]['count'];
	}

	public function fetchFileAssociationBySystemCode( $strSystemCode, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileAssociations::createService()->fetchFileAssociationByLeaseIdByCustomerIdBySystemCodeByCid( $this->m_intLeaseId, $this->m_intCustomerId, $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileByFileTypeSystemCode( $strFileTypeSystemCode, $objDatabase ) {
		return CFiles::fetchFileByCustomerIdByFileTypeSystemCodeByCid( $this->m_intCustomerId, $strFileTypeSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileSignaturesCountByFileAssociationIds( $arrintFileAssociationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesCountByCustomerIdLeaseIdByFileAssociationIdsByCid( $this->m_intCustomerId, $this->m_intLeaseId, $arrintFileAssociationIds, $this->m_intCid, $objDatabase );
	}

	public function fetchFileSignaturesByFileAssociationIds( $arrintFileAssociationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByCustomerIdLeaseIdByFileAssociationIdsByCid( $this->m_intCustomerId, $this->m_intLeaseId, $arrintFileAssociationIds, $this->m_intCid, $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function buildFileAssociationFilePath() {
		return 'leases/residents/' . $this->getLeaseId() . '/';
	}

	public function buildFileAssociationFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildFileAssociationFilePath();
	}

	public function buildFileAssociationFileName( $strExtension = 'docx', $objDocument = NULL ) {

	    if( true == valObj( $objDocument, 'CDocument' ) && CDocumentSubType::DOCUMENT_TEMPLATE == $objDocument->getDocumentSubTypeId() ) {
	        $strFileNamePrefix = 'lease_template_' . $objDocument->getId() . '_';
	    } else {
	        $strFileNamePrefix = 'lease_document_';
	    }

	    return $strFileNamePrefix . $this->getId() . '_' . time() . '.' . $strExtension;

	}

	public function buildFilePath() {
		return date( 'Y/m/d', strtotime( $this->getIntervalDateTime() ) ) . '/' . $this->getLeaseIntervalId() . '/';
	}

	public function buildFileName( $strExtension = 'docx', $objDocument = NULL ) {

		if( true == valObj( $objDocument, 'CDocument' ) && CDocumentSubType::DOCUMENT_TEMPLATE == $objDocument->getDocumentSubTypeId() ) {
			$strFileNamePrefix = 'addendum_template_' . $objDocument->getId() . '_';
		} else {
			$strFileNamePrefix = 'addendum_document_';
		}

		return $strFileNamePrefix . $this->getId() . '_' . date( 'Ymdhis' ) . '_' . mt_rand() . '.' . $strExtension;
	}

	public function logActivityForOccupantStatusChange( $objCompanyUser, $intOldOccupantStatusTypeId = NULL, $intNewOccupantStatusTypeId = NULL, $objOldUnit, $objNewUnit, $objDatabase ) {

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();

		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );
		$objSelectedEvent = $objEventLibrary->createEvent( [ $this ], CEventType::STATUS_CHANGE );
		$objSelectedEvent->setCid( $this->getCid() );
		$objSelectedEvent->setPropertyId( $this->getPropertyId() );

		$objSelectedEvent->setCompanyUser( $objCompanyUser );

		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objSelectedEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
			$objSelectedEvent->setCompanyEmployee( $objCompanyEmployee );
		}

		$objSelectedEvent->setOldStatusId( $intOldOccupantStatusTypeId );
		$objSelectedEvent->setNewStatusId( $intNewOccupantStatusTypeId );
		$objSelectedEvent->setOldReferenceData( $objOldUnit );
		$objSelectedEvent->setNewReferenceData( $objNewUnit );
		$objSelectedEvent->setDataReferenceId( $this->getId() );

		if( true == isset( $objOldUnit ) && valObj( $objOldUnit, 'CUnitSpace' ) ) {
			$objSelectedEvent->setUnitSpaceIds( [ $objOldUnit->getId() ] );
		}

		$objSelectedEvent->setEventDatetime( 'NOW()' );

		$objEventLibrary->buildEventDescription( $this );

		return $objEventLibrary;
	}

	public function logActivityForOccupantTypeChange( $objCompanyUser, $intOldOccupantTypeId = NULL, $intNewOccupantTypeId = NULL, $objLeaseCustomer = NULL, $objDatabase ) {

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objSelectedEvent = $objEventLibrary->createEvent( [ $this ], CEventType::CUSTOMER_TYPE_CHANGE );
		$objSelectedEvent->setCid( $this->getCid() );
		$objSelectedEvent->setLeaseId( $this->getLeaseId() );
		$objSelectedEvent->setPropertyId( $this->getPropertyId() );
		$objSelectedEvent->setCompanyUser( $objCompanyUser );
		$objSelectedEvent->setOldStatusId( $intOldOccupantTypeId );
		$objSelectedEvent->setNewStatusId( $intNewOccupantTypeId );
		$objSelectedEvent->setDataReferenceId( $this->getId() );
		$objSelectedEvent->setEventDatetime( 'NOW()' );

		$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );
		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objSelectedEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
			$objSelectedEvent->setCompanyEmployee( $objCompanyEmployee );
		}

		$objEventLibrary->buildEventDescription( $objLeaseCustomer );
		return $objEventLibrary;
	}

	public function isTransferredOccupant( $objDatabase ) {
		$boolIsTransferredOccupant = false;
		$arrobjEvents = \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventsByReferenceTypeIdByEventTypeIdByReferenceIdsByPropertyIdByCid( CEventReferenceType::LEASE_CUSTOMER, CEventType::STATUS_CHANGE, [ $this->getId() ], $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjEvents ) ) {
			$arrobjEvents = rekeyObjects( 'DataReferenceId', $arrobjEvents );
			if( true == array_key_exists( $this->getId(), $arrobjEvents ) && CLeaseStatusType::PAST == $arrobjEvents[$this->getId()]->getNewStatusId() && CLeaseStatusType::PAST == $this->getLeaseStatusTypeId() && CCustomerType::PRIMARY != $this->getCustomerTypeId() ) {
				$boolIsTransferredOccupant = true;
			}
		}

		return $boolIsTransferredOccupant;
	}

	public function updateUnitSpaceStatusType( $objOriginalLeaseCustomer, $intCompanyUserId, $objDatabase, $boolDontLogEvents = false ) {

		$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getLeaseId(), $this->getCid(), $objDatabase, false );

		$objUnitSpaceStatusManager = new CUnitSpaceStatusManager();

		if( false == valObj( $objLease, 'CLease' ) ) {
			trigger_error( 'Failed to load lease object.', E_USER_ERROR );
			return false;
		}

		$objUnitSpace = $objLease->getOrFetchUnitSpace( $objDatabase );

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			if( false == $objUnitSpaceStatusManager->changeUnitSpaceStatus( $intCompanyUserId, $objUnitSpace, $objOriginalLeaseCustomer, $this, $objLease, $objDatabase, $boolDontLogEvents ) ) {
				$this->addErrorMsgs( $objUnitSpaceStatusManager->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function insert( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsIntegrated = false, $boolDontUpdateUnitStatus = false, $boolDontLogEvents = false ) {

		if( true == is_null( $this->getCustomerRelationshipId() ) ) {
			$objCustomerRelationship = CCustomerRelationships::fetchDefaultCustomerRelationshipByOccupancyTypeIdByPropertyIdByCustomerTypeIdByCid( COccupancyType::CONVENTIONAL, $this->getPropertyId(), $this->getCustomerTypeId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objCustomerRelationship, 'CCustomerRelationship' ) ) {
				$this->setCustomerRelationshipId( $objCustomerRelationship->getId() );
			}
		}

		if( true == $boolIsIntegrated || true == $boolReturnSqlOnly ) {
			return parent::insert( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == parent::insert( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == $this->getIsSyncApplicationAndLease() && true == is_null( $this->m_intApplicantApplicationId ) && false == CApplicationUtils::insertApplicantAndApplicantApplication( $this, $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		if( true != $this->getIsUngrouped() && $this->getCustomerTypeId() == CCustomerType::PRIMARY ) {
			if( false == $boolDontUpdateUnitStatus ) {
				if( false == $this->updateUnitSpaceStatusType( NULL, $intCompanyUserId, $objDatabase, $boolDontLogEvents ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsIntegrated = false, $boolIsNotPrimaryLeaseCustomer = false, $boolDontUpdateUnitStatus = false, $boolIsAllowUpdateLeaseStatus = false, $boolDontLogEvents = false, $arrintDontUpdateCustomerIds = [] ) {

		if( true == $boolIsNotPrimaryLeaseCustomer || true == $boolIsIntegrated || true == $boolReturnSqlOnly ) {
			return parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$objOriginalLeaseCustomer = $this->fetchOriginalLeaseCustomer( $objDatabase );

		if( false == valObj( $objOriginalLeaseCustomer, 'CLeaseCustomer' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load Original lease customer.' ) ) );
			return false;
		}

		if( $objOriginalLeaseCustomer->getLeaseStatusTypeId() == $this->getLeaseStatusTypeId() ) {
			return parent::update( $intCompanyUserId, $objDatabase );
		}

		// Adding condition to allow update lease status individually for non-primary
		$arrintTransferTypes 	= [ CLeaseIntervalType::TRANSFER, CLeaseIntervalType::RENEWAL ];
		$arrintLeaseStatusTypes = [ CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT, CLeaseStatusType::CANCELLED ];

		if( $this->getCustomerTypeId() != CCustomerType::PRIMARY && false == $boolIsNotPrimaryLeaseCustomer ) {

			$objLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchActiveLeaseIntervalByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolIncludeCancelledLeaseInterval = true );

			if( true == valObj( $objLeaseInterval, 'CLeaseInterval' )
					&& true == in_array( $objLeaseInterval->getLeaseIntervalTypeId(), $arrintTransferTypes )
					&& true == in_array( $objLeaseInterval->getLeaseStatusTypeId(), $arrintLeaseStatusTypes ) ) {
						$boolIsAllowUpdateLeaseStatus = true;
			}
		}

		if( false == $boolIsAllowUpdateLeaseStatus && true != $this->getIsUngrouped() && $this->getCustomerTypeId() != CCustomerType::PRIMARY && false == $boolIsNotPrimaryLeaseCustomer && false == $this->getIsIndividualTransfer() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Independent update of lease status type on non-primary lease customer is not allowed.' ) ) );
			return false;
		}

		if( false == parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true != $this->getIsUngrouped() && $this->getCustomerTypeId() == CCustomerType::PRIMARY ) {

			$arrobjLeaseCustomers = ( array ) \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchNonPrimaryGroupedLeaseCustomersByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, $arrintDontUpdateCustomerIds );

			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {

				$objLeaseCustomer->setLeaseStatusTypeId( $this->getLeaseStatusTypeId() );

				if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsIntegrated = false, $boolIsNotPrimaryLeaseCustomer = true, $boolDontUpdateUnitStatus ) ) {

					return false;
				}
			}

			if( false == $boolDontUpdateUnitStatus ) {
				if( false == $this->updateUnitSpaceStatusType( $objOriginalLeaseCustomer, $intCompanyUserId, $objDatabase, $boolDontLogEvents ) ) {
					return false;
				}
			}
		}

		// Update lease_status_type_id & active_lease_interval_id.
		if( CCustomerType::PRIMARY == $this->getCustomerTypeId() && ( $objOriginalLeaseCustomer->getCustomerTypeId() != $this->getCustomerTypeId() || $objOriginalLeaseCustomer->getLeaseStatusTypeId() != $this->getLeaseStatusTypeId() ) ) {
			if( false == \Psi\Eos\Entrata\CLeases::createService()->syncLeaseStatusTypeAndActiveLeaseInterval( $this->getCid(), $this->getLeaseId(), $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to sync lease status type and active lease interval.' ) ) );
	    		return false;
	    	}
		}

		// Update lease_cam_share and scheduled_charges
		$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		if( true === valObj( $objLease, 'CLease' ) && COccupancyType::COMMERCIAL == $objLease->getOccupancyTypeId()
		    && ( ( $objOriginalLeaseCustomer->getLeaseStatusTypeId() == CLeaseStatusType::NOTICE && $this->getLeaseStatusTypeId() == CLeaseStatusType::PAST )
		         || ( $objOriginalLeaseCustomer->getLeaseStatusTypeId() == CLeaseStatusType::PAST && $this->getLeaseStatusTypeId() == CLeaseStatusType::NOTICE ) ) ) {

			$objTenantLibrary = new CTenantLibrary();
			$objTenantLibrary->setLease( $objLease );
			$objTenantLibrary->setDatabase( $objDatabase );

			$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );
			$objTenantLibrary->setClient( $objClient );

			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );
			$objTenantLibrary->setCompanyUser( $objCompanyUser );

			if( true == $objTenantLibrary->prepareDataToUpdatePoolSharesAndCharges()
			    && true == $objTenantLibrary->calculatePoolSharesAndChargesForUpdate()
			    && false == $objTenantLibrary->updatePoolSharesAndCharges() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to update pool share and charge amount.' ) ) );

				return false;
			}

		}

		return true;
	}

	public function fetchOriginalLeaseCustomer( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomerByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function isActive() {
		if( true == in_array( $this->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {
			return true;
		}
		return false;
	}

	public function performMoveIn( $strMoveInDate, $intCompanyUserId, $objDatabase, $boolIsUnitTransfer = false, $intLeaseStatusTypeId = CLeaseStatusType::CURRENT, $boolIsFromLeasemodification = false, $boolIsUpdateNonLeasingOccupant = false ) {

		$boolIsValid 		= true;

		$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );
		$objLeaseProcess->setCustomerRelationshipId( $this->getCustomerRelationshipId() );

		$objLeaseProcess->setMoveInDate( $strMoveInDate );
		$objLeaseProcess->setNoticeDate( NULL );
		$objLeaseProcess->setMoveOutDate( NULL );
		$objLeaseProcess->setTerminationStartDate( NULL );

		if( false == $boolIsUnitTransfer && ( false == $objLeaseProcess->validate( 'validate_move_in', $objDatabase ) || false == $objLeaseProcess->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
		}

		( true == $boolIsUnitTransfer ) ? $this->setLeaseStatusTypeId( CLeaseStatusType::FUTURE ) : $this->setLeaseStatusTypeId( $intLeaseStatusTypeId );

		if( false == $boolIsFromLeasemodification ) {
			$this->setIsUngrouped( 0 );
			$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );
		}

		if( true == $boolIsUpdateNonLeasingOccupant ) {
			$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );
		}

		return $boolIsValid;
	}

	public function performMoveInDateForMidLeaseApplicant( $strMoveInDate, $intCompanyUserId, $objDatabase ) {

		$boolIsValid = true;

		$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );
		$objLeaseProcess->setCustomerRelationshipId( $this->getCustomerRelationshipId() );

		$objLeaseProcess->setNoticeDate( NULL );
		$objLeaseProcess->setMoveOutDate( NULL );
		$objLeaseProcess->setTerminationStartDate( NULL );

		if( false == valId( $objLeaseProcess->getId() ) ) {
			$objLeaseProcess->setMoveInDate( $strMoveInDate );
			if( false == $objLeaseProcess->validate( 'validate_move_in', $objDatabase ) || false == $objLeaseProcess->insert( $intCompanyUserId, $objDatabase ) ) {
				$boolIsValid &= false;
				$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
			}
		}

		return $boolIsValid;
	}

    public function performPlaceOnFuture( $strMoveInDate, $intCompanyUserId, $objDatabase, $boolIsRenewalTransfer ) {

    	$boolIsValid 		= true;

    	$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    	$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );
    	$objLeaseProcess->setCustomerRelationshipId( $this->getCustomerRelationshipId() );

    	$objLeaseProcess->setMoveInDate( $strMoveInDate );
    	$objLeaseProcess->setNoticeDate( NULL );
    	$objLeaseProcess->setMoveOutDate( NULL );
    	$objLeaseProcess->setTerminationStartDate( NULL );
    	if( false == $boolIsRenewalTransfer && ( false == $objLeaseProcess->validate( 'validate_move_in', $objDatabase ) || false == $objLeaseProcess->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
    	}

    	if( true == $boolIsRenewalTransfer ) {
            $this->setIsUngrouped( 0 );
        }

    	$this->setLeaseStatusTypeId( CLeaseStatusType::FUTURE );

    	$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );

    	return $boolIsValid;
    }

	public function performPlaceOnNotice( $strNoticeDate, $strMoveOutDate, $intMoveOutReasonId, $intCompanyUserId, $objDatabase, $boolIsUpdateLeaseCustomer = true, $boolIsFromRenewalLeaseApproval = false ) {

		$boolIsValid = true;
		if( true == $boolIsFromRenewalLeaseApproval ) {
			$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessForLeaseApprovalByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		} else {
			$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}
		$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );
		$objLeaseProcess->setCustomerRelationshipId( $this->getCustomerRelationshipId() );
		$objLeaseProcess->setNoticeDate( $strNoticeDate );
		$objLeaseProcess->setMoveOutDate( $strMoveOutDate );
		$objLeaseProcess->setTerminationStartDate( NULL );
		$objLeaseProcess->setRefundPaymentTypeId( CPaymentType::CASH );
		$objLeaseProcess->setMoveOutReasonListItemId( $intMoveOutReasonId );

		if( false == $objLeaseProcess->validate( 'mid_lease_move_out', $objDatabase ) || false == $objLeaseProcess->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
		}

		if( true == $boolIsUpdateLeaseCustomer ) {
			$this->setIsUngrouped( true );
			$this->setLeaseStatusTypeId( CLeaseStatusType::NOTICE );

			$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );
		}

		return $boolIsValid;
	}

	public function performCancelNotice( $intCompanyUserId, $intLeaseStatusTypeId, $objDatabase ) {

		$boolIsValid = true;

		$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchCustomLeaseProcessByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $this->getLeaseId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objLeaseProcess, 'CLeaseProcess' ) ) {
			$objLeaseProcess->setNoticeDate( NULL );
			$objLeaseProcess->setMoveOutDate( NULL );
			$objLeaseProcess->setTerminationStartDate( NULL );
			$objLeaseProcess->setMoveOutReasonListItemId( NULL );
			$objLeaseProcess->setFmoStartedOn( NULL );
			$objLeaseProcess->setFmoApprovedOn( NULL );
            $objLeaseProcess->setFmoProcessedOn( NULL );

			if( false == $objLeaseProcess->update( $intCompanyUserId, $objDatabase ) ) {
				$boolIsValid &= false;
				$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
			}
		}

		$this->setIsUngrouped( false );
		$this->setLeaseStatusTypeId( $intLeaseStatusTypeId );

		$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );

		return $boolIsValid;
	}

	public function performMoveOut( $strMoveOutDate, $intCompanyUserId, $objDatabase, $intMoveOutReasonId = NULL, $boolIsUpdateLeaseCustomer = false ) {

		$boolIsValid = true;

		$objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$objLeaseProcess->setCustomerTypeId( $this->getCustomerTypeId() );
		$objLeaseProcess->setCustomerRelationshipId( $this->getCustomerRelationshipId() );

		$objLeaseProcess->setNoticeDate( $strMoveOutDate );
		$objLeaseProcess->setMoveOutDate( $strMoveOutDate );
		$objLeaseProcess->setTerminationStartDate( NULL );
		$objLeaseProcess->setFmoStartedOn( $strMoveOutDate );
		$objLeaseProcess->setFmoApprovedOn( $strMoveOutDate );
		$objLeaseProcess->setFmoProcessedOn( $strMoveOutDate );
		$objLeaseProcess->setMoveOutReasonListItemId( $intMoveOutReasonId );

		if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
			$this->setCustomerTypeId( CCustomerType::RESPONSIBLE );
		}

		if( false == $objLeaseProcess->validate( 'mid_lease_move_out', $objDatabase ) || false == $objLeaseProcess->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
		}

		if( false == $boolIsUpdateLeaseCustomer ) {

			if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
				$this->setCustomerTypeId( CCustomerType::RESPONSIBLE );
			}

			$this->setIsUngrouped( 1 );
			$this->setLeaseStatusTypeId( CLeaseStatusType::PAST );
			$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );
		}

		return $boolIsValid;
	}

	public function performMidLeaseMoveOut( $strMoveOutDate, $intMoveOutReasonId, $intCompanyUserId, $objDatabase, $objLeaseProcess = NULL ) {

		$boolIsValid = true;

		if( true == valObj( $objLeaseProcess, 'CLeaseProcess' ) ) {
			$objLeaseProcess->setMoveOutDate( $strMoveOutDate );
			$objLeaseProcess->setTerminationStartDate( NULL );
			$objLeaseProcess->setMoveOutReasonListItemId( $intMoveOutReasonId );

			$objLeaseProcess->setFmoStartedOn( $strMoveOutDate );
			$objLeaseProcess->setFmoApprovedOn( $strMoveOutDate );
			$objLeaseProcess->setFmoProcessedOn( $strMoveOutDate );

			if( false == $objLeaseProcess->validate( 'mid_lease_move_out', $objDatabase ) || false == $objLeaseProcess->update( $intCompanyUserId, $objDatabase ) ) {
				$boolIsValid &= false;
				$this->addErrorMsgs( $objLeaseProcess->getErrorMsgs() );
			}
		}

		$this->setLeaseStatusTypeId( CLeaseStatusType::PAST );
		$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );

		return $boolIsValid;
	}

	public function performReverseMoveOut( $intCompanyUserId, $objDatabase ) {

		$this->setIsUngrouped( 1 );
		$this->setLeaseStatusTypeId( CLeaseStatusType::NOTICE );

		$boolIsValid &= $this->update( $intCompanyUserId, $objDatabase, false, false, true, true );

		return $boolIsValid;
	}

	public function checkApplicationRpkStatus( $intPropertyId, $objDatabase ) {

		if( true == is_null( $intPropertyId ) ) return false;

		$boolIsAppRpkAvailable = false;

		$strSql = 'SELECT
    					lc.id
				   FROM
    					lease_customers lc
    					JOIN leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
    					LEFT JOIN applications a ON ( l.active_lease_interval_id = a.lease_interval_id AND l.cid = a.cid )
				    WHERE
    					l.cid = ' . ( int ) $this->getCid() . '
    					AND l.property_id = ' . ( int ) $intPropertyId . '
    					AND lc.id = ' . ( int ) $this->getId() . '
    					AND ( a.id IS NULL
    					OR a.app_remote_primary_key IS NOT NULL
    					OR ( a.application_stage_id, a.application_status_id ) IN( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ], CApplicationStage::APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ] ] ) . ' ) )
    					AND lc.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;

		$arrmixApplicationData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixApplicationData ) ) {

			$boolIsAppRpkAvailable = true;
		}

		return $boolIsAppRpkAvailable;
	}

	public function getOneTimeScheduledCharges() {
		$arrmixRequiredParameters        = $this->getRequiredParameters();
		$strOneTimeLeaseScheduledCharges = '';

		if( true == valArr( $arrmixRequiredParameters ) && true == array_key_exists( 'is_default_roommate_notification', $arrmixRequiredParameters ) ) {
			if( true == array_key_exists( 'exclude_financial_details', $arrmixRequiredParameters ) && true == $arrmixRequiredParameters['exclude_financial_details'] ) {
				return NULL;
			}

			$strOneTimeLeaseScheduledCharges .= __( '<br><br><p class="margin10-bottom font12 text-black bold pad10 highlight-gray"> One Time Charges:</p>' );
		}

		$strOneTimeLeaseScheduledCharges .= $this->loadScheduledChargesByArTriggerIds( CArTrigger::$c_arrintNotificationLetterOneTimeTrigggers );
		return ( false == valStr( $strOneTimeLeaseScheduledCharges ) ) ? NULL : $strOneTimeLeaseScheduledCharges;
	}

	public function getRecurringScheduledCharges() {
		$strRecurringLeaseScheduledCharges = '';
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( true == valArr( $arrmixRequiredParameters ) && true == array_key_exists( 'is_default_roommate_notification', $arrmixRequiredParameters ) ) {
			if( true == array_key_exists( 'exclude_financial_details', $arrmixRequiredParameters ) && true == $arrmixRequiredParameters['exclude_financial_details'] ) {
				return $strRecurringLeaseScheduledCharges;
			}

			$strRecurringLeaseScheduledCharges .= __( '<br><br>Your Scheduled Charge details are shown below:&nbsp; <br><br><p class="margin10-bottom font12 text-black bold pad10 highlight-gray"> Recurring Charges:</p>' );
		}

		$strRecurringLeaseScheduledCharges .= $this->loadScheduledChargesByArTriggerIds( CArTrigger::$c_arrintRecurringArTriggers );
		return ( false == valStr( $strRecurringLeaseScheduledCharges ) ) ? NULL : $strRecurringLeaseScheduledCharges;
	}

	public function getTotalScheduledCharges() {
		$strLeaseScheduledChargesTotal = $this->loadScheduledChargesByArTriggerIds( array_merge( CArTrigger::$c_arrintRecurringArTriggers, CArTrigger::$c_arrintNotificationLetterOneTimeTrigggers ), true );
		return ( false == valStr( $strLeaseScheduledChargesTotal ) ) ? NULL : $strLeaseScheduledChargesTotal;
	}

	public function getMoneygramAccountNumber() {
		$objMoneyGramAccount = $this->loadMoneyGramAccount();
		return ( false == is_null( $objMoneyGramAccount ) ) ? $objMoneyGramAccount->getId() : NULL;
	}

	public function getMoneygramReceiveCode() {
		$objMoneyGramAccount = $this->loadMoneyGramAccount();
		return ( false == is_null( $objMoneyGramAccount ) ) ? CMoneyGramAccount::MONEYGRAM_AGENT_ID : NULL;
	}

	public function loadScheduledChargesByArTriggerIds( $arrintArTriggerIds, $boolReturnTotalScheduledChargesAmountOnly = false ) {

		$arrintInstallmentScheduledChargeIds = [];
		$strScheduledChargeMaxEndDate        = NULL;
		$arrintFullyPostedScheduledChargeIds = [];
		$arrobjInstallmentScheduledCharges   = [];

		$intSelectedLeaseIntervalId = ( true == valArrKeyExists( $this->getRequiredParameters(), 'lease_interval_id' ) ) ? $this->getRequiredParameters()['lease_interval_id'] : $this->getLeaseIntervalId();
		$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByArTriggerIdsByLeaseIdsByCid( $arrintArTriggerIds, [ $this->getLeaseId() ], $this->m_intCid, $this->m_objDatabase, false, $boolReturnTotalScheduledChargesAmountOnly, true, true, [ $intSelectedLeaseIntervalId ] );

		foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
			if( true == valId( $objScheduledCharge->getInstallmentId() ) ) {
				$arrintInstallmentScheduledChargeIds[] = $objScheduledCharge->getId();
				$arrobjInstallmentScheduledCharges[] = $objScheduledCharge;
				unset( $arrobjScheduledCharges[$objScheduledCharge->getId()] );

				if( strtotime( $strScheduledChargeMaxEndDate ) < strtotime( $objScheduledCharge->getChargeEndDate() ) ) {
					$strScheduledChargeMaxEndDate = $objScheduledCharge->getChargeEndDate();
				}
			}

			$boolIsPostedOneTimeScheduledCharge		= ( false == is_null( $objScheduledCharge->getPostedThroughDate() ) && true == in_array( $objScheduledCharge->getArTriggerId(), array_merge( CArTrigger::$c_arrintOneTimeLeaseChargesTypeArTriggers, CArTrigger::$c_arrintLeadSectionOtherLeadArTriggers ) ) );
			$boolIsPostedRecurringScheduledCharge	= ( false == is_null( $objScheduledCharge->getChargeEndDate() ) && strtotime( $objScheduledCharge->getPostedThroughDate() ) >= strtotime( $objScheduledCharge->getChargeEndDate() ) );

			if( $boolIsPostedOneTimeScheduledCharge || $boolIsPostedRecurringScheduledCharge ) {
				$arrintFullyPostedScheduledChargeIds[] = $objScheduledCharge->getId();
			}
		}

		$arrmixScheduledChargesWithActiveTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnreversedTransactionScheduledChargesByScheduledChargeIdsByCid( $arrintFullyPostedScheduledChargeIds, $this->m_intCid, $this->m_objDatabase );
		$arrintFullyPostedReversedScheduledChargeIds  = array_diff( $arrintFullyPostedScheduledChargeIds, array_keys( $arrmixScheduledChargesWithActiveTransactions ) );

		foreach( $arrintFullyPostedReversedScheduledChargeIds as $intFullyPostedReversedScheduledChargeId ) {
			unset( $arrobjScheduledCharges[$intFullyPostedReversedScheduledChargeId] );
		}

		if( true == valArr( $arrintInstallmentScheduledChargeIds ) ) {
			// to post the scheduled charges temporarily updated status to current
			$this->m_objDatabase->begin();

				if( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
					$this->setLeaseStatusTypeId( CLeaseStatusType::FUTURE );

					if( false == $this->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
						$this->addErrorMsg( $this->getErrorMsgs() );
						$this->m_objDatabase->rollback();
						return;
					}
				}

			$this->setLeaseStatusTypeId( CLeaseStatusType::CURRENT );
				if( false == $this->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
					$this->addErrorMsg( $this->getErrorMsgs() );
					$this->m_objDatabase->rollback();
					return;
				}

			// To fetch all installment charges and then done their addition.
			$arrobjLeaseCustomers = [ $this ];
			$objPostScheduledChargesLibrary  = CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::NORMAL, $this->m_objDatabase );
			$objPostScheduledChargesCriteria = $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->m_intCid, [ current( $arrobjLeaseCustomers )->getPropertyId() ], SYSTEM_USER_ID );

			$objPostScheduledChargesCriteria->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_AR_TRANSACTIONS )
				->setIsDryRun( true )
				->setScheduledChargeIds( $arrintInstallmentScheduledChargeIds )
				->setLeaseIds( [ $this->getLeaseId() ] )
				->setPostWindowEndDateOverride( $strScheduledChargeMaxEndDate );

			$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
			$arrobjArTransactions = ( array ) $objPostScheduledChargesLibrary->getArTransactions();
			$this->m_objDatabase->rollback();

			$arrobjPostedArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByScheduledChargeIdsByLeaseIdsByCid( $arrintInstallmentScheduledChargeIds, $this->getLeaseId(), $this->m_intCid, $this->m_objDatabase );
			if( true == valArr( $arrobjPostedArTransaction ) ) {
				$arrobjArTransactions += $arrobjPostedArTransaction;
			}

			asort( $arrobjArTransactions );
			$arrfltSumOfInstallmentAmt = [];

			$arrobjInstallmentScheduledCharges = rekeyObjects( 'Id', $arrobjInstallmentScheduledCharges );
			foreach( $arrobjArTransactions as $objArTransaction ) {
				if( false == in_array( $objArTransaction->getScheduledChargeId(), $arrintInstallmentScheduledChargeIds ) ) {
					continue;
				}

				$arrfltSumOfInstallmentAmt[$objArTransaction->getScheduledChargeId()] = bcadd( $arrfltSumOfInstallmentAmt[$objArTransaction->getScheduledChargeId()], $objArTransaction->getTransactionAmount(), 2 );

				if( true == valObj( $arrobjInstallmentScheduledCharges[$objArTransaction->getScheduledChargeId()], 'CScheduledCharge' ) ) {
					$arrobjInstallmentScheduledCharges[$objArTransaction->getScheduledChargeId()]->setChargeAmount( $arrfltSumOfInstallmentAmt[$objArTransaction->getScheduledChargeId()] );
				}
			}

		}

		if( true == $boolReturnTotalScheduledChargesAmountOnly ) {
			$arrobjLeaseScheduledCharges = ( array ) rekeyObjects( 'LeaseId', $arrobjScheduledCharges, true );
			foreach( $arrobjLeaseScheduledCharges as $arrobjScheduledCharges ) {
				$intTotalCharges = 0;
				foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
					$intTotalCharges = bcadd( $intTotalCharges, $objScheduledCharge->getChargeAmount(), 2 );
				}

				$strLeaseScheduledCharges = $intTotalCharges;
			}

			$arrobjLeaseWiseInstallmentScheduledCharges = ( array ) rekeyObjects( 'LeaseId', $arrobjInstallmentScheduledCharges, true );
			foreach( $arrobjLeaseWiseInstallmentScheduledCharges as $arrobjInstallmentScheduledCharges ) {
				$intTotalCharges = 0;
				foreach( $arrobjInstallmentScheduledCharges as $objScheduledCharge ) {
					$intTotalCharges = bcadd( $intTotalCharges, $objScheduledCharge->getChargeAmount(), 2 );
				}

				$strLeaseScheduledCharges += $intTotalCharges;
			}
		} else {

			$objProperty     = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->m_intCid, $this->m_objDatabase );
			$arrobjArCodes   = ( array ) $objProperty->fetchArCodes( $this->m_objDatabase, new CArCodesFilter( false, true ) );

			$arrobjLeaseWiseInstallmentScheduledCharges = ( array ) rekeyObjects( 'LeaseId', $arrobjInstallmentScheduledCharges, true );
			foreach( $arrobjLeaseWiseInstallmentScheduledCharges as $intLeaseId => $arrobjInstallmentScheduledCharges ) {

				$strScheduledCharges = '<p class="margin10-bottom font12 text-black bold pad10 highlight-gray">' . __( 'Installment Charges:' ) . '</p>
										<table class="clean block margin30-bottom font14" width="50%;" style="margin:5px">';

				$arrobjInstallmentScheduledCharges = rekeyObjects( 'InstallmentId', $arrobjInstallmentScheduledCharges, true );

				foreach( $arrobjInstallmentScheduledCharges as $arrobjInstallmentCharges ) {
					$objCurrentInstallmentCharge = current( $arrobjInstallmentCharges );
					$strScheduledCharges         .= '<tr>
											<td style="width: 420px;" class="bold valign-middle pad0-left">' . $objCurrentInstallmentCharge->getInstallmentName() .
					                                '( ' . __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $objCurrentInstallmentCharge->getChargeStartDate() ] ) . '-' . __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $objCurrentInstallmentCharge->getChargeEndDate() ] ) . ' )
											</td>
											<td style="width: 50%;" class="bold valign-middle pad0-right font14 text-black">' . __( 'Total:' ) . '</td>
										</tr>';

					foreach( $arrobjInstallmentCharges as $objInstallmentCharge ) {
						$strChargeCodeName   = ( true == valArr( $arrobjArCodes ) && true == array_key_exists( $objInstallmentCharge->getArCodeId(), $arrobjArCodes ) ) ? $arrobjArCodes[$objInstallmentCharge->getArCodeId()]->getName() : '';
						$strScheduledCharges .= '<tr>
												<td style="width: 420px;" class="valign-middle pad0-left">' . $strChargeCodeName . ':</td>
												<td style="width: 50%;" class="valign-middle pad0-right">' . __( '{%m, 0, n:()}', [ $objInstallmentCharge->getChargeAmount() ] ) . '</td>
											</tr>';
					}
				}
				$strScheduledCharges      .= '</table>';
				$strLeaseScheduledCharges = $strScheduledCharges;
			}

			if( true == valArr( $arrobjScheduledCharges ) ) {
				$arrobjLeaseScheduledCharges = ( array ) rekeyObjects( 'LeaseId', $arrobjScheduledCharges, true );
				foreach( $arrobjLeaseScheduledCharges as $intLeaseId => $arrobjLeaseScheduledCharge ) {

					$intTotalCharges     = 0;
					$strScheduledCharges = $strLeaseScheduledCharges;

					if( true == valArr( $arrobjLeaseWiseInstallmentScheduledCharges ) && true == array_key_exists( $intLeaseId, $arrobjLeaseWiseInstallmentScheduledCharges ) ) {
						$strScheduledCharges .= '<p class="margin10-bottom font12 text-black bold pad10 highlight-gray">' . __( 'Other Recurring Charges:' ) . '</p>';
					}
					$strScheduledCharges .= '<table class="clean block margin30-bottom font14" width="50%;" style="margin:5px"> ';

					foreach( $arrobjLeaseScheduledCharge as $objScheduledCharge ) {
						$strChargeCodeName = ( true == valArr( $arrobjArCodes ) && true == array_key_exists( $objScheduledCharge->getArCodeId(), $arrobjArCodes ) ) ? $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getName() : '';

						$strScheduledCharges .= '<tr>
										<td style="width: 120px;" class="bold valign-middle pad0-left">' . $strChargeCodeName . ':</td>
										<td style="width: 50%;" class="align-right valign-middle pad0-right">' . __( '{%m, 0, n:()}', [ $objScheduledCharge->getChargeAmount() ] ) . '</td>
									</tr>';

						$intTotalCharges = bcadd( $intTotalCharges, $objScheduledCharge->getChargeAmount(), 2 );

					}

					$strScheduledCharges .= '<tr class="noborder">
									<td style="width: 120px;" class="bold valign-middle font14 text-black pad0-left">' . __( 'Total:' ) . '</td>
									<td style="width: 50%;" class="align-right valign-middle bold font14 text-black pad0-right">' . __( '{%m, 0, n:();nots}', [ $intTotalCharges ] ) . '</td>
								</tr>
							</table>';

					$strLeaseScheduledCharges = $strScheduledCharges;
				}
			}
		}

		return $strLeaseScheduledCharges;
	}

	public function loadMoneyGramAccount() {

		if( false == valId( $this->getLeaseId() ) ) {
			return NULL;
		}
		$objPaymentDatabase	= CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::PAYMENT, CDatabaseUserType::PS_DEVELOPER );

		if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
			return NULL;
		}
		$objPaymentDatabase->open();
		$objMoneyGramAccount = \Psi\Eos\Payment\CMoneyGramAccounts::createService()->fetchMoneyGramAccountByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $this->getLeaseId(), $this->m_intCid, $objPaymentDatabase );
		$objPaymentDatabase->close();
		if( false == valObj( $objMoneyGramAccount, 'CMoneyGramAccount' ) ) {
			return NULL;
		}
		return $objMoneyGramAccount;
	}

	public function getRoommateDetails( $boolIsRetrieveRoommatesWithContactDetails = false ) {

		$intSelectedLeaseIntervalId = ( true == valArrKeyExists( $this->getRequiredParameters(), 'lease_interval_id' ) ) ? $this->getRequiredParameters()['lease_interval_id'] : $this->getLeaseIntervalId();
		if( false == valId( $intSelectedLeaseIntervalId ) ) {
			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $this->m_objDatabase );
			if( true == valObj( $objLease, CLease::class ) ) {
				$this->setLeaseIntervalId( $objLease->getActiveLeaseIntervalId() );
			}
		}

		$arrmixLeaseTerms = ( array ) \Psi\Eos\Entrata\CLeaseStartWindows::createService()->fetchLeaseStartWindowsByLeaseIdByPropertyIdByCid( $this->getLeaseId(), $this->getPropertyId(), $this->getCid(), $this->m_objDatabase, $intSelectedLeaseIntervalId );

		if( false == valArr( $arrmixLeaseTerms ) ) {
			return NULL;
		}

		$arrintOverLappingLeaseStartWindowIds 	= array_keys( rekeyArray( 'id', $arrmixLeaseTerms ) );
		$arrmixLeaseTerms 						= rekeyArray( 'property_unit_id', $arrmixLeaseTerms );
		$arrintPropertyUnitIds 					= array_filter( array_keys( rekeyArray( 'property_unit_id', $arrmixLeaseTerms ) ) );

		if( false == valArr( $arrintOverLappingLeaseStartWindowIds ) || false == valArr( $arrmixLeaseTerms ) || false == valArr( $arrintPropertyUnitIds ) ) {
			return NULL;
		}

		$objBulkUnitAssignmentFilter = new CBulkUnitAssignmentFilter();
		$objBulkUnitAssignmentFilter->setPropertyId( $this->getPropertyId() );
		$objBulkUnitAssignmentFilter->setWaitListApplicationStageStatusIds( $objBulkUnitAssignmentFilter->loadApplicationStageStatuses( $this->getCid(), $this->m_objDatabase ) );
		$objBulkUnitAssignmentFilter->setLeaseStartWindowIds( $arrintOverLappingLeaseStartWindowIds );

		$arrmixCustomersRoommateInfo = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchRoommatesContactInformationByPropertyUnitIdsByBulkUnitAssignmentFilterByCid( $arrintPropertyUnitIds, $objBulkUnitAssignmentFilter, $this->getCid(), $this->m_objDatabase, false, $boolIsRetrieveRoommatesWithContactDetails );
		$arrmixCustomersRoommateInfo = rekeyArray( 'lease_id', $arrmixCustomersRoommateInfo, false, true );

		return $arrmixCustomersRoommateInfo;
	}

	public function getRoommatesWithContactDetails() {
		$strRoommatesWithContactDetails = '';
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$intSelectedLeaseIntervalId = ( true == valArrKeyExists( $arrmixRequiredParameters, 'lease_interval_id' ) ) ? $arrmixRequiredParameters['lease_interval_id'] : $this->getLeaseIntervalId();
		$arrmixCustomersRoommateInfo = $this->getRoommateDetails( false );
		$arrmixSelectedCustomerContactDetails = rekeyArray( 'lease_interval_id', $arrmixCustomersRoommateInfo[$this->getLeaseId()] )[$intSelectedLeaseIntervalId];

		if( true == valArr( $arrmixRequiredParameters ) && true == array_key_exists( 'is_default_roommate_notification', $arrmixRequiredParameters ) ) {
			if( true == array_key_exists( 'include_rm_contact', $arrmixRequiredParameters ) && false == $arrmixRequiredParameters['include_rm_contact'] ) {
				return $strRoommatesWithContactDetails;
			}

			$strRoommatesWithContactDetails .= __( '<br><br>Your Roommates and their contact information are included below:&nbsp;<br>' );
		}

		if( true == valArr( $arrmixCustomersRoommateInfo ) ) {
			foreach( $arrmixCustomersRoommateInfo as $arrmixCustomerRoommateInfos ) {
				foreach( $arrmixCustomerRoommateInfos as $arrmixCustomerRoommateInfo ) {
					if( $this->getLeaseId() != $arrmixCustomerRoommateInfo['lease_id'] && $arrmixSelectedCustomerContactDetails['property_unit_id'] == $arrmixCustomerRoommateInfo['property_unit_id'] ) {
						if( ( strtotime( $arrmixSelectedCustomerContactDetails['lease_start_date'] ) <= strtotime( $arrmixCustomerRoommateInfo['lease_end_date'] ) ) && ( strtotime( $arrmixSelectedCustomerContactDetails['lease_end_date'] ) >= strtotime( $arrmixCustomerRoommateInfo['lease_start_date'] ) ) ) {
							$strRoommatesWithContactDetails .= $arrmixCustomerRoommateInfo['customers_name_full'] . ':' . __( '{%h, 0, NATIONAL_RAW}', [ $arrmixCustomerRoommateInfo['phone_numbers'] ] ) . ',' . $arrmixCustomerRoommateInfo['email_address'] . ',' . __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $arrmixCustomerRoommateInfo['lease_start_date'] ] ) . ' - ' . __( '{%t, 0, DATE_NUMERIC_STANDARD}', $arrmixCustomerRoommateInfo['lease_end_date'] ) . ' <br>';
						}
					}
				}
			}

		}

		return $strRoommatesWithContactDetails;
	}

	public function getRoommatesWithContactDetailsWithoutLeaseDates() {
		$strRoommatesWithContactDetails = '';
		$intSelectedLeaseIntervalId = ( true == valArrKeyExists( $this->getRequiredParameters(), 'lease_interval_id' ) ) ? $this->getRequiredParameters()['lease_interval_id'] : $this->getLeaseIntervalId();
		$arrmixCustomersRoommateInfo = $this->getRoommateDetails( false );
		$arrmixSelectedCustomerContactDetails = rekeyArray( 'lease_interval_id', $arrmixCustomersRoommateInfo[$this->getLeaseId()] )[$intSelectedLeaseIntervalId];

		if( true == valArr( $arrmixCustomersRoommateInfo ) ) {
			foreach( $arrmixCustomersRoommateInfo as $arrmixCustomerRoommateInfos ) {
				foreach( $arrmixCustomerRoommateInfos as $arrmixCustomerRoommateInfo ) {
					if( $this->getLeaseId() != $arrmixCustomerRoommateInfo['lease_id'] && $arrmixSelectedCustomerContactDetails['property_unit_id'] == $arrmixCustomerRoommateInfo['property_unit_id'] ) {
						if( ( strtotime( $arrmixSelectedCustomerContactDetails['lease_start_date'] ) <= strtotime( $arrmixCustomerRoommateInfo['lease_end_date'] ) ) && ( strtotime( $arrmixSelectedCustomerContactDetails['lease_end_date'] ) >= strtotime( $arrmixCustomerRoommateInfo['lease_start_date'] ) ) ) {
							$strRoommatesWithContactDetails .= $arrmixCustomerRoommateInfo['customers_name_full'] . ':' . __( '{%h, 0, NATIONAL_RAW}', [ $arrmixCustomerRoommateInfo['phone_numbers'] ] ) . ',' . $arrmixCustomerRoommateInfo['email_address'] . ' <br>';
						}
					}
				}
			}

		}

		return $strRoommatesWithContactDetails;
	}

	public function getStudentRoommates() {
		$strStudentRoommates = '';
		$intSelectedLeaseIntervalId = ( true == valArrKeyExists( $this->getRequiredParameters(), 'lease_interval_id' ) ) ? $this->getRequiredParameters()['lease_interval_id'] : $this->getLeaseIntervalId();
		$arrmixCustomersRoommateInfo = $this->getRoommateDetails( true );
		$arrmixSelectedCustomerContactDetails = rekeyArray( 'lease_interval_id', $arrmixCustomersRoommateInfo[$this->getLeaseId()] )[$intSelectedLeaseIntervalId];

		if( true == valArr( $arrmixCustomersRoommateInfo ) ) {
			foreach( $arrmixCustomersRoommateInfo as $arrmixCustomerRoommateInfos ) {
				foreach( $arrmixCustomerRoommateInfos as $arrmixCustomerRoommateInfo ) {
					if( $this->getLeaseId() != $arrmixCustomerRoommateInfo['lease_id'] && $arrmixSelectedCustomerContactDetails['property_unit_id'] == $arrmixCustomerRoommateInfo['property_unit_id'] ) {
						if( ( strtotime( $arrmixSelectedCustomerContactDetails['lease_start_date'] ) <= strtotime( $arrmixCustomerRoommateInfo['lease_end_date'] ) ) && ( strtotime( $arrmixSelectedCustomerContactDetails['lease_end_date'] ) >= strtotime( $arrmixCustomerRoommateInfo['lease_start_date'] ) ) ) {
							$strStudentRoommates .= $arrmixCustomerRoommateInfo['customers_name_full'] . '<br>';
						}
					}
				}
			}
		}

		return $strStudentRoommates;
	}

	public function getResidentScheduledAppointmentDate() {

		$objPropertyTimeZone = CTimeZones::fetchTimeZoneNameByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		$objResidentScheduledAppointment = \Psi\Eos\Entrata\CCalendarEvents::createService()->fetchCalendarEventByLeaseIdByLeaseCustomerIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objResidentScheduledAppointment, 'CCalendarEvent' ) ) {
			return NULL;
		}
		return __( '{%t, 0, DATE_NUMERIC_STANDARD} from {%t, 1, TIME_SHORT} to {%t, 2, TIME_SHORT}', [ CInternationalDateTime::create( $objResidentScheduledAppointment ->getStartDateTime(), $objPropertyTimeZone->getTimeZoneName() ), CInternationalDateTime::create( $objResidentScheduledAppointment->getStartDateTime(), $objPropertyTimeZone->getTimeZoneName() ), CInternationalDateTime::create( $objResidentScheduledAppointment->getEndDateTime(), $objPropertyTimeZone->getTimeZoneName() ) ] );
	}

	public function getResidentDocumentSigningLoginContent() {

		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = CApplications::fetchApplicationByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objApplication, 'CApplication' ) ) {
			return NULL;
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$strLoginId = __( '<b>Login Id: </b>' ) . $objCustomer->getUsername();
		$strPassword = '';
		if( true == is_null( $objCustomer->getPasswordEncrypted() ) || 0 == strlen( $objCustomer->getPasswordEncrypted() ) ) {

			$strPassword = md5( uniqid( mt_rand(), true ) );
			$strPassword = \Psi\CStringService::singleton()->substr( $strPassword, 0, 6 );
			$objCustomer->setPassword( CHash::createService()->hashCustomer( $strPassword ) );
			$strPassword = __( '<b>Password: </b>' ) . $strPassword;
			if( false == $objCustomer->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$this->m_objDatabase->rollback();
			}
		}

		$arrobjPropertyPreferences		= $objProperty->getOrFetchPropertyPreferences( $this->m_objDatabase );

		$strSecureBaseName = '';
		$objWebsite = CWebsites::createService()->fetchWebsiteByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $this->getLeaseId(), $this->getCid(), $this->m_objDatabase, false, true, false, true );

		if( true == valObj( $objWebsite, CWebsite::class ) ) {
			$strResidentPortalPrefix = ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix = ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal' . CConfig::get( 'TLD' );
			$strSecureBaseName = $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
		}

		if( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
			$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		}

		$strApplicationUrl = $strSecureBaseName . 'resident_portal/?module=authentication';

		if( CLeaseIntervalType::RENEWAL == $objApplication->getLeaseIntervalTypeId() && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES']->getValue() ) ) {

			$objApplicant = CApplicants::fetchApplicantByCustomerIdByApplicationIdByCid( $this->getCustomerId(), $objApplication->getId(), $this->getCid(), $this->m_objDatabase );

			if( true == valObj( $objApplicant, 'CApplicant' ) ) {
				$objOneTimeLink	= new COneTimeLink();

				$intUniqueId 		= \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );
				$strApplicationUrl 	= $strSecureBaseName . 'resident_portal/?module=authentication&action=autologin&property[id]=' . $objProperty->getId() . '&application[id]=' . $objApplication->getId() . '&applicant[key]=' . urlencode( $intUniqueId ) . '&applicant[id]=' . $objApplicant->getId() . '&login_from_one_time_link=1';

				$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );

				$this->m_objDatabase->begin();

				if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert one time link.' ), NULL ) );
					$this->m_objDatabase->rollback();
					return NULL;
				}

				$this->m_objDatabase->commit();
			}
		}

		$strLink = __( '<a href="{%s, 0}"  target="_blank"><u>Click here</u></a> to review and sign your document.', [ $strApplicationUrl ] );

		$strLoginIdPassword = '<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td style="padding-top:20px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLoginId . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strPassword . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLink . '</td>
									</tr>
								</table>';

		return $strLoginIdPassword;

	}

	public function getResidentLeaseSigningInvitationLoginContent() {
		$intApplicationId = $this->getRequiredParameters()['application_id'] ?? NULL;

		if( false == valId( $intApplicationId ) ) {
			return '';
		}

		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $intApplicationId, $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) ) {
			return NULL;
		}

		$objPrimaryApplicant = CApplicants::fetchApplicantByApplicationIdByCustomerTypeIdByCid( $objApplication->getId(), CCustomerType::PRIMARY, $this->getCid(), $this->m_objDatabase );
		$objApplicant = CApplicants::fetchApplicantByLeaseIdByCustomerIdByCid( $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
			return NULL;
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$objCustomerPortalSetting = CCustomerPortalSettings::fetchCustomCustomerPortalSettingByCustomerIdByCid( $objCustomer->getId(), $this->getCid(), $this->m_objDatabase );

		$strLoginId = __( '<b>Login Id: </b>' ) . $objCustomer->getUsername();
		$strPassword = '';
		if( true == is_null( $objCustomer->getPasswordEncrypted() ) || 0 == strlen( $objCustomer->getPasswordEncrypted() ) ) {

			$strPassword = md5( uniqid( mt_rand(), true ) );
			$strPassword = \Psi\CStringService::singleton()->substr( $strPassword, 0, 6 );
			$objCustomer->setPassword( CHash::createService()->hashCustomer( $strPassword ) );
			$strPassword = __( '<b>Password: </b>' ) . $strPassword;
			if( false == $objCustomer->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$strPassword = '';
			}
		}

		$arrobjPropertyPreferences		= $objProperty->getOrFetchPropertyPreferences( $this->m_objDatabase );

		$strSecureBaseName = '';
		$objWebsite = CWebsites::createService()->fetchWebsiteByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $this->getLeaseId(), $this->getCid(), $this->m_objDatabase, false, true, false, true );

		if( true == valObj( $objWebsite, CWebsite::class ) ) {
			$strResidentPortalPrefix = ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix = ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal' . CConfig::get( 'TLD' );
			$strSecureBaseName = $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
		}

		if( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
			$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
		}

		$boolIsStageOrTrunkEnvironment = ( 'trunk' === CONFIG_ENVIRONMENT || 'stage' === CONFIG_ENVIRONMENT ) ? true : false;
		$boolIsRp40Enabled = $objProperty->getIsRp40Enabled( $this->m_objDatabase );

		$strApplicationUrl = '';
		$strApplicationEnrollUrl = '';

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['USE_DIRECT_EMAIL_LINK_LOGIN_TO_SIGN_LEASES']->getValue() ) && ( false == is_null( $objCustomerPortalSetting ->getUsername() ) && false == is_null( $objCustomerPortalSetting ->getPasswordEncrypted() ) ) ) {

			if( true == valObj( $objApplicant, 'CApplicant' ) ) {

				$objOneTimeLink	= new COneTimeLink();
				$intUniqueId 		= \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );

				if( true == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ] ) ) {
					if( true == $boolIsStageOrTrunkEnvironment ) {
						$strSecureBaseName = str_replace( '.prospectportaldev.', '.residentportaldev.', $strSecureBaseName );
					} else {
						$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
					}
					if( true == $boolIsRp40Enabled ) {
						$strApplicationUrl = $strSecureBaseName . 'auth';
						$strApplicationEnrollUrl = $strSecureBaseName . 'auth';
					} else {
						$strApplicationUrl 	= $strSecureBaseName . 'resident_portal/?module=authentication&action=autologin&property[id]=' . $objProperty->getId() . '&application[id]=' . $objApplication->getId() . '&applicant[key]=' . urlencode( $intUniqueId ) . '&applicant[id]=' . $objApplicant->getId() . '&login_from_one_time_link=1';
						$strApplicationEnrollUrl = $strSecureBaseName . 'resident_portal/?module=authentication';
					}
					$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );
				} elseif( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
					if( true == $boolIsStageOrTrunkEnvironment ) {
						$strSecureBaseName = str_replace( '.residentportaldev.', '.prospectportaldev.', $strSecureBaseName );
					} else {
						$strSecureBaseName = str_replace( '.residentportal.', '.prospectportal.', $strSecureBaseName );
					}

					$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/autologin/property[id]/' . $objProperty->getId() . '/application[id]/' . $objApplication->getId() . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $objApplicant->getId() . '/login_from_one_time_link/1/';
					$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );
				} else {
					$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
					if( true == $boolIsStageOrTrunkEnvironment ) {
						$strSecureBaseName = str_replace( '.prospectportaldev.', '.residentportaldev.', $strSecureBaseName );
					} else {
						$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
					}
					if( true == $boolIsRp40Enabled ) {
						$strApplicationUrl = $strSecureBaseName . 'auth';
						$strApplicationEnrollUrl = $strSecureBaseName . 'auth';
					} else {
						$strApplicationUrl = $strSecureBaseName . 'resident_portal/?module=authentication&action=autologin&application[id]=' . $objApplication->getId() . '&property[id]=' . $objProperty->getId() . '&applicant[key]=' . urlencode( $intUniqueId ) . '&applicant[id]=' . $objApplicant->getId() . '&login_from_one_time_link=1';
						$strApplicationEnrollUrl = $strSecureBaseName . 'resident_portal/?module=authentication';
					}
					$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $objApplicant->getId(), 'key_encrypted' => $intUniqueId ] );
				}

				if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
					$strApplicationUrl = $strSecureBaseName . 'resident_portal/?module=authentication&action=view_login';
				}
			}
		} else {
			if( true == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ] ) ) {
				$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
				if( true == $boolIsStageOrTrunkEnvironment ) {
					$strSecureBaseName = str_replace( '.prospectportaldev.', '.residentportaldev.', $strSecureBaseName );
				} else {
					$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
				}
				if( true == $boolIsRp40Enabled ) {
					$strApplicationUrl = $strSecureBaseName . 'auth';
					$strApplicationEnrollUrl = $strSecureBaseName . 'auth';
				} else {
					$strApplicationUrl = $strSecureBaseName . 'resident_portal/?module=authentication&action=view_login';
					$strApplicationEnrollUrl = $strSecureBaseName . 'resident_portal/?module=authentication';
				}
			} elseif( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
				if( true == $boolIsStageOrTrunkEnvironment ) {
					$strSecureBaseName = str_replace( '.residentportaldev.', '.prospectportaldev.', $strSecureBaseName );
				} else {
					$strSecureBaseName = str_replace( '.residentportal.', '.prospectportal.', $strSecureBaseName );
				}
				$strApplicationUrl = $strSecureBaseName . 'Apartments/module/applicant_authentication/action/view_login/returning_applicant/1/property[id]/' . $objProperty->getId();
				$strApplicationEnrollUrl = $strSecureBaseName . 'Apartments/module/applicant_authentication/action/view_login/returning_applicant/1/property[id]/' . $objProperty->getId();
			}
		}

		$strGuarantorFullName = '';

		if( true == valObj( $objPrimaryApplicant, 'CApplicant' ) ) {
			$strGuarantorFullName = $objPrimaryApplicant->getNameFirst() . ' ' . $objPrimaryApplicant->getNameLast() . ' ' . $objPrimaryApplicant->getNameLastMatronymic();
		}

		$strEnrollUrl = ( true == valStr( $strApplicationEnrollUrl ) ) ? __( 'Please note, if you have already enrolled in Resident Portal this link will only log you in automatically one time. If you have not yet enrolled <a href="{%s,0}"  target="_blank"><u>Click here</u></a>.', [ $strApplicationEnrollUrl ] ) : '';
		$strLink = __( '<a href="{%s, 0}"  target="_blank"><u>Click here</u></a> to review and sign your lease.', [ $strApplicationUrl ] ) . $strEnrollUrl;
		$strGuarantorOrApplicantFullName = ( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) ? $strGuarantorFullName : $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . ' ' . $objApplicant->getNameLastMatronymic();
		$strPropertyPhoneNumber = $objProperty->getCustomPropertyOfficePhoneNumberText();

		$strResidentLeaseSigningInvitationContent = __( '<div align="center">
							<table border="0" cellpadding="0" cellspacing="0" style="width:570px;">
								<tbody>
									<tr>
										<td align="left" style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;">Lease documents for ' . $strGuarantorOrApplicantFullName . ' at ' . $objProperty->getPropertyName() . ' are ready for your signature! Click on the link below to log in and follow the instructions to review and sign. ' . $strPropertyPhoneNumber . '</td>
									</tr>
									<tr>
										<td style="padding-top:20px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLoginId . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strPassword . '</td>
									</tr>
									<tr>
										<td style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">' . $strLink . '</td>
									</tr>
								</tbody>
							</table>
						</div>' );

		return $strResidentLeaseSigningInvitationContent;

	}

	public function getResidentPortalAutoLoginLink() {
		$strResidentPortalAutoLoginLink = '';
		$strSecureBaseName = '';

		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIdByCustomerIdByLeaseStatusTypeIdByCid( $this->getLeaseId(), $this->getCustomerId(), $this->getLeaseStatusTypeId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objApplication, CApplication::class ) ) {
			return NULL;
		}
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$intApplicantId = ( int ) \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantIdByLeaseIdByCustomerIdByCid( $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		$objWebsite = CWebsites::createService()->fetchWebsiteByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $this->getLeaseId(), $this->getCid(), $this->m_objDatabase, false, true, false, true );
		$boolIsRp40Enabled = $objProperty->getIsRp40Enabled( $this->m_objDatabase );

		if( true == valObj( $objWebsite, CWebsite::class ) ) {
			$strResidentPortalPrefix = ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix = ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal' . CConfig::get( 'TLD' );
			$strSecureBaseName = $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
		}

		if( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
			$strSecureBaseName = $objApplication->generateWebsiteUrl( $this->m_objDatabase );
		}

		if( false == valStr( $strSecureBaseName ) ) {
			return NULL;
		}

		$boolIsStageOrTrunkEnvironment = ( 'trunk' === CONFIG_ENVIRONMENT || 'stage' === CONFIG_ENVIRONMENT ) ? true : false;

		$arrmixCustomerPortalSettings = \Psi\Eos\Entrata\CCustomerPortalSettings::createService()->fetchCustomerPortalSettingUserNamePassWordByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		if( true == valArr( $arrmixCustomerPortalSettings ) && false == is_null( $arrmixCustomerPortalSettings[0]['username'] ) && false == is_null( $arrmixCustomerPortalSettings[0]['password_encrypted'] ) ) {
			$objOneTimeLink = new COneTimeLink();
			$intUniqueId    = \Psi\CStringService::singleton()->substr( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encryptUrl( uniqid( mt_rand(), true ), CONFIG_SODIUM_KEY_ID ), 0, 240 );
			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $intApplicantId, 'key_encrypted' => $intUniqueId ] );

			if( true == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ] ) ) {
				if( true == $boolIsStageOrTrunkEnvironment ) {
					$strSecureBaseName = str_replace( '.prospectportaldev.', '.residentportaldev.', $strSecureBaseName );
				} else {
					$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
				}
				if( true == $boolIsRp40Enabled ) {
					$strResidentPortalAutoLoginLink = $strSecureBaseName . 'auth';
				} else {
					$strSecureBaseName = str_replace( '.prospectportal.', '.residentportal.', $strSecureBaseName );
					$strResidentPortalAutoLoginLink = $strSecureBaseName . '?module=authentication&action=autologin&property[id]=' . $this->getPropertyId() . '&application[id]=' . $objApplication->getId() . '&applicant[key]=' . urlencode( $intUniqueId ) . '&applicant[id]=' . $intApplicantId . '&login_from_one_time_link=1';
				}
			} elseif( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
				if( true == $boolIsStageOrTrunkEnvironment ) {
					$strSecureBaseName = str_replace( '.residentportaldev.', '.prospectportaldev.', $strSecureBaseName );
				} else {
					$strSecureBaseName = str_replace( '.residentportal.', '.prospectportal.', $strSecureBaseName );
				}

				$strSecureBaseName = str_replace( '.residentportal.', '.prospectportal.', $strSecureBaseName );
				$strResidentPortalAutoLoginLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/autologin/property[id]/' . $this->getPropertyId() . '/application[id]/' . $objApplication->getId() . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $intApplicantId . '/login_from_one_time_link/1/';
			}

			if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				$strResidentPortalAutoLoginLink = $strSecureBaseName . 'resident_portal/?module=authentication&action=view_login';
			}

		}

		return $strResidentPortalAutoLoginLink;
	}

	public function getParcelAlertArrivedContent() {
		$strContent = $this->getParcelAlertContent( CPackageStatusType::UNDELIVERED );
		return $strContent;
	}

	public function getParcelAlertDeliveredContent() {
		$strContent = $this->getParcelAlertContent( CPackageStatusType::DELIVERED );
		return $strContent;
	}

	public function getParceAlertPickedUpContent() {
		$strContent = $this->getParcelAlertContent( CPackageStatusType::PICKEDUP );
		return $strContent;
	}

	public function getParcelAlertContent( $intPackageStatus ) {
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArrKeyExists( $arrmixRequiredParameters, 'package_ids' ) ) {
			return NULL;
		}
		$arrintPackageIds = $arrmixRequiredParameters['package_ids'];
		$arrobjPackages = ( array ) \Psi\Eos\Entrata\CPackages::createService()->fetchCustomerPackagesByPackageIdsByCid( $arrintPackageIds, $this->getCid(), $this->m_objDatabase );
		$intCount = 0;
		$strContent = '';
		$strShippingVendorImage = '';
		$strCssQuestion = 'font-weight: 400;padding: 10px;text-align: left;border-top: 1px solid #666;border-bottom: 1px solid #666;color: #666;max-width: 70px;';
		$strCssAnswer   = 'padding: 5px;border-top: 1px solid #666;border-bottom: 1px solid #666;color: #666;text-align: left;text-transform: uppercase;max-width: 200px;';

		$arrobjShippingVendors = ( array ) \Psi\Eos\Entrata\CShippingVendors::createService()->fetchShippingVendorsWithIconByCid( $this->getCid(), $this->m_objDatabase );

		$arrobjPropertyPreferences = ( array ) \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->getPropertyId() ], [ 'DISABLE_PARCEL_ALERT_TRACKING_NUMBER', 'DO_NOT_INCLUDE_PACKAGE_NOTES' ], $this->getCid(), $this->m_objDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'key', $arrobjPropertyPreferences );

		$strBaseUrl = 'http://' . $_SERVER['HTTP_HOST'];

		if( true === CONFIG_IS_SECURE_URL ) {
			$strBaseUrl = 'https://' . $_SERVER['HTTP_HOST'];
		}

		foreach( $arrobjPackages as $objPackage ) {

			if( false == is_null( $objPackage->getPackageName() ) ) {
				if( 'f' == $objPackage->getCustomShippingVendor() ) {
					$strVendorName = str_replace( ')', '', str_replace( '(', '', str_replace( ' ', '-', strtolower( $objPackage->getPackageName() ) ) ) );

					switch( $strVendorName ) {
						case 'lasership':
							$strShippingVendorIcon = 'laser';
							break;

						case 'correos-de-mexico':
							$strShippingVendorIcon = 'correo-de-mexico';
							break;

						case 'canpar-express':
							$strShippingVendorIcon = 'canpar';
							break;

						case 'apc-postal-logistics':
							$strShippingVendorIcon = 'apc';
							break;

						case 'purolator':
							$strShippingVendorIcon = 'puralator';
							break;

						default:
							$strShippingVendorIcon = $strVendorName;
					}

					$strShippingVendorImage = '<img src = "' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'package_' . $strShippingVendorIcon . '.gif"  style = "width:36px;height:26px;" alt = "' . $objPackage->getPackageName() . '">';
				} else {
					$objShippingVendor      = $arrobjShippingVendors[$objPackage->getPackageTypeId()];
					$strShippingVendorImage = '<i class="custom-shipping" style = "text-align:center; color:#ffffff; font-weight:bold; padding:5px 0;" >
						<span class="two-lines margin0" > ' . $objPackage->getPackageName() . '</span ></i >';

					if( true == valObj( $objShippingVendor, 'CShippingVendor' ) && true == $objShippingVendor->getIsVendorIcon() && false == is_null( $objShippingVendor->getFullsizeUri() ) ) {
						$strShippingVendorImage = '<img src = "' . \CConfig::get( 'media_library_path' ) . $objShippingVendor->getFullsizeUri() . '"  class="custom-shipping" style = "width:36px;height:26px;" alt = "' . $objPackage->getPackageName() . '">';
					}
				}
			}

			$strPackageStatus = __( 'Arrival' );
			$strPackageDate   = __( '{%t,0,DATETIME_NUMERIC_SHORT_NOTZ}', [ $objPackage->getCreatedOn() ] );

			$objPropertyTimeZone = \Psi\Eos\Entrata\CTimeZones::createService()->fetchTimeZoneNameByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
			$strDeliveredOn = CInternationalDateTime::create( $objPackage->getDeliveredOn(), $objPropertyTimeZone->getTimeZoneName() );

			if( CPackageStatusType::PICKEDUP == $intPackageStatus ) {
				$strPackageStatus = __( 'PICKED UP' );
				$strPackageDate   = __( '{%t,0,DATETIME_NUMERIC_SHORT_NOTZ}', [ $strDeliveredOn ] );
			} elseif( CPackageStatusType::DELIVERED == $intPackageStatus ) {
				$strPackageStatus = __( 'DELIVERED' );
				$strPackageDate   = __( '{%t,0,DATETIME_NUMERIC_SHORT_NOTZ}', [ $strDeliveredOn ] );
			}

			$strContent .= '<br/><br/>
						<table class="margin40-top" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="2" style="padding: 15px;border-top: 1px solid #666;background: #f3f3f3;color: #666;text-align: center;text-transform: uppercase;"> ' . __( 'PACKAGE {%d, 0} DETAILS', [ ++$intCount ] ) . '</td>
							</tr> 
							<tr>
								<td style= "' . $strCssQuestion . '">' . __( 'Shipping Vendor' ) . '</td>
								<td style= "' . $strCssAnswer . '">' . $strShippingVendorImage . '</td>
							</tr>';

			if( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'DISABLE_PARCEL_ALERT_TRACKING_NUMBER', $arrobjPropertyPreferences ) || false == $arrobjPropertyPreferences['DISABLE_PARCEL_ALERT_TRACKING_NUMBER']->getValue() ) {
				$strContent .= '<tr>
								<td style="' . $strCssQuestion . '">' . __( 'Tracking' ) . '</td>
								<td style= "' . $strCssAnswer . '">' . $objPackage->getBarCode() . '</td>
							</tr>';
			}

			if( CPackageStatusType::UNDELIVERED == $intPackageStatus && false == is_null( $objPackage->getNotes() ) && 1 == $objPackage->getIsNotePublished() && ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'DO_NOT_INCLUDE_PACKAGE_NOTES', $arrobjPropertyPreferences ) || false == $arrobjPropertyPreferences['DO_NOT_INCLUDE_PACKAGE_NOTES']->getValue() ) ) {
				$strContent .= '<tr>
									<td style= "' . $strCssQuestion . '">' . __( 'Notes' ) . '</td >
									<td style= "' . $strCssAnswer . '">' . $objPackage->getNotes() . '</td >
								</tr >';
			}

			$strContent .= '<tr>
								<td style="' . $strCssQuestion . '">' . $strPackageStatus . '</td>
								<td style= "' . $strCssAnswer . '">' . $strPackageDate . '</td>
							</tr>
						</table>';
		}

		$strContent .= '<br/>';
		return $strContent;
	}

	public function getResidentAddress() {
		$objLease = $this->fetchLease( $this->m_objDatabase );
		$objCustomer = $this->fetchCustomer( $this->m_objDatabase );
		$boolIsSetCustomerAddress = false;
		$objCustomerAddress = NULL;

		$strCustomerAddress = '';

		if( COccupancyType::COMMERCIAL == $objLease->getOccupancyTypeId() && CLeaseStatusType::CANCELLED != $objLease->getLeaseStatusTypeId() ) {
			$arrobjCustomerAddresses		= rekeyObjects( 'AddressTypeId', ( array ) \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdByCid( $objLease->getPrimaryCustomerId(), $this->getCid(), $this->m_objDatabase ) );
			$arrintLeaseAddressTypesPriority = CAddressType::$c_arrintActiveLeaseAdresssTypesPriority;
			if( CLeaseStatusType::PAST == $objLease->getLeaseStatusTypeId() ) {
				$arrintLeaseAddressTypesPriority	= CAddressType::$c_arrintPastLeaseAdresssTypesPriority;
			}

			foreach( $arrintLeaseAddressTypesPriority as $intAddressTypeId ) {
				if( false === array_key_exists( $intAddressTypeId, $arrobjCustomerAddresses ) ) {
					continue;
				}
				$objCustomerAddress		= $arrobjCustomerAddresses[$intAddressTypeId];
				break;
			}
		} else {
			$arrobjCustomerAddresses	= rekeyObjects( 'AddressTypeId', ( array ) $objCustomer->fetchAddressesByAddressTypeIds( [ CAddressType::BILLING, CAddressType::PERMANENT, CAddressType::FORWARDING ], $this->m_objDatabase ) );
			$objCustomerAddress			= ( true == array_key_exists( CAddressType::PERMANENT, $arrobjCustomerAddresses ) ) ? $arrobjCustomerAddresses[CAddressType::PERMANENT] : $objCustomerAddress;

			if( true === array_key_exists( CAddressType::BILLING, $arrobjCustomerAddresses ) ) {
				$objCustomerAddress 		= $arrobjCustomerAddresses[CAddressType::BILLING];
				$boolIsSetCustomerAddress	= true;
			} elseif( $objLease->getLeaseStatusTypeId() == CLeaseStatusType::PAST && true == array_key_exists( CAddressType::FORWARDING, $arrobjCustomerAddresses ) ) {
				$objCustomerAddress			= $arrobjCustomerAddresses[CAddressType::FORWARDING];
				$boolIsSetCustomerAddress	= true;
			}
		}

		if( ( COccupancyType::COMMERCIAL == $objLease->getOccupancyTypeId() && false === valObj( $objCustomerAddress, CCustomerAddress::class ) ) || false === $boolIsSetCustomerAddress ) {
			$objCustomerAddress     = new CCustomerAddress();

			if( true == in_array( $objLease->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) ) {
				$objCustomerUnitAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressByPropertyUnitIdByCid( $objLease->getPropertyUnitId(), $this->getCid(), $this->m_objDatabase );
				if( true == valObj( $objCustomerUnitAddress, 'CUnitAddress' ) ) {
					$objCustomerAddress->setStreetLine1( ( true == valStr( $objCustomerUnitAddress->getStreetLine1() ) ) ? $objCustomerUnitAddress->getStreetLine1() : NULL );
					$objCustomerAddress->setStreetLine2( ( true == valStr( $objCustomerUnitAddress->getStreetLine2() ) ) ? $objCustomerUnitAddress->getStreetLine2() : NULL );
					$objCustomerAddress->setCity( ( true == valStr( $objCustomerUnitAddress->getCity() ) ) ? $objCustomerUnitAddress->getCity() : NULL );
					$objCustomerAddress->setStateCode( ( true == valStr( $objCustomerUnitAddress->getStateCode() ) ) ? $objCustomerUnitAddress->getStateCode() : NULL );
					$objCustomerAddress->setPostalCode( ( true == valStr( $objCustomerUnitAddress->getPostalCode() ) ) ? $objCustomerUnitAddress->getPostalCode() : NULL );
				}
			} elseif( true == valObj( $objCustomer, CCustomer::class ) ) {
				$objCustomerAddress->setStreetLine1( ( true == valStr( $objCustomer->getPrimaryStreetLine1() ) ) ? $objCustomer->getPrimaryStreetLine1() : NULL );
				$objCustomerAddress->setStreetLine2( ( true == valStr( $objCustomer->getPrimaryStreetLine2() ) ) ? $objCustomer->getPrimaryStreetLine2() : NULL );
				$objCustomerAddress->setCity( ( true == valStr( $objCustomer->getPrimaryCity() ) ) ? $objCustomer->getPrimaryCity() : NULL );
				$objCustomerAddress->setStateCode( ( true == valStr( $objCustomer->getPrimaryStateCode() ) ) ? $objCustomer->getPrimaryStateCode() : NULL );
				$objCustomerAddress->setPostalCode( ( true == valStr( $objCustomer->getPrimaryPostalCode() ) ) ? $objCustomer->getPrimaryPostalCode() : NULL );
			}

		}

		if( true == valObj( $objCustomerAddress, CCustomerAddress::class ) ) {
			$strCustomerAddress .= ( true == valStr( $objCustomerAddress->getStreetLine1() ) ) ? $objCustomerAddress->getStreetLine1() . '<br>' : '';
			$strCustomerAddress .= ( true == valStr( $objCustomerAddress->getStreetLine2() ) ) ? $objCustomerAddress->getStreetLine2() . '<br>' : '';
			$strCustomerAddress .= ( true == valStr( $objCustomerAddress->getCity() ) ) ? $objCustomerAddress->getCity() . ' ' : '';
			$strCustomerAddress .= ( true == valStr( $objCustomerAddress->getStateCode() ) ) ? $objCustomerAddress->getStateCode() . ' ' : '';
			$strCustomerAddress .= ( true == valStr( $objCustomerAddress->getPostalCode() ) ) ? $objCustomerAddress->getPostalCode() . '<br>' : '';
		} else {
			$objPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressForRPByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), CAddressType::PRIMARY, $this->getCid(), $this->m_objDatabase );

			if( false == valObj( $objPropertyAddress, CPropertyAddresses::class ) ) {
				return '';
			}

			$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getStreetLine1() ) ) ? $objPropertyAddress->getStreetLine1() . '<br>' : '';
			$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getStreetLine2() ) ) ? $objPropertyAddress->getStreetLine2() . '<br>' : '';
			$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getStreetLine3() ) ) ? $objPropertyAddress->getStreetLine3() . '<br>' : '';

			if( true == valStr( $objPropertyAddress->getCity() ) ) {
				$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getCity() ) ) ? $objPropertyAddress->getCity() . ' ' : '';
				$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getStateCode() ) ) ? $objPropertyAddress->getStateCode() . ' ' : '';
				$strCustomerAddress .= ( true == valStr( $objPropertyAddress->getPostalCode() ) ) ? $objPropertyAddress->getPostalCode() . '<br>' : '';
			}
		}

		return $strCustomerAddress;
	}

	public function getRenewalOfferGeneratedContent() {
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArr( $arrmixRequiredParameters ) || false == valObj( $objProperty, CProperty::class ) || false == valObj( $objCustomer, CCustomer::class ) ) {
			return '';
		}

		$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'RENEWAL_OFFER_NOTIFICATION_TO_APPLICANT', $this->m_objDatabase );

		$strEmailContent = __( '<table cellpadding="0" cellspacing="0" border="0" style="width:570px;"><tr><td style="font:normal normal bold 15px/18px arial; color:#000000; padding-top:30px; padding-bottom:10px;" align="left"><strong> {%s, 0} {%s, 1} {%s, 2} </strong></td></tr>', [ $objCustomer->getNameFirst(), $objCustomer->getNameLast(), $objCustomer->getNameLastMatronymic() ] );

		if( true == valObj( $objPropertyPreference, CPropertyPreference::class ) && true == valStr( $objPropertyPreference->getValue() ) ) {
			$strEmailContent .= __( '<tr> <td style="font: normal 11px/18px Arial, Helvetica, sans-serif; color: #000000;" align="left" class="break-word">{%s, 0}</td></tr>', [ $objPropertyPreference->getValue() ] );
		} else {
			$strEmailContent .= __( '<tr><td  style="font:normal normal bold 15px/18px arial; color:#000000; padding-bottom:10px;" align="left"><strong>You have been sent one or more offer(s) for lease renewal.</strong><br/></td></tr><tr><td style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left">Please follow the link below to login to your Resident Portal</br> account and review your options.</td></tr>' );
		}

		$strEmailContent .= __( '<tr><td style="padding-top:10px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left" ><b>Login &#160;ID&#160;:</b>&#160; {%s, 0}</td></tr>', [ $objCustomer->getUsername() ] );

		if( true == array_key_exists( 'notes', $arrmixRequiredParameters ) && true == valStr( $arrmixRequiredParameters['notes'] ) ) {
			$strEmailContent .= __( '<tr><td style="padding-top:10px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left" > {%s, 0} </td></tr>', [ $arrmixRequiredParameters['notes'] ] );
		}

		$strResidentPortalUrl = $objProperty->generateResidentPortalWebsiteUrl( $this->m_objDatabase );

		if( true == valStr( $strResidentPortalUrl ) ) {
			$strEmailContent .= __( '<tr><td  style="padding-top:5px; font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left"> <a href="{%s, 0}"  target="_blank"><u>Click here</u></a> to review and select your renewal offer. </td></tr>', [ $strResidentPortalUrl ] );
		}

		return $strEmailContent . __( '<tr><td align="left" style="padding-top:20px;"><div Style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" style="margin-top:20px;" align="left"><strong>Please retain for your records.<br/></strong></div></td></tr></table>' );

	}

	public function getForgotPasswordAssistanceEmailContent() {
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $this->m_objDatabase );
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArrKeyExists( $arrmixRequiredParameters, 'is_required_new_version' ) || false == valObj( $objCustomer, CCustomer::class ) ) {
			return '';
		}

		if( true == valArrKeyExists( $arrmixRequiredParameters, 'is_send_to_group' ) && true == $arrmixRequiredParameters['is_send_to_group'] ) {
			$strProductName = __( 'Reservation Hub' );
		} else if( true == valArrKeyExists( $arrmixRequiredParameters, 'is_tenant' ) && true == $arrmixRequiredParameters['is_tenant'] ) {
			$strProductName = __( 'Tenant Portal' );
		} else {
			$strProductName = __( 'Resident Portal' );
		}

		if( true == $arrmixRequiredParameters['is_required_new_version'] && true == valArrKeyExists( $arrmixRequiredParameters, 'temporary_password' ) ) {
			return '<tr>
							<td style="background-color:#FFF;">
								<table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px; margin:0 auto;">
									<tr>
										<td style="height:30px;" colspan="3"></td>
									</tr>
									<tr>
										<td style="width:20px;"></td>
										<td style="font-size:14px;line-height:26px;">'.
			       __( 'Hey {%s, customer_first_name},<br>
											The following password will temporarily let you log in to your {%s, product_name} account, and allow you to create a new password. This temporary password will expire in 24 hours.', ['customer_first_name' => $objCustomer->getNameFirst(), 'product_name' => $strProductName]).
			       '</td>
										<td style="width:20px;"></td>
									</tr>
									<tr>
										<td style="height:30px;" colspan="3"></td>
									</tr>
								</table>
					
								<table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px; margin:0 auto;">
									<tr>
										<td style="width:20px;"></td>
										<td style="font-size:14px;">
											<table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px; margin:0 auto;border:1px solid #A3D1FF;">
												<tr>
													<td valign="center" colspan="3" style="background: #3399FF;line-height:36px;">
														<h2 style="font-size:16px;font-weight:bold;color:#ffffff;padding-left:20px;line-height:48px;margin:0px">'.
			       __('Temporary Credentials').
			       '</h2>
													</td>
												</tr>
												<tr>
													<td colspan="3" style="height:1px; background:#3399FF;"></td>
												</tr>
												<tr>
													<td colspan="3" style="height:20px;"></td>
												</tr>
												<tr>
													<td valign="top" colspan="1" style="width: 180px;">
														<em style="font-size:14px;font-weight:100;color:#70B8FF;padding-left:20px;">'.__('Email').'</em>
													</td>
													<td colspan="1">
														<span style="font-size:14px;">'.__('{%s, customer_email_address}', ['customer_email_address' => $objCustomer->getEmailAddress()]).'</span>
													</td>
													<td colspan="1"></td>
												</tr>
												<tr>
													<td colspan="3" style="height:15px;"></td>
												</tr>
												<tr>
													<td valign="top" colspan="1" style="width: 180px;">
														<em style="font-size:14px;font-weight:100;color:#70B8FF;padding-left:20px;">'.__('Temporary Password').'</em>
													</td>
													<td colspan="1">
														<span style="font-size:14px;">'.__('{%s, temporary_password}', ['temporary_password' => $arrmixRequiredParameters['temporary_password']]).'</span>
													</td>
													<td colspan="1"></td>
												</tr>
												<tr>
													</td>
												</tr>
											</table>
										</td>
										<td style="width:20px;"></td>
									</tr>
									<tr>
										<td style="height:50px;" colspan="3"></td>
									</tr>
								</table>
							</td>
						</tr>';
		}

		if( true == valArrKeyExists( $arrmixRequiredParameters, 'customer_password_link' ) ) {
			$strCustomerUsernameLinkOrText = __( '<a href="{%s, 0}" style="font-size:16px;font-weight:bold;color:#ffffff;padding:10px 25px;background: #3399FF; text-decoration: none;">Password Reset</a>', $arrmixRequiredParameters['customer_password_link'] );
		} else {
			$strCustomerUsernameLinkOrText = __( 'New password for your username <b>{%s, 0}</b> is <b>{%s, 1}</b>', [ $objCustomer->getUsername(), $objCustomer->getPassword() ] );
		}
		return '<tr>
						<td style="background-color:#FFF;">
							<table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px; margin:0 auto;">
								<tr>
									<td style="height:30px;" colspan="3"></td>
								</tr>
								<tr>
									<td style="width:20px;"></td>
									<td style="font-size:14px;line-height:26px;">'.
		       __( 'Hey {%s, customer_first_name},<br>
										The link below will take you to your {%s, product_name} password reset page. This link will expire in 24 hours.', ['customer_first_name' => $objCustomer->getNameFirst(), 'product_name' => $strProductName]).
		       '</td>
									<td style="width:20px;"></td>
								</tr>
								<tr>
									<td style="height:30px;" colspan="3"></td>
								</tr>
				
								<tr>
									<td style="width:20px;"></td>
									<td>'.
		       __('{%s, customer_user_name_link_or_text}', ['customer_user_name_link_or_text' => $strCustomerUsernameLinkOrText]).
		       '</td>
									<td style="width:20px;"></td>
								</tr>
				
								<tr>
									<td style="height:50px;" colspan="3"></td>
								</tr>
							</table>
						</td>
					</tr>';
	}

	public function getTemporaryPasswordText() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valArrKeyExists( $arrmixRequiredParameters, 'is_required_new_version' ) && true == $arrmixRequiredParameters['is_required_new_version'] ) {
			return __( 'Temporary Password Access' );
		}
		return __( 'Password Reset Link' );
	}

	public function getResidentPortalProductName() {
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( true == valArrKeyExists( $arrmixRequiredParameters, 'is_send_to_group' ) && true == $arrmixRequiredParameters['is_send_to_group'] ) {
			$strProductName = __( 'Reservation Hub' );
		} else if( true == valArrKeyExists( $arrmixRequiredParameters, 'is_tenant' ) && true == $arrmixRequiredParameters['is_tenant'] ) {
			$strProductName = __( 'Tenant Portal' );
		} else {
			$strProductName = __( 'Resident Portal' );
		}

		return $strProductName;
	}

	public function getRenewalRequestNotes() {
		return $this->getRequiredParameters()['notes'] ?? '';
	}

}
?>