<?php

class CUnitSpaceOccupancyHistory extends CBaseUnitSpaceOccupancyHistory {

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPropertyId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPropertyUnitId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getUnitSpaceId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLeaseId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valLeaseActionId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLeaseActionId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_action_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valLeaseActionDate() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLeaseActionDate())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_action_date', '' ));
        // }

        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getNotes())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>