<?php

class CScheduledTaskGroup extends CBaseScheduledTaskGroup {

	const LEAD_STATUS_CHANGES 				= 1;
	const LEAD_QUOTES 						= 2;
	const LEAD_APPOINTMENTS 				= 3;
	const LEAD_INCOMING_CONTACTS 			= 4;
	const LEAD_OTHER 						= 5;
	const LEAD_NO_PROGRESSION_ACTION 		= 6;
	const APPLICANT_STATUS_CHANGES 			= 7;
	const APPLICANT_APPOINTMENTS 			= 8;
	const APPLICANT_INCOMING_CONTACT 		= 9;
	const APPLICANT_NO_PROGRESSION_ACTION 	= 10;
	const STATUS_CHANGES 					= 11;
	const APPOINTMENTS 						= 12;
	const INCOMING_CONTACT 					= 13;
	const RESIDENT_INSURANCE 				= 14;
	const RESIDENT_PORTAL 					= 15;
	const OTHER 							= 16;
	const RENEWALS 							= 17;
	const TRANSFERS 						= 18;
	const MID_LEASE_MODIFICATIONS 			= 19;
	const REPLACEMENT_LEASE 				= 20;
	const RENEWAL_NO_PROGRESSION_ACTION 	= 21;
	const CREATED_FROM						= 22;
	const STATUS_CHANGED_TO					= 23;
	const WORK_ORDER_SURVEY					= 25;
	const PARCEL_ALERT					    = 35;
	const CHECKLISTS                        = 36;

	public static $c_arrintLeadScheduledTaskGroups = [
		self::LEAD_STATUS_CHANGES,
		self::LEAD_QUOTES,
		self::LEAD_APPOINTMENTS,
	 	self::LEAD_INCOMING_CONTACTS,
		self::LEAD_OTHER
	];

	public static $c_arrintApplicantScheduledTaskGroups = [
		self::APPLICANT_STATUS_CHANGES,
		self::APPLICANT_APPOINTMENTS,
		self::APPLICANT_INCOMING_CONTACT
	];

	public static $c_arrintLeadAndApplicantScheduledTaskGroups = [
		self::LEAD_STATUS_CHANGES,
		self::LEAD_QUOTES,
		self::LEAD_APPOINTMENTS,
		self::LEAD_INCOMING_CONTACTS,
		self::LEAD_OTHER,
		self::APPLICANT_STATUS_CHANGES,
		self::APPLICANT_APPOINTMENTS,
		self::APPLICANT_INCOMING_CONTACT
	];

	public static $c_arrintLeadAndApplicantNoProgressionScheduledTaskGroupIds = [
		self::APPLICANT_NO_PROGRESSION_ACTION,
		self::LEAD_NO_PROGRESSION_ACTION,
		self::RENEWAL_NO_PROGRESSION_ACTION
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>