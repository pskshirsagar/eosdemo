<?php

class CApPayeeContact extends CBaseApPayeeContact {

	protected $m_boolIsLoggedIn;

	protected $m_boolIsPrimary;
	protected $m_boolIsInvited;

	protected $m_intApLegalEntityId;
	protected $m_intApPayeeLocationId;
	protected $m_intAuthenticationLogId;
	protected $m_intApPayeeStatusTypeId;
	protected $m_intApPayeeContactLocationId;
	protected $m_intApPayeeContactsCount;

	protected $m_strNotes;
	protected $m_strCompanyName;
	protected $m_strLocationName;
	protected $m_strConfirmPassword;
	protected $m_strApPayeeName;

	protected $m_arrmixApPayeeContact = [];
	protected $m_arrobjApPayeeContactLocations = [];

	/**
	 * Add Functions
	 */

	public function addApPayeeContactEvent( $arrmixEvent ) {
		$this->m_arrmixApPayeeContact = $arrmixEvent;
	}

	/**
	 * Get Functions
	 */

	public function getApPayeeContactEvent() {
		return $this->m_arrmixApPayeeContact;
	}

	public function getApPayeeContactLocations() {
		return $this->m_arrobjApPayeeContactLocations;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getAuthenticationLogId() {
		return $this->m_intAuthenticationLogId;
	}

	public function getConfirmPassword() {
		return $this->m_strConfirmPassword;
	}

	public function getTaxNumberMasked() {
		if( 4 == strlen( $this->getTaxNumberEncrypted() ) ) {
			return $this->getTaxNumberEncrypted();
		} else {
			return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getTaxNumberEncrypted() ) );
		}
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getApPayeeStatusTypeId() {
		return $this->m_intApPayeeStatusTypeId;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function getIsInvited() {
		return $this->m_boolIsInvited;
	}

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER ] ) );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function getApPayeeContactLocationId() {
		return $this->m_intApPayeeContactLocationId;
	}

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getApPayeeContactsCount() {
		return $this->m_intApPayeeContactsCount;
	}

	/**
	 * Set Functions
	 */

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = $boolIsLoggedIn;
	}

	public function setAuthenticationLogId( $intAuthenticationLogId ) {
		$this->m_intAuthenticationLogId = CStrings::strToIntDef( $intAuthenticationLogId, NULL, true );
	}

	public function setConfirmPassword( $strConfirmPassword ) {
		return $this->m_strConfirmPassword = CStrings::strTrimDef( $strConfirmPassword, 240, NULL, true );
	}

	public function setTaxNumber( $strTaxNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strTaxNumber, 'XXXX' ) ) return;
		$strTaxNumber = CStrings::strTrimDef( $strTaxNumber, 20, NULL, true );
		if( true == valStr( $strTaxNumber ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setApPayeeStatusTypeId( $intApPayeeStatusTypeId ) {
		$this->m_intApPayeeStatusTypeId = $intApPayeeStatusTypeId;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	public function setNotes( $strNotes ) {
		$this->m_strNotes = $strNotes;
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = $intApPayeeLocationId;
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->m_boolIsPrimary = CStrings::strToBool( $boolIsPrimary );
	}

	public function setIsInvited( $boolIsInvited ) {
		$this->m_boolIsInvited = CStrings::strToBool( $boolIsInvited );
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->m_intApLegalEntityId = $intApLegalEntityId;
	}

	public function setApPayeeContactLocationId( $intApPayeeContactLocationId ) {
		$this->m_intApPayeeContactLocationId = ( int ) $intApPayeeContactLocationId;
	}

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = $strApPayeeName;
	}

	public function setApPayeeContactsCount( $strApPayeeContactsCount ) {
		$this->m_intApPayeeContactsCount = ( int ) $strApPayeeContactsCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['ap_payee_status_type_id'] ) ) $this->setApPayeeStatusTypeId( $arrmixValues['ap_payee_status_type_id'] );
		if( true == isset( $arrmixValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrmixValues['ap_legal_entity_id'] );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
		if( true == isset( $arrmixValues['is_primary'] ) ) $this->setIsPrimary( $arrmixValues['is_primary'] );
		if( true == isset( $arrmixValues['notes'] ) ) $this->setNotes( $arrmixValues['notes'] );
		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		if( true == isset( $arrmixValues['ap_payee_contact_location_id'] ) ) {
			$this->setApPayeeContactLocationId( $arrmixValues['ap_payee_contact_location_id'] );
		}
		if( true == isset( $arrmixValues['ap_payee_name'] ) ) $this->setApPayeeName( $arrmixValues['ap_payee_name'] );

		if( true == isset( $arrmixValues['ap_payee_contacts_count'] ) ) {
			$this->setApPayeeContactsCount( $arrmixValues['ap_payee_contacts_count'] );
		}
	}

	public function addApPayeeContactLocations( $objApPayeeContactLocation ) {
		$this->m_arrobjApPayeeContactLocations[] = $objApPayeeContactLocation;
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction, $boolIsValidate = NULL, $objClientDatabase = NULL, $arrobjApPayeeeContacts = NULL ) {

		$boolIsValid = true;

		$objApPayeeContactValidator = new CApPayeeContactValidator();
		$objApPayeeContactValidator->setApPayeeContact( $this );
		$boolIsValid &= $objApPayeeContactValidator->validate( $strAction, $boolIsValidate, $objClientDatabase, $arrobjApPayeeeContacts );

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	/**
	 * Create Functions
	 */

	public function createAuthenticationLog() {

		$objAuthenticationLog = new CAuthenticationLog();
		$objAuthenticationLog->setCid( $this->getCid() );
		$objAuthenticationLog->setIpAddress( getRemoteIpAddress() );
		$objAuthenticationLog->setLoginDatetime( 'NOW()' );
		$objAuthenticationLog->setApPayeeId( $this->getApPayeeId() );

		return $objAuthenticationLog;
	}

	public function createApPayeeContactLocation() {

		$objApPayeeContactLocation = new CApPayeeContactLocation();
		$objApPayeeContactLocation->setCid( $this->getCid() );

		return $objApPayeeContactLocation;
	}

	/**
	 * Other Functions
	 *
	 */

	public function encryptPassword() {
		if( true == valStr( $this->getPasswordEncrypted() ) ) {
			$this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function encryptPasswordAnswer() {
		if( false == valStr( $this->getPasswordAnswerEncrypted() ) ) {
			$this->setPasswordAnswerEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->getPasswordAnswerEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function getPasswordDecrypted() {
		if( false == valStr( $this->m_strPasswordEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

}
?>