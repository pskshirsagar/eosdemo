<?php

class CCommercialInsurancePolicy extends CBaseCommercialInsurancePolicy {

	const BACK_DATE_DAYS = '-60 days';

	const INACTIVE = 'Inactive';
	const ACTIVE = 'Active';

	const COMMERCIAL_INSURANCE_POLICY_REFERENCE_TYPE = 'commercial_insurance_policy';

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCommercialInsurancePolicyStatusType() {
		$arrstrCommercialInsurancePolicies = [
			self::INACTIVE 				=> __( 'Inactive' ),
			self::ACTIVE 				=> __( 'Active' )
		];

		return $arrstrCommercialInsurancePolicies;
	}

	public static $c_arrstrValidInsurancePolicyStatusTypes = [ self::ACTIVE, self::INACTIVE ];
	public static $c_arrstrActiveInsurancePolicyStatusTypes = [ self::ACTIVE ];
	public static $c_arrstrInActiveInsurancePolicyStatusTypes = [ self::INACTIVE ];

	protected $m_intLeaseId;
	protected $m_intMinimumLiability;
	protected $m_strFileName;
	protected $m_strFilePath;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true === isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( $arrmixValues['file_name'] );
		}
		if( true === isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( $arrmixValues['file_path'] );
		}

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommercialSuiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatusType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyNumber() {
		$boolIsValid = true;
		if( false === valStr( $this->getInsurancePolicyNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( 'Policy Number is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valInsuranceProviderName() {
		$boolIsValid = true;
		if( false === valStr( $this->getInsuranceProviderName() ) || 0 == strlen( trim( $this->getInsuranceProviderName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Provider Name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valInsuranceLiabilityLimit() {
		$boolIsValid = true;

		// Todo: Temporary removing this validation until insurance policy setting are unhidden from property level
		// if( true == $boolIsValid ) {
		//	if( 0 >= $this->getInsuranceLiabilityLimit() ) {
		//		$boolIsValid = false;
		//		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount is required.' ) ) );
		//	}
		// }

		// if( 0 < $this->getMinimumLiability() && 0 < $this->getInsuranceLiabilityLimit() ) {
		//	if( $this->getMinimumLiability() > $this->getInsuranceLiabilityLimit() ) {
		//		$boolIsValid = false;
		//		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount can not be less than Minimum Liability.' ) ) );
		//	}
		// }

		return $boolIsValid;
	}

	public function valInsuranceDeductible() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyEffectiveDate() {
		$boolIsValid = true;

		if( false === valStr( $this->getInsurancePolicyEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date is required.' ) ) );
		} else {
			$strInsurancePolicyEffectiveDate = __( '{%t, 0, DATE_NUMERIC_MMDDYYYY}', [ $this->getInsurancePolicyEffectiveDate() ] );
			$mixValidatedEffectiveDate = CValidation::checkDateFormat( $strInsurancePolicyEffectiveDate, true );
			if( false === $mixValidatedEffectiveDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date must be in mm/dd/yyyy format.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valInsurancePolicyEndDate() {
		$boolIsValid = true;

		if( false === valStr( $this->getInsurancePolicyEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is required.' ) ) );
		} else {
			$strInsurancePolicyEndDate = __( '{%t, 0, DATE_NUMERIC_MMDDYYYY}', [ $this->getInsurancePolicyEndDate() ] );
			$mixValidatedEndDate = CValidation::checkDateFormat( $strInsurancePolicyEndDate, true );
			if( false === $mixValidatedEndDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in mm/dd/yyyy format.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerifiedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartAndEndDate() {
		$boolIsValid = true;
		$boolIsValid &= $this->valInsurancePolicyEffectiveDate();
		$boolIsValid &= $this->valInsurancePolicyEndDate();

		if( true == $boolIsValid ) {
			$strStartDate 	= strtotime( $this->getInsurancePolicyEffectiveDate() );
			$strEndDate 	= strtotime( $this->getInsurancePolicyEndDate() );

			if( $strEndDate < $strStartDate ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( ' Start date should not be greater than End date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAgentEmail() {

		$boolIsValid = true;

		if( true === valStr( $this->getAgentEmail() ) ) {

			if( false == CValidation::validateEmailAddresses( $this->getAgentEmail() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_email', __( 'Agent Email is not valid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAgentPhone() {

		$boolIsValid = true;

		if( true === valStr( $this->getAgentPhone() ) ) {
			$strPhoneNumberCheck = '/^[0-9 \-]+$/';

			if( preg_match( $strPhoneNumberCheck, $this->getAgentPhone() ) !== 1 ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_phone', __( 'Agent Phone is not valid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_custom_commercial_insurance_policy':
				$boolIsValid &= $this->valInsuranceProviderName();
				$boolIsValid &= $this->valAgentEmail();
				$boolIsValid &= $this->valInsurancePolicyNumber( $boolIsCustomPolicy = true );
				$boolIsValid &= $this->valInsuranceLiabilityLimit();
				$boolIsValid &= $this->valInsurancePolicyTypeId();
				$boolIsValid &= $this->valStartAndEndDate();
				$boolIsValid &= $this->valInsuranceDeductible();
				break;

			case 'validate_start_and_end_date':
				$boolIsValid &= $this->valStartAndEndDate();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function buildFileData( $arrmixInsuranceFileData, $objFile, $objDatabase ) {
		if( false === valId( $objFile->getId() ) || false === valId( $objFile->getId() ) ) {
			$objFile->setId( $objFile->fetchNextId( $objDatabase ) );
		}

		$objFile->setTitle( $arrmixInsuranceFileData['name'] );

		$arrstrFileInfo = pathinfo( $arrmixInsuranceFileData['name'] );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = 'policy_document_' . $objFile->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false === valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addSessionErrorMsg( 'Invalid file extension.' );
			return false;
		}
		$strMimeType = $objFileExtension->getMimeType();
		$objFile->setFileExtensionId( $objFileExtension->getId() );

		$strPath = $objFile->getId() . '/policy_documents/';

		$objFile->setFilePath( $strPath );

		if( true === isset( $arrmixInsuranceFileData['tmp_name'] ) )	$objFile->setTempFileName( $arrmixInsuranceFileData['tmp_name'] );
		if( true === isset( $arrmixInsuranceFileData['error'] ) )		$objFile->setFileError( $arrmixInsuranceFileData['error'] );
		if( true === isset( $strFileName ) ) 	$objFile->setFileName( $strFileName );
		if( true === isset( $strMimeType ) ) 	$objFile->setFileType( $strMimeType );
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 4096, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true === isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true === isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setMinimumLiability( $intMinimumLiability ) {
		$this->m_intMinimumLiability = $intMinimumLiability;
	}

	public function getMinimumLiability() {
		return $this->m_intMinimumLiability;
	}

	public function createCommercialInsurancePolicy( $arrmixCommercialInsurancePolicy ) {
		$this->setCid( $arrmixCommercialInsurancePolicy['cid'] );
		$this->setPropertyId( $arrmixCommercialInsurancePolicy['property_id'] );
		$this->setCommercialSuiteId( $arrmixCommercialInsurancePolicy['commercial_suite_id'] );
		return $this;
	}

    public function setCreatedCustomerId( $intCreatedCustomerId ) {
        $this->setDetailsField( 'created_customer_id', $intCreatedCustomerId );
    }

    public function setUpdatedCustomerId( $intUpdatedCustomerId ) {
        $this->setDetailsField( 'updated_customer_id', $intUpdatedCustomerId );
    }

    public function getCreatedCustomerId() {
        return $this->getDetailsField( 'created_customer_id' );
    }

    public function getUpdatedCustomerId() {
        return $this->getDetailsField( 'updated_customer_id' );
    }

	public function setAgentName( $strAgentName ) {
		$this->setDetailsField( 'agent_name', $strAgentName );
	}

	public function getAgentName() {
		return $this->getDetailsField( 'agent_name' );
	}

	public function setAgentPhone( $strAgentPhone ) {
		$this->setDetailsField( 'agent_phone', $strAgentPhone );
	}

	public function getAgentPhone() {
		return $this->getDetailsField( 'agent_phone' );
	}

	public function setAgentEmail( $strAgentEmail ) {
		$this->setDetailsField( 'agent_email', $strAgentEmail );
	}

	public function getAgentEmail() {
		return $this->getDetailsField( 'agent_email' );
	}

	public function setAgentNaic( $strNaicNumber ) {
		$this->setDetailsField( 'agent_naic', $strNaicNumber );
	}

	public function getAgentNaic() {
		return $this->getDetailsField( 'agent_naic' );
	}

	public function setDamageAmount( $fltDamageAmount ) {
		$this->setDetailsField( 'damage_amount', $fltDamageAmount );
	}

	public function getDamageAmount() {
		return $this->getDetailsField( 'damage_amount' );
	}

	public function setPropertyAmount( $fltPropertyAmount ) {
		$this->setDetailsField( 'property_amount', $fltPropertyAmount );
	}

	public function getPropertyAmount() {
		return $this->getDetailsField( 'property_amount' );
	}

	public function setUmbrellaAmount( $fltUmbrellaAmount ) {
		$this->setDetailsField( 'umbrella_amount', $fltUmbrellaAmount );
	}

	public function getUmbrellaAmount() {
		return $this->getDetailsField( 'umbrella_amount' );
	}

	public function setAutoAmount( $fltAutoAmount ) {
		$this->setDetailsField( 'auto_amount', $fltAutoAmount );
	}

	public function getAutoAmount() {
		return $this->getDetailsField( 'auto_amount' );
	}

	public function setWorkersCompAmount( $fltWorkersCompAmount ) {
		$this->setDetailsField( 'workers_comp_amount', $fltWorkersCompAmount );
	}

	public function getWorkersCompAmount() {
		return $this->getDetailsField( 'workers_comp_amount' );
	}

	public function setLiquorLiabilityAmount( $fltLiquorLiabilityAmount ) {
		$this->setDetailsField( 'liquor_liability', $fltLiquorLiabilityAmount );
	}

	public function getLiquorLiabilityAmount() {
		return $this->getDetailsField( 'liquor_liability' );
	}

	public function setOtherAmount( $fltOtherAmount ) {
		$this->setDetailsField( 'other', $fltOtherAmount );
	}

	public function getOtherAmount() {
		return $this->getDetailsField( 'other' );
	}

}
?>