<?php

class CCamPoolType extends CBaseCamPoolType {
	const CAM           = 1;
	const INSURANCE     = 2;
	const TAX           = 3;
	const OTHER         = 4;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCamPoolTypeByCamPoolTypeId( $intCamPoolTypeId ) {

		switch( $intCamPoolTypeId ) {
			case self::CAM:
				return __( 'CAM' );
				break;

			case self::INSURANCE:
				return __( 'Insurance' );
				break;

			case self::TAX:
				return __( 'Tax' );
				break;

			case self::OTHER:
				return __( 'Other' );
				break;

			default:
				// default case
				break;
		}
	}

}
?>