<?php

class CApplicationFloorplan extends CBaseApplicationFloorplan {

	protected $m_strFloorplanName;

    /**
     * Get Functions
     */

    public function getFloorplanName() {

    	return $this->m_strFloorplanName;
    }

    /**
     * Set Functions
     */

    public function setFloorplanName( $strFloorplanName ) {

    	$this->m_strFloorplanName = $strFloorplanName;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['floorplan_name'] ) )
    		$this->setFloorplanName( $arrValues['floorplan_name'] );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getApplicationId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', __( 'Application id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyFloorplanId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Peoperty floorplan id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valApplicationId();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valPropertyFloorplanId();
            	break;

            case VALIDATE_DELETE:
            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>