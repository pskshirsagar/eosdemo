<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDrawRequests
 * Do not add any new functions to this class.
 */

class CDrawRequests extends CBaseDrawRequests {

	public static function fetchDrawRequestsByJobIdByCidByFilter( $intJobId, $intCid, $objDatabase, $arrmixJobDrawRequestFilter ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strWhereCondition      = '';
		$arrstrStatusCondition  = [];

		if( true == valArr( $arrmixJobDrawRequestFilter['status'] ) && true == in_array( 'Saved', $arrmixJobDrawRequestFilter['status'] ) ) {
			$arrstrStatusCondition[] = '( dr.posted_on IS NULL AND dr.deposited_on IS NULL )';
		}
		if( true == valArr( $arrmixJobDrawRequestFilter['status'] ) && true == in_array( 'Finalized', $arrmixJobDrawRequestFilter['status'] ) ) {
			$arrstrStatusCondition[] = '( dr.posted_on IS NOT NULL AND dr.deposited_on IS NULL )';
		}
		if( true == valArr( $arrmixJobDrawRequestFilter['status'] ) && true == in_array( 'Deposited', $arrmixJobDrawRequestFilter['status'] ) ) {
			$arrstrStatusCondition[] = '( dr.posted_on IS NOT NULL AND dr.deposited_on IS NOT NULL )';
		}

		if( true == valStr( $arrmixJobDrawRequestFilter['from_draw_date'] ) ) {
			$strWhereCondition .= ' AND dr.draw_date :: date >= \'' . date( 'Y-m-d', strtotime( $arrmixJobDrawRequestFilter['from_draw_date'] ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $arrmixJobDrawRequestFilter['to_draw_date'] ) ) {
			$strWhereCondition .= ' AND dr.draw_date :: date <= \'' . date( 'Y-m-d', strtotime( $arrmixJobDrawRequestFilter['to_draw_date'] ) ) . ' 23:59:59\'';
		}

		if( true == valArr( $arrstrStatusCondition ) ) {
			$strWhereCondition .= ' AND ( ' . implode( ' OR ', $arrstrStatusCondition ) . ' )';
		}

		$strSql = 'SELECT 
						DISTINCT dr.id,
						dr.bank_account_id,
						ba.account_name AS bank_account_name,
						dr.name,
						dr.draw_date,
						dr.posted_on,
						dr.deposited_on,
						CASE
							WHEN dr.posted_on IS NULL AND dr.deposited_on IS NULL THEN \'Saved\'
							WHEN dr.posted_on IS NOT NULL AND dr.deposited_on IS NULL THEN \'Finalized\'
							WHEN dr.posted_on IS NOT NULL AND dr.deposited_on IS NOT NULL THEN \'Deposited\'
						END AS status,
						CASE
							WHEN dr.id = subquery.id THEN 1 ELSE 0
						END AS is_recent_finalized,
						sum ( drd.draw_amount ) OVER ( PARTITION BY dr.cid, dr.id ) AS draw_amount,
						sum ( CASE
								WHEN ( ( ad.transaction_amount - ad.retention_amount ) = subquery2.ap_detail_drawn_amount ) THEN ad.retention_amount
							ELSE 0
						END ) OVER ( PARTITION BY dr.cid, dr.id ) AS retaintion_amount,
						sum ( CASE
								WHEN gd.amount = drd.draw_amount THEN drd.draw_amount
								WHEN ad.retention_amount = drd.draw_amount THEN 0
							ELSE ad.transaction_amount
						END ) OVER ( PARTITION BY dr.cid, dr.id ) AS total_amount
					FROM 
						draw_requests dr
						JOIN draw_request_details drd ON ( dr.cid = drd.cid AND dr.id = drd.draw_request_id AND drd.rejected_on IS NULL )
						LEFT JOIN gl_details gd ON ( gd.cid = drd.cid AND drd.gl_detail_id = gd.id )
						LEFT JOIN ap_details ad ON ( drd.cid = ad.cid AND drd.ap_detail_id = ad.id )
						LEFT JOIN bank_accounts ba ON ( dr.cid = ba.cid AND dr.bank_account_id = ba.id )
						LEFT JOIN 
						(
							SELECT id,
									cid
							FROM 
								draw_requests
							WHERE cid = ' . ( int ) $intCid . ' AND job_id = ' . ( int ) $intJobId . ' AND
									posted_on IS NOT NULL AND deposited_on IS NULL
							ORDER BY 
								draw_date DESC, posted_on DESC
							LIMIT 1
						) AS subquery ON ( dr.cid = subquery.cid )
						LEFT JOIN 
						(
							SELECT
								DISTINCT drd1.ap_detail_id,
								dr1.id AS draw_id,
								dr1.cid,
								sum ( drd1.draw_amount ) OVER ( PARTITION BY dr1.cid, dr1.id, drd1.ap_detail_id ) AS ap_detail_drawn_amount
							FROM
								draw_requests dr1
								JOIN draw_request_details drd1 ON ( dr1.cid = drd1.cid AND dr1.id = drd1.draw_request_id AND drd1.rejected_on IS NULL )
								JOIN ap_details ad1 ON ( drd1.cid = ad1.cid AND drd1.ap_detail_id = ad1.id )
							WHERE
								drd1.cid = ' . ( int ) $intCid . '
								AND dr1.job_id = ' . ( int ) $intJobId . '
						) AS subquery2 ON ( subquery2.cid = dr.cid AND subquery2.draw_id = dr.id AND subquery2.ap_detail_id = ad.id )
					WHERE 
						dr.cid = ' . ( int ) $intCid . '
						AND dr.job_id = ' . ( int ) $intJobId . $strWhereCondition . '
					ORDER BY
						status DESC,
						dr.draw_date DESC, posted_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestByIdByJobIdByCid( $intDrawRequestId, $intJobId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || ( true == is_null( $intDrawRequestId ) && true == is_null( $intJobId ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						dr.*,
						ap.company_name,
						ap.ap_payee_type_id,
						ac.name as ar_code_name,
						ba.account_name AS bank_account_name,
						jfs.funding_amount,
						COALESCE( subquery2.drawn_amount, 0 ) AS total_drawn_amount,
						CASE
							WHEN dr.id = subquery.id THEN 1 ELSE 0
						END AS is_recent_finalized
					FROM
						draw_requests dr
						JOIN ap_payees ap ON ( dr.cid = ap.cid AND dr.ap_payee_id = ap.id )
						LEFT JOIN bank_accounts ba ON ( dr.cid = ba.cid AND dr.bank_account_id = ba.id AND ba.is_disabled = 0 )
						LEFT JOIN ar_codes ac ON ( ac.cid = dr.cid AND ac.id = dr.ar_code_id )
						LEFT JOIN job_funding_sources jfs ON ( ap.cid = jfs.cid AND ap.id = jfs.ap_payee_id AND dr.job_id = jfs.job_id )
						LEFT JOIN (
							SELECT 
								id,
								cid
							FROM 
								draw_requests
							WHERE 
								cid = ' . ( int ) $intCid . ' 
								AND job_id = ' . ( int ) $intJobId . ' 
								AND posted_on IS NOT NULL 
								AND deposited_on IS NULL
							ORDER BY 
								draw_date DESC, 
								posted_on DESC
							LIMIT 1
						) as subquery ON ( dr.cid = subquery.cid )
						LEFT JOIN LATERAL (
							SELECT
								dr1.cid,
								dr1.ap_payee_id,
								SUM ( drd1.draw_amount ) as drawn_amount
							FROM 
								draw_requests dr1
								JOIN draw_request_details drd1 ON ( dr1.cid = drd1.cid AND dr1.id = drd1.draw_request_id )
							WHERE
								dr1.cid = ' . ( int ) $intCid . '
								AND dr1.job_id = ' . ( int ) $intJobId . '
								AND dr1.ap_payee_id = dr.ap_payee_id
								AND drd1.rejected_on IS NULL
								AND dr1.id <> ' . ( int ) $intDrawRequestId . '
							GROUP BY
								dr1.cid,
								dr1.ap_payee_id 
						) AS subquery2 ON ( dr.cid = subquery2.cid AND dr.ap_payee_id = subquery2.ap_payee_id )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawRequestId . '
						AND dr.job_id = ' . ( int ) $intJobId;

		return parent::fetchDrawRequest( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestInvoicesJEsByIdByJobIdByCid( $intDrawRequestId, $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || ( true == is_null( $intDrawRequestId ) && true == is_null( $intJobId ) ) ) return NULL;

		$strSql = '( SELECT
						ah.id,
						NULL as detail_id,
						CASE WHEN ah.reversal_ap_header_id IS NOT NULL AND ah.reversal_ap_header_id < ah.id THEN
							ah.header_number || \' ( REVERSED )\'
						ELSE
							ah.header_number
						END AS header_number,
						sum( drd.draw_amount ) AS transaction_amount,
						ah.post_date,
						ah.ap_header_sub_type_id as header_type_id,
						ah.reversal_ap_header_id,
						CASE WHEN ah.reversal_ap_header_id IS NOT NULL AND ah.reversal_ap_header_id < ah.id THEN
							1
						ELSE
							0
						END AS is_reversal_record,
						array_remove( array_agg(DISTINCT ac.name order by ac.name DESC ), NULL ) AS contract_names,
						ap.company_name as vendor,
						drd.rejected_by as rejected_by
					FROM
						draw_requests dr
						JOIN draw_request_details drd ON (dr.cid = drd.cid AND dr.id = drd.draw_request_id)
						JOIN ap_details ad ON (drd.cid = ad.cid AND ad.id = drd.ap_detail_id)
						JOIN ap_headers ah ON (ah.cid = ad.cid AND ah.id = ad.ap_header_id)
						JOIN ap_payees ap ON (ap.cid = ah.cid AND ap.id = ah.ap_payee_id)
						LEFT JOIN ap_contracts ac ON (ad.cid = ac.cid AND ad.ap_contract_id = ac.id)
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawRequestId . '
						AND dr.job_id = ' . ( int ) $intJobId . '
					GROUP BY
						ah.id,
						ah.cid,
						ap.company_name,
						drd.rejected_by)
					UNION
					( SELECT
						gh.id,
						gd.id as detail_id,
						gh.header_number::text,
						gd.amount,
						gh.post_date,
						NULL as header_type_id,
						NULL AS reversal_ap_header_id,
						0 AS is_reversal_record,
						array_agg(DISTINCT ac.name order by ac.name DESC ) AS contract_names,
						NULL as vendor,
						drd.rejected_by as rejected_by
					FROM
						draw_requests dr
						JOIN draw_request_details drd ON (dr.cid = drd.cid AND dr.id = drd.draw_request_id)
						JOIN gl_details gd ON (drd.cid = gd.cid AND gd.id = drd.gl_detail_id)
						JOIN gl_headers gh ON (gh.cid = gd.cid AND gh.id = gd.gl_header_id)
						LEFT JOIN ap_contracts ac ON (gd.cid = ac.cid AND gd.ap_contract_id = ac.id)
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawRequestId . '
						AND dr.job_id = ' . ( int ) $intJobId . '
					GROUP BY
						gh.id,
						gh.cid,
						gd.id,
						gd.cid,
						drd.rejected_by)
					ORDER BY
						post_date DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestPaymentsDetailsByIdByJobIdByCid( $intDrawRequestId, $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || ( true == is_null( $intDrawRequestId ) && true == is_null( $intJobId ) ) ) return NULL;

		$strSql = 'SELECT
						dr.id AS draw_request_id,
						at.id AS ar_transaction_id,
						ap.id AS ar_payment_id
					FROM
						draw_requests dr
						JOIN ar_deposit_transactions adt ON ( dr.cid = adt.cid AND dr.ar_deposit_transaction_id = adt.id )
						JOIN ar_transactions at ON ( adt.cid = at.cid AND adt.ar_transaction_id = at.id )
						JOIN ar_payments ap ON ( at.cid = ap.cid AND at.ar_payment_id = ap.id )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawRequestId . '
						AND dr.job_id = ' . ( int ) $intJobId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestsByJobIdsByCid( $arrintJobsIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || ( true == is_null( $arrintJobsIds ) ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT dr.id,
						dr.name
					FROM
						draw_requests dr
						JOIN draw_request_details drds ON ( dr.cid = drds.cid AND dr.id = drds.draw_request_id AND drds.rejected_on IS NULL )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.job_id IN ( ' . implode( ',', $arrintJobsIds ) . ' )
					ORDER BY
						dr.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawDateByIdByCid( $intDrawId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						TO_CHAR ( dr.draw_date, \'DD Mon, YYYY\' ) AS draw_date
					FROM
						draw_requests dr
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestDrawRequestsByIdByJobIdByCid( $intDrawRequestId, $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) || false == valId( $intDrawRequestId ) ) return NULL;

		$strSql = 'SELECT
						dr.*
					FROM
						draw_requests dr
						JOIN draw_requests dr1 ON ( dr1.cid = dr.cid AND dr1.job_id = dr.job_id )
					WHERE
						dr1.cid = ' . ( int ) $intCid . '
						AND dr1.job_id = ' . ( int ) $intJobId . '
						AND dr1.id = ' . ( int ) $intDrawRequestId . '
						AND dr.posted_on IS NOT NULL
						AND dr1.draw_date <= dr.draw_date 
						AND dr1.id <> dr.id
					ORDER BY
						dr.draw_date,
						dr.id';

		return parent::fetchDrawRequests( $strSql, $objDatabase );

	}

	public static function fetchDrawRequestsByIdsByJobIdByCid( $arrintDrawRequestIds, $intJobId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || ( true == valArr( $arrintDrawRequestIds ) && true == is_null( $intJobId ) ) ) return NULL;

		$strSql = 'SELECT
						dr.*,
						ap.company_name,
						ap.ap_payee_type_id,
						ac.name as ar_code_name,
						ba.account_name AS bank_account_name
					FROM
						draw_requests dr
						JOIN ap_payees ap ON ( dr.cid = ap.cid AND dr.ap_payee_id = ap.id )
						LEFT JOIN bank_accounts ba ON ( dr.cid = ba.cid AND dr.bank_account_id = ba.id AND ba.is_disabled = 0 )
						LEFT JOIN ar_codes ac ON ( ac.cid = dr.cid AND ac.id = dr.ar_code_id )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id IN ( ' . implode( ',', $arrintDrawRequestIds ) . ' )
						AND dr.job_id = ' . ( int ) $intJobId;

		return parent::fetchDrawRequests( $strSql, $objDatabase );
	}

	public static function fetchPropertyNameByDrawRequestIdByCid( $intDrawRequestId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intDrawRequestId ) ) return NULL;

		$strSql = 'SELECT
						dr.id,
						p.property_name
					FROM
						draw_requests dr
						JOIN properties p ON ( p.cid = dr.cid AND p.id = dr.property_id )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.id = ' . ( int ) $intDrawRequestId;

		return extractArrayKeyValuePairs( fetchData( $strSql, $objDatabase ), 'id', 'property_name' );

	}

	public static function fetchDrawRequestsCountByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT ( * ) AS total_draw_count,
						COALESCE( SUM ( CASE WHEN dr.posted_on IS NOT NULL THEN 1 ELSE 0 END ), 0 ) AS finalize_draw_count
					FROM
						draw_requests dr
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND dr.ap_payee_id = ' . ( int ) $intApPayeeId;

		return fetchData( $strSql, $objDatabase );

	}

}
?>