<?php

class CRevenueOverrideRent extends CBaseRevenueOverrideRent {

	protected $m_intUnitSpaceConfigurationId;
	protected $m_intIsRenewal;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableOnDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideRentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideRentReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getUnitSpaceConfigurationId() {
		return $this->m_intUnitSpaceConfigurationId;
	}

	public function setUnitSpaceConfigurationId( $intUnitSpaceConfigurationId ) {
		$this->m_intUnitSpaceConfigurationId = CStrings::strToIntDef( $intUnitSpaceConfigurationId, NULL, false );
	}

	public function getIsRenewal() {
		return $this->m_intIsRenewal;
	}

	public function setIsRenewal( $intIsRenewal ) {
		$this->m_intIsRenewal = CStrings::strToIntDef( $intIsRenewal, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['unit_space_configuration_id'] ) ) {
			$this->m_intUnitSpaceConfigurationId = trim( stripcslashes( $arrmixValues['unit_space_configuration_id'] ) );
		}
		if( isset( $arrmixValues['is_renewal'] ) ) {
			$this->m_intIsRenewal = trim( stripcslashes( $arrmixValues['is_renewal'] ) );
		}
	}

}
?>