<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationRequests
 * Do not add any new functions to this class.
 */

class CScreeningApplicationRequests extends CBaseScreeningApplicationRequests {

	public static function fetchScreeningApplicationRequestByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeCancelledScreeningApplicationRequest = false ) {
		$strCheckCancelledScreeningApplicationRequestSql = ( false == $boolIncludeCancelledScreeningApplicationRequest ) ? ' AND ( request_status_type_id <>' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ')' : '';
		$strSql = 'SELECT * FROM screening_application_requests  WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . $strCheckCancelledScreeningApplicationRequestSql . ' ORDER BY id DESC LIMIT 1';

		return parent::fetchScreeningApplicationRequest( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestByRequestTypeIdsByApplicationIdByCid( $arrintScreeningRequestTypeId, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningRequestTypeId ) ) return NULL;

		$strSql = 'SELECT * FROM
						screening_application_requests
					WHERE
						request_status_type_id IN ( ' . implode( ', ', $arrintScreeningRequestTypeId ) . ' )
						AND application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						id DESC
					LIMIT 1';

		return parent::fetchScreeningApplicationRequest( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByApplicationIdsByCid( $arrintResidentVerifyApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentVerifyApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						screening_application_requests
					WHERE
						application_id IN ( ' . implode( ',', $arrintResidentVerifyApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND request_status_type_id <> ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;

		return parent::fetchScreeningApplicationRequests( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestByRemoteScreeningIdByCid( $intRemoteScreeningId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_application_requests
					WHERE
						remote_screening_id = ' . ( int ) $intRemoteScreeningId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicationRequest( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByScreeningVendorIdByRequestStatusTypeIdByCid( $arrintScreeningVendorIds, $intRequestStatusTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningVendorIds ) ) return NULL;

		$strSql = 'SELECT
						sap.*,
						app.property_id
    				FROM
						screening_application_requests sap
    					JOIN applications app ON sap.application_id = app.id AND app.cid = sap.cid
    				WHERE
						request_status_type_id = ' . ( int ) $intRequestStatusTypeId . '
    					AND sap.cid = ' . ( int ) $intCid . '
    					AND remote_screening_id IS NOT NULL
    					AND screening_vendor_id IN ( ' . implode( ',', $arrintScreeningVendorIds ) . ' ) ';

		return parent::fetchScreeningApplicationRequests( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsClientIds( $objDatabase ) {

		$strSql = '	SELECT
						array_to_string ( array_agg ( DISTINCT cid ORDER BY CID ASC ), \',\' ) AS client_ids
					FROM
						screening_application_requests
					WHERE
						cid > 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsNotInResultsByCid( $intCid, $objDatabase ) {

		$strSql = '	SELECT
						sa_req.cid,
						apps.property_id,
						sa_req.id,
						sa_req.application_id,
						a.id AS applicant_id
					FROM
						screening_application_requests sa_req
						LEFT JOIN screening_applicant_results sa_res ON ( sa_res.cid = sa_req.cid AND sa_res.screening_application_request_id = sa_req.id )
						LEFT JOIN applications apps ON ( apps.cid = sa_req.cid AND apps.id = sa_req.application_id )
						JOIN applicant_applications aa ON ( aa.cid = sa_req.cid AND aa.application_id = sa_req.application_id )
						JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
					WHERE
						sa_req.cid = ' . ( int ) $intCid . '
						AND sa_req.remote_screening_id IS NOT NULL
						AND sa_res.screening_application_request_id IS NULL
					ORDER BY
						cid,
						property_id,
						application_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningApplicationRequestDataByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						screening_decision_type_id,
						screening_recommendation_type_id
					FROM
						screening_application_requests
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;

		return  fetchData( $strSql, $objDatabase );
	}

	public static function fetchUncancelledScreeningApplicationRequestByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						screening_application_requests
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid . '
						AND request_status_type_id <> ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;

		return parent::fetchScreeningApplicationRequest( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningApplicationRequestByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT remote_screening_id, request_status_type_id FROM screening_application_requests WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC LIMIT 1';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return  $arrmixResult[0];
	}

}
?>