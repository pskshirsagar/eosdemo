<?php

class CCorporateVerification extends CBaseCorporateVerification {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 0 == strlen( $this->m_intPropertyId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCorporateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNameFirst ) || 0 == strlen( $this->m_strNameFirst ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Corporate Contact First Name is required.' ) ) );
		} elseif( false == preg_match( '/^[a-zA-Z0-9\,\&\-\.\']*$/', str_replace( ' ', '', $this->m_strNameFirst ) ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Corporate Contact First Name is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNameLast ) || 0 == strlen( $this->m_strNameLast ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Corporate Contact Last Name is required.' ) ) );
		} elseif( false == preg_match( '/^[a-zA-Z0-9\,\&\-\.\']*$/', str_replace( ' ', '', $this->m_strNameLast ) ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Corporate Contact Last Name is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		if( false == isset( $this->m_strEmailAddress ) || 0 == strlen( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Corporate Contact Email is required.' ) ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Corporate Contact Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL ) );
		}

		return $boolIsValid;
	}

	public function valContactNumber() {
		$boolIsValid = true;
		$strContactPhoneNumber   = $this->getContactNumber();
		$objContactPhoneNumber = $this->createPhoneNumber( $strContactPhoneNumber );

		$intContactPhoneLength	= strlen( $objContactPhoneNumber->getNumber() );

		if( 0 == $intContactPhoneLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_number', __( 'Corporate Contact Number is required.' ) ) );
		} elseif( true == valStr( $objContactPhoneNumber->getNumber() ) && 0 < $intContactPhoneLength && false == $objContactPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'contact_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objContactPhoneNumber->getFormattedErrors(), [ 'region' => $objContactPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objContactPhoneNumber->getCountryCode() ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strFileName ) || 0 == strlen( $this->m_strFileName ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Corporate Letter of Responsibility is required' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_corporate_verification':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valContactNumber();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valFileName();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function encryptUploadedFile( $objDocumentManager, $strFolderName = PATH_MOUNTS_CORPORATE_DOCS, $intCid, $strTempFileName, $boolEncrypt = true ) {

		$strUploadPath = CDocumentManagerUtils::getTemporaryPath( PATH_MOUNTS_CORPORATE_DOCS, $intCid );

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				return false;
			}
		}

		$strDestinationPath = $strUploadPath . $this->getFileName();
		if( false == move_uploaded_file( $strTempFileName,  $strDestinationPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			return false;
		}

		$strFileContent = file_get_contents( $strDestinationPath );

		if( false == $objDocumentManager->putDocument( CDocumentCriteria::forPut( -1, $strFolderName, $this->getFilePath() . $this->getFileName(), $strFileContent, $boolEncrypt ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt document' ) ) );
			return false;
		}

		return true;
	}

	public function downloadCorporateVerificationDocument( $objObjectStorageGateway ) {

		$objObjectGetResponse = $objObjectStorageGateway->getObject( [
			'cid'           => $this->getCid(),
			'objectId'      => -1,
			'container'     => PATH_MOUNTS_CORPORATE_DOCS,
			'key'           => $this->getFilePath() . $this->getFileName(),
			'outputFile'    => 'temp'
		] );

		$strFullPath = $objObjectGetResponse['outputFile'];

		$objResponse = $objObjectStorageGateway->getObject( [
			'cid'         => $this->getCid(),
			'objectId'    => -1,
			'container'   => PATH_MOUNTS_CORPORATE_DOCS,
			'key'         => $this->getFilePath() . $this->getFileName(),
			'checkExists' => true
		] );

		$boolIsExists = $objResponse['isExists'];

		if( false == $boolIsExists ) {
			trigger_error( 'File is not present or has been deleted.', E_USER_ERROR );
			exit;
		}

		$arrstrFileParts = pathinfo( PATH_MOUNTS_CORPORATE_DOCS . $this->getFileName() );

		if( false == valArr( $arrstrFileParts ) ) {
			return false;
		}

		if( 'pdf' == $arrstrFileParts['extension'] ) {
			header( 'Pragma:public' );
			header( 'Expires:0' );
			header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control:public' );
			header( 'Content-Description:File Transfer' );
			header( 'Content-Type: application/pdf' );
			header( 'Content-Disposition: attachment; filename="' . $arrstrFileParts['filename'] . date( '_M_d_Y_g_i_a', time() ) . '.pdf"' );
			echo file_get_contents( $strFullPath );
		} else {
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length: ' . filesize( $strFullPath ) );
			header( 'Content-Disposition: attachment; filename= ' . $this->getFileName() );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Transfer-Encoding:binary' );
			echo file_get_contents( $strFullPath );
		}

		CFileIo::deleteFile( $strFullPath );
		exit();
	}

	public function encryptUploadedObject( $objObjectStorageGateway, $strFolderName = PATH_MOUNTS_CORPORATE_DOCS, $intCid, $strTempFileName, $boolEncrypt = true ) {

		$strUploadPath = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_CORPORATE_DOCS, $intCid );

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				return false;
			}
		}

		$strDestinationPath = $strUploadPath . $this->getFileName();

		if( false == move_uploaded_file( $strTempFileName,  $strDestinationPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			return false;
		}

		$strFileContent = file_get_contents( $strDestinationPath );
		CFileIo::deleteFile( $strDestinationPath );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( [
			'cid'       => $intCid,
			'objectId'  => -1,
			'container' => $strFolderName,
			'key'       => $this->getFilePath() . $this->getFileName(),
			'data'      => $strFileContent,
			'noEncrypt' => !$boolEncrypt
		] );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt document' ) ) );
			return false;
		}

		return true;
	}

	public function setVerificationDocDetails() {
		$strPath    = 'corporate_verification/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';
		$this->setFilePath( $strPath );
		$this->setFileName( $_FILES['corporate_verification']['name']['document'] );
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}
?>