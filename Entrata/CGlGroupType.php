<?php

class CGlGroupType extends CBaseGlGroupType {

	const STANDARD_GROUP      = 1;
	const TOTAL_GROUP         = 2;
	const NET_CASH_FLOW_GROUP = 3;

	/**
	 * Other Functions
	 */

	public static function populateSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'GL_GROUP_TYPE_STANDARD_GROUP', self::STANDARD_GROUP );
		$objSmarty->assign( 'GL_GROUP_TYPE_TOTAL_GROUP', self::TOTAL_GROUP );
		$objSmarty->assign( 'GL_GROUP_TYPE_NET_CASH_FLOW_GROUP', self::NET_CASH_FLOW_GROUP );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['GL_GROUP_TYPE_STANDARD_GROUP']      = self::STANDARD_GROUP;
		$arrmixTemplateParameters['GL_GROUP_TYPE_TOTAL_GROUP']         = self::TOTAL_GROUP;
		$arrmixTemplateParameters['GL_GROUP_TYPE_NET_CASH_FLOW_GROUP'] = self::NET_CASH_FLOW_GROUP;

		return $arrmixTemplateParameters;
	}

}
?>