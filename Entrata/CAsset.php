<?php

class CAsset extends CBaseAsset {

	const LOOKUP_TYPE_AP_CODES                           = 'ap_codes';
	const LOOKUP_TYPE_UNIT_OF_MEASURES                   = 'unit_of_measures';
	const LOOKUP_TYPE_INVENTORY_AP_CODE                  = 'inventory_ap_codes';
	const LOOKUP_TYPE_FIXED_ASSETS_AP_CODE               = 'fixed_assets_ap_codes';
	const LOOKUP_TYPE_PURCHASE_ORDERS_VENDOR             = 'purchase_orders_vendor';
	const LOOKUP_TYPE_FIXED_ASSETS_PROPERTIES            = 'fixed_assets_properties';
	const LOOKUP_TYPE_LOCATIONS_ON_PROPERTIES            = 'locations_on_properties';
	const LOOKUP_TYPE_PURCHASE_ORDERS_PO_NUMBER          = 'purchase_orders_po_number';
	const LOOKUP_TYPE_ADD_PURCHASE_ORDER_VENDOR          = 'add_purchase_order_vendor';
	const LOOKUP_TYPE_PURCHASE_ORDERS_CREATED_BY         = 'purchase_orders_created_by';
	const LOOKUP_TYPE_ADD_PURCHASE_ORDER_PROPERTY        = 'add_purchase_order_property';
	const LOOKUP_TYPE_JOB_PURCHASE_ORDERS_VENDOR         = 'job_purchase_orders_vendor';
	const LOOKUP_TYPE_JOB_PURCHASE_ORDERS_PO_NUMBER      = 'job_purchase_orders_po_number';
	const LOOKUP_TYPE_PURCHASE_ORDERS_HEADER_NUMBER      = 'purchase_orders_header_number';
	const LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_VENDOR     = 'approve_purchase_orders_vendor';
	const LOOKUP_TYPE_INVENTORY_ADJUSTMENT_PROPERTIES    = 'inventory_adjustment_properties';
	const LOOKUP_TYPE_VENDOR_PURCHASE_ORDERS_PO_NUMBER   = 'vendor_purchase_orders_po_number';
	const LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_PO_NUMBER  = 'approve_purchase_orders_po_number';
	const LOOKUP_TYPE_APPROVE_PURCHASE_ORDERS_CREATED_BY = 'approve_purchase_orders_created_by';

	protected $m_intAssetCount;
	protected $m_intPoApDetailId;
	protected $m_intUnitOfMeasureId;
	protected $m_intUnitOfMeasureTypeId;
	protected $m_intPropertyTypeId;

	protected $m_boolIsStudentProperty;

	protected $m_fltQtyOnHand;
	protected $m_fltQtyOnOrder;
	protected $m_fltExtendedCost;

	protected $m_strApHeaderIds;
	protected $m_strPropertyName;
	protected $m_intPropertyType;
	protected $m_strApCodeName;
	protected $m_strApCodeNumber;
	protected $m_strVendorCompanyName;
	protected $m_strAssetLocationName;
	protected $m_strUnitOfMeasureName;
	protected $m_strApCodeCategoryName;

	protected $m_objAssetDetail;
	protected $m_objAssetTransaction;

	/**
	 * Get Functions
	 *
	 */

	public function getUnitOfMeasureTypeId() {
		return $this->m_intUnitOfMeasureTypeId;
	}

	public function getApCodeNumber() {
		return $this->m_strApCodeNumber;
	}

	public function getApCodeName() {
		return $this->m_strApCodeName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyType() {
		return $this->m_intPropertyType;
	}

	public function getAssetLocationName() {
		return $this->m_strAssetLocationName;
	}

	public function getApCodeCategoryName() {
		return $this->m_strApCodeCategoryName;
	}

	public function getUnitOfMeasureName() {
		return $this->m_strUnitOfMeasureName;
	}

	public function getQtyOnHand() {
		return $this->m_fltQtyOnHand;
	}

	public function getExtendedCost() {
		return $this->m_fltExtendedCost;
	}

	public function getApHeaderIds() {
		return $this->m_strApHeaderIds;
	}

	public function getQtyOnOrder() {
		return $this->m_fltQtyOnOrder;
	}

	public function getPoApDetailId() {
		return $this->m_intPoApDetailId;
	}

	public function getAssetDetail() {
		return $this->m_objAssetDetail;
	}

	public function getAssetTransaction() {
		return $this->m_objAssetTransaction;
	}

	public function getUnitOfMeasureId() {
		return $this->m_intUnitOfMeasureId;
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function getIsStudentProperty() {
		return $this->m_boolIsStudentProperty;
	}

	public function getVendorCompanyName() {
		return $this->m_strVendorCompanyName;
	}

	public function getAssetCount() {
		return $this->m_intAssetCount;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setUnitOfMeasureTypeId( $intUnitOfMeasureTypeId ) {
		$this->set( 'm_intUnitOfMeasureTypeId', CStrings::strToIntDef( $intUnitOfMeasureTypeId, NULL, false ) );
	}

	public function setApCodeNumber( $strApCodeNumber ) {
		$this->m_strApCodeNumber = $strApCodeNumber;
	}

	public function setApCodeName( $strApCodeName ) {
		$this->m_strApCodeName = $strApCodeName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyType( $intPropertyType ) {
		$this->m_intPropertyType = $intPropertyType;
	}

	public function setAssetLocationName( $strAssetLocationName ) {
		$this->m_strAssetLocationName = $strAssetLocationName;
	}

	public function setApCodeCategoryName( $strApCodeCategoryName ) {
		$this->m_strApCodeCategoryName = $strApCodeCategoryName;
	}

	public function setUnitOfMeasureName( $strUnitOfMeasureName ) {
		$this->m_strUnitOfMeasureName = $strUnitOfMeasureName;
	}

	public function setQtyOnHand( $fltQtyOnHand ) {
		$this->m_fltQtyOnHand = $fltQtyOnHand;
	}

	public function setExtendedCost( $fltExtendedCost ) {
		$this->m_fltExtendedCost = $fltExtendedCost;
	}

	public function setApHeaderIds( $strApHeaderIds ) {
		$this->m_strApHeaderIds = $strApHeaderIds;
	}

	public function setQtyOnOrder( $fltQtyOnOrder ) {
		$this->m_fltQtyOnOrder = $fltQtyOnOrder;
	}

	public function setPoApDetailId( $intPoApDetailId ) {
		$this->m_intPoApDetailId = $intPoApDetailId;
	}

	public function setUnitOfMeasureId( $intUnitOfMeasureId ) {
		$this->set( 'm_intUnitOfMeasureId', CStrings::strToIntDef( $intUnitOfMeasureId, NULL, false ) );
	}

	public function setAssetDetail( $objAssetDetail ) {
		$this->m_objAssetDetail = $objAssetDetail;
	}

	public function setAssetTransaction( $objAssetTransaction ) {
		$this->m_objAssetTransaction = $objAssetTransaction;
	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		$this->m_intPropertyTypeId = $intPropertyTypeId;
	}

	public function setIsStudentProperty( $boolIsStudentProperty ) {
		$this->m_boolIsStudentProperty = $boolIsStudentProperty;
	}

	public function setVendorCompanyName( $strVendorCompanyName ) {
		$this->m_strVendorCompanyName = CStrings::strTrimDef( $strVendorCompanyName, 150, NULL, true );
	}

	public function setAssetCount( $intAssetCount ) {
		$this->set( 'm_intAssetCount', CStrings::strToIntDef( $intAssetCount, NULL, false ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_code_number'] ) ) {
			$this->setApCodeNumber( $arrmixValues['ap_code_number'] );
		}
		if( true == isset( $arrmixValues['ap_code_name'] ) ) {
			$this->setApCodeName( $arrmixValues['ap_code_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['property_type'] ) ) {
			$this->setPropertyType( $arrmixValues['property_type'] );
		}
		if( true == isset( $arrmixValues['asset_location_name'] ) ) {
			$this->setAssetLocationName( $arrmixValues['asset_location_name'] );
		}
		if( true == isset( $arrmixValues['ap_code_category_name'] ) ) {
			$this->setApCodeCategoryName( $arrmixValues['ap_code_category_name'] );
		}
		if( true == isset( $arrmixValues['unit_of_measure_name'] ) ) {
			$this->setUnitOfMeasureName( $arrmixValues['unit_of_measure_name'] );
		}
		if( true == isset( $arrmixValues['qty_on_hand'] ) ) {
			$this->setQtyOnHand( $arrmixValues['qty_on_hand'] );
		}
		if( true == isset( $arrmixValues['qty_on_order'] ) ) {
			$this->setQtyOnOrder( $arrmixValues['qty_on_order'] );
		}
		if( true == isset( $arrmixValues['extended_cost'] ) ) {
			$this->setExtendedCost( $arrmixValues['extended_cost'] );
		}
		if( true == isset( $arrmixValues['unit_of_measure_id'] ) ) {
			$this->setUnitOfMeasureId( $arrmixValues['unit_of_measure_id'] );
		}
		if( true == isset( $arrmixValues['property_type_id'] ) ) {
			$this->setPropertyTypeId( $arrmixValues['property_type_id'] );
		}
		if( true == isset( $arrmixValues['is_student_property'] ) ) {
			$this->setIsStudentProperty( $arrmixValues['is_student_property'] );
		}
		if( true == isset( $arrmixValues['unit_of_measure_type_id'] ) ) {
			$this->setUnitOfMeasureTypeId( $arrmixValues['unit_of_measure_type_id'] );
		}
		if( true == isset( $arrmixValues['ap_header_ids'] ) ) {
			$this->setApHeaderIds( $arrmixValues['ap_header_ids'] );
		}

		if( true == isset( $arrmixValues['asset_count'] ) ) {
			$this->setAssetCount( $arrmixValues['asset_count'] );
		}

	}

	public function createAssetDetail() {

		$objAssetDetail = new CAssetDetail();

		$objAssetDetail->setAssetId( $this->getId() );
		$objAssetDetail->setCid( $this->getCid() );

		return $objAssetDetail;
	}

	public function createAssetTransaction() {

		$objAssetTransaction = new CAssetTransaction();

		$objAssetTransaction->setAssetId( $this->getId() );
		$objAssetTransaction->setCid( $this->getCid() );

		return $objAssetTransaction;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId( $objCompanyUser = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );

		} elseif( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $this->getCid(), $objCompanyUser->getId(), $objCompanyUser->getIsAdministrator(), true, true );

			$arrobjProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchProperties( $strSql, $objClientDatabase );

			if( false == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please select valid property.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valApCodeId( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getApCodeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'Catalog item is required.' ) );

		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$arrobjApCodes = ( array ) \Psi\Eos\Entrata\CApCodes::createService()->fetchAllApCodesByAssetTypeIdsByCid( [ CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED ], $this->getCid(), $objClientDatabase );

			if( false == array_key_exists( $this->getApCodeId(), $arrobjApCodes ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'Please select valid catalog item.' ) );

				return false;
			}

			$arrobjGlAccountProperties = ( array ) rekeyObjects( 'PropertyId', CGlAccountProperties::fetchGlAccountPropertiesByApCodeIdByCid( $this->getApCodeId(), $this->getCid(), $objClientDatabase ) );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjGlAccountProperties ) && false == array_key_exists( $this->getPropertyId(), $arrobjGlAccountProperties ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'The gl account associated to this item is restricted and not available for use by the selected property. Please update the gl account setup, or select a different catalog item or property.' ) );
			}
		}

		return $boolIsValid;

	}

	public function valAssetLocationId() {

		$boolIsValid = true;

		if( false == valId( $this->getAssetLocationId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_location_id', 'Location is required.' ) );
		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$arrmixAssetLocations = ( array ) \Psi\Eos\Entrata\CAssetLocations::createService()->fetchActiveAssetLocationsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

			$arrintLocationIds = [];

			foreach( $arrmixAssetLocations as $arrmixAssetLocation ) {
				$arrintLocationIds[$arrmixAssetLocation['id']] = $arrmixAssetLocation['id'];
			}

			if( false == array_key_exists( $this->getAssetLocationId(), $arrintLocationIds ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_location_id', 'Please select valid location.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valSerialNumber( $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valStr( $this->getSerialNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'serial_number', 'Serial number is required.' ) );

			return false;
		}

		if( NULL == $objClientDatabase ) {
			return NULL;
		}

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND serial_number = \'' . addslashes( $this->getSerialNumber() ) . '\'
							AND id <> ' . ( int ) $this->getId();

		$intAssetsCount = \Psi\Eos\Entrata\CAssets::createService()->fetchAssetCount( $strWhereSql, $objClientDatabase );

		if( 0 < $intAssetsCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'serial_number', 'Serial number already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyAssetLocationAssociation( $objClientDatabase = NULL ) : bool {
		if( true == valId( $this->getPropertyId() )
		    && true == valId( $this->getAssetDetail()->getAssetLocationId() )
		    && true == valObj( $objClientDatabase, CDatabase::class )
		    && true == valObj( $this->getAssetDetail(), CAssetDetail::class ) ) {
			$objAssetLocation = \Psi\Eos\Entrata\CAssetLocations::createService()->fetchAssetLocationByIdByPropertyIdByCid( $this->getAssetDetail()->getAssetLocationId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );

			if( false == valObj( $objAssetLocation, CAssetLocation::class ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_location', __( 'Asset location {%s, 0} is not associated with the selected property.', [ $this->getAssetDetail()->getAssetLocationName() ] ) ) );

				return false;
			}
		}

		return true;
	}

	public function validate( $strAction, $objCompanyUser = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valApCodeId( $objClientDatabase );
				$boolIsValid &= $this->valPropertyId( $objCompanyUser, $objClientDatabase );

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSerialNumber( $objClientDatabase );
				break;

			case 'item_transfer':
				$boolIsValid &= $this->valPropertyId( $objCompanyUser, $objClientDatabase );
				$boolIsValid &= $this->valPropertyAssetLocationAssociation( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function createEvent() {

		$objEvent = new CEvent();
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventTypeId( CEventType::ASSET_EDITED );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setScheduledDatetime( 'NOW()' );
		$objEvent->setDataReferenceId( $this->getId() );

		return $objEvent;
	}

}

?>