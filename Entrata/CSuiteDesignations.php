<?php

class CSuiteDesignations extends CBaseSuiteDesignations {

	public static function fetchSuiteDesignations( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSuiteDesignation', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSuiteDesignation( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSuiteDesignation', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCustomSuiteDesignations( $objDatabase ) {
		$strSql = 'SELECT * FROM suite_designations';
		return self::fetchSuiteDesignations( $strSql, $objDatabase );
	}

}
?>