<?php

class CLateFeeBatchDetail extends CBaseLateFeeBatchDetail {

	protected $m_objArTransaction;
	protected $m_strBatchDateTime;
	protected $m_strArTransactionMemo;
	protected $m_intRetainedParentLateFeebatchDetailId;
	protected $m_intArCodeId;

	/**
	 * Get Functions
	 */

	public function getArTransaction() {
		return $this->m_objArTransaction;
	}

	public function getBatchDateTime() {
		return $this->m_strBatchDateTime;
	}

	public function getArTransactionMemo() {
		return $this->m_strArTransactionMemo;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['batch_datetime'] ) ) 		$this->setBatchDateTime( $arrValues['batch_datetime'] );
		if( true == isset( $arrValues['ar_transaction_memo'] ) ) 	$this->setArTransactionMemo( $arrValues['ar_transaction_memo'] );
		if( true == isset( $arrValues['ar_code_id'] ) ) 			$this->setArCodeId( $arrValues['ar_code_id'] );
	}

	public function setArTransaction( $objArTransaction ) {
		return $this->m_objArTransaction = $objArTransaction;
	}

	public function setBatchDateTime( $strBatchDateTime ) {
		return $this->m_strBatchDateTime = $strBatchDateTime;
	}

	public function setArTransactionMemo( $strArTransactionMemo ) {
		$this->m_strArTransactionMemo = $strArTransactionMemo;
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	/**
	 * Create Functions
	 */

	public function createArTransaction( $intLeaseIntervalId ) {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setArCodeId( $this->getArCodeId() );
		$objArTransaction->setPostDate( $this->getPostDate() );
		$objArTransaction->setLeaseIntervalId( $intLeaseIntervalId );
		$objArTransaction->setArTriggerId( CArTrigger::LATE_FEE );
		$objArTransaction->setMemo( $this->getArTransactionMemo() );

		return $objArTransaction;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'management_compay_id', __( 'client id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLateFeeBatchId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLateFeeBatchId() ) || 0 >= ( int ) $this->getLateFeeBatchId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_batch_id', __( 'Late fee batch id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseId() ) || 0 >= ( int ) $this->getLeaseId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerId() ) || 0 >= ( int ) $this->getCustomerId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Customer id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLateFeeFormulaId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLateFeeFormulaId() ) || 0 >= ( int ) $this->getLateFeeFormulaId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_formula_id', __( 'Late fee formula id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLateFeeActionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLateFeeActionTypeId() ) || 0 >= ( int ) $this->getLateFeeActionTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_action_type_id', __( 'Late fee action type id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPostDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPostedAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getPostedAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_amount', __( 'Amount is required.' ) ) );
		}

		if( 0 >= ( float ) $this->getPostedAmount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_amount', __( 'Late fee charge amount must be greater than zero.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valWaiveComments() {
		$boolIsValid = true;
		if( CLateFeeActionType::WAIVE == $this->getLateFeeActionTypeId() && false == valStr( $this->getMemo() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', __( 'A comment is required to waive a late fee.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valSuggestedAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getSuggestedAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new  CErrorMsg( ERROR_TYPE_VALIDATION, 'suggested_amount', __( 'Suggested amount is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsInitialLateFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsExpired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

	switch( $strAction ) {
		case VALIDATE_INSERT:
		case VALIDATE_UPDATE:
        	$boolIsValid &= $this->valCid();
        	$boolIsValid &= $this->valPropertyId();
       		$boolIsValid &= $this->valLeaseId();
        	$boolIsValid &= $this->valCustomerId();
        	$boolIsValid &= $this->valLateFeeFormulaId();
            $boolIsValid &= $this->valLateFeeActionTypeId();
            $boolIsValid &= $this->valArTransactionId();
            $boolIsValid &= $this->valPostDate();
            $boolIsValid &= $this->valPostedAmount();
            $boolIsValid &= $this->valSuggestedAmount();
            $boolIsValid &= $this->valWaiveComments();
            break;

		case VALIDATE_DELETE:
			break;

		default:
			$boolIsValid = true;
			break;
	}

		return $boolIsValid;
	}

}
?>