<?php

class CWebsiteAdRun extends CBaseWebsiteAdRun {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDateAndEndDate( $objDatabase ) {
		$boolIsValid = true;

		if( false == CValidation::isValidDate( $this->getStartDate() ) || false == CValidation::isValidDate( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Start date/ End date should not be empty and it should be valid date format.' ) ) );
		} else {
			if( true == is_null( $this->getId() ) && ( strtotime( $this->getStartDate() ) < strtotime( date( 'm/d/Y' ) ) || strtotime( $this->getEndDate() ) < strtotime( date( 'm/d/Y' ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Start date/ End date should be greater then equal to current date' ) ) );
			} elseif( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Start date should be less than end date.' ) ) );
			} else {
				// Validate with the database and check that current date should not in range of start_date and end_date.

				$arrmixActiveAdRun = \Psi\Eos\Entrata\CWebsiteAdRuns::createService()->fetchWebsiteAdRunsByStartAndEndDateByWebsiteIdByCid( $this->getId(), $this->getStartDate(), $this->getEndDate(), $this->getWebsiteId(), $this->getCid(), $objDatabase );

				if( true == valArr( $arrmixActiveAdRun ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', '<mark>' . $arrmixActiveAdRun[0]['name'] . ' </mark> this ad run associated with selected date range please select different dates.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStartDateAndEndDate( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStartDateAndEndDate( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>