<?php

class CBankAccountMerchantAccountLog extends CBaseBankAccountMerchantAccountLog {

    public function mapBankAccountMerchantAccountLog( $objBankAccountMerchantAccountLog ) {

    	$this->setCid( $objBankAccountMerchantAccountLog->getCid() );
    	$this->setMerchantAccountId( $objBankAccountMerchantAccountLog->getMerchantAccountId() );
    	$this->setBankAccountLogId( $objBankAccountMerchantAccountLog->getId() );
    	$this->setCreatedBy( $objBankAccountMerchantAccountLog->getCreatedBy() );
    	$this->setCreatedOn( $objBankAccountMerchantAccountLog->getCreatedOn() );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBankAccountLogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMerchantAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>