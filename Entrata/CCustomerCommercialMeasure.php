<?php

class CCustomerCommercialMeasure extends CBaseCustomerCommercialMeasure {

	protected $m_intFileId;
	protected $m_intFileExtensionId;

	protected $m_strFileName;
	protected $m_strFileTypeName;
	protected $m_strFileTitle;
	protected $m_strSignInitiatedOn;

	protected $m_fltPostedSalesAmount;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['posted_sale_amount'] ) ) {
			$this->setPostedSalesAmount( $arrmixValues['posted_sale_amount'] );
		}

		if( true == isset( $arrmixValues['file_id'] ) ) {
			$this->setFileId( $arrmixValues['file_id'] );
		}

		if( true == isset( $arrmixValues['file_extension_id'] ) ) {
			$this->setFileExtensionId( $arrmixValues['file_extension_id'] );
		}

		if( true == isset( $arrmixValues['file_title'] ) ) {
			$this->setFileTitle( $arrmixValues['file_title'] );
		}

		if( true == isset( $arrmixValues['file_type_name'] ) ) {
			$this->setFileTypeName( $arrmixValues['file_type_name'] );
		}

		if( true == isset( $arrmixValues['sign_initiated_on'] ) ) {
			$this->setSignInitiatedOn( $arrmixValues['sign_initiated_on'] );
		}
	}

	public function getPostedSalesAmount() {
		return $this->m_fltPostedSalesAmount;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function getFileTitle() {
		return $this->m_strFileTitle;
	}

	public function getFileTypeName() {
		return $this->m_strFileTypeName;
	}

	public function getSignInitiatedOn() {
		return $this->m_strSignInitiatedOn;
	}

	public function setPostedSalesAmount( $fltPostedSalesAmount ) {
		$this->m_fltPostedSalesAmount = CStrings::strToFloatDef( $fltPostedSalesAmount, NULL, false, 2 );
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->m_intFileExtensionId = $intFileExtensionId;
	}

	public function setFileTitle( $strFileTitle ) {
		$this->m_strFileTitle = $strFileTitle;
	}

	public function setFileTypeName( $strFileTypeName ) {
		$this->m_strFileTypeName = $strFileTypeName;
	}

	public function setSignInitiatedOn( $strSignInitiatedOn ) {
		$this->m_strSignInitiatedOn = $strSignInitiatedOn;
	}

	public function valMeasureMonth() {
		$boolIsValid = true;
		if( false == valStr( $this->getMeasureMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'measure_month', __( 'Measure Month is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valMeasureDate() {
		$boolIsValid = true;
		if( false == valStr( $this->getMeasureDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'measure_date', __( 'Measure Date is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valGrossSales() {
		$boolIsValid = true;
		if( true == is_null( $this->getGrossSales() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_sale', __( 'Sales total is required.' ) ) );
		}

		if( -1 >= ( int ) $this->getGrossSales() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_sale_amount', __( 'Please enter valid amount.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMeasureMonth();
				$boolIsValid &= $this->valMeasureDate();
				$boolIsValid &= $this->valGrossSales();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->createCustomerCommercialMeasureActivityLog( $this, $intCurrentUserId, CEventSubType::SALES_NUMBER_ADDED, $objDatabase );
		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createCustomerCommercialMeasureActivityLog( $objCustomerCommercialMeasure, $intCurrentUserId, $intEventSubTypeId, $objDatabase ) {
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCurrentUserId, $objCustomerCommercialMeasure->getCid(), $objDatabase );
		$objLease       = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objCustomerCommercialMeasure->getLeaseId(), $objCustomerCommercialMeasure->getCid(), $objDatabase );

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $objCustomerCommercialMeasure ], CEventType::CUSTOMER_COMMERCIAL_MEASURE, $intEventSubTypeId );
		$objEvent->setCid( $objCustomerCommercialMeasure->getCid() );
		$objEvent->setPropertyId( $objLease->getPropertyId() );
		$objEvent->setLeaseId( $objCustomerCommercialMeasure->getLeaseId() );
		$objEvent->setEventDatetime( CEvent::getCurrentUtcTime() );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setUpdatedBy( $objCompanyUser->getId() );
		$objEvent->setUpdatedOn( 'NOW()' );
		$objEvent->setCreatedBy( $objCompanyUser->getId() );
		$objEvent->setCreatedOn( 'NOW()' );
		$objEvent->setSaleNumber( $objCustomerCommercialMeasure->getGrossSales() );
		$objEvent->setMeasureMonth( $objCustomerCommercialMeasure->getMeasureMonth() );
		$objEvent->setDataReferenceId( $objCustomerCommercialMeasure->getId() );

		$objEventLibrary->buildEventDescription( $this );

		if( false === $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );

			return false;
		}

		return true;
	}

}
?>