<?php

class CMarketingMediaAssociation extends CBaseMarketingMediaAssociation {

	protected $m_intMediaCategoryId;
	protected $m_strMediaCategoryName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingMediaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingMediaTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingMediaSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingStorageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingStorageTypeReference() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOptimized() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getMediaLibraryFullSizeUri() {
		// imageOptimizer Condition for converting images to webp format on the fly.
		if( true === defined( 'CONFIG_COMMON_PATH' ) && true === defined( 'CONFIG_LOAD_WEBP' ) ) {
			return ( true === valStr( $this->getFullsizeUri() ) ? CONFIG_COMMON_PATH . '/images/imageOptimizer.php?f=webp&src=' . $this->getMarketingStorageTypeReference() : NULL );
		}

		return CConfig::get( 'media_library_path' ) . $this->getMarketingStorageTypeReference();
	}

	public function getMediaLibraryThumbnailUri() {
		return CConfig::get( 'media_library_path' ) . $this->getThumbnailUri();
	}

	public function getCachedImageOrThumbnailerPath( $intWidth = 100, $intHeight = 100, $boolIsUseThumbnail = false ) {
		if( false == valStr( $this->getFileName() ) ) {
			return '/Common/images/no_image.gif';
		}

		$strThumbnailerPath = '/Common/images/thumbNailer.php?src=';

		if( true == defined( 'CONFIG_COMMON_PATH' ) ) {
			$strThumbnailerPath = CONFIG_COMMON_PATH . '/images/thumbNailer.php?src=';
		}

		// imageOptimizer Condition for converting images to webp format on the fly.
		if( true === defined( 'CONFIG_COMMON_PATH' ) && true === defined( 'CONFIG_LOAD_WEBP' ) ) {
			$strThumbnailerPath = CONFIG_COMMON_PATH . '/images/imageOptimizer.php?f=webp&src=';
		}

		if( true == $boolIsUseThumbnail && true == valStr( $this->getThumbnailUri() ) ) {
			$strThumbnailerPath .= $this->getThumbnailUri();
		} else {
			$strThumbnailerPath .= $this->getMarketingStorageTypeReference();
		}

		$strThumbnailerPath .= '&w=' . ( int ) $intWidth . '&h=' . ( int ) $intHeight;

		return $strThumbnailerPath;
	}

	public function getMediaCategoryName() {
		return $this->m_strMediaCategoryName;
	}

	public function getMediaCategoryId() {
		return $this->m_intMediaCategoryId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['media_category_id'] ) )		        $this->setMediaCategoryId( $arrstrValues['media_category_id'] );
		if( true == isset( $arrstrValues['media_category_name'] ) )		        $this->setMediaCategoryName( $arrstrValues['media_category_name'] );
	}

	public function setMediaCategoryName( $strMediaCategoryName ) {
		$this->m_strMediaCategoryName = $strMediaCategoryName;
	}

	public function setMediaCategoryId( $intMediaCategoryId ) {
		$this->m_intMediaCategoryId = $intMediaCategoryId;
	}

}
?>