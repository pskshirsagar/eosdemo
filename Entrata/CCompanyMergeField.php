<?php

class CCompanyMergeField extends CBaseCompanyMergeField {

	protected $m_strDefaultValue;
	protected $m_strMergeFieldValues;
	protected $m_strBlockName;
	protected $m_objOldCompanyMergeField;
	protected $m_objUpdatedByUser;
	protected $m_objCreatedByUser;
	protected $m_arrobjCompanyMergeFieldValues;
	protected $m_strHtmlInputType;
	protected $m_strDataType;

	protected $m_arrstrMergeFieldTypesOptions;

	protected $m_arrstrDataTypesOptions;

	const INPUT_TYPE_NUMBER 		    = 'number';
	const INPUT_TYPE_METHOD   	        = 'method';
	const INPUT_TYPE_DATE   	        = 'date';
	const INPUT_TYPE_CURRENCY  	        = 'currency';
	const INPUT_TYPE_MATH   	        = 'math';

	const INPUT_TYPE_BUTTON 	        = 'button';
	const INPUT_TYPE_CHECKBOX 		    = 'checkbox';
	const INPUT_TYPE_DROPDOWN 	        = 'dropdown';
	const INPUT_TYPE_FILE 		    	= 'file';
	const INPUT_TYPE_HIDDEN 		    = 'hidden';
	const INPUT_TYPE_IMAGE 		    	= 'image';
	const INPUT_TYPE_PASSWORD 		    = 'password';
	const INPUT_TYPE_RADIO 		    	= 'radio';
	const INPUT_TYPE_SUBMIT 		    = 'submit';
	const INPUT_TYPE_TEXT 		    	= 'text';
	const INPUT_TYPE_TEXTAREA   	    = 'textarea';

	const DATA_TYPE_CURRENCY  	        = 'currency';
	const DATA_TYPE_NUMERIC   	        = 'numeric';
	const DATA_TYPE_BOOLEAN  	        = 'boolean';
	const DATA_TYPE_DATETIME  	        = 'datetime';
	const DATA_TYPE_DATE                = 'date';
	const DATA_TYPE_HYPERLINK  	        = 'hyperlink';
	const DATA_TYPE_TEXT  	            = 'text';
	const DATA_TYPE_PHONE  	            = 'phone';

	const DATA_TYPE_EMAIL               = 'email';
	const DATA_TYPE_PERCENT             = 'percent';
	const DATA_TYPE_TAXID_MASKED        = 'taxid_masked';
	const DATA_TYPE_POSTAL_CODE         = 'postal_code';


	public static $c_strExcludeMergeFieldTypesForAddEdit = [
		CCompanyMergeField::INPUT_TYPE_IMAGE
	];

	public static $c_strExcludeDataTypesForAddEdit = [
		CCompanyMergeField::DATA_TYPE_BOOLEAN,
		CCompanyMergeField::DATA_TYPE_DATETIME,
		CCompanyMergeField::DATA_TYPE_DATE,
		CCompanyMergeField::DATA_TYPE_HYPERLINK,
		CCompanyMergeField::DATA_TYPE_TEXT,
		CCompanyMergeField::DATA_TYPE_PHONE,
		CCompanyMergeField::DATA_TYPE_EMAIL,
		CCompanyMergeField::DATA_TYPE_PERCENT,
		CCompanyMergeField::DATA_TYPE_TAXID_MASKED,
		CCompanyMergeField::DATA_TYPE_POSTAL_CODE,
	];

	public function getMergeFieldValues() {
		$arrstrValues = json_decode( json_encode( $this->getViewExpression() ), true )['values'];
		if( false == valArr( $arrstrValues ) ) {
			return NULL;
		}
        $strValues = array_reduce( $arrstrValues, function( $strCarry, $arrstrValue ){
            if( false == is_null( $strCarry ) ) $strCarry .= ', ';
            $strCarry .= $arrstrValue['value'];
            return $strCarry;
        }, NULL );

	    return $strValues;
	}

	public function addCompanyMergeFieldValue( $objCompanyMergeFieldValue ) {
		$this->m_arrobjCompanyMergeFieldValues[] = $objCompanyMergeFieldValue;
	}

	public function getCompanyMergeFieldValues() {
	    $arrstrReturn = json_decode( json_encode( $this->getViewExpression() ), true )['values'];
        if( false == valArr( $arrstrReturn ) ) return NULL;
        array_walk( $arrstrReturn, function( &$strValue, $strKey ){
	        $strValue['is_default'] = ( bool ) $strValue['is_default'];
        } );
        return $arrstrReturn;
	}

	public function getOldCompanyMergeField() {
		return $this->m_objOldCompanyMergeField;
	}

	public function getBlockName() {
		return $this->m_strBlockName;
	}

	public function getUpdatedByUser() {
		return $this->m_objUpdatedByUser;
	}

	public function getCreatedByUser() {
	    return $this->m_objCreatedByUser;
	}

	public function getHtmlInputType() {
		if( true == is_null( $this->m_strHtmlInputType ) ) {
			return $this->getDetailsField( 'html_input_type' );
		}
		return $this->m_strMarketingName;
	}

	public function setCompanyMergeFieldValues( $arrobjCompanyMergeFieldValues ) {
		$this->m_arrobjCompanyMergeFieldValues = $arrobjCompanyMergeFieldValues;
	}

	public function setOldCompanyMergeField( $objOldCompanyMergeField ) {
		$this->m_objOldCompanyMergeField = $objOldCompanyMergeField;
	}

	public function setBlockName( $strBlockName ) {
		$this->m_strBlockName = $strBlockName;
	}

	public function setUpdatedByUser( $objUpdatedByUser ) {
		$this->m_objUpdatedByUser = $objUpdatedByUser;
	}

	public function setCreatedByUser( $objCreatedByUser ) {
	    $this->m_objCreatedByUser = $objCreatedByUser;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrstrValues['merge_field_values'] ) && $boolDirectSet ) {
			$this->m_strMergeFieldValues = trim( $arrstrValues['merge_field_values'] );
		} elseif( isset( $arrstrValues['merge_field_values'] ) ) {
			$this->setMergeFieldValues( $arrstrValues['merge_field_values'] );
		}

		if( isset( $arrstrValues['default_value'] ) && $boolDirectSet ) {
			$this->m_strDefaultValue = trim( $arrstrValues['default_value'] );
		} elseif( isset( $arrstrValues['default_value'] ) ) {
			$this->setDefaultValue( $arrstrValues['default_value'] );
		}

		if( isset( $arrstrValues['block_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strBlockName', trim( stripcslashes( $arrstrValues['block_name'] ) ) );
		} elseif( isset( $arrstrValues['block_name'] ) ) {
			$this->setBlockName( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['block_name'] ) : $arrstrValues['block_name'] );
		}
	}

	public function setMergeFieldValues( $strMergeFieldValues ) {
		$this->m_strMergeFieldValues = $strMergeFieldValues;
	}

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName( $objDatabase, $boolIsCheckId = false ) {
		$boolIsValid = true;

		if( CMergeFieldType::DROPDOWN_MERGE_FIELD == $this->getMergeFieldTypeId() ) {
			$strPrfix			= \Psi\CStringService::singleton()->substr( $this->getName(), 0, 3 );
			$strMergeFieldName	= \Psi\CStringService::singleton()->substr( $this->getName(), 3 );
		} else {
			$strMergeFieldName	= $this->getName();
		}

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Undefined database object.' ) );
			return $boolIsValid;
		}

		if( CMergeFieldType::DROPDOWN_MERGE_FIELD == $this->getMergeFieldTypeId() ) {
			$objExistingCompanyMergeField = CCompanyMergeFields::fetchCompanyMergeFieldByNameByCid( $this->getName(), $this->getCid(), $objDatabase );

			if( true == $boolIsCheckId ) {
				if( true == valObj( $objExistingCompanyMergeField, 'CCompanyMergeField' ) && $this->getId() != $objExistingCompanyMergeField->getId() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Merge field "' . $this->getName() . '" already exists, please choose a different name.' ) );
				}
			} else {
				if( true == valObj( $objExistingCompanyMergeField, 'CCompanyMergeField' ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Merge field "' . $this->getName() . '" already exists, please choose a different name.' ) );
				}
			}

			if( false == valStr( $strMergeFieldName ) || ( CMergeFieldType::DROPDOWN_MERGE_FIELD == $this->getMergeFieldTypeId() && 'dd_' != $strPrfix ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please use a valid company merge field name, prefix with "dd_"' ) );
			}
		} else {
			if( true == valObj( $this->getOldCompanyMergeField(), 'CCompanyMergeField' ) ) {
				$strOldCompanyMergeFieldName = $this->getOldCompanyMergeField()->getName();
			}

			if( $this->getName() != $strOldCompanyMergeFieldName ) {
				$objExistingCompanyMergeField = CCompanyMergeFields::fetchCompanyMergeFieldByNameByCid( $this->getName(), $this->getCid(), $objDatabase );
			}

			if( true == valObj( $objExistingCompanyMergeField, 'CCompanyMergeField' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Merge field "' . $this->getName() . '" already exists, please choose a different name.' ) );
			}
		}

		$strRegEx = '/[^-\w]+/'; // Used to match if string contains invalid char. [VT]
		$strRegEx1 = '/^[^a-zA-Z]/'; // Used to match if first char. is not a valid char. true == preg_match( $strRegEx1, $this->getName() ) [VT]

		if( true == preg_match( $strRegEx, $strMergeFieldName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Merge code can\'t contain special character' ) ) );
		}

		if( true == preg_match( $strRegEx1, $strMergeFieldName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Merge code must contain alphnumeric value, and must start with alphabet.' ) ) );
		}

		return $boolIsValid;
	}

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMultiselect() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valName( $objDatabase );
        		break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName( $objDatabase, $boolIsCheckId = true );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intUserId );
    	$this->setDeletedOn( ' NOW() ' );
    	$this->setUpdatedBy( $intUserId );
    	$this->setUpdatedOn( ' NOW() ' );

    	if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
    		return true;
    	}
    }

    public function createCompanyMergeFieldValue() {
    	$objCompanyMergeFieldValue = new CCompanyMergeFieldValue();
    	$objCompanyMergeFieldValue->setCid( $this->getCid() );
    	$objCompanyMergeFieldValue->setCompanyMergeFieldId( $this->getId() );

    	return $objCompanyMergeFieldValue;
    }

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public  function getHtmlInputTypesOptions( $strKey = '' ) {

		$this->m_arrstrHtmlInputTypesOptions = [
			SELF::INPUT_TYPE_CHECKBOX   => __( 'Checkbox' ),
			SELF::INPUT_TYPE_DROPDOWN   => __( 'Dropdown' ),
			SELF::INPUT_TYPE_IMAGE      => __( 'Image' ),
			SELF::INPUT_TYPE_RADIO   => __( 'Radio' ),
			SELF::INPUT_TYPE_TEXT       => __( 'Text' ),
			SELF::INPUT_TYPE_TEXTAREA   => __( 'Text Area' )
		];

		return  ( ( true == valStr( $strKey ) ) ?  $this->m_arrstrHtmlInputTypesOptions[$strKey] : $this->m_arrstrHtmlInputTypesOptions );
	}

	public  function getMergeFieldTypesOptions( $strKey = '' ) {

		$this->m_arrstrMergeFieldTypesOptions = [
			CMergeFieldType::MATH_MERGE_FIELD       => __( 'Math' ),
			CMergeFieldType::DROPDOWN_MERGE_FIELD   => __( 'Drop Down' ),
		];

		return  ( ( true == valStr( $strKey ) ) ?  $this->m_arrstrMergeFieldTypesOptions[$strKey] : $this->m_arrstrMergeFieldTypesOptions );
	}

	public function getDataTypesOptions( $strKey = '' ) {
			$this-> m_arrstrDataTypesOptions = [
				SELF::DATA_TYPE_BOOLEAN         => __( 'Boolean' ),
				SELF::DATA_TYPE_CURRENCY        => __( 'Currency' ),
				SELF::DATA_TYPE_DATE            => __( 'Date' ),
				SELF::DATA_TYPE_DATETIME        => __( 'DateTime' ),
				SELF::DATA_TYPE_EMAIL           => __( 'Email' ),
				SELF::DATA_TYPE_HYPERLINK       => __( 'Hyperlink' ),
				SELF::DATA_TYPE_NUMERIC         => __( 'Numeric' ),
				SELF::DATA_TYPE_PERCENT         => __( 'Percent' ),
				SELF::DATA_TYPE_PHONE           => __( 'Phone' ),
				SELF::DATA_TYPE_TAXID_MASKED    => __( 'Taxid_Masked' ),
				SELF::DATA_TYPE_TEXT            => __( 'Text' ),
				SELF::DATA_TYPE_POSTAL_CODE     => __( 'Postal_Code' )
			];

		return ( ( true == valStr( $strKey ) ) ? $this-> m_arrstrDataTypesOptions[$strKey] : $this-> m_arrstrDataTypesOptions );
	}

}
?>