<?php

class CMarketingIntegrationLogType extends CBaseMarketingIntegrationLogType {

	const ILS_PORTAL           = 1;
	const FACEBOOK_MARKETPLACE = 2;
	const GOOGLE_MY_BUSINESS   = 3;
	const YEXT                 = 4;
	const GUEST_CARD           = 5;
	const POTENTIAL_LEADS      = 6;
	const FACEBOOK_LEAD_ADS    = 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadMarketingIntegrationLogType( $intInternetListingServiceId ) : int {
		$intMarketingIntegrationLogTypeId = NULL;

		switch( $intInternetListingServiceId ) {
			case CInternetListingService::FACEBOOK_MARKETPLACE:
				$intMarketingIntegrationLogTypeId = self::FACEBOOK_MARKETPLACE;
				break;

			case CInternetListingService::GOOGLE_MY_BUSINESS:
				$intMarketingIntegrationLogTypeId = self::GOOGLE_MY_BUSINESS;
				break;

			case CInternetListingService::YEXT_INTEGRATION:
				$intMarketingIntegrationLogTypeId = self::YEXT;
				break;

			case CInternetListingService::FACEBOOK_LEAD_ADS:
				$intMarketingIntegrationLogTypeId = self::FACEBOOK_LEAD_ADS;
				break;

			default:
				$intMarketingIntegrationLogTypeId = self::ILS_PORTAL;
				break;
		}

		return $intMarketingIntegrationLogTypeId;
	}

}
?>