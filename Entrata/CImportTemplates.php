<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportTemplates
 * Do not add any new functions to this class.
 */

class CImportTemplates extends CBaseImportTemplates {

	public static function fetchActiveImportTemplatesByCid( $intCid, $objDatabase, $boolIsIncludeCSVTemplate = false, $boolIsExcludeHybridTemplate = false, $boolIsIncludeAllHybridTemplate = false ) {
		$strWhere = '';

		if( false == $boolIsIncludeCSVTemplate ) {
			$strWhere .= ' AND import_type_id <> ' . CImportType::CSV;
		}

		if( true == $boolIsExcludeHybridTemplate ) {
			$strWhere .= ' AND ( import_type_id IN( ' . CImportType::UTILITY . ', ' . CImportType::CSV . ' ) OR import_data_type_id <> ' . CImportDataType::CHARGE_CODES . ' OR CAST( details as jsonb )->>\'renewal_offers_and_one_time_scheduled_charges_only\' <> \'true\' ) ';
		}

		if( true == $boolIsIncludeAllHybridTemplate ) {
			$strWhere .= ' AND import_type_id IN( ' . CImportType::UTILITY . ', ' . CImportType::CSV . ', ' . CImportDataType::CHARGE_CODES . ' ) order by id DESC';
		}

		return self::fetchImportTemplates( sprintf( ' SELECT * FROM import_templates WHERE cid = %d AND is_active = true' . $strWhere, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByImportTypeIdsByIntegrationDataBaseIdByCid( $arrintImportTypeIds, $intIntegrationDataBaseId, $intCid, $objDatabase, $arrintImportDataTypeIds = NULL, $boolIsMaintenanceProbelm = false ) {
		if( false == valArr( $arrintImportTypeIds ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == valArr( $arrintImportDataTypeIds ) ) {
			$strWhere = ' AND import_data_type_id IN( ' . implode( ',', $arrintImportDataTypeIds ) . ' )';
		}

		$strWhereCondition = '';
		if( true == $boolIsMaintenanceProbelm ) {
			$strWhereCondition = ' AND CAST( details as jsonb )->>\'property_migration_options\' ILIKE ( \'%' . CImportDataType::MAINTENANCE_PROBLEMS . '%\' )';
		}

		$strSql = 'SELECT
							*
						FROM
							import_templates
						WHERE
							cid = ' . ( int ) $intCid . '
							AND import_type_id IN ( ' . implode( ',', $arrintImportTypeIds ) . ' ) ' . $strWhere . '
							AND CAST( details as jsonb )->>\'integration_database_id\' = \'' . $intIntegrationDataBaseId . '\' ' . $strWhereCondition . '
							AND is_active = true
						ORDER BY
							id ASC';

		return self::fetchImportTemplates( $strSql, $objDatabase );
	}

	public static function fetchImportTemplatesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							*
						FROM
							import_templates
							WHERE
								cid = ' . ( int ) $intCid . '
								AND id IN ( ' . implode( ',', $arrintIds ) . ' )
							ORDER BY
								id ASC';

		return self::fetchImportTemplates( $strSql, $objDatabase );
	}

	public static function fetchCustomImportTemplatesByCid( $arrstrFilter, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strWhere = $strTableName = '';
		if( true == valStr( $arrstrFilter['import_data_type_ids'] ) ) {
			$strWhere .= ' AND it.import_data_type_id IN( ' . $arrstrFilter['import_data_type_ids'] . ' ) ';
		} else {
			$strWhere .= ' AND it.import_data_type_id IN( ' . implode( ',', [ CImportDataType::RESIDENT_INSURANCE, CImportDataType::CHARGE_CODES, CImportDataType::LEASING_AGENTS, CImportDataType::LEAD_SOURCES, CImportDataType::LEASE_TERMS, CImportDataType::PROPERTY_TYPE_MAPPING, CImportDataType::MOVE_OUT_REASONS, CImportDataType::PETS, CImportDataType::MAINTENANCE_PROBLEMS, CImportDataType::MAINTENANCE_LOCATIONS ] ) . ' ) ';
		}

		if( true == valStr( $arrstrFilter['template_id'] ) ) {
			$strWhere .= ' AND it.id = ' . $arrstrFilter['template_id'];
		}

		if( true == valStr( $arrstrFilter['import_type_ids'] ) ) {
			$strWhere .= ' AND it.import_type_id IN( ' . $arrstrFilter['import_type_ids'] . ' ) ';
		}

		if( true == valStr( $arrstrFilter['to_date'] ) && true == valStr( $arrstrFilter['from_date'] ) ) {
			$strWhere .= ' AND it.created_on::date >= \'' . $arrstrFilter['from_date'] . '\' AND it.created_on::date <= \'' . $arrstrFilter['to_date'] . '\'';
		}

		if( true == valStr( $arrstrFilter['property_ids'] ) && 'All' != $arrstrFilter['property_ids'] ) {
			$strWhere .= '  AND it.id::text = pp.value AND pp.property_id IN ( ' . $arrstrFilter['property_ids'] . ' ) ';
			$strTableName = '  , property_preferences pp ';
		}

		if( false == $arrstrFilter['show_archive'] ) {
			$strWhere .= ' AND is_active = true ';
		}

		$strSql = 'SELECT
							DISTINCT(it.*)
						FROM
							import_templates it ' . $strTableName . '
						WHERE
							it.cid = ' . ( int ) $intCid . $strWhere . ' 
							ORDER BY id DESC';

		return self::fetchImportTemplates( $strSql, $objDatabase );

	}

	public static function fetchImportTemplateByNameByImportTypeIdByCid( $strName, $intImportTypeId, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) || false == valId( $intImportTypeId ) ) return NULL;
		return self::fetchImportTemplate( sprintf( 'SELECT * FROM import_templates WHERE name = %s AND import_type_id = %d AND cid = %d LIMIT 1', '\'' . $strName . '\'', ( int ) $intImportTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportTemplateByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchImportTemplate( sprintf( 'SELECT * FROM import_templates WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportTemplateByImportDataTypeIdByCid( $intImportDataTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intImportDataTypeId ) ) return NULL;
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE import_data_type_id = %d AND cid = %d ', ( int ) $intImportDataTypeId, ( int ) $intCid ) . 'order by id DESC', $objDatabase );
	}

}
?>