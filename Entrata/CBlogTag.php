<?php

class CBlogTag extends CBaseBlogTag {

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Tag Name is required.' ) ) );
		} elseif( false == is_null( $this->getName() ) ) {
			$objWebsiteBlogTag = \Psi\Eos\Entrata\CBlogTags::createService()->fetchBlogTagsByNameByWebsiteIdByCid( $this->getName(), $this->m_intWebsiteId, $this->m_intCid, $objDatabase );

			if( true == is_object( $objWebsiteBlogTag ) && $objWebsiteBlogTag->getId() != $this->getId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Tag Name already exists.' ) ) );
				$boolIsValid	= false;
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setUrl( $strUrl ) {
		$this->m_strUrl = CStrings::strTrimDef( $strUrl, 256, NULL, true );
		$this->m_strUrl = preg_replace( '/(\s|-|\s-|-\s|_)+/m', '-', preg_replace( '/(^-|`|\^|\\|\[|\])+|[^0-9a-zA-z_\s\-]+/m', '', \Psi\CStringService::singleton()->strtolower( $this->m_strUrl ) ) );
	}

}
?>