<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCellProviders
 * Do not add any new functions to this class.
 */

class CCellProviders extends CBaseCellProviders {

	public static function fetchCellProvider( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCellProvider', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>