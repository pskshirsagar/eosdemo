<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliations
 * Do not add any new functions to this class.
 */

class CGlReconciliations extends CBaseGlReconciliations {

	public static function fetchGlReconciliationsByFormFilterByCid( $arrmixReconciliationsFilter, $intCid, $intPageNo = 0, $intPageSize = 25, $objClientDatabase ) {
		$arrstrWhere = [];

		if( true == valIntArr( $arrmixReconciliationsFilter['bank_account_ids'] ) ) {
			$arrstrWhere[] = 'AND gr.bank_account_id IN ( ' . sqlIntImplode( $arrmixReconciliationsFilter['bank_account_ids'] ) . ' ) ';
		}

		if( true == valStr( $arrmixReconciliationsFilter['reconciled_date'] ) && true == CValidation::validateDate( $arrmixReconciliationsFilter['reconciled_date'] ) ) {
			$arrstrWhere[] = 'AND gr.statement_date = \'' . date( 'Y-m-d', strtotime( $arrmixReconciliationsFilter['reconciled_date'] ) ) . '\' ';
		}

		if( true == is_numeric( $arrmixReconciliationsFilter['ending_balance'] ) ) {
			$arrstrWhere[] = 'AND gr.ending_amount = ' . ( float ) $arrmixReconciliationsFilter['ending_balance'];
		}

		if( true == valId( $arrmixReconciliationsFilter['gl_reconciliation_status_id'] ) ) {
			$arrstrWhere[] = 'AND gr.gl_reconciliation_status_type_id = ' . ( int ) $arrmixReconciliationsFilter['gl_reconciliation_status_id'];
		}

		if( true == valId( $arrmixReconciliationsFilter['bank_account_type_id'] ) ) {

			$strCondition = 'AND ba.is_credit_card_account = 0';

			if( 2 == $arrmixReconciliationsFilter['bank_account_type_id'] ) {
				$strCondition = ' AND ba.is_credit_card_account = 1';
			}

			$arrstrWhere[] = $strCondition;

		}

		$strSql = 'SELECT
						gr.id,
						gr.bank_account_id,
						gr.gl_reconciliation_status_type_id,
						gr.beginning_date,
						gr.statement_date,
						gr.beginning_amount,
						gr.ending_amount,
						gr.is_post_month,
						ba.account_name,
						gr.is_post_month,
						grst.name AS bank_reconciliation_status,
						ba.is_credit_card_account,
						CASE
							WHEN ' . CGlReconciliationStatusType::PAUSED . ' = gr.gl_reconciliation_status_type_id
							THEN \'Open\'
							WHEN ( ' . CGlReconciliationStatusType::RECONCILED . ' = gr.gl_reconciliation_status_type_id OR ' . CGlReconciliationStatusType::REOPENED . ' = gr.gl_reconciliation_status_type_id )
							THEN \'Completed\'
							ELSE \'Pending\'
						END AS gl_reconciliation_status_type
					FROM
						gl_reconciliations gr
						JOIN bank_accounts ba ON ( gr.cid = ba.cid AND gr.bank_account_id = ba.id )
						JOIN gl_reconciliation_status_types grst ON ( gr.gl_reconciliation_status_type_id = grst.id )
					WHERE
						gr.cid = ' . ( int ) $intCid . '
						AND gr.deleted_on IS NULL
						AND gr.is_system = 0
						' . implode( PHP_EOL . ' ', $arrstrWhere ) . '
					ORDER BY
						' . $arrmixReconciliationsFilter['sort_by'] . ' ' . $arrmixReconciliationsFilter['sort_dir'] . '
					OFFSET '
		          . ( int ) $intPageNo . '
					LIMIT '
		          . ( int ) $intPageSize;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedGlReconciliationsByCid( $intCid, $objPagination, $objClientDatabase, $objGlReconciliationsFilter = NULL, $arrintBankAccountIds = [] ) {

		$strOrderBy		= 'gr.statement_date DESC, gr.id DESC';
		$strCondition	= '';

		$arrstrSortBy	= [ 'statement_date', 'account_name', 'gl_reconciliation_status_type_id' ];

		if( true == valStr( $objGlReconciliationsFilter->getSortBy() ) && true == in_array( $objGlReconciliationsFilter->getSortBy(), $arrstrSortBy ) ) {
			$strOrderBy = $objGlReconciliationsFilter->getSortBy() . ' ' . $objGlReconciliationsFilter->getSortDirection() . ', gr.id ' . $objGlReconciliationsFilter->getSortDirection();
		}

		if( true == valObj( $objGlReconciliationsFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valArr( $objGlReconciliationsFilter->getBankAccountIds() ) ) {
				$strCondition .= ' AND gr.bank_account_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getBankAccountIds() ) . ' )';
			} else if( true == valIntArr( $arrintBankAccountIds ) ) {
				$strCondition .= ' AND gr.bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )';
			}

			if( true == valStr( $objGlReconciliationsFilter->getStatementDate() ) && true == CValidation::validateDate( $objGlReconciliationsFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gr.statement_date = \'' . date( 'Y-m-d', strtotime( $objGlReconciliationsFilter->getStatementDate() ) ) . '\'';
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getEndingAmount() ) ) {
				$strCondition .= ' AND gr.ending_amount = ' . ( float ) $objGlReconciliationsFilter->getEndingAmount();
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getGlReconciliationStatusTypeId() ) ) {
				$strCondition .= ' AND gr.gl_reconciliation_status_type_id = ' . ( int ) $objGlReconciliationsFilter->getGlReconciliationStatusTypeId();
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getIsSystem() ) ) {
				$strCondition .= ' AND gr.is_system = ' . ( int ) $objGlReconciliationsFilter->getIsSystem();
			}
		}

		$strSql = 'SELECT
						gr.*,
						ba.account_name,
						gr.is_post_month,
						grst.name AS bank_reconciliation_status
					FROM
						gl_reconciliations gr
						JOIN bank_accounts ba ON ( gr.cid = ba.cid AND gr.bank_account_id = ba.id )
						JOIN gl_reconciliation_status_types grst ON ( gr.gl_reconciliation_status_type_id = grst.id )
					WHERE
						gr.cid = ' . ( int ) $intCid . $strCondition . '
						AND gr.deleted_by IS NULL
						AND gr.deleted_on IS NULL
					ORDER BY ' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalPaginatedGlReconciliationsByCid( $intCid, $objClientDatabase, $objGlReconciliationsFilter = NULL, $arrintBankAccountIds = [] ) {

		$strCondition = '';

		if( true == valObj( $objGlReconciliationsFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valArr( $objGlReconciliationsFilter->getBankAccountIds() ) ) {
				$strCondition .= ' AND bank_account_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getBankAccountIds() ) . ' )';
			} else if( true == valIntArr( $arrintBankAccountIds ) ) {
				$strCondition .= ' AND bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )';
			}

			if( true == valStr( $objGlReconciliationsFilter->getStatementDate() ) && true == CValidation::validateDate( $objGlReconciliationsFilter->getStatementDate() ) ) {
				$strCondition .= ' AND statement_date = \'' . date( 'Y-m-d', strtotime( $objGlReconciliationsFilter->getStatementDate() ) ) . '\'';
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getEndingAmount() ) ) {
				$strCondition .= ' AND ending_amount = ' . ( float ) $objGlReconciliationsFilter->getEndingAmount();
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getGlReconciliationStatusTypeId() ) ) {
				$strCondition .= ' AND gl_reconciliation_status_type_id = ' . ( int ) $objGlReconciliationsFilter->getGlReconciliationStatusTypeId();
			}

			if( true == is_numeric( $objGlReconciliationsFilter->getIsSystem() ) ) {
				$strCondition .= ' AND is_system = ' . ( int ) $objGlReconciliationsFilter->getIsSystem();
			}
		}

		$strSql = 'SELECT
						COUNT( id )
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . $strCondition . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchMaxGlReconciliationNotByIdByBankAccountIdByCid( $intId, $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_system = 0
						AND id <> ' . ( int ) $intId . '
					ORDER BY
						statement_date DESC
					LIMIT 1';

		return self::fetchGlReconciliation( $strSql, $objClientDatabase );
	}

	public static function fetchLastGlReconciliationsByBankAccountIdByGlReconciliationStatusTypeIdByCid( $intBankAccountId, $intGlReconciliationStatusTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND gl_reconciliation_status_type_id = ' . ( int ) $intGlReconciliationStatusTypeId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					LIMIT 1';

		return self::fetchGlReconciliation( $strSql, $objClientDatabase );
	}

	public static function fetchGlReconciliationsByIdsByCid( $arrintGlReconciliationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlReconciliationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gr.*,
						ba.account_name AS bank_account_name
					FROM
						gl_reconciliations gr
						LEFT JOIN bank_accounts ba ON ( gr.cid = ba.cid AND gr.bank_account_id = ba.id )
					WHERE
						gr.cid = ' . ( int ) $intCid . '
						AND gr.id IN ( ' . implode( ',', $arrintGlReconciliationIds ) . ' )';

		return self::fetchGlReconciliations( $strSql, $objClientDatabase );
	}

	public static function fetchUnPausedGlReconciliationBankAccountIdsByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase, $intGlReconciliationId = NULL ) {

		if( false == valArr( $arrintBankAccountIds ) ) {
			return NULL;
		}

		// while editing first and last bank reconciliation we have to skip current gl reconcilation id
		$strCondition = '';
		if( true == valId( $intGlReconciliationId ) ) {
			$strCondition = 'AND id <> ' . ( int ) $intGlReconciliationId;
		}

		$strSql = 'SELECT
						DISTINCT( bank_account_id )
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
						AND gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::PAUSED . '
						AND is_system = 0
						AND deleted_by IS NULL
						AND deleted_on IS NULL ' .
						$strCondition;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlReconciliationsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchGlReconciliations( $strSql, $objClientDatabase );
	}

	public static function fetchGlReconciliationStatementDatesByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						( to_char( a.statement_date, \'Month DD, YYYY\') ) as statement_date
					FROM
						( SELECT
								statement_date
							FROM
								gl_reconciliations
							WHERE
								cid = ' . ( int ) $intCid . '
								AND bank_account_id = ' . ( int ) $intBankAccountId . '
								AND is_system = 0
								AND deleted_by IS NULL
								AND deleted_on IS NULL
							ORDER BY
								statement_date DESC ) a ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchNonSystemGlReconciliationsByBankAccountIdByNotIdByCid( $intBankAccountId, $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND id <> ' . ( int ) $intId . '
						AND is_system = 0
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchGlReconciliations( $strSql, $objClientDatabase );
	}

	public static function fetchEndingBalanceByBankAcccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ending_amount AS balance
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_system = 0
						AND ( gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::RECONCILED . '
								OR gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::REOPENED . ' )
					ORDER BY
						statement_date DESC,
						id DESC
					LIMIT 1';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return ( true == valArr( $arrstrData ) && true == is_numeric( $arrstrData[0]['balance'] ) ) ? $arrstrData[0]['balance'] : 0;
	}

	public static function fetchOpenGlReconciliationByBankAcccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intBankAccountId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::PAUSED . '
						AND is_system = 0
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					LIMIT 1';

		return self::fetchGlReconciliation( $strSql, $objClientDatabase );
	}

	public static function fetchLastGlReconciliationIdsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						MAX( id ) AS id
					FROM
						gl_reconciliations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::PAUSED . '
						AND is_system = 0
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					GROUP BY
						bank_account_id';

		$arrmixData = fetchData( $strSql, $objClientDatabase );
		$arrmixData = array_keys( ( array ) rekeyArray( 'id', $arrmixData ) );

		return $arrmixData;
	}

	public static function fetchGlReconciliationIdByBankAccountIdByStatementDateByCid( $intBankAccountId, $strStatementDate, $intCid, $objClientDatabase, $boolIncludeSystem = true, $boolIncludeDeleted = true ) {

		$strIncludeSystem = '';
		if( false == $boolIncludeSystem ) {
			$strIncludeSystem = ' AND gr.is_system = 0';
		}
		$strIncludeDeleted = '';
		if( false == $boolIncludeDeleted ) {
			$strIncludeDeleted = '
				AND gr.deleted_by IS NULL
				AND gr.deleted_on IS NULL';
		}

		$strSql = 'SELECT
						gr.id
					FROM
						gl_reconciliations gr
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gr.bank_account_id = ' . ( int ) $intBankAccountId . '
						' . $strIncludeSystem . '
						' . $strIncludeDeleted . '
						AND gr.statement_date = \'' . $strStatementDate . '\'';

		return self::fetchColumn( $strSql, 'id', $objClientDatabase );
	}

	public static function fetchNonSystemGlReconciliationDetailsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						ba.id as bank_account_id,
						ba.account_name,
						ba.check_account_number_masked,
						gr.id
					FROM
						gl_reconciliations gr
						JOIN bank_accounts ba ON ( gr.cid = ba.cid AND gr.bank_account_id = ba.id )
					WHERE
						gr.cid = ' . ( int ) $intCid . '
						AND gr.is_system = 0
						AND gr.deleted_by IS NULL
						AND gr.deleted_on IS NULL
						AND gr.bank_account_id = ' . ( int ) $intBankAccountId . ' ';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchSystemGlReconciliationByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intBankAccountId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliations gr
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . '
						AND gl_reconciliation_status_type_id = ' . CGlReconciliationStatusType::RECONCILED . '
						AND is_system = 1
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					LIMIT 1';

		return self::fetchGlReconciliation( $strSql, $objClientDatabase );
	}

}
?>