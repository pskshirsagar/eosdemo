<?php

class CGlSetting extends CBaseGlSetting {

	public function getFormattedPostMonth( $strPostMonth ) {
		if( true == is_null( $strPostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $strPostMonth );
	}

	/**
	 * Validate Function
	 */

	public function validate( $strAction, $objDatabase = NULL, $boolIsValidManagementFeeAccount = false ) {
		$boolIsValid = true;

		$objGlSettingValidator = new CGlSettingValidator();
		$objGlSettingValidator->setGlSetting( $this );

		$boolIsValid &= $objGlSettingValidator->validate( $strAction, $objDatabase, $boolIsValidManagementFeeAccount );

		return $boolIsValid;
	}

	public function formatPostMonth( $strPostDate ) {
		$strPostDate = trim( $strPostDate );
		$arrstrPostDate = explode( '/', $strPostDate );
		if( true == valArr( $arrstrPostDate ) && ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostDate ) ) ) {
			return $arrstrPostDate[0] . '/01/' . $arrstrPostDate[1];
		} else {
			return $strPostDate;
		}
	}

 	public function mapKeyValuePair( $objGlSetting, $arrmixGlAccounts, $arrobjArCodes ) {

 		$strKeyValuePair = '';

 		// re-indexing
 		foreach( $arrmixGlAccounts as $arrmixGlAccount ) {
 			$arrmixGlAccounts[$arrmixGlAccount['id']] = $arrmixGlAccount;
 		}

		if( true == ( $objGlSetting->getRetainedEarningsGlAccountId() ) && true == array_key_exists( $objGlSetting->getRetainedEarningsGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Retained Earnings GL Account::' . $arrmixGlAccounts[$objGlSetting->getRetainedEarningsGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getRetainedEarningsGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Retained Earnings GL Account::' . '~^~';
		}

		if( true == ( $objGlSetting->getMarketRentGlAccountId() ) && true == array_key_exists( $objGlSetting->getMarketRentGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Market Rent GL Account::' . $arrmixGlAccounts[$objGlSetting->getMarketRentGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getMarketRentGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Market Rent GL Account::' . '~^~';
		}
		if( true == ( $objGlSetting->getGainToLeaseGlAccountId() ) && true == array_key_exists( $objGlSetting->getGainToLeaseGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Gain To Lease GL Account::' . $arrmixGlAccounts[$objGlSetting->getGainToLeaseGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getGainToLeaseGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Gain To Lease GL Account::' . '~^~';
		}
		if( true == ( $objGlSetting->getLossToLeaseGlAccountId() ) && true == array_key_exists( $objGlSetting->getLossToLeaseGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Loss To Lease GL Account::' . $arrmixGlAccounts[$objGlSetting->getLossToLeaseGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getLossToLeaseGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Loss To Lease GL Account::' . '~^~';
		}
		if( true == ( $objGlSetting->getVacancyLossGlAccountId() ) && true == array_key_exists( $objGlSetting->getVacancyLossGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Vacancy Loss GL Account::' . $arrmixGlAccounts[$objGlSetting->getVacancyLossGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getVacancyLossGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Vacancy Loss GL Account::' . '~^~';
		}

		if( true == ( $objGlSetting->getApGlAccountId() ) && true == array_key_exists( $objGlSetting->getApGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'AP Account::' . $arrmixGlAccounts[$objGlSetting->getApGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objGlSetting->getApGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'AP Account::' . '~^~';
		}

		$strKeyValuePair .= 'Is Cash Basis::' . $objGlSetting->getIsCashBasis() . '~^~';

		$strKeyValuePair .= 'Auto Update Post Month::' . $objGlSetting->getArAutoAdvancePostMonth() . '~^~';

		if( true == ( $objGlSetting->getArAdvanceDay() ) ) {
			$strKeyValuePair .= 'AR Post Month Closing Day::' . $objGlSetting->getArAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'AR Post Month Closing Day::' . '~^~';
		}

		if( true == ( $objGlSetting->getApAdvanceDay() ) ) {
			$strKeyValuePair .= 'AP Post Month Closing Day::' . $objGlSetting->getApAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'AP Post Month Closing Day::' . '~^~';
		}

		if( true == ( $objGlSetting->getGlAdvanceDay() ) ) {
			$strKeyValuePair .= 'GL Post Month Closing Day::' . $objGlSetting->getGlAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'GL Post Month Closing Day::' . '~^~';
		}

		$strKeyValuePair .= 'Ignore Holidays And Weekends::' . $objGlSetting->getIgnoreHolidaysAndWeekends() . '~^~';

		$strKeyValuePair .= 'Activate GL Posting::' . $objGlSetting->getActivateStandardPosting() . '~^~';

		$strKeyValuePair .= 'Allow Edit Of Consolidate Box On Invoices::' . $objGlSetting->getAllowApConsolidation() . '~^~';

		// Temporarily commenting the code as we have removed the field gl_settings.post_dtf_with_ap_invoice
		// $strKeyValuePair .= 'AP Intercompany Posting Setting::' . $objGlSetting->getPostDtfWithApInvoice() . '~^~';

		return $strKeyValuePair;
	}

	public function log( $objOldGlSetting, $objNewGlSetting, $arrmixGlAccounts, $arrobjArCodes, $intCompanyUserId, $objClientDatabase ) {

		$arrGlSettingLogfields = [
												'RetainedEarningsGlAccountId',
												'MarketRentGlAccountId',
												'GainToLeaseGlAccountId',
												'LossToLeaseGlAccountId',
												'VacancyLossGlAccountId',
												'ApGlAccountId',
												'IsCashBasis',
												'ArAutoAdvancePostMonth',
												'ArAdvanceDay',
												'ApAdvanceDay',
												'GlAdvanceDay',
												'IgnoreHolidaysAndWeekends',
												'ActivateStandardPosting',
												'EditIsConsolidated',
												'ReturnFeeArCodeId'
		];

		$arrstrChangedGlSetting = compareObjects( $objNewGlSetting, $objOldGlSetting, $arrGlSettingLogfields );

		$intUpdateGlSettingCounter = 0;
		$intDeleteGlSettingCounter = 0;

		foreach( $arrstrChangedGlSetting as $strChangedGlSetting ) {
			if( true == isset( $strChangedGlSetting['new_value'] ) ) {
				$intUpdateGlSettingCounter++;
			} else {
				$intDeleteGlSettingCounter++;
			}
		}

		$strGlSettingDiscription = ( 1 >= $intUpdateGlSettingCounter && 0 == $intDeleteGlSettingCounter ) ? 'A company GL setting has been modified.' : 'Company GL settings have been modified.';

		if( 0 < $intDeleteGlSettingCounter && 0 == $intUpdateGlSettingCounter ) {
		 	$strGlSettingDiscription = ( 1 == $intDeleteGlSettingCounter ) ? ' A company GL setting has been deleted.' : 'Company GL settings have been deleted.';
		}

		if( 0 < $intUpdateGlSettingCounter && 0 < $intDeleteGlSettingCounter ) {
		 	$strGlSettingDiscription = ( 1 == $intUpdateGlSettingCounter && 1 == $intDeleteGlSettingCounter ) ? 'A company GL setting has been modified and deleted.' : 'Company GL settings have been modified and deleted.';
		}

		$strOldGlSettingsKeyValuePair = $this->mapKeyValuePair( $objOldGlSetting, $arrmixGlAccounts, $arrobjArCodes );
	 	$strNewGlSettingsKeyValuePair = $this->mapKeyValuePair( $objNewGlSetting, $arrmixGlAccounts, $arrobjArCodes );

		$objTableLog = new CTableLog();

		return $objTableLog->insert( $intCompanyUserId, $objClientDatabase, 'gl_settings', $objNewGlSetting->getId(), 'UPDATE', $objNewGlSetting->getCid(), $strOldGlSettingsKeyValuePair, $strNewGlSettingsKeyValuePair, $strGlSettingDiscription );
	}

}
?>