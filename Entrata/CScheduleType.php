<?php

class CScheduleType extends CBaseScheduleType {

	const DAILY					= 1;
	const WEEKLY				= 2;
	const MONTHLY				= 3;
	const DAY_OF_THE_MONTH		= 4;
	const YEARLY				= 5;
	const EVENT_TRIGGER			= 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function createScheduleType( $strScheduleDetails, $intScheduleTypeId = NULL ) {
		if( true == valStr( $strScheduleDetails ) ) {
			$arrmixScheduleDetails = json_decode( $strScheduleDetails, true );
		} else {
			$arrmixScheduleDetails = $strScheduleDetails;
		}
		$intScheduleTypeId = $intScheduleTypeId ?: $arrmixScheduleDetails['schedule_type_id'] ?: NULL;
		if( true == is_null( $intScheduleTypeId ) ) {
			return NULL;
		}

		$objScheduleType = NULL;
		switch( $intScheduleTypeId ) {
			case self::DAILY:
				$objScheduleType = new CDailyScheduleType();
				break;

			case self::WEEKLY:
				$objScheduleType = new CWeeklyScheduleType();
				break;

			case self::MONTHLY:
				$objScheduleType = new CMonthlyScheduleType();
				break;

			case self::DAY_OF_THE_MONTH:
				$objScheduleType = new CDayOfTheMonthScheduleType();
				break;

			case self::YEARLY:
				$objScheduleType = new CYearlyScheduleType();
				break;

			case self::EVENT_TRIGGER:
				$objScheduleType = new CEventTriggerScheduleType();
				break;

			default:
				// Invalid schedule type id was passed
				break;
		}

		// If a schedule type was constructed, pass the details to it for parsing
		if( false == is_null( $objScheduleType ) ) {
			if( true == valStr( $strScheduleDetails ) ) {
				foreach( json_decode( $strScheduleDetails ) as $strKey => $mixValue ) {
					$strSetter = 'set' . \Psi\Libraries\UtilInflector\CInflector::createService()->camelize( $strKey );
					if( true == method_exists( $objScheduleType, $strSetter ) ) {
						$objScheduleType->$strSetter( $mixValue );
					}
				}
			} else {
				$objScheduleType->parseDetails( $arrmixScheduleDetails );
			}
		}

		return $objScheduleType;
	}

}
?>