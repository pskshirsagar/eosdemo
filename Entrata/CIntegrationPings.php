<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see        \Psi\Eos\Entrata\CIntegrationPings
 * Do not add any new functions to this class.
 */

class CIntegrationPings extends CBaseIntegrationPings {

	public static function fetchCustomIntegrationPingByDate( $strDate, $objDatabase ) {
		$strSql = 'SELECT * FROM integration_pings WHERE to_char( ping_datetime, \'YYYY-MM-DD\' ) = \'' . $strDate . '\'';

		return self::fetchIntegrationPings( $strSql, $objDatabase, false );
	}

}

?>