<?php

class CCustomerPortalSetting extends CBaseCustomerPortalSetting {

	public function getPassword() {
		return $this->m_strPasswordEncrypted;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPasswordEncrypted = CStrings::strTrimDef( $strPassword, 240, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( $arrmixValues['password'] );
		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {

		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );

		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + CCustomer::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {
			$this->setLoginAttemptCount( 1 );
		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );

	}

}
?>