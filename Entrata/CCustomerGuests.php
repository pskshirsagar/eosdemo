<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerGuests
 * Do not add any new functions to this class.
 */

class CCustomerGuests extends CBaseCustomerGuests {

	public static function fetchCustomerGuestByIdByCustomerIdByCid( $intCustomerGuestId, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM customer_guests WHERE id = ' . ( int ) $intCustomerGuestId . ' AND customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerGuest( $strSql, $objDatabase );

	}

	public static function fetchCustomerGuestDetailByCustomerIdByCid( $intCustomerGuestId, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cg.id,
						cg.guest_name,
						cg.phone_number,
						cg.frequency,
						cg.week_days,
						cg.month_days,
						cg.start_date,
						cg.end_date,
						cg.start_time,
						cg.end_time,
						cg.custom_guest_type_name,
						cg.is_key_permitted,
						cg.is_parcel_pickup_permitted,
						cg.is_no_expiration,
						gt.id as guest_type_id,
						gt.name,
						gft.name as frequency_name,
						gft.id  as frequency_type_id
					FROM
						customer_guests cg
						JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
						JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
					WHERE
						cg.id = ' . ( int ) $intCustomerGuestId . '
						AND cg.customer_id = ' . ( int ) $intCustomerId . '
						AND cg.cid = ' . ( int ) $intCid;

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult[0];

	}

	public static function fetchUndeletedCustomerGuestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cg.*,
						gft.name as guest_frequency_type,
						CASE
							WHEN cg.custom_guest_type_name IS NOT NULL
							THEN cg.custom_guest_type_name
							ELSE gt.name
						END guest_type
					FROM
						customer_guests cg
						JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
						JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
					WHERE
						cg.lease_id = ' . ( int ) $intLeaseId . '
						AND cg.cid = ' . ( int ) $intCid . '
						AND cg.deleted_by IS NULL
					ORDER BY
						cg.created_on DESC';

		return self::fetchCustomerGuests( $strSql, $objDatabase );
	}

	public static function fetchTodaysCustomerGuestsWithCustomerDetailsByFilterByLeaseStatusTypeIdsByCid( $objCustomerServicesFilter, $arrintLeaseStatusTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $objCustomerServicesFilter->getPropertyIds() ) ) {
			return false;
		}

		$intPropertyId = ( true == valId( $objCustomerServicesFilter->getSelectedPropertyId() ) ? $objCustomerServicesFilter->getSelectedPropertyId() : current( $objCustomerServicesFilter->getPropertyIds() ) );

		if( true == valId( $intPropertyId ) ) {
			$strSearchByProperty = ' AND l.property_id = ' . $intPropertyId . ' ';
		} else {
			$strSearchByProperty = ' AND l.property_id IN ( ' . implode( ',', $objCustomerServicesFilter->getPropertyIds() ) . ' )';
		}

		$arrstrAndParameters = [];

		if( true == valObj( $objCustomerServicesFilter, 'CCustomerServicesFilter' ) && false == is_null( $objCustomerServicesFilter->getResidentName() ) ) {
			$arrstrExplodedSearch = explode( ' ', $objCustomerServicesFilter->getResidentName() );
			$arrstrExplodedSearch = array_filter( array_map( 'trim', $arrstrExplodedSearch ) );

			if( true == valArr( $arrstrExplodedSearch ) ) {
				foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
					if( false === strrchr( $strExplodedSearch, '\'' ) ) {
						$strFilteredExplodedSearch = $strExplodedSearch;
					} else {
						$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
					}
					$arrstrAndParameters[] = $strFilteredExplodedSearch;
				}
			}
		}

		$strSql = ' SELECT
							cg.*,
							gft.name as guest_frequency_type,
							CASE
								WHEN cg.custom_guest_type_name IS NOT NULL
								THEN cg.custom_guest_type_name
								ELSE gt.name
							END guest_type,
							func_format_unit_number( NULL, NULL, l.building_name, l.unit_number_cache ) AS bldg_unit,
							func_format_customer_name( c.name_first, c.name_last ) AS customer
						FROM
							customer_guests cg
							JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
							JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
							JOIN customers c ON ( c.cid = cg.cid AND c.id = cg.customer_id )
							JOIN cached_leases l ON ( l.cid = cg.cid AND l.id = cg.lease_id )
						WHERE
							cg.cid = ' . ( int ) $intCid . '
							' . $strSearchByProperty;

		if( true == valArr( $arrstrAndParameters ) ) {
			$intCount = \Psi\Libraries\UtilFunctions\count( $arrstrAndParameters );
			for( $intParameter = 0; $intParameter < $intCount; $intParameter++ ) {
				$strAndParameters = str_replace( ',', '', $arrstrAndParameters[$intParameter] );

				$strBuildingName     = $strAndParameters;
				$strBillToUnitNumber = $strAndParameters;
				if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {
					$arrstrUnitNumber    = explode( '-', $strAndParameters );
					$strBuildingName     = $arrstrUnitNumber[0];
					$strBillToUnitNumber = $arrstrUnitNumber[1];
				}

				$strSql .= ' AND ( ( c.name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  c.name_last ILIKE  E\'%' . $strAndParameters . '%\' OR l.unit_number_cache ILIKE  E\'%' . $strBillToUnitNumber . '%\' )
								AND ( c.name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  c.name_last ILIKE  E\'%' . $strAndParameters . '%\' OR l.unit_number_cache ILIKE  E\'%' . $strBillToUnitNumber . '%\' )
								AND ( c.name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  c.name_last ILIKE  E\'%' . $strAndParameters . '%\' OR l.unit_number_cache ILIKE  E\'%' . $strBillToUnitNumber . '%\'  ) ) ';
			}
		}

		if( false == is_null( $objCustomerServicesFilter->getOtherName() ) ) {
			$strSql .= ' AND ( lower( cg.guest_name ) LIKE E\'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerServicesFilter->getOtherName() ) ) ) . '%\' ) ';
		}

		$strSql .= 'AND l.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) ';

		$strSql .= ' AND cg.deleted_by IS NULL
					 AND cg.start_date <= \'' . date( 'm/d/Y' ) . '\'
					 AND ( cg.end_date >= \'' . date( 'm/d/Y' ) . '\' OR cg.end_date IS NULL )
					 AND ( CASE
							WHEN ( gft.id IN ( ' . CGuestFrequencyType::DAILY . ', ' . CGuestFrequencyType::WEEKLY . ', ' . CGuestFrequencyType::MONTHLY . ', ' . CGuestFrequencyType::WEEK_DAY . ', ' . CGuestFrequencyType::YEARLY . ' ) OR cg.is_no_expiration = 1 )
							THEN 1 = 1
							WHEN gft.id = ' . CGuestFrequencyType::NEVER . '
							THEN \'' . date( 'm/d/Y' ) . '\' = cg.start_date
						END )
					ORDER BY
							cg.created_on DESC';

		return self::fetchCustomerGuests( $strSql, $objDatabase );
	}

	public static function fetchCustomerGuestsDeatilsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cg.id,
						cg.guest_name,
						CASE
							WHEN cg.custom_guest_type_name IS NOT NULL
							THEN cg.custom_guest_type_name
							ELSE gt.name
						END guest_type
					FROM
						customer_guests cg
						JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
					WHERE
						cg.customer_id = ' . ( int ) $intCustomerId . '
						AND cg.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
					ORDER
						BY created_on DESC';

		return self::fetchCustomerGuests( $strSql, $objDatabase );
	}

	public static function fetchSimpleCustomerGuestDetailsById( $intCustomerGuestId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cg.*,
						 func_format_customer_name( c.name_first, c.name_last ) AS customer
					FROM
						customer_guests cg
						JOIN customers c ON ( cg.customer_id = c.id AND cg.cid = c.cid )
					WHERE
						cg.id = ' . ( int ) $intCustomerGuestId . '
						AND cg.cid = ' . ( int ) $intCid;

		return self::fetchCustomerGuest( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTodaysCustomerGuestsByPropertyIdByLeaseStatusTypeIdsByCid( $intPropertyId, $arrintLeaseStatusTypeIds, $intCid, $intPageNo, $intPageSize, $boolIsReturnCount = false, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( true == $boolIsReturnCount ) {
			$strSelectSql        = ' count( cg.id ) as count ';
			$strSelectOrderBySql = '';
		} else {
			$strSelectSql = '
									cg.*,
									gft.name as guest_frequency_type,
									CASE
										WHEN cg.custom_guest_type_name IS NOT NULL
										THEN cg.custom_guest_type_name
										ELSE util_get_translated( \'name\', gt.name, gt.details ) 
									END guest_type,
										func_format_unit_number( NULL, NULL, l.building_name, l.unit_number_cache ) AS bldg_unit,
										func_format_customer_name( c.name_first, c.name_last ) AS customer';

			$strSelectOrderBySql = ' ORDER BY created_on DESC ';
		}

		$strSql = ' SELECT
							' . $strSelectSql . '
						FROM
							customer_guests cg
							JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
							JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
							JOIN customers c ON ( c.cid = cg.cid AND c.id = cg.customer_id )
							JOIN cached_leases l ON ( l.cid = cg.cid AND l.id = cg.lease_id )
						WHERE
							cg.cid = ' . ( int ) $intCid . '
							AND l.property_id = ' . ( int ) $intPropertyId . '
							AND l.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
							AND cg.deleted_by IS NULL
							AND cg.start_date <= \'' . date( 'm/d/Y' ) . '\'
							AND ( cg.end_date >= \'' . date( 'm/d/Y' ) . '\' OR cg.end_date IS NULL )
							AND ( CASE
									WHEN ( gft.id IN ( ' . CGuestFrequencyType::DAILY . ', ' . CGuestFrequencyType::WEEKLY . ', ' . CGuestFrequencyType::MONTHLY . ', ' . CGuestFrequencyType::WEEK_DAY . ', ' . CGuestFrequencyType::YEARLY . ' ) OR cg.is_no_expiration = 1 ) 
									THEN 1 = 1
									WHEN gft.id = ' . CGuestFrequencyType::NEVER . '
									THEN \'' . date( 'm/d/Y' ) . '\' = cg.start_date
								END )
						 ' . $strSelectOrderBySql;

		$strSql .= ( false == $boolIsReturnCount ) ? 'OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit : '';

		if( false == $boolIsReturnCount ) {
			return self::fetchCustomerGuests( $strSql, $objDatabase );
		} else {
			$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );
			if( true == isset ( $arrintResponse ) ) {
				return $arrintResponse[0][count];
			}

			return 0;
		}
	}

	public static function fetchAllCustomerGuest( $intCustomerId, $intCid, $objDatabase, $intLeaseId = NULL ) {

		$strWhereClause = '';
		if( true == valId( $intLeaseId ) ) {
			$strWhereClause = ' AND cg.lease_id = ' . ( int ) $intLeaseId;
		}

		$strSql = 'SELECT
						cg.id,
						cg.guest_name,
						cg.frequency,
						cg.week_days,
						cg.month_days,
						cg.start_date,
						cg.end_date,
						cg.start_time,
						cg.end_time,
						cg.custom_guest_type_name,
						cg.is_key_permitted,
						cg.is_parcel_pickup_permitted,
						cg.phone_number,
						cg.is_no_expiration,
						gt.id as guest_type_id,
						gt.name,
						gft.name as frequency_name,
						gft.id  as frequency_type_id,
						CASE 
						WHEN cg.created_by=' . \CUser::ID_RESIDENT_PORTAL . ' OR cg.created_by=' . \CUser::ID_RESIDENT_PORTAL_APP . ' OR cg.created_by=' . \CUser::ID_RPWHITELABELEDAPP . ' 
						THEN \'You\'
						ELSE \'Property\'
					END AS created_by
					FROM
						customer_guests cg
						JOIN guest_types gt ON ( gt.id = cg.guest_type_id )
						JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
					WHERE
						cg.customer_id = ' . ( int ) $intCustomerId . $strWhereClause . '
						AND cg.cid = ' . ( int ) $intCid . '
						AND cg.deleted_by IS NULL
						AND cg.deleted_on IS NULL';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;

	}

	public static function fetchScheduledSmartGuestsForTodayByLeaseStatusTypeIdsByPropertyIdsByCid( $arrintLeaseStatusTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $strCustomDate = NULL ) : array {
		$strSql = 'SELECT 
						cg.id, 
						cl.cid,
						cl.property_id,
						cl.property_unit_id,
						cg.guest_name, 
						cg.phone_number,
						cg.frequency, 
						cg.guest_frequency_type_id, 
						gft.name frequency_name, 
						cg.start_date,
						cg.end_date, 
						cg.week_days, 
						cg.month_days, 
						cg.start_time, 
						cg.end_time,
						CASE 
							WHEN cg.start_time::TIME <= ( current_time + INTERVAL \'15 minutes\' ) AND cg.start_time::TIME >= ( current_time ) 
							THEN \'yes\' 
							WHEN cg.end_time::TIME <= current_time AND cg.end_time::TIME >= ( current_time - INTERVAL \'15 minutes\' ) 
							THEN \'no\' 
							ELSE \'\' 
						END scheduled_status
					FROM 
						customer_guests cg
						JOIN cached_leases cl ON ( cl.id = cg.lease_id AND cl.cid = cg.cid )
						JOIN guest_frequency_types gft ON ( gft.id = cg.guest_frequency_type_id )
					WHERE
						cg.cid = ' . ( int ) $intCid . '
						AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND (cg.start_date <= CURRENT_DATE AND (cg.end_date IS NULL OR CURRENT_DATE <= cg.end_date) )
						AND ( ( cg.start_time::TIME <= ( current_time + INTERVAL \'15 minutes\' ) AND cg.start_time::TIME >= ( current_time ) ) OR ( cg.end_time::TIME <= current_time AND cg.end_time::TIME >= ( current_time - INTERVAL \'15 minutes\' ) ) ) 
						AND (
							(
								' . CGuestFrequencyType::DAILY . ' = cg.guest_frequency_type_id
								AND cg.frequency IS NOT NULL
								AND (
									CURRENT_DATE = ANY(
										SELECT dd::date
										FROM generate_series( cg.start_date::timestamp, cg.end_date::timestamp, (cg.frequency::text || \' day\')::interval) dd
										WHERE ( ( CURRENT_DATE - interval \'1 week\') < dd AND dd < (CURRENT_DATE + interval \'1 week\') ) -- to get limited records for comparision
									)
								)
							)
							OR
							(
								' . CGuestFrequencyType::WEEKLY . ' = cg.guest_frequency_type_id
								AND cg.frequency IS NOT NULL
								AND (
									CURRENT_DATE = ANY(
										SELECT dd::date
										FROM generate_series( cg.start_date::timestamp, cg.end_date::timestamp, (cg.frequency::text || \' week\')::interval) dd
										WHERE 
											( cg.week_days IS NULL ) OR (EXTRACT( dow FROM dd )::VARCHAR = ANY( string_to_array( cg.week_days, \',\' ) ) )
											AND ( ( CURRENT_DATE - interval \'1 month\') < dd AND dd < (CURRENT_DATE + interval \'1 month\') ) -- to get limited records for comparision
									)
								)
							)
							OR
							(
								' . CGuestFrequencyType::MONTHLY . ' = cg.guest_frequency_type_id
								AND cg.frequency IS NOT NULL
								AND (
									CURRENT_DATE = ANY(
										SELECT 
											dd::date
										FROM 
											generate_series( cg.start_date::timestamp, cg.start_date::timestamp + ( cg.frequency::text || \' month\' )::interval, ( \'1 day\')::interval ) dd
										WHERE 
											( cg.month_days IS NULL ) OR ( EXTRACT( DAY FROM dd )::VARCHAR = ANY( string_to_array(cg.month_days, \',\' ) ) )
                                            AND ( 
                                                ( EXTRACT( MONTH FROM cg.start_date::DATE ) != EXTRACT( MONTH FROM current_date ) AND EXTRACT( MONTH FROM dd ) =  EXTRACT( MONTH FROM cg.start_date::DATE + ( cg.frequency::text || \' month\')::interval ) ) 
                                                OR ( EXTRACT( MONTH FROM cg.start_date::DATE ) = EXTRACT( MONTH FROM current_date ) AND EXTRACT( MONTH FROM dd ) = EXTRACT( MONTH FROM ( current_date ) ) 
                                                )
              )
                                            AND dd >= CURRENT_DATE -- to get limited records for comparision
									)
									OR (
									\'32\' = ANY( string_to_array( cg.month_days, \',\' ) ) AND ( CURRENT_DATE = ( date_trunc(\'MONTH\', CURRENT_DATE ) + INTERVAL \'1 MONTH - 1 day\' )::DATE ) --last day
									)
								)
							)
							OR
							(
								' . CGuestFrequencyType::WEEK_DAY . ' = cg.guest_frequency_type_id
								AND cg.frequency IS NOT NULL
								AND (
									CURRENT_DATE = ANY(
										SELECT dd::date
										FROM generate_series( cg.start_date::timestamp, cg.end_date::timestamp, (cg.frequency::text || \' day\')::interval ) dd
										WHERE ( cg.week_days IS NULL ) OR ( EXTRACT( dow FROM dd )::VARCHAR = ANY( string_to_array( cg.week_days, \',\' ) ) )
										AND ( ( CURRENT_DATE - interval \'1 month\') < dd AND dd < (CURRENT_DATE + interval \'1 month\') ) -- to get limited records for comparision
									)
								)
							)
							OR
							(
								' . CGuestFrequencyType::YEARLY . ' = cg.guest_frequency_type_id
								AND cg.frequency IS NOT NULL
								AND (
									CURRENT_DATE = ANY(
										SELECT dd::date
										FROM generate_series( cg.start_date::timestamp, cg.end_date::timestamp, (cg.frequency::text || \' year\')::interval ) dd
									)
								)
							)
							OR
							(
								' . CGuestFrequencyType::NEVER . ' = cg.guest_frequency_type_id
								AND (
									CURRENT_DATE = cg.start_date::date
								)
							)
						 )';

		if( true == valStr( $strCustomDate ) ) {
			$strSql = str_replace( 'CURRENT_DATE', '\'' . $strCustomDate . '\'::date', $strSql );
		}

		return ( array ) fetchData( $strSql, $objDatabase );
	}

}

?>