<?php

class CBudgetChangeOrderLog extends CBaseBudgetChangeOrderLog {

	const ACTION_CREATED		= 'Created';
	const ACTION_EDITED			= 'Edited';
	const ACTION_POSTED			= 'Posted';
	const ACTION_UNPOSTED		= 'Unposted';
	const ACTION_APPROVED		= 'Approved';
	const ACTION_UNAPPROVED		= 'Unapproved';
	const ACTION_DELETED		= 'Deleted';
	const ACTION_CANCELLED		= 'Cancelled';
	const ACTION_RETURN_TO_PREVIOUS		= 'Return to previous';
	const ACTION_RETURN_TO_BEGINNING	= 'Return to beginning';

	protected $m_strCompanyEmployeeFullName;

	public static $c_arrstrBudgetChangeOrderLogActions = [
		self::ACTION_CREATED			=> self::ACTION_CREATED,
		self::ACTION_EDITED				=> self::ACTION_EDITED,
		self::ACTION_APPROVED			=> self::ACTION_APPROVED,
		self::ACTION_UNAPPROVED			=> self::ACTION_UNAPPROVED,
		self::ACTION_DELETED			=> self::ACTION_DELETED,
		self::ACTION_CANCELLED			=> self::ACTION_CANCELLED,
		self::ACTION_RETURN_TO_PREVIOUS		=> self::ACTION_RETURN_TO_PREVIOUS,
		self::ACTION_RETURN_TO_BEGINNING	=> self::ACTION_RETURN_TO_BEGINNING
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetChangeOrderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrValues['company_employee_full_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strCompanyEmployeeFullName', trim( $arrValues['company_employee_full_name'] ) );
		} elseif( isset( $arrValues['company_employee_full_name'] ) ) {
			$this->setCompanyEmployeeFullName( $arrValues['company_employee_full_name'] );
		}
	}

	/**
	 * Get Functions
	 */

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	/**
	 * Other functions
	 */

	public static function assignTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_CREATED'] = self::ACTION_CREATED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_EDITED'] = self::ACTION_EDITED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_APPROVED'] = self::ACTION_APPROVED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_UNAPPROVED'] = self::ACTION_UNAPPROVED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_DELETED'] = self::ACTION_DELETED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_CANCELLED'] = self::ACTION_CANCELLED;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_RETURN_TO_PREVIOUS'] = self::ACTION_RETURN_TO_PREVIOUS;
		$arrmixTemplateParameters['CHANGE_ORDER_LOG_ACTION_RETURN_TO_BIGINNING'] = self::ACTION_RETURN_TO_BEGINNING;

		return $arrmixTemplateParameters;
	}

}
?>