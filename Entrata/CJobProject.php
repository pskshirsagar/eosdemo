<?php

class CJobProject extends CBaseJobProject {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityJobProjectTimingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceJobProjectTimingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>