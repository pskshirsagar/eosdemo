<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyOccupations
 * Do not add any new functions to this class.
 */

class CPropertyOccupations extends CBasePropertyOccupations {

	public static function fetchPublishedPropertyOccupationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolReturnCount = false ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return 0;
		}
			$strSql = 'SELECT 
					po.*,
					util_get_translated( \'name\', o.name, o.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as occupation_name
					FROM 
						property_occupations po, occupations o 
						WHERE 
							po.property_id::int=' . ( int ) $intPropertyId . ' AND 
							po.cid = ' . ( int ) $intCid . ' AND 
							po.occupation_id = o.id AND 
							po.cid = o.cid';

			if( false == $boolReturnCount ) {
				return self::fetchPropertyOccupations( $strSql, $objDatabase );
			} else {

				$arrmixValues = fetchData( $strSql, $objDatabase );
				if( true == valArr( $arrmixValues ) ) {
					return \Psi\Libraries\UtilFunctions\count( $arrmixValues );
				}

				return 0;
			}
	}

	public static function fetchPropertyOccupationByPropertyIdByOccupationIdsByCid( $intPropertyId, $arrintOccupationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintOccupationIds ) ) return NULL;

		$strSql = 'SELECT po.*,o.name as occupation_name FROM property_occupations po, occupations o WHERE po.occupation_id IN( ' . implode( ',', $arrintOccupationIds ) . ' ) AND po.occupation_id = o.id AND po.cid = o.cid AND po.property_id::int = ' . ( int ) $intPropertyId . ' AND po.cid::int = ' . ( int ) $intCid;
		return self::fetchPropertyOccupations( $strSql, $objDatabase );
	}

	public static function fetchPropertyOccupationByPropertyIdByOccupationIdByCid( $intPropertyId, $intOccupationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_occupations WHERE occupation_id = ' . ( int ) $intOccupationId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1 ';
		return self::fetchPropertyOccupation( $strSql, $objDatabase );
	}

	public static function fetchApplicantPropertyOccupationByApplicationIdByApplicantIdByOccupationIdByCid( $intPropertyId, $intApplicationId, $intApplicantId, $intOccupationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT po.*
						FROM applicant_applications aa, property_occupations po
						WHERE
							aa.cid = po.cid
							AND aa.application_id =' . ( int ) $intApplicationId . '
							AND aa.applicant_id =' . ( int ) $intApplicantId . '
							AND po.property_id = ' . ( int ) $intPropertyId . '
							AND po.occupation_id = ' . ( int ) $intOccupationId . '
							AND po.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		 return self::fetchPropertyOccupation( $strSql, $objDatabase );
	}

	public static function fetchAllPropertyOccupationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyOccupations( sprintf( 'SELECT * FROM property_occupations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}
}
?>