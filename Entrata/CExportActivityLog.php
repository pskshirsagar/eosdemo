<?php

class CExportActivityLog extends CBaseExportActivityLog {

	const EXPORT_BATCH_CREATE = 'Create';
	const EXPORT_BATCH_DELETE = 'Delete';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountingExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivityType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchSnapshotDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivityDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>