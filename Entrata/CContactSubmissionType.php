<?php

class CContactSubmissionType extends CBaseContactSubmissionType {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		 $boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			trigger_error( __( 'client id required.' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please add contact submission type.' ) ) );
		}

		if( true == $boolIsValid ) {
			$arrobjContactSubmissionTypes = CContactSubmissionTypes::fetchContactSubmissionTypesByNameByCid( $this->getName(), $this->getCid(), $objDatabase );

			if( false == is_null( $this->getId() ) && true == valArr( $arrobjContactSubmissionTypes ) ) {
				unset( $arrobjContactSubmissionTypes[$this->getId()] );
			}

			if( true == valArr( $arrobjContactSubmissionTypes ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'contact submission type already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>