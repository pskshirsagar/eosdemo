<?php

class CDefaultCustomerRelationship extends CBaseDefaultCustomerRelationship {

	const PRIMARY			= 1;
	const SPOUSE			= 2;
	const ROOMMATE			= 3;
	const CHILD				= 4;
	const OTHER				= 5;
	const GUARANTOR			= 6;
	const HUD_HEAD			= 7;
	const HUD_SPOUSE		= 8;
	const HUD_COHEAD		= 9;
	const HUD_DEPENDENT		= 10;
	const HUD_OTHER_ADULT	= 11;
	const HUD_FOSTER_CHILD	= 12;
	const HUD_LIVE_IN_AID	= 13;
	const HUD_NONE			= 14;
	const HUD_FOSTER_ADULT  = 37;

	const TIC_ADULT_CO_TENANT_A         = 'A';
	const HUD_HEAD_OF_HOUSEHOLD_H	= 'H';
	const HUD_SPOUSE_S				= 'S';
	const HUD_CO_HEAD_K				= 'K';
	const HUD_DEPENDENT_D			= 'D';
	const HUD_OTHER_ADULT_O			= 'O';
	const HUD_FOSTER_CHILD_F		= 'F';
	const HUD_LIVE_IN_ATTENDANT_L	= 'L';
	const HUD_NONE_N				= 'N';
	const HUD_FOSTER_ADULT_FA       = 'FA';

	public static $c_arrmixHudEligibleCustomerRelationshipCodes = [
		self::HUD_HEAD_OF_HOUSEHOLD_H,
		self::HUD_SPOUSE_S,
		self::HUD_CO_HEAD_K,
		self::HUD_OTHER_ADULT_O,
		self::HUD_FOSTER_CHILD_F,
		self::HUD_FOSTER_ADULT_FA
	];

	public static $c_arrmixHudIneligibleCustomerRelationshipCodes = [
		self::HUD_LIVE_IN_ATTENDANT_L,
		self::HUD_NONE_N
	];

	public static $c_arrmixHudDefaultCustomerRelationshipCodes = [
		self::HUD_HEAD			=> self::HUD_HEAD_OF_HOUSEHOLD_H,
		self::HUD_SPOUSE		=> self::HUD_SPOUSE_S,
		self::HUD_COHEAD		=> self::HUD_CO_HEAD_K,
		self::HUD_DEPENDENT		=> self::HUD_DEPENDENT_D,
		self::HUD_OTHER_ADULT	=> self::HUD_OTHER_ADULT_O,
		self::HUD_FOSTER_CHILD	=> self::HUD_FOSTER_CHILD_F,
		self::HUD_LIVE_IN_AID	=> self::HUD_LIVE_IN_ATTENDANT_L,
		self::HUD_NONE			=> self::HUD_NONE_N,
		self::HUD_FOSTER_ADULT  => self::HUD_FOSTER_ADULT_FA
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCustomerRelationshipGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowInternally() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowExternally() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>