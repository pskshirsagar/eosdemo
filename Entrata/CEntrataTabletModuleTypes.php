<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntrataTabletModuleTypes
 * Do not add any new functions to this class.
 */

class CEntrataTabletModuleTypes extends CBaseEntrataTabletModuleTypes {

    public static function fetchEntrataTabletModuleTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CEntrataTabletModuleType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchEntrataTabletModuleType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CEntrataTabletModuleType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>