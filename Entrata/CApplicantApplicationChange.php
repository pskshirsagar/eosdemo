<?php

class CApplicantApplicationChange extends CBaseApplicantApplicationChange {

	protected $m_strApplicantName;
	protected $m_strApplicantApplicationChangeType;
	protected $m_strUsername;

	/**
	 * Set Functions
	 */

	public function setApplicantName( $strApplicantName ) {
		$this->m_strApplicantName = $strApplicantName;
	}

	public function setApplicantApplicationChangeType( $strApplicantApplicationChangeType ) {
		$this->m_strApplicantApplicationChangeType = $strApplicantApplicationChangeType;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['applicant_name'] ) ) 						$this->setApplicantName( $arrmixValues['applicant_name'] );
		if( true == isset( $arrmixValues['applicant_application_change_type'] ) ) 	$this->setApplicantApplicationChangeType( $arrmixValues['applicant_application_change_type'] );
		if( true == isset( $arrmixValues['username'] ) )							$this->setUsername( $arrmixValues['username'] );

		return;
	}

	/**
	 * Get Functions
	 */

	public function getApplicantName() {
		return $this->m_strApplicantName;
	}

	public function getApplicantApplicationChangeType() {
		return $this->m_strApplicantApplicationChangeType;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantApplicationChangeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ); // TODO: Change the autogenerated stub
	}

}
?>