<?php

class CDefaultLateFeeFormula extends CBaseDefaultLateFeeFormula {

	const STANDARD			= 1;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLateFeePostTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFixedAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAmountPerDay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFixedPercent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPercentPerDay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFirstLateDay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDailyLateFeeStartDay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMinimumBalanceDue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxMonthlyLateFeePercent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxMonthlyLateFeeAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLegalFeeAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCurrentChargesOnly() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledChargesOnly() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseChargeOnAmountDue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAutoPostNsfLateFees() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostFeesToNextPeriod() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncludePastPeriodTransactions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowToApplyLegalFee() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valForgiveHolidays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valForgiveWeekends() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAutoPosted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeFirstDayLateFee() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeDailyLateFee() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDailyLateFeeEndDay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>