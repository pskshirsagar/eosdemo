<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeadCommunicationSchedules
 * Do not add any new functions to this class.
 */

class CLeadCommunicationSchedules extends CBaseLeadCommunicationSchedules {

	public static function fetchCustomLeadCommunicationSchedulesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						lead_communication_schedules
					WHERE
						cid = ' . ( int ) $intCid . '
					ORDER BY
						order_num ASC';

		return self::fetchLeadCommunicationSchedules( $strSql, $objDatabase );
	}
}
?>