<?php

class CImplementationChecklist extends CBaseImplementationChecklist {

	protected $m_intParentModuleId;

    /**
     * Get Functions
     */

    public function getParentModuleId() {
    	return $this->m_intParentModuleId;
    }

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['parent_module_id'] ) ) 	$this->setParentModuleId( $arrmixValues['parent_module_id'] );
    }

    public function setParentModuleId( $intParentModuleId ) {
    	$this->m_intParentModuleId = $intParentModuleId;
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsImplemented() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCompletionRecommended() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>