<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserDetails
 * Do not add any new functions to this class.
 */

class CCompanyUserDetails extends CBaseCompanyUserDetails {

	public static function fetchCompanyUserDetailByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
      	return self::fetchCompanyUserDetail( sprintf( 'SELECT * FROM %s WHERE company_user_id = %d  and cid = %d LIMIT 1', 'company_user_details', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
		// return self::fetchCompanyUserDetail( $strSql, $objDatabase );
    }
}
?>