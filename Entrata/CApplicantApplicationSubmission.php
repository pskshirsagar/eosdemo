<?php

class CApplicantApplicationSubmission extends CBaseApplicantApplicationSubmission {

	/**
	 * get Functions
	 */

    public function getApplicationSubmissionContent() {

		if( true == file_exists( $this->getFilePath() . $this->getFileName() ) && 0 < filesize( $this->getFilePath() . $this->getFileName() ) ) {
			$handle = fopen( $this->getFilePath() . $this->getFileName(), 'r' );
			$strHtmlContent = fread( $handle, filesize( $this->getFilePath() . $this->getFileName() ) );
			fclose( $handle );
	        return $strHtmlContent;
		} else {
			return NULL;
		}
    }

    public function valId() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApplicantApplicationId() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getApplicantApplicationId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_application_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApplicationSubmissionTypeId() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getApplicationSubmissionTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_submission_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFilePath() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getFilePath() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFileName() {
        $boolIsValid = true;

    /**
     * Validation example
     */

        // if( true == is_null( $this->getFileName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>