<?php

class CFileType extends CBaseFileType {

	protected $m_arrobjDocument;

	const SYSTEM_CODE_POLICY 										= 'POLICY';
	const SYSTEM_CODE_LEASE_ESA										= 'ESA';
	const SYSTEM_CODE_LEASE_PACKET									= 'LP';
	const SYSTEM_CODE_LEASE_ADDENDUM								= 'LA';
	const SYSTEM_CODE_PRE_SIGNED									= 'PRES';
	const SYSTEM_CODE_PARTIALLY_SIGNED								= 'PS';
	const SYSTEM_CODE_SIGNED										= 'SIGNED';
	const SYSTEM_CODE_OTHER_ESIGNED_PACKET							= 'OEP';
	const SYSTEM_CODE_OTHER_ESIGNED_LEASE							= 'OEL';
	const SYSTEM_CODE_LEASE_DOCUMENT								= 'LD';
	const SYSTEM_CODE_OTHER_DOCUMENT								= 'OTHER';
	const SYSTEM_CODE_INVOICE										= 'INVOICE';
	const SYSTEM_CODE_AP_PAYMENT_DOCUMENT							= 'APDOC';
	const SYSTEM_CODE_AP_PAYMENT_CHECK_FILE				            = 'CHECK';
	const SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE				        = 'AVID';
	const SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT				= 'CHECKCOPY';
	const SYSTEM_CODE_OFFER_TO_RENT									= 'OTR';
	const SYSTEM_CODE_PROOF_OF_INSURANCE							= 'POI';
	const SYSTEM_CODE_INSURANCE_CLAIM							    = 'IC';
	const SYSTEM_CODE_GUEST_CARD									= 'GC';
	const SYSTEM_CODE_MESSAGE_CENTER								= 'MESSAGE';
	const SYSTEM_CODE_APPLICATION									= 'APP';
	const SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT					= 'FMOS';
	const SYSTEM_CODE_UNIT_TRANSFER									= 'UTS';
	const SYSTEM_CODE_ADVERSE_ACTION_LETTER							= 'AAL';
	const SYSTEM_CODE_RENEWAL_OFFER_SENT							= 'ROS';
	const SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED						= 'ROA';
	const SYSTEM_CODE_LATE_NOTICE									= 'LTNTC';
	const SYSTEM_CODE_SENT_TO_COLLECTION							= 'STC';
	const SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT					= 'PCL';
	const SYSTEM_CODE_COLLECTIONS_NOTICE							= 'COLLECTNTC';
	const SYSTEM_CODE_QUOTE											= 'QUOTE';
	const SYSTEM_CODE_PROOF_OF_ELECTRICITY							= 'POE';
	const SYSTEM_CODE_REFUND_STATEMENT								= 'RS';
	const SYSTEM_CODE_MAINTENANCE_ATTACHMENT						= 'MA';
	const SYSTEM_CODE_INSPECTION_SIGNATURE							= 'IS';
	const SYSTEM_CODE_WORK_ORDER									= 'WO';
	const SYSTEM_CODE_WAITLIST										= 'WAIT';
	const SYSTEM_CODE_PARENTAL_CONSENT_FORM							= 'PCF';
	const SYSTEM_CODE_REPORT										= 'REPORT';
	const SYSTEM_CODE_NACHA											= 'NACHA';
	const SYSTEM_CODE_AVID											= 'AVID';
	const SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID						= 'CRP';
	const SYSTEM_CODE_MILITARY_DD_1746						        = 'MILDD1746';
	const SYSTEM_CODE_AR_TRANSACTION_ATTACHMENT						= 'ARTRANS';
	const SYSTEM_CODE_DOB_VERIFICATION								= 'DOBV';
	const SYSTEM_CODE_SSN_VERIFICATION								= 'SV';
	const SYSTEM_CODE_CITIZENSHIP_VERIFICATION						= 'CIV';
	const SYSTEM_CODE_VERIFICATION_DOCUMENT							= 'VED';
	const SYSTEM_CODE_PROOF_OF_INCOME								= 'POIN';
	const SYSTEM_CODE_PROOF_OF_EXPENSES								= 'POE';
	const SYSTEM_CODE_PROOF_OF_ASSETS								= 'POA';
	const SYSTEM_CODE_RV_ELECTRONIC_TRANSACTION_AGREEMENT			= 'RVETA';
	const SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION						= 'RVCA';
	const SYSTEM_CODE_REPAYMENT_AGREEMENT							= 'RA';
	const SYSTEM_CODE_VERIFICATION_FORM								= 'VF';
	const SYSTEM_CODE_PACKET										= 'PACKET';
	const SYSTEM_CODE_ADDENDUM										= 'ADDENDUM';
	const SYSTEM_CODE_SIGNATURE										= 'SIGNATURE';
	const SYSTEM_CODE_HUD50059										= 'HUD50059';
	const SYSTEM_CODE_TRIAL_HUD50059								= 'TRIAL_HUD50059';
	const SYSTEM_CODE_HUD50059A										= 'HUD50059A';
	const SYSTEM_CODE_HUD52670										= 'HUD52670';
	const SYSTEM_CODE_HUD52670A_PART1								= 'HUD52670A1';
	const SYSTEM_CODE_HUD52670A_PART2								= 'HUD52670A2';
	const SYSTEM_CODE_HUD52670A_PART3								= 'HUD52670A3';
	const SYSTEM_CODE_HUD52670A_PART4								= 'HUD52670A4';
	const SYSTEM_CODE_HUD52670A_PART5								= 'HUD52670A5';
	const SYSTEM_CODE_HUD52670A_PART6								= 'HUD52670A6';
	const SYSTEM_CODE_HUD52671A										= 'HUD52671A';
	const SYSTEM_CODE_HUD52671B										= 'HUD52671B';
	const SYSTEM_CODE_HUD52671C										= 'HUD52671C';
	const SYSTEM_CODE_HUD52671D										= 'HUD52671D';
	const SYSTEM_CODE_HUD90105A										= 'HUD90105A';
	const SYSTEM_CODE_HUD90105B										= 'HUD90105B';
	const SYSTEM_CODE_HUD90105C										= 'HUD90105C';
	const SYSTEM_CODE_HUD90105D										= 'HUD90105D';
	const SYSTEM_CODE_TEXAS_TIC										= 'TX_TIC';
	const SYSTEM_CODE_MICHIGAN_TIC									= 'MI_TIC';
	const SYSTEM_CODE_TENNESSEE_TIC									= 'TN_TIC';
	const SYSTEM_CODE_OREGON_TIC									= 'OR_TIC';
	const SYSTEM_CODE_MASSACHUSETTS_TIC								= 'MA_TIC';
	const SYSTEM_CODE_FLORIDA_TIC								    = 'FL_TIC';
	const SYSTEM_CODE_NEWJERSEY_TIC									= 'NJ_TIC';
	const SYSTEM_CODE_ARKANSAS_TIC									= 'AR_TIC';
	const SYSTEM_CODE_WASHINGTON_TIC								= 'WA_TIC';
	const SYSTEM_CODE_VIRGINIA_TIC                                  = 'VA_TIC';
	const SYSTEM_CODE_GEORGIA_TIC									= 'GA_TIC';
	const SYSTEM_CODE_MARYLAND_TIC									= 'MD_TIC';
	const SYSTEM_CODE_CALIFORNIA_TIC								= 'CA_TIC';
	const SYSTEM_CODE_RHODE_ISLAND_TIC								= 'RI_TIC';
	const SYSTEM_CODE_OKLAHOMA_TIC									= 'OK_TIC';
    const SYSTEM_CODE_LOUISIANA_TIC                                 = 'LA_TIC';
	const SYSTEM_CODE_KENTUCKY_TIC                                  = 'KY_TIC';
	const SYSTEM_CODE_MISSISSIPPI_TIC								= 'MS_TIC';
	const SYSTEM_CODE_MISSOURI_TIC								    = 'MO_TIC';
	const SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC					= 'MO_EX_TIC';
	const SYSTEM_CODE_SOUTH_CAROLINA_TIC							= 'SC_TIC';
	const SYSTEM_CODE_CALIFORNIA2_TIC							    = 'CA2_TIC';
	const SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC				= 'TX_SC_TIC';
	const SYSTEM_CODE_ILLINOIS_TIC									= 'IL_TIC';
	const SYSTEM_CODE_NEVADA_TIC							        = 'NV_TIC';
	const SYSTEM_CODE_CONNECTICUT_TIC								= 'CT_TIC';
	const SYSTEM_CODE_NORTH_CAROLINA_TIC							= 'NC_TIC';
	const SYSTEM_CODE_PENNSYLVANIA_TIC								= 'PA_TIC';
	const SYSTEM_CODE_IDAHO_TIC								        = 'ID_TIC';
	const SYSTEM_CODE_VERMONT_TIC                                   = 'VT_TIC';
    const SYSTEM_CODE_NEW_HAMPSHIRE_TIC                             = 'NH_TIC';
	const SYSTEM_CODE_OHIO_TIC                                      = 'OH_TIC';
	const SYSTEM_CODE_NEW_YORK_CITY_TIC								= 'NYC_TIC';
	const SYSTEM_CODE_UTAH_TIC								        = 'UT_TIC';
	const SYSTEM_CODE_WYOMING_TIC									= 'WY_TIC';
	const SYSTEM_CODE_ARIZONA_TIC								    = 'AZ_TIC';
	const SYSTEM_CODE_MONTANA_TIC								    = 'MT_TIC';
	const SYSTEM_CODE_PUERTO_RICO_TIC								= 'PR_TIC';
	const SYSTEM_CODE_MINNESOTA_TIC								    = 'MN_TIC';
	const SYSTEM_CODE_HAWAII_TIC								    = 'HI_TIC';
	const SYSTEM_CODE_SOUTH_DAKOTA_TIC							    = 'SD_TIC';
	const SYSTEM_CODE_NORTH_DAKOTA_TIC							    = 'ND_TIC';
	const SYSTEM_CODE_INDIANA_TIC								    = 'IN_TIC';
	const SYSTEM_CODE_WEST_VIRGINIA_TIC								= 'WV_TIC';
	const SYSTEM_CODE_VIRGIN_ISLANDS_TIC							= 'VI_TIC';
	const SYSTEM_CODE_COLORADO_TIC                                  = 'CO_TIC';
	const SYSTEM_CODE_IOWA_TIC                                      = 'IA_TIC';
	const SYSTEM_CODE_ALASKA_TIC                                    = 'AK_TIC';
	const SYSTEM_CODE_DELAWARE_TIC								    = 'DE_TIC';
	const SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC					    = 'DC_TIC';
	const SYSTEM_CODE_WISCONSIN_TIC 								= 'WI_TIC';
	const SYSTEM_CODE_ALABAMA_TIC								    = 'AL_TIC';
	const SYSTEM_CODE_NEW_YORK_TIC 									= 'NY_TIC';
	const SYSTEM_CODE_MAINE_TIC								        = 'ME_TIC';
	const SYSTEM_CODE_NEBRASKA_TIC								    = 'NE_TIC';
	const SYSTEM_CODE_KANSAS_TIC								    = 'KS_TIC';
	const SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE				= 'RE120DN';
	const SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE				= 'RE90DN';
	const SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE				= 'RE60DN';
	const SYSTEM_CODE_CORPORATE_JOB_DOCUMENT						= 'CJD';
	const SYSTEM_CODE_CORPORATE_AP_CONTRACT_DOCUMENT				= 'CAPD';
	const SYSTEM_CODE_LIEN_WAIVER									= 'LW';
	const SYSTEM_CODE_DRAW_REQUEST_DOCUMENT							= 'DRD';
	const SYSTEM_CODE_MOVE_IN_STATEMENT								= 'MIS';
	const SYSTEM_CODE_PET											= 'PET';
	const SYSTEM_CODE_PETIMAGE										= 'PETIMG';
	const SYSTEM_CODE_MOVE_IN										= 'MOVEIN';
	const SYSTEM_CODE_ADD_ON										= 'ADDON';
	const SYSTEM_CODE_ROOM_MATE										= 'ROOMMATE';
	const SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT	= 'DMUD';
	const SYSTEM_CODE_MOVE_IN_ADJUSTMENT							= 'MIA';
	const SYSTEM_CODE_PET_ADDENDUM									= 'PA';
	const SYSTEM_CODE_PROOF_OF_LAWFUL_PRESENCE						= 'POLP';
	const SYSTEM_CODE_EARLY_TERMINATION_AGREEMENT					= 'ETA';
	const SYSTEM_CODE_SMALL_BALANCE_REMINDER						= 'SMALLBAL';
	const SYSTEM_CODE_USER_DOCUMENT									= 'USRD';
	const SYSTEM_CODE_TENANT_STATEMENT								= 'TENSTMT';
	const SYSTEM_CODE_LEASE_ABSTRACT								= 'LEASEABST';
	const SYSTEM_CODE_RENEWAL_MONTH_CHARGES_STATEMENT				= 'RMCS';
	const SYSTEM_CODE_MILITARY_MIMO									= 'MILMIMO';
	const SYSTEM_CODE_MILITARY_PROMO_DEMO							= 'MILPRODEM';
	const SYSTEM_CODE_MILITARY_MAC_PAYMENT							= 'MILMACPMT';
	const SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_COL				= 'COL';
	const SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_NAHMA			= 'NAHMA';
	const SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_WBARS            = 'WBARS';
	const SYSTEM_CODE_LEASE_AGREEMENT								= 'LEASE';
	const SYSTEM_CODE_DELINQUENCY_EVENTS							= 'DQEVENTS';
	const SYSTEM_CODE_WFPM											= 'WFPM';
	const SYSTEM_CODE_SCREENING_DECISION_SUMMARY_LETTER             = 'SDSL';
	const SYSTEM_CODE_RENTAL_COLLECTION_PAYMENT_PROOF               = 'RCPP';
	const SYSTEM_CODE_COPY_1										= '1099COPY1';
	const SYSTEM_CODE_COPY_2										= '1099COPY2';
	const SYSTEM_CODE_COPY_B										= '1099COPYB';
	const SYSTEM_CODE_COPY_C										= '1099COPYC';
	const SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE		= 'TC120DN';
	const SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE		= 'TC90DN';
	const SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE		= 'TC60DN';
	const SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE		= 'TC30DN';
	const SYSTEM_CODE_MEXICO_TIC								    = 'MX_TIC';
	const SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE		    = 'HUD120DN';
	const SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE		    = 'HUD90DN';
	const SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE		    = 'HUD60DN';
	const SYSTEM_CODE_HUD91067		                                = 'HUD91067';

	const SYSTEM_CODE_SCREENING_AUTHORIZATION_AND_RIGHTS				= 'SAR';
	const SYSTEM_CODE_CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION = 'CAET';
	const SYSTEM_CODE_CONSUMER_AUTHORIZATION_CANADA						= 'CAC';
	const SYSTEM_CODE_UNKNOWN											= 'UNKNOWN';
	const SYSTEM_CODE_NSF											    = 'NSF';
	const SYSTEM_CODE_VIOLATION_MANAGER                                 = 'VM';
	const SYSTEM_CODE_LEASE_DATES_ADJUSTMENT                            = 'LDA';
	const SYSTEM_CODE_GCIC                                              = 'GCIC';
	const SYSTEM_CODE_MAT_FILE                                          = 'MAT_FILE';
	const SYSTEM_CODE_MAT_CERTS                                         = 'MAT_CERTS';
	const SYSTEM_CODE_MAT_BASE                                          = 'MAT_BASE';
	const SYSTEM_CODE_MAT_HAP                                           = 'MAT_HAP';
	const SYSTEM_CODE_MAT_ADDR                                          = 'MAT_ADDR';

	const PROOF_OF_LAWFUL_PRESENCE_NAME									= 'Proof Of Lawful Presence';

	const SYSTEM_CODE_SCREENING_REPORT                                  = 'SR';

	const SYSTEM_CODE_PERCENT_RENT_SALE                                 = 'PRS';
	const SYSTEM_CODE_CUSTOMER_INVOICE                                  = 'CI';
	const SYSTEM_CODE_MANAGEMENT_FEE_INVOICE							= 'MFI';
	const SYSTEM_CODE_JOURNAL_ENTRY_ATTACHMENT							= 'JEA';

	const SYSTEM_CODE_TRIPLE_NET_RECONCILIATION                         = 'NNNR';
	const SYSTEM_CODE_LEASE_CLAUSE_DOCUMENT                             = 'LEASECLAUS';
    const SYSTEM_CODE_APPLICATION_UPLOAD                                = 'APP_UPLOAD';
    const SYSTEM_CODE_CHAT                                              = 'CHAT';
	const SYSTEM_CODE_APPLICANT_IDENTIFICATION                          = 'APPID';
	const SYSTEM_CODE_CORPORATE_LETTER_OF_RESPONSIBILITY                = 'CLOR';
	const SYSTEM_CODE_PURCHASE_ORDER                                    = 'PO';
	const SYSTEM_CODE_GL_RECONCILIATION                                 = 'GL_REC';
	const SYSTEM_CODE_LEASE_ACCOMMODATION_ATTACHMENT					= 'LEASEACCM';

	const PATH_SENT_TO_COLLECTION				= 'sent_to_collections_statements/';
	const PATH_PRE_COLLECTION_LETTER_SENT		= 'sent_to_pre_collections_letter/';
	const PATH_FINANCIAL_MOVE_OUT_STATEMENT		= 'financial_move_out_statements/';
	const PATH_REFUND_STATEMENT					= 'refund_statements/';
	const PATH_WORK_ORDER						= 'work_orders/';
	const PATH_WAITLIST							= 'waitlist/';
	const PATH_LATE_NOTICE						= 'late_notices/';
	const PATH_AFFORDABLE_LEASE					= 'affordable/lease/';
	const PATH_AFFORDABLE_PROPERTY				= 'affordable/property/';
	const PATH_RENTAL_COLLECTION_PAYMENT_PROOF  = 'rental_collection_proof/';

//	const PATH_AFFORDABLE_ELECTRONIC_REPORTING	= PATH_MOUNTS_DOCUMENTS_AFFORDABLE . 'electronic_reporting/';
	// @FIXME: Once svn server is upgraded to PHP7, remove the below line and uncomment the above line.
	const PATH_AFFORDABLE_ELECTRONIC_REPORTING	= 'documents/affordable/electronic_reporting/';

	const PATH_MOVE_IN_STATEMENT				= 'move_in_statements/';
	const PATH_RENEWAL_MONTH_CHARGES_STATEMENT	= 'renewal_month_charges_statements/';
	const PATH_HUD50059                         = 'subsidy_certifications/';
	const PATH_HUD52670                         = 'subsidy_hap_requests/';
	const PATH_HUD52671A                        = 'special_claims/';
	const PATH_TENANT_STATEMENT					= 'tenant_statements/';
	const PATH_MILITARY_MIMO					= 'milmimo/';
	const PATH_TIC					            = 'TIC/';
	const PATH_MAT_FILE					        = 'MATFiles/';
	const PATH_MAT_HAP					        = 'MATHAP/';

	const PATH_LEASE_ACCOMMODATIONS				= 'lease_accommodations/';
	const PATH_TRIPLE_NET_RECONCILIATION		= 'nnnr/';
	const PATH_TENANT_LEASE_ABSTRACT			= 'LEASEABST/';
	const PATH_CUSTOMER_INVOICE			        = 'customer_invoices/';
	const PATH_LEASE_CLAUSE_DOCUMENT            = 'leaseclaus/';
	const PATH_MANAGEMENT_FEE_INVOICE			= 'management_fee_invoices/';
	const PATH_LEASE_DATES_ADJUSTMENT			= 'lease_dates_adjustments/';
	const PATH_MOVE_IN_ADJUSTMENT				= 'move_in_adjustments/';
	const PATH_EARLY_TERMINATION_AGREEMENT		= 'early_termination_agreements/';
	const PATH_INVOICE							= 'invoices/';
	const PATH_JOURNAL_ENTRY_ATTACHMENT			= 'journal_entry_attachments/';
	const PATH_PURCHASE_ORDER       			= 'purchase_orders/';
	const PATH_CORPORATE_JOB_DOCUMENT           = 'job_documents/';
	const PATH_CORPORATE_AP_CONTRACT_DOCUMENT   = 'ap_contract_documents/';
	const PATH_LIEN_WAIVER                      = 'lien_waivers/';
	const PATH_DRAW_REQUEST_DOCUMENT            = 'draw_request_documents/';
    const PATH_REPAYMENTS                       = 'repayments/';
    const PATH_AP_PAYMENT_DOCUMENT              = 'ap_payment_document/';
	const PATH_RECONCILIATIONS					= 'gl_reconciliations/';

    const SCREENING_AUTHORIZATION_AND_RIGHTS_NAME				    = 'Screening Authorization and Rights';
    const CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION_NAME    = 'Consumer Authorization and Electronic Transaction';
	const GEORGIA_CRIME_INFORMATION_CENTER_CONSENT_FORM_NAME        = 'Georgia Crime Information Center (GCIC)';
	const CONSUMER_AUTHORIZATION_CANADA_NAME                        = 'Consumer Authorization Canada';
	const SYSTEM_CODE_CAPITAL_ONE									= 'CAPITAL';
	const SYSTEM_CODE_SCREENING_TERMS_AND_CONDITIONS		        = 'STAC';

	const NAHMA_4_0  = '4_0';
	const NAHMA_5_0 = '5_0';

	public static $c_arrstrAffordableFileTypes = [ self::SYSTEM_CODE_HUD50059, self::SYSTEM_CODE_HUD50059A, self::SYSTEM_CODE_HUD52670, self::SYSTEM_CODE_HUD52670A_PART1, self::SYSTEM_CODE_HUD52670A_PART2, self::SYSTEM_CODE_HUD52670A_PART3, self::SYSTEM_CODE_HUD52670A_PART4, self::SYSTEM_CODE_HUD52670A_PART5, self::SYSTEM_CODE_HUD52670A_PART6, self::SYSTEM_CODE_HUD52671A, self::SYSTEM_CODE_HUD52671B, self::SYSTEM_CODE_HUD52671C, self::SYSTEM_CODE_HUD52671D, self::SYSTEM_CODE_HUD90105A, self::SYSTEM_CODE_HUD90105B, self::SYSTEM_CODE_HUD90105C, self::SYSTEM_CODE_HUD90105D, self::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE, self::SYSTEM_CODE_TEXAS_TIC, self::SYSTEM_CODE_MICHIGAN_TIC, self::SYSTEM_CODE_TENNESSEE_TIC, self::SYSTEM_CODE_OREGON_TIC, self::SYSTEM_CODE_MASSACHUSETTS_TIC, self::SYSTEM_CODE_WASHINGTON_TIC, self::SYSTEM_CODE_VIRGINIA_TIC, self::SYSTEM_CODE_FLORIDA_TIC, self::SYSTEM_CODE_NEWJERSEY_TIC, self::SYSTEM_CODE_ARKANSAS_TIC, self::SYSTEM_CODE_GEORGIA_TIC, self::SYSTEM_CODE_MARYLAND_TIC, self::SYSTEM_CODE_CALIFORNIA_TIC, self::SYSTEM_CODE_RHODE_ISLAND_TIC, self::SYSTEM_CODE_OKLAHOMA_TIC, self::SYSTEM_CODE_LOUISIANA_TIC, self::SYSTEM_CODE_KENTUCKY_TIC, self::SYSTEM_CODE_MISSISSIPPI_TIC, self::SYSTEM_CODE_MISSOURI_TIC, self::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC, self::SYSTEM_CODE_SOUTH_CAROLINA_TIC, self::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC, self::SYSTEM_CODE_CALIFORNIA2_TIC, self::SYSTEM_CODE_ILLINOIS_TIC, self::SYSTEM_CODE_NEVADA_TIC, self::SYSTEM_CODE_PENNSYLVANIA_TIC, self::SYSTEM_CODE_IDAHO_TIC, self::SYSTEM_CODE_VERMONT_TIC, self::SYSTEM_CODE_NEW_HAMPSHIRE_TIC, self::SYSTEM_CODE_OHIO_TIC, self::SYSTEM_CODE_CONNECTICUT_TIC, self::SYSTEM_CODE_NORTH_CAROLINA_TIC, self::SYSTEM_CODE_NEW_YORK_CITY_TIC, self::SYSTEM_CODE_UTAH_TIC, self::SYSTEM_CODE_WYOMING_TIC, self::SYSTEM_CODE_ARIZONA_TIC, self::SYSTEM_CODE_MONTANA_TIC, self::SYSTEM_CODE_PUERTO_RICO_TIC, self::SYSTEM_CODE_MINNESOTA_TIC, self::SYSTEM_CODE_HAWAII_TIC, self::SYSTEM_CODE_SOUTH_DAKOTA_TIC, self::SYSTEM_CODE_NORTH_DAKOTA_TIC, self::SYSTEM_CODE_INDIANA_TIC, self::SYSTEM_CODE_WEST_VIRGINIA_TIC, self::SYSTEM_CODE_VIRGIN_ISLANDS_TIC, self::SYSTEM_CODE_COLORADO_TIC, self::SYSTEM_CODE_IOWA_TIC, self::SYSTEM_CODE_ALASKA_TIC, self::SYSTEM_CODE_DELAWARE_TIC, self::SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC, self:: SYSTEM_CODE_WISCONSIN_TIC, self::SYSTEM_CODE_ALABAMA_TIC, self::SYSTEM_CODE_NEW_YORK_TIC, self::SYSTEM_CODE_MAINE_TIC, self::SYSTEM_CODE_NEBRASKA_TIC, self::SYSTEM_CODE_KANSAS_TIC, self::SYSTEM_CODE_MEXICO_TIC, self::SYSTEM_CODE_HUD91067 ];

	public static $c_arrstrTaxCreditCertificationFileTypes = [ self::SYSTEM_CODE_TEXAS_TIC, self::SYSTEM_CODE_MICHIGAN_TIC, self::SYSTEM_CODE_TENNESSEE_TIC, self::SYSTEM_CODE_OREGON_TIC, self::SYSTEM_CODE_MASSACHUSETTS_TIC, self::SYSTEM_CODE_WASHINGTON_TIC, self::SYSTEM_CODE_VIRGINIA_TIC, self::SYSTEM_CODE_NEWJERSEY_TIC, self::SYSTEM_CODE_FLORIDA_TIC, self::SYSTEM_CODE_ARKANSAS_TIC, self::SYSTEM_CODE_GEORGIA_TIC, self::SYSTEM_CODE_MARYLAND_TIC, self::SYSTEM_CODE_CALIFORNIA_TIC, self::SYSTEM_CODE_RHODE_ISLAND_TIC, self::SYSTEM_CODE_OKLAHOMA_TIC, self::SYSTEM_CODE_LOUISIANA_TIC, self::SYSTEM_CODE_KENTUCKY_TIC, self::SYSTEM_CODE_MISSISSIPPI_TIC, self::SYSTEM_CODE_MISSOURI_TIC, self::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC, self::SYSTEM_CODE_SOUTH_CAROLINA_TIC, self::SYSTEM_CODE_CALIFORNIA2_TIC, self::SYSTEM_CODE_SOUTH_CAROLINA_TIC, self::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC, self::SYSTEM_CODE_ILLINOIS_TIC, self::SYSTEM_CODE_NEVADA_TIC, self::SYSTEM_CODE_PENNSYLVANIA_TIC, self::SYSTEM_CODE_IDAHO_TIC, self:: SYSTEM_CODE_VERMONT_TIC, self:: SYSTEM_CODE_NEW_HAMPSHIRE_TIC, self::SYSTEM_CODE_OHIO_TIC, self::SYSTEM_CODE_CONNECTICUT_TIC, self::SYSTEM_CODE_NORTH_CAROLINA_TIC, self::SYSTEM_CODE_NEW_YORK_CITY_TIC, self::SYSTEM_CODE_UTAH_TIC, self::SYSTEM_CODE_WYOMING_TIC, self::SYSTEM_CODE_ARIZONA_TIC, self::SYSTEM_CODE_MONTANA_TIC, self::SYSTEM_CODE_PUERTO_RICO_TIC, self::SYSTEM_CODE_MINNESOTA_TIC, self::SYSTEM_CODE_HAWAII_TIC, self::SYSTEM_CODE_SOUTH_DAKOTA_TIC, self::SYSTEM_CODE_NORTH_DAKOTA_TIC, self::SYSTEM_CODE_INDIANA_TIC, self::SYSTEM_CODE_WEST_VIRGINIA_TIC, self::SYSTEM_CODE_VIRGIN_ISLANDS_TIC, self::SYSTEM_CODE_COLORADO_TIC, self::SYSTEM_CODE_IOWA_TIC, self::SYSTEM_CODE_ALASKA_TIC, self::SYSTEM_CODE_DELAWARE_TIC, self::SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC, self:: SYSTEM_CODE_WISCONSIN_TIC, self::SYSTEM_CODE_ALABAMA_TIC, self::SYSTEM_CODE_NEW_YORK_TIC, self::SYSTEM_CODE_MAINE_TIC, self::SYSTEM_CODE_NEBRASKA_TIC, self::SYSTEM_CODE_KANSAS_TIC, self::SYSTEM_CODE_MEXICO_TIC ];

	public static $c_arrstrTaxCreditFileTypes = [
		'TX' => self::SYSTEM_CODE_TEXAS_TIC,
		'MI' => self::SYSTEM_CODE_MICHIGAN_TIC
	];

	public static $c_arrstrHudCertificationFileTypes = [ self::SYSTEM_CODE_HUD50059, self::SYSTEM_CODE_HUD50059A, self::SYSTEM_CODE_TRIAL_HUD50059 ];

	public static $c_arrstrEvictionPacketFileTypes = [ self::SYSTEM_CODE_LEASE_PACKET, self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM, self::SYSTEM_CODE_LEASE_ESA, self::SYSTEM_CODE_OTHER_DOCUMENT, self::SYSTEM_CODE_LEASE_DOCUMENT, self::SYSTEM_CODE_PACKET, self::SYSTEM_CODE_ADDENDUM, self::SYSTEM_CODE_MOVE_IN_ADJUSTMENT, self::SYSTEM_CODE_PET_ADDENDUM, self::SYSTEM_CODE_OTHER_ESIGNED_LEASE, self::SYSTEM_CODE_LEASE_AGREEMENT ];


	public static $c_arrstrModelLeaseFileTypes = [ self::SYSTEM_CODE_HUD90105A, self::SYSTEM_CODE_HUD90105B, self::SYSTEM_CODE_HUD90105C, self::SYSTEM_CODE_HUD90105D, self::SYSTEM_CODE_HUD91067 ];

	public static $c_arrstrCancelledLeaseFileTypes = [ self::SYSTEM_CODE_PRE_SIGNED, self::SYSTEM_CODE_SIGNED, self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_OTHER_ESIGNED_LEASE, self::SYSTEM_CODE_LEASE_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM, self::SYSTEM_CODE_PARTIALLY_SIGNED ];

	public static $c_arrintDeletableLeaseFileTypes = [ self::SYSTEM_CODE_LEASE_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM, self::SYSTEM_CODE_PRE_SIGNED, self::SYSTEM_CODE_PARTIALLY_SIGNED, self::SYSTEM_CODE_SIGNED ];

	public static $c_arrintSigningDocumentFileType = [ self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_OTHER_ESIGNED_LEASE, self::SYSTEM_CODE_LEASE_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM ];

	public static $c_arrstrSeparateLeaseDocumentExcludedFileTypeSystemCodes = [ self::SYSTEM_CODE_LEASE_ESA, self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_PRE_SIGNED, self::SYSTEM_CODE_PARTIALLY_SIGNED, self::SYSTEM_CODE_SIGNED ];

	public static $c_arrstrDefaultExcludedLeaseDocumentFileTypeSystemCodes = [ self::SYSTEM_CODE_LEASE_ESA, self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM, self::SYSTEM_CODE_LEASE_PACKET ];

	public static $c_arrstrApplicantExcludedFileTypeSystemCodes = [ self::SYSTEM_CODE_LEASE_ESA, self::SYSTEM_CODE_OTHER_ESIGNED_PACKET, self::SYSTEM_CODE_LEASE_ADDENDUM ];

	public static $c_arrstrDocStorageFileTypes = [ self::SYSTEM_CODE_LEASE_ADDENDUM, self::SYSTEM_CODE_LEASE_DOCUMENT, self::SYSTEM_CODE_OTHER_ESIGNED_LEASE, self::SYSTEM_CODE_PARTIALLY_SIGNED, self::SYSTEM_CODE_PRE_SIGNED, self::SYSTEM_CODE_SIGNED, self::SYSTEM_CODE_APPLICATION, self::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT, self::SYSTEM_CODE_GUEST_CARD, self::SYSTEM_CODE_LATE_NOTICE, self::SYSTEM_CODE_MAINTENANCE_ATTACHMENT, self::SYSTEM_CODE_OFFER_TO_RENT, self::SYSTEM_CODE_OTHER_DOCUMENT, self::SYSTEM_CODE_AP_PAYMENT_DOCUMENT, self::SYSTEM_CODE_AP_PAYMENT_CHECK_FILE, self::SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE, self::SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT, self::SYSTEM_CODE_INVOICE, self::SYSTEM_CODE_PARENTAL_CONSENT_FORM, self::SYSTEM_CODE_POLICY, self::SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT, self::SYSTEM_CODE_PROOF_OF_ELECTRICITY, self::SYSTEM_CODE_PROOF_OF_INSURANCE, self::SYSTEM_CODE_REFUND_STATEMENT, self::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED, self::SYSTEM_CODE_RENEWAL_OFFER_SENT, self::SYSTEM_CODE_REPORT, self::SYSTEM_CODE_SENT_TO_COLLECTION, self::SYSTEM_CODE_UNIT_TRANSFER, self::SYSTEM_CODE_QUOTE, self::SYSTEM_CODE_ADVERSE_ACTION_LETTER, self::SYSTEM_CODE_WORK_ORDER, self::SYSTEM_CODE_WAITLIST, self::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID, self::SYSTEM_CODE_AR_TRANSACTION_ATTACHMENT, self::SYSTEM_CODE_MOVE_IN_ADJUSTMENT, self::SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION, self::SYSTEM_CODE_RV_ELECTRONIC_TRANSACTION_AGREEMENT, self::SYSTEM_CODE_REPAYMENT_AGREEMENT, self::SYSTEM_CODE_VERIFICATION_FORM, self::SYSTEM_CODE_UNKNOWN, self::SYSTEM_CODE_ADD_ON, self::SYSTEM_CODE_MOVE_IN, self::SYSTEM_CODE_PET_ADDENDUM, self::SYSTEM_CODE_PROOF_OF_LAWFUL_PRESENCE, self::SYSTEM_CODE_CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION, self::SYSTEM_CODE_USER_DOCUMENT, self::SYSTEM_CODE_RENEWAL_MONTH_CHARGES_STATEMENT, self::SYSTEM_CODE_NSF, self::SYSTEM_CODE_DELINQUENCY_EVENTS, self::SYSTEM_CODE_SMALL_BALANCE_REMINDER, self::SYSTEM_CODE_SCREENING_AUTHORIZATION_AND_RIGHTS, self::SYSTEM_CODE_EARLY_TERMINATION_AGREEMENT, self::SYSTEM_CODE_TENANT_STATEMENT, self::SYSTEM_CODE_SCREENING_DECISION_SUMMARY_LETTER, self::SYSTEM_CODE_PERCENT_RENT_SALE, self::SYSTEM_CODE_MANAGEMENT_FEE_INVOICE, self::SYSTEM_CODE_APPLICATION_UPLOAD, self::SYSTEM_CODE_VIOLATION_MANAGER, self::SYSTEM_CODE_CORPORATE_LETTER_OF_RESPONSIBILITY, self::SYSTEM_CODE_LEASE_DATES_ADJUSTMENT, self::SYSTEM_CODE_APPLICANT_IDENTIFICATION ];

	public static $c_arrstrDocStorageNonEditableFileTypes = [ CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID ];

	public static $c_arrstrNoticesFileTypes = [ self::SYSTEM_CODE_LATE_NOTICE, self::SYSTEM_CODE_SMALL_BALANCE_REMINDER, self::SYSTEM_CODE_COLLECTIONS_NOTICE ];

	public static $c_arrstrAffordableHudcertificationFileTypes = [ self::SYSTEM_CODE_HUD50059, self::SYSTEM_CODE_HUD50059A, self::SYSTEM_CODE_HUD52670, self::SYSTEM_CODE_HUD52670A_PART1, self::SYSTEM_CODE_HUD52670A_PART2, self::SYSTEM_CODE_HUD52670A_PART3, self::SYSTEM_CODE_HUD52670A_PART4, self::SYSTEM_CODE_HUD52670A_PART5, self::SYSTEM_CODE_HUD52670A_PART6, self::SYSTEM_CODE_HUD52671A, self::SYSTEM_CODE_HUD52671B, self::SYSTEM_CODE_HUD52671C, self::SYSTEM_CODE_HUD52671D, self::SYSTEM_CODE_TRIAL_HUD50059 ];

	public static $c_arrstrFileTypes1099 = [ self::SYSTEM_CODE_COPY_1, self::SYSTEM_CODE_COPY_2, self::SYSTEM_CODE_COPY_B, self::SYSTEM_CODE_COPY_C ];

	public static $c_arrintLeaseSigningFileTypes = [ self::SYSTEM_CODE_LEASE_ADDENDUM ];

	public static $c_arrstrTaxCreditRecertificationNotices = [ self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ];

	public static $c_arrstrCustomRecertificationNotices = [ self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE ];

	public static $c_arrstrCustomHudRecertificationNotices = [ self::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE ];

	public static $c_arrstrAffordableRecertificationNoticeFileTypes = [ self::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE, self::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE, self::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE ];

	public static $c_arrstrAffordableElectronicReportFileTypes = [ self::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_COL, self::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_NAHMA, self::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_WBARS ];

	public static $c_arrstrScreeningPolicyDocuments = [
		self::SYSTEM_CODE_CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION => self::CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION_NAME,
		self::SYSTEM_CODE_SCREENING_AUTHORIZATION_AND_RIGHTS => self::SCREENING_AUTHORIZATION_AND_RIGHTS_NAME,
		self::SYSTEM_CODE_CONSUMER_AUTHORIZATION_CANADA => self::CONSUMER_AUTHORIZATION_CANADA_NAME
	];

	public function addDocument( $objDocument ) {
		$this->m_arrobjDocument[$objDocument->getId()] = $objDocument;
	}

	public function getDocument() {
		return $this->m_arrobjDocument;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'SYSTEM_CODE_POLICY', self::SYSTEM_CODE_POLICY );
		$objSmarty->assign( 'SYSTEM_CODE_LEASE_ESA', self::SYSTEM_CODE_LEASE_ESA );
		$objSmarty->assign( 'SYSTEM_CODE_LEASE_PACKET', self::SYSTEM_CODE_LEASE_PACKET );
		$objSmarty->assign( 'SYSTEM_CODE_LEASE_ADDENDUM', self::SYSTEM_CODE_LEASE_ADDENDUM );
		$objSmarty->assign( 'SYSTEM_CODE_PRE_SIGNED', self::SYSTEM_CODE_PRE_SIGNED );
		$objSmarty->assign( 'SYSTEM_CODE_PARTIALLY_SIGNED', self::SYSTEM_CODE_PARTIALLY_SIGNED );
		$objSmarty->assign( 'SYSTEM_CODE_SIGNED', self::SYSTEM_CODE_SIGNED );
		$objSmarty->assign( 'SYSTEM_CODE_OTHER_ESIGNED_PACKET', self::SYSTEM_CODE_OTHER_ESIGNED_PACKET );
		$objSmarty->assign( 'SYSTEM_CODE_OTHER_ESIGNED_LEASE', self::SYSTEM_CODE_OTHER_ESIGNED_LEASE );
		$objSmarty->assign( 'SYSTEM_CODE_LEASE_DOCUMENT', self::SYSTEM_CODE_LEASE_DOCUMENT );
		$objSmarty->assign( 'SYSTEM_CODE_OTHER_DOCUMENT', self::SYSTEM_CODE_OTHER_DOCUMENT );
		$objSmarty->assign( 'SYSTEM_CODE_INVOICE', self::SYSTEM_CODE_INVOICE );
		$objSmarty->assign( 'SYSTEM_CODE_AP_PAYMENT_DOCUMENT', self::SYSTEM_CODE_AP_PAYMENT_DOCUMENT );
		$objSmarty->assign( 'SYSTEM_CODE_AP_PAYMENT_CHECK_FILE', self::SYSTEM_CODE_AP_PAYMENT_CHECK_FILE );
		$objSmarty->assign( 'SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE', self::SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE );
		$objSmarty->assign( 'SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT', self::SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT );
		$objSmarty->assign( 'SYSTEM_CODE_OFFER_TO_RENT', self::SYSTEM_CODE_OFFER_TO_RENT );
		$objSmarty->assign( 'SYSTEM_CODE_PROOF_OF_INSURANCE', self::SYSTEM_CODE_PROOF_OF_INSURANCE );
		$objSmarty->assign( 'SYSTEM_CODE_LEASE_CLAUSE_DOCUMENT', self::SYSTEM_CODE_LEASE_CLAUSE_DOCUMENT );
		$objSmarty->assign( 'SYSTEM_CODE_GUEST_CARD', self::SYSTEM_CODE_GUEST_CARD );
		$objSmarty->assign( 'SYSTEM_CODE_MESSAGE_CENTER', self::SYSTEM_CODE_MESSAGE_CENTER );
		$objSmarty->assign( 'SYSTEM_CODE_APPLICATION', self::SYSTEM_CODE_APPLICATION );
		$objSmarty->assign( 'SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT', self::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT );
		$objSmarty->assign( 'SYSTEM_CODE_UNIT_TRANSFER', self::SYSTEM_CODE_UNIT_TRANSFER );
		$objSmarty->assign( 'SYSTEM_CODE_ADVERSE_ACTION_LETTER', self::SYSTEM_CODE_ADVERSE_ACTION_LETTER );
		$objSmarty->assign( 'SYSTEM_CODE_RENEWAL_OFFER_SENT', self::SYSTEM_CODE_RENEWAL_OFFER_SENT );
		$objSmarty->assign( 'SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED', self::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED );
		$objSmarty->assign( 'SYSTEM_CODE_LATE_NOTICE', self::SYSTEM_CODE_LATE_NOTICE );
		$objSmarty->assign( 'SYSTEM_CODE_SENT_TO_COLLECTION', self::SYSTEM_CODE_SENT_TO_COLLECTION );
		$objSmarty->assign( 'SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT', self::SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT );
		$objSmarty->assign( 'SYSTEM_CODE_QUOTE', self::SYSTEM_CODE_QUOTE );
		$objSmarty->assign( 'SYSTEM_CODE_PROOF_OF_ELECTRICITY', self::SYSTEM_CODE_PROOF_OF_ELECTRICITY );
		$objSmarty->assign( 'SYSTEM_CODE_REFUND_STATEMENT', self::SYSTEM_CODE_REFUND_STATEMENT );
		$objSmarty->assign( 'SYSTEM_CODE_MAINTENANCE_ATTACHMENT', self::SYSTEM_CODE_MAINTENANCE_ATTACHMENT );
		$objSmarty->assign( 'SYSTEM_CODE_WORK_ORDER', self::SYSTEM_CODE_WORK_ORDER );
		$objSmarty->assign( 'SYSTEM_CODE_WAITLIST', self::SYSTEM_CODE_WAITLIST );
		$objSmarty->assign( 'SYSTEM_CODE_RV_ELECTRONIC_TRANSACTION_AGREEMENT', self::SYSTEM_CODE_RV_ELECTRONIC_TRANSACTION_AGREEMENT );
		$objSmarty->assign( 'SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION', self::SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION );
		$objSmarty->assign( 'SYSTEM_CODE_REPAYMENT_AGREEMENT', self::SYSTEM_CODE_REPAYMENT_AGREEMENT );
		$objSmarty->assign( 'SYSTEM_CODE_VERIFICATION_FORM', self::SYSTEM_CODE_VERIFICATION_FORM );
		$objSmarty->assign( 'SYSTEM_CODE_PACKET', self::SYSTEM_CODE_PACKET );
		$objSmarty->assign( 'SYSTEM_CODE_ADDENDUM', self::SYSTEM_CODE_ADDENDUM );
		$objSmarty->assign( 'SYSTEM_CODE_SIGNATURE', self::SYSTEM_CODE_SIGNATURE );
		$objSmarty->assign( 'SYSTEM_CODE_MOVE_IN_STATEMENT', self::SYSTEM_CODE_MOVE_IN_STATEMENT );
		$objSmarty->assign( 'SYSTEM_CODE_SMALL_BALANCE_REMINDER', self::SYSTEM_CODE_SMALL_BALANCE_REMINDER );
		$objSmarty->assign( 'SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID', self::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID );
		$objSmarty->assign( 'SYSTEM_CODE_COLLECTIONS_NOTICE', self::SYSTEM_CODE_COLLECTIONS_NOTICE );
		$objSmarty->assign( 'SYSTEM_CODE_MANAGEMENT_FEE_INVOICE', self::SYSTEM_CODE_MANAGEMENT_FEE_INVOICE );
	}

 	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) || 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Document Type Name is required.' ) ) );
			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {

			$intConfilictingFileTypeCount = CFileTypes::fetchConflictingNameCount( $this, $objDatabase );

			if( 0 < $intConfilictingFileTypeCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Document Type Name is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolSoftDelete = false ) {

		if( false == $boolSoftDelete ) {
			return parent::delete( $intUserId, $objDatabase );
		}

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		if( $this->update( $intUserId, $objDatabase ) ) {
			return true;
		}
	}

}
?>