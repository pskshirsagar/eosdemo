<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitKeyValues
 * Do not add any new functions to this class.
 */

class CUnitKeyValues extends CBaseUnitKeyValues {

	public static function fetchCustomUnitKeyValuesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM unit_key_values WHERE property_unit_id = ' . ( int ) $intPropertyUnitId . ' AND cid = ' . ( int ) $intCid;

		$arrobjUnitKeyValues = self::fetchUnitKeyValues( $strSql, $objDatabase );

		// We need to rekey array by key
		$arrobjRekeyedUnitKeyValues = [];
		if( true == valArr( $arrobjUnitKeyValues ) ) {
			foreach( $arrobjUnitKeyValues as $objUnitKeyValue ) {
				$arrobjRekeyedUnitKeyValues[$objUnitKeyValue->getKey()] = $objUnitKeyValue;
			}
		}

		return $arrobjRekeyedUnitKeyValues;
	}

	public static function fetchUnitKeyValuesByPropertyUnitIdByCidByKey( $intPropertyUnitId, $intCid, $strKey, $objDatabase ) {
		$strSql = 'SELECT * FROM unit_key_values WHERE property_unit_id = ' . ( int ) $intPropertyUnitId . ' AND cid = ' . ( int ) $intCid . ' AND key = \'' . trim( $strKey ) . '\'';
		return self::fetchUnitKeyValue( $strSql, $objDatabase );
	}

	public static function fetchUnitKeyValuesByPropertyUnitIdsByKeyByCid( $arrintPropertyUnitIds, $intCid, $strKey, $objDatabase ) {
		if( false == valArr( $arrintPropertyUnitIds ) || false == valId( $intCid ) || false == valStr( $strKey ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM unit_key_values WHERE property_unit_id IN( ' . implode( ',', $arrintPropertyUnitIds ) . ') AND cid = ' . ( int ) $intCid . ' AND key = \'' . trim( $strKey ) . '\'';

		return self::fetchUnitKeyValues( $strSql, $objDatabase );
	}

}
?>