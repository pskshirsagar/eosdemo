<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryAssistanceTypes
 * Do not add any new functions to this class.
 */

class CMilitaryAssistanceTypes extends CBaseMilitaryAssistanceTypes {

	public static function fetchMilitaryAssistanceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryAssistanceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchMilitaryAssistanceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryAssistanceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedMilitaryAssistanceTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM military_assistance_types WHERE is_published=true order by order_num';
		return self::fetchMilitaryAssistanceTypes( $strSql, $objDatabase );
	}

}
?>