<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseChecklists
 * Do not add any new functions to this class.
 */

class CLeaseChecklists extends CBaseLeaseChecklists {

	public static function fetchCompletedLeaseChecklistByLeaseIdByLeaseIntervalIdByChecklistTriggerIdByPropertyIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistTrigger, $intPropertyId, $intCid, $objDatabase, $boolIsReturnBoolean = false ) {

		$strSelect = ( true == $boolIsReturnBoolean ? ' CASE WHEN count( lc.id ) > 0 THEN 1 ELSE 0 END is_lease_checklist_completed ' : ' lc.* ' );

		$strSql = 'SELECT ' . $strSelect . '
                    FROM lease_checklists lc
						JOIN checklists c ON ( c.cid = lc.cid AND c.id = lc.checklist_id AND c.checklist_trigger_id = ' . ( int ) $intChecklistTrigger . ' AND c.deleted_by IS NULL )
						JOIN property_checklists pc ON ( c.cid = pc.cid AND c.id = pc.checklist_id AND pc.property_id = ' . ( int ) $intPropertyId . ' )
					WHERE lc.cid = ' . ( int ) $intCid . ' 
						AND lc.lease_id = ' . ( int ) $intLeaseId . ' 
						AND lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' 
						AND lc.completed_on IS NOT NULL';

		if( true == $boolIsReturnBoolean ) {
			return self::fetchColumn( $strSql, 'is_lease_checklist_completed', $objDatabase );
		} else {
			return parent::fetchLeaseChecklist( $strSql, $objDatabase );
		}
	}

	public static function fetchIsCompletedAllItemsByLeaseIdByLeaseIntervalIdByChecklistTriggerIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistTriggerId, $intCid, $objDatabase, $boolIsShowOnPortal = false ) {

		$strWhereClause = ( true == $boolIsShowOnPortal ) ? ' AND ci.show_on_portals = true' : '';
		$strSql = 'SELECT CASE
         					WHEN count(ci.id) = count(lci.id) THEN 1
         					ELSE 0
       					END as is_all_items_completed
					FROM checklist_items ci
     					JOIN checklists c ON (c.cid = ci.cid AND c.id = ci.checklist_id AND
       						c.deleted_by IS NULL)
     					JOIN lease_checklists lc ON (lc.cid = ci.cid AND lc.checklist_id =
       						ci.checklist_id AND lc.checklist_id = c.id)
     					LEFT JOIN lease_checklist_items lci ON (ci.cid = lc.cid AND lc.id =
       						lci.lease_checklist_id AND ci.id = lci.checklist_item_id AND
       						lc.checklist_id = ci.checklist_id)
					WHERE lc.cid = ' . ( int ) $intCid . ' AND
      					lc.lease_id = ' . ( int ) $intLeaseId . ' AND
      					lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' AND
      					c.checklist_trigger_id = ' . ( int ) $intChecklistTriggerId . ' AND
      					ci.deleted_by IS NULL AND
      					ci.deleted_on IS NULL
      					' . $strWhereClause . '
					GROUP BY lc.cid,
         					lc.id';

		return self::fetchColumn( $strSql, 'is_all_items_completed', $objDatabase );
	}

	public static function fetchAllChecklistByLeaseIdByLeaseIntervalIdByChecklistTriggerIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistTriggerId, $intCid, $objDatabase, $boolIsShowOnPortal = false ) {

		$strWhereClause = ( true == $boolIsShowOnPortal ) ? ' AND ci.show_on_portals = true' : '';
		$strSql = 'SELECT c.id
					FROM checklist_items ci
     					JOIN checklists c ON (c.cid = ci.cid AND c.id = ci.checklist_id AND
       						c.deleted_by IS NULL)
     					JOIN lease_checklists lc ON (lc.cid = ci.cid AND lc.checklist_id =
       						ci.checklist_id AND lc.checklist_id = c.id)
     					LEFT JOIN lease_checklist_items lci ON (ci.cid = lc.cid AND lc.id =
       						lci.lease_checklist_id AND ci.id = lci.checklist_item_id AND
       						lc.checklist_id = ci.checklist_id)
					WHERE lc.cid = ' . ( int ) $intCid . ' AND
      					lc.lease_id = ' . ( int ) $intLeaseId . ' AND
      					lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' AND
      					c.checklist_trigger_id = ' . ( int ) $intChecklistTriggerId . ' AND
      					ci.deleted_by IS NULL AND
      					ci.deleted_on IS NULL
      					' . $strWhereClause . ' limit 1 ';
		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchCompletedLeaseChecklistsByLeaseIdByLeaseIntervalIdByChecklistTriggerIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistTrigger, $intCid, $objDatabase ) {

		$strSql = 'SELECT lc.* 
                    FROM lease_checklists lc 
						JOIN checklists c ON ( c.cid = lc.cid AND c.id = lc.checklist_id AND c.checklist_trigger_id = ' . ( int ) $intChecklistTrigger . ' AND c.deleted_by IS NULL ) 
					WHERE lc.cid = ' . ( int ) $intCid . ' 
						AND lc.lease_id = ' . ( int ) $intLeaseId . ' 
						AND lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' 
						AND lc.cid = ' . ( int ) $intCid . '
						AND lc.completed_on IS NOT NULL';

			return parent::fetchLeaseChecklists( $strSql, $objDatabase );
	}

	public static function fetchLeaseChecklistsByLeaseIdsByLeaseIntervalIdsByChecklistIdByCid( $arrintLeaseIds, $arrintLeaseIntervalIds, $intChecklistId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						lease_checklists lc
					WHERE
						lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lc.lease_interval_id  IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
						AND lc.checklist_id = ' . ( int ) $intChecklistId . '
						AND lc.cid = ' . ( int ) $intCid;
		return parent::fetchLeaseChecklists( $strSql, $objDatabase );
	}

	public static function fetchLeaseChecklistByLeaseIdByLeaseIntervalIdByChecklistIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistId, $intCid, $objDatabase ) {
		return parent::fetchLeaseChecklist( sprintf( 'SELECT * FROM lease_checklists WHERE lease_id = %d AND lease_interval_id  = %d AND checklist_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intLeaseIntervalId, ( int ) $intChecklistId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistIdsByLeaseIdsByChecklistIdByCid( $arrintLeaseIds, $intChecklistId, $intCid, $objDatabase ) {

		$strSql = 'SELECT lc.lease_id,
						  lc.id as lease_checklist_id
                    FROM lease_checklists lc
					WHERE lc.cid = ' . ( int ) $intCid . ' 
						AND lc.lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' ) 
						AND lc.checklist_id = ' . ( int ) $intChecklistId;

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompletedLeaseChecklistByLeaseIdByLeaseIntervalIdByChecklistIdByCid( $intLeaseId, $intLeaseIntervalId, $intChecklistId, $intCid, $objDatabase ) {

		$strSql = 'SELECT completed_on 
					 FROM lease_checklists
					 WHERE lease_id = ' . ( int ) $intLeaseId . ' 
					    AND lease_interval_id  = ' . ( int ) $intLeaseIntervalId . ' 
					    AND checklist_id = ' . ( int ) $intChecklistId . ' 
					    AND cid = ' . ( int ) $intCid . ' 
					    AND completed_on IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

}
?>