<?php

class CIpsPropertyMigration extends CBaseIpsPropertyMigration {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>