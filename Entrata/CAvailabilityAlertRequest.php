<?php

class CAvailabilityAlertRequest extends CBaseAvailabilityAlertRequest {

	protected $m_intSendAvailabilityAlert;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyId;

	protected $m_boolAgreesToTerms;

	/**
	 *  Set Functions
	 */

	public function setSendAvailabilityAlert( $intSendAvailabilityAlert ) {
		return $this->m_intSendAvailabilityAlert = CStrings::strToIntDef( $intSendAvailabilityAlert, NULL, false );
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		return $this->m_intPropertyFloorplanId = CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false );
	}

	public function setPropertyId( $intPropertyId ) {
		return $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		return $this->m_boolAgreesToTerms = $boolAgreesToTerms;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['send_availability_alert'] ) ) $this->setSendAvailabilityAlert( $arrmixValues['send_availability_alert'] );
		if( true == isset( $arrmixValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrmixValues['property_floorplan_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['agrees_to_terms'] ) ) $this->setAgreesToTerms( $arrmixValues['agrees_to_terms'] );
	}

	public function setDefaults() {
		$intNextQuater = mktime( 0, 0, 0, date( 'm' ) + 3, date( 'd' ),   date( 'Y' ) );

		$this->setAvailabilityAlertFrequencyId( CAvailabilityAlertFrequency::TYPE_WEEKLY );
		$this->setTimeZoneId( CTimeZone::MOUNTAIN_STANDARD_TIME );
		$this->setIsEmailAlert( 1 );
		$this->setSendStartHour( 9 );
		$this->setSendStopHour( 21 );
		$this->setStartDate( date( 'm/d/Y' ) );
		$this->setEndDate( date( 'm/d/Y', $intNextQuater ) );
		$this->setAvailableOnStartDate( date( 'm/d/Y' ) );
		$this->setAvailableOnEndDate( date( 'm/d/Y', $intNextQuater ) );
		$this->setSendAvailabilityAlert( 0 );
		$this->setPropertyId( $this->m_intPropertyId );
		$this->setAgreesToTerms( 0 );
	}

	/**
	 * Get Functions
	 */

	public function getSendAvailabilityAlert() {
		return $this->m_intSendAvailabilityAlert;
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	/**
	 * Create Functions
	 */

	public function createMessage() {
		$objMessage = new CMessage();
		$objMessage->setDefaults();

		$objMessage->setCid( $this->getCid() );
		$objMessage->setApplicantId( $this->getApplicantId() );
		$objMessage->setMessageTypeId( CMessageType::AVAILABILITY_ALERT );
		$objMessage->setMessageOperatorId( $this->getMessageOperatorId() );
		$objMessage->setPhoneNumber( $this->getCellNumber() );
		$objMessage->setPropertyId( $this->getPropertyId() );

		return $objMessage;
	}

	/**
	 * Other Functions
	 */

	public function compareDates( $strDate1, $strDate2 ) {
		$arrintDate1 = explode( '/', $strDate1 );
		$arrintDate2 = explode( '/', $strDate2 );
		$intTimestamp1 = mktime( 0, 0, 0, ( int ) $arrintDate1[0], ( int ) $arrintDate1[1], ( int ) $arrintDate1[2] );
		$intTimestamp2 = mktime( 0, 0, 0, ( int ) $arrintDate2[0], ( int ) $arrintDate2[1], ( int ) $arrintDate2[2] );

		if( $intTimestamp2 >= $intTimestamp1 ) return true;
		return false;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchWebsite( $intWebsiteId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsites::createService()->fetchWebsiteByIdByCid( $intWebsiteId, $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageOperatorId() {
		$boolIsValid = true;

		if( 1 == $this->getIsTextAlert() && true == is_null( $this->getMessageOperatorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_operator_id', __( 'A message operator is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTimeZoneId( $boolCanBeNull = true ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull && true == is_null( $this->getTimeZoneId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_zone_id', __( 'A time zone is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAvailabilityAlertFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAvailabilityAlertFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'availability_alert_frequency_id', __( 'A alert frequency is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == isset( $this->m_strEmailAddress ) || false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'A valid email address is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCellNumber() {
		$boolIsValid = true;

		if( 1 == $this->getIsTextAlert() ) {
			if( true == is_null( $this->getCellNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', __( 'A cell number is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valStartDate( $boolCanBeNull = false ) {
		$boolIsValid = true;

		$intLength = strlen( trim( $this->m_strStartDate ) );

		if( false == $boolCanBeNull ) {
			if( true == is_null( $this->m_strStartDate ) || 0 == $intLength ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'A valid start date is required.' ) ) );
			}
		}

		if( 0 < $intLength && false == CValidation::validateDate( $this->m_strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date must be in mm/dd/yyyy form.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDate( $boolCanBeNull = false ) {
		$boolIsValid = true;

		$intStartDateLength = strlen( trim( $this->m_strStartDate ) );
		$intEndDateLength 	= strlen( trim( $this->m_strEndDate ) );

		if( false == $boolCanBeNull ) {
			if( true == is_null( $this->m_strEndDate ) || 0 == $intEndDateLength ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'A valid end date is required.' ) ) );
			}
		}

		if( 0 < $intEndDateLength ) {
			if( false == CValidation::validateDate( $this->m_strEndDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in mm/dd/yyyy form.' ) ) );
			} elseif( 0 < $intStartDateLength && true == CValidation::validateDate( $this->m_strStartDate ) && false == $this->compareDates( $this->m_strStartDate, date( 'm/d/Y', strtotime( $this->m_strEndDate ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date should be greater than start date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAvailableOnStartDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strAvailableOnStartDate ) || false == CValidation::validateDate( $this->m_strAvailableOnStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on_start_date', __( 'A valid available on start date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAvailableOnEndDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strAvailableOnEndDate ) || false == CValidation::validateDate( $this->m_strAvailableOnEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on_end_date', __( 'A valid available on end date is required.' ) ) );

		} elseif( true == CValidation::validateDate( $this->m_strAvailableOnStartDate ) && false == $this->compareDates( $this->m_strAvailableOnStartDate, $this->m_strAvailableOnEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on_end_date', __( 'End date must be greater than start date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSendStartHour() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSendStopHour() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastAlertedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEmailAlert() {
		$boolIsValid = true;

		if( 0 == $this->m_intIsEmailAlert && 0 == $this->m_intIsTextAlert ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_email_alert', __( 'E-mail alert or text alert is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsEmailOrTextAlert() {
		$boolIsValid = true;

		if( 0 == $this->m_intIsEmailAlert && 0 == $this->m_intIsTextAlert ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_email_alert', __( 'At lease one type of availability is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsTextAlert() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAgreesToTerms() {
		$boolIsValid = true;

		if( 1 == $this->m_intIsTextAlert && false == $this->m_boolAgreesToTerms ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_text_alert', __( 'You need to accept our Terms & Conditions before proceeding.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMobileNumber( $strSectionName = NULL ) {
		$boolIsValid = true;

		$intMobileNoLength 	= strlen( $this->getCellNumber() );
		$strSectionName 	= ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Cell' );

		if( true == is_null( $this->getCellNumber() ) || 0 == $intMobileNoLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', __( '{%s,0} number is required.', [ $strSectionName ] ) ) );
		}

		if( false == is_null( $this->getCellNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->getCellNumber(), false ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', __( 'Valid {%s,0} number is required.', [ $strSectionName ] ), 504 ) );
			$boolIsValid = false;

		} elseif( 0 < $intMobileNoLength && ( 10 > $intMobileNoLength || 15 < $intMobileNoLength ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', __( '{%s,0} number must be between 10 and 15 characters.', [ $strSectionName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valAvailableOnStartDate();
				$boolIsValid &= $this->valAvailableOnEndDate();
				$boolIsValid &= $this->valCellNumber();
				$boolIsValid &= $this->valMessageOperatorId();
				$boolIsValid &= $this->valIsEmailAlert();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_guest_card':
				$boolIsValid &= $this->valStartDate( true );
				$boolIsValid &= $this->valEndDate( true );
				$boolIsValid &= $this->valCellNumber();
				$boolIsValid &= $this->valMessageOperatorId();
				$boolIsValid &= $this->valIsEmailAlert();
				$boolIsValid &= $this->valAgreesToTerms();
				break;

			case 'insert_guest_card_prospect_portal':
				$boolIsValid &= $this->valStartDate( true );
				$boolIsValid &= $this->valEndDate( true );
				$boolIsValid &= $this->valIsEmailAlert();
				$boolIsValid &= $this->valAgreesToTerms();
				break;

			case 'insert_availability_alert':
				$boolIsValid &= $this->valMessageOperatorId();
				$boolIsValid &= $this->valIsEmailOrTextAlert();
				break;

			case 'communication_settings':
				$boolIsValid &= ( false == is_null( $this->getIsEmailAlert() ) && 0 < $this->getIsEmailAlert() ) ? $this->valEmailAddress()	: true;
				$boolIsValid &= ( false == is_null( $this->getIsTextAlert() ) && 0 < $this->getIsTextAlert() ) ? $this->valMobileNumber() : true;
				break;

			case 'communication_settings_required_details':
				$boolIsValid &= $this->valAvailabilityAlertFrequencyId();
				$boolIsValid &= $this->valTimeZoneId( $boolCanBeNull = false );
				$boolIsValid &= $this->valAvailableOnStartDate();
				$boolIsValid &= $this->valAvailableOnEndDate();
				break;

			case 'leasing_center_availability_alert_request':
			case 'call_center_availability_alert_request':
				$boolIsValid &= $this->valStartDate( false );
				$boolIsValid &= $this->valEndDate( false );
				$boolIsValid &= $this->valIsEmailOrTextAlert();
				$boolIsValid &= ( false == is_null( $this->getIsEmailAlert() ) && 0 < $this->getIsEmailAlert() ) ? $this->valEmailAddress()	: true;
				$boolIsValid &= ( false == is_null( $this->getIsTextAlert() ) && 0 < $this->getIsTextAlert() ) ? $this->valMobileNumber() : true;
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>