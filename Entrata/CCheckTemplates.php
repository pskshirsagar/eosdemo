<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCheckTemplates
 * Do not add any new functions to this class.
 */

class CCheckTemplates extends CBaseCheckTemplates {

	public static function fetchDefaultCheckTemplateByCid( $intCid, $objClientDatabase ) {
		return self::fetchCheckTemplate( 'SELECT * FROM check_templates WHERE cid = ' . ( int ) $intCid . ' AND is_default = 1 LIMIT 1', $objClientDatabase );
	}

	public static function fetchSimpleCheckTemplatesByCid( $intCid, $objClientDatabase ) {
		return self::fetchCheckTemplates( 'SELECT * FROM check_templates WHERE cid = ' . ( int ) $intCid . ' ORDER BY is_system desc, LOWER ( name ) ASC', $objClientDatabase );
	}

	public static function fetchCheckTemplatesByIdsByCid( $arrintCheckTemplateIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintCheckTemplateIds ) ) return NULL;
		return self::fetchCheckTemplates( 'SELECT * FROM check_templates WHERE id IN ( ' . implode( ',', $arrintCheckTemplateIds ) . ' ) AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchDefaultOrPreprintedCheckTemplateByCid( $intCid, $objClientDatabase, $boolIsPreprinted = true ) {

		$strSystemCode = NULL;

		if( false == $boolIsPreprinted ) {
			$strSystemCode = 'default';
		} else {
			$strSystemCode = 'preprinted';
		}

		$strSql = 'SELECT
						*
					FROM
						check_templates
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_default = 1
						AND system_code = \'' . $strSystemCode . '\'';

		return self::fetchCheckTemplate( $strSql, $objClientDatabase );
	}
}
?>