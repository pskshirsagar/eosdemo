<?php

class CWebsiteCanonicalUrl extends CBaseWebsiteCanonicalUrl {

	protected $m_arrobjErrorMsgs;

	public function __construct() {
		parent::__construct();
		$this->m_arrobjErrorMsgs = NULL;
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUrl( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', __( 'Link is required. ' ) ) );
		} elseif( false == CValidation::checkUrl( $this->getUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', CDisplayMessages::create()->getMessage( 'VALID_LINK_REQUIRED' ) ) );
		} elseif( false == CValidation::checkUrlPostfix( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', __( 'Please add a \'/\' to the end of your Link URL. ' ) ) );
		} elseif( true == is_object( $objDatabase ) ) {
			$objWebsiteCanonicalUrl = \Psi\Eos\Entrata\CWebsiteCanonicalUrls::createService()->fetchCanonicalUrlByUrlByWebsiteIdByCid( $this->getUrl(), $this->m_intWebsiteId, $this->m_intCid, $objDatabase );
			if( true == is_object( $objWebsiteCanonicalUrl ) && $objWebsiteCanonicalUrl->getId() != $this->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Link already exists. ' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valCanonicalUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getCanonicalUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'canonical_url', __( 'URL is required. ' ) ) );
		} elseif( false == CValidation::checkUrl( $this->getCanonicalUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'canonical_url', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

	 public function valUpdatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	 }

	public function valUpdatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUrl( $objDatabase );
				$boolIsValid &= $this->valCanonicalUrl();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function applyRequestForm( $arrobjRequestForm, $arrstrFormFields = NULL ) {
		if( true == valArr( $arrstrFormFields ) ) {
			$arrobjRequestForm = $this->mergeIntersectArray( $arrstrFormFields, $arrobjRequestForm );
		}

		$this->setValues( $arrobjRequestForm, false );
		return;
	}

	public function mergeIntersectArray( $arrstrFormFields, $arrobjRequestForm ) {

		$arrobjResult = array_merge( $arrstrFormFields, $arrobjRequestForm );

		foreach( $arrobjResult as $strKey => $strValue ) {
			if( false == array_key_exists( $strKey, $arrstrFormFields ) ) {
				unset( $arrobjResult[$strKey] );
			}
		}

		return $arrobjResult;
	}

	public function addErrorMsg( $objErrorMsg ) {
		$this->m_arrobjErrorMsgs[] = $objErrorMsg;
	}

	public function getErrorMsgs() {
		return $this->m_arrobjErrorMsgs;
	}

}
?>