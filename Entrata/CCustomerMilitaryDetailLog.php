<?php

class CCustomerMilitaryDetailLog extends CBaseCustomerMilitaryDetailLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerMilitaryDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryComponentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryPayGradeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryRankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryAssistanceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryNonMilitaryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFutureMilitaryInstallationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerMilitaryDetailLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitIdCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommandingOfficer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastAssignment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyAddressStreet1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyAddressStreet2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyAddressCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyAddressStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyAddressPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDutyEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeployedLocation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFutureInstallationOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFutureReportDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnlistmentDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRankEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChangeInDutyDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndOfServiceDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeploymentStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeploymentEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPromotable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsWarrantCandidate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActiveReservist() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDeployed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>