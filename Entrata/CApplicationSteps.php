<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSteps
 * Do not add any new functions to this class.
 */

class CApplicationSteps extends CBaseApplicationSteps {

	public static function fetchApplicationSteps( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationStep', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchApplicationStep( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationStep', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllApplicationSteps( $objDatabase ) {
		return self::fetchApplicationSteps( 'SELECT * FROM application_steps', $objDatabase );
	}

	public static function fetchApplicationStepsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		$strSql = 'SELECT * FROM application_steps WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )';
		return self::fetchApplicationSteps( $strSql, $objDatabase );
	}
}
?>