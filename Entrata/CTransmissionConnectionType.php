<?php

class CTransmissionConnectionType extends CBaseTransmissionConnectionType {

	const EMAIL 	 = 1;
	const URL 	 	 = 2;
	const FTP 	 	 = 3;
	const SFTP 	 	 = 4;

	public static $c_arrmixTransmissionConnectionTypeName = [ self::URL => 'URL', self::FTP => 'FTP', self::SFTP => 'SFTP' ];
	public static $c_arrmixTransmissionConnectionTypeIds = [ self::FTP => self::FTP, self::SFTP => self::SFTP ];
}
?>