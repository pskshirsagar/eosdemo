<?php

class CPropertyBankAccountLog extends CBasePropertyBankAccountLog {

	protected $m_intGlAccountId;

	protected $m_strPropertyName;
	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;

    /**
     * Get Functions
     */

    public function getGlAccountId() {
    	return $this->m_intGlAccountId;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getGlAccountName() {
    	return $this->m_strGlAccountName;
    }

    public function getGlAccountNumber() {
    	return $this->m_strGlAccountNumber;
    }

    /**
     * Set Functions
     */

    public function setGlAccountId( $intGlAccountId ) {
    	$this->m_intGlAccountId = CStrings::strToIntDef( $intGlAccountId, NULL, false );
    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setGlAccountName( $strGlAccountName ) {
    	$this->m_strGlAccountName = $strGlAccountName;
    }

    public function setGlAccountNumber( $strGlAccountNumber ) {
    	$this->m_strGlAccountNumber = $strGlAccountNumber;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['gl_account_id'] ) ) $this->setGlAccountId( $arrmixValues['gl_account_id'] );
    	if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
    	if( true == isset( $arrmixValues['gl_account_name'] ) ) $this->setGlAccountName( $arrmixValues['gl_account_name'] );
    	if( true == isset( $arrmixValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrmixValues['gl_account_number'] );

    	return;
    }

    public function mapPropertyBankAccountLog( $objPropertyBankAccount ) {

		$this->setCid( $objPropertyBankAccount->getCid() );
		$this->setPropertyId( $objPropertyBankAccount->getPropertyId() );
		$this->setBankApCodeId( $objPropertyBankAccount->getBankApCodeId() );
		$this->setIsReimbursedProperty( $objPropertyBankAccount->getIsReimbursedProperty() );
		$this->setCreatedBy( $objPropertyBankAccount->getCreatedBy() );
		$this->setCreatedOn( $objPropertyBankAccount->getCreatedOn() );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBankAccountLogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDueToFrom() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>