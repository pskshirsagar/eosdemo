<?php

class CPropertyTaxedArCode extends CBasePropertyTaxedArCode {

	protected $m_strTaxedArCodeName;

    /**
     * Get Functions
     */
    public function getTaxedArCodeName() {
    	return $this->m_strTaxedArCodeName;
    }

    /**
     * Set Functions
     */

    public function setTaxedArCodeName( $strTaxedArCodeName ) {
    	$this->m_strTaxedArCodeName = $strTaxedArCodeName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );
    	if( true == isset( $arrmixValues['taxed_ar_code_name'] ) ) $this->setTaxedArCodeName( $arrmixValues['taxed_ar_code_name'] );
    	return;
    }

    /**
     * Vallidation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyTaxId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaxedArCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = true ) {

    	if( false == $boolIsSoftDelete ) {
    		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

    	$this->setDeletedBy( $intCurrentUserId );
    	$this->setDeletedOn( ' NOW() ' );
    	$this->setUpdatedBy( $intCurrentUserId );
    	$this->setUpdatedOn( ' NOW() ' );

    	return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

}
?>