<?php

class CApFormula extends CBaseApFormula {

	protected $m_intCountProperty;
	protected $m_intCountAllocationDetail;
	protected $m_intIsAllocation;
	protected $m_arrintPropertyIds;

	protected $m_strPropertyName;
	protected $m_strLineItemType;

	protected $m_boolIsApMigrationMode;
	/**
	 * Get Functions
	 */

	public function getCountAllocationDetail() {
		return $this->m_intCountAllocationDetail;
	}

	public function getCountProperty() {
		return $this->m_intCountProperty;
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLineItemType() {
		return $this->m_strLineItemType;
	}

	public function getIsApMigrationMode() {
		return $this->m_boolIsApMigrationMode;
	}

	public function getIsAllocation() {
		return $this->m_intIsAllocation;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['count_allocation_detail'] ) ) $this->setCountAllocationDetail( $arrmixValues['count_allocation_detail'] );
		if( true == isset( $arrmixValues['count_property'] ) ) $this->setCountProperty( $arrmixValues['count_property'] );
		if( true == isset( $arrmixValues['property_ids'] ) ) $this->setPropertyIds( $arrmixValues['property_ids'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['line_item_type'] ) ) $this->setLineItemType( $arrmixValues['line_item_type'] );
		if( true == isset( $arrmixValues['is_ap_migration_mode'] ) ) $this->setIsApMigrationMode( $arrmixValues['is_ap_migration_mode'] );
		if( true == isset( $arrmixValues['is_allocation'] ) ) $this->setIsAllocation( $arrmixValues['is_allocation'] );

		return;
	}

	public function setCountAllocationDetail( $intCountAllocationDetail ) {
		$this->m_intCountAllocationDetail = CStrings::strToIntDef( $intCountAllocationDetail, NULL, false );
	}

	public function setCountProperty( $intCountProperty ) {
		$this->m_intCountProperty = CStrings::strToIntDef( $intCountProperty, NULL, false );
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setLineItemType( $strLineItemType ) {
		$this->m_strLineItemType = $strLineItemType;
	}

	public function setIsApMigrationMode( $boolIsApMigrationMode ) {
		$this->m_boolIsApMigrationMode = $boolIsApMigrationMode;
	}

	public function setIsAllocation( $intIsAllocation ) {
		$this->m_intIsAllocation = $intIsAllocation;
	}

	/**
	 * Validate Functions
	 */

	public function valApFormulaTypeId() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getApFormulaTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_formula_type_id', __( 'Allocation method is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valName( $objClientDatabase ) {

		if( false == valStr( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invoice allocation name is required.' ) ) );
			return false;
		}

		$strCondition = '';

		if( true == is_numeric( $this->getId() ) ) {
			$strCondition = 'AND id <> ' . $this->getId();
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND name ILIKE \'' . addslashes( $this->getName() ) . '\'' .
						$strCondition;

		$intCountExistingInvoiceAllocationName = \Psi\Eos\Entrata\CApFormulas::createService()->fetchApFormulaCount( $strWhere, $objClientDatabase );

		if( 0 < $intCountExistingInvoiceAllocationName ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invoice allocation name already exists.' ) ) );
			return false;
		}

		return true;
	}

	public function valCountAllocationDetail() {

		$boolIsValid = true;

		if( 1 > $this->getCountAllocationDetail() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'At least one line item is required.' ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valPropertyIds( $objClientDatabase ) {

		$boolIsValid 				= true;
		$arrstrInvalidPropretyNames = [];

		// add validation if number of spaces option is selected then check property is student property or not.
		if( true == valId( $this->getApFormulaTypeId() ) && CApFormulaType::NO_OF_SPACES == $this->getApFormulaTypeId() && true == valArr( $this->getPropertyIds() ) ) {

			$arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByCid( $this->getPropertyIds(), $this->getCid(), $objClientDatabase );
			$arrobjPropertyPreferences = rekeyObjects( 'Key', ( array ) CPropertyPreferences::fetchPropertyPreferencesByKeyByPropertyIdsByCid( 'ENABLE_SEMESTER_SELECTION', $this->getPropertyIds(), $this->getCid(), $objClientDatabase ) );

			foreach( $arrobjProperties as $objProperty ) {
				$boolIsStudentSemesterSelectionEnabled = ( true == valArr( $arrobjPropertyPreferences ) && true == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) ? true : false;
				if( false == in_array( COccupancyType::STUDENT, $objProperty->getOccupancyTypeIds() ) || false == $boolIsStudentSemesterSelectionEnabled ) {
					$arrstrInvalidPropretyNames[$objProperty->getId()] = $objProperty->getPropertyName();
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrInvalidPropretyNames ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_ids', __( "Number of spaces allocation method can be used only for student properties. Property(s) '{%s, 0}' is not valid.", [ implode( '\', \'', $arrstrInvalidPropretyNames ) ] ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valApFormulaTypeId();
				$boolIsValid &= $this->valCountAllocationDetail();
				$boolIsValid &= $this->valPropertyIds( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function creatApFormulaProperty() {

		$objApFormulaProperty = new CApFormulaProperty();

		$objApFormulaProperty->setCid( $this->getCid() );
		$objApFormulaProperty->setApFormulaId( $this->getId() );

		return $objApFormulaProperty;
	}

	public function creatApFormulaUnitType() {

		$objApFormulaUnitType = new CApFormulaUnitType();

		$objApFormulaUnitType->setCid( $this->getCid() );
		$objApFormulaUnitType->setApFormulaId( $this->getId() );

		return $objApFormulaUnitType;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchApFormulaProperties( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CApFormulaProperties::createService()->fetchApFormulaPropertiesByApFormulaIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApFormulaUnitTypes( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CApFormulaUnitTypes::createService()->fetchApFormulaUnitTypesByApFormulaIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

}
?>