<?php

class CReviewDetail extends CBaseReviewDetail {

	protected $m_strReviewTypeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle( $objReview = NULL ) {
		$boolIsValid = true;

		if( true == valObj( $objReview, 'CReview' ) && CReviewType::RESIDENT == $objReview->getReviewTypeId() ) {
			return true;
		}

		$this->setTitle( $this->getSanitizedFormFieldReview( $this->getTitle() ) );

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'testimonial', __( 'Review Title is required' ) ) );
		}

		return $boolIsValid;
	}

	public function valReview() {
		$boolIsValid = true;

		$this->setReview( $this->getSanitizedFormFieldReview( $this->getReview() ) );

		if( false == valStr( $this->getReview() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'review',  __( 'Review is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valReviewUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is not a valid one.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReviewDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverallRating() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinalResponse() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRecommended() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsProspectPortalPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRespondedTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasAttributes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLcResponseCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponseApproved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNotificationSent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideUnpublishedOnDashboard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideUnrespondedOnDashboard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideNeedsAttributesOnDashboard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDetailStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewRecommendationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objReview = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valTitle( $objReview );
				$boolIsValid &= $this->valReview();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReview();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_api_insert':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valTitle( $objReview );
				break;

			case 'validate_prospect_portal_reviews':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valReview();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Get Functions
	 */

	public function getReviewTypeName() {
		return $this->m_strReviewTypeName;
	}

	public function getSanitizedFormFieldReview( $strFormField ) {
		return preg_replace( '/(^\'|[\s\']\'[\']*|[\/~`\!@#\$%\^&\*\(\)\+=\{\}\[\]\|;:"\<\>\?\\\][\']*)/', ' ', strip_tags( $strFormField ) );
	}

	/**
	 *  Set Function
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['review_type_name'] ) ) {
			$this->setReviewTypeName( $arrmixValues['review_type_name'] );
		}
	}

	public function setReviewTypeName( $strReviewTypeName ) {
		$this->m_strReviewTypeName = $strReviewTypeName;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createReviewDetailEvent( $intReviewEventTypeId = NULL ) {

		$objReviewDetailEvent = new CReviewDetailEvent();

		$objReviewDetailEvent->setCid( $this->getCid() );
		$objReviewDetailEvent->setPropertyId( $this->getPropertyId() );
		$objReviewDetailEvent->setReviewDetailId( $this->getId() );

		if( true == valId( $intReviewEventTypeId ) ) {
			$objReviewDetailEvent->setReviewEventTypeId( $intReviewEventTypeId );
		}

		return $objReviewDetailEvent;
	}

	public function createReviewDetailRating() {

		$objReviewDetailRating = new CReviewDetailRating();

		$objReviewDetailRating->setCid( $this->getCid() );
		$objReviewDetailRating->setPropertyId( $this->getPropertyId() );
		$objReviewDetailRating->setReviewDetailId( $this->getId() );

		return $objReviewDetailRating;
	}

}
?>