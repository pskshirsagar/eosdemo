<?php

class CAuthenticationLogType extends CBaseAuthenticationLogType {

	const ENTRATA 					= 1;
	const ENTRATA_CLIENT_ADMIN 		= 2;
	const ENTRATA_LEAD_ALERT 		= 3;
	const ENTRATA_DOC_SCAN 			= 4;
	const SITE_TABLET 				= 5;
	const ENTRATA_ACTIVE_DIRECTORY 	= 6;
	const ENTRATA_SAML 				= 7;
	const ENTRATA_PASSWORD_HISTORY 	= 8;
	const PROSPECT_PORTAL 			= 9;
	const OWNER_PORTAL 				= 10;
	const EMPLOYEE_PORTAL 			= 11;
	const CHAT_PANEL 			    = 12;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>