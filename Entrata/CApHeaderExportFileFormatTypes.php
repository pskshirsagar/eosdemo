<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportFileFormatTypes
 * Do not add any new functions to this class.
 */

class CApHeaderExportFileFormatTypes extends CBaseApHeaderExportFileFormatTypes {

	public static function fetchApHeaderExportFileFormatTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApHeaderExportFileFormatType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApHeaderExportFileFormatType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApHeaderExportFileFormatType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApHeaderExportFileFormatNameById( $intId, $objDatabase ) {
		$mixObjApHeaderExportFileFormatName = self::fetchApHeaderExportFileFormatType( sprintf( 'SELECT name FROM ap_header_export_file_format_types WHERE id = %d', ( int ) $intId ), $objDatabase );

		if( valObj( $mixObjApHeaderExportFileFormatName, 'CApHeaderExportFileFormatType' ) ) {
		return $mixObjApHeaderExportFileFormatName->getName();
		}
		return false;
	}

	public static function fetchApHeaderExportFileFormatTypesByApHeaderExportBatchTypeId( $intApHeaderExportBatchTypeId, $objDatabase ) {
		return self::fetchApHeaderExportFileFormatTypes( sprintf( 'SELECT * FROM ap_header_export_file_format_types WHERE ap_header_export_batch_type_id = %d ORDER BY name', ( int ) $intApHeaderExportBatchTypeId ), $objDatabase );
	}

}
?>