<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaders
 * Do not add any new functions to this class.
 */

class CGlHeaders extends CBaseGlHeaders {

	public static function fetchCustomGlHeaderByIdByCid( $intGlHeaderId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gh.*,
						gh1.post_date AS reverse_post_date,
						gh1.header_number AS offsetting_header_number,
						cd.name as bulk_company_department_name,
						gd.name as custom_tag_name,
						p.property_name,
						ghs.frequency_id,
						ghs.frequency_interval,
						ghs.day_of_week,
						ghs.days_of_month,
						ghs.start_date,
						ghs.end_date,
						ghs.number_of_occurrences,
						ghs.is_reverse_next_month,
						ghs.is_paused as is_scheduled_je_paused,
						ghs.approved_by,
						ghs.disabled_by,
						art.name AS ap_routing_tag_name,
						jp.name AS bulk_job_phase_name,
						ac.name AS bulk_ap_contract_name
					FROM
						gl_headers gh
						LEFT JOIN gl_headers gh1 ON ( gh.cid = gh1.cid AND gh.id = gh1.offsetting_gl_header_id )
						LEFT JOIN company_departments cd ON ( gh.cid = cd.cid AND gh.bulk_company_department_id = cd.id )
						LEFT JOIN gl_dimensions gd ON ( gh.cid = gd.cid AND gh.bulk_gl_dimension_id = gd.id )
						LEFT JOIN properties p ON ( gh.cid = p.cid AND p.id = gh.bulk_property_id )
						LEFT JOIN gl_header_schedules ghs ON ( gh.cid = ghs.cid AND gh.gl_header_schedule_id = ghs.id )
						LEFT JOIN ap_routing_tags art ON ( gh.cid = art.cid AND gh.ap_routing_tag_id = art.id )
						LEFT JOIN job_phases jp ON ( gh.cid = jp.cid AND gh.bulk_job_phase_id = jp.id )
						LEFT JOIN ap_contracts ac ON ( gh.cid = ac.cid AND gh.bulk_ap_contract_id = ac.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.id = ' . ( int ) $intGlHeaderId;

		return self::fetchGlHeader( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeadersByIdsByCid( $arrintIds, $intCid, $objClientDatabase, $boolIsBulkPrint = false ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gh.*,
						CASE
							WHEN fa.gl_header_id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_attachment,
						gb.name AS book_name
					FROM
						gl_headers gh
						LEFT JOIN file_associations fa ON ( gh.cid = fa.cid AND gh.id = fa.gl_header_id AND fa.deleted_by IS NULL )
						LEFT JOIN gl_books gb ON ( gh.cid = gb.cid AND gh.gl_book_id = gb.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		if( true == $boolIsBulkPrint ) {
			$strSql .= 'ORDER BY gh.header_number DESC';
		}
		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentsByGlReconciliationFilterByCid( $objGlReconciliationFilter, $intCid, $objClientDatabase ) {

		$strCondition						= '';
		$strWhereCondition					= '';
		$strUnCheckedApPaymentIdsCondition	= '';

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			} else {

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' ) OR
											gd.cash_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' )
										) ';
			}

			if( true == valIntArr( $objGlReconciliationFilter->getApPaymentIds() ) ) {
				$strUnCheckedApPaymentIdsCondition .= ' AND ap.id IN( ' . implode( ',', $objGlReconciliationFilter->getApPaymentIds() ) . ' )';
			}

			if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strPostDate = $strPostMonth = $objGlReconciliationFilter->getStatementDate();

				$arrstrPostMonth = explode( '/', $strPostDate );

				if( 1 == $objGlReconciliationFilter->getIsBankRecOnPostMonth() && true == valArr( $arrstrPostMonth ) && 3 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
					$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[2];
				}

				if( 1 == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {
					$strWhereCondition = ' reversal_ah.post_month > \'' . $strPostMonth . '\'';
				} else {
					$strWhereCondition = ' reversal_ah.post_date > \'' . $strPostDate . '\'';
				}
			}
		}

		$strSql = 'SELECT
						DISTINCT ON( sub_query.ap_payment_id, sub_query.ap_header_id,sub_query.gl_transaction_type_id ) ap_payment_id AS ap_payment_id,
						sub_query.*
					FROM (
					( SELECT
						ap.id AS ap_payment_id,
						ap.payment_number,
						ap.payment_number AS transaction_id,
						func_format_refund_customer_names( ap.payee_name ) AS payee_name,
						ap.ap_payment_type_id,
						ah.id AS ap_header_id,
						gh.post_date,
						gh.post_month,
						SUM ( gd.amount ) OVER ( PARTITION BY ap.id, gh.gl_transaction_type_id ) AS amount,
						gd.gl_header_id,
						gd.property_id,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gh.gl_transaction_type_id,
						gtt.description AS gl_transaction_type,
						gd.gl_reconciliation_id,
						gh.memo,
						gd.memo AS description,
						ap.bank_account_id,
						ah.ap_payee_id,
						ah.deleted_by,
						ah.deleted_on
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id )
						LEFT JOIN ap_headers reversal_ah ON reversal_ah.cid = ah.cid AND ah.id = reversal_ah.reversal_ap_header_id
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
						JOIN load_properties ( ARRAY[' . $intCid . ']::INTEGER[], ARRAY[' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ']::INTEGER[] ) lp ON ad.cid = lp.cid
							AND ( ad.property_id = lp.property_id OR ( ad.reimbursement_method_id IN ( ' . CReimbursementMethod::STANDARD_REIMBURSEMENT_DO_NOT_USE_SUBLEDGERS . ', ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . ' ) AND ad.inter_co_property_id = lp.property_id ) )
						JOIN gl_headers gh ON ( aa.cid = gh.cid AND aa.id = gh.reference_id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id )
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gd.gl_reconciliation_id IS NULL
						AND aa.is_deleted = false
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' ) ' .
						$strCondition . $strUnCheckedApPaymentIdsCondition . ' AND CASE
						WHEN ap.is_unclaimed_property = TRUE THEN ' . $strWhereCondition . '
							ELSE 1 = 1
						END )
						
				UNION
					( SELECT
						ap.id AS ap_payment_id,
						ap.payment_number,
						ap.payment_number AS transaction_id,
						func_format_refund_customer_names( ap.payee_name ) AS payee_name,
						ap.ap_payment_type_id,
						ah.id AS ap_header_id,
						gh.post_date,
						gh.post_month,
						SUM ( gd.amount ) OVER ( PARTITION BY ap.id, gh.gl_transaction_type_id, ah.header_number, ad.ap_header_id ) AS amount,
						gd.gl_header_id,
						gd.property_id,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gh.gl_transaction_type_id,
						gtt.description AS gl_transaction_type,
						gd.gl_reconciliation_id,
						gh.memo,
						gd.memo AS description,
						ap.bank_account_id,
						ah.ap_payee_id,
						ah.deleted_by,
						ah.deleted_on
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id )
						LEFT JOIN ap_headers reversal_ah ON reversal_ah.cid = ah.cid AND ah.id = reversal_ah.reversal_ap_header_id
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
						JOIN load_properties ( ARRAY[' . $intCid . ']::INTEGER[], ARRAY[' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ']::INTEGER[] ) lp ON ad.cid = lp.cid
							AND ( ad.property_id = lp.property_id OR ( ad.reimbursement_method_id IN ( ' . CReimbursementMethod::STANDARD_REIMBURSEMENT_DO_NOT_USE_SUBLEDGERS . ', ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . ' ) AND ad.inter_co_property_id = lp.property_id ) )
						JOIN gl_headers gh ON ( aa.cid = gh.cid AND aa.id = gh.reference_id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id )
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gd.gl_reconciliation_id IS NULL
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' ) ' .
						$strCondition . $strUnCheckedApPaymentIdsCondition . ' 
						AND CASE
                            WHEN ap.is_unclaimed_property = TRUE THEN ' . $strWhereCondition . '
                            ELSE 1 = 1
                          END )
						

					) AS sub_query
				ORDER BY
					ap_payment_id,
					ap_header_id,
					gl_transaction_type_id,
					gl_header_id DESC'; // We want latest payment and void transactions so added order by DESC gl_header_id

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchArDepositsByGlReconciliationFilterByCid( $objGlReconciliationFilter, $intCid, $objClientDatabase ) {

		$arrstrWhereConditions = $arrstrGlDetailsWhereConditions = [];

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valIntArr( $objGlReconciliationFilter->getArDepositIds() ) ) {
				$arrstrWhereConditions[] = 'AND ad.id IN ( ' . sqlIntImplode( $objGlReconciliationFilter->getArDepositIds() ) . ' )';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$arrstrWhereConditions[] = 'AND gh.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$arrstrWhereConditions[] = 'AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			} else {

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$arrstrWhereConditions[] = 'AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$arrstrWhereConditions[] = 'AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$arrstrGlDetailsWhereConditions[] = '
					AND (
						gd.accrual_gl_account_id IN ( ' . sqlIntImplode( $objGlReconciliationFilter->getGlAccountIds() ) . ' )
						OR
						gd.cash_gl_account_id IN ( ' . sqlIntImplode( $objGlReconciliationFilter->getGlAccountIds() ) . ' )
					)';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$arrstrGlDetailsWhereConditions[] = 'AND gd.property_id IN ( ' . sqlIntImplode( $objGlReconciliationFilter->getPropertyIds() ) . ' ) ';
			}
		}

		$strSql = '
			SELECT DISTINCT ON ( ad.id )
				gh.id AS gl_header_id,
				ad.deposit_amount AS amount,
				ad.deposit_number,
				ad.deposit_number AS transaction_id,
				ad.origin_ar_deposit_id,
				gh.id AS gl_header_id,
				gh.post_date,
				gh.post_month,
				gh.reference_id AS ar_deposit_id,
				gtt.name AS gl_transaction_type,  
				sub_gd.accrual_gl_account_id,
				sub_gd.cash_gl_account_id,
				sub_gd.property_id
			FROM
				ar_deposits ad
				JOIN gl_headers gh ON gh.cid = ad.cid AND gh.reference_id = ad.id AND gh.gl_transaction_type_id = ad.gl_transaction_type_id
				JOIN gl_transaction_types gtt ON gtt.id = gh.gl_transaction_type_id
				JOIN LATERAL (
					SELECT
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.property_id
					FROM
						gl_details gd
					WHERE
						gd.cid = gh.cid
						AND gd.gl_header_id = gh.id
						AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
						AND gd.post_month = gh.post_month
						AND gd.gl_reconciliation_id IS NULL
						' . implode( ' ', $arrstrGlDetailsWhereConditions ) . '
				) AS sub_gd ON TRUE
			WHERE
				ad.cid = ' . ( int ) $intCid . '
				AND ad.deposit_amount != 0
				AND gh.gl_transaction_type_id IN ( ' . sqlIntImplode( [ CGlTransactionType::AR_DEPOSIT, CGlTransactionType::AR_DEPOSIT_REVERSAL ] ) . ' )
				AND gh.gl_header_status_type_id NOT IN ( ' . sqlIntImplode( CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
				' . implode( ' ', $arrstrWhereConditions ) . '
			ORDER BY
				ad.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchJournalEntriesByGlReconciliationFilterByCid( $objGlReconciliationFilter, $intCid, $objClientDatabase ) {

		$strCondition				= '';
		$strUnCheckedJEIdsCondition	= '';

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			} else {
				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' ) OR
											gd.cash_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' )
										) ';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' ) ';
			}

			if( true == valIntArr( $objGlReconciliationFilter->getGeneralJournalIds() ) ) {
				$strUnCheckedJEIdsCondition .= ' AND gh.id IN( ' . implode( ',', $objGlReconciliationFilter->getGeneralJournalIds() ) . ' )';
			}
		}

		$strSql	= 'SELECT
						gd.gl_header_id,
						gd.id AS gl_detail_id,
						gd.property_id,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount,
						gd.memo AS description,
						p.property_name,
						gh.post_date,
						gh.post_month,
						gtt.description as gl_transaction_type,
						gd.gl_reconciliation_id,
						gd.is_confidential,
						gh.header_number,
						gh.header_number AS transaction_id,
						gh.offsetting_gl_header_id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month )
						JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS false
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ .
						$strCondition . $strUnCheckedJEIdsCondition . '
						AND gd.gl_reconciliation_id IS NULL
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					ORDER BY
						gd.gl_header_id,
						gd.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllApPaymentsByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationFilter, $intCid, $objClientDatabase, $intSystemReconciliationId = NULL ) {

		if( true == is_null( $intGlReconciliationId )
			|| ( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' )
				&& false == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) ) {
			return NULL;
		}

		$strCondition 						= '';
		$strWhereCondition                  = '';
		$strOuterCondition					= 'AND gd.gl_reconciliation_id IS NULL ';
		$strUnCheckedApPaymentIdsCondition	= '';
		$strUnionForSystemReconciliation	= '';

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
				$strCondition .= ' AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
			}

			if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				$strCondition = '';

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gd.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gd.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' ) OR
												gd.cash_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' )
											) ';
			}

			if( false == is_null( $objGlReconciliationFilter->getReconciledDate() )
				&& strtotime( $objGlReconciliationFilter->getReconciledDate() ) >= strtotime( date( 'm/d/Y', strtotime( '04/18/2014' ) ) ) ) {
				$strOuterCondition = 'AND gd.id IN (
													SELECT
														gl_detail_id
													FROM
														gl_reconciliation_exceptions gre
													WHERE
														gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
														AND cid = ' . ( int ) $intCid . '
												)';
			}

			if( true == valIntArr( $objGlReconciliationFilter->getApPaymentIds() ) ) {
				$strUnCheckedApPaymentIdsCondition .= ' AND ap.id IN( ' . implode( ',', $objGlReconciliationFilter->getApPaymentIds() ) . ' )';
			}

			if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strPostDate = $strPostMonth = $objGlReconciliationFilter->getStatementDate();
				$arrstrPostMonth = explode( '/', $strPostDate );

				if( 1 == $objGlReconciliationFilter->getIsBankRecOnPostMonth() && true == valArr( $arrstrPostMonth ) && 3 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
					$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[2];
				}

				if( 1 == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {
					$strWhereCondition = ' reversal_ah.post_month > \'' . $strPostMonth . '\'';
				} else {
					$strWhereCondition = ' reversal_ah.post_date > \'' . $strPostDate . '\'';
				}
			}
		}

		if( true == valId( $intSystemReconciliationId ) ) {
			$strUnionForSystemReconciliation = ' UNION 
			SELECT
					gd.*,
					gh.post_date AS gh_post_date,
					gh.memo AS gh_memo
				FROM
					gl_details gd
					JOIN gl_headers gh ON gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
								AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' )
				WHERE
					gd.cid = ' . ( int ) $intCid . '
					AND gd.property_id IN( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' )
					AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					AND gd.gl_reconciliation_id = ' . ( int ) $intSystemReconciliationId . $strCondition . ' ';
		}

		$strSql = '
			WITH cte_gl_details AS (
				SELECT
					gd.*,
					gh.post_date AS gh_post_date,
					gh.memo AS gh_memo
				FROM
					gl_details gd
					JOIN gl_headers gh ON gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
								AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' )
				WHERE
					gd.cid = ' . ( int ) $intCid . '
					AND gd.property_id IN( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' )
					AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					AND gd.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
				' . $strUnionForSystemReconciliation . ' 
				UNION

				SELECT
					gd.*,
					gh.post_date AS gh_post_date,
					gh.memo AS gh_memo
				FROM
					gl_details gd
					JOIN gl_headers gh ON gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
								AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' )
				WHERE
					gd.cid = ' . ( int ) $intCid . '
					AND gd.property_id IN( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' )
					AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					' . $strOuterCondition . '
					' . $strCondition . '
			)

			SELECT DISTINCT ON( sub_query.ap_payment_id, sub_query.ap_header_id,sub_query.gl_transaction_type_id )
				ap_payment_id AS ap_payment_id,
				sub_query.*  
			FROM (
					(
						SELECT 
							ap.id AS ap_payment_id,
							ap.payment_number,
							ap.payment_number AS transaction_id,
							func_format_refund_customer_names( ap.payee_name ) AS payee_name,
							ap.ap_payment_type_id,
							ah.id AS ap_header_id,
							gd.gh_post_date AS post_date,
							gd.post_month,
							SUM ( gd.amount ) OVER ( PARTITION BY ap.id, gd.gl_transaction_type_id ) AS amount,
							gd.gl_header_id,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gtt.description AS gl_transaction_type,
							gd.gl_transaction_type_id,
							gd.gl_reconciliation_id,
							gd.gh_memo AS memo,
							gd.memo AS description,
							ap.bank_account_id,
							ah.ap_payee_id,
							ah.deleted_by,
							ah.deleted_on
						FROM
							ap_payments ap
							JOIN ap_headers ah ON ap.cid = ah.cid AND ap.id = ah.ap_payment_id
							LEFT JOIN ap_headers reversal_ah ON reversal_ah.cid = ah.cid AND ah.id = reversal_ah.reversal_ap_header_id
							JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month
							JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
							JOIN cte_gl_details gd ON aa.cid = gd.cid AND aa.id = gd.reference_id AND aa.gl_transaction_type_id = gd.gl_transaction_type_id
							JOIN gl_transaction_types gtt ON gd.gl_transaction_type_id = gtt.id
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND gd.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . '
							' . $strUnCheckedApPaymentIdsCondition . '
							AND aa.is_deleted = false
							AND CASE
									WHEN ap.is_unclaimed_property = TRUE THEN ' . $strWhereCondition . '
								ELSE 1 = 1
								END
					)
					UNION
					(
						SELECT 
							ap.id AS ap_payment_id,
							ap.payment_number,
							ap.payment_number AS transaction_id,
							func_format_refund_customer_names( ap.payee_name ) AS payee_name,
							ap.ap_payment_type_id,
							ah.id AS ap_header_id,
							gd.gh_post_date AS post_date,
							gd.post_month,
							SUM ( gd.amount ) OVER ( PARTITION BY ap.id, gd.gl_transaction_type_id, ah.header_number, ad.ap_header_id ) AS amount,
							gd.gl_header_id,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gtt.description AS gl_transaction_type,
							gd.gl_transaction_type_id,
							gd.gl_reconciliation_id,
							gd.gh_memo AS memo,
							gd.memo AS description,
							ap.bank_account_id,
							ah.ap_payee_id,
							ah.deleted_by,
							ah.deleted_on
						FROM
							ap_payments ap
							JOIN ap_headers ah ON ap.cid = ah.cid AND ap.id = ah.ap_payment_id
							LEFT JOIN ap_headers reversal_ah ON reversal_ah.cid = ah.cid AND ah.id = reversal_ah.reversal_ap_header_id
							JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month
							JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
							JOIN cte_gl_details gd ON aa.cid = gd.cid AND aa.id = gd.reference_id AND aa.gl_transaction_type_id = gd.gl_transaction_type_id
							JOIN gl_transaction_types gtt ON gd.gl_transaction_type_id = gtt.id
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							' . $strUnCheckedApPaymentIdsCondition . '
							AND CASE
									WHEN ap.is_unclaimed_property = TRUE THEN ' . $strWhereCondition . '
								ELSE 1 = 1
								END
					)
				) AS sub_query
			ORDER BY
				ap_payment_id,
				ap_header_id,
				gl_transaction_type_id,
				gl_header_id DESC ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllArDepositsByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationFilter, $intCid, $objClientDatabase, $intSystemReconciliationId = NULL ) {

		if( true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strCondition						= '';
		$strOuterCondition					= 'AND gd.gl_reconciliation_id IS NULL ';
		$strUnCheckedArDepositIdsCondition	= '';
		$strUnionForSystemReconciliation	= '';

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			}

			if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
				$strCondition .= ' AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				$strCondition = '';

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' ) OR
											gd.cash_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' )
										) ';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getPropertyIds() ) ) . ' ) ';
			}

			if( true == valIntArr( $objGlReconciliationFilter->getArDepositIds() ) ) {
				$strUnCheckedArDepositIdsCondition .= ' AND ad.id IN( ' . implode( ',', $objGlReconciliationFilter->getArDepositIds() ) . ' )';
			}

			if( false == is_null( $objGlReconciliationFilter->getReconciledDate() )
					&& strtotime( $objGlReconciliationFilter->getReconciledDate() ) >= strtotime( date( 'm/d/Y', strtotime( '04/18/2014' ) ) ) ) {
				$strOuterCondition = 'AND gd.id IN ( 	SELECT
														gl_detail_id
													FROM
														gl_reconciliation_exceptions gre
													WHERE
														gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
														AND cid = ' . ( int ) $intCid . '
												)';
			}

			if( true == valId( $intSystemReconciliationId ) ) {
				$strUnionForSystemReconciliation = ' OR ( gd.gl_reconciliation_id = ' . ( int ) $intSystemReconciliationId . $strCondition . ' ) ';
			}
		}

		$strSql	= '
			WITH cte_gl_details AS (
				SELECT
					gd.*,
					gh.memo AS gh_memo,
					gh.post_date AS gh_post_date,
					gh.reference_id AS gh_reference_id
				FROM
					gl_details gd
					JOIN gl_headers gh ON gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month
					LEFT JOIN gl_reconciliations gr ON gd.cid = gr.cid AND gd.gl_reconciliation_id = gr.id
				WHERE
					gh.cid = ' . ( int ) $intCid . '
					AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' )
					AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					AND ( gr.id IS NULL OR gr.is_system = 0 )
					AND gd.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . $strUnionForSystemReconciliation . '

				UNION ALL

				SELECT
					gd.*,
					gh.memo AS gh_memo,
					gh.post_date AS gh_post_date,
					gh.reference_id AS gh_reference_id
				FROM
					gl_details gd
					JOIN gl_headers gh ON gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month
					LEFT JOIN gl_reconciliations gr ON gd.cid = gr.cid AND gd.gl_reconciliation_id = gr.id
				WHERE
					gh.cid = ' . ( int ) $intCid . '
					AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' )
					AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					AND ( gr.id IS NULL OR gr.is_system = 0 )
					' . $strOuterCondition . ' ' . $strCondition . '
			)

			SELECT DISTINCT ON ( ad.id )
				gd.gl_header_id,
				gd.accrual_gl_account_id,
				gd.cash_gl_account_id,
				gd.property_id,
				ad.deposit_amount AS amount,
				gd.gh_post_date AS post_date,
				gd.post_month,
				ad.deposit_number,
				ad.deposit_number AS transaction_id,
				gtt.description AS gl_transaction_type,
				gd.gl_reconciliation_id,
				gd.gh_memo,
				gd.memo AS description,
				gd.gh_reference_id AS ar_deposit_id,
				ad.origin_ar_deposit_id
			FROM
				ar_deposits ad
				JOIN cte_gl_details gd ON gd.cid = ad.cid AND gd.gh_reference_id = ad.id AND gd.gl_transaction_type_id = ad.gl_transaction_type_id
				JOIN gl_transaction_types gtt ON gd.gl_transaction_type_id = gtt.id
			WHERE
				ad.cid = ' . ( int ) $intCid . '
				AND ad.deposit_amount != 0
				' . $strUnCheckedArDepositIdsCondition . '
		    ORDER BY
				ad.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchReconciledArDepositByArDepositIdByCid( $intArDepositId, $intCid, $objClientDatabase ) {

		if( true == is_null( $intArDepositId ) ) {
			return NULL;
		}

		$strSql	= 'SELECT
						gh.reference_id,
						gh.id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.reference_id = ' . ( int ) $intArDepositId . '
						AND gh.gl_transaction_type_id IN ( ' . implode( ', ', CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds ) . ' )
						AND gd.gl_reconciliation_id IS NOT NULL
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						LIMIT 1';

		return self::fetchGlHeader( $strSql, $objClientDatabase );
	}

	public static function fetchAllJournalEntriesByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationFilter, $intCid, $objClientDatabase, $intSystemBankRecId = NULL ) {

		if( true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strCondition						= '';
		$strOuterCondition					= ' gd.gl_reconciliation_id IS NULL ';
		$strUnCheckedJEIdsCondition			= '';
		$strConditionForSystemReconciliation	= '';

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
				$strCondition .= ' AND gh.post_date >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
			}

			if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() ) {

				$strCondition = '';

				if( true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
					$strCondition .= ' AND gh.post_month >= \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
				}

				if( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
					$strCondition .= ' AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
				}
			}

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND	( gd.accrual_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' ) OR
											gd.cash_gl_account_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getGlAccountIds() ) ) . ' )
										) ';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', array_unique( $objGlReconciliationFilter->getPropertyIds() ) ) . ' ) ';
			}

			if( false == is_null( $objGlReconciliationFilter->getReconciledDate() )
			&& strtotime( $objGlReconciliationFilter->getReconciledDate() ) >= strtotime( date( 'm/d/Y', strtotime( '04/18/2014' ) ) ) ) {
				$strOuterCondition = ' gd.id IN (	SELECT
														gl_detail_id
													FROM
														gl_reconciliation_exceptions gre
													WHERE
														gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
														AND cid = ' . ( int ) $intCid . '
												)';
			}

			if( true == valIntArr( $objGlReconciliationFilter->getGeneralJournalIds() ) ) {
				$strUnCheckedJEIdsCondition .= ' AND gh.id IN( ' . implode( ',', $objGlReconciliationFilter->getGeneralJournalIds() ) . ' )';
			}

			if( true == valId( $intSystemBankRecId ) ) {
				$strConditionForSystemReconciliation = ' OR ( gd.gl_reconciliation_id = ' . $intSystemBankRecId . $strCondition . $strUnCheckedJEIdsCondition . ' ) ';
			}
		}

		$strSql	= 'SELECT
						gd.gl_header_id,
						gd.id AS gl_detail_id,
						gd.property_id,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount,
						gh.post_date,
						gd.memo AS description,
						p.property_name,
						gtt.description as gl_transaction_type,
						gh.header_number,
						gd.gl_reconciliation_id,
						gd.is_confidential,
						gh.memo,
						gh.header_number AS transaction_id,
						gh.offsetting_gl_header_id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month )
						JOIN gl_transaction_types gtt ON ( gh.gl_transaction_type_id = gtt.id )
						JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
						LEFT JOIN gl_reconciliations gr ON ( gd.cid = gr.cid AND gd.gl_reconciliation_id = gr.id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS false
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						AND ( ( gd.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . $strUnCheckedJEIdsCondition . ' ) OR ( ' . $strOuterCondition . ' ' . $strCondition . $strUnCheckedJEIdsCondition . ' ) ' . $strConditionForSystemReconciliation . ' )
					ORDER BY
						gd.gl_header_id,
						gd.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllGlHeadersByGlHeaderIdsByCid( $arrintGlHeaderIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlHeaderIds ) )	return NULL;

		$strSql = 'WITH seleced_ap_headers AS (
						SELECT
							gh1.cid,
							gh1.gl_transaction_type_id,
							ah1.ap_payment_id,
							ah1.id AS payment_ap_header_id
						FROM
							ap_headers ah1
							JOIN ap_details ad1 ON ( ah1.cid = ad1.cid AND ah1.id = ad1.ap_header_id )
							JOIN ap_allocations aa1 ON ad1.cid = aa1.cid AND ( ad1.id = aa1.credit_ap_detail_id OR ( ad1.id = aa1.charge_ap_detail_id AND ad1.is_cross_allocation = true ) )
							JOIN gl_headers gh1 ON ( aa1.cid = gh1.cid AND aa1.id = gh1.reference_id AND aa1.gl_transaction_type_id = gh1.gl_transaction_type_id AND gh1.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						WHERE
							ah1.cid = ' . ( int ) $intCid . '
							AND gh1.id IN ( ' . implode( ',', $arrintGlHeaderIds ) . ' )
							AND ah1.is_template = false
					)

				SELECT
					gh.*
				FROM
					gl_headers gh
					JOIN ap_allocations aa ON gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' )
					JOIN ap_details ad ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id OR ( aa.charge_ap_detail_id = ad.id AND ad.is_cross_allocation = true ) )
					JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
					JOIN seleced_ap_headers sap ON ( sap.cid = ah.cid AND sap.ap_payment_id = ah.ap_payment_id AND sap.gl_transaction_type_id = gh.gl_transaction_type_id AND sap.payment_ap_header_id = ah.id )
				WHERE
					gh.cid = ' . ( int ) $intCid . '
					AND ah.ap_payment_id IS NOT NULL;';

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlHeadersByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_headers
					WHERE
						reference_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApHeaderGlTransactionTypeIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND offsetting_gl_header_id IS NULL';

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlHeadersByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					from 
						gl_headers
					WHERE
						reference_id = ' . ( int ) $intApHeaderId . '
						AND gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApHeaderGlTransactionTypeIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND offsetting_gl_header_id IS NULL';

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeadersByApAllocationIdsByGlTransactionTypeIdByCid( $arrintApAllocationIds, $intGlTransactionTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApAllocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_headers gh
					WHERE
						gh.reference_id IN ( ' . implode( ',', $arrintApAllocationIds ) . ' )
						AND gh.gl_transaction_type_id = ' . ( int ) $intGlTransactionTypeId . '
						AND gh.cid = ' . ( int ) $intCid;

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeadersByOffsettingGlHeaderIdsByCid( $arrintOffsettingGlHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintOffsettingGlHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_headers
					WHERE
						offsetting_gl_header_id IN ( ' . implode( ',', $arrintOffsettingGlHeaderIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeaderByOffsettingGlHeaderIdByGlHeaderStatusTypeByCid( $intOffsettingGlHeaderId, $intGlHeaderStatusTypeId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intOffsettingGlHeaderId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_headers
					WHERE
						offsetting_gl_header_id =' . ( int ) $intOffsettingGlHeaderId . '
						AND cid = ' . ( int ) $intCid . '
						AND gl_header_status_type_id = ' . ( int ) $intGlHeaderStatusTypeId;

		return self::fetchGlHeader( $strSql, $objClientDatabase );
	}

	public static function fetchExportedArDepositByArDepositIdByCid( $intArDepositId, $intCid, $objClientDatabase ) {

		if( false == valId( $intArDepositId ) ) {
			return NULL;
		}

		$strSql	= 'SELECT
						gh.reference_id,
						gh.id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.reference_id = ' . ( int ) $intArDepositId . '
						AND gh.gl_transaction_type_id NOT IN ( ' . implode( ', ', CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds ) . ' )
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					LIMIT 1';

		return self::fetchGlHeader( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedGlHeaderTemplatesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objPagination, $objClientDatabase, $objGlHeaderSchedulesFilter = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strOrderBy				= 'gh.id DESC';
		$arrstrAndConditions	= [];

		if( true == valObj( $objGlHeaderSchedulesFilter, 'CGlHeaderSchedulesFilter' ) ) {

			if( true == valArr( $objGlHeaderSchedulesFilter->getPropertyIds() ) ) {
				$arrstrAndConditions[] = 'gd.property_id IN ( ' . implode( ',', $objGlHeaderSchedulesFilter->getPropertyIds() ) . ' )';
			} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
				$arrstrAndConditions[] = 'gd.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			if( true == valArr( $objGlHeaderSchedulesFilter->getStatusTypes() )
			    && true == valArr( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() )
			    && ( in_array( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_RECURRING, $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) ) {

				$strTempConditions = '';
				foreach( $objGlHeaderSchedulesFilter->getStatusTypes() as $strStatusType ) {

					if( true == valStr( $strTempConditions ) ) {
						$strTempConditions .= ' OR ';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_DISABLED == strtolower( $strStatusType ) ) {
						$strTempConditions .= '( gh.gl_header_schedule_id IS NOT NULL AND ghs.disabled_by IS NOT NULL AND ghs.disabled_on IS NOT NULL )';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_APPROVED == strtolower( $strStatusType ) ) {
						$strTempConditions .= '( gh.gl_header_schedule_id IS NOT NULL AND ghs.approved_by IS NOT NULL AND ghs.approved_on IS NOT NULL AND ghs.disabled_by IS NULL AND ghs.disabled_on IS NULL )';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_PENDING == strtolower( $strStatusType ) ) {
						$strTempConditions .= ' ( gh.gl_header_schedule_id IS NOT NULL AND ghs.approved_by IS NULL AND ghs.approved_on IS NULL AND ghs.disabled_by IS NULL AND ghs.disabled_on IS NULL )';
					}
				}

				if( in_array( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_MANUAL, $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) {
					$strTempConditions .= ' OR ( gh.gl_header_schedule_id IS NULL )';
				}

				if( true == valStr( $strTempConditions ) ) {
					$arrstrAndConditions[] = ' ( ' . $strTempConditions . ' ) ';
				}
			}

			if( true == valStr( $objGlHeaderSchedulesFilter->getName() ) ) {
				$arrstrAndConditions[] = '( gh.template_name ILIKE \'%' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getName() ) ) ) . '%\'
				                          OR gh.reference ILIKE \'%' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getName() ) ) ) . '%\'
				                          OR gh.memo ILIKE \'%' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getName() ) ) ) . '%\')';
			}

			if( true == valArr( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) {
				$arrstrAndConditions[] = ( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_RECURRING == current( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) ? 'gh.gl_header_schedule_id IS NOT NULL' : 'gh.gl_header_schedule_id IS NULL';
			}

			if( true == valId( $objGlHeaderSchedulesFilter->getGlHeaderId() ) ) {
				$arrstrAndConditions[] = 'gh.id = ' . ( int ) $objGlHeaderSchedulesFilter->getGlHeaderId();
			}

			if( true == valStr( $objGlHeaderSchedulesFilter->getSortBy() ) ) {

				$strOrderBy = addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getSortBy() ) ) );

				if( true == in_array( $strOrderBy, [ 'template_name', 'property_name', 'reference', 'memo' ] ) ) {
					$strOrderBy = $objClientDatabase->getCollateSort( $strOrderBy );
				}

				if( true == valStr( $objGlHeaderSchedulesFilter->getSortDirection() ) ) {

					$strSortDirection = addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getSortDirection() ) ) );
					if( 'gl_header_schedule_id' == $strOrderBy ) {
						if( 'asc' == $strSortDirection ) {
							$strSortDirection = 'desc';
						} else {
							$strSortDirection = 'asc';
						}
					}
					$strOrderBy .= ' ' . $strSortDirection;
				}

			}
		} else {
			$arrstrAndConditions[] = 'gd.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		$strConditions = ( true == valArr( $arrstrAndConditions ) ) ? ' AND ' . implode( ' AND ', $arrstrAndConditions ) : '';

		$strSql = 'SELECT
						DISTINCT ( gh.id ),
						gh.template_name,
						gh.memo,
						gh.reference,
						gh.template_gl_header_id,
						gh.gl_header_schedule_id,
						ghs.frequency_id,
						ghs.next_post_date,
						ghs.start_date,
						ghs.end_date,
						ghs.approved_by,
						ghs.approved_on,
						ghs.disabled_by,
						ghs.disabled_on,
						CASE
							WHEN ghs.id IS NULL AND gh.is_reverse IS TRUE THEN \'Y\'
							WHEN ghs.is_reverse_next_month = 1 THEN \'Y\'
							ELSE \'N\'
						END as reverses,
						(
						SELECT
							SUM ( CASE
									WHEN gd.template_amount_type_id IS NOT NULL AND gd.template_balance_from_id IS NOT NULL THEN 0
									WHEN gd.amount > 0 THEN gd.amount
									ELSE 0
								END )
						FROM
							gl_details gd
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND gd.gl_header_id = gh.id
							AND gd.cid = gh.cid
						) AS debit_amount,
						(
						SELECT
							CASE
								WHEN 1 < COUNT ( DISTINCT gd.property_id ) THEN COUNT ( DISTINCT gd.property_id )::TEXT
								ELSE p.property_name
							END AS property_name
						FROM
							gl_details gd
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND gd.gl_header_id = gh.id
							AND gd.cid = gh.cid
						) AS property_name,
						CASE
							WHEN gh.gl_header_schedule_id IS NULL THEN \'Z\'
							WHEN ghs.disabled_by IS NOT NULL AND ghs.disabled_on IS NOT NULL THEN initcap( \'' . CGlHeaderSchedulesFilter::STATUS_TYPE_DISABLED . '\' )
							WHEN ghs.approved_by IS NOT NULL AND ghs.approved_on IS NOT NULL THEN initcap( \'' . CGlHeaderSchedulesFilter::STATUS_TYPE_APPROVED . '\' )
							WHEN ghs.approved_by IS NULL AND ghs.approved_on IS NULL THEN initcap( \'' . CGlHeaderSchedulesFilter::STATUS_TYPE_PENDING . '\' )
							ELSE \'Z\'
						END AS status
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gd.gl_transaction_type_id = gh.gl_transaction_type_id )
						LEFT JOIN gl_header_schedules ghs ON ( gh.cid = ghs.cid AND gh.gl_header_schedule_id = ghs.id )
						JOIN properties p ON ( p.cid = gd.cid AND p.id = gd.property_id )
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS true
						AND pgs.activate_standard_posting IS true
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED . '
						' . $strConditions . '
						ORDER BY ' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeaderTemplatesCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objGlHeaderSchedulesFilter = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$boolUseAndKeyword		= false;
		$arrstrAndConditions	= [];

		if( true == valObj( $objGlHeaderSchedulesFilter, 'CGlHeaderSchedulesFilter' ) ) {

			if( true == valArr( $objGlHeaderSchedulesFilter->getPropertyIds() ) ) {
				$arrstrAndConditions[] = 'gd.property_id IN ( ' . implode( ',', $objGlHeaderSchedulesFilter->getPropertyIds() ) . ' )';
			} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
				$arrstrAndConditions[] = 'gd.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			if( true == valArr( $objGlHeaderSchedulesFilter->getStatusTypes() )
			    && true == valArr( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() )
			    && ( in_array( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_RECURRING, $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) ) {

				$strTempConditions = '';
				foreach( $objGlHeaderSchedulesFilter->getStatusTypes() as $strStatusType ) {

					if( true == valStr( $strTempConditions ) ) {
						$strTempConditions .= ' OR ';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_DISABLED == strtolower( $strStatusType ) ) {
						$strTempConditions .= '( gh.gl_header_schedule_id IS NOT NULL AND ghs.disabled_by IS NOT NULL AND ghs.disabled_on IS NOT NULL )';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_APPROVED == strtolower( $strStatusType ) ) {
						$strTempConditions .= '( gh.gl_header_schedule_id IS NOT NULL AND ghs.approved_by IS NOT NULL AND ghs.approved_on IS NOT NULL AND ghs.disabled_by IS NULL AND ghs.disabled_on IS NULL )';
					}

					if( CGlHeaderSchedulesFilter::STATUS_TYPE_PENDING == strtolower( $strStatusType ) ) {
						$strTempConditions .= ' ( gh.gl_header_schedule_id IS NOT NULL AND ghs.approved_by IS NULL AND ghs.approved_on IS NULL AND ghs.disabled_by IS NULL AND ghs.disabled_on IS NULL )';
					}
				}

				if( in_array( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_MANUAL, $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) {
					$strTempConditions .= ' OR ( gh.gl_header_schedule_id IS NULL )';
				}

				if( true == valStr( $strTempConditions ) ) {
					$arrstrAndConditions[] = ' ( ' . $strTempConditions . ' ) ';
				}
			}

			if( true == valStr( $objGlHeaderSchedulesFilter->getName() ) ) {
				$arrstrAndConditions[] = 'gh.template_name ILIKE \'%' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $objGlHeaderSchedulesFilter->getName() ) ) ) . '%\'';
			}

			if( true == valArr( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) {
				$arrstrAndConditions[] = ( CGlHeaderSchedulesFilter::TEMPLATE_TYPE_RECURRING == current( $objGlHeaderSchedulesFilter->getScheduledGlHeaderTypes() ) ) ? 'gh.gl_header_schedule_id IS NOT NULL' : 'gh.gl_header_schedule_id IS NULL';
			}

			if( true == valId( $objGlHeaderSchedulesFilter->getGlHeaderId() ) ) {
				$arrstrAndConditions[] = 'gh.id = ' . ( int ) $objGlHeaderSchedulesFilter->getGlHeaderId();
			}
		}

		$strConditions = '';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT ( gh.id ) )
					FROM
							gl_headers gh
							JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gd.gl_transaction_type_id = gh.gl_transaction_type_id )
							LEFT JOIN gl_header_schedules ghs ON ( gh.cid = ghs.cid AND gh.gl_header_schedule_id = ghs.id )
							LEFT JOIN properties p ON ( p.cid = gd.cid AND p.id = gd.property_id )
							JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS true
						AND gd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pgs.activate_standard_posting IS true
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED . '
						' . $strConditions;

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

	public static function fetchGlHeadersByGlHeaderScheduleIdsByCids( $arrintGlHeaderScheduleIds, $arrintCids, $objClientDatabase, $boolIsTemplate = true ) {

		if( false == valIntArr( $arrintGlHeaderScheduleIds ) || false == valIntArr( $arrintCids ) ) return false;
		$strSqlConditions = ( true == $boolIsTemplate ) ? 'AND gh.is_template IS true' : '';
		$strSql = 'SELECT
						gh.*
					FROM
						gl_headers gh
					WHERE
						gh.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						' . $strSqlConditions . '
						AND gh.gl_header_schedule_id IN ( ' . implode( ',', $arrintGlHeaderScheduleIds ) . ' ) ';

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchGLHeaderTemplateCountByTemplateGLHeaderIdByCid( $intTemplateGLHeaderId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						count( id ) as gl_header_count
					FROM
						gl_headers gh
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.template_gl_header_id = ' . ( int ) $intTemplateGLHeaderId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeadersByCidBySearchKeywords( $intCid, $strSearchedString, $arrmixRequestData, $objDatabase ) {

		$intAssetTypeId					= ( int ) $arrmixRequestData['asset_type_id'];
		$strAccountType					= $arrmixRequestData['account_type'];
		$strSortOrder					= '';
		$strGlAccountType					= '';
		$strSortedColumn				= '';
		$strWhereCondition				= '';
		$strGlAccountUsageTypeJoinCondition	= '';
		$strGlAccountUsageTypeWhereCondition	= '';

		if( valId( $intAssetTypeId ) && valStr( $strAccountType ) ) {
			switch( $strAccountType ) {
				case 'consumption':
					if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

						$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::DEPRECIATION;
						$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )';
					}

					if( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $intAssetTypeId ) {
						$strGlAccountType					= ' AND glgt.id = ' . CGlAccountType::EXPENSES;
					}
					break;

				case 'debit':
					if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

						$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::FIXED_ASSET;
						$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )';
					} elseif( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $intAssetTypeId ) {

						$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::INVENTORY;
						$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )';
					}

					if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED != $intAssetTypeId && CAssetType::INVENTORY_INVENTORIED_AND_TRACKED != $intAssetTypeId ) {
						$strGlAccountType					= ' AND glgt.id = ' . CGlAccountType::EXPENSES;
					}
					break;

				case 'credit':
					if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $intAssetTypeId ) {

						$strGlAccountUsageTypeWhereCondition	= ' AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::ACCUMULATED_DEPRECIATION;
						$strGlAccountUsageTypeJoinCondition	= ' JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )';
					}
					break;

				default:
					// nothing to do
			}
		}

		if( true == valStr( $strSearchedString ) ) {

			$strSortedColumn	= ',CASE
										WHEN ( gat.formatted_account_number || \' \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
											OR ( lower( gat.name ) = lower( E\'' . $strSearchedString . '\' ) )
											OR ( gat.formatted_account_number = E\'' . $strSearchedString . '\' )
											OR ( gat.formatted_account_number || \': \' || lower( gat.name ) = lower( E\'' . $strSearchedString . '\' )
										)
										THEN 1
										WHEN ( gat.formatted_account_number ILIKE E\'' . $strSearchedString . '%\' OR gat.name ILIKE E\'' . $strSearchedString . '%\' )
										THEN 2
										ELSE 3
									END AS sort_order';
			$strSortOrder		= ' ORDER BY sort_order';

			$strWhereCondition	= ' AND ( gat.formatted_account_number ILIKE E\'%' . $strSearchedString . '%\' OR gat.name ILIKE E\'%' . $strSearchedString . '%\' )';

		}

		$strSql = 'SELECT
							DISTINCT ga.*,
							glat.name AS gl_account_type_name,
							gat.formatted_account_number AS account_number,
							gat.name AS account_name,
							gat.gl_group_id,
							gat.gl_account_type_id
							' . $strSortedColumn . '
						FROM
							gl_accounts ga
							JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
							JOIN gl_trees glt ON (gat.cid = glt.cid AND gat.gl_tree_id = glt.id)
							JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
							' . $strGlAccountUsageTypeJoinCondition . '
							JOIN gl_charts gc ON (glt.gl_chart_id = gc.id AND glt.cid = gc.cid)
						WHERE
							gat.cid = ' . ( int ) $intCid . $strGlAccountType . '
							AND	gc.system_code = \'' . addslashes( CGlChart::PROPERTY_DEFAULT_SYSTEM_CODE ) . '\'
							' . $strGlAccountUsageTypeWhereCondition . $strWhereCondition . '
							AND	gat.disabled_by IS NULL
							AND gat.is_default=1' . $strSortOrder;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlHeadersBySearchedStringByPropertyIdsByLookupTypeByCid( $intCid, $strSearchString, $strLookupType, $objGeneralJournalFilter, $arrintPropertyIds, $objDatabase ) {

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $strSearchString );

		$arrstrAdvancedSearchParameters = [];
		$strSql = '';

		if( CGlHeader::LOOKUP_TYPE_GENERAL_JOURNAL_PROPERTIES == $strLookupType ) {

			$strOrderBy							= NULL;
			$strSqlForExactMatch				= NULL;
			$strConditionForExcludingExactMatch	= NULL;
			$arrstrAdvancedSearchParameters		= [];

			$arrstrArrayFilteredExplodedSearch = array_filter( $arrstrFilteredExplodedSearch );

			if( false == empty( $arrstrArrayFilteredExplodedSearch ) ) {

				$strSearchedString					= addslashes( trim( $strSearchString ) );
				$strOrderBy							= ' ORDER BY order_number DESC';
				$strConditionForExcludingExactMatch	= ' AND p.id NOT IN ( SELECT id FROM exact_match )';

				$strSqlForExactMatch	= 'WITH exact_match AS (
											SELECT
												p.id,
												CASE
													WHEN p.lookup_code IS NULL THEN p.property_name
													ELSE p.lookup_code || \' - \' || p.property_name
												END AS property_name,
												p.order_num,
												2 AS order_number,
												to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month
											FROM
												properties p
												JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
											WHERE
												p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
												AND p.cid = ' . ( int ) $intCid . '
												AND LOWER( p.property_name ) = LOWER( \'' . $strSearchedString . '\' )
												AND pgs.activate_standard_posting IS true
											ORDER BY lower( p.property_name ) ASC, p.order_num
										)
										( SELECT * FROM exact_match ) UNION';
			}

			$strSql 					= '( SELECT
											p.id,
											CASE
												WHEN p.lookup_code IS NULL THEN p.property_name
												ELSE p.lookup_code || \' - \' || p.property_name
											END AS property_name,
											p.order_num,
											1 AS order_number,
											to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month
										FROM
											properties p
											JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
										WHERE
											p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
											AND p.cid = ' . ( int ) $intCid .
														$strConditionForExcludingExactMatch .
														' AND pgs.activate_standard_posting IS true';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, '( p.property_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\' OR p.lookup_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\' )' );
				}
			}

			if( true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY LOWER( p.property_name ) ASC, p.order_num ) ' . $strOrderBy;

			$strSql	= $strSqlForExactMatch . $strSql;

		} elseif( true == valArr( $arrstrFilteredExplodedSearch ) ) {

			$boolUseAndKeyword   	= false;
			$arrstrAndConditions 	= [];
			$strJoinCondition		= '';

			if( true == is_numeric( $objGeneralJournalFilter->getGlAccountId() ) ) {
				$arrstrAndConditions[] = '( gd.accrual_gl_account_id = ' . ( int ) $objGeneralJournalFilter->getGlAccountId() . ' OR gd.cash_gl_account_id = ' . ( int ) $objGeneralJournalFilter->getGlAccountId() . ' )';
			}

			if( true == is_numeric( $objGeneralJournalFilter->getGlAccountId() ) ) {
				$arrstrAndConditions[] = '( gd.accrual_gl_account_id = ' . ( int ) $objGeneralJournalFilter->getGlAccountId() . ' OR gd.cash_gl_account_id = ' . ( int ) $objGeneralJournalFilter->getGlAccountId() . ' )';
			}

			if( true == valArr( $objGeneralJournalFilter->getPropertyIds() ) ) {
				$arrstrAndConditions[] = 'gd.property_id IN ( ' . implode( ',', $objGeneralJournalFilter->getPropertyIds() ) . ' )';
			}

			if( true == valStr( $objGeneralJournalFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objGeneralJournalFilter->getPostDateFrom() ) ) {
				$arrstrAndConditions[] = 'gh.post_date >= \'' . date( 'Y-m-d', strtotime( $objGeneralJournalFilter->getPostDateFrom() ) ) . ' 00:00:00\'';
			}

			if( true == valStr( $objGeneralJournalFilter->getPostDateTo() ) && true == CValidation::validateDate( $objGeneralJournalFilter->getPostDateTo() ) ) {
				$arrstrAndConditions[] = 'gh.post_date <= \'' . date( 'Y-m-d', strtotime( $objGeneralJournalFilter->getPostDateTo() ) ) . ' 23:59:59\'';
			}

			if( true == valStr( $objGeneralJournalFilter->getReference() ) ) {
				$arrstrAndConditions[] = 'gh.reference ILIKE \'%' . addslashes( $objGeneralJournalFilter->getReference() ) . '%\'';
			}

			if( true == valStr( $objGeneralJournalFilter->getCreatedBy() ) ) {
				$arrstrAndConditions[] = 'gh.created_by = ' . ( int ) $objGeneralJournalFilter->getCreatedBy();
			}

			if( true == valStr( $objGeneralJournalFilter->getPostMonth() ) ) {
				$arrstrAndConditions[] = 'gh.post_month = \'' . date( 'm-d-Y', strtotime( $objGeneralJournalFilter->getPostMonth() ) ) . ' \'';
			}

			if( true == valArr( $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) ) {

				$arrstrAndConditions[] = 'gh.gl_header_status_type_id IN ( ' . implode( ',', $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) . ' ) ';

				if( 1 == \Psi\Libraries\UtilFunctions\count( $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) && true == in_array( CGlHeaderStatusType::REVERSED, $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) ) {
					$arrstrAndConditions[] = 'gh.offsetting_gl_header_id IS NOT NULL';
				}

				if( 1 == \Psi\Libraries\UtilFunctions\count( $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) && true == in_array( CGlHeaderStatusType::POSTED, $objGeneralJournalFilter->getGlHeaderStatusTypeIds() ) ) {
					$arrstrAndConditions[] = 'gh.offsetting_gl_header_id IS NULL';
				}
			}

			if( true == is_numeric( $objGeneralJournalFilter->getJobId() ) ) {
				$arrstrAndConditions[] = 'jp.job_id = ' . ( int ) $objGeneralJournalFilter->getJobId();
				$strJoinCondition = 'JOIN job_phases jp ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id )';
			}

			$strConditions = '';

			if( true == valArr( $arrstrAndConditions ) ) {
				$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
			}

			if( false == valArr( $arrintPropertyIds ) ) {
				return;
			}

			$strSql = ' SELECT
							DISTINCT( gh.id ),
							gh.header_number,
							gh.post_date,
							gh.post_month,
							gh.memo,
							gh.created_by,
							gh.created_on,

							( SELECT SUM ( CASE WHEN gd.amount > 0 THEN gd.amount ELSE 0 END ) FROM gl_details gd WHERE gd.cid = ' . ( int ) $intCid . ' AND gd.gl_header_id = gh.id AND gd.cid = gh.cid )
							AS debit_amount,

							CASE
								WHEN ( 1 < ( SELECT COUNT( DISTINCT property_id) FROM gl_details gd WHERE gd.cid = ' . ( int ) $intCid . ' AND gd.gl_header_id = gh.id AND gd.cid = gh.cid ) )
								THEN \'Multiple\'
								ELSE p.property_name
							END AS property_name
						FROM
							gl_headers gh
							LEFT JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gh.cid = gd.cid )
							LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
							LEFT JOIN properties p ON ( p.id = gd.property_id AND p.cid = gd.cid ) '
							. $strJoinCondition . '
						WHERE
							gh.cid = ' . ( int ) $intCid . '
							AND gh.id IN (
											SELECT
												DISTINCT( gd.gl_header_id )
											FROM
												gl_details gd
												JOIN property_gl_settings pgs ON ( gd.property_id = pgs.property_id AND gd.cid = pgs.cid )
											WHERE
	 											gd.cid = ' . ( int ) $intCid . '
												AND gd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
												AND pgs.activate_standard_posting IS true
											)
							AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . $strConditions;

			$arrstrAdvancedSearchParameters = [];

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'gh.header_number::text ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			if( true == $objGeneralJournalFilter->getIsShowExportedTransactions() ) {
				$strSql .= ' AND aea.id IS NOT NULL';
			} elseif( true == is_numeric( $objGeneralJournalFilter->getIsShowExportedTransactions() ) && false == $objGeneralJournalFilter->getIsShowExportedTransactions() ) {
				$strSql .= 'AND aea.id IS NULL';
			}

			$strSql .= ' AND gh.reference_id IS NULL
					GROUP BY
						 gh.cid,
						 gh.id,
						 gh.header_number,
						 gh.post_date,
						 gh.post_month,
						 gh.memo,
						 gh.created_by,
						 gh.created_on,
						 p.property_name';

			if( true == is_numeric( $objGeneralJournalFilter->getAmountMin() ) ) {

				$strCaseCondition = ( true == is_numeric( $objGeneralJournalFilter->getAmountMin() ) ) ? 'CASE WHEN gd.amount > 0 THEN gd.amount ELSE 0 END ' : 'gd.amount';

				$strSql .= '
					HAVING
							  ( SELECT SUM (' . $strCaseCondition . ') FROM gl_details gd WHERE gd.gl_header_id = gh.id AND gd.cid = gh.cid )

						>=' . $objGeneralJournalFilter->getAmountMin();
				$boolUseAndKeyword	  = true;

			}

			if( true == is_numeric( $objGeneralJournalFilter->getAmountMax() ) ) {

				$strCaseCondition = ( true == is_numeric( $objGeneralJournalFilter->getAmountMin() ) ) ? 'CASE WHEN gd.amount > 0 THEN gd.amount ELSE 0 END ' : 'gd.amount';

				if( true == $boolUseAndKeyword ) {
					$strSql .= ' AND';
				} else {
					$strSql .= ' HAVING';
				}
				$strSql .= ' ( SELECT SUM (' . $strCaseCondition . ' ) FROM gl_details gd WHERE gd.gl_header_id = gh.id AND gd.cid = gh.cid )
						<=' . $objGeneralJournalFilter->getAmountMax();
			}

			$strSql .= ' ORDER BY gh.id DESC ';
		}

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedGlHeadersByApContractIdByCid( $intApContractId, $intCid, $objClientDatabase, $objPagination = NULL ) {

		if( false == valId( $intApContractId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT gh.*,
						CASE WHEN
						( 1 < ( SELECT COUNT ( DISTINCT property_id) FROM gl_details gd
								WHERE
									gd.cid = gh.cid
									AND gd.gl_header_id = gh.id
									AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
									AND gd.cid = ' . ( int ) $intCid . ' ) )
							THEN \'Multiple\'
							ELSE p.property_name
						END AS property_name,
						SUM( gd.amount ) AS gl_entry_amount
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gd.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' )
						JOIN properties p ON ( p.cid = gh.cid AND gd.property_id = p.id )
						JOIN job_phases jp ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id )
						JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
						JOIN ap_contracts ac ON ( ac.cid = j.cid AND ac.job_id = j.id AND ac.id = gd.ap_contract_id )
					WHERE
						ac.id = ' . ( int ) $intApContractId . '
						AND gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS FALSE
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id <>  \'' . CGlHeaderStatusType::DELETED . '\'
					GROUP BY
						gh.id,
						gh.cid,
						property_name
					ORDER BY
						gh.created_on DESC,
						gh.header_number DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

			return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedGlHeadersByJobIdByCid( $intJobId, $intCid, $objClientDatabase, $objGeneralJournalFilter = NULL, $objPagination = NULL ) {

		if( false == valId( $intJobId ) ) {
			return NULL;
		}

		if( true == valStr( $objGeneralJournalFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objGeneralJournalFilter->getPostDateFrom() ) ) {
			$arrstrAndConditions[] = 'gh.post_date >= \'' . date( 'Y-m-d', strtotime( $objGeneralJournalFilter->getPostDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objGeneralJournalFilter->getPostDateTo() ) && true == CValidation::validateDate( $objGeneralJournalFilter->getPostDateTo() ) ) {
			$arrstrAndConditions[] = 'gh.post_date <= \'' . date( 'Y-m-d', strtotime( $objGeneralJournalFilter->getPostDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objGeneralJournalFilter->getJeHeaderNumber() ) ) {
			$arrstrAndConditions[] = 'gh.header_number = ' . ( int ) $objGeneralJournalFilter->getJeHeaderNumber();
		}

		if( true == valStr( $objGeneralJournalFilter->getPostMonth() ) && true == CValidation::validateDate( $objGeneralJournalFilter->getPostMonth() ) ) {
			$arrstrAndConditions[] = 'gh.post_month = \'' . date( 'm-d-Y', strtotime( $objGeneralJournalFilter->getPostMonth() ) ) . ' \'';
		}

		$strConditions = '';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql = 'SELECT
						DISTINCT gh.id,
						gh.created_by,
						gh.gl_header_status_type_id,
						gh.header_number,
						gh.post_date,
						gh.post_month,
						gh.memo,
						SUM( gd.amount ) AS gl_entry_amount
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
						JOIN job_phases jp ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id )
						JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
					WHERE
						j.id = ' . ( int ) $intJobId . '
						AND gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS FALSE
						AND gh.gl_header_status_type_id <>  \'' . CGlHeaderStatusType::DELETED . '\''
						. $strConditions . '
					GROUP BY
						gh.cid,
						gh.id,
						gh.created_by,
						gh.gl_header_status_type_id,
						gh.header_number,
						gh.post_date,
						gh.post_month,
						gh.memo';

		if( true == is_numeric( $objGeneralJournalFilter->getAmountMin() ) ) {

			$strSql .= '
				HAVING
						SUM (gd.amount)
					>=' . $objGeneralJournalFilter->getAmountMin();
			$boolUseAndKeyword	  = true;

		}

		if( true == is_numeric( $objGeneralJournalFilter->getAmountMax() ) ) {

			if( true == $boolUseAndKeyword ) {
				$strSql .= ' AND';
			} else {
				$strSql .= ' HAVING';
			}
			$strSql .= ' SUM (gd.amount)
					<=' . $objGeneralJournalFilter->getAmountMax();
		}

		$strSql .= ' ORDER BY
						gh.header_number DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlHeadersCountByGlBookIdByCid( $intGlBookId, $intCid, $objClientDatabase ) {

		$strWhere = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_book_id = ' . ( int ) $intGlBookId . ' ';
		return parent::fetchRowCount( $strWhere, 'gl_headers', $objClientDatabase );
	}

	public static function fetchGLDetailsByGlTransactionTypeIdsByExportBatchIdByGlTreeIdByGlExportBookTypeIdByCid( $arrintDepositeGlTransactionTypeIds, $intGlExportBatchId, $intGlTreeId, $intGlExportBookTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDepositeGlTransactionTypeIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						gd.id, gd.property_id, gd.gl_header_id, gd.amount, gd.cash_gl_account_id,';
		$strSql .= 'CONCAT( to_char( ad.post_date, \'mm/yyyy\' ), \'-\',ad.deposit_number, \' Deposited \', to_char( ad.transaction_datetime, \'mm/\DD/yyyy\' ), \'-Ctrl#\',ad.deposit_number ) as memo,';
		$strSql .= 'gat.account_number, gat.name AS account_name, gat.secondary_number, gat.id as gl_account_id,
						gh.post_date as transaction_datetime, gh.post_month,
						p.property_name, p.lookup_code, gh.header_number,gh.reference, gd.gl_transaction_type_id as transaction_type_id,
						CASE
							WHEN ( to_char( gh.post_date, \'mm/yyyy\' ) = to_char( gh.post_month, \'mm/yyyy\' ) )
							THEN gh.post_date
							ELSE gh.post_month
						END AS post_date
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gd.cid = gh.cid )
						JOIN properties p ON ( gd.property_id = p.id and gd.cid = p.cid )
						JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = gd.accrual_gl_account_id )
						JOIN ar_deposits ad ON ( gd.cid = ad.cid AND gd.reference_id = ad.id )
						JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template = false
						AND gd.gl_transaction_type_id IN (' . implode( ', ', $arrintDepositeGlTransactionTypeIds ) . ')
						AND aea.accounting_export_batch_id = ' . ( int ) $intGlExportBatchId . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')

						AND gh.is_template is FALSE
						AND gat.gl_account_id = ';
		$strSql .= ( CAccountingExportBatch::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) ? 'gd.accrual_gl_account_id' : 'gd.cash_gl_account_id';
		$strSql .= '
					ORDER BY
						gd.property_id,
						post_date,
						gat.name';

		return \Psi\Eos\Entrata\CAccountingExportBatchDetails::createService()->fetchAccountingExportBatchDetails( $strSql, $objDatabase );

	}

	public static function fetchGlHeadersByGlDetailIdsByCid( $arrintGlDetailIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintGlDetailIds ) ) return NULL;

		$strSql = 'SELECT
						gd.id AS gl_detail_id,
						gh.*
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.id IN ( ' . sqlIntImplode( $arrintGlDetailIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDateRangeFromPeriod( $intPropertyId, $intTemplateBalanceFromId, $intCid, $objDatabase ) {

		if( $intTemplateBalanceFromId == 7 ) {
			$strRunOnDate = date( '01/01/Y' );
		} elseif( $intTemplateBalanceFromId == 8 ) {
			$strRunOnDate = date( '01/01/Y', strtotime( 'last year' ) );
		} else {
			$strRunOnDate = date( 'm/d/Y' );
		}

		$strSql = 'SELECT * FROM get_fiscal_periods( ' . ( int ) $intCid . ', ARRAY[' . $intPropertyId . '], \'' . $strRunOnDate . '\' , \'' . 'gl' . '\', 0 )';

		$arrstrDateRange = fetchData( $strSql, $objDatabase );
		$intCurrentCalendarMonth	= ( int ) date( 'm', strtotime( $strRunOnDate ) );
		$intCurrentCalendarQuarter	= ( floor( ( $intCurrentCalendarMonth - 1 ) / 3 ) * 3 ) + 1;
		$intCurrentCalendarYear		= ( int ) date( 'Y', strtotime( $strRunOnDate ) );

		switch( $intTemplateBalanceFromId ) {
			case CGlHeader::CURRENT_PERIOD:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['current_post_month'], 'end_date' => $arrstrDateRange[0]['current_post_month'], 'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_month'] ];
				break;

			case CGlHeader::PRIOR_PERIOD:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['prior_post_month'], 'end_date' => $arrstrDateRange[0]['prior_post_month'], 'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_month'] ];
				break;

			case CGlHeader::CURRENT_QUARTER:
				$strStartDate			= date( 'm/01/Y', mktime( 0, 0, 0, $intCurrentCalendarQuarter, 1, $intCurrentCalendarYear ) );
				$strEndDate				= date( 'm/t/Y', mktime( 0, 0, 0, $intCurrentCalendarQuarter + 2, 1, $intCurrentCalendarYear ) );
				$arrstrDateRangeResult		= [ 'start_date' => $strStartDate, 'end_date' => $strEndDate, 'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_date'] ];
				break;

			case CGlHeader::PRIOR_QUARTER:
				$strStartDate			= date( 'm/01/Y', mktime( 0, 0, 0, $intCurrentCalendarQuarter - 3, 1, $intCurrentCalendarYear ) );
				$strEndDate				= date( 'm/t/Y', mktime( 0, 0, 0, $intCurrentCalendarQuarter - 1, 1, $intCurrentCalendarYear ) );
				$arrstrDateRangeResult		= [ 'start_date' => $strStartDate, 'end_date' => $strEndDate, 'pre_start_date' => $arrstrDateRange[0]['prior_fiscal_year_start_date'] ];
				break;

			case CGlHeader::CURRENT_FISCAL_QUARTER:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['current_fiscal_quarter_start_month'], 'end_date' => $arrstrDateRange[0]['current_fiscal_quarter_end_month'],'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_month'] ];
				break;

			case CGlHeader::PRIOR_FISCAL_QUARTER:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['prior_fiscal_quarter_start_month'], 'end_date' => $arrstrDateRange[0]['prior_fiscal_quarter_end_month'],'pre_start_date' => $arrstrDateRange[0]['prior_fiscal_year_start_month'] ];
				break;

			case CGlHeader::CURRENT_CALENDER_YEAR:
				$strStartDate			= date( '01/01/Y' );
				$strEndDate				= date( '12/31/Y' );
				$arrstrDateRangeResult		= [ 'start_date' => $strStartDate, 'end_date' => $strEndDate,'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_date'] ];
				break;

			case CGlHeader::PRIOR_CALENDER_YEAR:
				$strPreviousCalenderYear	= date( 'Y' ) - 1;
				$strStartDate			= date( '01/01/' . $strPreviousCalenderYear );
				$strEndDate				= date( '12/01/' . $strPreviousCalenderYear );
				$arrstrDateRangeResult		= [ 'start_date' => $strStartDate, 'end_date' => $strEndDate,'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_start_date'] ];
				break;

			case CGlHeader::CURRENT_FISCAL_YEAR:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['current_fiscal_year_start_month'], 'end_date' => $arrstrDateRange[0]['current_fiscal_year_end_month'],'pre_start_date' => $arrstrDateRange[0]['current_fiscal_year_to_date_start_month'] ];
				break;

			case CGlHeader::PRIOR_FISCAL_YEAR:
				$arrstrDateRangeResult		= [ 'start_date' => $arrstrDateRange[0]['prior_fiscal_year_start_month'], 'end_date' => $arrstrDateRange[0]['prior_fiscal_year_end_month'],'pre_start_date' => $arrstrDateRange[0]['prior_fiscal_year_start_month'] ];
				break;

			default:
				// Do nothing
				break;
		}
		return $arrstrDateRangeResult;
	}

	public static function fetchCreditDebitAmountForAccountBalance( $intPropertyId, $strGlAccountUsageType, $intTemplateBalanceFromId, $intGlBookId, $intGlAccountId, $arrstrDateRange, $intCid, $objDatabase ) {
		$strStartPostMonth		= $arrstrDateRange['start_date'];
		$strEndPostMonth		= $arrstrDateRange['end_date'];
		$strPreStartPostMonth	= $arrstrDateRange['pre_start_date'];

		$strZeroBalances = ' WHERE ( trial_balance.debits <> 0 OR trial_balance.credits <> 0 ) OR trial_balance.opening_balance <> 0';

		if( $intTemplateBalanceFromId == 3 || $intTemplateBalanceFromId == 4 || $intTemplateBalanceFromId == 7 || $intTemplateBalanceFromId == 8 ) {
			$strPostMonth = 'gh.post_date';
		} else {
			$strPostMonth = 'gh.post_month';
		}

		$strCompareWithDateSql = $strPostMonth . ' >= \'' . $strStartPostMonth . '\' AND ' . $strPostMonth . ' <= \'' . $strEndPostMonth . '\' ';

		$strBegBalanceDateSql = $strPostMonth . ' < \'' . $strStartPostMonth . '\'';

		$strBegBalanceIncomeStatementSql = $strPostMonth . ' >= \'' . $strPreStartPostMonth . '\' AND ' . $strPostMonth . ' < \'' . $strStartPostMonth . '\' ';

		$strBegBalanceRetainedEarningsSql = $strPostMonth . ' < \'' . $strPreStartPostMonth . '\'';

		$strSummarizeSelectSql = ' SUM( CASE WHEN tbr_data.current_amount::NUMERIC > 0 THEN tbr_data.current_amount::NUMERIC ELSE 0 END ) AS debits,
									SUM( CASE WHEN tbr_data.current_amount::NUMERIC < 0 THEN tbr_data.current_amount::NUMERIC ELSE 0 END ) AS credits,';

		$strSql = '
			DROP TABLE IF EXISTS tbr_data_cte;
			CREATE TEMP TABLE tbr_data_cte AS
			(
				SELECT
					gd.cid,
					gd.property_id,
					gd.' . $strGlAccountUsageType . ' AS gl_account_id,
					0 AS opening_balance,
					gd.amount AS current_amount
				FROM
					gl_details gd
					JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ],  ARRAY[ ' . ( int ) $intPropertyId . ' ] ) load_prop ON load_prop.property_id = gd.property_id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
					JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
						AND gh.is_template = false AND gh.gl_book_id = ' . ( int ) $intGlBookId . ' )
				WHERE
					gd.cid = ' . ( int ) $intCid . '
					AND gd.' . $strGlAccountUsageType . ' = ' . ( int ) $intGlAccountId . '
					AND ' . $strCompareWithDateSql . '

				UNION ALL

				SELECT
					gd.cid,
					gd.property_id,
					gd.' . $strGlAccountUsageType . ' AS gl_account_id,
					SUM( CASE
							WHEN gh.id IS NOT NULL AND ' . $strBegBalanceDateSql . ' AND ga.gl_account_type_id IN ( ' . CGlAccountType::INCOME . ', ' . CGlAccountType::EXPENSES . ' ) AND ' . $strBegBalanceIncomeStatementSql . ' THEN gd.amount
							WHEN gh.id IS NOT NULL AND ' . $strBegBalanceDateSql . ' AND ga.gl_account_type_id NOT IN ( ' . CGlAccountType::INCOME . ', ' . CGlAccountType::EXPENSES . ' ) THEN gd.amount
							ELSE 0
						END ) AS opening_balance,
					0 AS current_amount
				FROM
					gl_details gd
					JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ],  ARRAY[ ' . ( int ) $intPropertyId . ' ] ) load_prop ON load_prop.property_id = gd.property_id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
					JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ') AND gh.is_template = false AND gh.gl_book_id = ' . ( int ) $intGlBookId . ' )
					JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.' . $strGlAccountUsageType . ' )
				WHERE
					gd.cid = ' . ( int ) $intCid . '
					AND ' . $strBegBalanceDateSql . ' AND gd.' . $strGlAccountUsageType . ' = ' . ( int ) $intGlAccountId . '
				GROUP BY
					gd.cid,
					gd.property_id,
					gd.' . $strGlAccountUsageType . '

				UNION ALL

				SELECT
					pgs.cid,
					pgs.property_id,
					pgs.retained_earnings_gl_account_id AS gl_account_id,
					( prior_year_income_expenses.amount ) AS opening_balance,
					0 AS current_amount
				FROM
					property_gl_settings pgs
					JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . ( int ) $intPropertyId . ' ] ) load_prop ON load_prop.property_id = pgs.property_id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
					JOIN gl_account_trees gat ON ( gat.cid = pgs.cid AND gat.gl_account_id = pgs.retained_earnings_gl_account_id )
					JOIN
						(
							SELECT
								gd.cid,
								gd.property_id,
								SUM( gd.amount ) AS amount
							FROM
								gl_headers gh
								JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
								JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ],  ARRAY[ ' . ( int ) $intPropertyId . ' ] ) load_prop ON load_prop.property_id = gd.property_id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
								JOIN gl_account_trees AS gat ON ( gat.cid = gd.cid AND gat.gl_account_id = gd.' . $strGlAccountUsageType . ' )
							WHERE
								gh.cid = ' . ( int ) $intCid . '
								AND gat.gl_account_type_id IN ( ' . CGlAccountType::INCOME . ', ' . CGlAccountType::EXPENSES . ' )
								AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
								AND gh.is_template = false
								AND gh.gl_book_id = ' . ( int ) $intGlBookId . '
								AND gd.' . $strGlAccountUsageType . ' = ' . ( int ) $intGlAccountId . '
								AND ' . $strBegBalanceRetainedEarningsSql . '
							GROUP BY
								gd.cid,
								gd.property_id
						) AS prior_year_income_expenses ON ( prior_year_income_expenses.cid = pgs.cid AND prior_year_income_expenses.property_id = pgs.property_id )
				WHERE
					pgs.cid = ' . ( int ) $intCid . '
					AND gat.is_default = 1
			);

			SELECT
				*
			FROM
				(
					SELECT
						p.property_name,
						p.id AS property_group_id,
						p.lookup_code,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						SUM( COALESCE( tbr_data.opening_balance, 0 ) ) AS opening_balance,
						' . $strSummarizeSelectSql . '
						gat.grouping_gl_account_id
					FROM
						gl_account_trees gat
						JOIN properties p ON p.cid = gat.cid
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . ( int ) $intPropertyId . ' ] ) load_prop ON load_prop.property_id = p.id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
						LEFT JOIN tbr_data_cte AS tbr_data ON tbr_data.cid = p.cid AND tbr_data.property_id = p.id AND tbr_data.gl_account_id = gat.gl_account_id
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND gat.is_default = 1
					GROUP BY
						p.property_name, p.id,
						p.lookup_code,
						gat.grouping_gl_account_id,
						gat.formatted_account_number,
						gat.name
					ORDER BY
						p.property_name,
						gat.formatted_account_number
					) AS trial_balance
			' . $strZeroBalances . ' ';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlHeadersByReclassGlHeaderIdsByGlTransactionTypeIdsByCid( $arrintReclassGlHeaderIds, $arrintGlTransactionTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintReclassGlHeaderIds ) || false == valArr( $arrintGlTransactionTypeIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reclass_gl_header_id IN ( ' . implode( ',', $arrintReclassGlHeaderIds ) . ' )
						AND gl_transaction_type_id IN ( ' . implode( ',', $arrintGlTransactionTypeIds ) . ' )';

		return self::fetchGlHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchUnitTypeBudgetActualsByJobIdByByCid( $intJobId, $intCid, $objDatabase, $intJobPhaseId = NULL ) {
		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strJobPhasesFields = '';
		$strJobPhasesGroupBy = '';
		$strJobPhaseWhereClause = '';

		if( true == valId( $intJobPhaseId ) ) {
			$strJobPhasesFields = 'jp.id AS job_phase_id,';
			$strJobPhasesGroupBy = ', jp.id';
			$strJobPhaseWhereClause = ' AND jp.id = ' . ( int ) $intJobPhaseId;
		}

		$strSql = 'SELECT
						pu.unit_type_id,
						ad.ap_code_id,
						ad.post_month,
						' . $strJobPhasesFields . '
						SUM( gd.amount ) total_actual_amount
					FROM
						ap_details ad
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' )
						JOIN gl_details gd ON ( gd.cid = ad.cid AND gd.ap_detail_id = ad.id )
						JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
						JOIN property_units pu ON ( pu.cid = gd.cid AND pu.id = gd.property_unit_id )
						JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.accrual_gl_account_id AND ga.gl_account_usage_type_id <> ' . CGlAccountUsageType::ACCOUNTS_PAYABLE . ' )
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND jp.cid = ' . ( int ) $intCid . '
						AND ah.is_posted = TRUE
						AND ah.deleted_on IS NULL
						' . $strJobPhaseWhereClause . '
					GROUP BY
						pu.unit_type_id,
						ad.ap_code_id,
						ad.post_month ' . $strJobPhasesGroupBy . '
					UNION ALL
					SELECT
						pu.unit_type_id,
						gd.ap_code_id,
						gd.post_month,
						' . $strJobPhasesFields . '
						SUM( gd.amount ) total_actual_amount
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
						JOIN job_phases jp ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id )
						JOIN property_units pu ON ( pu.cid = gd.cid AND pu.id = gd.property_unit_id )
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND jp.cid = ' . ( int ) $intCid . '
						' . $strJobPhaseWhereClause . '
					GROUP BY
						pu.unit_type_id,
						gd.ap_code_id,
						gd.post_month ' . $strJobPhasesGroupBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGlHeadersByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApCodeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						gh.header_number,
						gd.ap_code_id
					FROM
						gl_headers gh
						JOIN gl_details gd ON (gd.cid = gh.cid AND gd.gl_header_id = gh.id)
						JOIN periods p On (p.cid = gd.cid AND p.id = gd.period_id)
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gd.ap_code_id IN (' . implode( ', ', $arrintApCodeIds ) . ' )
						AND gh.is_reverse IS FALSE
						AND gd.job_phase_id IS NOT NULL
						AND p.gl_locked_on IS NULL
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.is_template IS FALSE
						AND gh.gl_header_status_type_id <>  \'' . CGlHeaderStatusType::DELETED . '\'
					GROUP BY
						gh.header_number,
						gd.ap_code_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchGlHeadersCountByApContractIdByCid( $intApContractId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApContractId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT gh.id )
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
						JOIN ap_contracts ac ON ( ac.cid = gd.cid AND ac.id = gd.ap_contract_id )
					WHERE
						ac.id = ' . ( int ) $intApContractId . '
						AND gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS FALSE
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id <>  \'' . CGlHeaderStatusType::DELETED . '\' ';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCompanyUsersAssociatedWithGlHeadersByCid( $intCid, $objClientDatabase ) {

						$strSql = '--PSI User
									( 
									SELECT
										-1 as user_id,
										\'Entrata\' as company_user_name
									FROM
										gl_headers gh
										JOIN company_users cu ON ( cu.cid = gh.cid and cu.id = gh.created_by )
									WHERE
										gh.cid = ' . ( int ) $intCid . '
										AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
										AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
										AND cu.company_employee_id IS NULL
										AND gh.gl_header_type_id NOT IN( ' . CGlHeaderType::ACCRUAL_MONTH_END . ' , ' . CGlHeaderType::CASH_MONTH_END . ' )
										AND gh.gl_header_schedule_id IS NULL
										AND gh.is_template = false
									LIMIT 1
									)

									UNION ALL

									--Recurring JE
									(
									SELECT
										-3 as user_id,
										\'Recurring JE\' as company_user_name
									FROM
										gl_headers gh
									WHERE
										gh.cid = ' . ( int ) $intCid . '
										AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
										AND gh.gl_header_schedule_id IS NOT NULL
										AND gh.is_template = false
									LIMIT 1
									)

									UNION ALL

									--GPR JE
									(
									SELECT
										-2 as user_id,
										\'Auto GPR JE\' as company_user_name
									FROM
										gl_headers gh
									WHERE
										gh.cid = ' . ( int ) $intCid . '
										AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
										AND gh.gl_header_type_id IN( ' . CGlHeaderType::ACCRUAL_MONTH_END . ' , ' . CGlHeaderType::CASH_MONTH_END . ' )
									LIMIT 1
									)

									UNION ALL

									--Non PSI User
									(
									SELECT
										DISTINCT cu.id as user_id,
										ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_user_name
									FROM
										gl_headers gh
										JOIN company_users cu ON ( cu.cid = gh.cid AND cu.id = gh.created_by )
										JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
									WHERE
										gh.cid = ' . ( int ) $intCid . '
										AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
										AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
									ORDER BY
										company_user_name )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalGeneralJournalsByFormFilterByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objFormFilter, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyIds ) || false === valObj( $objFormFilter, 'CFormFilter' ) ) return NULL;

		$objPeriodFilter			= $objFormFilter->getFilterObject( 'gl_post_month' );
		$arrmixFilterConfig			= $objFormFilter->getFilterConfigs();
		$arrmixFilterFieldValues	= $objFormFilter->getFilterFieldValues();

		$strConditions 					= '';
		$strJoinConditions				= '';
		$strConditionsOnGlHeaders		= '';
		$strConditionsOnGlDetails 		= '';
		$strJoinCondition				= '';
		$strHistoricalCondition			= '';
		$strHistoricalSelectCondition	= '';
		$arrstrAndConditionsOnGlHeaders	= [];
		$arrstrAndConditionsOnGlDetails	= [];

		// GL account ids
		if( true == valArr( $arrmixFilterFieldValues['gl_account_ids'] ) ) {
			$arrstrAndConditionsOnGlDetails[] = '( gd.accrual_gl_account_id IN ( ' . implode( ',', $arrmixFilterFieldValues['gl_account_ids'] ) . ' )
													 OR gd.cash_gl_account_id IN ( ' . implode( ',', $arrmixFilterFieldValues['gl_account_ids'] ) . ' ) )';
		}

		// property group ids
		if( true == valArr( $arrmixFilterFieldValues['property_group_ids'] ) ) {
			$arrintPropertyIds = $arrmixFilterFieldValues['property_group_ids'];
		}

		// header number
		if( false == is_infinite( ( int ) $arrmixFilterFieldValues['header_number']['min-amount'] ) && 0 < $arrmixFilterFieldValues['header_number']['min-amount'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' gh.header_number >= ' . ( int ) $arrmixFilterFieldValues['header_number']['min-amount'];
		}

		if( false == is_infinite( ( float ) $arrmixFilterFieldValues['header_number']['max-amount'] ) && 0 < $arrmixFilterFieldValues['header_number']['max-amount'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' gh.header_number <= ' . ( int ) $arrmixFilterFieldValues['header_number']['max-amount'];
		}

		// transaction date
		if( true == valStr( $arrmixFilterFieldValues['transaction_date']['min-date'] ) && true == CValidation::validateDate( $arrmixFilterFieldValues['transaction_date']['min-date'] ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'gh.post_date >= \'' . date( 'Y-m-d', strtotime( $arrmixFilterFieldValues['transaction_date']['min-date'] ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $arrmixFilterFieldValues['transaction_date']['max-date'] ) && true == CValidation::validateDate( $arrmixFilterFieldValues['transaction_date']['max-date'] ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'gh.post_date <= \'' . date( 'Y-m-d', strtotime( $arrmixFilterFieldValues['transaction_date']['max-date'] ) ) . ' 23:59:59\'';
		}

		// reference
		if( true == valStr( $arrmixFilterFieldValues['reference_note'] ) ) {
			$arrstrAndConditionsOnGlHeaders[] = '( gh.reference ILIKE \'%' . addslashes( $arrmixFilterFieldValues['reference_note'] ) . '%\' OR gh.memo ILIKE \'%' . addslashes( $arrmixFilterFieldValues['reference_note'] ) . '%\' )';
		}

		// created by
		if( true == valArr( $arrmixFilterFieldValues['company_user_ids'] ) ) {

			if( true == in_array( -1, $arrmixFilterFieldValues['company_user_ids'] ) ) {
				$arrstrOrConditionsOnGlHeaders[] = 'gh.id IN (
																SELECT
																	DISTINCT gh.id
																FROM
																	gl_headers gh
																	JOIN company_users cu ON ( cu.cid = gh.cid and cu.id = gh.created_by )
																WHERE
																	gh.cid = ' . ( int ) $intCid . '
																	AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
																	AND cu.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
																	AND cu.company_employee_id IS NULL 
																	AND gh.gl_header_type_id NOT IN( ' . CGlHeaderType::ACCRUAL_MONTH_END . ' , ' . CGlHeaderType::CASH_MONTH_END . ' )
																	AND gh.gl_header_schedule_id IS NULL
																	AND gh.is_template = false
															)';
			}

			if( true == in_array( -2, $arrmixFilterFieldValues['company_user_ids'] ) ) {
				$arrstrOrConditionsOnGlHeaders[] = 'gh.gl_header_type_id IN( ' . CGlHeaderType::ACCRUAL_MONTH_END . ' , ' . CGlHeaderType::CASH_MONTH_END . ' )';
			}

			if( true == in_array( -3, $arrmixFilterFieldValues['company_user_ids'] ) ) {
				$arrstrOrConditionsOnGlHeaders[] = '( gh.gl_header_schedule_id IS NOT NULL AND gh.is_template = false )';
			}

			$arrstrOrConditionsOnGlHeaders[] = 'gh.created_by IN( ' . implode( ',', $arrmixFilterFieldValues['company_user_ids'] ) . ' )';

			if( true == valArr( $arrstrOrConditionsOnGlHeaders ) ) {
				$arrstrAndConditionsOnGlHeaders[] = '( ' . implode( ' OR ', array_unique( $arrstrOrConditionsOnGlHeaders ) ) . ' )';
			}

		}

		// routing tags
		if( true == valIntArr( $arrmixFilterFieldValues['routing_tag_ids'] ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'gh.ap_routing_tag_id IN ( ' . implode( ',', $arrmixFilterFieldValues['routing_tag_ids'] ) . ' )';
		}

		// header status types
		if( true == valArr( $arrmixFilterFieldValues['gl_header_status_type_ids'] ) ) {

			$arrstrAndConditionsOnGlHeaders[] = 'gh.gl_header_status_type_id IN ( ' . implode( ',', $arrmixFilterFieldValues['gl_header_status_type_ids'] ) . ' ) ';

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilterFieldValues['gl_header_status_type_ids'] )
			    && true == in_array( CGlHeaderStatusType::REVERSED, $arrmixFilterFieldValues['gl_header_status_type_ids'] ) ) {
				$arrstrAndConditionsOnGlHeaders[] = 'gh.offsetting_gl_header_id IS NOT NULL';
			}

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilterFieldValues['gl_header_status_type_ids'] )
			    && true == in_array( CGlHeaderStatusType::POSTED, $arrmixFilterFieldValues['gl_header_status_type_ids'] ) ) {
				$arrstrAndConditionsOnGlHeaders[] = 'gh.offsetting_gl_header_id IS NULL';
			}

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilterFieldValues['gl_header_status_type_ids'] )
			    && true == in_array( CGlHeaderStatusType::ROUTED, $arrmixFilterFieldValues['gl_header_status_type_ids'] ) ) {
				$arrstrAndConditionsOnGlHeaders[] = 'gh.offsetting_gl_header_id IS NULL';
			}
		} else {
			$arrstrAndConditionsOnGlHeaders[] = 'gh.gl_header_status_type_id IN ( ' . CGlHeaderStatusType::TEMPORARY . ', ' . CGlHeaderStatusType::POSTED . ', ' . CGlHeaderStatusType::ROUTED . ', ' . CGlHeaderStatusType::REVERSED . ' ) ';
		}

		// post month range
		if( true == valStr( $objPeriodFilter->getStartPostMonth() ) && true == CValidation::validateDate( $objPeriodFilter->getStartPostMonth() ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'DATE_TRUNC( \'day\', gh.post_month ) >= \'' . date( 'm-d-Y', strtotime( $objPeriodFilter->getStartPostMonth() ) ) . ' \'';
		}

		if( true == valStr( $objPeriodFilter->getEndPostMonth() ) && true == CValidation::validateDate( $objPeriodFilter->getEndPostMonth() ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'DATE_TRUNC( \'day\', gh.post_month ) <= \'' . date( 'm-d-Y', strtotime( $objPeriodFilter->getEndPostMonth() ) ) . ' \'';
		}

		// file attachment
		if( true == isset( $arrmixFilterFieldValues['file_attachments'] ) && 'with_attachment' == $arrmixFilterFieldValues['file_attachments'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' fa.id IS NOT NULL';
		} elseif( true == isset( $arrmixFilterFieldValues['file_attachments'] ) && 'without_attachment' == $arrmixFilterFieldValues['file_attachments'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' fa.id IS NULL';
		}

		// Confidential
		if( true == isset( $arrmixFilterFieldValues['confidential'] ) && 'confidential' == $arrmixFilterFieldValues['confidential'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' gd_details.is_confidential IS TRUE';
		} elseif( true == isset( $arrmixFilterFieldValues['confidential'] ) && 'not_confidential' == $arrmixFilterFieldValues['confidential'] ) {
			$arrstrAndConditionsOnGlHeaders[] = ' gd_details.is_confidential IS FALSE';
		}

		// Accounting Method
		if( NULL !== $arrmixFilterConfig['accounting_method']['submitted_value'] && true == valArr( $arrmixFilterFieldValues['accounting_method'] ) ) {
			$strAccountingMethod = "'" . implode( "','", $arrmixFilterFieldValues['accounting_method'] ) . "'";
			$arrstrAndConditionsOnGlHeaders[] = 'gd_details.accounting_method IN ( ' . $strAccountingMethod . ' ) ';
		}

		// Gl Books
		if( NULL !== $arrmixFilterConfig['gl_book_ids']['submitted_value'] && true == valArr( $arrmixFilterFieldValues['gl_book_ids'] ) ) {
			$arrstrAndConditionsOnGlHeaders[] = 'gh.gl_book_id IN ( ' . implode( ',', $arrmixFilterFieldValues['gl_book_ids'] ) . ' ) ';
		}

		// Custom tags
		if( true == valArr( $arrmixFilterFieldValues['gl_dimension_ids'] ) ) {
			$arrstrAndConditionsOnGlDetails[] = 'gd.gl_dimension_id IN ( ' . implode( ',', $arrmixFilterFieldValues['gl_dimension_ids'] ) . ' )';
		}

		// Jobs
		if( true == valArr( $arrmixFilterFieldValues['job_ids'] ) ) {
			$arrstrAndConditionsOnGlDetails[] = 'jp.job_id IN ( ' . implode( ',', $arrmixFilterFieldValues['job_ids'] ) . ' )';
			$strJoinConditions = ' LEFT JOIN job_phases jp ON ( gd.cid = jp.cid AND gd.job_phase_id = jp.id )';
		}

		// Contracts
		if( true == valArr( $arrmixFilterFieldValues['contract_ids'] ) ) {
			$arrstrAndConditionsOnGlDetails[] = 'gd.ap_contract_id IN ( ' . implode( ',', $arrmixFilterFieldValues['contract_ids'] ) . ' )';
		}

		if( true == valArr( $arrstrAndConditionsOnGlHeaders ) ) {
			$strConditionsOnGlHeaders = ' AND ' . implode( ' AND ', array_unique( $arrstrAndConditionsOnGlHeaders ) );
		}

		if( true == valArr( $arrstrAndConditionsOnGlDetails ) ) {
			$strConditionsOnGlDetails = ' AND ' . implode( ' AND ', array_unique( $arrstrAndConditionsOnGlDetails ) );
		}

		if( true == valStr( $strConditionsOnGlDetails ) ) {

			$strConditions = ' AND EXISTS (
											SELECT
													1
											FROM
												gl_details gd
												' . $strJoinConditions . '
											WHERE
												gd.cid = ' . ( int ) $intCid . '
												AND gd.cid = gh.cid
												AND gd.gl_header_id = gh.id ' . $strConditionsOnGlDetails . '
										) ';
		}

		if( false == $boolShowDisabledData ) {
			$strSql = '	WITH properties_cte AS ( SELECT property_id FROM load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp )';
			$strJoinCondition = ' JOIN properties_cte p_temp ON ( p_temp.property_id = gd.property_id )';
		} else {
			$strHistoricalSelectCondition = ' , bool_and( LEAST( p.is_disabled::BOOLEAN, COALESCE( pp.ps_product_id, 0 ) = ' . \CPsProduct::HISTORICAL_ACCESS . ' ) ) AS is_historical';
			$strHistoricalCondition       = ' AND gd_details.is_historical IS TRUE';
			$strJoinCondition             = ' LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS . ' )';
		}

		$strSql .= '
					SELECT
						COUNT( sub.id ),
						SUM( CASE 
								WHEN sub.gl_header_status_type_id = ' . CGlHeaderStatusType::TEMPORARY . ' THEN 0
								ELSE 1
							 END
							) AS selectable_header_count
					FROM
						(
							SELECT
								gh.id,
								gh.gl_header_status_type_id
							FROM
								gl_headers gh
								JOIN gl_books gb ON ( gh.cid = gb.cid AND gh.gl_book_id = gb.id )
								JOIN LATERAL (
									SELECT
										SUM ( CASE WHEN gd.amount > 0 THEN gd.amount ELSE 0 END ) AS debit_amount,
										MAX ( CASE
											WHEN gd.accrual_gl_account_id IS NOT NULL AND gd.cash_gl_account_id IS NOT NULL THEN \'Both\'
											WHEN gd.accrual_gl_account_id IS NOT NULL THEN \'Accrual\'
											WHEN gd.accrual_gl_account_id IS NULL THEN \'Cash\'
										END ) AS accounting_method,
										( CASE
											WHEN MIN ( p.property_name ) <> MAX ( p.property_name ) THEN \'Multiple\'
											ELSE MAX ( p.property_name )
											END ) AS property_name,
											bool_or( gd.is_confidential ) AS is_confidential
											' . $strHistoricalSelectCondition . '
										FROM
											gl_details gd
											JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
											' . $strJoinCondition . '
										WHERE
											gd.cid = ' . ( int ) $intCid . '
											AND gd.gl_transaction_type_id = gh.gl_transaction_type_id
											AND gh.id = gd.gl_header_id
											' . ( ( false == $boolShowDisabledData ) ? ' AND p.is_disabled = 0' : '' ) . '
									) AS gd_details ON TRUE
								LEFT JOIN LATERAL
										(
											SELECT
												fa.id,
												fa.gl_header_id
											FROM
												file_associations fa
											WHERE
												gh.cid = fa.cid
												AND gh.id = fa.gl_header_id
												AND fa.deleted_by IS NULL
											ORDER BY
												fa.id DESC
											LIMIT 1
										) fa ON TRUE
							WHERE
								gh.cid = ' . ( int ) $intCid . '
								AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . $strConditionsOnGlHeaders . $strHistoricalCondition . '
								AND gh.is_template IS FALSE
								AND gd_details.debit_amount IS NOT NULL
								AND gh.reference_id IS NULL' . $strConditions;

		if( true == is_numeric( $arrmixFilterFieldValues['amount']['min-amount'] ) || true == is_numeric( $arrmixFilterFieldValues['amount']['max-amount'] ) ) {
			$strSql .= ' GROUP BY
								gh.cid,
								gh.gl_header_status_type_id,
								gh.id,
								gl_book_id,
								gh.header_number,
								gh.post_date,
								gh.post_month,
								gh.memo,
								gh.created_by,
								gh.created_on,
								gd_details.debit_amount,
								gd_details.property_name,
								gd_details.accounting_method,
								gh.reference,
								gb.name,
								fa.gl_header_id
						HAVING ';
		}

		if( true == is_numeric( $arrmixFilterFieldValues['amount']['min-amount'] ) ) {
			$strSql .= '(	SELECT
								SUM ( gd.amount )
							FROM
								gl_details gd
							WHERE
								gd.cid = gh.cid
								AND gd.gl_header_id = gh.id
								AND gd.amount > 0
								AND gd.cid = ' . ( int ) $intCid . '
						) >= ' . ( int ) $arrmixFilterFieldValues['amount']['min-amount'];
		}

		if( true == is_numeric( $arrmixFilterFieldValues['amount']['min-amount'] ) && true == is_numeric( $arrmixFilterFieldValues['amount']['max-amount'] ) ) {
			$strSql .= ' AND ';
		}

		if( true == is_numeric( $arrmixFilterFieldValues['amount']['max-amount'] ) ) {
			$strSql .= '(
							SELECT
								SUM ( gd.amount )
							FROM
								gl_details gd
							WHERE
								gd.cid = gh.cid
								AND gd.gl_header_id = gh.id
								AND gd.amount > 0
								AND gd.cid = ' . ( int ) $intCid . '
						) <= ' . ( int ) $arrmixFilterFieldValues['amount']['max-amount'];
		}

		$strSql .= ' ) AS sub';

		return fetchData( $strSql, $objClientDatabase );
	}

}

?>