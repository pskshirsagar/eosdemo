<?php

class CCustomerDetail extends CBaseCustomerDetail {

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCustomerId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valInvoiceDeliveryTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getInvoiceDeliveryTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_delivery_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>