<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlGroups
 * Do not add any new functions to this class.
 */

class CDefaultGlGroups extends CBaseDefaultGlGroups {

	public static function fetchDefaultGlGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultGlGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultGlGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultGlGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>