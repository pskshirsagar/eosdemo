<?php

class CWebsiteWidget extends CBaseWebsiteWidget {

	protected $m_intSupportedType;
	protected $m_intWidgetSpaceId;
	protected $m_intWebsiteWidgetMediaId;

	protected $m_strName;
	protected $m_strFullsizeUri;
	protected $m_strWidgetNamespace;

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrintValues['widget_namespace'] ) ) {
			$this->setWidgetNamespace( $arrintValues['widget_namespace'] );
		}
		if( true == isset( $arrintValues['widget_space_id'] ) ) {
			$this->setWidgetSpaceId( $arrintValues['widget_space_id'] );
		}
		if( true == isset( $arrintValues['supported_type'] ) ) {
			$this->setSupportedType( $arrintValues['supported_type'] );
		}
		if( true == isset( $arrintValues['fullsize_uri'] ) ) {
			$this->setFullsizeUri( $arrintValues['fullsize_uri'] );
		}
		if( true == isset( $arrintValues['name'] ) ) {
			$this->setName( $arrintValues['name'] );
		}
		if( true == isset( $arrintValues['website_widget_media_id'] ) ) {
			$this->setWebsiteWidgetMediaId( $arrintValues['website_widget_media_id'] );
		}
	}

	public function setWidgetSpaceId( $intWidgetSpaceId ) {
		$this->m_intWidgetSpaceId = $intWidgetSpaceId;
	}

	public function setWidgetNamespace( $strWidgetNamespace ) {
		$this->m_strWidgetNamespace = $strWidgetNamespace;
	}

	public function getWidgetSpaceId() {
		return $this->m_intWidgetSpaceId;
	}

	public function getWidgetNamespace() {
		return $this->m_strWidgetNamespace;
	}

	public function setSupportedType( $intSupportedType ) {
		$this->m_intSupportedType = $intSupportedType;
	}

	public function getSupportedType() {
		return $this->m_intSupportedType;
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->m_strFullsizeUri = $strFullsizeUri;
	}

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function setWebsiteWidgetMediaId( $intWebsiteWidgetMediaId ) {
		$this->m_intWebsiteWidgetMediaId = $intWebsiteWidgetMediaId;
	}

	public function getWebsiteWidgetMediaId() {
		return $this->m_intWebsiteWidgetMediaId;
	}

	public function valWidgetName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getWidgetName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'widget_name', __( 'Custom widget name is required.' ) ) );
		} elseif( true == preg_match( '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $this->getWidgetName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'widget_name', __( 'Special characters are not allowed in custom widget name.' ) ) );
		}

		if( true == $boolIsValid ) {
			$objPreexistingCustomWidget = \Psi\Eos\Entrata\CWebsiteWidgets::createService()->fetchWebsiteWidgetsByWidgetNameByWebsiteTemplateIdByCid( $this->getWidgetName(), $this->getWebsiteTemplateId(), $this->getCid(), $objDatabase );

			if( true == valArr( $objPreexistingCustomWidget ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'widget_name', __( 'A Custom widget with this name already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valUrl( $strWebsitePreferencesValue, $strWebsitePreferencesKey ) {
		$boolIsValid = true;

		if( true == valStr( $strWebsitePreferencesValue ) ) {
			if( false == CValidation::checkUrl( $strWebsitePreferencesValue, false, true ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strWebsitePreferencesKey, CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
			}

			$strWebsitePreferencesDomain    = parse_url( $strWebsitePreferencesValue, PHP_URL_HOST );
			$strValidateResponse            = CValidation::checkDomain( $strWebsitePreferencesDomain );

			if( 1 != $strValidateResponse ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', $strValidateResponse ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrstrWidgetParameters = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valWidgetName( $objDatabase );

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUrl( $arrstrWidgetParameters['url'], $arrstrWidgetParameters['url_type'] );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData = fetchData( 'SELECT setval( \'public.website_widgets_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function createWebsiteWidgetMedia() {
		$objWebsiteWidgetMedia = new CWebsiteWidgetMedia();

		$objWebsiteWidgetMedia->setCid( $this->getCid() );
		$objWebsiteWidgetMedia->setWebsiteId( $this->getWebsiteId() );
		$objWebsiteWidgetMedia->setWebsiteWidgetId( $this->getId() );
		return $objWebsiteWidgetMedia;
	}

}
?>