<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRelationshipGroups
 * Do not add any new functions to this class.
 */

class CCustomerRelationshipGroups extends CBaseCustomerRelationshipGroups {

	public static function fetchCustomerRelationshipGroupsByCidByIds( $intCid, $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_relationship_groups WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . sqlIntImplode( $arrintIds ) . ' ) ';

		return self::fetchCustomerRelationshipGroups( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupByOccupancyTypeIdByPropertyIdByCid( $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == in_array( $intOccupancyTypeId, [ COccupancyType::AFFORDABLE, COccupancyType::MILITARY ] ) ) {
			$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
		}

		$strSql = 'SELECT
					*,
					crg.id as id
					FROM
						customer_relationship_groups crg
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = crg.cid AND pcrg.customer_relationship_group_id = crg.id )
					WHERE
						pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND crg.cid = ' . ( int ) $intCid . '
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId;

		return self::fetchCustomerRelationshipGroup( $strSql, $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipGroupIdByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT id FROM customer_relationship_groups WHERE cid = ' . ( int ) $intCid . ' AND is_default = true AND is_system = true';

		return self::fetchCustomerRelationshipGroup( $strSql, $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipGroupsIdByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT id FROM customer_relationship_groups WHERE cid = ' . ( int ) $intCid . ' AND is_default = true AND is_system = true';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupIdByCidByName( $intCid, $strName, $intId, $objDatabase ) {

		$strCondition = '';
		if( ( int ) $intId != '' ) {
			$strCondition = ' AND id <> ' . ( int ) $intId;
		}
		$strSql = 'SELECT id FROM customer_relationship_groups WHERE cid = ' . ( int ) $intCid . ' AND name = \'' . $strName . '\'' . $strCondition;

		return self::fetchCustomerRelationshipGroups( $strSql, $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupsByOccupancyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					*,
					crg.id as id
					FROM
						customer_relationship_groups crg
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = crg.cid AND pcrg.customer_relationship_group_id = crg.id )
					WHERE
						pcrg.property_id = ' . ( int ) $intPropertyId . '
						AND crg.cid = ' . ( int ) $intCid . '
						AND crg.occupancy_type_id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )';

		return self::fetchCustomerRelationshipGroups( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerRelationshipGroupByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					pd.property_id,
					crg.name as name
					FROM
						customer_relationship_groups crg
						JOIN property_details pd ON crg.cid = pd.cid AND crg.id = pd.customer_relationship_group_id
					WHERE
						pd.property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND pd.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

}
?>
