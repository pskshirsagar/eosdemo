<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountMonthNotes
 * Do not add any new functions to this class.
 */

class CBudgetGlAccountMonthNotes extends CBaseBudgetGlAccountMonthNotes {

	public static function fetchBudgetGlAccountMonthNotesByBudgetGlAccountMonthIdsByCid( $arrintBudgetGlAccountMonthIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintBudgetGlAccountMonthIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						budget_gl_account_month_notes
					WHERE
						cid = ' . ( int ) $intCid . '
						AND budget_gl_account_month_id IN ( ' . sqlIntImplode( $arrintBudgetGlAccountMonthIds ) . ' )';

		return self::fetchBudgetGlAccountMonthNotes( $strSql, $objDatabase );
	}

}

?>