<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultUnitSpaceNamePatterns
 * Do not add any new functions to this class.
 */

class CDefaultUnitSpaceNamePatterns extends CBaseDefaultUnitSpaceNamePatterns {

	public static function fetchDefaultUnitSpaceNamePatterns( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultUnitSpaceNamePattern::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultUnitSpaceNamePattern( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultUnitSpaceNamePattern::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>