<?php
class CWebsitePreference extends CBaseWebsitePreference {

	protected  $m_arrstrAllowedScriptTagKeys;

	const WEBSITE_ON_BEHIND_LOGIN	= 1;
	const WEBSITE_HIDDEN        	= 2;

	public function __construct() {
		parent::__construct();
	}

	/**
	 *
	 * Validation Functions
	 */

	public function valRssFeed() {
		$boolIsValid = true;

		if( false == is_null( $this->getValue() ) && false == CValidation::checkUrl( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'LOBBY_RSS_FEED_LINK', 'Invalid rss feed url.' ) );
		}
		return $boolIsValid;
	}

	public function valWebsiteFeed() {
		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) || false == CValidation::checkUrl( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'LOBBY_DISPLAY_WEBSITE_FEED_LINK', 'Invalid website feed url.' ) );
		}
		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;

		$arrmixWebsitePreferencesKeys = [
			'ADOBE_DTM_PATH',
			'CONVENTIONAL_CTA_BTN_URL',
			'FLEXIBLE_CTA_CUSTOM_URL',
			'STUDENT_CTA_CUSTOM_URL',
			'MILITARY_CTA_BTN_URL',
			'INSTAGRAM_URL',
			'VIMEO_URL',
			'CORP_SOCIAL_MEDIA_URL_FACEBOOK',
			'CORP_SOCIAL_MEDIA_URL_TWITTER',
			'CORP_SOCIAL_MEDIA_URL_YOUTUBE',
			'CORP_SOCIAL_MEDIA_URL_BLOG',
			'CORP_SOCIAL_MEDIA_URL_FLICKR',
			'CORP_SOCIAL_MEDIA_URL_TUMBLR',
			'CORP_SOCIAL_MEDIA_URL_LINKEDIN',
			'CORP_SOCIAL_MEDIA_URL_MYSPACE',
			'CORP_SOCIAL_MEDIA_URL_INSTAGRAM',
			'CORP_SOCIAL_MEDIA_URL_PINTEREST',
			'CORP_SOCIAL_MEDIA_URL_YELP',
			'CORP_SOCIAL_MEDIA_URL_FOURSQUARE',
			'INVESTOR_URL',
			'SNIPPET_CSS_REFERENCE',
			'REQUEST_INFO_EXTERNAL_URL',
			'EQUAL_HOUSING_OPPORTUNITY_EXTERNAL_URL',
            'ADA_ICON_EXTERNAL_LINK',
			'TERM_OF_USE_EXTERNAL_LINK',
			'PRIMARY_PRIVACY_POLICY_EXTERNAL_LINK',
			'SECONDARY_PRIVACY_POLICY_EXTERNAL_LINK'
		];

		if( true == in_array( $this->getKey(), $arrmixWebsitePreferencesKeys, true ) && true == valStr( $this->getValue() ) && false == CValidation::checkUrl( $this->getValue(), false, true ) ) {
			$boolIsValid = false;
			displayJsonOutput( [ 'type' => 'error', 'message' => CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ] );
		}

		return $boolIsValid;
	}

	public function valCustomUrl( $strCustomUrl, $strCtaFilterType ) {

		$boolIsValid = true;
		if( false == is_null( $strCtaFilterType ) ) {
			if( 'customurl' == $this->getValue() ) {
				if( NULL == $strCustomUrl && $strCtaFilterType == $this->getKey() ) {
					$boolIsValid = false;
					displayJsonOutput( [ 'type' => 'error', 'message' => __( 'Please enter a valid URL or change the CTA Filter Type.' ) ] );
				}
			}
		}

		return $boolIsValid;
	}

	public function valLandingPagesKeys() {
		$boolIsValid = true;

		$arrstrLandingPagesKeysErrorMessages = [
			'LANDING_PAGE_GUEST_CARD_URL' => 'Custom URL is required. ',
			'LANDING_PAGE_GUEST_CARD_DEFAULT_LEAD_SOURCE' => 'Lead Source is required. ',
			'LANDING_PAGE_GUEST_CARD_CONFIRMATION_MESSAGE' => 'Confirmation Page Content is required. ',
			'LANDING_PAGE_GUEST_CARD_HEADING' => 'Landing Page Heading is required. ',
			'LANDING_PAGE_GUEST_CARD_TITLE' => 'Title is required. ',
			'LANDING_PAGE_GUEST_CARD_DESCRIPTION' => 'Description is required. '
		];

		if( true == in_array( $this->getKey(), [ 'LANDING_PAGE_GUEST_CARD_HEADING', 'LANDING_PAGE_GUEST_CARD_TITLE', 'LANDING_PAGE_GUEST_CARD_DESCRIPTION' ] ) ) {
			$this->setValue( strip_tags( $this->getValue() ) );
			if( 'LANDING_PAGE_GUEST_CARD_DESCRIPTION' == $this->getKey() ) {
				$this->setValue( \Psi\CStringService::singleton()->substr( $this->getValue(), 0, 320 ) );
			}
		}

		if( true == array_key_exists( $this->getKey(), $arrstrLandingPagesKeysErrorMessages ) && true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			displayJsonOutput( [ 'type' => 'error', 'message' => $arrstrLandingPagesKeysErrorMessages[$this->getKey()] ] );
		}

		return $boolIsValid;
	}

	public function valLandingPageGuestCardUrl( $objWebsite = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		if( 'LANDING_PAGE_GUEST_CARD_URL' == $this->getKey() && false == is_null( $this->getValue() ) ) {
			$this->setValue( getSanitizedFormField( $this->getValue() ) );
			$this->setValue( \Psi\CStringService::singleton()->preg_replace( '/\s+/', '-', \Psi\CStringService::singleton()->strtolower( trim( $this->getValue() ) ) ) );
			$arrstrWebsiteDocumentsSeoUri = $objWebsite->fetchWebsiteDocumentsCountBySeoUri( $this->getValue(), $objDatabase );
			if( 0 < $arrstrWebsiteDocumentsSeoUri[0]['count'] ) {
				$boolIsValid = false;
				displayJsonOutput( [ 'type' => 'error', 'message' => 'Custom URL is already in use for Custom Pages. ' ] );
			}
		}

		return $boolIsValid;

	}

	public function valGoogleAnalyticsTrackingKeys() {
		$boolIsValid = true;

		$arrstrGoogleAnalyticsTrackingKeysErrorMessages = [
			'BING_WEBMASTER_TOOL_KEY' => 'Invalid Bing Webmaster Meta Tag Key. ',
			'BING_ADS_UET_KEY' => 'There should not be any code in this field, all that is needed is the unique BING UET ID.',
			'GOOGLE_ANALYTICS_TRACKING_KEY' => 'Invalid Google Analytics Tracking Key. ',
			'GOOGLE_ANALYTICS_MACRO_TRACKING_KEY' => 'Invalid Google Analytics Macro Tracking Key. ',
			'GOOGLE_TAG_MANAGER' => 'Invalid Google Tag Manager Key. ',
			'GOOGLE_WEBMASTER_META_TAG' => 'Invalid Google Search Console Meta Tag Key. ',
			'RESIDENT_PORTAL_GOOGLE_ANALYTICS_TRACKING_KEY' => 'Invalid ResidentPortal Google Analytics Tracking Key. ',
			'RESIDENT_PORTAL_GOOGLE_ANALYTICS_MACRO_TRACKING_KEY' => 'Invalid ResidentPortal Google Analytics Macro Tracking Key. ',
			'PINTEREST_VERIFICATION_CODE' => 'Invalid Pinterest Meta Tag Key. ',
			'BAIDU_ANALYTICS_TRACKING_KEY' => 'Invalid Baidu Analytics Tracking Key.'
		];

		if( true == array_key_exists( $this->getKey(), $arrstrGoogleAnalyticsTrackingKeysErrorMessages ) && false == is_null( $this->getValue() ) && strip_tags( $this->getValue() ) != $this->getValue() ) {
			$boolIsValid = false;
			displayJsonOutput( [ 'type' => 'error', 'message' =>$arrstrGoogleAnalyticsTrackingKeysErrorMessages[$this->getKey()] ] );
		}

		return $boolIsValid;
	}

	public function valAllOtherWebsitePreferences( $objDatabase ) {
		$boolIsValid = true;
		$this->setValue( $this->getValue() );

		if( ( true == preg_match( '#&lt;script(.*?)&gt;(.*?)&lt;/script&gt;#is', $this->getValue() ) || ( true == preg_match( '#<script(.*?)>(.*?)</script>#is', $this->getValue() ) ) ) ) {
			$boolIsValid = false;
			$strLabel	= \Psi\Eos\Entrata\CWebsiteSettingKeys::createService()->fetchWebsiteSettingKeyLabelByKey( $this->getKey(), $objDatabase );
			displayJsonOutput( [ 'type' => 'error', 'message' => 'JS code is not allowed in "' . $strLabel . '" setting' ] );
		}

		return $boolIsValid;
	}

	public function getSanitizedWebsitePreferenceValue( $objDatabase ) {
		$boolIsValid = true;

		if( true == in_array( $this->getKey(), [ 'STATISTICS_JAVASCRIPT', 'ACCESSIBILITY_JAVASCRIPT', 'WEBSITE_CHAT_HTML' ] ) ) {
			$this->setValue( $this->getValue() );
		} elseif( true == in_array( $this->getKey(), [ 'FACEBOOK_PIXEL_TRACKING_CODE_NEW', 'GOOGLE_ADS_PHONE_SNIPPET' ] ) ) {
			if( ( true == preg_match( '#&lt;noscript(.*?)&gt;(.*?)&lt;/noscript&gt;#is', $this->getValue() ) ) || ( true == preg_match( '#<noscript(.*?)>(.*?)</noscript>#is', $this->getValue() ) ) || ( true == preg_match( '#<noscript(.*?)>(.*?)#is', $this->getValue() ) ) ) {
				$arrstrPreferenceValue = explode( '<noscript>', $this->getValue() );
				$strSanitizedValue = strip_tags( tidy_parse_string( $arrstrPreferenceValue[0] ), '<script>' );

				$arrstrSubPreferenceValue = explode( '</noscript>', $arrstrPreferenceValue[1] );
				$strSanitizedValue .= '<noscript>' . $arrstrSubPreferenceValue[0] . '</noscript>';
				$strSanitizedValue .= strip_tags( tidy_parse_string( $arrstrSubPreferenceValue[1] ), '<script>' );
			} else {
				$strSanitizedValue	= tidy_parse_string( $this->getValue() );
				$strSanitizedValue 	= strip_tags( $strSanitizedValue, '<script>' );
				$strSanitizedValue	= htmlspecialchars_decode( $strSanitizedValue );
			}

			$this->setValue( $strSanitizedValue );
		} elseif( true == in_array( $this->getKey(), [ 'STATE_SEO_FORMAT_EXTENSION', 'CITY_SEO_FORMAT_EXTENSION', 'AREA_SEO_FORMAT_EXTENSION', 'SUB_AREA_SEO_FORMAT_EXTENSION', 'PROPERTY_SEO_FORMAT_EXTENSION' ] ) ) {
			$this->setValue( \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/(^-+)|([^0-9a-zA-Z_\s\-]+)/i', '/(\s|-|\s-|-\s|_)+/', '/^-+|-$/' ], [ '', '-', '' ], trim( $this->getValue() ) ) ) );
		} elseif( 'BLOG_DESCRIPTION' == $this->getKey() ) {
			$this->setValue( CStrings::strTrimDef( $this->getValue(), 320, NULL, true ) );
		} elseif( 'MAP_ZOOM_LEVEL' == $this->getKey() ) {
			$this->setValue( $this->getValue() );
			if( 12 > $this->getValue() ) {
				$boolIsValid = false;
				displayJsonOutput( [ 'type' => 'error', 'message' => 'Zoom value should be 12 or above.' ] );
			}
		} else {
			$boolIsValid = $this->valAllOtherWebsitePreferences( $objDatabase );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objWebsite = NULL, $objDatabase = NULL, $strCustomUrl = NULL, $strCtaFilterType =NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				if( false == $this->valUrl() ) {
					$boolIsValid = false;
					break;
				} elseif( false == $this->valLandingPagesKeys() ) {
					$boolIsValid = false;
					break;
				} elseif( false == $this->valLandingPageGuestCardUrl( $objWebsite, $objDatabase ) ) {
					$boolIsValid = false;
					break;
				} elseif( false == $this->valGoogleAnalyticsTrackingKeys() ) {
					$boolIsValid = false;
					break;
				} elseif( false == $this->getSanitizedWebsitePreferenceValue( $objDatabase ) ) {
					$boolIsValid = false;
					break;
				} elseif( false == $this->valCustomUrl( $strCustomUrl, $strCtaFilterType ) ) {
					$boolIsValid = false;
					break;
				}

			case VALIDATE_DELETE:
				break;

			case 'lobby_display_rss_feed':
				$boolIsValid &= $this->valRssFeed();
				break;

			case 'lobby_display_website_feed':
				$boolIsValid &= $this->valWebsiteFeed();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>