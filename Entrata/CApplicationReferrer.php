<?php

class CApplicationReferrer extends CBaseApplicationReferrer {

	protected $m_strApplicationName;
	protected $m_strReferrerTypeName;

	/**
	 * Set Functions
	 */
	public  function setReferrerTypeName( $strReferrerTypeName ) {
		$this->m_strReferrerTypeName = $strReferrerTypeName;
	}

	public  function setApplicationName( $strApplicationName ) {
		$this->m_strApplicationName = $strApplicationName;
	}

	/**
	 * Get or fetch Functions
	 */

	public function getApplicationName() {
		return $this->m_strApplicationName;
	}

	public function getReferrerTypeName() {
		return $this->m_strReferrerTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferrerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferrerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['application_name'] ) ) 		$this->setApplicationName( $arrmixValues['application_name'] );
		if( true == isset( $arrmixValues['referrer_type_name'] ) ) 		$this->setReferrerTypeName( $arrmixValues['referrer_type_name'] );
	}

}
?>