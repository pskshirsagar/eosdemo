<?php

class CCommercialSaleBreakpoint extends CBaseCommercialSaleBreakpoint {

	const MONTHLY       = 1;
	const END_OF_PERIOD = 2;

	public static $c_arrintReportTimings = [ self::MONTHLY, self::END_OF_PERIOD ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	protected $m_boolIsMonthlySalesAssociated;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_monthly_sale_associated'] ) ) {
			$this->setIsMonthlySalesAssociated( $arrmixValues['is_monthly_sale_associated'] );
		}
	}

	public function getIsMonthlySalesAssociated() {
		return $this->m_boolIsMonthlySalesAssociated;
	}

	public function setIsMonthlySalesAssociated( $boolIsMonthlySalesAssociated ) {
		$this->m_boolIsMonthlySalesAssociated = $boolIsMonthlySalesAssociated;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getReportTimings() : array {
		return [
			self::MONTHLY         => __( 'Monthly' ),
			self::END_OF_PERIOD   => __( 'End of Period' )
		];
	}

}
?>