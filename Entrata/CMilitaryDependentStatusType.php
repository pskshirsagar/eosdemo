<?php

class CMilitaryDependentStatusType extends CBaseMilitaryDependentStatusType {


	const WITH_DEPENDENT			= 1;
	const WITHOUT_DEPENDENT			= 2;

	public static $c_arrintMilitaryDependentStatusTypes = [
		self::WITH_DEPENDENT => [
			'id'    => '1',
			'name' => 'With Dependent'
		],
		self::WITHOUT_DEPENDENT => [
			'id'    => '2',
			'name' => 'Without Dependent'
		]
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
