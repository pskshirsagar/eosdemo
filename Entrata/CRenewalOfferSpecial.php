<?php
class CRenewalOfferSpecial extends CBaseRenewalOfferSpecial {

	protected $m_strSpecialName;
	protected $m_intSpecialPostTypeId;
	protected $m_intArCodeId;
	protected $m_intAmount;
	protected $m_strSpecialDescription;
	protected $m_intCompanyMediaFileId;

    /**
     * Get Functions
     */

    public function getSpecialName() {
    	return $this->m_strSpecialName;
    }

    public function getSpecialPostTypeId() {
    	return $this->m_intSpecialPostTypeId;
    }

    public function getArCodeId() {
    	return $this->m_intArCodeId;
    }

    public function getAmount() {
    	return $this->m_intAmount;
    }

    public function getSpecialDescription() {
    	return $this->m_strSpecialDescription;
    }

    public function getCompanyMedialFileId() {
    	return $this->m_intCompanyMediaFileId;
    }

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrValues['special_name'] ) && $boolDirectSet ) {
			$this->m_strSpecialName = trim( $arrValues['special_name'] );
		} elseif( isset( $arrValues['special_name'] ) ) {
			$this->setSpecialName( $arrValues['special_name'] );
		}

		if( isset( $arrValues['special_description'] ) && $boolDirectSet ) {
			$this->m_strSpecialDescription = trim( $arrValues['special_description'] );
		} elseif( isset( $arrValues['special_description'] ) ) {
			$this->setSpecialDescription( $arrValues['special_description'] );
		}

		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) {
			$this->m_intCompanyMediaFileId = trim( $arrValues['company_media_file_id'] );
		} elseif( isset( $arrValues['company_media_file_id'] ) ) {
			$this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		}

		if( isset( $arrValues['special_post_type_id'] ) && $boolDirectSet ) {
			$this->m_intSpecialPostTypeId = trim( $arrValues['special_post_type_id'] );
		} elseif( isset( $arrValues['special_post_type_id'] ) ) {
			$this->setSpecialPostTypeId( $arrValues['special_post_type_id'] );
		}

    	if( isset( $arrValues['amount'] ) && $boolDirectSet ) {
    		$this->m_intAmount = trim( $arrValues['amount'] );
    	} elseif( isset( $arrValues['amount'] ) ) {
    		$this->setAmount( $arrValues['amount'] );
    	}

    	if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) {
    		$this->m_intArCodeId = trim( $arrValues['ar_code_id'] );
    	} elseif( isset( $arrValues['ar_code_id'] ) ) {
    		$this->setArCodeId( $arrValues['ar_code_id'] );
    	}
    }

    public function setSpecialName( $strSpecialName ) {
    	$this->m_strSpecialName = $strSpecialName;
    }

    public function setSpecialPostTypeId( $intSpecialPostTypeId ) {
    	$this->m_intSpecialPostTypeId = $intSpecialPostTypeId;
    }

    public function setArCodeId( $intArCodeId ) {
    	$this->m_intArCodeId = $intArCodeId;
    }

    public function setAmount( $intAmount ) {
    	$this->m_intAmount = $intAmount;
    }

    public function setSpecialDescription( $strSpecialDescription ) {
    	$this->m_strSpecialDescription = $strSpecialDescription;
    }

    public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
    	return $this->m_intCompanyMediaFileId = $intCompanyMediaFileId;
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRenewalOfferId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSpecialId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>