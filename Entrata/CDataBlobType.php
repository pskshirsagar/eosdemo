<?php

class CDataBlobType extends CBaseDataBlobType {

	const LEASE_CUSTOMERS 		= 1;
	const PROPERTIES 			= 2;
	const LEADS 				= 3;
	const EMPLOYEES 			= 4;
	const OWNERS 				= 5;
	const VENDORS 				= 6;
	const LEASE_CUSTOMERS_LEADS	= 7;
	const REPORTS				= -1;	// This is temporary blob type constant for reports.
	const COMPANY_OWNERS		= 8;
	const RESERVATIONHUB		= 9;

}
?>