<?php

class CFileTag extends CBaseFileTag {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeyword( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( false == valStr( $this->getKeyword() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Tag name is required.' ) ) );
            return $boolIsValid;
        }

        if( false == is_null( $objDatabase ) ) {

            $intConfilictingFileTagCount = CFileTags::fetchConflictingFileTagCount( $this, $objDatabase );

            if( 0 < $intConfilictingFileTagCount ) {
                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Tag name is already in use.' ) ) );
                return $boolIsValid;
            }
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
                $boolIsValid &= $this->valKeyword( $objDatabase );
                break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;

        }

        return $boolIsValid;
    }

}

?>