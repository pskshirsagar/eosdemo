<?php

class CIntegrationSyncType extends CBaseIntegrationSyncType {

	const NOW 			= 1;
	const QUEUE 		= 2;
	const SCHEDULED 	= 3;
	const ONCE 			= 4;
	const MANUAL		= 5;
	const AMQP_QUEUE    = 6;

	public static $c_arrintIntegrationSyncTypeNameById = [
		self::NOW		 => 'Now',
		self::QUEUE		 => 'Queue',
		self::SCHEDULED  => 'Scheduled',
		self::ONCE		 => 'Once',
		self::MANUAL	 => 'Manual',
		self::AMQP_QUEUE => 'Amqp Queue'
	];

}

?>