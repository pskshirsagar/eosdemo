<?php

class CTabletButton extends CBaseTabletButton {
    protected $m_strName;
    protected $m_strHandle;
	protected $m_objTabletButtonType;

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valWebsiteId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getWebsiteId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valTabletButtonTypeId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getTabletButtonTypeId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tablet_button_type_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getOrderNum())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ));
        // }

        return $boolIsValid;
    }

    public function valExternalUrl() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strExternalUrl ) ) {
			if( false == CValidation::checkUrl( $this->m_strExternalUrl ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_url', 'Invalid URL.  Sample for correct url ' . CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valExternalUrl();
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getName() {
        return $this->m_strName;
    }

    public function getHandle() {
        return $this->m_strHandle;
    }

    public function getTabletButtonType() {
        return $this->m_objTabletButtonType;
    }

    /**
     * Set Functions
     */

    public function setName( $strName ) {
        $this->m_strName = $strName;
    }

    public function setHandle( $strHandle ) {
        $this->m_strHandle = $strHandle;
    }

    public function setTabletButtonType( $objTabletButtonType ) {
        $this->m_objTabletButtonType = $objTabletButtonType;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['name'] ) ) 	        $this->setName( $arrValues['name'] );
		if( true == isset( $arrValues['handle'] ) )			$this->setHandle( $arrValues['handle'] );

		return;
    }

}
?>