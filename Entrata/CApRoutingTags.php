<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApRoutingTags
 * Do not add any new functions to this class.
 */

class CApRoutingTags extends CBaseApRoutingTags {

	public static function fetchActiveApRoutingTagsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_routing_tags
					WHERE
						is_published = 1
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchApRoutingTags( $strSql, $objClientDatabase );
	}

	public static function fetchApRoutingTagsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_routing_tags
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
					ORDER BY
						is_published DESC,
						name';

		return self::fetchApRoutingTags( $strSql, $objClientDatabase );
	}

	public static function fetchApRoutingTagsNotByIdByCid( $intId, $intCid, $objClientDatabase ) {
		if( false == valId( $intId ) ) return false;

		$strSql = 'SELECT
						id,
						name
					FROM
						ap_routing_tags
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	id <> ' . ( int ) $intId . '
						AND is_published = 1
						AND deleted_by IS NULL';

		return self::fetchApRoutingTags( $strSql, $objClientDatabase );
	}

	public static function fetchValidatedApRoutingTagByIdByCid( $intId, $intCid, $objClientDatabase ) {
		if( false == valId( $intId ) ) return false;

		$strSql = 'SELECT
						count( ah.id ) AS ap_header
					FROM
						ap_routing_tags art
						LEFT JOIN ap_headers ah ON ( art.cid = ah.cid AND art.id = ah.ap_routing_tag_id )
					WHERE
						art.cid = ' . ( int ) $intCid . '
						AND art.id = ' . ( int ) $intId . '
						AND art.deleted_by IS NULL
					LIMIT 1';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		return $arrmixData[0];
	}

	public static function fetchApRoutingTagsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						art.*
					FROM
						ap_routing_tags art
					WHERE
						art.cid = ' . ( int ) $intCid . '
						AND art.id IN( ' . implode( ',', $arrintIds ) . ' )
						AND is_published = 1
						AND art.deleted_by IS NULL';

		return self::fetchApRoutingTags( $strSql, $objClientDatabase );
	}

}
?>