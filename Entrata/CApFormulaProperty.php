<?php

class CApFormulaProperty extends CBaseApFormulaProperty {

	protected $m_intPropertyUnitCount;
	protected $m_intApFormulaTypeId;

	protected $m_strPropertyName;
	protected $m_strAccountName;
	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;

	protected $m_arrobjGlAccountProperties;
	protected $m_arrobjApFormulaProperties;

	/**
	 * Get Functions
	 */

	public function getApFormulaTypeId() {
		return $this->m_intApFormulaTypeId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getGlAccountProperties() {
		return $this->m_arrobjGlAccountProperties;
	}

	public function getApFormulaProperties() {
		return $this->m_arrobjApFormulaProperties;
	}

	public function getPropertyUnitCount() {
		return $this->m_intPropertyUnitCount;
	}

	/**
	 * Set Functions
	 */

	public function setApFormulaTypeId( $intApFormulaTypeId ) {
		$this->m_intApFormulaTypeId = CStrings::strToIntDef( $intApFormulaTypeId, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = CStrings::strTrimDef( $strGlAccountName, 50, NULL, true );
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = CStrings::strTrimDef( $strGlAccountNumber, 50, NULL, true );
	}

	public function setGlAccountProperties( $arrobjGlAccountProperties ) {
		$this->m_arrobjGlAccountProperties = $arrobjGlAccountProperties;
	}

	public function setApFormulaProperties( $arrobjApFormulaProperties ) {
		$this->m_arrobjApFormulaProperties = $arrobjApFormulaProperties;
	}

	public function setPropertyUnitCount( $intPropertyUnitCount ) {
		$this->m_intPropertyUnitCount = CStrings::strToIntDef( $intPropertyUnitCount, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) )					$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['account_name'] ) )					$this->setAccountName( $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['gl_account_name'] ) ) 				$this->setGlAccountName( $arrmixValues['gl_account_name'] );
		if( true == isset( $arrmixValues['gl_account_number'] ) )				$this->setGlAccountNumber( $arrmixValues['gl_account_number'] );
		if( true == isset( $arrmixValues['ap_formula_type_id'] ) )	$this->setGlAccountNumber( $arrmixValues['ap_formula_type_id'] );

		return;
	}

	/**
	 * Validation Functions
	 */

	public function valPropertyId() {

		if( false == is_numeric( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
			return false;
		}

		$arrobjApFormulaProperties = ( array ) $this->getApFormulaProperties();

		if( true == valArr( $arrobjApFormulaProperties ) ) {

			foreach( $arrobjApFormulaProperties as $intKey1 => $objApFormulaProperty1 ) {

				foreach( $arrobjApFormulaProperties as $intKey2 => $objApFormulaProperty2 ) {

					if( $intKey1 != $intKey2
							&& $objApFormulaProperty1->getPropertyId() == $objApFormulaProperty2->getpropertyId()
							&& $objApFormulaProperty1->getGlAccountId() == $objApFormulaProperty2->getGlAccountId() ) {

						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is repeated.' ) ) );
						return false;
					}
				}
			}
		}

		return true;
	}

	public function valGlAccountId( $objApFormula ) {

		$boolIsValid = true;

		if( 1 == $objApFormula->getIsGlAccountAssociated() && false == is_numeric( $this->getGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is required.' ) ) );
			$boolIsValid &= false;
		}

		$arrobjGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == array_key_exists( $this->getGlAccountId(), $arrobjGlAccountProperties ) ) {

			if( false == array_key_exists( $this->getPropertyId(), $arrobjGlAccountProperties[$this->getGlAccountId()] ) ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0}" is not associated with "{%s, 1}" Property.', [ $this->getGlAccountName(), $this->getPropertyName() ] ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valPercent() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getPercent() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Percent is required.' ) ) );
			$boolIsValid &= false;
		} elseif( 0 >= $this->getPercent() || 100 < $this->getPercent() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Percent should be greater than {%d, 0} and less than or equal to {%d, 1}.', [ 0, 100 ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPropertyUnitCount() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getPropertyUnitCount() ) || 0 == $this->getPropertyUnitCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'units', __( "You may not use the property '{%s, 0}' in invoice allocation as the count of units or count of spaces for this property is zero for selected unit types.", [ $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objApFormula = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_invoice_allocation_type_percent':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId( $objApFormula );
				$boolIsValid &= $this->valPercent();
				break;

			case 'validate_invoice_allocation_type_no_of_units':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId( $objApFormula );
				$boolIsValid &= $this->valPropertyUnitCount();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>