<?php

class CIntegrationClient extends CBaseIntegrationClient {

	protected $m_objProperty;

	protected $m_strBaseServiceUri;
	protected $m_strIntermediaryServiceUri;
	protected $m_strFinalServiceUri;
	protected $m_strIntegrationClientTypeName;
	protected $m_strIntegrationVersionName;
	protected $m_strClientName;
	protected $m_strCompanyStatusTypeName;
	protected $m_strIntegrationDatabaseName;
	protected $m_strIntegrationSyncTypeName;
	protected $m_strIntegrationPeriodName;
	protected $m_strIntegrationServiceName;
	protected $m_strIntegrationClientStatusTypeName;
	protected $m_strIntegrationClientDatabaseName;

	protected $m_intIntegrationDatabaseIntegrationClientStatusTypeId;

    /**
     * Get Functions
     */

    public function getBaseServiceUri() {
    	return $this->m_strBaseServiceUri;
	}

	 public function getIntegrationDatabaseIntegrationClientStatusTypeId() {
    	return $this->m_intIntegrationDatabaseIntegrationClientStatusTypeId;
	 }

	public function getFinalServiceUri() {
    	return $this->m_strFinalServiceUri;
	}

	public function getUsername() {
		if( false == valStr( $this->m_strUsernameEncrypted ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strUsernameEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getPassword() {
		if( false == valStr( $this->m_strPasswordEncrypted ) ) {
			return NULL;
		}

		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getIntegrationClientTypeName() {
		return $this->m_strIntegrationClientTypeName;
	}

	public function getIntegrationVersionName() {
		return $this->m_strIntegrationVersionName;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getCompanyStatusTypeName() {
		return $this->m_strCompanyStatusTypeName;
	}

	public function getIntegrationDatabaseName() {
		return $this->m_strIntegrationDatabaseName;
	}

	public function getIntegrationSyncTypeName() {
		return $this->m_strIntegrationSyncTypeName;
	}

	public function getIntegrationPeriodName() {
		return $this->m_strIntegrationPeriodName;
	}

	public function getIntegrationServiceName() {
		return $this->m_strIntegrationServiceName;
	}

	public function getIntegrationClientStatusTypeName() {
		return $this->m_strIntegrationClientStatusTypeName;
	}

	public function getIntegrationClientDatabaseName() {
		return $this->m_strIntegrationClientDatabaseName;
	}

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['username'] ) : $arrmixValues['username'] );
        if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['password'] ) : $arrmixValues['password'] );
        if( true == isset( $arrmixValues['base_service_uri'] ) ) $this->setBaseServiceUri( $arrmixValues['base_service_uri'] );
        if( true == isset( $arrmixValues['integration_database_integration_client_status_type_id'] ) ) $this->setIntegrationDatabaseIntegrationClientStatusTypeId( $arrmixValues['integration_database_integration_client_status_type_id'] );
        if( true == isset( $arrmixValues['intermediary_service_uri'] ) ) $this->setIntermediaryServiceUri( $arrmixValues['intermediary_service_uri'] );
        if( true == isset( $arrmixValues['final_service_uri'] ) ) $this->setFinalServiceUri( $arrmixValues['final_service_uri'] );
        if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
        if( true == isset( $arrmixValues['integration_version_name'] ) ) $this->setIntegrationVersionName( $arrmixValues['integration_version_name'] );
        if( true == isset( $arrmixValues['client_name'] ) ) $this->setClientName( $arrmixValues['client_name'] );
        if( true == isset( $arrmixValues['company_status_type_name'] ) ) $this->setCompanyStatusTypeName( $arrmixValues['company_status_type_name'] );
        if( true == isset( $arrmixValues['integration_database_name'] ) ) $this->setIntegrationDatabaseName( $arrmixValues['integration_database_name'] );
        if( true == isset( $arrmixValues['integration_sync_type_name'] ) ) $this->setIntegrationSyncTypeName( $arrmixValues['integration_sync_type_name'] );
        if( true == isset( $arrmixValues['integration_period_name'] ) ) $this->setIntegrationPeriodName( $arrmixValues['integration_period_name'] );
        if( true == isset( $arrmixValues['integration_service_name'] ) ) $this->setIntegrationServiceName( $arrmixValues['integration_service_name'] );
        if( true == isset( $arrmixValues['integration_client_status_type_name'] ) ) $this->setIntegrationClientStatusTypeName( $arrmixValues['integration_client_status_type_name'] );
        if( true == isset( $arrmixValues['name'] ) ) $this->setIntegrationClientDatabaseName( $arrmixValues['name'] );

        return;
    }

	public function setBaseServiceUri( $strBaseServiceUri ) {
    	$this->m_strBaseServiceUri = $strBaseServiceUri;
	}

	public function setIntegrationDatabaseIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
    	$this->m_intIntegrationDatabaseIntegrationClientStatusTypeId = $intIntegrationClientStatusTypeId;
	}

	public function setFinalServiceUri( $strFinalServiceUri ) {
    	$this->m_strFinalServiceUri = $strFinalServiceUri;
	}

	public function setUsername( $strUsername ) {
		if( true == valStr( $strUsername ) ) {
			$this->setUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setSyncTimesByBitstring( $strBitString ) {
		$this->setBitwiseSyncTimes( ( int ) bindec( $strBitString ) );
	}

	public function setSyncHour( $intMilitaryHour ) {
		if( false == is_numeric( $intMilitaryHour ) ) {
			trigger_error( 'Invalid Integration Client Request: Bitwise flag is not numeric in set', E_USER_ERROR );
		}

		$this->setBitwiseSyncTimes( $this->getBitwiseSyncTime() | $this->bitwiseFlagToInteger( ( int ) $intMilitaryHour ) );
	}

	public function setSyncHours( $strCommaDelimitedHours ) {
		foreach( explode( ',', $strCommaDelimitedHours ) as $intMilitaryHour ) {
			$this->setSyncHour( trim( $intMilitaryHour ) );
		}
	}

	public function setProperty( $objProperty ) {
		return $this->m_objProperty = $objProperty;
	}

	public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		return $this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
	}

	public function setIntegrationVersionName( $strIntegrationVersionName ) {
		return $this->m_strIntegrationVersionName = $strIntegrationVersionName;
	}

	public function setClientName( $strClientName ) {
		return $this->m_strClientName = $strClientName;
	}

	public function setCompanyStatusTypeName( $strCompanyStatusTypeName ) {
		return $this->m_strCompanyStatusTypeName = $strCompanyStatusTypeName;
	}

	public function setIntegrationDatabaseName( $strIntegrationDatabaseName ) {
		return $this->m_strIntegrationDatabaseName = $strIntegrationDatabaseName;
	}

	public function setIntegrationSyncTypeName( $strIntegrationSyncTypeName ) {
		return $this->m_strIntegrationSyncTypeName = $strIntegrationSyncTypeName;
	}

	public function setIntegrationPeriodName( $strIntegrationPeriodName ) {
		return $this->m_strIntegrationPeriodName = $strIntegrationPeriodName;
	}

	public function setIntegrationServiceName( $strIntegrationServiceName ) {
		return $this->m_strIntegrationServiceName = $strIntegrationServiceName;
	}

	public function setIntegrationClientStatusTypeName( $strIntegrationClientStatusTypeName ) {
		return $this->m_strIntegrationClientStatusTypeName = $strIntegrationClientStatusTypeName;
	}

	public function setIntegrationClientDatabaseName( $strIntegrationClientDatabaseName ) {
		return $this->m_strIntegrationClientDatabaseName = $strIntegrationClientDatabaseName;
	}

	/**
	 * Fetch Functions
	 */

    public function fetchClient( $objDatabase ) {

        return CClients::fetchClientById( $this->getCid(), $objDatabase );
    }

    public function fetchIntegrationDatabase( $objDatabase ) {

        return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchCustomIntegrationDatabaseByIdByCid( $this->getIntegrationDatabaseId(), $this->getCid(), $objDatabase );
    }

    public function fetchIntegrationQueuesByIntegrationQueueStatusTypeId( $intIntegrationQueueStatusTypeId, $objDatabase ) {

        return \Psi\Eos\Entrata\CIntegrationQueues::createService()->fetchIntegrationQueuesByIntegrationClientIdByIntegrationQueueStatusTypeIdByCid( $this->m_intId, $intIntegrationQueueStatusTypeId, $this->getCid(), $objDatabase );
    }

    public function fetchIntegrationQueues( $objDatabase ) {

    	return \Psi\Eos\Entrata\CIntegrationQueues::createService()->fetchIntegrationQueuesByIntegrationClientIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
    }

    public function fetchIntegrationClientKeyValues( $objDatabase ) {
    	return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByIntegrationClientIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
    }

    public function fetchIntegrationClientKeyValueByKey( $strKey, $objDatabase ) {
    	if( false == valStr( $strKey ) ) return NULL;
    	return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByKeyByIntegrationClientIdByCid( $strKey, $this->m_intId, $this->getCid(), $objDatabase );
    }

	public function fetchPropertyIntegrationClients( $objDatabase ) {
    	return \Psi\Eos\Entrata\CPropertyIntegrationClients::createService()->fetchPropertyIntegrationClientsByIntegrationClientIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
    }

   	/**
   	 * Validation Functions
   	 */

    public function valIntegrationDatabaseId() {
        $boolIsValid = true;

		if( false == isset( $this->m_intIntegrationDatabaseId ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_database_id', 'You must provide an integration database id.' ) );
		}

		return $boolIsValid;
    }

    public function valIntegrationClientTypeId() {
        $boolIsValid = true;

		if( false == isset( $this->m_intIntegrationClientTypeId ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_client_type_id', 'You must provide an integration client type id.' ) );
		}

        return $boolIsValid;
    }

    public function valIntegrationSyncTypeId() {
        $boolIsValid = true;

		if( false == isset( $this->m_intIntegrationSyncTypeId ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_sync_type_id', 'You must provide an integration sync type id.' ) );
		}

        return $boolIsValid;
    }

	public function valIsPropertySpecific() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHourlyRunValues() {
        $boolIsValid = true;

        if( false == is_null( $this->m_strHourlyRunValues ) ) {

        	$strTempHourlyRunValues 	= preg_replace( '/[^0-9,]/', '', $this->m_strHourlyRunValues );

			if( $strTempHourlyRunValues != $this->m_strHourlyRunValues ) {
				$boolIsValid = false;
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_run_values', 'Enter a valid Hourly Run Values.' ) );
			} elseif( false == preg_match( '/(^,)(.*)(,$)/', $this->m_strHourlyRunValues ) ) {
				$boolIsValid = false;
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_run_values', 'Hourly Run Values should start and end with comma.' ) );
			} elseif( false == preg_match( '/(^,)[0-9]+(,([0-9])+)*(,$)/', $this->m_strHourlyRunValues ) ) {
				$boolIsValid = false;
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_run_values', 'Enter a valid Hourly Run Values.' ) );
			}
		}

        return $boolIsValid;
    }

    public function valIntegrationServiceId( $objDatabase ) {

        $boolIsValid = true;

        if( false == isset( $this->m_intIntegrationServiceId ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_service_id', 'You must provide an integration service id.' ) );
		}

		if( true == $boolIsValid && false == is_null( $objDatabase ) ) {

			$intExistingIntegrationClientCount = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientCountByIntegrationDatabaseIdByServiceIdByCid( $this->getIntegrationDatabaseId(), $this->getIntegrationServiceId(), $this->getCid(), $objDatabase );

			if( 0 < $intExistingIntegrationClientCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_service_id', 'Service is already integrated.' ) );
			}
		}

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valServiceUri() {
        $boolIsValid = true;

        // Strip Out extra slashes PY
        if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->getServiceUri() ) ) {
        	$this->setServiceUri( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->getServiceUri() ) );
        }

		if( ( true == is_null( $this->getBaseServiceUri() ) && true == is_null( $this->getServiceUri() ) ) || false == CValidation::checkUrl( $this->getBaseServiceUri() . $this->getServiceUri() ) ) {
	    	$boolIsValid = false;
	    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_uri', 'Url of web service is required.' ) );
			return false;
    	} else {
    		$strWsdUrl = $this->getBaseServiceUri() . $this->getServiceUri();
    	}
	    $arrstrStreamOptions = [];
	    if( CIntegrationClientType::EMH_ONE_WAY == $this->getIntegrationClientTypeId() ) {
			$arrmixSoapParams 	= [ 'trace' => 1, 'exceptions' => 1, 'keep_alive' => false, 'local_cert' => '/etc/ssl/identrust/identrust.pem', 'passphrase' => 'eIUyx&aWsO5z9fiO5amE' ];
		} else {
			$arrmixSoapParams 	 = [ 'trace' => 1, 'exceptions' => 1, 'keep_alive' => false ];
		    $arrstrStreamOptions = [ 'ssl' => [ 'ciphers' => 'AES256-SHA:DES-CBC3-SHA:AES128-SHA:RC4-SHA:RC4-MD5', 'verify_peer_name' => false, 'verify_peer' => false ] ];
	    }
		$strStreamContext                   = stream_context_create( $arrstrStreamOptions );
		$arrmixSoapParams['stream_context'] = $strStreamContext;

		try {
			@$objSoapClient  = new SoapClient( $strWsdUrl, $arrmixSoapParams );
		} catch( SoapFault $objSoapFault ) {
			$boolIsValid = false;

			// if the url didn't work and the ssl setting is set to true, check to see if the wsdl can be found without ssl.  If so give
			// ssl is not installed error, otherwise, give file does not exist error.

	    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_uri', 'WSDL service uri connection could not be made.  Make sure your service uri is correct.' ) );
		}

        return $boolIsValid;
    }

    public function valSyncFromDate() {
    	$boolIsValid = true;

    	if( true == isset( $this->m_intIntegrationServiceId ) && $this->m_intIntegrationServiceId == CIntegrationService::SEND_EVENT && false == isset( $this->m_strSyncFromDate ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sync_from_date', 'You must provide a Sync from date.' ) );
    	}

    	return $boolIsValid;
    }

    public function valUsername() {
        $boolIsValid = true;

        if( true == is_null( trim( $this->getUsername() ) ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getUsername() ) ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', ' Username is required.' ) );
		}

        return $boolIsValid;
    }

    public function valPassword() {
        $boolIsValid = true;

        if( true == is_null( trim( $this->getPassword() ) ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getPassword() ) ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', ' Password is required.' ) );
		}

        return $boolIsValid;
    }

    public function valDatabaseName() {
        $boolIsValid = true;

        if( CIntegrationClientType::CREDIT_SYSTEM == $this->m_intIntegrationClientTypeId ) {
        	return $boolIsValid;
        }

        if( true == is_null( trim( $this->m_strDatabaseName ) ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->m_strDatabaseName ) ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', ' Database name is required.' ) );
		}

        return $boolIsValid;
    }

    public function valBitwiseSyncTimes() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valLastSyncOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valExpiresOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsSkipValidation = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valIntegrationDatabaseId();
            	$boolIsValid &= $this->valIntegrationClientTypeId();
            	$boolIsValid &= $this->valIntegrationSyncTypeId();
            	$boolIsValid &= $this->valHourlyRunValues();
            	$boolIsValid &= $this->valIntegrationServiceId( $objDatabase );
            	$boolIsValid &= $this->valSyncFromDate();

           		if( false == in_array( $this->getIntegrationClientTypeId(), [CIntegrationClientType::MRI_SIMPLE, CIntegrationClientType::ENTRATA] ) && !$boolIsSkipValidation ) {
            		$boolIsValid &= $this->valServiceUri();
            	}
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Create Functions
     */

	public function createIntegrationResult() {

		$objIntegrationResult = new CIntegrationResult();
		$objIntegrationResult->setDefaults();
		$objIntegrationResult->setCid( $this->getCid() );
		$objIntegrationResult->setIntegrationClientId( $this->getId() );
		$objIntegrationResult->setIntegrationServiceId( $this->getIntegrationServiceId() );

		return $objIntegrationResult;
	}

	public function createIntegrationClientKeyValue() {

		$objIntegrationKeyValue = new CIntegrationClientKeyValue();
		$objIntegrationKeyValue->setCid( $this->getCid() );
		$objIntegrationKeyValue->setIntegrationClientId( $this->getId() );
		$objIntegrationKeyValue->setIntegrationDatabaseId( $this->getIntegrationDatabaseId() );

		return $objIntegrationKeyValue;
	}

	public function createPropertyIntegrationClient() {

		$objPropertyIntegrationClient = new CPropertyIntegrationClient();
		$objPropertyIntegrationClient->setCid( $this->getCid() );
		$objPropertyIntegrationClient->setIntegrationClientId( $this->getId() );

		return $objPropertyIntegrationClient;
	}

	/**
	 * Other Functions
	 */

	public function getOrFetchServiceUri( $objDatabase ) {

		if( 0 == \Psi\CStringService::singleton()->strlen( $this->getServiceUri() ) ) {

			$objIntegrationDatabase = $this->fetchIntegrationDatabase( $objDatabase );

    		$strEnvironment = CConfig::get( 'environment' );
			$strEnvironment = ( true == valStr( $strEnvironment ) ) ? $strEnvironment : 'production';

			if( 'production' === $strEnvironment ) {
				$this->setBaseServiceUri( $objIntegrationDatabase->getBaseUri() );

			} else {
				$this->setBaseServiceUri( $objIntegrationDatabase->getDevBaseUri() );
			}

			$this->setFinalServiceUri( $this->getSeriviceUri() );
			$this->rebuildServiceUri();
		}

		return $this->getServiceUri();
	}

    public function rebuildServiceUri() {
    	$strFinalServiceUri = $this->getBaseServiceUri() . $this->getServiceUri();
		$this->setFinalServiceUri( $strFinalServiceUri );
    }

     public function unsetSyncHour( $intMilitaryHour ) {
		if( false == is_numeric( $intMilitaryHour ) ) {
			trigger_error( 'Invalid Integration Client Request: Bitwise flag is not numeric in unset', E_USER_ERROR );
		}

		$this->setBitwiseSyncTimes( $this->getBitwiseSyncTime() & ( ~$this->bitwiseFlagToInteger( ( int ) $intMilitaryHour ) ) );
	 }

	public function unsetSyncHours( $strCommaDelimitedHours ) {
		foreach( explode( ',', $strCommaDelimitedHours ) as $intMilitaryHour ) {
			$this->unsetSyncHour( trim( $intMilitaryHour ) );
		}
	}

    public function holdActiveQueueItems( $intCompanyUserId, $objDatabase ) {
    	$boolIsValid = true;

    	// Fetch Active Queue Items that have not been sent
    	$arrobjIntegrationQueues = $this->fetchIntegrationQueuesByIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::READY, $objDatabase );

    	if( true == isset ( $arrobjIntegrationQueues ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjIntegrationQueues ) ) {
    		foreach( $arrobjIntegrationQueues as $objIntegrationQueue ) {
    			$objIntegrationQueue->setIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::WAITING );
    			$boolIsValid &= $objIntegrationQueue->update( $intCompanyUserId, $objDatabase );
    		}
    	}

        return $boolIsValid;
    }

    public function activateOnHoldQueueItems( $intCompanyUserId, $objDatabase ) {
    	$boolIsValid = true;

    	// Fetch OnHold Queue Items that haven't been sent
    	$arrobjIntegrationQueues = $this->fetchIntegrationQueuesByIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::WAITING, $objDatabase );

    	if( true == isset( $arrobjIntegrationQueues ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjIntegrationQueues ) ) {
    		foreach( $arrobjIntegrationQueues as $objIntegrationQueue ) {
    			$objIntegrationQueue->setIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::READY );
    			$boolIsValid &= $objIntegrationQueue->update( $intCompanyUserId, $objDatabase );
    		}
    	}

        return $boolIsValid;
   	}

	private function bitwiseFlagToInteger( $intBitNumber ) {
		switch( $intBitNumber ) {
			case 0:
				return 1;

			case 1:
				return 2;

			case 2:
				return 4;

			case 3:
				return 8;

			case 4:
				return 16;

			case 5:
				return 32;

			case 6:
				return 64;

			case 7:
				return 128;

			case 8:
				return 256;

			case 9:
				return 512;

			case 10:
				return 1024;

			case 11:
				return 2048;

			case 12:
				return 4096;

			case 13:
				return 8192;

			case 14:
				return 16384;

			case 15:
				return 32768;

			case 16:
				return 65536;

			case 17:
				return 131072;

			case 18:
				return 262144;

			case 19:
				return 524288;

			case 20:
				return 1048576;

			case 21:
				return 2097152;

			case 22:
				return 4194304;

			case 23:
				return 8388608;

			default:
				trigger_error( 'Invalid Integration Client Request: Unable to map a bitwise flag to an integer', E_USER_ERROR );
				break;
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strIntegrationClientNewKeyValuePair = '';
		$arrstrIntegrationClient = $this->objectToArrayMapping();
		$arrstrSubmittedKeys 	= array_keys( $arrstrIntegrationClient );
		if( true == valArr( $arrstrSubmittedKeys ) ) {
			foreach( $arrstrSubmittedKeys as $strValue ) {
				$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrIntegrationClient ) ) ? $arrstrIntegrationClient[$strValue] : '';
				$strIntegrationClientNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
			}
		}

		if( CIntegrationClientType::YARDI_RPORTAL == $this->getIntegrationClientTypeId() ) {
			$this->setIsPropertySpecific( true );
		}

		if( false === parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
			return false;

		$objTableLog = new CTableLog();
		$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_clients', $this->getCid(), 'INSERT', $this->getCid(), NULL, $strIntegrationClientNewKeyValuePair );

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objCurrentIntegrationClient = NULL ) {

		if( false === parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
			return false;

		if( true == valObj( $objCurrentIntegrationClient, 'CIntegrationClient' ) ) {
			// new values
			$arrstrRequestIntegrationClient = $this->objectToArrayMapping();
			// old values
			$arrstrCurrentIntegrationClient = $objCurrentIntegrationClient->objectToArrayMapping();

			$arrstrDifferenceInArrays = array_diff_assoc( $arrstrRequestIntegrationClient, $arrstrCurrentIntegrationClient );

			if( true == valArr( $arrstrDifferenceInArrays ) ) {

				$arrstrExistingKeys  	= array_keys( $arrstrCurrentIntegrationClient );
				$arrstrSubmittedKeys 	= array_keys( $arrstrRequestIntegrationClient );

				$arrstrUnionIntegrationClientKeys = array_unique( array_merge( $arrstrExistingKeys, $arrstrSubmittedKeys ) );
				$strIntegrationClientNewKeyValuePair = '';
				$strIntegrationClientOldKeyValuePair = '';

				// Code added for formation of Integration Client key/value pair string -- starts
				if( true == valArr( $arrstrUnionIntegrationClientKeys ) ) {
					foreach( $arrstrUnionIntegrationClientKeys as $strValue ) {
						$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrRequestIntegrationClient ) ) ? $arrstrRequestIntegrationClient[$strValue] : '';

						$strIntegrationClientNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
						$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentIntegrationClient ) ) ? $arrstrCurrentIntegrationClient[$strValue] : '';

						$strIntegrationClientOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
					}
				}
				// Code added for formation of Integration Client key/value pair string -- ends

				$objTableLog = new CTableLog();
				$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_clients', $this->getCid(), 'UPDATE', $this->getCid(), $strIntegrationClientOldKeyValuePair, $strIntegrationClientNewKeyValuePair, 'An integration client has been modified.' );
				return true;
			}
		}
		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false === parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
			return false;

		$arrstrCurrentIntegrationClient = $this->objectToArrayMapping();
		$arrstrExistingKeys  	= array_keys( $arrstrCurrentIntegrationClient );
		$strIntegrationClientOldKeyValuePair = '';

		// Code added for formation of Integration Client key/value pair string -- starts
		if( true == valArr( $arrstrExistingKeys ) ) {
			foreach( $arrstrExistingKeys as $strValue ) {
				$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentIntegrationClient ) ) ? $arrstrCurrentIntegrationClient[$strValue] : '';
				$strIntegrationClientOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
			}
		}
		// Code added for formation of Integration Client key/value pair string -- ends

		$objTableLog = new CTableLog();
		$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_clients', $this->getCid(), 'DELETE', $this->getCid(), $strIntegrationClientOldKeyValuePair, NULL );

		return true;
	}

	public function objectToArrayMapping() {

		$arrstrIntegrationClient = [];
		$arrstrIntegrationClient['id'] 							= $this->getId();
		$arrstrIntegrationClient['cid'] 		= $this->getCid();
		$arrstrIntegrationClient['integration_database_id'] 	= $this->getIntegrationDatabaseId();
		$arrstrIntegrationClient['integration_client_type_id'] 	= $this->getIntegrationClientTypeId();
		$arrstrIntegrationClient['integration_client_status_type_id']	= $this->getIntegrationClientStatusTypeId();
		$arrstrIntegrationClient['integration_sync_type_id'] 			= $this->getIntegrationSyncTypeId();
		$arrstrIntegrationClient['integration_service_id'] 				= $this->getIntegrationServiceId();
		$arrstrIntegrationClient['integration_period_id'] 				= $this->getIntegrationPeriodId();
		$arrstrIntegrationClient['service_uri'] 						= $this->getServiceUri();
		$arrstrIntegrationClient['hourly_run_values'] 					= $this->getHourlyRunValues();
		$arrstrIntegrationClient['expires_on'] 							= $this->getExpiresOn();
		$arrstrIntegrationClient['order_num'] 							= $this->getOrderNum();

		return $arrstrIntegrationClient;
	}

}
?>