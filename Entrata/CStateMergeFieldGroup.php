<?php

class CStateMergeFieldGroup extends CBaseStateMergeFieldGroup {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMergeFieldGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

            default:
        }

        return $boolIsValid;
    }
}
?>