<?php

class CApPayeeComplianceStatus extends CBaseApPayeeComplianceStatus {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Cid is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Ap Payee is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApLegalEntityId( $objClientDatabase = NULL ) {

		if( false == valId( $this->getApLegalEntityId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_legal_entity_id', __( 'Ap legal entity is required.' ) ) );

			return false;
		}

		if( true == valObj( $objClientDatabase, CDatabase::class ) ) {
		$objApLegalEntity = \Psi\Eos\Entrata\CApLegalEntities::createService()->fetchApLegalEntityByIdByApPayeeIdByCid( $this->getApLegalEntityId(), $this->getApPayeeId(), $this->getCid(), $objClientDatabase, true );

		if( false == valObj( $objApLegalEntity, CApLegalEntity::class ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_legal_entity_id', __( 'Invalid ap legal entity.' ) ) );

			return false;
		}
		}

		return true;
	}

	public function valPropertyId( $objClientDatabase = NULL ) {

		if( false == valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );

			return false;
		}

		if( true == valObj( $objClientDatabase, CDatabase::class ) ) {
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		if( false == valObj( $objProperty, CProperty::class ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Invalid property.' ) ) );

			return false;
		}
		}

		return true;
	}

	public function valComplianceStatusId() {
		$boolIsValid = true;

		if( false == valId( $this->getComplianceStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_status_id', __( 'Compliance status is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUniqueRecord( $objClientDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique_record', __( 'Failed to load client database.' ) ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid ) {
			$arrobjApPayeeComplianceStatuses = ( array ) \Psi\Eos\Entrata\CApPayeeComplianceStatuses::createService()->fetchApPayeeComplianceStatusesByPropertyIdsByApLegalEntityIdsByApPayeeIdsByCid( [ $this->getPropertyId() ], [ $this->getApLegalEntityId() ], [ $this->getApPayeeId() ], $this->getCid(), $objClientDatabase );
			$objApPayeeComplianceStatus      = array_pop( $arrobjApPayeeComplianceStatuses );
			if( true == valObj( $objApPayeeComplianceStatus, 'CApPayeeComplianceStatus' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique_record', __( 'Combination of client, ap payee, ap legal entity and property is already exist.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolSkipEntityAndPropertyValidation = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valApLegalEntityId( ( false == $boolSkipEntityAndPropertyValidation ) ? $objClientDatabase : NULL );
				$boolIsValid &= $this->valPropertyId( ( false == $boolSkipEntityAndPropertyValidation ) ? $objClientDatabase : NULL );
				$boolIsValid &= $this->valComplianceStatusId();
				if( $strAction == VALIDATE_INSERT ) {
					$boolIsValid &= $this->valUniqueRecord( $objClientDatabase );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>