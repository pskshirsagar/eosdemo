<?php

class CScreeningApplicantRequirementType extends CBaseScreeningApplicantRequirementType {

	const DEFAULT		= 0;
	const REQUIRED 		= 1;
	const OPTIONAL	 	= 2;
	const NOT_REQUIRED	= 3;

	public static function getScreeningApplicantRequirementTypeCssClass( $intScreeningApplicantRequirementTypeId ) {
		$strScreeningApplicantRequirementTypeCssClass = '';

		switch( $intScreeningApplicantRequirementTypeId ) {
			case self::REQUIRED:
			case self::OPTIONAL:
				$strScreeningApplicantRequirementTypeCssClass = 'greenlight';
				break;

			case self::NOT_REQUIRED:
				$strScreeningApplicantRequirementTypeCssClass = 'redlight';
				break;

			default:
				// added default case
		}

		return $strScreeningApplicantRequirementTypeCssClass;
	}

	public static function getScreeningApplicantRequirementTypeName( $intScreeningApplicantRequirementTypeId ) {
		$strScreeningApplicantRequirementTypeName = '';

		switch( $intScreeningApplicantRequirementTypeId ) {
			case self::REQUIRED:
				$strScreeningApplicantRequirementTypeName = __( 'Required' );
				break;

			case self::OPTIONAL:
				$strScreeningApplicantRequirementTypeName = __( 'Optional' );
				break;

			case self::NOT_REQUIRED:
				$strScreeningApplicantRequirementTypeName = __( 'Not required' );
				break;

			default:
				// added default case
		}

		return $strScreeningApplicantRequirementTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>