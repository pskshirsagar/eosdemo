<?php

class CIpsClientChecklistQuestion extends CBaseIpsClientChecklistQuestion {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsChecklistId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>