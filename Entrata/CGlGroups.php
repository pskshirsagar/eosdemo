<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlGroups
 * Do not add any new functions to this class.
 */

class CGlGroups extends CBaseGlGroups {

	public static function fetchCustomGlGroupsByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gg.*,
						dgg.name as system_name
					FROM
						gl_groups gg
						LEFT JOIN default_gl_groups dgg ON ( dgg.id = gg.default_gl_group_id )
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gg.gl_tree_id = ' . ( int ) $intGlTreeId . '
					ORDER BY
						gg.order_num,
						gg.gl_group_type_id DESC';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdByParentGlGroupIdByCid( $intGlTreeId, $intParentGlGroupId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND parent_gl_group_id = ' . ( int ) $intParentGlGroupId . '
					ORDER BY
						order_num';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	// This function actually gets the main level groups nested under "income statement and balance sheet"

	public static function fetchRootGlGroupsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gg.*
					FROM
						gl_groups gg
						JOIN gl_groups gg_parent ON ( gg.cid = gg_parent.cid AND gg.parent_gl_group_id = gg_parent.id AND gg_parent.gl_account_type_id IS NULL )
					WHERE
						gg.cid =' . ( int ) $intCid;

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupByIdByGlTreeIdByCid( $intId, $intGLTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGLTreeId . '
						AND id = ' . ( int ) $intId;

		return self::fetchGlGroup( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchCustomGlGroupsByOriginGlGroupIdByCid( $intOriginGlGroupId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid =' . ( int ) $intCid . '
						AND origin_gl_group_id = ' . ( int ) $intOriginGlGroupId;

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	// This function returns the main groups i.e. "income statement and balance sheet"

	public static function fetchParentGlGroupsByGlTreeIdByCid( $intGLTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGLTreeId . '
						AND parent_gl_group_id IS NULL
					ORDER BY
						system_code DESC';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchCountEnableGlGroupByParentGlGroupIdByByCid( $intParentGlGroupId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( id )
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND parent_gl_group_id = ' . ( int ) $intParentGlGroupId . '
						AND disabled_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsBySystemCodeByCid( $strSystemCode, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND system_code = \'' . $strSystemCode . '\'';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchMaxOrderNumByParentGlGroupIdByCid( $intParentGlGroupId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						MAX( order_num ) AS order_num
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND parent_gl_group_id = ' . ( int ) $intParentGlGroupId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCountDisabledGlGroupByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( gg.id )
					FROM
						gl_account_trees gat
						JOIN gl_groups gg ON ( gat.cid = gg.cid AND gat.gl_group_id = gg.id AND gat.gl_tree_id = gg.gl_tree_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND gg.disabled_by IS NOT NULL
						AND EXISTS ( SELECT
										1
									FROM
										gl_trees gt
									WHERE
										gt.id = gat.gl_tree_id
										AND gt.cid = gat.cid
										AND gt.deleted_on IS NULL
									)';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdByParentGlGroupIdsByCid( $intGlTreeId, $arrintParentGlGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintParentGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND parent_gl_group_id IN ( ' . implode( ',',  $arrintParentGlGroupIds ) . ')
					ORDER BY
						order_num';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	// This function recursively search all parent groups and return 1st not flattened ( GL Group type Standard Group i.e. 1 ) parent Group

	public static function fetchNotFlattenedParentGlGroupByCidById( $intCid, $intId, $objClientDatabase ) {

		$strSql = 'WITH RECURSIVE
						recursive_gl_groups( id, parent_gl_group_id, name, gl_group_type_id )
					AS (
							SELECT
								gg1.id,
								gg1.parent_gl_group_id,
								gg1.name,
								gg1.gl_group_type_id
							FROM
								gl_groups gg1
							WHERE
								gg1.id	= ' . ( int ) $intId . '
								AND gg1.cid =' . ( int ) $intCid . '

							UNION ALL

							SELECT
								gg2.id,
								gg2.parent_gl_group_id,
								gg2.name,
								gg2.gl_group_type_id
							FROM
								gl_groups gg2,
								recursive_gl_groups rgg
							WHERE
								gg2.id = rgg.parent_gl_group_id
								AND gg2.cid =' . ( int ) $intCid . '
						)

					SELECT
						id,
						parent_gl_group_id,
						name,
						gl_group_type_id
					FROM
						recursive_gl_groups
					WHERE
						gl_group_type_id = ' . CGlGroupType::STANDARD_GROUP . '
						AND id != ' . ( int ) $intId . '
						LIMIT 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupByGlTreeIdBySystemCodeByCid( $intGlTreeId, $strSystemCode, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND system_code = \'' . $strSystemCode . '\'';

		return self::fetchGlGroup( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlGroupsByGlTreeIdByCid( $intGLTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGLTreeId . '
						AND gl_account_type_id IS NOT NULL
						AND disabled_on IS NULL
					ORDER BY
						name';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchParentGlGroupByGlTreeIdByGlGroupTypeIdByCid( $intGlTreeId, $intGlGroupTypeId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						glg.name
					FROM
						gl_groups glg
					WHERE
						glg.cid =' . ( int ) $intCid . '
						AND glg.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND glg.gl_group_type_id = ' . ( int ) $intGlGroupTypeId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsForMasterOrChildGlTreesOrByGlGroupTypeIdByCid( $intGlGroupTypeId, $intCid, $objClientDatabase, $boolIsMasterGlTree = true ) {

		$strCondition = NULL;

		if( true == is_numeric( $intGlGroupTypeId ) ) {
			$strCondition = ' AND gg.gl_group_type_id = ' . ( int ) $intGlGroupTypeId;
		}

		$strSql = 'SELECT
						gg.*
					FROM
						gl_groups gg
						JOIN gl_trees gt ON ( gg.cid = gt.cid AND gg.gl_tree_id = gt.id )
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gt.is_system = ' . ( int ) $boolIsMasterGlTree .
						$strCondition . '
						AND gg.disabled_on IS NULL
					ORDER BY
						gg.order_num';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsForActiveGlTreesByGlGroupTypeIdsByCid( $arrintGlGroupTypeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gg.*
					FROM
						gl_groups gg
						JOIN gl_trees gt ON ( gg.cid = gt.cid AND gg.gl_tree_id = gt.id )
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gg.gl_group_type_id IN ( ' . implode( ',', $arrintGlGroupTypeIds ) . ' )
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL
					ORDER BY
						gg.order_num';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchChildGlGroupsByGlGroupIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'WITH RECURSIVE
						recursive_gl_groups( id, cid, parent_gl_group_id )
					AS (
							SELECT
								id,
								cid,
								parent_gl_group_id
							FROM
								gl_groups
							WHERE
								cid = ' . ( int ) $intCid . '
								AND id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )

							UNION

							SELECT
								gg1.id,
								gg1.cid,
								gg1.parent_gl_group_id
							FROM
								gl_groups gg1,
								recursive_gl_groups rgg
							WHERE
								gg1.cid = rgg.cid
								AND gg1.parent_gl_group_id = rgg.id
								AND gg1.cid = ' . ( int ) $intCid . '
						)

					SELECT
						gg.*
					FROM
						recursive_gl_groups rgg1
						JOIN gl_groups gg ON ( rgg1.cid = gg.cid AND rgg1.id = gg.id )
					WHERE
						gg.cid = ' . ( int ) $intCid . '
					ORDER BY
						gg.parent_gl_group_id';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdByGlAccountTypeIdsByCid( $intGlTreeId, $intGlAccountTypeIds, $intCid, $objClientDatabase ) {
		$strSql = ' SELECT
						 *
					FROM
						gl_groups gg
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gg.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gg.gl_account_type_id IN ( ' . implode( ',', $intGlAccountTypeIds ) . ' )';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlAccountIdsByCid( $arrintGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						gg.*,
						gat.gl_account_id
					FROM
						gl_groups gg
						LEFT JOIN gl_trees gt ON ( gt.cid = gg.cid AND gt.id = gg.gl_tree_id )
						LEFT JOIN gl_account_trees gat ON ( gat.cid = gg.cid AND gat.gl_group_id = gg.id )
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gt.is_system = 1
						AND gat.gl_account_id IN ( ' . implode( ',', $arrintGlAccountIds ) . ' )';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdsBySystemCodesByGlAccountTypeIdByCid( $arrintGlTreeIds, $intGlAccountTypeId, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlTreeIds ) || false == valId( $intGlAccountTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT gg.*
					FROM
						gl_groups gg
					WHERE
						gg.cid = ' . ( int ) $intCid . '
						AND gg.gl_tree_id IN ( ' . implode( ',', $arrintGlTreeIds ) . ' )
						AND gg.gl_account_type_id = ' . ( int ) $intGlAccountTypeId . '
						AND gg.system_code IN ( \'' . CGlGroup::EQUITY_SYSTEM_CODE . '\', \'' . CGlGroup::ASSETS_SYSTEM_CODE . '\', \'' . CGlGroup::INCOME_SYSTEM_CODE . '\', \'' . CGlGroup::EXPENSES_SYSTEM_CODE . '\', \'' . CGlGroup::LIABILITIES_SYSTEM_CODE . '\')';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsData( $intGlTreeId, $intCid, $objClientDatabase ) {

		$strSql = '
			WITH RECURSIVE cte_gl_group_incst ( level, parent, id, name, sort, gl_account_type_id, cid, gl_account_types, gl_group_type_id ) AS
			(
				SELECT
					0,
					NULL::INTEGER,
					glg.id,
					glg.name,
					glg.order_num::text,
					glg.gl_account_type_id,
					glg.cid,
					glg.gl_account_type_id::text,
					glg.gl_group_type_id
				FROM
					gl_groups glg
				WHERE
					glg.cid = ' . ( int ) $intCid . '
					AND glg.parent_gl_group_id IS NULL
					AND glg.gl_tree_id = ' . ( int ) $intGlTreeId . '
					
				UNION
				
				SELECT
					LEVEL + 1,
					glg.parent_gl_group_id,
					glg.id,
					glg.name,
					CASE
						WHEN LEVEL < 1 THEN glg.order_num::text
						ELSE ( sort::text || \' | \' || glg.order_num )
					END::text AS order_num,
					glg.gl_account_type_id,
					glg.cid,
					COALESCE ( gl_account_types || \' | \' || glg.gl_account_type_id, glg.gl_account_type_id::text ),
					glg.gl_group_type_id
				FROM
					gl_groups glg
					JOIN gl_group_types ggt ON ( ggt.id = glg.gl_group_type_id )
					JOIN cte_gl_group_incst ON ( glg.cid = ' . ( int ) $intCid . ' AND glg.parent_gl_group_id = cte_gl_group_incst.id )
				WHERE
					glg.cid = ' . ( int ) $intCid . '
					AND glg.gl_tree_id = ' . ( int ) $intGlTreeId . '
				)

				SELECT
					repeat ( \'- \', LEVEL - 1) || NAME AS gl_group_name,
					id,
					CASE
						WHEN gl_account_type_id IN ( ' . CGlAccountType::ASSETS . ', ' . CGlAccountType::LIABILITIES . ', ' . CGlAccountType::EQUITY . ' )
						THEN 1 ELSE 2
					END AS rank
				FROM
					cte_gl_group_incst
				WHERE
					parent IS NOT NULL
					AND gl_group_type_id = ' . ( int ) CGlGroupType::STANDARD_GROUP . '
					AND gl_account_type_id IN ( ' . CGlAccountType::ASSETS . ', ' . CGlAccountType::LIABILITIES . ', ' . CGlAccountType::EQUITY . ' )
				ORDER BY
					rank,
					sort';

		return ( array ) fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdsByGlAccountTypeIdByCid( $arrintGlTreeIds, $intGlAccountTypeId, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlTreeIds ) || false == valId( $intGlAccountTypeId ) ) return NULL;

		$strSql = 'SELECT
						sub.id,
						sub.cid,
						sub.gl_account_type_id,
						sub.gl_tree_id
					FROM
						(
							SELECT
								id,
								cid,
								gl_account_type_id,
								gl_tree_id,
								ROW_NUMBER() OVER ( PARTITION BY gl_account_type_id, gl_tree_id ) AS row_id
						  FROM
								gl_groups
						  WHERE
								cid = ' . ( int ) $intCid . '
								AND gl_tree_id IN ( ' . implode( ',', $arrintGlTreeIds ) . ' )
								AND gl_account_type_id = ' . ( int ) $intGlAccountTypeId . '
						) AS sub
					WHERE
						sub.row_id < 2';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGroupsDataByCidByGlAccountTypeIds( $intCid, $intGlAccountTypeIds, $objClientDatabase ) {

		if( false == valId( $intCid ) && false == valArr( $intGlAccountTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT gg.*
					FROM gl_groups gg
					      JOIN gl_account_trees gat ON (gg.cid = gat.cid AND gat.gl_tree_id = gg.gl_tree_id AND gg.id = gat.gl_group_id)
					      JOIN gl_accounts ga ON (ga.id = gat.gl_account_id AND gat.cid = ga.cid)
					WHERE gg.cid = ' . ( int ) $intCid . ' AND
					      gg.is_system = 1 AND
					      gg.gl_account_type_id IN  ( ' . sqlIntImplode( $intGlAccountTypeIds ) . ' ) AND
					      gg.disabled_on IS NULL
				    GROUP BY
				          gg.id,
                          gg.cid';

		return self::fetchGlGroups( $strSql, $objClientDatabase );
	}

	public static function fetchCircularGlGroupsByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'WITH RECURSIVE child_groups ( id, cid, gl_tree_id, parent_gl_group_id, path, is_circular ) AS (
						SELECT
							id,
							cid,
							gl_tree_id,
							parent_gl_group_id,
							ARRAY [ id ],
							FALSE
						FROM
							gl_groups
						WHERE
							cid = ' . ( int ) $intCid . '
							AND gl_tree_id = ' . ( int ) $intGlTreeId . ' 
							AND parent_gl_group_id IS NOT NULL
					UNION ALL
						SELECT
							parent.id,
							parent.cid,
							parent.gl_tree_id,
							parent.parent_gl_group_id,
							PATH || parent.id,
							parent.id = ANY ( PATH )
						FROM
							gl_groups AS parent
							JOIN child_groups AS child ON parent.id = child.parent_gl_group_id AND parent.cid = child.cid
						WHERE
							parent.cid = ' . ( int ) $intCid . '
							AND parent.gl_tree_id = ' . ( int ) $intGlTreeId . ' 
							AND parent.parent_gl_group_id IS NOT NULL
							AND NOT is_circular )

						SELECT 1 from
							child_groups
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_circular IS TRUE
						LIMIT 1';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>