<?php
use Psi\Eos\Entrata\CReportFilters;
use Psi\Eos\Entrata\CReportNewInstances;
use Psi\Eos\Entrata\CReports;

class CReportSchedule extends CBaseReportSchedule {

	const REFTYPE_GROUP				= 'group';
	const REFTYPE_NEW_GROUP			= 'new_group';
	const REFTYPE_NEW_INSTANCE		= 'new_instance';
	const REFTYPE_FILTER			= 'filter';

	const DLOPT_IS_TEMPORARY		= 'is_temporary';
	const DLOPT_SELF_DESTRUCT		= 'self_destruct';
	const DLOPT_SEND_EMAIL			= 'send_email';
	const DLOPT_IS_INTERNAL			= 'is_internal';
	const DLOPT_IS_MIGRATED			= 'is_migrated';

	const DLOPT_ALL_FLAGS			= [ CReportSchedule::DLOPT_IS_TEMPORARY, CReportSchedule::DLOPT_SEND_EMAIL, CReportSchedule::DLOPT_SELF_DESTRUCT, CReportSchedule::DLOPT_IS_INTERNAL, CReportSchedule::DLOPT_IS_MIGRATED ];

	const STATUS_INCOMPLETE			= 'incomplete';
	const STATUS_COMPLETED			= 'completed';
	const STATUS_FAILED				= 'failed';

	const RUNKEY_FORMAT				= '\R\u\n_Y_m_d_H_i_s';
	const FAILED_RUN_LIFETIME		= 14;

	private $m_arrmixDownloadOptions;
	private $m_objReportGroup;
	private $m_objReportNewGroup;
	/** @var CReportNewInstance[] */
	private $m_arrobjReportInstances;
	private $m_strRunKey;

	// region Base Overrides

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['download_options'] ) && true == valArr( $arrmixValues['download_options'], 0 ) ) {
			// Download options are usually strings, but if we get an array, we force it to be a string for parent::setValues()
			$arrmixValues['download_options'] = json_encode( $arrmixValues['download_options'] );
		}

		if( true == isset( $arrmixValues['details'] ) && true == valArr( $arrmixValues['details'], 0 ) ) {
			$arrmixValues['details'] = json_encode( $arrmixValues['details'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['report_instances'] ) ) {
			$this->setReportInstances( array_map( function( $arrmixReportInstance ) {
				$objReportInstance = new CReportNewInstance();
				$objReportInstance->setValues( $arrmixReportInstance );
				return $objReportInstance;
			}, $arrmixValues['report_instances'] ) );
		} elseif( true == isset( $arrmixValues['report_filters'] ) ) {
			// Handle histories created before the shift to instances
			$this->setReportInstances( array_map( function( $arrmixReportFilter ) {
				$objReportFilter = new CReportFilter();
				$objReportFilter->setValues( $arrmixReportFilter );
				return $objReportFilter->toReportInstance();
			}, $arrmixValues['report_filters'] ) );
		}

		if( true == isset( $arrmixValues['filter_overrides'] ) ) $this->setFilterOverrides( $arrmixValues['filter_overrides'] );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// endregion

	// region Fetch Methods

	public function fetchReportObjects( CDatabase $objDatabase ) {
		if( true == valId( $this->getReportNewInstanceId() ) ) {
			return CReports::createService()->fetchReportsByReportNewInstanceIdByCid( $this->getReportNewInstanceId(), $this->getCid(), $objDatabase );
		} elseif( true == valId( $this->getReportNewGroupId() ) ) {
			return CReports::createService()->fetchReportsByReportNewGroupIdByCid( $this->getReportNewGroupId(), $this->getCid(), $objDatabase );
		} elseif( true == valId( $this->getReportGroupId() ) ) {
			// Fetch the reports that match the report group's instances
			return CReports::createService()->fetchReportsByReportGroupIdByCid( $this->getReportGroupId(), $this->getCid(), $objDatabase );
		} elseif( true == valId( $this->getReportVersionId() ) ) {
			// Fetch the report with the version set on the schedule
			$objReport = CReports::createService()->fetchReportByReportVersionIdByCid( $this->getReportVersionId(), $this->getCid(), $objDatabase );
			if( false == valObj( $objReport, 'CReport' ) ) {
				foreach( CSandboxReportLoader::getSandboxReports() as $objSandboxReport ) {
					if( $objSandboxReport->getReportVersionId() == $this->getReportVersionId() ) {
						$objReport = $objSandboxReport;
						break;
					}
				}
			}
			return [ $objReport->getReportVersionId() => $objReport ];
		} elseif( false == empty( $this->getReportInstances() ) ) {
			$arrobjReportVersions = [];
			foreach( $this->getReportInstances() as $objReportInstance ) {
				$objReport = CReports::createService()->fetchReportByReportVersionIdByCid( $objReportInstance->getReportVersionId(), $this->getCid(), $objDatabase );
				$arrobjReportVersions[$objReportInstance->getReportVersionId()] = $objReport;
			}
			return $arrobjReportVersions;
		}

		return NULL;
	}

	public function fetchReportInstances( $objDatabase ) {
		switch( $this->getReferenceType() ) {
			case self::REFTYPE_GROUP:
				$arrobjReportFilters = CReportFilters::createService()->fetchReportFiltersByReportGroupIdByCid( $this->getReportGroupId(), $this->getCid(), $objDatabase ) ?: [];
				$arrmixDownloadOptions = json_decode( json_encode( $this->getDownloadOptions() ), true );

				if( true == valArr( $arrobjReportFilters ) ) {
					foreach( $arrobjReportFilters as $objReportFilter ) {
						if( true == isset( $arrmixDownloadOptions[$objReportFilter->getId()] ) ) {
							$objReportFilter->setDownloadOptions( $arrmixDownloadOptions[$objReportFilter->getId()] );
						} else {
							// Assume we only have one set of download options that apply to all filters
							$objReportFilter->setDownloadOptions( $this->getDownloadOptions() );
						}
					}
				}

				return array_map( function( CReportFilter $objReportFilter ) {
					return $objReportFilter->toReportInstance();
				}, $arrobjReportFilters );

			case self::REFTYPE_NEW_GROUP:
				return CReportNewInstances::createService()->fetchReportInstancesForGroup( $this->getReportNewGroupId(), $this->getCid(), $objDatabase ) ?: [];

			case self::REFTYPE_NEW_INSTANCE:
				$objReportInstance = CReportNewInstances::createService()->fetchActiveReportNewInstanceByIdByCid( $this->getReportNewInstanceId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objReportInstance, 'CReportNewInstance' ) ) {
					return [ $objReportInstance->getId() => $objReportInstance ];
				} else {
					return [];
				}

			case self::REFTYPE_FILTER:
				$strSql = '
					SELECT
						rf.*,
						rv.id AS report_version_id,
						r.name AS report_name,
						r.report_type_id
					FROM
						report_schedules rs
						JOIN report_filters rf ON rf.cid = rs.cid AND rf.id = rs.report_filter_id
						JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id AND r.is_schedulable = TRUE AND COALESCE( r.is_published, TRUE ) = TRUE )
						JOIN report_versions rv ON ( rv.cid = rs.cid AND rv.id = rs.report_version_id AND COALESCE( rv.definition ->> \'is_published\', \'true\' ) = \'true\')
					WHERE
						rs.cid = ' . ( int ) $this->getCid() . '
						AND rs.id = ' . ( int ) $this->getId() . '
						AND COALESCE( rv.expiration, now() ) >= now()';

				$objReportFilter = CReportFilters::createService()->fetchReportFilter( $strSql, $objDatabase );

				if( true == valObj( $objReportFilter, 'CReportFilter' ) ) {
					$objReportFilter->setDownloadOptions( $this->m_arrmixDownloadOptions );
				} else {
					return [];
				}

				return array_map( function( CReportFilter $objReportFilter ) {
					return $objReportFilter->toReportInstance();
				}, [ $objReportFilter->getId() => $objReportFilter ] );

			default:
				return [];
		}
	}

	public function fetchRecipientInformation( $objDatabase ) {
		$strSql = '
			SELECT
				COALESCE ( cu.id, 0 ) AS company_user_id,
				COALESCE ( ce.name_first, rsr.details ->>\'name\' ) AS name_first,
				COALESCE ( ce.name_last, \'\' ) AS name_last,
				COALESCE ( ce.email_address, rsr.details ->>\'email\' ) AS email_address,
				/* This code is collecting property ids and group ids together, then removing duplicates and sorting the array
				The sorting is actually unnecessary, but done to demonstrate identical results to the previous query */
				array_to_string ( ARRAY ( SELECT unnest( array_agg ( prop_grp.property_group_id ) ) GROUP BY 1 ORDER BY 1 ), \',\' ) AS property_group_ids,
				jsonb_array_elements_text( COALESCE( rsr.details -> \'output_locale\', \'["' . CLocaleContainer::createService()->getDefaultLocaleCode() . '"]\'::jsonb ) ) AS output_locale
			FROM
				report_schedules rs
				JOIN report_schedule_recipients rsr ON rs.cid = rsr.cid AND rs.id = rsr.report_schedule_id
				LEFT JOIN company_user_groups cug ON rsr.cid = cug.cid AND rsr.report_schedule_recipient_type_id = ' . CReportScheduleRecipientType::COMPANY_GROUP . ' AND rsr.reference_id = cug.company_group_id
				LEFT JOIN company_users cu ON
					rsr.cid = cu.cid
					AND cu.deleted_on IS NULL
					AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					AND (
						( rsr.report_schedule_recipient_type_id = ' . CReportScheduleRecipientType::COMPANY_USER . ' AND rsr.reference_id = cu.id )
						OR cug.company_user_id = cu.id
					)
				LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
				LEFT JOIN company_user_property_groups cupg ON rs.cid = cupg.cid AND cu.id = cupg.company_user_id
				LEFT JOIN LATERAL ( 
									SELECT
										pg.cid,
										pg.id AS property_group_id
									FROM
										property_groups AS pg
									WHERE
										pg.cid = ' . ( int ) $this->getCid() . '
										AND pg.deleted_by IS NULL 
										AND pg.deleted_on IS NULL
										AND ( CASE
												WHEN cu.is_administrator = 1 THEN TRUE
												WHEN cu.id IS NULL AND rsr.report_schedule_recipient_type_id = ' . CReportScheduleRecipientType::EMAIL_ADDRESS . ' THEN TRUE
												ELSE FALSE
											END ) 
									UNION
									SELECT
										pg.cid,
										lp.property_id AS property_group_id
									FROM
										property_groups AS pg
										JOIN load_properties ( ARRAY [ pg.cid ], ARRAY [ pg.id ] ) AS lp ON lp.cid = pg.cid
									WHERE
										pg.cid = ' . ( int ) $this->getCid() . '
										AND pg.deleted_by IS NULL 
										AND pg.deleted_on IS NULL
										AND cupg.property_group_id = pg.id
								) AS prop_grp ON TRUE
			WHERE
				rs.cid = ' . ( int ) $this->getCid() . '
				AND rs.id = ' . ( int ) $this->getId() . '
				AND ( cu.is_disabled = 0 OR rsr.reference_id = 0 )
				AND rsr.report_schedule_recipient_type_id IN ( ' . CReportScheduleRecipientType::COMPANY_GROUP . ',' . CReportScheduleRecipientType::COMPANY_USER . ',' . CReportScheduleRecipientType::EMAIL_ADDRESS . ' )
			GROUP BY
				1, 2, 3, 4, output_locale';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrmixResult = [];
		$arrstrEmailAddresses = [];
		$intCounter = -1;
		foreach( $arrmixData as $arrmixRow ) {

			if( 0 == $arrmixRow['company_user_id'] && false == in_array( $arrmixRow['email_address'], $arrstrEmailAddresses, false ) ) {
				$arrmixRow['company_user_id'] = $intCounter;
				$intCounter--;
			} elseif( 0 == $arrmixRow['company_user_id'] ) {
				$arrmixRow['company_user_id'] = array_search( $arrmixRow['email_address'], $arrstrEmailAddresses, false );
			}

			$arrmixResult[( int ) $arrmixRow['company_user_id'] . '-' . $arrmixRow['output_locale']] = [
				'company_user_id'		=> $arrmixRow['company_user_id'],
				'name_first'			=> $arrmixRow['name_first'],
				'name_last'				=> $arrmixRow['name_last'],
				'email_address'			=> $arrmixRow['email_address'],
				'property_group_ids'	=> array_values( array_unique( array_map( 'intval', explode( ',', $arrmixRow['property_group_ids'] ) ) ) ),
				'output_locale'			=> $arrmixRow['output_locale']
			];
			$arrstrEmailAddresses[$arrmixRow['company_user_id']] = $arrmixRow['email_address'];
		}

		return $arrmixResult;
	}

	/**
	 * Returns the list of recipients grouped by their property group access lists
	 *
	 * @param $objDatabase
	 * @param null $strEntrataUserInformation
	 * @return array
	 */
	public function fetchRecipientsByUniquePropertyGroups( $objDatabase, $strEntrataUserInformation = NULL ) {
		$arrmixRecipientInfo = $this->fetchRecipientInformation( $objDatabase, $strEntrataUserInformation );

		$arrmixRecipientGroups = [];
		foreach( $arrmixRecipientInfo as $mixRecipientKey => $arrmixRecipient ) {
			$strPropertyGroupKey = implode( ',', $arrmixRecipient['property_group_ids'] );
			if( false == array_key_exists( $strPropertyGroupKey, $arrmixRecipientGroups ) ) {
				$arrmixRecipientGroups[$strPropertyGroupKey] = [];
			}
			$arrmixRecipientGroups[$strPropertyGroupKey][$mixRecipientKey] = $arrmixRecipient;
		}

		return $arrmixRecipientGroups;
	}

	public function fetchRecipientCache( CDatabase $objDatabase, CCompanyUser $objCompanyUser ) {
		if( false == valId( $this->getId() ) ) {
			return json_encode( [
				[
					'id' => NULL,
					'report_schedule_recipient_type_id' => CReportScheduleRecipientType::COMPANY_USER,
					'company_user_id' => $objCompanyUser->getId(),
					'name_first' => $objCompanyUser->getNameFirst(),
					'name_last' => $objCompanyUser->getNameLast(),
					'email_address' => $objCompanyUser->getEmailAddress()
				]
			] );
		}

		$strSql = '
			SELECT
				( \'[\' || array_to_string( array_agg( info.details ), \', \' ) || \']\' )::JSON recipients
			FROM (
				SELECT
					rsr.cid,
					rsr.report_schedule_id,
					( REGEXP_REPLACE( \'{"id":\' || rsr.id || \',"report_schedule_recipient_type_id":\' || rsr.report_schedule_recipient_type_id ||
					CASE
						WHEN rsr.report_schedule_recipient_type_id = 1
						THEN \',"id":\' || cg.id || \',"name":"\' || util_get_translated( \'name\', cg.name, cg.details ) || \'","company_users":[\' ||
							array_to_string( array_agg(
								   \'{"id":\' || cgu.id
								|| \',"name_first":"\' || COALESCE( cge.name_first, \'\' )
								|| \'","name_last":"\' || COALESCE( cge.name_last, \'\' )
								|| \'","email_address":"\' || COALESCE( cge.email_address, \'\' )
								|| \'"}\'
							), \', \' )
						|| \']\'

						WHEN rsr.report_schedule_recipient_type_id = 2
						THEN  \',"id":\' || cu.id
							|| \',"name_first":"\' || COALESCE( ce.name_first, \'\' )
							|| \'","name_last":"\' || COALESCE( ce.name_last, \'\' )
							|| \'","email_address":"\' || COALESCE( ce.email_address, \'\' )
							|| \'"\'

						WHEN rsr.report_schedule_recipient_type_id = 4
						THEN  \',"id":\' || apc.id
							|| \',"name_first":"\' || COALESCE( apc.name_first, \'\' )
							|| \'","name_last":"\' || COALESCE( apc.name_last, \'\' )
							|| \'","email_address":"\' || COALESCE( apc.email_address, \'\' )
							|| \'"\'

					END
					|| \'}\', E\'[\r\n\t]\', \' \', \'g\') )::JSON AS details
				FROM
					report_schedule_recipients rsr

					-- Recipient Type 1: Company Group
					LEFT JOIN company_groups cg ON rsr.report_schedule_recipient_type_id = 1 AND cg.cid = rsr.cid AND cg.id = rsr.reference_id
					LEFT JOIN company_user_groups cug ON cug.cid = cg.cid AND cug.company_group_id = cg.id
					LEFT JOIN company_users cgu ON cgu.cid = cug.cid AND cgu.id = cug.company_user_id
					LEFT JOIN company_employees cge ON cge.cid = cgu.cid AND cge.id = cgu.company_employee_id

					-- Recipient Type 2: Company User
					LEFT JOIN company_users cu ON rsr.report_schedule_recipient_type_id = 2 AND cu.cid = rsr.cid AND cu.id = rsr.reference_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id

					-- Recipient Type 3: Legal Entity Owner
					-- LEFT JOIN legal_entity_owners -- This table not implemented yet?

					-- Recipient Type 4: AP Payee Contact
					LEFT JOIN ap_payee_contacts apc ON rsr.report_schedule_recipient_type_id = 4 AND apc.cid = rsr.cid AND apc.id = rsr.reference_id
				WHERE
					rsr.cid = ' . ( int ) $this->getCid() . '
					AND rsr.report_schedule_id = ' . ( int ) $this->getId() . '
				GROUP BY
					rsr.id,
					rsr.cid,
					cg.id,
					cg.cid,
					cu.id,
					cu.cid,
					ce.id,
					ce.cid,
					apc.id,
					apc.cid
				ORDER BY
					rsr.id
			) info
			WHERE
				info.cid = ' . ( int ) $this->getCid();

		$arrmixData = fetchData( $strSql, $objDatabase );
		return $arrmixData[0]['recipients'];
	}

	// endregion

	// region RabbitMQ Serialization

	public function toArrayWithInstances() {
		$arrmixResult = parent::toArray();
		if( true == valArr( $this->getReportInstances() ) ) {
			$arrmixResult['report_instances'] = array_map( function ( CReportNewInstance $objReportInstance ) {
				return $objReportInstance->toArrayWithDependencies();
			}, $this->getReportInstances() );
		}
		return $arrmixResult;
	}

	// endregion

	// region Progress Tracking

	public function generateRunKey() {
		$strRunKey = date( self::RUNKEY_FORMAT );
		$this->setRunKey( $strRunKey );
		return $strRunKey;
	}

	public function beginReportScheduleRun( $arrstrCorrelationsIds ) {
		// Add structure for { runs: { <time>: { <correlationId1>: { status: incomplete }, <correlationId2>: { status: incomplete }, ... } }
		foreach( $arrstrCorrelationsIds as $strCorrelationId ) {
			$this->setDetailsField( [ 'runs', $this->getRunKey(), $strCorrelationId ], ( object ) [ 'status' => self::STATUS_INCOMPLETE ] );
		}
	}

	public function finishMessage( CDatabase $objDatabase, $strCorrelationId, $strStatus, $strMessage = NULL ) {
		if( true == is_null( $this->getRunKey() ) ) return;
		if( false == valId( $this->getId() ) ) return;

		// Begin transaction
		$objDatabase->begin();

		// Reselect schedule for update
		$objLockedSchedule = \Psi\Eos\Entrata\CReportSchedules::createService()->fetchReportScheduleForUpdate( $this->getCid(), $this->getId(), $objDatabase );

		// Add status, message to { runs: { <time>: { <correlationId>: { status: <status>, message: <message> } } } }
		$objLockedSchedule->setDetailsField( [ 'runs', $this->getRunKey(), $strCorrelationId, 'status' ], $strStatus );
		if( false == is_null( $strMessage ) ) {
			$objLockedSchedule->setDetailsField( [ 'runs', $this->getRunKey(), $strCorrelationId, 'message' ], $strMessage );
		}

		if( false == $objLockedSchedule->update( $objLockedSchedule->getCompanyUserId(), $objDatabase ) ) {
			error_log( json_encode( $objLockedSchedule->getErrorMsgs() ) );
		}

		// Commit transaction
		$objDatabase->commit();

		$this->setDetails( $objLockedSchedule->getDetails() );
	}

	public function getRunStatus( $strRunKey = NULL ) {
		// Make sure we have a run key
		$strRunKey = $strRunKey ?? $this->getRunKey();
		if( true == is_null( $strRunKey ) ) return NULL;

		// Make sure we have data for the run key
		$objRun = $this->getDetailsField( [ 'runs', $strRunKey ] );
		if( true == is_null( $objRun ) ) return NULL;

		$arrstrRun = json_decode( json_encode( $objRun ), true );
		$boolCompleted = true;
		$boolAllFailed = true;
		foreach( $arrstrRun as $strCorrelationId => $arrstrMessage ) {
			switch( $arrstrMessage['status'] ) {
				case self::STATUS_INCOMPLETE:
					$boolCompleted = false;
					break;

				case self::STATUS_COMPLETED:
					$boolAllFailed = false;
					break;

				default:
					// Do nothing
					break;
			}
		}

		if( false == $boolCompleted ) {
			return self::STATUS_INCOMPLETE;
		} elseif( true == $boolAllFailed ) {
			return self::STATUS_FAILED;
		} else {
			return self::STATUS_COMPLETED;
		}
	}

	public function cleanUpRuns() {
		// Go through runs, and remove:
		$arrmixRuns = ( array ) json_decode( json_encode( $this->getDetailsField( [ 'runs' ] ) ), true ) ?: [];
		foreach( $arrmixRuns as $strRunKey => $arrmixRun ) {
			switch( $this->getRunStatus( $strRunKey ) ) {
				case self::STATUS_INCOMPLETE:
				case self::STATUS_FAILED:
					// Runs where there are only complete and incomplete after: 2 weeks (indicates a SIGKILL event)
					// Runs where there are errors after: 2 weeks
					$objDate = DateTime::createFromFormat( self::RUNKEY_FORMAT, $strRunKey );
					$objNow = new DateTime();
					$objDiff = $objDate->diff( $objNow );
					if( $objDiff->days > self::FAILED_RUN_LIFETIME ) {
						unset( $arrmixRuns[$strRunKey] );
					}
					break;

				default:
				case self::STATUS_COMPLETED:
					// Runs where all messages are complete
					unset( $arrmixRuns[$strRunKey] );
					break;
			}
		}
		$this->setDetailsField( [ 'runs' ], ( object ) $arrmixRuns );
	}

	// endregion

	// region Validation

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportScheduleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceIds() {
		$intRefCount = ( is_null( $this->getReportGroupId() ) ? 0 : 1 );
		$intRefCount += ( is_null( $this->getReportNewGroupId() ) ? 0 : 1 );
		$intRefCount += ( is_null( $this->getReportNewInstanceId() ) ? 0 : 1 );
		$intRefCount += ( is_null( $this->getReportFilterId() ) ? 0 : 1 );
		$boolIsValid = $intRefCount == 1;

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Exactly one of report_group_id, report_new_group_id, report_new_instance_id, and report_filter_id must be non-null' ) );
		}

		return $boolIsValid;
	}

	public function valReportFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDownloadOptions() {
		$boolIsValid = true;

		if( false == valObj( $this->getDownloadOptions(), 'stdClass' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'download_options', 'Download options is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReferenceIds();
				$boolIsValid &= $this->valDownloadOptions();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// endregion

	// region Getters

	public function getDownloadOption( $strKey, $mixDefaultValue = NULL ) {
		if( false == isset( $this->m_arrmixDownloadOptions ) ) {
			$this->m_arrmixDownloadOptions = json_decode( json_encode( $this->getDownloadOptions() ) ?: '{}', true );
		}
		return isset( $this->m_arrmixDownloadOptions[$strKey] ) ? $this->m_arrmixDownloadOptions[$strKey] : $mixDefaultValue;
	}

	public function getIsTemporary() {
		return $this->getDownloadOption( self::DLOPT_IS_TEMPORARY, true );
	}

	public function getSelfDestruct() {
		return $this->getDownloadOption( self::DLOPT_SELF_DESTRUCT, false );
	}

	public function getSendEmail() {
		return $this->getDownloadOption( self::DLOPT_SEND_EMAIL, false );
	}

	public function getOrFetchReportGroup( CDatabase $objDatabase ) {
		if( !is_null( $this->getReportGroupId() && valObj( $this->m_objReportGroup, CReportGroup::class ) ) ) {
			$this->m_objReportGroup = \Psi\Eos\Entrata\CReportGroups::createService()->fetchReportGroupByIdByCid( $this->getReportGroupId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objReportGroup;
	}

	public function getOrFetchReportNewGroup( CDatabase $objDatabase ) {
		if( !is_null( $this->getReportNewGroupId() && valObj( $this->m_objReportNewGroup, CReportNewGroup::class ) ) ) {
			$this->m_objReportNewGroup = \Psi\Eos\Entrata\CReportNewGroups::createService()->fetchReportNewGroupByIdByCid( $this->getReportNewGroupId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objReportNewGroup;
	}

	public function getOrFetchReportInstances( $objDatabase = NULL ) {
		if( true == is_null( $this->getReportInstances() ) && false == is_null( $this->getId() ) ) {
			$this->setReportInstances( $this->fetchReportInstances( $objDatabase ) );
		}

		foreach( $this->getReportInstances() as $intReportFilterId => $objReportInstance ) {
			$arrmixDownloadOptions = json_decode( json_encode( $this->getDownloadOptions() ), true ) ?: [];
			foreach( $arrmixDownloadOptions as $strOutputType => $arrmixDownloadOption ) {
				if( true == isset( $arrmixDownloadOptions[$strOutputType]['overrides'][$objReportInstance->getReportVersionId() . '-' . $objReportInstance->getId()] ) ) {
					$arrmixDownloadOptions[$strOutputType] = array_merge( $arrmixDownloadOptions[$strOutputType], $arrmixDownloadOptions[$strOutputType]['overrides'][$objReportInstance->getReportVersionId() . '-' . $objReportInstance->getId()] );
				}
			}
			$objReportInstance->setDownloadOptions( $arrmixDownloadOptions );
		}

		return $this->getReportInstances();
	}

	public function getReportInstances() {
		return $this->m_arrobjReportInstances;
	}

	public function getRunKey() {
		return $this->m_strRunKey;
	}

	public function getFilterOverrides() {
		return json_decode( json_encode( $this->getDetailsField( [ 'filter_overrides' ] ) ), true );
	}

	public function getIsActive() {
		return false != is_null( $this->m_strDeletedOn );
	}

	public function getIsScheduleForReportPacket() {
		return false == is_null( $this->getReportGroupId() ) || false == is_null( $this->getReportNewGroupId() );
	}

	public function getReferenceType() {
		if( false == is_null( $this->getReportGroupId() ) ) {
			return self::REFTYPE_GROUP;
		} elseif( false == is_null( $this->getReportNewGroupId() ) ) {
			return self::REFTYPE_NEW_GROUP;
		} elseif( false == is_null( $this->getReportNewInstanceId() ) ) {
			return self::REFTYPE_NEW_INSTANCE;
		} elseif( false == is_null( $this->getReportFilterId() ) ) {
			return self::REFTYPE_FILTER;
		} else {
			return NULL;
		}
	}

	// endregion

	// region Setters

	public function setReportScheduleTypeId( $intReportScheduleTypeId ) {
		parent::setReportScheduleTypeId( $intReportScheduleTypeId );
		return $this;
	}

	public function setCreatedBy( $intCreatedBy ) {
		parent::setCreatedBy( $intCreatedBy );
		return $this;
	}

	public function setDownloadOptions( $jsonDownloadOptions ) {
		parent::setDownloadOptions( $jsonDownloadOptions );
		$this->m_arrmixDownloadOptions = json_decode( json_encode( $this->getDownloadOptions() ), true );
		return $this;
	}

	public function setDownloadOption( $strKey, $mixValue ) {
		if( false == isset( $this->m_arrmixDownloadOptions ) ) {
			$this->m_arrmixDownloadOptions = json_decode( json_encode( $this->getDownloadOptions() ) ?: '{}', true );
		}
		$this->m_arrmixDownloadOptions[$strKey] = $mixValue;
		$this->setDownloadOptions( json_encode( $this->m_arrmixDownloadOptions ) );
		return $this;
	}

	public function setIsTemporary( $boolIsTemporary ) {
		return $this->setDownloadOption( self::DLOPT_IS_TEMPORARY, $boolIsTemporary );
	}

	public function setSelfDestruct( $boolSelfDestruct ) {
		return $this->setDownloadOption( self::DLOPT_SELF_DESTRUCT, $boolSelfDestruct );
	}

	public function setSendEmail( $boolSendEmail ) {
		return $this->setDownloadOption( self::DLOPT_SEND_EMAIL, $boolSendEmail );
	}

	/**
	 * @param CReportNewInstance[] $arrobjReportInstances
	 * @return $this
	 */
	public function setReportInstances( array $arrobjReportInstances = [] ) {
		$this->m_arrobjReportInstances = $arrobjReportInstances;
		return $this;
	}

	public function setRunKey( $strRunKey ) {
		$this->m_strRunKey = $strRunKey;
		return $this;
	}

	public function setFilterOverrides( $arrmixFilterOverrides ) {
		$this->setDetailsField( 'filter_overrides', $arrmixFilterOverrides );
		return $this;
	}

	public function clearFilterOverrides() {
		$jsonDetails = $this->getDetails();
		unset( $jsonDetails->filter_overrides );
		$this->setDetails( $jsonDetails );
		return $this;
	}

	// endregion

}