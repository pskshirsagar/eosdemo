<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssets
 * Do not add any new functions to this class.
 */

class CCustomerAssets extends CBaseCustomerAssets {

	public static function fetchCustomerAssetsByStatusByLeaseIdByCid( $intStatus, $intLeaseId, $intCid, $objDatabase ) {

		$strWhereCondition = '';

		if( $intStatus == CCustomerAsset::CURRENT || $intStatus == '' ) {

			$strWhereCondition = 'AND (
									( DATE( to_char( ca.asset_effective_date, \'YYYY-MM-DD\' ) ) <= NOW()
										AND ( DATE( to_char( ca.date_divested, \'YYYY-MM-DD\' ) ) >= NOW() + INTERVAL \'-2 years\' OR DATE( to_char( ca.date_divested, \'YYYY-MM-DD\' ) ) IS NULL )
									)
									OR ( DATE( to_char( ca.asset_effective_date, \'YYYY-MM-DD\' ) ) IS NULL )
									)
									AND CASE WHEN ca.date_ended IS NOT NULL THEN
											DATE (to_char(ca.date_ended, \'YYYY-MM-DD\')) >= NOW()
										ELSE
											TRUE
										END';
		}

		if( $intStatus == CCustomerAsset::PAST ) {
			$strWhereCondition = 'AND CASE WHEN ca.date_ended IS NOT NULL 
										THEN
											DATE (to_char(ca.date_ended, \'YYYY-MM-DD\')) < NOW()
										ELSE
											( DATE( to_char( ca.date_divested, \'YYYY-MM-DD\' ) ) < NOW() + INTERVAL \'-2 years\' )
										END';
		}

		if( $intStatus == CCustomerAsset::FUTURE ) {
			$strWhereCondition = 'AND ( DATE( to_char( ca.asset_effective_date, \'YYYY-MM-DD\' ) ) > NOW()  )
								 AND CASE WHEN ca.date_ended IS NOT NULL THEN
										DATE (to_char(ca.date_ended, \'YYYY-MM-DD\')) > NOW()
									ELSE
										TRUE
									END';
		}

		$strSql = ' SELECT
						ca.*
					FROM
						customer_assets ca
						JOIN lease_customers lc ON ( ca.cid = lc.cid AND ca.customer_id = lc.customer_id )
					WHERE
						ca.cid =' . ( int ) $intCid . '
						AND lc.lease_id =' . ( int ) $intLeaseId . '
						' . $strWhereCondition . '
						AND ca.deleted_by IS NULL
						AND ca.deleted_on IS NULL
				    ORDER BY
				        ca.asset_effective_date';

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerIdByCustomerAssetTypeIdsByCid( $intCustomerId, $arrintCustomerAssetTypeIds, $intCid, $objDatabase, $boolIncludeDeletedCA = false, $boolIsShowCategorizedInfo = false ) {

		if( false == valArr( $arrintCustomerAssetTypeIds ) ) return NULL;

		$strCheckDeletedCASql = ( false == $boolIncludeDeletedCA ) ? ' AND ca.deleted_on IS NULL' : '';

		$strCaseStatement = '';
		$strOrderBy = ' ORDER BY ca.asset_effective_date';
		if( true == $boolIsShowCategorizedInfo ) {
			$strOrderBy = ' ORDER BY subq.asset_status, subq.asset_effective_date';
			$strCaseStatement = ' ,CASE
							        WHEN ( ca.date_ended IS NOT NULL AND ca.date_ended < CURRENT_DATE ) OR ( ca.date_divested < ( CURRENT_DATE - INTERVAL \'2 years\' )  ) THEN \'' . __( 'Past' ) . '\'
							        WHEN ( ca.asset_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
							        ELSE \'' . __( 'Current' ) . '\'
                                END as asset_status';
		}

		$strSql = 'SELECT
						* ' . $strCaseStatement . '
					FROM
						customer_assets ca
					WHERE
						ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.customer_asset_type_id IN ( ' . implode( ',', $arrintCustomerAssetTypeIds ) . ' ) ' . $strCheckDeletedCASql;

		$strSql = ( true == $boolIsShowCategorizedInfo ) ? 'SELECT * FROM ( ' . $strSql . ' ) as subq ' . $strOrderBy : $strSql . $strOrderBy;

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetByCustomerIdByCustomerAssetTypeIdByCid( $intCustomerId, $intCustomerAssetTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						customer_assets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND customer_asset_type_id = ' . ( int ) $intCustomerAssetTypeId . ' LIMIT 1';

		return self::fetchCustomerAsset( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL AND ca.deleted_on IS NULL ' : '';

		$strSql = 'SELECT
						ca.*
					FROM
						customer_assets as ca
						JOIN applicants a ON ( a.cid = ca.cid AND a.customer_id = ca.customer_id )
						JOIN applicant_applications as aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
					AND aa.cid = ' . ( int ) $intCid . '
					AND aa.application_id = ' . ( int ) $intApplicationId . $strCheckDeletedAASql . '
					ORDER BY ca.customer_id';

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerIdWithoutNullMarketValueByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedCA = false ) {

		$strCheckDeletedCASql = ( false == $boolIncludeDeletedCA ) ? ' AND ca.deleted_on IS NULL' : '';
		$strSql = 'SELECT
						*
					FROM
						customer_assets ca
					WHERE
						ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.market_value IS NOT NULL ' . $strCheckDeletedCASql;

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerIdWithoutZeroMarketValueByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedCA = false ) {

		$strCheckDeletedCASql = ( false == $boolIncludeDeletedCA ) ? ' AND ca.deleted_on IS NULL' : '';
		$strSql = 'SELECT
						*,
				        CASE
					        WHEN ( ca.date_ended IS NOT NULL AND ca.date_ended < CURRENT_DATE ) OR ( ca.date_divested < ( CURRENT_DATE - INTERVAL \'2 years\' )  ) THEN \'' . __( 'Past' ) . '\'
					        WHEN ( ca.asset_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
					        ELSE \'' . __( 'Current' ) . '\'
                        END as status
					FROM
						customer_assets ca
					WHERE
						ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.market_value > 0 ' . $strCheckDeletedCASql;

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerIdWithNullMarketValueByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedCA = false ) {

		$strCheckDeletedCASql = ( false == $boolIncludeDeletedCA ) ? ' AND ca.deleted_on IS NULL' : '';
		$strSql = 'SELECT
						*
					FROM
						customer_assets ca
					WHERE
						ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.cid = ' . ( int ) $intCid . $strCheckDeletedCASql;

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCurrentCustomerAssetsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, array $arrmixFamilyMembers = [], $strDesiredSubsidyCertificationEffectiveDate = 'NOW()', $strPossibleSubsidyCertificationEffectiveThroughDate = NULL ) {

		$strDesiredSubsidyCertificationEffectiveDate = ( false == valStr( $strDesiredSubsidyCertificationEffectiveDate ) ) ? 'NOW()' : $strDesiredSubsidyCertificationEffectiveDate;
		$strPossibleSubsidyCertificationEffectiveThroughDate = ( true == valStr( $strPossibleSubsidyCertificationEffectiveThroughDate ) ) ? '\'' . $strPossibleSubsidyCertificationEffectiveThroughDate . '\'::date' : '\'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date + INTERVAL \'1 YEAR -1 DAY\'';

		$strWhereCondition = '  AND (
										(
											DATE( to_char( ca.asset_effective_date, \'YYYY-MM-DD\' ) ) <= ' . $strPossibleSubsidyCertificationEffectiveThroughDate . '
											AND
											(
												DATE( to_char( ca.date_divested, \'YYYY-MM-DD\' ) ) + INTERVAL \'+2 years\' >= \'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date
												OR
												DATE( to_char( ca.date_divested, \'YYYY-MM-DD\' ) ) IS NULL
											)
										)
										OR
										(
											DATE( to_char( ca.asset_effective_date, \'YYYY-MM-DD\' ) ) IS NULL
										)
									)
								AND (
										CASE WHEN ca.date_ended IS NOT NULL THEN
											DATE ( to_char( ca.date_ended, \'YYYY-MM-DD\') ) >= \'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date
										ELSE
											TRUE
										END
									)';

		$strSql = 'SELECT
						ca.id,
						ca.customer_id,
						ca.customer_asset_type_id,
						ca.market_value,
						ca.disposal_amount,
						ca.cost_to_dispose,
						ca.debt_amount,
						ca.cash_value,
						ca.interest_rate,
						ca.actual_income,
						ca.asset_effective_date,
						ca.date_divested,
						cat.name AS description,
						( CASE WHEN ca.date_divested IS NULL THEN \'C\'
							ELSE \'I\'
						END ) AS asset_status
					FROM
						customer_assets ca
						JOIN customer_asset_types cat ON ( ca.customer_asset_type_id = cat.id )
						JOIN customers c ON ( ca.customer_id = c.id AND ca.cid = c.cid )
						JOIN applicants a ON ( c.id = a.customer_id AND c.cid = a.cid )
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE
						aa.application_id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND aa.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND ca.deleted_by IS NULL
						AND ca.deleted_on IS NULL
					ORDER BY ca.id DESC';

		$arrmixCurrentCustomerAssets = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixFamilyMembers ) ) {

			$arrmixFamilyMembers = rekeyArray( 'customer_id', $arrmixFamilyMembers, NULL, false );

			foreach( $arrmixCurrentCustomerAssets as $intKey => $arrmixCurrentCustomerAsset ) {
				$arrmixCurrentCustomerAssets[$intKey]['member_number'] = $arrmixFamilyMembers[$arrmixCurrentCustomerAsset['customer_id']]['member_number'];
			}
		}

		return $arrmixCurrentCustomerAssets;

	}

	public static function fetchCustomerAssetsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_assets WHERE cid = ' . ( int ) $intCid . ' AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND deleted_by IS NULL AND deleted_on IS NULL';

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchPublishedCustomerAssetsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
            * 
            FROM 
              customer_assets 
            WHERE 
              deleted_by IS NULL 
              AND deleted_on IS NULL
              AND customer_id = ' . ( int ) $intCustomerId . ' 
              AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchCustomerDetailsCustomerAssetsByIdByLeaseIdByCid( $intAssetId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = ' SELECT 
						c.id AS customer_id,
						c.name_first,
						c.name_last
					FROM
						customer_assets ca
						JOIN customers c ON (ca.cid = c.cid AND ca.customer_id = c.id)
						JOIN lease_customers lc ON ( ca.cid = lc.cid AND ca.customer_id = lc.customer_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND lc.lease_id = ' . ( int ) $intLeaseId . '
						' . ( true == valId( $intAssetId ) ? ' AND ca.id = ' . ( int ) $intAssetId : '' ) . '
					GROUP BY
						c.id,
						c.name_first,
						c.name_last';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerAssetsWithStatusByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*,
				        CASE
					        WHEN ( ca.date_ended IS NOT NULL AND ca.date_ended < CURRENT_DATE ) OR ( ca.date_divested < ( CURRENT_DATE - INTERVAL \'2 years\' )  ) THEN \'' . __( 'Past' ) . '\'
					        WHEN ( ca.asset_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
					        ELSE \'' . __( 'Current' ) . '\'
                        END as status
					FROM
						customer_assets ca
					WHERE
						ca.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.cid = ' . ( int ) $intCid . ' 
						AND deleted_by IS NULL 
						AND deleted_on IS NULL';

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

	public static function fetchActiveCustomerAssetsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
							ca.*
						FROM
							customer_assets ca
							JOIN customers c ON ( ca.cid = c.cid AND ca.customer_id = c.id )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND ca.deleted_by IS NULL
							AND ca.deleted_on IS NULL
						ORDER BY
							id ASC;';

		return self::fetchCustomerAssets( $strSql, $objDatabase );
	}

}
?>
