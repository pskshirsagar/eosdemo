<?php

class CApplicationStatus extends CBaseApplicationStatus {
	const STARTED 				= 1;
	const PARTIALLY_COMPLETED 	= 2;
	const COMPLETED				= 3;
	const APPROVED 				= 4;
	const ON_HOLD 				= 5;
	const CANCELLED 			= 6;

	public static $c_arrstrApplicationStatus = [
		self::STARTED 				=> 'Started',
	  	self::PARTIALLY_COMPLETED 	=> 'Partially Completed',
		self::COMPLETED 			=> 'Completed',
	  	self::APPROVED 				=> 'Approved',
	  	self::ON_HOLD 				=> 'Archived',
	  	self::CANCELLED 			=> 'Cancelled'
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getApplicationStatuses() {
		$arrstrApplicationStatuses = [
			self::STARTED 				=> __( 'Started' ),
			self::PARTIALLY_COMPLETED 	=> __( 'Partially Completed' ),
			self::COMPLETED 			=> __( 'Completed' ),
			self::APPROVED 				=> __( 'Approved' ),
			self::ON_HOLD 				=> __( 'Archived' ),
			self::CANCELLED 			=> __( 'Cancelled' )
		];

		return $arrstrApplicationStatuses;
	}

	public function getSubsidizedApplicationStatuses() {
		$arrstrSubsidizedApplicationStatuses = [
			CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED => __( 'Complete' ), CApplicationStatus::CANCELLED => __( 'Cancelled' ), CApplicationStatus::ON_HOLD => __( 'Archived' ) ],
			CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED => __( 'Started' ), CApplicationStatus::COMPLETED => __( 'Complete' ), CApplicationStatus::CANCELLED => __( 'Cancelled' ) ],
			CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED => __( 'Started' ), CApplicationStatus::PARTIALLY_COMPLETED => __( 'Partially Complete' ), CApplicationStatus::COMPLETED => __( 'Complete' ), CApplicationStatus::APPROVED => __( 'Processed' ), CApplicationStatus::CANCELLED => __( 'Cancelled' ) ],
			CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED => __( 'Processed' ), CApplicationStatus::PARTIALLY_COMPLETED => __( 'Partially Complete' ), CApplicationStatus::COMPLETED => __( 'Processed' ), CApplicationStatus::APPROVED => __( 'Finalized' ), CApplicationStatus::CANCELLED => __( 'Cancelled' ) ]
		];

		return $arrstrSubsidizedApplicationStatuses;
	}

	public static $c_arrintActiveApplicationStatusIds 				= [ self::STARTED, self::PARTIALLY_COMPLETED, self::COMPLETED, self::APPROVED ];
	public static $c_arrintInactiveApplicationStatusIds 			= [ self::ON_HOLD, self::CANCELLED ];
	public static $c_arrintTransferQuotesApplicationStatusTypeIds	= [ self::STARTED, self::PARTIALLY_COMPLETED ];
	public static $c_arrintMoveinPendingApplicationStatusTypeIds	= [ self::APPROVED, self::COMPLETED ];
	public static $c_arrintRenewalPendingApplicationStatusIds		= [ self::STARTED, self::PARTIALLY_COMPLETED, self::COMPLETED ];

	public function applicationStatusIdToStr( $intApplicationStatusId, $intLeaseIntervalTypeId = NULL, $intApplicationStageId = NULL, $intOccupancyTypeId = COccupancyType::CONVENTIONAL ) {

		switch( true ) {
			case CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId && CApplicationStage::PRE_APPLICATION == $intApplicationStageId:
				return $this->renewalStatusIdToStr( $intApplicationStatusId );

			default:
				switch( $intOccupancyTypeId ) {
					case COccupancyType::AFFORDABLE:
						return $this->getSubsidizedApplicationStatuses()[$intApplicationStageId][$intApplicationStatusId];
						break;

					default:

						return $this->getApplicationStatuses()[$intApplicationStatusId];
						break;
				}
		}
	}

	public function renewalStatusIdToStr( $intApplicationStatusId ) {
		switch( $intApplicationStatusId ) {
			case self::COMPLETED:
				return __( 'Generated' );

			case self::CANCELLED:
				return __( 'Rejected' );

			default:
				return self::createService()->getApplicationStatuses()[$intApplicationStatusId];
		}
	}

}
?>
