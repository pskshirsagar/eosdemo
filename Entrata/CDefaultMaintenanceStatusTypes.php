<?php

class CDefaultMaintenanceStatusTypes extends CBaseDefaultMaintenanceStatusTypes {

	public static function fetchDefaultMaintenanceStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultMaintenanceStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultMaintenanceStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultMaintenanceStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>