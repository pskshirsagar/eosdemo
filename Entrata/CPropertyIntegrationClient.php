<?php

class CPropertyIntegrationClient extends CBasePropertyIntegrationClient {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntegrationClientId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$strPropertyIntegrationClientNewKeyValuePair = '';
    	$arrstrPropertyIntegrationClient = $this->objectToArrayMapping();
    	$arrstrSubmittedKeys 	= array_keys( $arrstrPropertyIntegrationClient );
    	if( true == valArr( $arrstrSubmittedKeys ) ) {
    		foreach( $arrstrSubmittedKeys as $strValue ) {
    			$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrPropertyIntegrationClient ) ) ? $arrstrPropertyIntegrationClient[$strValue] : '';
    			$strPropertyIntegrationClientNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
    		}
    	}

    	if( false === parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
    		return false;

    	$objTableLog = new CTableLog();
    	$objTableLog->insert( $intCurrentUserId, $objDatabase, 'property_integration_clients', $this->getCid(), 'INSERT', $this->getCid(), NULL, $strPropertyIntegrationClientNewKeyValuePair );

    	return true;
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objCurrentPropertyIntegrationClient = NULL ) {

    	if( false === parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
    		return false;

    	if( true == valObj( $objCurrentPropertyIntegrationClient, 'CPropertyIntegrationClient' ) ) {
    		// new values
    		$arrstrRequestPropertyIntegrationClient = $this->objectToArrayMapping();
    		// old values
    		$arrstrCurrentPropertyIntegrationClient = $objCurrentPropertyIntegrationClient->objectToArrayMapping();

    		$arrstrDifferenceInArrays = array_diff_assoc( $arrstrRequestPropertyIntegrationClient, $arrstrCurrentPropertyIntegrationClient );

    		if( true == valArr( $arrstrDifferenceInArrays ) ) {

    			$arrstrExistingKeys  	= array_keys( $arrstrCurrentPropertyIntegrationClient );
    			$arrstrSubmittedKeys 	= array_keys( $arrstrRequestPropertyIntegrationClient );

    			$arrstrUnionPropertyPropertyIntegrationClientKeys = array_unique( array_merge( $arrstrExistingKeys, $arrstrSubmittedKeys ) );
    			$strPropertyIntegrationClientNewKeyValuePair = '';
    			$strPropertyIntegrationClientOldKeyValuePair = '';

    			// Code added for formation of Property Integration Client key/value pair string -- starts
    			if( true == valArr( $arrstrUnionPropertyIntegrationClientKeys ) ) {
    				foreach( $arrstrUnionPropertyIntegrationClientKeys as $strValue ) {
    					$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrRequestPropertyIntegrationClient ) ) ? $arrstrRequestPropertyIntegrationClient[$strValue] : '';

    					$strPropertyIntegrationClientNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
    					$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentPropertyIntegrationClient ) ) ? $arrstrCurrentPropertyIntegrationClient[$strValue] : '';

    					$strPropertyIntegrationClientOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
    				}
    			}
    			// Code added for formation of Property Integration Client key/value pair string -- ends

    			$objTableLog = new CTableLog();
    			$objTableLog->insert( $intCurrentUserId, $objDatabase, 'property_integration_clients', $this->getCid(), 'UPDATE', $this->getCid(), $strPropertyIntegrationClientOldKeyValuePair, $strPropertyIntegrationClientNewKeyValuePair, 'An integration client has been modified.' );
    			return true;
    		}
    	}
    	return true;
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( false === parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
    		return false;

    	$arrstrCurrentPropertyIntegrationClient = $this->objectToArrayMapping();
    	$arrstrExistingKeys  	= array_keys( $arrstrCurrentPropertyIntegrationClient );
    	$strPropertyIntegrationClientOldKeyValuePair = '';

    	// Code added for formation of Property Integration Client key/value pair string -- starts
    	if( true == valArr( $arrstrExistingKeys ) ) {
    		foreach( $arrstrExistingKeys as $strValue ) {
    			$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentPropertyIntegrationClient ) ) ? $arrstrCurrentPropertyIntegrationClient[$strValue] : '';
    			$strPropertyIntegrationClientOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
    		}
    	}
    	// Code added for formation of Property Integration Client key/value pair string -- ends

    	$objTableLog = new CTableLog();
    	$objTableLog->insert( $intCurrentUserId, $objDatabase, 'property_integration_clients', $this->getCid(), 'DELETE', $this->getCid(), $strPropertyIntegrationClientOldKeyValuePair, NULL );

    	return true;
    }

    public function objectToArrayMapping() {

    	$arrstrPropertyIntegrationClient = [];
    	$arrstrPropertyIntegrationClient['id'] 					= $this->getId();
    	$arrstrPropertyIntegrationClient['cid'] 					= $this->getCid();
    	$arrstrPropertyIntegrationClient['integration_client_id'] 	= $this->getIntegrationClientId();
    	$arrstrPropertyIntegrationClient['property_id']			= $this->getPropertyId();
    	return $arrstrPropertyIntegrationClient;
    }

}
?>