<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotifications
 * Do not add any new functions to this class.
 */

class CNotifications extends CBaseNotifications {

	public static function fetchSentNotificationsByCompanyUserIdByCid( $objCompanyUser, $intCid, $objDatabase ) {
		$strAdminUserCondition = '';

		if( false == $objCompanyUser->getIsAdministrator() ) {
			$strAdminUserCondition = ' AND n.created_by =	' . $objCompanyUser->getId();
		}

		$strSql = 'SELECT
						n.*
					FROM
						notifications n
					WHERE
						n.cid = ' . ( int ) $intCid . $strAdminUserCondition . '
						AND n.start_date::date <= NOW()::date
					ORDER BY n.id DESC';

		return parent::fetchNotifications( $strSql, $objDatabase );
	}

	public static function fetchActiveNotificationsByCidByCompanyUser( $intCid, $objCompanyUser, $objDatabase, $strMessageType, $arrmixFilterData = NULL, $boolIsCountOnly = false ) {

		$intOffset = ( 0 < $arrmixFilterData['page_no'] ) ? $arrmixFilterData['page_size'] * ( $arrmixFilterData['page_no'] - 1 ) : 0;
		$intLimit = ( int ) $arrmixFilterData['page_size'];

		$strCompanyUserIdCondition = '';

		if( false == $objCompanyUser->getIsAdministrator() ) {
			$strCompanyUserIdCondition = ' AND n.created_by = ' . $objCompanyUser->getId();
		}

		$strMessageDateCondition = 'n.start_date::date <= NOW()::date';
		if( 'scheduled' == $strMessageType ) {
			$strMessageDateCondition = 'n.start_date::date > NOW()::date';
		}

		$strGroupAndOrderByCondition = ' GROUP BY n.id, cu.id, n.cid, ce.name_first, ce.name_last, n.start_date,cu.company_user_type_id ORDER BY n.id ' . $arrmixFilterData['sort_criteria'];
		$strSelectCondition = 'n.*,( CASE WHEN cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' THEN ( ce.name_first || \' \' || ce.name_last ) ELSE \'System\' END) AS company_user_name';
		$strPaginationCondition = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		if( true == $boolIsCountOnly ) {
			$strPaginationCondition = '';
			$strSelectCondition = 'count(n.id)';
			$strGroupAndOrderByCondition = '';
		}

		$strSql = 'SELECT
					' . $strSelectCondition . '
					FROM
						notifications n
						JOIN company_users cu ON ( n.cid = cu.cid AND n.created_by = cu.id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						n.cid = ' . ( int ) $intCid . '
						' . $strCompanyUserIdCondition . '
						AND ' . $strMessageDateCondition . $strGroupAndOrderByCondition . '
					' . $strPaginationCondition;

		return ( true == $boolIsCountOnly ) ? fetchData( $strSql, $objDatabase )[0]['count'] : parent::fetchNotifications( $strSql, $objDatabase );
	}

	public static function fetchNotificationDetailsByIdByCid( $intNotificationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						n.*,
						(
					        SELECT
								count(nl.id)
					        FROM
								notification_logs nl
					        WHERE
								nl.cid = ' . ( int ) $intCid . ' AND nl.notification_id = ' . ( int ) $intNotificationId . ' AND nl.viewed_on IS NOT NULL
				        ) AS read_count,
						CASE WHEN n.end_date >= CURRENT_DATE THEN
				    	(
					         SELECT
					         	count(*)
					         FROM
					         (
			         			((
									( SELECT
											cu.id
									  FROM
											notification_properties np
										    JOIN load_properties ( ARRAY [' . ( int ) $intCid . '], ARRAY [ np.property_group_id ], NULL, TRUE ) lp ON ( lp.cid = np.cid )
										    JOIN view_company_user_properties cup ON ( cup.cid = np.cid AND cup.property_id = lp.property_id )
										    JOIN company_users cu ON ( cu.cid = np.cid AND cu.id = cup.company_user_id )
									  WHERE
											cu.cid = ' . ( int ) $intCid . '
											AND np.notification_id = ' . ( int ) $intNotificationId . '
											AND cu.is_administrator<>1
                  							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
                  							AND cu.is_disabled <> 1
									)
									UNION
                   					(
										SELECT id FROM  company_users WHERE cid = ' . ( int ) $intCid . ' AND is_administrator=1 AND company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND is_disabled <> 1
									)
					            )
						        INTERSECT
					         	(
					                SELECT
					                	cug.company_user_id
					                FROM
										company_user_groups cug
					                	JOIN notification_company_groups ncg ON ( ncg.cid = cug.cid AND ( ncg.company_group_id IS NULL OR ncg.company_group_id = cug.company_group_id ) )
					             	WHERE
										ncg.cid = ' . ( int ) $intCid . ' AND ncg.notification_id = ' . ( int ) $intNotificationId . '
					            )
							)
							UNION( SELECT company_user_id from notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id =  ' . ( int ) $intNotificationId . ' )
							) as inner_view
						)
						ELSE
					    (
					      SELECT
					          count ( * )
					      FROM
							( SELECT company_user_id FROM notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id =  ' . ( int ) $intNotificationId . ') as inner_view
					   ) END AS total_post_count
					FROM
						notifications n
					WHERE
						n.id = ' . ( int ) $intNotificationId . '
						AND n.cid = ' . ( int ) $intCid;

		return self::fetchNotification( $strSql, $objDatabase );
	}

	public static function fetchNotificationPostedCompanyUsersByNotificationByCid( $objNotification, $intCid, $objDatabase ) {

		if( strtotime( $objNotification->getStartDate() ) <= strtotime( date( 'm/d/Y' ) ) ) {

			$strUserIdSql = 'SELECT company_user_id FROM notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id = ' . $objNotification->getId();
		} else {

			$strUserIdSql = 'SELECT
							 		*
								FROM
							 	(
									(
								 		(
											( SELECT
													cu.id
												FROM
													notification_properties np
													JOIN load_properties ( ARRAY [' . ( int ) $intCid . '], ARRAY [ np.property_group_id ], NULL, TRUE ) lp ON ( lp.cid = np.cid )
													JOIN view_company_user_properties cup ON ( cup.cid = np.cid AND cup.property_id = lp.property_id )
													JOIN company_users cu ON ( cu.cid = np.cid AND cu.id = cup.company_user_id )
												WHERE
													cu.cid = ' . ( int ) $intCid . '
													AND np.notification_id = ' . ( int ) $objNotification->getId() . '
													AND cu.is_administrator<>1
													AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
													AND cu.is_disabled <> 1
											)
											UNION
												(
												SELECT id FROM	company_users WHERE cid = ' . ( int ) $intCid . ' AND is_administrator=1 AND company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND is_disabled <> 1
											)
											)
										INTERSECT
										(
											SELECT
												cug.company_user_id
											FROM
												company_user_groups cug
												JOIN notification_company_groups ncg ON ( ncg.cid = cug.cid AND ( ncg.company_group_id IS NULL OR ncg.company_group_id = cug.company_group_id ) )
										 	WHERE
												ncg.cid = ' . ( int ) $intCid . ' AND ncg.notification_id = ' . ( int ) $objNotification->getId() . '
										)
									)
									UNION
									( SELECT company_user_id from notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id =	' . ( int ) $objNotification->getId() . ' )
								) inner_view';
		}

		$strSql = 'SELECT
						cu.id,
						ce.name_first,
						ce.name_last,
						nl.viewed_on
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						LEFT JOIN notification_logs nl ON ( nl.cid = cu.cid AND nl.company_user_id = cu.id AND nl.notification_id = ' . ( int ) $objNotification->getId() . ' )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.id IN (
							' . $strUserIdSql . '
						)
					ORDER BY name_first';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNotificationNotViewedCompanyUsersByNotificationByCid( $objNotification, $intCid, $objDatabase ) {

		if( strtotime( $objNotification->getEndDate() ) < strtotime( date( 'm/d/Y' ) ) ) {

			$strUserIdSql = 'SELECT company_user_id FROM notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id = ' . $objNotification->getId() . ' AND viewed_on IS NULL';
		} else {

			$strUserIdSql = 'SELECT
							 		*
								FROM
							 	(
									(
										(
									 		(
												( SELECT
													cu.id
													FROM
														notification_properties np
														JOIN load_properties ( ARRAY [' . ( int ) $intCid . '], ARRAY [ np.property_group_id ], NULL, TRUE ) lp ON ( lp.cid = np.cid )
														JOIN view_company_user_properties cup ON ( cup.cid = np.cid AND cup.property_id = lp.property_id )
														JOIN company_users cu ON ( cu.cid = np.cid AND cu.id = cup.company_user_id )
													WHERE
														cu.cid = ' . ( int ) $intCid . '
														AND np.notification_id = ' . ( int ) $objNotification->getId() . '
														AND cu.is_administrator<>1
															AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
															AND cu.is_disabled <> 1
												)
												UNION
													(
													SELECT id FROM	company_users WHERE cid = ' . ( int ) $intCid . ' AND is_administrator=1 AND company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND is_disabled <> 1
												)
												)
											INTERSECT
											(
												SELECT
													cug.company_user_id
												FROM
													company_user_groups cug
													JOIN notification_company_groups ncg ON ( ncg.cid = cug.cid AND ( ncg.company_group_id IS NULL OR ncg.company_group_id = cug.company_group_id ) )
											 	WHERE
													ncg.cid = ' . ( int ) $intCid . ' AND ncg.notification_id = ' . ( int ) $objNotification->getId() . '
											)
										)

										UNION

										( SELECT company_user_id from notification_logs WHERE cid = ' . ( int ) $intCid . ' AND notification_id =	' . ( int ) $objNotification->getId() . ' )
									)
									EXCEPT
									(
										SELECT
											company_user_id
										FROM
											notification_logs
										WHERE
											cid = ' . ( int ) $intCid . '
											AND notification_id = ' . ( int ) $objNotification->getId() . '
											AND viewed_on IS NOT NULL
									)
								) inner_view';
		}

		$strSql = 'SELECT
						cu.id,
						ce.name_first,
						ce.name_last,
						NULL as viewd_on
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.id IN (
								' . $strUserIdSql . '
								 )
					ORDER BY name_first';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnloggedNotificationDetailsByCompanyUserByCid( $objCompanyUser, $intCid, $objDatabase ) {

		$strJoinCondition = '';
		$strUserCondition = '';

		$arrintNotifications = [];

		if( true == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {

			$strJoinCondition = ' JOIN notification_company_groups ncg ON ( n.cid = ncg.cid AND n.id = ncg.notification_id )
								 JOIN company_user_groups cug ON ( ( n.cid = cug.cid ) AND ( ncg.company_group_id IS NULL OR cug.company_group_id = ncg.company_group_id ) )';

			$strUserCondition = ' AND cug.company_user_id = ' . ( int ) $objCompanyUser->getId();

		} elseif( false == $objCompanyUser->getIsPSIUser() ) {

			$strJoinCondition = ' JOIN notification_company_groups ncg ON ( n.cid = ncg.cid AND n.id = ncg.notification_id )
								 JOIN company_user_groups cug ON ( ( n.cid = cug.cid ) AND ( ncg.company_group_id IS NULL OR cug.company_group_id = ncg.company_group_id ) )
								 JOIN notification_properties np ON ( n.cid = np.cid AND np.notification_id = n.id)
								 JOIN load_properties ( ARRAY [' . ( int ) $intCid . '], ARRAY [ np.property_group_id ], NULL, TRUE ) lp ON ( lp.cid = np.cid )
								 JOIN view_company_user_properties cup ON ( n.cid = cup.cid AND cup.property_id = lp.property_id )';

			$strUserCondition = ' AND cug.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
								 AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId();
		}

		$strSql = '(
					SELECT
						DISTINCT n.id
					FROM
						notifications n
						' . $strJoinCondition . '
						JOIN notification_modules nm ON ( n.cid = nm.cid AND n.id = nm.notification_id )
						LEFT JOIN notification_logs nl ON ( n.cid = nl.cid AND n.id = nl.notification_id AND nl.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' )
					WHERE
						n.cid = ' . ( int ) $intCid . '
						' . $strUserCondition . '
						AND ( CURRENT_DATE BETWEEN n.start_date AND n.end_date AND ( nl.id IS NULL OR nl.viewed_on IS NULL ) )
					)
					EXCEPT
					(
						SELECT
							notification_id
						FROM
							notification_logs nl
						WHERE
							nl.cid = ' . ( int ) $intCid . '
							AND nl.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
					)';

		$arrmixNotificationDetails = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixNotificationDetails ) ) {
			foreach( $arrmixNotificationDetails as $arrmixNotificationDetail ) {
				$arrintNotifications[] = $arrmixNotificationDetail['id'];
			}
		}

		return $arrintNotifications;
	}

	public static function fetchNonSyncedNotificationMessages( $objDatabase ) {
		$strSql = 'SELECT 
						n.id,
						n.cid,
						n.title,
						n.description,
						date_trunc(\'day\', n.end_date ) + interval \'1 day\' - interval \'1 second\' as end_date,
						array_to_string(array_agg(nl.company_user_id), \',\') as company_user_ids
					FROM 
						notifications n
						JOIN notification_logs nl ON( n.cid = nl.cid AND n.id = nl.notification_id )
					WHERE 
						n.start_date::DATE = ' . date( '\'Y-m-d\'', strtotime( '+ 1 day' ) ) . '::DATE
					GROUP BY
						n.id,
						n.cid';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchNotificationDetailByIdByCid( $intNotificationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT n.*,
						(CASE
							WHEN cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' THEN (ce.name_first || \' \' || ce.name_last)
							ELSE \'System\'
						END) AS company_user_name
					FROM 
						notifications n
						JOIN company_users cu on (n.cid = cu.cid AND n.created_by = cu.id)
						LEFT JOIN company_employees ce ON (cu.cid = ce.cid AND ce.id = cu.company_employee_id)
					WHERE n.id = ' . ( int ) $intNotificationId . '
						AND n.cid = ' . ( int ) $intCid;

		return self::fetchNotification( $strSql, $objDatabase );
	}

}
?>