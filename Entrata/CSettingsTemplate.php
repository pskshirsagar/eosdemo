<?php

use Psi\Eos\Entrata\CSettingsTemplateProperties;

class CSettingsTemplate extends CBaseSettingsTemplate {

	protected $m_arrintPropertyIds;
	protected $m_arrintPropertySettingGroupIds;
	protected $m_arrobjSettingsTemplatesOccupancyTypeIds;

	protected $m_arrobjPropertySettingLogs;
	protected $m_arrobjCustomPropertySettings;
	protected $m_arrobjDeletePropertySettings;
	protected $m_arrobjInsertPropertySettings;

	protected $m_intPropertyCount;
	protected $m_intUniqueSettingsCount;
	protected $m_boolIsSettingTemplateDisabled;

	protected $m_strUpdatePropertySettingsSql;
	protected $m_strFullDescription;

	/**
	 * Create functions
	 */

	public function createSettingsTemplateProperty( $intPropertyId = NULL ) {
		$objSettingsTemplateProperty = new CSettingsTemplateProperty();
		$objSettingsTemplateProperty->setCid( $this->getCid() );
		$objSettingsTemplateProperty->setPropertyId( $intPropertyId );
		$objSettingsTemplateProperty->setSettingsTemplateId( $this->getId() );
		$objSettingsTemplateProperty->setSettingTemplateTypeId( $this->getSettingsTemplateTypeId() );

		return $objSettingsTemplateProperty;
	}

	public function createPropertySetting( $strKey, $strValue, $boolIsNotification = false ) {
		$objPropertySetting = ( true == $boolIsNotification ) ? new CPropertyNotification() : new CPropertyPreference();
		$objPropertySetting->setCid( $this->getCid() );
		$objPropertySetting->setPropertyId( $this->getPropertyId() );
		$objPropertySetting->setKey( $strKey );
		$objPropertySetting->setValue( $strValue );

		return $objPropertySetting;
	}

	public function createPropertySettingLog( $intPropertySettingKeyId, $strAction, $strNewValue, $strNewText ) {
		$objPropertySettingLog = new CPropertySettingLog();
		$objPropertySettingLog->setCid( $this->m_intCid );
		$objPropertySettingLog->setPropertyId( $this->getPropertyId() );
		$objPropertySettingLog->setPropertySettingKeyId( $intPropertySettingKeyId );
		$objPropertySettingLog->setAction( $strAction );
		$objPropertySettingLog->setNewValue( $strNewValue );
		$objPropertySettingLog->setNewText( $strNewText );

		return $objPropertySettingLog;
	}

	/**
	 * Get functions
	 */

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getPropertySettingGroupIds() {
		return $this->m_arrintPropertySettingGroupIds;
	}

	public function getPropertySettingLogs() {
		return $this->m_arrobjPropertySettingLogs;
	}

	public function getUniqueSettingsCount() {
		return $this->m_intUniqueSettingsCount;
	}

	public function getIsSettingTemplateDisabled() {
		return $this->m_boolIsSettingTemplateDisabled;
	}

	public function getFullDescription() {
		return $this->m_strFullDescription;
	}

	/**
	* Set functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_count'] ) ) {
			$this->setPropertyCount( $arrmixValues['property_count'] );
		}
		if( true == isset( $arrmixValues['unique_settings_count'] ) ) {
			$this->setUniqueSettingsCount( $arrmixValues['unique_settings_count'] );
		}
		if( true == isset( $arrmixValues['occupancy_type_ids'] ) ) {
			$this->setOccupancyTypeIds( $arrmixValues['occupancy_type_ids'] );
		}

		if( true == isset( $arrmixValues['is_disabled'] ) ) {
			$this->setIsSettingTemplateDisabled( $arrmixValues['is_disabled'] );
		}

		if( true == isset( $arrmixValues['full_description'] ) ) {
			$this->setFullDescription( $arrmixValues['full_description'] );
		}
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setPropertySettingLogs( $arrobjPropertySettingLogs ) {
		$this->m_arrobjPropertySettingLogs = $arrobjPropertySettingLogs;
	}

	public function setPropertySettingGroupIds( $arrintPropertySettingGroupIds ) {
		$this->m_arrintPropertySettingGroupIds = $arrintPropertySettingGroupIds;
	}

	public function setUniqueSettingsCount( $intUniqueSettingsCount ) {
		$this->m_intUniqueSettingsCount = $intUniqueSettingsCount;
	}

	public function setTemplateName( $strTemplateName, $strLocaleCode = NULL ) {
		$this->m_strTemplateName = getSanitizedFormField( CStrings::strTrimDef( $strTemplateName, 50, NULL, true ) );
	}

	public function setIsSettingTemplateDisabled( $boolIsSettingTemplateDisabled ) {
		$this->m_boolIsSettingTemplateDisabled = $boolIsSettingTemplateDisabled;
	}

	public function setFullDescription( $strFullDescription ) {
		$this->m_strFullDescription = $strFullDescription;
	}

	/**
	* Validate functions
	*/

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'cid', 'Client id is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 0 >= ( int ) $this->m_intPropertyId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'property_id', 'Property id is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valSettingsTemplateTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intSettingsTemplateTypeId ) || 0 >= ( int ) $this->m_intSettingsTemplateTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'setting_template_type_id', 'Settings profile type id is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valTemplateName() {
		$boolIsValid = true;

		$strTemplateName = preg_replace( '/[^a-zA-Z0-9\s]/', '', trim( $this->m_strTemplateName ) );
		if( false == valStr( $strTemplateName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'template_name', 'Valid profile name is required.', NULL ) );
		} elseif( 3 > strlen( trim( $strTemplateName ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'template_name', 'A profile name must contain at least 3 characters.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valOccupancyTypes() {
		$boolIsValid = true;

		if( false == valArr( $this->m_arrobjSettingsTemplatesOccupancyTypeIds ) && false == $this->getIsDefault() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'occupancy_types', ' Valid occupancy type is required. ', NULL ) );
		}
		return $boolIsValid;
	}

	public function valConflictingSettingTemplate( $objDatabase, $boolUpdate = false ) {

		$boolIsValid = true;

		if( true == valStr( $this->m_strTemplateName ) ) {
			$arrintSettingsTemplateCount = reset( \Psi\Eos\Entrata\CSettingsTemplates::createService()->fetchConflictingSettingsTemplateCountByIdByNameByTemplateTypeIdByCid( $this->m_intId, $this->m_strTemplateName, $this->m_intSettingsTemplateTypeId, $this->m_intCid, $objDatabase ) );

			if( ( 0 < $arrintSettingsTemplateCount['count'] && false == $boolUpdate ) || ( 1 == $arrintSettingsTemplateCount['count'] && true == $boolUpdate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'template_name', 'Profile name already exists.', NULL ) );
			} else {
				$strSql = 'SELECT id FROM properties WHERE lower( property_name ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->m_strTemplateName ) ) ) . '\' AND cid = ' . ( int ) $this->m_intCid . ' AND is_disabled = 0';
				$arrintPropertyIds = fetchData( $strSql, $objDatabase );

				if( true == valArr( $arrintPropertyIds ) && ( true == $boolUpdate && ( 1 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) || false == $boolUpdate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, 'template_name', 'Profile name matches with existing property.', NULL ) );
				}
			}
		}
 		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSettingsTemplateTypeId();
				$boolIsValid &= $this->valTemplateName();
				$boolIsValid &= $this->valOccupancyTypes();
				if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$boolIsValid &= $this->valConflictingSettingTemplate( $objClientDatabase );
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSettingsTemplateTypeId();
				$boolIsValid &= $this->valTemplateName();
				$boolIsValid &= $this->valOccupancyTypes();
				if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$boolIsValid &= $this->valConflictingSettingTemplate( $objClientDatabase, true );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	/**
	* Other functions
	*/

	public function updateSettingsTemplatePropertySettings( $arrstrPropertySettings = [], $arrstrPropertySettingLogs = [], $intCurrentUserId, $objDatabase, $boolIsNotification = false ) {
		$this->m_arrobjInsertPropertySettings 		= [];
		$this->m_arrobjDeletePropertySettings 		= [];
		$this->m_strUpdatePropertySettingsSql		= '';
		$this->m_arrobjPropertySettingLogs			= [];

		$arrobjPropertySettingKeys				= ( array ) \Psi\Eos\Entrata\CPropertySettingKeys::createService()->fetchPropertySettingKeysByPropertySettingGroupIds( $this->m_arrintPropertySettingGroupIds, $objDatabase );
		$this->loadUpdatePropertySettingsAndLogs( $arrstrPropertySettings, $arrobjPropertySettingKeys, $arrstrPropertySettingLogs, $intCurrentUserId, $objDatabase, $boolIsNotification );

		$boolIsValid = true;

		switch( NULL ) {
			default:

				if( false == $boolIsNotification ) {
					$strClassName = 'CPropertyPreferences';
					$strDeleteErrorMessage = 'Settings profile Property preferences failed to delete';
					$strInsertErrorMessage = 'Settings profile Property preferences failed to insert';
					$strUpdateErrorMessage = 'Settings profile Property preferences failed to update.';
				} else {
					$strClassName = 'CPropertyNotifications';
					$strDeleteErrorMessage = 'Settings profile Property notifications failed to delete';
					$strInsertErrorMessage = 'Settings profile Property notifications failed to insert';
					$strUpdateErrorMessage = 'Settings profile Property notifications failed to update.';
				}

				if( true == valArr( $this->m_arrobjDeletePropertySettings ) && false == $strClassName::bulkDelete( $this->m_arrobjDeletePropertySettings, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strDeleteErrorMessage ) );
					break;
				}

				if( true == valArr( $this->m_arrobjInsertPropertySettings ) && false == $strClassName::bulkInsert( $this->m_arrobjInsertPropertySettings, $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strInsertErrorMessage ) );
					break;
				}

				if( true == valStr( $this->m_strUpdatePropertySettingsSql ) && false === fetchData( $this->m_strUpdatePropertySettingsSql, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strUpdateErrorMessage ) );
					break;
				}

				// Create log entry
				if( true == valArr( $this->m_arrobjPropertySettingLogs ) ) {
					$this->setPropertySettingLogs( $this->m_arrobjPropertySettingLogs );
				}
		}

		return $boolIsValid;
	}

	public function loadUpdatePropertySettingsAndLogs( $arrstrPropertySettings, $arrobjPropertySettingKeys, $arrstrPropertySettingLogs, $intCurrentUserId, $objDatabase, $boolIsNotification ) {

		$arrobjOldPropertySettings		= rekeyObjects( 'key', ( array ) $this->fetchPropertySettings( $objDatabase, $boolIsNotification ) );

		foreach( $arrstrPropertySettings as $strKey => $arrstrPropertySetting ) {
			$strNewText = ( true == valArr( $arrstrPropertySettingLogs ) && true == array_key_exists( $strKey, $arrstrPropertySettingLogs ) ) ? $arrstrPropertySettingLogs[$strKey]['value'] : '';
			// Invalid Key
			if( false == array_key_exists( $strKey, $arrobjPropertySettingKeys ) ) {
				continue;
			}

			// Delete Preference
			if( $arrobjPropertySettingKeys[$strKey]->getDeleteValue() == $arrstrPropertySetting['value'] ) {

				if( true == array_key_exists( $strKey, $arrobjOldPropertySettings ) ) {
					$this->m_arrobjDeletePropertySettings[] = $arrobjOldPropertySettings[$strKey];
					$this->m_arrobjPropertySettingLogs[]	= $this->createPropertySettingLog( $arrobjPropertySettingKeys[$strKey]->getId(), 'DELETE', $arrstrPropertySetting['value'], $strNewText );
				}

				continue;
			}

			// Add Preference
			if( false == array_key_exists( $strKey, $arrobjOldPropertySettings ) ) {
				$objPropertySetting = $this->createPropertySetting( $strKey, $arrstrPropertySetting['value'], $boolIsNotification );
				$objPropertySetting->setPropertySettingKeyId( $arrobjPropertySettingKeys[$strKey]->getId() );
				$this->m_arrobjInsertPropertySettings[$strKey]  = $objPropertySetting;
				$this->m_arrobjPropertySettingLogs[]			= $this->createPropertySettingLog( $arrobjPropertySettingKeys[$strKey]->getId(), 'INSERT', $arrstrPropertySetting['value'], $strNewText );
				continue;
			}

			// Update Preference
			if( $arrobjOldPropertySettings[$strKey]->getValue() != $arrstrPropertySetting['value'] ) {
				$arrobjOldPropertySettings[$strKey]->setValue( $arrstrPropertySetting['value'] );
				$this->m_arrobjPropertySettingLogs[]	= $this->createPropertySettingLog( $arrobjPropertySettingKeys[$strKey]->getId(), 'UPDATE', $arrstrPropertySetting['value'], $strNewText );

				$this->m_strUpdatePropertySettingsSql .= $arrobjOldPropertySettings[$strKey]->update( $intCurrentUserId, $objDatabase, true );
			}
		}
	}

	public function associateOrDisassociateSettingsTemplateProperties( $intCurrentUserId, $objDatabase, $boolIsChooseSettingsTemplate ) {

		$arrintDeletePropertyIds = [];
		$arrobjInsertSettingsTemplateProperties = [];

		$arrintSettingsTemplatePropertyIds 	= $this->getPropertyIds();
		$arrintExistingPropertyIds 			= ( array ) CSettingsTemplateProperties::createService()->fetchSimpleSettingsTemplatePropertiesBySettingsTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrintSettingsTemplatePropertyIds ) ) {
			if( false == valArr( $arrintExistingPropertyIds ) ) {
				return true;
			}

			$arrintDeletePropertyIds = $arrintExistingPropertyIds;
		} else {
			if( false == $boolIsChooseSettingsTemplate ) {
				foreach( $arrintExistingPropertyIds as $intPropertyId ) {
					if( false == in_array( $intPropertyId, $arrintSettingsTemplatePropertyIds ) ) {
						$arrintDeletePropertyIds[] = $intPropertyId;
					}
				}
			}

			foreach( $arrintSettingsTemplatePropertyIds as $intPropertyId ) {
				if( false == in_array( $intPropertyId, $arrintExistingPropertyIds ) ) {
					$arrobjInsertSettingsTemplateProperties[] 	= $this->createSettingsTemplateProperty( $intPropertyId );
					$arrintDeletePropertyIds[] 					= $intPropertyId;
				}
			}
		}

		$arrobjDeleteSettingsTemplateProperties = CSettingsTemplateProperties::createService()->fetchSettingsTemplatePropertiesBySettingTemplateTypeIdByPropertyIdsByCId( $this->getSettingsTemplateTypeId(), $arrintDeletePropertyIds, $this->getCid(), $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				// Delete properties association with other templates / current template
				if( true == valArr( $arrobjDeleteSettingsTemplateProperties ) && false == CSettingsTemplateProperties::createService()->bulkDelete( $arrobjDeleteSettingsTemplateProperties, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to disassociate existing / old settings profile properties.' ) );
				}

				foreach( $arrobjInsertSettingsTemplateProperties as $objSettingsTemplateProperty ) {
					if( false == $objSettingsTemplateProperty->validate( VALIDATE_INSERT, $objDatabase ) ) {
						$boolIsValid = false;
						$this->addErrorMsgs( $objSettingsTemplateProperty->getErrorMsgs() );
					}
				}

				if( false == $boolIsValid ) {
					break;
				}

				if( true == valArr( $arrobjInsertSettingsTemplateProperties ) && false == CSettingsTemplateProperties::createService()->bulkInsert( $arrobjInsertSettingsTemplateProperties, $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to associate new settings profile properties.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function updatePropertySettings( $arrstrPreferenceKeys, $arrstrNotificationKeys, $intCurrentUserId, $objDatabase ) {
		if( false == valArr( $arrstrPreferenceKeys ) && false == valArr( $arrstrNotificationKeys ) ) {
			return NULL;
		}

		$boolIsValid = true;

		if( true == valArr( $arrstrPreferenceKeys ) ) {
			$boolIsValid &= $this->modifyPropertySettings( 'property_preferences', $arrstrPreferenceKeys, $intCurrentUserId, $objDatabase );
		}

		if( true == $boolIsValid && true == valArr( $arrstrNotificationKeys ) ) {
			$boolIsValid &= $this->modifyPropertySettings( 'property_notifications', $arrstrNotificationKeys, $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function modifyPropertySettings( $strTableName, $arrstrKeys, $intCurrentUserId, $objDatabase ) {
		if( true == is_null( $strTableName ) ) {
			return false;
		}

		// Delete the property preferences
		$strDeletePropertyPreferencesSql = 'DELETE FROM
												' . $strTableName . '
											WHERE
												cid = ' . ( int ) $this->getCid() . '
												AND property_id IS NOT NULL
												AND key IN( \'' . implode( '\', \'', $arrstrKeys ) . '\' )
												AND key NOT IN( SELECT key FROM ' . $strTableName . ' WHERE property_id = ' . $this->getPropertyId() . ' AND cid = ' . $this->getCid() . ')
												AND property_id IN( SELECT property_id FROM settings_template_properties WHERE settings_template_id = ' . $this->getId() . ' AND cid = ' . $this->getCid() . ');';

		if( false === fetchData( $strDeletePropertyPreferencesSql, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete ' . $strTableName . ' .' ) );
			return false;
		}

		// Update the property preferences
		$strUpdatePropertyPreferencesSql = 'UPDATE
												' . $strTableName . ' pp
											SET
												value = stpp.value
											FROM
												settings_template_properties stp
											    JOIN settings_templates st ON( st.id = stp.settings_template_id AND st.cid = stp.cid )
											    JOIN ' . $strTableName . ' stpp ON( stpp.property_id = st.property_id AND st.cid = stpp.cid )
											WHERE
												pp.property_id = stp.property_id
											    AND pp.cid = stp.cid
											    AND pp.key = stpp.key
											    AND ( pp.value <> stpp.value OR pp.value IS NULL )
												AND pp.cid =' . ( int ) $this->getCid() . '
												AND st.property_id =' . ( int ) $this->getPropertyId() . '
												AND pp.key IN( \'' . implode( '\', \'', $arrstrKeys ) . '\' );';

		if( false === fetchData( $strUpdatePropertyPreferencesSql, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update ' . $strTableName . ' .' ) );
			return false;
		}

		// Insert the property preferences
		$strInsertPropertyPreferencesSql = 'INSERT INTO
												' . $strTableName . '
											(
												cid,
												property_id,
												property_setting_key_id,
												key,
												value,
												updated_by ,
												updated_on,
												created_by,
												created_on
											)
											(
												SELECT
													stpp.cid,
													stp.property_id,
													stpp.property_setting_key_id,
													stpp.key,
													stpp.value,
													' . ( int ) $intCurrentUserId . ' as updated_by,
													NOW() as updated_on,
													' . ( int ) $intCurrentUserId . ' as created_by,
													NOW() as created_on
												FROM
													' . $strTableName . ' stpp
													JOIN settings_templates st ON( st.property_id = stpp.property_id AND st.cid = stpp.cid )
													JOIN settings_template_properties stp ON( stp.settings_template_id = st.id AND st.cid = stp.cid )
													LEFT JOIN ' . $strTableName . ' pp ON( pp.key = stpp.key AND pp.cid = stpp.cid
																							AND pp.property_id IS NOT NULL
																							AND stp.property_id = pp.property_id )
												WHERE
													stpp.cid = ' . ( int ) $this->getCid() . '
													AND st.property_id = ' . ( int ) $this->getPropertyId() . '
													AND pp.id IS NULL
													AND stpp.key IN( \'' . implode( '\', \'', $arrstrKeys ) . '\' )
											);';

		if( false === fetchData( $strInsertPropertyPreferencesSql, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert ' . $strTableName . ' .' ) );
			return false;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objClientDatabase, $objConnectDatabase = NULL ) {
		$arrobjSettingsTemplateProperties	= CSettingsTemplateProperties::createService()->fetchSettingsTemplatePropertiesBySettingsTemplateIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase );
		$arrobjPropertyPreferences			= ( array ) $this->fetchPropertySettings( $objClientDatabase );
		$arrobjPropertyNotifications		= ( array ) $this->fetchPropertySettings( $objClientDatabase, true );

		if( true == valArr( $arrobjSettingsTemplateProperties ) && false === CSettingsTemplateProperties::createService()->bulkDelete( $arrobjSettingsTemplateProperties, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete settings profile properties.' ) );
			return false;
		}

		if( true == valArr( $arrobjPropertyPreferences ) && false === CPropertyPreferences::bulkDelete( $arrobjPropertyPreferences, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete on Property Preferences' ) );
			return false;
		}

		if( true == valArr( $arrobjPropertyNotifications ) && false === \Psi\Eos\Entrata\CPropertyNotifications::createService()->bulkDelete( $arrobjPropertyNotifications, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete on Property Notifications' ) );
			return false;
		}

		return parent::delete( $intCurrentUserId, $objClientDatabase );
	}

	/**
	* Fetch functions
	*/

	public function fetchPropertySettings( $objDatabase, $boolIsNotification = false ) {
		$strClassName 		= ( false == $boolIsNotification ) ? 'CPropertyPreferences' : 'CPropertyNotifications';
		$strFunctionName 	= ( false == $boolIsNotification ) ? 'fetchPropertyPreferencesByPropertyIdByCid' : 'fetchPropertyNotificationsByPropertyIdByCid';

		return $strClassName::$strFunctionName( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchProperties( $objDatabase, $boolReturnArray = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesBySettingsTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolReturnArray );
	}

	public function fetchSettingsTemplateProperties( $objDatabase ) {
		return CSettingsTemplateProperties::createService()->fetchSettingsTemplatePropertiesBySettingTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDatabasesByDatabaseTypeIds( $arrintDatabaseTypeIds, $objClientDatabase, $objConnectDatabase, &$arrobjDatabases ) {

		$boolIsClientDatabase = false;
		$arrintDatabaseTypeUserTypes = [];
		foreach( $arrintDatabaseTypeIds as $intDatabaseTypeId ) {

			if( 0 == $intDatabaseTypeId || CDatabaseType::CLIENT == $intDatabaseTypeId ) {
				$boolIsClientDatabase = true;
				continue;
			}

			$intDefaultDatabaseUserTypeId = CDatabaseUser::getDefaultDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseTypeId );
			$arrintDatabaseTypeUserTypes[$intDatabaseTypeId]['type_id'] = $intDatabaseTypeId;
			$arrintDatabaseTypeUserTypes[$intDatabaseTypeId]['user_id'] = $intDefaultDatabaseUserTypeId;
		}

		$arrobjDatabases = ( array ) CDatabases::fetchDatabasesByDatabaseTypeIdsByDefaultUserTypeIds( $arrintDatabaseTypeUserTypes, $objConnectDatabase );

		if( true == $boolIsClientDatabase ) {
			$arrobjDatabases[$objClientDatabase->getDataBaseTypeId()] = $objClientDatabase;
		}
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->m_arrobjSettingsTemplatesOccupancyTypeIds = CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrobjSettingsTemplatesOccupancyTypeIds;
	}

}
?>