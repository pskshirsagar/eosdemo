<?php

class CScheduledCharge extends CBaseScheduledCharge {

	protected $m_intKey;
	protected $m_intPropertyId;
	protected $m_intCurrentAmount;
	protected $m_intCaptureDelayDays;
	protected $m_intRateChangePercent;
	protected $m_intFutureMtmLeaseIntervalId;
	protected $m_intCustomerRelationshipId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intArTransactionId;
	protected $m_intSubMaintenanceProblemId;
	protected $m_intInstallmentId;
	protected $m_intLeasingTierSpecialId;
	protected $m_intArTriggerTypeId;
	protected $m_intQuoteLeaseTermId;
	protected $m_intArCodeGroupId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intSubsidyCertificationId;
	protected $m_intLeaseAssociationLeaseStatusTypeId;
	protected $m_intCamCalculationTypeId;
	protected $m_intApCodeId;
	protected $m_intGlAccountId;
	protected $m_intArCodeSummaryTypeId;
	protected $m_intLedgerFilterId;
	protected $m_intLeaseIntervalStatusTypeId;
	protected $m_intUnitSpaceId;

	protected $m_strLeaseNumber;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveOutDate;
	protected $m_strMoveInDate;
	protected $m_strArCodeName;
	protected $m_strArCodeDescription;
	protected $m_strArTriggerName;
	protected $m_strUnitNumberCache;
	protected $m_strLeaseStatusTypeName;
	protected $m_strMaintenanceProblemName;
	protected $m_strMaintenanceActionName;
	protected $m_strInstallmentName;
	protected $m_strLeaseTermName;
	protected $m_strFileAssociationIds;
	protected $m_strCustomerNameFull;
	protected $m_strPropertyCamBudgetStartDate;
	protected $m_strLeaseTermExpiryDate;
	protected $m_strChargeFrequency;
	protected $m_strInstallmentPostOnDate;

	protected $m_objArCode;
	protected $m_objArTrigger;
	protected $m_objPropertyGlSetting;
	protected $m_objPropertyChargeSetting;
	protected $m_objPropertyIntegrationDatabase;
	private   $m_objSubsidyCertificationScheduledCharge;
	protected $m_arrmixProposedScheduledCharge;

	protected $m_arrobjArTransactions;
	protected $m_arrmixModulePermissions;
	protected $m_arrintInstallmentScheduledChargeIds;

	protected $m_fltTaxAmount;
	protected $m_fltChargeAmountForDisplay;
	protected $m_fltPercentChange;
	protected $m_fltLeaseCamShareAmount;
	protected $m_fltTotalMonthlyChargeAmount;

	protected $m_boolIsCharge;
	protected $m_boolPostAllAtOnce;
	protected $m_boolIsRent;
	protected $m_boolIsReversed;
	protected $m_boolIsHistorical;
	protected $m_boolIsPropertyIntegrationDatabasesFetched;
	protected $m_boolProrateCharges;
	protected $m_boolPublishToOfferLetter;

	protected $m_fltTaxPercent;

	const MIN_PERCENT_VALUE = -9999.9999;
	const MAX_PERCENT_VALUE = 9999.9999;

	use TProcessBulkAddEditScheduledCharges;

	public static $c_arrmixModules = [
        'RESIDENT_SYSTEM_ADD_SCHEDULED_CHARGES'		        => CModule::RESIDENT_SYSTEM_ADD_SCHEDULED_CHARGES,
        'RESIDENT_SYSTEM_EDIT_SCHEDULED_CHARGES'	        => CModule::RESIDENT_SYSTEM_EDIT_SCHEDULED_CHARGES,
        'RESIDENT_SYSTEM_DELETE_SCHEDULED_CHARGES' 	        => CModule::RESIDENT_SYSTEM_DELETE_SCHEDULED_CHARGES,
        'RESIDENT_SYSTEM_ADD_SCHEDULED_CREDITS' 	        => CModule::RESIDENT_SYSTEM_ADD_SCHEDULED_CREDITS,
        'RESIDENT_SYSTEM_EDIT_SCHEDULED_CREDITS' 	        => CModule::RESIDENT_SYSTEM_EDIT_SCHEDULED_CREDITS,
        'RESIDENT_SYSTEM_ALLOW_CUSTOM_CHARGE_DAYS' 	        => CModule::RESIDENT_SYSTEM_ALLOW_CUSTOM_CHARGE_DAYS,
        'RESIDENT_SYSTEM_EDIT_MONTH_TO_MONTH_AMOUNT'        => CModule::RESIDENT_SYSTEM_EDIT_MONTH_TO_MONTH_AMOUNT,

		'LEAD_ADD_SCHEDULED_CHARGES'		                => CModule::LEAD_ADD_SCHEDULED_CHARGES,
        'LEAD_EDIT_SCHEDULED_CHARGES'	                    => CModule::LEAD_EDIT_SCHEDULED_CHARGES,
        'LEAD_DELETE_SCHEDULED_CHARGES_CREDITS' 	        => CModule::LEAD_DELETE_SCHEDULED_CHARGES_CREDITS,
        'LEAD_ADD_SCHEDULED_CREDITS' 	                    => CModule::LEAD_ADD_SCHEDULED_CREDITS,
        'LEAD_EDIT_SCHEDULED_CREDITS' 	                    => CModule::LEAD_EDIT_SCHEDULED_CREDITS,
        'LEAD_ALLOW_CUSTOM_CHARGE_DAYS' 	                => CModule::LEAD_ALLOW_CUSTOM_CHARGE_DAYS,
		'LEAD_EDIT_MONTH_TO_MONTH_AMOUNT'                   => CModule::LEAD_EDIT_MONTH_TO_MONTH_AMOUNT
	];

	public function __construct() {
		parent::__construct();

		$this->m_fltTaxAmount = '0';
		return;
	}

	/**
	 * Add Functions
	 */

	public function addArTransaction( $objArTransaction ) {
		$this->m_arrobjArTransactions[] = $objArTransaction;
	}

	/**
	 * Get Functions
	 */

	public function getProposedScheduledCharge() {
		return $this->m_arrmixProposedScheduledCharge;
	}

	public function getLeaseNumber() {
		return $this->m_strLeaseNumber;
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getFutureMtmLeaseIntervalId() {
		return $this->m_intFutureMtmLeaseIntervalId;
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function getIsReversed() {
		return $this->m_boolIsReversed;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getArCodeDescription() {
		return $this->m_strArCodeDescription;
	}

	public function getPostAllAtOnce() {
		return $this->m_boolPostAllAtOnce;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getMaintenanceProblemName() {
		return $this->m_strMaintenanceProblemName;
	}

	public function getMaintenanceActionName() {
		return $this->m_strMaintenanceActionName;
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getCurrentAmount() {
		return $this->m_intCurrentAmount;
	}

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function getRateChangePercent() {
		return $this->m_intRateChangePercent;
	}

	public function getCaptureDelayDays() {
		return $this->m_intCaptureDelayDays;
	}

	public function getChargeAmountForDisplay() {
		return $this->m_fltChargeAmountForDisplay;
	}

	public function getLeaseStatusTypeName() {
		return $this->m_strLeaseStatusTypeName;
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function getArCodeGroupId() {
		return $this->m_intArCodeGroupId;
	}

	public function getArCodeSummaryTypeId() {
		return $this->m_intArCodeSummaryTypeId;
	}

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getFormattedChargeAmount() {
		if( 0 > $this->getChargeAmount() ) {
			return '(' . __( '{%m, 0, nots}', [ abs( $this->getChargeAmount() ) ] ) . ')';
		} else {
			return __( '{%m, 0, nots}', [ $this->getChargeAmount() ] );
		}
	}

	public function getSubMaintenanceProblemId() {
		return $this->m_intSubMaintenanceProblemId;
	}

	public function getInstallmentName() {
		return $this->m_strInstallmentName;
	}

	public function getChargeAmountByLeaseStartDateByLeaseEndDate( $strLeaseStartDate, $strLeaseEndDate ) {
		$boolIsValid = true;

		if( false == is_null( $this->getChargeStartDate() ) && false == is_null( $this->getChargeEndDate() ) ) {

			$intChargeStartDate = strtotime( date( 'm/1/Y 00:00:00', strtotime( $this->getChargeStartDate() ) ) );
			$intChargeEndDate 	= strtotime( date( 'm/1/Y 00:00:00', strtotime( $this->getChargeEndDate() ) ) );
			$intLeaseStartDate 	= strtotime( date( 'm/1/Y 00:00:00', strtotime( $strLeaseStartDate ) ) );
			$intLeaseEndDate 	= strtotime( date( 'm/1/Y 00:00:00', strtotime( $strLeaseEndDate ) ) );

			if( ( $intChargeStartDate > $intLeaseStartDate ) || ( $intChargeEndDate < $intLeaseEndDate ) ) {
				$boolIsValid = false;
			}
		}

		return ( true == $boolIsValid ) ? $this->getChargeAmount() : 0;
	}

	public function getKey() {
		return $this->m_intKey;
	}

	public function getIsCharge() {
		return $this->m_boolIsCharge;
	}

	public function getIsRent() {
		return $this->m_boolIsRent;
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getLeasingTierSpecialId() {
		return $this->m_intLeasingTierSpecialId;
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function getQuoteLeaseTermId() {
		return $this->m_intQuoteLeaseTermId;
	}

	public function getPublishToOfferLetter() {
		return $this->m_boolPublishToOfferLetter;
	}

	public function getModulePermissions() {
		return $this->m_arrmixModulePermissions;
	}

	public function getSubsidyCertificationId() {
		return $this->m_intSubsidyCertificationId;
	}

	public function getSubsidyCertificationScheduledCharge() {
		return $this->m_objSubsidyCertificationScheduledCharge;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getLeaseAssociationLeaseStatusTypeId() {
	    return $this->m_intLeaseAssociationLeaseStatusTypeId;
    }

	public function getOrFetchPropertyGlSetting( $objDatabase ) {
		if( false == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$this->m_objPropertyGlSetting = CPropertyGlSettings::fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objPropertyGlSetting;
	}

	public function getOrFetchPropertyChargeSetting( $objDatabase ) {
		if( false == valObj( $this->m_objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			$this->m_objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objPropertyChargeSetting;
	}

	public function getOrFetchArTrigger( $objDatabase ) {
		if( false == valObj( $this->m_objArTrigger, 'CArTrigger' ) || $this->getArTriggerId() != $this->m_objArTrigger->getId() ) {
			$this->m_objArTrigger = \Psi\Eos\Entrata\CArTriggers::createService()->fetchArTriggerById( $this->getArTriggerId(), $objDatabase );
		}

		return $this->m_objArTrigger;
	}

	public function getOrfetchArCode( $objDatabase ) {
		if( false == valObj( $this->m_objArCode, 'CArCode' ) || $this->getArCodeId() != $this->m_objArCode->getId() ) {
			$this->m_objArCode = $this->fetchArCode( $objDatabase );
		}

		return $this->m_objArCode;
	}

	public function getOrFetchPropertyIntegrationDatabase( $objDatabase ) {
		if( false == isset( $this->m_boolIsPropertyIntegrationDatabasesFetched ) && false == valObj( $this->m_objPropertyIntegrationDatabase, 'CPropertyIntegrationDatabase' ) ) {
			$this->m_objPropertyIntegrationDatabase = \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabaseByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objPropertyIntegrationDatabase;
	}

    public function getPercentChange() {
		return $this->m_fltPercentChange;
    }

	public function getFileAssociationIds() {
		if( true == valArr( $this->m_strFileAssociationIds ) ) {
			return $this->m_strFileAssociationIds;
		} elseif( false == empty( $this->m_strFileAssociationIds ) ) {
			return $this->expandIds( $this->m_strFileAssociationIds );
		} else {
			return [];
		}
	}

	public function getIsHistorical() {
		return $this->m_boolIsHistorical;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function getLeaseCamShareAmount() {
		return $this->m_fltLeaseCamShareAmount;
	}

	public function getCamCalculationTypeId() {
		return $this->m_intCamCalculationTypeId;
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getPropertyCamBudgetStartDate() {
		return $this->m_strPropertyCamBudgetStartDate;
	}

	public function getChargeFrequency() {
		return $this->m_strChargeFrequency;
	}

	public function getInstallmentScheduledChargeIds() {
		return $this->m_arrintInstallmentScheduledChargeIds;
	}

	public function getTotalMonthlyChargeAmount() {
		return $this->m_fltTotalMonthlyChargeAmount;
	}

	/**
	 * @return string
	 */
	public function getLeaseTermExpiryDate() {
		return $this->m_strLeaseTermExpiryDate;
	}

	public function getProrateCharges() {
		return $this->m_boolProrateCharges;
	}

	public function getOriginalValues() {
		return $this->m_arrstrOriginalValues;
	}

	public function getMemo() {
		$objScheduledChargeMemo = new CScheduledChargesMemoLibrary();

		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		return $objScheduledChargeMemo->getMemo( $arrstrDetails, parent::getMemo() );
	}

	public function getHasTranslatedMemo() {
		return false == empty( $this->getDetailsField( [ 'memo_data', 'memo_template' ] ) );
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getLeaseIntervalStatusTypeId() {
		return $this->m_intLeaseIntervalStatusTypeId;
	}

	public function getPostOnDate() {
		return $this->m_strInstallmentPostOnDate;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		if( true == isset( $arrmixValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		if( true == isset( $arrmixValues['move_in_date'] ) ) $this->setMoveInDate( $arrmixValues['move_in_date'] );
		if( true == isset( $arrmixValues['move_out_date'] ) ) $this->setMoveOutDate( $arrmixValues['move_out_date'] );
		if( true == isset( $arrmixValues['future_mtm_lease_interval_id'] ) ) $this->setFutureMtmLeaseIntervalId( $arrmixValues['future_mtm_lease_interval_id'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['ar_code_description'] ) ) $this->setArCodeDescription( $arrmixValues['ar_code_description'] );
		if( true == isset( $arrmixValues['post_all_at_once'] ) ) $this->setPostAllAtOnce( $arrmixValues['post_all_at_once'] );
		if( true == isset( $arrmixValues['post_on_date'] ) ) $this->setPostOnDate( $arrmixValues['post_on_date'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
		if( true == isset( $arrmixValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrmixValues['customer_relationship_id'] );
		if( true == isset( $arrmixValues['lease_status_type_name'] ) ) $this->setLeaseStatusTypeName( $arrmixValues['lease_status_type_name'] );
		if( true == isset( $arrmixValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		if( true == isset( $arrmixValues['sub_maintenance_problem_id'] ) ) $this->setSubMaintenanceProblemId( $arrmixValues['sub_maintenance_problem_id'] );
		if( true == isset( $arrmixValues['maintenance_problem_name'] ) ) $this->setMaintenanceProblemName( $arrmixValues['maintenance_problem_name'] );
		if( true == isset( $arrmixValues['maintenance_action_name'] ) ) $this->setMaintenanceActionName( $arrmixValues['maintenance_action_name'] );
		if( true == isset( $arrmixValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrmixValues['ar_transaction_id'] );
		if( true == isset( $arrmixValues['is_reversed'] ) ) $this->setIsReversed( $arrmixValues['is_reversed'] );
		if( true == isset( $arrmixValues['installment_name'] ) ) $this->setInstallmentName( $arrmixValues['installment_name'] );
		if( true == isset( $arrmixValues['key'] ) ) $this->setKey( $arrmixValues['key'] );
		if( true == isset( $arrmixValues['is_charge'] ) ) $this->setIsCharge( $arrmixValues['is_charge'] );
		if( true == isset( $arrmixValues['is_rent'] ) ) $this->setIsRent( $arrmixValues['is_rent'] );
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( true == isset( $arrmixValues['leasing_tier_special_id'] ) ) $this->setLeasingTierSpecialId( $arrmixValues['leasing_tier_special_id'] );
		if( true == isset( $arrmixValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrmixValues['ar_trigger_type_id'] );
		if( true == isset( $arrmixValues['current_amount'] ) ) $this->setCurrentAmount( $arrmixValues['current_amount'] );
		if( true == isset( $arrmixValues['tax_amount'] ) ) $this->setTaxAmount( $arrmixValues['tax_amount'] );
		if( true == isset( $arrmixValues['capture_delay_days'] ) ) $this->setCaptureDelayDays( $arrmixValues['capture_delay_days'] );
		if( true == isset( $arrmixValues['quote_lease_term_id'] ) ) $this->m_intQuoteLeaseTermId = trim( $arrmixValues['quote_lease_term_id'] );
		if( true == isset( $arrmixValues['publish_to_offer_letter'] ) ) $this->m_boolPublishToOfferLetter = trim( $arrmixValues['publish_to_offer_letter'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['lease_interval_status_type_id'] ) ) $this->setLeaseIntervalStatusTypeId( $arrmixValues['lease_interval_status_type_id'] );

		if( true == isset( $arrmixValues['ar_code_group_id'] ) ) $this->m_intArCodeGroupId = trim( $arrmixValues['ar_code_group_id'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['lease_association_lease_status_type_id'] ) ) $this->m_intLeaseAssociationLeaseStatusTypeId = trim( $arrmixValues['lease_association_lease_status_type_id'] );
		if( true == isset( $arrmixValues['percent_change'] ) ) $this->setPercentChange( $arrmixValues['percent_change'] );

		if( true == isset( $arrmixValues['file_association_ids'] ) ) $this->setFileAssociationIds( $arrmixValues['file_association_ids'] );
		if( true == isset( $arrmixValues['is_historical'] ) )  $this->setIsHistorical( $arrmixValues['is_historical'] );
		if( true == isset( $arrmixValues['customer_name_full'] ) )  $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		if( true == isset( $arrmixValues['subsidy_certification_id'] ) )  $this->setSubsidyCertificationId( $arrmixValues['subsidy_certification_id'] );
		if( true == isset( $arrmixValues['lease_cam_share_amount'] ) )  $this->setLeaseCamShareAmount( $arrmixValues['lease_cam_share_amount'] );
		if( true == isset( $arrmixValues['cam_calculation_type_id'] ) )  $this->setCamCalculationTypeId( $arrmixValues['cam_calculation_type_id'] );
		if( true == isset( $arrmixValues['ap_code_id'] ) )  $this->setApCodeId( $arrmixValues['ap_code_id'] );
		if( true == isset( $arrmixValues['gl_account_id'] ) )  $this->setGlAccountId( $arrmixValues['gl_account_id'] );
		if( true == isset( $arrmixValues['property_cam_budget_start_date'] ) )  $this->setPropertyCamBudgetStartDate( $arrmixValues['property_cam_budget_start_date'] );
		if( true == isset( $arrmixValues['lease_term_expiry_date'] ) )  $this->setLeaseTermExpiryDate( $arrmixValues['lease_term_expiry_date'] );
		if( true == isset( $arrmixValues['prorate_charges'] ) )  $this->setProrateCharges( $arrmixValues['prorate_charges'] );
		if( true == isset( $arrmixValues['charge_frequency'] ) )  $this->setChargeFrequency( $arrmixValues['charge_frequency'] );
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );
		if( true == isset( $arrmixValues['ar_code_summary_type_id'] ) ) $this->setArCodeSummaryTypeId( $arrmixValues['ar_code_summary_type_id'] );
		if( true == isset( $arrmixValues['ledger_filter_id'] ) ) $this->setLedgerFilterId( $arrmixValues['ledger_filter_id'] );
		if( true == isset( $arrmixValues['total_monthly_charge_amount'] ) ) $this->setTotalMonthlyChargeAmount( $arrmixValues['total_monthly_charge_amount'] );
		if( true == isset( $arrmixValues['installment_scheduled_charge_ids'] ) ) $this->setInstallmentScheduledChargeIds( explode( ',', $arrmixValues['installment_scheduled_charge_ids'] ) );
		if( true == isset( $arrmixValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrmixValues['unit_space_id'] );

		$this->setChargeAmountForDisplay();
		return;
	}

	public function setProposedScheduledCharge( $arrmixProposedScheduledCharge ) {
		$this->m_arrmixProposedScheduledCharge = $arrmixProposedScheduledCharge;
	}

	public function setFutureMtmLeaseIntervalId( $intFutureMtmLeaseIntervalId ) {
		return $this->m_intFutureMtmLeaseIntervalId = $intFutureMtmLeaseIntervalId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, true );
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->m_intArTransactionId = $intArTransactionId;
	}

	public function setIsReversed( $boolIsReversed ) {
		$this->m_boolIsReversed = CStrings::strToBool( $boolIsReversed );
	}

	public function setLeaseNumber( $strLeaseNumber ) {
		$this->m_strLeaseNumber = $strLeaseNumber;
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->m_strLeaseStartDate = $strLeaseStartDate;
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = $strLeaseEndDate;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setArCodeDescription( $strArCodeDescription ) {
		$this->m_strArCodeDescription = $strArCodeDescription;
	}

	public function setPostAllAtOnce( $boolPostAllAtOnce ) {
		$this->m_boolPostAllAtOnce = $boolPostAllAtOnce;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = CStrings::strTrimDef( $strArTriggerName, 240, NULL, true );
	}

	public function setMaintenanceProblemName( $strMaintenanceProblemName ) {
		$this->m_strMaintenanceProblemName = CStrings::strTrimDef( $strMaintenanceProblemName, 240, NULL, true );
	}

	public function setMaintenanceActionName( $strMaintenanceActionName ) {
		$this->m_strMaintenanceActionName = CStrings::strTrimDef( $strMaintenanceActionName, 240, NULL, true );
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->m_intCustomerRelationshipId = $intCustomerRelationshipId;
	}

	public function setArTransactions( $arrobjArTransaction ) {
		$this->m_arrobjArTransactions = $arrobjArTransaction;
	}

	public function setCurrentAmount( $intCurrentAmount ) {
		$this->m_intCurrentAmount = $intCurrentAmount;
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->m_fltTaxAmount = CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 );
	}

	public function setRateChangePercent( $intRateChangePercent ) {
		$this->m_intRateChangePercent = $intRateChangePercent;
	}

	public function setCaptureDelayDays( $intCaptureDelayDays ) {
		$this->m_intCaptureDelayDays = $intCaptureDelayDays;
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->m_intLeaseIntervalTypeId = $intLeaseIntervalTypeId;
	}

	public function setChargeAmountForDisplay() {
		$this->m_fltChargeAmountForDisplay	= $this->getChargeAmount();
	}

	public function setLeaseStatusTypeName( $strLeaseStatusTypeName ) {
		$this->m_strLeaseStatusTypeName = $strLeaseStatusTypeName;
	}

	public function setSubMaintenanceProblemId( $intSubMaintenanceProblemId ) {
		$this->m_intSubMaintenanceProblemId = $intSubMaintenanceProblemId;
	}

	public function setInstallmentName( $strInstallmentName ) {
		$this->m_strInstallmentName = $strInstallmentName;
	}

	public function setKey( $intKey ) {
		$this->m_intKey = $intKey;
	}

	public function setIsCharge( $boolIsCharge ) {
		$this->m_boolIsCharge = $boolIsCharge;
	}

	public function setIsRent( $boolIsRent ) {
		$this->m_boolIsRent = $boolIsRent;
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	public function setLeasingTierSpecialId( $intLeasingTierSpecialId ) {
		$this->m_intLeasingTierSpecialId = $intLeasingTierSpecialId;
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->m_intArTriggerTypeId = $intArTriggerTypeId;
	}

	public function setQuoteLeaseTermId( $intQuoteLeaseTermId ) {
		$this->m_intQuoteLeaseTermId = CStrings::strToIntDef( $intQuoteLeaseTermId, NULL, false );
	}

	public function setPublishToOfferLetter( $boolPublishToOfferLetter ) {
		$this->set( 'm_boolPublishToOfferLetter', CStrings::strToBool( $boolPublishToOfferLetter ) );
	}

	public function setArCodeGroupId( $intArCodeGroupId ) {
		$this->m_intArCodeGroupId = $intArCodeGroupId;
	}

	public function setArCodeSummaryTypeId( $intArCodeSummaryTypeId ) {
		$this->m_intArCodeSummaryTypeId = $intArCodeSummaryTypeId;
	}

	public function setLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intLedgerFilterId = $intLedgerFilterId;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setModulePermissions( $arrmixModulePermissions ) {
		$this->m_arrmixModulePermissions = $arrmixModulePermissions;
	}

	public function setSubsidyCertificationId( $intSubsidyCertificationId ) {
		$this->m_intSubsidyCertificationId = $intSubsidyCertificationId;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setLeaseAssociationLeaseStatusTypeId( $intLeaseAssociationLeaseStatusTypeId ) {
	    $this->m_intLeaseAssociationLeaseStatusTypeId = $intLeaseAssociationLeaseStatusTypeId;
    }

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	public function setPropertyChargeSetting( $objPropertyChargeSetting ) {
		$this->m_objPropertyChargeSetting = $objPropertyChargeSetting;
	}

	public function setArTrigger( $objArTrigger ) {
		$this->m_objArTrigger = $objArTrigger;
	}

	public function setArCode( $objArCode ) {
		$this->m_objArCode = $objArCode;
	}

	public function setPropertyIntegrationDatabase( $objPropertyIntegrationDatabase ) {
		$this->m_objPropertyIntegrationDatabase = $objPropertyIntegrationDatabase;
	}

	public function setIsPropertyIntegrationDatabasesFetched( $boolIsPropertyIntegrationDatabasesFetched ) {
		$this->m_boolIsPropertyIntegrationDatabasesFetched = $boolIsPropertyIntegrationDatabasesFetched;
	}

	public function setPercentChange( $fltPercentChange ) {
		$this->m_fltPercentChange = $fltPercentChange;
	}

	public function setFileAssociationIds( $arrintFileAssociationIds ) {
		if( true == valArr( $arrintFileAssociationIds ) ) {
			$this->m_strFileAssociationIds = $this->flattenIds( $arrintFileAssociationIds );
		} elseif( false == empty( $arrintFileAssociationIds ) ) {
			$this->m_strFileAssociationIds = $arrintFileAssociationIds;
		} else {
			$this->m_strFileAssociationIds = NULL;
		}
	}

	public function setIsHistorical( $boolIsHistorical ) {
		$this->m_boolIsHistorical = $boolIsHistorical;
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function setSubsidyCertificationScheduledCharge( $objSubsidyCertificationScheduledCharge ) {
		$this->m_objSubsidyCertificationScheduledCharge = $objSubsidyCertificationScheduledCharge;
	}

	public function setLeaseCamShareAmount( $fltLeaseCamShareAmount ) {
		$this->m_fltLeaseCamShareAmount = $fltLeaseCamShareAmount;
	}

	public function setCamCalculationTypeId( $intCamCalculationTypeId ) {
		$this->m_intCamCalculationTypeId = $intCamCalculationTypeId;
	}

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = $intApCodeId;
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->m_intGlAccountId = $intGlAccountId;
	}

	public function setPropertyCamBudgetStartDate( $strPropertyCamBudgetStartDate ) {
		$this->m_strPropertyCamBudgetStartDate = $strPropertyCamBudgetStartDate;
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setInstallmentScheduledChargeIds( $arrintInstallmentScheduledChargeIds ) {
		$this->m_arrintInstallmentScheduledChargeIds = $arrintInstallmentScheduledChargeIds;
	}

	public function setTotalMonthlyChargeAmount( $fltTotalMonthlyChargeAmount ) {
		$this->m_fltTotalMonthlyChargeAmount = $fltTotalMonthlyChargeAmount;
	}

	public function setPostOnDate( $strPostOnDate ) {
		$this->m_strInstallmentPostOnDate = CStrings::strTrimDef( $strPostOnDate, -1, NULL, true );
	}

	/**
	 * @param string $strLeaseTermExpiryDate
	 */
	public function setLeaseTermExpiryDate( $strLeaseTermExpiryDate ) {
		$this->m_strLeaseTermExpiryDate = $strLeaseTermExpiryDate;
	}

	public function setProrateCharges( $boolProrateCharges ) {
		$this->set( 'm_boolProrateCharges', CStrings::strToBool( $boolProrateCharges ) );
	}

	public function setChargeFrequency( $strChargeFrequency ) {
		$this->m_strChargeFrequency = $strChargeFrequency;
	}

	public function setLeaseIntervalStatusTypeId( $intLeaseIntervalStatusTypeId ) {
		$this->m_intLeaseIntervalStatusTypeId = $intLeaseIntervalStatusTypeId;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	/**
	 * Create Functions
	 */

	public function createArTransaction( $intLastPostedOn, $strTransactionDatetime, $fltChargeAmount, $objPropertyGlSetting, $objDatabase, $objPropertyChargeSetting = NULL, $boolApplyRounding = true ) {

		$objArTransaction = new CArTransaction();

		$objArCode = $this->getOrfetchArCode( $objDatabase );
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objArTransaction->setParentScheduledChargeId( $this->getParentScheduledChargeId() );
		$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
		$objArTransaction->setArCodeId( $this->getArCodeId() );
		$objArTransaction->setPostDate( $strTransactionDatetime );
		$objArTransaction->setMemo( $this->getMemo() );

		if( true == $boolApplyRounding && true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			$fltChargeAmount = roundAmount( $objPropertyChargeSetting->getRoundTypeId(), $fltChargeAmount );
		}

		$objArTransaction->setTransactionAmount( $fltChargeAmount );
		$objArTransaction->setScheduledChargeId( $this->getId() );

		// We set this date here, so we can apply it to the scheduled payment if the user posts it.
		if( false == is_null( $intLastPostedOn ) ) {
			$objArTransaction->setScheduledChargeLastPostedOn( date( 'm/d/Y', $intLastPostedOn ) );
			$objArTransaction->setScheduledChargePostedThroughDate( date( 'm/d/Y', $intLastPostedOn ) );
		}

		// Set scheduled charge postmonth to Greatest of Ar post Month and month of Transaction post date
		$strPostMonth = ( strtotime( $objPropertyGlSetting->getArPostMonth() ) >= strtotime( date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) )  ? $objPropertyGlSetting->getArPostMonth() : date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) );
		$objArTransaction->setPostMonth( $strPostMonth );

		return $objArTransaction;
	}

	public static function modifyScheduledChargeDate( $strScheduledChargeDate ) {

		if( false == valStr( $strScheduledChargeDate ) ) return $strScheduledChargeDate;

		$strScheduledChargedateDateTime		 = new DateTime( $strScheduledChargeDate );
		$strScheduledChargedateDateTimeStamp = $strScheduledChargedateDateTime->format( 'U' );

		$objDateTime = new DateTime( '1st December 2099' ); // Max allowed date here
		$strMaxScheduledChargeDateTimeStamp = $objDateTime->format( 'U' ); // It gives the time difference in second from 1st January 1970 to 2099

		$objDateTime = new DateTime( '1st January 1970' ); // Min allowed date here
		$strMinScheduledChargeDateTimeStamp = $objDateTime->format( 'U' );

		if( $strScheduledChargedateDateTimeStamp > $strMaxScheduledChargeDateTimeStamp ) {
			$strScheduledChargeDate = '12/01/2099';
		}

		if( $strScheduledChargedateDateTimeStamp < $strMinScheduledChargeDateTimeStamp ) {
			$strScheduledChargeDate = '01/01/1970';
		}

		return $strScheduledChargeDate;

	}

	public function modifyEsiteScheduledChargeDate( $strScheduledChargeDate ) {

		if( false == valStr( $strScheduledChargeDate ) ) return $strScheduledChargeDate;

		$strScheduledChargedateDateTime		 = new DateTime( $strScheduledChargeDate );
		$strScheduledChargedateDateTimeStamp = $strScheduledChargedateDateTime->format( 'U' );

		$objDateTime = new DateTime( '1st December 2099' ); // Max allowed date here
		$strMaxScheduledChargeDateTimeStamp = $objDateTime->format( 'U' ); // It gives the time difference in second from 1st January 1970 to 2099

		$objDateTime = new DateTime( '1st January 1970' ); // Min allowed date here
		$strMinScheduledChargeDateTimeStamp = $objDateTime->format( 'U' );

		if( $strScheduledChargedateDateTimeStamp > $strMaxScheduledChargeDateTimeStamp ) {
			$strScheduledChargeDate = '2099/01/12';
		}

		if( $strScheduledChargedateDateTimeStamp < $strMinScheduledChargeDateTimeStamp ) {
			$strScheduledChargeDate = '1970/01/01';
		}

		return $strScheduledChargeDate;

	}

	public function createSubsidyCertificationScheduledCharge() {
		$this->m_objSubsidyCertificationScheduledCharge = new CSubsidyCertificationScheduledCharge();
		$this->m_objSubsidyCertificationScheduledCharge->setCid( $this->getCid() );
		$this->m_objSubsidyCertificationScheduledCharge->setPropertyId( $this->getPropertyId() );
		$this->m_objSubsidyCertificationScheduledCharge->setLeaseId( $this->getLeaseId() );
		$this->m_objSubsidyCertificationScheduledCharge->setScheduledChargeId( $this->getId() );
		$this->m_objSubsidyCertificationScheduledCharge->setSubsidyCertificationId( $this->getSubsidyCertificationId() );
		return $this->m_objSubsidyCertificationScheduledCharge;
	}

	/**
	 * Validation Functions
	 */

	public function valArCodeId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_intArCodeId ) || 0 >= ( int ) $this->m_intArCodeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Charge code is required.' ) ) );
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$intDefaultLedgerFilterId = \Psi\Eos\Entrata\CArCodes::createService()->fetchDefaultLedgerFilterIdByIdByCid( $this->m_intArCodeId, $this->getCid(), $objDatabase );

			if( CDefaultLedgerFilter::GROUP == $intDefaultLedgerFilterId ) {
				$objLease = $this->fetchLease( $objDatabase );

				if( true == valObj( $objLease, 'CLease' ) && false == valId( $objLease->getOrganizationContractId() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Group charge codes cannot be used on non-Group leases.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valLeaseIntervalId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intLeaseIntervalId ) || 0 >= ( int ) $this->m_intLeaseIntervalId ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_id', __( 'Lease interval is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intArTriggerId ) || 0 >= ( int ) $this->m_intArTriggerId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', __( 'Charge timing is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valChargeAmount() {
		$boolIsValid = true;
		if( CArFormula::FIXED_AMOUNT == $this->getArFormulaId() ) {
			if( false == isset( $this->m_fltChargeAmount ) && $this->getArOriginId() == CArOrigin::ADD_ONS ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', __( 'An active scheduled charge is required to move in Rentable Items.' ) ) );

			} elseif( false == isset( $this->m_fltChargeAmount ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', __( 'Charge amount is required.' ) ) );
			}
			if( 0 == $this->m_fltChargeAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', __( 'Charge amount cannot be zero.' ) ) );
			}
		}

		if( true == in_array( $this->getArFormulaId(), [ CArFormula::FIXED_BAH, CArFormula::FULL_BAH ] ) ) {
			if( false == isset( $this->m_fltChargeMultiplierAmount ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', __( 'Charge amount is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valGlAccountType( $objDatabase ) {
		$boolIsValid = true;

		if( true == isset( $this->m_intArCodeId ) && 0 < ( int ) $this->m_intArCodeId ) {
			if( false == isset( $objDatabase ) ) return false;
			$objArCode = $this->getOrfetchArCode( $objDatabase );

			if( true == valObj( $objArCode, 'CArCode' ) ) {
				if( CArTrigger::MONTHLY == $this->m_intArTriggerId && CDefaultArCode::BEGINNING_BALANCE == $objArCode->getDefaultArCodeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Charge code "Beginning Balance" is not allowed on a scheduled charge.' ) ) );
					$boolIsValid = false;
				}

			}
		}

		return $boolIsValid;
	}

	public function valLastPostedOn() {
		$boolIsValid = true;

		// If the field is not empty
		if( true == isset( $this->m_strLastPostedOn ) && false == CValidation::validateISODate( $this->m_strLastPostedOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_posted_on', __( 'Last posted on date is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostedThroughDate() {
		$boolIsValid = true;

		// If the field is not empty
		if( true == valStr( $this->m_strPostedThroughDate ) ) {
			$mixValidatedPostedThroughDate = CValidation::checkISODateFormat( $this->m_strPostedThroughDate, $boolFormat = true );

			if( false === $mixValidatedPostedThroughDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_through_date', __( 'Billing start date is not valid.' ) ) );
				$boolIsValid = false;
				return $boolIsValid;
			} else {
				$this->m_strPostedThroughDate = date( 'm/d/Y', strtotime( $mixValidatedPostedThroughDate ) );
			}

			if( true == valStr( $this->m_strChargeStartDate ) && strtotime( $this->m_strPostedThroughDate ) != strtotime( $this->m_strChargeStartDate ) && strtotime( '-1 day', strtotime( $this->m_strPostedThroughDate ) ) < strtotime( $this->m_strChargeStartDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_through_date', __( 'Billing start date must be greater than or equal to charge start date.' ) ) );
			} elseif( true == valStr( $this->m_strChargeEndDate ) && strtotime( '-1 day', strtotime( $this->m_strPostedThroughDate ) ) > strtotime( $this->m_strChargeEndDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_through_date', __( 'Billing start date must be less than charge end date.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valDeleteAbility( $objDatabase ) {
		$boolIsValid 					= true;
		$intUnreversedTransactionsCount = \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnreversedTransactionsCountByLeaseIdByScheduledChargeIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase );

		if( false == is_null( $this->getPostedThroughDate() ) && false == is_null( $this->getLastPostedOn() ) && 0 < $intUnreversedTransactionsCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You must reverse the transactions associated with this scheduled charge from the ledger before deleting it.' ) ) );
		}
		return $boolIsValid;
	}

	public function valScheduledChargeOnlyForRent( $intDefaultArCodeId ) {
		$boolIsValid = ( $intDefaultArCodeId == $this->getArCodeId() && CArTrigger::MONTHLY == $this->getArTriggerId() && true == is_null( $this->getArOriginReferenceId() ) && true == is_null( $this->getLeaseAssociationId() ) && CArOrigin::ADD_ONS == $this->getArOriginId() ) ? true : false;
		return $boolIsValid;
	}

	public function valDataIntegrity( $objDatabase, $boolIsWorkOrderCharge = false, $arrmixData = [] ) {

		$boolIsValid = true;
		$objScheduledChargeEndDate = NULL;

		$boolIsValid &= $this->valLeaseIntervalId();
		$boolIsValid &= $this->valArCodeId( $objDatabase );
		$boolIsValid &= $this->valArTriggerId();
		$boolIsValid &= $this->valChargeAmount();

		if( false == valId( $this->getArFormulaId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_id', __( 'Type is required. ' ) ) );
			$boolIsValid = false;
		}

		if( CArFormula::PER_ITEM == $this->getArFormulaId() ) {
			$boolIsValid &= $this->valChargeMultiplierAmount();
		}

		if( true == in_array( $this->getArFormulaId(), [ CArFormula::PERCENT_OF_CHARGE_CODE, CArFormula::PERCENT_OF_CHARGE_CODE_GROUP, CArFormula::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
			$boolIsValid &= $this->valChargePercent();
		}

		if( true == in_array( $this->getArFormulaId(), [ CArFormula::PER_ITEM, CArFormula::PER_ITEM_WITH_VOLUME_PRICING, CArFormula::PER_ITEM_WITH_TIERED_PRICING, CArFormula::PERCENT_OF_CHARGE_CODE, CArFormula::PERCENT_OF_CHARGE_CODE_GROUP, CArFormula::PERCENT_OF_CHARGE_CODE_TYPE ] ) && false == valId( $this->getArFormulaReferenceId() ) ) {
			$boolIsValid &= $this->valArFormulaReferenceId();
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->validateArMultiplierMatchesArTriggers( $arrmixData );
		}

		$objArTrigger					= $this->getOrFetchArTrigger( $objDatabase );
		$objPropertyGlSetting           = $this->getOrFetchPropertyGlSetting( $objDatabase );
		$objPropertyChargeSetting       = $this->getOrFetchPropertyChargeSetting( $objDatabase );
		$objPropertyIntergationDatabase = $this->getOrFetchPropertyIntegrationDatabase( $objDatabase );
		$objScheduledCharge				= \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargeByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$objLeaseInterval 				= \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

		if( true == valStr( $this->getChargeStartDate() ) && false == CValidation::checkISODateFormat( $this->getChargeStartDate(), true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Charge start date is not valid.' ) ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		if( true == valStr( $this->getChargeEndDate() ) && false == CValidation::checkISODateFormat( $this->getChargeEndDate(), true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Charge end date is not valid.' ) ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		if( true == valStr( $this->getChargeStartDate() ) )	$objChargeStartDate	= new DateTime( $this->getChargeStartDate() );
		if( true == valStr( $this->getChargeEndDate() ) )	$objChargeEndDate 	= new DateTime( $this->getChargeEndDate() );
		if( true == valStr( $this->getPostedThroughDate() ) && false === \Psi\CStringService::singleton()->stripos( $this->getPostedThroughDate(), 'now' ) ) {
			$objPostedThroughDate = new DateTime( $this->getPostedThroughDate() );
		} elseif( true == valStr( $this->getPostedThroughDate() ) ) {
			$objPostedThroughDate = new DateTime( webDateFormat( getCurrentDateTime() ) );
		}

		if( true == valObj( $objScheduledCharge, 'CScheduledCharge' ) ) {
			if( true == valStr( $objScheduledCharge->getChargeStartDate() ) )	$objScheduledChargeStartDate = new DateTime( $objScheduledCharge->getChargeStartDate() );
			if( true == valStr( $objScheduledCharge->getChargeEndDate() ) )		$objScheduledChargeEndDate = new DateTime( $objScheduledCharge->getChargeEndDate() );
		}

		if( true == valId( $this->getDeletedBy() ) && true == valStr( $this->getDeletedOn() ) && false == valStr( $this->getPostedThroughDate() ) ) return true;

		if( false == $boolIsWorkOrderCharge && true == valObj( $objArTrigger, 'CArTrigger' ) ) {
			if( CArTriggerType::CACHEABLE != $objArTrigger->getArTriggerTypeId() && true == $boolIsValid && false == valStr( $this->getChargeStartDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Charge start date is required. ' ) ) );
				$boolIsValid = false;
			}
		}

		if( true == valObj( $objArTrigger, 'CArTrigger' ) && CArTriggerType::RECURRING_LEASE_CHARGES == $objArTrigger->getArTriggerTypeId() && false == ( boolean ) $this->getEndsWithMoveOut() && false == valStr( $this->getChargeEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Scheduled charges must either have an end date or be set to end at move out.' ) ) );
			$boolIsValid = false;
		}

		// Don't allow charge end date to be set earlier than the posted_through_date. Commenting validation as per request in task 413303
		// if( true == $boolIsValid && $this->getArTriggerId() == CArTrigger::MONTHLY && true == valStr( $this->getChargeEndDate() ) && true == valStr( $this->getPostedThroughDate() ) && strtotime( $this->getChargeEndDate() ) < strtotime( $this->getPostedThroughDate() ) ) {
		//	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', 'A charge cannot have a posted_through_date greater than charge end date. ' ) );
		//	$boolIsValid = false;
		// }

		if( true == $boolIsValid && false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_gl_setting', __( 'Property GL Setting failed to load.' ) ) );
			$boolIsValid = false;
		}

		// Don't allow to change the frequency of already posted scheduled_charges
		if( true == $boolIsValid && true == valStr( $this->getPostedThroughDate() ) && true == valObj( $objScheduledCharge, 'CScheduledCharge' ) && false == valObj( $objPropertyIntergationDatabase, 'CPropertyIntegrationDatabase' )
		    && $this->getArTriggerId() != $objScheduledCharge->getArTriggerId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', __( 'The frequency of posted scheduled charge cannot be changed.' ) ) );
			$boolIsValid = false;
		}

		// Enter deeper validation here if entrata core is being used.
		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {

			if( true == $boolIsValid && false == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'prorate_charges', __( 'Failed to load property_charge_setting object associated with the scheduled charge.' ) ) );
				$boolIsValid = false;
			}

			if( true == $boolIsValid && false == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval', __( 'Failed to load lease_interval associated with the scheduled charge. ' ) ) );
				$boolIsValid = false;
			}

			// Don't allow active scheduled charges on cancelled lease_intervals.  Charges must be fully posted or deleted.
			if( true == $boolIsValid
			    && $objLeaseInterval->getLeaseStatusTypeId() == CLeaseStatusType::CANCELLED
			    && $objLeaseInterval->getLeaseIntervalTypeId() != CLeaseIntervalType::LEASE_MODIFICATION
			    && false == valStr( $this->getDeletedOn() )
			    && ( false == valStr( $this->getChargeEndDate() ) || false == valStr( $this->getPostedThroughDate() ) || $objPostedThroughDate != $objChargeEndDate ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_id', __( 'Active scheduled_charges must be associated to lease_intervals that are not cancelled.' ) ) );
				$boolIsValid = false;
			}

			// Parent scheduled charges must be ended before children can be created.
			if( true == $boolIsValid && false == is_null( $this->getParentScheduledChargeId() )
			    && ( $objLeaseInterval->getLeaseStatusTypeId() != CLeaseStatusType::CANCELLED
			         && $objLeaseInterval->getLeaseStatusTypeId() != CLeaseStatusType::APPLICANT ) ) {

				$objParentScheduledCharge = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargeByIdByCid( $this->getParentScheduledChargeId(), $this->getCid(), $objDatabase );

				if( false == valObj( $objParentScheduledCharge, 'CScheduledCharge' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_scheduled_charge_id', __( 'Parent scheduled charge could not be loaded from the database.' ) ) );
					$boolIsValid = false;
				}
			}

			// validate disabled proration settings on scheduled charges
			if( true == $boolIsValid
			    && false == valStr( $this->getDeletedBy() )
			    && false == valId( $this->getInstallmentId() )
			    && ( false == in_array( $this->getScheduledChargeTypeId(), [ CScheduledChargeType::EARLY_MOVE_IN, CScheduledChargeType::EXTENSION ] ) )
			    && true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {

				$objArCode = $this->getOrfetchArCode( $objDatabase );

				if( ( ( ( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) || true == is_null( $this->getPostedThroughDate() ) )
				        || $objChargeStartDate != $objScheduledChargeStartDate )
				      && true == valStr( $this->getChargeStartDate() ) ) && false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $objArCode->getProrateCharges(), $objLeaseInterval->getIsImmediateMoveIn() ) ) {
					if( CArTrigger::MONTHLY == $this->getArTriggerId() && $objChargeStartDate > ( new DateTime( $this->getChargeStartDate() ) )->modify( 'first day of this month' ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Scheduled charge start date should be the first of the month.' ) ) );
						$boolIsValid = false;
					} elseif( CArTrigger::WEEKLY == $this->getArTriggerId() && ( 'Mon' !== ( date( 'D', strtotime( $this->getChargeStartDate() ) ) ) && ( 1 != ( date( 'j', strtotime( $this->getChargeStartDate() ) ) ) ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Weekly scheduled charges should start on Monday or on the first day of the month.' ) ) );
						$boolIsValid = false;
					} elseif( CArTrigger::TWICE_PER_MONTH == $this->getArTriggerId() && ( 1 != ( date( 'j', strtotime( $this->getChargeStartDate() ) ) ) && 15 != ( date( 'j', strtotime( $this->getChargeStartDate() ) ) ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Scheduled charge start date should be the first OR fifteenth of the month.' ) ) );
						$boolIsValid = false;
					}
				}

				if( ( ( ( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) || true == is_null( $this->getPostedThroughDate() ) )
				        || $objChargeEndDate != $objScheduledChargeEndDate )
				      && true == valStr( $this->getChargeEndDate() ) ) && false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $objArCode->getProrateCharges() ) ) {
					if( CArTrigger::MONTHLY == $this->getArTriggerId() && $objChargeEndDate < ( new DateTime( $this->getChargeEndDate() ) )->modify( 'last day of this month' ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Scheduled charge end date should be the last of the month.' ) ) );
						$boolIsValid = false;
					} elseif( CArTrigger::WEEKLY == $this->getArTriggerId() && ( 'Sun' !== ( date( 'D', strtotime( $this->getChargeEndDate() ) ) ) && $objChargeEndDate < ( new DateTime( $this->getChargeEndDate() ) )->modify( 'last day of this month' ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Weekly scheduled charges should end on Sunday or on the last day of the month.' ) ) );
						$boolIsValid = false;
					} elseif( CArTrigger::TWICE_PER_MONTH == $this->getArTriggerId() && ( 14 != ( date( 'j', strtotime( $this->getChargeEndDate() ) ) ) && $objChargeEndDate < ( new DateTime( $this->getChargeEndDate() ) )->modify( 'last day of this month' ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Scheduled charge end date should be the fourteenth OR last of the month.' ) ) );
						$boolIsValid = false;
					}
				}

			}

			// Don't allow deleted on to be set on scheduled charges if posted_through_date is not null
			if( true == $boolIsValid && true == valStr( $this->getPostedThroughDate() ) && true == valStr( $this->getDeletedOn() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'A posted scheduled charge cannot be deleted.' ) ) );
				$boolIsValid = false;
			}

			// Don't allow scheduled_charge end_date < posted_through_date
			if( true == $boolIsValid && true == valStr( $this->getPostedThroughDate() )
			    && true == valStr( $this->getChargeEndDate() )
			    && ( false == valObj( $objScheduledCharge, 'CScheduledCharge' )
			         || ( $objChargeEndDate != $objScheduledChargeEndDate ) )
			    && $objChargeEndDate < $objPostedThroughDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'A charge cannot have a posted_through_date greater than charge end date.' ) ) );
				$boolIsValid = false;
			}

			if( false == in_array( $this->getArTriggerId(), CArTrigger:: $c_arrintOneTimeLeaseChargesTypeArTriggers ) ) {
				// Dont allow scheduled charges start/end dates > 75 years in future and before year 1970 in past.
				if( true == $boolIsValid && true == valStr( $this->getChargeStartDate() ) && ( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) || $objChargeStartDate != $objScheduledChargeStartDate )
				    && ( $objChargeStartDate < ( new DateTime( '01/01/1970' ) ) || $objChargeStartDate > ( new DateTime() )->modify( '75 years' ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Scheduled charge start date cannot be more than 75 years in future and before year 1970 in past.' ) ) );
					$boolIsValid = false;
				}

				if( true == $boolIsValid && true == valStr( $this->getChargeEndDate() ) && ( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) || $objChargeEndDate != $objScheduledChargeEndDate )
				    && ( $objChargeEndDate < ( new DateTime( '01/01/1970' ) ) || $objChargeEndDate > ( new DateTime() )->modify( '75 years' ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Scheduled charge end date cannot be more than 75 years in future and before year 1970 in past.' ) ) );
					$boolIsValid = false;
				}

				// Scheduled charge start date can not be greater than charge end date
				if( true == $boolIsValid && true == valStr( strtotime( $this->getChargeEndDate() ) ) && $objChargeStartDate > $objChargeEndDate ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Scheduled charge end date cannot be less than charge start date.' ) ) );
					$boolIsValid = false;
				}
			}

			// We shouldn't change the charge start date if last_posted_on IS NOT NULL
			if( true == $boolIsValid
			    && true == valId( $this->getId() )
			    && CArTrigger::MONTHLY == $this->getArTriggerId()
			    && ( false == valObj( $objScheduledCharge, 'CScheduledCharge' )
			         || $objChargeStartDate != $objScheduledChargeStartDate )
			    && true == valStr( $this->getLastPostedOn() ) ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Scheduled charge start date should not change if the scheduled charge posting has begun. ' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	/**
	 * @param \CDatabase $objDatabase
	 * @return bool
	 * @see It will check wheather used charge code is eligible for the military lease or not.
	 */

	public function valMilitaryChargeCode( CDatabase $objDatabase ) {

		$objLease					= $this->fetchLease( $objDatabase );
		$intDefaultLedgerFilterId	= \Psi\Eos\Entrata\CArCodes::createService()->fetchDefaultLedgerFilterIdByIdByCid( $this->getArCodeId(), $this->getCid(), $objDatabase );

		if( CDefaultLedgerFilter::MILITARY == $intDefaultLedgerFilterId && COccupancyType::MILITARY !== $objLease->getOccupancyTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Military charge codes cannot be used on non-military leases.' ) ) );
			return false;
		}

		return true;
	}

	public function validateArMultiplierMatchesArTriggers( $arrmixData ) {

		if( false == isset( $arrmixData['required_ar_trigger_ids'] ) || false == isset( $arrmixData['ar_trigger'] ) || false == isset( $arrmixData['multiplier_or_lookup_table_name'] ) ) {
			return true;
		}

		if( true == valArr( $arrmixData['required_ar_trigger_ids'] ) && false == in_array( $this->getArTriggerId(), $arrmixData['required_ar_trigger_ids'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', $arrmixData['multiplier_or_lookup_table_name'] . ' can not be used with ' . $arrmixData['ar_trigger']->getName() . ' charge timing' ) );
			return false;
		}

		return true;
	}

	public function valArFormulaReferenceId() {

		$boolIsValid = true;
		switch( $this->getArFormulaId() ) {
			case CArFormula::PER_ITEM:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The multiplier is required for the formula: Per Item. ' ) ) );
				$boolIsValid = false;
				break;

			case CArFormula::PER_ITEM_WITH_VOLUME_PRICING:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The lookup is required for the formula: Per Item with Volumn Pricing. ' ) ) );
				$boolIsValid = false;
				break;

			case CArFormula::PER_ITEM_WITH_TIERED_PRICING:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The lookup is required for the formula: Per Item with Tiered Pricing. ' ) ) );
				$boolIsValid = false;
				break;

			case CArFormula::PERCENT_OF_CHARGE_CODE:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The charge code is required for the formula: Percent of charge code. ' ) ) );
				$boolIsValid = false;
				break;

			case CArFormula::PERCENT_OF_CHARGE_CODE_GROUP:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The charge code group is required for the formula: Percent Of Charge Code Group. ' ) ) );
				$boolIsValid = false;
				break;

			case CArFormula::PERCENT_OF_CHARGE_CODE_TYPE:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_reference_id', __( 'The charge code type is required for the formula: Percent Of Charge Code Type. ' ) ) );
				$boolIsValid = false;
				break;

			default:
		}
		return $boolIsValid;
	}

	public function valChargeMultiplierAmount() {
		$boolIsValid = true;
		if( false == isset( $this->m_fltChargeMultiplierAmount ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_multiplier_amount', __( 'The multiplier amount is required for the formula: Per Item. ' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valChargePercent() {
		$boolIsValid = true;

		if( ( 0 == ( $this->m_fltChargePercent * 100 ) ) || ( self::MAX_PERCENT_VALUE < ( $this->m_fltChargePercent * 100 ) ) || ( self::MIN_PERCENT_VALUE >= ( $this->m_fltChargePercent * 100 ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_percent', __( 'The charge percent should be from {%f, 0, p:4;} to {%f, 1, p:4;} but it should not be 0.', [ self::MIN_PERCENT_VALUE, self::MAX_PERCENT_VALUE ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valInstallmentArCodeId( $objDatabase, $arrintDeletedScheduledChargeIds = [] ) {

		$boolIsValid = true;

		$arrobjSchdeuledCharges 	= ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchInstallmentScheduledChargesByLeaseIntervalIdByArCodeIdsByCId( $this->getLeaseIntervalId(), [ $this->getArCodeId() ], $this->getCid(), $objDatabase, true, $arrintDeletedScheduledChargeIds );
		$arrintScheduledChargeIds 	= array_keys( $arrobjSchdeuledCharges );
		$arrobjSchdeuledCharges 	= rekeyObjects( 'ArCodeId', $arrobjSchdeuledCharges );

		if( true == array_key_exists( $this->getArCodeId(), $arrobjSchdeuledCharges )
		    && true == valId( $this->getInstallmentId() )
		    && true == valId( $arrobjSchdeuledCharges[$this->getArCodeId()]->getInstallmentId() )
		    && $arrobjSchdeuledCharges[$this->getArCodeId()]->getInstallmentId() == $this->getInstallmentId()
		    && $arrobjSchdeuledCharges[$this->getArCodeId()]->getChargeAmount() > 0
		    && $this->getChargeAmount() > 0 ) {

			if( false == valId( $this->getId() ) || ( true == valId( $this->getId() ) && false == in_array( $this->getId(), $arrintScheduledChargeIds ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'There should be only 1 installment scheduled charge for a charge code.' ) ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsValidateInstallmentArCodeId = false, $arrmixData = [], $objOriginalScheduledCharge = NULL, $boolIsCurrentLeaseInterval = false, $arrintDeletedScheduledChargeIds = [] ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case 'pre_insert':
				$boolIsValid &= $this->valDataIntegrity( $objDatabase, false, $arrmixData );
				$boolIsValid &= $this->valLastPostedOn();
				break;

			case 'renewal_offer_accept':
			case 'insert_move_out':
			case 'insert_move_in':
			case 'update_move_out':
			case 'insert_work_order_charges':
				$boolIsValid &= $this->valDataIntegrity( $objDatabase, true, $arrmixData );
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->validatePermissions( $strAction );

				if( false == $boolIsCurrentLeaseInterval ) {
					$boolIsValid &= $this->valDataIntegrity( $objDatabase, false, $arrmixData );
				}
				if( CArTrigger::MONTHLY == $this->m_intArTriggerId ) {
					$boolIsValid &= $this->valGlAccountType( $objDatabase );
					$boolIsValid &= $this->valPostedThroughDate();

					if( true == $boolIsValid && true == $boolIsValidateInstallmentArCodeId ) {
						$boolIsValid &= $this->valInstallmentArCodeId( $objDatabase, $arrintDeletedScheduledChargeIds );
					}
				}

				$boolIsValid &= $this->valMilitaryChargeCode( $objDatabase );
				break;

			case 'add_customer_charge':
				$boolIsValid &= $this->valDataIntegrity( $objDatabase, false, $arrmixData );
				$boolIsValid &= $this->valLastPostedOn();
				break;

			case VALIDATE_UPDATE_BULK_UPDATE_SCHEDULED_CHARGES : {

				/** Don't allow to change the frequency of  scheduled_charges if in different ArTriggerTypeId.*/
				$arrobjArTriggers = $arrmixData['ar_triggers'];
				$objScheduledCharge             = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargeByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				$objPropertyIntergationDatabase = $this->getOrFetchPropertyIntegrationDatabase( $objDatabase );
				if( true == $boolIsValid
				    && valObj( $objScheduledCharge, 'CScheduledCharge' )
				    && false == valObj( $objPropertyIntergationDatabase, 'CPropertyIntegrationDatabase' )
				    && $this->getArTriggerTypeIdByArTriggerId( $this->getArTriggerId(), $arrobjArTriggers ) != $this->getArTriggerTypeIdByArTriggerId( $objScheduledCharge->getArTriggerId(), $arrobjArTriggers ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', __( 'The frequency type of the scheduled charge cannot be changed.' ) ) );
					$boolIsValid = false;
				}
			}
			case VALIDATE_UPDATE:
				if( true == valObj( $objOriginalScheduledCharge, 'CScheduledCharge' ) && true == valId( $objOriginalScheduledCharge->getId() ) && true == valId( $this->getId() ) && $objOriginalScheduledCharge->getId() == $this->getId() && serialize( $objOriginalScheduledCharge ) != serialize( $this ) )
					$boolIsValid &= $this->validatePermissions( $strAction );

				$boolIsValid &= $this->valDataIntegrity( $objDatabase, false, $arrmixData );
				if( CArTrigger::MONTHLY == $this->m_intArTriggerId ) {
					$boolIsValid &= $this->valGlAccountType( $objDatabase );
					$boolIsValid &= $this->valLastPostedOn();

					if( true == $boolIsValid && true == $boolIsValidateInstallmentArCodeId ) {
						$boolIsValid &= $this->valInstallmentArCodeId( $objDatabase, $arrintDeletedScheduledChargeIds );
					}
				}

				$boolIsValid &= $this->valMilitaryChargeCode( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->validatePermissions( $strAction );
				$boolIsValid &= $this->valDeleteAbility( $objDatabase );
				break;

			default:
				// default case
		}

		return $boolIsValid;
	}

	private function prepareValidationMessageForPermission( $intArTriggerTypeId, $boolIsEditAction = false, $boolIsCredit = false ) : string {

		$arrintOneTimeArTriggerTypeIds = [ CArTriggerType::APPLICATION_OR_LEASING, CArTriggerType::ONE_TIME_LEASE_CHARGES ];
		$arrintConditionalArTriggerTypeIds = [ CArTriggerType::CACHEABLE ];
		$strActionType = $boolIsEditAction ? __( 'edit' ) : __( 'add' );
		$strChargeType = $boolIsCredit ? __( 'credits' ) : __( 'charges' );
		$strScheduledCharge = __( 'recurring' );
		if( true == in_array( $intArTriggerTypeId, $arrintOneTimeArTriggerTypeIds ) ) {
			$strScheduledCharge = __( 'one time' );
		} elseif( true == in_array( $intArTriggerTypeId, $arrintConditionalArTriggerTypeIds ) ) {
			$strScheduledCharge = __( 'conditional' );
		}

		return __( 'You don\'t have permission to {%s, 0} {%s, 1} scheduled {%s, 2}.', [ $strActionType, $strScheduledCharge, $strChargeType ] );
	}

	public function validatePermissions( $strAction ) {

		$boolIsValid = true;
		$arrmixModulePermissions = $this->getModulePermissions();
		$intLeaseIntervalStatusTypeId = $this->getLeaseIntervalStatusTypeId();
		$intArTriggerTypeId = $this->getArTriggerTypeId();

		if( false == valArr( $arrmixModulePermissions ) ) {
			return $boolIsValid;
		}

		$arrmixModulePermissions['is_allowed_add_scheduled_charges']    = $arrmixModulePermissions['RESIDENT_SYSTEM_ADD_SCHEDULED_CHARGES'];
		$arrmixModulePermissions['is_allowed_edit_scheduled_charges']   = $arrmixModulePermissions['RESIDENT_SYSTEM_EDIT_SCHEDULED_CHARGES'];
		$arrmixModulePermissions['is_allowed_delete_scheduled_charges'] = $arrmixModulePermissions['RESIDENT_SYSTEM_DELETE_SCHEDULED_CHARGES'];
		$arrmixModulePermissions['is_allowed_add_scheduled_credits']    = $arrmixModulePermissions['RESIDENT_SYSTEM_ADD_SCHEDULED_CREDITS'];
		$arrmixModulePermissions['is_allowed_edit_scheduled_credits']   = $arrmixModulePermissions['RESIDENT_SYSTEM_EDIT_SCHEDULED_CREDITS'];

		if( in_array( $intLeaseIntervalStatusTypeId, [ CLeaseStatusType::APPLICANT, CLeaseStatusType::CANCELLED ] ) ) {
			$arrmixModulePermissions['is_allowed_add_scheduled_charges']    = $arrmixModulePermissions['LEAD_ADD_SCHEDULED_CHARGES'];
			$arrmixModulePermissions['is_allowed_edit_scheduled_charges']   = $arrmixModulePermissions['LEAD_EDIT_SCHEDULED_CHARGES'];
			$arrmixModulePermissions['is_allowed_delete_scheduled_charges'] = $arrmixModulePermissions['LEAD_DELETE_SCHEDULED_CHARGES_CREDITS'];
			$arrmixModulePermissions['is_allowed_add_scheduled_credits']    = $arrmixModulePermissions['LEAD_ADD_SCHEDULED_CREDITS'];
			$arrmixModulePermissions['is_allowed_edit_scheduled_credits']   = $arrmixModulePermissions['LEAD_EDIT_SCHEDULED_CREDITS'];
		}

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( true == $this->getIsCharge() && false == $arrmixModulePermissions['is_allowed_add_scheduled_charges'] ) {
					$strErrorMessage = $this->prepareValidationMessageForPermission( $intArTriggerTypeId );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( false == $this->getIsCharge() && false == $arrmixModulePermissions['is_allowed_add_scheduled_credits'] ) {
					$strErrorMessage = $this->prepareValidationMessageForPermission( $intArTriggerTypeId, false, true );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
					$boolIsValid &= false;
				}
				break;

			case VALIDATE_UPDATE:
				if( true == $this->getIsCharge() && false == $arrmixModulePermissions['is_allowed_edit_scheduled_charges'] ) {
					$strErrorMessage = $this->prepareValidationMessageForPermission( $intArTriggerTypeId, true );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( false == $this->getIsCharge() && false == $arrmixModulePermissions['is_allowed_edit_scheduled_credits'] ) {
					$strErrorMessage = $this->prepareValidationMessageForPermission( $intArTriggerTypeId, true, true );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
					$boolIsValid &= false;
				}
				break;

			case VALIDATE_DELETE:
				if( false == isset( $arrmixModulePermissions['is_allowed_delete_scheduled_charges'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( "You don't have permission to delete recurring scheduled credits." ) ) );
					$boolIsValid &= false;
				}
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchArCode( $objDatabase ) {
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->m_intArCodeId, $this->getCid(), $objDatabase );
	}

	public function fetchReversedTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchReversedTransactionsByLeaseIdByScheduledChargeIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseIntervalById( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );
	}

	public function fetchLease( CDatabase $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLease( 'SELECT * FROM leases WHERE id = ' . ( int ) $this->getLeaseId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function loadPostMonth( $objDatabase ) {

		$objPropertyGlSetting = CPropertyGlSettings::fetchPropertyGlSettingByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( __( 'Property GL Setting could not be loaded.' ) );
			exit;
		}

		return $objPropertyGlSetting->getArPostMonth();
	}

	public function determineProrateHardCutOff( $boolChargeMoveOutDay, $boolProrateUptoMoveOutDate ) {
// @FIXME - SCE: Need to discuss this logic.
		// Get the date the charges should have posted through
		$intProrateHardCutOff = NULL;
		if( true == valStr( $this->getMoveOutDate() ) ) {

			if( strtotime( $this->getMoveOutDate() ) >= strtotime( $this->getLeaseEndDate() ) ) {
				$intProrateHardCutOff = strtotime( trim( $this->getMoveOutDate() ) );

			} elseif( ( ( date( 'm/Y', strtotime( $this->getMoveOutDate() ) ) == date( 'm/Y', strtotime( $this->getLeaseEndDate() ) ) ) || ( ( strtotime( '+1 month', strtotime( date( 'm/' . $this->getRateIntervalOffset() . '/Y', strtotime( $this->getMoveOutDate() ) ) ) ) > strtotime( $this->getLeaseEndDate() ) ) ) ) ) {
				$intProrateHardCutOff = strtotime( $this->getLeaseEndDate() );

				// If move-out date is prior to lease end date, prorate rent to move out date
				if( strtotime( $this->getMoveOutDate() ) < strtotime( $this->getLeaseEndDate() ) && true == $boolProrateUptoMoveOutDate ) {
					$intProrateHardCutOff = strtotime( $this->getMoveOutDate() );
				}

			} elseif( strtotime( $this->getMoveOutDate() ) < strtotime( $this->getLeaseEndDate() ) ) {

				// If move-out date is prior to lease end date, prorate rent to move out date
				if( true == $boolProrateUptoMoveOutDate ) {
					$intProrateHardCutOff = strtotime( $this->getMoveOutDate() );
				} else {
					$intProrateHardCutOff = strtotime( date( 'm/t/Y', strtotime( $this->getMoveOutDate() ) ) );
				}

			}

			// If we are not charging for the move out date, back it up one day.
			if( false == $boolChargeMoveOutDay && $intProrateHardCutOff == strtotime( $this->getMoveOutDate() ) ) {
				$intProrateHardCutOff = strtotime( '-1 day', $intProrateHardCutOff );
			}

		}

		// We can't post beyond the charge end date.
		$intProrateHardCutOff = ( false == is_null( $intProrateHardCutOff )
		                          && ( false == valStr( $this->getChargeEndDate() )
		                               || '01/01/1970' == date( 'm/d/Y', strtotime( $this->getChargeEndDate() ) )
		                               || $intProrateHardCutOff < strtotime( $this->getChargeEndDate() ) ) ) ? $intProrateHardCutOff : strtotime( $this->getChargeEndDate() );

		return $intProrateHardCutOff;
	}

	public function determineEndsWithMoveOut( $strMoveOutDate ) {

		$strChargeEndDate 	= ( false == is_null( $this->getChargeEndDate() ) ) ? $this->getChargeEndDate() : '1/1/1970';
		$strMoveOutDate	= ( false == is_null( $strMoveOutDate ) ) ? $strMoveOutDate : '1/1/2099';

		if( ( strtotime( $strChargeEndDate ) > strtotime( $strMoveOutDate ) ) ) {
			return false;
		}

		return true;
	}

	public function determineStartsWithLease( $strLeaseStartDate ) {

		if( false == valStr( $strLeaseStartDate ) ) {
			trigger_error( __( 'Lease start date must not be null.' ), E_USER_WARNING );
			return false;
		}

		if( strtotime( $this->getChargeStartDate() ) < strtotime( $strLeaseStartDate ) ) {
			return false;
		}

		return true;
	}

	// We should always be soft deleting scheduled charges if its not an integrated client or needed as such

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsWorkOrderCharge = false ) {
		$boolIsValid = ( true == $this->valDataIntegrity( $objDatabase, $boolIsWorkOrderCharge ) ? true : false );
		// return ( true == $boolIsValid ) ? parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
		$boolIsValid = ( true == $boolIsValid ) ? parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
		// @FIXME: Any reason for not putting this logic in database trigger ? SRS
		// May be the SubsidyCertificationId is not available during the insertion. Post sc insertion will crate bad data in case SubsidyCertificationId is not set.
		if( false == is_null( $this->getSubsidyCertificationScheduledCharge() ) && true == is_null( $this->m_objSubsidyCertificationScheduledCharge->getId() ) && false == is_null( $this->m_objSubsidyCertificationScheduledCharge->getSubsidyCertificationId() ) ) {
			$this->m_objSubsidyCertificationScheduledCharge->setScheduledChargeId( $this->getId() );
			$boolIsValid = ( true == $boolIsValid ) ? $this->m_objSubsidyCertificationScheduledCharge->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = ( true == $this->valDataIntegrity( $objDatabase ) ? true : false );
		return ( true == $boolIsValid ) ? parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsWorkOrderCharge = false ) {
		if( false == valId( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolIsWorkOrderCharge );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	// This function determines if the scheduled charge is partially or completely un-posted and also not deleted. This function currently works only for recurring charges, we will enhance it for other types of scheduled charges also in near future

	public function isActive() {
		if( false == valStr( $this->getDeletedOn() )
		    && ( false == valStr( $this->getChargeEndDate() )
		         || false == valStr( $this->getPostedThroughDate() )
		         || new DateTime( $this->getPostedThroughDate() ) != new DateTime( $this->getChargeEndDate() ) ) ) {
			return true;
		}

		return false;
	}

    public function isEffectiveOn( $strDate ) {
        if( false == valStr( $this->getDeletedOn() )
            && new DateTime( $strDate ) >= new DateTime( $this->getChargeStartDate() )
            && ( false == valStr( $this->getChargeEndDate() )
                || new DateTime( $strDate ) <= new DateTime( $this->getChargeEndDate() ) ) ) {
            return true;
        }

        return false;
    }

	public function flattenIds( $arrintIds ) {
		if ( false == valArr( $arrintIds ) ) return '';
		return implode( ',', $arrintIds );
	}

	public function expandIds( $strIds ) {
		return array_filter( explode( ',', $strIds ) );
	}

	public function toArray() {
		return array_merge( parent::toArray(), [ 'is_charge' => $this->getIsCharge(), 'subsidy_certification_scheduled_charge' => $this->getSubsidyCertificationScheduledCharge() ] );
	}

	/**
	 * @return \CScheduledCharge
	 */
	public function createMTMScheduledCharge() : \CScheduledCharge {

		$objScheduledCharge = new \CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setParentScheduledChargeId( $this->getId() );
		$objScheduledCharge->setScheduledChargeLogTypeId( \CScheduledChargeLogType::MONTH_TO_MONTH_LEASE_TRUNCATION_INSERT );
		$objScheduledCharge->setLeaseId( $this->getLeaseId() );
		$objScheduledCharge->setCustomerId( $this->getCustomerId() );
		$objScheduledCharge->setArTriggerId( $this->getArTriggerId() );
		$objScheduledCharge->setArCascadeId( $this->getArCascadeId() );
		$objScheduledCharge->setArCascadeReferenceId( $this->getArCascadeReferenceId() );
		$objScheduledCharge->setArOriginId( $this->getArOriginId() );
		$objScheduledCharge->setArOriginReferenceId( $this->getArOriginReferenceId() );
		$objScheduledCharge->setArOriginObjectId( $this->getArOriginObjectId() );
		$objScheduledCharge->setLeaseTermMonths( $this->getLeaseTermMonths() );
		$objScheduledCharge->setArFormulaId( $this->getArFormulaId() );
		$objScheduledCharge->setArFormulaReferenceId( $this->getArFormulaReferenceId() );
		$objScheduledCharge->setChargePercent( $this->getChargePercent() );
		$objScheduledCharge->setChargeMultiplierAmount( $this->getChargeMultiplierAmount() );
		$objScheduledCharge->setRateIntervalOffset( $this->getRateIntervalOffset() );
		$objScheduledCharge->setRateIntervalStart( $this->getRateIntervalStart() ?? 1 );
		$objScheduledCharge->setRateIntervalOccurances( $this->getRateIntervalOccurances() ?? 1 );
		$objScheduledCharge->setInternalMemo( $this->getInternalMemo() );
		$objScheduledCharge->setStartsWithLease( true );
		$objScheduledCharge->setDeletedOn( $this->getDeleteOn() );
		$objScheduledCharge->setDeletedBy( $this->getDeleteBy() );

		return $objScheduledCharge;
	}

	private function getArTriggerTypeIdByArTriggerId( $intArTriggerId, $arrobjArTriggers ) {
		/** @var \CArTrigger $objArTrigger */
		foreach( $arrobjArTriggers as $objArTrigger ) {
			if( $objArTrigger->getId() == $intArTriggerId ) {
				return $objArTrigger->getArTriggerTypeId();
			}
		}
		return null;
	}

}