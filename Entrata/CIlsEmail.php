<?php

class CIlsEmail extends CBaseIlsEmail {

	protected $m_strContent;
	protected $m_arrobjIlsEmailErrors;

    public function __construct() {
        parent::__construct();

        $this->m_strContent = NULL;
        $this->m_arrobjIlsEmailErrors = NULL;

        return;
    }

    /**
     * Create Functions
     */

    public function createIlsEmailError() {

    	$objIlsEmailError = new CIlsEmailError();
    	$objIlsEmailError->setIlsEmailId( $this->getId() );
    	$objIlsEmailError->setCid( $this->getCid() );
    	$objIlsEmailError->setErrorDate( 'NOW()' );

    	return $objIlsEmailError;
    }

    /**
     * Fetch Functions
     */

    public function fetchProperty( $objDatabase ) {

		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	 public function fetchLeadSource( $objDatabase ) {

		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdByCid( $this->getLeadSourceId(), $this->getCid(), $objDatabase );
	 }

    /**
     * Get Functions
     */

	public function getContent( $boolIsGetSecureContent = false ) {

	 	if( true == isset( $this->m_strContent ) && true == valStr( $this->m_strContent ) ) {
    		if( true == $boolIsGetSecureContent ) {
    			return strip_tags( $this->m_strContent, '<DOCTYPE><html><head><title><p><body><div><b><i><u><table><tr><td><h1><h2><strong><\n><br>' );
    		}
    		return $this->m_strContent;

    	} else {

    		if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_html.txt' ) && 0 < filesize( $this->buildStoragePath() . $this->getId() . '_html.txt' ) ) {
				$strResource = fopen( $this->buildStoragePath() . $this->getId() . '_html.txt', 'r' );
				$this->m_strContent = fread( $strResource, filesize( $this->buildStoragePath() . $this->getId() . '_html.txt' ) );
				fclose( $strResource );

				if( true == $boolIsGetSecureContent ) {
					return strip_tags( $this->m_strContent, '<DOCTYPE><html><head><title><p><body><div><b><i><u><table><tr><td><h1><h2><strong><\n><br>' );
				}

		        return $this->m_strContent;
    		} else {
    			return NULL;
    		}
		}
	}

    public function getIlsEmailErrors() {
    	return $this->m_arrobjIlsEmailErrors;
    }

    /**
     * Set Functions
     */

    public function setContent( $strContent ) {
    	$this->m_strContent = $strContent;
    }

    public function setIlsEmailErrors( $arrobjIlsEmailErrors ) {
    	$this->m_arrobjIlsEmailErrors = $arrobjIlsEmailErrors;
    }

    /**
     * Add Functions
     */

    public function addIlsEmailError( $objIlsEmailError ) {
    	$this->m_arrobjIlsEmailErrors[$objIlsEmailError->getId()] = $objIlsEmailError;
    }

    /**
     * Validation Functions
     */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function buildStoragePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_ILS_EMAIL ) . date( 'Y', strtotime( $this->getCreatedOn() ) ) . '/' . date( 'm', strtotime( $this->getCreatedOn() ) ) . '/' . date( 'd', strtotime( $this->getCreatedOn() ) ) . '/';
    }

    /**
     * Database Functions
     */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;

		$this->m_strFilePath = date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';
		$this->m_strFileName = $this->getId() . '_html.txt';

		$strPath = getMountsPath( $this->getCid(), PATH_MOUNTS_ILS_EMAIL ) . $this->m_strFilePath;

		CFileIo::recursiveMakeDir( $strPath );

		// Now make the two files for each Integration result.
		$strHtmlFileName = $strPath . $this->getId() . '_html.txt';

		if( !$resHandle = CFileIo::filePutContents( $strHtmlFileName, $this->getContent() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Error while creating file for ID : ' . $this->getId() ) );
			return false;
	    }
		if( 'development' != CONFIG_ENVIRONMENT ) {
			chown( $strHtmlFileName, getApacheUser() );
		}
	    if( false == $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;

		return true;
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsSuccess = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolIsSuccess && true == file_exists( $this->buildStoragePath() . $this->getId() . '_html.txt' ) ) {
			unlink( $this->buildStoragePath() . $this->getId() . '_html.txt' );
		}

        return true;
    }

}
?>