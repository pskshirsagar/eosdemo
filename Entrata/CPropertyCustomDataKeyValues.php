<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCustomDataKeyValues
 * Do not add any new functions to this class.
 */

class CPropertyCustomDataKeyValues extends CBasePropertyCustomDataKeyValues {

	public static function fetchPropertyCustomDataKeyValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomDataKeyValue( sprintf( 'SELECT * FROM property_custom_data_key_values WHERE id = %d AND cid = %d ORDER BY key', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

}
?>