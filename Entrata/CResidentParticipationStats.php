<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentParticipationStats
 * Do not add any new functions to this class.
 */

class CResidentParticipationStats extends CBaseResidentParticipationStats {

	public static function fetchResidentParticipationStatsByCidsByDate( $arrintCids, $strDate, $objDatabase ) {

		$strSql = 'SELECT *
					 FROM resident_participation_stats
					WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' )
					  AND updated_on::date = \'' . $strDate . '\'::date';

		return self::fetchResidentParticipationStats( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

}
?>