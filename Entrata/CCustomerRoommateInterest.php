<?php

class CCustomerRoommateInterest extends CBaseCustomerRoommateInterest {

	protected $m_strCustomerRoommateInterestQuestion;
	protected $m_strCustomerRoommateInterestAnswer;

	protected $m_strRoommateInterestOptionSystemCode;
	protected $m_strRoommateInterestSystemCode;
	protected $m_strRoommateInterestCategoryName;

	protected $m_intCustomerRoommateInterestIsRequired;
	protected $m_intPropertyUnitId;
	protected $m_intRoommateInterestInputTypeId;
	protected $m_intMatchingWeight;
	protected $m_intLeaseId;

	protected $m_objCustomerRoommateInterest;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestId() {

        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestOptionId() {
        $boolIsValid = true;

        if( ( ( CInputType::LONG_DATE == $this->getRoommateInterestInputTypeId() || CInputType::TEXT == $this->getRoommateInterestInputTypeId() ) && true == $this->getCustomerRoommateInterestIsRequired() && false == valStr( $this->getWrittenResponse() ) ) || ( ( CInputType::SINGLE_SELECT == $this->getRoommateInterestInputTypeId() || CInputType::MULTI_SELECT == $this->getRoommateInterestInputTypeId() ) && true == $this->getCustomerRoommateInterestIsRequired() && true == is_null( $this->getRoommateInterestOptionId() ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'roommate_interest_option_id', __( 'Please select option' ) ) );
        }
        return $boolIsValid;
    }

    public function valPreferenceDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWrittenResponse() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImportanceRating() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valRoommateInterestOptionId();
            	break;

            case VALIDATE_DELETE:
            	break;

           	default:
    			$boolIsValid = true;
    		    break;
        }

        return $boolIsValid;
    }

    public function setCustomerRoommateInterestQuestion( $strCustomerRoommateInterestQuestion ) {
    	$this->m_strCustomerRoommateInterestQuestion = CStrings::strTrimDef( $strCustomerRoommateInterestQuestion, 2000, NULL, true );
    }

    public function getCustomerRoommateInterestQuestion() {
    	return $this->m_strCustomerRoommateInterestQuestion;
    }

    public function sqlCustomerRoommateInterestQuestion() {
    	return  ( true == isset( $this->m_strCustomerRoommateInterestQuestion ) ) ? '\'' . addslashes( $this->m_strCustomerRoommateInterestQuestion ) . '\'' : 'NULL';
    }

    public function getCustomerRoommateInterestAnswer() {
    	return $this->m_strCustomerRoommateInterestAnswer;
    }

    public function setCustomerRoommateInterestAnswer( $strCustomerRoommateInterestAnswer ) {
    	$this->m_strCustomerRoommateInterestAnswer = CStrings::strTrimDef( $strCustomerRoommateInterestAnswer, 2000, NULL, true );
    }

    public function sqlCustomerRoommateInterestAnswer() {
    	return  ( true == isset( $this->m_strCustomerRoommateInterestAnswer ) ) ? '\'' . addslashes( $this->m_strCustomerRoommateInterestAnswer ) . '\'' : 'NULL';
    }

    public function getCustomerRoommateInterestIsRequired() {
    	return $this->m_intCustomerRoommateInterestIsRequired;
    }

    public function setCustomerRoommateInterestIsRequired( $intCustomerRoommateInterestIsRequired ) {
    	$this->m_intCustomerRoommateInterestIsRequired = CStrings::strTrimDef( $intCustomerRoommateInterestIsRequired, 2000, NULL, true );
    }

    public function setPropertyUnitId( $intPropertyUnitId ) {
    	$this->m_intPropertyUnitId = $intPropertyUnitId;
    }

    public function getPropertyUnitId() {
    	return $this->m_intPropertyUnitId;
    }

    public function sqlPropertyUnitId() {
    	return ( true == isset( $this->m_intPropertyUnitId ) ) ? '\'' . addslashes( $this->m_intPropertyUnitId ) . '\'' : 'NULL';
    }

    public function setRoommateInterestOptionSystemCode( $strRoommateInterestOptionSystemCode ) {
    	$this->m_strRoommateInterestOptionSystemCode = $strRoommateInterestOptionSystemCode;
    }

    public function getRoommateInterestOptionSystemCode() {
    	return $this->m_strRoommateInterestOptionSystemCode;
    }

    public function sqlRoommateInterestOptionSystemCode() {
    	return ( true == isset( $this->m_strRoommateInterestOptionSystemCode ) ) ? '\'' . addslashes( $this->m_strRoommateInterestOptionSystemCode ) . '\'' : 'NULL';
    }

    public function setRoommateInterestSystemCode( $strRoommateInterestSystemCode ) {
		$this->m_strRoommateInterestSystemCode = $strRoommateInterestSystemCode;
    }

    public function getRoommateInterestSystemCode() {
    	return $this->m_strRoommateInterestSystemCode;
    }

    public function sqlRoommateInterestSystemCode() {
    	return ( true == isset( $this->m_strRoommateInterestSystemCode ) ) ? '\'' . addslashes( $this->m_strRoommateInterestSystemCode ) . '\'' : 'NULL';
    }

    public function setRoommateInterestCategoryName( $strRoommateInterestCategoryName ) {
    	$this->m_strRoommateInterestCategoryName = $strRoommateInterestCategoryName;
    }

    public function getRoommateInterestCategoryName() {
    	return $this->m_strRoommateInterestCategoryName;
    }

    public function sqlRoommateInterestCategoryName() {
    	return ( true == isset( $this->m_strRoommateInterestCategoryName ) ) ? '\'' . addslashes( $this->m_strRoommateInterestCategoryName ) . '\'' : 'NULL';
    }

    public function setRoommateInterestInputTypeId( $intInputTypeId ) {
    	$this->m_intRoommateInterestInputTypeId = $intInputTypeId;
    }

    public function getRoommateInterestInputTypeId() {
    	return $this->m_intRoommateInterestInputTypeId;
    }

    public function sqlRoommateInterestInputTypeId() {
    	return ( true == isset( $this->m_intRoommateInterestInputTypeId ) ) ? '\'' . addslashes( $this->m_intRoommateInterestInputTypeId ) . '\'' : 'NULL';
    }

    public function setMatchingWeight( $intMatchingWeight ) {
    	$this->m_intMatchingWeight = $intMatchingWeight;
    }

    public function getMatchingWeight() {
    	return $this->m_intMatchingWeight;
    }

    public function sqlMatchingWeight() {
    	return ( true == isset( $this->m_intMatchingWeight ) ) ? '\'' . addslashes( $this->m_intMatchingWeight ) . '\'' : 'NULL';
    }

    public function setLeaseId( $intLeaseId ) {
    	$this->m_intLeaseId = $intLeaseId;
    }

    public function getLeaseId() {
    	return $this->m_intLeaseId;
    }

    public function sqlLeaseId() {
    	return ( true == isset( $this->m_intLeaseId ) ) ? '\'' . addslashes( $this->m_intLeaseId ) . '\'' : 'NULL';
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrValues['question'] ) && $boolDirectSet ) {
    		 $this->m_strCustomerRoommateInterestQuestion = trim( stripcslashes( $arrValues['question'] ) );
    	} elseif( isset( $arrValues['question'] ) ) {
    		$this->setCustomerRoommateInterestQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
    	}

    	if( isset( $arrValues['answer'] ) && $boolDirectSet ) {
    		$this->m_strCustomerRoommateInterestAnswer = trim( stripcslashes( $arrValues['answer'] ) );
    	} elseif( isset( $arrValues['answer'] ) ) {
    		$this->setCustomerRoommateInterestAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
    	}

    	if( isset( $arrValues['is_required'] ) && $boolDirectSet ) {
    		$this->m_intCustomerRoommateInterestIsRequired = trim( $arrValues['is_required'] );
    	} elseif( isset( $arrValues['is_required'] ) ) {
    		$this->setCustomerRoommateInterestIsRequired( $arrValues['is_required'] );
    	}

    	if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) {
    		$this->m_intPropertyUnitId = trim( $arrValues['property_unit_id'] );
    	} elseif( isset( $arrValues['property_unit_id'] ) ) {
    		$this->setPropertyUnitId( $arrValues['property_unit_id'] );
    	}

    	if( isset( $arrValues['roommate_interest_option_system_code'] ) && $boolDirectSet ) {
    		$this->m_strRoommateInterestOptionSystemCode = trim( $arrValues['roommate_interest_option_system_code'] );
    	} elseif( isset( $arrValues['roommate_interest_option_system_code'] ) ) {
    		$this->setPropertyUnitId( $arrValues['roommate_interest_option_system_code'] );
    	}

    	if( isset( $arrValues['roommate_interest_system_code'] ) && $boolDirectSet ) {
    		$this->m_strRoommateInterestSystemCode = trim( $arrValues['roommate_interest_system_code'] );
    	} elseif( isset( $arrValues['roommate_interest_system_code'] ) ) {
    		$this->setRoommateInterestSystemCode( $arrValues['roommate_interest_system_code'] );
    	}

    	if( isset( $arrValues['roommate_interest_category_name'] ) && $boolDirectSet ) {
    		$this->m_strRoommateInterestCategoryName = trim( $arrValues['roommate_interest_category_name'] );
    	} elseif( isset( $arrValues['roommate_interest_category_name'] ) ) {
    		$this->setRoommateInterestCategoryName( $arrValues['roommate_interest_category_name'] );
    	}

    	if( isset( $arrValues['roommate_interest_input_type_id'] ) && $boolDirectSet ) {
    		$this->m_intRoommateInterestInputTypeId = trim( $arrValues['roommate_interest_input_type_id'] );
    	} elseif( isset( $arrValues['roommate_interest_input_type_id'] ) ) {
    		$this->setRoommateInterestInputTypeId( $arrValues['roommate_interest_input_type_id'] );
    	}

    	if( isset( $arrValues['matching_weight'] ) && $boolDirectSet ) {
    		$this->m_intMatchingWeight = trim( $arrValues['matching_weight'] );
    	} elseif( isset( $arrValues['matching_weight'] ) ) {
    		$this->setMatchingWeight( $arrValues['matching_weight'] );
    	}

    	if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) {
    		$this->m_intLeaseId = trim( $arrValues['lease_id'] );
    	} elseif( isset( $arrValues['lease_id'] ) ) {
    		$this->setLeaseId( $arrValues['lease_id'] );
    	}
    }

}
?>