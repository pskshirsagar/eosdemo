<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationServiceLogs
 * Do not add any new functions to this class.
 */

class CIntegrationServiceLogs extends CBaseIntegrationServiceLogs {

	public static function fetchIntegrationServiceLogByIntegrationDatabaseIdByIntegrationServiceIdByLogDateByCid( $intIntegrationDatabaseId, $intIntegrationServiceId, $intCid, $objDatabase ) {
		 $strSql = 'SELECT
						*
					FROM
						integration_service_logs
					WHERE
						integration_database_id 		= ' . ( int ) $intIntegrationDatabaseId . '
						AND cid  	= ' . ( int ) $intCid . '
						AND integration_service_id 	= ' . ( int ) $intIntegrationServiceId . '
						AND log_date = \'' . date( 'Y-m-d' ) . '\' LIMIT 1';

		return self::fetchIntegrationServiceLog( $strSql, $objDatabase );
	}

	public static function fetchIntegrationServiceLogsByCidWithIntegrationServiceLogsFilter( $intCid, $arrmixIntegrationServiceLogsFilter, $objDatabase ) {

		$strSql = 'SELECT
						isl.*,
						id.database_name as integration_database_name,
						ict.name as integration_client_type_name,
						iss.name as integration_service_name
					FROM
						integration_service_logs isl
						join integration_databases id ON ( id.cid = isl.cid AND id.id = isl.integration_database_id ),
						integration_client_types ict,
						integration_services iss
					WHERE
						id.integration_client_type_id=ict.id
						AND iss.id= isl.integration_service_id
						AND isl.cid  	= ' . ( int ) $intCid . '
						AND isl.integration_database_id  	= id.id ';

		if( true == ( isset( $arrmixIntegrationServiceLogsFilter['integration_Client_types'] ) && valArr( $arrmixIntegrationServiceLogsFilter['integration_Client_types'] ) ) ) {

			$strSql .= ' AND id.integration_client_type_id IN ( ' . implode( ',', $arrmixIntegrationServiceLogsFilter['integration_Client_types'] ) . ')';
		}

		if( true == ( isset( $arrmixIntegrationServiceLogsFilter['integration_services_type_id'] ) && valArr( $arrmixIntegrationServiceLogsFilter['integration_services_type_id'] ) ) ) {

			$strSql .= ' AND isl.integration_service_id IN ( ' . implode( ',', $arrmixIntegrationServiceLogsFilter['integration_services_type_id'] ) . ')';
		}

		if( true == isset( $arrmixIntegrationServiceLogsFilter['start_date'] ) ) {

			$strSql .= ' AND isl.log_date >= \'' . date( 'Y-m-d', strtotime( $arrmixIntegrationServiceLogsFilter['start_date'] ) ) . '\'';
		}

		if( true == isset( $arrmixIntegrationServiceLogsFilter['end_date'] ) ) {

			$strSql .= ' AND isl.log_date <= \'' . date( 'Y-m-d', strtotime( $arrmixIntegrationServiceLogsFilter['end_date'] ) ) . '\'';
		}

		return fetchData( $strSql, $objDatabase );
	}

}
?>