<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantPhoneNumbers
 * Do not add any new functions to this class.
 */

class CApplicantPhoneNumbers extends CBaseApplicantPhoneNumbers {

	public static function fetchApplicantPhoneNumberByApplicantIdByPhoneNumberTypeIdByCid( $intApplicantId, $intPhoneNumberTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM applicant_phone_numbers WHERE applicant_id =' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' LIMIT 1';
		return self::fetchApplicantPhoneNumber( $strSql, $objDatabase );
	}
}
?>