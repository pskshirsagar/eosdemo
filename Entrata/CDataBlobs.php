<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see        \Psi\Eos\Entrata\CDataBlobs
 * Do not add any new functions to this class.
 */

class CDataBlobs extends CBaseDataBlobs {

	public static function rebuildDataBlobsByCid( $intCid, $objDatabase ) {
		self::rebuildPropertiesDataBlobByCid( $intCid, $objDatabase );
		self::rebuildLeadsDataBlobByCid( $intCid, $objDatabase );
		self::rebuildLeaseCustomersDataBlobByCid( $intCid, $objDatabase );
	}

	public static function rebuildPropertiesDataBlobByCid( $intCid, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND data_blob_type_id = ' . CDataBlobType::PROPERTIES . ';

					INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						p.cid,
						' . CDataBlobType::PROPERTIES . ' AS data_blob_type_id,
						p.id as property_id,
						NULL as lease_customer_id,
						NULL as customer_id,
						NULL as lease_id,
						NULL as employee_id,
						NULL as applicant_application_id,
						NULL as applicant_id,
						NULL as application_id,
						NULL as ap_payee_id,
						p.property_name as data_field1,
						NULL as data_field2,
						NULL as data_field3,
						1 as order_num,
						p.property_name as blob,
						p.updated_on as updated_on
					FROM
						properties p
					WHERE p.cid = ' . ( int ) $intCid . ';';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		}
	}

	public static function rebuildLeadsDataBlobByCid( $intCid, $objDatabase ) {

		$strSql = 'DELETE
					FROM
						data_blobs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND data_blob_type_id = ' . CDataBlobType::LEADS . ';

					INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						aa.cid,
						' . CDataBlobType::LEADS . ' AS data_blob_type_id,
						ca.property_id as property_id,
						NULL as lease_customer_id,
						NULL as customer_id,
						NULL as lease_id,
						NULL as employee_id,
						aa.id as applicant_application_id,
						a.id as applicant_id,
						ca.id as application_id,
						NULL as ap_payee_id,
						COALESCE ( TRIM ( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \', COALESCE (  a.name_last_matronymic, \'\' ) ) ) ) as data_field1,
						NULL AS data_field2,
						NULL AS data_field3,
						ca.application_stage_id as order_num,
						( COALESCE ( a.name_first, \'\' ) || \'~..$\' ||
						TRIM( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \', COALESCE ( a.name_last_matronymic, \'\' ) ) ) || \'~..$\' ||
						COALESCE ( ca.property_name, \'\' ) || \'~..$\' ||
						COALESCE ( ca.building_name, \'\' ) || \'~..$\' ||
						COALESCE ( ca.unit_number_cache, \'\' ) || \'~..$\' ||
						COALESCE ( ass.stage_name || \' \' || ass.status_name , \'\' )),
						GREATEST( aa.updated_on, ca.updated_on, a.updated_on ) as updated_on
					FROM
						applicant_applications aa
						JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
						JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
						JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
					WHERE aa.deleted_by IS NULL AND aa.cid =  ' . ( int ) $intCid . '
						AND NOT EXISTS ( SELECT cid FROM data_blobs where cid = aa.cid AND applicant_application_id = aa.id AND application_id = ca.id );';

		$arrmixResult = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		}
	}

	public static function rebuildLeaseCustomersDataBlobByCid( $intCid, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . ';
						
						WITH primary_phone_number AS (

							SELECT 
							    customer_id, cid, array_to_string( array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    is_primary IS TRUE 
							    AND deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							GROUP BY customer_id, cid
						),
						additional_phone_number AS (
							SELECT 
							    customer_id, cid, array_to_string(array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    is_primary IS FALSE 
							    AND deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							GROUP BY customer_id, cid
						)

					INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						lc.cid,
						' . CDataBlobType::LEASE_CUSTOMERS . ' AS data_blob_type_id,
						l.property_id as property_id,
						lc.id as lease_customer_id,
						lc.customer_id as customer_id,
						lc.lease_id as lease_id,
						NULL as employee_id,
						NULL as applicant_application_id,
						NULL as applicant_id,
						NULL as application_id,
						NULL as ap_payee_id,
						TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) as data_field1,
						l.unit_number_cache as data_field2,
						COALESCE ( l.building_name, \'\' ) as data_field3,
						CASE
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ' )
							THEN 1
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' )
							THEN 2
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
							THEN 3
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . ' )
							THEN 4
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::PAST . ' )
							THEN 5
							ELSE 6
						END::integer as order_num,
						( COALESCE ( c.name_first, c.company_name, \'\' ) || \'~..$\' ||
							COALESCE ( TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) ) || \'~..$\' ||
							COALESCE ( l.property_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.building_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.unit_number_cache, \'\' ) || \'~..$\' || \'\' || \'~..$\' ||
							COALESCE ( lst.name, \'\' ) || \'~..$\' ||
							COALESCE ( cps.username, c.email_address, \'\' ) || \'~..$\' ||
							COALESCE ( apn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( ppn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( ppn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( c.remote_primary_key, \'\' ) || \'~..$\' ||
							COALESCE ( c.company_name, \'\' ) ) as blob,
							GREATEST( lc.updated_on, l.updated_on, c.updated_on, p.updated_on, cps.updated_on ) as updated_on
					FROM
						lease_customers lc
						JOIN cached_leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
						JOIN lease_status_types lst ON ( lst.id = lc.lease_status_type_id )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
						JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
						LEFT JOIN customer_portal_settings cps ON ( c.id = cps.customer_id AND c.cid = cps.cid )
						LEFT JOIN primary_phone_number ppn ON (ppn.cid = c.cid AND ppn.customer_id = c.id)
						LEFT JOIN additional_phone_number apn ON (apn.cid = c.cid AND apn.customer_id = c.id)
					WHERE lc.cid = ' . ( int ) $intCid . '
						AND NOT EXISTS ( SELECT cid FROM data_blobs WHERE cid = lc.cid AND lease_customer_id = lc.id );';
		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		}
	}

	public static function rebuildLeaseCustomersDataBlobByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . ';
						
						WITH primary_phone_number AS (

							SELECT 
							    customer_id, cid, array_to_string( array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    is_primary IS TRUE 
							    AND deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							    AND customer_id = ' . ( int ) $intCustomerId . '
							GROUP BY customer_id, cid
						),
						additional_phone_number AS (
							SELECT 
							    customer_id, cid, array_to_string(array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    is_primary IS FALSE 
							    AND deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							    AND customer_id = ' . ( int ) $intCustomerId . '
							GROUP BY customer_id, cid
						)

					INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						lc.cid,
						' . CDataBlobType::LEASE_CUSTOMERS . ' AS data_blob_type_id,
						l.property_id as property_id,
						lc.id as lease_customer_id,
						lc.customer_id as customer_id,
						lc.lease_id as lease_id,
						NULL as employee_id,
						NULL as applicant_application_id,
						NULL as applicant_id,
						NULL as application_id,
						NULL as ap_payee_id,
						TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) as data_field1,
						l.unit_number_cache as data_field2,
						COALESCE ( l.building_name, \'\' ) as data_field3,
						CASE
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ' )
							THEN 1
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' )
							THEN 2
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
							THEN 3
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . ' )
							THEN 4
							WHEN ( lc.lease_status_type_id = ' . CLeaseStatusType::PAST . ' )
							THEN 5
							ELSE 6
						END::integer as order_num,
						( COALESCE ( c.name_first, c.company_name, \'\' ) || \'~..$\' ||
							COALESCE ( TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) ) || \'~..$\' ||
							COALESCE ( l.property_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.building_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.unit_number_cache, \'\' ) || \'~..$\' || \'\' || \'~..$\' ||
							COALESCE ( lst.name, \'\' ) || \'~..$\' ||
							COALESCE ( cps.username, c.email_address, \'\' ) || \'~..$\' ||
							COALESCE ( apn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( ppn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( ppn.phone_number, \'\' ) || \'~..$\' ||
							COALESCE ( c.remote_primary_key, \'\' ) || \'~..$\' ||
							COALESCE ( c.company_name, \'\' ) ) as blob,
							GREATEST( lc.updated_on, l.updated_on, c.updated_on, p.updated_on, cps.updated_on ) as updated_on
					FROM
						lease_customers lc
						JOIN cached_leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
						JOIN lease_status_types lst ON ( lst.id = lc.lease_status_type_id )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
						JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
						LEFT JOIN customer_portal_settings cps ON ( c.id = cps.customer_id AND c.cid = cps.cid )
						LEFT JOIN primary_phone_number ppn ON (ppn.cid = c.cid AND ppn.customer_id = c.id)
						LEFT JOIN additional_phone_number apn ON (apn.cid = c.cid AND apn.customer_id = c.id)
					WHERE lc.cid = ' . ( int ) $intCid . '
						AND c.id = ' . ( int ) $intCustomerId . '
						AND NOT EXISTS ( SELECT cid FROM data_blobs WHERE cid = lc.cid AND lease_customer_id = lc.id );';
		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		}
	}

	public static function fetchDataBlobsByDataBlobTypeIdBySearchDataByPropertyIdByCustomerIdByCid( $intDataBlobTypeId, $strSearchData = NULL, $intPropertyId = NULL, $intCustomerId = NULL, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strWHere = '';
		if( false == is_null( $strSearchData ) ) {
			$strWHere .= ' AND db.blob NOT ILIKE \'%$Cancelled~%\'
							AND db.blob NOT ILIKE \'%$Past~%\'
							AND db.blob NOT ILIKE \'%$Evicted~%\'
							AND db.blob ILIKE ( \'%' . addslashes( $strSearchData ) . '%\' )';
		}

		if( true == valId( $intPropertyId ) ) {
			$strWHere .= ' AND db.property_id IN ( ' . ( int ) $intPropertyId . ' )';
		}

		if( true == valId( $intCustomerId ) ) {
			$strWHere .= ' AND db.customer_id = ' . ( int ) $intCustomerId;
		}

		$strSql = 'SELECT
			 				db.*
						FROM
							data_blobs db
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.data_blob_type_id = ' . ( int ) $intDataBlobTypeId . ' ' . $strWHere . '
						ORDER BY
							db.order_num,
							lower( db.data_field1 )
							LIMIT 100';

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteDataBlobForLeaseCustomersByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs db
					WHERE
						db.cid = ' . ( int ) $intCid . '
						AND db.property_id = ' . ( int ) $intPropertyId . '
						AND data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . '
						AND ( NOT EXISTS ( SELECT cid FROM lease_customers WHERE cid = db.cid AND property_id = db.property_id AND id = db.lease_customer_id ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function updateDataBlobForLeaseCustomersByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = '	WITH phone_numbers AS (
							SELECT 
							    customer_id, cid, array_to_string( array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							GROUP BY customer_id, cid
						)
					UPDATE
						data_blobs
					SET
						cid = lc.cid,
						data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . ',
						property_id = l.property_id,
						lease_customer_id = lc.id,
						customer_id = lc.customer_id,
						lease_id = lc.lease_id,
						employee_id = NULL,
						applicant_application_id = NULL,
						applicant_id = NULL,
						application_id = NULL,
						ap_payee_id = NULL,
						data_field1 = TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ),
						data_field2 = l.unit_number_cache,
						data_field3 = COALESCE ( l.building_name, \'\' ),
						order_num = CASE
								WHEN ( lc.lease_status_type_id = 4 )
								THEN 1
								WHEN ( lc.lease_status_type_id = 5 )
								THEN 2
								WHEN ( lc.lease_status_type_id = 3 )
								THEN 3
								WHEN ( lc.lease_status_type_id = 1 )
								THEN 4
								WHEN ( lc.lease_status_type_id = 6 )
								THEN 5
								ELSE 6
							END::integer,
						blob = 	( COALESCE ( c.name_first, c.company_name, \'\' ) || \'~..$\' ||
								COALESCE ( TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) ) || \'~..$\' ||
								COALESCE ( l.property_name, \'\' ) || \'~..$\' ||
								COALESCE ( l.building_name, \'\' ) || \'~..$\' ||
								COALESCE ( l.unit_number_cache, \'\' ) || \'~..$\' || \'\' || \'~..$\' ||
								COALESCE ( lst.name, \'\' ) || \'~..$\' ||
								COALESCE ( cps.username, c.email_address, \'\' ) || \'~..$\' ||
								COALESCE ( pn.phone_number, \'\' ) || \'~..$\' ||
								COALESCE ( c.remote_primary_key, \'\' ) || \'~..$\' ||
							    COALESCE ( c.company_name, \'\' ) || \'~..$\' ||
						        COALESCE ( c.preferred_name, \'\' ) || \'~..$\' ||
						        COALESCE ( c.details->\'doing_business_as\'->>\'name\', \'\' ) || \'~..$\' ||
								COALESCE ( c.alt_name_first, \'\' ) || \' \' || COALESCE ( c.alt_name_middle, \'\' ) || \' \' || COALESCE ( c.alt_name_last, \'\' ) ),
						updated_on = GREATEST( lc.updated_on, l.updated_on, c.updated_on, p.updated_on, cps.updated_on )
					FROM
						lease_customers lc
						JOIN cached_leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
						JOIN lease_status_types lst ON ( lst.id = lc.lease_status_type_id )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
						JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
						LEFT JOIN customer_portal_settings cps ON ( c.id = cps.customer_id AND c.cid = cps.cid )
						LEFT JOIN phone_numbers pn ON (pn.cid = c.cid AND pn.customer_id = c.id)
					WHERE
						lc.cid = ' . ( int ) $intCid . '
						AND lc.property_id = ' . ( int ) $intPropertyId . '
						AND data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . '
						AND data_blobs.cid = lc.cid and data_blobs.lease_customer_id = lc.id
						AND ( data_blobs.updated_on IS NULL OR data_blobs.updated_on < GREATEST( lc.updated_on, l.updated_on, c.updated_on, p.updated_on, cps.updated_on ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function insertDataBlobForLeaseCustomersByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = '
					WITH phone_numbers AS (
							SELECT 
							    customer_id, cid, array_to_string( array_agg( regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) ), \', \') phone_number
							FROM 
							    customer_phone_numbers
							WHERE 
							    deleted_on IS NULL 
							    AND phone_number IS NOT NULL 
							    AND cid = ' . ( int ) $intCid . '
							GROUP BY customer_id, cid
						)
					INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						lc.cid,
						' . CDataBlobType::LEASE_CUSTOMERS . ' AS data_blob_type_id,
						l.property_id as property_id,
						lc.id as lease_customer_id,
						lc.customer_id as customer_id,
						lc.lease_id as lease_id,
						NULL as employee_id,
						NULL as applicant_application_id,
						NULL as applicant_id,
						NULL as application_id,
						NULL as ap_payee_id,
						TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) as data_field1,
						l.unit_number_cache as data_field2,
						COALESCE ( l.building_name, \'\' ) as data_field3,
						CASE
							WHEN ( lc.lease_status_type_id = 4 )
								THEN 1
							WHEN ( lc.lease_status_type_id = 5 )
								THEN 2
							WHEN ( lc.lease_status_type_id = 3 )
								THEN 3
							WHEN ( lc.lease_status_type_id = 1 )
								THEN 4
							WHEN ( lc.lease_status_type_id = 6 )
								THEN 5
							ELSE 6
						END::integer as order_num,
						( COALESCE ( c.name_first, c.company_name, \'\' ) || \'~..$\' ||
							COALESCE ( TRIM( CONCAT ( COALESCE ( c.name_last, \'\' ), \' \', COALESCE ( c.name_last_matronymic, \'\' ) ) ) ) || \'~..$\' ||
							COALESCE ( l.property_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.building_name, \'\' ) || \'~..$\' ||
							COALESCE ( l.unit_number_cache, \'\' ) || \'~..$\' || \'\' || \'~..$\' ||
							COALESCE ( lst.name, \'\' ) || \'~..$\' ||
							COALESCE ( cps.username, c.email_address, \'\' ) || \'~..$\' ||
							COALESCE ( c.secondary_number, \'\' ) || \'~..$\' ||
							COALESCE ( pn.phone_number , \'\' ) || \'~..$\' ||
							COALESCE ( c.remote_primary_key, \'\' ) || \'~..$\' ||
							COALESCE ( c.company_name, \'\' ) || \'~..$\' ||
							COALESCE ( c.preferred_name, \'\' ) || \'~..$\' ||
							COALESCE ( c.details->\'doing_business_as\'->>\'name\', \'\' ) || \'~..$\' ||
							COALESCE ( c.alt_name_first, \'\' ) || \' \' || COALESCE ( c.alt_name_middle, \'\' ) || \' \' || COALESCE ( c.alt_name_last, \'\' ) ),
							GREATEST( lc.updated_on, l.updated_on, c.updated_on, p.updated_on, cps.updated_on ) as updated_on
					FROM
						lease_customers lc
						JOIN cached_leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
						JOIN lease_status_types lst ON ( lst.id = lc.lease_status_type_id )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
						JOIN properties p ON ( p.id = l.property_id AND p.cid = l.cid )
						LEFT JOIN customer_portal_settings cps ON ( c.id = cps.customer_id AND c.cid = cps.cid )
						LEFT JOIN phone_numbers pn ON (pn.cid = c.cid AND pn.customer_id = lc.customer_id)
					WHERE lc.cid = ' . ( int ) $intCid . '
						AND lc.property_id = ' . ( int ) $intPropertyId . '
						AND NOT EXISTS ( SELECT cid FROM data_blobs db2 WHERE db2.cid = lc.cid AND db2.data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . ' AND db2.lease_customer_id = lc.id )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function deleteDataBlobForLeadsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs db
					WHERE
						db.cid = ' . ( int ) $intCid . '
						AND db.cid = ' . ( int ) $intPropertyId . '
						AND data_blob_type_id = ' . CDataBlobType::LEADS . '
						AND ( NOT EXISTS ( SELECT cid FROM applicant_applications WHERE cid = db.cid AND id = db.applicant_application_id AND deleted_by IS NULL ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function updateDataBlobForLeadsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'UPDATE data_blobs
					SET
						cid = aa.cid,
						data_blob_type_id = ' . CDataBlobType::LEADS . ',
						property_id = ca.property_id,
						lease_customer_id = NULL,
						customer_id = NULL,
						lease_id = NULL,
						employee_id = NULL,
						applicant_application_id = aa.id,
						applicant_id = a.id,
						application_id = ca.id,
						ap_payee_id = NULL,
						data_field1 = COALESCE ( TRIM( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \', COALESCE ( a.name_last_matronymic, \'\' ) ) ) ),
						data_field2 = NULL,
						data_field3 = NULL,
						order_num = ca.application_stage_id,
						blob = ( COALESCE ( a.name_first, \'\' ) || \'~..$\' ||
									TRIM( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \',COALESCE ( a.name_last_matronymic, \'\' ) ) ) || \'~..$\' ||
									COALESCE ( ca.property_name, \'\' ) || \'~..$\' ||
									COALESCE ( ca.building_name, \'\' ) || \'~..$\' ||
									COALESCE ( ca.unit_number_cache, \'\' ) || \'~..$\' ||
									COALESCE ( ass.stage_name || \' \' || ass.status_name , \'\' ) || \'~..$\' ||
									COALESCE ( a.preferred_name, \'\' ) || \'~..$\' ||
									COALESCE ( a.alt_name_first, \'\' ) || \' \' || COALESCE ( a.alt_name_middle, \'\' ) || \' \' || COALESCE ( a.alt_name_last, \'\' ) || \'~..$\' ||
									COALESCE ( cc.name_first, \'\' ) || \'~..$\' ||
									COALESCE ( cc.name_last, \'\' ) || \'~..$\' ||
									COALESCE ( a.username, a.email_address, \'\' ) ),
						updated_on = GREATEST( aa.updated_on, ca.updated_on, a.updated_on )
					FROM
						applicant_applications aa
						JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
						JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
						JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
						LEFT JOIN customer_contacts cc ON (cc.cid = ca.cid AND 1 = (ca.details::jsonb->>\'is_representative_primary_contact\')::INTEGER AND cc.id = (ca.details::jsonb->>\'representative_customer_contact_id\')::INTEGER )
					WHERE
						aa.deleted_by IS NULL
						AND aa.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND data_blob_type_id = ' . CDataBlobType::LEADS . '
						AND data_blobs.cid = aa.cid AND data_blobs.applicant_application_id = aa.id
						AND ( data_blobs.updated_on IS NULL OR data_blobs.updated_on < GREATEST( aa.updated_on, ca.updated_on, a.updated_on ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function insertDataBlobForLeadsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'INSERT INTO public.data_blobs (
							cid,
							data_blob_type_id,
							property_id,
							lease_customer_id,
							customer_id,
							lease_id,
							employee_id,
							applicant_application_id,
							applicant_id,
							application_id,
							ap_payee_id,
							data_field1,
							data_field2,
							data_field3,
							order_num,
							blob,
							updated_on
					)
					SELECT
						aa.cid,
						' . CDataBlobType::LEADS . ' AS data_blob_type_id,
						ca.property_id as property_id,
						NULL as lease_customer_id,
						NULL as customer_id,
						NULL as lease_id,
						NULL as employee_id,
						aa.id as applicant_application_id,
						a.id as applicant_id,
						ca.id as application_id,
						NULL as ap_payee_id,
						COALESCE ( TRIM( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \', COALESCE ( a.name_last_matronymic, \'\' ) ) ) ) as data_field1,
						NULL AS data_field2,
						NULL AS data_field3,
						ca.application_stage_id as order_num,
						( COALESCE ( a.name_first, \'\' ) || \'~..$\' ||
							COALESCE ( TRIM( CONCAT ( COALESCE ( a.name_last, \'\' ), \' \', COALESCE ( a.name_last_matronymic, \'\' ) ) ) ) || \'~..$\' ||
							COALESCE ( ca.property_name, \'\' ) || \'~..$\' ||
							COALESCE ( ca.building_name, \'\' ) || \'~..$\' ||
							COALESCE ( ca.unit_number_cache, \'\' ) || \'~..$\' ||
							COALESCE ( ass.stage_name || \' \' || ass.status_name , \'\' ) || \'~..$\' ||
							COALESCE ( a.preferred_name, \'\' ) || \'~..$\' ||
							COALESCE ( a.alt_name_first, \'\' ) || \' \' || COALESCE ( a.alt_name_middle, \'\' ) || \' \' || COALESCE ( a.alt_name_last, \'\' ) || \'~..$\' ||
							COALESCE ( cc.name_first, \'\' ) || \'~..$\' ||
							COALESCE ( cc.name_last, \'\' )  || \'~..$\' ||
							COALESCE ( a.username, a.email_address, \'\' ) ),
						GREATEST( aa.updated_on, ca.updated_on, a.updated_on ) as updated_on
					FROM
							applicant_applications aa
							JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
							JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
							JOIN applicants a ON ( aa.applicant_id = a.id AND aa.cid = a.cid )
							LEFT JOIN customer_contacts cc ON (cc.cid = ca.cid AND 1 = (ca.details::jsonb->>\'is_representative_primary_contact\')::INTEGER AND cc.id = (ca.details::jsonb->>\'representative_customer_contact_id\')::INTEGER )
					WHERE aa.deleted_by IS NULL
							AND aa.cid = ' . ( int ) $intCid . '
							AND ca.property_id = ' . ( int ) $intPropertyId . '
							AND NOT EXISTS ( SELECT db2.cid FROM data_blobs db2 where db2.cid = aa.cid AND db2.data_blob_type_id = ' . CDataBlobType::LEADS . ' AND db2.applicant_application_id = aa.id AND db2.application_id = aa.application_id )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function deleteDataBlobForPropertiesByCid( $intCid, $objDatabase ) {
		$strSql = 'DELETE
					FROM
						data_blobs db
					WHERE
						db.cid = ' . ( int ) $intCid . '
						AND data_blob_type_id = ' . CDataBlobType::PROPERTIES . '
						AND ( NOT EXISTS ( SELECT cid FROM properties WHERE cid = db.cid AND id = db.property_id ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}

	}

	public static function updateDataBlobForPropertiesByCid( $intCid, $objDatabase ) {
		$strSql = 'UPDATE data_blobs
					SET
						cid = p.cid,
						data_blob_type_id = ' . CDataBlobType::PROPERTIES . ',
						property_id = p.id,
						lease_customer_id = NULL,
						customer_id = NULL,
						lease_id = NULL,
						employee_id = NULL,
						applicant_application_id = NULL,
						applicant_id = NULL,
						application_id = NULL,
						ap_payee_id = NULL,
						data_field1 = p.property_name,
						data_field2 = NULL,
						data_field3 = NULL,
						order_num = 1,
						blob = property_name,
						updated_on = p.updated_on
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND data_blob_type_id = ' . CDataBlobType::PROPERTIES . '
						AND data_blobs.cid = p.cid AND data_blobs.property_id = p.id
						AND ( data_blobs.updated_on IS NULL OR data_blobs.updated_on < p.updated_on )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function insertDataBlobForPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'INSERT INTO public.data_blobs (
						cid,
						data_blob_type_id,
						property_id,
						lease_customer_id,
						customer_id,
						lease_id,
						employee_id,
						applicant_application_id,
						applicant_id,
						application_id,
						ap_payee_id,
						data_field1,
						data_field2,
						data_field3,
						order_num,
						blob,
						updated_on
					)
					SELECT
						p.cid,
						' . CDataBlobType::PROPERTIES . ' AS data_blob_type_id,
						p.id as property_id,
						NULL as lease_customer_id,
						NULL as customer_id,
						NULL as lease_id,
						NULL as employee_id,
						NULL as applicant_application_id,
						NULL as applicant_id,
						NULL as application_id,
						NULL as ap_payee_id,
						p.property_name as data_field1,
						NULL as data_field2,
						NULL as data_field3,
						1 as order_num,
						p.property_name as blob,
						p.updated_on
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND NOT EXISTS ( SELECT cid FROM data_blobs where cid = p.cid AND data_blob_type_id = ' . CDataBlobType::PROPERTIES . ' AND property_id = p.id )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function fetchDataBlobsByCidByPropertyIdsByDataBlobTypeIdsBySearchKeywordsByLimit( $intCid, $arrintPropertyIds, $arrintDataBlobTypeIds, $arrstrSearchKeywords, $intLimit = 17, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintDataBlobTypeIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrSearchKeywords ) ) {
			return NULL;
		}

		$strSqlWhere = '';
		$strSqlRank  = 'db.data_field2';

		$arrstrSqlSearchWords = $arrstrTsRankWords = [];

		$strNewsearchKeyword = pg_escape_literal( '%' . preg_replace( '/[&\(\)\- ]/', '', implode( '', $arrstrSearchKeywords ) ) . '%' );

		if( true == is_numeric( $strNewsearchKeyword ) && 10 == strlen( $strNewsearchKeyword ) ) {
			$arrstrSearchKeywords = [ $strNewsearchKeyword ];
		}

		foreach( $arrstrSearchKeywords as $strSearchKeyword ) {

			if( false == valStr( $strSearchKeyword ) ) {
				continue;
			}
			if( '-' == $strSearchKeyword ) {
				continue;
			}

			$strSearchKeyword       = str_replace( ',', '', $strSearchKeyword );
			$strSearchKeyword       = trim( str_replace( '%', '\%', $strSearchKeyword ) );
			$arrstrSqlSearchWords[] = ' blob ilike ' . pg_escape_literal( '%' . $strSearchKeyword . '%' );

			if( true == valStr( $strSearchKeyword ) ) {
				$arrstrTsRankWords[] = $strSearchKeyword;
			}
		}

		if( true == valArr( $arrstrSqlSearchWords ) ) {
			$strSqlWhere .= ' AND ' . implode( ' AND ', $arrstrSqlSearchWords );
		}

		// to set sorting order for high raking words at top of result
		$strSqlRank = 'db.order_num,
						CASE WHEN db.data_field2 ILIKE ' . pg_escape_literal( implode( ' ', $arrstrTsRankWords ) ) . ' THEN 0 ELSE 1 END,
						CASE WHEN db.data_field1 ILIKE ' . pg_escape_literal( implode( ' ', $arrstrTsRankWords ) ) . ' THEN 0 ELSE 1 END';
		if( true == valArr( $arrstrTsRankWords ) ) {
			$strSqlRank .= sprintf( ', ts_rank( to_tsvector( blob ), to_tsquery( quote_literal( %s ) ) ) DESC ', pg_escape_literal( implode( ' & ', $arrstrTsRankWords ) ) );
		}

		if( false == valStr( $strSqlWhere ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							db.data_blob_type_id,
							db.property_id,
							db.lease_id,
							db.lease_customer_id,
							db.customer_id,
							db.applicant_id,
							db.application_id,
							ct.id as customer_type_id,
							CASE WHEN db.data_blob_type_id = 3 THEN a.name_first ELSE c.name_first END as name_first,
							CASE WHEN db.data_blob_type_id = 3 THEN a.name_last ELSE c.name_last END as name_last,
                            CASE WHEN db.data_blob_type_id = 3 THEN
                                COALESCE( NULLIF( TRIM( a.name_last ), \'\') || \' \' || NULLIF( TRIM( a.name_last_matronymic ), \'\') || \', \' || NULLIF( TRIM( a.name_first ), \'\'), NULLIF( TRIM( a.name_last ), \'\') || \', \' || NULLIF( TRIM( a.name_first ), \'\'), NULLIF( TRIM( a.name_last_matronymic ), \'\') || \', \' || NULLIF( TRIM( a.name_first ), \'\'), NULLIF( TRIM( a.name_last ), \'\'), NULLIF( TRIM( a.name_last_matronymic ), \'\'), NULLIF( TRIM( a.name_first ), \'\'),\'\')
                            ELSE
                                COALESCE( NULLIF( TRIM( c.name_last ), \'\') || \' \' || NULLIF( TRIM( c.name_last_matronymic ), \'\') || \', \' || NULLIF( TRIM( c.name_first ), \'\'), NULLIF( TRIM( c.name_last ), \'\') || \', \' || NULLIF( TRIM( c.name_first ), \'\'), NULLIF( TRIM( c.name_last_matronymic ), \'\') || \', \' || NULLIF( TRIM( c.name_first ), \'\'), NULLIF( TRIM( c.name_last ), \'\'), NULLIF( TRIM( c.name_last_matronymic ), \'\'), NULLIF( TRIM( c.name_first ), \'\'),\'\')
                            END as full_name,
							CASE WHEN ( data_blob_type_id = 1 AND \'\' <> split_part( blob, \'~..$\', 6 ) ) THEN
								split_part( blob, \'~..$\', 3 ) || \' \' || split_part( blob, \'~..$\', 4 ) || \' \' || split_part( blob, \'~..$\', 5 ) || \'-\' || split_part( blob, \'~..$\', 6 )
							ELSE
								split_part( blob, \'~..$\', 3 ) || \' \' || split_part( blob, \'~..$\', 4 ) || \' \' || split_part( blob, \'~..$\', 5 )
							END as property_building_unit,
	                        CASE WHEN ( data_blob_type_id = 1 AND \'\' <> split_part( blob, \'~..$\', 6 ) ) THEN
								split_part( blob, \'~..$\', 4 ) || \' \' || split_part( blob, \'~..$\', 5 ) || \'-\' || split_part( blob, \'~..$\', 6 )
							ELSE
								split_part( blob, \'~..$\', 4 ) || \' \' || split_part( blob, \'~..$\', 5 )
							END  as building_unit,
							split_part( blob, \'~..$\', 3 ) as property_name,
							CASE WHEN ( data_blob_type_id = 1 ) THEN split_part(blob, \'~..$\', 6 ) ELSE \'\'	END as space_number,
							util_get_translated( \'stage_name\', ases.stage_name, ases.details ) ||  \' \' ||  util_get_translated( \'status_name\', ases.status_name, ases.details ) as application_stage_status,
							util_get_translated( \'name\', lst.name, lst.details ) as lease_status_type
						FROM
							data_blobs db
							LEFT JOIN leases l ON l.id = db.lease_id AND l.cid = db.cid AND l.property_id = db.property_id
							LEFT JOIN lease_customers lc ON lc.customer_id = db.customer_id AND lc.cid = db.cid AND lc.lease_id = db.lease_id AND lc.property_id = db.property_id
                            LEFT JOIN applicant_applications aa ON aa.id = db.applicant_application_id AND aa.cid=db.cid
                            LEFT JOIN customer_types ct ON ct.id = lc.customer_type_id OR ct.id=aa.customer_type_id
                            LEFT JOIN applicants a ON a.id = db.applicant_id AND a.cid = db.cid
							LEFT JOIN customers c ON c.id = db.customer_id AND c.cid = db.cid
							LEFT JOIN lease_status_types lst ON lst.id = lc.lease_status_type_id
							LEFT JOIN cached_applications ca ON aa.application_id = ca.id and aa.cid = ca.cid
							LEFT JOIN application_stage_statuses ases ON ca.lease_interval_type_id = ases.lease_interval_type_id AND ca.application_stage_id = ases.application_stage_id AND ca.application_status_id = ases.application_status_id 
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.data_blob_type_id IN ( ' . implode( ',', $arrintDataBlobTypeIds ) . ' )
							AND db.property_id = ANY ( array[' . implode( ',', $arrintPropertyIds ) . '] )
							' . $strSqlWhere . '
							AND ( db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS . ' OR lower( split_part(db.blob , \'~..$\', 7 ) ) NOT LIKE \'%applicant%\' )
							AND lower( db.blob ) NOT LIKE \'%cancelled%\'
							AND lower( db.blob ) NOT LIKE \'%lease approved%\'
							AND lower( db.blob ) NOT LIKE \'%lease renewal approved%\'
							AND lower( db.blob ) NOT LIKE \'%lease transfer approved%\'
							AND lower( db.blob ) NOT LIKE \'%lease modification approved%\'
							AND ( db.order_num <> 49 )
							AND ( l.occupancy_type_id NOT IN ( ' . implode( ',', COccupancyType::$c_arrintExcludedQuickSearchOccupancyTypes ) . ' ) OR l.occupancy_type_id IS NULL )
						ORDER BY
							' . $strSqlRank . '
					) AS temp
					LIMIT ' . ( int ) $intLimit;

		$arrstrSearchResults = fetchData( $strSql, $objDatabase );

		return $arrstrSearchResults;
	}

	public static function analyzeDataBlobsTables( $objDatabase ) {

		$strSql = 'ANALYZE clients;
					ANAlyze properties;
					ANALYZE applicant_applications;
					ANALYZE cached_applications;
					ANALYZE application_status_types;
					ANALYZE applicants;
					ANALYZE lease_customers;
					ANALYZE cached_leases;
					ANALYZE customers;
					ANALYZE customer_portal_settings;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function updateMismatchUnitNumberCacheForLeads( $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'UPDATE
						data_blobs
					SET
						updated_on = \'2011-01-01\'
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id IN (
							SELECT
								ca.id
							FROM
								data_blobs db
							JOIN
								cached_applications ca ON ( ca.cid = db.cid AND ca.id = db.application_id )
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.property_id = ' . ( int ) $intPropertyId . '
								AND db.data_blob_type_id = ' . CDataBlobType::LEADS . '
								AND split_part ( db.blob, \'~..$\', 5 ) <> ca.unit_number_cache
						)';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		} elseif( false === $arrmixResult ) {
			return false;
		}
	}

	public static function fetchDataBlobsByCidDataBlobTypeIdsBySearchKeywordsByLimit( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $intAllowEvictedResidents, $intAllowEvictingresidents, $intLimit = 17, $objDatabase ) {

		$strSql = ' SELECT filtered_blob.*
							   FROM
									( SELECT
										DISTINCT ON(db.lease_customer_id)
										db.application_id,
										db.cid,
										db.data_blob_type_id,
										db.property_id,
										db.lease_customer_id,
										db.order_num,
										db.data_field1,
										db.blob,
										db.lease_id
									FROM
										data_blobs db';

		// leases join is used like this to optimise query according to Puneet's suggestion
		$strSql .= ' JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id )
													 JOIN property_gl_settings pgs ON ( pgs.cid = li.cid AND pgs.property_id = li.property_id
																							AND CASE
																								WHEN pgs.activate_standard_posting IS true
																								THEN ( db.blob NOT ILIKE \'%$Future%\' OR li.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::TRANSFER . ', ' . CLeaseIntervalType::RENEWAL . ' ) )
																								ELSE TRUE
																							END
																							)
													 LEFT JOIN leases l ON ( l.active_lease_interval_id = li.id AND l.cid = li.cid AND l.id = li.lease_id )';

		if( true == $arrmixFastLookup['restrict_to_load_fmo_processed_leases'] ) {
			$strSql .= ' JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
		}

		$strSqlWhere = ' AND CASE WHEN l.id IS NOT NULL THEN TRUE ELSE FALSE END';

		$strSql .= ' WHERE
						db.cid = ' . ( int ) $intCid . $strSqlWhere;

		if( true == valStr( $arrmixFastLookup['property_ids'] ) ) {
			$strPropertyIds = $arrmixFastLookup['property_ids'];
			if( true == valStr( $strPropertyIds ) ) {
				$strSql .= ' AND  db.property_id IN (' . $strPropertyIds . ' )
							AND l.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )';
			}
		}

		$strSql .= '	AND db.blob NOT ILIKE \'%Cancelled%\'
						AND db.blob NOT ILIKE \'%Lease Approved%\'
						AND ( 	db.blob ILIKE \'%$applicant~%\'
						OR db.blob ILIKE \'%$future~%\'
						OR db.blob ILIKE \'%$current~%\'
						OR db.blob ILIKE \'%$notice~%\'
						OR db.blob ILIKE \'%$past~%\'
											';
		if( '1' == $intAllowEvictedResidents ) {
			$strSql .= ' OR db.blob ILIKE \'%$evicted~%\' ';
		}

		if( '1' == $intAllowEvictingresidents ) {
			$strSql .= ' OR db.blob ILIKE \'%$evicting~%\' ';
		}

		$strSql .= ' )
						) as filtered_blob
						WHERE
							filtered_blob.blob ILIKE \'%' . implode( '%\' AND filtered_blob.blob ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							AND filtered_blob.cid = ' . ( int ) $intCid . '
						ORDER BY
							filtered_blob.order_num,
							lower( filtered_blob.data_field1 )';
		$strSql .= ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDataBlobsByCidDataBlobTypeIdsBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strLookUpType     = $arrmixFastLookup['lookup_type'];
		$arrintPropertyIds = [ $arrmixFastLookup['property_id'] ];
		$strSql            = '';
		$strJoinSql        = '';

		$strWhereSql = ' AND CASE
								WHEN
									pgs.activate_standard_posting IS false
								THEN
									 db.blob NOT ILIKE \'%Cancelled%\'
				                WHEN
									pgs.activate_standard_posting IS true
								THEN
									db.blob ILIKE \'%Cancelled%\' OR db.blob ILIKE \'%current%\' OR db.blob ILIKE \'%notice%\' OR db.blob ILIKE \'%past%\' OR db.blob ILIKE \'%future%\' OR db.blob ILIKE \'%applicant%\'
				END ';

		$arrintEligibleIndexPositionsForSearch = [
			1, // First name
			2, // Last name
			3, // Property Name
			5, // Unit Number
			7, // Lease Status
			13 // Business Name
		];

		$arrstrFilteredExplodedSearch = array_filter( $arrstrFilteredExplodedSearch );
		// leases join is used like this to optimise query according to Puneet's suggestion
		$strJoinSql .= ' JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id )
							JOIN property_gl_settings pgs ON ( pgs.cid = li.cid AND pgs.property_id = li.property_id )
							LEFT JOIN leases l ON ( l.active_lease_interval_id = li.id AND l.cid = li.cid AND l.id = li.lease_id AND l.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' ) ) 
							LEFT JOIN applications ap ON( l.cid = ap.cid AND l.id = ap.lease_id AND ap.lease_interval_id = li.id )';

// 			if( true == $arrmixFastLookup['restrict_to_load_fmo_processed_leases'] ) {
// 				$strJoinSql .= ' JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
// 			}

		$arrstrWhereSqls = [];
		foreach( $arrintEligibleIndexPositionsForSearch as $intEligibleIndexPosition ) {
			$arrstrWhereSqls[] = 'split_part( db.blob, \'~..$\', ' . ( int ) $intEligibleIndexPosition . ') ILIKE \'%' . implode( '%\' OR split_part( db.blob, \'~..$\', ' . ( int ) $intEligibleIndexPosition . ') ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'';
		}

		$strWhereSql .= ' AND ( ' . implode( ' OR ', $arrstrWhereSqls ) . ' )
						 AND CASE
								WHEN l.id IS NOT NULL
								THEN TRUE
								ELSE FALSE
							END';

		if( true == valArr( $arrstrFilteredExplodedSearch ) && true == valArr( $arrintPropertyIds ) ) {

			$strSql = 'SELECT filtered_blob.*
						FROM 
							( SELECT
				                db.*,
				                ap.application_status_id,
				              CASE
				               WHEN ap.application_status_id IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) THEN 1
				               ELSE 0
				             END is_archieved
							FROM
								data_blobs db
								' . $strJoinSql . '
							WHERE
								db.cid = ' . ( int ) $intCid . '
								AND db.data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . '
								AND db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND db.blob ILIKE \'%' . implode( '%\' AND db.blob ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
								' . $strWhereSql . '
							ORDER BY
								db.order_num,
								lower( db.data_field1 ) 
						) as filtered_blob
	                        WHERE filtered_blob.cid = ' . ( int ) $intCid . '  AND
	                              filtered_blob.is_archieved <> 1';

			$arrstrSearchResults = fetchData( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDataBlobsForReassociatePaymentByCidDataBlobTypeIdsBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrSqlSearchWords, $arrstrTsRankWords, $boolIsModulePermissionForCustomerSystem, $boolIsModulePermissionForProspectSystem, $boolIsAdministrator, $objDatabase ) {

		$strSqlWhere = '';
		$strSqlRank  = 'db.data_field2';
		if( true == valArr( $arrstrSqlSearchWords ) ) {
			$strSqlWhere .= ' AND ' . implode( ' AND ', $arrstrSqlSearchWords );
		}

		// to set sorting order for high raking words at top of result
		if( true == valArr( $arrstrTsRankWords ) ) {
			$strSqlRank = sprintf( ' ts_rank( to_tsvector( blob ), to_tsquery( quote_literal( \'%s\' ) ) ) DESC ', implode( ' & ', $arrstrTsRankWords ) );
		}

		$intDataBlobTypeId = ( true == is_numeric( $arrmixFastLookup['data_blob_type_id'] ) ) ? $arrmixFastLookup['data_blob_type_id'] : NULL;

		$arrintPropertyIds = [ $arrmixFastLookup['property_id'] ];

		if( true == valStr( $strSqlWhere ) && valArr( $arrintPropertyIds ) ) {

			$strSql     = '';
			$strJoinSql = '';

			$strJoinSql .= ' JOIN leases l ON ( l.id = db.lease_id AND l.cid = db.cid )
		    					 JOIN property_gl_settings pgs ON ( pgs.cid = l.cid AND pgs.property_id = l.property_id )
		    					 JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id AND l.active_lease_interval_id = li.id )';

			$strJoinSql .= 'LEFT JOIN applications a ON( l.cid = a.cid AND l.id = a.lease_id AND a.lease_interval_id = li.id AND a.application_status_id NOT IN( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) ) ';

			if( true == $arrmixFastLookup['restrict_to_load_fmo_processed_leases'] ) {
				$strJoinSql .= ' JOIN leases l ON ( l.id = db.lease_id AND l.cid = db.cid )
								 JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
			}

			if( true == valStr( $strJoinSql ) ) {
				$strSqlWhere .= ' AND l.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )';
			}

			$strSql = 'SELECT date_blob_reassociate.*
						FROM ( SELECT
								db.application_id,
								db.data_blob_type_id,
								db.property_id,
								db.lease_customer_id,
								db.lease_id,
								db.applicant_id,
								db.blob,
								CASE
                                    WHEN a.application_status_id IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) THEN 1
                                ELSE 0
                                    END is_archieved FROM
							data_blobs db
							 ' . $strJoinSql . '
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
			          $strSqlWhere .
			          ' AND
									CASE
										WHEN
											pgs.activate_standard_posting IS false
										THEN
											 db.blob NOT ILIKE \'%Cancelled%\'
						                WHEN
											pgs.activate_standard_posting IS true
										THEN
											db.blob ILIKE \'%Cancelled%\' OR db.blob ILIKE \'%current%\' OR db.blob ILIKE \'%notice%\' OR db.blob ILIKE \'%past%\' OR db.blob ILIKE \'%future%\' OR db.blob ILIKE \'%applicant%\'
									END
							  AND db.blob NOT ILIKE \'%Lease Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Renewal Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Transfer Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Modification Approved%\'';

			if( CDataBlob::LOOKUP_TYPE_REASSCOCIATE_PAYMENT != $arrmixFastLookup['lookup_type'] ) {
				$strSql .= ' AND ( db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS . ' OR db.blob NOT ILIKE \'%Applicant%\' )';
			}

			if( true == $boolIsAdministrator ) {
				if( true == is_numeric( $intDataBlobTypeId ) ) {
					$strSql .= ' AND db.data_blob_type_id = ' . ( int ) $intDataBlobTypeId . ' ';
				}
			} else {

				if( true == is_numeric( $intDataBlobTypeId ) ) {

					if( ( CDataBlobType::LEASE_CUSTOMERS == $intDataBlobTypeId && true == $boolIsModulePermissionForCustomerSystem ) || ( CDataBlobType::LEADS == $intDataBlobTypeId && true == $boolIsModulePermissionForProspectSystem ) ) {
						$strSql .= ' AND db.data_blob_type_id = ' . ( int ) $intDataBlobTypeId . ' ';
					} else {
						if( false == $boolIsModulePermissionForCustomerSystem ) {
							$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS;
						}

						if( false == $boolIsModulePermissionForProspectSystem ) {
							$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEADS;
						}
					}
				} else {

					if( false == $boolIsModulePermissionForCustomerSystem ) {
						$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS;
					}

					if( false == $boolIsModulePermissionForProspectSystem ) {
						$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEADS;
					}
				}
			}

			$strOrderBySql = ' ORDER BY db.order_num, ' . $strSqlRank . ' ) as date_blob_reassociate WHERE date_blob_reassociate.is_archieved <> 1  ';

		}

		return fetchData( $strSql . $strOrderBySql, $objDatabase );
	}

	public static function fetchDataBlobsByCidBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $boolAddPayment, $boolOtherIncomePayment, $objDatabase ) {

		$strCondition = 'l.occupancy_type_id NOT IN ( ' . implode( ',', COccupancyType::$c_arrintExcludedOccupancyTypes ) . ' ) ';

		if( $boolAddPayment == true && $boolOtherIncomePayment == false ) {
			$strCondition = 'l.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType :: OTHER_INCOME ] ) ) . ' ) ';
		} elseif( $boolAddPayment == false && $boolOtherIncomePayment == true ) {
			$strCondition = 'l.occupancy_type_id = ' . COccupancyType :: OTHER_INCOME;
		}

		$strSql = ' SELECT filtered_blob.*
							   FROM
									( SELECT
										DISTINCT ON(db.lease_customer_id)
										db.application_id,
										db.cid,
										db.data_blob_type_id,
										db.property_id,
										db.lease_customer_id,
										db.order_num,
										db.data_field1,
										db.blob,
										db.lease_id,
										CASE
											WHEN ap.application_status_id IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) THEN 1
										ELSE 0
											END is_archieved
									FROM
										data_blobs db';

		// leases join is used like this to optimise query according to Puneet's suggestion
		$strSql .= ' JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id )
													 JOIN property_gl_settings pgs ON ( pgs.cid = li.cid AND pgs.property_id = li.property_id
																							AND CASE
																								WHEN pgs.activate_standard_posting IS true
																								THEN ( db.blob NOT ILIKE \'%$Future%\' OR li.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::TRANSFER . ', ' . CLeaseIntervalType::RENEWAL . ' ) )
																								ELSE TRUE
																							END
																							)
													 LEFT JOIN leases l ON ( l.active_lease_interval_id = li.id AND l.cid = li.cid AND l.id = li.lease_id )';

		if( true == $arrmixFastLookup['restrict_to_load_fmo_processed_leases'] ) {
			$strSql .= ' JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
		}
		$strSql .= 'LEFT JOIN applications ap ON( l.cid = ap.cid AND l.id = ap.lease_id AND ap.lease_interval_id = li.id )';

		$strSqlWhere = ' AND CASE
										                	WHEN l.id IS NOT NULL
															THEN TRUE
										                	ELSE FALSE
										              	END';

		$strSql .= ' WHERE
										db.cid = ' . ( int ) $intCid . $strSqlWhere;

		if( true == valStr( $arrmixFastLookup['property_ids'] ) ) {
			$strPropertyIds = $arrmixFastLookup['property_ids'];
			if( true == valStr( $strPropertyIds ) ) {
				$strSql .= ' AND  db.property_id IN (' . $strPropertyIds . ' )
														 AND ' . $strCondition . '';
			}
		}

		$strSql .= ' AND db.blob ILIKE \'%' . implode( '%\' AND db.blob ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'	AND db.blob NOT ILIKE \'%Lease Approved%\'
													AND ( 	db.blob ILIKE \'%$applicant~%\'
													OR db.blob ILIKE \'%$future~%\'
													OR db.blob ILIKE \'%Cancelled~%\'
													OR db.blob ILIKE \'%$current~%\'
													OR db.blob ILIKE \'%$notice~%\'
													OR db.blob ILIKE \'%$past~%\'
											';
		$strSql .= ' OR db.blob ILIKE \'%$evicted~%\' 
						 OR db.blob ILIKE \'%$evicting~%\' )';

		$strSql .= ' ) as filtered_blob
						WHERE
							filtered_blob.is_archieved <> 1
						ORDER BY
							filtered_blob.order_num,
							lower( filtered_blob.data_field1 )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDataBlobsByCidByPropertyIdBySearchKeywords( $intCid, $boolIsAdministrator, $arrmixRequestData, $arrstrExplodedSearch, $boolIsAllowCustomerSystems, $boolIsAllowProspectSystems, $objDatabase ) {

		$arrmixFastLookup         = $arrmixRequestData['fast_lookup_filter'];
		$arrstrMatchSearchParam   = [];
		$arrintCountSearchResults = [];

		$intDataBlobTypeId = ( true == is_numeric( $arrmixFastLookup['data_blob_type_id'] ) ) ? $arrmixFastLookup['data_blob_type_id'] : NULL;

		$arrintPropertyIds = [ $arrmixFastLookup['property_id'] ];

		$strSqlWhere = '';
		$strSqlRank  = 'db.data_field2';

		if( true == valStr( $arrmixFastLookup['child_property_ids'] ) ) {
			$arrintChildPropertyIds = explode( ',', $arrmixFastLookup['child_property_ids'] );
			if( true == valArr( $arrintPropertyIds ) ) {
				$arrintPropertyIds = array_merge( $arrintPropertyIds, $arrintChildPropertyIds );
			}
		}

		if( true == valArr( $arrstrExplodedSearch ) ) {

			$arrstrSqlSearchWords = $arrstrTsRankWords = [];

			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {

				if( false == valStr( $strExplodedSearch ) ) {
					continue;
				}

				if( '-' == $strExplodedSearch ) {
					continue;
				}

				$strExplodedSearch = str_replace( '%', '\%', $strExplodedSearch );

				$strExplodedSearch = addslashes( trim( $strExplodedSearch ) );

				// if seach word is all number then check only word begining with search word (by adding $searchword). This is required to show more revelent results for unit numbers
				if( ( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrExplodedSearch ) ) && ( true == is_numeric( $strExplodedSearch ) ) ) {
					$strExplodedSearch = '$' . $strExplodedSearch;
				}

				$arrstrSqlSearchWords[] = ' blob ilike \'%' . $strExplodedSearch . '%\' ';

				// prepare list of words for ts_query and ts_rank
				$strExplodedSearch = preg_replace( '/[&\(\)]/', '', $strExplodedSearch );

				if( true == valStr( $strExplodedSearch ) ) {
					$arrstrTsRankWords[] = $strExplodedSearch;
				}
			}

			if( true == valArr( $arrstrSqlSearchWords ) ) {
				$strSqlWhere .= ' AND ' . implode( ' AND ', $arrstrSqlSearchWords );
			}

			// to set sorting order for high raking words at top of result
			if( true == valArr( $arrstrTsRankWords ) ) {
				$strSqlRank = sprintf( ' ts_rank( to_tsvector( blob ), to_tsquery( \'%s\' ) ) DESC ', implode( ' & ', $arrstrTsRankWords ) );
			}
		}

		if( true == valStr( $strSqlWhere ) && valArr( $arrintPropertyIds ) ) {
			$strSql        = '';
			$strJoinSql    = '';
			$strGroupBySql = '';

			if( CDataBlob::LOOKUP_TYPE_REASSCOCIATE_PAYMENT == $arrmixFastLookup['lookup_type'] ) {
				$strJoinSql .= ' JOIN leases l ON ( l.id = db.lease_id AND l.cid = db.cid )
		    					 JOIN property_gl_settings pgs ON ( pgs.cid = l.cid AND pgs.property_id = l.property_id )
		    					 JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id AND l.active_lease_interval_id = li.id
		    												AND CASE
								    							WHEN pgs.activate_standard_posting IS true
								    							THEN ( db.blob NOT ILIKE \'%$Future%\' OR li.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::TRANSFER . ', ' . CLeaseIntervalType::RENEWAL . ' ) )
								    							ELSE TRUE
															END
		    												)';
			}

			if( true == $arrmixFastLookup['restrict_to_load_fmo_processed_leases'] ) {
				$strJoinSql .= ' JOIN leases l ON ( l.id = db.lease_id AND l.cid = db.cid )
								 JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
			}

			if( true == valStr( $strJoinSql ) ) {
				$strSqlWhere .= ' AND l.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )';
			}

			if( false == is_null( $arrmixRequestData['is_from_inspection'] ) && 1 == $arrmixRequestData['is_staff_inspection'] ) {
				$strJoinSql .= 'JOIN leases l ON ( db.cid = l.cid AND db.lease_id = l.id AND db.property_id = l.property_id )
								JOIN lease_processes lp ON ( l.cid = lp.cid AND l.id = lp.lease_id )';

				$strGroupBySql = 'GROUP BY
									db.lease_customer_id,
									db.application_id,
                                    db.data_blob_type_id,
                                    db.property_id,
						            db.lease_id,
                                    db.applicant_id,
                                    db.blob,
                                    db.order_num';
				if( false == valArr( $arrstrTsRankWords ) ) {
					$strGroupBySql .= ',db.data_field2';
				}

			}

			$strSelectSql = 'SELECT
								db.application_id,
								db.data_blob_type_id,
								db.property_id,
								db.lease_customer_id,
								db.lease_id,
								db.applicant_id,
								db.blob';

			$strSql = ' FROM
							data_blobs db
							 ' . $strJoinSql . '
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
			          $strSqlWhere .
			          ' AND db.blob NOT ILIKE \'%Cancelled%\'
							  AND db.blob NOT ILIKE \'%Lease Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Renewal Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Transfer Approved%\'
							  AND db.blob NOT ILIKE \'%Lease Modification Approved%\'';

			if( CDataBlob::LOOKUP_TYPE_REASSCOCIATE_PAYMENT != $arrmixFastLookup['lookup_type'] ) {
				$strSql .= ' AND ( db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS . ' OR db.blob NOT ILIKE \'%Applicant%\' )';
			}

			if( true == $boolIsAdministrator ) {
				if( true == is_numeric( $intDataBlobTypeId ) ) {
					$strSql .= ' AND db.data_blob_type_id = ' . ( int ) $intDataBlobTypeId . ' ';
				}
			} else {

				if( true == is_numeric( $intDataBlobTypeId ) ) {

					if( ( CDataBlobType::LEASE_CUSTOMERS == $intDataBlobTypeId && true == $boolIsAllowCustomerSystems ) || ( CDataBlobType::LEADS == $intDataBlobTypeId && true == $boolIsAllowProspectSystems ) ) {
						$strSql .= ' AND db.data_blob_type_id = ' . ( int ) $intDataBlobTypeId . ' ';
					} else {
						if( false == $boolIsAllowCustomerSystems ) {
							$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS;
						}

						if( false == $boolIsAllowProspectSystems ) {
							$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEADS;
						}
					}
				} else {

					if( false == $boolIsAllowCustomerSystems ) {
						$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS;
					}

					if( false == $boolIsAllowProspectSystems ) {
						$strSql .= ' AND db.data_blob_type_id <> ' . CDataBlobType::LEADS;
					}
				}
			}

			if( false == is_null( $arrmixRequestData['is_from_inspection'] ) ) {
				$strSql .= ' AND db.data_field2 IS NOT NULL AND ( db.blob NOT ILIKE \'%$Past~%\'';
				if( 1 == $arrmixRequestData['is_staff_inspection'] ) {
					$strSql .= ' OR ( db.blob ILIKE \'%$Past~%\' AND lp.move_out_date > ( CURRENT_DATE - INTERVAL \'90 days\' ) )';
				}
				$strSql .= ' )';
			}

			$strOrderBySql = ' ORDER BY db.order_num, ' . $strSqlRank . ' ';

			return fetchData( $strSelectSql . $strSql . $strGroupBySql . $strOrderBySql, $objDatabase );

		}
	}

	public static function fetchDataBlobsByCidByPropertyIdByDataBlobTypeIdBySearchKeywords( $intCid, $arrmixRequestData, $arrstrFilteredExplodedSearch, $objDatabase ) {

		$intDataBlobTypeId = CDataBlobType::LEASE_CUSTOMERS;

		if( true == is_numeric( $arrmixRequestData['data_blob_type_id'] ) ) {
			$intDataBlobTypeId = $arrmixRequestData['data_blob_type_id'];
		}

		if( true == is_numeric( $arrmixRequestData['property_id'] ) ) {
			// Bad - this variable should never be overridden
			$arrintPropertyIds = [ $arrmixRequestData['property_id'] ];
		} elseif( true == valStr( $arrmixRequestData['property_ids'] ) ) {
			$arrintPropertyIds = explode( ',', $arrmixRequestData['property_ids'] );
		}

		if( true == is_numeric( $arrmixRequestData['pm_enabled_only'] ) && 1 == $arrmixRequestData['pm_enabled_only'] ) {
			$arrintPropertyIds = array_intersect( $arrintPropertyIds, $arrintPmEnabledPropertyIds );
		}

		if( true == valStr( $arrmixRequestData['child_property_ids'] ) ) {
			$arrintChildPropertyIds = explode( ',', $arrmixRequestData['child_property_ids'] );
			$arrintPropertyIds      = array_merge( $arrintPropertyIds, $arrintChildPropertyIds );
		}

		if( true == valStr( $arrmixRequestData['payment_ar_code_id'] ) ) {
			$arrintPropertyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByArCodeIdByByCid( $arrmixRequestData['payment_ar_code_id'], $intCid, $objDatabase );
			$arrintPropertyIds = array_intersect( $arrintPropertyIds, array_keys( rekeyArray( 'id', $arrintPropertyIds ) ) );
		}

		$strSql        = '';
		$strJoinSql    = '';
		$strLookUpType = $arrmixRequestData['lookup_type'];
		if( CDataBlobType::LEASE_CUSTOMERS == $intDataBlobTypeId ) {

			$strSql = ' AND db.blob NOT ILIKE \'%Cancelled%\'';

			if( CDataBlob::LOOKUP_TYPE_CUSTOMERS_FOR_CHECK21_BATCHES == $strLookUpType ) {
				$arrintEligibleIndexPositionsForSearch = [
					1, // First name
					2, // Last name
					3, // Property Name
					5, // Unit Number
					7, // Lease Status
					13 // Business Name
				];

				$arrstrFilteredExplodedSearch = array_filter( $arrstrFilteredExplodedSearch );
				// leases join is used like this to optimise query according to Puneet's suggestion
				$strJoinSql .= ' JOIN lease_intervals li ON ( db.cid = li.cid AND db.lease_id = li.lease_id )
								JOIN property_gl_settings pgs ON ( pgs.cid = li.cid AND pgs.property_id = li.property_id
																	AND CASE
																		WHEN pgs.activate_standard_posting IS true
																		THEN ( db.blob NOT ILIKE \'%$Future%\' OR li.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::TRANSFER . ', ' . CLeaseIntervalType::RENEWAL . ' ) )
																		ELSE TRUE
																	END
																	)
								LEFT JOIN leases l ON ( l.active_lease_interval_id = li.id AND l.cid = li.cid AND l.id = li.lease_id AND l.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' ) ) ';

				if( true == $arrmixRequestData['restrict_to_load_fmo_processed_leases'] ) {
					$strJoinSql .= ' JOIN lease_processes lp ON ( l.id = lp.lease_id AND l.cid = lp.cid AND lp.customer_id IS NULL AND lp.fmo_started_on IS NULL )';
				}

				$arrstrWhereSqls = [];
				foreach( $arrintEligibleIndexPositionsForSearch as $intEligibleIndexPosition ) {
					$arrstrWhereSqls[] = 'split_part( db.blob, \'~..$\', ' . ( int ) $intEligibleIndexPosition . ') ILIKE \'%' . implode( '%\' OR split_part( db.blob, \'~..$\', ' . ( int ) $intEligibleIndexPosition . ') ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'';
				}

				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrWhereSqls ) . ' )
							 AND CASE
									WHEN l.id IS NOT NULL
									THEN TRUE
									ELSE FALSE
								END';
			}

			if( CDataBlob::LOOKUP_TYPE_CALL_CENTER_WORK_ORDER_RESIDENTS == $strLookUpType ) {
				$strSql = ' AND db.blob NOT ILIKE \'%$Cancelled~%\'';
				$strSql .= ' AND db.blob NOT ILIKE \'%$Past~%\'';
				$strSql .= ' AND db.blob NOT ILIKE \'%$Evicted~%\'';
			}
		}

		if( CDataBlobType::LEADS == $intDataBlobTypeId ) {

			if( true == valArr( $arrmixRequestData['excluding_application_ids'] ) ) {
				$strSql = ' AND db.application_id NOT IN ( ' . implode( ',', $arrmixRequestData['excluding_application_ids'] ) . ') ';
			}

			if( true == valArr( $arrmixRequestData['excluding_applicant_ids'] ) ) {
				$strSql .= ' AND db.applicant_id NOT IN ( ' . implode( ',', $arrmixRequestData['excluding_applicant_ids'] ) . ' ) ';
			}

			if( 1 == $arrmixRequestData['combine_leads_status_only'] ) {
				$arrstrWhereSqls = [];

				$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

				$strJoinSql .= ' JOIN applicant_applications aa ON ( aa.id = db.applicant_application_id AND aa.application_id = db.application_id AND aa.cid = db.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )';
				$strJoinSql .= ' JOIN cached_applications ca ON ( ca.id = aa.application_id AND ca.cid = aa.cid AND ca.combined_application_id IS NULL )';

				foreach( CApplicationStageStatus::$c_arrstrCombineLeadApplicationStageStatuses as $strApplicationStageStatus ) {
					$arrstrWhereSqls[] = ' db.blob ILIKE \'%' . $strApplicationStageStatus . '\' ';
				}
			}

			if( true == isset( $arrstrWhereSqls ) && true == valArr( $arrstrWhereSqls ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrWhereSqls ) . ' ) ';
			}

			if( 1 == $arrmixRequestData['active_leads_status_only'] ) {
				$arrstrWhereSqls = [];

				foreach( CApplicationStageStatus::$c_arrstrNonActiveProspectStageStatuses as $strNonActiveProspectStageStatus ) {
					$arrstrWhereSqls[] = ' db.blob NOT ILIKE \'%' . $strNonActiveProspectStageStatus . '\' ';
				}

				if( true == isset( $arrstrWhereSqls ) && true == valArr( $arrstrWhereSqls ) ) {
					$strSql .= ' AND ( ' . implode( ' AND ', $arrstrWhereSqls ) . ' ) ';
				}
			}
		}

		if( true == valStr( $arrmixRequestData['active_resident_only'] ) ) {
			$strSql .= ' AND db.blob NOT ILIKE \'%$Past~%\'';
			$strSql .= ' AND db.blob NOT ILIKE \'%$Cancelled~%\'';
		}

		if( true == valStr( $arrmixRequestData['ignore_applicants'] ) && 1 == $arrmixRequestData['ignore_applicants'] ) {
			$strSql .= ' AND db.blob NOT ILIKE \'%$Applicant~%\'';
		}

		if( true == valStr( $arrmixRequestData['hide_not_responsible'] ) ) {
			$strJoinSql .= ( CDataBlobType::LEASE_CUSTOMERS == $intDataBlobTypeId ) ? ' JOIN lease_customers ct ON ( db.lease_customer_id = ct.id AND db.cid = ct.cid )' : '';
			$strJoinSql .= ( CDataBlobType::LEADS == $intDataBlobTypeId ) ? ' JOIN applicant_applications ct ON ( db.applicant_application_id = ct.id AND db.cid = ct.cid )' : '';

			$strSql .= ' AND ct.customer_type_id NOT IN ( ' . CCustomerType::NOT_RESPONSIBLE . ' )';
		}

		if( true == valArr( $arrintPropertyIds ) ) {

			$strSql = 'SELECT
			 				db.*
						FROM
							data_blobs db
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . '
							AND db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND db.blob ILIKE \'%' . implode( '%\' AND db.blob ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							' . $strSql . '
						ORDER BY
							db.order_num,
							lower( db.data_field1 )
							LIMIT 100';

			return fetchData( $strSql, $objDatabase );
		}

	}

	public static function buildFilteredExplodedSearchBySearchedString( $strSearchedString ) {

		$arrmixExplodedSearch         = [];
		$arrmixFilteredExplodedSearch = [];
		$strFilteredExplodedSearch    = '';

		if( true == valStr( $strSearchedString ) ) {

			$arrmixExplodedSearch = explode( ' ', $strSearchedString );

			if( true == valArr( $arrmixExplodedSearch ) ) {

				if( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixExplodedSearch ) ) {
					array_push( $arrmixFilteredExplodedSearch, $strSearchedString );
				}

				if( true == valArr( $arrmixFilteredExplodedSearch ) && true == isset( $arrmixFilteredExplodedSearch[0] ) ) {
					$arrmixFilteredExplodedSearch[0] = addslashes( $arrmixFilteredExplodedSearch[0] );
				}

				foreach( $arrmixExplodedSearch as $strExplodedSearch ) {

					if( false === strrchr( $strExplodedSearch, '\'' ) ) {
						$strFilteredExplodedSearch = $strExplodedSearch;
					} else {
						$strFilteredExplodedSearch = '\' || quote_literal( \'' . addslashes( $strExplodedSearch ) . '\') || \'';
					}
					array_push( $arrmixFilteredExplodedSearch, $strFilteredExplodedSearch );
				}
			}
		}

		return $arrmixFilteredExplodedSearch;
	}

	public static function fetchDocumentManagementSearchDataBlobsByCidBySearchKeyword( $strKeyword, $arrintPropertyIds, $arrmixSearchOptions, $intCid, $intLimit, $objDatabase ) {

		$strKeyword = trim( $strKeyword );

		$strSqlFields   = [];
		$arrstrSqlWhere = [];

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrstrSqlWhere[] = 'db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		} else {
			return NULL;
		}

		if( 'unit_number_cache' == $arrmixSearchOptions['field'] ) {
			$strSqlFields[]   = ' DISTINCT( split_part ( blob, \'~..$\', 5 ) ) AS unit_number_cache';
			$arrstrSqlWhere[] = 'split_part ( blob, \'~..$\', 5 ) ilike \'%' . addslashes( $strKeyword ) . '%\'';
		} elseif( 'person_name' == $arrmixSearchOptions['field'] ) {
			$strSqlFields[] = ' split_part ( blob, \'~..$\', 2 ) as last_name,
				split_part ( blob, \'~..$\', 1 ) as first_name,
				split_part ( blob, \'~..$\', 3 ) AS property_name,
				split_part ( blob, \'~..$\', 4 ) AS building_name,
				split_part ( blob, \'~..$\', 5 ) AS unit_number_cache';

			$arrstrSqlKewordSearch = [];

			$arrstrExplodedSearch         = explode( ' ', $strKeyword );
			$arrstrFilteredExplodedSearch = [];
			if( true == valArr( $arrstrExplodedSearch ) ) {
				foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
					if( false === strrchr( $strExplodedSearch, '\'' ) ) {
						$strFilteredExplodedSearch = $strExplodedSearch;
					} else {
						$strFilteredExplodedSearch = '\' || quote_literal( \'' . addslashes( trim( $strExplodedSearch ) ) . '\') || \'';
					}

					array_push( $arrstrFilteredExplodedSearch, $strFilteredExplodedSearch );
				}
			}
			$arrstrFilteredExplodedSearch = array_filter( $arrstrFilteredExplodedSearch );

			$arrstrSqlKewordSearch[] = '(' . 'split_part ( blob, \'~..$\', 2 ) || \' \' || split_part ( blob, \'~..$\', 1 )' . 'ILIKE \'%' . implode( '%\' AND ' . 'split_part ( blob, \'~..$\', 2 ) || \' \' || split_part ( blob, \'~..$\', 1 )' . ' ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )';

			$arrstrSqlKewordSearch[] = 'split_part ( blob, \'~..$\', 4 ) ilike \'%' . addslashes( $strKeyword ) . '%\'';
			$arrstrSqlKewordSearch[] = 'split_part ( blob, \'~..$\', 5 ) ilike \'%' . addslashes( $strKeyword ) . '%\'';

			$arrstrSqlWhere[] = '( ' . implode( ' OR ', $arrstrSqlKewordSearch ) . ' )';
		}

		if( CEntityType::RESIDENT == $arrmixSearchOptions['entity_type_id'] ) {
			$arrstrSqlWhere[] = 'db.data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS;
		} elseif( CEntityType::LEAD == $arrmixSearchOptions['entity_type_id'] ) {
			$arrstrSqlWhere[] = 'db.data_blob_type_id = ' . CDataBlobType::LEADS;
		}

		$arrstrSqlWhere[] = '( db.data_blob_type_id <> ' . CDataBlobType::LEASE_CUSTOMERS . ' OR lower( db.blob ) NOT LIKE \'%applicant%\' )';

		$strSql = 'SELECT
		 				' . implode( ',', $strSqlFields ) . '
		 				FROM
						data_blobs db
					WHERE
						db.cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrSqlWhere ) . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchResidentDataBlobsByRequestDataBySearchKeywordsByCid( $arrmixRequestData, $arrstrSearchKeywords, $intCid, $objDatabase ) {

		$intDataBlobTypeId = CDataBlobType::LEASE_CUSTOMERS;
		$strSqlWhere = '';

		if( true == is_numeric( $arrmixRequestData['data_blob_type_id'] ) ) {
			$intDataBlobTypeId = $arrmixRequestData['data_blob_type_id'];
		}

		if( true == is_numeric( $arrmixRequestData['property_id'] ) ) {
			// Bad - this variable should never be overridden
			$arrintPropertyIds = [ $arrmixRequestData['property_id'] ];
		} elseif( true == valStr( $arrmixRequestData['property_ids'] ) ) {
			$arrintPropertyIds = explode( ',', $arrmixRequestData['property_ids'] );
		}

		$strNewsearchKeyword = pg_escape_literal( '%' . preg_replace( '/[&\(\)\- ]/', '', implode( '', $arrstrSearchKeywords ) ) . '%' );

		if( true == is_numeric( $strNewsearchKeyword ) && 10 == strlen( $strNewsearchKeyword ) ) {
			$arrstrSearchKeywords = [ $strNewsearchKeyword ];
		}

		foreach( $arrstrSearchKeywords as $strSearchKeyword ) {

			if( false == valStr( $strSearchKeyword ) ) {
				continue;
			}
			if( '-' == $strSearchKeyword ) {
				continue;
			}

			$strSearchKeyword = preg_replace( '/[^a-zA-Z]/', '', $strSearchKeyword );
			if( '' == $strSearchKeyword ) {
				continue;
			}

			$strSearchKeyword = str_replace( ',', '', $strSearchKeyword );
			$strSearchKeyword = trim( str_replace( '%', '\%', $strSearchKeyword ) );
			$arrstrSqlSearchWords[] = ' blob ilike ' . pg_escape_literal( '%' . $strSearchKeyword . '%' );

			if( true == valStr( $strSearchKeyword ) && '' != $strSearchKeyword ) {
				$arrstrFilteredExplodedSearch[] = $strSearchKeyword;
			}
		}

		if( true == valArr( $arrstrSqlSearchWords ) ) {
			$strSqlWhere .= ' AND ' . implode( ' AND ', $arrstrSqlSearchWords );
		}

		if( false == valStr( $strSqlWhere ) ) {
			return NULL;
		}

		$strSql        = '';
		if( CDataBlobType::LEASE_CUSTOMERS == $intDataBlobTypeId ) {

			$strSql = ' AND db.blob NOT ILIKE \'%Cancelled%\'';
			$strSql .= ' AND db.blob NOT ILIKE \'%$Future~%\'';
			$strSql .= ' AND db.blob NOT ILIKE \'%$Past~%\'';
		}

		if( true == valArr( $arrintPropertyIds ) ) {

			$strSql = 'SELECT
			 				db.*
						FROM
							data_blobs db
						WHERE
							db.cid = ' . ( int ) $intCid . '
							AND db.data_blob_type_id = ' . CDataBlobType::LEASE_CUSTOMERS . '
							AND db.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							' . $strSqlWhere . '
							AND db.blob ILIKE \'%' . implode( '%\' AND db.blob ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							' . $strSql . '
						ORDER BY
							db.order_num,
							lower( db.data_field1 )
							LIMIT 100';

			return fetchData( $strSql, $objDatabase );
		}
	}

}

?>