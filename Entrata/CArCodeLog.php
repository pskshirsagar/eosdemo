<?php

class CArCodeLog extends CBaseArCodeLog {

	const IS_IGNORED_FALSE 				= '0';
	const PSI_ADMIN_DISPLAY_USERNAME    = 'System';

	protected $m_strArCodeGroupName;
	protected $m_strUsername;
	protected $m_strWriteOffArCodeName;
	protected $m_strArOriginName;
	protected $m_strArCodeTypeName;
	protected $m_strArTriggerName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_code_group'] ) ) $this->setArCodeGroupName( $arrmixValues['ar_code_group'] );
		if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( $arrmixValues['username'] );
		if( true == isset( $arrmixValues['write_off_ar_code_name'] ) ) $this->setWriteOffArCodeName( $arrmixValues['write_off_ar_code_name'] );
		if( true == isset( $arrmixValues['ar_origin_name'] ) ) $this->setArOriginName( $arrmixValues['ar_origin_name'] );
		if( true == isset( $arrmixValues['ar_code_type_name'] ) ) $this->setArCodeTypeName( $arrmixValues['ar_code_type_name'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
	}

	public function setArCodeGroupName( $strArCodeGroupName ) {
		$this->m_strArCodeGroupName = $strArCodeGroupName;
	}

	public function getArCodeGroupName() {
		return $this->m_strArCodeGroupName;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = trim( $strUsername );
	}

	public function getUsername( $intUserId, $intCid, $objDatabase ) {

		if( true == valId( $intUserId ) && true == valId( $intCid ) && true == valObj( $objDatabase, CDatabase::Class ) ) {
			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intUserId, $intCid, $objDatabase );
		}
		if( true == valId( $intUserId ) && false == in_array( $intUserId, [ CUser::ID_RESIDENT_PORTAL, CUser::ID_VENDOR_ACCESS ] )
		    && ( true == valObj( $objCompanyUser, CDatabase::class ) && true == in_array( $objCompanyUser->getCompanyUserTypeId(), CCompanyUserType::$c_arrintPsiUserTypeIds ) ) ) {
			return self::PSI_ADMIN_DISPLAY_USERNAME;
		}

		return $this->m_strUsername;
	}

	public function setWriteOffArCodeName( $strWriteOffArCodeName ) {
		$this->m_strWriteOffArCodeName = $strWriteOffArCodeName;
	}

	public function getWriteOffArCodeName() {
		return $this->m_strWriteOffArCodeName;
	}

	public function setArOriginName( $strArOriginName ) {
		$this->m_strArOriginName = $strArOriginName;
	}

	public function getArOriginName() {
		return $this->m_strArOriginName;
	}

	public function setArCodeTypeName( $strArCodeTypeName ) {
		$this->m_strArCodeTypeName = $strArCodeTypeName;
	}

	public function getArCodeTypeName() {
		return $this->m_strArCodeTypeName;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorArCodeLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategorizationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDebitGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModifiedDebitGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWriteOffArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeSummaryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLedgerFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaptureDelayDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaptureOnApplicationApproval() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideCharges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideCredits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaiveLateFees() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCash() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowMoveOutReminder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegratedOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontExport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProrateCharges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCategorizationQueued() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPaymentInKind() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReserved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAmountIsEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorityNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategorizedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategorizedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>