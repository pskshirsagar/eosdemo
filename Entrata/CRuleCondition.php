<?php

class CRuleCondition extends CBaseRuleCondition {

	protected $m_strDescription;

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumAmount() {

		$boolIsValid = true;

		if( false == isset( $this->m_fltMinimumAmount ) || true == empty( $this->m_fltMinimumAmount ) ) {
			if( CRouteRuleType::OVER_BUDGET == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Over Budget amount.' ) ) );
			} elseif( CRouteRuleType::UNDER_BUDGET == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Under Budget amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_GREATER_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN, CRouteRuleType::TOTAL_AMOUNT_GREATER_THAN, CRouteRuleType::AMOUNT_GREATER_THAN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_GREATER_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Greater Than amount.' ) ) );
			} elseif( CRouteRuleType::FINANCIAL_MOVE_OUT_STARTED_DAYS_AFTER_MOVE_OUT_DATE == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter number of days.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_GREATER_THAN, CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_LESS_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid number of transactions.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_GREATER_THAN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_GREATER_THAN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Greater Than amount By Percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_LESS_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN, CRouteRuleType::TOTAL_AMOUNT_LESS_THAN, CRouteRuleType::AMOUNT_LESS_THAN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_LESS_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Less Than amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_LESS_THAN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_LESS_THAN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Less Than amount By Percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN, CRouteRuleType::TOTAL_AMOUNT_BETWEEN, CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Minimum amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Minimum amount By Percent.' ) ) );
			} elseif( CRouteRuleType::INCREASE_BUDGET_BY == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter change orders increase budget by amount.' ) ) );
			} elseif( CRouteRuleType::INCREASE_TOTAL_COST_BY == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter change orders increase total contract cost by amount.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Invoice total to PO total variance is greater than amount.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Invoice total to PO total variance is less than amount.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Invoice total to PO total variance is greater than percent.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Invoice total to PO total variance is less than percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_JOB_BUDGET_BY, CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter over job/job phase budget by amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_JOB_BUDGET_BY_PERCENT, CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter over job/job phase budget by percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_CONTRACT_COST_BY, CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter over contract cost/contract phase cost by amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_CONTRACT_COST_BY_PERCENT, CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter over contract cost/contract phase cost by percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Increases Monthly Billing By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::DECREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Decreases Monthly Billing By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Increases Monthly Billing By More Than amount.' ) ) );
			} elseif( CRouteRuleType::DECREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Decreases Monthly Billing By More Than amount.' ) ) );
			} elseif( CRouteRuleType::INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Increases Total Contract Value By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Decreases Total Contract Value By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Increases Total Contract Value By More Than amount.' ) ) );
			} elseif( CRouteRuleType::DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Decreases Total Contract Value By More Than amount.' ) ) );
			} elseif( false == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH, CRouteRuleType::TRANSACTION_CHARGE_CODE_IS, CRouteRuleType::TRANSACTION_REQUEST_IS_FOR, CRouteRuleType::ACCOMMODATION_IS ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter amount.' ) ) );
			}

			$boolIsValid = false;

			if( ( false == isset( $this->m_fltMinimumAmount ) || true == empty( $this->m_fltMinimumAmount ) ) && ( false == isset( $this->m_fltMaximumAmount ) || true == empty( $this->m_fltMaximumAmount ) ) && $this->getRouteRuleTypeId() == CRouteRuleType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Month field can not be empty.' ) ) );
				$boolIsValid = false;
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH, CRouteRuleType::TRANSACTION_CHARGE_CODE_IS ] ) ) {
				$boolIsValid = true;
			}

		} elseif( false == is_numeric( $this->m_fltMinimumAmount ) ) {
			if( CRouteRuleType::OVER_BUDGET == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Over Budget amount.' ) ) );
			} elseif( CRouteRuleType::UNDER_BUDGET == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Under Budget amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_GREATER_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN, CRouteRuleType::AMOUNT_GREATER_THAN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_GREATER_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Greater Than amount.' ) ) );
			} elseif( CRouteRuleType::TOTAL_GREATER_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Greater Than amount By Percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_LESS_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN, CRouteRuleType::AMOUNT_LESS_THAN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_LESS_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Less Than amount.' ) ) );
			} elseif( CRouteRuleType::FINANCIAL_MOVE_OUT_STARTED_DAYS_AFTER_MOVE_OUT_DATE == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid number of days.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_GREATER_THAN, CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_LESS_THAN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid number of transactions.' ) ) );
			} elseif( CRouteRuleType::TOTAL_LESS_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Less Than amount By Percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN, CRouteRuleType::TOTAL_AMOUNT_BETWEEN, CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Minimum amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Minimum amount By Percent.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Invoice total to PO total variance is greater than amount.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Invoice total to PO total variance is less than amount.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Invoice total to PO total variance is greater than percent.' ) ) );
			} elseif( CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Invoice total to PO total variance is less than percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_JOB_BUDGET_BY, CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid over job/job phase budget by amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_JOB_BUDGET_BY_PERCENT, CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid over job/job phase budget by percent.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_CONTRACT_COST_BY, CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid over contract cost/contract phase cost by amount.' ) ) );
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::OVER_CONTRACT_COST_BY_PERCENT, CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid over contract cost/contract phase cost by percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Increases Monthly Billing By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::DECREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Decreases Monthly Billing By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Increases Monthly Billing By More Than amount.' ) ) );
			} elseif( CRouteRuleType::DECREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Decreases Monthly Billing By More Than amount.' ) ) );
			} elseif( CRouteRuleType::INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Increases Total Contract Value By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Decreases Total Contract Value By More Than Percent.' ) ) );
			} elseif( CRouteRuleType::INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Increases Total Contract Value By More Than amount.' ) ) );
			} elseif( CRouteRuleType::DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Decreases Total Contract Value By More Than amount.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid amount.' ) ) );
			}

			$boolIsValid = false;

		} else {
			if( CRouteRuleType::FINANCIAL_MOVE_OUT_STARTED_DAYS_AFTER_MOVE_OUT_DATE == $this->getRouteRuleTypeId() ) {
				if( ( int ) $this->m_fltMinimumAmount <> $this->m_fltMinimumAmount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid number of days.' ) ) );
					$boolIsValid = false;
				} elseif( 0 > ( int ) $this->m_fltMinimumAmount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Number of days cannot be negative.' ) ) );
					$boolIsValid = false;
				}
			}

			if( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_GREATER_THAN, CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_LESS_THAN ] ) ) {
				if( ( int ) $this->m_fltMinimumAmount <> $this->m_fltMinimumAmount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid number of transactions.' ) ) );
					$boolIsValid = false;
				} elseif( 0 > ( int ) $this->m_fltMinimumAmount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Number of transactions cannot be negative.' ) ) );
					$boolIsValid = false;
				}
			}

			if( 0 > ( float ) $this->m_fltMinimumAmount ) {
				if( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN ] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Refund total can not be negative.' ) ) );
					$boolIsValid = false;
				} elseif( CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN == $this->getRouteRuleTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Refund total minimum amount can not be negative.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valMaximumAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltMaximumAmount ) || true == empty( $this->m_fltMaximumAmount ) ) {
			if( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN, CRouteRuleType::TOTAL_AMOUNT_BETWEEN, CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Maximum amount.' ) ) );
				$boolIsValid = false;
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter Maximum amount By Percent.' ) ) );
				$boolIsValid = false;
			}
		} elseif( false == is_numeric( $this->m_fltMaximumAmount ) ) {
			if( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN, CRouteRuleType::TOTAL_AMOUNT_BETWEEN, CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Maximum amount.' ) ) );
				$boolIsValid = false;
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Please enter valid Maximum amount By Percent.' ) ) );
				$boolIsValid = false;
			}
		} elseif( ( true == isset( $this->m_fltMinimumAmount ) && is_numeric( $this->m_fltMinimumAmount ) ) && $this->m_fltMinimumAmount >= $this->m_fltMaximumAmount ) {
			if( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN, CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN, CRouteRuleType::TOTAL_AMOUNT_BETWEEN, CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN, CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Minimum amount should be lesser than Maximum amount.' ) ) );
				$boolIsValid = false;
			} elseif( true == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::TOTAL_BETWEEN_PERCENT, CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_amount', __( 'Minimum amount By percent should be lesser than Maximum amount By Percent.' ) ) );
				$boolIsValid = false;
			}
		} elseif( 0 > ( float ) $this->m_fltMaximumAmount ) {
			if( CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_amount', __( 'Refund total maximum amount can not be negative.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valConditionDetails() {
		$boolIsValid = true;

		$arrintConditionDetailIds = $this->getArrayConditionDetails();

		if( CRouteRuleType::TRANSACTION_REQUEST_IS_FOR == $this->getRouteRuleTypeId() && false == valArr( $arrintConditionDetailIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one option.' ) ) );
			$boolIsValid = false;
		}

		if( CRouteRuleType::TRANSACTION_CHARGE_CODE_IS == $this->getRouteRuleTypeId() && false == valArr( $arrintConditionDetailIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one charge code.' ) ) );
			$boolIsValid = false;
		}

		if( CRouteRuleType::ACCOMMODATION_IS == $this->getRouteRuleTypeId() && false == valArr( $arrintConditionDetailIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one category.' ) ) );
			$boolIsValid = false;
		}

		if( $arrintConditionDetailIds[0] == '0' ) {
			if( CRouteRuleType::SPECIFIC_JOB_CATEGORY == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select job category.' ) ) );
				$boolIsValid = false;
			}

			if( CRouteRuleType::FINANCIAL_MOVE_OUT_IS == $this->getRouteRuleTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select financial move-out type.' ) ) );
				$boolIsValid = false;
			}

			return $boolIsValid;
		}

		if( true == valArr( $arrintConditionDetailIds ) ) return $boolIsValid;

		switch( $this->getRouteRuleTypeId() ) {
			case CRouteRuleType::SPECIFIC_VENDORS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Vendor(Payee).' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_GL_ACCOUNTS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one GL Account.' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_BANK_ACCOUNTS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Bank Account.' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_COMPANY_USERS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one company user.' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_COMPANY_GROUP:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one company group.' ) ) );
				break;

			case CRouteRuleType::VENDOR_ACCESS_STATUS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Vendor (Payee) Access Status.' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_ROUTING_TAGS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Routing Tag.' ) ) );
				break;

			case CRouteRuleType::SPECIFIC_JOB_COST_CODE:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Job Cost Code.' ) ) );
				break;

			case CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_IS_ALLOCATED_TO:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one charge code.' ) ) );
				break;

			case CRouteRuleType::INVOICE_PO_TYPE_IS:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one invoice/PO type.' ) ) );
				break;

			default:
				$arrobjGroupedRuleConditionDetails = [];
		}

		$boolIsValid &= false;

		return $boolIsValid;
	}

	public function valRuleCondition() {
		$boolIsValid = true;

		if( true == in_array( $this->getRouteRuleTypeId(), CRouteRuleType::$c_arrintRouteRuleTypeIdsWithDetails ) ) {

			$boolIsValid &= $this->valConditionDetails();

		} elseif( false == in_array( $this->getRouteRuleTypeId(), [ CRouteRuleType::INVOICE_LINKED_TO_APPROVED_PO, CRouteRuleType::INVOICE_LINKED_TO_APPROVED_PO_NO, CRouteRuleType::CHANGE_ORDER_TYPE_IS, CRouteRuleType::CONTRACT_CHANGE_ORDER, CRouteRuleType::JOB_CHANGE_ORDER, CRouteRuleType::INVOICE_TOTAL_IS_EQUAL_TO_PO_TOTAL, CRouteRuleType::REFUND_INVOICE_IS_LINKED_TO_AN_FMO_THAT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING, CRouteRuleType::REFUND_PAYMENT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING ] ) ) {
			$boolIsValid &= $this->valMinimumAmount();
			$boolIsValid &= $this->valMaximumAmount();
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRuleCondition();
				break;

			case VALIDATE_DELETE:
			default:
				//
				break;
		}

		return $boolIsValid;
	}

	// Get Functions

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function getArrayConditionDetails() {
		$arrintConditionDetails = [];

		if( true == valStr( $this->getConditionDetails() ) ) {
			$arrintConditionDetails = ( array ) json_decode( $this->getConditionDetails() );
		}

		return $arrintConditionDetails;
	}

	// Set Functions

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	public function setJsonConditionDetails( $arrintDetailIds ) {
		$strJsonDetails = ( true == valArr( $arrintDetailIds ) ) ? json_encode( $arrintDetailIds ) : NULL;
		return parent::setConditionDetails( $strJsonDetails );
	}

	// TODO: Currently we need description for Route Type: Job Budget, creating the function for now, as per need it can be refactored into service - NRW
	// load condition description

	public function prepareDescription( $intRouteTypeId, $objDatabase ) {

		switch( $intRouteTypeId ) {
			case CRouteType::JOB_BUDGET:
			default:
				$this->prepareJobBudgetRuleDescription( $objDatabase );
				break;
		}
	}

	private function prepareJobBudgetRuleDescription( $objDatabase ) {
		$strRouteType = 'Job Budget';

		switch( $this->getRouteRuleTypeId() ) {
			case CRouteRuleType::TOTAL_GREATER_THAN:
				$strDescription = $strRouteType . ' amount is greater than $' . number_format( $this->getMinimumAmount(), 3 );
				break;

			case CRouteRuleType::TOTAL_LESS_THAN:
				$strDescription = $strRouteType . ' amount is less than $' . number_format( $this->getMinimumAmount(), 3 );
				break;

			case CRouteRuleType::TOTAL_BETWEEN:
				$strDescription = $strRouteType . ' amount is between $' . number_format( $this->getMinimumAmount(), 3 ) . ' and ' . number_format( $this->getMaximumAmount(), 3 );
				break;

			case CRouteRuleType::SPECIFIC_JOB_CATEGORY:
				$arrstrJobCategories = $this->fetchSimpleActiveJobCategories( $objDatabase );

				if( true == valArr( $arrstrJobCategories ) ) {
					$strDescription = $strRouteType . ' is associated with job category ';

					foreach( $arrstrJobCategories as $arrstrJobCategory ) {
						$strDescription .= $arrstrJobCategory['name'] . ', ';
					}
				}
				break;

			default:
				$strDescription = '';
				break;
		}

		$this->setDescription( $strDescription );
	}

	// Fetch Functions

	public function fetchSimpleActiveJobCategories( $objDatabase ) {
		return \Psi\Eos\Entrata\CJobCategories::fetchActiveJobCategoriesByIdsByCid( $this->getArrayConditionDetails(), $this->getCid(), $objDatabase, true );
	}

}
?>