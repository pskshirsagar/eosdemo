<?php

class CGlHeader extends CBaseGlHeader {

	const LOOKUP_TYPE_GENERAL_JOURNAL_PROPERTIES	= 'general_journal_properties';
	const FIXED_AMOUNT_METHOD						= 1;
	const ACCOUNT_BALANCE_METHOD					= 2;

	const BEGINNING_BALANCE							= 1;
	const NET_CHANGE								= 2;
	const ENDING_BALANCE							= 3;

	const CURRENT_PERIOD							= 1;
	const PRIOR_PERIOD								= 2;
	const CURRENT_QUARTER							= 3;
	const PRIOR_QUARTER								= 4;
	const CURRENT_FISCAL_QUARTER					= 5;
	const PRIOR_FISCAL_QUARTER						= 6;
	const CURRENT_CALENDER_YEAR						= 7;
	const PRIOR_CALENDER_YEAR						= 8;
	const CURRENT_FISCAL_YEAR						= 9;
	const PRIOR_FISCAL_YEAR							= 10;

	const TEMPLATE_TYPE_MANUAL						= 'manual';
	const TEMPLATE_TYPE_RECURRING					= 'recurring';

	protected $m_boolIsReverse;
	protected $m_boolIsScheduledJEPaused;
	protected $m_boolIsAlReadyCreated			= false;
	protected $m_boolIsRuleStopUpdated			= true;
	protected $m_boolIsSetToPause				= false;
	protected $m_boolIsPropertyGroup			= false;
	protected $m_boolIsAllocation				= false;
	protected $m_boolIsFromBillbackListing		= false;
	protected $m_boolIsBillbackJe				= false;
	protected $m_boolIsCompanyNumberUpdated		= false;
	protected $m_boolIsConfirmReverse;
	protected $m_boolIsAutoApprovedCreatorStop	= false;

	protected $m_intOldId;
	protected $m_intIsPaused;
	protected $m_intDayOfWeek;
	protected $m_intPropertyId;
	protected $m_intApprovedBy;
	protected $m_intDisabledBy;
	protected $m_intFrequencyId;
	protected $m_intGlDetailsCount;
	protected $m_intFrequencyInterval;
	protected $m_intJournalEntryNumber;
	protected $m_intBulkToGlAccountId;
	protected $m_intBulkFromGlAccountId;
	protected $m_intAmountMethodTypeId;
	protected $m_intNumberOfOccurrences;
	protected $m_intOffsettingHeaderNumber;
	protected $m_intGlHeaderScheduleNumber;
	protected $m_intBulkTemplateAmountTypeId;
	protected $m_intBulkTemplateBalanceFromId;
	protected $m_intBillbackPropertyId;

	protected $m_fltDebitAmount;
	protected $m_fltCreditAmount;

	protected $m_strEndDate;
	protected $m_strReverseOn;
	protected $m_strStartDate;
	protected $m_strGlBookName;
	protected $m_strDaysOfMonth;
	protected $m_strPropertyName;
	protected $m_strApprovalNote;
	protected $m_strTemplateType;
	protected $m_strCustomTagName;
	protected $m_strReversePostDate;
	protected $m_strReversePostMonth;
	protected $m_strAccountingMethod;
	protected $m_strApRoutingTagName;
	protected $m_strBulkJobPhaseName;
	protected $m_strBulkApContractName;
	protected $m_strPropertyUnitNumber;
	protected $m_strCreateTemplateFrom;
	protected $m_strBulkToGlAccountName;
	protected $m_strBulkFromGlAccountName;
	protected $m_strBulkCompanyDepartmentName;

	protected $m_objEmailDatabase;
	protected $m_objGlHeaderSchedule;

	protected $m_arrintDeletedGlDetailIds;
	protected $m_arrintCompanyUserPropertyIds;
	protected $m_arrintBillbackApDetailIds;
	protected $m_arrobjGlAccounts;

	protected $m_arrobjGlDetails;
	protected $m_arrobjErrorMsgs;
	protected $m_arrobjProperties;
	protected $m_arrobjSuccessMsgs;
	protected $m_arrobjSystemEmails;
	protected $m_arrobjFailRoutedDetails;
	protected $m_arrobjPropertyGlSettings;
	protected $m_arrobjCompanyUserPreferences;
	protected $m_arrobjMigrationModeEnabledProperties;

	protected $m_arrmixGlDetails;
	protected $m_arrobjAttachments;
	protected $m_arrmixExternalUrls;

	public static $c_strTemplateAmountType = [
		1	=> 'Beginning Balance',
		2	=> 'Net Change',
		3	=> 'Ending Balance'
	];

	public static $c_strTemplateBalanceFromType = [
		1	=> 'Current Period',
		2	=> 'Prior Period',
		3	=> 'Current Quarter',
		4	=> 'Prior Quarter',
		5	=> 'Current Fiscal Quarter',
		6	=> 'Prior Fiscal Quarter',
		7	=> 'Current Calendar Year',
		8	=> 'Prior Calendar Year',
		9	=> 'Current Fiscal Year',
		10	=> 'Prior Fiscal Year'
	];

	public function __construct() {
		parent::__construct();

		$this->m_intPropertyId = NULL;

		return;
	}

	/**
	 * Add Functions
	 */

	public function addErrorMsg( $objErrorMsg ) {
		$this->m_arrobjErrorMsgs[] = $objErrorMsg;
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			$this->m_arrobjErrorMsgs = array_merge( ( array ) $this->m_arrobjErrorMsgs, ( array ) $arrobjErrorMsgs );
		}
	}

	public function addSuccessMsg( $objSuccessMsg ) {
		$this->m_arrobjSuccessMsgs[] = $objSuccessMsg;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getIsPaused() {
		return $this->m_intIsPaused;
	}

	public function getCreditAmount() {
		return $this->m_fltCreditAmount;
	}

	public function getDebitAmount() {
		return $this->m_fltDebitAmount;
	}

	public function getFormattedPostMonth() {

		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getReversePostDate() {
		return $this->m_strReversePostDate;
	}

	public function getReversePostMonth() {
		return $this->m_strReversePostMonth;
	}

	public function getOffsettingHeaderNumber() {
		return $this->m_intOffsettingHeaderNumber;
	}

	public function getAccountingMethod() {
		return $this->m_strAccountingMethod;
	}

	public function getGlDetails() {
		return $this->m_arrobjGlDetails;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getPropertyGlSettings() {
		return $this->m_arrobjPropertyGlSettings;
	}

	public function getCompanyUserPreferences() {
		return $this->m_arrobjCompanyUserPreferences;
	}

	public function getFailRoutedDetails() {
		return $this->m_arrobjFailRoutedDetails;
	}

	public function getDetails() {
		return $this->m_arrobjGlDetails;
	}

	public function getIsRuleStopUpdated() {
		return $this->m_boolIsRuleStopUpdated;
	}

	public function getSystemEmails() {
		return $this->m_arrobjSystemEmails;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getDeletedGlDetailIds() {
		return $this->m_arrintDeletedGlDetailIds;
	}

	public function getGlHeaderScheduleNumber() {
		return $this->m_intGlHeaderScheduleNumber;
	}

	public function getIsAlReadyCreated() {
		return $this->m_boolIsAlReadyCreated;
	}

	public function getErrorMsgs() {
		return $this->m_arrobjErrorMsgs;
	}

	public function getSuccessMsgs() {
		return $this->m_arrobjSuccessMsgs;
	}

	public function getMigrationModeEnabledProperties() {
		return $this->m_arrobjMigrationModeEnabledProperties;
	}

	public function getPostedGlDetails() {
		return $this->m_arrmixGlDetails;
	}

	public function getGlDetailsCount() {
		return $this->m_intGlDetailsCount;
	}

	public function getEmailDatabase() {
		return $this->m_objEmailDatabase;
	}

	public function getGlHeaderSchedule() {
		return $this->m_objGlHeaderSchedule;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	public function getFormattedReverseOnMonth() {

		if( true == is_null( $this->m_strReversePostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strReversePostMonth );
	}

	public function getBulkCompanyDepartmentName() {
		return $this->m_strBulkCompanyDepartmentName;
	}

	public function getCustomTagName() {
		return $this->m_strCustomTagName;
	}

	public function getCreateTemplateFrom() {
		return $this->m_strCreateTemplateFrom;
	}

	public function getTemplateType() {
		return $this->m_strTemplateType;
	}

	public function getJournalEntryNumber() {
		return $this->m_intJournalEntryNumber;
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function getDayOfWeek() {
		return $this->m_intDayOfWeek;
	}

	public function getDaysOfMonth() {
		return $this->m_strDaysOfMonth;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function getIsScheduledJEPaused() {
		return $this->m_boolIsScheduledJEPaused;
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function getPropertyUnitNumber() {
		return $this->m_strPropertyUnitNumber;
	}

	public function getApRoutingTagName() {
		return $this->m_strApRoutingTagName;
	}

	public function getBulkJobPhaseName() {
		return $this->m_strBulkJobPhaseName;
	}

	public function getBulkFromGlAccountName() {
		return $this->m_strBulkFromGlAccountName;
	}

	public function getBulkToGlAccountName() {
		return $this->m_strBulkToGlAccountName;
	}

	public function getBulkFromGlAccountId() {
		return $this->m_intBulkFromGlAccountId;
	}

	public function getBulkToGlAccountId() {
		return $this->m_intBulkToGlAccountId;
	}

	public function getBulkApContractName() {
		return $this->m_strBulkApContractName;
	}

	public function getGlBookName() {
		return $this->m_strGlBookName;
	}

	public function getAmountMethodTypeId() {
		return $this->m_intAmountMethodTypeId;
	}

	public function getBulkTemplateBalanceFromId() {
		return $this->m_intBulkTemplateBalanceFromId;
	}

	public function getBulkTemplateAmountTypeId() {
		return $this->m_intBulkTemplateAmountTypeId;
	}

	public function getIsAttachment() {
		return $this->m_boolIsAttachment;
	}

	public function getIsSetToPause() {
		return $this->m_boolIsSetToPause;
	}

	public function getIsPropertyGroup() {
		return $this->m_boolIsPropertyGroup;
	}

	public function getIsAllocation() {
		return $this->m_boolIsAllocation;
	}

	public function getBillbackPropertyId() {
		return $this->m_intBillbackPropertyId;
	}

	public function getBillbackApDetailIds() {
		return $this->m_arrintBillbackApDetailIds;
	}

	public function getIsFromBillbackListing() {
		return $this->m_boolIsFromBillbackListing;
	}

	public function getIsBillbackJe() {
		return $this->m_boolIsBillbackJe;
	}

	public function getIsCompanyNumberUpdated() : bool {
		return $this->m_boolIsCompanyNumberUpdated;
	}

	public function getIsConfirmReverse() {
		return $this->m_boolIsConfirmReverse;
	}

	public function getIsAutoApprovedCreatorStop() : bool {
		return $this->m_boolIsAutoApprovedCreatorStop;
	}

	public function getGlAccounts() {
		return $this->m_arrobjGlAccounts;
	}

	public function getAttachments() {
		return $this->m_arrobjAttachments;
	}

	public function getExternalUrls() {
		return $this->m_arrmixExternalUrls;
	}

	public function getOldId() {
		return $this->m_intOldId;
	}

	/**
	 * Set Functions
	 */

	public function setGlDetails( $arrobjGlDetails ) {
		$this->m_arrobjGlDetails = $arrobjGlDetails;
	}

	public function addGlDetail( $objGlDetail ) {
		$this->m_arrobjGlDetails[$objGlDetail->getId()] = $objGlDetail;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setPropertyGlSettings( $arrobjPropertyGlSettings ) {
		$this->m_arrobjPropertyGlSettings = $arrobjPropertyGlSettings;
	}

	public function setCompanyUserPreferences( $arrobjCompanyUserPreferences ) {
		$this->m_arrobjCompanyUserPreferences = $arrobjCompanyUserPreferences;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setIsPaused( $intIsPaused ) {
		$this->m_intIsPaused = $intIsPaused;
	}

	public function setCreditAmount( $fltAmount ) {
		$this->m_fltCreditAmount = CStrings::strToFloatDef( $fltAmount, 0 );
	}

	public function setDebitAmount( $fltAmount ) {
		$this->m_fltDebitAmount = CStrings::strToFloatDef( $fltAmount, 0 );
	}

	// Override setPostMonth function. It will acccept date in dd/yyyy format and set it to dd/01/yyyy format

	public function setPostMonth( $strPostMonth ) {

		$this->m_strPostMonth = CStrings::strTrimDef( $strPostMonth, -1, NULL, true );

		// Overwrite it in correct format if not null
		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$this->m_strPostMonth = $strPostMonth;
		}
	}

	public function setReversePostDate( $strReversePostDate ) {
		$this->m_strReversePostDate = $strReversePostDate;
	}

	public function setReversePostMonth( $strReversePostMonth ) {

		if( false == is_null( $strReversePostMonth ) ) {

			$arrstrReversePostMonth = explode( '/', $strReversePostMonth );

			if( true == valArr( $arrstrReversePostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrReversePostMonth ) ) {
				$strReversePostMonth = $arrstrReversePostMonth[0] . '/01/' . $arrstrReversePostMonth[1];
			}

		}
		$this->m_strReversePostMonth = $strReversePostMonth;
	}

	public function setOffsettingHeaderNumber( $intOffsettingHeaderNumber ) {
		$this->m_intOffsettingHeaderNumber = $intOffsettingHeaderNumber;
	}

	public function setAccountingMethod( $strAccountingMethod ) {
		$this->m_strAccountingMethod = $strAccountingMethod;
	}

	public function setFailRoutedDetails( $arrobjFailRoutedDetails ) {
		$this->m_arrobjFailRoutedDetails = $arrobjFailRoutedDetails;
	}

	public function setDetails( $arrobjGlDetails ) {
		$this->m_arrobjGlDetails = $arrobjGlDetails;
	}

	public function setIsRuleStopUpdated( $boolIsRuleStopUpdated ) {
		$this->m_boolIsRuleStopUpdated = $boolIsRuleStopUpdated;
	}

	public function setSystemEmails( $objSysyemEmail, $strKey ) {
		$this->m_arrobjSystemEmails[$strKey] = $objSysyemEmail;
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		return $this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setDeletedGlDetailIds( $arrintDeletedGlDetailIds ) {
		return $this->m_arrintDeletedGlDetailIds = $arrintDeletedGlDetailIds;
	}

	public function setIsAlReadyCreated( $boolIsAlReadyCreated ) {
		return $this->m_boolIsAlReadyCreated = $boolIsAlReadyCreated;
	}

	public function setGlHeaderScheduleNumber( $intGlHeaderScheduleNumber ) {
		return $this->m_intGlHeaderScheduleNumber = $intGlHeaderScheduleNumber;
	}

	public function setMigrationModeEnabledProperties( $arrobjMigrationModeEnabledProperties ) {
		$this->m_arrobjMigrationModeEnabledProperties = $arrobjMigrationModeEnabledProperties;
	}

	public function setPostedGlDetails( $arrmixGlDetails ) {
		$this->m_arrmixGlDetails = $arrmixGlDetails;
	}

	public function setGlDetailsCount( $intGlDetailsCount ) {
		$this->m_intGlDetailsCount = $intGlDetailsCount;
	}

	public function setEmailDatabase( $objEmailDatabase ) {
		$this->m_objEmailDatabase = $objEmailDatabase;
	}

	public function setGlHeaderSchedule( $objGlHeaderSchedule ) {
		$this->m_objGlHeaderSchedule = $objGlHeaderSchedule;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function setBulkCompanyDepartmentName( $strBulkCompanyDepartmentName ) {
		$this->m_strBulkCompanyDepartmentName = $strBulkCompanyDepartmentName;
	}

	public function setCustomTagName( $strCustomTagName ) {
		$this->m_strCustomTagName = $strCustomTagName;
	}

	public function setCreateTemplateFrom( $strCreateTemplateFrom ) {
		$this->m_strCreateTemplateFrom = $strCreateTemplateFrom;
	}

	public function setTemplateType( $strTemplateType ) {
		$this->m_strTemplateType = $strTemplateType;
	}

	public function setAmountMethodTypeId( $intAmountMethodTypeId ) {
		$this->m_intAmountMethodTypeId = $intAmountMethodTypeId;
	}

	public function setBulkTemplateBalanceFromId( $intBulkTemplateBalanceFromId ) {
		$this->m_intBulkTemplateBalanceFromId = $intBulkTemplateBalanceFromId;
	}

	public function setBulkTemplateAmountTypeId( $intBulkTemplateAmountTypeId ) {
		$this->m_intBulkTemplateAmountTypeId = $intBulkTemplateAmountTypeId;
	}

	public function setJournalEntryNumber( $intJournalEntryNumber ) {
		$this->m_intJournalEntryNumber = $intJournalEntryNumber;
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->m_intFrequencyId = CStrings::strToIntDef( $intFrequencyId, NULL, false );
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->m_intFrequencyInterval = CStrings::strToIntDef( $intFrequencyInterval, NULL, false );
	}

	public function setDayOfWeek( $intDayOfWeek ) {
		$this->m_intDayOfWeek = CStrings::strToIntDef( $intDayOfWeek, NULL, false );
	}

	public function setDaysOfMonth( $strDaysOfMonth ) {
		$this->m_strDaysOfMonth = CStrings::strTrimDef( $strDaysOfMonth, 255, NULL, true );
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = CStrings::strTrimDef( $strStartDate, -1, NULL, true );
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = CStrings::strTrimDef( $strEndDate, -1, NULL, true );
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->m_intNumberOfOccurrences = CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false );
	}

	public function setIsScheduledJEPaused( $boolIsScheduledJEPaused ) {
		$this->m_boolIsScheduledJEPaused = $boolIsScheduledJEPaused;
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->m_intApprovedBy = CStrings::strToIntDef( $intApprovedBy, NULL, false );
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->m_intDisabledBy = CStrings::strToIntDef( $intDisabledBy, NULL, false );
	}

	public function setPropertyUnitNumber( $strPropertyUnitNumber ) {
		$this->m_strPropertyUnitNumber = $strPropertyUnitNumber;
	}

	public function setApRoutingTagName( $strApRoutingTagName ) {
		$this->m_strApRoutingTagName = $strApRoutingTagName;
	}

	public function setBulkJobPhaseName( $strBulkJobPhaseName ) {
		$this->m_strBulkJobPhaseName = $strBulkJobPhaseName;
	}

	public function setBulkFromGlAccountId( $intBulkFromGlAccountId ) {
		$this->m_intBulkFromGlAccountId = $intBulkFromGlAccountId;
	}

	public function setBulkToGlAccountId( $intBulkToGlAccountId ) {
		$this->m_intBulkToGlAccountId = $intBulkToGlAccountId;
	}

	public function setBulkFromGlAccountName( $strBulkFromGlAccountName ) {
		$this->m_strBulkFromGlAccountName = $strBulkFromGlAccountName;
	}

	public function setBulkToGlAccountName( $strBulkToGlAccountName ) {
		$this->m_strBulkToGlAccountName = $strBulkToGlAccountName;
	}

	public function setBulkApContractName( $strBulkApContractName ) {
		$this->m_strBulkApContractName = $strBulkApContractName;
	}

	public function setGlBookName( $strGlBookName ) {
		$this->m_strGlBookName = $strGlBookName;
	}

	public function setIsAttachment( $boolIsAttachment ) {
		$this->m_boolIsAttachment = $boolIsAttachment;
	}

	public function setIsSetToPause( $boolIsSetToPause ) {
		$this->m_boolIsSetToPause = $boolIsSetToPause;
	}

	public function setIsPropertyGroup( $boolIsPropertyGroup ) {
		$this->m_boolIsPropertyGroup = $boolIsPropertyGroup;
	}

	public function setIsAllocation( $boolIsAllocation ) {
		$this->m_boolIsAllocation = $boolIsAllocation;
	}

	public function setBillbackPropertyId( $intBillbackPropertyId ) {
		$this->m_intBillbackPropertyId = CStrings::strToIntDef( $intBillbackPropertyId, NULL, false );
	}

	public function setBillbackApDetailIds( $arrintBillbackApDetailIds ) {
		return $this->m_arrintBillbackApDetailIds = $arrintBillbackApDetailIds;
	}

	public function setIsFromBillbackListing( $boolIsFromBillbackListing ) {
		$this->m_boolIsFromBillbackListing = $boolIsFromBillbackListing;
	}

	public function setIsBillbackJe( $boolIsBillbackJe ) {
		$this->m_boolIsBillbackJe = $boolIsBillbackJe;
	}

	public function setIsCompanyNumberUpdated( bool $boolIsCompanyNumberUpdated ) {
		$this->m_boolIsCompanyNumberUpdated = $boolIsCompanyNumberUpdated;
	}

	public function setIsConfirmReverse( $boolIsConfirmReverse ) {
		$this->m_boolIsConfirmReverse = $boolIsConfirmReverse;
	}

	public function setIsAutoApprovedCreatorStop( bool $boolIsAutoApprovedCreatorStop ) {
		$this->m_boolIsAutoApprovedCreatorStop = $boolIsAutoApprovedCreatorStop;
	}

	public function setGlAccounts( $arrobjGlAccounts ) {
		$this->m_arrobjGlAccounts = $arrobjGlAccounts;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['credit_amount'] ) ) {
			$this->setCreditAmount( $arrmixValues['credit_amount'] );
		}
		if( true == isset( $arrmixValues['debit_amount'] ) ) {
			$this->setDebitAmount( $arrmixValues['debit_amount'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['reverse_post_date'] ) ) {
			$this->setReversePostDate( $arrmixValues['reverse_post_date'] );
		}
		if( true == isset( $arrmixValues['reverse_post_month'] ) ) {
			$this->setReversePostMonth( $arrmixValues['reverse_post_month'] );
		}
		if( true == isset( $arrmixValues['offsetting_header_number'] ) ) {
			$this->setOffsettingHeaderNumber( $arrmixValues['offsetting_header_number'] );
		}
		if( true == isset( $arrmixValues['header_number'] ) ) {
			$this->setOffsettingHeaderNumber( $arrmixValues['header_number'] );
		}
		if( true == isset( $arrmixValues['accounting_method'] ) ) {
			$this->setAccountingMethod( $arrmixValues['accounting_method'] );
		}
		if( true == isset( $arrmixValues['scheduled_header_number'] ) ) {
			$this->setGlHeaderScheduleNumber( $arrmixValues['scheduled_header_number'] );
		}
		if( true == isset( $arrmixValues['bulk_company_department_name'] ) ) {
			$this->setBulkCompanyDepartmentName( $arrmixValues['bulk_company_department_name'] );
		}
		if( true == isset( $arrmixValues['custom_tag_name'] ) ) {
			$this->setCustomTagName( $arrmixValues['custom_tag_name'] );
		}
		if( true == isset( $arrmixValues['template_type'] ) ) {
			$this->setTemplateType( $arrmixValues['template_type'] );
		}
		if( true == isset( $arrmixValues['create_template_from'] ) ) {
			$this->setCreateTemplateFrom( $arrmixValues['create_template_from'] );
		}
		if( true == isset( $arrmixValues['amount_method_type_id'] ) ) {
			$this->setAmountMethodTypeId( $arrmixValues['amount_method_type_id'] );
		}
		if( true == isset( $arrmixValues['journal_entry_number'] ) ) {
			$this->setJournalEntryNumber( $arrmixValues['journal_entry_number'] );
		}
		if( true == isset( $arrmixValues['bulk_job_phase_name'] ) ) {
			$this->setBulkJobPhaseName( $arrmixValues['bulk_job_phase_name'] );
		}
		if( true == isset( $arrmixValues['bulk_ap_contract_name'] ) ) {
			$this->setBulkApContractName( $arrmixValues['bulk_ap_contract_name'] );
		}

		if( true == isset( $arrmixValues['bulk_template_amount_type_id'] ) ) {
			$this->setBulkTemplateAmountTypeId( $arrmixValues['bulk_template_amount_type_id'] );
		}
		if( true == isset( $arrmixValues['bulk_template_balance_from_id'] ) ) {
			$this->setBulkTemplateBalanceFromId( $arrmixValues['bulk_template_balance_from_id'] );
		}
		if( true == isset( $arrmixValues['bulk_from_gl_account_id'] ) ) {
			$this->setBulkFromGlAccountId( $arrmixValues['bulk_from_gl_account_id'] );
		}
		if( true == isset( $arrmixValues['bulk_to_gl_account_id'] ) ) {
			$this->setBulkToGlAccountId( $arrmixValues['bulk_to_gl_account_id'] );
		}
		if( true == isset( $arrmixValues['bulk_from_gl_account_name'] ) ) {
			$this->setBulkFromGlAccountName( $arrmixValues['bulk_from_gl_account_name'] );
		}
		if( true == isset( $arrmixValues['bulk_to_gl_account_name'] ) ) {
			$this->setBulkToGlAccountName( $arrmixValues['bulk_to_gl_account_name'] );
		}

		if( true == isset( $arrmixValues['frequency_id'] ) ) {
			$this->setFrequencyId( $arrmixValues['frequency_id'] );
		}
		if( true == isset( $arrmixValues['frequency_interval'] ) ) {
			$this->setFrequencyInterval( $arrmixValues['frequency_interval'] );
		}
		if( true == isset( $arrmixValues['day_of_week'] ) ) {
			$this->setDayOfWeek( $arrmixValues['day_of_week'] );
		}
		if( true == isset( $arrmixValues['days_of_month'] ) ) {
			$this->setDaysOfMonth( $arrmixValues['days_of_month'] );
		}
		if( true == isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( $arrmixValues['start_date'] );
		}
		if( true == isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( $arrmixValues['end_date'] );
		}
		if( true == isset( $arrmixValues['number_of_occurrences'] ) ) {
			$this->setNumberOfOccurrences( $arrmixValues['number_of_occurrences'] );
		}
		if( true == isset( $arrmixValues['is_scheduled_je_paused'] ) ) {
			$this->setIsScheduledJEPaused( $arrmixValues['is_scheduled_je_paused'] );
		}
		if( true == isset( $arrmixValues['approved_by'] ) ) {
			$this->setApprovedBy( $arrmixValues['approved_by'] );
		}
		if( true == isset( $arrmixValues['disabled_by'] ) ) {
			$this->setDisabledBy( $arrmixValues['disabled_by'] );
		}
		if( true == isset( $arrmixValues['property_unit_number'] ) ) {
			$this->setPropertyUnitNumber( $arrmixValues['property_unit_number'] );
		}
		if( true == isset( $arrmixValues['is_reverse'] ) ) {
			$this->setIsReverse( $arrmixValues['is_reverse'] );
		}
		if( true == isset( $arrmixValues['ap_routing_tag_name'] ) ) {
			$this->setApRoutingTagName( $arrmixValues['ap_routing_tag_name'] );
		}
		if( true == isset( $arrmixValues['book_name'] ) ) {
			$this->setGlBookName( $arrmixValues['book_name'] );
		}
		if( true == isset( $arrmixValues['is_attachment'] ) ) {
			$this->setIsAttachment( $arrmixValues['is_attachment'] );
		}

		if( true == isset( $arrmixValues['is_property_group'] ) ) {
			$this->setIsPropertyGroup( $arrmixValues['is_property_group'] );
		}

		if( true == isset( $arrmixValues['is_allocation'] ) ) {
			$this->setIsAllocation( $arrmixValues['is_allocation'] );
		}

		if( true == isset( $arrmixValues['billback_property_id'] ) )		$this->setBillbackPropertyId( $arrmixValues['billback_property_id'] );
		if( true == isset( $arrmixValues['billback_ap_detail_ids'] ) )		$this->setBillbackApDetailIds( $arrmixValues['billback_ap_detail_ids'] );
		if( true == isset( $arrmixValues['is_from_billback_listing'] ) )	$this->setIsFromBillbackListing( $arrmixValues['is_from_billback_listing'] );
		if( true == isset( $arrmixValues['gl_accounts'] ) )					$this->setGlAccounts( $arrmixValues['gl_accounts'] );
		if( true == isset( $arrmixValues['is_billback_je'] ) )	            $this->setIsBillbackJe( $arrmixValues['is_billback_je'] );

		return;
	}

	public function setAttachments( $arrobjAttachments ) {
		$this->m_arrobjAttachments = $arrobjAttachments;
	}

	public function addAttachment( $objAttachment ) {
		$this->m_arrobjAttachments[$objAttachment->getId()] = $objAttachment;
	}

	public function setExternalUrls( $arrmixExternalUrls ) {
		$this->m_arrmixExternalUrls = $arrmixExternalUrls;
	}

	public function setOldId( $intId ) {
		$this->m_intOldId = $intId;
	}

	public function setErrorMsgs( array $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	/**
	 * Validate Function
	 */

	public function validate( $strAction, $objCompanyUser = NULL, $boolIsSkipLineItems = true, $objDatabase = NULL, $boolEditExportedJePermission = false, $boolIsPropertyGlLocked = false, $boolIsReverse = false, $arrmixParameters = [], $boolJeAllowPostingToRestrictedGlAccount = false, $boolIsFromJeLibarary = false, $boolShowDisabledData = false, $boolIsImportJe = false, $arrintBillbackApDetailIds = [], $boolIsFromBillbackListing = false ) {
		$boolIsValid = true;

		$objGlHeaderValidator = new CGlHeaderValidator();
		$objGlHeaderValidator->setGlHeader( $this );
		$objGlHeaderValidator->setGlDetails( $this->getGlDetails() );
		$boolIsValid &= $objGlHeaderValidator->validate( $strAction, $objCompanyUser, $boolIsSkipLineItems, $objDatabase, $boolEditExportedJePermission, $boolIsPropertyGlLocked, $boolIsReverse, $arrmixParameters, $boolJeAllowPostingToRestrictedGlAccount, $boolIsFromJeLibarary, $boolShowDisabledData, $boolIsImportJe, $arrintBillbackApDetailIds, $boolIsFromBillbackListing );

		return $boolIsValid;
	}

	public function loadReverseDefaultData() {

		if( strtotime( date( 'm/d/y', strtotime( $this->getPostDate() ) ) ) < strtotime( date( 'm/d/y' ) ) ) {
			$this->setReversePostDate( ( date( 'm/d/Y' ) ) );
		} else {
			$this->setReversePostDate( date( 'm/d/Y', strtotime( $this->getPostDate() ) ) );
		}

		if( strtotime( $this->getPostMonth() ) < strtotime( date( 'm/d/y' ) ) ) {
			$this->setReversePostMonth( preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', date( 'm/d/Y' ) ) );
		}
	}

	public function createGlDetail() {

		$objGlDetail = new CGlDetail();
		$objGlDetail->setDefaults();
		$objGlDetail->setGlHeaderId( $this->getId() );
		$objGlDetail->setCid( $this->getCid() );
		$objGlDetail->setGlTransactionTypeId( $this->getGlTransactionTypeId() );
		$objGlDetail->setPostMonth( $this->getPostMonth() );

		return $objGlDetail;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setGlHeaderId( $this->getId() );

		return $objFileAssociation;
	}

	public function fetchGlDetails( $objDatabase ) {
		return CGlDetails::fetchCustomGlDetailsByGlHeaderIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFiles( $objDatabase ) {
		return CFiles::fetchFilesByGlHeaderIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == parent::insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		$intLastInsertedGlDetailId = NULL;

		if( true == valArr( $this->getDetails() ) ) {
			foreach( $this->getDetails() as $objGlDetail ) {

				$objGlDetail->setGlHeaderId( $this->getId() );

				// Below code written to add id of from_gl_account_id to to_gl_account_id of offsetting gl account id for reference to identify from and to gl account.
				if( true == valId( $objGlDetail->getTemplateAmountTypeId() ) && true == valId( $objGlDetail->getTemplateBalanceFromId() ) && 1 == $objGlDetail->getAmount() ) {
					$objGlDetail->setOffsettingGlDetailId( $intLastInsertedGlDetailId );
				}

				if( true == in_array( $objGlDetail->getGlTransactionTypeId(), CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) ) {
					$objGlDetail->setReferenceId( $this->getReferenceId() );
				}

				if( false == $objGlDetail->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objGlDetail->getErrorMsgs() );
					return false;
				}

				$intLastInsertedGlDetailId = $objGlDetail->getId();
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == parent::update( $intCurrentUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}
		$intLastInsertedGlDetailId = NULL;

		if( true == valArr( $this->getDetails() ) ) {
			foreach( $this->getDetails() as $objGlDetail ) {
				if( true == valId( $objGlDetail->getTemplateAmountTypeId() ) && true == valId( $objGlDetail->getTemplateBalanceFromId() ) && 1 == $objGlDetail->getAmount() ) {
					$objGlDetail->setOffsettingGlDetailId( $intLastInsertedGlDetailId );
				}
				$objGlDetail->setGlHeaderId( $this->getId() );

				if( false == $objGlDetail->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					$this->addErrorMsgs( $objGlDetail->getErrorMsgs() );
					return false;
				}
				$intLastInsertedGlDetailId = $objGlDetail->getId();
			}
		}

		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == parent::insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}
		$intLastInsertedGlDetailId = NULL;
		if( true == valArr( $this->getDetails() ) ) {
			foreach( $this->getDetails() as $objGlDetail ) {

				$objGlDetail->setGlHeaderId( $this->getId() );

				if( true == valId( $objGlDetail->getTemplateAmountTypeId() ) && true == valId( $objGlDetail->getTemplateBalanceFromId() ) && 1 == $objGlDetail->getAmount() && false == valId( $objGlDetail->getOffsettingGlDetailId() ) ) {
					$objGlDetail->setOffsettingGlDetailId( $intLastInsertedGlDetailId );
				}

				if( false == $objGlDetail->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objGlDetail->getErrorMsgs() );
					$objDatabase->rollback();
					return false;
				}
				$intLastInsertedGlDetailId = $objGlDetail->getId();
			}
		}

		return true;
	}

	public function approveJournalEntry( $intCompanyUserId, $objDatabase, $boolIsCreatorAutoApproved = false ) {

		$this->setGlHeaderStatusTypeId( CGlHeaderStatusType::POSTED );

		if( !is_null( $this->getReversePostMonth() ) ) {
			$this->setGlHeaderStatusTypeId( CGlHeaderStatusType::REVERSED );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function isModifiedJournalEntry( $objOldGlHeader ) {

		$boolIsUpdated = false;

		// Compare for the gl_header
		if( $this->getGlHeaderTypeId() != $objOldGlHeader->getGlHeaderTypeId()
			|| $this->getReference() != $objOldGlHeader->getReference()
			|| date('m/d/Y', strtotime($this->getPostDate()))  != date('m/d/Y', strtotime($objOldGlHeader->getPostDate()))
			|| $this->getPostMonth() != $objOldGlHeader->getPostMonth()
			|| $this->getGlBookId() != $objOldGlHeader->getGlBookId()
			|| $this->getMemo() != $objOldGlHeader->getMemo()
			|| $this->getReversePostMonth() != $objOldGlHeader->getReversePostMonth()
			|| $this->getBulkPropertyId() != $objOldGlHeader->getBulkPropertyId()
			|| $this->getBulkCompanyDepartmentId() != $objOldGlHeader->getBulkCompanyDepartmentId()
			|| $this->getBulkPropertyUnitId() != $objOldGlHeader->getBulkPropertyUnitId()
			|| $this->getBulkPropertyBuildingId() != $objOldGlHeader->getBulkPropertyBuildingId()
			|| $this->getBulkGlDimensionId() != $objOldGlHeader->getBulkGlDimensionId()
			|| $this->getBulkIsConfidential() != $objOldGlHeader->getBulkIsConfidential()
			|| $this->getApRoutingTagId() != $objOldGlHeader->getApRoutingTagId()
			|| $this->getBulkJobPhaseId() != $objOldGlHeader->getBulkJobPhaseId()
			|| $this->getBulkApContractId() != $objOldGlHeader->getBulkApContractId()
			|| $this->getGlHeaderStatusTypeId() != $objOldGlHeader->getGlHeaderStatusTypeId()
			|| $this->getTemplateName() != $objOldGlHeader->getTemplateName() ) {
			$boolIsUpdated = true;
		}

		if( false == $boolIsUpdated ) {
			$arrstrExternalUrlUpdate = ( array ) json_decode( $this->getExternalUrl(), true );
			$arrstrExternalUrl       = ( array ) json_decode( $objOldGlHeader->getExternalUrl(), true );
			if( true == valArr( array_diff_assoc( $arrstrExternalUrlUpdate, $arrstrExternalUrl ) ) || true == valArr( array_diff_assoc( $arrstrExternalUrl, $arrstrExternalUrlUpdate ) ) ) {
				$boolIsUpdated = true;
			}
		}

		if( true == $objOldGlHeader->getIsTemplate() && false == valId( $objOldGlHeader->getGlHeaderScheduleId() ) && 'recurring' == $this->getTemplateType() ) {
			$boolIsUpdated = true;
		}

		// Compare the gl_details
		if( false == $boolIsUpdated ) {

			$arrobjNewGlDetails = $this->getGlDetails();
			$arrobjOldGlDetails = $objOldGlHeader->getGlDetails();

			// Check if the new line item added or existing has modified.
			foreach( $arrobjNewGlDetails as $objNewGlDetail ) {

				if( true == array_key_exists( $objNewGlDetail->getId(), $arrobjOldGlDetails ) ) {

					$objOldGlDetail = $arrobjOldGlDetails[$objNewGlDetail->getId()];

					if( $objOldGlDetail->getPropertyId() != $objNewGlDetail->getPropertyId()
						|| $objOldGlDetail->getPropertyUnitId() != $objNewGlDetail->getPropertyUnitId()
						|| $objOldGlDetail->getPropertyBuildingId() != $objNewGlDetail->getPropertyBuildingId()
						|| $objOldGlDetail->getAccrualGlAccountId() != $objNewGlDetail->getAccrualGlAccountId()
						|| $objOldGlDetail->getCashGlAccountId() != $objNewGlDetail->getCashGlAccountId()
						|| $objOldGlDetail->getPeriodId() != $objNewGlDetail->getPeriodId()
						|| $objOldGlDetail->getAmount() != $objNewGlDetail->getAmount()
						|| $objOldGlDetail->getMemo() != $objNewGlDetail->getMemo()
						|| $objOldGlDetail->getCompanyDepartmentId() != $objNewGlDetail->getCompanyDepartmentId()
						|| $objOldGlDetail->getGlDimensionId() != $objNewGlDetail->getGlDimensionId()
						|| $objOldGlDetail->getIsConfidential() != $objNewGlDetail->getIsConfidential()
						|| $objOldGlDetail->getJobPhaseId() != $objNewGlDetail->getJobPhaseId()
						|| $objOldGlDetail->getApCodeId() != $objNewGlDetail->getApCodeId()
						|| $objOldGlDetail->getApContractId() != $objNewGlDetail->getApContractId()
					    || $objOldGlDetail->getUnitTypeId() != $objNewGlDetail->getUnitTypeId()
					    || $objOldGlDetail->getMaintenanceLocationId() != $objNewGlDetail->getMaintenanceLocationId()
					    || $objOldGlDetail->getTemplateAmountTypeId() != $objNewGlDetail->getTemplateAmountTypeId()
					    || $objOldGlDetail->getTemplateBalanceFromId() != $objNewGlDetail->getTemplateBalanceFromId() ) {
						$boolIsUpdated = true;
					}
				} else {
					$boolIsUpdated = true;
					break;
				}
			}

			// Check if the line items has deleted.
			if( false == $boolIsUpdated ) {

				foreach( $arrobjOldGlDetails as $objOldGlDetail ) {

					if( false == array_key_exists( $objOldGlDetail->getId(), $arrobjNewGlDetails ) ) {
						$boolIsUpdated = true;
						break;
					}
				}
			}
		}

		return $boolIsUpdated;
	}

	public function createGlHeaderLog() {

		$objGlHeaderLog = new CGlHeaderLog();

		$objGlHeaderLog->setCid( $this->getCid() );
		$objGlHeaderLog->setGlHeaderId( $this->getId() );
		$objGlHeaderLog->setClosePeriodId( $this->getClosePeriodId() );
		$objGlHeaderLog->setGlHeaderTypeId( $this->getGlHeaderTypeId() );
		$objGlHeaderLog->setGlTransactionTypeId( $this->getGlTransactionTypeId() );
		$objGlHeaderLog->setGlHeaderStatusTypeId( $this->getGlHeaderStatusTypeId() );
		$objGlHeaderLog->setOffsettingGlHeaderId( $this->getOffsettingGlHeaderId() );
		$objGlHeaderLog->setGlHeaderScheduleId( $this->getGlHeaderScheduleId() );
		$objGlHeaderLog->setTemplateGlHeaderId( $this->getTemplateGlHeaderId() );
		$objGlHeaderLog->setGlBookId( $this->getGlBookId() );
		$objGlHeaderLog->setHeaderNumber( $this->getHeaderNumber() );
		$objGlHeaderLog->setTransactionDatetime( $this->getTransactionDatetime() );
		$objGlHeaderLog->setPostMonth( $this->getPostMonth() );
		$objGlHeaderLog->setPostDate( $this->getPostDate() );
		$objGlHeaderLog->setReference( $this->getReference() );
		$objGlHeaderLog->setMemo( $this->getMemo() );
		$objGlHeaderLog->setApprovalNote( $this->getApprovalNote() );
		$objGlHeaderLog->setBulkCompanyDepartmentId( $this->getBulkCompanyDepartmentId() );
		$objGlHeaderLog->setBulkGlDimensionId( $this->getBulkGlDimensionId() );
		$objGlHeaderLog->setBulkIsConfidential( $this->getBulkIsConfidential() );
		$objGlHeaderLog->setBulkPropertyId( $this->getBulkPropertyId() );
		$objGlHeaderLog->setBulkPropertyUnitId( $this->getBulkPropertyUnitId() );
		$objGlHeaderLog->setReversePostMonth( $this->getReversePostMonth() );

		return $objGlHeaderLog;
	}

	public function createEvent( $intEventTypeId, $intEventSubTypeId = NULL, $strEventHandle = NULL, $strNotes = NULL, $objDatabase = NULL ) {
		if( false == valId( $intEventTypeId ) ) return NULL;

		$objEvent = new CEvent();
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventTypeId( $intEventTypeId );
		$objEvent->setEventSubTypeId( $intEventSubTypeId );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setEventHandle( $strEventHandle );
		$objEvent->setNotes( $strNotes );

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$arrintPropertyIds = $this->getManagerialPropertyIds( $objDatabase );
			if( true == valArr( $arrintPropertyIds ) ) {
				$objEvent->setPropertyId( current( $arrintPropertyIds ) );
			}
		}

		$objEventReference = $objEvent->createEventReference();
		$objEventReference->setEventReferenceTypeId( CEventReferenceType::JOURNAL_ENTRY_TEMPLATE );
		$objEventReference->setReferenceId( $this->getId() );

		$objEvent->setEventReferences( [ $objEventReference ] );

		return $objEvent;
	}

	public function logHistory( $strLogAction, $intCurrentUserId, $objDatabase ) {

		$arrobjGlDetailLogs = [];

		$objGlHeaderLog		= $this->createGlHeaderLog();
		$arrobjGlDetails	= ( array ) $this->getGlDetails();

		$objGlHeaderLog->setAction( $strLogAction );

		if( in_array( $strLogAction, [ \CGlHeaderLog::ACTION_ATTACHMENT_ADDED, \CGlHeaderLog::ACTION_ATTACHMENT_REMOVED ] ) ) {

			$arrobjOldGlDetailLogs = ( array ) \Psi\Eos\Entrata\CGlDetailLogs::createService()->fetchPreviousGlDetailLogsByGlHeaderIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			foreach( $arrobjOldGlDetailLogs as $objGlDetailLog ) {
				$objGlDetailLog->setId( NULL );
				$arrobjGlDetailLogs[]	= $objGlDetailLog;
			}
		} else {
			foreach( $arrobjGlDetails as $objGlDetail ) {
				$objGlDetailLog			= $objGlDetail->createGlDetailLog();
				$arrobjGlDetailLogs[]	= $objGlDetailLog;
			}
		}

		if( false == $objGlHeaderLog->insert( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objGlHeaderLog->getErrorMsgs() );
			return false;
		}

		foreach( $arrobjGlDetailLogs as $objGlDetailLog ) {

			$objGlDetailLog->setGlHeaderLogId( $objGlHeaderLog->getId() );

			if( !valId( $objGlDetailLog->getGlHeaderId() ) ) {
				$objGlDetailLog->setGlHeaderId( $this->getId() );
			}

			if( false == $objGlDetailLog->insert( $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objGlDetailLog->getErrorMsgs() );
				return false;
			}
		}
		return true;
	}

	public function validUrl( $strUrl ) {

		$boolIsValid = true;

		if( false == is_null( $strUrl ) && false == filter_var( $strUrl, FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Invalid URL.' ) );

		}
		return $boolIsValid;
	}

	public function rejectJournalEntry( $intCurrentUserId, $objClientDatabase ) {

		if( CGlHeaderStatusType::REJECTED != $this->getGlHeaderStatusTypeId() ) return false;

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	/**
	 * @param $objDatabase
	 * @return array
	 */
	public function getManagerialPropertyIds( $objDatabase ) : array {
		return ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertyIdsByCid( $this->getCid(), $objDatabase );
	}

}
?>