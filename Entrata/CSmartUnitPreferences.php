<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartUnitPreferences
 * Do not add any new functions to this class.
 */

class CSmartUnitPreferences extends CBaseSmartUnitPreferences {

	public static function fetchSmartUnitPreferencesByKeyByPropertyUnitIdsByCid( $strKey, $arrstrPropertyUnitIds, $intCid, $objDatabase ) : array {

		if( false === valArr( $arrstrPropertyUnitIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						sup.*
					FROM
						smart_unit_preferences sup
					WHERE
						sup.cid = ' . ( int ) $intCid . '
						AND sup.property_unit_id IN( ' . implode( ',', $arrstrPropertyUnitIds ) . ' )
						AND sup.key ILIKE  \'' . $strKey . '\'';

		return ( array ) self::fetchSmartUnitPreferences( $strSql, $objDatabase );
	}

	public static function fetchSmartUnitPreferencesOfPastDateByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) : array {

		if( false === valId( $intPropertyId ) || false === valId( $intCid ) ) {
			return [];
		}

		$strSql = 'SELECT
						sup.*,
						TO_CHAR (( sup.value :: DATE ), \'MM / DD / YYYY\' )  as expired_reset_date
					FROM
						smart_unit_preferences sup
						JOIN property_units AS pu ON ( sup.property_unit_id = pu.id )
						JOIN properties AS p on ( p.id = pu.property_id)
					WHERE
						sup.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '
						AND CAST( TO_CHAR((sup.value::DATE ), \'MM/DD/YYYY\') AS TIMESTAMP ) < CURRENT_TIMESTAMP';

		return ( array ) self::fetchSmartUnitPreferences( $strSql, $objDatabase );
	}

	public static function fetchSmartUnitPreferenceByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {

		if( false === valId( $intPropertyUnitId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
            *
          FROM 
            smart_unit_preferences
          WHERE 
            cid = ' . ( int ) $intCid . ' 
            AND property_unit_id = ' . ( int ) $intPropertyUnitId . ' 
          ORDER BY
            ' . $objDatabase->getCollateSort( 'id' ) . ' ASC';

		return self::fetchSmartUnitPreference( $strSql, $objDatabase );
	}

}
?>