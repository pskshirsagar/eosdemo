<?php

class CArAllocationCriteriaType extends CBaseArAllocationCriteriaType {

	const CHARGE_CODE_PRIORITY              = 2;
	const CURRENT_POST_MONTH                = 5;
	const FUTURE_POST_MONTHS                = 6;

	public static $c_arrintCriteriaTypeIdsWithoutSortMethod	= [ self::CHARGE_CODE_PRIORITY, self::CURRENT_POST_MONTH, self::FUTURE_POST_MONTHS ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOptional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
