<?php

class CPropertyIdentificationType extends CBasePropertyIdentificationType {

	protected $m_strSystemCode;

    public function __construct() {
        parent::__construct();

        $this->m_strSystemCode = NULL;

        return;
    }

    /**
     * Get Functions
     */

    public function getSystemCode() {
        return $this->m_strSystemCode;
    }

    /**
     * Set Functions
     */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
	}

    public function setSystemCode( $strSystemCode ) {
        $this->m_strSystemCode = CStrings::strTrimDef( $strSystemCode, 10, NULL, true );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyIdentificationTypeId( $objDatabase ) {
        $boolIsValid = true;

        if( true == isset( $objDatabase ) ) {

        	$objPropertyIdentificationType = CPropertyIdentificationTypes::fetchPropertyIdentificationTypeByPropertyIdByCompanyIdentificationTypeIdByCid( $this->getPropertyId(), $this->getCompanyIdentificationTypeId(), $this->getCid(), $objDatabase );

        	if( true == valObj( $objPropertyIdentificationType, 'CPropertyIdentificationType' ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_identification_type', __( 'Identification type {%s,0} is already associated with the property.', [ $objPropertyIdentificationType->getSystemCode() ] ) ) );
        	}
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCompanyIdentificationTypeId( $objDatabase );
            	$boolIsValid &= $this->valCid();
				break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>