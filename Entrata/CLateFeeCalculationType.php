<?php

class CLateFeeCalculationType extends CBaseLateFeeCalculationType {

	const FIXED_AMOUNT											= 1;
	const PERCENT_OF_LATE_BALANCE								= 2;
	const GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE	= 3;
	const FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE				= 4;
	const PERCENT_OF_CHARGE_CODE								= 5;
	const PERCENT_OF_CHARGE_CODE_GROUP							= 6;
	const PERCENT_OF_CHARGE_CODE_TYPE							= 7;

	public static $c_arrintArFormulaCalculationTypes = [
		self::PERCENT_OF_CHARGE_CODE,
		self::PERCENT_OF_CHARGE_CODE_GROUP,
		self::PERCENT_OF_CHARGE_CODE_TYPE
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'FIXED_AMOUNT', 										self::FIXED_AMOUNT );
		$objSmarty->assign( 'PERCENT_OF_LATE_BALANCE', 								self::PERCENT_OF_LATE_BALANCE );
		$objSmarty->assign( 'GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE', 	self::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE );
		$objSmarty->assign( 'FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE', 			self::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE );
		$objSmarty->assign( 'PERCENT_OF_CHARGE_CODE', 								self::PERCENT_OF_CHARGE_CODE );
		$objSmarty->assign( 'PERCENT_OF_CHARGE_CODE_GROUP', 						self::PERCENT_OF_CHARGE_CODE_GROUP );
		$objSmarty->assign( 'PERCENT_OF_CHARGE_CODE_TYPE', 							self::PERCENT_OF_CHARGE_CODE_TYPE );
	}

}
?>