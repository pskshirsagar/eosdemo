<?php

class CCompanyApplication extends CBaseCompanyApplication {

	protected $m_intPropertyId;
	protected $m_arrintCustomerRelationshipIds;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['property_id'] ) )						$this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['customer_relationship_ids'] ) )		$this->setCustomerRelationshipIds( $arrmixValues['customer_relationship_ids'] );
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setCustomerRelationshipIds( $arrintCustomerRelationshipIds ) {
    	$this->m_arrintCustomerRelationshipIds = $arrintCustomerRelationshipIds;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getCustomerRelationshipIds() {
    	return $this->m_arrintCustomerRelationshipIds;
    }

    public function createPropertyApplication() {

		$objPropertyApplication = new CPropertyApplication();
		$objPropertyApplication->setCid( $this->getCid() );

		return $objPropertyApplication;
	}

	public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;

        if( true == is_null( $this->getTitle() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valOccupancyType() {
		$boolIsValid = true;

		if( true == is_null( $this->getOccupancyTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Type is required.' ) ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
             	$boolIsValid &= $this->valCid();
               	$boolIsValid &= $this->valTitle();
               	$boolIsValid &= $this->valOccupancyType();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Create Functions
     */

		//     function createCompanyApplicationDocument() {

		// 		$objCompanyApplicationDocument = new CCompanyApplicationDocument();
		// 		$objCompanyApplicationDocument->setDefaults();
		// 		$objCompanyApplicationDocument->setCid( $this->m_intCid );
		// 		$objCompanyApplicationDocument->setCompanyApplicationId( $this->m_intId );
		// 		$objCompanyApplicationDocument->setIsPublished( 1 );

		// 		return $objCompanyApplicationDocument;
		// 	}

    public function createPolicyDocument() {

		$objDocument = new CDocument();

		$objDocument->setCid( $this->getCid() );
		$objDocument->setCompanyApplicationId( $this->getId() );
    	$objDocument->setTitle( 'Application Policy Document' );
		$objDocument->setDocumentTypeId( CDocumentType::MERGE_DOCUMENT );
		$objDocument->setDocumentSubTypeId( CDocumentSubType::APPLICATION_POLICY_DOCUMENT );
		$objDocument->setRequirePrintedForm( 0 );

		$objDocument->setRequireConfirmation( 1 );
		$objDocument->setIsForPrimaryApplicant( 1 );
		$objDocument->setIsForCoApplicant( 1 );
		$objDocument->setIsForCoSigner( 1 );
		$objDocument->setAttachToEmail( 1 );

		return $objDocument;
	}

	/**
	 * Fetch Functions
	 */

   	public function fetchPolicyDocuments( $objDatabase ) {

		return CDocuments::fetchDocumentsByCompanyApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchDocumentAddendas( $objDatabase ) {

    	return CDocumentAddendas::fetchDocumentAddendasByCidByCompanyApplicationId( $this->getCid(), $this->getId(), $objDatabase );
    }

   	public function fetchPolicyDocumentById( $intDocumentId, $objDatabase ) {

		return CDocuments::fetchDocumentByIdByCompanyApplicationIdByCid( $intDocumentId, $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchDocumentAddendaById( $intDocumentAddendaId, $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendaByIdByCompanyApplicationIdByCid( $this->getId(), $intDocumentAddendaId, $this->getCid(), $objDatabase );
	}

	// End

	public function fetchPropertyApplications( $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationsByCompanyApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * OtherFunctions
	 */

	public function getApplicationDocumentIdByLeaseCustomerId( $intCustomerTypeId ) {

		$intApplicationDocumentId = NULL;

		switch( $intCustomerTypeId ) {
			case CCustomerType::PRIMARY:
				$intApplicationDocumentId = $this->getPrimaryDocumentId();
				break;

			case CCustomerType::RESPONSIBLE:
			case CCustomerType::NON_LEASING_OCCUPANT:
				$intApplicationDocumentId = $this->getCoApplicantDocumentId();
				break;

			case CCustomerType::GUARANTOR:
				$intApplicationDocumentId = $this->getCoSignerDocumentId();
				break;

			default:
				// default case
				break;
		}

		return $intApplicationDocumentId;
	}

	/**
	 * DB Functions
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = true;

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y h:i:s' ) );

		$boolIsValid = $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $boolIsValid;
	}

}
?>