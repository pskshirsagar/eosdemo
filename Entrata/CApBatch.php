<?php

class CApBatch extends CBaseApBatch {

	protected $m_intBankGlAccountId;
	protected $m_intOriginalControlCount;

	protected $m_arrintUtilityBillIds;

	// Get functions

	public function getUtilityBillIds() {
		return $this->m_arrintUtilityBillIds;
	}

	public function getBankGlAccountId() {
		return $this->m_intBankGlAccountId;
	}

	public function getOriginalControlCount() {
		return $this->m_intOriginalControlCount;
	}

	public function getFormattedPostMonth() {

		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	// Set functions

	public function setUtilityBillIds( $arrintUtilityBillIds ) {
		$this->m_arrintUtilityBillIds = $arrintUtilityBillIds;
	}

	public function setBankGlAccountId( $intBankGlAccountId ) {
		$this->m_intBankGlAccountId = $intBankGlAccountId;
	}

	public function setOriginalControlCount( $intOriginalControlCount ) {
		$this->m_intOriginalControlCount = $intOriginalControlCount;
	}

	// Override setPostMonth function. It will acccept date in dd/yyyy format and set it to dd/01/yyyy format

	public function setPostMonth( $strPostMonth ) {

		if( false == is_null( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$this->m_strPostMonth = $strPostMonth;
		}
	}

	/**
	 * Create Functions
	 */

	public function createApHeader() {

		$objApHeader = new CApHeader();
		$objApHeader->setDefaults();
		$objApHeader->setCid( $this->getCid() );
		$objApHeader->setApBatchId( $this->getId() );
		$objApHeader->setTransactionDatetime( 'NOW()' );
		$objApHeader->setGlTransactionTypeId( CGlTransactionType::AP_PAYMENT );
		return $objApHeader;
	}

	public function valPostMonth( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is required.' ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month of format mm/yyyy is required.' ) );
		}

		return $boolIsValid;
	}

	public function valControlCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getControlCount() ) || 0 == strlen( trim( $this->getControlCount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_count', 'Number of invoices is required.' ) );
		} elseif( false == preg_match( '/^\d{1,}$/', $this->getControlCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_count', 'Invalid Number of invoices.' ) );
		} elseif( 0 >= $this->getControlCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_count', 'Number of invoices should be greater than zero.' ) );
		} elseif( $this->getControlCount() < $this->getOriginalControlCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_count', 'The new number of invoices is less than the actual number of invoices already entered for this batch. Please click Go to Summary button to delete invoices from this batch if needed.' ) );
		}

		return $boolIsValid;
	}

	public function valControlTotal() {
		$boolIsValid = true;

		if( true == is_null( $this->getControlTotal() ) || 0 == strlen( trim( $this->getControlTotal() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', 'Control total is required.' ) );
		} elseif( false == is_numeric( $this->getControlTotal() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', 'Control total should be numeric.' ) );
		} elseif( 9999999999999.99 < $this->getControlTotal() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', 'Control total should not be greater than $9,999,999,999,999.99.' ) );
		} elseif( 0 == $this->getControlTotal() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', 'Control total should not be zero.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPaused() {
		$boolIsValid = true;

		if( false == $this->getIsPaused() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_paused', 'This Invoice batch is already processed.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'rpc_webservice_insert':
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valIsPaused();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'pre_insert':
				$boolIsValid &= $this->valControlCount();
				$boolIsValid &= $this->valControlTotal();
				$boolIsValid &= $this->valPostMonth();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['bank_gl_account_id'] ) && $boolDirectSet ) $this->m_intBankGlAccountId = trim( $arrmixValues['bank_gl_account_id'] );
		elseif ( isset( $arrmixValues['bank_gl_account_id'] ) ) $this->setBankGlAccountId( $arrmixValues['bank_gl_account_id'] );
		return;
	}

}
?>