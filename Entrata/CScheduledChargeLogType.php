<?php

class CScheduledChargeLogType extends CBaseScheduledChargeLogType {

	const MANUAL_CHANGE 								= 1;
	const APPLICATION_RENEWAL_UPDATE					= 2;
	const LEASE_START_DATE_CHANGED 						= 3;
	const LEASE_END_DATE_CHANGED						= 4;
	const MOVE_IN_DATE_CHANGED 							= 5;
	const MOVE_OUT_DATE_APPLIED 						= 6;
	const MOVE_OUT_DATE_REVERTED 						= 7;
	const RENEWAL_APPROVED 								= 8;
	const RENEWAL_REVERTED 								= 9;
	const PRORATION_DISABLED 							= 10;
	const PRORATION_RE_ENABLED 							= 11;
	const PM_SOFTWARE_INTEGRATION  						= 12;
	const ENTRATA_CORE_DATA_MIGRATION 					= 13;
	const MONTH_TO_MONTH_LEASE_TRUNCATION_INSERT 		= 14;
	const MONTH_TO_MONTH_LEASE_TRUNCATION_UPDATE		= 15;
	const MONTH_TO_MONTH_LEASE_TRUNCATION_FEE_INSERT 	= 16;
	const SCHEDULED_CHARGE_POSTING_REWIND 				= 17;
	const SCHEDULED_CHARGE_POSTING 						= 18;
	const INTERVAL_CANCELLATION 						= 19;
	const TRUNCATION_REVERSAL 							= 20;
	const TRUNCATION_FEE_REVERSAL 						= 21;
	const TRUNCATION_REVERSAL_PARENT_UPDATE 			= 22;
	const MOVE_IN_DATE_CLEAN_UP 						= 23;
	const INSTALLMENT_PLAN_CHANGE 						= 24;
	const CONVERT_TO_OTHER_INCOME						= 25;
	const CHARGE_CODE_TYPE_CHANGED						= 26;
	const CHARGE_CODE_CHANGED_THROUGH_BULK_UPDATE		= 27;
	const AMENITY_RESERVATION							= 28;
	const DISASSOCIATED_RATE							= 29;
	const CHARGE_CODE_CODE_CLEAN_UP_REQUEST				= 30;
	const LEASE_ABSTRACT								= 31;
	const AFFORDABLE_PROCESS_CERTIFICATION 				= 32;
	const ACCELERATED_RENT_APPLIED						= 33;
	const ACCELERATED_RENT_REVERTED						= 34;
	const ACCELERATED_RENT_ADJUSTED						= 35;
	const FORCEFULLY_ENDED								= 36;
	const ADD_ON_INSERTED								= 37;
	const ADD_ON_UPDATED								= 38;
	const REBUILD_SCHEDULED_CHARGES						= 39;
	const API_CHANGE									= 40;
	const APPLY_DATES_ON_EVENT_CHARGES					= 41;
	const APPROVAL_ROUTING								= 42;
	const RECALCULATE_CHARGE_AMOUNT						= 43;
	const SPECIAL_CHARGE_DELETION_WITH_CAP              = 44;
	const EARLY_MOVE_IN                                 = 45;
	const TOTAL_VALUE_CALCULATED                        = 46;
	const EXTENSION_APPROVED							= 47;
	const EXTENSION_REVERTED							= 48;
	const RENEWAL_TRUNCATION							= 49;
	const BULK_SCHEDULED_CHARGE_ADJUSTMENT              = 50;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>