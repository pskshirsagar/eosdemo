<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationPeriods
 * Do not add any new functions to this class.
 */

class CIntegrationPeriods extends CBaseIntegrationPeriods {

	public static function fetchIntegrationPeriods( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIntegrationPeriod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchIntegrationPeriod( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIntegrationPeriod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllIntegrationPeriods( $objDatabase ) {
		return self::fetchIntegrationPeriods( 'SELECT * FROM integration_periods WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}
}
?>