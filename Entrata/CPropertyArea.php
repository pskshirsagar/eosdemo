<?php

class CPropertyArea extends CBasePropertyArea {

    public function valCompanyAreaId() {
        $boolIsValid = true;

        if( true == is_null( $this->getCompanyAreaId() ) || false == is_numeric( $this->getCompanyAreaId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_area_id', __( 'You must choose an area or sub-area within a state.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDuplicates( $objDatabase ) {
        $boolIsValid = true;

		$intConflictingPropertyAreaCount = \Psi\Eos\Entrata\CPropertyAreas::createService()->fetchConflictingPropertyAreaCountByPropertyAreaByCid( $this, $objDatabase );

		if( 0 < $intConflictingPropertyAreaCount ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_area_id', __( 'The search area you selected is already associated.' ) ) );
		}

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCompanyAreaId();
            	$boolIsValid &= $this->valDuplicates( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>