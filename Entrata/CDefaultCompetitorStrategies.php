<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCompetitorStrategies
 * Do not add any new functions to this class.
 */

class CDefaultCompetitorStrategies extends CBaseDefaultCompetitorStrategies {

	public static function fetchDefaultCompetitorStrategies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultCompetitorStrategy', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDefaultCompetitorStrategy( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultCompetitorStrategy', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>