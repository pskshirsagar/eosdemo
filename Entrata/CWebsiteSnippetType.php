<?php

class CWebsiteSnippetType extends CBaseWebsiteSnippetType {

	const FLOORPLAN_AVAILABILITY_SNIPPET		= 1;
	const FLOORPLAN_TAB_SNIPPET					= 2;
	const GUEST_CARD_SNIPPET					= 4;
	const RATINGS_REVIEWS_SNIPPET 				= 5;
	const RESIDENT_PORTAL_LOGIN_SNIPPET			= 6;
	const EMPLOYEE_PORTAL_LOGIN_SNIPPET			= 7;
	const AMENITIES_SNIPPET 					= 8;
	const SITE_PLAN_LINK_SNIPPET				= 9;
	const APPLICATION_SNIPPET 					= 10;
	const PROPERTY_SEARCH_SNIPPET				= 11;
	const MULTI_PROPERTY_CONTACT_FORM_SNIPPET	= 12;
	const MULTI_CONTACT_BUTTON_SNIPPET 			= 13;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSnippetType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>