<?php

class COrganizationContractStatusType extends CBaseOrganizationContractStatusType {

	const ACTIVE		= 1;
	const NEGOTIATING	= 2;
	const PAST			= 3;
	const ARCHIVED		= 4;

	public static $c_arrintActiveOrganizationContractStatusTypeIds = [ self::ACTIVE, self::NEGOTIATING ];

	public static function getOrganizationContractStatusTypeNameById( $intLeaseStatusTypeId ) {
		$strStatusTypeName = NULL;

		switch( $intLeaseStatusTypeId ) {

			case self::ACTIVE:
				$strStatusTypeName = __( 'Active' );
				break;

			case self::NEGOTIATING:
				$strStatusTypeName = __( 'Negotiating' );
				break;

			case self::PAST:
				$strStatusTypeName = __( 'Past' );
				break;

			case self::ARCHIVED:
				$strStatusTypeName = __( 'Archived' );
				break;

			default:
				// default case
				break;
		}

		return $strStatusTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>