<?php

class CApplicationPreferenceType extends CBaseApplicationPreferenceType {

	const TYPE_GLOBAL 					= 1;
	const PRIMARY_APPLICATION 			= 2;
	const CO_APPLICATION_APPLICATION 	= 3;
	const CO_SIGNER_APPLICATION 		= 4;
	const PRIMARY_LEASE 				= 5;
	const PRIMARY_CO_APPLICANT_LEASE 	= 6;
	const PRIMARY_CO_SIGNER_LEASE 		= 7;
}
?>