<?php

class CApPayeeContactValidator {

	protected $m_boolIsValidDetails;
	protected $m_objApPayeeContact;

	public function __construct() {
		return;
	}

	public function setApPayeeContact( $objApPayeeContact ) {
		$this->m_objApPayeeContact = $objApPayeeContact;
	}

	public function valId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_objApPayeeContact->getApPayeeId() ) ) {
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valApPayeeContactTypeId( $boolIsValidate, $objClientDatabase ) {

		$objDbApPayeeContact		= NULL;
		$objPrimaryApPayeeContact	= NULL;

		if( true == is_numeric( $this->m_objApPayeeContact->getId() ) ) {
			$objDbApPayeeContact		= CApPayeeContacts::fetchApPayeeContactByIdByCid( $this->m_objApPayeeContact->getId(), $this->m_objApPayeeContact->getCid(), $objClientDatabase );
		} else {
			$objPrimaryApPayeeContact	= CApPayeeContacts::fetchApPayeeContactByApPayeeContactTypeIdByApPayeeIdByCid( true, $this->m_objApPayeeContact->getApPayeeId(), $this->m_objApPayeeContact->getCid(), $objClientDatabase );
		}

		if( ( false == $boolIsValidate && 0 == \Psi\Libraries\UtilFunctions\count( $objPrimaryApPayeeContact ) && 0 == \Psi\Libraries\UtilFunctions\count( $objDbApPayeeContact )
				&& false == $this->m_objApPayeeContact->getIsPrimary() )
			|| ( true == valObj( $objDbApPayeeContact, 'CApPayeeContact' ) && $objDbApPayeeContact->getIsPrimary() != $this->m_objApPayeeContact->getIsPrimary()
				&& true == $objDbApPayeeContact->getIsPrimary()
				&& true == is_numeric( $this->m_objApPayeeContact->getId() ) ) ) {

			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_contact_type_id', 'At least one contact type should be primary.' ) );
			return false;
		}

		return true;
	}

	public function valApPayeeLocationId( $objClientDatabase ) {
		$boolIsValid = true;

		if( NULL == $this->m_objApPayeeContact->getApPayeeLocationId() ) {
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'Location is required.' ) );
			return false;
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->m_objApPayeeContact->getCid() . '
							AND ap_payee_location_id = ' . ( int ) $this->m_objApPayeeContact->getApPayeeLocationId() . '
							AND LOWER ( name_first ) LIKE LOWER( \'' . $this->m_objApPayeeContact->getNameFirst() . '\' )
							AND LOWER ( name_last ) LIKE LOWER( \'' . $this->m_objApPayeeContact->getNameLast() . '\' )
							AND id != ' . ( int ) $this->m_objApPayeeContact->getId();

		if( 1 <= CApPayeeContacts::fetchApPayeeContactCount( $strWhereSql, $objClientDatabase ) ) {

			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'A contact with this first and last name already exists for this location.' ) );
		}

		return $boolIsValid;
	}

	public function valVpContactId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitleOld() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getTitle() ) && false == preg_match( '/^[a-zA-Z ]+$/', $this->m_objApPayeeContact->getTitle() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Contact Details:- Title has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getTitle() ) && false == preg_match( '/^[a-zA-Z\\\'\"\,\- ]+$/', $this->m_objApPayeeContact->getTitle() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Position/Dept has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirstOld( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Contact Details:- First Name is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeContact->getNameFirst() ) && false == preg_match( '/^[a-zA-Z ]+$/', $this->m_objApPayeeContact->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Contact Details:- First Name has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirstForUpdate( $boolIsMandatory = false ) {
		$boolIsValid = true;
		static $boolIsRepeatValue = false;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameFirst() ) ) {
			$boolIsValid = false;
			if( false == $boolIsRepeatValue ) {
				$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First Name is required.' ) );
				$boolIsRepeatValue = true;
			}
		}

		return $boolIsValid;
	}

	public function valNameMiddleOld() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getNameMiddle() ) && false == preg_match( '/^[a-zA-Z ]+$/', $this->m_objApPayeeContact->getNameMiddle() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', 'Contact Details:- Middle Name has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLastOld( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameLast() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Contact Details:- Last Name is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeContact->getNameLast() ) && false == preg_match( '/^[a-zA-Z ]+$/', $this->m_objApPayeeContact->getNameLast() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Contact Details:- Last Name has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameLast() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLastForUpdate( $boolIsMandatory = false ) {
		$boolIsValid = true;
		static $boolIsRepeatValue = false;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getNameLast() ) ) {
			$boolIsValid = false;

			if( false == $boolIsRepeatValue ) {
				$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Last Name is required.' ) );
				$boolIsRepeatValue = true;
			}
		}

		return $boolIsValid;
	}

	public function valStreetLine1Old( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Address is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeContact->getStreetLine1() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\" ]+$/', $this->m_objApPayeeContact->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Contact Details:- Street Line 1 has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $boolIsMandatory = false ) {

		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getStreetLine1() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Address Line 1 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getStreetLine2() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\"\-\>\\/\,\\\ ]+$/', $this->m_objApPayeeContact->getStreetLine2() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', 'Address Line 2 has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getStreetLine3() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\" ]+$/', $this->m_objApPayeeContact->getStreetLine3() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line3', 'Contact Details:- Street Line 3 has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valCityOld() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getCity() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeContact->getCity() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'Contact Details:- City has invalid characters.' ) );
		}

		if( true == is_numeric( $this->m_objApPayeeContact->getCity() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'Contact Details:- City can not be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valCity( $boolIsMandatory = false ) {

		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->m_objApPayeeContact->getCity() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		}

		if( true == is_numeric( $this->m_objApPayeeContact->getCity() ) ) {
			$boolIsValid = false;
			$this->m_boolIsValidDetails &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City can not be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode( $boolIsMandatory = false ) {

		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getStateCode() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getProvince() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeContact->getProvince() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', 'Contact Details:- Province has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCodeOld() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getPostalCode() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeContact->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Contact Details:- Postal code has invalid characters.' ) );
		}

		if( true == ctype_alpha( $this->m_objApPayeeContact->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Contact Details:- Postal Code can\'t be only characters.' ) );
		}

		$intLength = strlen( str_replace( '-', '', $this->m_objApPayeeContact->getPostalCode() ) );
		if( 0 < $intLength && ( 5 > $intLength || 6 < $intLength ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Contact Details:- Postal Code must be between 5 and 6 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $boolIsMandatory = false ) {

		$boolIsValid = true;

		if( false != $boolIsMandatory && false != is_null( $this->m_objApPayeeContact->getPostalCode() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required.' ) );
		}

		$intLength = strlen( str_replace( '-', '', $this->m_objApPayeeContact->getPostalCode() ) );
		if( CCountry::CODE_USA == $this->m_objApPayeeContact->getCountryCode()
		    && 0 < $intLength && ( ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->m_objApPayeeContact->getPostalCode() )
		    && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->m_objApPayeeContact->getPostalCode() ) ) ) ) {

				$boolIsValid = false;
				$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format.' ) );
		}

		if( CCountry::CODE_CANADA == $this->m_objApPayeeContact->getCountryCode()
		    && 0 < $intLength && false == preg_match( '/^[A-Z]{1}\d{1}[A-Z]{1} \d{1}[A-Z]{1}\d{1}$/', $this->m_objApPayeeContact->getPostalCode() ) ) {

				$boolIsValid = false;
				$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 6 characters in XXX XXX format and should contain only alphanumeric characters.' ) );
		}
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */

	public function valPhoneNumberOld() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getPhoneNumber() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( $this->m_objApPayeeContact->getPhoneNumber() );

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Contact Details:- Phone number must be between 10 and 15 characters.' ) );
		} elseif( false == ( CValidation::validateFullPhoneNumber( $this->m_objApPayeeContact->getPhoneNumber(), false ) ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Contact Details:- Phone number is not valid.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolShowValidationMsg = false ) {

		$boolIsValid = true;

		if( true == $boolShowValidationMsg && true == is_null( $this->m_objApPayeeContact->getPhoneNumber() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		$strPhoneNumber = explode( ':', $this->m_objApPayeeContact->getPhoneNumber() )[0];
		$intLength = strlen( preg_replace( '/[^0-9]/', '', $strPhoneNumber ) );

		if( false == is_null( $this->m_objApPayeeContact->getPhoneNumber() ) && 0 < $intLength && 10 > $intLength ) {

			$boolIsValid = false;
			$this->m_boolIsValidDetails &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be 10 digits.' ) );
		} elseif( false == is_null( $this->m_objApPayeeContact->getPhoneNumber() ) && false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $strPhoneNumber ) ) {
			$boolIsValid = false;
			$this->m_boolIsValidDetails &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumberOld() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getFaxNumber() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( $this->m_objApPayeeContact->getFaxNumber() );

		if( 0 < $intLength && ( 7 > $intLength || 20 < $intLength ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Contact Details:- Fax number must be between 7 and 20 characters.' ) );
		} elseif( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->m_objApPayeeContact->getFaxNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Contact Details:- Fax number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getFaxNumber() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( preg_replace( '/[^0-9]/', '', $this->m_objApPayeeContact->getFaxNumber() ) );

		if( 0 < $intLength && 10 > $intLength ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number must be 10 digits.' ) );
		} elseif( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->m_objApPayeeContact->getFaxNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valMobileNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getMobileNumber() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( $this->m_objApPayeeContact->getMobileNumber() );

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', 'Contact Details:- Mobile number must be between 10 and 15 characters.' ) );
		} elseif( false == preg_match( '/^[0-9]+$/', $this->m_objApPayeeContact->getMobileNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', 'Contact Details:- Mobile number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddressOld() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objApPayeeContact->getEmailAddress() ) ) {
			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Contact Details:- Email address is not a valid one.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolShowValidationMsg = false ) {

		$boolIsValid = true;

		if( true == $boolShowValidationMsg && true == is_null( $this->m_objApPayeeContact->getEmailAddress() ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeContact->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objApPayeeContact->getEmailAddress() ) ) {
			$boolIsValid = false;
			// $this->m_boolIsValidDetails &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddressRequired( $objClientDatabase, $boolIsMandatory ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getEmailAddress() ) && 0 == strlen( trim( $this->m_objApPayeeContact->getEmailAddress() ) ) ) {
			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
		}

		if( false == $boolIsMandatory ) {
			return $boolIsValid;
		}

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->m_objApPayeeContact->getCid() . '
							AND email_address = \'' . addslashes( $this->m_objApPayeeContact->getEmailAddress() ) . '\'
							AND ap_payee_id IN (
													SELECT
														ap.id
													FROM
														ap_payees ap
													WHERE
														ap.cid = ' . ( int ) $this->m_objApPayeeContact->getCid() . '
														AND ap.ap_payee_type_id = ' . CApPayeeType::OWNER . '
												)';

		if( 0 < $this->m_objApPayeeContact->getId() ) {
			$strWhereSql .= ' AND id != ' . ( int ) $this->m_objApPayeeContact->getId();
		}

		$intDuplicateEmailCount = CApPayeeContacts::fetchApPayeeContactCount( $strWhereSql, $objClientDatabase );

		if( 0 < $intDuplicateEmailCount ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getTaxNumberEncrypted() ) && 10 > strlen( $this->m_objApPayeeContact->getTaxNumberEncrypted() ) ) {
			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', 'Contact Details:- Tax number is not a valid one.' ) );
		} elseif( '*' == \Psi\CStringService::singleton()->substr( $this->m_objApPayeeContact->getTaxNumberEncrypted(), 0, 1 ) ) {
			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', 'Contact Details:- Tax number is not a valid one.' ) );
		}
		return $boolIsValid;
	}

	public function valDuplicates( $objClientDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getId() ) ) return $boolIsValid;

		$strWhereSql = 'WHERE cid = ' . ( int ) $this->m_objApPayeeContact->getCid() . ' AND ap_payee_id = ' . ( int ) $this->m_objApPayeeContact->getApPayeeId() . ' AND ap_payee_contact_type_id = ' . ( int ) $this->m_objApPayeeContact->getApPayeeContactTypeId();

		if( 0 < CApPayeeContacts::fetchApPayeeContactCount( $strWhereSql, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION,  'value', 'A value pair with this same cid, ap_payee_id, ap_payee_contact_type_id has already been added.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getPasswordEncrypted() ) && 6 > strlen( $this->m_objApPayeeContact->getPasswordEncrypted() ) ) {
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password cannot be less than 6 characters.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valConfirmPassword() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeContact->getPasswordEncrypted() ) && true == is_null( $this->m_objApPayeeContact->getConfirmPassword() ) ) {

			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Confirmation password is required.' ) );
			$boolIsValid &= false;
		} elseif( false == is_null( $this->m_objApPayeeContact->getConfirmPassword() ) && 6 > strlen( $this->m_objApPayeeContact->getConfirmPassword() ) ) {

			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Confirmation Password cannot be less than 6 characters.' ) );
			$boolIsValid &= false;
		} elseif( 0 != strcmp( $this->m_objApPayeeContact->getPasswordEncrypted(), $this->m_objApPayeeContact->getConfirmPassword() ) ) {

			$boolIsValid &= false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Passwords do not match.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordOnly() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeContact->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateFirstNameAndLastName( $arrobjApPayeeContacts ) {

		$boolIsValid = true;

		if( false == valArr( $arrobjApPayeeContacts )
			|| ( false == valStr( $this->m_objApPayeeContact->getNameFirst() ) && false == valStr( $this->m_objApPayeeContact->getNameLast() ) ) ) {
			return $boolIsValid;
		}

		$arrstrFullNames = [];

		foreach( $arrobjApPayeeContacts as $objApPayeeeContact ) {
			$arrstrFullNames[] = \Psi\CStringService::singleton()->strtolower( $objApPayeeeContact->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->strtolower( $objApPayeeeContact->getNameLast() );
		}

		if( true == valStr( $this->m_objApPayeeContact->getNameFirst() )
			&& true == valStr( $this->m_objApPayeeContact->getNameLast() )
			&& true == in_array( \Psi\CStringService::singleton()->strtolower( $this->m_objApPayeeContact->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->strtolower( $this->m_objApPayeeContact->getNameLast() ), $arrstrFullNames ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Duplicate first name and last name is not allowed' ) );
		}

		return $boolIsValid;
	}

	public function valFullAddress() {
		$boolIsValid = true;
		$boolIsValid &= !is_null( $this->m_objApPayeeContact->getStreetLine1() );
		$boolIsValid &= !is_null( $this->m_objApPayeeContact->getCity() );
		if( CCountry::CODE_USA == $this->m_objApPayeeContact->getCountryCode() || CCountry::CODE_CANADA == $this->m_objApPayeeContact->getCountryCode() ) {
			$boolIsValid &= !is_null( $this->m_objApPayeeContact->getStateCode() );
		} else {
			$boolIsValid &= !is_null( $this->m_objApPayeeContact->getProvince() );
		}
		return $boolIsValid;
	}

	public function valWarrantyVendor() {

		$boolIsValid = true;

		if( false == $this->valFullAddress()
			&& true == is_null( $this->m_objApPayeeContact->getPhoneNumber() )
			&& true == is_null( $this->m_objApPayeeContact->getEmailAddress() )
			&& true == $this->m_boolIsValidDetails ) {

			$boolIsValid = false;
			$this->m_objApPayeeContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Either full address, phone number or email address is required.' ) );

		} elseif( ( $this->valPhoneNumber() ^ $this->valEmailAddress() )
					|| ( $this->valStreetLine1() && $this->valCity() && ( $this->valStateCode( false, true ) || $this->valProvince( false, true ) ) ) ) {

			$boolIsValid = ( bool ) $this->m_boolIsValidDetails;
		} else {

			$boolIsValid = ( bool ) $this->m_boolIsValidDetails;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsValidate, $objClientDatabase = NULL, $arrobjApPayeeContacts = NULL ) {

		$boolIsValid				= true;
		$this->m_boolIsValidDetails	= true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitleOld();
				$boolIsValid &= $this->valNameFirstOld( $boolIsMandatory = true );
				$boolIsValid &= $this->valNameMiddleOld();
				$boolIsValid &= $this->valNameLastOld( $boolIsMandatory = true );
				$boolIsValid &= $this->valEmailAddressOld();
				$boolIsValid &= $this->valTaxNumberEncrypted();
				$boolIsValid &= $this->valPhoneNumberOld();
				$boolIsValid &= $this->valMobileNumber();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valPostalCodeOld();
				break;

			case 'VALIDATE_SCRIPT_INSERT':
				$boolIsValid &= $this->valTitleOld();
				$boolIsValid &= $this->valNameFirstOld();
				$boolIsValid &= $this->valNameMiddleOld();
				$boolIsValid &= $this->valNameLastOld();
				$boolIsValid &= $this->valEmailAddressOld();
				$boolIsValid &= $this->valPhoneNumberOld();
				$boolIsValid &= $this->valMobileNumber();
				$boolIsValid &= $this->valFaxNumberOld();
				$boolIsValid &= $this->valPostalCodeOld();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitleOld();
				$boolIsValid &= $this->valNameFirstOld( $boolIsValidate );
				$boolIsValid &= $this->valNameMiddleOld();
				$boolIsValid &= $this->valNameLastOld( $boolIsValidate );
				$boolIsValid &= $this->valEmailAddressOld();
				$boolIsValid &= $this->valPhoneNumberOld();
				$boolIsValid &= $this->valMobileNumber();
				$boolIsValid &= $this->valFaxNumberOld();
				$boolIsValid &= $this->valPostalCodeOld();

				if( '*******' != $this->m_objApPayeeContact->getTaxNumberEncrypted() ) {

					$boolIsValid &= $this->valTaxNumberEncrypted();
				}
				break;

			case 'VALIDATE_OWNERS_INSERT_OR_UPDATE':
				$boolIsValid &= $this->valApPayeeContactTypeId( $boolIsValidate, $objClientDatabase );
				$boolIsValid &= $this->valNameFirst( $boolIsMandatory = true );
				$boolIsValid &= $this->valNameLast( $boolIsMandatory = true );
				$boolIsValid &= $this->valEmailAddressRequired( $objClientDatabase, $boolIsMandatory = true );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_quick_view':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'validate_warranty_vendor':
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valWarrantyVendor();
				$boolIsValid &= $this->valPostalCode();
				break;

			case 'insert_business_info':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'insert_contact_info':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valDuplicateFirstNameAndLastName( $arrobjApPayeeContacts );
				break;

			case 'insert_vendor_contact':
				$boolIsValid &= $this->valNameFirst( $boolIsValidate );
				$boolIsValid &= $this->valNameLast( $boolIsValidate );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'update_business_info':
				$boolIsValid &= $this->valStreetLine1( true );
				$boolIsValid &= $this->valCity( true );
				$boolIsValid &= $this->valStateCode( true );
				$boolIsValid &= $this->valPostalCode( true );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'update_contact_info':
				$boolIsValid &= $this->valApPayeeLocationId( $objClientDatabase );
				$boolIsValid &= $this->valNameFirstForUpdate( $boolIsValidate );
				$boolIsValid &= $this->valNameLastForUpdate( $boolIsValidate );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case 'insert_vendor_remittance_contact_info':
				$boolIsValid &= $this->valStreetLine1( true );
				$boolIsValid &= $this->valCity( true );
				$boolIsValid &= $this->valStateCode( true );
				$boolIsValid &= $this->valPostalCode( true );
				break;

			case 'insert_vendor_contact_info':
				$boolIsValid &= $this->valStreetLine1( true );
				$boolIsValid &= $this->valCity( true );
				$boolIsValid &= $this->valStateCode( true );
				$boolIsValid &= $this->valPostalCode( true );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'validate_forgot_password':
				$boolIsValid &= $this->valEmailAddressRequired( $objClientDatabase, $boolIsMandatory = false );
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'validate_owner':
				$boolIsValid &= $this->valTitleOld();
				$boolIsValid &= $this->valNameFirstOld( $boolIsMandatory = true );
				$boolIsValid &= $this->valNameMiddleOld();
				$boolIsValid &= $this->valNameLastOld( $boolIsMandatory = false );
				$boolIsValid &= $this->valEmailAddressOld();
				$boolIsValid &= $this->valTaxNumberEncrypted();
				$boolIsValid &= $this->valPhoneNumberOld();
				$boolIsValid &= $this->valMobileNumber();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valPostalCodeOld();
				break;

			case 'update_password':
				$boolIsValid &= $this->valPasswordOnly();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valConfirmPassword();
				break;

			case 'validate_verify_password':
				$boolIsValid &= $this->valPasswordOnly();
				$boolIsValid &= $this->valConfirmPassword();
				break;

			case 'insert_location_contact_info':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'update_vendor_email':
				$boolIsValid &= $this->valEmailAddress();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>