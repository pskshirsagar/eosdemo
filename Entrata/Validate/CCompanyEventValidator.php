<?php
class CCompanyEventValidator {

	protected $m_objCompanyEvent;

	public function __construct() {
		return;
	}

	public function setobjCompanyEvent( $objCompanyEvent ) {
		$this->m_objCompanyEvent = $objCompanyEvent;
	}

	public function valRequestedAssociations() {
		// It is required
		$boolIsValid = true;
		if( false == valArr( $this->m_objCompanyEvent->getRequestedPropertyAssociations() ) ) {
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'At least one property is required.' ) ) );
			$boolIsValid = false;
		}
		// It has to have at least one requested property
		return $boolIsValid;
	}

	public function valId() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_objCompanyEvent->getId() ) {
			trigger_error( 'Id is required - CCompanyEvent', E_USER_ERROR );
			return false;
		}
		// Must be greater than zero
		if( 1 > $this->m_objCompanyEvent->getId() ) {
			trigger_error( 'Id has to be greater than zero - CCompanyEvent', E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_objCompanyEvent->getCid() ) {
			trigger_error( 'A Client Id is required - CCompanyEvent', E_USER_ERROR );
			return false;
		} elseif( 1 > $this->m_objCompanyEvent->getCid() ) {
			trigger_error( 'A Client Id has to be greater than zero - CCompanyEvent', E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_objCompanyEvent->getName() ) {
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Event name is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > strlen( $this->m_objCompanyEvent->getName() ) ) {
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Event name is required.' ) ) );
			$boolIsValid = false;
		}

		// 		if( true == $boolIsValid && true == preg_match( '/[^a-zA-Z0-9-_ ]+/', $this->m_objCompanyEvent->m_strName )) {
		// 			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( NULL, 'name', 'Invalid Event name.'));
		// 			$boolIsValid = false;
		// 		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objCompanyEvent->getDescription() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Event description is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valDrivingDirections() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateStartForPast() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objCompanyEvent->getDateStart() ) ) {

			$strDateToday	= date( 'm/d/Y', time() );
			$strDateStart	= $this->m_objCompanyEvent->getDateStart();

			if( strtotime( $strDateStart ) < strtotime( $strDateToday ) && false == valId( $this->m_objCompanyEvent->getId() ) ) {

				$boolIsValid = false;
				$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Event start date is not allowed from past.' ) ) );
				return $boolIsValid;
			}

			if( false == is_null( $this->m_objCompanyEvent->getTimeStart() ) && false == is_null( $this->m_objCompanyEvent->getTimeEnd() ) ) {

				$strStartDateTime = strtotime( $this->m_objCompanyEvent->getDateStart() . ' ' . $this->m_objCompanyEvent->getTimeStart() );
				$strEndDateTime = strtotime( $this->m_objCompanyEvent->getDateEnd() . ' ' . $this->m_objCompanyEvent->getTimeEnd() );
				if( $strEndDateTime < $strStartDateTime ) {
					$boolIsValid = false;
					$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'Your event end time must be after your event start time.' ) ) );
					return $boolIsValid;
				}

			}
		}

		return $boolIsValid;
	}

	public function valDateEndForPast() {
		$boolIsValid = true;
		if( true == valId( $this->m_objCompanyEvent->getId() ) && false == $this->m_objCompanyEvent->getIsPublished() ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->m_objCompanyEvent->getDateEnd() ) ) {

			$strDateToday	= date( 'm/d/Y', time() );
			$strDateEnd	= $this->m_objCompanyEvent->getDateEnd();

			if( strtotime( $strDateEnd ) < strtotime( $strDateToday ) ) {

				$boolIsValid = false;
				$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'Event end date is not allowed from past.' ) ) );
				return $boolIsValid;
			}
		}
		return $boolIsValid;
	}

	public function valDateStart() {
		$boolIsValid = true;

		// If the field is not empty
		if( false == is_null( $this->m_objCompanyEvent->getDateStart() ) ) {

			if( 1 !== CValidation::checkDate( $this->m_objCompanyEvent->getDateStart() ) ) {
				$boolIsValid = false;
				$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date must be in mm/dd/yyyy form.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateEnd() {
		$boolIsValid = true;

		// If the field is not empty
		if( false == is_null( $this->m_objCompanyEvent->getDateEnd() ) ) {

			if( 1 !== CValidation::checkDate( $this->m_objCompanyEvent->getDateEnd() ) ) {
				$boolIsValid = false;
				$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'End date must be in mm/dd/yyyy form.' ) ) );
			}

			// Make sure that this date is after the start date
			if( true == $boolIsValid && false == is_null( $this->m_objCompanyEvent->getDateStart() ) && 1 === CValidation::checkDate( $this->m_objCompanyEvent->getDateStart() ) && true == is_null( $this->m_objCompanyEvent->getTimeStart() ) || true == is_null( $this->m_objCompanyEvent->getTimeEnd() ) ) {
				if( strtotime( $this->m_objCompanyEvent->getDateEnd() ) < strtotime( $this->m_objCompanyEvent->getDateStart() ) ) {
					$boolIsValid = false;
					$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'End date must be on or after Start date.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valTimeStart() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCompanyEvent->getTimeStart() ) && true == is_null( $this->m_objCompanyEvent->getDateStart() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_start', __( 'Start date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTimeEnd() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCompanyEvent->getTimeEnd() ) && true == is_null( $this->m_objCompanyEvent->getDateEnd() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_end', __( 'End date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode( $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsValid && false == is_null( $this->m_objCompanyEvent->getPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCompanyEvent->getPostalCode(), $strCountryCode ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Valid Postal code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartEndDateAndTime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCompanyEvent->getDateStart() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date is required.' ) ) );
		}

		if( true == is_null( $this->m_objCompanyEvent->getTimeStart() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_start', __( 'Start time is required.' ) ) );
		}

		if( true == is_null( $this->m_objCompanyEvent->getTimeEnd() ) ) {
			$boolIsValid = false;
			$this->m_objCompanyEvent->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_end', __( 'End time is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// DB required fields
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDateStart();
				$boolIsValid &= $this->valTimeStart();
				$boolIsValid &= $this->valDateEnd();
				$boolIsValid &= $this->valTimeEnd();
				$boolIsValid &= $this->valDateStartForPast();
				$boolIsValid &= $this->valDateEndForPast();
				$boolIsValid &= $this->valPostalCode( $this->m_objCompanyEvent->getCountryCode() );
				if( 1 == $this->m_objCompanyEvent->getIsGlobal() ) {
					$boolIsValid &= $this->valRequestedAssociations();
				}
				break;

			case VALIDATE_UPDATE:
				// DB required fields
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				if( true == $this->checkNonTranslatedFields() ) {
					$boolIsValid &= $this->valDateStart();
					$boolIsValid &= $this->valTimeStart();
					$boolIsValid &= $this->valDateEnd();
					$boolIsValid &= $this->valTimeEnd();
					$boolIsValid &= $this->valDateStartForPast();
					$boolIsValid &= $this->valDateEndForPast();
					$boolIsValid &= $this->valPostalCode();
					if( 1 == $this->m_objCompanyEvent->getIsGlobal() ) {
						$boolIsValid &= $this->valRequestedAssociations();
					}
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'resident_portal_insert':
			case 'resident_portal_update':
				// DB required fields
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStartEndDateAndTime();
				$boolIsValid &= $this->valDateStart();
				$boolIsValid &= $this->valDateEnd();
				$boolIsValid &= $this->valDateStartForPast( true );
				$boolIsValid &= $this->valDateEndForPast();
				$boolIsValid &= $this->valDescription();
				if( 1 == $this->m_objCompanyEvent->getIsGlobal() ) {
					$boolIsValid &= $this->valRequestedAssociations();
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function checkNonTranslatedFields() {
		$boolIsValidate = true;
		$objLocaleContainer = CLocaleContainer::createService();
		if( NULL !== $objLocaleContainer->getTargetLocaleCode() && $objLocaleContainer->getDefaultLocaleCode() != $objLocaleContainer->getTargetLocaleCode() ) {
			$boolIsValidate = false;
		}
		return $boolIsValidate;
	}

}
?>