<?php

class CScheduledCallValidator {

	protected $m_objScheduledCall;

	public function setScheduledCall( $objScheduledCall ) {
		$this->m_objScheduledCall = $objScheduledCall;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getName() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledCallTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getScheduledCallTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_call_type_id', 'Scheduled Call type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledCallPropertiesPreferences( $arrintScheduledCallPropertyPreferences ) {
		$boolIsValid = true;

		if( false == valArr( $arrintScheduledCallPropertyPreferences ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please associate at least one property.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledCallResidentStatusPreferences( $arrintScheduledCallResidentStatusPreferences ) {
		$boolIsValid = true;

		if( false == valArr( $arrintScheduledCallResidentStatusPreferences ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_status_type_id', 'Please associate at least one resident status.' ) );
		}

		return $boolIsValid;
	}

	public function valCallFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getCallFrequencyId() ) && false == is_null( $this->m_objScheduledCall->getScheduledCallTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_frequency_id', 'Call frequency is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallPriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getCallPriorityId() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_priority_id', 'Call priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVoiceTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getVoiceTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'voice_type_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCallSendType() {
		$boolIsValid = true;

		if( 1 != $this->m_objScheduledCall->getIsOneTime() && 0 != $this->m_objScheduledCall->getIsOneTime() ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select scheduled call send type.' ) );
		}

		return $boolIsValid;
	}

	public function valMultipleTimeFrequencyType() {
		$boolIsValid = true;

		if( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE != $this->m_objScheduledCall->getScheduledCallTypeId() && CScheduledCallType::RENT_REMINDER != $this->m_objScheduledCall->getScheduledCallTypeId() ) {
			if( true == is_null( $this->m_objScheduledCall->getIsWeekly() ) && 0 == $this->m_objScheduledCall->getIsOneTime() ) {
				$boolIsValid = false;
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select multiple time call send type.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getStartDate() ) ) {
			$boolIsValid = false;

			if( ( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE != $this->m_objScheduledCall->getScheduledCallTypeId() && CScheduledCallType::RENT_REMINDER != $this->m_objScheduledCall->getScheduledCallTypeId() && 1 == $this->m_objScheduledCall->getIsOneTime() ) || CCallFrequency::ONCE == $this->m_objScheduledCall->getCallFrequencyId() ) {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Send date is required.' ) );
			} else {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date is required.' ) );
			}
		}

		if( strtotime( date( 'm/d/y' ) ) > strtotime( $this->m_objScheduledCall->getStartDate() ) ) {
			$boolIsValid = false;
			if( ( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE != $this->m_objScheduledCall->getScheduledCallTypeId() && CScheduledCallType::RENT_REMINDER != $this->m_objScheduledCall->getScheduledCallTypeId() ) && 1 == $this->m_objScheduledCall->getIsOneTime() ) {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Send date must be greater than or equal to today\'s date.' ) );
			} else {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be greater than or equal to today\'s date.' ) );
			}
		}

		if( false == is_null( $this->m_objScheduledCall->getStartDate() ) && false == CValidation::validateDate( \Psi\CStringService::singleton()->substr( $this->m_objScheduledCall->getStartDate(), 0, 10 ) ) ) {
			$boolIsValid = false;

			if( ( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE != $this->m_objScheduledCall->getScheduledCallTypeId() && CScheduledCallType::RENT_REMINDER != $this->m_objScheduledCall->getScheduledCallTypeId() ) && 1 == $this->m_objScheduledCall->getIsOneTime() ) {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Send date must be in mm/dd/yyyy format.' ) );
			} else {
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be in mm/dd/yyyy format.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE == $this->m_objScheduledCall->getScheduledCallTypeId() || CScheduledCallType::RENT_REMINDER == $this->m_objScheduledCall->getScheduledCallTypeId() || 0 == $this->m_objScheduledCall->getIsOneTime() ) {
			if( CCallFrequency::ONCE != $this->m_objScheduledCall->getCallFrequencyId() ) {
				if( true == is_null( $this->m_objScheduledCall->getEndDate() ) ) {
					$boolIsValid = false;
					$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date is required.' ) );
				}

				if( false == is_null( $this->m_objScheduledCall->getEndDate() ) && false == CValidation::validateDate( \Psi\CStringService::singleton()->substr( $this->m_objScheduledCall->getEndDate(), 0, 10 ) ) ) {
					$boolIsValid = false;
					$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date must be in mm/dd/yyyy format.' ) );
				}

				// Make sure that end date is after the start date
				if( true == $boolIsValid && false == is_null( $this->m_objScheduledCall->getStartDate() ) ) {
					if( strtotime( $this->m_objScheduledCall->getEndDate() ) < strtotime( $this->m_objScheduledCall->getStartDate() ) ) {
						$boolIsValid = false;
						$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date must be on or after start date.' ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valBeginSendTime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getBeginSendTime() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_send_time', 'Scheduled start time is required.' ) );
		}

		$strScheduledCallStartTime = strtotime( $this->m_objScheduledCall->getBeginSendTime() );

		$objScheduledCallType = $this->m_objScheduledCall->getScheduledCallType();

		if( true == is_null( $objScheduledCallType ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_send_time', 'Scheduled start time is dependent on scheduled call type dropdown.' ) );

			return $boolIsValid;
		}

		$strAllowedStartTimeSeconds = strtotime( $objScheduledCallType->getBeginSendTime() );

		if( $strScheduledCallStartTime <= $strAllowedStartTimeSeconds ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_send_time', 'Scheduled start time must be after ' . date( 'h:i a', $strAllowedStartTimeSeconds ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valEndSendTime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getEndSendTime() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_send_time', 'Scheduled end time is required.' ) );
		}

		$strScheduledCallStartTime = strtotime( $this->m_objScheduledCall->getBeginSendTime() );
		$strScheduledCallEndTime   = strtotime( $this->m_objScheduledCall->getEndSendTime() );

		// Make sure that end time is 4 hrs greater than start time
		if( true == $boolIsValid && false == is_null( $this->m_objScheduledCall->getBeginSendTime() ) ) {
			if( ( $strScheduledCallEndTime - 14400 ) < $strScheduledCallStartTime ) {
				$boolIsValid = false;
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_send_time', 'Scheduled end time must be 4 hours greater than scheduled start time.' ) );
			}
		}

		if( true == $boolIsValid ) {
			$objScheduledCallType = $this->m_objScheduledCall->getScheduledCallType();

			if( true == is_null( $objScheduledCallType ) ) {
				$boolIsValid = false;
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_send_time', 'Scheduled end time is dependent on scheduled call type dropdown.' ) );

				return $boolIsValid;
			}

			$strAllowedEndTimeSeconds = strtotime( $objScheduledCallType->getEndSendTime() );

			if( $strScheduledCallEndTime >= $strAllowedEndTimeSeconds ) {
				$boolIsValid = false;
				$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_send_time', 'Scheduled end time must be before ' . date( 'h:i a', $strAllowedEndTimeSeconds ) . '.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMonthDays() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getMonthDays() ) && 2 == $this->m_objScheduledCall->getIsWeekly() && 1 != $this->m_objScheduledCall->getIsOneTime() ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_days', 'Please select atleast one day of the month.' ) );
		}

		return $boolIsValid;
	}

	public function valWeekDays() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objScheduledCall->getWeekDays() ) && 1 == $this->m_objScheduledCall->getIsWeekly() && 1 != $this->m_objScheduledCall->getIsOneTime() ) {
			$boolIsValid = false;
			$this->m_objScheduledCall->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_days', 'Please select atleast one day of the week.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrintScheduledCallPropertyPreferences, $arrintScheduledCallResidentStatusPreferences ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valCallFrequencyId();
				$boolIsValid &= $this->valCallPriorityId();
				$boolIsValid &= $this->valVoiceTypeId();
				$boolIsValid &= $this->valMultipleTimeFrequencyType();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valBeginSendTime();
				$boolIsValid &= $this->valEndSendTime();
				$boolIsValid &= $this->valMonthDays();
				$boolIsValid &= $this->valWeekDays();
				break;

			case 'scheduled_call_insert_new':
			case 'scheduled_call_update_new':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valScheduledCallResidentStatusPreferences( $arrintScheduledCallResidentStatusPreferences );
				$boolIsValid &= $this->valCallFrequencyId();
				$boolIsValid &= $this->valCallPriorityId();
				$boolIsValid &= $this->valVoiceTypeId();
				$boolIsValid &= $this->valMultipleTimeFrequencyType();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valBeginSendTime();
				$boolIsValid &= $this->valEndSendTime();
				$boolIsValid &= $this->valMonthDays();
				$boolIsValid &= $this->valWeekDays();
				$boolIsValid &= $this->valScheduledCallPropertiesPreferences( $arrintScheduledCallPropertyPreferences );
				break;

			case 'scheduled_call_type':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valScheduledCallTypeId();
				break;

			case 'scheduled_call_properties':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valScheduledCallPropertiesPreferences( $arrintScheduledCallPropertyPreferences );
				break;

			case 'scheduled_call_residents':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valScheduledCallResidentStatusPreferences( $arrintScheduledCallResidentStatusPreferences );
				break;

			case 'happy_birthday_options':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valBeginSendTime();
				$boolIsValid &= $this->valEndSendTime();
				break;

			case 'rent_reminder_options':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valCallFrequencyId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valBeginSendTime();
				$boolIsValid &= $this->valEndSendTime();
				break;

			case 'late_rent_notice_options':
			case 'custom_options':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valScheduledCallTypeId();
				$boolIsValid &= $this->valCallSendType();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valBeginSendTime();
				$boolIsValid &= $this->valEndSendTime();
				$boolIsValid &= $this->valMultipleTimeFrequencyType();
				$boolIsValid &= $this->valMonthDays();
				$boolIsValid &= $this->valWeekDays();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

}

?>