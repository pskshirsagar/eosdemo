<?php

class CArTransactionValidator {

	/**
	 *
	 * @var CArTransaction
	 */

	protected $m_objArTransaction;

	public function __construct() {
		return;
	}

	public function setArTransaction( $objArTransaction ) {
		$this->m_objArTransaction = $objArTransaction;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getId() ) || 0 >= ( int ) $this->m_objArTransaction->getId() ) {
			$boolIsValid = false;
			trigger_error( __( 'Valid ID required.' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getCid() ) || 0 >= ( int ) $this->m_objArTransaction->getCid() ) {
			$boolIsValid = false;
			trigger_error( __( 'Valid client id required.' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPropertyId( $boolIsTrigger = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getPropertyId() ) || 0 >= ( int ) $this->m_objArTransaction->getPropertyId() ) {
			if( false == $boolIsTrigger ) {
				$boolIsValid = false;
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id',  __( 'Property is required.' ) ) );
			} else {
				$boolIsValid = false;
				trigger_error( __( 'Valid property id required ' ), E_USER_ERROR );
			}
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getLeaseId() ) || 0 >= ( int ) $this->m_objArTransaction->getLeaseId() ) {
			$boolIsValid = false;
			trigger_error( __( 'Valid lease id required ' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valArCodeId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getArCodeId() ) || 0 >= ( int ) $this->m_objArTransaction->getArCodeId() ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id',  __( 'Charge code is required.' ) ) );
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$intDefaultLedgerFilterId = \Psi\Eos\Entrata\CArCodes::createService()->fetchDefaultLedgerFilterIdByIdByCid( ( int ) $this->m_objArTransaction->getArCodeId(), $this->m_objArTransaction->getCid(), $objDatabase );
			if( true == isset( $intDefaultLedgerFilterId ) && CDefaultLedgerFilter::GROUP == $intDefaultLedgerFilterId ) {
				$objLease = $this->m_objArTransaction->getOrFetchLease( $objDatabase );

				if( true == valObj( $objLease, 'CLease' ) && false == valId( $objLease->getOrganizationContractId() ) ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Group charge codes cannot be used on non-Group leases.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;

		if( ( true == is_null( $this->m_objArTransaction->getArCodeTypeId() ) || 0 >= ( int ) $this->m_objArTransaction->getArCodeTypeId() ) && false == is_null( $this->m_objArTransaction->getArCodeId() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_type_id',  __( 'Charge code type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArTransaction->getArPaymentId() ) || 0 >= ( int ) $this->m_objArTransaction->getArPaymentId() ) {
			$boolIsValid = false;
			trigger_error( __( 'Valid A/R payment id required.' ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPostDate( $strAction = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		if( 'post_deposit_held_ar_transaction' == $strAction ) {
			$strArTransactionType = __( 'Beginning deposit' );
		} elseif( 0 > $this->m_objArTransaction->getTransactionAmount() ) {
			$strArTransactionType = __( 'Concession' );
		} else {
			$strArTransactionType = __( 'Charge' );
		}

		if( false == valStr( $this->m_objArTransaction->getPostDateOnly() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( '{%s, 0} date is required.', [ $strArTransactionType ] ) ) );
		} elseif( false == CValidation::checkISODateFormat( $this->m_objArTransaction->getPostDateOnly(), true ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( '{%s, 0} date is not valid.', [ $strArTransactionType ] ) ) );
		}

		if( 'post_deposit_held_ar_transaction' == $strAction ) {
			$objLease = $this->m_objArTransaction->getOrFetchLease( $objDatabase );

			if( true == valObj( $objLease, 'CLease ' ) ) {
				$intLeaseStartDate 	= strtotime( ( true == valStr( $objLease->getLeaseStartDate() ) ) ? $objLease->getLeaseStartDate() : date( 'm/d/Y' ) );
				$intPostDate		= strtotime( CValidation::checkISODateFormat( $this->m_objArTransaction->getPostDateOnly(), true ) ); // formatting the date

				if( $intPostDate < $intLeaseStartDate ) {
					$boolIsValid = false;
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Post date should not be less than lease start date.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valIntegrity( $objDatabase, $intLedgerFilterId ) {

		$objLease 	= $this->m_objArTransaction->getOrFetchLease( $objDatabase );

		if( false == valStr( $this->m_objArTransaction->getPostMonth() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
			return false;
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_objArTransaction->getPostMonth() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
			return false;
		}

		if( false == valStr( $this->m_objArTransaction->getPostDateOnly() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Date received is required.' ) ) );
			return false;

		} elseif( false == CValidation::checkISODateFormat( $this->m_objArTransaction->getPostDateOnly(), true ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Date received is not valid.' ) ) );
			return false;
		}

		$intLeaseStartDate 	= strtotime( ( true == valStr( $objLease->getLeaseStartDate() ) ) ? $objLease->getLeaseStartDate() : date( 'm/d/Y' ) );
		$intPostDate		= strtotime( CValidation::checkISODateFormat( $this->m_objArTransaction->getPostDateOnly(), true ) ); // formatting the date

		if( $intPostDate < $intLeaseStartDate ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Date received should not be less than lease start date.' ) ) );
			return false;
		}

		$arrmixDepositArTransactions 	= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchSimpleDepositHeldArTransactionsDataByLeaseIdByArCodeIdByCid( $objLease->getId(), $this->m_objArTransaction->getArCodeId(), $objLease->getCid(), $intLedgerFilterId, $objDatabase );
		foreach( $arrmixDepositArTransactions as $arrmixArTransaction ) {

			if( strtotime( $this->m_objArTransaction->getPostDate() ) < strtotime( $arrmixArTransaction['post_date'] ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Date received should not be less than deposit post date ( {%t, post_date, DATE_NUMERIC_STANDARD} ).', [ 'post_date' => date( 'm/d/Y', strtotime( $arrmixArTransaction['post_date'] ) ) ] ) ) );
				return false;
			}

			if( strtotime( $this->m_objArTransaction->getPostMonth() ) < strtotime( $arrmixArTransaction['post_month'] ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Post month should not be less than deposit post month ( {%t, post_month, DATE_NUMERIC_POSTMONTH} )', [ 'post_month' => $arrmixArTransaction['post_month'] ] ) ) );
				return false;
			}
		}

		$arrmixArTransactions 			= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchSimpleArTransactionsDataByIdsByLeaseIdByByCid( $this->m_objArTransaction->getChargeArTransactionIds(), $objLease->getId(), $objLease->getCid(), $objDatabase );
		foreach( $arrmixArTransactions as $arrmixArTransaction ) {

			if( strtotime( $this->m_objArTransaction->getPostDate() ) < strtotime( $arrmixArTransaction['post_date'] ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Date received should not be less than charge post date ( {%t, post_date, DATE_NUMERIC_STANDARD} ). ', [ 'post_date' => date( 'm/d/Y', strtotime( $arrmixArTransaction['post_date'] ) ) ] ) ) );
				return false;
			}

			if( strtotime( $this->m_objArTransaction->getPostMonth() ) < strtotime( $arrmixArTransaction['post_month'] ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Post month should not be less than charge post month ( {%t, post_month, DATE_NUMERIC_POSTMONTH} ).', [ 'post_month' => $arrmixArTransaction['post_month'] ] ) ) );
				return false;
			}
		}

		return true;
	}

	public function valDeletePostDate() {
		$boolIsValid = true;

		if( 0 > $this->m_objArTransaction->getTransactionAmount() && true == valStr( $this->m_objArTransaction->getArPaymentId() ) ) {
			$strArTransactionType = __( 'Payment' );
		} elseif( 0 > $this->m_objArTransaction->getTransactionAmount() ) {
			$strArTransactionType = __( 'Concession' );
		} else {
			$strArTransactionType = __( 'Charge' );
		}

		if( false == valStr( $this->m_objArTransaction->getDeletePostDate() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( '{%s, 0} date is required.', [ $strArTransactionType ] ) ) );
			return false;

		} elseif( false == CValidation::checkISODateFormat( $this->m_objArTransaction->getDeletePostDate() ) && false == CValidation::validateISODate( $this->m_objArTransaction->getDeletePostDate() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( '{%s, 0} date is not valid.', [ $strArTransactionType ] ) ) );
			return false;
		}

		if( strtotime( date( 'm/d/Y', strtotime( $this->m_objArTransaction->getPostDate() ) ) ) > strtotime( date( 'm/d/Y', strtotime( $this->m_objArTransaction->getDeletePostDate() ) ) ) ) {

			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( 'The charge could not be reversed. Enter a reversal date after the charge date ({%t, formatted_transaction_date, DATE_NUMERIC_STANDARD}) to reverse this charge.', [ 'formatted_transaction_date' => $this->m_objArTransaction->getFormattedTransactionDate() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valArPaymentStatusTypeId( $objPaymentDatabase ) {

		$this->m_objArTransaction->getOrFetchArPayment( $objPaymentDatabase );

		if( false == valObj( $this->m_objArTransaction->getArPayment(), 'CArPayment' ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unable to load payment.' ) ) );
			return false;
		}

		if( CPaymentStatusType::RECEIVED != $this->m_objArTransaction->getArPayment()->getPaymentStatusTypeId() ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Only payments with status type received can be returned.' ) ) );
			return false;
		}

		return true;
	}

	public function valReturnItemFee() {

		if( 0 > ( float ) $this->m_objArTransaction->getReturnItemFee() ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Return item fees cannot be negative.' ) ) );
			return false;
		}

		return true;
	}

	public function valPostMonthFormat() {
		$boolIsValid = true;

		$strPostMonth = date( 'm/1/Y', strtotime( $this->m_objArTransaction->getPostMonth() ) );

		if( false == valStr( $this->m_objArTransaction->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );

		} elseif( false == \Psi\CStringService::singleton()->preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_objArTransaction->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth( $objDatabase, $boolIsValPostMonth = false, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {
		$boolIsValid 		  = true;
		$objPropertyGlSetting = $this->m_objArTransaction->getOrFetchPropertyGlSetting( $objDatabase );
		$strLockPeriod 		  = date( 'm/1/Y', strtotime( $objPropertyGlSetting->getArLockMonth() ) );
		$strPostMonth 		  = date( 'm/1/Y', strtotime( $this->m_objArTransaction->getPostMonth() ) );

		// strtotime() function allows maximum up to the year '2038', on 32-bit server
		$strCurrentPostMonth  = date( 'm/d/Y', strtotime( $strPostMonth ) );
		$strMinPostMonth      = date( 'm/d/Y', strtotime( '1/1/1970' ) );
		$strMaxPostMonth      = date( 'm/d/Y', strtotime( '12/1/2099' ) );

		// Check if post_month is valid
		if( false == valStr( $this->m_objArTransaction->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_objArTransaction->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		} elseif( ( strtotime( $strCurrentPostMonth ) < strtotime( $strMinPostMonth ) ) || ( strtotime( $strCurrentPostMonth ) > strtotime( $strMaxPostMonth ) ) ) {
			$boolIsValid &= false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Entrata does not support transactions outside the date year range of 1970 to 2099.' ) ) );
		} elseif( ( CPropertyGlSetting::VALIDATION_MODE_OPEN_PERIODS == $intUserValidationMode || CPropertyGlSetting::VALIDATION_MODE_STRICT == $intUserValidationMode ) && false == is_null( $this->m_objArTransaction->getPostMonth() ) && true == CValidation::validateDate( $objPropertyGlSetting->getArLockMonth() ) && strtotime( $strLockPeriod ) >= strtotime( $strPostMonth ) ) {
			$boolIsValid &= false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Warning: Period locked. You cannot post a transaction on or before {%t, 0, DATE_NUMERIC_POSTMONTH}', [ $objPropertyGlSetting->getArLockMonth() ] ) ) );
		}

		if( false == $boolIsValid || false == $this->m_objArTransaction->getValidatePastOrFuturePostMonth() || true == $this->m_objArTransaction->getOverridePostMonthPermission() ) return $boolIsValid;

		// This condition makes sure that we do not validate post month permission while auto allocation
		if( true == $boolIsValPostMonth ) return true;

		$intPastPostMonth 	= strtotime( $objPropertyGlSetting->getArPostMonth() );
		$intFuturePostMonth = $intPastPostMonth;

		// Get valid post month values
		if( false == is_null( $this->m_objArTransaction->getPastPostMonth() ) && false == is_null( $this->m_objArTransaction->getFuturePostMonth() ) ) {
			$intPastPostMonth 	= strtotime( date( 'm/d/Y', $intPastPostMonth ) . ' - ' . $this->m_objArTransaction->getPastPostMonth() . ' month' );
			$intFuturePostMonth = strtotime( date( 'm/d/Y', $intFuturePostMonth ) . ' + ' . $this->m_objArTransaction->getFuturePostMonth() . ' month' );
		}

		// Check if month is between past and future
		if( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) {

			// Modify message for REFUND
			if( CArCodeType::REFUND == $this->m_objArTransaction->getArCodeTypeId() ) {
				if( $intPastPostMonth != $intFuturePostMonth ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of the refund should be between {%t, past_post_month, DATE_NUMERIC_POSTMONTH} and {%t, future_post_month, DATE_NUMERIC_POSTMONTH}', [ 'past_post_month' => $intPastPostMonth, 'future_post_month' => $intFuturePostMonth ] ) ) );
				} else {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of the refund should be {%t, future_post_month, DATE_NUMERIC_POSTMONTH}', [ 'future_post_month' => date( 'm/d/Y', $intFuturePostMonth ) ] ) ) );
				}
				return false;
			}

			if( $intPastPostMonth != $intFuturePostMonth ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be between {%t, past_post_month, DATE_NUMERIC_POSTMONTH} and {%t, future_post_month, DATE_NUMERIC_POSTMONTH} for this transaction.', [ 'past_post_month' => $intPastPostMonth, 'future_post_month' => $intFuturePostMonth ] ) ) );
			} else {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be {%t, future_post_month, DATE_NUMERIC_POSTMONTH} for this transaction.', [ 'future_post_month' => $intFuturePostMonth ] ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valDeletePostMonth( $objDatabase ) {

		$boolIsValid = true;

		if( true == $this->m_objArTransaction->getIsTemporary() ) return $boolIsValid;

		$objPropertyGlSetting   = $this->m_objArTransaction->getOrFetchPropertyGlSetting( $objDatabase );
		$strPostMonth           = date( 'm/1/Y', strtotime( $this->m_objArTransaction->getDeletePostMonth() ) );
		$strMinPostMonth        = date( 'm/d/Y', strtotime( '1/1/1970' ) );
		$strMaxPostMonth        = date( 'm/d/Y', strtotime( '12/1/2099' ) );

		// Check if post_month is valid
		if( false === CValidation::checkDateFormat( $this->m_objArTransaction->getDeletePostMonth() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'Post month must be in mm/yyyy format.' ) ) );
			return false;
		}

		if( strtotime( $objPropertyGlSetting->getArLockMonth() ) >= strtotime( $this->m_objArTransaction->getDeletePostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'Warning: Period locked. You cannot post a reversal on or before {%t, ar_lock_month, DATE_NUMERIC_STANDARD}.', [ 'ar_lock_month' => $objPropertyGlSetting->getArLockMonth() ] ) ) );
		}

		if( strtotime( $this->m_objArTransaction->getDeletePostMonth() ) < strtotime( $this->m_objArTransaction->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'Transactions cannot be reversed in a post month earlier than the post month on the original transaction ( {%t, post_month, DATE_NUMERIC_POSTMONTH} )', [ 'post_month' => $this->m_objArTransaction->getPostMonth() ] ) ) );
		}

		if( ( strtotime( $strPostMonth ) < strtotime( $strMinPostMonth ) ) || ( strtotime( $strPostMonth ) > strtotime( $strMaxPostMonth ) ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'Entrata does not support transactions outside the date year range of 1970 to 2099.' ) ) );
		}

		if( false == $boolIsValid || false == $this->m_objArTransaction->getValidatePastOrFuturePostMonth() || true == $this->m_objArTransaction->getOverridePostMonthPermission() ) return $boolIsValid;

		$intPastPostMonth 	= strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) );
		$intFuturePostMonth = $intPastPostMonth;

		if( false == is_null( $this->m_objArTransaction->getPastPostMonth() ) && false == is_null( $this->m_objArTransaction->getFuturePostMonth() ) ) {
			$intPastPostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) . ' - ' . $this->m_objArTransaction->getPastPostMonth() . ' month' );
			$intFuturePostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) . ' + ' . $this->m_objArTransaction->getFuturePostMonth() . ' month' );
		}

		// Check if month is between past and future
		if( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) {
			if( $intPastPostMonth != $intFuturePostMonth ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Reverse post month should be between {%t, past_post_month, DATE_NUMERIC_POSTMONTH} and {%t, future_post_month, DATE_NUMERIC_POSTMONTH} for this transaction.', [ 'past_post_month' => $intPastPostMonth, 'future_post_month' => $intFuturePostMonth ] ) ) );
			} else {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Reverse post month should be {%t, past_post_month, DATE_NUMERIC_POSTMONTH} for this transaction.', [ 'future_post_month' => $intFuturePostMonth ] ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valConflictingBeginingBalanceArTransaction( $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Invalid database object.' ), E_USER_ERROR );
		}

		$boolIsValid		= true;
		$objArTransaction 	= \Psi\Eos\Entrata\CArTransactions::createService()->fetchConflictingArTransactionByArTransactionBySystemCode( $this->m_objArTransaction, CDefaultArCode::BEGINNING_BALANCE, $objDatabase );

		if( true == valObj( $objArTransaction, 'CArTransaction' ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( ' A company lease may have at most one begining balance transaction.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTransactionAmount( $strAction = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case NULL:
			case 'post_deposit_held_ar_transaction':
				if( 0 == ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Beginning deposit amount should not be zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_charge_ar_transaction':
				if( 0 >= ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Charge amount must be greater than zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'transaction_approval_request':
				if( 0 == ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Amount cannot be zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_check_ar_transaction':
				if( 0 >= ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Check amount must be greater than zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_concession_ar_transaction':
				// FYI - Concession amount is positive but transaction_amount for a concession is negative
				if( 0 <= ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'concession_amount', __( 'Credit amount must be greater than zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_payment_ar_transaction':
				// FYI - Concession amount is positive but transaction_amount for a concession is negative
				if( 0 == ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Payment must have an amount greater than zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_payment_reverse_ar_transaction':
				// FYI - Reverse payment amount is negative but transaction_amount for a reverse is negative
				if( 0 >= ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Payment must have an amount greater than zero.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'post_adjustment_ar_transaction':
				if( false == is_numeric( $this->m_objArTransaction->getTransactionAmount() ) && 0 != $this->m_objArTransaction->getTransactionAmount() ) {
					$boolIsValid = false;
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Adjustment amount is not valid.' ) ) );
				}
				break;

			case 'post_bulk_charge_ar_transaction':
				if( false == is_numeric( $this->m_objArTransaction->getTransactionAmount() ) || 0 == $this->m_objArTransaction->getTransactionAmount() ) {
					$boolIsValid = false;
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Amount must be greater than zero.' ) ) );
				}
				break;

			case 'post_move_in_charge':
				if( 0 == ( float ) $this->m_objArTransaction->getTransactionAmount() ) {
					$boolIsValid = false;
					$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Charge amount cannot be zero.' ) ) );
				}
				break;

			default:
				// deafault case;
				break;
		}

		return $boolIsValid;
	}

	public function valConcessionAllocationTotal( $arrobjLeaseAllocations ) {
		$boolIsValid 		= true;
		$fltAllocationTotal = 0.00;

		if( true == valArr( $arrobjLeaseAllocations ) ) {
			foreach( $arrobjLeaseAllocations as $objLeaseAllocation ) {
				$fltAllocationTotal += $objLeaseAllocation->getTransactionAmount();
			}
		}

		if( $fltAllocationTotal > ( -1 * $this->m_objArTransaction->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Allocation total cannot exceed transaction amount.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReturnAbility( $objPaymentDatabase ) {

		$boolIsValid = true;

		$this->m_objArTransaction->getOrFetchArPayment( $objPaymentDatabase );

		if( true == valObj( $this->m_objArTransaction->getArPayment(), 'CArPayment' ) ) {
			if( CPaymentStatusType::RECEIVED != $this->m_objArTransaction->getArPayment()->getPaymentStatusTypeId() ) {
				$boolIsValid = false;
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You can only return non electronic payments with status = Received.' ) ) );
			}

			if( true == CPaymentTypes::isElectronicPayment( $this->m_objArTransaction->getArPayment()->getPaymentTypeId() ) ) {
				$boolIsValid = false;
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot return electronic payments.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDisassociateAbility( $objPaymentDatabase ) {

		$boolIsValid = true;

		$this->m_objArTransaction->getOrFetchArPayment( $objPaymentDatabase );

		if( false == valObj( $this->m_objArTransaction->getArPayment(), 'CArPayment' ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Payment failed to load.' ) ) );
			return;
		}

		$arrintPaymentStatusTypesAllowedtoDisassociate = [ CPaymentStatusType::CAPTURED, CPaymentStatusType::AUTHORIZED, CPaymentStatusType::PHONE_CAPTURE_PENDING, CPaymentStatusType::RECALL, CPaymentStatusType::RECEIVED ];

		if( false == in_array( $this->m_objArTransaction->getArPayment()->getPaymentStatusTypeId(), $arrintPaymentStatusTypesAllowedtoDisassociate ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'The status of the payment you are trying to disassociate is not allowed.' ) ) );
		}

		return $boolIsValid;
	}

	// Make sure that a payment can't be reversed if it is already in a deposit.  The deposit needs to be deleted first.

	public function valDeleteAbility( $objDatabase ) {

		$boolIsValid = true;

		if( true == $this->m_objArTransaction->getIsDeleted() ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot delete a reversed transaction.' ) ) );
			return false;
		}

		$arrintAllowedArCodeTypesForReversals = array_merge( CArCodeType::$c_arrintDepositAndOtherFeesArCodeTypes, [ CArCodeType::RENT, CArCodeType::REFUND, CArCodeType::PAYMENT ] );

		if( false == in_array( $this->m_objArTransaction->getArCodeTypeId(), $arrintAllowedArCodeTypesForReversals ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot reverse a transaction that is not of type charge, credit, payment, refund check or refund adjustment.' ) ) );
			return false;
		}

		if( CArCodeType::PAYMENT == $this->m_objArTransaction->getArCodeTypeId() && true == is_numeric( $this->m_objArTransaction->getArPaymentId() ) ) {
			$objArPayment = $this->m_objArTransaction->fetchWorksArPayment( $objDatabase );

			if( true == CPaymentTypes::isElectronicPayment( $objArPayment->getPaymentTypeId() ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot delete an electronic payment.  You must reverse it in the Payments module.' ) ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valResidentWorksChargeOrConcessionArTransaction( $arrobjArAllocations, $objClientDatabase, $boolDissallowZeroAmounts = false, $boolIsValPostMonth = false, $boolIsIgnoreChargeAmount = false, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {

		$boolIsValid 					 = true;
		$boolIsAddANewChargeOrConcession = ( ( 0 == strlen( $this->m_objArTransaction->getTransactionAmount() ) || 0 == $this->m_objArTransaction->getTransactionAmount() ) && false == $boolDissallowZeroAmounts ) ? false : true;
		$boolUseUnallocatedFunds		 = ( false == is_null( $this->m_objArTransaction->getUseUnallocatedAmount() ) )  ? true : false;

		if( ( false == $boolIsAddANewChargeOrConcession && false == $boolUseUnallocatedFunds ) ) {
			$boolIsValid &= false;
			if( true == valArr( $arrobjArAllocations ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Please enter a payment amount or apply unallocated funds or both.' ) ) );
			} else {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Please enter a payment amount.' ) ) );
			}
		}

		if( true == $boolIsAddANewChargeOrConcession ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->valLeaseId();
			$boolIsValid &= $this->valArCodeId( $objClientDatabase );
			$boolIsValid &= $this->valArCodeTypeId();
			$boolIsValid &= $this->valPostDate();

			$strAction = ( 0 < $this->m_objArTransaction->getTransactionAmount() ) ? 'post_charge_ar_transaction' : 'post_concession_ar_transaction';

			if( false == $boolIsIgnoreChargeAmount ) {
				$boolIsValid &= $this->valTransactionAmount( $strAction );
			}

			$boolIsValid &= $this->valPostMonth( $objClientDatabase, $boolIsValPostMonth, $intUserValidationMode );
		}

		// Make sure that none of the allocations are over the amount due and that the total of them all is not greater than the unallocated amount
		$fltAllocationsTotal = 0.00;
		if( true == valArr( $arrobjArAllocations ) ) {
			foreach( $arrobjArAllocations as $objArAllocation ) {
				if( true == isset( $_REQUEST['ar_allocations'][$objArAllocation->getCreditArTransactionId()]['selected'] ) && 1 == $_REQUEST['ar_allocations'][$objArAllocation->getCreditArTransactionId()]['selected'] ) {
					$fltAllocationsTotal += str_replace( '$', '', $_REQUEST['ar_allocations'][$objArAllocation->getCreditArTransactionId()]['allocation_amount'] );
				}
			}
		}

		$fltCeilingAllocationAmount = ( float ) $this->m_objArTransaction->getTransactionAmount();

		if( true == $boolUseUnallocatedFunds ) {
			// Load the allocations for this payment
			$objLease = $this->m_objArTransaction->getOrFetchLease( $objClientDatabase );

			$fltUnallocatedAmount = 0.00;
			if( true == valObj( $objLease, 'CLease' ) ) {
				$fltUnallocatedAmount = $objLease->fetchUnallocatedChargeAmount( $objClientDatabase );
			}
			$fltCeilingAllocationAmount += ( float ) $fltUnallocatedAmount;
		}

		if( round( abs( $fltCeilingAllocationAmount ), 2 ) < round( abs( $fltAllocationsTotal ), 2 ) ) {
			$boolIsValid &= false;
			if( true == $boolUseUnallocatedFunds ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Allocation amount cannot exceed the transaction amount plus the unallocated amount.' ) ) );
			} else {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Allocation amount cannot exceed the transaction amount.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valBadDebtArTransaction() {

		$boolIsValid = true;

		if( 0 <= $this->m_objArTransaction->getTransactionAmount() ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, 'transaction_amount', __( 'Amount should not be zero.' ) ) );

		} elseif( 0 == $this->m_objArTransaction->getBadDebtLimitAmount() ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, 'transaction_amount', __( 'We could not find transactions to write off.' ) ) );

		} elseif( $this->m_objArTransaction->getTransactionAmount() < ( -1 * $this->m_objArTransaction->getBadDebtLimitAmount() ) ) {
			$boolIsValid = false;
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, 'transaction_amount', __( 'Amount cannot be greater than {%m, 0} .', [ $this->m_objArTransaction->getBadDebtLimitAmount() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valWriteOffTransaction( $objDatabase ) {

		if( CArTrigger::BAD_DEBT_WRITE_OFF == $this->m_objArTransaction->getArTriggerId() ) return false;

		if( 0 < $this->m_objArTransaction->getTransactionAmount() ) {
			$arrobjCreditTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedArTransactionsByArTransactionIdsByCid( [ $this->m_objArTransaction->getId() ], $this->m_objArTransaction->getCid(), $objDatabase, [ CArTrigger::BAD_DEBT_WRITE_OFF ] );
			if( true == valArr( $arrobjCreditTransactions ) ) return false;
		}

		return true;
	}

	public function valChargeArTransactionInUserValidationMode( $strAction, $objDatabase, $intUserValidationMode ) {

		$arrobjGlDetails 		= ( array ) CGlDetails::fetchGlDetailsForUserValidationModeByArTransactionIdByCid( $this->m_objArTransaction->getId(), $this->m_objArTransaction->getCid(), $objDatabase );
		$objPropertyGlSetting	= $this->m_objArTransaction->getOrFetchPropertyGlSetting( $objDatabase );
		$strAction 				= ( 'hard_edit_charge' == $strAction ) ? __( 'update' ) : __( 'delete' );

		foreach( $arrobjGlDetails as $objGlDetail ) {
				/**@var $objGlDetail \CGlDetail*/
			if( false == is_null( $objGlDetail->getAccountingExportBatchId() ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot hard {%s, 0} this charge as related GL is exported.', [ $strAction ] ) ) );
				return false;
			}

			if( CPropertyGlSetting::VALIDATION_MODE_OPEN_PERIODS == $intUserValidationMode && date( 'Y-m-d H:i:s', strtotime( $objGlDetail->getPostMonth() ) ) <= date( 'Y-m-d H:i:s', strtotime( $objPropertyGlSetting->getArLockMonth() ) ) ) {
				$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot hard {%s, 0} this charge as transaction is in close period.', [ $strAction ] ) ) );
				return false;
			}
		}
		return true;
	}

	public function valHardEditDeleteDepositTransaction( $objDatabase, $strAction, $intOldArTransactionId ) {

		$strAction										= ( 'hard_edit_beginning_deposit_held' == $strAction ) ? __( 'edit' ) : __( 'delete' );
		$fltTotalBeginningDepositHeldTransactionAmount 	= 0;
		$fltTotalDepositCreditTransactionAmount 		= 0;
		$arrobjArTransactions 							= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchDepositArTransactionsByLeaseIdsByCid( [ $this->m_objArTransaction->getLeaseId() ], $this->m_objArTransaction->getCid(), $objDatabase );
		$objPropertyGlSetting							= $this->m_objArTransaction->getOrFetchPropertyGlSetting( $objDatabase );

		foreach( $arrobjArTransactions as $objArTransaction ) {

			if( CArCodeType::DEPOSIT == $objArTransaction->getArCodeTypeId() && false == $objArTransaction->getIsDepositCredit() && $intOldArTransactionId != $objArTransaction->getId() ) {
				if( false == is_null( $objArTransaction->getArTransactionId() ) ) {
					$fltTotalBeginningDepositHeldTransactionAmount += ( $objArTransaction->getTransactionAmount() );
				} else {
					$fltTotalBeginningDepositHeldTransactionAmount += ( $objArTransaction->getTransactionAmount() - $objArTransaction->getTransactionAmountDue() );
				}

			}

			if( CArCodeType::DEPOSIT == $objArTransaction->getArCodeTypeId() && true == $objArTransaction->getIsDepositCredit() ) {
				$fltTotalDepositCreditTransactionAmount += ( $objArTransaction->getTransactionAmount() * -1 ) - ( $objArTransaction->getTransactionAmountDue() * -1 );
			}
		}

		if( 'edit' == $strAction ) {
			$fltTotalBeginningDepositHeldTransactionAmount += $this->m_objArTransaction->getTransactionAmount();
		}

		if( $fltTotalBeginningDepositHeldTransactionAmount < $fltTotalDepositCreditTransactionAmount ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot hard {%s, 0} this beginning deposit because it is already associated with charges.', [ $strAction ] ) ) );
			return false;
		}

		return true;
	}

	public function valNotes() {

		if( '' == $this->m_objArTransaction->getMemo() && false == \Psi\CStringService::singleton()->preg_match( '#([^\s]+)#', $this->m_objArTransaction->getMemo() ) ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Notes is required.' ) ) );
			return false;
		}
		return true;
	}

	public function valSubsidyChargeCode( $objDatabase ) {

		$objLease   = $this->m_objArTransaction->getOrFetchLease( $objDatabase );
		$intDefaultLedgerFilterId = \Psi\Eos\Entrata\CArCodes::createService()->fetchDefaultLedgerFilterIdByIdByCid( $this->m_objArTransaction->getArCodeId(), $this->m_objArTransaction->getCid(), $objDatabase );

		if( COccupancyType::AFFORDABLE != $objLease->getOccupancyTypeId() && CDefaultLedgerFilter::SUBSIDY == $intDefaultLedgerFilterId ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Subsidy codes cannot be used on non-subsidized leases.' ) ) );
			return false;
		}
		return true;
	}

	/**
	 * @param \CDatabase $objDatabase
	 * @return bool
	 * @see It will check wheather used charge code is eligible for the military lease or not.
	 */

	public function valMilitaryChargeCode( CDatabase $objDatabase ) {

		$objLease					= $this->m_objArTransaction->getOrFetchLease( $objDatabase );
		$intDefaultLedgerFilterId	= \Psi\Eos\Entrata\CArCodes::createService()->fetchDefaultLedgerFilterIdByIdByCid( $this->m_objArTransaction->getArCodeId(), $this->m_objArTransaction->getCid(), $objDatabase );

		if( CDefaultLedgerFilter::MILITARY == $intDefaultLedgerFilterId && COccupancyType::MILITARY !== $objLease->getOccupancyTypeId() ) {
			$this->m_objArTransaction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Military charge codes cannot be used on non-military leases.' ) ) );
			return false;
		}

		return true;
	}

}
?>