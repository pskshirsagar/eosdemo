<?php

class CStandardApInvoiceValidator extends CApInvoiceValidator {

	// Validate function

	public function validate( $strAction, $arrmixValidationData = [] ) {
		$boolIsValid = true;

		$this->setValidationData( $arrmixValidationData );

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'quick_pay_invoice':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApHeaderTypeId();
				$boolIsValid &= $this->valApHeaderSubTypeId();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valApPayeeAccountId();
				$boolIsValid &= $this->valApRemittanceId();
				$boolIsValid &= $this->valApPayeeTermId();
				$boolIsValid &= $this->valApPayeeDetails();
				$boolIsValid &= $this->valApPayeeStatusTypeId();
				$boolIsValid &= $this->valApPayeeComplianceStatus();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valPoApHeaderIds();
				$boolIsValid &= $this->valApRoutingTagId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valDuplicateInvoiceNumber();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valTransactionAmountDue();
				$boolIsValid &= $this->valTaxAmounts();
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valApPayeeProperties();
				$boolIsValid &= $this->valQuickPay();
				break;

			case 'insert_invoice':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApHeaderTypeId();
				$boolIsValid &= $this->valApHeaderSubTypeId();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valApPayeeAccountId();
				$boolIsValid &= $this->valApRemittanceId();
				$boolIsValid &= $this->valApPayeeTermId();
				$boolIsValid &= $this->valApPayeeDetails();
				$boolIsValid &= $this->valApPayeeStatusTypeId();
				$boolIsValid &= $this->valApPayeeComplianceStatus();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valPoApHeaderIds();
				$boolIsValid &= $this->valApRoutingTagId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valDuplicateInvoiceNumber();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valTransactionAmountDue();
				$boolIsValid &= $this->valTaxAmounts();
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valApPayeeProperties();
				break;

			case 'update_invoice':
				// Check if the invoice is posted
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// Get Functions

	// set Functions

	// Val Functions

}
?>