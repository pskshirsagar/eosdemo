<?php
class CRateValidator {

	protected $m_objRate;

	public function __construct() {
		return;
	}

	public function setRate( $objRate ) {
		$this->m_objRate = $objRate;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->m_objRate->getCid() ) || 0 >= $this->m_objRate->getCid() ) {
			trigger_error( __( 'Invalid Company Rate Request:  Client Id is required - CRate::valCid()' ), E_USER_ERROR );
			exit;
		}

		return $boolIsValid;
	}

	public function valArCascadeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArCascadeId() ) || 0 >= $this->m_objRate->getArCascadeId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_cascade_id', __( 'Cascade id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArCascadeReferenceId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArCascadeReferenceId() ) || 0 >= $this->m_objRate->getArCascadeReferenceId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_cascade_reference_id', __( 'Cascade reference id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArOriginId() ) || 0 >= $this->m_objRate->getArOriginId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_origin_id', __( 'Origin id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArOriginCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPhaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArPhaseId() ) || 0 >= $this->m_objRate->getArPhaseId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_phase_id', __( 'Phase id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArTriggerId() ) || 0 >= $this->m_objRate->getArTriggerId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', __( 'Please select charge at.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArCodeTypeId() ) || 0 >= $this->m_objRate->getArCodeTypeId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_type_id', __( 'Please select ar code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArCodeId() ) || 0 >= $this->m_objRate->getArCodeId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Please select ar code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getLeaseIntervalTypeId() ) || 0 >= $this->m_objRate->getLeaseIntervalTypeId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_type_id', __( 'Lease interval type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArFormulaId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getArFormulaId() ) || 0 >= $this->m_objRate->getArFormulaId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_formula_id', __( 'Please select fee handling.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArFormulaReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getSpaceConfigurationId() ) || 0 >= $this->m_objRate->getSpaceConfigurationId() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_configuration_id', __( 'Space configuration id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;

		if( CArTrigger::APPLICATION_COMPLETED == $this->m_objRate->getArTriggerId() ) {
			if( true == is_null( $this->m_objRate->getCustomerRelationshipId() ) || 0 >= $this->m_objRate->getCustomerRelationshipId() ) {
				$boolIsValid = false;
				$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Please select charge fee to.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowOffsetStartDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowLeaseStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getWindowLeaseStartDate() ) || 0 >= $this->m_objRate->getWindowLeaseStartDate() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'window_lease_start_date', __( 'Window lease start date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateAmount( $objDatabase ) {

		$boolIsValid = true;

		if( CArFormula::FIXEDAMOUNT == $this->m_objRate->getArFormulaId() ) {
			if( true == valObj( $objDatabase, 'CDatabase' ) ) {
				$objArCodesFilter = new CArCodesFilter();
				$objArCodesFilter->setGlAccountTypeIds( [ CGlAccountType::INCOME, CGlAccountType::LIABILITIES ] );

				$arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->m_objRate->getCid(), $objArCodesFilter, $objDatabase );
			}

			if( true == is_null( $this->m_objRate->getRateAmount() ) || false == $this->m_objRate->getRateAmount() ) {
				$boolIsValid = false;
				$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_amount', __( 'Rate amount is required.' ) ) );
			} elseif( 0 > $this->m_objRate->getRateAmount() && ( true == is_null( $arrobjArCodes ) || false == valArr( $arrobjArCodes ) ) ) {

				// Allow -ve rent amount for all charges of gl_account_type operating income and ar_trigger move-in or recurring
				$boolIsValid = false;
				$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_amount', __( 'Rate amount can\'t be less than zero for selected charge code or charge type.' ) ) );
			} elseif( 0 > $this->m_objRate->getRateAmount() && ( false == array_key_exists( $this->m_objRate->getArCodeId(), $arrobjArCodes ) || false == array_key_exists( $this->m_objRate->getArTriggerId(), [ CArTrigger::MOVE_IN => CArTrigger::MOVE_IN, CChargeType::Hourly => CChargeType::Hourly, CChargeType::DAILY => CChargeType::DAILY, CChargeType::WEEKLY => CChargeType::WEEKLY, CChargeType::EVERYTWOWEEKS => CChargeType::EVERYTWOWEEKS, CChargeType::TWICEPERMONTH => CChargeType::TWICEPERMONTH, CChargeType::MONTHLY => CChargeType::MONTHLY, CChargeType::QUARTERLY => CChargeType::QUARTERLY, CChargeType::TWICEPERYEAR => CChargeType::TWICEPERYEAR, CChargeType::YEARLY => CChargeType::YEARLY ] ) ) ) {

				// Allow -ve rent amount for all charges of gl_account_type operating
				// income and ar_trigger move-in or recurring
				$boolIsValid = false;
				$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_amount', __( 'Rate amount can\'t be less than zero for selected charge code or charge type.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valRatePercent() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getRatePercent() ) || 0 >= $this->m_objRate->getRatePercent() ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_percent', __( 'Please select percent of charge code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNormalizedAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getNormalizedAmount() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'normalized_amount', __( 'Normalized amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNormalizedPercent() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getNormalizedPercent() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'normalized_percent', __( 'Normalized percent is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateIntervalStart() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getRateIntervalStart() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_interval_start', __( 'Rate interval start is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateIntervalOccurances() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getRateIntervalOccurances() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_interval_occurances', __( 'Rate interval occurances is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateIntervalOffset() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getRateIntervalOffset() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_interval_offset', __( 'Rate interval offset is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMinAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objRate->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->m_objRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Please select effective date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeactivationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $objProperty ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valArCascadeId();
				$boolIsValid &= $this->valArCascadeReferenceId();
				$boolIsValid &= $this->valArOriginId();
				$boolIsValid &= $this->valArPhaseId();
				$boolIsValid &= $this->valArTriggerId();
				$boolIsValid &= $this->valArCodeTypeId();
				$boolIsValid &= $this->valArCodeId();
				$boolIsValid &= $this->valLeaseIntervalTypeId();
				$boolIsValid &= $this->valArFormulaId();
				$boolIsValid &= $this->valSpaceConfigurationId();
				$boolIsValid &= $this->valCustomerRelationshipId();
				$boolIsValid &= $this->valLeaseTermMonths();
				$boolIsValid &= $this->valWindowOffsetStartDays();
				$boolIsValid &= $this->valWindowLeaseStartDate();
				$boolIsValid &= $this->valRateAmount( $objDatabase );
				$boolIsValid &= $this->valRatePercent();
				$boolIsValid &= $this->valNormalizedAmount();
				$boolIsValid &= $this->valNormalizedPercent();
				$boolIsValid &= $this->valRateIntervalStart();
				$boolIsValid &= $this->valRateIntervalOccurances();
				$boolIsValid &= $this->valRateIntervalOffset();
				$boolIsValid &= $this->valEffectiveDate();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>