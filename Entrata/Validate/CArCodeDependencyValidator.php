<?php

class CArCodeDependencyValidator {

	const MODE_EDIT_CHARGE_CODE = 'EDIT_CHARGE_CODE';
	const MODE_CLEAN_UP_CHARGE_CODE = 'CLEAN_UP_CHARGE_CODE';

	public static function checkArCodeDependencies( $objDatabase,  $objArCode = NULL, $objDataChange = NULL, $boolIsSqlOnly = false, $boolIsAffectedTablesOnly = false,$intChargeCodeMode = self::MODE_EDIT_CHARGE_CODE ) {

		$boolIsValid 		= true;

		if( false == valObj( $objArCode, 'CArCode' ) && false == valObj( $objDataChange, 'CDataChange' ) ) {
			$boolIsValid = false;
			return $boolIsValid;
		}

		$arrstrSqls   		= [];
		$arrstrSubSqls   	= [];
		$strErrorMessage 	= NULL;

		if( true == valObj( $objArCode, 'CArCode' ) ) {

			$intArCodeId = $objArCode->getId();
			$intCid		 = $objArCode->getCId();

			if( false == $objArCode->getIsDisabled() ) {
				return $boolIsValid;
			}

			// If user disabling any system_ar_code then do not make any database request
			if( true == $objArCode->getIsDisabled() && true == $objArCode->getIsSystem() ) {
				$strErrorMessage = __( 'System charge codes can\'t be disabled.' );
				$boolIsValid &= false;
				$objArCode->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'enabled', $strErrorMessage ) );
				return $boolIsValid;
			}

			$objArCode->valPropertyGlSettingDependencies( 'enabled', $objDatabase );
		}

		if( true == valObj( $objDataChange, 'CDataChange' ) ) {
			$intArCodeId = $objDataChange->getOldReferenceId();
			$intCid		 = $objDataChange->getCId();
		}

		/** @var $objAbstractArCodeDependencySqlGenerator \Psi\Core\AccountsReceivables\ArCodes\ArCodeDependencySqlGenerator\CAbstractArCodeDependencyValidatorSqlGenerator */

	  $objAbstractArCodeDependencySqlGenerator = \Psi\Core\AccountsReceivables\ArCodes\ArCodeDependencySqlGenerator\CArCodeDependencyValidatorSqlGeneratorFactory::getObject( $intChargeCodeMode );
	  $arrstrSqls = $objAbstractArCodeDependencySqlGenerator->getPropertyRelatedSqls( $intCid, $intArCodeId );

	  if( true == $boolIsSqlOnly ) {
	  	return 	$arrstrSqls;
	  }

	  $strSql = implode( PHP_EOL . ' UNION ALL ' . PHP_EOL, $arrstrSqls );

	  $arrmixArCodeDependencies = fetchData( $strSql, $objDatabase );

	  $arrstrSubSqls = $objAbstractArCodeDependencySqlGenerator->getNonPropertyRelatedSqls( $intCid, $intArCodeId );

	  $strNewSql = implode( PHP_EOL . ' UNION ALL ' . PHP_EOL, $arrstrSubSqls );

	  $arrmixArCodeDependencies_without_property_id = fetchData( $strNewSql, $objDatabase );

	  $arrmixArCodeDependencies = array_merge( $arrmixArCodeDependencies_without_property_id, $arrmixArCodeDependencies );

	  	if( true == $boolIsAffectedTablesOnly ) {
			return $arrmixArCodeDependencies;
		}

		foreach( $arrmixArCodeDependencies as $arrmixArCodeDependency ) {
			if( 1 == $arrmixArCodeDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not update charge code, it is in use' ) : ' ';
				$boolIsValid = false;
			} elseif( 1 < $arrmixArCodeDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not update charge code, it is in use' ) : ' ';
				$boolIsValid = false;
			}

		}

		if( false == $boolIsValid ) {
			$strErrorMessage .= '.';
			$objArCode->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'validate', $strErrorMessage ) );
		}

		return [ $boolIsValid, $arrmixArCodeDependencies ];

	}

}
?>