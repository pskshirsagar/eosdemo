<?php

/**
 * Class CPetValidator
 */
class CPetValidator {

	protected $m_objLease;
	protected $m_arrobjErrorMsgs;
	protected $m_arrobjWarningMsgs;

	protected $m_boolIsNewPet;

    public function __construct() {
        return;
    }

	public function setLease( CLease $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function addErrorMsg( $objErrorMsg, $boolTreatAsWarning = false ) {
    	if( true == $boolTreatAsWarning ) {
			$this->m_arrobjWarningMsgs[] = $objErrorMsg;
		} else {
			$this->m_arrobjErrorMsgs[] = $objErrorMsg;
		}
	}

	public function getErrorMsgs() {
		return $this->m_arrobjErrorMsgs;
	}

	public function getWarningMsgs() {
		return $this->m_arrobjWarningMsgs;
	}

	/**
	* Validation Functions
	*
	*/

	/**
	 * @param string     $strAction
	 * @param array      $arrobjCustomerPets
	 * @param \CDatabase $objDatabase
	 * @return bool
	 */
	public function bulkValidate( string $strAction, array $arrobjCustomerPets, CDatabase $objDatabase ) {

		$boolIsValid	= true;

		if( false == valArr( $arrobjCustomerPets ) ) {
			return $boolIsValid;
		}

		$arrmixCustomerPetsData		= $this->preparePetValidateData( $arrobjCustomerPets, $objDatabase );

		if( false == valArr( $arrmixCustomerPetsData['pet_rate_associations'] ) ) {
			return $boolIsValid;
		}

		switch( $strAction ) {

			case 'validate_manage_pet':
				$boolIsValid &= $this->valMaxPetLimit( $arrmixCustomerPetsData, $objDatabase );
				$boolIsValid &= $this->valMaxPetTypeLimit( $arrmixCustomerPetsData );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @param array      $arrmixCustomerPetsData
	 * @param \CDatabase $objDatabase
	 * @return bool
	 */
	public function valMaxPetLimit( array $arrmixCustomerPetsData, CDatabase $objDatabase ) {

		$boolIsValid	= true;
		if( false == valId( $this->m_objLease->getPropertyUnitId() ) ) {
			return $boolIsValid;
		}

		$intPetCount				= 0;
		$arrobjCustomerPets			= $arrmixCustomerPetsData['customer_pets'];
		$boolTreatAsWarning		= $arrmixCustomerPetsData['is_treat_as_warning'];
		$arrmixPetRateAssociations	= $arrmixCustomerPetsData['pet_rate_associations'];

		foreach( $arrobjCustomerPets as $objCustomerPet ) {
			if( true == valObj( $arrmixPetRateAssociations, 'CPetRateAssociation' ) ) {
				if( 999 == $arrmixPetRateAssociations[$objCustomerPet->getPetTypeId()]->getPetMaxAllowed() ) {
					continue;
				}
			}

			$intPetCount++;
		}

		$objPropertyUnit = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->m_objLease->getPropertyUnitId(), $this->m_objLease->getCid(), $objDatabase );

		if( true == valId( $objPropertyUnit->getMaxPets() ) && $intPetCount > $objPropertyUnit->getMaxPets() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Pet Limit Exceed. Allowed Limit is {%d, 0}', [ $objPropertyUnit->getMaxPets() ] ), NULL, NULL ), $boolTreatAsWarning );
			if( false == $boolTreatAsWarning ) {
				$boolIsValid	= false;
			}
		}

		return $boolIsValid;
	}

	/**
	 * @param array $arrmixCustomerPetsData
	 * @return bool
	 */
	public function valMaxPetTypeLimit( array $arrmixCustomerPetsData ) {

		$boolIsValid				= true;
		$arrobjCustomerPets			= $arrmixCustomerPetsData['customer_pets'];
		$arrintNewPetTypeIds		= $arrmixCustomerPetsData['new_pet_type_ids'];
		$arrmixPetRateAssociations	= $arrmixCustomerPetsData['pet_rate_associations'];

		$arrobjCustomerPets			= rekeyObjects( 'PetTypeId', $arrobjCustomerPets, true );

		foreach( $arrmixPetRateAssociations as $intPetTypeId => $objPetRateAssociation ) {

			$intPetMaxAllowedCount	= $objPetRateAssociation->getPetMaxAllowed();
			$intPetTypeCount		= \Psi\Libraries\UtilFunctions\count( $arrobjCustomerPets[$intPetTypeId] );

			if( 0 < $intPetMaxAllowedCount && $intPetTypeCount > $intPetMaxAllowedCount ) {

				$boolTreatAsWarning	= in_array( $intPetTypeId, $arrintNewPetTypeIds ) ? false : true;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'The maximum number of {%s, pet_name} allowed is {%d, max_limit}', [ 'pet_name' => $objPetRateAssociation->getPetTypeName(), 'max_limit' => ( int ) $intPetMaxAllowedCount ] ), NULL, NULL ), $boolTreatAsWarning );
				if( false == $boolTreatAsWarning ) {
					$boolIsValid	= false;
				}
			}
		}

		return $boolIsValid;
	}

	/**
	 * @param array      $arrobjCustomerPets
	 * @param \CDatabase $objDatabase
	 * @return array
	 */
	public function preparePetValidateData( array $arrobjCustomerPets, CDatabase $objDatabase ) {

		$boolTreatAsWarning				    = true;
		$arrobjNonAssistanceCustomerPets	= [];
		$arrintNewPetTypeIds				= [];
		$arrmixPetRateAssociations			= [];

		foreach( $arrobjCustomerPets as $objCustomerPet ) {

			if( false == $objCustomerPet->getIsAssistanceAnimal() && true == in_array( $objCustomerPet->getLeaseStatusTypeId(), [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) ) {
				$arrobjNonAssistanceCustomerPets[]	= $objCustomerPet;

				// checking insertion of new non-assistance pet for type of validation messages
				if( false == valId( $objCustomerPet->getId() ) ) {
					$boolTreatAsWarning	= false;
					$arrintNewPetTypeIds[]	= $objCustomerPet->getPetTypeId();
				} else {
					if( true == valArr( $objCustomerPet->getChangedColumns() ) && true == array_key_exists( 'IsAssistanceAnimal', $objCustomerPet->getChangedColumns() ) ) {
						$boolTreatAsWarning	= false;
					}
				}
			}

			if( true == valId( $objCustomerPet->getPetTypeId() ) ) {
				$arrintPetTypeIds[] = $objCustomerPet->getPetTypeId();
			}
		}

		if( true == valArr( $arrintPetTypeIds ) ) {
			$arrmixPetRateAssociations = ( array ) \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationsByLeaseIdByPropertyIdByArOriginReferenceIdsByCid( $this->m_objLease->getId(), $this->m_objLease->getPropertyId(), $arrintPetTypeIds, $this->m_objLease->getCid(), $objDatabase );
			$arrmixPetRateAssociations = rekeyObjects( 'PetTypeId', $arrmixPetRateAssociations );
		}

		return [ 'customer_pets' => $arrobjNonAssistanceCustomerPets, 'is_treat_as_warning' => $boolTreatAsWarning, 'new_pet_type_ids' => $arrintNewPetTypeIds, 'pet_rate_associations' => $arrmixPetRateAssociations ];
	}

}
?>