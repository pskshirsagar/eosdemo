<?php
class CArAllocationValidator {

	protected $m_objArAllocation;

	public function __construct() {
		return;
	}

	public function setArAllocation( $objArAllocation ) {
		$this->m_objArAllocation = $objArAllocation;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getLeaseId() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAllocationDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getAllocationDatetime() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_datetime', __( 'Allocation date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valChargeArTransactionId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getChargeArTransactionId() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_ar_transaction_id', __( 'Charge A/R transaction is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCreditArTransactionId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_ar_transaction_id', __( 'Credit A/R transaction is required.' ) ) );
		} elseif( $this->m_objArAllocation->getChargeArTransactionId() == $this->m_objArAllocation->getCreditArTransactionId() ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_ar_transaction_id', __( 'Charge A/R  should not be equal to Credit A/R transaction id.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAllocationAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objArAllocation->getAllocationAmount() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', __( 'Allocation amount is required.' ) ) );
		} elseif( 0 >= $this->m_objArAllocation->getAllocationAmount() && false == valId( $this->m_objArAllocation->getId() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', __( 'Invalid allocation amount.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAllocationIntegrity( $objDatabase ) {

		$boolIsValid 		  = true;
		$arrintTransactionIds = [];

		if( false == is_numeric( $this->m_objArAllocation->getChargeArTransactionId() ) || false == is_numeric( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			return $boolIsValid;
		}

		if( true == valStr( $this->m_objArAllocation->getChargeArTransactionId() ) ) {
			$arrintTransactionIds = [ $this->m_objArAllocation->getChargeArTransactionId() ];
		}

		if( true == valStr( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			array_push( $arrintTransactionIds, $this->m_objArAllocation->getCreditArTransactionId() );
		}

		$arrobjArTransactions 		 = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchViewArTransactionsByIdsByCid( $arrintTransactionIds, $this->m_objArAllocation->getCid(), $objDatabase );
		if( true == valArr( $arrobjArTransactions ) ) {
			if( true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' ) && true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) ) {
				$arrintArCodeLedgerFilterIds = \Psi\Eos\Entrata\CArCodes::createService()->fetchLedgerFilterIdsByArCodeIdsByCid( [ $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArCodeId(), $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArCodeId() ], $this->m_objArAllocation->getCid(), $objDatabase );

				$arrintArCodeLedgerFilterIds = rekeyArray( 'id', $arrintArCodeLedgerFilterIds );

				if( $arrintArCodeLedgerFilterIds[$arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArCodeId()]['ledger_filter_id'] != $arrintArCodeLedgerFilterIds[$arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArCodeId()]['ledger_filter_id'] ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ledger_filter_id', __( 'Charge and credit transaction\'s ledger type should be same.' ) ) );
					return false;
				}
				$arrintDefaultArCodeIds = [ CDefaultArCode::SUBSIDY_REFUND_PAYABLE, CDefaultArCode::UTILITY_REIMBURSEMENT, CDefaultArCode::OVERPAID_UTILITY_REIMBURSEMENT ];
				if( false == $this->m_objArAllocation->getIsDeleted()
					&& ( ( true == in_array( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getDefaultArCodeId(), $arrintDefaultArCodeIds )
						&& false == in_array( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getDefaultArCodeId(), $arrintDefaultArCodeIds ) ) ||
						( true == in_array( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getDefaultArCodeId(), $arrintDefaultArCodeIds )
						&& false == in_array( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getDefaultArCodeId(), $arrintDefaultArCodeIds ) ) ) ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_ar_code', __( 'Utility reimbursements can only be allocated to subsidy refund payable transactions.' ) ) );
					return false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valPostDate( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( false == valStr( $this->m_objArAllocation->getPostDate() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date is required.' ) ) );
		} elseif( false == CValidation::checkISODateFormat( $this->m_objArAllocation->getPostDate(), true ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date is not valid.' ) ) );
			return false;
		}

		$strMaxPostDate = NULL;
		$arrintTransactionIds = [];

		if( true == valStr( $this->m_objArAllocation->getChargeArTransactionId() ) ) {
			$arrintTransactionIds = [ $this->m_objArAllocation->getChargeArTransactionId() ];
		}

		if( true == valStr( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			array_push( $arrintTransactionIds, $this->m_objArAllocation->getCreditArTransactionId() );
		}

		$arrobjArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByIdsByCid( $arrintTransactionIds, $this->m_objArAllocation->getCid(), $objDatabase );

		if( true == valArr( $arrobjArTransactions ) ) {
			if( true == isset( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()] ) && true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' ) && true == isset( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()] ) && true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) ) {
				$strMaxPostDate = ( strtotime( date( 'm/d/Y', strtotime( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPostDate() ) ) ) > strtotime( date( 'm/d/Y', strtotime( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPostDate() ) ) ) ) ? $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPostDate() : $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPostDate();
			} elseif( true == isset( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()] ) && true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' ) && ( false == isset( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()] ) || false == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) ) ) {
				$strMaxPostDate = $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPostDate();
			} elseif( true == isset( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()] ) && true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) && ( false == isset( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()] ) || false == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' ) ) ) {
				$strMaxPostDate = $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPostDate();
			}

			if( strtotime( $this->m_objArAllocation->getPostDate() ) < strtotime( date( 'm/d/Y', strtotime( $strMaxPostDate ) ) ) ) {
				$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date must be greater than or equal to {%t, 0, DATE_NUMERIC_STANDARD}.', [ $strMaxPostDate ] ) ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valPostMonth( $objDatabase = NULL, $objPropertyGlSetting ) {
		$boolIsValid = true;

		$strPostMonth = date( 'm/1/Y', strtotime( $this->m_objArAllocation->getPostMonth() ) );

 		if( false == valStr( $this->m_objArAllocation->getPostMonth() ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
			return false;
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_objArAllocation->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month must be in mm/yyyy format.' ) ) );
			return false;
		}

		$arrintTransactionIds = [];

		if( true == valStr( $this->m_objArAllocation->getChargeArTransactionId() ) ) {
			$arrintTransactionIds = [ $this->m_objArAllocation->getChargeArTransactionId() ];
		}

		if( true == valStr( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			array_push( $arrintTransactionIds, $this->m_objArAllocation->getCreditArTransactionId() );
		}

		$arrobjArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByIdsByCid( $arrintTransactionIds, $this->m_objArAllocation->getCid(), $objDatabase );

		if( true == valArr( $arrobjArTransactions ) ) {

			$strMaxPostMonth = NULL;

			if( true == isset( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()] )
 				&& true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' )
 				&& true == isset( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()] )
 				&& true == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) ) {

				$strMaxPostMonth = ( strtotime( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPostMonth() ) > strtotime( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPostMonth() ) ) ? $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPostMonth() : $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPostMonth();
			}

			if( strtotime( $strPostMonth ) < strtotime( $strMaxPostMonth ) ) {
				$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month must be greater than or equal to {%t, 0, DATE_NUMERIC_POSTMONTH}.', [ $strMaxPostMonth ] ) ) );
				return false;
			}
		}

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( __( 'Failed to load property gl settings.' ), E_USER_ERROR );
		}

		if ( false == $objPropertyGlSetting->getActivateStandardPosting() ) return true;

		if( ( CPropertyGlSetting::VALIDATION_MODE_OPEN_PERIODS == $objPropertyGlSetting->getUserValidationMode() || CPropertyGlSetting::VALIDATION_MODE_STRICT == $objPropertyGlSetting->getUserValidationMode() ) && true == valStr( $objPropertyGlSetting->getArLockMonth() ) && strtotime( $this->m_objArAllocation->getPostMonth() ) <= strtotime( $objPropertyGlSetting->getArLockMonth() ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Post month selected is in a locked period.' ) ) );
			return false;
		}

		if( false == $this->m_objArAllocation->getValidatePastOrFuturePostMonth() || true == $this->m_objArAllocation->getOverridePostMonthPermission() ) return $boolIsValid;

		$intPastPostMonth 	= strtotime( $objPropertyGlSetting->getArPostMonth() );
		$intFuturePostMonth = $intPastPostMonth;

		if( false == is_null( $this->m_objArAllocation->getPastPostMonth() ) && false == is_null( $this->m_objArAllocation->getFuturePostMonth() ) ) {
			$intPastPostMonth	= strtotime( date( 'm/d/Y', $intPastPostMonth ) . ' - ' . $this->m_objArAllocation->getPastPostMonth() . ' month' );
			$intFuturePostMonth = strtotime( date( 'm/d/Y', $intFuturePostMonth ) . ' + ' . $this->m_objArAllocation->getFuturePostMonth() . ' month' );
		}

		if( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) {
			if( $intPastPostMonth != $intFuturePostMonth ) {
				$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'post month should be between {%t, 0, DATE_NUMERIC_POSTMONTH} and {%t, 1, DATE_NUMERIC_POSTMONTH} for this allocation.', [ $intPastPostMonth, $intFuturePostMonth ] ) ) );
			} else {
				$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be {%t, 0, DATE_NUMERIC_POSTMONTH} for this allocation.', [ $intFuturePostMonth ] ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

 	public function valDeletePostDate() {

    	$boolIsValid = true;

    	if( false == valStr( $this->m_objArAllocation->getDeletePostDate() ) ) {
    		$boolIsValid = false;
    		$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( 'Delete post date is required.' ) ) );
    	} elseif( false == CValidation::checkISODateFormat( $this->m_objArAllocation->getDeletePostDate() ) && false == CValidation::validateISODate( $this->m_objArAllocation->getDeletePostDate() ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( 'Delete post date is not valid.' ) ) );
			return false;
		}

		if( strtotime( date( 'm/d/y', strtotime( $this->m_objArAllocation->getPostDate() ) ) ) > strtotime( $this->m_objArAllocation->getDeletePostDate() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', __( 'You cannot reverse an allocation having a reversal date proceeding the original post date ({%t, 0, DATE_NUMERIC_STANDARD})', [ $this->m_objArAllocation->getPostDate() ] ) ) );
		}

    	return $boolIsValid;
    }

	public function valDeletePostMonth( $objDatabase = NULL, $objPropertyGlSetting ) {

		$boolIsValid = true;

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( __( 'Failed to load property gl settings.' ), E_USER_ERROR );
		}

		if ( false == $objPropertyGlSetting->getActivateStandardPosting() ) return true;

		if( true == valStr( $objPropertyGlSetting->getArLockMonth() ) && strtotime( $this->m_objArAllocation->getDeletePostMonth() ) <= strtotime( $objPropertyGlSetting->getArLockMonth() ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Post month selected is in a locked period.' ) ) );
			return false;
		}

		if( strtotime( $this->m_objArAllocation->getDeletePostMonth() ) < strtotime( $this->m_objArAllocation->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'You cannot reverse a transaction into a post month proceeding the original transaction\'s post month ({%t, 0, DATE_NUMERIC_POSTMONTH})', [ $this->m_objArAllocation->getPostMonth() ] ) ) );
		}

		if( false == $this->m_objArAllocation->getValidatePastOrFuturePostMonth() || true == $this->m_objArAllocation->getOverridePostMonthPermission() ) return $boolIsValid;

		$strPostMonth = date( 'm/1/Y', strtotime( $this->m_objArAllocation->getDeletePostMonth() ) );

		$intPastPostMonth 	= strtotime( $objPropertyGlSetting->getArPostMonth() );
		$intFuturePostMonth = $intPastPostMonth;

		if( false == is_null( $this->m_objArAllocation->getPastPostMonth() ) && false == is_null( $this->m_objArAllocation->getFuturePostMonth() ) ) {
			$intPastPostMonth = strtotime( date( 'm/d/Y', $intPastPostMonth ) . ' - ' . $this->m_objArAllocation->getPastPostMonth() . ' month' );
			$intFuturePostMonth = strtotime( date( 'm/d/Y', $intFuturePostMonth ) . ' + ' . $this->m_objArAllocation->getFuturePostMonth() . ' month' );
		}

		if( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) {
			if( $intPastPostMonth != $intFuturePostMonth ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be between {%t, 0, DATE_NUMERIC_POSTMONTH} and {%t, 1, DATE_NUMERIC_POSTMONTH} for this allocation.' ) ) );
			} else {
				$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be {%t, 0, DATE_NUMERIC_POSTMONTH} for this allocation.' ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valIsDeleted() {

		if( false == $this->m_objArAllocation->getIsDeleted() ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'The delete flag must be set.' ) ) );
			return false;
		}

		return true;
	}

	public function valDeleteDatetime() {

		if( true == is_null( $this->m_objArAllocation->getDeleteDatetime() ) || ( false == CValidation::checkDateTime( $this->m_objArAllocation->getDeleteDatetime() ) && false == CValidation::checkDate( $this->m_objArAllocation->getDeleteDatetime() ) ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot delete an allocation without specifying a valid date of deletion.' ) ) );
			return false;
		}
		return true;
	}

	public function valArAllocationDependencies( $objDatabase, $objPropertyGlSetting = NULL ) {

		$arrintTransactionIds = [];
		if( true == valStr( $this->m_objArAllocation->getChargeArTransactionId() ) ) {
			$arrintTransactionIds = [ $this->m_objArAllocation->getChargeArTransactionId() ];
		}

		if( true == valStr( $this->m_objArAllocation->getCreditArTransactionId() ) ) {
			array_push( $arrintTransactionIds, $this->m_objArAllocation->getCreditArTransactionId() );
		}

		$arrobjArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByIdsByCid( $arrintTransactionIds, $this->m_objArAllocation->getCid(), $objDatabase );

		// Temporary code implemented to resolve system error #1779835 ( Task Id #588427 )
		if( false == valArr( $arrobjArTransactions )
			|| false == valObj( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()], 'CArTransaction' )
			|| false == valObj( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()], 'CArTransaction' ) ) {
			return false;
		}

		if( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArCodeId() != $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArCodeId() ) {
			if( 0 < $this->m_objArAllocation->getAllocationAmount() ) {

				if( CArCodeType::REFUND == $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArCodeTypeId() ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Negative refund payables cannot be allocated to anything besides a refund payable.' ) ) );
					return false;
				}

				if( CArCodeType::DEPOSIT == $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArCodeTypeId() && true == $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getIsDepositCredit() ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Positive deposit credits cannot be allocated to anything besides a deposit credit.' ) ) );
					return false;
				}
			} elseif( 0 > $this->m_objArAllocation->getAllocationAmount() && false == $this->m_objArAllocation->getIsDeleted() ) {

				if( CArCodeType::REFUND == $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArCodeTypeId() ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Negative refund payables cannot be allocated to anything besides a refund payable (reverse).' ) ) );
					return false;
				}

				if( CArCodeType::DEPOSIT == $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArCodeTypeId() && true == $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getIsDepositCredit() ) {
					$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Positive deposit credits cannot be allocated to anything besides a deposit credit (reversal).' ) ) );
					return false;
				}
			}
		}
		if( true == $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getIsTemporary() || true == $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getIsTemporary() ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot allocate a temporary transaction.' ) ) );
			return false;
		}
		if( 0 < $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getTransactionAmount() || 0 > $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getTransactionAmount() ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Associated charges must be positive, AND credits negative.' ) ) );
			return false;
		}

		if( false == $this->m_objArAllocation->getIsDeleted() && ( ( false == is_null( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArTransactionId() ) && $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getArTransactionId() != $this->m_objArAllocation->getCreditArTransactionId() )
			|| ( false == is_null( $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArTransactionId() ) && $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getArTransactionId() != $this->m_objArAllocation->getChargeArTransactionId() ) ) ) {
			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Reversed items can only be allocated to each other.' ) ) );
			return false;
		}

		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting()
			&& ( $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getPropertyId() != $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getPropertyId()
				|| $arrobjArTransactions[$this->m_objArAllocation->getChargeArTransactionId()]->getLeaseId() != $arrobjArTransactions[$this->m_objArAllocation->getCreditArTransactionId()]->getLeaseId() ) ) {

			$this->m_objArAllocation->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Charge or Credit property_id or lease_id does not match the allocation that is inserting or updating.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase, $objPropertyGlSetting ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'post_ar_allocation':
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valPostMonth( $objDatabase, $objPropertyGlSetting );
				$boolIsValid &= $this->valChargeArTransactionId();
				$boolIsValid &= $this->valCreditArTransactionId();
				$boolIsValid &= $this->valAllocationAmount();
				$boolIsValid &= $this->valAllocationIntegrity( $objDatabase );
				$boolIsValid &= $this->valPostDate( $objDatabase );
				$boolIsValid &= $this->valArAllocationDependencies( $objDatabase, $objPropertyGlSetting );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletePostMonth( $objDatabase, $objPropertyGlSetting );
				$boolIsValid &= $this->valIsDeleted();
				$boolIsValid &= $this->valDeleteDatetime();
				$boolIsValid &= $this->valDeletePostDate();
				$boolIsValid &= $this->valArAllocationDependencies( $objDatabase, $objPropertyGlSetting );
				break;

			case 'update_post_month':
				$boolIsValid &= $this->valPostMonth( $objDatabase, $objPropertyGlSetting );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>