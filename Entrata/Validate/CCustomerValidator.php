<?php

class CCustomerValidator {

	protected $m_objCustomer;
	protected $m_objLeaseCustomer;

    public function __construct() {
        return;
    }

    public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
    }

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getId() ) || 0 >= ( int ) $this->m_objCustomer->getId() ) {
			$boolIsValid = false;
			trigger_error( 'Company customer id is required', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getCid() ) || 0 >= ( int ) $this->m_objCustomer->getCid() ) {
			$boolIsValid = false;
			trigger_error( 'client id required.', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valLeadSourceId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getLeadSourceId() ) && 0 >= $this->m_objCustomer->getLeadSourceId() ) {
			$boolIsValid = false;
			trigger_error( 'Customer Lead Source Id invalid.', E_USER_ERROR );
		}

		$objProperty = $this->m_objCustomer->getProperty();

		if( false == is_null( $objDatabase ) && true == valObj( $objDatabase, 'CDatabase' ) && true == valObj( $objProperty, 'CProperty' ) ) {

			$arrobjLeadSources = \Psi\Eos\Entrata\CLeadSources::createService()->fetchCustomLeadSourcesByPropertyIdByCid( $objProperty->getId(), $objProperty->getCid(), $objDatabase );

			if( true == valArr( $arrobjLeadSources ) ) {
				if( true == is_null( $this->m_objCustomer->getLeadSourceId() ) || false == is_numeric( $this->m_objCustomer->getLeadSourceId() )
					|| false == in_array( $this->m_objCustomer->getLeadSourceId(), array_keys( $arrobjLeadSources ) ) ) {

					$boolIsValid = false;
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lead_source_id', __( '"How you heard about us?" is required.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valMaritalStatusTypeId() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getMaritalStatusTypeId() ) && 0 >= $this->m_objCustomer->getMaritalStatusTypeId() ) {
			$boolIsValid = false;
			trigger_error( 'Marital Status Id invalid.', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valLeasingAgentId() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getLeasingAgentId() ) && 0 >= $this->m_objCustomer->getLeasingAgentId() ) {
			$boolIsValid = false;
			trigger_error( 'Leasing Agent Id invalid.', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPasswordQuestion() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getPasswordQuestion() ) || 0 == strlen( $this->m_objCustomer->getPasswordQuestion() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secret_question', __( 'Secret question is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPasswordAnswer() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getPasswordAnswer() ) || 0 == strlen( $this->m_objCustomer->getPasswordAnswer() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secret_answer', __( 'Secret answer is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsIntegratedClient() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getIntegrationClientTypes() ) && 0 < \Psi\Libraries\UtilFunctions\count( $this->m_objCustomer->getIntegrationClientTypes() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Residents cannot be added manually if integration exists with an external accounting system.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameFirstAndNameLastOrCompanyName( $boolAddErrorMessage = true ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomer->getNameFirst() )
		    && false == valStr( $this->m_objCustomer->getNameLast() ) && false == valStr( $this->m_objCustomer->getCompanyName() ) ) {

		    $boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name, Last name or Company name is required.' ) ) );
			}
		} elseif( false == valStr( $this->m_objCustomer->getCompanyName() ) && false == valStr( $this->m_objCustomer->getNameFirst() ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name is required.' ) ) );
			}
		} elseif( false == valStr( $this->m_objCustomer->getCompanyName() ) && false == valStr( $this->m_objCustomer->getNameLast() ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );
			}
		} elseif( true == valStr( $this->m_objCustomer->getNameFirst() ) && true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), '@' )
				   || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getNameLast() ) && true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), '@' )
				   || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getCompanyName() ) && true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getCompanyName(), '@' )
				   || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getCompanyName(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getNameFirst() ) && 1 > strlen( $this->m_objCustomer->getNameFirst() ) ) {

			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name must have at least one letter.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getCompanyName() ) && 2 > strlen( $this->m_objCustomer->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Company name must have at least two letters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $boolAddErrorMessage = true ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getNameFirst() ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name is required.' ) ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( 1 > strlen( $this->m_objCustomer->getNameFirst() ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name must have at least one letter.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameMiddle() {

		$boolIsValid = true;

		if( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameMiddle(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameMiddle(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', __( 'Names cannot contain @ signs or email headers.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolAddErrorMessage = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getNameLast() ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );
			}
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyName( $boolIsFromGroup = false ) {
		$boolIsValid = true;

		$strDisplayName = ( $boolIsFromGroup == true ) ? 'Group' :  'Company';
		if( false == valStr( $this->m_objCustomer->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( $strDisplayName . ' name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valBusinessName() {
		$boolIsValid = true;

		// This condition protects against email bots that are using forms to send spam.
		if( true == valStr( $this->m_objCustomer->getNameFirst() ) && ( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameFirst(), 'Content-type' ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getNameMiddle() ) && ( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameMiddle(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameMiddle(), 'Content-type' ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getNameLast() ) && ( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getNameLast(), 'Content-type' ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );
		} elseif( true == valStr( $this->m_objCustomer->getCompanyName() ) && ( true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getCompanyName(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objCustomer->getCompanyName(), 'Content-type' ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Names cannot contain @ signs or email headers.' ) ) );
		}

		if( false == valStr( $this->m_objCustomer->getCompanyName() ) ) {
			$boolIsValid &= false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Business name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumbers( $boolIsValidateForExtension = false, $boolIsPrimaryPhoneReq = false, $boolSkipValidateSecondaryNumber = false ) {
		$boolIsValid  = $this->valPrimaryPhoneNumber( $boolIsPrimaryPhoneReq, $boolIsValidateForExtension );

		if( false == $boolSkipValidateSecondaryNumber ) {
			$boolIsValid &= $this->valSecondaryPhoneNumber();
		}

		return $boolIsValid;
	}

	public function valI18nPrimaryPhoneNumber( $boolIsRequired = false, $strFieldName = 'primary_phone_number' ) :bool {
		$boolIsValid = true;
		$objI18nPrimaryPhoneNumber = $this->m_objCustomer->getI18nPrimaryPhoneNumber();
		if( true == valObj( $objI18nPrimaryPhoneNumber, 'i18n\CPhoneNumber' ) && false == $objI18nPrimaryPhoneNumber->isValid( $boolIsRequired ) ) {
			$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, $strFieldName, __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPrimaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPrimaryPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPrimaryPhoneNumber->getFormattedErrors() ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPrimaryPhoneNumber( $boolIsReqd = false, $boolIsValidateForExtension = false, $strFieldName = NULL ) {

		$objI18nPrimaryPhoneNumber = $this->m_objCustomer->getI18nPrimaryPhoneNumber();
		if( true == valObj( $objI18nPrimaryPhoneNumber, 'i18n\CPhoneNumber' ) ) {

			$strFieldName = ( true == is_null( $strFieldName ) ) ? 'primary_phone_number' : $strFieldName;

			if( false == $objI18nPrimaryPhoneNumber->isValid( $boolIsReqd ) ) {
				$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, $strFieldName, __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPrimaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPrimaryPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPrimaryPhoneNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {

			$strPrimaryPhoneNumber 		= $this->m_objCustomer->getPrimaryPhoneNumber();
			$intPrimaryNumberLength		= strlen( $strPrimaryPhoneNumber );

			if( true == is_null( $strPrimaryPhoneNumber ) ) {
				if( false == $boolIsReqd ) {
					return true;
				}
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number is required.' ) ) );
				return false;
			}

			if( true == is_null( $this->m_objCustomer->getPrimaryPhoneNumberTypeId() ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number type is required.' ) ) );
				return false;
			}

			if( true == $boolIsValidateForExtension && 0 < $intPrimaryNumberLength && 10 > $intPrimaryNumberLength ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number must be 10 characters.' ), '' ) );
				return false;
			}

			if( 0 < $intPrimaryNumberLength && ( 10 > $intPrimaryNumberLength || 15 < $intPrimaryNumberLength ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number must be between 10 and 15 characters.' ), '' ) );
				return false;
			}

		if( 1 != $this->checkPhoneNumberFullValidation( $strPrimaryPhoneNumber ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Enter valid primary phone number.' ), 504 ) );
				return false;
		} elseif( true == isset( $strPrimaryPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $strPrimaryPhoneNumber, false ) ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Valid primary phone number is required.' ), 504 ) );
				return false;
		}
		}

		return true;
	}

	public function valSecondaryPhoneNumber( $boolIsReqd = false ) {

		$objI18nSecondaryPhoneNumber = $this->m_objCustomer->getI18nSecondaryPhoneNumber();
		if( true == valObj( $objI18nSecondaryPhoneNumber, 'i18n\CPhoneNumber' ) ) {

			if( false == $objI18nSecondaryPhoneNumber->isValid( $boolIsReqd ) ) {
				$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nSecondaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nSecondaryPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nSecondaryPhoneNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$strPrimaryPhoneNumber 		= $this->m_objCustomer->getPrimaryPhoneNumber();
			$strSecondaryPhoneNumber 	= $this->m_objCustomer->getSecondaryPhoneNumber();
			$intSecondaryNumberLength	= strlen( $strSecondaryPhoneNumber );

			if( true == is_null( $strSecondaryPhoneNumber ) ) {
				if( false == $boolIsReqd ) {
					return true;
				}
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number is required.' ) ) );
				return false;
			}

			if( true == is_null( $this->m_objCustomer->getSecondaryPhoneNumberTypeId() ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number type is required.' ) ) );
				return false;
			}

			if( 0 < $intSecondaryNumberLength && ( 10 > $intSecondaryNumberLength || 15 < $intSecondaryNumberLength ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number must be between 10 and 15 characters.' ), '' ) );
				return false;
			}

			if( false == is_null( $strSecondaryPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $strSecondaryPhoneNumber, false ) ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Valid secondary phone number is required.' ), 504 ) );
				return false;
			}
		}

		return true;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolCanBeNull = false ) {

		$boolIsValid = true;
		$objI18nPhoneNumber = $this->m_objCustomer->getI18nPhoneNumber();
		if( true == valObj( $objI18nPhoneNumber, 'i18n\CPhoneNumber' ) ) {
			if( false == valStr( $objI18nPhoneNumber->getNumber() ) ) {
				$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Primary phone number is required.' ) ) );
				$boolIsValid = false;
			}
			if( true == valStr( $objI18nPhoneNumber->getNumber() ) && false == $objI18nPhoneNumber->isValid() ) {
				$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPhoneNumber->getFormattedErrors() ] ) ) );
				$boolIsValid = false;
			}
		} else {
			if( true == $boolCanBeNull ) {
				if( false == is_null( $this->m_objCustomer->getPhoneNumber() ) && 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getPhoneNumber() ) ) {
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Enter valid primary phone number.' ), 504 ) );
					$boolIsValid = false;
				}
			} else {
				if( true == is_null( $this->m_objCustomer->getPhoneNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->m_objCustomer->getPhoneNumber() ) ) ) {
					$boolIsValid = false;
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Primary phone number is required.' ) ) );
				} elseif( 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getPhoneNumber() ) ) {
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Enter valid primary phone number.' ), 504 ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valMobileNumber( $boolCanBeNull = false ) {

		$boolIsValid = true;

		$objI18nMobileNumber = $this->m_objCustomer->getI18nMobileNumber();
		if( true == valObj( $objI18nMobileNumber, 'i18n\CPhoneNumber' ) ) {
			if( false == $objI18nMobileNumber->isValid( $boolCanBeNull ) ) {
				$this->m_objCustomer->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nMobileNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nMobileNumber->getCountryCode(), 'formatted_errors' => $objI18nMobileNumber->getFormattedErrors() ] ) ) );
				$boolIsValid = false;
			}
		} else {
			if( true == $boolCanBeNull ) {
				if( false == is_null( $this->m_objCustomer->getMobileNumber() ) && 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getMobileNumber() ) ) {
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Enter valid mobile number.' ), 504 ) );
					$boolIsValid = false;
				}
			} else {

				if( true == is_null( $this->m_objCustomer->getMobileNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->m_objCustomer->getMobileNumber() ) ) ) {
					$boolIsValid = false;
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Enter valid mobile number.' ) ) );
				} elseif( 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getMobileNumber() ) ) {
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Enter valid mobile number.' ), 504 ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valWorkNumber( $boolCanBeNull = false ) {

		$boolIsValid = true;

		if( true == $boolCanBeNull ) {
			if( false == is_null( $this->m_objCustomer->getWorkNumber() ) && 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getWorkNumber() ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_number', __( 'Enter valid secondary phone number.' ), 504 ) );
				$boolIsValid = false;
			}
		} else {
			if( true == is_null( $this->m_objCustomer->getWorkNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->m_objCustomer->getWorkNumber() ) ) ) {
	            $boolIsValid = false;
	            $this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_number', __( 'Enter valid secondary phone number.' ) ) );
	        } elseif( 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getWorkNumber() ) ) {
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_number', __( 'Enter valid secondary phone number.' ), 504 ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valEmployersPhoneNumber() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getEmployersPhoneNumber() ) && 1 != $this->checkPhoneNumberFullValidation( $this->m_objCustomer->getEmployersPhoneNumber() ) ) {
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employers_phone_number', __( 'Enter valid company phone number.' ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEmploymentGrossSalary() {

		$boolIsValid = true;

		if( 0 > ( float ) $this->m_objCustomer->getEmploymentGrossSalary() ) {
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_gross_salary', __( 'Salary must be greater than zero.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPrimaryStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getPrimaryStreetLine1() ) || 0 == strlen( $this->m_objCustomer->getPrimaryStreetLine1() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_street_line1', __( 'Address is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryCity() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getPrimaryCity() ) || 0 == strlen( $this->m_objCustomer->getPrimaryCity() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_city', __( 'City is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomer->getPrimaryStateCode() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_state_code', __( 'State is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryPostalCode( $boolCanBeNull = false, $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
	    	if( true == is_null( $this->m_objCustomer->getPrimaryPostalCode() ) || 0 == strlen( $this->m_objCustomer->getPrimaryPostalCode() ) ) {
	    		$boolIsValid = false;
	    		$this->m_objCustomer->addErrorMsg( new CErrorMsg( NULL, 'primary_postal_code', __( 'Zip code is required.' ) ) );
	    	}
		}
    	if( true == $boolIsValid && false == is_null( $this->m_objCustomer->getPrimaryPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCustomer->getPrimaryPostalCode(), $strCountryCode ) ) {
    		$boolIsValid = false;
	    	$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_postal_code',  __( 'Enter valid zip code.' ) ) );
    	}

    	return $boolIsValid;
	}

	public function valTaxNumber( $strAction, $intOccupancyTypeId = NULL, $objDatabase = NULL, $boolIsValidateForSSN = false ) {

		$boolIsValid 			= true;
		$objTaxNumberValidator 	= \Psi\Eos\Entrata\Validate\TaxNumber\CTaxNumberValidator::createService();

		if( COccupancyType::COMMERCIAL == $intOccupancyTypeId ) {
			$boolIsValid = $objTaxNumberValidator->validate( $strAction, $this->m_objCustomer->getTaxNumber() );

		} elseif( true == valId( $this->m_objCustomer->getTaxIdTypeId() ) ) {

			$objTaxIdType = $this->loadTaxIdType( $this->m_objCustomer->getTaxIdTypeId(), $objDatabase );
			if( true == valobj( $objTaxIdType, 'CTaxIdType' ) ) {
				$boolIsValid = $objTaxNumberValidator->validate( $strAction, $this->m_objCustomer->getTaxNumber(), $objTaxIdType );
			}
		} elseif( true == $boolIsValidateForSSN && COccupancyType::AFFORDABLE == $intOccupancyTypeId ) {
			$boolIsValid = $objTaxNumberValidator->validate( 'subsidized_customer_secure_info', $this->m_objCustomer->getTaxNumber() );
		}

		if( false == $boolIsValid ) {
			$this->m_objCustomer->addErrorMsgs( $objTaxNumberValidator->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	private function loadTaxIdType( $intTaxIdTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CTaxIdTypes::createService()->fetchTaxIdTypeById( $intTaxIdTypeId, $objDatabase );
	}

	public function valBirthDate( $boolBirthDateIsRequired = false ) {

		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomer->getBirthDate() ) ) {
			$mixValidatedBirthDate = CValidation::checkISOBirthDateFormat( $this->m_objCustomer->getBirthDate(), $boolFormat = true );

			if( false === $mixValidatedBirthDate ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', __( 'Birth date is not valid' ) ) );
			} else {
				$this->m_objCustomer->setBirthDate( date( 'm/d/Y', strtotime( $mixValidatedBirthDate ) ) );
			}
		} elseif( true == $boolBirthDateIsRequired ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', __( 'Birth date is required.' ) ) );
		}

		if( true == $boolIsValid && true == valStr( $this->m_objCustomer->getBirthDate() ) && strtotime( $this->m_objCustomer->getBirthDate() ) > strtotime( date( 'm/d/Y' ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', __( 'Birth date cannot be in the future.' ) ) );
		}

		if( true == $boolIsValid && false == $this->m_objCustomer->getIsOverrideAndAllowUnderAgeOccupant() && true == valObj( $this->m_objLeaseCustomer, 'CLeaseCustomer' ) ) {

			$arrintCustomerTypesAllowedToOverrideUnderAge = [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::GUARANTOR ];

			if( false == is_null( $this->m_objCustomer->getBirthDate() ) && true == in_array( $this->m_objLeaseCustomer->getCustomerTypeId(), $arrintCustomerTypesAllowedToOverrideUnderAge ) ) {

				$intCustomerAge = 0;
				$arrstrExplodedCustomerBirthDate = explode( '/', $this->m_objCustomer->getBirthDate() );

				if( true == valArr( $arrstrExplodedCustomerBirthDate ) && 3 == \Psi\Libraries\UtilFunctions\count( $arrstrExplodedCustomerBirthDate ) ) {
					$intCustomerAge = ( date( 'md', date( 'U', mktime( 0, 0, 0, $arrstrExplodedCustomerBirthDate[0], $arrstrExplodedCustomerBirthDate[1], $arrstrExplodedCustomerBirthDate[2] ) ) ) > date( 'md' ) ? ( ( date( 'Y' ) - $arrstrExplodedCustomerBirthDate[2] ) - 1 ) : ( date( 'Y' ) - $arrstrExplodedCustomerBirthDate[2] ) );
				}

				if( CCustomer::MINIMUM_OCCUPANT_AGE_ALLOWED_TO_BE_RESPONSIBLE_ON_LEASE > $intCustomerAge && false == $this->m_objCustomer->getIsOverrideAndAllowUnderAgeOccupant() ) {
					$boolIsValid = false;
					$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date_under_age', __( 'Occupant must be 18 years or older to be legally responsible for the lease.' ) ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valCustomerUnderAge() {

		$boolIsValid = true;
		$arrintCustomerTypesAllowedToOverrideUnderAge = [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::GUARANTOR ];

		if( false == is_null( $this->m_objCustomer->getBirthDate() ) && true == in_array( $this->m_objLeaseCustomer->getCustomerTypeId(), $arrintCustomerTypesAllowedToOverrideUnderAge ) ) {

			$arrstrExplodedCustomerBirthDate = explode( '/', $this->m_objCustomer->getBirthDate() );

			if( true == valArr( $arrstrExplodedCustomerBirthDate ) && 3 == \Psi\Libraries\UtilFunctions\count( $arrstrExplodedCustomerBirthDate ) ) {
				$intCustomerAge = ( date( 'md', date( 'U', mktime( 0, 0, 0, $arrstrExplodedCustomerBirthDate[0], $arrstrExplodedCustomerBirthDate[1], $arrstrExplodedCustomerBirthDate[2] ) ) ) > date( 'md' ) ? ( ( date( 'Y' ) - $arrstrExplodedCustomerBirthDate[2] ) - 1 ) : ( date( 'Y' ) - $arrstrExplodedCustomerBirthDate[2] ) );

				if( CCustomerType::GUARANTOR != $this->m_objLeaseCustomer->getCustomerTypeId() && CCustomer::MINIMUM_OCCUPANT_AGE_REQUIRED_TO_OVERRIDE_MINIMUM_OCCUPANT_AGE_LIMIT > $intCustomerAge ) {
						$boolIsValid &= false;
						$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_under_age', __( 'Occupant must be 14 years or older (in order to override).' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valEmploymentDateStarted() {

		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomer->getEmploymentDateStarted() ) ) {
			$mixValidatedEmploymentDateStarted = CValidation::checkISODateFormat( $this->m_objCustomer->getEmploymentDateStarted(), $boolFormat = true );
			if( false === $mixValidatedEmploymentDateStarted ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_started', __( 'Start date is not valid.' ) ) );
			} else {
				$this->m_objCustomer->setEmploymentDateStarted( date( 'm/d/Y', strtotime( $mixValidatedEmploymentDateStarted ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valEmploymentDateTerminated() {

		$boolIsValid = true;
		$boolIsValid &= $this->valEmploymentDateStarted();

		if( true == valStr( $this->m_objCustomer->getEmploymentDateTerminated() ) ) {
			$mixValidatedEmploymentDateTerminated = CValidation::checkISODateFormat( $this->m_objCustomer->getEmploymentDateTerminated(), $boolFormat = true );
			if( false === $mixValidatedEmploymentDateTerminated ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_terminated', __( 'End date is not valid' ) ) );
			} else {
				$this->m_objCustomer->setEmploymentDateTerminated( date( 'm/d/Y', strtotime( $mixValidatedEmploymentDateTerminated ) ) );
			}

			if( true == valStr( $this->m_objCustomer->getEmploymentDateStarted() ) && true == $boolIsValid
				&& ( strtotime( $this->m_objCustomer->getEmploymentDateStarted() ) > strtotime( $this->m_objCustomer->getEmploymentDateTerminated() ) ) ) {

				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_date_terminated', __( 'End date must be greater than or equal to start date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valUsername( $objDatabase, $boolUsernameRequired = false, $boolCheckPropertyIsEnabled = false, $boolIsFromSelfAccountCreation = false ) {

		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getUsername() ) && 0 < strlen( $this->m_objCustomer->getUsername() ) || true == $boolUsernameRequired ) {

			if( true == is_null( $this->m_objCustomer->getUsername() ) || 0 == strlen( trim( $this->m_objCustomer->getUsername() ) ) ) {

				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ) ) );
				return $boolIsValid;

			} elseif( false == CValidation::validateEmailAddresses( $this->m_objCustomer->getUsername() ) ) {

				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address does not appear to be valid.' ) ) );
				return $boolIsValid;
			}

			// Make sure it does not already exist in the client
			if( false == is_null( $objDatabase ) ) {

				$intPreExistingCustomerCount = CCustomerPortalSettings::fetchPreExistingCustomerCountByUsernameByCid( $this->m_objCustomer->getUsername(), $this->m_objCustomer->getCid(), $this->m_objCustomer->getId(), $objDatabase, $boolIsFromSelfAccountCreation );

				if( 0 < $intPreExistingCustomerCount ) {

					if( true == $boolCheckPropertyIsEnabled ) {
						$intCustomCustomerPortalSettingCount = CCustomerPortalSettings::fetchCustomCustomerPortalSettingCountByUsernameByCid( $this->m_objCustomer->getUsername(), $this->m_objCustomer->getCid(), $objDatabase );
						if( 0 < $intCustomCustomerPortalSettingCount ) {
							$boolIsValid = false;
							$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is already being used.' ), 319 ) );
						}
					} else {
						$boolIsValid = false;
						$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is already being used.' ), 319 ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( false != is_null( $this->m_objCustomer->getPassword() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password is required.' ) ) );
		} else {
			if( 6 > strlen( $this->m_objCustomer->getPassword() ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password must be at least 6 characters long.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valContactInfo() {

		$boolIsValid 			= true;
		$boolUsernameIsValid 	= true;
		$boolPhoneNumberIsValid = true;

		if( true == is_null( $this->m_objCustomer->getUsername() ) || 0 == strlen( trim( $this->m_objCustomer->getUsername() ) ) ) {
			$boolUsernameIsValid = false;
		} elseif( false == CValidation::validateEmailAddresses( $this->m_objCustomer->getUsername() ) ) {
			$boolUsernameIsValid = false;
		}

		if( true == is_null( $this->m_objCustomer->getPhoneNumber() ) || 1 != CValidation::checkPhoneNumber( $this->m_objCustomer->getPhoneNumber() ) ) {
			$boolPhoneNumberIsValid = false;
		}

		if( false == $boolUsernameIsValid && false == $boolPhoneNumberIsValid ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'An email address is required if a phone number is not entered.' ) ) );
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A phone number is required if an email address is not entered.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolCanBeNull = false ) {

		$boolIsValid = true;

		if( false == $boolCanBeNull ) {

			if( false == is_null( $this->m_objCustomer->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objCustomer->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter a valid email address.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getPrimaryPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCustomer->getPrimaryPostalCode() ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Please enter valid postal code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTotalNumberOfVehicles( $objDatabase ) {

		$boolIsValid = true;

		$intCount = CLeaseCustomerVehicles::fetchLeaseCustomerVehicleCount( 'WHERE customer_id=' . $this->m_objCustomer->getId() . 'AND  cid = ' . $this->m_objCustomer->getCid(), $objDatabase );
		if( CCustomer::TOTAL_NUMBER_OF_VEHICLES_ALLOWED <= $intCount ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Customer can only add upto 4 vehicles' ) ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted( $strSectionName = NULL, $boolRequired = false, $arrobjPropertyApplicationPreferences, $objDatabase, $boolReformatTaxNumber = true ) {

		$boolIsValid = true;
		$intTaxIdTypeId = CTaxIdType::SSN;
		if( true == isset( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_TYPE'] ) ) {
			$intTaxIdTypeId = $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_TYPE']->getValue() ?? CTaxIdType::SSN;
			$objTaxIdType = CTaxIdTypes::fetchTaxIdTypeById( $intTaxIdTypeId, $objDatabase );
		} else {
			return $boolIsValid;
		}

		$strTaxIdTypeAbbreviation = $objTaxIdType->getAbbreviation() ?? CTaxIdType::SSN;
		if( CTaxIdType::SSN != $intTaxIdTypeId || 0 == strlen( $strSectionName ) ) {
			$strSectionName = $strTaxIdTypeAbbreviation;
		}

		$intTaxNumberLength = \Psi\CStringService::singleton()->strlen( $this->m_objCustomer->getTaxNumber( $boolReformatTaxNumber ) );
		if( 0 == $intTaxNumberLength ) {
			if( true == $boolRequired ) {
				$boolIsValid &= false;
				$strErrorMessage = __( '{%s,0} is required.', [ $strSectionName ] );
			}

		} else {
			if( CTaxIdType::SSN == $intTaxIdTypeId ) {
				if( ( 9 != $intTaxNumberLength ) || ( false == is_numeric( $this->m_objCustomer->getTaxNumber() ) ) || ( 0 == ( int ) $this->m_objCustomer->getTaxNumber() ) ) {
					$boolIsValid &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			} else {
				if( ( 15 < $intTaxNumberLength )
				    || ( 0 >= $intTaxNumberLength )
				    || ( 0 != \Psi\CStringService::singleton()->preg_match( '/[^a-z0-9\/\x5c-]/', $this->m_objCustomer->getTaxNumber( $boolReformatTaxNumber ) ) ) ) {
					$boolIsValid &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', $strErrorMessage . $this->m_objCustomer->getTaxNumber( $boolReformatTaxNumber ), '' ) );
		}
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function checkPhoneNumberFullValidation( $strNumber ) {
		if( true == preg_match( '/\-{3,}/', $strNumber ) || 1 !== preg_match( '/^[\(]{0,1}(\d{1,3})[\)]?[\-)]?[\s]{0,}(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?[\s]?(\d*)$/', $strNumber ) ) {
			return __( 'Phone number not in valid format.' );
		}

		return 1;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valFaxNumber() {

		$boolIsValid = true;
		$intLength 	 = strlen( $this->m_objCustomer->getFaxNumber() );

		if( true == valStr( $this->m_objCustomer->getFaxNumber() ) ) {
			if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Fax number must be between 10 and 15 characters.' ), 504 ) );
				$boolIsValid = false;

			} elseif( false == ( CValidation::validateFullPhoneNumber( $this->m_objCustomer->getFaxNumber(), false ) ) ) {

				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Valid fax number is required.' ), 504 ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function valPreviousPostalCode( $strCountryCode = NULL ) {

		$boolIsValid = true;

		if( true == $boolIsValid && false == is_null( $this->m_objCustomer->getPreviousPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCustomer->getPreviousPostalCode(), $strCountryCode ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code',  __( 'Enter valid previous zip code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPermanentPostalCode( $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsValid && false == is_null( $this->m_objCustomer->getPermanentPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCustomer->getPermanentPostalCode(), $strCountryCode ) ) {
	    	$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permanent_postal_code',  __( 'Enter valid permanent zip code.' ) ) );
	    }

		return $boolIsValid;
	}

	public function valIdentificationExpiration() {

		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomer->getIdentificationExpiration() ) ) {
			$mixValidatedIdentificationExpiration = CValidation::checkISODateFormat( $this->m_objCustomer->getIdentificationExpiration(), $boolFormat = true );
			if( false === $mixValidatedIdentificationExpiration ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_expiration', __( 'Expiration date is not valid.' ) ) );
			} else {
				$this->m_objCustomer->setIdentificationExpiration( date( 'm/d/Y', strtotime( $mixValidatedIdentificationExpiration ) ) );
			}

			if( true == $boolIsValid && true == valStr( $this->m_objCustomer->getBirthDate() ) && strtotime( $this->m_objCustomer->getIdentificationExpiration() ) <= strtotime( $this->m_objCustomer->getBirthDate() ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_expiration', __( 'Expiration date must be greater than birth date.' ) ) );
			}
		}

		return $boolIsValid;

	}

	public function valStudentIdNumber() {
		$boolIsValid = true;

		if( true == $boolIsValid && 15 < strlen( $this->m_objCustomer->getStudentIdNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_id', __( 'Student Id is not valid.' ), '' ) );
		}

		if( true == $boolIsValid && 0 < strlen( $this->m_objCustomer->getStudentIdNumber() ) && false == preg_match( '/^([-a-z0-9])+$/i', $this->m_objCustomer->getStudentIdNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_id', __( 'Student Id is not valid.' ), '' ) );
		}

		return $boolIsValid;
	}

	public function valEmployersPostalCode( $strCountryCode ) {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomer->getEmployersPostalCode() ) ) {
			if( true == $boolIsValid && false == is_null( $this->m_objCustomer->getEmployersPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objCustomer->getEmployersPostalCode(), $strCountryCode ) ) {
				$boolIsValid = false;
				$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employers_postal_code',  __( 'Enter valid company zip code.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valEmailAddressWithPassword() {
		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomer->getPassword() ) && false == valStr( $this->m_objCustomer->getUsername() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddressRequired() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomer->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ) ) );
		}
		return $boolIsValid;
	}

	Public function valDuplicateCustomer( $objDatabase ) {
		$boolIsValid = true;
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByCompanyNameByAddressByCid( $this->m_objCustomer, $objDatabase );
		if( true == valObj( $objCustomer, 'Ccustomer' ) ) {
			$boolIsValid = false;
			$this->m_objCustomer->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, ' ', __( 'Same company already exist.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $objLeaseCustomer = NULL, $objProperty = NULL, $intOldCustomerTypeId = NULL, $boolIsValidateBirthDate = true, $boolIsValidateForExtension = false, $arrobjPropertyPreferences = NULL, $objLease = NULL, $boolIsValidateForSSN = true, $boolIsFromGroup = false, $boolIsEmailRequired = false, $strPhoneNumberType = NULL, $boolSkipValidateSecondaryNumber = false ) {
		$boolIsValid = true;
		$this->m_objLeaseCustomer = $objLeaseCustomer;

		switch( $strAction ) {

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				break;

			case 'VALIDATE_TAX_NUMBER':
				$boolIsValid	= $this->valId();
				$strSectionName	= CTaxIdTypes::fetchTaxIdTypeAbbreviationById( $this->m_objCustomer->getTaxIdTypeId(), $objDatabase );
				if( CTaxIdType::SSN !== $this->m_objCustomer->getTaxIdTypeId() ) {
					$boolReformatTaxNumber = false;
				}

				$boolIsValid &= $this->valTaxNumberEncrypted( $strSectionName, false, $arrobjPropertyPreferences, $objDatabase, $boolReformatTaxNumber );
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeadSourceId();
				$boolIsValid &= $this->valMaritalStatusTypeId();
				$boolIsValid &= $this->valLeasingAgentId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valBirthDate();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'pre_insert_is_integrated':
				$boolIsValid &= $this->valIsIntegratedClient();
				break;

			case 'pre_insert':
				$boolIsValid &= $this->valIsIntegratedClient();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'application_insert':
			case 'application_update':
				// $boolIsValid &= $this->valNameFirst( $boolAddErrorMessage = true );
				// $boolIsValid &= $this->valNameLast( $boolAddErrorMessage = true );
				// $boolIsValid &= $this->valLeadSourceId( $objDatabase );
				// $boolIsValid &= $this->valBirthDate( true );
				// $boolIsValid &= $this->valTaxNumber();
				// $boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'guest_card_insert':
			case 'guest_card_update':
				// $boolIsValid &= $this->valNameFirst();
				// $boolIsValid &= $this->valNameLast();
				//$boolIsValid &= $this->valContactInfo(); // Validates either email or phone number
				// $boolIsValid &= $this->valBirthDate();
				// $boolIsValid &= $this->valLeadSourceId( $objDatabase );
				break;

			case 'self_enrollment_insert':
				// self enrollment is where the resident can self create themselves in entrata.
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
			//	$boolIsValid &= $this->valWorkNumber( true );
			//	$boolIsValid &= $this->valMobileNumber( true );
				$boolIsValid &= $this->valPrimaryStreetLine1();
				// $boolIsValid &= $this->valPrimaryStreetLine2();
				// $boolIsValid &= $this->valPrimaryStreetLine3();
				$boolIsValid &= $this->valPrimaryCity();
				$boolIsValid &= $this->valPrimaryStateCode();
				$boolIsValid &= $this->valPrimaryPostalCode();
				// $boolIsValid &= $this->valPasswordQuestion();
				// $boolIsValid &= $this->valPasswordAnswer();
				$boolIsValid &= $this->valUsername( $objDatabase, true );
				break;

			case 'self_enrollment_insert_rp20':
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_COMPANY_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valCompanyName();
				} else {
					$boolIsValid &= $this->valNameFirst();
					$boolIsValid &= $this->valNameLast();
				}

				$boolIsValid &= $this->valUsername( $objDatabase, true, true, true );
				break;

			case 'enrollment_update':
				// enrollment is where the resident is creating their passwords for the first time.  Their data should already be correct.
				// $boolIsValid &= $this->valPasswordQuestion();
				// $boolIsValid &= $this->valPasswordAnswer();
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'enrollment_update_rpc':
				// enrollment is where the resident is creating their passwords for the first time.  Their data should already be correct.
				// $boolIsValid &= $this->valPasswordQuestion();
				// $boolIsValid &= $this->valPasswordAnswer();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valPassword();
				break;

			case 'password_update':
				$boolIsValid &= $this->valPassword();
				break;

			case 'customer_profile_update':
				$boolIsValid &= $this->valUsername( $objDatabase, true );
				$boolIsValid &= $this->valPhoneNumber( true );
			//	$boolIsValid &= $this->valWorkNumber( true );
			//	$boolIsValid &= $this->valMobileNumber( true );
				// $boolIsValid &= $this->valVehicleYear( true );

				// $boolIsValid &= $this->valPrimaryStreetLine1();
				// $boolIsValid &= $this->valPrimaryCity();
				// $boolIsValid &= $this->valPrimaryStateCode();
				// $boolIsValid &= $this->valPrimaryPostalCode();
				// $boolIsValid &= $this->valEmailAddress();
				break;

            case 'reset_password':
                $boolIsValid &= $this->valUsername( $objDatabase, true );
				break;

			case 'account_information':
				$boolIsValid &= $this->valUsername( $objDatabase, true );
				break;

			case 'contact_information':
				$boolIsValid &= $this->valPhoneNumber( true );
				$boolIsValid &= $this->valWorkNumber( true );
			// 	$boolIsValid &= $this->valMobileNumber( true );
				break;

			case 'address_information':
				$boolIsValid &= $this->valPrimaryStreetLine1();
				$boolIsValid &= $this->valPrimaryCity();
				$boolIsValid &= $this->valPrimaryStateCode();
				$boolIsValid &= $this->valPrimaryPostalCode();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'resident_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valNameFirstAndNameLastOrCompanyName( true );
				// $boolIsValid &= $this->valNameFirst();
				// $boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valBirthDate();
				// $boolIsValid &= $this->valEmploymentDateStarted();
				$boolIsValid &= $this->valEmploymentDateTerminated();
				break;

			case 'mobile_information':
				// $boolIsValid &= $this->valMobileNumber( false );
				break;

			case 'vehicle_insert':
				$boolIsValid &= $this->valTotalNumberOfVehicles( $objDatabase );
				break;

			// new resident view validations
			case 'customer_basic_info':
				$boolIsValid &= $this->valId();

				if( false == valStr( $this->m_objCustomer->getCompanyName() ) & COccupancyType::COMMERCIAL != $objLease->getOccupancyTypeId() ) {
					$boolIsValid &= $this->valNameFirst();
					$boolIsValid &= $this->valNameLast();
					$boolIsValid &= $this->valNameMiddle();
				}
				if( COccupancyType::COMMERCIAL == $objLease->getOccupancyTypeId() ) {
					$boolIsValid &= $this->valCompanyName();
				}

				if( ( $intOldCustomerTypeId != $this->m_objCustomer->getCustomerTypeId() ) && ( $this->m_objCustomer->getCustomerTypeId() == CCustomerType::RESPONSIBLE || $this->m_objCustomer->getCustomerTypeId() == CCustomerType::GUARANTOR ) ) {
					$boolIsValidBirthDate = $this->valBirthDate( false );
					$boolIsValid &= $boolIsValidBirthDate;
					if( true == $boolIsValidBirthDate ) {
						$boolIsValid &= $this->valCustomerUnderAge();
					}
				}

				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
				break;

			// new other income view validations
			case 'other_income_customer_basic_info':
				$boolIsValid &= $this->valId();

				if( false == valStr( $this->m_objCustomer->getCompanyName() ) ) {
					$boolIsValid &= $this->valNameFirst();
					$boolIsValid &= $this->valNameLast();
					$boolIsValid &= $this->valNameMiddle();
				}

				$boolIsValid &= $this->valUsername( $objDatabase );
				// $boolIsValid &= $this->valPhoneNumbers();
				$boolIsValid &= $this->valPrimaryPostalCode( true, $this->m_objCustomer->getPrimaryCountryCode() );
				$boolIsValid &= $this->valPermanentPostalCode( $this->m_objCustomer->getPermanentCountryCode() );
				$boolIsValid &= $this->valPreviousPostalCode( $this->m_objCustomer->getPreviousCountryCode() );
				$boolIsValid &= $this->valFaxNumber();
				break;

			case 'customer_employers_details':
				$boolIsValid &= $this->valId();
				// $boolIsValid &= $this->valEmploymentDateStarted();
				$boolIsValid &= $this->valEmploymentDateTerminated();
				$boolIsValid &= $this->valEmployersPhoneNumber();
				$boolIsValid &= $this->valEmploymentGrossSalary();
				$boolIsValid &= $this->valEmployersPostalCode( $this->m_objCustomer->getEmployersCountryCode() );
				break;

			case 'customer_secure_info':
				$boolIsValid &= $this->valId();

				if( true == $boolIsValidateBirthDate ) {
					$boolIsValidBirthDate = $this->valBirthDate( true );
					$boolIsValid 		 &= $boolIsValidBirthDate;
					if( true == $boolIsValidBirthDate ) {
						$boolIsValid &= $this->valCustomerUnderAge();
					}
				}

				$boolIsValid &= $this->valTaxNumber( $strAction, $objLease->getOccupancyTypeId(), $objDatabase, $boolIsValidateForSSN );

				$boolIsValid &= $this->valIdentificationExpiration();
				$boolIsValid &= $this->valStudentIdNumber();
				break;

			case 'customer_quick_view':
				$boolIsValid &= $this->valUsername( $objDatabase, $boolIsEmailRequired, false, true );
				break;

			case 'customer_quick_view_basic_info':
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'validate_resident_portal_profile_update':
				$boolIsValid &= $this->valUsername( $objDatabase, false, false, true );
				$boolIsValid &= $this->valEmailAddress( true );
				$boolIsValid &= $this->valEmailAddressWithPassword();
				break;

			case 'resident_portal_profile_update':
				$boolIsValid &= $this->valUsername( $objDatabase, false );

				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_COMPANY_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valCompanyName();
				}
				break;

			case 'insert_occupant':
				$boolIsValid 		 &= $this->valNameFirst();
				$boolIsValid 		 &= $this->valNameLast();
				$boolIsValidBirthDate = $this->valBirthDate( true );
				$boolIsValid 		 &= $boolIsValidBirthDate;
				if( true == $boolIsValidBirthDate ) {
					$boolIsValid &= $this->valCustomerUnderAge();
				}
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valNameMiddle();
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'update_occupant':
				$boolIsValid		 &= $this->valNameFirst();
				$boolIsValid 		 &= $this->valNameLast();
				$boolIsValidBirthDate = $this->valBirthDate( true );
				$boolIsValid 		 &= $boolIsValidBirthDate;

				$boolIsValidateAfterChangeBirthDateOrCustomerLeaseType = ( ( $this->m_objCustomer->getBirthDate() == $objLeaseCustomer->getCustomerBirthDate() && $intOldCustomerTypeId == $this->m_objCustomer->getCustomerTypeId() ) ) ? false : true;
				if( true == $boolIsValidateBirthDate || true == $boolIsValidateAfterChangeBirthDateOrCustomerLeaseType && true == $boolIsValidBirthDate ) {
						$boolIsValid &= $this->valCustomerUnderAge();
				}

				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valNameMiddle();
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'insert_primary_misc_income_individual':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameMiddle();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'insert_primary_misc_income_business':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valBusinessName();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'update_outbound_call_customer':
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'update_contact_info':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValidBirthDate = $this->valBirthDate( false );
				$boolIsValid &= $boolIsValidBirthDate;
				if( true == $boolIsValidBirthDate ) {
					$boolIsValid &= $this->valCustomerUnderAge();
				}
				$boolIsValid &= $this->valUsername( $objDatabase, true );
				// $boolIsValid &= $this->valPrimaryPhoneNumber( true );
				// $boolIsValid &= $this->valSecondaryPhoneNumber( false );
				break;

			case 'update_customer_email':
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'validate_commercial_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valTaxNumber( $strAction, COccupancyType::COMMERCIAL );
				break;

			case 'insert_group_info':
			case 'update_group_info':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyName( $boolIsFromGroup );
				if( 'update_group_info' == $strAction ) {
					$boolIsValid &= $this->valUsername( $objDatabase, $boolIsEmailRequired );
				} else {
					$boolIsValid &= $this->valEmailAddress();
				}

				$boolIsValid &= $this->valTaxNumber( $strAction, NULL, $objDatabase );
				break;

			case 'group_occupant_customer':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase );
				$boolIsValid &= $this->valBirthDate();
				// $boolIsValid &= $this->valMobileNumber();
				break;

			case 'validate_insert_or_update_referrer':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valEmailAddressRequired();
				break;

			case 'validate_insert_or_update_organization_customer':
				$boolIsValid &= $this->valCompanyName();
				if( true == valId( $this->m_objCustomer->getId() ) ) {
					$boolIsValid &= $this->valDuplicateCustomer( $objDatabase );
				}
				break;

			case 'insert_customer':
				$boolIsValid	&= $this->valNameFirst();
				$boolIsValid	&= $this->valNameMiddle();
				$boolIsValid 	&= $this->valNameLast();
				$boolIsValid	&= $this->valBirthDate();
				$boolIsValid	&= $this->valEmailAddress();
				$boolIsValid	&= $this->valUsername( $objDatabase );
				$boolIsValid	&= $this->valI18nPrimaryPhoneNumber();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>