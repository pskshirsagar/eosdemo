<?php

class CApplicantIncomeValidator {

	protected $m_objApplicantIncome;

	public function __construct() {
		return;
	}

	public function setApplicantIncome( $objApplicantIncome ) {
		$this->m_objApplicantIncome = $objApplicantIncome;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getId() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getApplicantId() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', __( 'Applicant id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIncomeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getIncomeTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_type_id', __( 'Income type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		// if( true == is_null( $this->m_objApplicantIncome->getFrequencyId())) {
		//    $boolIsValid = false;
		//    $this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', '' ));
		// }

		return $boolIsValid;
	}

	public function valPayerName( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getPayerName() ) ) {
			$boolIsValid = false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_name' : 'payers_name';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} name is required.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPayerPhoneNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true ) {

		$boolIsValid = true;
		$intLength 	 = strlen( $this->m_objApplicantIncome->getPayerPhoneNumber() );

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . '_phone_number' : 'payers_phone_number';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_objApplicantIncome->getPayerPhoneNumber() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} phone number is required.', [ $strSectionName ] ) ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} phone number must be between 10 and 15 characters.', [ $strSectionName ] ) ) );

		} elseif( false == is_null( $this->m_objApplicantIncome->getPayerPhoneNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objApplicantIncome->getPayerPhoneNumber(), false ) ) ) {

			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} phone number is required.', [ $strSectionName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPayerAddress( $intIncomeTypeId = 0, $strSectionName = NULL, $intIsAlien = NULL ) {

		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();

		if( 0 == strlen( $this->m_objApplicantIncome->getStreetLine1() ) && 0 == strlen( $this->m_objApplicantIncome->getStreetLine2() ) && 0 == strlen( $this->m_objApplicantIncome->getStreetLine3() ) ) {
			$boolIsValid = false;
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_street_line1' : 'street_line1';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} street is required.', [ $strSectionName ] ) ) );
		}

		if( 0 == $intIsAlien ) {
			if( 0 == strlen( $this->m_objApplicantIncome->getCity() ) ) {
				$boolIsValid = false;
				$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_city' : 'city';
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} city is required.', [ $strSectionName ] ) ) );
			}

			if( 0 == strlen( $this->m_objApplicantIncome->getPostalCode() ) ) {
				$boolIsValid = false;
				$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_postal_code' : 'postal_code';
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} zip/postal code is required.', [ $strSectionName ] ) ) );
			} elseif( false == CValidation::validatePostalCode( $this->m_objApplicantIncome->getPostalCode() ) ) {
				$boolIsValid = false;
				$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_postal_code' : 'postal_code';
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Invalid {%s,0} zip/postal code.', [ $strSectionName ] ) ) );
			}

			if( 0 == strlen( $this->m_objApplicantIncome->getStateCode() ) ) {
				$boolIsValid = false;
				$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_state_code' : 'state_code';
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} state/province code is required.', [ $strSectionName ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPostalCode( $isPrimaryApplicant = true, $strSectionName = NULL, $arrobjPropertyPreferences = NULL ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();

		if( 0 == strlen( trim( $this->m_objApplicantIncome->getPostalCode() ) ) && true == valArr( $arrobjPropertyPreferences ) && ( ( true == $isPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) || ( false == $isPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CO_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) ) ) {

			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionName ] ) ) );

		} elseif( 0 < strlen( trim( $this->m_objApplicantIncome->getPostalCode() ) ) && false == CValidation::validatePostalCode( $this->m_objApplicantIncome->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Invalid {%s,0} zip/postal code.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPosition( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getPosition() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_position' : 'position';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} position is required.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valAmount( $intIncomeTypeId = 0, $strSectionName = NULL, $strCustomerFieldName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getAmount() ) || 0 == ( int ) $this->m_objApplicantIncome->getAmount() ) {
			$boolIsValid &= false;
			$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
			$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'monthly income';
			$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_gross_amount' : 'gross_amount';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valContactName( $intIncomeTypeId = 0, $strSectionName = NULL, $strCustomerFieldName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getContactName() ) ) {
			$boolIsValid &= false;
			$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
			$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact name';
			$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_contact_name' : 'contact_name';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valContactPhoneNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = strlen( $this->m_objApplicantIncome->getContactPhoneNumber() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact phone';
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_contact_phone_number' : 'contact_phone_number';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_objApplicantIncome->getContactPhoneNumber() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ) ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number must be between 10 and 15 characters.', [ $strSectionName, $strCustomerFieldName ] ) ) );

		} elseif( false == is_null( $this->m_objApplicantIncome->getContactPhoneNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objApplicantIncome->getContactPhoneNumber(), false ) ) ) {

			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valContactFaxNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = strlen( $this->m_objApplicantIncome->getContactFaxNumber() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact fax';
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_contact_fax_number' : 'contact_fax_number';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_objApplicantIncome->getContactFaxNumber() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ) ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number must be between 10 and 15 characters.', [ $strSectionName, $strCustomerFieldName ] ) ) );

		} elseif( false == is_null( $this->m_objApplicantIncome->getContactFaxNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objApplicantIncome->getContactFaxNumber(), false ) ) ) {

			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valContactEmail( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = strlen( $this->m_objApplicantIncome->getContactEmail() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact email';
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_contact_email' : 'contact_email';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_objApplicantIncome->getContactEmail() ) || 0 == $intLength ) {
				$boolIsValid = false;
				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ) ) );
			}
		}

		if( 0 < $intLength && false == CValidation::validateEmailAddresses( $this->m_objApplicantIncome->getContactEmail() ) ) {
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDateStarted( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = false ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_date_started' : 'date_started';

		if( true == $boolIsRequired && true == is_null( $this->m_objApplicantIncome->getDateStarted() ) ) {
			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date is required.', [ $strSectionName ] ) ) );

		} elseif( ( true == $boolIsRequired && ( true == is_null( $this->m_objApplicantIncome->getDateStarted() ) || 0 > strlen( $this->m_objApplicantIncome->getDateStarted() ) || false == CValidation::validateDate( $this->m_objApplicantIncome->getDateStarted() ) ) ) || ( false == is_null( $this->m_objApplicantIncome->getDateStarted() ) && 0 < strlen( $this->m_objApplicantIncome->getDateStarted() ) && 1 !== CValidation::checkDate( $this->m_objApplicantIncome->getDateStarted() ) ) ) {
			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date must be in mm/dd/yyyy form.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valDateStopped( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = false ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_date_stopped' : 'date_stopped';

		if( true == $boolIsRequired && true == is_null( $this->m_objApplicantIncome->getDateStopped() ) ) {
			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date is required.', [ $strSectionName ] ) ) );

		} elseif( ( true == $boolIsRequired && ( true == is_null( $this->m_objApplicantIncome->getDateStopped() ) || 0 > strlen( $this->m_objApplicantIncome->getDateStopped() ) || false == CValidation::validateDate( $this->m_objApplicantIncome->getDateStopped() ) ) ) || ( false == is_null( $this->m_objApplicantIncome->getDateStopped() ) && 0 < strlen( $this->m_objApplicantIncome->getDateStopped() ) && 1 !== CValidation::checkDate( $this->m_objApplicantIncome->getDateStopped() ) ) ) {
			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date must be in mm/dd/yyyy form.', [ $strSectionName ] ) ) );
		}

		//         if( true == $boolIsValid ){
		//         	if(( strtotime( $this->m_objApplicantIncome->getDateStopped())) >= ( strtotime( date( "m/d/Y" )))) {
		//         		$boolIsValid &= false;
		// 				$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName , $strSectionName . ' stopped date must be less than current date.' ));
		//         	}
		//         }

		if( false == is_null( $this->m_objApplicantIncome->getDateStarted() ) && false == is_null( $this->m_objApplicantIncome->getDateStopped() ) && ( strtotime( $this->m_objApplicantIncome->getDateStarted() ) ) > ( strtotime( $this->m_objApplicantIncome->getDateStopped() ) ) ) {
			$boolIsValid &= false;
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date must be greater than the start date.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantIncome->getDescription() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_description' : '_description';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} description is required.', [ $strSectionName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $intIsAlien = NULL ) {

		$boolIsValid = true;

		if( false == valObj( $objIncomeType, 'CIncomeType' ) ) {
			return $boolIsValid;
		}

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			$arrobjPropertyApplicationPreferences = [];
		}

		if( false == valArr( $arrstrIncomeTypeCustomFields ) ) {
			return $boolIsValid;
		}

		if( false == array_key_exists( $objIncomeType->getId(), $arrstrIncomeTypeCustomFields ) ) {
			return $boolIsValid;
		}

		$intIncomeTypeId 			= $objIncomeType->getId();

		if( false == is_null( $objIncomeType->getIncomeTypeCount() ) ) {
			$this->m_objApplicantIncome->setIncomeTypeCount( '_' . $objIncomeType->getIncomeTypeCount() );
		}

		$strSectionName 			= $objIncomeType->getName();

		$strRenameIncomeTypeKey 		= 'RENAME_INCOME_TYPE_' . $objIncomeType->getPreferenceKey();
		// $strOverrideIncomeTypeKey	= 'OVERRIDE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey();

		if( true == array_key_exists( $strRenameIncomeTypeKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences[$strRenameIncomeTypeKey]->getValue() ) ) {
			$strSectionName 		= $arrobjPropertyApplicationPreferences[$strRenameIncomeTypeKey]->getValue();
		}

		$strRequireIncomeTypeNameFieldKey 			= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_NAME';
		$strRequireIncomeTypePhoneNumberFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_PHONE_NUMBER';
		$strRequireIncomeTypeAddressFieldKey 		= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_ADDRESS';
		$strRequireIncomeTypeContactNameFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_NAME';
		$strRequireIncomeTypeContactPhoneFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';
		$strRequireIncomeTypeContactFaxFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_FAX_NUMBER';
		$strRequireIncomeTypeContactEmailFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_EMAIL';
		$strRequireIncomeTypePositionFieldKey 		= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_POSITION';
		$strRequireIncomeTypeDateStartedFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DATE_STARTED';
		$strRequireIncomeTypeDateStoppedFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DATE_STOPPED';
		$strRequireIncomeTypeDescriptionFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DESCRIPTION';

		if( true == array_key_exists( 'PAYER_NAME', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valPayerName( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'ADDRESS', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeAddressFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valPayerAddress( $intIncomeTypeId, $strSectionName, $intIsAlien );
		} elseif( 0 < strlen( $this->m_objApplicantIncome->getPostalCode() ) && false == CValidation::validatePostalCode( $this->m_objApplicantIncome->getPostalCode() ) ) {
			$boolIsValid &= false;
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objApplicantIncome->getIncomeTypeCount() . '_postal_code' : 'postal_code';
			$this->m_objApplicantIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Invalid {%s,0} zip/postal code.', [ $strSectionName ] ) ) );
		}

		if( true == array_key_exists( 'CONTACT_NAME', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactName( $intIncomeTypeId, $strSectionName, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_NAME']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_PHONE', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactPhoneFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactPhoneNumber( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_PHONE']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_PHONE', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactPhoneFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantIncome->getContactPhoneNumber() ) ) {
			$boolIsValid &= $this->valContactPhoneNumber( $intIncomeTypeId, $strSectionName, $booIsRequired = false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_PHONE']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantIncome->getContactFaxNumber() ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intIncomeTypeId, $strSectionName, $booIsRequired = false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_EMAIL', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactEmail( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_EMAIL', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantIncome->getContactEmail() ) ) {
			$boolIsValid &= $this->valContactEmail( $intIncomeTypeId, $strSectionName, $booIsRequired = false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );
		}

		$boolIsValid &= $this->valAmount( $intIncomeTypeId, $strSectionName, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['MONTHLY_INCOME']['FIELD_NAME'] );

		if( true == array_key_exists( 'POSITION', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypePositionFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valPosition( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'STARTED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDateStartedFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDateStarted( $intIncomeTypeId, $strSectionName, true );

		} elseif( true == array_key_exists( 'STARTED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeDateStartedFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantIncome->getDateStarted() ) ) {
			$boolIsValid &= $this->valDateStarted( $intIncomeTypeId, $strSectionName, $booIsRequired = false );
		}

		if( true == array_key_exists( 'STOPPED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDateStoppedFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDateStopped( $intIncomeTypeId, $strSectionName, true );

		} elseif( true == array_key_exists( 'STOPPED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeDateStoppedFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantIncome->getDateStopped() ) ) {
			$boolIsValid &= $this->valDateStopped( $intIncomeTypeId, $strSectionName, $booIsRequired = false );
		}

		if( true == array_key_exists( 'DESCRIPTION', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDescriptionFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDescription( $intIncomeTypeId, $strSectionName );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $intIsAlien, $arrobjPropertyPreferences = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'applicant_incomes':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $intIsAlien );
				break;

			case 'application_incomes':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $intIsAlien );
				break;

			case 'applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valAmount( 0, 'Applicant' );
				break;

			case 'new_applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valIncomeTypeId();
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_APPLICANT_EMPLOYER_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valPayerName( 0, 'Employer' );
				}
				$boolIsValid &= $this->valPostalCode( true, 'Employer', $arrobjPropertyPreferences );
				$boolIsValid &= $this->valAmount( 0, 'Applicant' );
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_EMPLOYMENT_START_DATE', $arrobjPropertyPreferences ) ) {
					$boolIsEmploymentStartDateRequired = true;
				} else {
					$boolIsEmploymentStartDateRequired = false;
				}
				$boolIsValid &= $this->valDateStarted( CIncomeType::CURRENT_EMPLOYER, 'Employment', $boolIsEmploymentStartDateRequired );
				break;

			case 'secondary_applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CO_APPLICANT_EMPLOYER_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valPayerName( 0, 'Employer' );
				}
				$boolIsValid &= $this->valPostalCode( false, 'Employer', $arrobjPropertyPreferences );
				$boolIsValid &= $this->valIncomeTypeId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>