<?php
 class CSpecialValidator {

 	protected $m_objSpecial;

 	public function __construct() {
 		return;
 	}

 	public function setSpecial( $objSpecial ) {
 		$this->m_objSpecial = $objSpecial;
 	}

 	public function valId() {
 		if( true == is_null( $this->m_objSpecial->getId() ) ) {
 			trigger_error( __( 'Id is required - {%s,0}', [ 'CSpecial' ] ), E_USER_ERROR );
 			return false;
 		}
 		return true;
 	}

 	public function valCid() {
 		if( true == is_null( $this->m_objSpecial->getCid() ) ) {
 			trigger_error( __( 'A Client Id is required - {%s,0}', [ 'CSpecial' ] ), E_USER_ERROR );
 			return false;
 		}
 		return true;
 	}

 	public function valName() {

 		if( true == is_null( $this->m_objSpecial->getName() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
 			return false;
 		} elseif( 1 > \Psi\CStringService::singleton()->strlen( $this->m_objSpecial->getName() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
 			return false;
 		}
 		return true;
 	}

	public function valLimit() {
 		if( true == is_null( $this->m_objSpecial->getQuantityBudgeted() ) && 1 == $this->m_objSpecial->getLimitQuantity() ) {
			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity_budgeted', __( 'Special Cap amount is required.' ) ) );
			return false;
		} elseif( 0 >= $this->m_objSpecial->getQuantityBudgeted() && 1 == $this->m_objSpecial->getLimitQuantity() ) {
			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity_budgeted', __( 'Quantity budgeted must be greater than zero.' ) ) );
			return false;
		}
		return true;
	}

 	public function valStartDate() {
 		if( true == is_null( $this->m_objSpecial->getStartDate() ) ) return true;

 		if( 1 !== CValidation::checkDate( $this->m_objSpecial->getStartDate() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date must be in correct date format.' ) ) );
 			return false;
 		}
 		return true;
 	}

 	public function valEndDate() {
 		$boolIsValid = true;

 		if( false == is_null( $this->m_objSpecial->getEndDate() ) ) {
 			if( 1 !== CValidation::checkDate( $this->m_objSpecial->getEndDate() ) ) {
 				$boolIsValid = false;
 				$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'End date must be in correct date format.' ) ) );
 			}

 			if( true == $boolIsValid && false == is_null( $this->m_objSpecial->getEndDate() ) && 1 === CValidation::checkDate( $this->m_objSpecial->getStartDate() ) ) {
 				if( strtotime( $this->m_objSpecial->getEndDate() ) <= strtotime( $this->m_objSpecial->getStartDate() ) ) {
 					$boolIsValid = false;
 					$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'End Date must be after Start Date.' ) ) );
 				}
 			}
 		}

 		return $boolIsValid;
 	}

 	public function valArCodeId() {
 		$boolIsValid = true;
		$intArCodeId = $this->m_objSpecial->getArCodeId();
 		if( true == empty( $intArCodeId ) ) {
 			$boolIsValid = false;
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Please select charge code.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valSpecialPromoCode() {
 		$boolIsValid = true;

 		if( false == $this->m_objSpecial->getUseCode() ) {
 			return $boolIsValid;
 		}

 		if( 0 == strlen( $this->m_objspecial->getCouponCode() ) ) {
 			$boolIsValid = false;
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'coupon_code', __( 'Use code is required.' ) ) );

 		} elseif( false == ctype_alnum( $this->m_objspecial->getCouponCode() ) ) {
 			$boolIsValid = false;
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'coupon_code', __( 'Use code allows only alphanumeric characters.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valDescription() {
 		if( true == is_null( $this->m_objSpecial->getDescription() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Description is required.' ) ) );
 			return false;
 		}

 		return true;
 	}

 	public function valSpecialRecipientId() {
 		if( true == is_null( $this->m_objSpecial->getSpecialRecipientId() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Recipient is required.' ) ) );
 			return false;
 		}

 		return true;
 	}

 	public function valSpecialTypeId() {
 		if( true == is_null( $this->m_objSpecial->getSpecialTypeId() ) ) {
 			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Type is required.' ) ) );
 			return false;
 		}

 		return true;
 	}

 	public function valApplicationStageStatus() {
		if( true == is_null( $this->m_objSpecial->getApplicationStageStatusId() ) && 1 == $this->m_objSpecial->getLimitQuantity() ) {
			$this->m_objSpecial->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_stage_status', __( 'Decrease Special Cap At is required.' ) ) );
			return false;
		}
		return true;
	}

 	public function validate( $strAction ) {
 		$boolIsValid = true;

 		switch( $strAction ) {
 			case VALIDATE_INSERT:
 			case VALIDATE_UPDATE:
 				$boolIsValid &= $this->valCid();
 				$boolIsValid &= $this->valName();
 				$boolIsValid &= $this->valSpecialRecipientId();
 				$boolIsValid &= $this->valSpecialTypeId();
 				$boolIsValid &= $this->valStartDate();
 				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valLimit();
				if( $this->m_objSpecial->getSpecialRecipientId() != CSpecialRecipient::RESIDENTS ) {
					$boolIsValid &= $this->valApplicationStageStatus();
				}
 				break;

 			case VALIDATE_DELETE:
 				$boolIsValid &= $this->valId();
 				$boolIsValid &= $this->valCid();
 				break;

 			default:
 				$boolIsValid = true;
 				break;
 		}

 		return $boolIsValid;
 	}

 }
?>