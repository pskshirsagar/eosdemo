<?php

use Psi\Libraries\UtilErrorMsg\CErrorMsg;

class CApInvoiceValidator {

	use \Psi\Libraries\UtilErrorMsg\TErrorMessages;

	protected $m_objApInvoice;
	protected $m_objClientDatabase;
	protected $m_objApPayee;
	protected $m_objApPayeeLocation;
	protected $m_arrobjBankAccounts;

	protected $m_arrobjApPayeeComplianceStatuses;
	protected $m_arrobjRoutingTagCompanyPreferences = [];
	protected $m_arrobjProperties = [];
	protected $m_arrobjPropertyGlSettings = [];
	protected $m_arrobjApPayeePropertyGroups = [];

	protected $m_arrintBankAccountProperties = [];
	protected $m_arrintApCodeProperties = [];

	protected $m_arrmixValidationData = [];

	public function __construct( $objApInvoice, $objClientDatabase ) {
		$this->setApInvoice( $objApInvoice );
		$this->setClientDatabase( $objClientDatabase );
	}

	// Validate function

	public function validate( $strAction, $arrmixValidationData = [] ) {
		$boolIsValid = true;

		$this->setValidationData( $arrmixValidationData );

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_ap_invoice_exception':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApHeaderTypeId();
				$boolIsValid &= $this->valApHeaderSubTypeId();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valApPayeeAccountId();
				$boolIsValid &= $this->valApRemittanceId();
				$boolIsValid &= $this->valApPayeeTermId();
				$boolIsValid &= $this->valApPayeeProperties();
				$boolIsValid &= $this->valApPayeeDetails();
				$boolIsValid &= $this->valApPayeeStatusTypeId();
				$boolIsValid &= $this->valApPayeeComplianceStatus();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valPoApHeaderIds();
				$boolIsValid &= $this->valApRoutingTagId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valDuplicateInvoiceNumber();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valTransactionAmountDue();
				$boolIsValid &= $this->valTaxAmounts();
				$boolIsValid &= $this->valRetentionPercent();
				$boolIsValid &= $this->valRetentionAmount();
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valRequirePoForInvoice();
				$boolIsValid &= $this->valInvoiceTotalQuantity();
				$boolIsValid &= $this->valApCodePropertyAssociation();
				$boolIsValid &= $this->valParentPropertyPostMonth();
				break;

			case 'validate_create_credit_card_invoice_exception':
				$boolIsValid &= $this->valCreateCcInvoice();
				break;

			case 'validate_import_invoice':
				$boolIsValid &= $this->valInvoiceAttachment();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// Get Functions

	private function getApInvoice() {
		return $this->m_objApInvoice;
	}

	private function getApHeader() {
		return $this->m_objApInvoice->getApHeader();
	}

	private function getApDetails() {
		return $this->m_objApInvoice->getApHeader()->getApDetails();
	}

	private function getValidationData() {
		return $this->m_arrmixValidationData;
	}

	private function getValidationDataByKey( $strKey ) {
		return getArrayElementByKey( $strKey, $this->m_arrmixValidationData );
	}

	private function getClientDatabase() {
		return $this->m_objClientDatabase;
	}

	// set Functions

	private function setApInvoice( $objApInvoice ) {
		$this->m_objApInvoice = $objApInvoice;
	}

	private function setClientDatabase( $objClientDatabase ) {
		$this->m_objClientDatabase = $objClientDatabase;
	}

	private function setValidationData( $arrmixValidationData ) {
		$this->m_arrmixValidationData = $arrmixValidationData;
	}

	// Val Functions

	private function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'id', __( 'id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getCid() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApHeaderTypeId() {
		$boolIsValid = true;

		if( CApHeaderType::INVOICE != $this->getApHeader()->getApHeaderTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_header_type_id', __( 'ap header type must be invoice.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApHeaderSubTypeId() {
		$boolIsValid = true;

		if( false == array_key_exists( $this->getApHeader()->getApHeaderSubTypeId(), CApHeaderSubType::$c_arrstrApHeaderSubTypes ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_header_sub_type_id', __( 'ap header sub type must be invoice.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApHeaderModeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApHeaderModeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_header_mode_id', __( 'ap header mode is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPhysicalStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPhysicalStatusTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_physical_status_type_id', __( 'ap physical status type is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApFinancialStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApFinancialStatusTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_financial_status_type_id', __( 'ap financial status type is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPayeeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Valid vendor is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valOrderHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getOrderHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'order_header_id', __( 'Order header is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeLocationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPayeeLocationId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_location_id', __( 'Vendor location is required.' ) ) );
			return $boolIsValid;
		}

		$this->getOrFetchApPayeeLocation();

		if( false == valObj( $this->m_objApPayeeLocation, 'CApPayeeLocation' ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_location', __( 'Failed to load vendor location.' ) ) );
			$boolIsValid &= false;

		} elseif( false == is_null( $this->m_objApPayeeLocation->getDisabledBy() ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_location', __( 'You have selected a disabled vendor location.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	private function valApPayeeAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPayeeAccountId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_account_id', __( 'Vendor account is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApRemittanceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApRemittanceId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_remittance_id', __( 'Ap remittance is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeTermId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPayeeTermId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_term_id', __( 'Ap payee term is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valGlTransactionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getGlTransactionTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'gl_transaction_type_id', __( 'Gl transaction type is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valJobPhaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getJobPhaseId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'job_phase_id', __( 'Job phase is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApContractId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApContractId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_contract_id', __( 'Contract is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkJobPhaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkJobPhaseId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_job_phase_id', __( 'Bulk job phase is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkApContractId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkApContractId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_ap_contract_id', __( 'Bulk Ap Contract is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkBankAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkBankAccountId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_bank_account_id', __( 'Bulk bank account is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_property_id', __( 'Bulk property is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkApFormulaId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkApFormulaId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_ap_formula_id', __( 'Bulk formula is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkApCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkApCodeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_ap_code_id', __( 'Bulk Ap Code is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkUnitNumberId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkUnitNumberId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_unit_number_id', __( 'Bulk unit number is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkCompanyDepartmentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkCompanyDepartmentId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_company_department_id', __( 'Bulk company department is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkGlDimensionId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkGlDimensionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_gl_dimension_id', __( 'Bulk gl dimension is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkPropertyBuildingId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkPropertyBuildingId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_property_building_id', __( 'Bulk property building is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkBudgetApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkBudgetApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_budget_ap_header_id', __( 'Bulk budget ap header is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApContractApHeaderIds() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApContractApHeaderIds() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_contract_ap_header_ids', __( 'Ap contract ap header ids required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPoApHeaderIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getApHeader()->getPoApHeaderIds() ) ) {
			return $boolIsValid;
		}

		$arrintPOApHeaderIds = array_unique( array_filter( $this->getApHeader()->getPoApHeaderIds() ) );

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getApHeader()->getCid() . '
						AND id IN ( ' . implode( ',', $arrintPOApHeaderIds ) . ' )
						AND deleted_on IS NULL
						AND ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ap_header_sub_type_id IN ( ' . sqlIntImplode( [ CApHeaderSubType::CATALOG_PURCHASE_ORDER, CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER ] ) . ' ) ';

		$intPOCount = ( int ) CApHeaders::fetchApHeaderCount( $strWhere, $this->getClientDatabase() );

		if( $intPOCount != \Psi\Libraries\UtilFunctions\count( $arrintPOApHeaderIds ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'name', __( 'PO does not exist.' ) ) );
		}

		if( true === $boolIsValid && true === in_array( $this->getApHeader()->getApHeaderSubTypeId(), [ CApHeaderSubType::STANDARD_INVOICE, CApHeaderSubType::STANDARD_JOB_INVOICE ] ) ) {

			$arrintPoApDetailIds = ( true === valArr( $this->getApHeader()->getApDetails() ) ) ? array_filter( array_keys( rekeyObjects( 'PoApDetailId', $this->getApHeader()->getApDetails() ) ) ) : [];

			$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS', $this->getApHeader()->getCid(), $this->getClientDatabase() );

			if( ( true === valObj( $objCompanyPreference, 'CCompanyPreference' ) && '1' !== $objCompanyPreference->getValue() ) || false === valObj( $objCompanyPreference, 'CCompanyPreference' ) ) {
				$arrstrPoNumbers = array_filter( array_keys( rekeyArray( 'header_number', ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchPoLineItemDetailsByIdsByApHeaderSubTypeIdByCid( $arrintPoApDetailIds, $this->getApHeader()->getApHeaderSubTypeId(), $this->getApHeader()->getCid(), $this->getClientDatabase(), true ) ) ) );

				if( true === valArr( $arrstrPoNumbers ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'po_ap_header_ids', __( 'The selected line items of  #' . sqlIntImplode( $arrstrPoNumbers ) . ' PO(s) is/are invoiced and "Allow multiple invoices for standard PO line items" setting is set to NO. Please select the available PO line items.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	private function valReversalApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getReversalApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'reversal_ap_header_id', __( 'Reversal ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPaymentApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getPaymentApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'payment_ap_header_id', __( 'Payment ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valScheduledApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getScheduledApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'scheduled_ap_header_id', __( 'Scheduled ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTemplateApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTemplateApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'template_ap_header_id', __( 'Template ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valReimbursementApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getReimbursementApHeaderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'reimbursement_ap_header_id', __( 'Reimbursement ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getFrequencyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Frequency id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valLeaseCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getLeaseCustomerId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'lease_customer_id', __( 'Lease customer id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valRefundArTransactionId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getRefundArTransactionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'refund_ar_transaction_id', __( 'Refund ar transaction id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApBatchId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApBatchId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_batch_id', __( 'Ap batch id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApHeaderExportBatchId() {
		$boolIsValid = true;
		if( true == is_null( $this->getApHeader()->getAccountingExportBatchId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_header_export_batch_id', __( 'Ap header export batch id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPaymentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPaymentId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payment_id', __( 'Ap payment id is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valComplianceJobId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getComplianceJobId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'compliance_job_id', __( 'Compliance job is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApRoutingTagId() {
		$boolIsValid = true;

		$this->getOrFetchRoutingTagCompanyPreferences();

		if( true == is_null( $this->getApHeader()->getApRoutingTagId() ) && true == array_key_exists( 'USE_ROUTING_TAGS', $this->m_arrobjRoutingTagCompanyPreferences ) &&
			true == array_key_exists( 'ROUTING_TAGS_REQUIRED_INVOICE', $this->m_arrobjRoutingTagCompanyPreferences ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_routing_tag_id', __( 'Ap routing tag required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBudgetChangeOrderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBudgetChangeOrderId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'budget_charge_order_id', __( 'Budget change order is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valRemotePrimaryKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getRemotePrimaryKey() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'remote_primary_key', __( 'Remote primary key is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTemplateName() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTemplateName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'template_name', __( 'Template name is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTransactionDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTransactionDatetime() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_datetime', __( 'Invoice datetime is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPostDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getApHeader()->getPostDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_date', __( 'Invoice post date is required.' ) ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getApHeader()->getPostDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_date', __( 'Invoice post date should be in mm/dd/yyyy format.' ) ) );
			return $boolIsValid;
		}

		if( false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', $this->getApHeader()->getPostDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_date', __( 'Invoice date should be in mm/dd/yyyy format.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	private function valDueDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getApPaymentId() ) ) {
			return $boolIsValid;
		}

		$strDueDate = ( new DateTime( $this->getApHeader()->getDueDate() ) )->format( 'mm/dd/yyyy' );

		if( false == valStr( $strDueDate ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'due_date', __( 'Invoice due date is required in the format mm/dd/yyyy.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPostMonth() {
		$boolIsValid                = true;
		$arrstrGlLockedProperties   = [];

		if( false == valStr( $this->getApHeader()->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );

		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getApHeader()->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		$this->getOrFetchProperties();
		$this->getOrFetchPropertyGlSettings();

		if( ( false == valArr( $this->getApHeader()->getApDetails() ) ||
				false == valArr( $this->m_arrobjProperties ) ||
				false == valArr( $this->m_arrobjPropertyGlSettings ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'No property found for invoice {%s, 0}<br>', [ $this->getApHeader()->getHeaderNumber() ] ) ) );
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$arrobjApDetails = ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		foreach( array_keys( $arrobjApDetails ) as $intPropertyId ) {

			$objPropertyGlSetting = getArrayElementByKey( $intPropertyId, $this->m_arrobjPropertyGlSettings );

			// If no property gl setting available or property is in migration mode and initial import is on then skip iteration.
			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) || ( 1 == $this->getApHeader()->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() ) ) {
				continue;
			}

			$objProperty = getArrayElementByKey( $intPropertyId, $this->m_arrobjProperties );

			// Validate for AP/GL lock period
			if( true == valObj( $objProperty, 'CProperty' ) && ( strtotime( $this->getApHeader()->getPostMonth() ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) || strtotime( $this->getApHeader()->getPostMonth() ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) ) {
				$arrstrGlLockedProperties[$objProperty->getId()] = $objProperty->getPropertyName();
			}
		}

		if( true == valArr( $arrstrGlLockedProperties ) ) {
			$boolIsValid &= false;
			$strMessage = __( 'GL or AP is locked for property(s) \'{%s, 0}\' of invoice number {%s, 1}.', [ implode( '\', \'', array_unique( $arrstrGlLockedProperties ) ), $this->getApHeader()->getHeaderNumber() ] );
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_month', $strMessage ) );
		}

		// Validation to check earlier post month for payment ap header
		if( true == valArr( $this->getApHeader()->getOriginalApHeaders() ) ) {

			$arrstrHeaderNumbers = [];

			foreach( $this->getApHeader()->getOriginalApHeaders() as $objOriginalApHeader ) {
				if( strtotime( $this->getApHeader()->getPostMonth() ) < strtotime( $objOriginalApHeader->getPostMonth() ) ) {
					$arrstrHeaderNumbers[] = $objOriginalApHeader->getHeaderNumber();
				}
			}

			if( true == valArr( $arrstrHeaderNumbers ) ) {
				$boolIsValid &= false;
				$arrstrHeaderNumbers = array_unique( $arrstrHeaderNumbers );
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'post_month', __( 'The payment post month for invoice(s) \'{%s, 0}\' is earlier than the invoice\'s post month.', [ implode( ',', $arrstrHeaderNumbers ) ] ) ) );
			}
		}

		return $boolIsValid;
	}

	private function valScheduledPaymentDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getScheduledPaymentDate() ) && true == CValidation::validateDate( $this->getApHeader()->getScheduledPaymentDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'scheduled_payment_date', __( 'Valid scheduled payment date is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getStartDate() ) ) return $boolIsValid;

		if( false == CValidation::checkDateFormat( $this->getApHeader()->getStartDate(), true ) && true == CValidation::validateDate( $this->getApHeader()->getStartDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'start_date', __( 'Valid Start date is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getEndDate() ) && true == CValidation::validateDate( $this->getApHeader()->getEndDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valFrequencyInterval() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getFrequencyInterval() ) && false == valId( $this->getApHeader()->getFrequencyInterval() ) && ( 0 >= $this->getApHeader()->getFrequencyInterval() || 365 < $this->getApHeader()->getFrequencyInterval() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Valid Frequency interval is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valDayOfWeek() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getDayOfWeek() ) && ( CFrequency::WEEKLY == $this->getApHeader()->getFrequencyId() || CFrequency::WEEKDAY_OF_MONTH == $this->getApHeader()->getFrequencyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'day_of_week', __( 'Weekday is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valDaysOfMonth() {
		$boolIsValid = true;

		if( false == valArr( $this->getApHeader()->getDaysOfMonth() ) && CFrequency::MONTHLY == $this->getApHeader()->getFrequencyId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'days_of_month', __( 'Day(s) of month is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getNumberOfOccurrences() ) && false == valId( $this->getApHeader()->getNumberOfOccurrences() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Valid number of occurrences is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valLastPostedDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getLastPostedDate() ) && true == CValidation::validateDate( $this->getApHeader()->getLastPostedDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'last_posted_date', __( 'Valid last posted date is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valNextPostDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApHeader()->getNextPostDate() ) && true == CValidation::validateDate( $this->getApHeader()->getNextPostDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'next_post_date', __( 'Next post date is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valHeaderNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApPaymentId() ) && true == is_null( $this->getApHeader()->getHeaderNumber() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'header_number', __( 'Header number is required.' ) ) );

		} elseif( false == is_null( $this->getApHeader()->getHeaderNumber() ) && true == preg_match( '/(http|https):\/\//i', $this->getApHeader()->getHeaderNumber() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'header_number', __( 'Invalid invoice number.' ) ) );
		}

		return $boolIsValid;
	}

	private function valDuplicateInvoiceNumber() {
		$boolIsValid = true;

		$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR', $this->getApHeader()->getCid(), $this->getClientDatabase() );

		$strWhereClause = '';

		if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) {
			return $boolIsValid;
		} elseif( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) {
			$strWhereClause = ' AND EXISTS (
									SELECT NULL FROM
										company_preferences cp
									WHERE
										cp.cid = ' . ( int ) $this->getApHeader()->getCid() . '
										AND cp.key = \'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR\'
										AND cp.value = \'0\'
								)';
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getApHeader()->getCid() . '
						AND header_number = \'' . addslashes( $this->getApHeader()->getHeaderNumber() ) . '\'
						AND ap_payee_id = ' . ( int ) $this->getApHeader()->getApPayeeId() . '
						AND id != ' . ( int ) $this->getApHeader()->getId() . '
						AND ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
						AND reversal_ap_header_id IS NULL
						AND COALESCE( ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL' . $strWhereClause;

		if( 0 < CApHeaders::fetchApHeaderCount( $strWhere, $this->getClientDatabase() ) ) {

			$objApPayee	= CApPayees::fetchApPayeeByIdByApPayeeLocationIdByCid( $this->getApHeader()->getApPayeeId(), $this->getApHeader()->getApPayeeLocationId(), $this->getApHeader()->getCid(), $this->getClientDatabase(), true );

			if( false == valObj( $objApPayee, 'CApPayee' ) ) {
				$objApPayee = CApPayees::fetchApPayeeByIdByApPayeeLocationIdByCid( $this->getApHeader()->getApPayeeId(), $this->getApHeader()->getApPayeeLocationId(), $this->getApHeader()->getCid(), $this->getClientDatabase(), false );
			}

			$strApPayeeName = $objApPayee->getCompanyName();

			if( false == is_null( $objApPayee->getLocationName() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getLocationName();
			}

			if( false == is_null( $objApPayee->getCity() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getCity();
			}

			if( false == is_null( $objApPayee->getStateCode() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getStateCode();
			}

			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'is_invoice_exists', __( 'This invoice number "{%s, 0}" has already been used for "{%s, 1}" vendor. Please supply a different invoice number. ', [ $this->getApHeader()->getHeaderNumber(), $strApPayeeName ] ) ) );
		}

		return $boolIsValid;
	}

	private function valExternalUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getExternalUrl() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'external_url', __( 'External url is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valAccountNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getAccountNumber() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'account_number', __( 'Account number is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valHeaderMemo() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getHeaderMemo() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'header_memo', __( 'Memo is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valControlTotal() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getControlTotal() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'control_total', __( 'Control total is required.' ) ) );

		} elseif( false == is_numeric( $this->getApHeader()->getControlTotal() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'control_total', __( 'Invalid Control total.' ) ) );

		} elseif( 9999999999999.99 < $this->getApHeader()->getControlTotal() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'control_total', __( 'Control total should not be greater than 9,999,999,999,999.99.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPreApprovalAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getPreApprovalAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'pre_approval_amount', __( 'Pre approval amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTransactionAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTransactionAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Transaction amount is required.' ) ) );

		} elseif( 0 >= $this->getApHeader()->getTransactionAmount() && true == $this->getValidationDataByKey( 'use_item_catalog' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Transaction amount should be greater than zero.' ) ) );

		} elseif( 1000000000 < abs( $this->getApHeader()->getTransactionAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Transaction amount should be between {%m, 0, p:0} and {%m, 1, p:0}.', [ - 1000000000, 1000000000 ] ) ) );
		}

		$arrobjApDetails = $this->getApHeader()->getApDetails();

		if( true == valArr( $arrobjApDetails ) ) {

			$fltTotalAmount = 0.00;

			foreach( $arrobjApDetails as $objApDetail ) {
				$fltTotalAmount = bcadd( $fltTotalAmount, $objApDetail->getTransactionAmount(), 2 );
			}

			if( 0 != bccomp( $fltTotalAmount, $this->getApHeader()->getTransactionAmount(), 2 ) ) {
				$boolIsValid &= false;

				$strHeaderNumbers = $this->getApHeader()->getHeaderNumber();

				if( true == valId( $this->getApHeader()->getApPaymentId() ) ) {
					$strHeaderNumbers = implode( ',', array_keys( rekeyObjects( 'HeaderNumber', ( array ) CApHeaders::fetchHeaderNumbersByApHeaderIdsByCid( array_filter( explode( ',', $this->getApHeader()->getHeaderNumber() ) ), $this->getApHeader()->getCid(), $this->getClientDatabase() ) ) ) );
				}

				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Invoice total and the sum of the detail lines should be the same for invoice number(s) \'{%s, 0}\'', [ $strHeaderNumbers ] ) ) );
			}
		}

		return $boolIsValid;
	}

	private function valTransactionAmountDue() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTransactionAmountDue() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount_due', __( 'Transaction amount due is required.' ) ) );

		} elseif( ( $this->getApHeader()->getTransactionAmountDue() > $this->getApHeader()->getTransactionAmount() ) && CApHeaderSubType::CREDIT_MEMO_INVOICE != $this->getApHeader()->getApHeaderSubTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'transaction_amount_due', __( 'Transaction amount due must be less than or equal to transaction amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTaxAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getTaxAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_amount', __( 'Tax amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valDiscountAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getDiscountAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'discount_amount', __( 'Discount amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valShippingAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getShippingAmount() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'shipping_amount', __( 'Shipping amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valTaxAmounts() {
		$boolIsValid		= true;
		$arrobjApDetails	= $this->getApHeader()->getApDetails();

		if( false == valArr( $arrobjApDetails ) ) {
			return $boolIsValid;
		}

		$fltTaxAmount 		= 0;
		$fltDiscountAmount 	= 0;
		$fltShippingAmount	= 0;

		foreach( $arrobjApDetails as $objApDetail ) {
			$fltTaxAmount		= bcadd( $fltTaxAmount, $objApDetail->getTaxAmount(), 2 );
			$fltDiscountAmount	= bcadd( $fltDiscountAmount, $objApDetail->getDiscountAmount(), 2 );
			$fltShippingAmount	= bcadd( $fltShippingAmount, $objApDetail->getShippingAmount(), 2 );
		}

		if( round( $this->getApHeader()->getTaxAmount(), 4 ) > round( $fltTaxAmount, 4 ) ||
			round( $this->getApHeader()->getDiscountAmount(), 4 ) > round( abs( $fltDiscountAmount ), 4 ) ||
			round( $this->getApHeader()->getShippingAmount(), 4 ) > round( $fltShippingAmount, 4 ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_amount', __( 'Invoice sub-totals is not distributed properly. Please click on Distribute On Line Items button.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkRetentionPercent() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getBulkRententionPercent() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_retension_percent', __( 'Bulk retention percent is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valBulkIsConfidential() {
		$boolIsValid = true;

		if( false == $this->getApHeader()->getBulkIsConfidential() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_is_confidential', __( 'Bulk confidential is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function vallBulkIs1099() {
		$boolIsValid = true;

		if( false == $this->getApHeader()->getBulkIs1099() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bulk_is_1099', __( 'Bulk 1099 is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPayWithSingleCheck() {
		$boolIsValid = true;

		if( false == $this->getApHeader()->getPayWithSingleCheck() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'pay_with_single_check', __( 'Pay with single check is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valIsPosted() {
		$boolIsValid = true;

		if( false == $this->getApHeader()->getIsPosted() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'is_posted', __( 'Posted flag is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valPostedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getPostedOn() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'posted_on', __( 'Posted on is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApprovedBy() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApprovedBy() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'approved_by', __( 'Approved by is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApprovedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getApHeader()->getApprovedOn() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'approved_on', __( 'Approved on is required.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeProperties() {
		$boolIsValid = true;

		$arrstrPropertyNames			= [];
		$arrstrDisabledPropertyNames	= [];
		$arrobjApDetails				= ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		$this->getOrFetchProperties();
		$this->getOrFetchApPayeePropertyGroups();

		foreach( $arrobjApDetails as $objApDetail ) {

			if( false == is_null( $objApDetail->getDeletedOn() ) && false == is_null( $objApDetail->getDeletedBy() ) ) {
				continue;
			}

			$objApPayeePropertyGroup		= getArrayElementByKey( $objApDetail->getPropertyId(), $this->m_arrobjApPayeePropertyGroups );
			$objProperty			= getArrayElementByKey( $objApDetail->getPropertyId(), $this->m_arrobjProperties );

			if( false == valObj( $objApPayeePropertyGroup, 'CApPayeePropertyGroup' ) && true == valObj( $objProperty, 'CProperty' ) ) {

				$arrstrPropertyNames[$objProperty->getId()] = $objProperty->getPropertyName();

			} elseif( true == valObj( $objProperty, 'CProperty' ) && true == $objProperty->getIsDisabled() ) {

				$arrstrDisabledPropertyNames[$objProperty->getId()] = $objProperty->getPropertyName();
			}
		}

		if( true == valArr( $arrstrPropertyNames ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'property_id', __( ' The property(s) \'{%s, 0}\' is/are not associated with selected vendor location.', [ implode( ', ', $arrstrPropertyNames ) ] ) ) );
		}

		if( true == valArr( $arrstrDisabledPropertyNames ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'disabled_property_ids', __( 'The property(s) \'{%s, 0}\' are disabled.', [ implode( ', ', $arrstrDisabledPropertyNames ) ] ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeStatusTypeId() {
		$boolIsValid = true;

		$this->getOrFetchApPayee();

		if( false == valObj( $this->m_objApPayee, 'CApPayee' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_status_type_id', __( 'Failed to load ap payee.' ) ) );

		} elseif( CApPayeeStatusType::LOCKED == $this->m_objApPayee->getApPayeeStatusTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_status_type_id', __( 'Invoice "{%s, 0}" is associated to a vendor on hold "{%s, 1}" and cannot be processed. <br/>', [ $this->getApHeader()->getHeaderNumber(), $this->getApHeader()->getPayeeName() ] ) ) );

		} elseif( CApPayeeStatusType::INACTIVE == $this->m_objApPayee->getApPayeeStatusTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee', __( 'Vendor is inactive.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeComplianceStatus() {
		$boolIsValid = true;

		$this->getOrFetchProperties();
		$this->getOrFetchApPayeeComplianceStatuses();

		$arrintPropertyIds = ( array ) array_keys( rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() ) );

		foreach( array_unique( $arrintPropertyIds ) as $intPropertyId ) {

			$objProperty = getArrayElementByKey( $intPropertyId, $this->m_arrobjProperties );
			$objApPayeeComplianceStatus = getArrayElementByKey( $intPropertyId, $this->m_arrobjApPayeeComplianceStatuses );

			if( false == valObj( $objApPayeeComplianceStatus, 'CApPayeeComplianceStatus' ) ) continue;

			if( CComplianceStatus::NON_COMPLIANT == $objApPayeeComplianceStatus->getComplianceStatusId() && false == $objApPayeeComplianceStatus->getAllowNotCompliantInvoices() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'compliance_status_id_' . $intPropertyId, __( 'Vendor compliance status is not compliant for the property - [ {%s, 0}] and ruleset does not allow invoice creation.', [ $objProperty->getPropertyName() ] ) ) );

			} elseif( CComplianceStatus::REQUIRED == $objApPayeeComplianceStatus->getComplianceStatusId() && false == $objApPayeeComplianceStatus->getAllowRequiredInvoices() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'compliance_status_id_' . $intPropertyId, __( 'Vendor compliance status is incomplete for the property - [ {%s, 0}] and ruleset does not allow invoice creation.', [ $objProperty->getPropertyName() ] ) ) );

			} elseif( CComplianceStatus::PENDING == $objApPayeeComplianceStatus->getComplianceStatusId() && false == $objApPayeeComplianceStatus->getAllowPendingInvoices() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'compliance_status_id_' . $intPropertyId, __( 'Vendor compliance status is pending for the property - [ {%s, 0}] and ruleset does not allow invoice creation.', [ $objProperty->getPropertyName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	private function valPostDateAndDueDate() {
		$boolIsValid = true;

		if( false == ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getPostDate(), 0, 2 ) || false == valStr( $this->getApHeader()->getDueDate() ) ) {
			return $boolIsValid;
		}

		$intPostDate	= mktime( 0, 0, 0, ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getPostDate(), 0, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getPostDate(), 3, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getPostDate(), 6, 4 ) );
		$intDueDate		= mktime( 0, 0, 0, ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getDueDate(), 0, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getDueDate(), 3, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $this->getApHeader()->getDueDate(), 6, 4 ) );

		if( $intPostDate > $intDueDate ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'due_date', __( 'Invoice due date should be greater than or equal to invoice date.' ) ) );
		}

		return $boolIsValid;
	}

	private function valApPayeeDetails() {
		$boolIsValid = true;

		if( false == valId( $this->getApHeader()->getApPayeeId() ) ) {
			return $boolIsValid;
		}

		$strSql = 'SELECT ap_payee_id, ap_payee_location_id, default_ap_remittance_id FROM ap_payee_accounts WHERE cid = ' . ( int ) $this->getApHeader()->getCid() . ' AND id = ' . ( int ) $this->getApHeader()->getApPayeeAccountId() . ';';
		$arrmixApPayeeAccountInfo = fetchData( $strSql, $this->getClientDatabase() );

		$strSql = 'SELECT ap_payee_id FROM ap_payee_locations WHERE cid = ' . ( int ) $this->getApHeader()->getCid() . ' AND id = ' . ( int ) $this->getApHeader()->getApPayeeLocationId() . ';';
		$arrmixApPayeeLocationInfo = fetchData( $strSql, $this->getClientDatabase() );

		$strSql = 'SELECT ap_payee_id, ap_payee_location_id FROM ap_remittances WHERE cid = ' . ( int ) $this->getApHeader()->getCid() . ' AND id = ' . ( int ) $this->getApHeader()->getApRemittanceId() . ';';
		$arrmixApRemittanceInfo = fetchData( $strSql, $this->getClientDatabase() );

		if( ( $this->getApHeader()->getApPayeeId() != $arrmixApPayeeAccountInfo[0]['ap_payee_id'] && true == valId( $this->getApHeader()->getApPayeeAccountId() ) )
		    || ( $this->getApHeader()->getApPayeeId() != $arrmixApPayeeLocationInfo[0]['ap_payee_id'] && true == valId( $this->getApHeader()->getApPayeeLocationId() ) )
		    || ( $this->getApHeader()->getApPayeeId() != $arrmixApRemittanceInfo[0]['ap_payee_id'] && true == valId( $this->getApHeader()->getApRemittanceId() ) ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payees', __( 'Vendor is no longer associated with details [Location/Account/Remittance]. Please select a correct vendor.' ) ) );

		} elseif( $this->getApHeader()->getApPayeeLocationId() != $arrmixApPayeeAccountInfo[0]['ap_payee_location_id'] ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_accounts', __( 'Vendor account is no longer associated with Location. Please select a correct vendor account.' ) ) );

		} elseif( $this->getApHeader()->getApRemittanceId() != $arrmixApPayeeAccountInfo[0]['default_ap_remittance_id'] && $this->getApHeader()->getApPayeeLocationId() != $arrmixApRemittanceInfo[0]['ap_payee_location_id'] ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_payee_accounts', __( 'The selected Remittance is not associated to the Vendor Location or Account.' ) ) );
		}

		return $boolIsValid;
	}

	private function valGlPosting() {
		$boolIsValid = true;

		// $arrobjApDetails = ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		$this->getOrFetchProperties();
		$this->getOrFetchPropertyGlSettings();

		$arrstrInactiveGlPostingProperties = [];

		foreach( $this->m_arrobjPropertyGlSettings as $objPropertyGlSetting ) {
			$objProperty = getArrayElementByKey( $objPropertyGlSetting->getPropertyId(), $this->m_arrobjProperties );

			// If property is in migration mode and intial import is on then skip iteration.
			if( 1 == $this->getApHeader()->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() ) {
				continue;
			}

			if( false == $objPropertyGlSetting->getActivateStandardPosting() ) {
				$boolIsValid &= false;

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$arrstrInactiveGlPostingProperties[$objProperty->getId()] = $objProperty->getPropertyName();
				}
			}
		}

		if( true == valArr( $arrstrInactiveGlPostingProperties ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'activate_standard_posting', __( 'GL posting is not enabled for property(s) \'{%s, 0}\'.', [ implode( ',', $arrstrInactiveGlPostingProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	private function valApDetails() {
		$boolIsValid = true;

		if( false == valArr( $this->getApHeader()->getApDetails() ) ) {
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_details', __( 'At least one detail line item is required.' ) ) );
			$boolIsValid &= false;
		} else {

			foreach( $this->getApHeader()->getApDetails() as $objApDetail ) {
				if( true == valId( $objApDetail->getGlAccountId() ) ) continue;

				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_details', __( 'One or more line items does not have gl account.' ) ) );
				$boolIsValid &= false;
				break;
			}
		}

		return $boolIsValid;
	}

	private function valPropertyBankAccounts() {
		$boolIsValid = true;

		$this->getOrFetchProperties();
		$this->getOrFetchBankAccountProperties();

		$arrobjApDetails = ( array ) $this->getApHeader()->getApDetails();

		$arrmixUnAssociatedBankAccountProperties = [];

		foreach( $arrobjApDetails as $objApDetail ) {
			$objProperty = getArrayElementByKey( $objApDetail->getPropertyId(), $this->m_arrobjProperties );

			$arrintBankAccountProperties = ( array ) getArrayElementByKey( $objApDetail->getBankAccountId(), $this->m_arrintBankAccountProperties );

			// Check if the property is not associated with the bank account.
			if( true == valObj( $objProperty, 'CProperty' ) && false == array_key_exists( $objApDetail->getPropertyId(), $arrintBankAccountProperties ) ) {
				$arrmixUnAssociatedBankAccountProperties[$objProperty->getId()] = $objProperty->getPropertyName();
			} elseif( true == empty( $objApDetail->getPropertyId() ) ) {
				$arrmixUnAssociatedBankAccountProperties[] = '';
			}
		}

		foreach( $arrmixUnAssociatedBankAccountProperties as $arrstrUnAssociatedBankAccountProperties ) {
			$boolIsValid &= false;
			$strPropertyNames = ( true == valArr( $arrstrUnAssociatedBankAccountProperties ) ) ? implode( ', ', $arrstrUnAssociatedBankAccountProperties ) : '';
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'The property(s) \'{%s, 0}\' is not associated with bank account.', [ $strPropertyNames ] ) ) );
		}

		return $boolIsValid;
	}

	private function valRequirePoForInvoice() {

		$boolIsValid = true;
		$arrstrApDetailProperties         = [];

		if( true == valObj( $this->m_objApPayee, 'CApPayee' ) ) {
			$intApPayeeRequirePosForInvoices = $this->m_objApPayee->getRequirePoForInvoice();
			$this->m_arrobjApPayeeLocations = rekeyObjects( 'id', Psi\Eos\Entrata\CApPayeeLocations::createService()->fetchApPayeeLocationsByApPayeeIdByCid( $this->m_objApPayee->getId(), $this->m_objApPayee->getCid(), $this->getClientDatabase() ) );
			if( true == valArr( $this->m_arrobjApPayeeLocations ) ) {
			$this->m_arrobjPropertyPreferences = rekeyObjects( 'PropertyId', ( array ) Psi\Eos\Entrata\CApPayeePropertyGroups::createService()->fetchApPayeePropertyGroupsWithPropertyGroupNameByApPayeeIdByApPayeeLocationIdsByKeyByCid( $this->m_objApPayee->getId(), array_keys( $this->m_arrobjApPayeeLocations ), $this->getApHeader()->getCid(), $this->getClientDatabase() ) );
			}
			$arrobjApDetails = ( array ) $this->getApHeader()->getApDetails();
			foreach( $arrobjApDetails as $objApDetail ) {
				if( true == valArr( $this->m_arrobjPropertyPreferences ) && true == array_key_exists( $objApDetail->getPropertyId(), $this->m_arrobjPropertyPreferences ) ) {
				$objApDetail->setPropertySettingRequirePosForInvoices( $this->m_arrobjPropertyPreferences[$objApDetail->getPropertyId()]->getPropertyPreferenceValue() );

				$arrstrApDetailProperties[] = $this->m_arrobjPropertyPreferences[$objApDetail->getPropertyId()]->getPropertyGroupName();
				}
			}
		}

		$arrintPOApHeaderIds = $this->getApHeader()->getPoApHeaderIds();

		if( true == $intApPayeeRequirePosForInvoices && true == $objApDetail->getPropertySettingRequirePosForInvoices() && false == valArr( $arrintPOApHeaderIds ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'require_po_for_invoice', __( 'A PO is required to add an invoice for this vendor for property(s) {%s, 0}', [ implode( ',', $arrstrApDetailProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	private function valCreateCcInvoice() {
		$boolIsValid						= true;
		$intBankAccountIdOfBankRec			= 0;
		$arrobjApPayments					= NULL;
		$arrmixPropertyIdsAndGlAccountIds	= [];

		$arrintApPaymentIds	= array_filter( array_keys( rekeyObjects( 'CcApPaymentId', $this->getApHeader()->getApDetails() ) ) );
		$arrintGlAccountIds	= array_filter( array_keys( rekeyObjects( 'GlAccountId', $this->getApHeader()->getApDetails() ) ) );

		if( true == valArr( $arrintApPaymentIds ) ) {
			$arrobjApPayments	= ( array ) CApPayments::fetchApPaymentsByIdsByCid( $arrintApPaymentIds, $this->getApHeader()->getCid(), $this->getClientDatabase() );
		} else {
			// In case of invoice creation from Bank Rec
			foreach( ( array ) $this->getApHeader()->getApDetails() as $objApDetail ) {
				if( true == valId( $objApDetail->getPropertyId() ) && true == valId( $objApDetail->getGlAccountId() ) ) {
					$arrmixPropertyIdsAndGlAccountIds[]		= ' ( ' . $objApDetail->getPropertyId() . ',' . $objApDetail->getGlAccountId() . ' ) ';
				}
			}
			$intBankAccountIdOfBankRec	= ( true == valArr( $arrmixPropertyIdsAndGlAccountIds ) ) ? current( array_keys( rekeyObjects( 'BankAccountId', CPropertyBankAccounts::fetchPropertyBankAccountsByPropertyIdsAndGlAccountIdsByCid( $arrmixPropertyIdsAndGlAccountIds, $this->getApHeader()->getCid(), $this->getClientDatabase() ) ) ) ) : 0;
		}

		$arrobjGlAccounts	= CGlAccounts::fetchGlAccountsByIdsByCid( $arrintGlAccountIds, $this->getApHeader()->getCid(), $this->getClientDatabase(), true );

		foreach( ( array ) $this->getApHeader()->getApDetails() as $objApDetail ) {

			$objApPayment	= ( true == valArr( $arrobjApPayments ) ) ? getArrayElementByKey( $objApDetail->getCcApPaymentId(), $arrobjApPayments ) : NULL;
			$objGlAccount	= getArrayElementByKey( $objApDetail->getGlAccountId(), $arrobjGlAccounts );

			if( ( true == valObj( $objApPayment, 'CApPayment' ) && CApPaymentType::CREDIT_CARD != $objApPayment->getApPaymentTypeId() ) ) {
				continue;
			}

			// validate if the bank account of the invoice is not same as payment's bank account
			if( false == valId( $objApDetail->getBankAccountId() ) ) {
				$boolIsValid	&= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Please add default AP Bank Account for the properties.' ) ) );
				break;
			} elseif( ( true == valId( $intBankAccountIdOfBankRec ) && $intBankAccountIdOfBankRec == $objApDetail->getBankAccountId() )
						|| ( true == valObj( $objApPayment, 'CApPayment' ) && $objApPayment->getBankAccountId() == $objApDetail->getBankAccountId() ) ) {
				$boolIsValid	&= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Unable to create invoice due to Default AP Bank Account for the property being the same as the Credit Card Bank Account of the payment. Please change the properties Default AP Bank Account and go to Incomplete Invoices to complete and save.' ) ) );
				break;
			}

			// validate if the Gl account of the invoice is not a cash account
			if( false == valObj( $objGlAccount, 'CGlAccount' ) || CGlAccountUsageType::BANK_ACCOUNT == $objGlAccount->getGlAccountUsageTypeId() ) {
				$boolIsValid	&= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Gl account should be present and should not be a cash account of the bank account.' ) ) );
				break;
			}
		}

		return $boolIsValid;
	}

	private function valInvoiceAttachment() {
		$boolIsValid = true;

		if( false == $this->getApHeader()->getIsAttachment() ) {
			$boolIsValid = false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'is_attachment', __( 'Invoice does not have an attachment url.' ) ) );
		}
		return $boolIsValid;
	}

	private function valRetentionPercent() {
		$boolIsValid = true;

		if( CApHeaderSubType::STANDARD_JOB_INVOICE != $this->getApHeader()->getApHeaderSubTypeId() ) {
			return $boolIsValid;
		}

		if( true == valId( $this->getApHeader()->getBulkRetentionPercent() ) && false == valId( $this->getApHeader()->getBulkApContractId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_contract_id', __( 'Contract is required if retention percent is provided.' ) ) );
		}

		return $boolIsValid;
	}

	private function valRetentionAmount() {
		$boolIsValid = true;

		if( CApHeaderSubType::STANDARD_JOB_INVOICE != $this->getApHeader()->getApHeaderSubTypeId() ) {
			return $boolIsValid;
		}

		$fltTransactionAmount = 0.00;

		foreach( ( array ) $this->getApHeader()->getApDetails() as $objApDetail ) {

			if( $objApDetail->getPreApprovalAmount() > bcsub( $objApDetail->getTransactionAmount(), $objApDetail->getRetentionAmount(), 2 ) && 0 < $objApDetail->getTransactionAmount() ) {
				$boolIsValid &= false;
			}

			$fltTransactionAmount = bcadd( $fltTransactionAmount, bcsub( $objApDetail->getTransactionAmountDue(), $objApDetail->getRetentionAmount(), 2 ), 2 );
		}

		if( false == $boolIsValid ) {
			$strMesaage = ( bccomp( 0.00, $fltTransactionAmount, 2 ) ) ? __( ' As such you can only pay up to ${%m, 0} against this invoice.', [ $fltTransactionAmount ] ) : __( ' As such you can not pay any amount against this invoice. ' );
			$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'retention_amount', __( 'The retention has not been released for \'{%s, 0}\' invoice.{%s, 1}', [ $this->getApHeader()->getHeaderNumber(), $strMesaage ] ) ) );
		}

		return $boolIsValid;
	}

	private function valInvoiceTotalQuantity() {
		$boolIsValid = true;

		$arrobjRekeyedApDetails	= rekeyObjects( 'InventoryId', $this->getApHeader()->getApDetails() );
		$arrintInventoryIds		= array_filter( array_unique( array_keys( $arrobjRekeyedApDetails ) ) );

		if( false == valArr( $arrintInventoryIds ) ) {
			return $boolIsValid;
		}

		$arrobjAssetTransactions = ( array ) \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchActiveAssetTransactionsByIdsByCid( $arrintInventoryIds, $this->getApHeader()->getCid(), $this->getClientDatabase() );

		foreach( $arrobjRekeyedApDetails as $intInventoryId => $objApDetail ) {

			$objAssetTransaction = getArrayElementByKey( $intInventoryId, $arrobjAssetTransactions );

			if( false == valObj( $objAssetTransaction, 'CAssetTransaction' ) ) {
				continue;
			}

			if( false == valId( $this->getApHeader()->getApBatchId() ) && true == valId( $objAssetTransaction->getApDetailId() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'quantity', __( 'Invoice is already created for selected purchase order.' ) ) );
				break;

			} elseif( $objApDetail->getQuantityOrdered() > $objAssetTransaction->getQuantity() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'quantity', __( 'Invoice \'{%s, 0}\' quantity should be less than or equal to the received quantity.', [ $this->getApHeader()->getHeaderNumber() ] ) ) );
				break;
			}
		}

		return $boolIsValid;
	}

	private function valApCodePropertyAssociation() {
		$boolIsValid = true;

		$arrintApCodeIds = array_filter( array_keys( rekeyObjects( 'ApCodeId', $this->getApHeader()->getApDetails() ) ) );

		if( false == valArr( $arrintApCodeIds ) || CApHeaderSubType::CATALOG_INVOICE != $this->getApHeader()->getApHeaderSubTypeId() ) return $boolIsValid;

		$this->getOrFetchProperties();
		$this->getOrFetchApCodeProperties();

		foreach( $this->getApHeader()->getApDetails() as $objApDetail ) {
			if( false == valId( $objApDetail->getApCodeId() ) ) continue;

			$arrintApCodeProperties = ( array ) getArrayElementByKey( $objApDetail->getApCodeId(), $this->m_arrintApCodeProperties );
			$objProperty = getArrayElementByKey( $objApDetail->getPropertyId(), $this->m_arrobjProperties );

			if( false == in_array( $objApDetail->getPropertyId(), $arrintApCodeProperties ) && true == valStr( $objApDetail->getItemName() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( CErrorMsg::create( CErrorMsg::ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Catalog item "{%s, 0}" is no longer associated to Property "{%s, 1}".', [ $objApDetail->getItemName(), $objProperty->getPropertyName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	private function valParentPropertyPostMonth() {

		$boolIsValid        = true;
		$boolIsIntercompany = $this->getValidationDataByKey( 'is_intercompany_invoice' );

		if( !valArr( $this->getApHeader()->getApDetails() ) ) {
			return $boolIsValid;
		}

		if( !$boolIsIntercompany ) {
			$this->getOrFetchBankAccounts();

			$boolIsIntercompany = \CIntercompanyInvoiceHelper::setIntercompanyPropertiesToApHeader( $this->getApHeader(), $this->m_arrobjBankAccounts, false );
		}

		$strPostMonth   = $this->getApHeader()->getPostMonth();

		$arrintParentPropertyIds = [];

		if( $boolIsIntercompany ) {

			$arrintParentPropertyIds    = ( array ) array_keys( rekeyObjects( 'InterCoPropertyId', $this->getApHeader()->getApDetails() ) );

			if( !valArr( $arrintParentPropertyIds ) ) {
				$arrintBankAccountIds       = ( array ) array_keys( rekeyObjects( 'BankAccountId', $this->getApHeader()->getApDetails() ) );

				if( valIntArr( $arrintBankAccountIds ) ) {
					$arrobjPropertyBankAccounts = ( array ) \Psi\Eos\Entrata\CPropertyBankAccounts::createService()->fetchPropertyBankAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $this->getApHeader()->getCid(), $this->getClientDatabase(), true, true );

					if( valArr( $arrobjPropertyBankAccounts ) ) {
						$arrintParentPropertyIds = ( array ) array_keys( rekeyObjects( 'PropertyId', $arrobjPropertyBankAccounts ) );
					}
				}
			}
		}

		$arrobjPropertyGlSetting = rekeyObjects( 'PropertyId', ( array ) \Psi\Eos\Entrata\CPropertyGlSettings::createService()->fetchPropertyGlSettingsByPropertyIdsByCid( $arrintParentPropertyIds, $this->getApHeader()->getCid(), $this->getClientDatabase() ) );

		foreach( $arrobjPropertyGlSetting as $intParentPropertyId => $objPropertyGlSetting ) {

			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
			 || ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && 1 == $this->getApHeader()->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() ) ) {
				$boolIsValid &= true;
			}

			if( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) || strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) {
				$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intParentPropertyId, $this->getApHeader()->getCid(), $this->getClientDatabase() );
				$this->addErrorMsg( new \Psi\Libraries\UtilErrorMsg\CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'GL or AP is locked for parent property \'{%s, 0}\' of invoice number {%s, 1}.', [ $objProperty->getPropertyName(), $this->getApHeader()->getHeaderNumber() ] ) ) );

				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	// Get or fetch Functions

	private function getOrFetchApPayee() {

		if( false == valObj( $this->m_objApPayee, 'CApPayee' ) ) {
			$this->m_objApPayee = CApPayees::fetchSimpleApPayeeByIdByCid( $this->getApHeader()->getApPayeeId(), $this->getApHeader()->getCid(), $this->getClientDatabase() );
		}

		return $this->m_objApPayee;
	}

	private function getOrFetchApPayeeComplianceStatuses() {
		$this->getOrFetchApPayeeLocation();

		$arrobjApDetails = ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		if( false == valArr( $this->m_arrobjApPayeeComplianceStatuses ) && true == valObj( $this->m_objApPayeeLocation, 'CApPayeeLocation' ) ) {
			$this->m_arrobjApPayeeComplianceStatuses = \Psi\Eos\Entrata\CApPayeeComplianceStatuses::createService()->fetchApPayeeComplianceStatusesByPropertyIdsByApLegalEntityIdsByApPayeeIdsByCid( array_keys( $arrobjApDetails ), [ $this->m_objApPayeeLocation->getApLegalEntityId() ], [ $this->getApHeader()->getApPayeeId() ], $this->getApHeader()->getCid(), $this->getClientDatabase() );
			$this->m_arrobjApPayeeComplianceStatuses = rekeyObjects( 'PropertyId', $this->m_arrobjApPayeeComplianceStatuses );
		}

		return $this->m_arrobjApPayeeComplianceStatuses;
	}

	private function getOrFetchApPayeeLocation() {

		if( false == valObj( $this->m_objApPayeeLocation, 'CApPayeeLocation' ) ) {
			$this->m_objApPayeeLocation = CApPayeeLocations::fetchApPayeeLocationByIdByCid( $this->getApHeader()->getApPayeeLocationId(), $this->getApHeader()->getCid(), $this->getClientDatabase() );
		}

		return $this->m_objApPayeeLocation;
	}

	private function getOrFetchRoutingTagCompanyPreferences() {

		if( false == valArr( $this->m_arrobjRoutingTagCompanyPreferences ) ) {
			$this->m_arrobjRoutingTagCompanyPreferences = ( array ) \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( [ 'use_routing_tags', 'routing_tags_required_invoice' ], $this->getApHeader()->getcid(), $this->getClientDatabase() );
			$this->m_arrobjRoutingTagCompanyPreferences = rekeyobjects( 'key', $this->m_arrobjRoutingTagCompanyPreferences );
		}

		return $this->m_arrobjRoutingTagCompanyPreferences;
	}

	private function getOrFetchProperties() {
		$arrobjApDetails = ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		if( false == valArr( $this->m_arrobjProperties ) && true == valArr( $arrobjApDetails ) ) {
			$this->m_arrobjProperties	= ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByCid( array_keys( $arrobjApDetails ), $this->getApHeader()->getCid(), $this->getClientDatabase() );
		}

		return $this->m_arrobjProperties;
	}

	private function getOrFetchPropertyGlSettings() {
		$arrobjApDetails = ( array ) rekeyObjects( 'PropertyId', $this->getApHeader()->getApDetails() );

		if( false == valArr( $this->m_arrobjPropertyGlSettings ) && true == valArr( $arrobjApDetails ) ) {
			$this->m_arrobjPropertyGlSettings	= ( array ) CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( array_keys( $arrobjApDetails ), $this->getApHeader()->getCid(), $this->getClientDatabase() );
			$this->m_arrobjPropertyGlSettings	= ( array ) rekeyObjects( 'PropertyId', $this->m_arrobjPropertyGlSettings );
		}

		return $this->m_arrobjPropertyGlSettings;
	}

	private function getOrFetchApPayeePropertyGroups() {

		if( false == valArr( $this->m_arrobjApPayeePropertyGroups ) ) {
			$this->m_arrobjApPayeePropertyGroups	= ( array ) CApPayeePropertyGroups::fetchCustomApPayeePropertyGroupsByApPayeeLocationIdByCid( $this->getApHeader()->getApPayeeLocationId(), $this->getApHeader()->getCid(), $this->getClientDatabase() );
			$this->m_arrobjApPayeePropertyGroups	= ( array ) rekeyObjects( 'PropertyId', $this->m_arrobjApPayeePropertyGroups );
		}

		return $this->m_arrobjApPayeePropertyGroups;
	}

	private function getOrFetchBankAccountProperties() {

		if( false == valArr( $this->m_arrintBankAccountProperties ) ) {
			$arrintBankAccountIds		= ( array ) array_keys( rekeyObjects( 'BankAccountId', $this->getApHeader()->getApDetails() ) );
			$arrobjPropertyBankAccounts	= ( array ) CPropertyBankAccounts::fetchPropertyBankAccountsByBankAccountIdsByCid( array_filter( $arrintBankAccountIds ), $this->getApHeader()->getCid(), $this->getClientDatabase(), true );

			foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
				$this->m_arrintBankAccountProperties[$objPropertyBankAccount->getBankAccountId()][$objPropertyBankAccount->getPropertyId()] = $objPropertyBankAccount->getPropertyId();
			}
		}

		return $this->m_arrintBankAccountProperties;
	}

	private function getOrFetchApCodeProperties() {

		if( false == valArr( $this->m_arrintApCodeProperties ) ) {
			$arrintApCodeIds		= array_filter( array_keys( rekeyObjects( 'ApCodeId', $this->getApHeader()->getApDetails() ) ) );
			$arrintApCatalogItems	= ( array ) \Psi\Eos\Entrata\CApCatalogItems::createService()->fetchApCatalogItemsWithPropertyGroupAssociationsByApCodeIdsByCid( $arrintApCodeIds, $this->getApHeader()->getCid(), $this->getClientDatabase() );

			foreach( $arrintApCatalogItems as $arrintApCatalogItem ) {
				$this->m_arrintApCodeProperties[$arrintApCatalogItem['ap_code_id']][$arrintApCatalogItem['property_id']] = $arrintApCatalogItem['property_id'];
			}
		}

		return $this->m_arrintApCodeProperties;
	}

	private function getOrFetchBankAccounts() {
		$arrobjApDetails = ( array ) rekeyObjects( 'BankAccountId', $this->getApHeader()->getApDetails() );

		if( false == valArr( $this->m_arrobjProperties ) && true == valArr( $arrobjApDetails ) ) {
			$this->m_arrobjBankAccounts   = ( array ) \Psi\Eos\Entrata\CBankAccounts::createService()->fetchBankAccountsByIdsByCid( array_keys( $arrobjApDetails ), $this->getApHeader()->getCid(), $this->getClientDatabase() );
		}

		return $this->m_arrobjBankAccounts;
	}

}
?>
