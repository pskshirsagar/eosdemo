<?php

define( 'EXP_MONTH_MIN', 0 );
define( 'EXP_MONTH_MAX', 12 );

 class CScheduledPaymentValidator {

 	/** @var CScheduledPayment */
	protected $m_objScheduledPayment;

	public function setScheduledPayment( $objScheduledPayment ) {
		$this->m_objScheduledPayment = $objScheduledPayment;
	}

 	// Verify that payment (if variable) is posting within 10 days of the date it is supposed to post.
 	// The reason for this is because if a person sets up a recurring payment and has a balance of $500, but
 	// they pay the $500 with check (so their balance is zero), the system will try all month to post the payment.
 	// Then when the next months charges are posted on the 25th, the payment gets posted on the 25th for the future months charges.
 	// This really makes people unhappy!

 	public function validateVariablePaymentBouncePosting( $arrstrScheduledPaymentTransactionStartDate ) {
 		// What is the last posted on day?
 		// Make sure 6 days haven't passed since the day.
 		// For example, if payment is supposed to post on the 1st, payment should try to post until the 6th.

 		$intProcessDay = $this->m_objScheduledPayment->getProcessDay();

 		if( false == is_null( $intProcessDay ) ) {
		    if( CScheduledPayment::LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
			    $intPostOnDay = ( int ) date( 't' );
		    } elseif( CScheduledPayment::SECOND_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
			    $intPostOnDay = ( int ) date( 't' ) - 1;
		    } elseif( CScheduledPayment::THIRD_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
			    $intPostOnDay = ( int ) date( 't' ) - 2;
		    } else {
			    $intPostOnDay = $intProcessDay;
		    }
	    } else {
		    $intPostOnDay = ( int ) date( 'j', strtotime( $this->m_objScheduledPayment->getPaymentStartDate() ) );
		    if( CScheduledPayment::LAST_DAY_OF_THE_MONTH == $intPostOnDay ) {
			    $intPostOnDay = ( int ) date( 't' );
		    } elseif( CScheduledPayment::SECOND_LAST_DAY_OF_THE_MONTH == $intPostOnDay ) {
			    $intPostOnDay = ( int ) date( 't' ) - 1;
		    } elseif( CScheduledPayment::THIRD_LAST_DAY_OF_THE_MONTH == $intPostOnDay ) {
			    $intPostOnDay = ( int ) date( 't' ) - 2;
		    }
	    }

		$intTotalDaysinMonth = ( int ) date( 't' ) + 1;

	    if( true == valArr( $arrstrScheduledPaymentTransactionStartDate ) && true == valStr( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) ) {
 			$intTotalDaysinMonth = ( int ) date( 't', strtotime( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) ) + 1;

		    $intScheduledPaymentTransactionMonth = ( int ) date( 'm', strtotime( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) );

 			$intCurrentMonth = ( int ) date( 'm' );

 			// if current month and scheduled payment transaction month are different we need to set the value of $intPostOnDay in scheduled payment transaction month
 			if( $intCurrentMonth != $intScheduledPaymentTransactionMonth && \CScheduledPayment::LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
	        	$intPostOnDay = ( int ) date( 't', strtotime( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) );
	        } elseif( $intCurrentMonth != $intScheduledPaymentTransactionMonth && \CScheduledPayment::SECOND_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
			    $intPostOnDay = ( int ) date( 't', strtotime( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) ) - 1;
		    } elseif( $intCurrentMonth != $intScheduledPaymentTransactionMonth && \CScheduledPayment::THIRD_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
			    $intPostOnDay = ( int ) date( 't', strtotime( $arrstrScheduledPaymentTransactionStartDate[$this->m_objScheduledPayment->getCid()][$this->m_objScheduledPayment->getId()] ) ) - 2;
		    }
	    }

	    $arrintApprovedPostOnDays = [];

 		// Create an array of approved post days.
 		for( $intX = 0; $intX < 6; $intX++ ) {
 			$arrintApprovedPostOnDays[] = $intPostOnDay;
 			$intPostOnDay++;
 			if( $intTotalDaysinMonth == $intPostOnDay ) {
 				$intPostOnDay = 1;
 			}
 		}

	    $intToday = date( 'j' );

	    if( true == in_array( $intToday, $arrintApprovedPostOnDays ) ) {

 			$intApprovedPostOnLastDay = array_pop( $arrintApprovedPostOnDays );
 			if( $intToday == $intApprovedPostOnLastDay ) {
 				$this->m_objScheduledPayment->setIsApprovedPostOnLastDay( true );
 			} else {
 				$this->m_objScheduledPayment->setIsApprovedPostOnLastDay( false );
 			}

 			return true;
 		}

 		return false;
 	}

 	public function valCid() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getCid() ) || 0 >= ( int ) $this->m_objScheduledPayment->getCid() ) {
 			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Payment Request:  Client id required - CScheduledPayment::valCid()', E_USER_ERROR );
 		}

 		return $boolIsValid;
 	}

 	public function valHasAmountExeceededMerchantlimit() {
 		$boolIsValid = true;

		if( true == $this->m_objScheduledPayment->getHasAmountExeceededMerchantLimit() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Amount can not exceed maximum payment amount allowed on this merchant account. Please enter $' . $this->m_objScheduledPayment->getMaximumMerchantAmount() . ' or less.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valLeaseId() {
 		$boolIsValid = true;

 		if( false == $this->m_objScheduledPayment->getIsFloating() && ( true == is_null( $this->m_objScheduledPayment->getLeaseId() ) || 0 >= ( int ) $this->m_objScheduledPayment->getLeaseId() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'lease_id', 'Lease is required.', NULL ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCustomerId() {
 		$boolIsValid = true;

 		if( false == $this->m_objScheduledPayment->getIsFloating() && ( true == is_null( $this->m_objScheduledPayment->getCustomerId() ) || 0 >= ( int ) $this->m_objScheduledPayment->getCustomerId() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'customer_id', 'Resident is required.', NULL ) );
 		}

 		return $boolIsValid;
 	}

 	public function valPaymentTypeId( $objClientDatabase = NULL ) {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getPaymentTypeId() ) || 0 >= ( int ) $this->m_objScheduledPayment->getPaymentTypeId() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Please select a payment method' ) );

 		} elseif( false == CPaymentTypes::isElectronicPayment( $this->m_objScheduledPayment->getPaymentTypeId() ) ) {
 			$boolIsValid = false;
 			trigger_error( 'Invalid Company Lease Payment Request:  Only electronic recurring payment types are acceptable - CScheduledPayment::valPaymentTypeId()', E_USER_ERROR );
 		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) && true == valObj( $this->m_objScheduledPayment->getMerchantAccount(), 'CMerchantAccount' ) ) {

 			$arrobjPaymentTypes = CMerchantAccounts::fetchRecurringPaymentTypesByMerchantAccountIdByCid( $this->m_objScheduledPayment->getMerchantAccount()->getCid(), $this->m_objScheduledPayment->getMerchantAccount()->getId(), $objClientDatabase );
			if( false == in_array( $this->m_objScheduledPayment->getPaymentTypeId(), array_column( $arrobjPaymentTypes, 'payment_type_id' ) ) ) {
				$this->m_objScheduledPayment->addErrorMsg( new \CErrorMsg( NULL, 'payment_type_id', 'Please select a different payment type. The selected payment type is not allowed for scheduled payments.' ) );
				$boolIsValid = false;
			}
	    }

 		return $boolIsValid;
 	}

 	public function valCreditCardNumber() {
 		$boolIsValid = true;

 		$arrintCreditCardPaymentTypes = CPaymentType::$c_arrintCreditCardPaymentTypes;
 		if( true == is_null( $this->m_objScheduledPayment->getCcCardNumber() ) && true == in_array( $this->m_objScheduledPayment->getPaymentTypeId(), $arrintCreditCardPaymentTypes ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Card number is required.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valPaymentAmount() {
 		$boolIsValid = true;

 		if( false == is_null( $this->m_objScheduledPayment->getPaymentAmount() ) && 0 >= ( float ) $this->m_objScheduledPayment->getPaymentAmount() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount is required.' ) ) );
 			return $boolIsValid;
 		}

 		if( ( float ) $this->m_objScheduledPayment->getPaymentAmount() != ( float ) $this->m_objScheduledPayment->getPaymentAmountConfirmation() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Confirmation payment amount does not match.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valPaymentCeilingAmount() {
 		$boolIsValid = true;

 		if( 0 >= ( float ) $this->m_objScheduledPayment->getPaymentCeilingAmount() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_ceiling_amount', __( 'A positive payment ceiling amount is required.' ) ) );
 		}

 		if( $this->m_objScheduledPayment->getPaymentCeilingAmount() != $this->m_objScheduledPayment->getPaymentCeilingAmountConfirmation() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_ceiling_amount', __( 'Confirmation payment ceiling amount does not match.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valEstimatedCeilingAmount() {
 		$boolIsValid = true;

 		if( 0 >= ( float ) $this->m_objScheduledPayment->getEstimatedPaymentAmount() ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_payment_amount', __( 'A positive estimated payment amount is required.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valPaymentStartDate( $boolIsSplitAdjustSetup = false ) {

 		$boolIsValid = true;

 		// We now will REQUIRE a start date.
 		if( 1 !== CValidation::checkDate( $this->m_objScheduledPayment->getPaymentStartDate() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_start_date', __( 'Payment start date is required.' ) ) );
 			return false;
 		}

 		// Make sure that the start date is after the day it was created
 		if( ( true == is_null( $this->m_objScheduledPayment->getId() ) || 0 >= ( int ) $this->m_objScheduledPayment->getId() ) && ( strtotime( date( 'Y-m-d' ) ) + 86400 ) > strtotime( $this->m_objScheduledPayment->getPaymentStartDate() ) ) {

				$boolIsValid = false;
				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_start_date', __( 'Payment Start Date must be after today.' ) ) );

 		} elseif( true == $boolIsSplitAdjustSetup && ( strtotime( \Psi\CStringService::singleton()->substr( $this->m_objScheduledPayment->getCreatedOn(), 0, 10 ) ) + 86400 ) > strtotime( $this->m_objScheduledPayment->getPaymentStartDate() ) ) {
 			// Check against the created on date
			$boolIsValid = false;
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_start_date', __( 'Payment Start Date must be after the day the recurring payment was created.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valPaymentEndDate() {
 		$boolIsValid = true;

 		// Ensure that if end date is set, that it is a valid date.
 		if( false == is_null( $this->m_objScheduledPayment->getPaymentEndDate() ) && 1 !== CValidation::checkDate( $this->m_objScheduledPayment->getPaymentEndDate() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', __( 'Payment End Date must be in mm/dd/yyyy form' ) ) );
 		}

 		// Ensure that start date is not after end date
 		if( false == is_null( $this->m_objScheduledPayment->getPaymentStartDate() ) && CScheduledPaymentFrequency::ONCE != $this->m_objScheduledPayment->getScheduledPaymentFrequencyId() && CScheduledPaymentType::ONETIME != $this->m_objScheduledPayment->getScheduledPaymentTypeId() ) {
 			// Ensure that start date is not after end date
 			if( false == is_null( $this->m_objScheduledPayment->getLeaseEndDate() ) && strtotime( $this->m_objScheduledPayment->getPaymentStartDate() ) >= strtotime( $this->m_objScheduledPayment->getLeaseEndDate() ) ) {
 				$boolIsValid = false;
 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', 'Lease End Date ( ' . $this->m_objScheduledPayment->getLeaseEndDate() . ' ) must be after Payment Start Date.' ) );
 			} elseif( false == is_null( $this->m_objScheduledPayment->getPaymentEndDate() ) && strtotime( $this->m_objScheduledPayment->getPaymentStartDate() ) >= strtotime( $this->m_objScheduledPayment->getPaymentEndDate() ) ) {
 				$boolIsValid = false;
 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', __( 'Payment End Date must be after Payment Start Date.' ) ) );
 			}
 		}

 		// Ensure that end date is not after cc expire date
 		if( true == in_array( $this->m_objScheduledPayment->getPaymentTypeId(), [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ] ) && true == valStr( $this->m_objScheduledPayment->getCcExpDateYear() ) && true == $this->m_objScheduledPayment->getCcExpDateMonth() ) {

 			$strDate = mktime( 0, 0, 0, $this->m_objScheduledPayment->getCcExpDateMonth(), 1, ( int ) $this->m_objScheduledPayment->getCcExpDateYear() );
 			$strDateCCExpiryNextMonth = date( 'm/d/Y', strtotime( '+1 month', $strDate ) );

 			if( false == is_null( $this->m_objScheduledPayment->getPaymentEndDate() ) && strtotime( $this->m_objScheduledPayment->getPaymentEndDate() ) >= strtotime( $strDateCCExpiryNextMonth ) ) {
 				if( 1 == $this->m_objScheduledPayment->getSplitbetweenRoommates() && false == is_null( $this->m_objScheduledPayment->getCcExpDateMonth() ) && false == is_null( $this->m_objScheduledPayment->getCcExpDateYear() ) ) {
 					$boolIsValid = false;
 					$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', __( 'The card must be valid through the selected end date. Please select an end date before the card is expired.' ) ) );
 				} elseif( 0 == $this->m_objScheduledPayment->getSplitbetweenRoommates() ) {
 					$boolIsValid = false;
 					$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', __( 'The card must be valid through the selected end date. Please select an end date before the card is expired.' ) ) );
 				}
 			}
 		}

 		if( false == $boolIsValid ) return false;

 		// Get the year and make sure it is not after 2069
 		$intEndYear = ( int ) \Psi\CStringService::singleton()->substr( $this->m_objScheduledPayment->getPaymentEndDate(), -4, 4 );
 		if( 2069 < $intEndYear ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_end_date', __( 'Payment End Date must be before the year 2070' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoUnitNumber() {
 		return true;
 	}

 	public function valBilltoNameFirst() {
 		$boolIsValid = true;

 		If( true == is_null( $this->m_objScheduledPayment->getBilltoNameFirst() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_name_first', __( 'Resident first name is required.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoNameLast() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoNameLast() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_name_last', __( 'Resident last name is required.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoStreetLine1() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoStreetLine1() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', __( 'Billing street address is required.' ), 612 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoCity() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoCity() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_city', __( 'City is required.' ), 613 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoStateCode() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoStateCode() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', __( 'State is required.' ), 614 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoPostalCode() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoPostalCode() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', __( 'Zip code is required.' ), 615 ) );
 		}

 		if( true == $boolIsValid && false == CValidation::validatePostalCode( $this->m_objScheduledPayment->getBilltoPostalCode() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', __( 'Zip code must be 5 digit long.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoPhoneNumber() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoPhoneNumber() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', __( 'Phone number is required.' ) ) );
 		}

 		if( false == is_null( $this->m_objScheduledPayment->getBilltoPhoneNumber() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->m_objScheduledPayment->getBilltoPhoneNumber(), true ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', __( 'Phone number must be 10 digits long.' ), 611 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoEmailAddress( $boolIsEmailAddressRequired = true ) {
 		$boolIsValid = true;

 		if( false == $boolIsEmailAddressRequired && true == is_null( $this->m_objScheduledPayment->getBilltoEmailAddress() ) ) {
 			return true;
 		}

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoEmailAddress() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_email_address', __( 'Email address is required.' ) ) );
 		}

 		if( false == is_null( $this->m_objScheduledPayment->getBilltoEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objScheduledPayment->getBilltoEmailAddress() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_email_address', __( 'Email Address does not appear to be valid.' ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valSecureReferenceNumber() {
 		$boolIsValid = true;

 		if( false == is_numeric( $this->m_objScheduledPayment->getSecureReferenceNumber() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Secure reference number is not valid.', 621 ) ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCcExpDateMonth() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getCcExpDateMonth() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Card expiration month is required.' ), 607 ) );
 		}

 		if( false == is_null( $this->m_objScheduledPayment->getCcExpDateMonth() ) && false == is_null( $this->m_objScheduledPayment->getCcExpDateYear() ) && $this->m_objScheduledPayment->getCcExpDateYear() == date( 'Y' ) && $this->m_objScheduledPayment->getCcExpDateMonth() < date( 'm' ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Expiration date is invalid or expired.' ), 607 ) );
 		}

        if( ( EXP_MONTH_MIN >= $this->m_objScheduledPayment->getCcExpDateMonth() || EXP_MONTH_MAX < $this->m_objScheduledPayment->getCcExpDateMonth() ) ) {
            $boolIsValid = false;
            $this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Expiration date is invalid or expired.' ), 607 ) );
        }

 		return $boolIsValid;
 	}

 	public function valCcExpDateYear() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getCcExpDateYear() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', __( 'Card expiration year is required.' ), 608 ) );
 		}

 		if( false == is_null( $this->m_objScheduledPayment->getCcExpDateYear() ) && $this->m_objScheduledPayment->getCcExpDateYear() < date( 'Y' ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', __( 'Expiration year is invalid or expired.' ), 607 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCcNameOnCard() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getCcNameOnCard() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_name_on_card', __( 'Name on card is required.' ), 609 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCcCardNumber() {
 		$boolIsValid = true;
 		$strCardNumber = $this->m_objScheduledPayment->getCcCardNumber();

 		if( true == is_null( $strCardNumber ) || 0 == strlen( $strCardNumber ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Credit card number is required.' ), 606 ) );
 			return false;
 		}

 		return $boolIsValid;
 	}

 	public function valCheckBankName() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getCheckBankName() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', __( 'Bank name is required.' ), 601 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCheckAccountTypeId() {
 		$boolIsValid = true;

 		if( false == is_numeric( $this->m_objScheduledPayment->getCheckAccountTypeId() ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', __( 'Bank account type is required.' ), 603 ) );
 		}

 		return $boolIsValid;
 	}

 	public function valCheckAccountNumber() {
		$boolIsValid = true;
 		$intCheckAccountNumber = $this->m_objScheduledPayment->getCheckAccountNumber();
 		if( true == empty( $intCheckAccountNumber ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Please enter the account number.' ), 602 ) );
 		}

 		if( 0 !== preg_match( '/[^a-zA-Z0-9\- ]/', $this->m_objScheduledPayment->getCheckAccountNumber() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number must be contain only space, dash and digits.' ), 622 ) );
 			return false;
 		}

 		if( CPaymentType::PAD == $this->m_objScheduledPayment->getPaymentTypeId() && ( 7 > strlen( $this->m_objScheduledPayment->getCheckAccountNumber() ) || 12 < strlen( $this->m_objScheduledPayment->getCheckAccountNumber() ) ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number must be 7 to 12 digits long.', 622 ) );
		    return false;
	    }

 		return $boolIsValid;
 	}

	public function valBimonthlyBillDay() {
		$boolIsValid = true;

		if( 1 == $this->m_objScheduledPayment->getBiMonthlyAutoPayment() && true == valId( $this->m_objScheduledPayment->getBiMonthlyFirstBillDay() ) && true == valId( $this->m_objScheduledPayment->getBiMonthlySecondBillDay() ) && $this->m_objScheduledPayment->getBiMonthlyFirstBillDay() == $this->m_objScheduledPayment->getBiMonthlySecondBillDay() ) {
			$boolIsValid = false;
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bi_monthly_first_payment_amount', __( 'First Payment Day & Second Payment Day must be different.' ), 603 ) );
		}

		return $boolIsValid;
	}

 	public function valCheckRoutingNumber( $objClientDatabase = NULL ) {
 		$boolIsValid = true;

 		// * Validation example */
 		$intCheckRoutingNumber = $this->m_objScheduledPayment->getCheckRoutingNumber();
 		if( true == empty( $intCheckRoutingNumber ) ) {
 			$boolIsValid = false;
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Please enter the routing number.' ), 605 ) );
 		}

 		if( 0 !== preg_match( '/[^0-9]/', $this->m_objScheduledPayment->getCheckRoutingNumber() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number must be a number.' ), 622 ) );
 			return false;
 		}

 		if( 9 != strlen( $this->m_objScheduledPayment->getCheckRoutingNumber() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number must be 9 digits long.' ), 623 ) );
 			return false;
 		}

 		if( false == is_null( $objClientDatabase ) ) {
 			// If it is 9 numeric characters see if it is a fed check participant
 			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->m_objScheduledPayment->getCheckRoutingNumber(), $objClientDatabase );

 			if( false == valObj( $objFedAchParticipant, 'CFedAchParticipant' ) ) {
 				$boolIsValid = false;
 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number is not valid.' ), 624 ) );
 			} else {
 				$this->m_objScheduledPayment->setCheckBankName( $objFedAchParticipant->getCustomerName() );
 			}
 		}

 		return $boolIsValid;
 	}

 	public function valCheckNameOnAccount() {

		$boolIsValid = true;
 		if( true == is_null( $this->m_objScheduledPayment->getCheckNameOnAccount() ) ) {
 			$boolIsValid = false;
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( 'Please enter the account holder\'s name.' ), 604 ) );
		} elseif( true == valStr( $this->m_objScheduledPayment->getCheckNameOnAccount() ) && false == preg_match( '/^[\p{L}\s\-_`\',.0-9]+$/ui', $this->m_objScheduledPayment->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( 'Account holder\'s name is not valid.' ) ) );
		}

 		return $boolIsValid;
 	}

 	public function valBilltoIpAddress() {
 		$boolIsValid = true;

 		if( true == is_null( $this->m_objScheduledPayment->getBilltoIpAddress() ) ) {
 			$boolIsValid = false;
 			trigger_error( 'Invalid Company Lease Payment Request:  Bill To IP Address required - CScheduledPayment::valBilltoIpAddress()', E_USER_ERROR );
 		}

 		return $boolIsValid;
 	}

 	public function valAgreesToPayConvenienceFees() {
 		$boolIsValid = true;

 		if( 0 < $this->m_objScheduledPayment->getConvenienceFeeAmount() && false == $this->m_objScheduledPayment->getAgreesToPayConvenienceFees() && true != $this->m_objScheduledPayment->getFeeWaived() ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'agrees_to_fees', __( 'Agreeing to pay convenience fee is required.' ) ) );
 			$boolIsValid &= false;
 		}

 		return $boolIsValid;
 	}

 	public function valAgreesToTerms() {
 		$boolIsValid = true;

 		if( false == $this->m_objScheduledPayment->getAgreesToTerms() || true == is_null( $this->m_objScheduledPayment->getTermsAcceptedOn() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'agrees_to_terms', __( 'Agreeing to terms is required.' ) ) );
 			$boolIsValid &= false;
 		}

 		return $boolIsValid;
 	}

 	public function valAgreesToDuplicateAutoPayment() {
 		$boolIsValid = true;

 		if( true == $this->m_objScheduledPayment->getIsRecentArPayment() && false == $this->m_objScheduledPayment->getAgreesToDuplicateAutopayment() ) {
 			$strDateTimePaymentStartDate = date_create( $this->m_objScheduledPayment->getPaymentStartDate() );
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'is_recent_arpayment', ' Please confirm the auto payment will begin on ' . date_format( $strDateTimePaymentStartDate, 'F dS' ) . '. ' ) );
 			$boolIsValid &= false;
 		}
 		return $boolIsValid;
	}

	public function valDuplicateVariableRecurringPayment( $objClientDatabase ) {

		$boolIsValid = true;

		if( true == $this->m_objScheduledPayment->getIsVariableAmount() && 0 < CScheduledPayments::fetchVariableRecurringPaymentCountByLeaseIdByDateByCid( $this->m_objScheduledPayment->getLeaseId(), $this->m_objScheduledPayment->getPaymentStartDate(), $this->m_objScheduledPayment->getCid(), $objClientDatabase, $this->m_objScheduledPayment->getSplitBetweenRoommates() ) ) {
			if( 1 !== ( int ) $this->m_objScheduledPayment->getSpecifyPaymentCeiling()
				|| ( 1 == ( int ) $this->m_objScheduledPayment->getSpecifyPaymentCeiling() &&
					( 1 == CScheduledPayments::fetchVariableCeilingRecurringPaymentOfRoomateCountByCustomerIdByLeaseIdByDateByCid( $this->m_objScheduledPayment->getCustomerId(), $this->m_objScheduledPayment->getLeaseId(), $this->m_objScheduledPayment->getPaymentStartDate(), $this->m_objScheduledPayment->getCid(), $objClientDatabase )
						|| 1 == CScheduledPayments::fetchVariableRecurringPaymentOfRoomateCountByCustomerIdByLeaseIdByDateByCid( $this->m_objScheduledPayment->getCustomerId(), $this->m_objScheduledPayment->getLeaseId(), $this->m_objScheduledPayment->getPaymentStartDate(), $this->m_objScheduledPayment->getCid(), $objClientDatabase ) ) ) ) {
				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'is_duplicate_auto_payment', __( 'There is another auto payment for the selected billing day. Please change the billing day.' ) ) );
				$boolIsValid &= false;
			}
		}
		return $boolIsValid;
	}

 	public function valPaymentInformation( $objClientDatabase = NULL ) {
 		$boolIsValid = true;

 		switch( $this->m_objScheduledPayment->getPaymentTypeId() ) {
 			case CPaymentType::ACH:
			    $boolIsValid &= $this->valCheckAccountNumber();
			    $boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
			    $boolIsValid &= $this->valCheckNameOnAccount();
				if( 0 == $this->m_objScheduledPayment->getUseStoredBillingInfo() ) {
					$boolIsValid &= $this->valConfirmCheckAccountNumber();
				}
 				$boolIsValid &= $this->valCheckAccountTypeId();
 				$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
 				break;

			case CPaymentType::PAD:
				$boolIsValid &= $this->valCheckAccountNumber();
				if( false == valId( $this->m_objScheduledPayment->getId() ) && false == valId( $this->m_objScheduledPayment->getCustomerPaymentAccountId() ) ) {
					$boolIsValid &= $this->validateCheckBankNumber();
					$boolIsValid &= $this->validateCheckBankTransitNumber();
				}
				$boolIsValid &= $this->validatePadBankInfo( $objClientDatabase );
				break;

 				// It's a credit card payment.  validate the credit card info
 			case CPaymentType::VISA:
 			case CPaymentType::MASTERCARD:
 			case CPaymentType::AMEX:
 			case CPaymentType::DISCOVER:
 				$boolIsValid &= $this->valCcExpDateYear();
 				$boolIsValid &= $this->valCcExpDateMonth();
 				$boolIsValid &= $this->valCcNameOnCard();
 				break;

 			default:
 				// default case
 		}

 		return $boolIsValid;
 	}

 	public function valBilltoInformation( $boolIsEmailAddressRequired = true ) {
 		$boolIsValid = true;

 		// Vaidations for SEPA info are handled separately.
 		if( CPaymentType::SEPA_DIRECT_DEBIT == $this->m_objScheduledPayment->getPaymentTypeId() ) {
 			return $boolIsValid;
		}

 		// If it's an electronic payment, then check the billing information
 		if( false == is_null( $this->m_objScheduledPayment->getPaymentTypeId() ) && true == CPaymentTypes::isElectronicPayment( $this->m_objScheduledPayment->getPaymentTypeId() ) ) {

 			$boolIsValid &= $this->valBilltoEmailAddress( $boolIsEmailAddressRequired );
 			$boolIsValid &= $this->valBilltoPhoneNumber();
 			$boolIsValid &= $this->valBilltoStreetLine1();

 			if( 1 != $this->m_objScheduledPayment->getIsAllowInternationalCard() || CPaymentType::ACH == $this->m_objScheduledPayment->getPaymentTypeId() ) {
 				$boolIsValid &= $this->valBilltoCity();
 				$boolIsValid &= $this->valBilltoStateCode();
 				$boolIsValid &= $this->valBilltoPostalCode();
 			}
 		}

 		return $boolIsValid;
 	}

 	public function valCheckBlacklist( $objClientDatabase ) {
 		$boolIsValid = true;

 		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
 		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objClientDatabase, $this->m_objScheduledPayment->getCheckRoutingNumber(), $this->m_objScheduledPayment->getCheckAccountNumber(), $this->m_objScheduledPayment->getCheckAccountTypeId(), $this->m_objScheduledPayment->getCheckNameOnAccount() );

 		if( NULL == $objBlacklistedEntry ) {
 			return true;
 		}

 		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
 			case CPaymentBlacklistType::NONE:
 				return true;
 			case CPaymentBlacklistType::ROUTING_NUMBER:
 				// TODO: this is for debugging on production, when ready, remove this line and uncomment code lines below
 				// @mail( CSystemEmail::RJENSEN_EMAIL_ADDRESS, 'Payment Blacklist Validation', 'Found blacklist entry: ' . $objBlacklistedEntry->getId() . ', scheduled payment id: ' . $this->getId() . ', client id: ' . $this->getCid() . ', customer id: ' . $this->getCustomerId(), 'from: ' . PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

 				// swap out the routing number
 				$this->m_objScheduledPayment->setCheckRoutingNumber( $objBlacklistedEntry->getLookupStringCorrectedDecrypted() );

 				// notify the user that we swapped the routing number
 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
 				$boolIsValid = false;
 				break;

 			default:
 				// TODO: this is for debugging on production, when ready, remove this line and uncomment code lines below
 				// @mail( CSystemEmail::RJENSEN_EMAIL_ADDRESS, 'Payment Blacklist Validation', 'Found blacklist entry: ' . $objBlacklistedEntry->getId() . ', scheduled payment id: ' . $this->getId() . ', client id: ' . $this->getCid() . ', customer id: ' . $this->getCustomerId(), 'from: ' . PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
 				$boolIsValid = false;
 				break;
 		}

 		return $boolIsValid;
 	}

 	public function valScheduledPaymentDuplicity( $objClientDatabase ) {
 		$boolIsValid = true;

 		if( true == $this->m_objScheduledPayment->getIsFloating() ) {
 			return $boolIsValid;
 		}

 		$objCompetingScheduledPayment = CScheduledPayments::fetchScheduledPaymentByCompetingScheduledPayment( $this->m_objScheduledPayment, $objClientDatabase );

 		if( false == is_null( $this->m_objScheduledPayment->getId() ) && true == is_numeric( $this->m_objScheduledPayment->getId() ) ) {
 			if( true == valObj( $objCompetingScheduledPayment, 'CScheduledPayment' ) ) {
 				if( $this->m_objScheduledPayment->getId() !== $objCompetingScheduledPayment->getId() ) {
 					$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'A single lease may not contain more than one recurring payment at the same time.' ) ) );
 					$boolIsValid &= false;
 				}
 			}

 		} elseif( true == valObj( $objCompetingScheduledPayment, 'CScheduledPayment' ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'A single lease may not contain more than one recurring payment at the same time.' ) ) );
 			$boolIsValid &= false;
 		}

 		return $boolIsValid;
 	}

 	public function valFloatingVariableConflict() {
 		$boolIsValid = true;

 		if( true == $this->m_objScheduledPayment->getIsVariableAmount() && true == is_null( $this->m_objScheduledPayment->getCustomerId() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'A floating payment cannot be variable.' ) ) );
 			$boolIsValid &= false;
 		}

 		return $boolIsValid;
 	}

 	public function valMerchantAccount( $objClientDatabase ) {
 		$boolIsValid = true;

 		$objProperty = $this->m_objScheduledPayment->getOrFetchProperty( $objClientDatabase );

 		if( true == valObj( $objProperty, 'CProperty' ) ) {

 			$objMerchantAccount = CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->m_objScheduledPayment->getCid(), $this->m_objScheduledPayment->getPropertyId(), $objClientDatabase, CPaymentMedium::RECURRING );

 			if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
 				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, $objProperty->getPropertyName() . ' is not configured to allow recurring payments.  First associate a merchant account.' ) );
 				$boolIsValid &= false;
 			}
 		}

 		return $boolIsValid;
 	}

 	public function valConfirmCheckAccountNumber() {
 		$boolIsValid = true;

	    if( false == valStr( $this->m_objScheduledPayment->getCheckAccountNumberEncrypted() ) ) {
		    return false;
	    }

	    $strCcCardNumber  = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_objScheduledPayment->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );

 		if( 0 != strlen( $strCcCardNumber ) && 0 == strlen( $this->m_objScheduledPayment->getConfirmCheckAccountNumber() ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confrim_check_account_number', __( 'Confirm account number is required.' ), 602 ) );
 			$boolIsValid = false;
 		} elseif( 0 != strlen( $strCcCardNumber ) && $strCcCardNumber != $this->m_objScheduledPayment->getConfirmCheckAccountNumber() ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verify_check_account_number', __( 'Confirm account number and account number do not match.' ) ) );
 			$boolIsValid = false;
 		}

 		return $boolIsValid;
 	}

 	public function validatePaymentSettings( $objClientDatabase ) {
 		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
 			trigger_error( 'Invalid database object in CScheduledPayment::validatePaymentSettings()', E_USER_ERROR );
 			return false;
 		}

		$arrobjPropertyPreferences	= ( array ) CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_objScheduledPayment->getPropertyId() ], [ 'DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS', 'PAST_RESIDENT_LOGIN_TOLERANCE_DAYS', 'REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS', 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION' ],  $this->m_objScheduledPayment->getCid(), $objClientDatabase );

 		$arrobjPropertyPreferences 	= rekeyObjects( 'key', $arrobjPropertyPreferences );
 		$objProperty				= $this->m_objScheduledPayment->getOrFetchProperty( $objClientDatabase );
 		$objPropertyPhoneNumber		= $objProperty->getOrFetchPrimaryPropertyPhoneNumber( $objClientDatabase );

 		$strPhoneNumberAddition 	= ( true == valObj( $objPropertyPhoneNumber, 'CPropertyPhoneNumber' ) && false == is_null( $objPropertyPhoneNumber->getPhoneNumber() ) ) ? ' at ' . $objPropertyPhoneNumber->getPhoneNumber() : '';

 		// DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS

 		$objLease = $this->m_objScheduledPayment->getOrFetchLease( $objClientDatabase );

		if( false == valObj( $objLease, 'CLease' ) ) {
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'lease_id', __( 'Auto payment unable to be created at this time. Please try again.' ), NULL ) );
			return false;
		}

		$objLeaseCustomer = $this->m_objScheduledPayment->fetchLeaseCustomer( $objClientDatabase );
 		$strLeaseEndDate = NULL;
 		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) && true == valArr( $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS'], 'CPropertyPreference' ) && 1 == $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS']->getvalue() ) {
 			$strLeaseEndDate = $objLease->getMoveOutDate();
 		}

 		if( NULL != $strLeaseEndDate && 0 < strlen( $strLeaseEndDate ) && ( date( 'Y' ) > \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) || ( date( 'Y' ) == \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) && date( 'm' ) >= \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) ) ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot make recurring payments at this time because either your lease ends in the middle of the current month, or your lease has expired. Please contact the leasing office for assistance' . $strPhoneNumberAddition . '.' ) );
 			return false;
 		}

 		if( ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION', $arrobjPropertyPreferences ) ) && NULL != $objLease->getCollectionsStartDate() && strtotime( $objLease->getCollectionsStartDate() ) <= strtotime( date( 'm/d/Y' ) ) ) {
 			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'lease_id', __( 'This lease has been sent to collections and payments are no longer allowed. Please contact your property office.' ), NULL ) );
 			return false;
 		}

 		// PAST_RESIDENT_LOGIN_TOLERANCE_DAYS
		$intPastResidentToleranceDays = ( true == isset( $arrobjPropertyPreferences['PAST_RESIDENT_LOGIN_TOLERANCE_DAYS'] ) && false == is_null( $arrobjPropertyPreferences['PAST_RESIDENT_LOGIN_TOLERANCE_DAYS']->getValue() ) ) ? $arrobjPropertyPreferences['PAST_RESIDENT_LOGIN_TOLERANCE_DAYS']->getValue() : CPropertyPreference::PAST_RESIDENT_LOGIN_TOLERANCE_DAYS;
		$boolPastResidentValidationCleared = true;

 		if( CLeaseStatusType::PAST == $objLease->getLeaseStatusTypeId() ) {
			$boolPastResidentValidationCleared = ( 0 < $intPastResidentToleranceDays ) ? $objLease->checkToleranceDaysWindow( $intPastResidentToleranceDays ) : false;

			if( false == $boolPastResidentValidationCleared ) {
				$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, 'Previous residents cannot set up recurring payments. Please contact the leasing office for assistance' . $strPhoneNumberAddition . '.' ) );
				return false;
			}
 		}

 		// REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS

 		// Calculate the balance of the current lease
 		$objLease->fetchScheduledChargeTotal( $objClientDatabase, \CArTrigger::$c_arrintRecurringArTriggers, $boolCheckIsHidden = true );

		$fltPaymentAmount = ( 1 == $this->m_objScheduledPayment->getBiMonthlyAutoPayment() ) ? ( $this->m_objScheduledPayment->getBiMonthlyFirstPaymentAmount() + $this->m_objScheduledPayment->getBiMonthlySecondPaymentAmount() ) : $this->m_objScheduledPayment->getPaymentAmount();

		if( false == $this->m_objScheduledPayment->getSplitBetweenRoommates()
			&& false == $this->m_objScheduledPayment->getIsVariableAmount()
			&& $fltPaymentAmount != $objLease->getScheduledChargeTotal()
			&& ( true == array_key_exists( 'REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS', $arrobjPropertyPreferences ) )
			&& 1 == $arrobjPropertyPreferences['REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS']->getValue() ) {

			$strErrorMsg = ( 0 == $objLease->getScheduledChargeTotal() ) ? __( 'You cannot set up a recurring payment at this point of time as you do not have any monthly charges associated to your lease.' ) : 'Your payment amount must be equal the sum of your monthly charges "$' . $objLease->getScheduledChargeTotal() . '."';
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMsg ) );

 			return false;
 		}

 		return true;
 	}

 	public function valRecurringPaymentMonthSelects( $objClientDatabase ) {
		$boolIsValid = true;

	    $arrobjPropertyPreferences	= ( array ) CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_objScheduledPayment->getPropertyId() ], [ 'DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS', 'ALLOW_PAST_RESIDENT_SCHEDULED_PAYMENT' ],  $this->m_objScheduledPayment->getCid(), $objClientDatabase );
	    $arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

	    $this->m_objScheduledPayment->buildRecurringPaymentMonthSelects( checkPreferenceIsSet( $arrobjPropertyPreferences, 'DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS' ), checkPreferenceIsSet( $arrobjPropertyPreferences, 'ALLOW_PAST_RESIDENT_SCHEDULED_PAYMENT' ), $objClientDatabase );

	    if( false === valArr( $this->m_objScheduledPayment->getStartMonths() ) || false === valArr( $this->m_objScheduledPayment->getEndMonths() ) ) {
		    $this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( NULL, 'recurring_payment_month_selects', __( 'An auto payment cannot be scheduled at this time for your lease. We require a minimum duration of 2 months for an auto payment to be scheduled. Your lease ends soon and your property requires the last payment to be made in person. Please contact your leasing office with any questions.' ) ) );
		    $boolIsValid = false;
	    }

	    return $boolIsValid;
    }

 	public function validateResidentWorksScheduledPayment( $strAction, $objClientDatabase ) {
 		$boolIsValid = true;

 		switch( $strAction ) {
 			case 'validate_resident_works_summary_info':
 				$boolIsValid &= $this->valAgreesToPayConvenienceFees();
 				$boolIsValid &= $this->valAgreesToTerms();

 			case 'validate_resident_works_billing_info':

 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valCreditCardNumber();
 				$boolIsValid &= $this->valBilltoInformation( false );
 				$boolIsValid &= $this->valPaymentInformation( $objClientDatabase );

 			case 'validate_resident_works_resident_info':

 				$boolIsValid &= $this->valCid();
 				$boolIsValid &= $this->valMerchantAccount( $objClientDatabase );

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 				}

 				$boolIsValid &= $this->valPaymentStartDate();
 				$boolIsValid &= $this->valPaymentEndDate();

 				if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {

 					// If payment ceiling amount is possible, validate it.
 					if( true == $this->m_objScheduledPayment->getSpecifyPaymentCeiling() ) {
 						$boolIsValid &= $this->valPaymentCeilingAmount();
 						$this->m_objScheduledPayment->setEstimatedPaymentAmount( 0 );
 					} else {
 						$boolIsValid &= $this->valEstimatedCeilingAmount();
 						$this->m_objScheduledPayment->setPaymentCeilingAmount( 0 );
 					}

 					$this->m_objScheduledPayment->setPaymentAmount( 0 );

 				} else {
 					$boolIsValid &= $this->valPaymentAmount();
 					$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 				}

 				$boolIsValid &= $this->valBilltoIpAddress();
 				$boolIsValid &= $this->valFloatingVariableConflict();
 				break;

 			case 'validate_resident_works_update':
 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valCid();
 				$boolIsValid &= $this->valBilltoInformation( false );
 				$boolIsValid &= $this->valHasAmountExeceededMerchantlimit();

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 				}

 				if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {

 					// If payment ceiling amount is set, validate it.
 					if( true == $this->m_objScheduledPayment->getSpecifyPaymentCeiling() ) {
 						$boolIsValid &= $this->valPaymentCeilingAmount();
 						$this->m_objScheduledPayment->setEstimatedPaymentAmount( 0 );
 					} else {

 						$boolIsValid &= $this->valEstimatedCeilingAmount();
 						$this->m_objScheduledPayment->setPaymentCeilingAmount( 0 );
 					}

 					$this->m_objScheduledPayment->setPaymentAmount( 0 );

 				} else {
 					$boolIsValid &= $this->valPaymentAmount();
 					$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 				}

 				$boolIsValid &= $this->valBilltoIpAddress();
 				$boolIsValid &= $this->valFloatingVariableConflict();
 				$boolIsValid &= $this->valPaymentInformation( $objClientDatabase );
 				break;

 			case 'validate_resident_works_delete':
 				break;

 			default:
 				// default case
 		}

 		return $boolIsValid;
 	}

	 private function validateCheckBankNumber() {
		 $boolIsValid = true;
		if( false == valStr( $this->m_objScheduledPayment->getCheckBankNumber() ) ) {
			$boolIsValid = false;
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_number', __( 'Bank Number is Required.' ), NULL ) );
		}
		 return $boolIsValid;
	 }

	 private function validateCheckBankTransitNumber() {
		 $boolIsValid = true;
		 if( false == valStr( $this->m_objScheduledPayment->getCheckBankTransitNumber() ) ) {
			$boolIsValid = false;
		 	$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_transit_number', __( 'Bank Transit Number is Required.' ), NULL ) );
		 }
		 return $boolIsValid;
	 }

 	private function validatePadBankInfo( $objClientDatabase ) {
		$boolIsValid = true;

		$objPadBankInfo = \Applications\ResidentPortal\Library\Intents\Common\Models\CPadBankInfo::create();

		try {
			$objPadBankInfo->setRoutingNumber( $this->m_objScheduledPayment->getCheckRoutingNumber() );
			$objPadBankInfo->setAccountNumber( $this->m_objScheduledPayment->getCheckAccountNumber() );
			if( CPsProduct::ENTRATA != $this->m_objScheduledPayment->getPsProductId() ) {
				$objPadBankInfo->setNickname( $this->m_objScheduledPayment->getAccountNickname() );
			}
			$objPadBankInfo->setAccountTypeId( $this->m_objScheduledPayment->getCheckAccountTypeId() );
			$objPadBankInfo->setNameOnAccount( $this->m_objScheduledPayment->getCheckNameOnAccount() );
		} catch( \Applications\ResidentPortal\Library\Intents\Exceptions\CFieldValidationException $objFieldValidationException ) {
			$this->m_objScheduledPayment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $objFieldValidationException->getFieldName(), $objFieldValidationException->getMessage() ) );
			$boolIsValid = false;
		}

		$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
		return $boolIsValid;
	}

 	public function validateResidentPortalScheduledPayment( $strAction, $objClientDatabase, $boolIsNotValidateBillingInfo = false, $boolIsReCreate = false ) {
 		$boolIsValid = true;

 		switch( $strAction ) {

			case 'validate_resident_portal_app_auto_payment':
				$boolIsValid &= $this->valDuplicateVariableRecurringPayment( $objClientDatabase );
				$boolIsValid &= $this->valRecurringPaymentMonthSelects( $objClientDatabase );
 			case 'validate_resident_portal_auto_payment':
 				$boolIsValid &= $this->valCid();

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 					$boolIsValid &= $this->validatePaymentSettings( $objClientDatabase );
 				}

 				if( true == $boolIsValid ) {
 					if( false == $boolIsReCreate ) {
 						$boolIsValid &= $this->valPaymentStartDate();
				    }
 					$boolIsValid &= $this->valPaymentEndDate();

 					if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {
 						// If payment ceiling amount is possible, validate it.

 						if( true == $this->m_objScheduledPayment->getSpecifyPaymentCeiling() || ( CScheduledPaymentType::BIMONTHLY == $this->m_objScheduledPayment->getScheduledPaymentTypeId() && true == $this->m_objScheduledPayment->getBiMonthlySpecifyPaymentCeiling() ) ) {
 							$boolIsValid &= $this->valPaymentCeilingAmount();
 							$this->m_objScheduledPayment->setEstimatedPaymentAmount( 0 );
 						} else {
 							$boolIsValid &= $this->valEstimatedCeilingAmount();
 							$this->m_objScheduledPayment->setPaymentCeilingAmount( 0 );
 						}

 						$this->m_objScheduledPayment->setPaymentAmount( 0 );
 					} else {
 						$boolIsValid &= $this->valPaymentAmount();
 						$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 					}

 					$boolIsValid &= $this->valBilltoIpAddress();
 				}
 				if( false == $boolIsNotValidateBillingInfo ) {
 					$boolIsValid &= $this->valPaymentTypeId( $objClientDatabase );

	 				switch( $this->m_objScheduledPayment->getPaymentTypeId() ) {
	 					case CPaymentType::ACH:
	 						$boolIsValid &= $this->valCheckAccountNumber();
	 						if( 0 == $this->m_objScheduledPayment->getUseStoredBillingInfo() ) {
	 							$boolIsValid &= $this->valConfirmCheckAccountNumber();
	 						}
	 						$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
	 						$boolIsValid &= $this->valCheckAccountTypeId();
	 						$boolIsValid &= $this->valCheckNameOnAccount();
	 						$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
	 						break;

						case CPaymentType::PAD:
							$boolIsValid &= $this->validatePadBankInfo( $objClientDatabase );
							break;

	 						// It's a credit card payment.  validate the credit card info
	 					case CPaymentType::VISA:
	 					case CPaymentType::MASTERCARD:
	 					case CPaymentType::AMEX:
	 					case CPaymentType::DISCOVER:
	 						$boolIsValid &= $this->valCcExpDateYear();
	 						$boolIsValid &= $this->valCcExpDateMonth();
	 						$boolIsValid &= $this->valCcNameOnCard();
	 						$boolIsValid &= $this->valCcCardNumber();
	 						break;

	 					default:
	 						// default case
	 				}
 				}

 				$boolIsValid &= $this->valAgreesToTerms();
 				$boolIsValid &= $this->valAgreesToPayConvenienceFees();
 				$boolIsValid &= $this->valAgreesToDuplicateAutoPayment();
				$boolIsValid &= $this->valBimonthlyBillDay();
 				break;

			case 'validate_duplicate_variable_recurring_payment':
				$boolIsValid &= $this->valDuplicateVariableRecurringPayment( $objClientDatabase );
				break;

 			case 'validate_resident_portal_summary_info':
 				$boolIsValid &= $this->valAgreesToTerms();
 				$boolIsValid &= $this->valAgreesToPayConvenienceFees();

 			case 'validate_resident_portal_billing_info':

 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valBilltoInformation();
 				$boolIsValid &= $this->valPaymentInformation( $objClientDatabase );

 			case 'validate_resident_portal_resident_info':

 				$boolIsValid &= $this->valCid();

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 					$boolIsValid &= $this->validatePaymentSettings( $objClientDatabase );
 				}

 				if( true == $boolIsValid ) {
 					$boolIsValid &= $this->valPaymentStartDate();
 					$boolIsValid &= $this->valPaymentEndDate();

 					if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {

 						// If payment ceiling amount is possible, validate it.
 						if( true == $this->m_objScheduledPayment->getSpecifyPaymentCeiling() ) {
 							$boolIsValid &= $this->valPaymentCeilingAmount();
 							$this->m_objScheduledPayment->setEstimatedPaymentAmount( 0 );
 						} else {
 							$boolIsValid &= $this->valEstimatedCeilingAmount();
 							$this->m_objScheduledPayment->setPaymentCeilingAmount( 0 );
 						}

 						$this->m_objScheduledPayment->setPaymentAmount( 0 );

 					} else {
 						$boolIsValid &= $this->valPaymentAmount();
 						$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 					}

 					$boolIsValid &= $this->valBilltoIpAddress();
 				}
 				break;

 			case 'validate_resident_portal_delete':
 				break;

 			default:
 				// default case
 		}

 		return $boolIsValid;
 	}

 	public function validate( $strAction, $objClientDatabase, $boolIsLeaseRenewed = false, $boolIsNewUpdate = false, $boolIsSplitAdjustSetup = false ) {
 		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
 		$boolIsValid = true;

 		switch( $strAction ) {
 			case VALIDATE_INSERT:
 				$boolIsValid &= $this->valCid();

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 				}

 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valPaymentStartDate();
 				$boolIsValid &= $this->valPaymentEndDate();

 				if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {
 					$boolIsValid &= $this->valPaymentCeilingAmount();
 					$this->m_objScheduledPayment->setPaymentAmount( 0 );
 				} else {
 					$boolIsValid &= $this->valPaymentAmount();
 					$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 				}

 				$boolIsValid &= $this->valBilltoIpAddress();
 				$boolIsValid &= $this->valBilltoInformation();
 				$boolIsValid &= $this->valPaymentInformation( $objClientDatabase );
 				break;

 			case VALIDATE_UPDATE:
 				$boolIsValid &= $this->valCid();

 				if( false == $this->m_objScheduledPayment->getIsFloating() ) {
 					$boolIsValid &= $this->valLeaseId();
 					$boolIsValid &= $this->valCustomerId();
 				}

 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valPaymentStartDate();
 				$boolIsValid &= $this->valPaymentEndDate();

 				if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {
 					$boolIsValid &= $this->valPaymentCeilingAmount();
 					$this->m_objScheduledPayment->setPaymentAmount( 0 );
 				} else {
 					$boolIsValid &= $this->valPaymentAmount();
 					$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 				}

 				$boolIsValid &= $this->valBilltoIpAddress();
 				$boolIsValid &= $this->valBilltoInformation();
 				$boolIsValid &= $this->valPaymentInformation( $objClientDatabase );
 				break;

 			case 'validate_resident_portal_update':
 				$boolIsValid &= $this->valCid();
 				if( false == $boolIsLeaseRenewed && false == $boolIsNewUpdate ) {
 				    $boolIsValid &= $this->valPaymentStartDate( $boolIsSplitAdjustSetup );
 				}

 				$boolIsValid &= $this->valPaymentEndDate();

 				if( true == $this->m_objScheduledPayment->getIsVariableAmount() ) {

 					// If payment ceiling amount is possible, validate it.
 					if( true == $this->m_objScheduledPayment->getSpecifyPaymentCeiling() ) {
 						$boolIsValid &= $this->valPaymentCeilingAmount();
 					}

 					$this->m_objScheduledPayment->setPaymentAmount( 0 );

 				} else {
 					$boolIsValid &= $this->valPaymentAmount();
 					$this->m_objScheduledPayment->setPaymentCeilingAmount( NULL );
 				}
 				break;

 			case 'validate_resident_portal_approve_roommate_auto_payment':
 				$boolIsValid &= $this->valCid();
 				$boolIsValid &= $this->valPaymentTypeId();
 				$boolIsValid &= $this->valPaymentStartDate();
 				$boolIsValid &= $this->valPaymentEndDate();
 				$boolIsValid &= $this->valBilltoIpAddress();

		 		switch( $this->m_objScheduledPayment->getPaymentTypeId() ) {
 					case CPaymentType::ACH:
 						$boolIsValid &= $this->valCheckAccountNumber();
 						if( 0 == $this->m_objScheduledPayment->getUseStoredBillingInfo() ) {
 							$boolIsValid &= $this->valConfirmCheckAccountNumber();
 						}
 						$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
 						$boolIsValid &= $this->valCheckAccountTypeId();
 						$boolIsValid &= $this->valCheckNameOnAccount();
 						$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
 						break;

					case CPaymentType::PAD:
						$boolIsValid &= $this->validatePadBankInfo( $objClientDatabase );
						break;

 						// It's a credit card payment.  validate the credit card info
 					case CPaymentType::VISA:
 					case CPaymentType::MASTERCARD:
 					case CPaymentType::AMEX:
 					case CPaymentType::DISCOVER:
 						$boolIsValid &= $this->valCcExpDateYear();
 						$boolIsValid &= $this->valCcExpDateMonth();
 						$boolIsValid &= $this->valCcNameOnCard();
 						$boolIsValid &= $this->valCcCardNumber();
 						break;

 					default:
 						// default case
		 		}
 				$boolIsValid &= $this->valAgreesToTerms();
 				break;

 			case VALIDATE_DELETE:
 				break;

 			default:
 				// default case
 		}

 		return $boolIsValid;
 	}

 }

?>