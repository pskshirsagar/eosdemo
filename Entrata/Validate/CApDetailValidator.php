<?php

class CApDetailValidator {

	protected $m_objApDetail;

	public function __construct() {
		return;
	}

	public function setApDetail( $objApDetail ) {
		$this->m_objApDetail = $objApDetail;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApDetail->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApDetail->getApHeaderId() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_id', __( 'Ap header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getPropertyId() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Property is required.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'Property is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', $strErrorMsg ) );
		}

		if( false == is_null( $this->m_objApDetail->getBankAccount() ) ) {
			$arrobjPropertyBankAccounts	= $this->m_objApDetail->getBankAccount()->getPropertyBankAccounts();
			$arrintBankPropertyIds		= extractObjectKeyValuePairs( $arrobjPropertyBankAccounts, 'property_id', 'property_id' );

			if( false == array_key_exists( $this->m_objApDetail->getPropertyId(), $arrintBankPropertyIds ) ) {
				$boolIsValid = false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'The property(s) \'{%s, 0}\' is not associated with bank account \'{%s, 1}\' ) ', [ $this->m_objApDetail->getPropertyName(), $this->m_objApDetail->getBankAccount()->getAccountName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valGlAccountId( $strAction = NULL, $intApHeaderSubTypeId = NULL, $arrintGlAccountsRestrictedForAp = [] ) {

		$boolIsValid			= true;
		$strErrorMsg			= NULL;
		$objPropertyGlSetting	= NULL;

		if( ( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId && ( false == valId( $this->m_objApDetail->getApCodeId() ) || false == valId( $this->m_objApDetail->getGlAccountId() ) ) ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Job cost code is required.' );
		} else {

			if( false == valId( $this->m_objApDetail->getGlAccountId() ) ) {

				$boolIsValid = false;

				if( true == valArr( $arrintGlAccountsRestrictedForAp ) ) {
					$strErrorMsg = __( 'The invoice \'{%s, 0}\' uses a Restricted GL account and you do not have permission to post or unpost to restricted GL accounts', [ $this->m_objApDetail->getHeaderNumber() ] );
				} elseif( true == valStr( $this->m_objApDetail->getHeaderNumber() ) ) {
					$strErrorMsg = __( 'GL account is required for Invoice #{%s,0}.', [ $this->m_objApDetail->getHeaderNumber() ] );
				} elseif( 'owner_distribution_invoice_insert' == $strAction ) {
					$strErrorMsg = __( 'Distribution GL account is required for owner \'{%s, 0}\'.', [ $this->m_objApDetail->getOwnerName() ] );
				} else {
					$strErrorMsg = __( 'GL account is required.' );
				}
			} elseif( true == valId( $this->m_objApDetail->getGlAccountId() ) && true == $this->m_objApDetail->getIsGlAccountDisabled() ) {
				$boolIsValid = false;
				$strErrorMsg = __( 'GL account \'{%s, 0}\' is disabled.', [ $this->m_objApDetail->getGlAccountName() ] );
			}
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'GL account is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', $strErrorMsg ) );
		}

		$arrobjGlAccountProperties = ( array ) $this->m_objApDetail->getGlAccountProperties();

		if( true == array_key_exists( $this->m_objApDetail->getGlAccountId(), $arrobjGlAccountProperties ) ) {

			if( false == array_key_exists( $this->m_objApDetail->getPropertyId(), $arrobjGlAccountProperties[$this->m_objApDetail->getGlAccountId()] ) ) {
				if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( '{%s, 0}: GL account "{%s, 1}" is not associated with Property "{%s, 2}".', [ $this->m_objApDetail->getInvoiceAllocationName(), $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
				} elseif( true == valStr( $this->m_objApDetail->getInvoicePropertyGroupName() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( '{%s, 0}: GL account "{%s, 1}" is not associated with Property "{%s, 2}".', [ $this->m_objApDetail->getInvoicePropertyGroupName(), $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
				} else {
					if( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId ) {
						$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code "{%s, 0}" is not associated with Property "{%s, 1}".', [ $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
					} else {
						$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0}" is not associated with Property "{%s, 1}".', [ $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
					}
				}

				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		// Prevent invoice from using AP's bucket gl account and bank's cash gl account
		$objPropertyGlSetting		= $this->m_objApDetail->getPropertyGlSetting();
		$arrobjPropertyBankAccounts	= ( array ) $this->m_objApDetail->getPropertyBankAccounts();

		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && $this->m_objApDetail->getGlAccountId() == $objPropertyGlSetting->getApGlAccountId() ) {
			$boolIsValid &= false;
		}

		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
			if( $objPropertyBankAccount->getPropertyId() == $this->m_objApDetail->getPropertyId()
				&& $objPropertyBankAccount->getBankGlAccountId() == $this->m_objApDetail->getGlAccountId() ) {
				$boolIsValid &= false;
				break;
			}
		}

		if( false == $boolIsValid ) {
			if( CApHeaderSubType::STANDARD_JOB_INVOICE != $intApHeaderSubTypeId ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Invalid gl account used, "{%s, 0}" may not be used with property "{%s, 1}".<br>', [ $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
			} else {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Invalid job cost code used, "{%s, 0}" may not be used with property "{%s, 1}".<br>', [ $this->m_objApDetail->getGlAccountName(), $this->m_objApDetail->getPropertyName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valGlAccountIdForPo( $boolIsApCodePresent, $arrobjGlAccountsRestrictedForApSetting = [], $boolPoAllowPostingToRestrictedGlAccounts = true, $arrmixPropertyGlAccounts = [], $arrobjGlAccounts = [], $intApHeaderSubTypeId = NULL ) {

		$boolIsValid					= true;
		$strGlAccountNumberNameString	= NULL;
		$strErrorMessage				= NULL;

		$arrobjGlAccountProperties		= ( array ) $this->m_objApDetail->getGlAccountProperties();

		if( true == valArr( $arrobjGlAccounts ) && true == isset( $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()] ) ) {
			if( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $intApHeaderSubTypeId ) {
				$strGlAccountNumberNameString = $arrmixPropertyGlAccounts['all_properties'][$this->m_objApDetail->getGlAccountId()];
			} else {
				$strGlAccountNumberNameString = $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()]->getAccountNumber() . ' : ' . $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()]->getAccountName();
			}
		}

		if( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $intApHeaderSubTypeId && ( false == valId( $this->m_objApDetail->getGlAccountId() ) || false == valId( $this->m_objApDetail->getApCodeId() ) ) && false == $boolIsApCodePresent ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code is required.' ) ) );
		} elseif( false == valId( $this->m_objApDetail->getGlAccountId() ) && false == $boolIsApCodePresent ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is required.' ) ) );
		} elseif( false == $boolIsApCodePresent && true == valObj( $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()], 'CGlAccount' )
				&& ( false == is_null( $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()]->getDisabledBy() ) || false == is_null( $arrobjGlAccounts[$this->m_objApDetail->getGlAccountId()]->getDisabledOn() ) ) ) {

			$boolIsValid &= false;

			if( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER != $intApHeaderSubTypeId ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account \'{%s, 0}\' is disabled.', [ $strGlAccountNumberNameString ] ) ) );
			} else {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code \'{%s, 0}\' is disabled.', [ $strGlAccountNumberNameString ] ) ) );
			}

		} elseif( false == $boolPoAllowPostingToRestrictedGlAccounts && true == isset( $arrobjGlAccountsRestrictedForApSetting[$this->m_objApDetail->getGlAccountId()] ) ) {

			$boolIsValid &= false;

			if( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER != $intApHeaderSubTypeId && false == $boolIsApCodePresent ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0}" is restricted and you don\'t have permission to use it.', [ $strGlAccountNumberNameString ] ) ) );
			} elseif( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $intApHeaderSubTypeId && false == $boolIsApCodePresent ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code "{%s, 0}" is restricted and you don\'t have permission to use it.', [ $strGlAccountNumberNameString ] ) ) );
			} elseif( true == $boolIsApCodePresent ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account associated with \'{%s, 0}\' is restricted and you don\'t have permission to use it.', [ $this->m_objApDetail->getItemName() ] ) ) );
			}

		} elseif( true == valArr( $arrobjGlAccountProperties ) && false == array_key_exists( $this->m_objApDetail->getPropertyId(), $arrobjGlAccountProperties ) ) {

			$boolIsValid &= false;

			if( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $intApHeaderSubTypeId ) {
				if( true == $boolIsApCodePresent && true == valId( $this->m_objApDetail->getPropertyId() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code associated to catalog item "{%s, 0}" is no longer associated to Property "{%s, 1}".', [ $this->m_objApDetail->getItemName(), $this->m_objApDetail->getPropertyName() ] ) ) );
				} elseif( true == valId( $this->m_objApDetail->getPropertyId() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Job cost code "{%s, 0}" is no longer associated with Property "{%s, 1}".', [ $strGlAccountNumberNameString, $this->m_objApDetail->getPropertyName() ] ) ) );
				}
			} else {
				if( true == $boolIsApCodePresent && true == valId( $this->m_objApDetail->getPropertyId() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account associated to catalog item "{%s, 0}" is no longer associated to Property "{%s, 1}".', [ $this->m_objApDetail->getItemName(), $this->m_objApDetail->getPropertyName() ] ) ) );
				} elseif( true == valId( $this->m_objApDetail->getPropertyId() ) ) {
					$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0}" is no longer associated with Property "{%s, 1}".', [ $strGlAccountNumberNameString, $this->m_objApDetail->getPropertyName() ] ) ) );
				}
			}

		} elseif( false == $boolIsApCodePresent && true == valArr( $arrmixPropertyGlAccounts )
				&& CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER != $intApHeaderSubTypeId
				&& CApHeaderSubType::STANDARD_PURCHASE_ORDER != $intApHeaderSubTypeId
				&& true == valId( $this->m_objApDetail->getPropertyId() )
				&& false == isset( $arrmixPropertyGlAccounts[$this->m_objApDetail->getPropertyId()] )
				&& false == isset( $arrmixPropertyGlAccounts['all_properties'][$this->m_objApDetail->getGlAccountId()] ) ) {

				$boolIsValid &= false;

				$strErrorMessage = __( 'Selected GL account is either deleted or disabled.' );

				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', $strErrorMessage ) );
		}
		return $boolIsValid;
	}

	public function valTransactionDatetime() {

		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getTransactionDatetime() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Detail transaction datetime is required.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'Detail transaction datetime is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valApprovedAmount() {

		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getPreApprovalAmount() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail amount is required.' );
		} elseif( 0 == ( float ) $this->m_objApDetail->getPreApprovalAmount() ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail amount cannot be zero.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'Bill detail amount is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valTransactionAmount() {

		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Transaction amount is required.' );
		} elseif( 1000000000 < abs( $this->m_objApDetail->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Line item amount must be between -$1,000,000,000 and $1,000,000,000.' ) ) );
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'Transaction amount is required for allocation \'{%s, 0}\'.', $this->m_objApDetail->getInvoiceAllocationName() );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valPaymentAmounts() {
		$boolIsValid = true;

		$intApprovedAmountDue = bcsub( $this->m_objApDetail->getPreApprovalAmount(), bcsub( $this->m_objApDetail->getTransactionAmount(), bcadd( $this->m_objApDetail->getTransactionAmountDue(), $this->m_objApDetail->getPaymentAmount(), 2 ), 2 ), 2 );

		if( 1 == bccomp( abs( $this->m_objApDetail->getPaymentAmount() ), abs( $intApprovedAmountDue ), 2 ) || ( 0 == $intApprovedAmountDue ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'One or more of the invoices in payment queue has been updated by another user, please refresh the page and try again.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBankAccount( $strAction = NULL, $arrmixParameters = [] ) {

		$boolIsValid 							= true;
		$intApBankAccountId 					= NULL;
		$intInterCoReimbursementBankAccountId 	= NULL;
		$intBankAccountTypeId					= NULL;
		$intReimbursedPropertyId				= NULL;
		$arrobjPropertyGlSettings				= ( true == isset( $arrmixParameters['property_gl_settings'] ) ) ? $arrmixParameters['property_gl_settings'] : [];
		$arrobjBankAccounts						= ( true == isset( $arrmixParameters['bank_accounts'] ) ) ? $arrmixParameters['bank_accounts'] : [];
		$intBankAccountId						= ( true == isset( $arrmixParameters['bank_account_id'] ) ) ? $arrmixParameters['bank_account_id'] : NULL;
		$intInterCoReimbursementBankAccountId	= ( true == array_key_exists( $this->m_objApDetail->getPropertyId(), $arrobjPropertyGlSettings ) ) ? $arrobjPropertyGlSettings[$this->m_objApDetail->getPropertyId()]->getInterCoReimbursementBankAccountId() : NULL;
		$intApBankAccountId						= ( true == valId( $intBankAccountId ) ) ? $intBankAccountId : NULL;

		if( true == valArr( $arrobjBankAccounts ) && true == valId( $intApBankAccountId ) && true == array_key_exists( $intApBankAccountId, $arrobjBankAccounts ) ) {

			$intBankAccountTypeId		= $arrobjBankAccounts[$intApBankAccountId]->getBankAccountTypeId();
			$intReimbursedPropertyId	= $arrobjBankAccounts[$intApBankAccountId]->getReimbursedPropertyId();
		}

		if( true == is_null( $this->m_objApDetail->getBankAccountId() ) ) {

			if( 'owner_distribution_invoice_insert' == $strAction ) {
				$strErrorMessage = __( 'Bank Account is not set for \'{%s, 0}\' property.', [ $this->m_objApDetail->getPropertyName() ] );
			} elseif( false == valId( $intInterCoReimbursementBankAccountId ) && $this->m_objApDetail->getPropertyId() != $intReimbursedPropertyId && CBankAccountType::INTER_COMPANY == $intBankAccountTypeId ) {
				$strErrorMessage = __( 'There is no default bank account specified for Inter-Company Reimbursements for property {%s, 0} Go to <a target="_blank" href="/?module=properties_setupxxx&load_large_dialog= ' . urlencode( '?module=property_financial_banking_bank_accountsxxx&property[id]=' . $this->m_objApDetail->getPropertyId() ) . '"> Property Settings</a> and add a default bank account. ', [ $this->m_objApDetail->getPropertyName() ] );
			} else {
				$strErrorMessage = __( 'Bank Account is not set for this property.' );
			}

			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valRequirePoForInvoice( $arrmixParameters = [] ) {

		$boolIsTemplate                             = ( true == isset( $arrmixParameters['is_template'] ) ) ? $arrmixParameters['is_template'] : false;
		$boolIsRecurringTemplate                    = ( true == isset( $arrmixParameters['is_template_recurring'] ) ) ? $arrmixParameters['is_template_recurring'] : false;
		$intApPayeeRequirePosForInvoices            = ( true == isset( $arrmixParameters['require_pos_for_invoices'] ) ) ? $arrmixParameters['require_pos_for_invoices'] : [];
		$arrintApDetailProperties					= ( true == isset( $arrmixParameters['ap_detail_properties'] ) ) ? $arrmixParameters['ap_detail_properties'] : [];
		$arrintPoApHeaderIds						= ( true == isset( $arrmixParameters['po_ap_header_ids'] ) ) ? array_filter( $arrmixParameters['po_ap_header_ids'] ) : [];

		$boolIsValid = true;
		if( true == $this->m_objApDetail->getPropertySettingRequirePosForInvoices() && true == $intApPayeeRequirePosForInvoices && false == valArr( $arrintPoApHeaderIds ) && false == $boolIsTemplate ) {

			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'require_po_for_invoice', __( 'A PO is required to add an invoice for this vendor for property(s) {%s, 0}', [ implode( ',', $arrintApDetailProperties ) ] ) ) );
		} elseif( true == $this->m_objApDetail->getPropertySettingRequirePosForInvoices() && true == $intApPayeeRequirePosForInvoices && false == valArr( $arrintPoApHeaderIds ) && true == $boolIsTemplate && true == $boolIsRecurringTemplate ) {

			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'require_po_for_invoice', __( 'A PO is required to add an invoice for this vendor for property(s).  To accomplish this, please use our Recurring Transactions feature.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getPostMonth() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail post month is required.' );
		}

		$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objApDetail->getPostMonth(), 0, 2 );
		$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objApDetail->getPostMonth(), 3, 2 );
		$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objApDetail->getPostMonth(), 6, 4 );

		if( false == checkdate( $intMonth, $intDay, $intYear ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail post month is invalid.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valBankGlAccountId() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getBankGlAccountId() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bank GL account id is required.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_gl_account_id', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valApGlAccountId() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getApGlAccountId() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Ap GL account id is required.' );
		}

		if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
			$strErrorMsg = __( 'Ap GL account id is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valApDescription() {
		$boolIsValid			= true;
		static $boolIsRepeat	= false;
		$strErrorMsg			= NULL;

		if( true == is_null( $this->m_objApDetail->getDescription() ) ) {
			$boolIsValid = false;
			if( false == $boolIsRepeat ) {
				$strErrorMsg = __( 'Description is required.' );
				$boolIsRepeat = true;
			}
		}

		if( true == valStr( $strErrorMsg ) ) {
			if( true == valStr( $this->m_objApDetail->getInvoiceAllocationName() ) ) {
				$strErrorMsg = __( 'Description is required for allocation \'{%s, 0}\'.', [ $this->m_objApDetail->getInvoiceAllocationName() ] );
			}
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valRate( $boolIsForPo = false, $intApHeaderSubTypeId = NULL ) {

		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getRate() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Rate is required.' );
		} elseif( 0 == ( float ) $this->m_objApDetail->getRate() && ( false == $this->m_objApDetail->getIsInvoiceSubTotals() || true == $boolIsForPo ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Rate can not be zero.' );
		} elseif( 0 < ( float ) $this->m_objApDetail->getRate() && CApHeaderSubType::CREDIT_MEMO_INVOICE == $intApHeaderSubTypeId ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Rate should be less than zero.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valQuantityOrdered( $intApHeaderSubTypeId = NULL ) {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getQuantityOrdered() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Ordered quantity is required.' );
		} elseif( 0 >= ( float ) $this->m_objApDetail->getQuantityOrdered() ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Ordered quantity should be greater than or equal to one.' );
		} elseif( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId && true == valArr( $this->m_objApDetail->getDetailsArray() ) ) {
			$arrmixDetails = $this->m_objApDetail->getDetailsArray();
			$fltTotalQuantity = 0;
			if( true == isset( $arrmixDetails['assigned_units'] ) ) {
				foreach( $arrmixDetails['assigned_units'] as $arrmixAssignedUnits ) {
					if( false == valArr( $arrmixAssignedUnits->property_units ) ) {
						continue;
					}
					$arrintPropertyUnitIds = array_filter( $arrmixAssignedUnits->property_units );
					$fltTotalQuantity      += ( $arrmixAssignedUnits->quantity * \Psi\Libraries\UtilFunctions\count( $arrintPropertyUnitIds ) );
				}
			}

			if( $fltTotalQuantity > $this->m_objApDetail->getQuantityOrdered() ) {
				$boolIsValid &= false;
				$strErrorMsg = __( 'Ordered quantity should be greater than or equal to quantity assigned to units.' );
			}
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valTransactionAmountForNegativeAmount() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		if( true == is_null( $this->m_objApDetail->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail amount is required.' );
		} elseif( 0 == ( float ) $this->m_objApDetail->getTransactionAmount() ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'Bill detail amount cannot be zero.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId( $boolIsPropertyUnitRequired = false ) {

		$boolIsValid = true;

		if( true == $boolIsPropertyUnitRequired && false == valId( $this->m_objApDetail->getPropertyUnitId() ) && false == valId( $this->m_objApDetail->getMaintenanceLocationId() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', __( 'Unit / Property Location is required.' ) ) );
		}

		if( false == is_null( $this->m_objApDetail->getPropertyUnitId() ) && true == valArr( $this->m_objApDetail->getPropertyUnits() ) && false == array_key_exists( $this->m_objApDetail->getPropertyUnitId(), $this->m_objApDetail->getPropertyUnits() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', __( 'Unit "{%s, 0}" is not associated with property "{%s, 1}".', [ $this->m_objApDetail->getUnitNumber(), $this->m_objApDetail->getPropertyName() ] ) ) );

			// setting prpoerty_unit_id and unit_number to NULL after validating the deleted property_unit.
			$this->m_objApDetail->setUnitNumber( NULL );
			$this->m_objApDetail->setPropertyUnitId( NULL );
		}

		return $boolIsValid;
	}

	public function valFixedAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApDetail->getFixedAmount() ) ) {

			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', __( 'Amount is required.' ) ) );
		} elseif( 0 == ( float ) $this->m_objApDetail->getFixedAmount() ) {

			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', __( 'Amount cannot be zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApCodeId( $boolIsApCodePresent, $boolIsAllowOverridingDefaultGlAccount = true, $boolIsInvoiceAllowPostingToRestrictedGlAccounts = false, $arrintGlAccountsRestrictedForAp = [] ) {

		$boolIsValid = true;

		if( false == $boolIsApCodePresent ) return $boolIsValid;

		if( 1 == $this->m_objApDetail->getIsDisabledItem() ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Catalog Item "{%s, 0}" is disabled.', [ $this->m_objApDetail->getItemName() ] ) ) );
		} elseif( true == is_null( $this->m_objApDetail->getApCodeId() ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Catalog Item is required.' ) ) );
		} elseif( true == is_null( $this->m_objApDetail->getGlAccountId() ) ) {
			$boolIsValid &= false;
			if( false == $boolIsAllowOverridingDefaultGlAccount ) {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'User must have permission to override or add GL Account.' ) ) );
			} else {
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Please enter a GL Account.' ) ) );
			}
		} elseif( false == $boolIsInvoiceAllowPostingToRestrictedGlAccounts && true == isset( $arrintGlAccountsRestrictedForAp[$this->m_objApDetail->getGlAccountId()] ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL Account associated with \'{%s, 0}\' is restricted and you don\'t have permission to use it.', [ $this->m_objApDetail->getItemName() ] ) ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApDetail->getDescription() ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureId( $boolIsApCodePresent ) {

		$boolIsValid = true;

		if( false == $boolIsApCodePresent ) return $boolIsValid;

		// if catalog item is selected then show validation

		if( false == valId( $this->m_objApDetail->getUnitOfMeasureId() ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_of_measure_id', __( 'Unit of measure is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyDepartment( $arrobjCompanyDepartments ) {

		$boolIsValid = true;

		if( false == is_null( $this->m_objApDetail->getCompanyDepartmentId() ) && false == isset( $arrobjCompanyDepartments[$this->m_objApDetail->getCompanyDepartmentId()] ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_department_id', __( 'Selected company department is either deleted or disabled.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlDimension( $arrobjGlDimensions ) {

		$boolIsValid = true;

		if( false == is_null( $this->m_objApDetail->getGlDimensionId() ) && false == isset( $arrobjGlDimensions[$this->m_objApDetail->getGlDimensionId()] ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_dimension_id', __( 'Selected custom tag is either deleted or disabled.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApTransactionType( $arrmixParameters = [] ) {

		$intApBankAccountId 		= NULL;
		$intBankAccountId			= ( true == isset( $arrmixParameters['bank_account_id'] ) ) ? $arrmixParameters['bank_account_id'] : NULL;
		$arrobjBankAccounts			= ( true == isset( $arrmixParameters['bank_accounts'] ) ) ? $arrmixParameters['bank_accounts'] : [];
		$arrobjPropertyGlSettings	= ( true == isset( $arrmixParameters['property_gl_settings'] ) ) ? $arrmixParameters['property_gl_settings'] : [];

		if( true == valId( $intBankAccountId ) ) {
			$intApBankAccountId = $intBankAccountId;
		} elseif( true == array_key_exists( $this->m_objApDetail->getPropertyId(), $arrobjPropertyGlSettings ) ) {
			$intApBankAccountId = $arrobjPropertyGlSettings[$this->m_objApDetail->getPropertyId()]->getApBankAccountId();
		}

		$boolIsValid = true;

		if( true == valArr( $arrobjBankAccounts ) ) {
			if( true == array_key_exists( $intApBankAccountId, $arrobjBankAccounts )
				&& $this->m_objApDetail->getPropertyId() != $arrobjBankAccounts[$intApBankAccountId]->getReimbursedPropertyId()
				&& $arrobjBankAccounts[$intApBankAccountId]->getBankAccountTypeId() == CBankAccountType::INTER_COMPANY
				&& $this->m_objApDetail->getGlAccountUsageTypeId() == CGlAccountUsageType::PENDING_REIMBURSEMENTS ) {

				$boolIsValid &= false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_transaction_type_id', __( 'You cannot choose a GL Account of type "Pending Reimbursement".' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsConfidential( $arrobjCompanyPreferences ) {

		$boolIsValid = true;

		if( true == $this->m_objApDetail->getIsConfidential()
			&& ( ( true == valArr( $arrobjCompanyPreferences ) && false == array_key_exists( 'USE_ITEM_CONFIDENTIALITY', $arrobjCompanyPreferences ) )
			|| false == valArr( $arrobjCompanyPreferences ) ) ) {

			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_confidential', __( '\'Use Item Confidentiality\' setting is disabled. Please enable it to use the confidential flag.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAllocation( $arrobjApFormulas, $intApHeaderSubTypeId, $arrintPropertyIdWiseApFormulaIds ) {

		$boolIsValid = true;

		if( 1 == $this->m_objApDetail->getIsAllocation()
			&& ( false == valArr( $arrobjApFormulas ) || ( true == valArr( $arrobjApFormulas ) && false == isset( $arrobjApFormulas[$this->m_objApDetail->getApFormulaId()] ) ) ) ) {

			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Selected allocation is deleted.' ) ) );

		} elseif( ( CApHeaderSubType::CATALOG_PURCHASE_ORDER == $intApHeaderSubTypeId || CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) && 1 == $this->m_objApDetail->getIsAllocation() && true == valArr( $arrobjApFormulas )
			&& true == isset( $arrintPropertyIdWiseApFormulaIds[$this->m_objApDetail->getPropertyId()] )
			&& 1 == $arrobjApFormulas[$arrintPropertyIdWiseApFormulaIds[$this->m_objApDetail->getPropertyId()]]->getIsGlAccountAssociated() ) {

			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Selected allocation is associated with Gl Account. Remove that association to use allocation.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsPropertyAssociatedToApCode() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApDetail->getPropertyId() ) ) {
			return $boolIsValid;
		}

		if( true == valIntArr( $this->m_objApDetail->getAssociatedApCodeProperties() ) && false == in_array( $this->m_objApDetail->getPropertyId(), $this->m_objApDetail->getAssociatedApCodeProperties() ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Selected property \'{%s, 0}\' is not associated to selected catalog item \'{%s, 1}\'. ', [ $this->m_objApDetail->getPropertyName(), $this->m_objApDetail->getItemName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valJobPhaseId( $intApHeaderSubTypeId = NULL ) {
		$boolIsValid = true;

		$strErrorMessage = ( false == is_null( $intApHeaderSubTypeId ) ) ? __( 'Job phase is required.' ) : __( 'Job is required.' );

		if( true == is_null( $this->m_objApDetail->getJobPhaseId() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valContractApCodeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_objApDetail->getApCodeId() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Job cost code is required for contract items.' ) ) );
		}

		return $boolIsValid;
	}

	public function valContactValue() {
		$boolIsValid = true;

		if( 0 >= $this->m_objApDetail->getTransactionAmount() ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Contract value should be greater than 0.' ) ) );
		} elseif( 1000000000 < abs( $this->m_objApDetail->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Contract value amount must be between -$1,000,000,000 and $1,000,000,000.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRecQuantity() {
		$boolIsValid = true;

		if( 0 != bccomp( $this->m_objApDetail->getActualAmount(), ( $this->m_objApDetail->getReceivedQuantity() * $this->m_objApDetail->getUnitCost() ) ) ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', __( 'Amount must be equal to Received * Unit Cost.' ) ) );
			return $boolIsValid;
		}

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->m_objApDetail->getAssetTypeId() || CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED == $this->m_objApDetail->getAssetTypeId() || CUnitOfMeasureType::EACH == $this->m_objApDetail->getUnitOfMeasureTypeId() ) {

			$arrintDecimalVal = explode( '.', $this->m_objApDetail->getReceivedQuantity() );
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintDecimalVal ) && 0 < $arrintDecimalVal[1] ) {
				$boolIsValid = false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', __( 'Receiving Quantity should not be in decimals for type Fixed Asset,Fixed Asset Non Depreciable OR Unit of measure type Each.' ) ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valRetentionAmount() {

		$boolIsValid = true;

		if( 0 < $this->m_objApDetail->getRetentionAmount() ) {
			if( false == valId( $this->m_objApDetail->getApContractId() ) ) {
				$boolIsValid &= false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_contract_id', __( 'Contract is required if retention percent is provided.' ) ) );
			} elseif( $this->m_objApDetail->getTransactionAmount() < $this->m_objApDetail->getRetentionAmount() ) {
				$boolIsValid &= false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retention_amount', __( 'Retention amount should not be greater than transaction amount.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCreditMemoTransactionAmount( $arrobjOriginalApDetails ) {

		$boolIsValid = true;

		if( true == array_key_exists( $this->m_objApDetail->getOriginalApDetailId(), $arrobjOriginalApDetails ) ) {
			$fltTransactionAmount = bcadd( ( float ) $this->m_objApDetail->getTransactionAmount(), 0, 2 );
			$fltOriginalDetailTransactionAmountDue = bcadd( ( float ) $arrobjOriginalApDetails[$this->m_objApDetail->getOriginalApDetailId()]->getTransactionAmountDue(), 0, 2 );
			$fltTransactionAmountDue = bcmul( $fltOriginalDetailTransactionAmountDue, -1, 2 );

			if( ( $fltOriginalDetailTransactionAmountDue > 0 && ( $fltTransactionAmount > 0 || $fltTransactionAmount < $fltTransactionAmountDue ) )
				|| ( $fltOriginalDetailTransactionAmountDue < 0 && ( $fltTransactionAmount < 0 || $fltTransactionAmount > $fltTransactionAmountDue ) ) ) {
				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Credit memo line item amount(s) must be less than or equal to the unpaid amount on the original invoice line item(s)' ) ) );
		}

		return $boolIsValid;
	}

	public function valDetails( $strUnitAssignmentType ) {
		$boolIsValid = true;

		$arrmixDetails = $this->m_objApDetail->getDetailsArray();

		if( false == valStr( $arrmixDetails['assignment_type'] ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', __( 'Assigned to is required.' ) ) );
		}

		$fltTotalQuantity = 0;

		foreach( $arrmixDetails['assigned_units'] as $intIndex => $arrmixAssignedUnits ) {
			$arrintPropertyUnitIds = $arrmixAssignedUnits['property_units'];

			if( true == valArr( $arrintPropertyUnitIds ) ) $arrintPropertyUnitIds = array_filter( $arrintPropertyUnitIds );

			if( false == valArr( $arrintPropertyUnitIds ) ) {
				$boolIsValid &= false;
				$strMessage = ( 'partial' == $strUnitAssignmentType ) ? __( 'Unit is required.' ) : __( 'At least one unit is required at line {%d, 0}.', [ ( int ) ( $intIndex + 1 ) ] );
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', $strMessage ) );
			}

			if( true == is_null( $arrmixAssignedUnits['quantity'] ) || 0 >= $arrmixAssignedUnits['quantity'] ) {
				$boolIsValid &= false;
				$strMessage = ( 'partial' == $strUnitAssignmentType ) ? __( 'Quantity is required.' ) : __( 'Quantity is required at line {%d, 0}.', [ ( int ) ( $intIndex + 1 ) ] );
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', $strMessage ) );
			} elseif( true == valArr( $arrintPropertyUnitIds ) ) {
				$fltTotalQuantity += ( $arrmixAssignedUnits['quantity'] * \Psi\Libraries\UtilFunctions\count( $arrintPropertyUnitIds ) );
			}
		}

		if( $fltTotalQuantity > $this->m_objApDetail->getQuantityOrdered() ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', __( 'Total quantity should not be greater than line item quantity.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnits( $strUnitAssignmentType ) {
		$boolIsValid = true;

		if( 'single' == $strUnitAssignmentType ) {
			$boolIsValid &= $this->valPropertyUnitId( true );
		} elseif( 'partial' == $strUnitAssignmentType || 'multiple' == $strUnitAssignmentType ) {
			$boolIsValid &= $this->valDetails( $strUnitAssignmentType );
		} else {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', __( 'Assigned to is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInvoiced( $boolIsDetailInvoiced, $strPoNumber ) {
		$boolIsValid = true;

		if( true == $boolIsDetailInvoiced ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Line item(s) are already invoiced for purchase order {%s, 0}.', [ $strPoNumber ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCatalogItem( $strItemName, $strInvItemname, $strPoNumber ) {
		$boolIsValid = true;

		if( true == valstr( $strItemName ) && $strItemName != $strInvItemname ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Catalog item name is not a match for purchase order {%s, 0}.', [ $strPoNumber ] ) ) );
		}

		return $boolIsValid;
	}

	public function valLineItemQuantity( $intQuantity, $strPoNumber ) {
		$boolIsValid = true;
		if( ( float ) $intQuantity < ( float ) $this->m_objApDetail->getQuantityOrdered() ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Invoice line item quantity must be less than or equal to the received quantity on PO {%s, 0}.', [ $strPoNumber ] ) ) );
		}

		return $boolIsValid;
	}

	public function valRetentionPayableGlAccountId() {

		$boolIsValid			        = true;

		if( false == valId( $this->m_objApDetail->getRetentionPayableGlAccountId() ) && $this->m_objApDetail->getRetentionAmount() > 0 ) {
			$boolIsValid = false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( NULL, NULL,  __( 'You must first select a Retention Payable GL account in Setup > Company > Financial > Corporate > Job Costing > Job Preferences before posting this invoice.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPreApprovalAmount() {

		$boolIsValid = true;

		if( ( $this->m_objApDetail->getTransactionAmount() > 0 && $this->m_objApDetail->getPreApprovalAmount() > $this->m_objApDetail->getTransactionAmount() )
		    || ( $this->m_objApDetail->getTransactionAmount() < 0 && $this->m_objApDetail->getPreApprovalAmount() < $this->m_objApDetail->getTransactionAmount() ) ) {
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pre_approval_amount', __( 'One or more of the invoice(s) has been Approved by another user, please refresh the page and try again.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOrderedQuantity() {
		$boolIsValid = true;

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->m_objApDetail->getAssetTypeId() || CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED == $this->m_objApDetail->getAssetTypeId() || CUnitOfMeasureType::EACH == $this->m_objApDetail->getUnitOfMeasureTypeId() ) {

			$arrintDecimalVal = explode( '.', $this->m_objApDetail->getOrderedQuantity() );
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintDecimalVal ) && 0 < $arrintDecimalVal[1] ) {
				$boolIsValid = false;
				$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', __( 'Ordered Quantity should not be in decimals for type Fixed Asset,Fixed Asset Non Depreciable OR Unit of measure type Each.' ) ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valIsGlAccountDisabled() {

		$boolIsValid = true;

		if( true == $this->m_objApDetail->getIsGlAccountDisabled() ) {
			$strGlAccountNumberNameString = $this->m_objApDetail->getGlAccountNumber() . ' : ' . $this->m_objApDetail->getGlAccountName();
			$boolIsValid &= false;
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account \'{%s, 0}\' is disabled.', [ $strGlAccountNumberNameString ] ) ) );
		}

		return $boolIsValid;
	}

	public function valBillbackInvoice( $arrobjBankAccounts ) {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		$objBankAccount = $arrobjBankAccounts[$this->m_objApDetail->getBankAccountId()];

		if( true == valObj( $objBankAccount, 'CBankAccount' ) && true == valId( $objBankAccount->getBankAccountTypeId() )
		    && CBankAccountType::INTER_COMPANY == $objBankAccount->getBankAccountTypeId()
		    && ( CReimbursementMethod::BILLBACK == $this->m_objApDetail->getReimbursementMethodId()
			 || CApTransactionType::BILL_BACK_REIMBURSEMENT == $this->m_objApDetail->getApTransactionTypeId() ) ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'The Billback feature cannot be used in combination with an intercompany type bank account.' );
		}

		if( true == $this->m_objApDetail->getIsReimbursementPresent() ) {
			$boolIsValid = false;
			$strErrorMsg = __( 'This line item is tied to a billback reimbursement and cannot be edited.' );
		}

		if( true == valStr( $strErrorMsg ) ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reimbursement_method_id', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valIsApprovedForPayment() {

		$boolIsValid = true;
		if( false == $this->m_objApDetail->getIsApprovedForPayment() ) {
			$this->m_objApDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_approve_for_payment', __( "We're unable to perform this action because the associated invoice is not approved/posted" ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsApCodePresent = false, $arrmixParameters = [] ) {

		$boolIsValid												= true;
		$boolIsUseRetention											= ( true == isset( $arrmixParameters['bool_is_use_retention'] ) ) ? $arrmixParameters['bool_is_use_retention'] : NULL;
		$boolIsDetailInvoiced										= ( true == isset( $arrmixParameters['is_invoiced'] ) ) ? $arrmixParameters['is_invoiced'] : false;
		$boolIsCreditMemoInvoice									= ( true == isset( $arrmixParameters['is_credit_memo_invoice'] ) ) ? $arrmixParameters['is_credit_memo_invoice'] : false;
		$boolIsFromReverseVoidedPayment								= ( true == isset( $arrmixParameters['is_from_reverse_voided_payment'] ) ) ? $arrmixParameters['is_from_reverse_voided_payment'] : NULL;
		$boolPoAllowPostingToRestrictedGlAccounts					= ( true == isset( $arrmixParameters['po_allow_posting_to_restricted_gl_accounts'] ) ) ? $arrmixParameters['po_allow_posting_to_restricted_gl_accounts'] : false;
		$boolIsInvoiceAllowPostingToRestrictedGlAccounts			= ( true == isset( $arrmixParameters['invoice_allow_posting_to_restricted_gl_accounts'] ) ) ? $arrmixParameters['invoice_allow_posting_to_restricted_gl_accounts'] : false;
		$boolIsAllowOverridingDefaultGlAccountOnCatalogPo 			= ( true == isset( $arrmixParameters['is_allow_overriding_default_gl_account_on_catalog_po'] ) ) ? $arrmixParameters['is_allow_overriding_default_gl_account_on_catalog_po'] : NULL;
		$boolIsAllowOverridingDefaultGlAccountOnCatalogInvoice 		= ( true == isset( $arrmixParameters['is_allow_overriding_default_gl_account_on_catalog_invoice'] ) ) ? $arrmixParameters['is_allow_overriding_default_gl_account_on_catalog_invoice'] : NULL;
		$intQuantity												= ( true == isset( $arrmixParameters['quantity'] ) ) ? $arrmixParameters['quantity'] : NULL;
		$intBankAccountId											= ( true == isset( $arrmixParameters['bank_account_id'] ) ) ? $arrmixParameters['bank_account_id'] : NULL;
		$intApHeaderSubTypeId										= ( true == isset( $arrmixParameters['ap_header_sub_type_id'] ) ) ? $arrmixParameters['ap_header_sub_type_id'] : NULL;
		$strPoNumber												= ( true == isset( $arrmixParameters['po_number'] ) ) ? $arrmixParameters['po_number'] : NULL;
		$strItemName												= ( true == isset( $arrmixParameters['item_name'] ) ) ? $arrmixParameters['item_name'] : NULL;
		$strInvItemname												= ( true == isset( $arrmixParameters['inv_item_name'] ) ) ? $arrmixParameters['inv_item_name'] : NULL;
		$strUnitAssignmentType										= ( true == isset( $arrmixParameters['assignment_type'] ) ) ? $arrmixParameters['assignment_type'] : NULL;
		$arrintUnassociatedApDetailIds								= ( true == isset( $arrmixParameters['unassociated_ap_detail_ids'] ) ) ? $arrmixParameters['unassociated_ap_detail_ids'] : [];
		$arrintGlAccountsRestrictedForAp							= ( true == isset( $arrmixParameters['restrict_ap_usage_gl_account'] ) ) ? $arrmixParameters['restrict_ap_usage_gl_account'] : [];
		$arrintPropertyIdWiseApFormulaIds							= ( true == isset( $arrmixParameters['ap_formula_ids'] ) ) ? $arrmixParameters['ap_formula_ids'] : [];
		$arrmixApCodes												= ( true == isset( $arrmixParameters['ap_codes'] ) ) ? $arrmixParameters['ap_codes'] : [];
		$arrmixPropertyGlAccounts									= ( true == isset( $arrmixParameters['property_gl_accounts'] ) ) ? $arrmixParameters['property_gl_accounts'] : [];
		$arrobjGlAccounts											= ( true == isset( $arrmixParameters['gl_accounts'] ) ) ? $arrmixParameters['gl_accounts'] : [];
		$arrobjApFormulas											= ( true == isset( $arrmixParameters['ap_formulas'] ) ) ? $arrmixParameters['ap_formulas'] : [];
		$arrobjGlDimensions											= ( true == isset( $arrmixParameters['gl_dimensions'] ) ) ? $arrmixParameters['gl_dimensions'] : [];
		$arrobjBankAccounts											= ( true == isset( $arrmixParameters['bank_accounts'] ) ) ? $arrmixParameters['bank_accounts'] : [];
		$arrobjOriginalApDetails									= ( true == isset( $arrmixParameters['original_ap_details'] ) ) ? $arrmixParameters['original_ap_details'] : NULL;
		$arrobjCompanyDepartments									= ( true == isset( $arrmixParameters['company_departments'] ) ) ? $arrmixParameters['company_departments'] : [];
		$arrobjCompanyPreferences									= ( true == isset( $arrmixParameters['company_preferences'] ) ) ? $arrmixParameters['company_preferences'] : [];
		$arrobjGlAccountsRestrictedForApSetting						= ( true == isset( $arrmixParameters['gl_accounts_restricted_for_ap_setting'] ) ) ? $arrmixParameters['gl_accounts_restricted_for_ap_setting'] : [];

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'validate_purchase_order_update':
				$boolIsValid &= $this->valOrderedQuantity();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountIdForPo( $boolIsApCodePresent, $arrobjGlAccountsRestrictedForApSetting, $boolPoAllowPostingToRestrictedGlAccounts, $arrmixPropertyGlAccounts, $arrobjGlAccounts, $intApHeaderSubTypeId );
				$boolIsValid &= $this->valApCodeId( $boolIsApCodePresent, $boolIsAllowOverridingDefaultGlAccountOnCatalogPo );
				$boolIsValid &= $this->valQuantityOrdered();
				// This condition is added so the line items created for tax, shipping and discount when setting is apply tax/Disc/Ship to GL Account we don't validate the rate field as it is 0.
				if( !( $this->m_objApDetail->getTransactionAmount() ) ) {
					$boolIsValid &= $this->valRate( true );
				}
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitOfMeasureId( $boolIsApCodePresent );
				// $boolIsValid &= $this->valCompanyDepartment( $arrobjCompanyDepartments );
				$boolIsValid &= $this->valGlDimension( $arrobjGlDimensions );
				$boolIsValid &= $this->valIsConfidential( $arrobjCompanyPreferences );
				if( $intApHeaderSubTypeId == CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER || $intApHeaderSubTypeId == CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ) {
					$boolIsValid &= $this->valJobPhaseId();
				}
				break;

			case 'single_invoice_update':
				$boolIsValid &= $this->valRate( false, $intApHeaderSubTypeId );
				$boolIsValid &= $this->valRetentionAmount();
				if( $intApHeaderSubTypeId == CApHeaderSubType::STANDARD_INVOICE || CApTransactionType::BILL_BACK_REIMBURSEMENT == $this->m_objApDetail->getApTransactionTypeId() ) {
					$boolIsValid &= $this->valBillbackInvoice( $arrobjBankAccounts );
				}
				if( true == $boolIsFromReverseVoidedPayment ) {
					$boolIsValid &= $this->valIsApprovedForPayment();
				}

			case 'single_invoice_insert':
				$boolIsValid &= $this->valRate( false, $intApHeaderSubTypeId );
				$boolIsValid &= $this->valPropertyId();
				if( true == $boolIsApCodePresent ) {
					$boolIsValid &= $this->valApCodeId( $boolIsApCodePresent, $boolIsAllowOverridingDefaultGlAccountOnCatalogInvoice, $boolIsInvoiceAllowPostingToRestrictedGlAccounts, $arrintGlAccountsRestrictedForAp );

					if( true == valId( $this->m_objApDetail->getBillbackPropertyId() ) || CApTransactionType::STANDARD_REIMBURSEMENT_NEW == $this->m_objApDetail->getApTransactionTypeId() ) {
						$boolIsValid &= $this->valIsGlAccountDisabled();
					}
				} else {
					$boolIsValid &= $this->valGlAccountId( NULL, $intApHeaderSubTypeId );
				}
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valApTransactionType( $arrmixParameters );
				$boolIsValid &= $this->valQuantityOrdered( $intApHeaderSubTypeId );
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valBankAccount( $strAction, $arrmixParameters );
				$boolIsValid &= $this->valAllocation( $arrobjApFormulas, $intApHeaderSubTypeId, $arrintPropertyIdWiseApFormulaIds );

				if( true != $boolIsCreditMemoInvoice && CApHeaderSubType::CREDIT_MEMO_INVOICE != $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valRequirePoForInvoice( $arrmixParameters );
				}

				if( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valJobPhaseId();
					$boolIsValid &= $this->valRetentionAmount();
					if( true == $boolIsUseRetention ) {
						$boolIsValid &= $this->valRetentionPayableGlAccountId();
					}
				}

				if( true == $boolIsCreditMemoInvoice ) {
					$boolIsValid &= $this->valCreditMemoTransactionAmount( $arrobjOriginalApDetails );
				}

				if( $intApHeaderSubTypeId == CApHeaderSubType::STANDARD_INVOICE
				    || CApTransactionType::BILL_BACK_REIMBURSEMENT == $this->m_objApDetail->getApTransactionTypeId() ) {
					$boolIsValid &= $this->valBillbackInvoice( $arrobjBankAccounts );
				}
				break;

			case 'single_po_insert':
			case 'single_po_update':
				$boolIsValid &= $this->valOrderedQuantity();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountIdForPo( $boolIsApCodePresent, $arrobjGlAccountsRestrictedForApSetting, $boolPoAllowPostingToRestrictedGlAccounts, $arrmixPropertyGlAccounts, $arrobjGlAccounts, $intApHeaderSubTypeId );
				if( !( $this->m_objApDetail->getTransactionAmount() ) ) {
					$boolIsValid &= $this->valRate( true );
				}
				$boolIsValid &= $this->valApCodeId( $boolIsApCodePresent, $boolIsAllowOverridingDefaultGlAccountOnCatalogPo );
				$boolIsValid &= $this->valQuantityOrdered();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitOfMeasureId( $boolIsApCodePresent );
				$boolIsValid &= $this->valIsConfidential( $arrobjCompanyPreferences );
// 				$boolIsValid &= $this->valCompanyDepartment( $arrobjCompanyDepartments );
				$boolIsValid &= $this->valGlDimension( $arrobjGlDimensions );
				$boolIsValid &= $this->valAllocation( $arrobjApFormulas, $intApHeaderSubTypeId, $arrintPropertyIdWiseApFormulaIds );
				if( $intApHeaderSubTypeId == CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER || $intApHeaderSubTypeId == CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ) {
					$boolIsValid &= $this->valJobPhaseId();
				}
				break;

			case 'payment_ap_detail_insert':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmountForNegativeAmount();
				$boolIsValid &= $this->valApprovedAmount();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'validate_payment_transaction_amounts':
				$boolIsValid &= $this->valPaymentAmounts();
				break;

			case 'rpc_webservice_invoice_insert':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				// $boolIsValid &= $this->valApDescription();
				$boolIsValid &= $this->valQuantityOrdered();
				$boolIsValid &= $this->valRate();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'validate_financial_move_out':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valBankAccount( $strAction, $arrmixParameters );
				break;

			case 'approve_invoice':
				$boolIsValid &= $this->valPreApprovalAmount();

			case 'post_invoice':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				if( true == $boolIsUseRetention && ( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId || CApHeaderSubType::CATALOG_JOB_INVOICE == $intApHeaderSubTypeId ) ) {
					$boolIsValid &= $this->valRetentionPayableGlAccountId();
				}
			case 'approve_post_routed_invoice':
			case 'import_invoice':
			case 'validate_gl_account':
				$boolIsValid &= $this->valGlAccountId( NULL, $intApHeaderSubTypeId, $arrintGlAccountsRestrictedForAp );
				if( CApHeaderSubType::STANDARD_JOB_INVOICE == $intApHeaderSubTypeId || CApHeaderSubType::CATALOG_JOB_INVOICE == $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valJobPhaseId( $intApHeaderSubTypeId );
				}
				break;

			case 'process_owner_distribution';
				$boolIsValid &= $this->valFixedAmount();
				break;

			case 'update_items';
				$boolIsValid &= $this->valRecQuantity();
				$boolIsValid &= $this->valGlAccountIdForPo( true, $arrobjGlAccountsRestrictedForApSetting, $boolPoAllowPostingToRestrictedGlAccounts );
				break;

			case 'owner_distribution_invoice_insert':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId( $strAction );
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valApDescription();
				$boolIsValid &= $this->valQuantityOrdered();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valBankAccount( $strAction, $arrmixParameters );
				break;

			case 'purchasing_tool_po_insert':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountIdForPo( $boolIsApCodePresent, $arrobjGlAccountsRestrictedForApSetting, $boolPoAllowPostingToRestrictedGlAccounts, $arrmixPropertyGlAccounts, $arrobjGlAccounts, $intApHeaderSubTypeId );
				$boolIsValid &= $this->valApCodeId( $boolIsApCodePresent, true, $boolPoAllowPostingToRestrictedGlAccounts, $arrintGlAccountsRestrictedForAp );
				$boolIsValid &= $this->valQuantityOrdered();
				$boolIsValid &= $this->valRate();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valIsPropertyAssociatedToApCode();
				$boolIsValid &= $this->valIsConfidential( $arrobjCompanyPreferences );
				// $boolIsValid &= $this->valCompanyDepartment( $arrobjCompanyDepartments );
				$boolIsValid &= $this->valGlDimension( $arrobjGlDimensions );
				$boolIsValid &= $this->valAllocation( $arrobjApFormulas, $intApHeaderSubTypeId, $arrintPropertyIdWiseApFormulaIds );
				break;

			case 'validate_contract_items':
				$boolIsValid &= $this->valContractApCodeId();
				$boolIsValid &= $this->valContactValue();
				if( $intApHeaderSubTypeId == CApHeaderSubType::CONTRACT_BUDGET ) {
					$boolIsValid &= $this->valJobPhaseId( $intApHeaderSubTypeId );
				}
				break;

			case 'assign_units':
				$boolIsValid &= $this->valPropertyUnits( $strUnitAssignmentType );
				break;

			case 'validate_attach_po':
				if( CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valCatalogItem( $strItemName, $strInvItemname, $strPoNumber );
					$boolIsValid &= $this->valLineItemQuantity( $intQuantity, $strPoNumber );
				}
				$boolIsValid &= $this->valIsInvoiced( $boolIsDetailInvoiced, $strPoNumber );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;

	}

}
?>