<?php

class CApHeaderValidator {

	protected $m_objApHeader;

	public function __construct() {
		return;
	}

	public function setApHeader( $objApHeader ) {
		$this->m_objApHeader = $objApHeader;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Vendor is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeIdAndApPayeeLocationId() {
		$boolIsValid = true;

		if( ( true == is_null( $this->m_objApHeader->getApPayeeId() )
			|| true == is_null( $this->m_objApHeader->getApPayeeLocationId() ) )
			&& false == is_numeric( $this->m_objApHeader->getLeaseCustomerId() ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id_ap_payee_location_id', __( 'Vendor and location is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeRemittanceId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getApRemittanceId() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_remittance_id', __( 'Vendor Remittance is required.' ) ) );
		}

		return $boolIsValid;

	}

	public function valApBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDatetime() {

		$boolIsValid	= true;
		$strMessage		= __( 'Invoice datetime is required' );

		if( CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId() ) {
			$strMessage		= __( 'Purchase order date is required' );
		}

		if( true == is_null( $this->m_objApHeader->getTransactionDatetime() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', ' ' . $strMessage . '' ) );
		}

		return $boolIsValid;
	}

	public function valPostDate( $intModule = NULL, $boolIsReverse = false, $boolIsVoidCheck = false ) {

		$boolIsValid	= true;
		$strMessage		= NULL;
		$strPostDate	= ( false == $boolIsReverse ) ? $this->m_objApHeader->getPostDate() : $this->m_objApHeader->getReversePostDate();

		if( false == valStr( $strPostDate ) ) {

			$boolIsValid &= false;
			if( true == $boolIsVoidCheck ) {
				$strMessage   = ( false == $boolIsReverse ) ? __( 'Invoice date is required.' ) : __( 'Date Cancelled is required.' );
			} else {
				$strMessage   = ( false == $boolIsReverse ) ? __( 'Invoice date is required.' ) : __( 'Date is required.' );
			}

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		} elseif( false == CValidation::validateDate( $strPostDate ) ) {
			$boolIsValid &= false;
			$strMessage	  = ( false == $boolIsReverse ) ? __( 'Invalid invoice date or invoice date should be in mm/dd/yyyy format.' ) : __( 'Date of format mm/dd/yyyy is required.' );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		} elseif( true == $boolIsReverse && true == valStr( $strPostDate ) && strtotime( $strPostDate ) < strtotime( $this->m_objApHeader->getPostDate() ) ) {
			$boolIsValid &= false;
			$strMessage   = ( CModule::INVOICES_POST_MONTH == $intModule ) ? __( 'Date should not be less than invoice date ( {%s, 0} ).', [ $this->m_objApHeader->getPostDate() ] ) : __( 'Date should not be less than payment date ( {%s, 0} ).', [ $this->m_objApHeader->getPostDate() ] );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		} elseif( false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getPostDate() ] ) ) ) {
			$boolIsValid &= false;
			$strMessage	  = __( 'Invoice date should be in mm/dd/yyyy format.' );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );

		}

		return $boolIsValid;
	}

	public function valDueDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getApPaymentId() ) && false == valStr( $this->m_objApHeader->getDueDate() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Invoice due date is required.' ) ) );
		} elseif( true == is_null( $this->m_objApHeader->getApPaymentId() ) && ( false == CValidation::validateDate( $this->m_objApHeader->getDueDate() )
					|| false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getDueDate() ] ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Invalid invoice due date or invoice due date should be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostDateAndDueDate() {

		if( false == ( int ) \Psi\CStringService::singleton()->substr( $this->m_objApHeader->getPostDate(), 0, 2 ) || false == valStr( $this->m_objApHeader->getDueDate() ) ) {
			return true;
		}

		$strFormattedPostDate	= date( 'm/d/Y', strtotime( $this->m_objApHeader->getPostDate() ) );
		$strFormattedDueDate	= date( 'm/d/Y', strtotime( $this->m_objApHeader->getDueDate() ) );

		$intPostDate	= mktime( 0, 0, 0, ( int ) \Psi\CStringService::singleton()->substr( $strFormattedPostDate, 0, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strFormattedPostDate, 3, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strFormattedPostDate, 6, 4 ) );
		$intDueDate		= mktime( 0, 0, 0, ( int ) \Psi\CStringService::singleton()->substr( $strFormattedDueDate, 0, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strFormattedDueDate, 3, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strFormattedDueDate, 6, 4 ) );

		if( $intPostDate > $intDueDate ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Invoice due date should be greater than or equal to invoice date.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valScheduledPaymentDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionAmount( $boolUseItemCatalog, $objClientDatabase = NULL ) {
		$boolIsValid		= true;
		$arrobjApDetails	= $this->m_objApHeader->getApDetails();

		if( true == is_null( $this->m_objApHeader->getTransactionAmount() ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Total amount is required.' ) ) );

		} elseif( 0 >= $this->m_objApHeader->getTransactionAmount() && true == $boolUseItemCatalog ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Total amount should be greater than zero.' ) ) );

		} elseif( 1000000000 < abs( $this->m_objApHeader->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Transaction amount should be between {%m, 0, p:0} and {%m, 1, p:0}.', [ - 1000000000, 1000000000 ] ) ) );
		}

		if( CApHeaderType::INVOICE == $this->m_objApHeader->getApHeaderTypeId() && true == valArr( $arrobjApDetails ) ) {

			$fltTotalTransactionAmount = 0.00;

			foreach( $arrobjApDetails as $objApDetail ) {
				$fltTotalTransactionAmount = bcadd( $fltTotalTransactionAmount, $objApDetail->getTransactionAmount(), 2 );
			}

			if( 0 != bccomp( $fltTotalTransactionAmount, $this->m_objApHeader->getTransactionAmount(), 2 ) ) {
				$boolIsValid = false;

				$strHeaderNumbers = $this->m_objApHeader->getHeaderNumber();
				if( true == valId( $this->m_objApHeader->getApPaymentId() ) ) {
					$strHeaderNumbers = implode( ',', array_keys( rekeyObjects( 'HeaderNumber', ( array ) CApHeaders::fetchHeaderNumbersByApHeaderIdsByCid( array_filter( explode( ',', $this->m_objApHeader->getHeaderNumber() ) ), $this->m_objApHeader->getCid(), $objClientDatabase ) ) ) );
				}

				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', __( 'Invoice total and the sum of the detail lines should be the same for invoice number \'{%s, 0}\'.', [ $strHeaderNumbers ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsDeletedApDetails( $arrintPaymentApDetailIds, $arrintApHeaderIds, $boolIsFromViewCheck, $intClientId, $objClientDatabase = NULL ) {

		$boolIsValid				= true;
		$boolIsDeletedApDetails		= false;
		$arrintApDetailIds			= [];
		$arrobjApAllocations		= ( array ) \Psi\Eos\Entrata\CApAllocations::createService()->fetchApAllocationsByLumpApHeaderIdsByApDetailIdsByCid( $arrintApHeaderIds, $arrintPaymentApDetailIds, $intClientId, $objClientDatabase );
		$arrobjDeletedApDetails		= ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchDeletedApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intClientId, $objClientDatabase );

		foreach( $arrobjApAllocations as $objApAllocation ) {
			$arrintApDetailIds[$objApAllocation->getChargeApDetailId()] = $objApAllocation->getChargeApDetailId();
			$arrintApDetailIds[$objApAllocation->getCreditApDetailId()] = $objApAllocation->getCreditApDetailId();
		}

		foreach( $arrobjDeletedApDetails as $intApDetailId => $objApDetail ) {
			if( true == in_array( $intApDetailId, $arrintApDetailIds ) ) {
				$boolIsDeletedApDetails = true;
				break;
			}
		}

		if( true == $boolIsDeletedApDetails && true == $boolIsFromViewCheck ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You cannot reverse the voided payment, as the associate invoice(s) line item(s) have been deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valHeaderNumber( $objClientDatabase = NULL ) {

		$boolIsValid	= true;
		$strWhere		= 'WHERE cid = ' . ( int ) $this->m_objApHeader->getCid() . '
							AND header_number = \'' . addslashes( $this->m_objApHeader->getHeaderNumber() ) . '\'
							AND deleted_on IS NOT NULL';

		if( CApHeaderType::INVOICE == $this->m_objApHeader->getApHeaderTypeId() ) {

			if( true == is_null( $this->m_objApHeader->getApPaymentId() ) && true == is_null( $this->m_objApHeader->getHeaderNumber() ) ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_number', __( 'Invoice number is required.' ) ) );
			} elseif( false == is_null( $this->m_objApHeader->getHeaderNumber() ) && true == preg_match( '/(http|https):\/\//i', $this->m_objApHeader->getHeaderNumber() ) ) {

				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_number', __( 'Invalid invoice number.' ) ) );
			}
		} elseif( CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId()
				&& true == valStr( $this->m_objApHeader->getHeaderNumber() )
				&& 0 < CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Purchase Order does not exist.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth( $intModule, $objCompanyUser, $boolIsReverse = false, $boolApDetailExists = false, $boolIsHistoricalInvoice = false, $boolIsFromRecurringScript = false ) {

		$boolIsValid = true;
		$strMessage					= NULL;
		$arrstrGlLockedProperties	= [];
		$arrstrOutOfRangeProperties	= [];

		if( CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId() ) {

			$strPostMonth = $this->m_objApHeader->getPostMonth();

			if( false == valStr( $strPostMonth ) ) {

				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post Month is required.' ) ) );
			} elseif( false == CValidation::validateDate( $strPostMonth ) ) {

				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Invalid post month.' ) ) );
			}

			return $boolIsValid;
		}

		$strPostMonth = ( false == $boolIsReverse ) ? $this->m_objApHeader->getPostMonth() : $this->m_objApHeader->getReversePostMonth();

		if( false == valStr( $strPostMonth ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post Month is required.' ) ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $strPostMonth ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post Month of format mm/yyyy is required.' ) ) );
		}

		if( true == is_null( $objCompanyUser ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Company user not found.' ) ) );
		}

		if( true == is_null( $intModule ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Module id not found.' ) ) );
		}

		if( true == valArr( $this->m_objApHeader->getPoApHeaderIds() )
			&& ( false == valArr( $this->m_objApHeader->getApDetails() )
			|| false == valArr( $this->m_objApHeader->getPropertyGlSettings() )
			|| false == valArr( $this->m_objApHeader->getProperties() ) )
			&& false == $boolApDetailExists ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The properties on the selected allocation do not match the properties on the attached PO. Please select a new property and try again.<br>', [ $this->m_objApHeader->getHeaderNumber() ] ) ) );
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == $boolIsReverse && ( true == strtotime( $strPostMonth ) < strtotime( $this->m_objApHeader->getPostMonth() ) ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post Month should not be less than the original Post Month ( {%t, 0, DATE_NUMERIC_POSTMONTH} ).', [ strtotime( $this->m_objApHeader->getPostMonth() ) ] ) ) );
		}

		$arrobjPropertyGlSettings	= rekeyObjects( 'PropertyId', $this->m_objApHeader->getPropertyGlSettings() );
		$arrobjApDetails			= rekeyObjects( 'PropertyId', $this->m_objApHeader->getApDetails() );

		foreach( array_keys( $arrobjApDetails ) as $intPropertyId ) {

			$objPropertyGlSetting = ( true == valArr( $arrobjPropertyGlSettings ) && true == array_key_exists( $intPropertyId, $arrobjPropertyGlSettings ) )? $arrobjPropertyGlSettings[$intPropertyId] : NULL;

			// If no property gl setting available or property is in migration mode and intial import is on then skip iteration.
			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
				|| ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && 1 == $this->m_objApHeader->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() && 1 != $this->m_objApHeader->getIsInitialImportPosted() ) ) {
				continue;
			}

			// Validate for AP/GL lock period
			if( true == array_key_exists( $intPropertyId, $this->m_objApHeader->getProperties() ) && ( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) || strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) ) {
				$arrstrGlLockedProperties[] = getArrayElementByKey( $intPropertyId, $this->m_objApHeader->getProperties() )->getPropertyName();
			}

			// If logged in user is administrator then no need to check post month's range permission
			if( 1 == $objCompanyUser->getIsAdministrator() || true == $boolIsFromRecurringScript ) {
				continue;
			}

			$intPastPostMonth	= strtotime( $objPropertyGlSetting->getApPostMonth() );
			$intFuturePostMonth	= strtotime( $objPropertyGlSetting->getApPostMonth() );

			$arrobjCompanyUserPreferences = ( array ) $this->m_objApHeader->getCompanyUserPreferences();

			// Create range of default AP post month based on user permission
			foreach( $arrobjCompanyUserPreferences as $objCompanyUserPreference ) {

				if( false == is_null( $objCompanyUserPreference->getValue() ) && 0 < $objCompanyUserPreference->getValue() ) {

					if( CModule::INVOICES_POST_MONTH == $intModule ) {

						// Subtract months to ap_post_month
						if( CCompanyUserPreference::INVOICES_PAST_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intPastPostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' -' . $objCompanyUserPreference->getValue() . ' month' );
						}

						// Add months to ap_post_month
						if( CCompanyUserPreference::INVOICES_FUTURE_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intFuturePostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' +' . $objCompanyUserPreference->getValue() . ' month' );
						}

					} elseif( CModule::CHECKS_POST_MONTH == $intModule ) {

						// Subtract months to ap_post_month
						if( CCompanyUserPreference::CHECKS_PAST_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intPastPostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' -' . $objCompanyUserPreference->getValue() . ' month' );
						}

						// Add months to ap_post_month
						if( CCompanyUserPreference::CHECKS_FUTURE_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intFuturePostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' +' . $objCompanyUserPreference->getValue() . ' month' );
						}
					}
				}
			}

			// Validate for range of default AP post month based on user permissions
			if( true == array_key_exists( $intPropertyId, $this->m_objApHeader->getProperties() )
				&& ( false == valArr( $this->m_objApHeader->getCompanyUserPropertyIds() ) || true == in_array( $intPropertyId, $this->m_objApHeader->getCompanyUserPropertyIds() ) )
				&& ( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) ) {

				$arrstrOutOfRangeProperties[] = getArrayElementByKey( $intPropertyId, $this->m_objApHeader->getProperties() )->getPropertyName();
			}
		}

		if( true == valArr( $arrstrOutOfRangeProperties ) ) {

			$boolIsValid &= false;
			$arrstrOutOfRangeProperties = array_unique( $arrstrOutOfRangeProperties );

			// If you change below error message then check its ocurrence in the CApPaymentsController.class.php in consolidateErrorMesssages() method.
			if( CModule::INVOICES_POST_MONTH == $intModule ) {
				$strMessage = __( 'Post month is not in range for property(s) \'{%s, 0}\' of invoice number {%s, 1}.', [ implode( '\', \'', $arrstrOutOfRangeProperties ), $this->m_objApHeader->getHeaderNumber() ] );
			} elseif( CModule::CHECKS_POST_MONTH == $intModule ) {
				$strMessage = __( 'Post month is not in range for property(s) \'{%s, 0}\'.', [ implode( '\', \'', $arrstrOutOfRangeProperties ) ] );
			}

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', $strMessage ) );
		}

		if( false == $boolIsHistoricalInvoice && true == valArr( $arrstrGlLockedProperties ) ) {

			$boolIsValid &= false;

			if( CModule::INVOICES_POST_MONTH == $intModule ) {
				if( CApHeaderSubType::MANAGEMENT_FEE_INVOICE == $this->m_objApHeader->getApHeaderSubTypeId() ) {
					$strMessage = __( 'AP is locked for property(s) \'{%s, 0}\'. Please open AP Period or edit the Post Month associated with the management fee you are trying to process.', [ implode( '\', \'', array_unique( $arrstrGlLockedProperties ) ), $this->m_objApHeader->getHeaderNumber() ] );
				} else {
					$strMessage = __( 'GL or AP is locked for property(s) \'{%s, 0}\' of invoice number {%s, 1}.', [ implode( '\', \'', array_unique( $arrstrGlLockedProperties ) ), $this->m_objApHeader->getHeaderNumber() ] );
				}
			} elseif( CModule::CHECKS_POST_MONTH == $intModule ) {
				$strMessage = __( 'GL or AP is locked for property(s) \'{%s, 0}\'.', [ implode( '\', \'', array_unique( $arrstrGlLockedProperties ) ) ] );
			}

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', $strMessage ) );
		}

		// Validation to check earlier post month for payment ap header
		$arrstrHeaderNumbers = [];

		foreach( $this->m_objApHeader->getOriginalApHeaders() as $objOriginalApHeader ) {
			if( strtotime( $this->m_objApHeader->getPostMonth() ) < strtotime( $objOriginalApHeader->getPostMonth() ) ) {
				$arrstrHeaderNumbers[] = $objOriginalApHeader->getHeaderNumber();
			}
		}

		if( true == valArr( $arrstrHeaderNumbers ) ) {
			$boolIsValid &= false;
			$arrstrHeaderNumbers = array_unique( $arrstrHeaderNumbers );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'The payment post month for invoice(s) \'{%s, 0}\' is earlier than the invoice\'s post month.', [ implode( ',', $arrstrHeaderNumbers ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInvoiceExists( $objClientDatabase ) {

		$boolIsValid = true;

		$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR', $this->m_objApHeader->getCid(), $objClientDatabase );

		if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) return $boolIsValid;

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->m_objApHeader->getCid() . '
						AND LOWER( header_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->m_objApHeader->getHeaderNumber() ) ) . '\'
						AND ap_payee_id = ' . ( int ) $this->m_objApHeader->getApPayeeId() . '
						AND id != ' . ( int ) $this->m_objApHeader->getId() . '
						AND reversal_ap_header_id IS NULL
						AND COALESCE( ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE;

		if( 0 < CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase ) ) {

			$objApPayee	= CApPayees::fetchApPayeeByIdByApPayeeLocationIdByCid( $this->m_objApHeader->getApPayeeId(), $this->m_objApHeader->getApPayeeLocationId(), $this->m_objApHeader->getCid(), $objClientDatabase, true );

			if( false == valObj( $objApPayee, 'CApPayee' ) ) {
				$objApPayee = CApPayees::fetchApPayeeByIdByApPayeeLocationIdByCid( $this->m_objApHeader->getApPayeeId(), $this->m_objApHeader->getApPayeeLocationId(), $this->m_objApHeader->getCid(), $objClientDatabase, false );
			}

			$strApPayeeName = $objApPayee->getCompanyName();

			if( false == is_null( $objApPayee->getLocationName() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getLocationName();
			}

			if( false == is_null( $objApPayee->getCity() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getCity();
			}

			if( false == is_null( $objApPayee->getStateCode() ) ) {
				$strApPayeeName .= ', ' . $objApPayee->getStateCode();
			}

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_invoice_exists', __( 'This invoice number "{%s, 0}" has already been used for "{%s, 1}" vendor. Please supply a different invoice number. ', [ $this->m_objApHeader->getHeaderNumber(), $strApPayeeName ] ) ) );

			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valIsApPayeeLocked() {
		$boolIsValid = true;

		if( CApPayeeStatusType::LOCKED == $this->m_objApHeader->getApPayeeStatusTypeId()
			&& CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId() ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_status_type_id', __( 'Vendor is on hold status.' ) ) );

		} elseif( CApPayeeStatusType::LOCKED == $this->m_objApHeader->getApPayeeStatusTypeId()
				&& CApHeaderType::INVOICE == $this->m_objApHeader->getApHeaderTypeId() ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_status_type_id', __( 'Invoice "{%s, 0}" is associated to a vendor on hold "{%s, 1}" and cannot be processed. <br/>', [ $this->m_objApHeader->getHeaderNumber(), $this->m_objApHeader->getApPayeeName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsApPayeeInactive() {

		$boolIsValid = true;

		if( CApPayeeStatusType::INACTIVE == $this->m_objApHeader->getApPayeeStatusTypeId() ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee', __( 'Vendor is inactive.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPoNumber( $objClientDatabase ) {

		$boolIsValid				= true;
		$arrintApHeaderSubTypeIds	= [ CApHeaderSubType::CATALOG_PURCHASE_ORDER, CApHeaderSubType::STANDARD_PURCHASE_ORDER ];

		if( true == valArr( $this->m_objApHeader->getPoApHeaderIds() ) ) {

			$strWhere = 'WHERE cid = ' . ( int ) $this->m_objApHeader->getCid() . '
							AND id IN( ' . implode( ',', $this->m_objApHeader->getPoApHeaderIds() ) . ' )
							AND deleted_on IS NOT NULL
							AND ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
							AND ap_header_sub_type_id IN ( ' . implode( ', ', $arrintApHeaderSubTypeIds ) . ' ) ';

			if( 0 < CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'PO does not exist.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valHeaderMemo( $strAction ) {
		$boolIsValid = true;

		$strMemoMsg = 'Memo is required.';

		if( 'void_check' == $strAction ) {
			$strMemoMsg = 'Note is required.';
		}

		if( true == is_null( $this->m_objApHeader->getHeaderMemo() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_memo', __( $strMemoMsg ) ) );
		}

		return $boolIsValid;
	}

	public function valGlPosting() {

		$boolIsValid	= true;

		$arrstrInactiveGlPostingProperties 	= [];

		$arrobjPropertyGlSettings = ( array ) $this->m_objApHeader->getPropertyGlSettings();

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {

			// If property is in migration mode and intial import is on then skip iteration.
			if( 1 == $this->m_objApHeader->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() ) {
				continue;
			}

			if( false == $objPropertyGlSetting->getActivateStandardPosting() ) {

				$boolIsValid &= false;

				if( true == array_key_exists( $objPropertyGlSetting->getPropertyId(), $this->m_objApHeader->getProperties() ) ) {
					$arrstrInactiveGlPostingProperties[] = getArrayElementByKey( $objPropertyGlSetting->getPropertyId(), $this->m_objApHeader->getProperties() )->getPropertyName();
				}
			}
		}

		if( true == valArr( $arrstrInactiveGlPostingProperties ) ) {
			$arrstrInactiveGlPostingProperties = array_unique( $arrstrInactiveGlPostingProperties );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_standard_posting', 'GL posting is not enabled for property(s) \'' . implode( ',', $arrstrInactiveGlPostingProperties ) . '\'.' ) );
		}

		return $boolIsValid;
	}

	public function valControlTotal() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getControlTotal() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', __( 'Control total is required.' ) ) );

		} elseif( false == is_numeric( $this->m_objApHeader->getControlTotal() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', __( 'Invalid Control total.' ) ) );
		} elseif( 9999999999999.99 < $this->m_objApHeader->getControlTotal() ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'control_total', __( 'Control total should not be greater than 9,999,999,999,999.99.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsUnpaidInvoices( $arrobjApHeaders ) {
		$boolIsValid = true;
		$arrstrHeaderNumber = [];

		foreach( $arrobjApHeaders as $objApHeader ) {
			if( 0 != $objApHeader->getPaidAmount() ) {
				$boolIsValid &= false;
				$arrstrHeaderNumber[] = $objApHeader->getHeaderNumber();
			}
		}

		if( false == $boolIsValid ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'partially_paid', __( 'Unable to change the bank account for invoices " {%s, 0} ". A partial payment has already been made.', [ implode( ',', $arrstrHeaderNumber ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valQuickPay( $objClientDatabase ) {

		$boolIsValid			= true;
		$arrstrBankAccountName	= [];
		$arrobjApDetails		= ( array ) $this->m_objApHeader->getApDetails();
		$arrobjBankAccounts		= ( array ) CBankAccounts::fetchBankAccountsByIdsByCid( array_keys( rekeyObjects( 'BankAccountId', $arrobjApDetails ) ), $this->m_objApHeader->getCid(), $objClientDatabase );

		if( 0 != $this->m_objApHeader->getIsOnHold() ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_on_hold', __( 'Selected invoice "{%s, 0}" is on hold and cannot be paid.', [ $this->m_objApHeader->getHeaderNumber() ] ) ) );
		}

		foreach( $arrobjBankAccounts as $objBankAccount ) {

			if( 1 == $objBankAccount->getIsCreditCardAccount() ) {
				$arrstrBankAccountName[$objBankAccount->getId()] = $objBankAccount->getAccountName();
			}
		}

		if( true == valArr( $arrstrBankAccountName ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This feature is not available for payment from "{%s, 0}" because it is a credit card account. Please select a different bank account for payment of this invoice, or pay using the Finalize Payments screen.', [ implode( '", "', $arrstrBankAccountName ) ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valIsNonRefundInvoice( $boolIsReverseFMO = false, $objClientDatabase ) {

		$boolIsValid = true;

		if( true == $boolIsReverseFMO ) return $boolIsValid;

		if( false == is_null( $this->m_objApHeader->getRefundArTransactionId() ) ) {

			$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByIdByCid( $this->m_objApHeader->getRefundArTransactionId(), $this->m_objApHeader->getCid(), $objClientDatabase );

			if( 1 != $objArTransaction->getIsDeleted() ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Refund invoice can not be deleted or reversed. First reverse the refund payable/FMO from the resident\'s ledger/profile.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsInvoicePending() {

		$boolIsValid = true;

		if( 0 != $this->m_objApHeader->getIsPosted()
			|| false == is_null( $this->m_objApHeader->getReversalApHeaderId() )
			|| 0 != bccomp( $this->m_objApHeader->getTransactionAmountDue(), $this->m_objApHeader->getTransactionAmount(), 2 ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_posted', __( 'Unable to delete the Invoice "{%s, 0}". ', [ $this->m_objApHeader->getHeaderNumber() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInvoiceReversible( $boolIsReverseFMO = false ) {

		$boolIsValid = true;

		if( true == $boolIsReverseFMO ) return $boolIsValid;

		if( false == is_null( $this->m_objApHeader->getReversalApHeaderId() ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_posted', __( 'This invoice is already reversed.' ) ) );
		}

		if( 0 != bccomp( $this->m_objApHeader->getTransactionAmountDue(), $this->m_objApHeader->getTransactionAmount(), 2 ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_posted', __( 'Paid invoice cannot be reversed.' ) ) );
		}

		return $boolIsValid;
	}

	public function valActiveApPayeeLocation() {

		$boolIsValid = true;

		$objApPayeeLocation = $this->m_objApHeader->getApPayeeLocation();

		if( true == valObj( $objApPayeeLocation, 'CApPayeeLocation' ) && false == is_null( $objApPayeeLocation->getDisabledBy() ) ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You have selected a disabled vendor location.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valApDetails() {

		$boolIsValid = true;

		if( false == valArr( $this->m_objApHeader->getApDetails() ) ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least one detail line item is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPropertyBankAccounts() {

		$boolIsValid = true;

		if( false == valArr( $this->m_objApHeader->getPropertyBankAccounts() ) ) {
			return $boolIsValid;
		}

		$arrmixAssociatedBankAccountPropertyIds = [];
		$arrmixUnAssociatedBankAccountProperties = [];

		$arrobjPropertyBankAccounts = $this->m_objApHeader->getPropertyBankAccounts();
		$arrobjProperties 			= $this->m_objApHeader->getProperties();

		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
			$arrmixAssociatedBankAccountPropertyIds[$objPropertyBankAccount->getBankAccountId()][$objPropertyBankAccount->getPropertyId()] = $objPropertyBankAccount->getPropertyId();
		}

		$arrobjApDetails = ( array ) $this->m_objApHeader->getApDetails();
		foreach( $arrobjApDetails as $objApDetail ) {

			// Check if the property is not associated with the bank account.
			if( true == array_key_exists( $objApDetail->getBankAccountId(), $arrmixAssociatedBankAccountPropertyIds ) && false == in_array( $objApDetail->getPropertyId(), $arrmixAssociatedBankAccountPropertyIds[$objApDetail->getBankAccountId()] ) ) {
				$arrmixUnAssociatedBankAccountProperties[$objApDetail->getBankAccountId()][$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
			}
		}

		foreach( $arrmixUnAssociatedBankAccountProperties as $arrstrUnAssociatedBankAccountProperties ) {

			// @FIXME - Show bankname in error message - $arrobjBankAccounts[$intBankAccountId]->getAccountName() - $objApDetail->getAccountName()
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The property(s) \'{%s, 0}\' is not associated with bank account.', [ implode( ', ', $arrstrUnAssociatedBankAccountProperties ) ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDisabledProperty( $boolShowDisabledData = false ) {

		$boolIsValid = true;

		$arrobjApDetails	= ( array ) $this->m_objApHeader->getApDetails();
		$arrobjProperties	= ( array ) $this->m_objApHeader->getProperties();

		if( false == valArr( $arrobjApDetails ) || false == valArr( $arrobjProperties ) ) {
			return $boolIsValid;
		}

		$arrstrPropertyNames = [];
		foreach( $arrobjApDetails as $objApDetail ) {

			if( true == array_key_exists( $objApDetail->getPropertyId(), $arrobjProperties ) && true == $arrobjProperties[$objApDetail->getPropertyId()]->getIsDisabled() ) {
				$arrstrPropertyNames[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
			}
		}

		if( true == valArr( $arrstrPropertyNames ) && false == $boolShowDisabledData ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The property(s) \'{%s, 0}\' are disabled.', [ implode( ', ', $arrstrPropertyNames ) ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valApPayeeProperties( $boolIsFromApprovePo = false, $boolShowDisabledData = false ) {

		$boolIsValid = true;

		if( true == valId( $this->m_objApHeader->getApPayeeId() ) ) {
			$arrstrPropertyNames         = [];
			$arrstrDisabledPropertyNames = [];

			$arrobjApDetails         = ( array ) rekeyObjects( 'PropertyId', $this->m_objApHeader->getApDetails() );
			$arrobjApPayeeProperties = ( array ) rekeyObjects( 'PropertyId', $this->m_objApHeader->getApPayeeProperties() );
			$arrobjProperties        = ( array ) $this->m_objApHeader->getProperties();

			foreach( $arrobjApDetails as $objApDetail ) {

				if( false == is_null( $objApDetail->getDeletedOn() ) && false == is_null( $objApDetail->getDeletedBy() ) ) {
					continue;
				}

				if( false == array_key_exists( $objApDetail->getPropertyId(), $arrobjApPayeeProperties ) && true == isset( $arrobjProperties[$objApDetail->getPropertyId()] ) ) {
					$arrstrPropertyNames[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
				} elseif( true == isset( $arrobjProperties[$objApDetail->getPropertyId()] ) && true == $arrobjProperties[$objApDetail->getPropertyId()]->getIsDisabled() ) {
					$arrstrDisabledPropertyNames[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
				}
			}

			if( true == $boolIsFromApprovePo && CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId() ) {
				$strErrorMsg = new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'PO#{%s,0} : The property(s) \'{%s, 1}\' is/are not associated with selected vendor location.', [ $this->m_objApHeader->getHeaderNumber(), implode( ', ', $arrstrPropertyNames ) ] ) );
			} else {
				$strErrorMsg = new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( ' The property(s) \'{%s, 0}\' is/are not associated with selected vendor location.', [ implode( ', ', $arrstrPropertyNames ) ] ) );
			}

			if( true == valArr( $arrstrPropertyNames ) ) {
				$this->m_objApHeader->addErrorMsg( $strErrorMsg );
				$boolIsValid = false;
			}

			if( true == valArr( $arrstrDisabledPropertyNames ) && true != $boolShowDisabledData ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Disabled property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrDisabledPropertyNames ) ] ) ) );
				$boolIsValid = false;
			}

			return $boolIsValid;
		}
	}

	public function valApPayee() {

		$boolIsValid = true;

		$objApPayee = $this->m_objApHeader->getApPayee();

		if( CApPayeeType::INTERCOMPANY != $objApPayee->getApPayeeTypeId() ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Vendor \'{%s, 0}\' is not of type Client.', [ $objApPayee->getCompanyName() ] ) ) );
			$boolIsValid = false;
		} elseif( CApPayeeStatusType::ACTIVE != $objApPayee->getApPayeeStatusTypeId() ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Vendor \'{%s, 0}\' is not active.', [ $objApPayee->getCompanyName() ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDefaultBankAccountOrMigrationModeProperties( $boolIsFromManagementFees = false ) {

		$boolIsValid = true;

		$arrstrApMigrationModeProperties		= [];
		$arrstrUnAssociatedPropertyBankAccounts = [];

		$arrobjApDetails			= ( array ) rekeyObjects( 'PropertyId', $this->m_objApHeader->getApDetails() );
		$arrobjPropertyGlSettings	= ( array ) rekeyObjects( 'PropertyId', $this->m_objApHeader->getPropertyGlSettings() );
		$arrobjProperties			= ( array ) $this->m_objApHeader->getProperties();
		foreach( $arrobjApDetails as $objApDetail ) {

			if( true == array_key_exists( $objApDetail->getPropertyId(), $arrobjPropertyGlSettings ) && true == is_null( $arrobjPropertyGlSettings[$objApDetail->getPropertyId()]->getApBankAccountId() ) ) {
				$arrstrUnAssociatedPropertyBankAccounts[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
			}

			if( true == $boolIsFromManagementFees && true == array_key_exists( $objApDetail->getPropertyId(), $arrobjPropertyGlSettings ) && true == $arrobjPropertyGlSettings[$objApDetail->getPropertyId()]->getIsApMigrationMode() ) {
				$arrstrApMigrationModeProperties[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();
			}
		}

		if( true == valArr( $arrstrUnAssociatedPropertyBankAccounts ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property(s) \'{%s, 0}\' is not associated with default bank account.', [ implode( ', ', $arrstrUnAssociatedPropertyBankAccounts ) ] ) ) );
			$boolIsValid = false;
		}

		if( true == valArr( $arrstrApMigrationModeProperties ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property(s) \'{%s, 0}\' is in AP Migration Mode and cannot process Management Fees. Please contact Entrata to disable AP Migration Mode.', [ implode( ', ', $arrstrApMigrationModeProperties ) ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUserAssignedProperties( $boolIsPostRequest = false, $boolIsFromRecurringScript = false ) {
		$boolIsValid = true;

		if( true == $boolIsFromRecurringScript ) return $boolIsValid;

		if( false == valArr( $this->m_objApHeader->getCompanyUserPropertyIds() ) || false == valArr( $this->m_objApHeader->getApDetails() ) ) {
			return $boolIsValid;
		}

		$arrstrUnAssociatedCompanyUserProperties	= [];
		$arrobjProperties							= ( array ) $this->m_objApHeader->getProperties();
		$arrobjApDetails							= ( array ) $this->m_objApHeader->getApDetails();

		foreach( $arrobjApDetails as $objApDetail ) {

			if( false == in_array( $objApDetail->getPropertyId(), $this->m_objApHeader->getCompanyUserPropertyIds() )
				&& ( false == $boolIsPostRequest || true == is_null( $objApDetail->getApprovedOn() ) )
				&& true == array_key_exists( $objApDetail->getPropertyId(), $arrobjProperties ) ) {

				$arrstrUnAssociatedCompanyUserProperties[$objApDetail->getPropertyId()] = $arrobjProperties[$objApDetail->getPropertyId()]->getPropertyName();

				if( false == is_null( $objApDetail->getPoHeaderNumber() ) ) {
					$arrintPoHeaderNumber[] = $objApDetail->getPoHeaderNumber();
				}

			}
		}

		if( true == valArr( $arrintPoHeaderNumber ) ) {
			$arrintPoHeaderNumber = array_unique( $arrintPoHeaderNumber );
		}

		if( true == valArr( $arrintPoHeaderNumber ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_number', __( 'We could not load Po(s) \'{%s, 0}\' because the property is not currently one of your assigned properties.', [ implode( ', ', $arrintPoHeaderNumber ) ] ) ) );
			$boolIsValid = false;
		} else if( true == valArr( $arrstrUnAssociatedCompanyUserProperties ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property(s) \'{%s, 0}\' is/are not assigned to this user.', [ implode( ', ', $arrstrUnAssociatedCompanyUserProperties ) ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valIsGlExportBatchCreated( $objClientDatabase, $boolExportedTransactionsPermission, $arrintOldApDetailsIds ) {

		$boolIsValid = true;

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || true == $boolExportedTransactionsPermission ) {
			return $boolIsValid;
		}

		if( true == valArr( $arrintOldApDetailsIds ) ) {

			$strWhere = 'WHERE cid = ' . ( int ) $this->m_objApHeader->getCid() . '
			AND ap_detail_id IN ( ' . implode( ',', $arrintOldApDetailsIds ) . ' )
			AND offsetting_gl_detail_id IS NULL
			AND EXISTS (
				SELECT 1
				from accounting_export_associations aea
				WHERE aea.gl_detail_id = gl_details.id and
				 aea.cid = gl_details.cid
				)';

			if( 0 < CGlDetails::fetchGlDetailCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_export_batch_id', __( 'GL is exported for this transaction and you do not have permission to edit exported transactions.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsApExportBatchCreated( $boolExportedTransactionsPermission ) {

		if( true == $boolExportedTransactionsPermission ) {
			return true;
		}

		$boolIsValid		= true;
		$arrobjApDetails	= $this->m_objApHeader->getApDetails();

		foreach( $arrobjApDetails as $objApDetail ) {

			if( false == is_null( $objApDetail->getApExportBatchId() ) ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_export_batch_id', __( 'AP is exported for this transaction and you do not have permission to edit exported transactions.' ) ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valIsInvoicePaid( $objClientDatabase ) {

		$boolIsValid = true;

		$this->m_objApHeader->refreshField( 'transaction_amount', 'ap_headers', $objClientDatabase );
		$this->m_objApHeader->refreshField( 'transaction_amount_due', 'ap_headers', $objClientDatabase );

		if( 0 != bccomp( $this->m_objApHeader->getTransactionAmountDue(), $this->m_objApHeader->getTransactionAmount(), 2 ) ) {

			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_posted', __( 'You cannot unpost the selected invoice because there are active Payment(s) associated to it.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceTotalQuantity( $objClientDatabase, $boolCheckIsApDetailIdSet ) {

		$boolIsValid				= true;
		$objApHeader 				= CApHeaders::fetchApHeaderByIdByCid( $this->m_objApHeader->getId(), $this->m_objApHeader->getCid(), $objClientDatabase );
		$arrobjAssetTransactions	= ( array ) \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchActiveAssetTransactionsByIdsByCid( array_filter( array_keys( rekeyObjects( 'InventoryId', $this->m_objApHeader->getApDetails() ) ) ), $this->m_objApHeader->getCid(), $objClientDatabase );

		if( false == valArr( $arrobjAssetTransactions ) ) {
			return $boolIsValid;
		}

		$arrobjRekeyedApDetails	= rekeyObjects( 'InventoryId', $this->m_objApHeader->getApDetails() );

		foreach( $arrobjRekeyedApDetails as $intInventoryId => $objApDetail ) {

			if( false == valId( $objApDetail->getInventoryId() ) || false == array_key_exists( $intInventoryId, $arrobjAssetTransactions ) ) {
				continue;
			}

			if( true == valObj( $objApHeader, 'CApHeader' ) && false == valId( $objApHeader->getApBatchId() )
				&& true == $boolCheckIsApDetailIdSet && true == valId( $arrobjAssetTransactions[$intInventoryId]->getApDetailId() ) ) {

				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', __( 'Invoice is already created for selected purchase order.' ) ) );
				$boolIsValid &= false;
				break;
			} elseif( $objApDetail->getQuantityOrdered() > $arrobjAssetTransactions[$intInventoryId]->getQuantity() ) {

				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', 'Invoice \'' . $this->m_objApHeader->getHeaderNumber() . '\' quantity should be less than or equal to the received quantity.' ) );
				$boolIsValid &= false;
				break;
			}
		}
		return $boolIsValid;
	}

	public function valInvoiceSubTotalsAmount() {

		$boolIsValid		= true;
		$arrobjApDetails	= $this->m_objApHeader->getApDetails();

		if( false == valArr( $arrobjApDetails ) ) {
			return $boolIsValid;
		}

		$fltTaxAmount 		= 0;
		$fltDiscountAmount 	= 0;
		$fltShippingAmount	= 0;

		foreach( $arrobjApDetails as $objApDetail ) {

			$fltTaxAmount		+= $objApDetail->getTaxAmount();
			$fltDiscountAmount	+= $objApDetail->getDiscountAmount();
			$fltShippingAmount	+= $objApDetail->getShippingAmount();
		}

		if( round( $this->m_objApHeader->getTaxAmount(), 4 ) > round( $fltTaxAmount, 4 )
			|| round( $this->m_objApHeader->getDiscountAmount(), 4 ) > round( abs( $fltDiscountAmount ), 4 )
			|| round( $this->m_objApHeader->getShippingAmount(), 4 ) > round( $fltShippingAmount, 4 ) ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_amount', __( 'Invoice sub-totals is not distributed properly. Please click on Distribute On Line Items button.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valIsValidForPayment( $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valId( $this->m_objApHeader->getIsPosted() ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_posted', NULL ) );
			return false;
		}

		if( false == valObj( $objClientDatabase, 'CDataBase' ) ) {
			return boolIsValid;
		}

		$intValidApDetailsCount = CApDetails::fetchCountOfApprovedUnpaidApDetailsByApHeaderIdByCid( $this->m_objApHeader->getId(), $this->m_objApHeader->getCid(), $objClientDatabase );

		if( 0 == $intValidApDetailsCount ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_approved_by', NULL ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valApCodeId( $boolIsApCodePresent ) {

		$boolIsValid = true;

		if( false == $boolIsApCodePresent && CApHeaderSubType::CATALOG_PURCHASE_ORDER == $this->m_objApHeader->getApHeaderSubTypeId() ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Catalog Item setup is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUseItemCatalog( $boolUseItemCatalog ) {

		$boolIsValid = true;

		if( false == $boolUseItemCatalog ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_item_catalog', __( 'Catalogs have been disabled. Please choose a different invoice type or re-enable Catalogs at Setup > Company > Financial > Catalogs.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUseItemReceive( $arrobjPoApHeaders ) {

		$boolIsValid					= true;
		$arrstrNotReceivedPoNumbers		= [];

		foreach( $arrobjPoApHeaders as $objPoApHeader ) {
			if( CApFinancialStatusType::APPROVED == $objPoApHeader->getApFinancialStatusTypeId()
				&& CApHeaderSubType::CATALOG_PURCHASE_ORDER == $objPoApHeader->getApHeaderSubTypeId()
				&& CApPhysicalStatusType::RECEIVED != $objPoApHeader->getApPhysicalStatusTypeId()
				&& CApPhysicalStatusType::PARTIALLY_RECEIVED != $objPoApHeader->getApPhysicalStatusTypeId() ) {
				$arrstrNotReceivedPoNumbers[] = $objPoApHeader->getHeaderNumber();
				continue;
			}
		}

		if( true == valArr( $arrstrNotReceivedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_physical_status_type', __( 'Item receiving must be entered for PO "{%s, 0}" before it can be invoiced.', [ implode( ', ', $arrstrNotReceivedPoNumbers ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPoFromContracts( $intApContractId, $arrobjPoApDetails ) {
		$boolIsValid	= true;

		if( false == valArr( $arrobjPoApDetails ) ) {
			return $boolIsValid;
		}

		$arrstrPoNumbers = [];
		foreach( $arrobjPoApDetails as $objPoApDetail ) {
			if( $intApContractId != $objPoApDetail->getApContractId() ) {
				$arrstrPoNumbers[] = $objPoApDetail->getPoHeaderNumber();
				continue;
			}
		}

		if( true == valArr( $arrstrPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'po_from_contracts', __( 'Purchase Order \'{%s, 0}\' is not associated with this contract.', [ implode( ', ', array_unique( $arrstrPoNumbers ) ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valPoFromJobs( $intJobPropertyId, $arrobjPoApDetails, $intJobId ) {
		$boolIsValid	= true;

		if( false == valArr( $arrobjPoApDetails ) ) {
			return $boolIsValid;
		}

		$arrstrPoNumbers	= [];
		$arrintPropertyIds	= array_keys( rekeyObjects( 'PropertyId', $arrobjPoApDetails ) );

		if( valId( $intJobId ) && true == valId( $intJobPropertyId ) ) {

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
				foreach( $arrobjPoApDetails as $objPoApDetail ) {
					if( $intJobPropertyId != $objPoApDetail->getPropertyId() ) {
						$arrstrPoNumbers[] = $objPoApDetail->getPoHeaderNumber();
						continue;
					}
				}

				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'po_from_jobs', __( 'Purchase Order \'{%s, 0}\' is multi-property.', [ implode( ', ', array_unique( $arrstrPoNumbers ) ) ] ) ) );
			}

			$boolIsValidPoForJob = true;
			$strPoNumber = NULL;
			foreach( $arrobjPoApDetails as $objPoApDetail ) {
				$strPoNumber = $objPoApDetail->getPoHeaderNumber();
				if( $intJobId != $objPoApDetail->getJobId() ) {
					$boolIsValidPoForJob = false;
					break;
				}
			}

			if( false == $boolIsValidPoForJob ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'po_from_jobs', __( 'Purchase Order \'{%s, 0}\' is not related to the current job.', $strPoNumber ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsInitialImport() {

		$boolIsValid = true;

		if( 1 == $this->m_objApHeader->getIsInitialImport() ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_initial_import', __( 'This is initial import invoice.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valApPayeeAccountNumber( $arrobjApRemittances ) {

		$boolIsValid 				= true;
		$arrobjTempApRemittances	= [];

		if( CApPaymentType::ACH != $this->m_objApHeader->getApPaymentTypeId() ) {
			return $boolIsValid;
		}

		foreach( $arrobjApRemittances as $objApRemittance ) {
			$arrobjTempApRemittances[$objApRemittance->getApPayeeId()][$objApRemittance->getApPaymentTypeId()] = $objApRemittance->getApPaymentTypeId();
		}

		if( false == array_key_exists( $this->m_objApHeader->getApPayeeId(), $arrobjTempApRemittances ) || false == array_key_exists( CApPaymentType::ACH, $arrobjTempApRemittances[$this->m_objApHeader->getApPayeeId()] ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Vendor \'{%s, 0} \' require at least one ACH remittance.', [ $this->m_objApHeader->getApPayeeName() ] ) ) );
			return false;
		}

		foreach( $arrobjApRemittances as $objApRemittance ) {

			if( CApPaymentType::ACH == $objApRemittance->getApPaymentTypeId() && NULL == $objApRemittance->getCheckAccountNumberEncrypted() ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Vendor \'{%s, 0} \' account number is required while generating ACH file.', [ $this->m_objApHeader->getApPayeeName() ] ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valPostDateForPo() {

		$boolIsValid = true;

		if( false == valStr( $this->m_objApHeader->getPostDate() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Purchase order date is required.' ) ) );
			return $boolIsValid;
		} elseif( false == CValidation::validateDate( $this->m_objApHeader->getPostDate() ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Purchase order date should be in mm/dd/yyyy format.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valApDetailsCount( $arrintRestrictedApDetailIds ) {

		$boolIsValid = true;

		if( false == valArr( $this->m_objApHeader->getApDetails() ) ) {

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least one detail line item is required.' ) ) );
			$boolIsValid &= false;

		} elseif( false == valArr( $arrintRestrictedApDetailIds ) ) {

			$boolIsValidData = false;
			foreach( $this->m_objApHeader->getApDetails() as $objApDetail ) {

				if( true == is_null( $objApDetail->getDeletedBy() ) && true == is_null( $objApDetail->getDeletedOn() ) ) {
					return $boolIsValid;
				}
			}

			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least one detail line item is required.' ) ) );
			return $boolIsValidData;
		}

		return $boolIsValid;
	}

	public function valApCodePropertyAssociation() {

		$boolIsValid			= true;
		$arrmixApCodeProperties	= ( array ) $this->m_objApHeader->getApCodeProperties();

		foreach( $this->m_objApHeader->getApDetails() as $objApDetail ) {

			if( true == valId( $objApDetail->getApCodeId()
				&& true == isset( $arrmixApCodeProperties[$objApDetail->getApCodeId()] )
				&& true == valArr( $arrmixApCodeProperties[$objApDetail->getApCodeId()] )
				&& false == isset( $arrmixApCodeProperties[$objApDetail->getApCodeId()][$objApDetail->getPropertyId()] ) ) ) {

				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Catalog item "{%s, 0}" is no longer associated to Property "{%s, 1}".', [ $objApDetail->getItemName(), $objApDetail->getPropertyName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCreateInvoiceFromPos( $arrobjPoApHeaders = [], $intApHeaderSubTypeId, $boolIsDuplicateInvoice, $boolIsFromTemplate, $arrintInvoiceTypePermissions, $arrintInitiallySelectedPoNumbers = [], $arrintAttachedPos = [] ) {

		$boolIsValid						= true;

		if( false == valArr( $arrobjPoApHeaders ) || false == valId( $intApHeaderSubTypeId ) ) {
			return $boolIsValid;
		}

		$arrstrPendingPoNumbers				= [];
		$arrstrClosedPoNumbers				= [];
		$arrstrCancelledPoNumbers			= [];
		$arrstrRejectedPoNumbers			= [];
		$arrstrMismatchedPoNumbers			= [];

		$arrstrMismatchedApHeaderSubTypes	= [];
		$arrstrDeniedApHeaderSubTypes		= [];
		$arrstrApHeaderSubTypes				= CApHeaderSubType::$c_arrstrApHeaderSubTypes;
		$arrstrPoApHeaderSubTypes			= CApHeaderSubType::$c_arrstrPoApHeaderSubTypes;

		foreach( $arrobjPoApHeaders as $objPoApHeader ) {

			if( $arrstrApHeaderSubTypes[$intApHeaderSubTypeId] != $arrstrPoApHeaderSubTypes[$objPoApHeader->getApHeaderSubTypeId()] ) {
				$arrstrMismatchedPoNumbers[]		= $objPoApHeader->getHeaderNumber();
				$arrstrMismatchedApHeaderSubTypes[]	= $arrstrPoApHeaderSubTypes[$objPoApHeader->getApHeaderSubTypeId()];
				continue;
			}

			if( false == in_array( $objPoApHeader->getHeaderNumber(), $arrintInitiallySelectedPoNumbers ) && false == in_array( $objPoApHeader->getHeaderNumber(), $arrintAttachedPos ) ) {

				switch( $objPoApHeader->getApFinancialStatusTypeId() ) {
					case CApFinancialStatusType::PENDING:
						$arrstrPendingPoNumbers[] = $objPoApHeader->getHeaderNumber();
						break;

					case CApFinancialStatusType::CLOSED:
						$arrstrClosedPoNumbers[] = $objPoApHeader->getHeaderNumber();
						break;

					case CApFinancialStatusType::CANCELLED:
						$arrstrCancelledPoNumbers[] = $objPoApHeader->getHeaderNumber();
						break;

					case CApFinancialStatusType::REJECTED:
						$arrstrRejectedPoNumbers[] = $objPoApHeader->getHeaderNumber();
						break;

					default:
				}

			}

		}

		if( true == valArr( $arrstrMismatchedApHeaderSubTypes ) && true == valArr( $arrintInvoiceTypePermissions ) ) {
			foreach( $arrstrMismatchedApHeaderSubTypes as $strApHeaderSubType ) {
				$intTempApHeaderSubTypeId = array_search( $strApHeaderSubType, $arrstrApHeaderSubTypes, true );
				if( false == in_array( $intTempApHeaderSubTypeId, $arrintInvoiceTypePermissions ) ) {
					$arrstrDeniedApHeaderSubTypes[$intTempApHeaderSubTypeId] = $arrstrApHeaderSubTypes[$intTempApHeaderSubTypeId];
				}
			}
		}

		if( true == valArr( $arrstrPendingPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_financial_status_type_id', __( 'Purchase Order \'{%s, 0}\' is pending approval.', [ implode( ', ', $arrstrPendingPoNumbers ) ] ) ) );
		}

		if( true == valArr( $arrstrClosedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_financial_status_type_id', __( 'Purchase Order "{%s, 0}" is closed.', [ implode( ', ', $arrstrClosedPoNumbers ) ] ) ) );
		}

		if( true == valArr( $arrstrCancelledPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_financial_status_type_id', __( 'Purchase Order "{%s, 0}" is canceled.', [ implode( ', ', $arrstrCancelledPoNumbers ) ] ) ) );
		}

		if( true == valArr( $arrstrRejectedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_financial_status_type_id', __( 'Purchase Order "{%s, 0}" is rejected.', [ implode( ', ', $arrstrRejectedPoNumbers ) ] ) ) );
		}

		if( true == valArr( $arrstrDeniedApHeaderSubTypes ) && true == valArr( $arrstrMismatchedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_sub_type_id', __( 'Purchase Order "{%s, 0}" is a "{%s, 1}" PO. User permission for "{%s, 2}" type Invoices is not enabled.', [ implode( ', ', $arrstrMismatchedPoNumbers ), implode( ', ', array_unique( $arrstrMismatchedApHeaderSubTypes ) ), implode( ', ', $arrstrDeniedApHeaderSubTypes ) ] ) ) );
		}

		if( true == $boolIsDuplicateInvoice && false == $boolIsFromTemplate && false == valArr( $arrstrDeniedApHeaderSubTypes ) && true == valArr( $arrstrMismatchedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_sub_type_id', __( 'Purchase Order "{%s, 0}" is a "{%s, 1}" PO. PO type must match Invoice type for a duplicate Invoice.', [ implode( ', ', $arrstrMismatchedPoNumbers ), implode( ', ', array_unique( $arrstrMismatchedApHeaderSubTypes ) ) ] ) ) );
		}

		if( true == $boolIsDuplicateInvoice && true == $boolIsFromTemplate && false == valArr( $arrstrDeniedApHeaderSubTypes ) && true == valArr( $arrstrMismatchedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_sub_type_id', __( 'Purchase Order "{%s, 0}" is a "{%s, 1}" PO. PO type must match Invoice type for an Invoice Template.', [ implode( ', ', $arrstrMismatchedPoNumbers ), implode( ', ', array_unique( $arrstrMismatchedApHeaderSubTypes ) ) ] ) ) );
		}

		if( false == $boolIsDuplicateInvoice && false == valArr( $arrstrDeniedApHeaderSubTypes ) && true == valArr( $arrstrMismatchedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_header_sub_type_id', __( 'Purchase Order types must be the same.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDuplicatePoNumber( array $arrintSelectedPoNumbers ) {

		$boolIsValid				= true;

		if( false == valArr( $arrintSelectedPoNumbers ) ) {
			return $boolIsValid;
		}

		$arrstrDuplicatePoNumber	= array_unique( array_diff_assoc( $arrintSelectedPoNumbers, array_unique( $arrintSelectedPoNumbers ) ) );

		if( true == valArr( $arrstrDuplicatePoNumber ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duplicate_po_number', __( 'Purchase Order "{%s, 0}"  already entered.', [ implode( ', ', $arrstrDuplicatePoNumber ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPoNumberExist( array $arrobjPoApHeaders, array $arrintSelectedPoNumbers ) {
		$boolIsValid	= true;

		if( false == valArr( $arrintSelectedPoNumbers ) ) {
			return $boolIsValid;
		}

		$arrstrPoNumbersNotFound			= [];
		$arrobjPoApHeadersByHeaderNumber	= ( true == valArr( $arrobjPoApHeaders ) ) ? rekeyObjects( 'HeaderNumber', $arrobjPoApHeaders ) : [];

		foreach( $arrintSelectedPoNumbers as $strPoNumber ) {
			if( false == valArr( $arrobjPoApHeadersByHeaderNumber ) || false == array_key_exists( $strPoNumber, $arrobjPoApHeadersByHeaderNumber ) ) {
				$arrstrPoNumbersNotFound[] = $strPoNumber;
			}
		}

		if( true == valArr( $arrstrPoNumbersNotFound ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_number', __( 'Purchase Order "{%s, 0}" not found.', [ implode( ', ', $arrstrPoNumbersNotFound ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valVendorAndAllowDuplicateInvoicesForPoNumber( array $arrobjPoApHeaders, array $arrobjPoApDetails, $boolAllowDuplicateInvoicesForStandardPos = false, $intApHeaderSubTypeId = NULL, array $arrintNewSelectedPOs ) {
		$boolIsValid	= true;

		if( false == valArr( $arrobjPoApHeaders ) ) {
			return $boolIsValid;
		}

		$arrstrMismatchedApHeaderSubTypes	= [];
		$arrstrMismatchedPoNumbers			= [];
		$arrstrApHeaderSubTypes				= CApHeaderSubType::$c_arrstrApHeaderSubTypes;
		$arrstrPoApHeaderSubTypes			= CApHeaderSubType::$c_arrstrPoApHeaderSubTypes;

		foreach( $arrobjPoApHeaders as $objPoApHeader ) {

			if( $arrstrApHeaderSubTypes[$intApHeaderSubTypeId] != $arrstrPoApHeaderSubTypes[$objPoApHeader->getApHeaderSubTypeId()] ) {
				$arrstrMismatchedPoNumbers[]        = $objPoApHeader->getHeaderNumber();
				$arrstrMismatchedApHeaderSubTypes[] = $arrstrPoApHeaderSubTypes[$objPoApHeader->getApHeaderSubTypeId()];
				continue;
			}
		}

		$arrstrInvoicedPoNumbers		= [];
		$arrobjPoApDetailsByApHeaderIds	= ( true == valArr( $arrobjPoApDetails ) ) ? rekeyObjects( 'ApHeaderId', $arrobjPoApDetails ) : [];

		$arrobjPoApHeadersByApPayeeId			= [];
		$arrobjPoApHeadersByApPayeeAccountId	= [];
		$arrobjPoApHeadersByApPayeeLocationId	= [];

		foreach( $arrobjPoApHeaders as $intPoApHeaderId => $objPoApHeader ) {
			if( false == $boolAllowDuplicateInvoicesForStandardPos && CApFinancialStatusType::CLOSED != $objPoApHeader->getApFinancialStatusTypeId()
				&& CApHeaderSubType::STANDARD_PURCHASE_ORDER == $objPoApHeader->getApHeaderSubTypeId()
			    && false == valArr( $arrstrMismatchedPoNumbers )
				&& ( false == valArr( $arrobjPoApDetailsByApHeaderIds ) || false == array_key_exists( $intPoApHeaderId, $arrobjPoApDetailsByApHeaderIds ) ) ) {
				$boolIsValid				&= false;
				$arrstrInvoicedPoNumbers[]	= $objPoApHeader->getHeaderNumber();
				continue;
			}
			$arrobjPoApHeadersByApPayeeId[$objPoApHeader->getApPayeeId()] = $objPoApHeader;
			$arrobjPoApHeadersByApPayeeAccountId[$objPoApHeader->getApPayeeAccountId()] = $objPoApHeader;
			$arrobjPoApHeadersByApPayeeLocationId[$objPoApHeader->getApPayeeLocationId()] = $objPoApHeader;
		}

		// validation for vendor, location and account
		if( false == valArr( $arrintNewSelectedPOs ) && ( 1 < \Psi\Libraries\UtilFunctions\count( array_keys( $arrobjPoApHeadersByApPayeeId ) ) || 1 < \Psi\Libraries\UtilFunctions\count( array_keys( $arrobjPoApHeadersByApPayeeLocationId ) ) || 1 < \Psi\Libraries\UtilFunctions\count( array_keys( $arrobjPoApHeadersByApPayeeAccountId ) ) ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_account', __( 'Vendor / Location / Account must match for all Purchase Orders entered.' ) ) );
		}

		if( true == valArr( $arrstrInvoicedPoNumbers ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allow_duplicate_po_number', __( 'Purchase Order "{%s, 0}" has already been used for an invoice and "Allow Multiple Invoices for Standard PO Line Items" is set to No.', [ implode( ', ', $arrstrInvoicedPoNumbers ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valApprovedByForPo() {

		$boolIsValid	= true;
		$intApprovedBy	= $this->m_objApHeader->getApprovedBy();

		if( false == valId( $intApprovedBy ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_by', __( 'Please approve the purchase order to close it.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Repeat frequency is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is required.' ) ) );
		} elseif( false == is_numeric( $this->m_objApHeader->getFrequencyInterval() ) || true == is_null( $this->m_objApHeader->getFrequencyInterval() ) || 0 >= $this->m_objApHeader->getFrequencyInterval() || 365 < $this->m_objApHeader->getFrequencyInterval() ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDayOfWeek() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getDayOfWeek() ) && ( CFrequency::WEEKLY == $this->m_objApHeader->getFrequencyId() || CFrequency::WEEKDAY_OF_MONTH == $this->m_objApHeader->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day_of_week', __( 'Weekday is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDaysOfMonth() {
		$boolIsValid = true;

		if( false == valArr( $this->m_objApHeader->getDaysOfMonth() ) && CFrequency::MONTHLY == $this->m_objApHeader->getFrequencyId() ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_month', __( 'Day(s) of month is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {

		$boolIsValid = true;

		if( false == valStr( $this->m_objApHeader->getStartDate() ) ) {
			$boolIsValid &= false;
			$arrobjErrorMessages = rekeyObjects( 'field', $this->m_objApHeader->getErrorMsgs() );

			if( false == isset( $arrobjErrorMessages['start_date'] ) || ( true == isset( $arrobjErrorMessages['start_date'] ) && 'Start date is required.' != $arrobjErrorMessages['start_date']->getMessage() ) ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is required.' ) ) );
			}
			return $boolIsValid;

		} elseif( true == valStr( $this->m_objApHeader->getStartDate() ) && false == CValidation::validateDate( $this->m_objApHeader->getStartDate() ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is invalid or start date should be in mm/dd/yyyy format.' ) ) );
			return $boolIsValid;
		}

		$strStartDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getStartDate() ] );

		if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $strStartDate ) ) {

			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is invalid.' ) ) );
			return $boolIsValid;
		}

		if( false == CValidation::checkDateFormat( $strStartDate, true ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndOn() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApHeader->getEndOn() ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_on', __( 'End on action is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid		= true;
		$strEndDate			= __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getEndDate() ] );
		$strNextPostDate	= __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getNextPostDate() ] );
		$strApHeaderType	= ( CApHeaderType::PURCHASE_ORDER == $this->m_objApHeader->getApHeaderTypeId() ) ? __( 'Purchase order' ) : __( 'Invoice' );

		if( true == valStr( $this->m_objApHeader->getEndOn() ) && false == is_null( $this->m_objApHeader->getEndDate() ) && ( 'date' == $this->m_objApHeader->getEndOn() || 'occurrences' == $this->m_objApHeader->getEndOn() ) ) {
			if( true == valStr( $this->m_objApHeader->getNextPostDate() ) && true == $this->valEndDate( $strEndDate ) && false == CValidation::checkDateFormat( $strNextPostDate, true ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'Next Post date is invalid.' ) ) );
			} elseif( true == $this->valEndDate( $strEndDate ) && strtotime( $this->m_objApHeader->getEndDate() ) < strtotime( $this->m_objApHeader->getNextPostDate() ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'The end date {%t, 0, DATE_NUMERIC_STANDARD} should be greater than or equal to the next occurrence date {%t, 1, DATE_NUMERIC_STANDARD} of the ' . $strApHeaderType . '.', [ $this->m_objApHeader->getEndDate(), $this->m_objApHeader->getNextPostDate() ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;
		$strStartDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getStartDate() ] );
		$strEndDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_objApHeader->getEndDate() ] );

		if( true == valStr( $this->m_objApHeader->getEndOn() ) && 'date' == $this->m_objApHeader->getEndOn() ) {
			if( false == valStr( $this->m_objApHeader->getEndDate() ) ) {
				$boolIsValid &= false;

				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is required.' ) ) );
				return $boolIsValid;
			} elseif( true == valStr( $this->m_objApHeader->getEndDate() ) && false == CValidation::validateDate( $this->m_objApHeader->getEndDate() ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is invalid or end date should be in mm/dd/yyyy format.' ) ) );
				return $boolIsValid;
			}

			if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $strEndDate ) ) {

				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is invalid.' ) ) );

				return $boolIsValid;
			}

			if( false == CValidation::checkDateFormat( $strEndDate, true ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is invalid.' ) ) );
			} elseif( true == $this->valStartDate( $strStartDate ) && strtotime( $this->m_objApHeader->getStartDate() ) > strtotime( $this->m_objApHeader->getEndDate() ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date should be greater than or equal to {%t, 0, DATE_NUMERIC_STANDARD}.', [ $this->m_objApHeader->getStartDate() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( true == valStr( $this->m_objApHeader->getEndOn() ) && 'occurrences' == $this->m_objApHeader->getEndOn() ) {
			if( true == is_null( $this->m_objApHeader->getNumberOfOccurrences() ) ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Number of occurrences is required.' ) ) );
			} elseif( false == is_numeric( $this->m_objApHeader->getNumberOfOccurrences() ) || 0 == $this->m_objApHeader->getNumberOfOccurrences() ) {
				$boolIsValid = false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Invalid number of occurrences.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valTemplateName( $strAction, $intApHeaderType, $objClientDatabase ) {

		$boolIsValid = true;

		// Validates for only templates
		if( false == valStr( $this->m_objApHeader->getTemplateName() ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_name', __( 'Template name can not be blank.' ) ) );
			return false;
		} elseif( true == isset( $strAction ) ) {

			$strWhere = 'WHERE
						cid = ' . ( int ) $this->m_objApHeader->getCid() . '
						AND LOWER( template_name ) = LOWER( \'' . addslashes( $this->m_objApHeader->getTemplateName() ) . '\' )
						AND id <> ' . ( int ) $this->m_objApHeader->getId() . '
						AND ap_header_type_id = ' . ( int ) $intApHeaderType . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

			if( 0 < CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid &= false;
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_name', __( 'Template name \'{%s, 0}\' already exists.', [ $this->m_objApHeader->getTemplateName() ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valReturnItems() {

		$boolIsValid = true;

		if( false == valArr( $this->m_objApHeader->getAssetTransactions() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Items already returned.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlDimension( $arrobjGlDimensions ) {

		$boolIsValid = true;

		if( false == is_null( $this->m_objApHeader->getBulkGlDimensionId() ) && false == isset( $arrobjGlDimensions[$this->m_objApHeader->getBulkGlDimensionId()] ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bulk_gl_dimension_id', __( 'Selected custom tag in \'Line Item Defaults\' section is either deleted or disabled.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRoutingTagRequired() {

		$boolIsValid = true;

		if( false == valId( $this->m_objApHeader->getApRoutingTagId() ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_routing_tag_id', __( 'Routing tag is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBillPayProperties() {
		$boolIsValid                = true;
		$arrstrNonBillPayProperties = [];
		$arrintBillPayPropertyIds   = ( array ) $this->m_objApHeader->getBillPayPropertyIds();

		foreach( ( array ) $this->m_objApHeader->getApDetails() as $objApDetail ) {
			if( true == in_array( $this->m_objApHeader->getApPaymentTypeId(), [ CApPaymentType::BILL_PAY, CApPaymentType::BILL_PAY_ACH, CApPaymentType::BILL_PAY_CHECK ], false ) && false == array_key_exists( $objApDetail->getPropertyId(), $arrintBillPayPropertyIds ) ) {
				$arrstrNonBillPayProperties[$objApDetail->getPropertyId()] = $objApDetail->getPropertyName();
			}
		}

		if( true == valArr( $arrstrNonBillPayProperties ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'properties', __( 'You cannot make bill payment for : {%s, 0} as they are not under bill pay contract.', [ implode( ',', $arrstrNonBillPayProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valApRemittanceForBillPay( $arrobjApRemittances ) {
		$boolIsValid 				= true;
		$arrobjTempApRemittances	= [];

		if( false == in_array( $this->m_objApHeader->getApPaymentTypeId(), [ CApPaymentType::BILL_PAY, CApPaymentType::BILL_PAY_ACH, CApPaymentType::BILL_PAY_CHECK ], false ) ) {
			return $boolIsValid;
		}

		$objApRemittance = $arrobjApRemittances[$this->m_objApHeader->getApRemittanceId()];

		if( false == valObj( $objApRemittance, 'CApRemittance' ) ) {
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Vendor \'{%s, 0} \' require at least one remittance.', [ $this->m_objApHeader->getApPayeeName() ] ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valRetentionPercent() {

		$boolIsValid = true;

		if( true == valId( $this->m_objApHeader->getBulkRetentionPercent() ) && false == valId( $this->m_objApHeader->getBulkApContractId() ) ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_contract_id', __( 'Contract is required if retention percent is provided.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRetentionAmount( $boolIsFromPayment = false ) {

		$boolIsValid            = true;
		$fltTransactionAmount   = 0.00;

		if( CApHeaderSubType::STANDARD_JOB_INVOICE == $this->m_objApHeader->getApHeaderSubTypeId() ) {

			foreach( ( array ) $this->m_objApHeader->getApDetails() as $objApDetail ) {

				if( false == $boolIsFromPayment && $objApDetail->getPreApprovalAmount() > bcsub( $objApDetail->getTransactionAmount(), $objApDetail->getRetentionAmount(), 2 ) && 0 < $objApDetail->getTransactionAmount() ) {
					$boolIsValid          &= false;
				} elseif( true == $boolIsFromPayment && $objApDetail->getTransactionAmountDue() < $objApDetail->getRetentionAmount() ) {
					$boolIsValid          &= false;
				}

				$fltTransactionAmount = bcadd( $fltTransactionAmount, bcsub( ( true == $boolIsFromPayment ) ? $objApDetail->getTransactionAmount() : $objApDetail->getTransactionAmountDue(), $objApDetail->getRetentionAmount(), 2 ), 2 );

			}

			if( false == $boolIsValid ) {
				$strMessage = ( bccomp( 0.00, $fltTransactionAmount, 2 ) ) ? __( ' As such you can only pay up to {%m, 0} against this invoice.', [ $fltTransactionAmount ] ) : __( ' As such you can not pay any amount against this invoice. ' );
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retention_amount', __( 'The retention has not been released for \'{%s, 0}\' invoice. {%s, 1}', [ $this->m_objApHeader->getHeaderNumber(), $strMessage ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valApPayeeDetails( $arrmixApPayeeInfo ) {
		$boolIsValid = true;

		if( false == valArr( $arrmixApPayeeInfo ) ) {
			return $boolIsValid;
		}
		if( $this->m_objApHeader->getApPayeeId() != $arrmixApPayeeInfo['accounts_ap_payee_id']
			|| $this->m_objApHeader->getApPayeeId() != $arrmixApPayeeInfo['locations_ap_payee_id']
			|| $this->m_objApHeader->getApPayeeId() != $arrmixApPayeeInfo['remittance_ap_payee_id'] ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_details', __( 'Vendor is no longer associated with this details [Location/Account/Remittance]. Please select a correct vendor.' ) ) );
		}

		if( $this->m_objApHeader->getApPayeeLocationId() != $arrmixApPayeeInfo['accounts_ap_payee_location_id'] ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id_ap_remittance_id', __( 'Vendor location & Vendor account are no more associated. Please select a valid association.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceRemittanceType( $arrobjApRemittances, $arrobjBankAccounts ) {
		$boolIsValid = true;

		if( CApPaymentType::WFPM != $this->m_objApHeader->getApPaymentTypeId() ) {
			return $boolIsValid;
		}

		$objApRemittance	= $arrobjApRemittances[$this->m_objApHeader->getApRemittanceId()];
		$objBankAccount		= $arrobjBankAccounts[$this->m_objApHeader->getBulkBankAccountId()];

		if( true == valObj( $objApRemittance, 'CApRemittance' ) && true == valObj( $objBankAccount, 'CBankAccount' ) ) {
			if( CApPaymentType::CREDIT_CARD != $objApRemittance->getApPaymentTypeId() && true == $objBankAccount->getIsCreditCardAccount() ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invoice {%s, 0} is associated to a credit card account but the remittance is set to Check/ACH. Update the invoice to a checking account or update the remittance to Credit Card.', [ $this->m_objApHeader->getHeaderNumber() ] ) ) );
				$boolIsValid &= false;
			} else if( CApPaymentType::CREDIT_CARD == $objApRemittance->getApPaymentTypeId() && false == $objBankAccount->getIsCreditCardAccount() ) {
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invoice {%s, 0} is associated to a checking account but the remittance is set to credit card. Update the invoice to a credit card account or update the remittance to Check/ACH.', [ $this->m_objApHeader->getHeaderNumber() ] ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valParentPropertyPostMonth( $intBankAccountId, $objClientDatabase ) {

		$boolIsValid    = true;
		$strPostMonth   = $this->m_objApHeader->getPostMonth();

		$arrintParentPropertyIds = [];

		if( valArr( $this->m_objApHeader->getApDetails() ) ) {

			$arrintParentPropertyIds    = ( array ) array_filter( array_keys( rekeyObjects( 'InterCoPropertyId', $this->m_objApHeader->getApDetails() ) ) );

			if( !valArr( $arrintParentPropertyIds ) ) {
				$arrintBankAccountIds       = ( array ) array_keys( rekeyObjects( 'BankAccountId', $this->m_objApHeader->getApDetails() ) );

				if( valIntArr( $arrintBankAccountIds ) ) {
					$arrobjPropertyBankAccounts = ( array ) \Psi\Eos\Entrata\CPropertyBankAccounts::createService()->fetchPropertyBankAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $this->m_objApHeader->getCid(), $objClientDatabase, true, true );

					if( valArr( $arrobjPropertyBankAccounts ) ) {
						$arrintParentPropertyIds = ( array ) array_keys( rekeyObjects( 'PropertyId', $arrobjPropertyBankAccounts ) );
					}
				}
			}
		} elseif( valId( $intBankAccountId ) ) {

			$arrobjPropertyBankAccounts = ( array ) \Psi\Eos\Entrata\CPropertyBankAccounts::createService()->fetchPropertyBankAccountsByBankAccountIdByCid( $intBankAccountId, $this->m_objApHeader->getCid(), $objClientDatabase, true, true );
			$arrintParentPropertyIds[]  = ( reset( $arrobjPropertyBankAccounts ) )->getPropertyId();
		}

		$arrobjPropertyGlSetting = ( array ) rekeyObjects( 'PropertyId', \Psi\Eos\Entrata\CPropertyGlSettings::createService()->fetchPropertyGlSettingsByPropertyIdsByCid( $arrintParentPropertyIds, $this->m_objApHeader->getCid(), $objClientDatabase ) );

		foreach( $arrobjPropertyGlSetting as $intParentPropertyId => $objPropertyGlSetting ) {

			// If no property gl setting available or property is in migration mode and intial import is on then skip.
			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
			 || ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && 1 == $this->m_objApHeader->getIsInitialImport() && true == $objPropertyGlSetting->getIsApMigrationMode() ) ) {
				$boolIsValid &= true;
			}

			// Validate for AP/GL lock period
			if( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) || strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) {
				$objProperty = getArrayElementByKey( $intParentPropertyId, $this->m_objApHeader->getProperties() );
				if( !valObj( $objProperty, 'CProperty' ) ) {
					$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intParentPropertyId, $this->m_objApHeader->getCid(), $objClientDatabase );
				}
				$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'GL or AP is locked for parent property \'{%s, 0}\' of invoice number {%s, 1}.', [ $objProperty->getPropertyName(), $this->m_objApHeader->getHeaderNumber() ] ) ) );

				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valDuplicateInvoiceForStandardPO( $boolIsAllowDuplicateInvoicesForPos, $objClientDatabase ) {

		$boolIsValid = true;

		if( true == $boolIsAllowDuplicateInvoicesForPos || false == valId( $this->m_objApHeader->getApExceptionQueueItemId() ) || false == valArr( $this->m_objApHeader->getPoApHeaderIds() ) ) return true;

		$strWhere = 'WHERE cid = ' . ( int ) $this->m_objApHeader->getCid() . '
							AND id IN( ' . implode( ',', $this->m_objApHeader->getPoApHeaderIds() ) . ' )
							AND deleted_on IS NOT NULL
							AND deleted_by IS NULL
							AND ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
							AND ap_financial_status_type_id = ' . CApFinancialStatusType::CLOSED;

		if( 0 < CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The attached purchase order(s) has already been invoiced.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCcBankAccount( $objClientDatabase ) {
		$boolIsValid = true;

		$arrintGlAccountIds = array_keys( rekeyObjects( 'GlAccountId', $this->m_objApHeader->getApDetails() ) );
		$arrobjGlAccounts   = CGlAccounts::fetchGlAccountsByIdsByCid( $arrintGlAccountIds, $this->m_objApHeader->getCid(), $objClientDatabase, true );

		$arrintBankAccountIds = ( array ) array_keys( rekeyObjects( 'BankAccountId', $this->m_objApHeader->getApDetails() ) );
		$arrobjBankAccounts = [];

		if( CApHeaderSubType::STANDARD_JOB_INVOICE == $this->m_objApHeader->getApHeaderSubTypeId() && true == valIntArr( $arrintBankAccountIds ) ) {
			$arrobjBankAccounts = ( array ) CBankAccounts::fetchBankAccountsByIdsByCid( $arrintBankAccountIds, $this->m_objApHeader->getCid(), $objClientDatabase );
		}

		foreach( ( array ) $this->m_objApHeader->getApDetails() as $objApDetail ) {
			if( true == valObj( $this->m_objApHeader->getApPayment(), 'CApPayment' ) && CApPaymentType::CREDIT_CARD == $this->m_objApHeader->getApPayment()->getApPaymentTypeId() ) {
				// validate if the bank account of the invoice is not same as payment's bank account
				if( false == valId( $objApDetail->getBankAccountId() ) ) {
					$boolIsValid        &= false;
					$this->m_objApHeader->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Please add default AP Bank Account for the properties.' ) ) );
					break;
				} elseif( $this->m_objApHeader->getApPayment()->getBankAccountId() == $objApDetail->getBankAccountId() ) {
					$boolIsValid        &= false;
					$this->m_objApHeader->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Unable to create invoice due to Default AP Bank Account for the property being the same as the Credit Card Bank Account of the payment. Please change the properties Default AP Bank Account and go to Incomplete Invoices to complete and save.' ) ) );
					break;
				}

				// validate if the Gl account of the invoice is not a cash account
				if( false == valId( $objApDetail->getGlAccountId() ) || false == valArr( $arrobjGlAccounts ) || CGlAccountUsageType::BANK_ACCOUNT == $arrobjGlAccounts[$objApDetail->getGlAccountId()]->getGlAccountUsageTypeId() ) {
					$boolIsValid        &= false;
					$this->m_objApHeader->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Gl account should be present and should not be a cash account of the bank account.' ) ) );
					break;
				}
			}
		}
		return $boolIsValid;
	}

	public function valBillbackInvoice( $boolIsIntercompanyInvoice, $intIsBillbackInvoice ) {
		$boolIsValid = true;

		if( true == $boolIsIntercompanyInvoice && 1 == $intIsBillbackInvoice ) {
			$boolIsValid = false;
			$this->m_objApHeader->setIsReimbursement( false );
			$this->m_objApHeader->setReimbursementApHeaderId( NULL );
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billback_invoice', __( 'The Billback feature cannot be used with our Inter-Company Banking feature. Please select a different bank account if you plan to bill this invoice back.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceAttachmentRequired() {

		$boolIsValid = true;
		if( false == $this->m_objApHeader->getIsAttachment() ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_attachment', __( 'Attachment required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInvoiceDeleted() {

		$boolIsValid = true;
		if( CApFinancialStatusType::DELETED == $this->m_objApHeader->getApFinancialStatusTypeId() ) {
			$boolIsValid &= false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_invoice_deleted', __( "We're unable to perform this action because the associated invoice is deleted" ) ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateBillbackReimbursements( $arrintBillbackApDetailIds, $objClientDatabase ) {
		$boolIsValid = true;
		if( 0 < \Psi\Eos\Entrata\CApDetailReimbursements::createService()->fetchApDetailReimbursementsCountByOriginalApDetailIdsByCid( $arrintBillbackApDetailIds, $this->m_objApHeader->getCid(), $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->m_objApHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billback_invoice', __( 'A billback invoice has already been created for this transaction.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [] ) {

		$boolIsValid								= true;
		$boolExportedTransactionsPermission			= ( true == isset( $arrmixParameters['bool_exported_transactions_permission'] ) ) ? $arrmixParameters['bool_exported_transactions_permission'] : false;
		$boolIsRequireMemoWhenReversingTransactions	= ( true == isset( $arrmixParameters['require_memo_when_reversing_transactions'] ) && true == $arrmixParameters['require_memo_when_reversing_transactions'] ) ? true : false;
		$boolAllowDuplicateInvoicesForStandardPos	= ( true == isset( $arrmixParameters['allow_duplicate_invoices_for_standard_pos'] ) ) ? $arrmixParameters['allow_duplicate_invoices_for_standard_pos'] : false;

		$arrobjApHeaders 							= ( true == isset( $arrmixParameters['ap_headers'] ) ) ? $arrmixParameters['ap_headers'] : [];
		$arrintRestrictedApDetailIds				= ( true == isset( $arrmixParameters['restricted_ap_detail_ids'] ) ) ? $arrmixParameters['restricted_ap_detail_ids'] : [];
		$arrobjGlDimensions							= ( true == isset( $arrmixParameters['gl_dimensions'] ) ) ? $arrmixParameters['gl_dimensions'] : [];
		$arrobjApRemittances						= ( true == isset( $arrmixParameters['ap_remittances'] ) ) ? $arrmixParameters['ap_remittances'] : [];
		$arrobjBankAccounts 						= ( true == isset( $arrmixParameters['bank_accounts'] ) ) ? $arrmixParameters['bank_accounts'] : [];
		$arrobjPoApHeaders 							= ( true == isset( $arrmixParameters['po_ap_headers'] ) ) ? $arrmixParameters['po_ap_headers'] : [];
		$arrobjPoApDetails 							= ( true == isset( $arrmixParameters['po_ap_details'] ) ) ? $arrmixParameters['po_ap_details'] : [];
		$arrintSelectedPoNumbers 					= ( true == isset( $arrmixParameters['selected_po_numbers'] ) ) ? $arrmixParameters['selected_po_numbers'] : [];
		$arrintInitiallySelectedPoNumbers           = ( true == isset( $arrmixParameters['initially_selected_po_number'] ) ) ? $arrmixParameters['initially_selected_po_number'] : [];
		$arrintOldApDetailsIds						= ( true == isset( $arrmixParameters['old_ap_detail_ids'] ) ) ? $arrmixParameters['old_ap_detail_ids'] : [];
		$arrintNewSelectedPOs                       = ( true == isset( $arrmixParameters['new_selected_po_number'] ) ) ? $arrmixParameters['new_selected_po_number'] : [];
		$arrintAttachedPos                          = ( true == isset( $arrmixParameters['attached_po_numbers'] ) ) ? $arrmixParameters['attached_po_numbers'] : [];

		$intApHeaderType							= CApHeaderType::INVOICE;
		$intModule									= ( true == isset( $arrmixParameters['module_id'] ) ) ? $arrmixParameters['module_id'] : NULL;
		$intJobId									= ( true == isset( $arrmixParameters['job_id'] ) ) ? $arrmixParameters['job_id'] : NULL;
		$intApContractId							= ( true == isset( $arrmixParameters['ap_contract_id'] ) ) ? $arrmixParameters['ap_contract_id'] : NULL;
		$intBankAccountId							= ( true == isset( $arrmixParameters['bank_account_id'] ) ) ? $arrmixParameters['bank_account_id'] : NULL;
		$intApHeaderSubTypeId						= ( true == isset( $arrmixParameters['ap_header_sub_type_id'] ) ) ? $arrmixParameters['ap_header_sub_type_id'] : NULL;
		$intJobPropertyId							= ( true == isset( $arrmixParameters['job_property_id'] ) ) ? $arrmixParameters['job_property_id'] : NULL;
		$objCompanyUser								= ( true == isset( $arrmixParameters['company_user'] ) ) ? $arrmixParameters['company_user'] : NULL;
		$objClientDatabase							= ( true == isset( $arrmixParameters['client_database'] ) ) ? $arrmixParameters['client_database'] : NULL;
		$boolIsReverseFmo							= ( true == isset( $arrmixParameters['is_reverse_fmo'] ) ) ? $arrmixParameters['is_reverse_fmo'] : false;
		$boolIsPeriodClosed							= ( true == isset( $arrmixParameters['is_period_closed'] ) ) ? $arrmixParameters['is_period_closed'] : false;
		$boolUseItemCatalog							= ( true == isset( $arrmixParameters['use_item_catalog'] ) ) ? $arrmixParameters['use_item_catalog'] : false;
		$boolIsApCodePresent						= ( true == isset( $arrmixParameters['is_ap_code_present'] ) ) ? $arrmixParameters['is_ap_code_present'] : false;
		$boolApDetailExists							= ( true == isset( $arrmixParameters['is_ap_details_exists'] ) ) ? $arrmixParameters['is_ap_details_exists'] : false;
		$boolIsTemplateRecurring					= ( true == isset( $arrmixParameters['is_template_recurring'] ) ) ? $arrmixParameters['is_template_recurring'] : false;
		$boolIsRoutingTagsRequired					= ( true == isset( $arrmixParameters['is_routing_tags_required'] ) ) ? $arrmixParameters['is_routing_tags_required'] : false;
		$boolIsAllowDuplicateInvoiceNumber			= ( true == isset( $arrmixParameters['allow_duplicate_invoice_numbers_per_vendor'] ) ) ? $arrmixParameters['allow_duplicate_invoice_numbers_per_vendor'] : false;
		$boolIsHistoricalInvoice					= ( true == isset( $arrmixParameters['is_historical_invoice'] ) ) ? $arrmixParameters['is_historical_invoice'] : false;
		$boolIsPayeeActive							= ( true == isset( $arrmixParameters['is_ap_payee_active'] ) ) ? $arrmixParameters['is_ap_payee_active'] : false;
		$boolIsFromRecurringScript					= ( true == isset( $arrmixParameters['is_from_recurring_script'] ) ) ? $arrmixParameters['is_from_recurring_script'] : false;
		$boolIsIntercompanyInvoice					= ( true == isset( $arrmixParameters['is_intercompany_invoice'] ) ) ? $arrmixParameters['is_intercompany_invoice'] : false;
		$intIsBillbackInvoice						= ( true == isset( $arrmixParameters['is_billback_invoice'] ) ) ? $arrmixParameters['is_billback_invoice'] : false;
		$intIsNewReimbursementInvoice				= ( true == isset( $arrmixParameters['is_new_reimbursement_invoice'] ) ) ? $arrmixParameters['is_new_reimbursement_invoice'] : false;
		$boolIsAllowDuplicateInvoicesForPos			= ( true == isset( $arrmixParameters['is_allow_duplicate_invoice_for_POs'] ) ) ? $arrmixParameters['is_allow_duplicate_invoice_for_POs'] : false;
		$boolIsRequiredAttachmentForInvoice			= ( true == isset( $arrmixParameters['is_require_attachment_for_invoice'] ) ) ? $arrmixParameters['is_require_attachment_for_invoice'] : false;

		$arrmixApPayeeInfo							= ( true == isset( $arrmixParameters['ap_payee_details'] ) ) ? $arrmixParameters['ap_payee_details'] : [];
		$boolIsInvoiceTemplate						= ( true == isset( $arrmixParameters['is_template'] ) ) ? $arrmixParameters['is_template'] : false;
		$boolIsBillbackReimbursementInvoice			= ( true == isset( $arrmixParameters['is_billback_reimbursement'] ) ) ? $arrmixParameters['is_billback_reimbursement'] : false;
		$boolShowDisabledData						= ( true == isset( $arrmixParameters['is_show_disabled_data'] ) ) ? $arrmixParameters['is_show_disabled_data'] : false;
		$boolIsDuplicateInvoice						= ( true == isset( $arrmixParameters['is_duplicate_invoice'] ) ) ? $arrmixParameters['is_duplicate_invoice'] : false;
		$boolIsFromTemplate							= ( true == isset( $arrmixParameters['is_from_template'] ) ) ? $arrmixParameters['is_from_template'] : false;
		$arrintInvoiceTypePermissions				= ( true == isset( $arrmixParameters['invoice_type_permissions'] ) ) ? $arrmixParameters['invoice_type_permissions'] : [];
		$arrintPaymentApDetailIds					= ( true == isset( $arrmixParameters['payment_ap_detail_ids'] ) ) ? $arrmixParameters['payment_ap_detail_ids'] : [];
		$arrintApHeaderIds							= ( true == isset( $arrmixParameters['ap_header_ids'] ) ) ? $arrmixParameters['ap_header_ids'] : [];
		$intClientId								= ( true == isset( $arrmixParameters['client_id'] ) ) ? $arrmixParameters['client_id'] : NULL;
		$boolIsFromViewCheck						= ( true == isset( $arrmixParameters['is_from_view_check'] ) ) ? $arrmixParameters['is_from_view_check'] : false;
		$boolIsInitialImport                        = ( true == isset( $arrmixParameters['is_initial_import'] ) ) ? $arrmixParameters['is_initial_import'] : false;
		$arrintBillbackApDetailIds				    = ( true == isset( $arrmixParameters['billback_ap_detail_ids'] ) ) ? $arrmixParameters['billback_ap_detail_ids'] : [];

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, false, false, $boolIsHistoricalInvoice );
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valGlPosting();

				if( false == $boolIsAllowDuplicateInvoiceNumber )
					$boolIsValid &= $this->valIsInvoiceExists( $objClientDatabase );

				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valApCodePropertyAssociation();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valHeaderNumber( $objClientDatabase );
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				if( 1 != $intIsNewReimbursementInvoice ) {
					$boolIsValid &= $this->valIsInvoiceExists( $objClientDatabase );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'validate_purchase_order_update':
				if( true == $boolIsTemplateRecurring ) {
					$boolIsValid &= $this->valFrequencyId();
					$boolIsValid &= $this->valFrequencyInterval();
					$boolIsValid &= $this->valDayOfWeek();
					$boolIsValid &= $this->valDaysOfMonth();
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndOn();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valNextPostDate();
					$boolIsValid &= $this->valNumberOfOccurrences();
				}
				if( true == $this->m_objApHeader->getIsTemplate() ) {
					$intApHeaderType = CApHeaderType::PURCHASE_ORDER;
					$boolIsValid &= $this->valTemplateName( $strAction, $intApHeaderType, $objClientDatabase );
				}
				if( true == $boolIsRoutingTagsRequired ) {
					$boolIsValid &= $this->valRoutingTagRequired();
				}
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valPostDateForPo();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valTransactionAmount( $boolUseItemCatalog, $objClientDatabase );
				$boolIsValid &= $this->valUserAssignedProperties();
				$boolIsValid &= $this->valGlDimension( $arrobjGlDimensions );
				$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				$boolIsValid &= $this->valApDetailsCount( $arrintRestrictedApDetailIds );
				$boolIsValid &= $this->valApCodePropertyAssociation();
				break;

			case 'validate_purchase_order_close':
				$boolIsValid &= $this->valApprovedByForPo();
				break;

			case 'single_po_insert':
				if( true == $boolIsTemplateRecurring ) {
					$boolIsValid &= $this->valFrequencyId();
					$boolIsValid &= $this->valFrequencyInterval();
					$boolIsValid &= $this->valDayOfWeek();
					$boolIsValid &= $this->valDaysOfMonth();
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndOn();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valNextPostDate();
					$boolIsValid &= $this->valNumberOfOccurrences();
				}
				if( true == $this->m_objApHeader->getIsTemplate() ) {
					$intApHeaderType = CApHeaderType::PURCHASE_ORDER;
					$boolIsValid &= $this->valTemplateName( $strAction, $intApHeaderType, $objClientDatabase );
				}

				if( true == $boolIsRoutingTagsRequired ) {
					$boolIsValid &= $this->valRoutingTagRequired();
				}
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valApPayeeDetails( $arrmixApPayeeInfo );
				$boolIsValid &= $this->valIsApPayeeInactive();
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valApCodeId( $boolIsApCodePresent );
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostDateForPo();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valUserAssignedProperties( false, $boolIsFromRecurringScript );
				$boolIsValid &= $this->valApCodePropertyAssociation();
				break;

			case 'validate_financial_move_out':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valApDetails();
				break;

			case 'single_invoice_insert':
				if( CApHeaderSubType::CREDIT_MEMO_INVOICE != $this->m_objApHeader->getApHeaderSubTypeId() ) {
					$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				}

				if( 'recurring' == $this->m_objApHeader->getTemplateType() ) {

					$boolIsValid &= $this->valFrequencyId();
					$boolIsValid &= $this->valFrequencyInterval();
					$boolIsValid &= $this->valDayOfWeek();
					$boolIsValid &= $this->valDaysOfMonth();
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndOn();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valNextPostDate();
					$boolIsValid &= $this->valNumberOfOccurrences();
				}

				if( true == $boolIsPayeeActive )
					$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valApPayeeRemittanceId();
				$boolIsValid &= $this->valIsApPayeeInactive();
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valApPayeeDetails( $arrmixApPayeeInfo );
				$boolIsValid &= $this->valIsApPayeeInactive();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valGlPosting();

				if( false == $boolIsInvoiceTemplate ) {
					$boolIsValid &= $this->valPostDate();
					$boolIsValid &= $this->valDueDate();
					$boolIsValid &= $this->valPoNumber( $objClientDatabase );
					$boolIsValid &= $this->valPostDateAndDueDate();
					$boolIsValid &= $this->valHeaderNumber();
					$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
					if( false == $boolIsBillbackReimbursementInvoice && 1 != $intIsNewReimbursementInvoice ) {
						$boolIsValid &= $this->valIsInvoiceExists( $objClientDatabase );
					}

					if( $boolIsBillbackReimbursementInvoice ) {
						$boolIsValid &= $this->valDuplicateBillbackReimbursements( $arrintBillbackApDetailIds, $objClientDatabase );
					}
					$boolIsValid &= $this->valCcBankAccount( $objClientDatabase );
				}

				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, false, $boolApDetailExists, false, $boolIsFromRecurringScript );

				if( true == valObj( $this->m_objApHeader->getApPayment(), 'CApPayment' ) ) {
					$boolIsValid &= $this->valCcBankAccount( $objClientDatabase );
				}

				if( true == $boolIsIntercompanyInvoice ) {
					$boolIsValid &= $this->valParentPropertyPostMonth( $intBankAccountId, $objClientDatabase );
				}

				if( true == $boolIsInvoiceTemplate ) {
					$intApHeaderType = CApHeaderType::INVOICE;
					$boolIsValid &= $this->valTemplateName( $strAction, $intApHeaderType, $objClientDatabase );
				}

				$boolIsValid &= $this->valUserAssignedProperties( false, $boolIsFromRecurringScript );
				$boolIsValid &= $this->valInvoiceSubTotalsAmount();
				$boolIsValid &= $this->valRetentionPercent();
				$boolIsValid &= $this->valBillbackInvoice( $boolIsIntercompanyInvoice, $intIsBillbackInvoice );
				if( true == $boolIsApCodePresent ) {
					$boolIsValid &= $this->valInvoiceTotalQuantity( $objClientDatabase, true );
				}
				if( CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valUseItemCatalog( $boolUseItemCatalog );
					$boolIsValid &= $this->valUseItemReceive( $arrobjPoApHeaders );
				}
				$boolIsValid &= $this->valApCodePropertyAssociation();

				if( true == $boolIsRoutingTagsRequired ) {
					$boolIsValid &= $this->valRoutingTagRequired();
				}

				if( true == $boolIsRequiredAttachmentForInvoice && false == $boolIsFromRecurringScript ) {
					$boolIsValid &= $this->valInvoiceAttachmentRequired();
				}

				$boolIsValid &= $this->valDuplicateInvoiceForStandardPO( $boolIsAllowDuplicateInvoicesForPos, $objClientDatabase );
				break;

			case 'single_invoice_update':
				if( true == $boolIsTemplateRecurring ) {
					$boolIsValid &= $this->valFrequencyId();
					$boolIsValid &= $this->valFrequencyInterval();
					$boolIsValid &= $this->valDayOfWeek();
					$boolIsValid &= $this->valDaysOfMonth();
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndOn();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valNextPostDate();
					$boolIsValid &= $this->valNumberOfOccurrences();
				}
				if( true == $this->m_objApHeader->getIsTemplate() ) {
					$intApHeaderType = CApHeaderType::INVOICE;
					$boolIsValid &= $this->valTemplateName( $strAction, $intApHeaderType, $objClientDatabase );
				}
				if( false == $this->m_objApHeader->getIsTemplate() ) {
					$boolIsValid &= $this->valHeaderNumber();
					$boolIsValid &= $this->valTransactionDatetime();
					$boolIsValid &= $this->valPostDate();
					$boolIsValid &= $this->valDueDate();
					$boolIsValid &= $this->valPostDateAndDueDate();
					$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				}
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valIsGlExportBatchCreated( $objClientDatabase, $boolExportedTransactionsPermission, $arrintOldApDetailsIds );
				$boolIsValid &= $this->valIsApExportBatchCreated( $boolExportedTransactionsPermission );
				$boolIsValid &= $this->valInvoiceSubTotalsAmount();
				$boolIsValid &= $this->valRetentionPercent();

				if( false == $boolIsPeriodClosed || ( 1 == $this->m_objApHeader->getIsTemporary() && 0 == $this->m_objApHeader->getIsPosted() ) || ( true == $boolIsPeriodClosed && true == $boolIsInitialImport && 0 == $this->m_objApHeader->getIsTemporary() && 0 == $this->m_objApHeader->getIsPosted() ) ) {
					$boolIsValid &= $this->valGlPosting();
					$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				}

				if( true == $boolIsIntercompanyInvoice ) {
					$boolIsValid &= $this->valParentPropertyPostMonth( $intBankAccountId, $objClientDatabase );
				}

				if( 1 == $this->m_objApHeader->getIsTemporary() && 0 == $this->m_objApHeader->getIsPosted()
					&& CApHeaderSubType::MANAGEMENT_FEE_INVOICE != $this->m_objApHeader->getApHeaderSubTypeId()
					&& false == $boolIsBillbackReimbursementInvoice
				    && 1 != $intIsNewReimbursementInvoice ) {
					$boolIsValid &= $this->valIsInvoiceExists( $objClientDatabase );
				}

				if( true == $boolIsApCodePresent ) {
					$boolIsValid &= $this->valInvoiceTotalQuantity( $objClientDatabase, false );
				}

				$boolIsValid &= $this->valApCodePropertyAssociation();
				$boolIsValid &= $this->valBillbackInvoice( $boolIsIntercompanyInvoice, $intIsBillbackInvoice );

				if( true == $boolIsRoutingTagsRequired ) {
					$boolIsValid &= $this->valRoutingTagRequired();
				}

				if( true == $boolIsRequiredAttachmentForInvoice && false == $boolIsFromRecurringScript ) {
					$boolIsValid &= $this->valInvoiceAttachmentRequired();
				}

				if( CApHeaderSubType::STANDARD_JOB_INVOICE == $this->m_objApHeader->getApHeaderSubTypeId() ) {
					$boolIsValid &= $this->valCcBankAccount( $objClientDatabase );
				}
				break;

			case 'refund_invoice_update':
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valIsGlExportBatchCreated( $objClientDatabase, $boolExportedTransactionsPermission, $arrintOldApDetailsIds );
				$boolIsValid &= $this->valIsApExportBatchCreated( $boolExportedTransactionsPermission );
				$boolIsValid &= $this->valBillbackInvoice( $boolIsIntercompanyInvoice, $intIsBillbackInvoice );

				if( false == $boolIsPeriodClosed || ( 1 == $this->m_objApHeader->getIsTemporary() && 0 == $this->m_objApHeader->getIsPosted() ) ) {
					$boolIsValid &= $this->valGlPosting();
					$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				}
				break;

			case 'historical_import_payment_ap_header_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate( $intModule );
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				break;

			case 'payment_ap_header_insert':
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate( $intModule );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, false, false, $boolIsHistoricalInvoice );
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valBillPayProperties();
				$boolIsValid &= $this->valIsDeletedApDetails( $arrintPaymentApDetailIds, $arrintApHeaderIds, $boolIsFromViewCheck, $intClientId, $objClientDatabase );
				break;

			/**
			 * Todo : Following is case is temporary and should be removed very soon.
			 */
			case 'payment_library_ap_header_insert':
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate( $intModule );
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valBillPayProperties();
				break;

			case 'void_check':
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, true );
				$boolIsValid &= $this->valPostDate( $intModule, true, true);
				$boolIsValid &= $this->valHeaderMemo( $strAction );
				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'unpost_invoice':
				$boolIsValid &= $this->valIsGlExportBatchCreated( $objClientDatabase, $boolExportedTransactionsPermission, $arrintOldApDetailsIds );
				$boolIsValid &= $this->valIsApExportBatchCreated( $boolExportedTransactionsPermission );
				$boolIsValid &= $this->valIsInvoicePaid( $objClientDatabase );
				break;

			case 'post_invoice':
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valUserAssignedProperties( true );
				$boolIsValid &= $this->valIsInitialImport();
				break;

			case 'approve_post_routed_invoice':
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valIsInitialImport();
				break;

			case 'approve_invoice':
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valRetentionAmount();
				break;

			case 'approve_routed_invoices':
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valIsApPayeeLocked();
				break;

			case 'pay_invoice':
				$boolIsValid &= $this->valIsApPayeeLocked();
				$boolIsValid &= $this->valUserAssignedProperties();
				$boolIsValid &= $this->valRetentionAmount( $boolIsFromPayment = true );
				$boolIsValid &= $this->valInvoiceRemittanceType( $arrobjApRemittances, $arrobjBankAccounts );
				break;

			case 'reverse_invoice':
				$boolIsValid &= $this->valIsInvoiceReversible( $boolIsReverseFmo );
				$boolIsValid &= $this->valIsNonRefundInvoice( $boolIsReverseFmo, $objClientDatabase );

				if( false == $boolIsValid ) {
					break;
				}

				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, true );
				$boolIsValid &= $this->valPostDate( $intModule, true );
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valUserAssignedProperties();
				if( true == $boolIsRequireMemoWhenReversingTransactions ) {
					$boolIsValid &= $this->valHeaderMemo( $strAction );
				}
				break;

			case 'rpd_slim_invoice_insert':
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valControlTotal();
				break;

			case 'is_invoice_unpaid':
				$boolIsValid &= $this->valIsUnpaidInvoices( $arrobjApHeaders );
				break;

			case 'delete_invoice':
				$boolIsValid &= $this->valIsNonRefundInvoice( $boolIsReverseFmo = false, $objClientDatabase );
				$boolIsValid &= $this->valIsInvoicePending();
				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'management_fee_invoice_insert':
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valActiveApPayeeLocation();
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valDefaultBankAccountOrMigrationModeProperties( true );
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valDisabledProperty( $boolShowDisabledData );
				$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				$boolIsValid &= $this->valApPayee();
				break;

			case 'user_assigned_properties':
				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'edit_accounts':
				$boolIsValid &= $this->valIsGlExportBatchCreated( $objClientDatabase, $boolExportedTransactionsPermission, $arrintOldApDetailsIds );
				$boolIsValid &= $this->valIsApExportBatchCreated( $boolExportedTransactionsPermission );
				break;

			case 'validate_quick_pay':
				$boolIsValid &= $this->valQuickPay( $objClientDatabase );
				break;

			case 'validate_for_payment':
				$boolIsValid &= $this->valIsValidForPayment( $objClientDatabase );
				break;

			case 'process_owner_distribution':
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, false, true );
				break;

			case 'owner_distribuition_insert':
				$boolIsValid &= $this->valApPayeeIdAndApPayeeLocationId();
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valDueDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				$boolIsValid &= $this->valActiveApPayeeLocation();
				$boolIsValid &= $this->valApDetails();
				$boolIsValid &= $this->valDefaultBankAccountOrMigrationModeProperties();
				$boolIsValid &= $this->valPropertyBankAccounts();
				$boolIsValid &= $this->valDisabledProperty();
				break;

			case 'approve_purchase_order':
				$boolIsValid &= $this->valUserAssignedProperties();
				$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				break;

			case 'approve_purchase_orders':
				$boolIsValid &= $this->valUserAssignedProperties();
				$boolIsValid &= $this->valApPayeeProperties( true, $boolShowDisabledData );
				break;

			case 'approve_routed_purchase_order':
				$boolIsValid &= $this->valApPayeeProperties();
				break;

			case 'approve_routed_purchase_orders':
				$boolIsValid &= $this->valApPayeeProperties( true );
				break;

			case 'edit_purchase_order':
			case 'view_purchase_order':
			case 'duplicate_purchase_order':
				$boolIsValid &= $this->valApPayeeProperties( false, $boolShowDisabledData );
				break;

			// incase of ach payment need to validate ap_payee account number
			case 'validate_ap_payee_account_number':
				$boolIsValid &= $this->valApPayeeAccountNumber( $arrobjApRemittances );
				break;

			case 'validate_return_items':
				$boolIsValid &= $this->valReturnItems();
				break;

			case 'validate_job_items':
			case 'validate_contract_items':
				$boolIsValid &= $this->valHeaderNumber();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valPostDateAndDueDate();
				$boolIsValid &= $this->valTransactionAmount( false, $objClientDatabase );
				break;

			case 'validate_bill_pay_ach_remittance':
				$boolIsValid &= $this->valApRemittanceForBillPay( $arrobjApRemittances );
				break;

			case 'clear_bank':
			case 'remove_from_unclaimed_properties':
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, true );
				$boolIsValid &= $this->valPostDate( $intModule, true );
				break;

			case 'reverse_invoice_from_void';
				$boolIsValid &= $this->valIsNonRefundInvoice( $boolIsReverseFmo, $objClientDatabase );

				if( false == $boolIsValid ) {
					break;
				}

				$boolIsValid &= $this->valPostDate( $intModule, true );
				$boolIsValid &= $this->valGlPosting();
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, true );
				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'validate_create_invoice_from_po';
				$boolIsValid &= $this->valCreateInvoiceFromPos( $arrobjPoApHeaders, $intApHeaderSubTypeId, $boolIsDuplicateInvoice, $boolIsFromTemplate, $arrintInvoiceTypePermissions, $arrintInitiallySelectedPoNumbers, $arrintAttachedPos );
				$boolIsValid &= $this->valDuplicatePoNumber( $arrintSelectedPoNumbers );
				$boolIsValid &= $this->valPoNumberExist( $arrobjPoApHeaders, $arrintSelectedPoNumbers );
				$boolIsValid &= $this->valVendorAndAllowDuplicateInvoicesForPoNumber( $arrobjPoApHeaders, $arrobjPoApDetails, $boolAllowDuplicateInvoicesForStandardPos, $intApHeaderSubTypeId, $arrintNewSelectedPOs );
				if( CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) {
					$boolIsValid &= $this->valUseItemCatalog( $boolUseItemCatalog );
					$boolIsValid &= $this->valUseItemReceive( $arrobjPoApHeaders );
				}
				break;

			case 'validate_create_invoice_from_po_job_contract':
				if( true == valId( $intJobId ) ) {
					$boolIsValid &= $this->valPoFromJobs( $intJobPropertyId, $arrobjPoApDetails, $intJobId );
				}

				if( true == valId( $intApContractId ) ) {
					$boolIsValid &= $this->valPoFromContracts( $intApContractId, $arrobjPoApDetails );
				}
				break;

			case 'validate_reverse_voided_payment':
				$boolIsValid &= $this->valIsInvoiceDeleted();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>
