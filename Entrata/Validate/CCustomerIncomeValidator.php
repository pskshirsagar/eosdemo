<?php

use Psi\Libraries\I18n\PostalAddress\CPostalAddress;
class CCustomerIncomeValidator {

	use TPostalAddressHelper;
	protected $m_objCustomerIncome;
	protected $m_boolValidateRequiredFields;

	const ADDRESS_POSTAL_CODE_FIELD     = 'postalCode';

	public function __construct() {
		$this->m_boolValidateRequiredFields = true;
		return;
	}

	public function setCustomerIncome( $objCustomerIncome ) {
		$this->m_objCustomerIncome = $objCustomerIncome;
	}

	public function setValidateRequiredFields( $boolValidateRequiredFields ) {
		$this->m_boolValidateRequiredFields = $boolValidateRequiredFields;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true === is_null( $this->m_objCustomerIncome->getCustomerId() ) || false == valStr( $this->m_objCustomerIncome->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Household member is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIncomeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getIncomeTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_type_id', __( 'Income type is required.' ), '', [ 'label' => __( 'Income Type' ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		// if( true == is_null( $this->m_objCustomerIncome->getFrequencyId())) {
		//    $boolIsValid = false;
		//    $this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', '' ));
		// }

		return $boolIsValid;
	}

	public function valFrequencyDetailId() {
		$boolIsValid = true;

		if( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintEmploymentIncomeType ) ) {

			$strFrequencyRequiredMsg	= __( 'Frequency of wages/salary is required.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintPension ) ) {

			$strFrequencyRequiredMsg	= __( 'Frequency of benefits/pension is required.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintPublicAssistance ) ) {

			$strFrequencyRequiredMsg	= __( 'Frequency of public assistance is required.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintOther ) ) {

			$strFrequencyRequiredMsg	= __( 'Frequency of other income is required.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintGeneral ) ) {

			$strFrequencyRequiredMsg	= __( 'Frequency of general income is required.' );
		}

		if( true === is_null( $this->m_objCustomerIncome->getFrequencyId() ) || false == valStr( $this->m_objCustomerIncome->getFrequencyId() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', $strFrequencyRequiredMsg ) );
		}
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		$intIncomeType = ( false === is_null( $this->m_objCustomerIncome->getIncomeTypeId() ) )? intval( $this->m_objCustomerIncome->getIncomeTypeId() ):0;

		if( true == in_array( $intIncomeType, CIncomeType::$c_arrintGeneral ) ) {
			$boolIsValid &= $this->valOtherIncome();
		}

		switch( $intIncomeType ) {

			case CIncomeType::BUSINESS_OR_RENTAL_INCOME:
			case CIncomeType::MILITARY_PAY:
			case CIncomeType::FEDERAL_WAGE:
				$boolIsValid &= $this->valEmploymentIncome();
				break;

			case CIncomeType::NON_FEDERAL_WAGES:
				$boolIsValid &= $this->valEmploymentIncome();
				$boolIsValid &= $this->valNonFederalWage();
				break;

			case CIncomeType::SSI:
			case CIncomeType::SOCIAL_SECURITY:
			case CIncomeType::PENSION_PAYMENTS:
			case CIncomeType::TEMPORARY_ASSISTANCE:
			case CIncomeType::GENERAL_RELIEF:
			case CIncomeType::UNEMPLOYMENT_BENEFITS:
			case CIncomeType::INDIAN_TRUST:
			case CIncomeType::CHILD_SUPPORT:
			case CIncomeType::OTHER_NON_WAGE_SOURCE:
				$boolIsValid &= $this->valOtherIncome();
				break;

			default:
				return $boolIsValid;
				break;
		}

		return $boolIsValid;
	}

	public function valEmploymentIncome() {
		$boolIsValid = true;

		$intFrequency = ( false === is_null( $this->m_objCustomerIncome->getFrequencyId() ) )? intval( $this->m_objCustomerIncome->getFrequencyId() ):0;

		switch( $intFrequency ) {

			case CCustomerIncome::HOURLY:
				$boolIsValid &= $this->valHourlyIncome();
				break;

			case CCustomerIncome::WEEKLY:
				$boolIsValid &= $this->valIncomeByFrequency( $strMsg = 'Weekly' );
				break;

			case CCustomerIncome::BI_WEEKLY:
				$boolIsValid &= $this->valIncomeByFrequency( $strMsg = 'Bi-weekly' );
				break;

			case CCustomerIncome::SEMI_MONTHLY:
				$boolIsValid &= $this->valIncomeByFrequency( $strMsg = 'Semi-monthly' );
				break;

			case CCustomerIncome::MONTHLY:
				$boolIsValid &= $this->valIncomeByFrequency( $strMsg = 'Monthly' );
				break;

			case CCustomerIncome::ANNUALY:
				$boolIsValid &= $this->valIncomeByFrequency( $strMsg = 'Annual' );
				break;

			default:
				return $boolIsValid;
				break;
		}
		return $boolIsValid;
	}

	public function valHourlyIncome() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getHourlyWage() ) || false == valStr( $this->m_objCustomerIncome->getHourlyWage() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_wage', __( 'Hourly wage is required.' ) ) );

		} elseif( ( false === is_numeric( $this->m_objCustomerIncome->getHourlyWage() ) ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_wage', __( 'Hourly wage is not valid.' ) ) );
		}

		if( true == is_null( $this->m_objCustomerIncome->getHourPerWeek() ) || false == valStr( $this->m_objCustomerIncome->getHourPerWeek() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hour_per_week', __( 'Hours per week is required.' ) ) );

		} elseif( ( false === is_numeric( $this->m_objCustomerIncome->getHourPerWeek() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hour_per_week', __( 'Hours per week is not valid.' ) ) );
		}

		if( true == is_null( $this->m_objCustomerIncome->getWeekPerYear() ) || false == valStr( $this->m_objCustomerIncome->getWeekPerYear() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_per_year', __( 'Weeks per year is required.' ) ) );

		} elseif( ( false === is_numeric( $this->m_objCustomerIncome->getWeekPerYear() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_per_year', __( 'Weeks per year is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIncomeByFrequency( $strMsg ) {

		$boolIsValid = true;

		if( true === is_null( $this->m_objCustomerIncome->getAmount() ) || false == valStr( $this->m_objCustomerIncome->getAmount() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( '{%s,0} salary is required.', [ $strMsg ] ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getAmount() ) ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( '{%s,0} salary is not valid.', [ $strMsg ] ) ) );

		}
		return $boolIsValid;
	}

	public function valNonFederalWage() {

		$boolIsValid = true;

		$boolOvertimeVal 			= $this->m_objCustomerIncome->getMemberReceivesOvertimePay();
		$boolBonusesVal 			= $this->m_objCustomerIncome->getMemberReceivesBonuses();
		$boolTipsVal 				= $this->m_objCustomerIncome->getMemberReceivesTips();

		if( true == $boolOvertimeVal ) {
			$boolIsValid &= $this->valOverTimeIncome();
		}

		if( true == $boolBonusesVal ) {
			$boolIsValid &= $this->valBonusesIncome();
		}

		if( true == $boolTipsVal ) {
			$boolIsValid &= $this->valTipsIncome();
		}

		return $boolIsValid;
	}

	public function valOverTimeIncome() {
		$boolIsValid = true;

		if( true === is_null( $this->m_objCustomerIncome->getOvertimeWage() ) || false == valStr( $this->m_objCustomerIncome->getOvertimeWage() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_wage', __( 'Overtime wage is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getOvertimeWage() ) || ( 0 >= $this->m_objCustomerIncome->getOvertimeWage() ) ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_wage', __( 'Overtime wage is not valid.' ) ) );
		}

		if( true === is_null( $this->m_objCustomerIncome->getOvertimeHoursPerWeek() ) || false == valStr( $this->m_objCustomerIncome->getOvertimeHoursPerWeek() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_hours_per_week', __( 'Overtime hours per week is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getOvertimeHoursPerWeek() ) || ( 0 >= $this->m_objCustomerIncome->getOvertimeHoursPerWeek() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_hours_per_week', __( 'Overtime hours per week is not valid.' ) ) );
		}

		if( true === is_null( $this->m_objCustomerIncome->getOvertimeWeeksPerYear() ) || false == valStr( $this->m_objCustomerIncome->getOvertimeWeeksPerYear() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_weeks_per_year', __( 'Overtime weeks per year is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getOvertimeWeeksPerYear() ) || ( 0 >= $this->m_objCustomerIncome->getOvertimeWeeksPerYear() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overtime_weeks_per_year', __( 'Overtime weeks per year is not valid.' ) ) );
		}
		return $boolIsValid;
	}

	public function valBonusesIncome() {
		$boolIsValid = true;

		if( true === is_null( $this->m_objCustomerIncome->getFrequencyOfBonuses() ) || false == valStr( $this->m_objCustomerIncome->getFrequencyOfBonuses() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_of_bonuses', __( 'Frequency of bonuses is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getFrequencyOfBonuses() ) || ( 0 >= $this->m_objCustomerIncome->getFrequencyOfBonuses() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_of_bonuses', __( 'Frequency of bonuses is not valid.' ) ) );
		}

		if( true === is_null( $this->m_objCustomerIncome->getBonusAmount() ) || false == valStr( $this->m_objCustomerIncome->getBonusAmount() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus_amount', __( 'Bonus amount is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getBonusAmount() ) || ( 0 >= $this->m_objCustomerIncome->getBonusAmount() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus_amount', __( 'Bonus amount is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTipsIncome() {
		$boolIsValid = true;

		if( true === is_null( $this->m_objCustomerIncome->getFrequencyOfTips() ) || false == valStr( $this->m_objCustomerIncome->getFrequencyOfTips() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_of_tips', __( 'Frequency of tips is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getFrequencyOfTips() ) || ( 0 >= $this->m_objCustomerIncome->getFrequencyOfTips() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_of_tips', __( 'Frequency of tips is not valid.' ) ) );
		}

		if( true === is_null( $this->m_objCustomerIncome->getTipAmount() ) || false == valStr( $this->m_objCustomerIncome->getTipAmount() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tip_amount', __( 'Tip amount is required.' ) ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getTipAmount() ) || ( 0 >= $this->m_objCustomerIncome->getTipAmount() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tip_amount', __( 'Tip amount is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOtherIncome() {
		$boolIsValid = true;

		if( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintPension ) ) {

			$strAmountRequiredMsg		= __( 'Benefits/pension amount is required.' );
			$strAmountNotValidMsg		= __( 'Benefits/pension amount is not valid.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintPublicAssistance ) ) {

			$strAmountRequiredMsg		= __( 'Public assistance amount is required.' );
			$strAmountNotValidMsg		= __( 'Public assistance amount is not valid.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintOther ) ) {

			$strAmountRequiredMsg		= __( 'Other income amount is required.' );
			$strAmountNotValidMsg		= __( 'Other income amount is not valid.' );

		} elseif( true == in_array( $this->m_objCustomerIncome->getIncomeTypeId(), CIncomeType::$c_arrintGeneral ) ) {

			$strAmountRequiredMsg		= __( 'General income amount is required.' );
			$strAmountNotValidMsg		= __( 'General income amount is not valid.' );
		}

		if( true === is_null( $this->m_objCustomerIncome->getAmount() ) || false == valStr( $this->m_objCustomerIncome->getAmount() ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', $strAmountRequiredMsg ) );

		} elseif( ( false == is_numeric( $this->m_objCustomerIncome->getAmount() ) || ( 0 >= $this->m_objCustomerIncome->getAmount() ) ) ) {

			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', $strAmountNotValidMsg ) );
		}

		return $boolIsValid;
	}

	public function valSocialSecurityClaimNumberEncrypted() {

		if( CIncomeType::SOCIAL_SECURITY == $this->m_objCustomerIncome->getIncomeTypeId() ) {

			$intLength = \Psi\CStringService::singleton()->strlen( $this->m_objCustomerIncome->getSocialSecurityClaimNumber() );

			if( 0 == $intLength ) {
				return true;
			}

			if( 12 < $intLength ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'social_security_claim_number_encrypted', __( 'Social Security Claim Number can not be more than 12 digits.' ) ) );
				return false;
			}

			if( !preg_match( '/^[a-zA-Z0-9]{1,12}$/', $this->m_objCustomerIncome->getSocialSecurityClaimNumber() ) ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'social_security_claim_number_encrypted', __( 'Social Security Claim Number must be alphanumeric characters.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valInstitutionName( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getInstitutionName() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_name' : 'payers_name';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valPayerPhoneNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true ) {

		$boolIsValid = true;
		$intLength 	 = strlen( $this->m_objCustomerIncome->getPayerPhoneNumber() );

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . '_phone_number' : 'payers_phone_number';

		if( true == $boolIsRequired && true == $this->m_boolValidateRequiredFields ) {
			if( true == is_null( $this->m_objCustomerIncome->getPayerPhoneNumber() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} phone number is required.', [ $strSectionName ] ) ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} phone number must be between 10 and 15 characters.', [ $strSectionName ] ) ) );

		} elseif( true == $this->m_boolValidateRequiredFields && false == is_null( $this->m_objCustomerIncome->getPayerPhoneNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objCustomerIncome->getPayerPhoneNumber(), false ) ) ) {

			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} phone number is required.', [ $strSectionName ] ), 504 ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPayerAddress( $strSectionName = NULL, $strFieldNamePrefix = NULL ) {
		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$boolIsValid = true;
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		if( false == $objPostalAddressService->isValid( $this->m_objCustomerIncome->getPostalAddress() ) && true == $this->m_boolValidateRequiredFields ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();
			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->m_objCustomerIncome->getPostalAddress()->getPostalCode() ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData      = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}
			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, $this->m_objCustomerIncome, $strFieldNamePrefix );
				$boolIsValid = false;
			}

		}

		return $boolIsValid;
	}

	public function valPostalCode( $boolIsPrimaryApplicant = true, $strSectionName = NULL, $arrobjPropertyPreferences = NULL ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $this->m_objCustomerIncome->getIncomeTypeId() )? 'income_' . $this->m_objCustomerIncome->getIncomeTypeId() . $this->m_objCustomerIncome->getIncomeTypeCount() . '_postal_code' : 'postal_code';

		if( true == $this->m_boolValidateRequiredFields && 0 == strlen( trim( $this->m_objCustomerIncome->getInstitutionPostalCode() ) ) && true == valArr( $arrobjPropertyPreferences ) && ( ( true == $boolIsPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) || ( false == $boolIsPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CO_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} zip/postal code is required.', [ $strSectionName ] ) ) );

		} elseif( 0 < strlen( trim( $this->m_objCustomerIncome->getInstitutionPostalCode() ) ) && false == CApplicationUtils::validateZipCode( $this->m_objCustomerIncome->getInstitutionPostalCode(), NULL ) ) {
			$boolIsValid = false;
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Income information';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName,  __( '{%s,0} address zip code must be 5 to 10 characters.', [ $strSectionNameToDisplay ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPosition( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && true == is_null( $this->m_objCustomerIncome->getPositionOrTitle() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_position' : 'position';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} position is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valAmount( $intIncomeTypeId = 0, $strSectionName = NULL, $strCustomerFieldName = NULL, $boolIsMonthlyIncomeOnly = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getAmount() ) || 0 == ( int ) $this->m_objCustomerIncome->getAmount() ) {
			$boolIsValid &= false;
			$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strMonthlyIncomeOnly   = ( true == $boolIsMonthlyIncomeOnly ) ? __( 'monthly income' ) : __( 'income' );
			$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : $strMonthlyIncomeOnly;
			$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_gross_amount' : 'gross_amount';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valContactName( $intIncomeTypeId = 0, $strSectionName = NULL, $strCustomerFieldName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerIncome->getContactName() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact name';
			$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_contact_name' : 'contact_name';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valContactPhoneNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$objContactPhoneNumber = $this->createPhoneNumber( $this->m_objCustomerIncome->getContactPhoneNumber() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : __( 'contact phone' );
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_contact_phone_number' : 'contact_phone_number';

		if( true == $boolIsRequired && true == $this->m_boolValidateRequiredFields ) {
			if( true == is_null( $objContactPhoneNumber->getNumber() ) ) {
				$boolIsValid &= false;
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		}

		if( true == valStr( $objContactPhoneNumber->getNumber() ) && false == $objContactPhoneNumber->isValid() ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,section_name} {%s,field_name} is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objContactPhoneNumber->getFormattedErrors(), [ 'section_name' => $strSectionName, 'field_name' => $strCustomerFieldName, 'region' => $objContactPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objContactPhoneNumber->getCountryCode() ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}
		return $boolIsValid;
	}

	public function valContactFaxNumber( $intIncomeTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = strlen( $this->m_objCustomerIncome->getContactFaxNumber() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact fax';
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_contact_fax_number' : 'contact_fax_number';

		if( true == $this->m_boolValidateRequiredFields && true == $boolIsRequired ) {
			if( true == is_null( $this->m_objCustomerIncome->getContactFaxNumber() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number must be between 10 and 15 characters.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( true == $this->m_boolValidateRequiredFields && false == is_null( $this->m_objCustomerIncome->getContactFaxNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objCustomerIncome->getContactFaxNumber(), false ) ) ) {

			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valContactEmail( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = strlen( $this->m_objCustomerIncome->getContactEmail() );

		$strSectionName 		= ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strCustomerFieldName 	= ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact email';
		$strFieldName 			= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_contact_email' : 'contact_email';

		if( true == $this->m_boolValidateRequiredFields && true == $boolIsRequired ) {
			if( true == is_null( $this->m_objCustomerIncome->getContactEmail() ) || 0 == $intLength ) {
				$boolIsValid &= false;
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		}

		if( 0 < $intLength && false == CValidation::validateEmailAddresses( $this->m_objCustomerIncome->getContactEmail() ) ) {
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valContactEmailFormat() {
		$boolIsValid = true;

		if( false === is_null( $this->m_objCustomerIncome->getContactEmail() ) && false === filter_var( $this->m_objCustomerIncome->getContactEmail(), FILTER_VALIDATE_EMAIL ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email', __( 'Email address does not appear to be valid.' ) ) );
			$this->m_objCustomerIncome->setContactEmail( NULL );
		}

		return $boolIsValid;
	}

	public function valDateStarted( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = false ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_date_started' : 'date_started';

		if( true == $this->m_boolValidateRequiredFields && true == $boolIsRequired && true == is_null( $this->m_objCustomerIncome->getDateStarted() ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( 0 < strlen( $this->m_objCustomerIncome->getDateStarted() ) && true == $boolIsRequired && ( ( ( true == is_null( $this->m_objCustomerIncome->getDateStarted() ) || 0 > strlen( $this->m_objCustomerIncome->getDateStarted() ) || false == CValidation::validateISODate( $this->m_objCustomerIncome->getDateStarted() ) ) ) || ( false == is_null( $this->m_objCustomerIncome->getDateStarted() ) && 0 < strlen( $this->m_objCustomerIncome->getDateStarted() ) && 1 !== CValidation::checkISODate( $this->m_objCustomerIncome->getDateStarted() ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date must be in mm/dd/yyyy form.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		} elseif( 0 < strlen( $this->m_objCustomerIncome->getDateStarted() ) && strtotime( $this->m_objCustomerIncome->getDateStarted() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date must not be greater than today\'s date.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		if( true === $boolIsValid ) {
			if( false == is_null( $this->m_objCustomerIncome->getDateStarted() ) || true == $boolIsRequired )
				$mixValidatedStartDate = CValidation::checkISODateFormat( $this->m_objCustomerIncome->getDateStarted(), $boolFormat = true );

			if( false === $mixValidatedStartDate ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} start date must be in mm/dd/yyyy format.', [ $strSectionName ] ) ) );
				$boolIsValid &= false;
			} else {
				if( true === isset( $mixValidatedStartDate ) )
					$this->m_objCustomerIncome->setDateStarted( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateEnded( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = false ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_date_stopped' : 'date_stopped';

		if( true == $this->m_boolValidateRequiredFields && true == $boolIsRequired && ( false == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( ( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && false == CValidation::validateISODate( $this->m_objCustomerIncome->getDateEnded() ) ) || ( true == stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) && 1 !== CValidation::checkISODate( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date must be in mm/dd/yyyy form.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		} elseif( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateStarted() ) ) ) && true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && ( strtotime( $this->m_objCustomerIncome->getDateStarted() ) ) > ( strtotime( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date must be greater than the start date.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		if( true === $boolIsValid ) {
			if( false == is_null( $this->m_objCustomerIncome->getDateEnded() ) || true == $boolIsRequired ) {
				$mixValidatedEndDate = CValidation::checkISODateFormat( $this->m_objCustomerIncome->getDateEnded(), $boolFormat = true );
			}

			if( false === $mixValidatedEndDate ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} stopped date must be in mm/dd/yyyy format.', [ $strSectionName ] ) ) );
				$boolIsValid &= false;
			} else {
				if( true === isset( $mixValidatedEndDate ) )
					$this->m_objCustomerIncome->setDateEnded( date( 'm/d/Y', strtotime( $mixValidatedEndDate ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valEndDate( $intIncomeTypeId = 0, $strSectionName = NULL, $boolIsRequired = false ) {

		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
		$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_end_date' : 'end_date';

		if( true == $boolIsRequired && false == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} end date is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && false == CValidation::checkISODateFormat( $this->m_objCustomerIncome->getDateEnded(), $boolFormat = true ) ) {
			$this->m_objCustomerIncome->setDateEnded( NULL );

		} elseif( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && CIncomeType::OTHER_INCOME_INDEFINITE_END_DATE != __( '{%t, 0, DATE_NUMERIC_ISO}', [ $this->m_objCustomerIncome->getDateEnded() ] ) && true == strtotime( $this->m_objCustomerIncome->getDateEnded() ) < strtotime( 'today' ) ) {
			$this->m_objCustomerIncome->setDateEnded( NULL );
		}

		return $boolIsValid;
	}

	// @FIXME: This funtion is duplicate wih "valDateEnded()"
	// Need to find where the verbage "date_stopped" is used.

	public function valIncomeEndDate( $strSectionName = NULL, $boolIsRequired = false ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && ( false == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} End date is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( ( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && false == CValidation::validateISODate( $this->m_objCustomerIncome->getDateEnded() ) ) || ( true == stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) && 1 !== CValidation::checkISODate( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} End date must be in mm/dd/yyyy form.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		} elseif( true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateStarted() ) ) ) && true == valStr( stripslashes( trim( $this->m_objCustomerIncome->getDateEnded() ) ) ) && ( strtotime( $this->m_objCustomerIncome->getDateStarted() ) ) > ( strtotime( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} End date must be greater than the start date.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		if( true === $boolIsValid ) {
			if( false == is_null( $this->m_objCustomerIncome->getDateEnded() ) || true == $boolIsRequired ) {
				$mixValidatedEndDate = CValidation::checkISODateFormat( $this->m_objCustomerIncome->getDateEnded(), $boolFormat = true );
			}

			if( false === $mixValidatedEndDate ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} End date must be in mm/dd/yyyy format.', [ $strSectionName ] ) ) );
				$boolIsValid &= false;
			} else {
				if( true === isset( $mixValidatedEndDate ) )
					$this->m_objCustomerIncome->setDateEnded( date( 'm/d/Y', strtotime( $mixValidatedEndDate ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateEndedIncome( $strAction = NULL ) {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && ( true === is_null( $this->m_objCustomerIncome->getDateEnded() ) || false == valStr( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'Terminate/end date is required.' ) ) );
			$boolIsValid &= false;

		} elseif( $strAction == 'VALIDATE_TERMINATE_INCOME' && ( strtotime( $this->m_objCustomerIncome->getIncomeEffectiveDate() ) > strtotime( $this->m_objCustomerIncome->getDateEnded() ) ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'Terminate/end date must be greater than or equal to effective date.' ) ) );
		}

		if( true === $boolIsValid ) {
			$mixValidatedStartDate = CValidation::checkISODateFormat( $this->m_objCustomerIncome->getDateEnded(), $boolFormat = true );
			if( false === $mixValidatedStartDate ) {
				$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'Terminate/end date must be in mm/dd/yyyy format.' ) ) );
				$boolIsValid &= false;
			} else {
				$this->m_objCustomerIncome->setDateEnded( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && true == is_null( $this->m_objCustomerIncome->getDescription() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_description' : '_description';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} description is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valTypeOfBusiness( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && false == valStr( $this->m_objCustomerIncome->getTypeOfBusiness() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_type_of_business' : '_type_of_business';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} type of busines is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valBusinessTaxId( $intIncomeTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && false == valStr( $this->m_objCustomerIncome->getBusinessTaxId() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_business_tax_id' : '_business_tax_id';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} business tax id is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		} elseif( preg_match( '/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $this->m_objCustomerIncome->getBusinessTaxId() ) ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerIncome->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intIncomeTypeId )? 'income_' . $intIncomeTypeId . $this->m_objCustomerIncome->getIncomeTypeCount() . '_business_tax_id' : '_business_tax_id';
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} business tax id can not contain special characters.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valIncomeEffectiveDate() {
		$boolIsValid = true;

		if( true == $this->m_boolValidateRequiredFields && true === is_null( $this->m_objCustomerIncome->getIncomeEffectiveDate() ) || false == valStr( $this->m_objCustomerIncome->getIncomeEffectiveDate() ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_effective_date', __( 'Effective date is required.' ) ) );
		}

		if( false === is_null( $this->m_objCustomerIncome->getDateEnded() ) && strtotime( $this->m_objCustomerIncome->getIncomeEffectiveDate() ) > strtotime( $this->m_objCustomerIncome->getDateEnded() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'Effective date should be less than terminate/end date.' ) ) );
		}

		$mixValidatedStartDate = CValidation::checkISODateFormat( $this->m_objCustomerIncome->getIncomeEffectiveDate(), $boolFormat = true );
		if( false === $mixValidatedStartDate ) {
			$this->m_objCustomerIncome->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_effective_date', __( 'Effective date must be in mm/dd/yyyy format.' ) ) );
			$boolIsValid &= false;
		} else {
			$this->m_objCustomerIncome->setIncomeEffectiveDate( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
		}

		return $boolIsValid;
	}

	public function valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $strFieldNamePrefix = NULL ) {

		$boolIsValid = true;

		if( false == valObj( $objIncomeType, 'CIncomeType' ) ) {
			return $boolIsValid;
		}

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			$arrobjPropertyApplicationPreferences = [];
		}

		if( false == valArr( $arrstrIncomeTypeCustomFields ) ) {
			return $boolIsValid;
		}

		if( false == array_key_exists( $objIncomeType->getId(), $arrstrIncomeTypeCustomFields ) ) {
			return $boolIsValid;
		}

		$strShowIncomeTypeKey = 'HIDE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey();
		if( true == array_key_exists( $strShowIncomeTypeKey, $arrobjPropertyApplicationPreferences ) ) {
			return $boolIsValid;
		}

		$intIncomeTypeId 			= $objIncomeType->getId();

		if( false == is_null( $objIncomeType->getIncomeTypeCount() ) ) {
			$this->m_objCustomerIncome->setIncomeTypeCount( '_' . $objIncomeType->getIncomeTypeCount() );
		}

		$strSectionName 			= $objIncomeType->getName();

		$strRenameIncomeTypeKey 	= 'RENAME_INCOME_TYPE_' . $objIncomeType->getPreferenceKey();
		// $strOverrideIncomeTypeKey 	= 'OVERRIDE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey();

		if( true == array_key_exists( $strRenameIncomeTypeKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences[$strRenameIncomeTypeKey]->getValue() ) ) {
			$strSectionName 		= $arrobjPropertyApplicationPreferences[$strRenameIncomeTypeKey]->getValue();
		}

		$strRequireIncomeTypeNameFieldKey 			= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_NAME';
		$strRequireIncomeTypePhoneNumberFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_PHONE_NUMBER';
		$strRequireIncomeTypeAddressFieldKey 		= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_ADDRESS';
		$strRequireIncomeTypeContactNameFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_NAME';
		$strRequireIncomeTypeContactPhoneFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';
		$strRequireIncomeTypeContactFaxFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_FAX_NUMBER';
		$strRequireIncomeTypeContactEmailFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_CONTACT_EMAIL';
		$strRequireIncomeTypePositionFieldKey 		= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_POSITION';
		$strRequireIncomeTypeDateStartedFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DATE_STARTED';
		$strRequireIncomeTypeDateStoppedFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DATE_STOPPED';
		$strRequireIncomeTypeDescriptionFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_DESCRIPTION';
		$strRequireIncomeTypeTypeOFBusinessFieldKey = 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_TYPE_OF_BUSINESS';
		$strRequireIncomeTypeBusinessTaxIdFieldKey 	= 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_BUSINESS_TAX_ID';
		$strRequireIncomeTypeEndDateFieldKey 	    = 'REQUIRE_INCOME_TYPE_' . $objIncomeType->getPreferenceKey() . '_END_DATE';

		if( true == array_key_exists( 'INSTITUTION_NAME', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valInstitutionName( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'ADDRESS', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeAddressFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valPayerAddress( $strSectionName, $strFieldNamePrefix );
		}

		if( true == array_key_exists( 'CONTACT_NAME', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactName( $intIncomeTypeId, $strSectionName, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_NAME']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_PHONE', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactPhoneFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactPhoneNumber( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_PHONE']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_PHONE', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactPhoneFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerIncome->getContactPhoneNumber() ) ) {
			$boolIsValid &= $this->valContactPhoneNumber( $intIncomeTypeId, $strSectionName, false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_PHONE']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerIncome->getContactFaxNumber() ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intIncomeTypeId, $strSectionName, false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_EMAIL', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactEmail( $intIncomeTypeId, $strSectionName, true, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_EMAIL', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerIncome->getContactEmail() ) ) {
			$boolIsValid &= $this->valContactEmail( $intIncomeTypeId, $strSectionName, false, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );
		}

		$boolIsValid &= $this->valAmount( $intIncomeTypeId, $strSectionName, $arrstrIncomeTypeCustomFields[$intIncomeTypeId]['MONTHLY_INCOME']['FIELD_NAME'] );

		if( true == array_key_exists( 'POSITION', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypePositionFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valPosition( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'STARTED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDateStartedFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDateStarted( $intIncomeTypeId, $strSectionName, true );

		} elseif( true == array_key_exists( 'STARTED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeDateStartedFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerIncome->getDateStarted() ) ) {
			$boolIsValid &= $this->valDateStarted( $intIncomeTypeId, $strSectionName, false );
		}

		if( true == array_key_exists( 'STOPPED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDateStoppedFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDateEnded( $intIncomeTypeId, $strSectionName, true );

		} elseif( true == array_key_exists( 'STOPPED_ON', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && false == array_key_exists( $strRequireIncomeTypeDateStoppedFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerIncome->getDateEnded() ) ) {
			$boolIsValid &= $this->valDateEnded( $intIncomeTypeId, $strSectionName, false );
		}

		if( true == array_key_exists( 'DESCRIPTION', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeDescriptionFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valDescription( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'TYPE_OF_BUSINESS', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeTypeOFBusinessFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valTypeOfBusiness( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'BUSINESS_TAX_ID', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeBusinessTaxIdFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valBusinessTaxId( $intIncomeTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'END_DATE', $arrstrIncomeTypeCustomFields[$intIncomeTypeId] ) && true == array_key_exists( $strRequireIncomeTypeEndDateFieldKey, $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences[$strRequireIncomeTypeEndDateFieldKey]->getValue() ) {
			$boolIsValid &= $this->valEndDate( $intIncomeTypeId, $strSectionName, $this->m_boolValidateRequiredFields );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $arrobjPropertyPreferences = NULL, $boolValidateEndDate = true, $strFieldNamePrefix = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeEffectiveDate();
				$boolIsValid &= $this->valIncomeEndDate();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valSocialSecurityClaimNumberEncrypted();
				$boolIsValid &= $this->valFrequencyDetailId();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valContactEmailFormat();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'customer_incomes':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $strFieldNamePrefix );
				break;

			case 'application_incomes':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valRequiredFields( $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $strFieldNamePrefix );
				break;

			case 'applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valAmount( 0, __( 'Applicant' ) );
				break;

			case 'new_applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeTypeId();
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_APPLICANT_EMPLOYER_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valInstitutionName( 0, 'Employer' );
				}
				$boolIsValid &= $this->valPostalCode( true, 'Employer', $arrobjPropertyPreferences );
				$boolIsValid &= $this->valAmount( 0, __( 'Applicant' ) );
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_EMPLOYMENT_START_DATE', $arrobjPropertyPreferences ) ) {
					$boolIsEmploymentStartDateRequired = true;
				} else {
					$boolIsEmploymentStartDateRequired = false;
				}
				$boolIsValid &= $this->valDateStarted( CIncomeType::CURRENT_EMPLOYER, 'Employment', $boolIsEmploymentStartDateRequired );
				break;

			case 'secondary_applicant_pre_screening':
				$boolIsValid &= $this->valCid();
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CO_APPLICANT_EMPLOYER_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valInstitutionName( 0, 'Employer' );
				}
				$boolIsValid &= $this->valPostalCode( false, 'Employer', $arrobjPropertyPreferences );
				$boolIsValid &= $this->valIncomeTypeId();
				break;

			case 'VALIDATE_INSERT_PAST_INCOME':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIncomeEffectiveDate();
				$boolIsValid &= $this->valDateEndedIncome();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valSocialSecurityClaimNumberEncrypted();
				$boolIsValid &= $this->valFrequencyDetailId();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valContactEmailFormat();
				break;

			case 'VALIDATE_INCREMENT_DECREMENT_PAST_INCOME':
				$boolIsValid &= $this->valIncomeEffectiveDate();
				$boolIsValid &= $this->valDateEndedIncome();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valSocialSecurityClaimNumberEncrypted();
				$boolIsValid &= $this->valFrequencyDetailId();
				$boolIsValid &= $this->valDetails();
				break;

			case 'VALIDATE_INCREMENT_DECREMENT':
				$boolIsValid &= $this->valIncomeEffectiveDate();
				if( true == $boolValidateEndDate ) {
					$boolIsValid &= $this->valDateEndedIncome();
				}
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valIncomeTypeId();
				$boolIsValid &= $this->valSocialSecurityClaimNumberEncrypted();
				$boolIsValid &= $this->valFrequencyDetailId();
				$boolIsValid &= $this->valDetails();
				break;

			case 'VALIDATE_INFORMATION':
				$boolIsValid &= $this->valContactEmailFormat();
				break;

			case 'VALIDATE_TERMINATE_INCOME':
				$boolIsValid &= $this->valDateEndedIncome( $strAction );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}
?>