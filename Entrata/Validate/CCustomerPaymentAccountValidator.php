<?php

use \Applications\ResidentPortal\Library\Intents\Common\Models\CPadBankInfo;

class CCustomerPaymentAccountValidator {

	/** @var CCustomerPaymentAccount */
	protected $m_objCustomerPaymentAccount;

	public function __construct() {
		return;
	}

	public function setCustomerPaymentAccount( $objCustomerPaymentAccount ) {
		$this->m_objCustomerPaymentAccount = $objCustomerPaymentAccount;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getId() ) || 0 >= ( int ) $this->m_objCustomerPaymentAccount->getId() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Payment Account Request:  Id required - CCustomerPaymentAccount::valId()', E_USER_WARNING );
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, 'id', 'Id required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCid() ) || 0 >= ( int ) $this->m_objCustomerPaymentAccount->getCid() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Payment Account Request:  Management id required - CCustomerPaymentAccount::valCid()', E_USER_WARNING );
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, 'cid', 'client id required.' ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCustomerId() ) || 0 >= ( int ) $this->m_objCustomerPaymentAccount->getCustomerId() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Payment Account Request:  Customer Id required - CCustomerPaymentAccount::valCustomerId()', E_USER_WARNING );
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, 'customer_id', 'Customer id required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) || 0 >= ( int ) $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Payment Account Request:  Ar Payment Type Id required - CCustomerPaymentAccount::valPaymentTypeId()', E_USER_WARNING );
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', 'Payment type id required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoAccountNumber() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoCompanyName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoNameFirst() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoNameMiddle() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoNameLast() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoStreetLine1() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomerPaymentAccount->getBilltoStreetLine1() ) ) {
			$boolIsValid = false;

			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', 'Billing street is required', 612 ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStreetLine2() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoStreetLine3() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoCity() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getBilltoCity() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_city', 'Billing city is required', 613 ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getBilltoStateCode() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', 'Billing state code is required', 614 ) );
		}

		return $boolIsValid;
	}

	public function valBilltoProvince() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBilltoPostalCode() {
		$boolIsValid = true;
		if( false == valStr( $this->m_objCustomerPaymentAccount->getBilltoPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', 'Billing postal code is required', 615 ) );
		} elseif( false == CApplicationUtils::validateZipCode( $this->m_objCustomerPaymentAccount->getBilltoPostalCode(), NULL ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  ' Billing address zip code must be 5 to 10 characters.', NULL ) );
		} elseif( false == CValidation::validateMilitaryPostalCode( $this->m_objCustomerPaymentAccount->getBilltoPostalCode(), $this->m_objCustomerPaymentAccount->getBilltoStateCode() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  'Billing military state zip code must start with 0 or 9.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valBilltoCountryCode() {
		$boolIsValid = true;
		if( false == preg_match( '/^[A-Z]{2}$/', $this->m_objCustomerPaymentAccount->getBilltoCountryCode() ) || 2 < strlen( $this->m_objCustomerPaymentAccount->getBilltoCountryCode() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_country_code', 'Billing country code is required', 615 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBilltoPhoneNumber() {
		$boolIsValid = true;

		// Validation example
		$intLength = ( ( false == is_null( $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() ) || 0 < strlen( $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() ) ) && true == preg_match( '/[0-9]/', $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() ) ) ? strlen( $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() ) : 0;

		if( 0 == $intLength ) return $boolIsValid;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getId() ) && 0 < $this->m_objCustomerPaymentAccount->getApplicantId() ) {
			if( 7 > $intLength || 15 < $intLength ) {
				$boolIsValid = false;
				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Billing phone number must be between 10 and 15 characters.', 611 ) );
			} elseif( false == is_null( $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber(), false ) ) ) {

				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Valid billing phone number is required.', 504 ) );
				$boolIsValid = false;
			}
		} else {
			$strPhoneNumber = str_replace( '-', '', $this->m_objCustomerPaymentAccount->getBilltoPhoneNumber() );
			if( false == preg_match( '/^[0-9]*$/', $strPhoneNumber ) ) {
				$boolIsValid = false;
				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Phone number must be in xxx-xxx-xxxx format.', 611 ) );
			}
		}

		return $boolIsValid;
	}

	public function valBilltoEmailAddress() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objCustomerPaymentAccount->getBilltoEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objCustomerPaymentAccount->getBilltoEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_email_address', 'Email Address does not appear to be valid' ) );
		}

		return $boolIsValid;
	}

	public function valSecureReferenceNumber() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objCustomerPaymentAccount->getSecureReferenceNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', 'Secure reference number is not valid.', 621 ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCcExpDateMonth() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', 'Expiration month is required', 607 ) );
		}

		if( false == is_null( $this->m_objCustomerPaymentAccount->getCcExpDateMonth() ) && false == is_null( $this->m_objCustomerPaymentAccount->getCcExpDateYear() ) && $this->m_objCustomerPaymentAccount->getCcExpDateYear() == date( 'Y' ) && $this->m_objCustomerPaymentAccount->getCcExpDateMonth() < date( 'm' ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration_date_month_year', 'Expiration date is invalid or expired.', 607 ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateYear() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCcExpDateYear() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', 'Expiration year is required', 608 ) );
		}

		if( false == is_null( $this->m_objCustomerPaymentAccount->getCcExpDateYear() ) && $this->m_objCustomerPaymentAccount->getCcExpDateYear() < date( 'Y' ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration_date_month_year', 'Expiration year is invalid or expired.', 607 ) );
		}

		return $boolIsValid;
	}

	public function valCcNameOnCard() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCcNameOnCard() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_name_on_card', 'Name on card is required.', 609 ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckBankName() ) || 0 == strlen( trim( $this->m_objCustomerPaymentAccount->getCheckBankName() ) ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'Bank name is required', 601 ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'Account type is required', 603 ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number is required' ), 602 ) );
		}

		if( false == preg_match( '/^[0-9]+$/', $this->m_objCustomerPaymentAccount->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number has invalid characters.' ) ) );
		}

		if( 4 > strlen( $this->m_objCustomerPaymentAccount->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number should be minimum 4 digits long.' ), 623 ) );
		}

		return $boolIsValid;
	}

	public function valConfirmCheckAccountNumber() {
		$boolIsValid = true;

		$strCcCardNumber = NULL;
		if( true == valStr( $this->m_objCustomerPaymentAccount->getCheckAccountNumberEncrypted() ) ) {
			$strCcCardNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_objCustomerPaymentAccount->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
		}

		if( 0 != strlen( $strCcCardNumber ) && 0 == strlen( $this->m_objCustomerPaymentAccount->getConfirmCheckAccountNumber() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confrim_check_account_number', 'Confirm account number is required.', 602 ) );
			$boolIsValid = false;
		} elseif( 0 != strlen( $strCcCardNumber ) && $strCcCardNumber != $this->m_objCustomerPaymentAccount->getConfirmCheckAccountNumber() ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verify_check_account_number', 'Confirm account number and account number do not match.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objPaymentDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) || 0 == strlen( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required', 605 ) );
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.', 622 ) );
			return false;
		}

		if( 9 != strlen( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be 9 digits long.', 623 ) );
			return false;
		}

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is not valid.', 624 ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckPadRoutingNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) || 0 == strlen( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required', 605 ) );
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.', 622 ) );
			return false;
		}

		if( 9 != strlen( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be 9 digits long.', 623 ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valCheckBlacklist( $objClientDatabase ) {
		$boolIsValid = true;

		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objClientDatabase, $this->m_objCustomerPaymentAccount->getCheckRoutingNumber(), $this->m_objCustomerPaymentAccount->getCheckAccountNumber(), $this->m_objCustomerPaymentAccount->getCheckAccountTypeId(), $this->m_objCustomerPaymentAccount->getCheckNameOnAccount() );

		if( NULL == $objBlacklistedEntry ) {
			return true;
		}

		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
			case CPaymentBlacklistType::NONE:
				return true;
			case CPaymentBlacklistType::ROUTING_NUMBER:
				// swap out the routing number
				$this->m_objCustomerPaymentAccount->setCheckRoutingNumber( $objBlacklistedEntry->getLookupStringCorrectedDecrypted() );

				// notify the user that we swapped the routing number
				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;

			default:
				$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valConflictingCustomerPaymentAccount( $objDatabase, $boolIsAdd = false ) {

		$boolIsValid = true;
		$intCustomerPaymentAccountCount = CCustomerPaymentAccounts::fetchConflictingCustomerPaymentAccountCountByCustomerPaymentAccountByCid( $this->m_objCustomerPaymentAccount, $objDatabase );
		if( 0 < $intCustomerPaymentAccountCount ) {
			$boolIsValid = false;
			$arrobjDuplicateCustomerPaymentAccount = CCustomerPaymentAccounts::fetchDuplicateCustomerPaymentAccountId( $this->m_objCustomerPaymentAccount, $objDatabase );
			$this->m_objCustomerPaymentAccount->setId( $arrobjDuplicateCustomerPaymentAccount[0]->getId() );
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __('This payment Method already exists.'), 624 ) );
		}

		// Checking the account number already exist with sodium encryption
		if( true == $boolIsAdd ) {
			$arrstrCustomerPaymentAccountsData = ( array ) \Psi\Eos\Entrata\CCustomerPaymentAccounts::createService()->fetchCustomerPaymentAccountsDataByPaymentTypeIdByCustomerIdByCid( $this->m_objCustomerPaymentAccount->getPaymentTypeId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $this->m_objCustomerPaymentAccount->getCid(), $objDatabase, $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() );

			$arrstrAccNumsAndCcCardNums = [];

			if( true == CPaymentTypes::isCreditCardPayment( $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) ) {
				foreach( $arrstrCustomerPaymentAccountsData as $strCcCreditCardNumber ) {
				    if( false == \Psi\Libraries\UtilFunctions\valStr( $strCcCreditCardNumber ) ) {
				        continue;
                    }

					$arrstrAccNumsAndCcCardNums[] = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strCcCreditCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
				}
				if( true == in_array( $this->m_objCustomerPaymentAccount->getCcCardNumber(), $arrstrAccNumsAndCcCardNums ) ) {
					$boolIsValid = false;
					$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __('This payment method already exists.'), 624 ) );
				}
			} elseif( \CPaymentType::ACH == $this->m_objCustomerPaymentAccount->getPaymentTypeId() || \CPaymentType::PAD == $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) {
				foreach( $arrstrCustomerPaymentAccountsData as $strAccountNumber ) {
					$arrstrAccNumsAndCcCardNums[] = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
				}
				if( true == in_array( $this->m_objCustomerPaymentAccount->getConfirmCheckAccountNumber(), $arrstrAccNumsAndCcCardNums ) ) {
					$boolIsValid = false;
					$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __('This payment method already exists.'), 624 ) );
				}
			}
		} elseif( false == $boolIsAdd && \CPaymentType::PAD == $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) {
			$objCustomerPaymentAccount = \Psi\Eos\Entrata\CCustomerPaymentAccounts::createService()->fetchCustomerPaymentAccountByIdByCustomerIdByCid( $this->m_objCustomerPaymentAccount->getId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $this->m_objCustomerPaymentAccount->getCid(), $objDatabase );

			if( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() != $objCustomerPaymentAccount->getCheckRoutingNumber() ) {
				$arrstrCustomerPaymentAccountsData = ( array ) \Psi\Eos\Entrata\CCustomerPaymentAccounts::createService()->fetchCustomerPaymentAccountsDataByPaymentTypeIdByCustomerIdByCid( $this->m_objCustomerPaymentAccount->getPaymentTypeId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $this->m_objCustomerPaymentAccount->getCid(), $objDatabase, $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() );

				$arrstrAccountNumbers = [];

				foreach( $arrstrCustomerPaymentAccountsData as $strAccountNumber ) {
					$arrstrAccountNumbers[] = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
				}
				if( true == in_array( $this->m_objCustomerPaymentAccount->getConfirmCheckAccountNumber(), $arrstrAccountNumbers ) ) {
					$boolIsValid = false;
					$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __('This payment method already exists.'), 624 ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerPaymentAccount->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'Account Holder&#39;s Name is required.', 604 ) );
		}

		return $boolIsValid;
	}

	public function valAccountNickName() {
		$boolIsValid = true;

		if( true == \Psi\Libraries\UtilFunctions\valStr( $this->m_objCustomerPaymentAccount->getAccountNickname() ) && false == preg_match( "/^[a\-z0\-9\s\-.',&\/\\\]+$/i", $this->m_objCustomerPaymentAccount->getAccountNickname() ) ) {
			$strAccountNickName = strip_tags( ( string ) $this->m_objCustomerPaymentAccount->getAccountNickname() );
			$this->m_objCustomerPaymentAccount->setAccountNickname( $strAccountNickName );
		}

		return $boolIsValid;
	}

	public function valPaymentAccountInfo( $objClientDatabase = NULL, $boolIsValidateBankName, $boolIsValidateBillToPostalCode = true, $strClientCountryCode = 'US', $boolIsSkipRoutingNumberValidation = false, $boolIsAdd = false, $boolValidatePostalCode = true, $objPaymentDatabase = NULL ) {
		$boolIsValid = true;
		switch( $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) {
			case NULL;
			$boolIsValid = false;
				break;

			case CPaymentType::ACH:
				if( true == $boolIsValidateBankName ) {
					$boolIsValid &= $this->valCheckBankName();
				}
				$boolIsValid &= $this->valCheckAccountNumber();
				$boolIsValid &= $this->valConfirmCheckAccountNumber();
				$boolIsValid &= $this->valCheckAccountTypeId();
				if( false == $boolIsSkipRoutingNumberValidation ) {
					$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
				}
				$boolIsValid &= $this->valConflictingCustomerPaymentAccount( $objClientDatabase, $boolIsAdd );
				$boolIsValid &= $this->valAccountNickName();
				$boolIsValid &= $this->valCheckNameOnAccount();
				$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
				break;

			case CPaymentType::PAD:
				$objPadBankInfo = CPadBankInfo::create();

				try {
					$objPadBankInfo->setRoutingNumber( $this->m_objCustomerPaymentAccount->getCheckRoutingNumber() );
					$objPadBankInfo->setAccountNumber( $this->m_objCustomerPaymentAccount->getCheckAccountNumber() );
					$objPadBankInfo->setNickname( $this->m_objCustomerPaymentAccount->getAccountNickname() );
					$objPadBankInfo->setAccountTypeId( $this->m_objCustomerPaymentAccount->getCheckAccountTypeId() );
					$objPadBankInfo->setNameOnAccount( $this->m_objCustomerPaymentAccount->getCheckNameOnAccount() );
				} catch( \Applications\ResidentPortal\Library\Intents\Exceptions\CFieldValidationException $objFieldValidationException ) {
					$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $objFieldValidationException->getFieldName(), $objFieldValidationException->getMessage() ) );
					$boolIsValid = false;
				}

				$boolIsValid &= $this->valConflictingCustomerPaymentAccount( $objClientDatabase, $boolIsAdd );
				$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
				break;

			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::DISCOVER:
			case CPaymentType::AMEX:
			$boolIsValid &= $this->valSecureReferenceNumber();
			$boolIsValid &= $this->valConflictingCustomerPaymentAccount( $objClientDatabase, $boolIsAdd );
			$boolIsValid &= $this->valCcExpDateMonth();
			$boolIsValid &= $this->valCcExpDateYear();
			$boolIsValid &= $this->valCcNameOnCard();
			$boolIsFromResidentPortalApp = ( true == valId( $this->m_objCustomerPaymentAccount->getUpdatedBy() ) && ( CUser::ID_RESIDENT_PORTAL_APP == $this->m_objCustomerPaymentAccount->getUpdatedBy() || CUser::ID_RPWHITELABELEDAPP == $this->m_objCustomerPaymentAccount->getUpdatedBy() ) );

			if( false != $boolIsValidateBillToPostalCode && 1 != $this->m_objCustomerPaymentAccount->getIsInternational() && false == $boolIsFromResidentPortalApp && $boolValidatePostalCode ) {
				$boolIsValid &= $this->valBilltoPostalCode();
			}
				break;

			case CPaymentType::SEPA_DIRECT_DEBIT:
				$boolIsValid &= $this->validateDuplicateSepaIbanAccount( $objClientDatabase, $objPaymentDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateDuplicateSepaIbanAccount( $objClientDatabase, $objPaymentDatabase ) {
		if( false == \Psi\Libraries\UtilFunctions\valObj( $objPaymentDatabase, \CDatabase::class ) ) {
			return true;
		}
		$arrobjPaymentBankAccounts = CCustomerPaymentAccounts::fetchConflictingPaymentBankAccountCountByCustomerPaymentAccountByCidByIban( $this->m_objCustomerPaymentAccount, $objPaymentDatabase );
		if( true == \Psi\Libraries\UtilFunctions\valArr( $arrobjPaymentBankAccounts ) ) {
			$arrintPaymentBankAccountIds = array_keys( rekeyObjects( 'Id', $arrobjPaymentBankAccounts ) );
			if( true == \Psi\Libraries\UtilFunctions\valArr( $arrintPaymentBankAccountIds ) ) {
				$intPaymentBankAccountCount = CCustomerPaymentAccounts::fetchConflictingCustomerPaymentAccountCountByPaymentBankAccountIdByCidByPropertyIdByCustomerId( $arrintPaymentBankAccountIds, $this->m_objCustomerPaymentAccount, $objClientDatabase );
				if( 0 < $intPaymentBankAccountCount ) {
					$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', __('This payment Method already exists.'), 624 ) );
					return false;
				}
			}
		}

		return true;
	}

	public function valGiactAccountVerificationAttempts( $objPaymentDatabase ) {
		$boolIsValid = true;

		$arrobjDeclinedGiactRequestLogs = \Psi\Eos\Payment\CGiactRequestLogs::createService()->fetchDeclinedGiactRequestLogsByCustomerIdByPropertyIdByCid( $this->m_objCustomerPaymentAccount->getCustomerId(), $this->m_objCustomerPaymentAccount->getPropertyId(), $this->m_objCustomerPaymentAccount->getCid(), $objPaymentDatabase );

		if( true == valArr( $arrobjDeclinedGiactRequestLogs ) && CGiactRequestLog::ALLOWEDRETRYLIMIT <= \Psi\Libraries\UtilFunctions\count( $arrobjDeclinedGiactRequestLogs ) ) {
			$boolIsValid = false;
			$objDeclinedGiactRequestLog = array_shift( $arrobjDeclinedGiactRequestLogs );

			$strPaymentDateTime = new DateTime( $objDeclinedGiactRequestLog->getRequestTimestamp() );
			$intTimeDifference 	= ( 24 - ( time() - $strPaymentDateTime->getTimestamp() ) / ( 60 * 60 ) );
			$intHours 			= floor( $intTimeDifference );
			$intMinutes 		= floor( ( $intTimeDifference - $intHours ) * 60 );

			$strHealTime		.= ( 0 != $intHours ) ? ( ' ' . ( int ) $intHours . ' hour(s)' ) : '';
			$strHealTime		.= ( 0 != $intMinutes ) ? ( ' ' . ( int ) $intMinutes . ' minutes' ) : '';
			$strHealTime		= ( '' != $strHealTime ) ? ' for next' . $strHealTime . '.' : '';

			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'You have exceeded the maximum number of allowed payment attempts (' . CGiactRequestLog::ALLOWEDRETRYLIMIT . ') and are restricted from making payment' . $strHealTime ) );
		}

		return $boolIsValid;
	}

	public function valCvvCode() {
		$boolIsValid = true;
		if( false == valId( $this->m_objCustomerPaymentAccount->getCvvCode() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cvv_code', 'CVV must be an valid integer' ) );
			$boolIsValid &= false;
		} elseif( CPaymentType::AMEX == $this->m_objCustomerPaymentAccount->getPaymentTypeId() && 4 <> strlen( $this->m_objCustomerPaymentAccount->getCvvCode() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cvv_code', 'Card verification value (CVV/CCV) must be 4 digits long for American Express cards.' ) );
			$boolIsValid &= false;
		} elseif( CPaymentType::AMEX <> $this->m_objCustomerPaymentAccount->getPaymentTypeId() && 3 <> strlen( $this->m_objCustomerPaymentAccount->getCvvCode() ) ) {
			$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cvv_code', 'Card verification value (CVV/CCV) must be 3 digits long for Visa, Discover, and Mastercard cards.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsValidateBankName, $boolIsFromMakeAPayment = false, $objPaymentDatabase = NULL, $strClientCountryCode = 'US', $boolIsValidateCvv = false, $boolIsSkipRoutingNumberValidation = false, $boolIsAdd = false, $boolValidatePostalCode = true ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentAccountInfo( $objClientDatabase, $boolIsValidateBankName, false, $strClientCountryCode, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode );

				$boolIsValid &= $this->valBilltoPhoneNumber();
				$boolIsValid &= $this->valBilltoStreetLine1();
				$boolIsValid &= $this->valBilltoCountryCode();

				if( 1 != $this->m_objCustomerPaymentAccount->getIsInternational() ) {
					$boolIsValid &= $this->valBilltoCity();
					$boolIsValid &= $this->valBilltoStateCode();
					if( true == $boolValidatePostalCode ) {
						$boolIsValid &= $this->valBilltoPostalCode();
					}
				}

				if( true == $boolIsValidateCvv ) {
					$boolIsValid &= $this->valCvvCode();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentAccountInfo( $objClientDatabase, $boolIsValidateBankName, false, $strClientCountryCode, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode );

				$boolIsValid &= $this->valBilltoPhoneNumber();
				$boolIsValid &= $this->valBilltoStreetLine1();
				$boolIsValid &= $this->valBilltoCountryCode();

				if( 1 != $this->m_objCustomerPaymentAccount->getIsInternational() ) {
					$boolIsValid &= $this->valBilltoCity();
					$boolIsValid &= $this->valBilltoStateCode();
					if( true == $boolValidatePostalCode ) {
						$boolIsValid &= $this->valBilltoPostalCode();
					}
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_payment_account_base_info':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentAccountInfo( $objClientDatabase, $boolIsValidateBankName, false, $strClientCountryCode, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode, $objPaymentDatabase );

				$boolIsValid &= $this->valBilltoPhoneNumber();

				if( true == $boolIsValidateCvv ) {
					$boolIsValid &= $this->valCvvCode();
				}
				break;

			case 'validate_customer_payment_account_for_rp2_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentAccountInfo( $objClientDatabase, $boolIsValidateBankName, true, $strClientCountryCode, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode );
				$boolIsValid &= $this->valBilltoCountryCode();
				$boolIsValid &= $this->valBilltoPhoneNumber();
				$boolIsValid &= $this->valBilltoStreetLine1();

				if( true == $boolIsValid && true == is_null( $this->m_objCustomerPaymentAccount->getPsProductOptionId() ) && CPaymentType::ACH == $this->m_objCustomerPaymentAccount->getPaymentTypeId() && false == $boolIsFromMakeAPayment ) {

					$objGiactValidationsLibrary						= new CGiactValidationsLibrary();
					$arrmixAccountVerificationTypeDataOfDefaultMA	= $objGiactValidationsLibrary->fetchAccountVerificationTypeDataOfDefaultMA( $this->m_objCustomerPaymentAccount->getPropertyId(), $this->m_objCustomerPaymentAccount->getCid(), NULL, $objClientDatabase );
					if( 1 == $arrmixAccountVerificationTypeDataOfDefaultMA['account_verification_type'] ) {
						$intAccountVerificationFrequency = $objGiactValidationsLibrary->fetchAccountVerificationFrequencyOfProperty( $this->m_objCustomerPaymentAccount->getPropertyId(), CPsProduct::RESIDENT_PORTAL, $this->m_objCustomerPaymentAccount->getCid(), $objClientDatabase );
						if( CProperty::ACCOUNT_VERIFICATION_TYPE_STORED_BILLING == $intAccountVerificationFrequency ) {

							$boolIsValid &= $this->valGiactAccountVerificationAttempts( $objPaymentDatabase );
							if( true == $boolIsValid ) {
								$arrmixResponse = ( array ) $objGiactValidationsLibrary->fetchGiactResponseAndBlacklistAccount( $this->m_objCustomerPaymentAccount->getCid(), $this->m_objCustomerPaymentAccount->getPropertyId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $arrmixAccountVerificationTypeDataOfDefaultMA['default_merchant_account_id'], $intAccountVerificationFrequency, $this->m_objCustomerPaymentAccount->getCheckAccountNumber(), $this->m_objCustomerPaymentAccount->getCheckAccountNumberEncrypted(), $this->m_objCustomerPaymentAccount->getCheckRoutingNumber(), $this->m_objCustomerPaymentAccount->getCheckAccountTypeId(), NULL, NULL, $objClientDatabase, $objPaymentDatabase );
								if( false == valArr( $arrmixResponse ) || false == $arrmixResponse['is_pass'] || false == valId( $arrmixResponse['giact_request_log_id'] ) ) {
									$boolIsValid &= false;
									$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The bank routing and account number entered could not be verified. Please check that the numbers are entered correctly or contact your bank for further assistance.' ) );
								} else {
									$this->m_objCustomerPaymentAccount->setAccountVerificationLogId( $arrmixResponse['giact_request_log_id'] );

								}
							}
						}
					}
				}

				if( NULL != $this->m_objCustomerPaymentAccount->getCvvCode() ) {
					$boolIsValid &= $this->valCvvCode();
				}
				break;

			case 'validate_customer_payment_account_for_rp2_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentAccountInfo( $objClientDatabase, $boolIsValidateBankName, true, $strClientCountryCode, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode );
				$boolIsValid &= $this->valBilltoCountryCode();
				$boolIsValid &= $this->valBilltoPhoneNumber();
				$boolIsValid &= $this->valBilltoStreetLine1();

				if( true == $boolIsValid && true == is_null( $this->m_objCustomerPaymentAccount->getPsProductOptionId() ) && CPaymentType::ACH == $this->m_objCustomerPaymentAccount->getPaymentTypeId() ) {
					$objMerchantAccount = CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->m_objCustomerPaymentAccount->getCid(), $this->m_objCustomerPaymentAccount->getPropertyId(), $objClientDatabase );
					if( true == valObj( $objMerchantAccount, 'CMerchantAccount' ) && true == ( bool ) $objMerchantAccount->getAccountVerificationType() ) {
						$arrstrPropertyPreferencesKeys = [ 'TURN_ON_FINANCIAL_PAYMENTS_ACCOUNT_VERIFICATION', 'FINANCIAL_PP_ACCOUNT_VERIFICATION_PAYMENT_SOURCE_ITEMS', 'VERIFY_RESIDENT_ACCOUNT_FREQUENCY' ];
						$boolIsVerifyEcheckAccount = $this->getIsVerifyEcheckAccount( $this->m_objCustomerPaymentAccount->getCid(), $this->m_objCustomerPaymentAccount->getPropertyId(), $arrstrPropertyPreferencesKeys, $objClientDatabase );
						if( true == $boolIsVerifyEcheckAccount ) {
							$objGiactValidationsLibrary = new CGiactValidationsLibrary();
							$boolIsPaymentMethodUsed = false;
							if( true == valId( $this->m_objCustomerPaymentAccount->getId() ) ) {
								$boolIsPaymentMethodUsed = \CPaymentController::checkIsPaymentMethodUsed( $this->m_objCustomerPaymentAccount->getId(), $this->m_objCustomerPaymentAccount->getCid(), $this->m_objCustomerPaymentAccount->getPropertyId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $objClientDatabase );
							}
							$arrmixResponse = ( array ) $objGiactValidationsLibrary->validateEcheckBankAccountWithGiact( $this->m_objCustomerPaymentAccount->getCid(), $this->m_objCustomerPaymentAccount->getPropertyId(), $this->m_objCustomerPaymentAccount->getCustomerId(), $objMerchantAccount->getId(), CProperty::ACCOUNT_VERIFICATION_TYPE_STORED_BILLING, $this->m_objCustomerPaymentAccount->getCheckAccountNumberEncrypted(), $this->m_objCustomerPaymentAccount->getCheckRoutingNumber(), $this->m_objCustomerPaymentAccount->getCheckAccountTypeId(), $objClientDatabase, CPsProduct::RESIDENT_PORTAL, $this->m_objCustomerPaymentAccount->getPsProductOptionId(), NULL, $boolIsPaymentMethodUsed );
							if( false == valArr( $arrmixResponse ) || false == $arrmixResponse['is_pass'] || false == valId( $arrmixResponse['giact_request_log_id'] ) ) {
								$boolIsValid &= false;
								$this->m_objCustomerPaymentAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The bank routing and account number entered could not be verified. Please check that the numbers are entered correctly or contact your bank for further assistance.' ) );
								$objGiactValidationsLibrary->addPaymentBlacklistEntry( $this->m_objCustomerPaymentAccount->getCheckAccountNumber(), $this->m_objCustomerPaymentAccount->getCheckRoutingNumber(), $arrmixResponse['account_response_code'], $objClientDatabase, $objPaymentDatabase, CUser::ID_RESIDENT_PORTAL );
							} else {
								$this->m_objCustomerPaymentAccount->setAccountVerificationLogId( $arrmixResponse['giact_request_log_id'] );

							}
						}
					}
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	protected function getIsVerifyEcheckAccount( $intCid, $intPropertyId, $arrstrPropertyPreferencesKeys, $objClientDatabase ) {
		$arrobjPropertyPreferences = rekeyObjects( 'Key', CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( $arrstrPropertyPreferencesKeys, $intPropertyId, $intCid, $objClientDatabase ) );

		if( true == isset( $arrobjPropertyPreferences['TURN_ON_FINANCIAL_PAYMENTS_ACCOUNT_VERIFICATION'] ) && true == $arrobjPropertyPreferences['TURN_ON_FINANCIAL_PAYMENTS_ACCOUNT_VERIFICATION']->getValue()
		    && true == isset( $arrobjPropertyPreferences['FINANCIAL_PP_ACCOUNT_VERIFICATION_PAYMENT_SOURCE_ITEMS'] )
		    && true == in_array( CPsProduct::RESIDENT_PORTAL, explode( ',', $arrobjPropertyPreferences['FINANCIAL_PP_ACCOUNT_VERIFICATION_PAYMENT_SOURCE_ITEMS']->getValue() ) )
		    && true == isset( $arrobjPropertyPreferences['VERIFY_RESIDENT_ACCOUNT_FREQUENCY'] ) && 1 == $arrobjPropertyPreferences['VERIFY_RESIDENT_ACCOUNT_FREQUENCY']->getValue() ) {
			return true;
		}

		return false;
	}

}
?>