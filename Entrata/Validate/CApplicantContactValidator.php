<?php
class CApplicantContactValidator {

	use TPostalAddressHelper;

	protected $m_objApplicantContact;
	const ADDRESS_POSTAL_CODE_FIELD     = 'postalCode';

	public function __construct() {
		return;
	}

	public function setApplicantContact( $objApplicantContact ) {
		$this->m_objApplicantContact = $objApplicantContact;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getApplicantId() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', __( 'Applicant id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerContactTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getCustomerContactTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_contact_type_id', __( 'Customer contact type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolIsRequired = false, $boolIsValidateRequired = false ) {
		$boolIsValid	= true;

		$boolIsHidden 	= false;

		if( 'applicant_emergency_contact' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_personal_reference_1' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_personal_reference_2' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_landlord_reference_1' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_1', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_landlord_reference_2' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_2', $arrobjPropertyApplicationPreferences );
		}

		if( false == $boolIsHidden ) {
			if( true == is_null( $this->m_objApplicantContact->getNameFirst() ) && true == $boolIsRequired && true == $boolIsValidateRequired ) {
				$boolIsValid = false;
				$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_first', __( '{%s,0} first name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicantContact->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicantContact->getNameFirst(), 'Content-type' ) ) {
				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_first', __( '{%s,0} first name cannot contain @ signs or email headers.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			} else {
				if( 1 > strlen( $this->m_objApplicantContact->getNameFirst() ) && true == $boolIsRequired && true == $boolIsValidateRequired ) {
					$boolIsValid = false;
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_first', __( '{%s,0} first name must have at least one letter.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolIsRequired = false, $boolIsValidateRequired = false ) {
		$boolIsValid 	= true;

		$boolIsHidden 	= false;

		if( 'applicant_emergency_contact' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_personal_reference_1' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_personal_reference_2' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_landlord_reference_1' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_1', $arrobjPropertyApplicationPreferences );
		} elseif( 'applicant_landlord_reference_2' == $strCustomerContactType ) {
			$boolIsHidden = array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_2', $arrobjPropertyApplicationPreferences );
		}

		if( false == $boolIsHidden ) {
			if( true == is_null( $this->m_objApplicantContact->getNameLast() ) && true == $boolIsRequired && true == $boolIsValidateRequired ) {
				$boolIsValid = false;
				$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_last', __( '{%s,0} last name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicantContact->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicantContact->getNameLast(), 'Content-type' ) ) {
				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_last', __( '{%s,0} last name cannot contain @ signs or email headers.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			} else {
				if( 0 < strlen( $this->m_objApplicantContact->getNameLast() ) && 2 > strlen( $this->m_objApplicantContact->getNameLast() ) && true == $boolIsValidateRequired ) {
					$boolIsValid = false;
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_name_last', __( '{%s,0} last name must have at least 2 letters.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCompanyName( $strSectionName, $strCustomerContactType ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_company_name', __( '{%s,0} company name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolIsRequired, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences = NULL, $boolValidateRequired = false ) {

		$boolIsValid	= true;

		$objPhoneNumber = $this->createPhoneNumber( $this->m_objApplicantContact->getPhoneNumber() );

		if( 'applicant_emergency_contact' == $strCustomerContactType && true == array_key_exists( 'RENAME_EMERGENCY_CONTACT_PHONE', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_EMERGENCY_CONTACT_PHONE']->getValue() ) {
			$strFieldName = ' ' . $arrobjPropertyApplicationPreferences['RENAME_EMERGENCY_CONTACT_PHONE']->getValue();
		} else {
			$strFieldName = ' phone number';
		}

		if( true == $boolIsRequired ) {

			if( false == valStr( $objPhoneNumber->getNumber() ) ) {
				if( true == $boolValidateRequired ) {
					$boolIsValid = false;
				}
				$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_phone_number', __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			}
		}

		if( true == valStr( $objPhoneNumber->getNumber() ) && false == $objPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_phone_number', __( '{%s,section_name} {%s,field_name} is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'section_name' => $strSectionName, 'field_name' => $strFieldName, 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valMobileNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getMobileNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Mobile number is required.' ), '' ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber( $boolCanBeNull = false, $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		$intLength = strlen( $this->m_objApplicantContact->getFaxNumber() );

		if( true == $boolCanBeNull ) {
			if( true == is_null( $this->m_objApplicantContact->getFaxNumber() ) || 0 == $intLength ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				} else {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number is required.', [ $this->m_objApplicantContact->getDefaultSectionName() ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				}
			} elseif( 7 > $intLength || 15 < $intLength ) {
				$boolIsValid = false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number must be between 7 and 15 characters.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				} else {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number must be between 7 and 15 characters.', [ $this->m_objApplicantContact->getDefaultSectionName() ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				}
			}

		} else {

			if( 7 > $intLength || 15 < $intLength ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number must be between 7 and 15 characters.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				} else {
					$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_fax_number', __( '{%s,0} fax number must be between 7 and 15 characters.', [ $this->m_objApplicantContact->getDefaultSectionName() ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolCanBeNull = false, $strSectionName, $strCustomerContactType, $boolValidateRequired = false ) {

		$boolIsValid = true;

		$intLength 		= strlen( trim( $this->m_objApplicantContact->getEmailAddress() ) );

		if( 0 < $intLength && false == CValidation::validateEmailAddresses( $this->m_objApplicantContact->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_email_address', __( '{%s,0} email address does not appear to be valid.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			return $boolIsValid;
		}

		if( false == $boolCanBeNull && ( true == is_null( $this->m_objApplicantContact->getEmailAddress() ) || 0 == $intLength ) && true == $boolValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_email_address', __( '{%s,0} email address is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRelationship( $strSectionName, $strCustomerContactType, $boolValidateRequired = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getRelationship() ) && true == $boolValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_relationship', __( '{%s,0} relationship is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_street_line1', __( '{%s,0} street line 1 is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine2( $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getStreetLine2() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_street_line2', __( '{%s,0} street line 2 is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valCity( $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getCity() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valStateCode( $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getStateCode() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valProvince( $strSectionName ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getProvince() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', __( '{%s,0} state/province is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $strSectionName, $strCustomerContactType, $boolValidateRequired = false, $arrobjPropertyApplicationPreferences = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getPostalCode() ) && true == $boolValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );

		} elseif( true == $this->checkIsContactAddressVisible( $strCustomerContactType, $arrobjPropertyApplicationPreferences ) && false == CApplicationUtils::validateZipCode( $this->m_objApplicantContact->getPostalCode(), NULL ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_postal_code',  __( '{%s,0} postal code must be 5 to 10 characters.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function checkIsContactAddressVisible( $strCustomerContactType, $arrobjPropertyApplicationPreferences ) {

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) return true;

		switch( $strCustomerContactType ) {
			case 'applicant_emergency_contact':
				$boolIsVisible = ( array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ? false : true;
				break;

			case 'applicant_personal_reference_1':
				$boolIsVisible = ( array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ? false : true;
				break;

			case 'applicant_personal_reference_2':
				$boolIsVisible = ( array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ? false : true;
				break;

			default:
				$boolIsVisible = true;
				break;
		}

		return $boolIsVisible;
	}

	public function valCountryCode( $strSectionName ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getCountryCode() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', __( '{%s,0} country code is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valAge( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences = NULL, $boolIsValidateRequired = false ) {
		$boolIsValid = true;

		if( 'applicant_emergency_contact' == $strCustomerContactType && true == array_key_exists( 'RENAME_EMERGENCY_CONTACT_AGE', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_EMERGENCY_CONTACT_AGE']->getValue() ) {
			$strFieldName = ' ' . $arrobjPropertyApplicationPreferences['RENAME_EMERGENCY_CONTACT_AGE']->getValue();
		} else {
			$strFieldName = ' age';
		}

		if( ( true == is_null( $this->m_objApplicantContact->getAge() ) || 0 >= $this->m_objApplicantContact->getAge() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_age', __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valHasAccessToUnit( $strSectionName, $strCustomerContactType, $boolIsValidateRequired = false ) {
		$boolIsValid = true;

		$strFieldName = ' Has Access To Unit';

		if( true == is_null( $this->m_objApplicantContact->getHasAccessToUnit() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_has_access_to_unit', __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valNotes( $strSectionName, $strCustomerContactType ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicantContact->getNotes() ) ) {
			$boolIsValid = false;
			$this->m_objApplicantContact->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strCustomerContactType . '_notes', __( '{%s,0} notes is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ] ) );
		}

		return $boolIsValid;
	}

	public function valRequiredApplicantContactFields( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired ) {
		$boolIsValid = true;

		if( 'applicant_emergency_contact' == $strCustomerContactType && false == array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT', $arrobjPropertyApplicationPreferences ) ) {
			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_FIRST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_LAST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_COMPANY_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valCompanyName( $strSectionName, $strCustomerContactType );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valRelationship( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_AGE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valAge( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPhoneNumber( true, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valPhoneNumber( false, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valApplicantContactAddress( $strSectionName, 'application_emergency_contact[postal_addresses][default]', $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} else {
				if( false == array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valEmailAddress( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
				}
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_HAS_ACCESS_TO_UNIT', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valHasAccessToUnit( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_EMERGENCY_CONTACT_NOTES', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNotes( $strSectionName, $strCustomerContactType );
			}
		}

		if( 'applicant_personal_reference_1' == $strCustomerContactType && false == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1', $arrobjPropertyApplicationPreferences ) ) {
			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_FIRST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_LAST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valRelationship( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_AGE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valAge( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPhoneNumber( true, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valPhoneNumber( false, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valApplicantContactAddress( $strSectionName, 'application_personal_reference_1[postal_addresses][default]', $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_1_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}
		}

		if( 'applicant_personal_reference_2' == $strCustomerContactType && false == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2', $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_FIRST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameFirst( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_LAST_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, true, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valNameLast( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, false, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valRelationship( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_AGE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valAge( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPhoneNumber( true, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valPhoneNumber( false, $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valApplicantContactAddress( $strSectionName, 'application_personal_reference_2[postal_addresses][default]', $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_PERSONAL_REFERENCE_2_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}
		}

		if( 'applicant_landlord_reference_1' == $strCustomerContactType ) {
			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_1_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPhoneNumber( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_1_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantContact->getPhoneNumber() ) ) {
				$boolIsValid &= $this->valPhoneNumber( false, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_1_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valFaxNumber( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_1_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantContact->getFaxNumber() ) ) {
				$boolIsValid &= $this->valFaxNumber( false, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_1_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valEmailAddress( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_1_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valApplicantContactAddress( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}
		}

		if( 'applicant_landlord_reference_2' == $strCustomerContactType ) {
			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_2_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPhoneNumber( '', $strSectionName, $strCustomerContactType, NULL, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_2_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantContact->getPhoneNumber() ) ) {
				$boolIsValid &= $this->valPhoneNumber( false, $strSectionName, $strCustomerContactType, NULL, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_2_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valFaxNumber( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} elseif( false == array_key_exists( 'HIDE_CONTACTS_LANDLORD_REFERENCE_2_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objApplicantContact->getFaxNumber() ) ) {
				$boolIsValid &= $this->valFaxNumber( false, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_2_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valEmailAddress( '', $strSectionName, $strCustomerContactType, $boolValidateRequired );
			} else {
				$boolIsValid &= $this->valEmailAddress( true, $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}

			if( true == array_key_exists( 'REQUIRE_CONTACTS_LANDLORD_REFERENCE_2_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valApplicantContactAddress( $strSectionName, $strCustomerContactType, $boolValidateRequired );
			}
		}

		return $boolIsValid;
	}

	public function valApplicantContactAddress( $strSectionName, $strCustomerContactType, $boolValidateRequired ) {
		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$boolIsValidTmp = true;

		if( false == $objPostalAddressService->isValid( $this->m_objApplicantContact->getPostalAddress() ) && true == $boolValidateRequired ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();

			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() )
				    || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->m_objApplicantContact->getPostalAddress()->getPostalCode() ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData      = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::CONTACTS ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}
			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, $this->m_objApplicantContact, $strCustomerContactType );
				$boolIsValidTmp = false;
			}
		}

		return  $boolIsValidTmp;
	}

	public function validate( $strAction, $strCustomerContactType, $strSectionName, $arrobjPropertyApplicationPreferences, $boolValidateRequired = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case 'validate_application_contacts':
				$boolIsValid &= $this->valRequiredApplicantContactFields( $strSectionName, $strCustomerContactType, $arrobjPropertyApplicationPreferences, $boolValidateRequired );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default break
				break;
		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}
?>