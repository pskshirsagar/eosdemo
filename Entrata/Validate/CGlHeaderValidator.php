<?php
class CGlHeaderValidator {

	protected $m_objGlHeader;
	protected $m_arrobjGlDetails;

	public function __construct() {
		return;
	}

	public function getGlDetails() {
		return $this->m_arrobjGlDetails;
	}

	public function setGlDetails( $objGlDetails ) {
		$this->m_arrobjGlDetails = $objGlDetails;
	}

	public function setGlHeader( $objGlHeader ) {
		$this->m_objGlHeader = $objGlHeader;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlHeader->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUserAssignedProperties( $boolShowDisabledData = false, $boolIsDeleteJe = false ) {
		$boolIsValid = true;

		if( $boolShowDisabledData ) {
			return $boolIsValid;
		}

		if( true == valArr( $this->m_objGlHeader->getCompanyUserPropertyIds() ) && true == valArr( $this->m_objGlHeader->getGlDetails() ) ) {

			$arrintPropertyIds	= ( array ) array_filter( array_keys( rekeyObjects( 'PropertyId', $this->m_objGlHeader->getGlDetails() ) ) );
			$arrobjProperties	= ( array ) $this->m_objGlHeader->getProperties();

			$arrstrPropertyNames         = [];
			$arrstrDisabledPropertyNames = [];

			foreach( $arrintPropertyIds as $intPropertyId ) {

				if( array_key_exists( $intPropertyId, $arrobjProperties ) && $arrobjProperties[$intPropertyId]->getIsDisabled() ) {
					$arrstrDisabledPropertyNames[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();

				} elseif( false == in_array( $intPropertyId, $this->m_objGlHeader->getCompanyUserPropertyIds() ) ) {
					$arrstrPropertyNames[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();
				}
			}

			if( $boolIsDeleteJe && valArr( $arrstrDisabledPropertyNames ) ) {
				$strModuleName = $this->m_objGlHeader->getIsTemplate() ? __( 'journal entry template' ) : __( 'journal entry' );
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Unable to delete this {%s, 0} because Property [{%s, 1}] is disabled.', [ $strModuleName, implode( ', ', $arrstrDisabledPropertyNames ) ] ) ) );
				return false;
			}

			if( true == valArr( $arrstrPropertyNames ) ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property(s) \'{%s, 0}\' is/are not assigned to this user.', [ implode( ', ', $arrstrPropertyNames ) ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valGlHeaderTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlHeader->getGlHeaderTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_header_type_id', __( 'Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlTransactionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlHeader->getGlTransactionTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_transaction_type_id', __( 'Transaction type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTransactionDatetime() {
		$boolIsValid = true;

		if( true == $this->m_objGlHeader->getIsTemplate() ) {
			return true;
		}

		if( true == is_null( $this->m_objGlHeader->getTransactionDatetime() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', __( 'Transaction date is required.' ) ) );
			return $boolIsValid;
		}

		$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objGlHeader->getTransactionDatetime(), 0, 2 );
		$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objGlHeader->getTransactionDatetime(), 3, 2 );
		$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $this->m_objGlHeader->getTransactionDatetime(), 6, 4 );

		if( true == $boolIsValid && false == checkdate( $intMonth, $intDay, $intYear ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', __( 'Transaction date is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlHeaderPostMonth( $strPostMonth, $boolIsValid, $boolIsReversed ) {

		if( false === valStr( $strPostMonth ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', ( true == $boolIsReversed ) ? __( 'Reverse Post month is required.' ) : __( 'Post month is required.' ) ) );
			return $boolIsValid;
		}

		if( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $strPostMonth ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', ( true == $boolIsReversed ) ? __( 'Reverse Post month of format mm/yyyy is required.' ) : __( 'Post month of format mm/yyyy is required.' ) ) );
			return $boolIsValid;
		}

		$objPostMonth		= new DateTime( $strPostMonth );
		$objBeginPostMonth	= new DateTime( '01/01/1970' );
		$objEndPostMonth	= new DateTime( '12/01/2099' );

		if( false == CValidation::validateDate( $strPostMonth ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is invalid.' ) ) );
			return $boolIsValid;
		}

		if( true == $boolIsReversed && strtotime( $strPostMonth ) < strtotime( $this->m_objGlHeader->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Reversal post month should not be less than the original journal entry\'s post month ( {%t, 0, DATE_NUMERIC_POSTMONTH} ).', [ strtotime( $this->m_objGlHeader->getPostMonth() ) ] ) ) );
			return $boolIsValid;
		}

		if( ( $objPostMonth->format( 'Y-m-d' ) < $objBeginPostMonth->format( 'Y-m-d' ) ) || ( $objPostMonth->format( 'Y-m-d' ) > $objEndPostMonth->format( 'Y-m-d' ) ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( ' Entrata does not support transactions outside the date year range of 1969 to 2099.' ) ) );
			return $boolIsValid;
		}

		if( false == valArr( $this->m_objGlHeader->getPropertyGlSettings() ) || false == valArr( $this->m_objGlHeader->getProperties() ) ) {
			$boolIsValid &= false;

			// show validation if line item property is selected
			if( true == valArr( $this->m_objGlHeader->getGlDetails() ) && true == valIntArr( array_keys( $this->m_objGlHeader->getGlDetails() ) ) ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property setting not found for selected property.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPostMonth( $objCompanyUser = NULL, $boolIsReversed = false, $boolIsPropertyGlLocked = false ) {
		$boolIsValid						= true;
		$strReversePostMonth				= NULL;
		$arrstrGlLockDateProperties			= [];
		$arrstrOutOfRangeProperties			= [];
		$arrstrReverseOutOfRangeProperties	= [];

		$strPostMonth = $this->m_objGlHeader->getPostMonth();

		$boolIsValid = $this->valGlHeaderPostMonth( $strPostMonth, $boolIsValid, false );

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == $this->m_objGlHeader->getIsTemplate() ) {
			return true;
		}

		if( true == $boolIsValid && true == $boolIsReversed ) {
			$strReversePostMonth	= $this->m_objGlHeader->getReversePostMonth();
			$boolIsValid			= $this->valGlHeaderPostMonth( $strReversePostMonth, $boolIsValid, true );
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$arrobjPropertyGlSettings	= rekeyObjects( 'PropertyId', array_filter( $this->m_objGlHeader->getPropertyGlSettings() ) );
		$arrobjGlDetails			= rekeyObjects( 'PropertyId', $this->m_objGlHeader->getGlDetails() );

		foreach( $arrobjGlDetails as $intPropertyId => $objGlDetail ) {

			$objPropertyGlSetting = ( true == array_key_exists( $intPropertyId, $arrobjPropertyGlSettings ) )? $arrobjPropertyGlSettings[$intPropertyId] : NULL;

			// If no property gl setting available or property is in migration mode and intial import is on then skip iteration.
			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
				|| ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
				&& 1 == $objPropertyGlSetting->getIsApMigrationMode() ) ) {
				continue;
			}

			// Validate for GL lock period
			if( true == array_key_exists( $intPropertyId, $this->m_objGlHeader->getProperties() )
				&& ( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() )
				|| ( true === valStr( $strReversePostMonth ) && strtotime( $strReversePostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) ) ) {

				$arrobjProperties = $this->m_objGlHeader->getProperties();
				$arrstrGlLockDateProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			// If logged in user is administrator then no need to check post month's range permission
			if( false == valObj( $objCompanyUser, 'CCompanyUser' ) || ( true == valObj( $objCompanyUser, 'CCompanyUser' ) && 1 == $objCompanyUser->getIsAdministrator() ) ) {
				continue;
			}

			$intPastPostMonth	= strtotime( $objPropertyGlSetting->getGlPostMonth() );
			$intFuturePostMonth	= strtotime( $objPropertyGlSetting->getGlPostMonth() );

			foreach( $this->m_objGlHeader->getCompanyUserPreferences() as $objCompanyUserPreference ) {

				if( false == is_null( $objCompanyUserPreference->getValue() ) && 0 < $objCompanyUserPreference->getValue() ) {

					// Subtract months to ap_post_month
					if( CCompanyUserPreference::GENERAL_JOURNAL_PAST_POST_MONTH == $objCompanyUserPreference->getKey() ) {
						$intPastPostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getGlPostMonth() ) ) . ' -' . $objCompanyUserPreference->getValue() . ' month' );
					}

					// Add months to ap_post_month
					if( CCompanyUserPreference::GENERAL_JOURNAL_FUTURE_POST_MONTH == $objCompanyUserPreference->getKey() ) {
						$intFuturePostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getGlPostMonth() ) ) . ' +' . $objCompanyUserPreference->getValue() . ' month' );
					}
				}
			}

			// Validate for range of default AP post month based on user permissions
			if( true == array_key_exists( $intPropertyId, $this->m_objGlHeader->getProperties() )
				&& ( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) ) {

				$arrobjProperties = $this->m_objGlHeader->getProperties();
				$arrstrOutOfRangeProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}
			// Validate for range of default reverse AP post month based on user permissions
			if( true == $boolIsReversed && true == array_key_exists( $intPropertyId, $this->m_objGlHeader->getProperties() )
				&& ( strtotime( $strReversePostMonth ) < $intPastPostMonth || strtotime( $strReversePostMonth ) > $intFuturePostMonth ) ) {

				$arrobjProperties = $this->m_objGlHeader->getProperties();
				$arrstrReverseOutOfRangeProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}
		}

		if( true == valArr( $arrstrOutOfRangeProperties ) && CGlHeaderStatusType::ROUTED != $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {
			$arrstrOutOfRangeProperties = array_unique( $arrstrOutOfRangeProperties );
			$boolIsValid &= false;

			$strErrorMessage = __( 'User does not have permission to post in {%t, 0, DATE_NUMERIC_POSTMONTH} for property(s) \'{%s, 1}\'.', [ $strPostMonth, implode( '\', \'', $arrstrOutOfRangeProperties ) ] );

			if( true == valId( $this->m_objGlHeader->getId() ) && true == valId( $this->m_objGlHeader->getOffsettingGlHeaderId() ) ) {
				if( $this->m_objGlHeader->getId() > $this->m_objGlHeader->getOffsettingGlHeaderId() )
				$strErrorMessage = __( 'User does not have permission to post in Reverse Post {%t, 0, DATE_NUMERIC_POSTMONTH} for property(s) \'{%s, 1}\'.', [ $strPostMonth, implode( '\', \'', $arrstrOutOfRangeProperties ) ] );
			}

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage ) );
		}

		if( true == $boolIsReversed && true == valArr( $arrstrReverseOutOfRangeProperties ) && CGlHeaderStatusType::ROUTED != $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {
			$arrstrReverseOutOfRangeProperties = array_unique( $arrstrReverseOutOfRangeProperties );
			$boolIsValid &= false;

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'User does not have permission to post in Reverse Post {%t, 0, DATE_NUMERIC_POSTMONTH} for property(s) \'{%s, 1}\'.', [ $strReversePostMonth, implode( '\', \'', $arrstrReverseOutOfRangeProperties ) ] ) ) );
		}

		if( true == valArr( $arrstrGlLockDateProperties ) && false == $boolIsPropertyGlLocked ) {
			$arrstrGlLockDateProperties = array_unique( $arrstrGlLockDateProperties );
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Post month is in GL locked period for property(s) \'{%s, 0}\'.', [ implode( '\', \'', $arrstrGlLockDateProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valGlBookId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlHeader->getGlBookId() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_book_id', __( 'GL Book is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlHeader->getMemo() ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', __( 'Memo is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostDate( $boolIsReversed = false ) {

		$boolIsValid = true;

		if( false == $boolIsReversed ) {
			$strPostDate = $this->m_objGlHeader->getPostDate();
		} else {
			$strPostDate = $this->m_objGlHeader->getReversePostDate();
		}

		if( false == valStr( $strPostDate ) ) {
			$boolIsValid &= false;
			if( true == $boolIsReversed ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Reversal date is required.' ) ) );
			} else {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Transaction date is required.' ) ) );
			}
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $strPostDate ) ) {
			$boolIsValid &= false;
			$strMessage    = ( false == $boolIsReversed ) ? __( 'Transaction date is invalid.' ) : __( 'Reversal date is invalid.' );
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
			return $boolIsValid;
		}

		$strPostDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $strPostDate ] );

		if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $strPostDate ) ) {
			$boolIsValid &= false;
			if( true == $boolIsReversed ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Reversal date is invalid.' ) ) );
			} else {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Transaction date is invalid.' ) ) );
			}
			return $boolIsValid;
		}

		if( true == $boolIsReversed ) {
			if( strtotime( date( 'm/d/y', strtotime( $strPostDate ) ) ) < strtotime( date( 'm/d/y', strtotime( $this->m_objGlHeader->getPostDate() ) ) ) ) {
				$boolIsValid &= false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Reversal date should not be less than the original transaction date ({%t, 0, DATE_NUMERIC_STANDARD}).', [ strtotime( $this->m_objGlHeader->getPostDate() ) ] ) ) );
				return $boolIsValid;
			}
		}

		if( true == $boolIsValid && false == CValidation::checkDateFormat( $strPostDate, true ) ) {
			$boolIsValid &= false;
			if( true == $boolIsReversed ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Reversal date is invalid.' ) ) );
			} else {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Transaction date is invalid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsJeDeleted() {

		$boolIsValid = true;

		if( CGlHeaderStatusType::DELETED == $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {

			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_header_status_type_id', __( 'JE \'{%d, 0}\' is already deleted.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsJeReversed() {

		$boolIsValid = true;

		if( CGlHeaderStatusType::REVERSED == $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {

			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_header_status_type_id', __( 'JE \'{%d, 0}\' is already reversed.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCurrentPostMonth() {

		$boolIsValid = true;

		if( true == $this->m_objGlHeader->getIsTemplate() ) {
			return true;
		}

		$arrobjRekeyedPropertySettingsByGlLockMonth	= array_keys( rekeyObjects( 'GlLockMonth', $this->m_objGlHeader->getPropertyGlSettings() ) );
		$strPropertyLockMonth						= max( $arrobjRekeyedPropertySettingsByGlLockMonth );

		if( strtotime( $this->m_objGlHeader->getPostMonth() ) <= strtotime( $strPropertyLockMonth ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'JE \'{%d, 0}\' cannot be deleted because it is in a closed period.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsExported( $boolEditExportedJePermission = false, $boolIsFromDeleteJe = false ) {

		if( true == $boolEditExportedJePermission ) {
			return true;
		}

		$boolIsValid = true;

		$strMessage = __( 'You do not have permission to edit exported journal entry.' );

		if( true == $boolIsFromDeleteJe ) {
			$strMessage = __( 'You do not have permission to delete exported journal entry.' );
		}

		foreach( ( array ) $this->m_objGlHeader->getGlDetails() as $objGlDetail ) {
			if( false == is_null( $objGlDetail->getAccountingExportBatchId() ) ) {
				$boolIsValid &= false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'accounting_export_batch_id', __( 'JE \'{%d, 0}\'  {%s, 1}', [ $this->m_objGlHeader->getHeaderNumber(), $strMessage ] ) ) );
				break;
			}
		}

		return $boolIsValid;
	}

	public function valIsReconciled() {

		$boolIsValid = true;

		foreach( ( array ) $this->m_objGlHeader->getGlDetails() as $objGlDetail ) {
			if( false == is_null( $objGlDetail->getGlReconciliationId() ) ) {
				$boolIsValid &= false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_reconciliation_id', __( 'JE \'{%d, 0}\' cannot be deleted, the JE has been reconciled.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
				break;
			}
		}

		return $boolIsValid;
	}

	public function valGlDetailCount() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objGlHeader->getGlDetailsCount() ) ) {
			$this->m_objGlHeader->setGlDetailsCount( \Psi\Libraries\UtilFunctions\count( $this->m_objGlHeader->getGlDetails() ) );
		}
		if( CGlHeader::ACCOUNT_BALANCE_METHOD == $this->m_objGlHeader->getAmountMethodTypeId() ) {
			if( 1 > $this->m_objGlHeader->getGlDetailsCount() ) {
				$boolIsValid = false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least one transaction detail is required.' ) ) );
			}
		} elseif( 2 > $this->m_objGlHeader->getGlDetailsCount() ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least two transaction details are required (debit & credit).' ) ) );
		}
		return $boolIsValid;
	}

	public function valPauseJePostMonth() {

		$boolIsValid		= true;
		$objPostMonth		= new DateTime( $this->m_objGlHeader->getPostMonth() );
		$objBeginPostMonth	= new DateTime( '01/01/1970' );
		$objEndPostMonth	= new DateTime( '12/01/2099' );

		if( true == is_null( $this->m_objGlHeader->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
			return $boolIsValid;
		}

		if( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_objGlHeader->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->m_objGlHeader->getPostMonth() ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is invalid.' ) ) );
			return $boolIsValid;
		}

		if( ( $objPostMonth->format( 'Y-m-d' ) < $objBeginPostMonth->format( 'Y-m-d' ) ) || ( $objPostMonth->format( 'Y-m-d' ) > $objEndPostMonth->format( 'Y-m-d' ) ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( ' Entrata does not support transactions outside the date year range of 1969 to 2099.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPostedJe() {

		if( 1 == $this->m_objGlHeader->getIsPaused() && CGlHeaderStatusType::POSTED == $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_je', __( 'JE "{%d, 0}" is already posted.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
			return false;
		}

		return true;
	}

	public function valDeletedJe() {

		if( CGlHeaderStatusType::DELETED == $this->m_objGlHeader->getGlHeaderStatusTypeId() ) {

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_je', __( 'JE \'{%d, 0}\' cannot be edited , the JE has been deleted.', [ $this->m_objGlHeader->getHeaderNumber() ] ) ) );
			return false;
		}

		return true;
	}

	public function valGlDetails() {

		$boolIsValid							= true;
		$boolIsValidAmount					    = true;
		$boolIsFalseTransaction					= false;

		$arrintPropertiesTransactionCount		= [];

		$arrstrOneTransactionProperties			= [];
		$arrstrCreditDebitMismatchProperties	= [];
		$arrstrZeroAmountProperties				= [];
		$arrstrSameToFromGlAccountProperties	= [];
		$arrstrMigrationModeDisabledProperties	= [];

		$arrobjProperties						= $this->m_objGlHeader->getProperties();
		$arrobjMigrationModeEnabledProperties	= $this->m_objGlHeader->getMigrationModeEnabledProperties();

		$arrmixJournalEntry						= [];
		$arrmixGlDetails						= $this->m_objGlHeader->getPostedGlDetails();

		foreach( $arrmixGlDetails as $arrmixGlDetail ) {
			if( !empty( $arrmixGlDetail['gl_account_id'] ) ) {

				$fltDebitAmount		= ( float ) trim( str_replace( ',', '', $arrmixGlDetail['debit'] ) );
				$fltCreditAmount	= ( float ) trim( str_replace( ',', '', $arrmixGlDetail['credit'] ) );

				if( 0.00 != $fltDebitAmount && 0.00 != $fltCreditAmount ) {
					$boolIsFalseTransaction = true;
				}

				if( ( ( ( 'recurring' == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
					  || 'manual' == $this->m_objGlHeader->getTemplateType() )
					|| false == $this->m_objGlHeader->getIsTemplate() )
				  && 0 == $fltDebitAmount
				  && 0 == $fltCreditAmount ) {
					$boolIsValidAmount = false;
				}

				if( true == isset( $arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['debit'] ) ) {
					$arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['debit'] += $fltDebitAmount;
				} else {
					$arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['debit'] = $fltDebitAmount;
				}

				if( true == isset( $arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['credit'] ) ) {
					$arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['credit'] += $fltCreditAmount;
				} else {
					$arrmixJournalEntry[$arrmixGlDetail['property_id']][$arrmixGlDetail['gl_account_id'] . '_' . $arrmixGlDetail['is_parent_property']]['credit'] = $fltCreditAmount;
				}

				if( true == is_numeric( $arrmixGlDetail['property_id'] ) && false == array_key_exists( $arrmixGlDetail['property_id'], $arrintPropertiesTransactionCount ) ) {
					$arrintPropertiesTransactionCount[$arrmixGlDetail['property_id']] = 1;
				} else {
					if( true == isset( $arrintPropertiesTransactionCount[$arrmixGlDetail['property_id']] ) ) {
						$arrintPropertiesTransactionCount[$arrmixGlDetail['property_id']]++;
					}
				}

				if( true == valId( $arrmixGlDetail['maintenance_location_id'] ) && false == valId( $arrmixGlDetail['job_phase_id'] ) ) {
					$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'For  property location, please select job details.' ) ) );
					$boolIsValid &= false;
				}
			} elseif( !empty( $arrmixGlDetail['from_gl_account_id'] ) && $arrmixGlDetail['from_gl_account_id'] == $arrmixGlDetail['to_gl_account_id'] ) {
				$arrstrSameToFromGlAccountProperties[] = $arrobjProperties[$arrmixGlDetail['property_id']]->getPropertyName();
			}
		}

		foreach( $arrmixJournalEntry as $intPropertyId => $arrmixGlAccounts ) {

			$fltPropertyWiseTotalDebitAmount	= 0.00;
			$fltPropertyWiseTotalCreditAmount	= 0.00;

			if( true == array_key_exists( $intPropertyId, $arrintPropertiesTransactionCount ) && ( 2 > $arrintPropertiesTransactionCount[$intPropertyId] )
				&& true == array_key_exists( $intPropertyId, $arrobjProperties ) ) {

				$arrstrOneTransactionProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			if( false == $this->m_objGlHeader->getIsTemplate() && true == is_numeric( $intPropertyId ) && false == array_key_exists( $intPropertyId, $arrobjMigrationModeEnabledProperties )
				&& true == array_key_exists( $intPropertyId, $arrobjProperties ) ) {

				$arrstrMigrationModeDisabledProperties[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			foreach( $arrmixGlAccounts as $arrmixAmount ) {

				$fltPropertyWiseTotalDebitAmount    = ( float ) round( bcadd( $fltPropertyWiseTotalDebitAmount, round( $arrmixAmount['debit'], 2 ), 2 ), 2 );
				$fltPropertyWiseTotalCreditAmount   = ( float ) round( bcadd( $fltPropertyWiseTotalCreditAmount, round( $arrmixAmount['credit'], 2 ), 2 ), 2 );
			}

			if( ( ( ( 'recurring' == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
				  || 'manual' == $this->m_objGlHeader->getTemplateType() )
				|| false == $this->m_objGlHeader->getIsTemplate() )
			   && ( true == array_key_exists( $intPropertyId, $arrobjProperties )
			   && number_format( $fltPropertyWiseTotalDebitAmount, 2, '.', '' ) != number_format( $fltPropertyWiseTotalCreditAmount, 2, '.', '' ) ) ) {
				$arrstrCreditDebitMismatchProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			if( ( ( ( 'recurring' == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
				  || 'manual' == $this->m_objGlHeader->getTemplateType() )
				|| false == $this->m_objGlHeader->getIsTemplate() )
			   && 0 == $fltPropertyWiseTotalDebitAmount
			   && 0 == $fltPropertyWiseTotalCreditAmount
			   && true == array_key_exists( $intPropertyId, $arrobjProperties ) ) {
				$arrstrZeroAmountProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}
		}

		if( true == $boolIsFalseTransaction && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Debit and credit on same line item is not allowed.' ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrCreditDebitMismatchProperties ) && ( CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() || 'manual' != $this->m_objGlHeader->getTemplateType() ) ) {

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Debit and credit totals should equal each other. Property(s) are out of balance \'{%s, 0}\'.', [ implode( ', ', $arrstrCreditDebitMismatchProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrOneTransactionProperties ) && ( CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() || 'manual' != $this->m_objGlHeader->getTemplateType() ) ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'At least two transaction details are required (debit & credit) for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrOneTransactionProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrMigrationModeDisabledProperties ) && CGlHeaderType::TRIAL_BALANCE == $this->m_objGlHeader->getGlHeaderTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Property(s) \'{%s, 0}\' are not configured to use trial balance.', [ implode( ', ', $arrstrMigrationModeDisabledProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrZeroAmountProperties ) && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Debit & credit amount is required for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrZeroAmountProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrSameToFromGlAccountProperties ) ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'From account & to account should not be same for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrSameToFromGlAccountProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( false == $boolIsValidAmount && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Amount can not be zero.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTemplateName( $strAction, $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valStr( $this->m_objGlHeader->getTemplateName() ) ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Template name cannot be blank.' ) ) );
			return false;
		}

		if( true == isset( $strAction ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->m_objGlHeader->getCid() . '
							AND is_template IS TRUE
							AND LOWER( template_name ) = LOWER( \'' . addslashes( $this->m_objGlHeader->getTemplateName() ) . '\' )
							AND gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED . '
							AND id <> ' . ( int ) $this->m_objGlHeader->getId();

			if( 0 < CGlHeaders::fetchGlHeaderCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid &= false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_name', __( 'Template name \'{%s, 0}\' already exists.', [ $this->m_objGlHeader->getTemplateName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valLineItemAmounts() {

		$boolIsValid			= true;
		$boolIsValidAmount		= true;
		$boolIsNonZeroAmount	= true;
		$arrintAmountByProperty	= [];

		foreach( $this->getGlDetails() as $objGlDetail ) {

			$arrintAmountByProperty[$objGlDetail->getPropertyId()]['amount'] = bcadd( $arrintAmountByProperty[$objGlDetail->getPropertyId()]['amount'], round( $objGlDetail->getAmount(), 2 ), 2 );

			if( 0 == $arrintAmountByProperty[$objGlDetail->getPropertyId()]['amount'] ) {
				unset( $arrintAmountByProperty[$objGlDetail->getPropertyId()] );
			}

			if( 0 == ( float ) abs( $objGlDetail->getAmount() ) ) {
				$boolIsNonZeroAmount = false;
			} elseif( 100000000000 <= ( float ) abs( $objGlDetail->getAmount() ) ) {
				$boolIsValidAmount = false;
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintAmountByProperty ) ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Few of selected Journal Entry(ies) having unbalanced amount.' ) ) );
		}

		if( !$boolIsNonZeroAmount ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'The selected Journal Entry line item amounts cannot equal 0.' ) ) );
		}

		if( !$boolIsValidAmount ) {
			$boolIsValid &= false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'The selected Journal Entry line item amounts must be between -$99,999,999,999.99 and $99,999,999,999.99' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlPosting( $boolJeAllowPostingToRestrictedGlAccount, $boolIsImportJe = false ) {

		$boolIsValid	= true;

		$arrstrInactiveGlPostingProperties 	= [];

		$arrobjPropertyGlSettings	= ( array ) $this->m_objGlHeader->getPropertyGlSettings();
		$arrobjGlAccounts			= ( array ) $this->m_objGlHeader->getGlAccounts();
		$arrobjGlDetails			= ( array ) $this->m_objGlHeader->getGlDetails();

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {

			// If property is in migration mode and intial import is on then skip iteration.
			if( true == $objPropertyGlSetting->getIsApMigrationMode() ) {
				continue;
			}

			if( false == $objPropertyGlSetting->getActivateStandardPosting() ) {

				$boolIsValid &= false;

				if( true == array_key_exists( $objPropertyGlSetting->getPropertyId(), $this->m_objGlHeader->getProperties() ) ) {
					$arrstrInactiveGlPostingProperties[] = getArrayElementByKey( $objPropertyGlSetting->getPropertyId(), $this->m_objGlHeader->getProperties() )->getPropertyName();
				}
			}
		}

		if( $boolIsImportJe ) {
			if( valArr( $arrstrInactiveGlPostingProperties ) ) {
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_standard_posting', __( 'The property(s) \'{%s, 0}\' are not entrata enabled. Journal Entry {%s, 1} cannot be imported.', [ implode( ', ', array_unique( $arrstrInactiveGlPostingProperties ) ), $this->m_objGlHeader->getReference() ] ) ) );
			}
			return $boolIsValid;
		}

		foreach( $arrobjGlDetails as $objGlDetail ) {
			if( true == array_key_exists( $objGlDetail->getAccrualGlAccountId(), $arrobjGlAccounts )
			    && true == $arrobjGlAccounts[$objGlDetail->getAccrualGlAccountId()]->getRestrictForGl()
			    && false == $boolJeAllowPostingToRestrictedGlAccount ) {
				$boolIsValid &= false;
				$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account {%s, 0} : {%s, 1} is restricted and you do not have permission to use restricted GL accounts.', [ $arrobjGlAccounts[$objGlDetail->getAccrualGlAccountId()]->getAccountNumber(), $arrobjGlAccounts[$objGlDetail->getAccrualGlAccountId()]->getAccountName() ] ) ) );
			}
		}

		if( true == valArr( $arrstrInactiveGlPostingProperties ) ) {
			$arrstrInactiveGlPostingProperties = array_unique( $arrstrInactiveGlPostingProperties );
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_standard_posting', __( 'GL posting is not enabled for property(s) \'{%s, 0}\'.', [ implode( ',', $arrstrInactiveGlPostingProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valJeLineItems( $boolIsImportJe = false ) :bool {

		$boolIsValid							= true;
		$boolIsValidAmount					    = true;
		$boolIsFalseTransaction					= false;

		$arrintPropertiesTransactionCount		= [];

		$arrstrZeroAmountProperties				= [];
		$arrstrOneTransactionProperties			= [];
		$arrstrCreditDebitMismatchProperties	= [];
		$arrstrSameToFromGlAccountProperties	= [];
		$arrstrMigrationModeDisabledProperties	= [];

		$arrobjProperties						= $this->m_objGlHeader->getProperties();
		$arrobjMigrationModeEnabledProperties	= $this->m_objGlHeader->getMigrationModeEnabledProperties();
		$arrobjGlDetails						= ( array ) $this->m_objGlHeader->getPostedGlDetails();

		$arrmixJournalEntry						= [];

		foreach( $arrobjGlDetails as $objGlDetail ) {

			if( !empty( $objGlDetail->getAccrualGlAccountId() ) || !empty( $objGlDetail->getCashGlAccountId() ) ) {

				$fltDebitAmount		= ( float ) trim( str_replace( ',', '', $objGlDetail->getDebitAmount() ) );
				$fltCreditAmount	= ( float ) trim( str_replace( ',', '', $objGlDetail->getCreditAmount() ) );

				if( 0.00 != $fltDebitAmount && 0.00 != $fltCreditAmount ) {
					$boolIsFalseTransaction = true;
				}

				if( 0 == $fltDebitAmount && 0 == $fltCreditAmount
				    && ( ( ( CGlHeader::TEMPLATE_TYPE_RECURRING == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
				           || CGlHeader::TEMPLATE_TYPE_MANUAL == $this->m_objGlHeader->getTemplateType() ) || false == $this->m_objGlHeader->getIsTemplate() ) ) {
					$boolIsValidAmount = false;
				}

				$intGlAccountId = $objGlDetail->getAccrualGlAccountId() ?? $objGlDetail->getCashGlAccountId();

				if( true == isset( $arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['debit'] ) ) {
					$arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['debit'] += $fltDebitAmount;
				} else {
					$arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['debit'] = $fltDebitAmount;
				}

				if( true == isset( $arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['credit'] ) ) {
					$arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['credit'] += $fltCreditAmount;
				} else {
					$arrmixJournalEntry[$objGlDetail->getPropertyId()][$intGlAccountId]['credit'] = $fltCreditAmount;
				}

				if( true == is_numeric( $objGlDetail->getPropertyId() ) && false == array_key_exists( $objGlDetail->getPropertyId(), $arrintPropertiesTransactionCount ) ) {
					$arrintPropertiesTransactionCount[$objGlDetail->getPropertyId()] = 1;
				} else if( true == isset( $arrintPropertiesTransactionCount[$objGlDetail->getPropertyId()] ) ) {
					$arrintPropertiesTransactionCount[$objGlDetail->getPropertyId()]++;
				}

				if( true == valId( $objGlDetail->getMaintenanceLocationId() ) && false == valId( $objGlDetail->getJobPhaseId() ) ) {
					$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'For  property location, please select job details.' ) ) );
					$boolIsValid &= false;
				}
			} elseif( !empty( $objGlDetail->getFromGlAccountId() )
					&& $objGlDetail->getFromGlAccountId() == $objGlDetail->getToGlAccountId()
			          && array_key_exists( $objGlDetail->getPropertyId(), $arrobjProperties ) ) {
				$arrstrSameToFromGlAccountProperties[] = $arrobjProperties[$objGlDetail->getPropertyId()]->getPropertyName();
			}
		}

		foreach( $arrmixJournalEntry as $intPropertyId => $arrmixGlAccounts ) {

			$fltPropertyWiseTotalDebitAmount	= 0.00;
			$fltPropertyWiseTotalCreditAmount	= 0.00;

			if( true == array_key_exists( $intPropertyId, $arrintPropertiesTransactionCount )
			    && true == array_key_exists( $intPropertyId, $arrobjProperties )
			    && ( 2 > $arrintPropertiesTransactionCount[$intPropertyId] )
			    && array_key_exists( $intPropertyId, $arrobjProperties ) ) {

				$arrstrOneTransactionProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			if( true == is_numeric( $intPropertyId )
			    && !array_key_exists( $intPropertyId, $arrobjMigrationModeEnabledProperties )
			    && true == array_key_exists( $intPropertyId, $arrobjProperties ) && !$this->m_objGlHeader->getIsTemplate()
			    && array_key_exists( $intPropertyId, $arrobjProperties ) ) {

				$arrstrMigrationModeDisabledProperties[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			foreach( $arrmixGlAccounts as $arrmixAmount ) {

				$fltPropertyWiseTotalDebitAmount    = ( float ) bcadd( $fltPropertyWiseTotalDebitAmount, round( $arrmixAmount['debit'], 2 ), 2 );
				$fltPropertyWiseTotalCreditAmount   = ( float ) bcadd( $fltPropertyWiseTotalCreditAmount, round( $arrmixAmount['credit'], 2 ), 2 );
			}

			if( ( true == array_key_exists( $intPropertyId, $arrobjProperties )
			      && number_format( $fltPropertyWiseTotalDebitAmount, 2, '.', '' ) != number_format( $fltPropertyWiseTotalCreditAmount, 2, '.', '' ) )
			    && ( ( ( CGlHeader::TEMPLATE_TYPE_RECURRING == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
			           || CGlHeader::TEMPLATE_TYPE_MANUAL == $this->m_objGlHeader->getTemplateType() )
			         || !$this->m_objGlHeader->getIsTemplate() ) ) {
				$arrstrCreditDebitMismatchProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}

			if( 0 == $fltPropertyWiseTotalDebitAmount
			    && 0 == $fltPropertyWiseTotalCreditAmount
			    && true == array_key_exists( $intPropertyId, $arrobjProperties )
			    && ( ( ( CGlHeader::TEMPLATE_TYPE_RECURRING == $this->m_objGlHeader->getTemplateType() && 1 != $this->m_objGlHeader->getIsScheduledJEPaused() )
			           || CGlHeader::TEMPLATE_TYPE_MANUAL == $this->m_objGlHeader->getTemplateType() )
			         || !$this->m_objGlHeader->getIsTemplate() )
			    && array_key_exists( $intPropertyId, $arrobjProperties ) ) {
				$arrstrZeroAmountProperties[] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}
		}

		if( true == $boolIsFalseTransaction && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Debit and credit on same line item is not allowed.' ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrCreditDebitMismatchProperties ) && ( CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() || 'manual' != $this->m_objGlHeader->getTemplateType() ) ) {

			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Debit and credit totals should equal each other. Property(s) are out of balance \'{%s, 0}\'.', [ implode( ', ', $arrstrCreditDebitMismatchProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrOneTransactionProperties ) && ( CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() || 'manual' != $this->m_objGlHeader->getTemplateType() ) ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'At least two transaction details are required (debit & credit) for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrOneTransactionProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrMigrationModeDisabledProperties ) && CGlHeaderType::TRIAL_BALANCE == $this->m_objGlHeader->getGlHeaderTypeId() ) {
			$strErrorMsg = __( 'Property(s) \'{%s, 0}\' are not configured to use trial balance.', [ implode( ', ', $arrstrMigrationModeDisabledProperties ) ] );
			if( $boolIsImportJe ) {
				$strErrorMsg = __( 'Property(s) \'{%s, 0}\' are not configured to use trial balance for Journal Entry {%s, 1}.', [ implode( ', ', $arrstrMigrationModeDisabledProperties ), $this->m_objGlHeader->getReference() ] );
			}
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMsg ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrZeroAmountProperties ) && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$strErrorMsg = __( 'Debit & credit amount is required for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrZeroAmountProperties ) ] );
			if( $boolIsImportJe ) {
				$strErrorMsg = __( 'Debit & credit amount is required for property(s) \'{%s, 0}\' for Journal Entry {%s, 1}.', [ implode( ', ', $arrstrZeroAmountProperties ), $this->m_objGlHeader->getReference() ] );
			}
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMsg ) );
			$boolIsValid &= false;
		}

		if( true == valArr( $arrstrSameToFromGlAccountProperties ) ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'From account & to account should not be same for property(s) \'{%s, 0}\'.', [ implode( ', ', $arrstrSameToFromGlAccountProperties ) ] ) ) );
			$boolIsValid &= false;
		}

		if( !$boolIsValidAmount && CGlHeader::ACCOUNT_BALANCE_METHOD != $this->m_objGlHeader->getAmountMethodTypeId() ) {
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Amount can not be zero.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valIsGlAccountReconciled( $objClientDatabase = NULL ) :bool {

		$boolIsValid							= true;

		$arrintPropertyIdsByGlAccountId		= [];
		$arrobjGlDetails					= ( array ) $this->m_objGlHeader->getPostedGlDetails();

		foreach( $arrobjGlDetails as $objGlDetail ) {

			if( valId( $objGlDetail->getAccrualGlAccountId() ) || valId( $objGlDetail->getCashGlAccountId() ) ) {

				$intGlAccountId = $objGlDetail->getAccrualGlAccountId() ?? $objGlDetail->getCashGlAccountId();

				$arrmixGlAccountProperties = ( array ) $objGlDetail->getGlAccountProperties();

				if( true == valArr( $arrmixGlAccountProperties ) && true == array_key_exists( $intGlAccountId, $arrmixGlAccountProperties ) && CGlAccountUsageType::BANK_ACCOUNT != $arrmixGlAccountProperties[$intGlAccountId]['gl_account_usage_type_id'] ) {
					continue;
				}

				if( !valId( $objGlDetail->getGlReconciliationId() ) ) {
					$arrintPropertyIdsByGlAccountId[$intGlAccountId][$objGlDetail->getPropertyId()] = $objGlDetail->getPropertyId();
				}
			}
		}

		if( valArr( $arrintPropertyIdsByGlAccountId ) ) {
			$arrmixBankAccount = ( array ) \Psi\Eos\Entrata\CGlReconciliations::createService()->fetchReconciledBankAccountNameByGlAccountIdAndPropertyIdByTransactionDateByPostMonthByCid( $arrintPropertyIdsByGlAccountId, $this->m_objGlHeader->getPostDate(), $this->m_objGlHeader->getPostMonth(), $this->m_objGlHeader->getCid(), $objClientDatabase );
			if( valArr( $arrmixBankAccount ) ) {
				$arrmixBankAccount = $arrmixBankAccount[0];

				if( $arrmixBankAccount['is_post_month'] ) {
					$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'This journal entry would impact a completed bank reconciliation. Please update the journal entry post month or reopen the \'{%s, 0}\' reconciliation.', [ $arrmixBankAccount['account_name'] ] ) ) );
				} else {
					$this->m_objGlHeader->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'This journal entry would impact a completed bank reconciliation. Please update the journal entry transaction date or reopen the \'{%s, 0}\' reconciliation.', [ $arrmixBankAccount['account_name'] ] ) ) );
				}
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	private function valGlAccounts( $boolJeAllowPostingToRestrictedGlAccount, $objClientDatabase ) {

		$boolIsValid                    = true;
		$arrintGlAccountIds             = [];
		$arrmixDisabledGlAccountNames   = [];
		$arrmixRestrictedGlAccountNames = [];

		foreach( $this->m_objGlHeader->getGlDetails() as $objGlDetail ) {
			$arrintGlAccountIds[] = $objGlDetail->getAccrualGlAccountId() ?? $objGlDetail->getCashGlAccountId();
		}

		$arrobjGlDetailsGlAccounts = ( array ) \Psi\Eos\Entrata\CGlAccounts::createService()->fetchGlAccountsByIdsByCid( $arrintGlAccountIds, $this->m_objGlHeader->getCid(), $objClientDatabase, true );

		foreach( $arrobjGlDetailsGlAccounts as $objGlAccount ) {
			if( !is_null( $objGlAccount->getDisabledBy() ) ) {
				$arrmixDisabledGlAccountNames[] = $objGlAccount->getAccountNumber() . ' - ' . $objGlAccount->getAccountName();
			}
			if( $objGlAccount->getRestrictForGl() ) {
				$arrmixRestrictedGlAccountNames[] = $objGlAccount->getAccountNumber() . ' - ' . $objGlAccount->getAccountName();
			}
		}

		if( valArr( $arrmixDisabledGlAccountNames ) ) {
			$strModuleName = $this->m_objGlHeader->getIsTemplate() ? __( 'journal entry template' ) : __( 'journal entry' );
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Unable to delete this {%s, 0} because GL Account [{%s, 1}] is disabled.', [ $strModuleName, implode( ', ', $arrmixDisabledGlAccountNames ) ] ) ) );
			$boolIsValid &= false;
		}

		if( !$boolJeAllowPostingToRestrictedGlAccount && valArr( $arrmixRestrictedGlAccountNames ) ) {
			$strModuleName = $this->m_objGlHeader->getIsTemplate() ? __( 'journal entry templates' ) : __( 'journal entries' );
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL account [{%s, 0}] is restricted, and you\'re not permissioned to delete the associated {%s, 1}.', [ implode( ', ', $arrmixRestrictedGlAccountNames ), $strModuleName ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDuplicateBillbackReimbursements( $arrintBillbackApDetailIds, $objClientDatabase ) {
		$boolIsValid = true;
		if( 0 < \Psi\Eos\Entrata\CApDetailReimbursements::createService()->fetchApDetailReimbursementsCountByOriginalApDetailIdsByCid( $arrintBillbackApDetailIds, $this->m_objGlHeader->getCid(), $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->m_objGlHeader->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'A billback journal entry has already been created for this transaction.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objCompanyUser = NULL, $boolIsSkipLineItems = true, $objClientDatabase = NULL, $boolEditExportedJePermission = false, $boolIsPropertyGlLocked = false, $boolIsReverse = false, $arrmixParameters = [], $boolJeAllowPostingToRestrictedGlAccount = false, $boolIsFromJeLibarary = false, $boolShowDisabledData = false, $boolIsImportJe = false, $arrintBillbackApDetailIds = [], $boolIsFromBillbackListing = false ) {

		$boolIsValid		= true;
		$boolIsDeleteJe		= $arrmixParameters['is_delete_je'] ?? false;
		$boolIsRequireMemo	= ( true == isset( $arrmixParameters['require_memo_when_reversing_transactions'] ) && true == $arrmixParameters['require_memo_when_reversing_transactions'] ) ? true : false;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valPostMonth( $objCompanyUser );
				$boolIsValid &= $this->valMemo();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPostMonth( $objCompanyUser );
				$boolIsValid &= $this->valIsJeDeleted();
				$boolIsValid &= $this->valCurrentPostMonth();
				$boolIsValid &= $this->valIsExported( $boolEditExportedJePermission, true );
				$boolIsValid &= $this->valIsReconciled();
				$boolIsValid &= $this->valGlAccounts( $boolJeAllowPostingToRestrictedGlAccount, $objClientDatabase );
				break;

			case 'journal_entry_approve':
				$boolIsValid &= $this->valLineItemAmounts( $this->getGlDetails() );
			case 'journal_entry_insert':
				$boolIsValid &= $this->valGlBookId();
				$boolIsValid &= $this->valGlHeaderTypeId();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valPostMonth( $objCompanyUser, $boolIsReverse );
				if( $boolIsFromBillbackListing ) {
					$boolIsValid &= $this->valDuplicateBillbackReimbursements( $arrintBillbackApDetailIds, $objClientDatabase );
				}
				$boolIsValid &= $this->valGlDetailCount();

				if( !$boolShowDisabledData ) {
					$boolIsValid &= $this->valUserAssignedProperties();
				}

				if( true == $this->m_objGlHeader->getIsTemplate() ) {
					$boolIsValid &= $this->valTemplateName( $strAction, $objClientDatabase );
				}

				if( true == $this->m_objGlHeader->getIsFromBillbackListing() || true == $this->m_objGlHeader->getIsBillbackJe() || $boolIsImportJe ) {
					$boolIsValid &= $this->valGlPosting( $boolJeAllowPostingToRestrictedGlAccount, $boolIsImportJe );
				}
				if( !$boolIsSkipLineItems ) {
					if( $boolIsFromJeLibarary ) {
						$boolIsValid &= $this->valJeLineItems( $boolIsImportJe );
					} else {
						$boolIsValid &= $this->valGlDetails();
					}
				}

				if( !$this->m_objGlHeader->getIsTemplate() && $boolIsFromJeLibarary && $boolIsValid ) {
					$boolIsValid &= $this->valIsGlAccountReconciled( $objClientDatabase );
				}
				break;

			case 'journal_entry_reverse':
				$boolIsValid &= $this->valIsJeReversed();
				$boolIsValid &= $this->valIsJeDeleted();
				$boolIsValid &= $this->valPostDate( true );
				$boolIsValid &= $this->valPostMonth( $objCompanyUser, true );

				if( true == $boolIsRequireMemo ) {
					$boolIsValid &= $this->valMemo();
				}

				$boolIsValid &= $this->valUserAssignedProperties();
				break;

			case 'journal_entry_update':
				$boolIsValid &= $this->valGlBookId();
				$boolIsValid &= $this->valGlHeaderTypeId();
				$boolIsValid &= $this->valGlTransactionTypeId();
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valPostMonth( $objCompanyUser, $boolIsReverse, $boolIsPropertyGlLocked );
				if( !$this->m_objGlHeader->getIsTemplate() ) {
					$boolIsValid &= $this->valIsExported( $boolEditExportedJePermission );
				}
				$boolIsValid &= $this->valGlDetailCount();
				if( true == $this->m_objGlHeader->getIsTemplate() ) {
					$boolIsValid &= $this->valTemplateName( $strAction, $objClientDatabase );
				}

				if( !$boolIsSkipLineItems ) {
					if( $boolIsFromJeLibarary ) {
						$boolIsValid &= $this->valJeLineItems();
					} else {
						$boolIsValid &= $this->valGlDetails();
					}
				}

				if( !$boolIsImportJe && !$this->m_objGlHeader->getIsTemplate() && $boolIsFromJeLibarary && $boolIsValid ) {
					$boolIsValid &= $this->valIsGlAccountReconciled( $objClientDatabase );
				}
				break;

			case 'journal_entry_pause':
				$boolIsValid &= $this->valPostDate();
				$boolIsValid &= $this->valPauseJePostMonth();
				$boolIsValid &= $this->valGlDetailCount();
				$boolIsValid &= $this->valGlBookId();
				if( $boolIsFromBillbackListing ) {
					$boolIsValid &= $this->valDuplicateBillbackReimbursements( $arrintBillbackApDetailIds, $objClientDatabase );
				}

				if( true == $this->m_objGlHeader->getIsFromBillbackListing() || $boolIsImportJe ) {
					$boolIsValid &= $this->valGlPosting( $boolJeAllowPostingToRestrictedGlAccount, $boolIsImportJe );
				}
				break;

			case 'user_assigned_properties':
				$boolIsValid &= $this->valUserAssignedProperties( $boolShowDisabledData, $boolIsDeleteJe );
				break;

			case 'journal_entry_edit':
				$boolIsValid &= $this->valPostedJe();
				$boolIsValid &= $this->valDeletedJe();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;

	}

}
?>