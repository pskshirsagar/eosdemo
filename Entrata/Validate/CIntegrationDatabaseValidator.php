<?php
class CIntegrationDatabaseValidator {

	protected $m_objIntegrationDatabase;

	public function __construct() {
		return;
	}

	public function setIntegrationDatabase( $objIntegrationDatabase ) {
		$this->m_objIntegrationDatabase = $objIntegrationDatabase;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objIntegrationDatabase->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIntegrationClientTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objIntegrationDatabase->getIntegrationClientTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_client_type_id', 'Integration client type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objIntegrationDatabase->getName() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDatabaseName() {
		$boolIsValid = true;

		if( CIntegrationClientType::CREDIT_SYSTEM == $this->m_objIntegrationDatabase->getIntegrationClientTypeId() ) {
			return $boolIsValid;
		}

		if( false == valStr( $this->m_objIntegrationDatabase->getDatabaseName() ) && false == valStr( $this->m_objIntegrationDatabase->getDevDatabaseName() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_name', 'Database name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBaseUri( $strAction, $objDatabase ) {

		$boolIsValid = true;

		// Strip Out extra slashes
		if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->m_objIntegrationDatabase->getBaseUri() ) ) {
			$this->m_objIntegrationDatabase->setBaseUri( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->m_objIntegrationDatabase->getBaseUri() ) );
		}

		if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->m_objIntegrationDatabase->getDevBaseUri() ) ) {
			$this->m_objIntegrationDatabase->setDevBaseUri( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->m_objIntegrationDatabase->getDevBaseUri() ) );
		}

		if( false == valStr( $this->m_objIntegrationDatabase->getBaseUri() ) && false == valStr( $this->m_objIntegrationDatabase->getDevBaseUri() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_uri', 'Base uri is required.' ) );

		} elseif( ( true == valStr( $this->m_objIntegrationDatabase->getBaseUri() ) && false == CValidation::checkUrl( $this->m_objIntegrationDatabase->getBaseUri() ) ) || ( true == valStr( $this->m_objIntegrationDatabase->getDevBaseUri() ) && false == CValidation::checkUrl( $this->m_objIntegrationDatabase->getDevBaseUri() ) ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_uri', 'Invalid base uri.' ) );
		}

		if( true == $boolIsValid && 'insert' == $strAction && false === \Psi\CStringService::singleton()->strpos( $this->m_objIntegrationDatabase->getBaseUri(), 'https://' ) ) {

			$objClient = $this->m_objIntegrationDatabase->fetchClient( $objDatabase );

			if( false == valObj( $objClient, 'CClient' ) ) return false;

			if( CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && false == in_array( $this->m_objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MIGRATION, CIntegrationClientType::JENARK ] ) && true == defined( 'CONFIG_ENVIRONMENT' ) && 'production' == CONFIG_ENVIRONMENT ) {
				$boolIsValid = false;
				$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_uri', 'Invalid Base Uri.  Only secure (https) connections allowed.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objIntegrationDatabase->getUsername() ) && false == valStr( $this->m_objIntegrationDatabase->getDevUsername() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', ' Username is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objIntegrationDatabase->getPassword() ) && false == valStr( $this->m_objIntegrationDatabase->getDevPassword() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', ' Password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valServerName() {

		$boolIsValid = true;

		if( CIntegrationClientType::YARDI_RPORTAL == $this->m_objIntegrationDatabase->getIntegrationClientTypeId() && false == valStr( $this->m_objIntegrationDatabase->getServerName() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'server_name', 'Server name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPlatform() {

		$boolIsValid = true;

		if( CIntegrationClientType::YARDI_RPORTAL == $this->m_objIntegrationDatabase->getIntegrationClientTypeId() && false == valStr( $this->m_objIntegrationDatabase->getPlatform() ) ) {
			$boolIsValid = false;
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'platform', 'Platform is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRestoreUrl() {
		$boolIsValid = true;

		//         if( CIntegrationClientType::MIGRATE_REAL_PAGE == $this->m_objIntegrationDatabase->getIntegrationClientTypeId() ) {
		// 	        // Strip Out extra slashes
		// 	        if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->m_objIntegrationDatabase->getRestoreUrl() ) ) {
		// 	        	$this->m_objIntegrationDatabase->setRestoreUrl( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->m_objIntegrationDatabase->getRestoreUrl() ) );
		// 	        }

		// 	        if( true == is_null( $this->m_objIntegrationDatabase->getRestoreUrl() ) ) {
		// 			    $boolIsValid = false;
		// 			    $this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'restore_url', 'Ftp url is required.' ) );

		// 	        } elseif( false == CValidation::checkUrl( $this->m_objIntegrationDatabase->getRestoreUrl() ) ) {
		// 			    $boolIsValid = false;
		// 			    $this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'restore_url', 'Invalid ftp url.' ) );

		// 	        }
		//         }

		return $boolIsValid;
	}

	public function valDatabaseServerName() {
		return true;
	}

	public function valDatabaseUsername() {
		return true;
	}

	public function valDatabasePassword() {
		return true;
	}

	public function valCustomers( $objClientDatabase ) {

		$arrobjCustomers = $this->m_objIntegrationDatabase->fetchCustomers( $objClientDatabase );

		if( true == valArr( $arrobjCustomers ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company customers.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valLeases( $objClientDatabase ) {

		$arrobjLeases = $this->m_objIntegrationDatabase->fetchLeases( $objClientDatabase );

		if( true == valArr( $arrobjLeases ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company leases.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valArCodes( $objClientDatabase ) {

		$arrobjArCodes = $this->m_objIntegrationDatabase->fetchArCodes( $objClientDatabase );

		if( true == valArr( $arrobjArCodes ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company charge codes.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valMaintenancePriorities( $objClientDatabase ) {

		$arrobjMaintenancePriorities = $this->m_objIntegrationDatabase->fetchMaintenancePriorities( $objClientDatabase );

		if( true == valArr( $arrobjMaintenancePriorities ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company maintenance priorities.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblems( $objClientDatabase ) {

		$arrobjMaintenanceProblems = $this->m_objIntegrationDatabase->fetchMaintenanceProblems( $objClientDatabase );

		if( true == valArr( $arrobjMaintenanceProblems ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company maintenance problems.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valMaintenanceStatusTypes( $objClientDatabase ) {

		$arrobjMaintenanceStatusTypes = $this->m_objIntegrationDatabase->fetchMaintenanceStatusTypes( $objClientDatabase );

		if( true == valArr( $arrobjMaintenanceStatusTypes ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to company maintenance status types.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valApPayees( $objClientDatabase ) {

		$arrobjApPayees = $this->m_objIntegrationDatabase->fetchApPayees( $objClientDatabase );

		if( true == valArr( $arrobjApPayees ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to ap payees.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valIntegrationClients( $objClientDatabase ) {

		$arrobjIntegrationClients = $this->m_objIntegrationDatabase->fetchIntegrationClients( $objClientDatabase );

		if( true == valArr( $arrobjIntegrationClients ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to integration clients.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPropertyIntegrationDatabases( $objClientDatabase ) {

		$arrobjPropertyIntegrationDatabases = $this->m_objIntegrationDatabase->fetchPropertyIntegrationDatabases( $objClientDatabase );

		if( true == valArr( $arrobjPropertyIntegrationDatabases ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to property integration databases.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valIntegrationServiceLogs( $objClientDatabase ) {

		$arrobjIntegrationServiceLogs = $this->m_objIntegrationDatabase->fetchIntegrationServiceLogs( $objClientDatabase );

		if( true == valArr( $arrobjIntegrationServiceLogs ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Integration database is associated to integration service logs.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	// This function checks if the production details and the staging details are same, throw an error.

	public function valConflictingDetails( $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) return false;

		$objClient = $this->m_objIntegrationDatabase->fetchClient( $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) return false;

		if( CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && trim( $this->m_objIntegrationDatabase->getDatabaseName() ) === trim( $this->m_objIntegrationDatabase->getDevDatabaseName() ) && trim( $this->m_objIntegrationDatabase->getUsernameEncrypted() ) === trim( $this->m_objIntegrationDatabase->getDevUsernameEncrypted() ) && trim( $this->m_objIntegrationDatabase->getPasswordEncrypted() ) === trim( $this->m_objIntegrationDatabase->getDevPasswordEncrypted() ) && trim( $this->m_objIntegrationDatabase->getBaseUri() ) === trim( $this->m_objIntegrationDatabase->getDevBaseUri() ) ) {
			$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please do not duplicate production details on dev Environment.' ) );
			return false;
		}

		return true;
	}

	public function valDescription() {

		$boolIsValid = true;

		if( ( true == in_array( $this->m_objIntegrationDatabase->getIntegrationclientStatusTypeId(), [ CIntegrationClientStatusType::ON_HOLD, CIntegrationClientStatusType::DISABLED ] ) ) ) {
			if( false == valStr( $this->m_objIntegrationDatabase->getDescription() ) ) {
				$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
				$boolIsValid = false;
			} elseif( '230' < strlen( $this->m_objIntegrationDatabase->getDescription() ) ) {
				$this->m_objIntegrationDatabase->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description should be 230 characters only.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {

		$boolIsValid = true;

		if( CIntegrationClientType::MRI_SIMPLE == $this->m_objIntegrationDatabase->getIntegrationClientTypeId() ) {

			 $this->m_objIntegrationDatabase->setName( 'MRI Simple' );
			 $this->m_objIntegrationDatabase->setDatabaseName( 'MRI Simple' );
			 $this->m_objIntegrationDatabase->setBaseUri( 'MRI Simple' );
			 $this->m_objIntegrationDatabase->setUsername( 'MRI Simple' );
			 $this->m_objIntegrationDatabase->setPassword( 'MRI Simple' );

			return true;
		}

		$strEnvironment = CConfig::get( 'environment' );
		$strEnvironment = ( true == valStr( $strEnvironment ) ) ? $strEnvironment : 'production';

		if( ( 'production' !== $strEnvironment ) ) {

			if( false == valStr( $this->m_objIntegrationDatabase->getDatabaseName() && true == valStr( $this->m_objIntegrationDatabase->getDevDatabaseName() ) ) ) {
				 $this->m_objIntegrationDatabase->setDatabaseName( $this->m_objIntegrationDatabase->getDevDatabaseName() );
			}

			if( false == valStr( $this->m_objIntegrationDatabase->getBaseUri() && true == valStr( $this->m_objIntegrationDatabase->getDevBaseUri() ) ) ) {
				 $this->m_objIntegrationDatabase->setBaseUri( $this->m_objIntegrationDatabase->getDevBaseUri() );
			}

			if( false == valStr( $this->m_objIntegrationDatabase->getUsername() && true == valStr( $this->m_objIntegrationDatabase->getDevUsername() ) ) ) {
				 $this->m_objIntegrationDatabase->setUsername( $this->m_objIntegrationDatabase->getDevUsername() );
			}

			if( false == valStr( $this->m_objIntegrationDatabase->getPassword() && true == valStr( $this->m_objIntegrationDatabase->getDevPassword() ) ) ) {
				 $this->m_objIntegrationDatabase->setPassword( $this->m_objIntegrationDatabase->getDevPassword() );
			}
		}

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIntegrationClientTypeId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDatabaseName();
				$boolIsValid &= $this->valBaseUri( $strAction, $objClientDatabase );
				$boolIsValid &= $this->valUsername();
				$boolIsValid &= $this->valPassword();
				// $boolIsValid &= $this->valRestoreUrl();
				$boolIsValid &= $this->valDatabaseServerName();
				$boolIsValid &= $this->valDatabaseUsername();
				$boolIsValid &= $this->valDatabasePassword();
				$boolIsValid &= $this->valServerName();
				$boolIsValid &= $this->valPlatform();
				$boolIsValid &= $this->valConflictingDetails( $objClientDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCustomers( $objClientDatabase );
				$boolIsValid &= $this->valLeases( $objClientDatabase );
				$boolIsValid &= $this->valArCodes( $objClientDatabase );
				$boolIsValid &= $this->valMaintenancePriorities( $objClientDatabase );
				$boolIsValid &= $this->valMaintenanceProblems( $objClientDatabase );
				$boolIsValid &= $this->valMaintenanceStatusTypes( $objClientDatabase );
				$boolIsValid &= $this->valApPayees( $objClientDatabase );
				$boolIsValid &= $this->valIntegrationClients( $objClientDatabase );
				$boolIsValid &= $this->valPropertyIntegrationDatabases( $objClientDatabase );
				$boolIsValid &= $this->valIntegrationServiceLogs( $objClientDatabase );
				break;

			case 'validate_conflicting_details':
				$boolIsValid &= $this->valConflictingDetails( $objClientDatabase );
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

}
?>