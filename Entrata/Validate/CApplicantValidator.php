<?php

class CApplicantValidator {

	protected $m_objApplicant;
	protected $m_objApplicationDataObject;
	protected $m_objProperty;
	protected $m_boolValidateRequired;
	protected $m_arrintPropertySubsidyTypes;

	const DEFAULT_REQUIRED_APPLICANT_AGE = 18;

	public function __construct( $objApplicant = NULL, $boolValidateRequired = true ) {
		$this->m_objApplicant = $objApplicant;
		$this->m_boolValidateRequired = $boolValidateRequired;
		return;
	}

	public function getAdo() {
		return $this->m_objApplicationDataObject;
	}

	public function setApplicationDataObject( $objApplicationDataObject ) {
		$this->m_objApplicationDataObject = $objApplicationDataObject;
	}

	public function getPropertySubsidyTypes() {
		return $this->m_arrintPropertySubsidyTypes;
	}

	public function setPropertySubsidyTypes( $arrintPropertySubsidyTypes ) {
		$this->m_arrintPropertySubsidyTypes = $arrintPropertySubsidyTypes;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case 'validate_member_step':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				if( $this->m_objApplicant->getCustomerTypeId() != CCustomerType::NOT_RESPONSIBLE ) {
					$boolIsValid &= $this->valUsername( $this->getAdo()->m_objDatabase, $boolUsernameRequired = false, $boolIsAllowDuplicateEmail = true );
				}
				$boolIsValid &= $this->valPassword( false, false );
				$boolIsBirthDateRequired = $this->m_boolValidateRequired && array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $this->getAdo()->m_arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->valBirthDate( $boolIsBirthDateRequired, $this->getRequiredApplicantAge() );

				$boolIsValid &= $this->valRequiredBasicInfoFields();

				if( true == array_key_exists( 'BASIC_INFO_IDENTIFICATION_TYPE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && \CProperty::TAX_ID == $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_IDENTIFICATION_TYPE'] && ( ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) ) || ( true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() ) ) ) ) {
					$strSectionName = CTaxIdType::createService()->getAbbreviations()[( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) ? ( $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) : ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ? $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() : '' )];

					$boolRequired   = ( bool ) ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN_REQUIRED', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN_REQUIRED']->getValue() ) ?? true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED']->getValue() ) );
					$boolIsValid    &= $this->valAffordableTaxNumberEncrypted( $strSectionName, $boolRequired );
				}
				break;

			case 'validate_additional_info_step':
				$boolIsValid &= $this->valRequiredAdditionalInfoFields( $this->getAdo()->m_arrobjPropertyApplicationPreferences, $this->getAdo()->m_objDatabase );
				$boolIsValid &= $this->valGeneralQuestions( $this->getAdo()->m_arrobjPropertyApplicationPreferences, $boolIsValidateRequired = false );
				$boolIsValid &= $this->valPublicJudgmentYear();
				$boolIsValid &= $this->valBankruptcyYear();
				break;

			case 'validate_basic_info_step':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $this->getAdo()->m_objDatabase, false, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword( false, false );
//				$boolIsValid &= $this->valIdentificationExpiration();
//				if( CCustomerType::PRIMARY == $this->m_objApplicant->getCustomerTypeId() ) {
//					$boolIsValid &= $this->valStudentIdNumber( $this->getAdo()->m_arrobjPropertyApplicationPreferences, false );
//				}

				// if customer type changed to not_responsible from responsible then set birthdate as NULL
				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getAdo()->m_objProperty->getId(), 'NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT', $this->getAdo()->m_objProperty->getCid(), $this->getAdo()->m_objDatabase );
				if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) && CCustomerType::NOT_RESPONSIBLE == $this->m_objApplicant->getCustomerTypeId() && true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && false == $this->getAdo()->m_arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) {
					$this->m_objApplicant->setBirthDate( NULL );
				}

				$intRequiredApplicantAge = 0;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( true == in_array( $this->m_objApplicant->getCustomerTypeId(), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) ) ) {
					$intRequiredApplicantAge = $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();

				} elseif( true == in_array( $this->m_objApplicant->getCustomerTypeId(), [ CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
					$intRequiredApplicantAge = 18;

				} elseif( CCustomerType::PRIMARY == $this->m_objApplicant->getCustomerTypeId() ) {
					$intRequiredApplicantAge = 18;
				}

				// validate Name Title
				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_TITLE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_GENDER', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valNamePrefix();
				}

				$intMaxApplicantAge = self::DEFAULT_REQUIRED_APPLICANT_AGE;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
					$intMaxApplicantAge = $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
				}

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) {
					if( true == in_array( COccupancyType::STUDENT, $this->getAdo()->m_objProperty->getOccupancyTypeIds() ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
						if( true == in_array( $this->m_objApplicant->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
							$boolIsValid = $this->valStudentBirthDate();
						} else {
							$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
						}
					} else {
						$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge, $intMaxApplicantAge );
					}

				} else {
					if( true == in_array( COccupancyType::STUDENT, $this->getAdo()->m_objProperty->getOccupancyTypeIds() ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
						if( true == in_array( $this->m_objApplicant->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
							$boolIsValid = $this->valStudentBirthDate();
						} else {
							$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
						}
					} else {
						$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
					}
				}

				$strLocaleCode = CLocale::DEFAULT_LOCALE;
				if( valArr( $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == valId( current( $this->getAdo()->m_arrobjPropertyApplicationPreferences )->getPropertyId() ) ) {
					$intPropertyId        = current( $this->getAdo()->m_arrobjPropertyApplicationPreferences )->getPropertyId();
					$objProperty          = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $this->m_intCid, $this->getAdo()->m_objDatabase );
					if( true == valObj( $objProperty, 'CProperty' ) ) {
						$strLocaleCode = $objProperty->getLocaleCode();
					}
				}

				if( true == array_key_exists( 'HIDE_BASIC_INFO_SECURE_INFO', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && false == $this->getAdo()->m_arrobjPropertyApplicationPreferences['HIDE_BASIC_INFO_SECURE_INFO']->getValue( $strLocaleCode ) ) {
					if( true == array_key_exists( 'BASIC_INFO_ASSIGN_TYPE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 'customer' == $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_ASSIGN_TYPE']->getValue( $strLocaleCode ) ) {
						if( true == array_key_exists( 'BASIC_INFO_IDENTIFICATION_TYPE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 'tax_id' == $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_IDENTIFICATION_TYPE']->getValue( $strLocaleCode ) ) {
							$boolIsValid &= $this->valTaxNumberEncrypted();
						}
					} else {
						$boolIsValid &= $this->valTaxNumberEncrypted();
					}
				}

				if( false == is_null( $this->m_objApplicant->getParentEmailAddress() ) ) {
					$boolIsValid &= $this->valParentEmailAddress();
				}
				break;

			case 'match_other_applicants':
				$strSSNSectionName = 'SSN';

				$strSSNSectionName = CTaxIdType::createService()->getAbbreviations()[( array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ? $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() : false ) ?? ( array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ? $this->getAdo()->m_arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() : false )];
				$strSSNSectionName = !valStr( $strSSNSectionName ) ? $strSSNSectionName : 'SSN';

				$boolIsValid &= $this->validateMatchOtherApplicants( $this->getAdo()->m_objApplication->getId(), $strSSNSectionName, $this->getAdo()->m_objDatabase );
				$boolIsValid &= $this->valNameFirst();

				if( false == $this->m_objApplicant->getCorporateLease() ) {
					$boolIsValid &= $this->valNameLast();
				}
				break;

			default:
				// do nothing
				break;
		}

		return $boolIsValid;
	}

	public function valParentEmailAddress() {
		$boolIsValid = true;

		$boolIsValid &= $this->m_objApplicant->validate( 'validate_parent_email' );

		if( \Psi\CStringService::singleton()->strtolower( trim( $this->m_objApplicant->getUsername() ) ) == \Psi\CStringService::singleton()->strtolower( trim( $this->m_objApplicant->getParentEmailAddress() ) ) ) {
			$boolIsValid &= false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_email_address', __( 'Parent\'s email address cannot be same as applicant\'s email address.' ) ) );
		}

		return $boolIsValid;
	}

	public function getRequiredApplicantAge() {
		$intRequiredApplicantAge = 0;

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( $this->m_objApplicant->getCustomerTypeId() == CCustomerType::PRIMARY || $this->m_objApplicant->getCustomerTypeId() == CCustomerType::GUARANTOR ) ) {
			$intRequiredApplicantAge = $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		} elseif( $this->m_objApplicant->getCustomerTypeId() != CCustomerType::NOT_RESPONSIBLE ) {
			$intRequiredApplicantAge = 18;
		}

		return $intRequiredApplicantAge;
	}

	public function valRequiredBasicInfoFields() {
		$boolIsValid = true;

		if( true == array_key_exists( 'SHOW_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && $this->m_objApplicant->getCustomerTypeId() == CCustomerType::GUARANTOR ) {
			$boolRelationshipToApplicantCanBeNull = ( false == array_key_exists( 'REQUIRE_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) ? true : false;
			$boolRelationshipToApplicantCanBeNull &= $this->m_boolValidateRequired;

			$boolIsValid &= $this->m_objApplicant->valRelationshipToApplicant( $boolRelationshipToApplicantCanBeNull );
		}

		if( true == $this->m_boolValidateRequired ) {

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_GENDER', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_TITLE', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->m_objApplicant->valGender();
			}

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 0 != $this->m_objApplicant->getHasMiddleName() ) {
				$boolIsValid &= $this->m_objApplicant->valNameMiddle();
			}

//			if( ( ( true == array_key_exists( 'REQUIRE_BASIC_INFO_TAX_NUMBER_OR_ID_NUMBER', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && ( true == $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_TAX_NUMBER_OR_ID_NUMBER']->getValue() ) && true == $this->m_objApplicant->getIsAlien() ) || true == array_key_exists( 'REQUIRE_BASIC_INFO_IDENTIFICATION_NUMBER', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) ) && 0 < $this->m_objApplicant->getCompanyIdentificationTypeId() ) {
//				$boolIsValid &= $this->m_objApplicant->valIdentificationValue();
//				$objCompanyIdentificationType = CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $this->m_objApplicant->getCompanyIdentificationTypeId(), $this->getAdo()->m_objClient->getId(), $this->getAdo()->m_objDatabase );
//
//				if( false == is_null( $this->m_objApplicant->getIdentificationNumber() ) && true == valObj( $objCompanyIdentificationType, 'CCompanyIdentificationType' ) && 'DR LIC' == $objCompanyIdentificationType->getSystemCode() ) {
//					$boolIsValid &= $this->m_objApplicant->valDlStateCode();
//				}
//			}
		}

		return $boolIsValid;
	}

	public function valNameFirst( $boolAddErrorMessage = true, $strSectionName = NULL ) {

		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'First ' );

		if( ( true == is_null( $this->m_objApplicant->getNameFirst() ) || 0 == strlen( $this->m_objApplicant->getNameFirst() ) ) ) {
			$boolIsValid = false;
			if( true == $boolAddErrorMessage ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s,0} name is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameFirst(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

		} else {
			if( 1 > strlen( $this->m_objApplicant->getNameFirst() ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name must have at least one letter.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolAddErrorMessage = true, $boolNameLastRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getNameLast() ) && 0 < strlen( $this->m_objApplicant->getNameLast() ) || true == $boolNameLastRequired ) {

			if( true == is_null( $this->m_objApplicant->getNameLast() ) || 0 == strlen( $this->m_objApplicant->getNameLast() ) ) {
				$boolIsValid = false;

				if( true == $boolAddErrorMessage ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Last Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}

			} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameLast(), 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Last Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

			}
		}

		return $boolIsValid;
	}

	// TO DO : Disassociate application check from while checking $intPrexistingApplicantCount

	public function valUsername( $objDatabase = NULL, $boolUsernameRequired = false, $boolAllowDuplicate = false, $boolUsernameConfirmRequired = false ) {
		$boolIsValid = true;

		if( false == CValidation::validateEmailAddresses( $this->m_objApplicant->getUsername() ) && 0 < strlen( $this->m_objApplicant->getUsername() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objApplicant->getUsername() ) || true == $boolUsernameRequired ) {
			if( true == is_null( $this->m_objApplicant->getUsername() ) || 0 == strlen( trim( $this->m_objApplicant->getUsername() ) ) ) {

				if( true == $this->m_boolValidateRequired ) {
					$boolIsValid = false;
                    $this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ), ERROR_CODE_EMAIL_REQUIRE, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}

				return $boolIsValid;

			} elseif( true == $boolUsernameConfirmRequired && false == is_null( $this->m_objApplicant->getUsernameConfirm() ) && true == $this->m_boolValidateRequired ) {
				if( 0 == strlen( trim( $this->m_objApplicant->getUsernameConfirm() ) ) ) {
					$boolIsValid = false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username_confirm', __( 'Confirm Email field is required.' ), ERROR_CODE_EMAIL_REQUIRE, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					return $boolIsValid;
				}
			}

			// Make sure it does not already exist in the client
			if( false == $boolAllowDuplicate && false == is_null( $objDatabase ) ) {

				$intPrexistingApplicantCount = CApplicants::fetchPreExistingApplicantCountWithPasswordByUsernameByCid( $this->m_objApplicant->getUsername(), $this->m_objApplicant->getId(), $this->m_objApplicant->getCid(), $objDatabase );

				if( 0 < $intPrexistingApplicantCount ) {
					$boolIsValid = false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'We\'ve found an account using that email already. Please login with your email and password, or if you have forgotten your password you can click the "Forgot/Generate Password?" link to reset it.' ), 319, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}
			}
		}

		if( true == $boolUsernameConfirmRequired && false == is_null( $this->m_objApplicant->getUsernameConfirm() ) && trim( $this->m_objApplicant->getUsername() ) != trim( $this->m_objApplicant->getUsernameConfirm() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username_confirm', __( 'Email and confirmation email do not match.' ), '', [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPassword( $boolPasswordConfirmRequired = true, $boolPasswordRequired = true ) {
		$boolIsValid = true;

		if( 0 < strlen( $this->m_objApplicant->getPassword() ) || true == $boolPasswordRequired ) {

			if( true == is_null( $this->m_objApplicant->getPassword() ) || 0 == strlen( $this->m_objApplicant->getPassword() ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password is required.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_objApplicant->getPassword ) && 6 > strlen( $this->m_objApplicant->getPassword ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password must be at least 6 characters long.' ) ) );
			}

			if( true == $boolIsValid && true == $boolPasswordConfirmRequired ) {
				if( true == is_null( $this->m_objApplicant->getPasswordConfirm() ) || 0 == strlen( $this->m_objApplicant->getPasswordConfirm() ) ) {
					$boolIsValid = false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Confirm Password is required.' ) ) );

				} elseif( trim( $this->m_objApplicant->getPassword ) != trim( $this->m_objApplicant->getPasswordConfirm() ) ) {
					$boolIsValid = false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password and confirmation password do not match.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valAffordableTaxNumberEncrypted( $strSectionName = NULL, $boolRequired = false ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->m_objApplicant->getTaxNumber() ) ) {
			if( false == $this->m_objApplicant->getIsAlien() ) {
				if( true == $boolRequired ) {
					$boolIsValid &= false;
				}
				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is required.', [ $strSectionName ] ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				} else {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is required.' ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}
			}
		} else {
			if( true == is_null( $this->m_objApplicant->getPropertyId() ) ) {
				$objActiveApplication = CApplications::fetchLatestApplicationByCustomerIdByCid( $this->m_objApplicant->getCustomerId(), $this->m_objApplicant->getCid(), $this->getAdo()->m_objDatabase );
				$this->m_objApplicant->setPropertyId( $objActiveApplication->getPropertyId() );
			}

			if( $this->m_objApplicant->getOrFetchProperty( $this->getAdo()->m_objDatabase )->getCountryCode() != 'MX' ) {
				if( ( 9 != \Psi\CStringService::singleton()->strlen( $this->m_objApplicant->getTaxNumber() ) )
				    || ( false == is_numeric( $this->m_objApplicant->getTaxNumber() ) )
				    || ( 0 == ( int ) $this->m_objApplicant->getTaxNumber() ) ) {
					$boolIsValid &= false;
					if( 0 < strlen( $strSectionName ) ) {
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is not valid.', [ $strSectionName ] ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					} else {
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is not valid.' ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			} else {

				if( ( 15 < \Psi\CStringService::singleton()->strlen( $this->m_objApplicant->getTaxNumber() ) )
				    || ( false == preg_match( '/^([-a-z0-9])+$/i', $this->m_objApplicant->getTaxNumber() ) )
				    || ( 0 > \Psi\CStringService::singleton()->strlen( $this->m_objApplicant->getTaxNumber() ) ) ) {
					$boolIsValid &= false;
					if( 0 < strlen( $strSectionName ) ) {
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is not valid.', [ $strSectionName ] ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					} else {
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is not valid.' ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valBirthDate1() {
		$boolIsValid = true;
		$boolIsBirthDateRequired = $this->m_boolValidateRequired && array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $this->getAdo()->m_arrobjPropertyApplicationPreferences );
		$boolIsValid &= $this->m_objApplicant->valBirthDate( $boolIsBirthDateRequired, $this->getRequiredApplicantAge() );
		return $boolIsValid;
	}

	public function valBirthDate( $boolBirthDateIsRequired = false, $intRequiredApplicantAge = NULL, $intMaxApplicantAge = NULL, $strSectionName = 'birth_date' ) {

		$boolIsValid	= true;
		$strBirthDate	= $this->m_objApplicant->getBirthDate();

		if( false == is_null( $strBirthDate ) && 0 < strlen( $strBirthDate ) && 1 !== CValidation::checkDate( $strBirthDate ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->setBirthDate( NULL );
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date must be in mm/dd/yyyy format.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

		} elseif( true == is_null( $strBirthDate ) && true == $boolBirthDateIsRequired && true == $this->m_boolValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date is required.' ), ' ', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		if( false == is_null( $strBirthDate ) && 0 < strlen( $strBirthDate ) ) {

			if( CCustomerType::GUARANTOR == $this->m_objApplicant->getCustomerTypeId() && 1 == CValidation::checkDate( $strBirthDate ) ) {

				if( false == is_null( $strBirthDate ) && 0 < strlen( $strBirthDate ) ) {
					$arrstrBirthDate	= explode( '/', $strBirthDate );
					$intYear 		= $arrstrBirthDate[2];

					// Year should be four digit
					if( 4 != strlen( $intYear ) ) {
						$boolIsValid = false;
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth year should be full year (yyyy form).' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			}

			if( true == $boolIsValid && false == is_null( $intRequiredApplicantAge ) && 0 != $intRequiredApplicantAge && false == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName,  __( 'Age should be above {%d,0} years.', [ $intRequiredApplicantAge ] ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			if( CCustomerType::NOT_RESPONSIBLE == $this->m_objApplicant->getCustomerTypeId() && true == $boolIsValid && false == is_null( $intMaxApplicantAge ) && 0 < $intMaxApplicantAge && true == $this->checkApplicantAge( $intMaxApplicantAge ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName,  __( 'Age should be less than {%d,0} years.', [ $intMaxApplicantAge ] ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			if( true == $boolIsValid && false == is_null( $strBirthDate ) && 0 < strlen( $strBirthDate ) && strtotime( $strBirthDate ) > strtotime( date( 'm/d/Y' ) ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date cannot be in the future.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			$intRequiredResponsibleApplicantAge = ( 0 != $intRequiredApplicantAge ) ? $intRequiredApplicantAge : 18;

			if( COccupancyType::AFFORDABLE == $this->getAdo()->m_objApplication->getOccupancyTypeId() ) {
				$this->m_objApplicant->setOccupancyTypeId( $this->getAdo()->m_objApplication->getOccupancyTypeId() );
				$intDefaultCustomerRelationshipId = CCustomerRelationships::fetchDefaultCustomerRelationshipIdByCustomerRelationshipIdByCid( $this->getAdo()->m_objApplicantApplication->getCustomerRelationshipId(), $this->getAdo()->m_objApplicant->getCid(), $this->getAdo()->m_objApplicant->getDatabase() );
				$this->m_objApplicant->setDefaultCustomerRelationshipId( $intDefaultCustomerRelationshipId );
			}

			if( CCustomerType::NOT_RESPONSIBLE == $this->m_objApplicant->getCustomerTypeId() ) {
				if( false == $this->m_objApplicant->isFosterOrLiveInAttendant() && false == $this->m_objApplicant->isFosterAdult() && true == $this->m_objApplicant->checkApplicantAge( $intRequiredResponsibleApplicantAge ) && ( true == valArr( $this->getPropertySubsidyTypes() ) && false == $this->m_objApplicant->isDependent() && ( count( array_intersect( [ CSubsidyType::HUD, CSubsidyType::TAX_CREDIT ], $this->getPropertySubsidyTypes() ) ) > 0 ) ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( 'Applicant older than {%d,0} can not be Non-Responsible.', [ $intRequiredResponsibleApplicantAge ] ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					$boolIsValid = false;
				}

				if( true == $this->m_objApplicant->isFosterAdult() && false == $this->m_objApplicant->checkApplicantAge( $intRequiredResponsibleApplicantAge ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( 'Age should be above ' ) . ( int ) $intRequiredResponsibleApplicantAge . ' ' . __( 'years.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function checkApplicantAge( $intRequiredApplicantAge ) {

		$boolIsValid = true;

		if( 0 == $intRequiredApplicantAge ) {
			return $boolIsValid;
		}

		$strYear    = ( int ) date( 'Y', time() ) - ( int ) date( 'Y', strtotime( $this->m_objApplicant->getBirthDate() ) );
		$strMonth 	= ( int ) date( 'm', time() ) - ( int ) date( 'm', strtotime( $this->m_objApplicant->getBirthDate() ) );
		$strDay		= ( int ) date( 'd', time() ) - ( int ) date( 'd', strtotime( $this->m_objApplicant->getBirthDate() ) );

		if( $strYear < $intRequiredApplicantAge || ( $strYear == $intRequiredApplicantAge && ( $strMonth < 0 || ( $strMonth == 0 && $strDay < 0 ) ) ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase ) {

        if( true == in_array( $this->getAdo()->m_objApplication->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::RENEWAL ] )
            && false == is_null( $this->getAdo()->m_objApplicantApplication->getCompletedOn() )
            && CLeaseStatusType::APPLICANT != $this->getAdo()->m_objApplicantApplication->getLeaseStatusTypeId() ) {
            return true;
        }

		$boolIsValid = true;

		$boolIsValid &= $this->valPersonalInformation( $arrobjPropertyApplicationPreferences, $objDatabase );

		if( ( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerType::GUARANTOR != $this->m_objApplicant->getCustomerTypeId() ) ) {
			$boolIsValid &= $this->valPreviousAddressInformation( $arrobjPropertyApplicationPreferences );
		}

		$boolIsValid &= $this->valCurrentAddressInformation( $arrobjPropertyApplicationPreferences );

		return $boolIsValid;
	}

	public function valPersonalInformation( $arrobjPropertyApplicationPreferences, $objDatabase ) {
		$boolIsValid = true;
		$strSectionName = '';

		if( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PERSONAL_INFO', $arrobjPropertyApplicationPreferences ) ) {
			return true;
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_NAME_MAIDEN', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valNameMaiden();
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_HEIGHT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valHeight( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_WEIGHT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valWeight( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_OCCUPATION', $arrobjPropertyApplicationPreferences ) ) {

			// If Occupations not associated with the property, it will not validate
			$intPropertyId = $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_OCCUPATION']->getPropertyId();

			$intPropertyOccupationCount = CPropertyOccupations::fetchPublishedPropertyOccupationsByPropertyIdByCid( $intPropertyId, $this->m_objApplicant->getCid(), $objDatabase, $boolReturnCount = true );

			if( 0 < $intPropertyOccupationCount ) {
				$boolIsValid &= $this->valOccupation( $boolIsValidateRequired );
			}
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_MARITAL_STATUS', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valMaritalStatusTypeId( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_EYE_COLOR', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valEyeColor( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_HAIR_COLOR', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valHairColor( $boolIsValidateRequired );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true ) {

		$boolIsValid = true;
		$strSectionName = '';

		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue() ) {
				$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue();
			}

			if( CCustomerAddress::ADDRESS_OWNER_TYPE_OWNER != $this->m_objApplicant->getCurrentAddressOwnerType() ) {

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameFirst( $strSectionName, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameFirst( $strSectionName, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameLast( $strSectionName, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameLast( $strSectionName, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) && CCustomerAddress::ADDRESS_OWNER_TYPE_RENTER == $this->m_objApplicant->getCurrentAddressOwnerType() ) {
					$boolIsValid &= $this->valCurrentAddressPhoneNumber( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressPhoneNumber( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerAddress::ADDRESS_OWNER_TYPE_RENTER == $this->m_objApplicant->getCurrentAddressOwnerType() ) {
					$boolIsValid &= $this->valCurrentAddressEmail( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressEmail( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentFaxNumber( false, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentFaxNumber( true, $strSectionName, $boolIsValidateRequired );
				}
			}
		}
		return $boolIsValid;
	}

	public function valCurrentAddressManagerNameFirst( $strSectionName = NULL, $boolIsRequired, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getCurrentManagerNameFirst() ) ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_manger_name_first', __( '{%s,0} manager first name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_manager_name_first', __( 'Current address manager first name is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getCurrentManagerNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getCurrentManagerNameFirst(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( 'Names cannot contain @ signs or email headers.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} else {
			if( true == $boolCheckLength && false == is_null( $this->m_objApplicant->getCurrentManagerNameFirst() ) && 2 > strlen( $this->m_objApplicant->getCurrentManagerNameFirst() ) ) {
				$boolIsValid = false;

				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( 'Current address manager first name must have at least 2 letters.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressManagerNameLast( $strSectionName = NULL, $boolIsRequired, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getCurrentManagerNameLast() ) ) {
				if( true == $this->m_boolValidateRequired ) {
					$boolIsValid = false;
				}

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_manager_name_last', __( '{%s,0} manager last name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_manager_name_last', __( 'Current address manager last name is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getCurrentManagerNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getCurrentManagerNameLast(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;

			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( 'Names cannot contain @ signs or email headers.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} else {
			if( true == $boolCheckLength && false == is_null( $this->m_objApplicant->getCurrentManagerNameLast() ) && 2 > strlen( $this->m_objApplicant->getCurrentManagerNameLast() ) ) {
				$boolIsValid = false;

				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( 'Current address manager last name must have at least 2 letters.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressPhoneNumber( $boolIsRequired = false, $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current address';
		$objCurrentPhoneNumber = $this->createPhoneNumber( $this->m_objApplicant->getCurrentPhoneNumber() );
		if( true == $boolIsRequired && false == valStr( $objCurrentPhoneNumber->getNumber() ) ) {

			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_phone_number', __( '{%s,0} phone number is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		} elseif( true == valStr( $objCurrentPhoneNumber->getNumber() && false == $objCurrentPhoneNumber->isValid() ) ) {
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_phone_number', __( '{%s,sectionName} phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objCurrentPhoneNumber->getFormattedErrors(), [ 'region' => $objCurrentPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objCurrentPhoneNumber->getCountryCode(), 'sectionName' => $strSectionNameToDisplay ] ), 504, [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCurrentAddressEmail( $boolIsRequired = false, $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current address';

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getCurrentEmailAddress() ) && true == $this->m_boolValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_email_address', __( '{%s,0} email is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_email_address', __( 'Current address email is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->m_objApplicant->getCurrentEmailAddress() ) && false == CValidation::validateEmailAddress( $this->m_objApplicant->getCurrentEmailAddress() ) ) {
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Current Address' );
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_email_address', __( '{%s,0} valid email is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => 'Current Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCurrentFaxNumber( $boolCanBeNull = false, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( false, $strSectionName, 'current_address_' );
		} elseif( 0 < strlen( $this->m_objApplicant->getCurrentFaxNumber() ) ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( true, $strSectionName, 'current_address_' );
		}

		return $boolIsValid;
	}

	public function valAdditionalInfoFaxNumber( $boolCanBeNull = false, $strSectionName = NULL, $strFaxNumberType = NULL ) {
		$boolIsValid 		= true;
		$strFaxValue 		= ( 'current_address_' == $strFaxNumberType ) ? $this->m_objApplicant->getCurrentFaxNumber() : $this->m_objApplicant->getPreviousFaxNumber();
		$intLength 	 		= strlen( $strFaxValue );
		$strSectionName 	= ( 0 < strlen( $strSectionName ) ) ? $strSectionName : ( 'previous_address_' == $strFaxNumberType ? __( 'Previous Address' ) : __( 'Current Address' ) );

		if( false == $boolCanBeNull && ( true == is_null( $strFaxValue ) || 0 == $intLength ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s,0} fax number is required.', [ $strSectionName ] ), '', [ 'label' => $strSectionName, 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s,0} fax number must be between 10 and 15 characters.', [ $strSectionName ] ), 504 ) );

		} elseif( true == isset( $strFaxValue ) && false == ( CValidation::validateFullPhoneNumber( $strFaxValue, false ) ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s,0} fax number is not valid.', [ $strSectionName ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true ) {
		$boolIsValid = true;
		$strSectionName = '';
		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue();
		}

		if( ( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerType::GUARANTOR != $this->m_objApplicant->getCustomerTypeId() )
				|| ( CCustomerType::GUARANTOR == $this->m_objApplicant->getCustomerTypeId() && true == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {

					if( CCustomerAddress::ADDRESS_OWNER_TYPE_OWNER != $this->m_objApplicant->getPreviousAddressOwnerType() ) {

						if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressManagerNameFirst( $strSectionName, $boolIsRequired = true, $boolCheckLength = false );
						} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressManagerNameFirst( $strSectionName, $boolIsRequired = false, $boolCheckLength = false );
						}

						if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressManagerNameLast( $strSectionName, $boolIsRequired = true, $boolCheckLength = false );
						} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressManagerNameLast( $strSectionName, $boolIsRequired = false, $boolCheckLength = false );
						}

						if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) && CCustomerAddress::ADDRESS_OWNER_TYPE_RENTER == $this->m_objApplicant->getPreviousAddressOwnerType() ) {
							$boolIsValid &= $this->valPreviousAddressPhoneNumber( true, $strSectionName, $boolIsValidateRequired );
						} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressPhoneNumber( false, $strSectionName, $boolIsValidateRequired );
						}

						if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerAddress::ADDRESS_OWNER_TYPE_RENTER == $this->m_objApplicant->getPreviousAddressOwnerType() ) {
							$boolIsValid &= $this->valPreviousAddressEmail( true, $strSectionName, $boolIsValidateRequired );
						} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
							$boolIsValid &= $this->valPreviousAddressEmail( false, $strSectionName, $boolIsValidateRequired );
						}

						if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences )
								&& !( $this->m_objApplicant->getCustomerTypeId() == CCustomerType::GUARANTOR && false == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {
									$boolIsValid &= $this->valPreviousFaxNumber( false, $strSectionName, $boolIsValidateRequired );
						} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
									$boolIsValid &= $this->valPreviousFaxNumber( true, $strSectionName, $boolIsValidateRequired );
						}

					}

		}

				return $boolIsValid;
	}

	public function valPreviousAddressManagerNameFirst( $strSectionName = NULL, $boolIsRequired, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getPreviousManagerNameFirst() ) && true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_manger_name_first', __( '{%s,0} manager first name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_manager_name_first', __( 'Previous address manager first name is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getPreviousManagerNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getPreviousManagerNameFirst(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( true == $boolCheckLength && false == is_null( $this->m_objApplicant->getPreviousManagerNameFirst() ) && 2 > strlen( $this->m_objApplicant->getPreviousManagerNameFirst() ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( 'Previous address manager first name must have at least 2 letters.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressManagerNameLast( $strSectionName = NULL, $boolIsRequired, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getPreviousManagerNameLast() ) && true == $this->m_boolValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_manager_name_last', __( '{%s,0} manager last name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_manager_name_last', __( 'Previous address manager last name is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getPreviousManagerNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getPreviousManagerNameLast(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( true == $boolCheckLength && false == is_null( $this->m_objApplicant->getPreviousManagerNameLast() ) && 2 > strlen( $this->m_objApplicant->getPreviousManagerNameLast() ) ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( 'Previous address manager last name must have at least 2 letters.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressPhoneNumber( $boolIsRequired = false, $strSectionName = NULL ) {
		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous address';
		$objPreviousPhoneNumber = $this->createPhoneNumber( $this->m_objApplicant->getPreviousPhoneNumber() );

		if( true == $boolIsRequired && false == valStr( $objPreviousPhoneNumber->getNumber() ) && true == $this->m_boolValidateRequired ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_phone_number', __( '{%s,0} phone number is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		} elseif( true == valStr( $objPreviousPhoneNumber->getNumber() && false == $objPreviousPhoneNumber->isValid() ) ) {
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, previous_phone_number, __( '{%s,sectionName} phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objPreviousPhoneNumber->getFormattedErrors(), [ 'region' => $objPreviousPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPreviousPhoneNumber->getCountryCode(), 'sectionName' => $strSectionNameToDisplay ] ), 504, [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousAddressEmail( $boolIsRequired = false, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getPreviousEmailAddress() ) && true == $this->m_boolValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_email_address', __( '{%s,0} email is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_email_address', __( 'Previous address email is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->m_objApplicant->getPreviousEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objApplicant->getPreviousEmailAddress() ) ) {
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous Address' );
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_email_address', __( '{%s,0} valid email is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousFaxNumber( $boolCanBeNull = false, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( false, $strSectionName, 'previous_address_' );
		} elseif( 0 < strlen( $this->m_objApplicant->getPreviousFaxNumber() ) ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( true, $strSectionName, 'previous_address_' );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveInDate( $boolIsRequired = false,  $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous address';

		if( true == $boolIsRequired && 0 == strlen( $this->m_objApplicant->getPreviousMoveInDate() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous address move-in date required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->m_objApplicant->getPreviousMoveInDate() ) && 1 !== CValidation::checkDate( $this->m_objApplicant->getPreviousMoveInDate() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date must be in mm/dd/yyyy form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->m_objApplicant->getPreviousMoveInDate() ) && strtotime( $this->getPreviousMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentAmount( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->m_objApplicant->getPreviousMonthlyPaymentAmount() ) || 0 > $this->m_objApplicant->getPreviousMonthlyPaymentAmount() ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( 'Previous address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddress( $strSectionName = NULL, $strStepName = NULL ) {

		$boolIsValid = true;

		if( 'addl_info' == $strStepName ) {
			$boolIsNonUsAddress = ( bool ) $this->m_objApplicant->getPreviousIsNonUsAddress();
		} else {
			$boolIsNonUsAddress = ( bool ) $this->m_objApplicant->getIsAlien();
		}

		if( 0 == strlen( $this->m_objApplicant->getPreviousStreetLine1() ) && 0 == strlen( $this->m_objApplicant->getPreviousStreetLine2() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} elseif( 0 < $this->m_objApplicant->getPreviousIsNonUsAddress() ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous street is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		if( false == $boolIsNonUsAddress ) {

			if( 0 == strlen( $this->m_objApplicant->getPreviousCity() ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( 'Previous city is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valPreviousPostalCode( $strSectionName );

			if( 0 == strlen( $this->m_objApplicant->getPreviousStateCode() ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( 'Previous state/province code is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valNameMaiden() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getNameMaiden() ) ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}

			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameMaiden(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApplicant->getNameMaiden(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} else {
			if( 2 > strlen( $this->m_objApplicant->getNameMaiden() ) && true == $boolIsValidateRequired ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name must have at least 2 letters.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valHeight() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getHeight() ) || 0 == $this->m_objApplicant->getHeight() ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'height', __( 'Height is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valWeight() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getWeight() ) || 0 == $this->m_objApplicant->getWeight() ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weight', __( 'Weight is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valOccupation() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getOccupationId() ) || 0 == $this->m_objApplicant->getOccupationId() ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupation_id', __( 'Occupation is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valEyeColor() {

		$boolIsValid = true;
		if( true == is_null( $this->m_objApplicant->getEyeColor() ) ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'eye_color', __( 'Eye color is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valHairColor() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objApplicant->getHairColor() ) ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hair_color', __( 'Hair color is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valMaritalStatusTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objApplicant->getMaritalStatusTypeId() ) ) {
			if( true == $this->m_boolValidateRequired ) {
				$boolIsValid = false;
			}
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marital_status_type_id', __( 'Marital Status is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valGeneralQuestions( $arrobjPropertyApplicationPreferences ) {

		$boolIsValid = true;
		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS', $arrobjPropertyApplicationPreferences ) ) {
			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_RENT', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_HAS_BEEN_SUED_FOR_RENT', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getWasSuedForRent() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}

					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_rent', '', '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->m_objApplicant->getWasSuedForRent() && true == is_null( $this->m_objApplicant->getSuedForRentComments() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sued_for_rent_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_DAMAGES', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_PROPERTY_DAMAGES', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getWasSuedForDamages() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_damages', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->m_objApplicant->getWasSuedForDamages() && true == is_null( $this->m_objApplicant->getSuedForDamageComments() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sued_for_damage_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_EVICTED', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_EVICTED', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getHasBeenEvicted() ) ) {
					$boolIsValid &= false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_been_evicted' ) );
				}

				if( true == $this->m_objApplicant->getHasBeenEvicted() && true == is_null( $this->m_objApplicant->getEvictedComments() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_been_evicted', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getHasBrokenLease() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_broken_lease', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->m_objApplicant->getHasBrokenLease() && true == is_null( $this->m_objApplicant->getBrokenLeaseComments() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_broken_lease', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( CCustomerType::GUARANTOR != $this->m_objApplicant->getCustomerTypeId() ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_FELONY', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_FELONY', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->m_objApplicant->getHasFelonyConviction() ) ) {
						if( true == $this->m_boolValidateRequired ) {
							$boolIsValid &= false;
						}
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_felony_conviction', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}

					if( true == $this->m_objApplicant->getHasFelonyConviction() && true == is_null( $this->m_objApplicant->getFelonyConvictionComments() ) ) {
						if( true == $this->m_boolValidateRequired ) {
							$boolIsValid &= false;
						}
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_conviction_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_PUBLIC_SUITS', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_PUBLIC_SUITS', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getHasPublicJudgment() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_public_judgment', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->m_objApplicant->getHasPublicJudgment() && true == is_null( $this->m_objApplicant->getPublicJudgmentYear() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'public_judgment_year', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_BANKRUPTCY', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_BANKRUPTCY', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getHasBankruptcy() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_bankruptcy', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->m_objApplicant->getHasBankruptcy() ) {
					if( false == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
						if( true == is_null( $this->m_objApplicant->getBankruptcyYear() ) ) {
							if( true == $this->m_boolValidateRequired ) {
								$boolIsValid &= false;
							}
							$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_year', __( 'Year is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( true == is_null( $this->m_objApplicant->getBankruptcyCounty() ) ) {
							if( true == $this->m_boolValidateRequired ) {
								$boolIsValid &= false;
							}
							$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_county', __( 'County is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( true == is_null( $this->m_objApplicant->getBankruptcyStateCode() ) ) {
							if( true == $this->m_boolValidateRequired ) {
								$boolIsValid &= false;
							}
							$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_state', __( 'State/Province is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
					}
				}
			}

			if( CCustomerType::GUARANTOR != $this->m_objApplicant->getCustomerTypeId() ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_INSURANCE', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->m_objApplicant->getHasRentersInsurance() ) ) {
						if( true == $this->m_boolValidateRequired ) {
							$boolIsValid &= false;
						}
						$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_renters_insurance', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}

					if( true == $this->m_objApplicant->getHasRentersInsurance() ) {
						if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_POLICY_NAME', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_objApplicant->getInsuranceProviderName() ) ) {
							if( true == $this->m_boolValidateRequired ) {
								$boolIsValid &= false;
							}
							$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renter_policy_name', __( 'Provider name is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_POLICY_NUMBER', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_objApplicant->getInsurancePolicyNumber() ) ) {
							if( true == $this->m_boolValidateRequired ) {
								$boolIsValid &= false;
							}
							$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renter_policy_number', __( 'Policy number is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
					}
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SMOKING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getSmokes() ) ) {
					$boolIsValid &= false;
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'smokes', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( true == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->m_objApplicant->getHasPreviousRentOwed() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_previous_rent_owed', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == is_null( $this->m_objApplicant->getIsReturningResident() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_returning_resident', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == is_null( $this->m_objApplicant->getIsUnemployed() ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_unemployed', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == $boolIsValid ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'general_questions', __( 'Please select Yes/No for given question(s)' ), __( 'Explaination is required.' ), [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		}

		return $boolIsValid;
	}

	public function valPublicJudgmentYear() {

		$boolIsValid = true;

		if( 0 < strlen( $this->m_objApplicant->getPublicJudgmentYear() ) ) {
			if( false == is_numeric( $this->m_objApplicant->getPublicJudgmentYear() ) || 4 > strlen( $this->m_objApplicant->getPublicJudgmentYear() ) || 0 == $this->m_objApplicant->getPublicJudgmentYear() || 1900 > $this->m_objApplicant->getPublicJudgmentYear() ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'public_judgment_year', __( 'Public judgment year must be in a valid format.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valBankruptcyYear() {

		$boolIsValid = true;

		if( 0 < strlen( $this->m_objApplicant->getBankruptcyYear() ) ) {
			if( false == is_numeric( $this->m_objApplicant->getBankruptcyYear() ) || 4 > strlen( $this->m_objApplicant->getBankruptcyYear() ) || 0 == $this->m_objApplicant->getBankruptcyYear() || 1900 > $this->m_objApplicant->getBankruptcyYear() ) {
				$boolIsValid = false;
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_year', __( 'Bankruptcy year must be in a valid format.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

//	public function valIdentificationExpiration() {
//
//		$boolIsValid = true;
//
//		if( false == is_null( $this->m_objApplicant->getIdentificationExpiration() ) && 0 < strlen( $this->m_objApplicant->getIdentificationExpiration() ) && 1 !== CValidation::checkDate( $this->m_objApplicant->getIdentificationExpiration() ) ) {
//			$boolIsValid = false;
//			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_expiration', __( 'ID expiration date must be in mm/dd/yyyy form.' ) ) );
//		}
//
//		return $boolIsValid;
//	}

//	public function valStudentIdNumber( $arrobjPropertyApplicationPreferences, $boolCompleteApplication ) {
//		$boolIsValid = true;
//
//		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_STUDENT_ID_NUMBER', $arrobjPropertyApplicationPreferences ) && true == $boolCompleteApplication ) {
//			if( 0 == strlen( $this->getStudentIdNumber() ) ) {
//				$boolIsValid = false;
//				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_id_number', __( 'Student Id is required.' ), '' ) );
//			}
//		}
//
//		if( true == $boolIsValid && 15 < strlen( $this->m_objApplicant->getStudentIdNumber() ) ) {
//			$boolIsValid = false;
//			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_id_number', __( 'Student Id is not valid.' ), '' ) );
//		}
//
//		if( true == $boolIsValid && 0 < strlen( $this->m_objApplicant->getStudentIdNumber() ) && false == preg_match( '/^([-a-z0-9])+$/i', $this->m_objApplicant->getStudentIdNumber() ) ) {
//			$boolIsValid = false;
//			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_id_number', __( 'Student Id is not valid.' ), '' ) );
//		}
//
//		return $boolIsValid;
//	}

	public function valTaxNumberEncrypted( $strSectionName = NULL, $boolRequired = false ) {
		return $this->m_objApplicant->valTaxNumberEncrypted( $strSectionName, $boolRequired, $this->getAdo()->m_arrobjPropertyApplicationPreferences, $this->getAdo()->m_objDatabase );
	}

	public function validateMatchOtherApplicants( $intApplicationId, $strSSNSectionName, $objDatabase ) {
		$intExistingApplicantCount = 0;
		if( false == is_null( $this->m_objApplicant->getUserName() ) && 0 < strlen( trim( $this->m_objApplicant->getUserName() ) ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByUsernameByCid( $intApplicationId, $this->m_objApplicant->getUserName(), $this->m_objApplicant->getCid(), $objDatabase, $this->m_objApplicant->getId(), $boolIncludeDeletedAA = true );

			if( 0 < $intExistingApplicantCount ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant with the same email address already exists on the current application.' ) ) );
				return false;
			}
		}

		if( false == is_null( $this->m_objApplicant->getTaxNumber() ) && 0 < strlen( trim( $this->m_objApplicant->getTaxNumber() ) ) ) {
			$boolIsTestClient = CClients::fetchIsTestClientsByClientIdByCompanyStatusTypeIds( $this->m_objApplicant->getCid(), [ CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::SALES_DEMO ], $objDatabase );

			if( false == $boolIsTestClient ) {
				$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByTaxNumberByCid( $intApplicationId, $this->m_objApplicant->getTaxNumber(), $this->m_objApplicant->getCid(), $objDatabase, $this->m_objApplicant->getId(), $boolIncludeDeletedAA = true, $arrintExcludeOccupantTypes = [ CCustomerType::NOT_RESPONSIBLE ] );

				if( 0 < $intExistingApplicantCount ) {
					$this->m_objApplicant->setTaxNumberMasked( NULL );
					$this->m_objApplicant->setTaxNumberEncrypted( NULL );
					$this->m_objApplicant->setTaxNumber( NULL );
					$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same {%s,0}.', [ $strSSNSectionName ] ) ) );
					return false;
				}
			}
		}

		if( false == is_null( $this->m_objApplicant->getBirthDate() ) && 0 < strlen( trim( $this->m_objApplicant->getBirthDate() ) ) && 1 == CValidation::checkDate( $this->m_objApplicant->getBirthDate() ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByBirthDateByCid( $intApplicationId, $this->m_objApplicant->getNameFirst(), $this->m_objApplicant->getNameLast(), $this->m_objApplicant->getBirthDate(), $this->m_objApplicant->getCid(), $objDatabase, $this->m_objApplicant->getId(), $boolIncludeDeletedAA = true );

			if( 0 < $intExistingApplicantCount ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same first name, last name and birth date.' ) ) );
				return false;
			}
		}

		if( false == is_null( $this->m_objApplicant->getPrimaryPhoneNumber() ) && 0 < strlen( trim( $this->m_objApplicant->getPrimaryPhoneNumber() ) ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByPhoneNumberByCid( $intApplicationId, $this->m_objApplicant->getNameFirst(), $this->m_objApplicant->getNameLast(), $this->m_objApplicant->getPrimaryPhoneNumber(), $this->m_objApplicant->getCid(), $objDatabase, $this->m_objApplicant->getId(), $boolIncludeDeletedAA = true );

			if( 0 < $intExistingApplicantCount ) {
				$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same first name, last name and phone number.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valNamePrefix() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApplicant->getNamePrefix() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_prefix', __( 'Title is required.' ), '', [ 'label' => __( 'Title' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valStudentBirthDate() {
		$boolIsValid = true;
		$strSectionName = 'birth_date';
		$intRequiredApplicantAge = 0;

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $this->getAdo()->m_arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( $this->m_objApplicant->getCustomerTypeId() == CCustomerType::PRIMARY || $this->m_objApplicant->getCustomerTypeId() == CCustomerType::GUARANTOR ) ) {
			$intRequiredApplicantAge = $this->getAdo()->m_arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		} elseif( $this->m_objApplicant->getCustomerTypeId() != CCustomerType::NOT_RESPONSIBLE ) {
			$intRequiredApplicantAge = 18;
		}

		if( true == is_null( $this->m_objApplicant->getBirthDate() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date is required.' ) ) );
		} elseif( 1 !== CValidation::checkDate( $this->m_objApplicant->getBirthDate() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->setBirthDate( NULL );
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date must be in mm/dd/yyyy format.' ) ) );
		} elseif( 0 != $intRequiredApplicantAge && false == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName,  __( 'Age should be above {%d,0} years.', [ $intRequiredApplicantAge ] ) ) );
		}

		return $boolIsValid;
	}

	public function getIsMinor( $intRequiredApplicantAge ) {
		$boolIsValid = false;

		$arrstrBirthDate	= explode( '/', $this->m_objApplicant->getBirthDate() );

		$strYear			= ( true == isset( $arrstrBirthDate[2] ) ? ( int ) date( 'Y' ) - ( int ) $arrstrBirthDate[2] : ( int ) date( 'Y' ) );
		$strMonth			= ( int ) date( 'm', time() ) - ( int ) date( 'm', strtotime( $this->m_objApplicant->getBirthDate() ) );
		$strDay			 = ( int ) date( 'd', time() ) - ( int ) date( 'd', strtotime( $this->m_objApplicant->getBirthDate() ) );

		if( $strMonth < 0 || ( $strMonth === 0 && ( int ) date( 'd', time() ) < ( int ) date( 'd', strtotime( $this->m_objApplicant->getBirthDate() ) ) ) ) {
			$strYear --;
		}

		if( $strYear < 18 && $strYear >= $intRequiredApplicantAge ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPreviousPostalCode( $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : 'Previous';

		if( 0 == strlen( $this->m_objApplicant->getPreviousPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '' ) );

		} elseif( false == CValidation::validatePostalCode( $this->m_objApplicant->getPreviousPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApplicant->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( 'Invalid {%s,0} zip/postal code.', [ $strSectionNameToDisplay ] ), '' ) );

		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}

?>
