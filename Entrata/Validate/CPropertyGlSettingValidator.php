<?php
class CPropertyGlSettingValidator {

	const VALIDATE_PERIOD_VALUES        = 'period';
	const VALIDATE_BULK_EDIT_CLOSINGS   = 'validate_bulk_edit_closings';

	const AR_AUTO_ADVANCE_POST_MONTH    = 'ar_auto_advance_post_month';
	const AP_AUTO_ADVANCE_POST_MONTH    = 'ap_auto_advance_post_month';
	const GL_AUTO_ADVANCE_POST_MONTH    = 'gl_auto_advance_post_month';

	const AR_ADVANCE_DAY                = 'ar_advance_day';
	const AP_ADVANCE_DAY                = 'ap_advance_day';
	const GL_ADVANCE_DAY                = 'gl_advance_day';

	protected $m_objPropertyGlSetting;

	public function __construct() {
		return;
	}

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	/**
	 * Validation Functions
	 */

	public function valRetainedEarningsGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getRetainedEarningsGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retained_earnings_gl_account_id', __( 'Retained earnings account is required.' ) ) );
		} elseif( true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) && false == array_key_exists( $this->m_objPropertyGlSetting->getRetainedEarningsGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retained_earnings_gl_account_id', __( 'Retained Earnings account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valApGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getApGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'AP account is required.' ) ) );
		} elseif( true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) && false == array_key_exists( $this->m_objPropertyGlSetting->getApGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'AP account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valInterCompanyArGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getInterCoArGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_gl_account_id', __( 'Inter company AR GL account is required.' ) ) );
		} elseif( true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) && false == array_key_exists( $this->m_objPropertyGlSetting->getInterCoArGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_gl_account_id', __( 'Inter company AR GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valInterCompanyApGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getInterCoApGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Inter company AP GL account is required.' ) ) );
		} elseif( true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) && false == array_key_exists( $this->m_objPropertyGlSetting->getInterCoApGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Inter company AP GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPendingReimbursementAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getPendingReimbursementsGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Pending reimbursement GL account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMarketRentGlAccountId() {

		$boolIsValid = true;

		// If 'Auto-post GPR Adjustment' button is 'No' then skip validation for this field.
		if( 0 == $this->m_objPropertyGlSetting->getAutoPostGprAdjustment() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objPropertyGlSetting->getMarketRentGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Market rent account is required.' ) ) );
		} elseif( true == is_numeric( $this->m_objPropertyGlSetting->getMarketRentGlAccountId() )
				  && true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() )
				  && false == array_key_exists( $this->m_objPropertyGlSetting->getMarketRentGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'market_rent_gl_account_id', __( 'Market rent ( Gross potential rent ) GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valGainToLeaseGlAccountId() {

		$boolIsValid = true;

		// If 'Auto-post GPR Adjustment' button is 'No' then skip validation for this field.
		if( 0 == $this->m_objPropertyGlSetting->getAutoPostGprAdjustment() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objPropertyGlSetting->getGainToLeaseGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Gain to lease account is required.' ) ) );
		} elseif( true == is_numeric( $this->m_objPropertyGlSetting->getGainToLeaseGlAccountId() )
				  && true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() )
				  && false == array_key_exists( $this->m_objPropertyGlSetting->getGainToLeaseGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gain_to_lease_gl_account_id', __( 'Gain to lease GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valLossToLeaseGlAccountId() {

		$boolIsValid = true;

		// If 'Auto-post GPR Adjustment' button is 'No' then skip validation for this field.
		if( 0 == $this->m_objPropertyGlSetting->getAutoPostGprAdjustment() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objPropertyGlSetting->getLossToLeaseGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Loss to lease account is required.' ) ) );
		} elseif( true == is_numeric( $this->m_objPropertyGlSetting->getLossToLeaseGlAccountId() )
				  && true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() )
				  && false == array_key_exists( $this->m_objPropertyGlSetting->getLossToLeaseGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'loss_to_lease_gl_account_id', __( 'Loss to lease GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valVacancyLossGlAccountId() {

		$boolIsValid = true;

		// If 'Auto-post GPR Adjustment' button is 'No' then skip validation for this field.
		if( 0 == $this->m_objPropertyGlSetting->getAutoPostGprAdjustment() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objPropertyGlSetting->getVacancyLossGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Vacancy loss account is required.' ) ) );
		} elseif( true == is_numeric( $this->m_objPropertyGlSetting->getVacancyLossGlAccountId() )
				  && true == valArr( $this->m_objPropertyGlSetting->getAssociatedGlAccounts() )
				  && false == array_key_exists( $this->m_objPropertyGlSetting->getVacancyLossGlAccountId(), $this->m_objPropertyGlSetting->getAssociatedGlAccounts() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacancy_loss_gl_account_id', __( 'Vacancy loss GL account is disabled or not associated with {%s, 0} Property.', [ $this->m_objPropertyGlSetting->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valAccelRentArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getAccelRentArCodeId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'accel_rent_ar_code_id', __( 'Accelerated rent charge code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSkipAccelRentArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getSkipAccelRentArCodeId() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'skip_accel_rent_ar_code_id', __( 'Skip Accelerated rent charge code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRentArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getRentArCodeId() ) ) {
			$boolIsValid &= false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rent_ar_code_id', __( 'Rent Charge Code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDepositArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getDepositArCodeId() ) ) {
			$boolIsValid &= false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_rent_ar_code_id', __( 'Security Deposit Charge Code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArPostMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getArPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_post_month', __( 'AR post month is required.' ) ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getArPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_post_month', __( 'Invalid date format for AR post month.' ) ) );
		} elseif( strtotime( $this->m_objPropertyGlSetting->getArLockMonth() ) >= strtotime( $this->m_objPropertyGlSetting->getArPostMonth() )
				  || strtotime( $this->m_objPropertyGlSetting->getGlLockMonth() ) >= strtotime( $this->m_objPropertyGlSetting->getArPostMonth() ) ) {
			$boolIsValid     = false;
			$strPropertyName = '';

			if( true == valStr( $this->m_objPropertyGlSetting->getPropertyName() ) ) {
				$strPropertyName = ' ' . __( 'of {%s, 0}', [ $this->m_objPropertyGlSetting->getPropertyName() ] );
			}
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_post_month', __( 'AR post month must be greater than the AR lock month and GL lock month{%s, 0}.', [ $strPropertyName ] ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getArPostMonth() );
		}

		return $boolIsValid;
	}

	public function valApPostMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getApPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_post_month', __( 'AP post month is required.' ) ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getApPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_post_month', __( 'Invalid date format for AP post month.' ) ) );
		} elseif( strtotime( $this->m_objPropertyGlSetting->getApLockMonth() ) >= strtotime( $this->m_objPropertyGlSetting->getApPostMonth() )
				  || strtotime( $this->m_objPropertyGlSetting->getGlLockMonth() ) >= strtotime( $this->m_objPropertyGlSetting->getApPostMonth() ) ) {
			$boolIsValid     = false;
			$strPropertyName = '';

			if( true == valStr( $this->m_objPropertyGlSetting->getPropertyName() ) ) {
				$strPropertyName = ' ' . __( 'of {%s, 0}', [ $this->m_objPropertyGlSetting->getPropertyName() ] );
			}
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_post_month', __( 'AP post month must be greater than AP lock month and GL lock month{%s, 0}.', [ $strPropertyName ] ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getApPostMonth() );
		}

		return $boolIsValid;
	}

	public function valGlPostMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getGlPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_post_month', __( 'GL post month is required.' ) ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getGlPostMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_post_month', __( 'Invalid date format for GL post month.' ) ) );
		} elseif( strtotime( $this->m_objPropertyGlSetting->getGlLockMonth() ) >= strtotime( $this->m_objPropertyGlSetting->getGlPostMonth() ) ) {
			$boolIsValid     = false;
			$strPropertyName = '';

			if( true == valStr( $this->m_objPropertyGlSetting->getPropertyName() ) ) {
				$strPropertyName = ' ' . __( 'of {%s, 0}', [ $this->m_objPropertyGlSetting->getPropertyName() ] );
			}
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_post_month', __( 'GL post month must be greater than the GL lock month{%s, 0}.', [ $strPropertyName ] ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getGlPostMonth() );
		}

		return $boolIsValid;
	}

	public function valActivateStandardPosting( $objClientDatabase ) {
		$boolIsValid = true;

		$objProperty = $this->m_objPropertyGlSetting->fetchProperty( $objClientDatabase );

		if( true == is_numeric( $this->m_objPropertyGlSetting->getId() ) ) {
			$objOldPropertyGlSetting = CPropertyGlSettings::fetchPropertyGlSettingByIdByCid( $this->m_objPropertyGlSetting->getId(), $this->m_objPropertyGlSetting->getCid(), $objClientDatabase );
		}

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			trigger_error( __( 'Property failed to load.' ), E_USER_ERROR );
		}

		if( true == $objProperty->isIntegrated( $objClientDatabase ) && true == $this->m_objPropertyGlSetting->getActivateStandardPosting() ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_standard_posting', __( 'Entrata cannot be enabled on an integrated property. Please contact your client success manager to disable integration on this property.' ) ) );
		}

		if( false == $objProperty->goLiveWithEntrata( $objClientDatabase ) && ( ( true == valObj( $objOldPropertyGlSetting, 'CPropertyGlSetting' ) && $objOldPropertyGlSetting->getActivateStandardPosting() != $this->m_objPropertyGlSetting->getActivateStandardPosting() ) || ( false == valObj( $objOldPropertyGlSetting, 'CPropertyGlSetting' ) && true == $this->m_objPropertyGlSetting->getActivateStandardPosting() ) ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_standard_posting', __( 'Unable to activate Entrata. There are transactions, invoices, or deposits in the system which need to be corrected.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDate( $strDate ) {
		$boolIsValid = true;

		if( true == is_null( $strDate ) ) {
			$boolIsValid = false;
		} elseif( 0 == preg_match( '/^\d{1,2}\/\d{1,2}\/\d{4}$/', trim( $strDate ) ) ) {
			$boolIsValid = false;
		} else {
			$arrstrMonthDayYear = explode( '/', $strDate );
			if( true == valArr( $arrstrMonthDayYear ) && 12 < $arrstrMonthDayYear[0] ) {
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valArAdvanceDay() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objPropertyGlSetting->getArAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_advance_day', __( 'AR advance day is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApAdvanceDay() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objPropertyGlSetting->getApAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_advance_day', __( 'AP advance day is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlAdvanceDay() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objPropertyGlSetting->getGLAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL advance day is required.' ) ) );
		}

		if( $this->m_objPropertyGlSetting->getGLAdvanceDay() < $this->m_objPropertyGlSetting->getArAdvanceDay() ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL advance day should not be less than AR advance day.' ) ) );
		}

		if( $this->m_objPropertyGlSetting->getGLAdvanceDay() < $this->m_objPropertyGlSetting->getApAdvanceDay() ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL advance day should not be less than AP advance day.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlLockMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getGlLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_lock_month', 'GL lock month is required.' ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getGlLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_lock_month', __( 'Invalid date format for GL lock month.' ) ) );
		} elseif( true == $this->valDate( $this->m_objPropertyGlSetting->getApLockMonth() ) && true == $this->valDate( $this->m_objPropertyGlSetting->getArLockMonth() )
				  && strtotime( $this->m_objPropertyGlSetting->getGlLockMonth() ) > strtotime( $this->m_objPropertyGlSetting->getApLockMonth() )
				  || strtotime( $this->m_objPropertyGlSetting->getGlLockMonth() ) > strtotime( $this->m_objPropertyGlSetting->getArLockMonth() ) ) {
			$boolIsValid     = false;
			$strPropertyName = '';

			if( true == valStr( $this->m_objPropertyGlSetting->getPropertyName() ) ) {
				$strPropertyName = ' ' . __( 'of {%s, 0}', [ $this->m_objPropertyGlSetting->getPropertyName() ] );
			}
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_lock_month', __( 'The GL lock month must be less than or equal to the AP lock month and AR lock month{%s, 0}.', [ $strPropertyName ] ) ) );
		} elseif( $this->isFutureLockMonth( $this->m_objPropertyGlSetting->getGlLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_lock_month', __( 'GL lock month cannot have an end date in the future.' ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getGlLockMonth() );
		}

		return $boolIsValid;
	}

	public function valApLockMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getApLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_lock_month', __( 'AP lock month is required.' ) ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getApLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_lock_month', __( 'Invalid date format for the AP lock month.' ) ) );
		} elseif( $this->isFutureLockMonth( $this->m_objPropertyGlSetting->getApLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_lock_month', __( 'AP lock month cannot have an end date in the future.' ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getApLockMonth() );
		}

		return $boolIsValid;
	}

	public function valArLockMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getArLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_lock_month', __( 'AR lock month is required.' ) ) );
		} elseif( false == $this->valDate( $this->m_objPropertyGlSetting->getArLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_lock_month', __( 'Invalid date format for AR lock month.' ) ) );
		} elseif( $this->isFutureLockMonth( $this->m_objPropertyGlSetting->getArLockMonth() ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_lock_month', __( 'AR lock month cannot have an end date in the future.' ) ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valDateRange( $this->m_objPropertyGlSetting->getArLockMonth() );
		}

		return $boolIsValid;
	}

	public function valDelinquentRentGlAccountId() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getDelinquentRentGlAccountId() ) && ( CPropertyGlSetting::GPR_BOOK_BASIS_ACCRUAL != $this->m_objPropertyGlSetting->getGprBookBasis() || CPropertyGlSetting::IS_MODIFIED_ACCRUAL_GPR_NO != $this->m_objPropertyGlSetting->getIsModifiedAccrualGpr() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delinquent_rent_gl_account_id', __( 'Delinquent rent account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrepaidRentGlAccountId() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objPropertyGlSetting->getPrepaidRentGlAccountId() ) && CPropertyGlSetting::IS_MODIFIED_ACCRUAL_GPR_NO != $this->m_objPropertyGlSetting->getIsModifiedAccrualGpr() ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'prepaid_rent_gl_account_id', __( 'Prepaid rent account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlAutoAdvancePostMonth() {

		$boolIsValid = true;

		if( 1 == $this->m_objPropertyGlSetting->getGlAutoAdvancePostMonth() && ( 0 == $this->m_objPropertyGlSetting->getApAutoAdvancePostMonth() || 0 == $this->m_objPropertyGlSetting->getArAutoAdvancePostMonth() ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_auto_advance_post_month', __( 'To enable GL auto update post month, please enable AR & AP auto update post month as well.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAutoAdvancePostMonth() {

		$boolIsValid    = true;
		$objCurrentDate = new DateTime();
		$strCurrentDate = preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/1/$3', $objCurrentDate->format( 'm/d/Y' ) );

		if( 1 == $this->m_objPropertyGlSetting->getArAutoAdvancePostMonth() && strtotime( $strCurrentDate ) > strtotime( preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/1/$3', $this->m_objPropertyGlSetting->getArPostMonth() ) ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_auto_advance_post_month', __( 'The post month must be advanced manually before auto-update can be turned on. The current AR post month for this property is less than the current calendar month. Please advance to the current calendar month then turn auto-update on.' ) ) );
		}

		if( 1 == $this->m_objPropertyGlSetting->getApAutoAdvancePostMonth() && strtotime( $strCurrentDate ) > strtotime( preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/1/$3', $this->m_objPropertyGlSetting->getApPostMonth() ) ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_auto_advance_post_month', __( 'The post month must be advanced manually before auto-update can be turned on. The current AP post month for this property is less than the current calendar month. Please advance to the current calendar month then turn auto-update on.' ) ) );
		}

		if( 1 == $this->m_objPropertyGlSetting->getGlAutoAdvancePostMonth() && strtotime( $strCurrentDate ) > strtotime( preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/1/$3', $this->m_objPropertyGlSetting->getGlPostMonth() ) ) ) {

			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_auto_advance_post_month', __( 'The post month must be advanced manually before auto-update can be turned on. The current GL post month for this property is less than the current calendar month. Please advance to the current calendar month then turn auto-update on.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDateRange( $strDate ) {

		$boolIsValid  = true;
		$objDate      = new DateTime( $strDate );
		$objBeginDate = new DateTime( '01/01/1970' );
		$objEndDate   = new DateTime( '12/01/2099' );

		if( ( $objDate->format( 'Y-m-d' ) < $objBeginDate->format( 'Y-m-d' ) ) || ( $objDate->format( 'Y-m-d' ) > $objEndDate->format( 'Y-m-d' ) ) ) {
			$boolIsValid = false;
			$this->m_objPropertyGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Entrata does not support transactions outside the date year range of {%d, 0} to {%d, 1}.', [ 1969, 2099 ] ) ) );
		}

		return $boolIsValid;
	}

	private function isFutureLockMonth( string $strLockMonth ) : bool {
		return ( strtotime( $strLockMonth ) > strtotime( date( 'm/d/Y' ) ) );
	}

	public function validate( $strAction, $objClientDatabase, $objOldPropertyGlSetting = NULL, $boolIsFromClosings = false, $boolIsSettingsTemplateProperty = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRetainedEarningsGlAccountId();
				$boolIsValid &= $this->valApGlAccountId();
				$boolIsValid &= $this->valMarketRentGlAccountId();
				$boolIsValid &= $this->valGainToLeaseGlAccountId();
				$boolIsValid &= $this->valLossToLeaseGlAccountId();
				$boolIsValid &= $this->valVacancyLossGlAccountId();
				$boolIsValid &= $this->valAccelRentArCodeId();
				$boolIsValid &= $this->valSkipAccelRentArCodeId();
				$boolIsValid &= $this->valActivateStandardPosting( $objClientDatabase );
				$boolIsValid &= $this->valArAdvanceDay();
				$boolIsValid &= $this->valApAdvanceDay();
				$boolIsValid &= $this->valGlAdvanceDay();
				$boolIsValid &= $this->valRentArCodeId();
				$boolIsValid &= $this->valGlAutoAdvancePostMonth();
				if( true == $boolIsFromClosings && false == $boolIsSettingsTemplateProperty ) {
					$boolIsValid &= $this->valAutoAdvancePostMonth();
				}

			case self::VALIDATE_PERIOD_VALUES:
				$boolIsValid &= $this->valArLockMonth();
				$boolIsValid &= $this->valApLockMonth();
				$boolIsValid &= $this->valGlLockMonth();
				$boolIsValid &= $this->valArPostMonth();
				$boolIsValid &= $this->valApPostMonth();
				$boolIsValid &= $this->valGlPostMonth();
				break;

			case self::VALIDATE_BULK_EDIT_CLOSINGS:
				$boolIsValid &= $this->valArAdvanceDay();
				$boolIsValid &= $this->valApAdvanceDay();
				$boolIsValid &= $this->valGlAdvanceDay();
				$boolIsValid &= $this->valGlAutoAdvancePostMonth();
				$boolIsValid &= $this->valAutoAdvancePostMonth();
				break;

			case self::AR_AUTO_ADVANCE_POST_MONTH:
			case self::AP_AUTO_ADVANCE_POST_MONTH:
			case self::GL_AUTO_ADVANCE_POST_MONTH:
				$boolIsValid &= $this->valGlAutoAdvancePostMonth();
				$boolIsValid &= $this->valAutoAdvancePostMonth();
				break;

			case self::AR_ADVANCE_DAY:
			case self::AP_ADVANCE_DAY:
			case self::GL_ADVANCE_DAY:
				$boolIsValid &= $this->valArAdvanceDay();
				$boolIsValid &= $this->valApAdvanceDay();
				$boolIsValid &= $this->valGlAdvanceDay();
				break;

			case 'active_gl_posting':
				$boolIsValid &= $this->valActivateStandardPosting( $objClientDatabase );
				break;

			case 'general_settings':
				break;

			case 'accounts_receivable_settings':
				$boolIsValid &= $this->valRetainedEarningsGlAccountId();
				$boolIsValid &= $this->valAccelRentArCodeId();
				break;

			case 'accounts_payable_settings':
				$boolIsValid &= $this->valApGlAccountId();
				break;

			case 'pending_reimbursements_settings':
				$boolIsValid &= $this->valInterCompanyApGlAccountId();
				$boolIsValid &= $this->valInterCompanyArGlAccountId();
				$boolIsValid &= $this->valPendingReimbursementAccountId();
				break;

			case 'gpr_settings':
				$boolIsValid &= $this->valMarketRentGlAccountId();
				$boolIsValid &= $this->valGainToLeaseGlAccountId();
				$boolIsValid &= $this->valLossToLeaseGlAccountId();
				$boolIsValid &= $this->valVacancyLossGlAccountId();
				$boolIsValid &= $this->valDelinquentRentGlAccountId();
				$boolIsValid &= $this->valPrepaidRentGlAccountId();
				break;

			case 'bank_account_settings':
			case VALIDATE_DELETE:
				break;

			case 'validate_additional_fee':
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>