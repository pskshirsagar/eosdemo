<?php

class CGlSettingValidator {

	protected $m_objGlSetting;

	public function __construct() {
		return;
	}

	public function setGlSetting( $objGlSetting ) {
		$this->m_objGlSetting = $objGlSetting;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRetainedEarningsGlAccountId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getRetainedEarningsGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retained_earnings_gl_account_id', __( 'Retained earnings account is required.' ) ) );
		}

		$arrobjGlAccountProperties = ( array ) CGlAccountProperties::fetchSimpleGlAccountPropertiesByGlAccountIdByCid( $this->m_objGlSetting->getRetainedEarningsGlAccountId(), $this->m_objGlSetting->getCid(), $objDatabase );

		foreach( $arrobjGlAccountProperties as $objGlAccountProperty ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retained_earnings_gl_account_id', __( 'Retained Earnings Account : GL account "{%d, 0} : {%s, 1}" is already associated with property(s).', [ $objGlAccountProperty->getGlAccountNumber(), $objGlAccountProperty->getGlAccountName() ] ) ) );
			break;
		}

		return $boolIsValid;
	}

	public function valApGlAccountId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getApGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'A/P account is required.' ) ) );
		}

		$arrobjGlAccountProperties = ( array ) CGlAccountProperties::fetchSimpleGlAccountPropertiesByGlAccountIdByCid( $this->m_objGlSetting->getApGlAccountId(), $this->m_objGlSetting->getCid(), $objDatabase );

		foreach( $arrobjGlAccountProperties as $objGlAccountProperty ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'A/P Account : GL account {%d, 0} : {%s, 1} is already associated with property(s).', [ $objGlAccountProperty->getGlAccountNumber(), $objGlAccountProperty->getGlAccountName() ] ) ) );
			break;
		}

		return $boolIsValid;
	}

	public function valMarketRentGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getMarketRentGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Market rent account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGainToLeaseGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getGainToLeaseGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Gain to lease account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLossToLeaseGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getLossToLeaseGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Loss to lease account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valVacancyLossGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getVacancyLossGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_gl_account_id', __( 'Vacancy loss account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultArCodeGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getDefaultArCodeGlAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_ar_code_gl_account_id', __( 'Default A/R code account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsCashBasis() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objGlSetting->getIsCashBasis() ) || 0 == $this->m_objGlSetting->getIsCashBasis() ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_cash_basis', __( 'Cash basis required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArAdvanceDay() {

		$boolIsValid = true;

		if( false == is_numeric( $this->m_objGlSetting->getArAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_advance_day', __( 'AR advance day is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApAdvanceDay() {

		$boolIsValid = true;

		if( false == is_numeric( $this->m_objGlSetting->getApAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_advance_day', __( 'AP advance day is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlAdvanceDay() {

		$boolIsValid = true;

		if( false == is_numeric( $this->m_objGlSetting->getGLAdvanceDay() ) ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL advance is required.' ) ) );
		}

		if( $this->m_objGlSetting->getGlAdvanceDay() < $this->m_objGlSetting->getArAdvanceDay() ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL post month closing day should not be less than AR post month closing day.' ) ) );
		}

		if( $this->m_objGlSetting->getGlAdvanceDay() < $this->m_objGlSetting->getApAdvanceDay() ) {
			$boolIsValid = false;
			$this->m_objGlSetting->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL post month closing day should not be less than AP post month closing day.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsValidManagementFeeAccount = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApGlAccountId( $objDatabase );
				$boolIsValid &= $this->valMarketRentGlAccountId();
				$boolIsValid &= $this->valGainToLeaseGlAccountId();
				$boolIsValid &= $this->valLossToLeaseGlAccountId();
				$boolIsValid &= $this->valVacancyLossGlAccountId();
				$boolIsValid &= $this->valRetainedEarningsGlAccountId( $objDatabase );
				$boolIsValid &= $this->valArAdvanceDay();
				$boolIsValid &= $this->valApAdvanceDay();
				$boolIsValid &= $this->valGlAdvanceDay();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>