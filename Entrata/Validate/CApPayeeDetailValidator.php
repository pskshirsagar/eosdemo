<?php
class CApPayeeDetailValidator {

	protected $m_objApPayeeDetail;

	public function __construct() {
		return;
	}

	public function setApPayeeDetail( $objApPayeeDetail ) {
		$this->m_objApPayeeDetail = $objApPayeeDetail;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentType() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getApPaymentTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payment_type_id', 'Payment type is required .' ) );
		}

		return $boolIsValid;
	}

	public function valECheckInormation( $objPaymentDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getCheckAccountTypeId() )
			|| true == is_null( $this->m_objApPayeeDetail->getCheckBankName() )
			|| true == is_null( $this->m_objApPayeeDetail->getCheckNameOnAccount() )
			|| true == is_null( $this->m_objApPayeeDetail->getCheckRoutingNumber() )
			|| true == is_null( $this->m_objApPayeeDetail->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payment_type_id', 'Please fill out all fields in the \'Electronic Payment (eCheck) Information\' section below.' ) );
		} else {
			$boolIsValid = $this->valCheckRoutingNumber( $objPaymentDatabase );
		}

		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'eCheck Info:- Account type is required .' ) );
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccountOld() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'eCheck Info:- Name on account is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeDetail->getCheckNameOnAccount() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeDetail->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'eCheck Info:- Name on account has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'Name on account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankNameOld() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckBankName() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'eCheck Info:- Bank name is required.' ) );
		}

		if( true == \Psi\CStringService::singleton()->stristr( $this->m_objApPayeeDetail->getCheckBankName(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApPayeeDetail->getCheckBankName(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'eCheck Info:- Bank name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valPayeeNameOld() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getPayeeName() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payee_name', 'General Info:- Name is required.' ) );
		}

		if( true == \Psi\CStringService::singleton()->stristr( $this->m_objApPayeeDetail->getPayeeName(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_objApPayeeDetail->getPayeeName(), 'Content-type' ) ) {
			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payee_name', 'General Info:- Name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valPayeeName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getPayeeName() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payee_name', 'Name on tax return is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckBankName() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'eCheck Info:- Bank name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumberOld( $objPaymentDatabase ) {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number is required.' ) );
		}

		if( false != is_null( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			return true;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number must be a number.' ) );
			return false;
		}

		if( 9 != strlen( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number must be 9 digits long.' ) );
			return false;
		}

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->m_objApPayeeDetail->getCheckRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number is not valid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objPaymentDatabase ) {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number is required.' ) );
		}

		if( false != is_null( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			return true;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number must be a number.' ) );
			return false;
		}

		if( 9 != strlen( $this->m_objApPayeeDetail->getCheckRoutingNumber() ) ) {
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number must be 9 digits long.' ) );
			return false;
		}

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->m_objApPayeeDetail->getCheckRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'eCheck Info:- Routing number is not valid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;

		if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() && true == is_null( $this->m_objApPayeeDetail->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'eCheck Info:- Check Account number is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeDetail->getCheckAccountNumberEncrypted() ) && false == preg_match( '/^[0-9a-zA-Z]+$/', $this->m_objApPayeeDetail->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', ' Check Account number has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valUsername( $objApPayee, $objClientDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getUsername() ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Portal Settings:- Password is required.' ) );
		}

		if( CApPayeeType::OWNER == $objApPayee->getApPayeeTypeId() ) {

			$intDuplicateUsernameCount = ( int ) CApPayeeDetails::fetchDuplicateUsernameCountByUsernameByCid( $this->m_objApPayeeDetail->getUsername(), $this->m_objApPayeeDetail->getCid(), $objClientDatabase, $this->m_objApPayeeDetail->getId() );

			if( 0 < $intDuplicateUsernameCount ) {
				$boolIsValid = false;
				$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Portal Settings:- Username already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMainEmail( $objApPayee, $objClientDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getMainEmail() ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Portal Settings:- Password is required.' ) );
		}

		if( false == CValidation::validateEmailAddresses( $this->m_objApPayeeDetail->getMainEmail() ) ) {
			$boolIsValid &= false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'main_email', 'Portal Settings:- Email address is not a valid one.' ) );
		}

		if( CApPayeeType::OWNER == $objApPayee->getApPayeeTypeId() ) {

			$intDuplicateMainEmailCount = ( int ) CApPayeeDetails::fetchDuplicateMainEmailCountByMainEmailByCid( $this->m_objApPayeeDetail->getMainEmail(), $this->m_objApPayeeDetail->getCid(), $objClientDatabase, $this->m_objApPayeeDetail->getId() );

			if( 0 < $intDuplicateMainEmailCount ) {
				$boolIsValid = false;
				$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'main_email', 'Portal Settings:- Main email already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPasswordQuestion() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordQuestion() ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordAnswerEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_answer_encrypted', 'Portal Settings:- Password answer is required.' ) );
		}

		if( false == preg_match( '/^[0-9a-zA-Z]+$/', $this->m_objApPayeeDetail->getPasswordQuestion() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_question', 'Portal Settings:- Password question has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordAnswerEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordAnswerEncrypted() ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->m_objApPayeeDetail->getPasswordQuestion() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_question', 'Portal Settings:- Password question is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxpayerIdNumber( $objApPayee = NULL, $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( false == is_null( $objApPayee ) && false == is_null( $objApPayee->getRemotePrimaryKey() ) ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->m_objApPayeeDetail->getTaxpayerIdNumberEncrypted() ) && false == preg_match( '/^([\d]{2}-[\d]{7})$|([\d]{3}-[\d]{2}-[\d]{4})$/', $this->m_objApPayeeDetail->getDecryptedTaxpayerIdNumber() ) ) {

			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'Tax id number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) );
			return false;
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->m_objApPayeeDetail->getCid() . '
							AND taxpayer_id_number_encrypted LIKE \'' . $this->m_objApPayeeDetail->getTaxpayerIdNumberEncrypted() . '\'
							AND id != ' . ( int ) $this->m_objApPayeeDetail->getId();

		if( 1 <= CApPayeeDetails::fetchApPayeeDetailCount( $strWhereSql, $objClientDatabase ) ) {

			$boolIsValid &= false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'Tax ID number already exists for another vendor.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->m_strPhoneNumber ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		$intLength = strlen( str_replace( '-', '', $this->m_objApPayeeDetail->getPhoneNumber() ) );

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be between 10 and 15 characters.' ) );
		} elseif( false == ( CValidation::validateFullPhoneNumber( $this->m_objApPayeeDetail->getPhoneNumber(), false ) ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valOfficeFax() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->getOfficeFax() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( $this->m_objApPayeeDetail->getOfficeFax() );

		if( 0 < $intLength && ( 7 > $intLength || 15 < $intLength ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number must be between 7 and 15 characters.' ) );
		} elseif( false == preg_match( '/^[0-9]+$/', $this->m_objApPayeeDetail->getOfficeFax() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeDetail->getEmailAddress() ) ) {

			if( true == is_null( $this->m_objApPayeeDetail->getEmailAddress() ) || false == CValidation::validateEmailAddresses( $this->m_objApPayeeDetail->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Please enter a valid email address.' ) );
				return;
			}
		}

		return $boolIsValid;
	}

	public function valLoginEmailNotification() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objApPayeeDetail->getUsername() ) || true == is_null( $this->m_objApPayeeDetail->getMainEmail() ) || true == is_null( $this->m_objApPayeeDetail->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Username, main email & password must be associated with this owner to send an email notification.' ) );
			return;
		}

		return $boolIsValid;

	}

	public function valOwnerPotalWebsiteExists( $objClientDatabase ) {
		$boolIsValid = true;

		$objWebsite = \Psi\Eos\Entrata\CWebsites::createService()->fetchOwnerPortalWebsiteByCid( $this->m_objApPayeeDetail->getCid(), $objClientDatabase );

		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'You must first add an Owner Portal website to the company to send a username and password.' ) );
		}

		return $boolIsValid;
	}

	public function valWebsite() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objApPayeeDetail->getWebsite() ) && false == CValidation::checkUrl( $this->m_objApPayeeDetail->getWebsite() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Invalid website url format.' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->m_strCity ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeDetail->getCity() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeDetail->getCity() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objApPayeeDetail->m_strPostalCode ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Zip code is required.' ) );
		}

		if( false == is_null( $this->m_objApPayeeDetail->getPostalCode() ) && false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->m_objApPayeeDetail->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objApPayeeDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objApPayee, $objClientDatabase, $objPaymentDatabase, $boolSendEmailNotificationWithLoginInfo ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( true == $boolSendEmailNotificationWithLoginInfo ) {
					$boolIsValid &= $this->valLoginEmailNotification();
					$boolIsValid &= $this->valOwnerPotalWebsiteExists( $objClientDatabase );
				}
				$boolIsValid &= $this->valUsername( $objApPayee, $objClientDatabase );
				$boolIsValid &= $this->valMainEmail( $objApPayee, $objClientDatabase );
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valPayeeNameOld();
				$boolIsValid &= $this->valTaxpayerIdNumber( $objApPayee );
				$boolIsValid &= $this->valCheckAccountTypeId();
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valCheckNameOnAccountOld();
				$boolIsValid &= $this->valCheckRoutingNumberOld( $objPaymentDatabase );
				$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				break;

			case 'VALIDATE_SCRIPT_INSERT':
				$boolIsValid &= $this->valPasswordAnswerEncrypted();
				$boolIsValid &= $this->valPayeeNameOld();
				$boolIsValid &= $this->valTaxpayerIdNumber( $objApPayee );
				$boolIsValid &= $this->valCheckAccountTypeId();
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valCheckNameOnAccountOld();
				$boolIsValid &= $this->valCheckRoutingNumberOld( $objPaymentDatabase );
				break;

			case VALIDATE_UPDATE:
				if( true == $boolSendEmailNotificationWithLoginInfo ) {
					$boolIsValid &= $this->valLoginEmailNotification();
					$boolIsValid &= $this->valOwnerPotalWebsiteExists( $objClientDatabase );
				}
				$boolIsValid &= $this->valUsername( $objApPayee, $objClientDatabase );
				$boolIsValid &= $this->valPayeeNameOld();
				$boolIsValid &= $this->valTaxpayerIdNumber( $objApPayee );
				$boolIsValid &= $this->valCheckAccountTypeId();
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valCheckNameOnAccountOld();
				$boolIsValid &= $this->valCheckRoutingNumberOld( $objPaymentDatabase );

				if( '*******' !== $this->m_objApPayeeDetail->getPasswordEncrypted() ) {
					$boolIsValid &= $this->valPassword();
				}

				if( '*******' !== $this->m_objApPayeeDetail->getCheckAccountNumberEncrypted() ) {
					$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'vendor_insert':
			case 'update_business_info':
				$boolIsValid &= $this->valPayeeName();
				$boolIsValid &= $this->valTaxpayerIdNumber( $objApPayee, $objClientDatabase );
				if( CApPaymentType::ACH == $this->m_objApPayeeDetail->getApPaymentTypeId() ) {
					$boolIsValid &= $this->valECheckInormation( $objPaymentDatabase );
				}
				break;

			case 'validate_quick_view':
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valOfficeFax();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valWebsite();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode();
				break;

			case 'bulk_update_1099':
				$boolIsValid &= $this->valTaxpayerIdNumber( $objApPayee );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>