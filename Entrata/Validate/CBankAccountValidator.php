<?php

class CBankAccountValidator {

	protected $m_objBankAccount;

	public function __construct() {
		return;
	}

	public function setBankAccount( $objBankAccount ) {
		$this->m_objBankAccount = $objBankAccount;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId( $objClientDatabase ) {

		$boolIsValid = true;

		$intDisabledPropertyBankAccount = CPropertyBankAccounts::fetchCountDisabledPropertyBankAccountsByBankAccountIdByCid( $this->m_objBankAccount->getId(), $this->m_objBankAccount->getCid(), $objClientDatabase );

		if( 0 < $intDisabledPropertyBankAccount ) {
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Bank account can not be enable, it is being assigned with disabled gl account(s).' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBankLoginTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountName( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objBankAccount->getAccountName() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', __( 'Account Name/ID is required.' ) ) );
			return $boolIsValid;
		}

		if( true == is_null( $objClientDatabase ) ) {
			return $boolIsValid;
		}

		$strWhereSql = 'WHERE cid = ' . ( int ) $this->m_objBankAccount->getCid() . ' AND id <> ' . ( int ) $this->m_objBankAccount->getId() . ' AND LOWER( account_name ) = \'' . addslashes( mb_strtolower( trim( $this->m_objBankAccount->getAccountName() ) ) ) . '\'';

		if( 0 < CBankAccounts::fetchBankAccountCount( $strWhereSql, $objClientDatabase ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', __( 'Provided account name already exist.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objBankAccount->getCheckBankName() ) ) {

			$boolIsValid &= false;

			if( 1 == $this->m_objBankAccount->getIsCreditCardAccount() ) {
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', __( 'CC Company Name is required.' ) ) );
			} else {
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', __( 'Bank Name is required.' ) ) );
			}

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valBankAccountTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objBankAccount->getBankAccountTypeId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_type_id', __( 'Account type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBankCode() {
		$boolIsValid = true;

		if( CTransmissionVendor::PP_EXPORT_CITI_BANK == $this->m_objBankAccount->getTransmissionVendorId() && true == is_null( $this->m_objBankAccount->getBankCode() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_code', __( 'Bank code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNextCheckNumber() {
		$boolIsValid = true;

		if( 0 == $this->m_objBankAccount->getIsCreditCardAccount() && true == is_null( $this->m_objBankAccount->getNextCheckNumber() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_check_number', __( '\'Bank Account :{%s, 0}\' does not have check number.', [ $this->m_objBankAccount->getAccountName() ] ) ) );
		}

		if( false == is_null( $this->m_objBankAccount->getNextCheckNumber() ) && false == preg_match( '/^[0-9]*$/', $this->m_objBankAccount->getNextCheckNumber() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_check_number', __( 'Next check number should contain digits only.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCheckRoutingNumber() ) ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->m_objBankAccount->getCheckRoutingNumber() ) && 9 > mb_strlen( trim( $this->m_objBankAccount->getCheckRoutingNumber() ) ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number must contain {%d, 0} digits.', [ 9 ] ) ) );
		}

		if( false == is_null( $this->m_objBankAccount->getCheckRoutingNumber() ) && false == preg_match( '/^[0-9]{5}([0-9]|D){1}[0-9]{3}$/', $this->m_objBankAccount->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number should contain numbers only.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCheckAccountNumber() ) ) {
			return $boolIsValid;
		}

		if( 0 == $this->m_objBankAccount->getIsCreditCardAccount() && CBankAccount::CASH_ACCOUNT_NUMBER < mb_strlen( $this->m_objBankAccount->getCheckAccountNumber() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number should not contain more than {%d, 0} characters.', [ 20 ] ) ) );
		}

		if( true == is_null( $objClientDatabase ) ) {
			return $boolIsValid;
		}

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->m_objBankAccount->getCid() . '
							AND id <> ' . $this->m_objBankAccount->getId() . '
							AND check_account_number_encrypted = \'' . $this->m_objBankAccount->getCheckAccountNumberEncrypted() . '\'
							AND is_credit_card_account = ' . $this->m_objBankAccount->getIsCreditCardAccount();

		if( 0 < CBankAccounts::fetchBankAccountCount( $strWhereSql, $objClientDatabase ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Provided account number already exists.' ) ) );
			return $boolIsValid;
		}

		if( 0 == $this->m_objBankAccount->getIsCreditCardAccount() && false === mb_strpos( $this->m_objBankAccount->getCheckAccountNumber(), 'C' ) && false === mb_strpos( $this->m_objBankAccount->getCheckAccountNumber(), 'c' ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'MICR symbol \'C\' is required in account number.' ) ) );
		} elseif( 0 == $this->m_objBankAccount->getIsCreditCardAccount() && 2 < mb_substr_count( mb_strtolower( $this->m_objBankAccount->getCheckAccountNumber() ), 'c' ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number must contain at least one MICR symbol \'C\', but not more than two.' ) ) );
		}

		$this->m_objBankAccount->setCheckAccountNumber( rtrim( $this->m_objBankAccount->getCheckAccountNumber() ) );
		$strLastCharOfAccountNumber = mb_substr( $this->m_objBankAccount->getCheckAccountNumber(), -1 );

		if( 0 == $this->m_objBankAccount->getIsCreditCardAccount() && 0 == \Psi\CStringService::singleton()->strcasecmp( $strLastCharOfAccountNumber, 'C' ) ) {

			$strCheckAccountNumber = mb_substr( $this->m_objBankAccount->getCheckAccountNumber(), 0, -1 );

			if( false == is_null( $strCheckAccountNumber ) && false == preg_match( '/^[0-9 CDcd]*$/', rtrim( $strCheckAccountNumber ) ) ) {
				$boolIsValid &= false;
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number should contain only numbers, spaces and MICR symbols ( C / D ).' ) ) );
			}
		} else if( 0 == $this->m_objBankAccount->getIsCreditCardAccount() ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'The MICR symbol \'C\' must come at the end of the account number.' ) ) );
		}

		// validations for the credit card account
		if( 1 == $this->m_objBankAccount->getIsCreditCardAccount() && CBankAccount::CREDIT_CARD_ACCOUNT_NUMBER < mb_strlen( $this->m_objBankAccount->getCheckAccountNumber() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number should not contain more than {%d, 0} characters.', [ 16 ] ) ) );
		}

		if( 1 == $this->m_objBankAccount->getIsCreditCardAccount() && false == preg_match( '/^[0-9]+$/', trim( $this->m_objBankAccount->getCheckAccountNumber() ) ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number should contain only numbers.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckReorderReminderDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckReorderReminderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentCheckNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintSignatureLimitAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objBankAccount->getPrintSignatureLimitAmount() ) && 0 > $this->m_objBankAccount->getPrintSignatureLimitAmount() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'print_signature_limit_amount', __( 'Do not print signature field can not be negative.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSecondSignatureLimitAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->m_objBankAccount->getSecondSignatureLimitAmount() ) && 0 > $this->m_objBankAccount->getSecondSignatureLimitAmount() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'second_signature_limit_amount', __( 'Require 2nd signature field can not be negative.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLoginUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDaddyPropertyBankAccounts() {

		$boolIsValid 									= true;
		$arrmixArrangedBankAccountGlAccountProperties	= [];
		$arrmixArrangedCreditCardGlAccountProperties	= [];
		$arrmixArrangedInterCompanyGlAccountProperties	= [];

		$arrintPropertyIds								= ( array ) $this->m_objBankAccount->getPropertyIds();
		$arrobjDaddyPropertyBankAccounts				= ( array ) $this->m_objBankAccount->getDaddyPropertyBankAccounts();
		$arrobjGlAccountProperties						= ( array ) $this->m_objBankAccount->getGlAccountProperties();

		foreach( $arrobjGlAccountProperties as $arrmixArrangedGlAccountProperty ) {
			if( false == is_null( $arrmixArrangedGlAccountProperty['property_id'] ) ) {

				if( true == in_array( $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'], [ CGlAccountUsageType::REIMBURSEMENT_PAYABLE, CGlAccountUsageType::REIMBURSEMENT_RECEIVABLE ] ) ) {
					$arrmixArrangedInterCompanyGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;

				} elseif( CGlAccountUsageType::CREDIT_CARD == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
					$arrmixArrangedCreditCardGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;

				} elseif( CGlAccountUsageType::BANK_ACCOUNT == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
					$arrmixArrangedBankAccountGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;

				}
			} else {
				foreach( $arrintPropertyIds as $intPropertyId ) {

					if( true == in_array( $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'], [ CGlAccountUsageType::REIMBURSEMENT_PAYABLE, CGlAccountUsageType::REIMBURSEMENT_RECEIVABLE ] ) ) {
						$arrmixArrangedInterCompanyGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;
					} elseif( CGlAccountUsageType::CREDIT_CARD == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
						$arrmixArrangedCreditCardGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;
					} elseif( CGlAccountUsageType::BANK_ACCOUNT == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
						$arrmixArrangedBankAccountGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['ap_code_id']] = $arrmixArrangedGlAccountProperty;
					}
				}
			}
		}

		if( CBankAccountType::INTER_COMPANY == $this->m_objBankAccount->getBankAccountTypeId() ) {

			foreach( $arrobjDaddyPropertyBankAccounts as $objDaddyPropertyBankAccount ) {

				if( true == is_null( $objDaddyPropertyBankAccount->getPropertyId() ) ) {
					$boolIsValid &= false;
					$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Owning entity is required.' ) ) );
				}

				if( true == is_null( $objDaddyPropertyBankAccount->getBankApCodeId() ) ) {
					$boolIsValid &= false;
					$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Cash account is required.' ) ) );
				}

				if( false == is_null( $objDaddyPropertyBankAccount->getPropertyId() ) && false == is_null( $objDaddyPropertyBankAccount->getBankApCodeId() ) && false == $objDaddyPropertyBankAccount->getIsReimbursedProperty() ) {

					$boolIsCashAssociatedWithProperty 	= false;

					if( true == array_key_exists( $objDaddyPropertyBankAccount->getBankApCodeId(), $arrmixArrangedBankAccountGlAccountProperties[$objDaddyPropertyBankAccount->getPropertyId()] )
					    || true == array_key_exists( $objDaddyPropertyBankAccount->getBankApCodeId(), $arrmixArrangedCreditCardGlAccountProperties[$objDaddyPropertyBankAccount->getPropertyId()] ) ) {
						$boolIsCashAssociatedWithProperty = true;
					}

					if( false == $boolIsCashAssociatedWithProperty && false == is_null( $objDaddyPropertyBankAccount->getBankApCodeId() ) ) {
						$boolIsValid &= false;
						$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', __( 'Cash account is not associated with Property.' ) ) );
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function valPropertyBankAccounts( $objClientDatabase, $strAction = NULL ) {

		$boolIsValid									= true;
		$arrstrProperties								= ( array ) $this->m_objBankAccount->getProperties();
		$arrintPropertyIds								= ( array ) $this->m_objBankAccount->getPropertyIds();
		$arrobjPropertyBankAccounts						= ( array ) $this->m_objBankAccount->getPropertyBankAccounts();
		$arrobjInterCompanyGlAccounts					= ( array ) $this->m_objBankAccount->getInterCompanyGlAccounts();
		$arrobjBankAccountGlAccounts					= ( array ) $this->m_objBankAccount->getBankAccountGlAccounts();
		$arrobjDaddyPropertyBankAccounts				= ( array ) $this->m_objBankAccount->getDaddyPropertyBankAccounts();
		$arrobjGlAccountProperties						= ( array ) $this->m_objBankAccount->getGlAccountProperties();
		$arrmixArrangedBankAccountGlAccountProperties	= [];
		$arrmixArrangedCreditCardGlAccountProperties	= [];
		$arrmixArrangedInterCompanyGlAccountProperties	= [];

		foreach( $arrobjGlAccountProperties as $arrmixArrangedGlAccountProperty ) {
			if( false == is_null( $arrmixArrangedGlAccountProperty['property_id'] ) ) {

				if( true == in_array( $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'], [ CGlAccountUsageType::REIMBURSEMENT_PAYABLE, CGlAccountUsageType::REIMBURSEMENT_RECEIVABLE ] ) ) {
					$arrmixArrangedInterCompanyGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;

				} elseif( CGlAccountUsageType::CREDIT_CARD == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
					$arrmixArrangedCreditCardGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;

				} elseif( CGlAccountUsageType::BANK_ACCOUNT == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
					$arrmixArrangedBankAccountGlAccountProperties[$arrmixArrangedGlAccountProperty['property_id']][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;
				}

			} else {
				foreach( $arrintPropertyIds as $intPropertyId ) {
					if( true == in_array( $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'], [ CGlAccountUsageType::REIMBURSEMENT_PAYABLE, CGlAccountUsageType::REIMBURSEMENT_RECEIVABLE ] ) ) {
						$arrmixArrangedInterCompanyGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;
					} elseif( CGlAccountUsageType::CREDIT_CARD == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
						$arrmixArrangedCreditCardGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;
					} elseif( CGlAccountUsageType::BANK_ACCOUNT == $arrmixArrangedGlAccountProperty['gl_account_usage_type_id'] ) {
						$arrmixArrangedBankAccountGlAccountProperties[$intPropertyId][$arrmixArrangedGlAccountProperty['id']] = $arrmixArrangedGlAccountProperty;
					}
				}
			}
		}

		if( VALIDATE_INSERT == $strAction && false == valArr( $arrobjPropertyBankAccounts ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'At least one associated property and GL account is required.' ) ) ); // ??
		}

		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {

			// Check that is owning entity property is repeated or not.
			foreach( $arrobjDaddyPropertyBankAccounts as $objDaddyPropertyBankAccount ) {
				if( false == is_null( $objDaddyPropertyBankAccount->getPropertyId() ) && $objDaddyPropertyBankAccount->getPropertyId() == $objPropertyBankAccount->getPropertyId() ) {
					$boolIsValid &= false;
					$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Owning entity and child property cannot be same.' ) ) );
				}
			}

			if( false == is_null( $objPropertyBankAccount->getPropertyId() ) && false == is_null( $objPropertyBankAccount->getGlAccountId() ) ) {

				if( true == array_key_exists( $objPropertyBankAccount->getPropertyId(), $arrstrProperties ) && true == array_key_exists( 'property_name', $arrstrProperties[$objPropertyBankAccount->getPropertyId()] ) ) {
					$objPropertyBankAccount->setPropertyName( $arrstrProperties[$objPropertyBankAccount->getPropertyId()]['property_name'] );
				}
				if( true == array_key_exists( $objPropertyBankAccount->getPropertyId(), $arrmixArrangedBankAccountGlAccountProperties ) ) {
					$objPropertyBankAccount->setGlAccountProperties( $arrmixArrangedBankAccountGlAccountProperties[$objPropertyBankAccount->getPropertyId()] );
				}

				if( true == array_key_exists( $objPropertyBankAccount->getPropertyId(), $arrmixArrangedCreditCardGlAccountProperties ) ) {
					$arrobjAssociatedGlAccounts = rekeyArray( 'id', array_merge( $objPropertyBankAccount->getGlAccountProperties(), $arrmixArrangedCreditCardGlAccountProperties[$objPropertyBankAccount->getPropertyId()] ) );
					$objPropertyBankAccount->setGlAccountProperties( $arrobjAssociatedGlAccounts );
				}

				if( true == array_key_exists( $objPropertyBankAccount->getGlAccountId(), $arrobjBankAccountGlAccounts ) ) {
					$objPropertyBankAccount->setAccountName( $arrobjBankAccountGlAccounts[$objPropertyBankAccount->getGlAccountId()]->getAccountName() );
					$objPropertyBankAccount->setAccountNumber( $arrobjBankAccountGlAccounts[$objPropertyBankAccount->getGlAccountId()]->getAccountNumber() );
				}

				if( CBankAccountType::INTER_COMPANY == $this->m_objBankAccount->getBankAccountTypeId() ) {

					if( true == array_key_exists( $objPropertyBankAccount->getPropertyId(), $arrmixArrangedInterCompanyGlAccountProperties ) ) {
						$objPropertyBankAccount->setGlAccountProperties( $arrmixArrangedInterCompanyGlAccountProperties[$objPropertyBankAccount->getPropertyId()] );
					}

					if( true == array_key_exists( $objPropertyBankAccount->getGlAccountId(), $arrobjInterCompanyGlAccounts ) ) {
						$objPropertyBankAccount->setAccountName( $arrobjInterCompanyGlAccounts[$objPropertyBankAccount->getGlAccountId()]->getAccountName() );
						$objPropertyBankAccount->setAccountNumber( $arrobjInterCompanyGlAccounts[$objPropertyBankAccount->getGlAccountId()]->getAccountNumber() );
					}
				}
			}

			$objPropertyBankAccount->setBankAccountTypeId( $this->m_objBankAccount->getBankAccountTypeId() );

			$boolIsValid &= $objPropertyBankAccount->validate( VALIDATE_INSERT, $objClientDatabase );

			$this->m_objBankAccount->addErrorMsgs( $objPropertyBankAccount->getErrorMsgs() );
		}
		return $boolIsValid;
	}

	public function valExistingPropertyBankAccounts() {

		$boolIsValid						= true;
		$arrintExistingPropertyGlAccounts	= [];
		$arrobjExistingPropertyBankAccounts	= ( array ) $this->m_objBankAccount->getExistingPropertyBankAccounts();
		$arrstrProperties					= ( array ) $this->m_objBankAccount->getProperties();
		$arrobjDaddyPropertyBankAccounts	= ( array ) $this->m_objBankAccount->getDaddyPropertyBankAccounts();
		$arrobjPropertyBankAccounts			= ( array ) $this->m_objBankAccount->getPropertyBankAccounts();

		$arrobjInputPropertyBankAccounts    = ( CBankAccountType::INTER_COMPANY == $this->m_objBankAccount->getBankAccountTypeId() ) ? $arrobjDaddyPropertyBankAccounts : $arrobjPropertyBankAccounts;

		// Check for existance of combination of property id and GL account id in database.
		foreach( $arrobjExistingPropertyBankAccounts as $objExistingPropertyBankAccount ) {

            if( CBankAccountType::INTER_COMPANY == $objExistingPropertyBankAccount->getBankAccountTypeId() && false == $objExistingPropertyBankAccount->getIsReimbursedProperty() ) continue;

			foreach( $arrobjInputPropertyBankAccounts as $objInputPropertyBankAccount ) {
				if( CBankAccountType::INTER_COMPANY == $objInputPropertyBankAccount->getBankAccountTypeId() && false == $objInputPropertyBankAccount->getIsReimbursedProperty() ) continue;
				if( false == is_null( $objInputPropertyBankAccount->getPropertyId() ) && false == is_null( $objInputPropertyBankAccount->getBankApCodeId() )
					&& $objInputPropertyBankAccount->getPropertyId() == $objExistingPropertyBankAccount->getPropertyId() && $objInputPropertyBankAccount->getBankApCodeId() == $objExistingPropertyBankAccount->getBankApCodeId()
					&& $this->m_objBankAccount->getId() != $objExistingPropertyBankAccount->getBankAccountId() ) {

					$arrintExistingPropertyGlAccounts[] = $objExistingPropertyBankAccount->getPropertyId();
				}
			}
		}

		if( true == valArr( $arrintExistingPropertyGlAccounts ) ) {
			$arrstrTempProperties = [];

			foreach( $arrintExistingPropertyGlAccounts as $intPropertyId ) {
				if( true == valArr( $arrstrProperties ) && true == array_key_exists( $intPropertyId, $arrstrProperties ) ) {
					$arrstrTempProperties[$intPropertyId] = $arrstrProperties[$intPropertyId]['property_name'];
				}
			}

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Selected property and Gl account combination already exists for \'{%s, 0}\' property(s).', [ implode( ', ', $arrstrTempProperties ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valDuplicatePropertyIdsAndAccounts() {

		$boolIsValid						= true;
		$arrintDuplicatePropertyIds			= [];
		$arrintDuplicatePropertyGlAccounts	= [];
		$arrobjClonedPropertyBankAccounts	= ( array ) $this->m_objBankAccount->getPropertyBankAccounts();
		$arrstrProperties					= ( array ) $this->m_objBankAccount->getProperties();
		$arrobjPropertyBankAccounts			= ( array ) $this->m_objBankAccount->getPropertyBankAccounts();
		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {

			$intDuplicatePropertyCount = 0;
			$intPropertyGlAccountCount = 0;

			// Check for duplication of property and property-GL account combination in line items itself.
			foreach( $arrobjClonedPropertyBankAccounts as $objClonedPropertyBankAccount ) {

				if( CBankAccountType::SINGLE_PROPERTY == $objPropertyBankAccount->getBankAccountTypeId() || CBankAccountType::SINGLE_PROPERTY == $objClonedPropertyBankAccount->getBankAccountTypeId() ) {
					continue;
				}

				if( false == is_null( $objPropertyBankAccount->getPropertyId() )
					&& $objPropertyBankAccount->getPropertyId() == $objClonedPropertyBankAccount->getPropertyId()
					&& true == $objClonedPropertyBankAccount->getIsPrimary() ) {
						$intDuplicatePropertyCount++;
				}

				if( false == is_null( $objPropertyBankAccount->getPropertyId() )
					&& false == is_null( $objPropertyBankAccount->getGlAccountId() )
					&& $objPropertyBankAccount->getPropertyId() == $objClonedPropertyBankAccount->getPropertyId()
					&& $objPropertyBankAccount->getGlAccountId() == $objClonedPropertyBankAccount->getGlAccountId() ) {

					$intPropertyGlAccountCount++;
				}
			}

			if( 1 < $intDuplicatePropertyCount ) {
				$arrintDuplicatePropertyIds[] = $objPropertyBankAccount->getPropertyId();
			}

			if( 1 < $intPropertyGlAccountCount ) {
				$arrintDuplicatePropertyGlAccounts[] = $objPropertyBankAccount->getPropertyId();
			}
		}

		if( true == valArr( $arrintDuplicatePropertyIds ) ) {
			$arrstrTempProperties = [];

			foreach( $arrintDuplicatePropertyIds as $intPropertyId ) {
				if( true == valArr( $arrstrProperties ) && true == array_key_exists( $intPropertyId, $arrstrProperties ) ) {
					$arrstrTempProperties[$intPropertyId] = $arrstrProperties[$intPropertyId]['property_name'];
				}
			}

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '\'{%s, 0}\' property(s) are added more than once.', [ implode( ', ', $arrstrTempProperties ) ] ) ) );
		}

		if( true == valArr( $arrintDuplicatePropertyGlAccounts ) ) {
			$arrstrTempProperties = [];

			foreach( $arrintDuplicatePropertyGlAccounts as $intPropertyId ) {
				if( true == valArr( $arrstrProperties ) && true == array_key_exists( $intPropertyId, $arrstrProperties ) ) {
					$arrstrTempProperties[$intPropertyId] = $arrstrProperties[$intPropertyId]['property_name'];
				}
			}

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property and GL account combination is added more than once for \'{%s, 0}\' property(s).', [ implode( ', ', $arrstrTempProperties ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckPropertyInPostedUnpaidInvoice( $objClientDatabase = NULL ) {

		$boolIsValid						= true;
		$arrobjExistingPropertyBankAccounts	= ( array ) $this->m_objBankAccount->getExistingPropertyBankAccounts();
		$arrobjDaddyPropertyBankAccounts	= ( array ) $this->m_objBankAccount->getDaddyPropertyBankAccounts();
		$arrobjPropertyBankAccounts			= ( array ) $this->m_objBankAccount->getPropertyBankAccounts();
		$arrstrUnAssociatedPropertyNames	= ( array ) $this->m_objBankAccount->getUnAssociatedPropertyNames();

		$arrintUnAssociatedPropertyIds		= array_diff( array_keys( rekeyObjects( 'PropertyId', $arrobjExistingPropertyBankAccounts ) ), array_keys( rekeyObjects( 'PropertyId', array_merge( $arrobjDaddyPropertyBankAccounts, $arrobjPropertyBankAccounts ) ) ) );

		// If user remove the property(s) of intercompany bank account then check if the property(s) used in posted( unpaid ) invoices then show error
		if( CBankAccountType::INTER_COMPANY == $this->m_objBankAccount->getBankAccountTypeId() ) {

			$arrobjUnAssociatedApDeatils = ( array ) CApDetails::fetchUnpaidApDetailsByPropertyIdsByBankAccountIdByCid( $arrintUnAssociatedPropertyIds, $this->m_objBankAccount->getId(), $this->m_objBankAccount->getCid(), $objClientDatabase );

			if( true == valArr( $arrobjUnAssociatedApDeatils ) ) {

				$arrstrPropertyNames = [];

				foreach( $arrobjUnAssociatedApDeatils as $objUnAssociatedApDeatil ) {
					$arrstrPropertyNames[$objUnAssociatedApDeatil->getPropertyId()] = $arrstrUnAssociatedPropertyNames[$objUnAssociatedApDeatil->getPropertyId()]['property_name'];
				}

				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You may not remove the property(s) \'{%s, 0}\' this property(s) is attached to an unpaid invoice(s) using this bank account.', [ implode( ', ', $arrstrPropertyNames ) ] ) ) );
				$boolIsValid &= false;
			}
		}
		return $boolIsValid;
	}

	public function valIsPostDate( $objClientDatabase = NULL ) {

		$boolIsValid = true;

		$objGlReconciliation	= CGlReconciliations::fetchOpenGlReconciliationByBankAcccountIdByCid( $this->m_objBankAccount->getId(), $this->m_objBankAccount->getCid(), $objClientDatabase );
		$objBankAccount			= CBankAccounts::fetchBankAccountByIdByCid( $this->m_objBankAccount->getId(), $this->m_objBankAccount->getCid(), $objClientDatabase );

		// if there is any bank reconcilation is created for selected bank account then user can not change 'Base bank reconciliation on'
		if( 0 < \Psi\Libraries\UtilFunctions\count( $objGlReconciliation ) && $this->m_objBankAccount->getIsBankRecOnPostMonth() != $objBankAccount->getIsBankRecOnPostMonth() ) {

			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_post_date', __( 'Bank account is associated with open bank reconciliation(s); you cannot change the \'Base Bank Reconciliation On\' setting until all open bank reconciliations for selected bank account have been closed.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valReserveAmount() {
		$boolIsValid = true;

		if( 0 > $this->m_objBankAccount->getReserveAmount() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reserve_amount', __( 'Reserve amount can not be negative.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankNumber() {
		$boolIsValid = true;

		if( CTransmissionVendor::PP_EXPORT_WELLS_FARGO == $this->m_objBankAccount->getTransmissionVendorId() && 0 >= $this->m_objBankAccount->getCheckBankNumber() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_id', __( 'Bank ID should contain only integers.' ) ) );
		}

		if( CTransmissionVendor::PP_EXPORT_WELLS_FARGO == $this->m_objBankAccount->getTransmissionVendorId() && false != is_null( $this->m_objBankAccount->getCheckBankNumber() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_id', __( 'Bank ID can not be empty for Wells Fargo Export Type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valImmediateDestinationName() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getImmediateDestinationName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_destination_name', __( 'Immediate destination name is required.' ) ) );

		} elseif( false == preg_match( '/^[a-zA-Z0-9 ]+$/', $this->m_objBankAccount->getImmediateDestinationName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_destination_name', __( 'Immediate destination name can only include alpha-numeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valImmediateDestinationId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getImmediateDestinationId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_destination_id', __( 'Immediate destination id is required.' ) ) );

		} elseif( false == valId( $this->m_objBankAccount->getImmediateDestinationId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_destination_id', __( 'Immediate destination id can only include numeric.' ) ) );

		} elseif( 9 > strlen( $this->m_objBankAccount->getImmediateDestinationId() ) || 10 < strlen( $this->m_objBankAccount->getImmediateDestinationId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_destination_id', __( 'Immediate destination id should be between 9 to 10 digits.' ) ) );
		}

		return $boolIsValid;
	}

	public function valImmediateOriginId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getImmediateOriginId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_origin_id', __( 'Immediate origin id is required.' ) ) );

		} elseif( true == valStr( $this->m_objBankAccount->getImmediateOriginId() ) && false == preg_match( '/^[0-9]+$/', $this->m_objBankAccount->getImmediateOriginId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_origin_id', __( 'Immediate origin id can only include numeric.' ) ) );

		} elseif( 9 > strlen( $this->m_objBankAccount->getImmediateOriginId() ) || 10 < strlen( $this->m_objBankAccount->getImmediateOriginId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_origin_id', __( 'Immediate origin id should be between 9 to 10 digits.' ) ) );
		}

		return $boolIsValid;
	}

	public function valImmediateOriginName() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getImmediateOriginName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_origin_name', __( 'Immediate origin name is required.' ) ) );

		} elseif( false == preg_match( '/^[a-zA-Z0-9 ]+$/', $this->m_objBankAccount->getImmediateOriginName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'immediate_origin_name', __( 'Immediate origin name can only include alpha-numeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCompanyName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Company name is required.' ) ) );

		} elseif( false == preg_match( '/^[a-zA-Z0-9 ]+$/', $this->m_objBankAccount->getCompanyName() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', __( 'Company name can only include alpha-numeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCompanyId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', __( 'Company id is required.' ) ) );

		} elseif( 9 > strlen( $this->m_objBankAccount->getCompanyId() ) || 10 < strlen( $this->m_objBankAccount->getCompanyId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', __( 'Company id should be either 9 or 10 characters.' ) ) );

		} elseif( false == preg_match( '/^([[:alnum:]]){9,9}?$/', $this->m_objBankAccount->getCompanyId() )
		           && false == preg_match( '/^([[:alnum:]]){10,10}?$/', $this->m_objBankAccount->getCompanyId() ) ) {

			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', __( 'Company Id should contain only alphanumeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOriginatingDfi() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getOriginatingDfi() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_dfi', __( 'Originating dfi is required.' ) ) );

		} elseif( false == preg_match( '/^[a-zA-Z0-9 ]+$/', $this->m_objBankAccount->getOriginatingDfi() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_dfi', __( 'Originating dfi can only include alpha-numeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		$intLength = mb_strlen( str_replace( '-', '', $this->m_objBankAccount->getBankPostalCode() ) );

		if( false == is_null( $this->m_objBankAccount->getBankPostalCode() ) && 0 < $intLength && ( ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->m_objBankAccount->getBankPostalCode() ) && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->m_objBankAccount->getBankPostalCode() ) ) ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code must be {%d, 0} or {%d, 1} character in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.', [ 5, 10 ] ) ) );
		}
		return $boolIsValid;
	}

	public function valPayerPostalCode() {
		$boolIsValid = true;

		$intLength = mb_strlen( str_replace( '-', '', $this->m_objBankAccount->getPayerPostalCode() ) );

		if( false == is_null( $this->m_objBankAccount->getPayerPostalCode() ) && 0 < $intLength && ( ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->m_objBankAccount->getPayerPostalCode() ) && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->m_objBankAccount->getPayerPostalCode() ) ) ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Payer postal code must be {%d, 0} or {%d, 1} characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.', [ 5, 10 ] ) ) );
		}
		return $boolIsValid;
	}

	public function valPayerPhoneNumberEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objBankAccount->getPayerPhoneNumber() ) ) {
			return $boolIsValid;
		}

		$arrstrPhoneNumber = explode( ':', $this->m_objBankAccount->getPayerPhoneNumber() );

		$intPhoneNumberLength = mb_strlen( preg_replace( '/[^0-9]/', '', $arrstrPhoneNumber[0] ) );

		if( 0 < $intPhoneNumberLength && 10 > $intPhoneNumberLength ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payer_phone_number_encrypted', __( 'Phone number must be {%d, 0} digits.', [ 10 ] ) ) );
		} elseif( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $arrstrPhoneNumber[0] ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payer_phone_number_encrypted', __( 'Phone number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsAchEnable() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objBankAccount->getIsAchEnable() ) || 1 == $this->m_objBankAccount->getIsAchEnable() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_ach_enable', __( 'Bank account(s) {%s,0} must be enabled for ACH payments in order to create ACH payments. This can be configured at: Setup > Company > Financial > Bank Accounts > Edit Bank Account.', [ $this->m_objBankAccount->getCheckBankName() ] ) ) );
		}
		return $boolIsValid;
	}

	public function valBankAccountSignature() {
		$boolIsValid = true;

		$arrobjApPayments	= ( array ) $this->m_objBankAccount->getApPayments();
		$arrobjFiles		= rekeyObjects( 'CheckComponentTypeId', ( array ) $this->m_objBankAccount->getFiles() );

		foreach( $arrobjApPayments as $objApPayment ) {
			if( false == in_array( $objApPayment->getApPaymentTypeId(), [ CApPaymentType::BILL_PAY_CHECK, CApPaymentType::BILL_PAY_ACH, CApPaymentType::BILL_PAY ] ) ) {
				continue;
			}

			if( false == array_key_exists( CCheckComponentType::SIGNATURE_LINE_ONE, $arrobjFiles ) ) {
				$boolIsValid = false;

				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_component_type_id', __( 'Signature 1 is not present for bank account \'{%s,0}\'', [ $this->m_objBankAccount->getAccountName() ] ) ) );
			} elseif( false == array_key_exists( CCheckComponentType::SIGNATURE_LINE_TWO, $arrobjFiles ) && false == is_null( $this->m_objBankAccount->getSecondSignatureLimitAmount() ) && $this->m_objBankAccount->getSecondSignatureLimitAmount() < $objApPayment->getPaymentAmount() ) {
				$boolIsValid = false;
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'get_second_signature_limit_amount', __( 'Signature 2 is not present for bank account \'{%s,0}\'', [ $this->m_objBankAccount->getAccountName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valBankAccountSecondSignature( $arrmixCheckComponents = [], $arrobjApPayments = [] ) {
		$boolIsValid			= true;
		$boolIsSignatureLineTwo	= true;

		if( false == valArr( $arrmixCheckComponents ) ) {
			return $boolIsValid;
		}

		if( false == valArr( $arrobjApPayments ) ) {
			$arrobjApPayments	= $this->m_objBankAccount->getApPayments();
		}

		$arrintCheckComponentTypes	= array_keys( rekeyObjects( 'CheckComponentTypeId', $arrmixCheckComponents[$this->m_objBankAccount->getId()][$this->m_objBankAccount->getCheckTemplateId()] ) );

		if( false == in_array( CCheckComponentType::SIGNATURE_LINE_TWO, $arrintCheckComponentTypes ) ) {
			$boolIsSignatureLineTwo = false;
		}

		foreach( $arrobjApPayments as $objApPayment ) {
			if( CApPaymentType::CHECK != $objApPayment->getApPaymentTypeId() ) {
				continue;
			}

			if( false == $boolIsSignatureLineTwo && false == is_null( $this->m_objBankAccount->getSecondSignatureLimitAmount() ) && $this->m_objBankAccount->getSecondSignatureLimitAmount() <= $objApPayment->getPaymentAmount() ) {
				$boolIsValid = false;
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'get_signature_line_two', __( 'Check amount requires a 2nd signature. Please enable \'Signature Line 2\' in the check layout for bank account \'{%s,0}\' .', [ $this->m_objBankAccount->getAccountName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valOriginatingBankId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getOriginatingBankId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_bank_id', __( 'Originating Bank Id is required only when WFPM - ACH is enabled.' ) ) );
		} elseif( !preg_match( '/^[0-9]*$/', $this->m_objBankAccount->getOriginatingBankId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_bank_id', __( 'Originating Bank Id must contain numbers only.' ) ) );
		} elseif( 9 > strlen( $this->m_objBankAccount->getOriginatingBankId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_bank_id', ( 'Originating Bank Id must be a 9 digit Routing Number.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSenderId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getSenderId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sender_id', __( 'Sender Id is required.' ) ) );
		} elseif( false == ctype_alnum( $this->m_objBankAccount->getSenderId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sender_id', __( 'Sender Id must contain only alphanumeric characters.' ) ) );
		} elseif( 1 > strlen( $this->m_objBankAccount->getSenderId() ) || 32 < strlen( $this->m_objBankAccount->getSenderId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sender_id', ( 'Sender Id should be between 1 to 32 characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valClientId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getClientId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_id', __( 'Client Id is required.' ) ) );
		} elseif( !preg_match( '/^[0-9]*$/', $this->m_objBankAccount->getClientId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_id', __( 'Client id can only include numeric characters.' ) ) );
		} elseif( 1 > strlen( $this->m_objBankAccount->getClientId() ) || 16 < strlen( $this->m_objBankAccount->getClientId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_id', ( 'Client Id should be between 1 to 16 digits.' ) ) );
		}

		return $boolIsValid;
	}

	public function valWfpmCeoCompanyId() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getWfpmCeoCompanyId() ) ) {
			return $boolIsValid;
		}

		if( 15 < mb_strlen( $this->m_objBankAccount->getWfpmCeoCompanyId() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wfpm_ceo_company_id', __( 'WFPM CEO company Id should not contain more than {%d, 0} characters.', [ 15 ] ) ) );
		}

		if( false == ctype_alnum( $this->m_objBankAccount->getWfpmCeoCompanyId() ) ) {

			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wfpm_ceo_company_id', __( 'WFPM CEO company Id should contain only alphanumeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostedInvoicesGlAccounts( $arrobjExistingPropertyBankAccounts, $objClientDatabase ) {
		$boolIsValid						= true;
		$arrmixExistingPropertyBankAccounts	= extractObjectKeyValuePairs( $arrobjExistingPropertyBankAccounts, 'PropertyId', 'BankApCodeId' );
		$arrmixPropertyBankAccounts			= extractObjectKeyValuePairs( $this->m_objBankAccount->getPropertyBankAccounts(), 'PropertyId', 'BankApCodeId' );

		$arrmixUpdatedPropertyBankAccounts	= array_diff( $arrmixExistingPropertyBankAccounts, $arrmixPropertyBankAccounts );

		if( true == valIntArr( $arrmixUpdatedPropertyBankAccounts ) ) {
			$arrobjUpdatedPropertyBankAccounts = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByPropertyIdsByBankAccountIdByCid( array_keys( $arrmixUpdatedPropertyBankAccounts ), $this->m_objBankAccount->getId(), $this->m_objBankAccount->getCid(), $objClientDatabase );
			if( true == valArr( $arrobjUpdatedPropertyBankAccounts ) ) {
				$boolIsValid = false;
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'You cannot edit or remove the GL account for this bank account, as there are posted invoices that have not yet been paid. Please pay or unpost these invoices before proceeding.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valCcExpMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && false == valStr( $this->m_objBankAccount->getCcFirstName() ) && false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			return $boolIsValid;
		}

		if( true == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && ( 0 == ( int ) $this->m_objBankAccount->getCcExpDateMonth() || 12 < ( int ) $this->m_objBankAccount->getCcExpDateMonth() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_year', __( 'Expiration Month Invalid.' ) ) );
			return $boolIsValid;
		}

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_month', __( 'Expiration month required.' ) ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCcExpYear() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && false == valStr( $this->m_objBankAccount->getCcFirstName() ) && false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			return $boolIsValid;
		}

		if( true == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && 0 == ( int ) $this->m_objBankAccount->getCcExpDateYear() ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_year', __( 'Expiration Year Invalid.' ) ) );
			return $boolIsValid;
		}

		if( false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_year', __( 'Expiration Year required.' ) ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCcExpDate() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && false == valStr( $this->m_objBankAccount->getCcFirstName() ) && false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			return $boolIsValid;
		}

		if( ( true == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && ( 0 != ( int ) $this->m_objBankAccount->getCcExpDateMonth() && 12 >= ( int ) $this->m_objBankAccount->getCcExpDateMonth() ) ) && ( true == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && 0 != ( int ) $this->m_objBankAccount->getCcExpDateYear() ) ) {
			if( 2 <> strlen( trim( $this->m_objBankAccount->getCcExpDateMonth() ) ) || 4 <> strlen( trim( $this->m_objBankAccount->getCcExpDateYear() ) ) ) {
				$boolIsValid = false;
				$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date', __( 'Expiration date must be 2 digit month and 4 digit year.' ) ) );
			} else {
				$strExpirationDate = $this->m_objBankAccount->getCcExpDateMonth() . '/1/' . $this->m_objBankAccount->getCcExpDateYear();
				if( strtotime( date( 'm/1/Y' ) ) > strtotime( $strExpirationDate ) ) {
					$boolIsValid = false;
					$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date', __( 'Expiration date can not be in the past.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCcFirstName() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && false == valStr( $this->m_objBankAccount->getCcFirstName() ) && false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			return $boolIsValid;
		}

		if( false == valStr( $this->m_objBankAccount->getCcFirstName() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_first_name', __( 'First Name is required.' ) ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCcLastName() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) && false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) && false == valStr( $this->m_objBankAccount->getCcFirstName() ) && false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			return $boolIsValid;
		}

		if( false == valStr( $this->m_objBankAccount->getCcLastName() ) ) {
			$boolIsValid = false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_last_name', __( 'Last Name is required.' ) ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCcExpirationDate() {
		$boolIsValid = true;

		if( false == valStr( $this->m_objBankAccount->getCcExpDateMonth() ) || false == valStr( $this->m_objBankAccount->getCcExpDateYear() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'exp_date', __( 'Fast Funds debit card information must be entered in Bank Account Setup to process payment.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyDiscretionaryData() {
		$boolIsValid = true;

		if( true == valStr( $this->m_objBankAccount->getCompanyDiscretionaryData() ) && false == preg_match( '/^[0-9]+$/', $this->m_objBankAccount->getCompanyDiscretionaryData() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Company_discretionary_data', __( 'Company discretionary data can only include numeric.' ) ) );
		} elseif( 20 < strlen( $this->m_objBankAccount->getCompanyDiscretionaryData() ) ) {
			$boolIsValid &= false;
			$this->m_objBankAccount->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Company_discretionary_data', __( 'Company discretionary data should be less than or equals to 20 digit.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $arrmixParameters = [] ) {
		$boolIsValid						= true;
		$arrobjApPayments					= ( true == isset( $arrmixParameters['ap_payments'] ) ) ? $arrmixParameters['ap_payments'] : [];
		$arrmixCheckComponents				= ( true == isset( $arrmixParameters['check_components'] ) ) ? $arrmixParameters['check_components'] : [];
		$boolIsVirtualCardEnabled			= ( true == isset( $arrmixParameters['is_virtual_card_enabled'] ) && true == $arrmixParameters['is_virtual_card_enabled'] ) ? true : false;
		$arrobjExistingPropertyBankAccounts	= ( true == isset( $arrmixParameters['existing_property_bank_accounts'] ) ) ? $arrmixParameters['existing_property_bank_accounts'] : [];

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAccountName( $objClientDatabase );
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valBankAccountTypeId();
				$boolIsValid &= $this->valNextCheckNumber();
				$boolIsValid &= $this->valCheckAccountNumberEncrypted( $objClientDatabase );
				$boolIsValid &= $this->valCheckRoutingNumber();
				$boolIsValid &= $this->valPayerPhoneNumberEncrypted();
				$boolIsValid &= $this->valPrintSignatureLimitAmount();
				$boolIsValid &= $this->valSecondSignatureLimitAmount();
				$boolIsValid &= $this->valDaddyPropertyBankAccounts();
				$boolIsValid &= $this->valPropertyBankAccounts( $objClientDatabase, $strAction );
				$boolIsValid &= $this->valExistingPropertyBankAccounts(); // ??
				$boolIsValid &= $this->valDuplicatePropertyIdsAndAccounts();
				$boolIsValid &= $this->valIsPostDate( $objClientDatabase );
				$boolIsValid &= $this->valReserveAmount();
				$boolIsValid &= $this->valCheckBankNumber();
				// FIXME: Temporarily commenting following validations
				// $boolIsValid &= $this->valPostalCode();
				// $boolIsValid &= $this->valPayerPostalCode();
				$boolIsValid &= $this->valBankCode();
				$boolIsValid &= $this->valWfpmCeoCompanyId();
				$boolIsValid &= $this->valPostedInvoicesGlAccounts( $arrobjExistingPropertyBankAccounts, $objClientDatabase );
				break;

			case 'validate_fast_fund':
				$boolIsValid &= $this->valCcExpMonth();
				$boolIsValid &= $this->valCcExpYear();
				$boolIsValid &= $this->valCcExpDate();
				$boolIsValid &= $this->valCcFirstName();
				$boolIsValid &= $this->valCcLastName();
				break;

			case VALIDATE_DELETE:
				break;

			case 'enable_bank_account':
				$boolIsValid &= $this->valGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valExistingPropertyBankAccounts();
				break;

			case 'validate_next_check_no':
				$boolIsValid &= $this->valNextCheckNumber();
				break;

			case 'validate_check_property_in_posted_unpaid_invoice':
				$boolIsValid &= $this->valCheckPropertyInPostedUnpaidInvoice( $objClientDatabase );
				break;

			case 'validate_ach_details':
				$boolIsValid &= $this->valImmediateDestinationName();
				$boolIsValid &= $this->valImmediateDestinationId();
				$boolIsValid &= $this->valImmediateOriginId();
				$boolIsValid &= $this->valImmediateOriginName();
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valCompanyId();
				$boolIsValid &= $this->valOriginatingDfi();
				$boolIsValid &= $this->valCompanyDiscretionaryData();
				break;

			case 'validate_bulk_bank_accounts':
				$boolIsValid &= $this->valPrintSignatureLimitAmount();
				$boolIsValid &= $this->valSecondSignatureLimitAmount();
				$boolIsValid &= $this->valReserveAmount();
				break;

			case 'validate_bank_account_signature':
				$boolIsValid &= $this->valBankAccountSignature();
				break;

			case 'validate_bank_account_second_signature':
				$boolIsValid &= $this->valBankAccountSecondSignature( $arrmixCheckComponents, $arrobjApPayments );
				break;

			case 'validate_wfpm_details':
				$boolIsValid &= $this->valOriginatingBankId();
				break;

			case 'validate_capital_one_details':
				$boolIsValid &= $this->valSenderId();
				if( true == $boolIsVirtualCardEnabled ) $boolIsValid &= $this->valClientId();
				break;

			case 'validate_fast_funds_details':
				$boolIsValid &= $this->valCcExpirationDate();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>