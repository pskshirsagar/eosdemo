<?php

class CApplicantAssetValidator {

	protected $m_objApplicantAsset;

    public function __construct() {
        return;
    }

    public function setApplicantAsset( $objApplicantAsset ) {
		$this->m_objApplicantAsset = $objApplicantAsset;
    }

 	public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getCid() ) ) {
            $boolIsValid = false;
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getApplicantId() ) ) {
            $boolIsValid = false;
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', __( 'Applicant id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valApplicantAssetTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getApplicantAssetTypeId() ) || 0 == strlen( $this->m_objApplicantAsset->getApplicantAssetTypeId() ) ) {
            $boolIsValid = false;
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_asset_type_id', __( 'Asset type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valInstitutionName( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getInstitutionName() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_institution_name' : 'institution_name';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} institution name is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

	public function valAssetValue( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getAssetValue() ) || 0 == ( int ) $this->m_objApplicantAsset->getAssetValue() ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_asset_value' : 'asset_value';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} asset value is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valAccountNumberEncrypted( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getAccountNumberEncrypted() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_account_number' : 'account_number';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} account number is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valRoutingNumber( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getRoutingNumber() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_routing_number' : 'routing_number';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} routing number is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valNameOnAccount( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getNameOnAccount() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_name_on_account' : 'name_on_account';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} name on acct is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valAddress( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getAddress() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_address' : 'address';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} address is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valCity( $intApplicantAssetTypeId = 0, $strSectionName, $intIsAlien = NULL ) {
        $boolIsValid = true;

        if( 0 == $intIsAlien && true == is_null( $this->m_objApplicantAsset->getCity() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_city' : 'city';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} city is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valStateCode( $intApplicantAssetTypeId = 0, $strSectionName, $intIsAlien ) {
        $boolIsValid = true;

		if( 0 == $intIsAlien && true == is_null( $this->m_objApplicantAsset->getStateCode() ) ) {
		    $boolIsValid = false;
		    $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_state_code' : 'state_code';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} state is required.', [ $strSectionName ] ) ) );
		}

        return $boolIsValid;
    }

	public function valContactName( $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objApplicantAsset->getContactName() ) ) {
            $boolIsValid = false;
            $strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_contact_name' : 'contact_name';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} contact name is required.', [ $strSectionName ] ) ) );
        }

        return $boolIsValid;
    }

    public function valContactPhoneNumber( $boolIsRequired = false, $intApplicantAssetTypeId = 0, $strSectionName = NULL ) {

        $boolIsValid = true;

        $intLength = strlen( trim( $this->m_objApplicantAsset->getContactPhoneNumber() ) );

        if( true == $boolIsRequired ) {
        	if( true == is_null( $this->m_objApplicantAsset->getContactPhoneNumber() ) || 0 == $intLength ) {
    			$boolIsValid = false;
    			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
	            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_contact_phone_number' : 'contact_phone_number';
	            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} contact phone number is required.', [ $strSectionName ] ) ) );
    		}
    	}

    	if( 0 < $intLength && 10 > $intLength || 15 < $intLength ) {
    		$boolIsValid = false;
    		$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_contact_phone_number' : 'contact_phone_number';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} contact phone number must be between 10 and 15 characters.', [ $strSectionName ] ) ) );

    	} elseif( false == is_null( $this->m_objApplicantAsset->getContactPhoneNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objApplicantAsset->getContactPhoneNumber(), false ) ) ) {
		 	$boolIsValid = false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objApplicantAsset->getDefaultSectionName();
            $strFieldName 	= ( 0 < $intApplicantAssetTypeId )? 'asset_' . $intApplicantAssetTypeId . '_contact_phone_number' : 'contact_phone_number';
            $this->m_objApplicantAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} contact phone number is required.', [ $strSectionName ] ), 504 ) );
		}

        return $boolIsValid;
    }

    public function valRequiredFields( $objApplicantAssetType, $arrobjPropertyApplicationPreferences, $arrstrApplicantAssetTypeCustomFields, $intIsAlien = NULL ) {

    	$boolIsValid = true;

		if( false == valObj( $objApplicantAssetType, 'CApplicantAssetType' ) ) {
			return $boolIsValid;
		}

        if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
        	$arrobjPropertyApplicationPreferences = [];
        }

    	if( false == valArr( $arrstrApplicantAssetTypeCustomFields ) ) {
        	return $boolIsValid;
        }

        if( false == array_key_exists( $objApplicantAssetType->getId(), $arrstrApplicantAssetTypeCustomFields ) ) {
			return $boolIsValid;
        }

		$intApplicantAssetTypeId 	= $objApplicantAssetType->getId();
		$strSectionName 			= $objApplicantAssetType->getName();

		$strRenameApplicantAssetTypeKey 		= 'RENAME_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey();
    	// $strOverrideApplicantAssetTypeKey	= 'OVERRIDE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey();

		if( true == array_key_exists( $strRenameApplicantAssetTypeKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences[$strRenameApplicantAssetTypeKey]->getValue() ) ) {
			$strSectionName 		= $arrobjPropertyApplicationPreferences[$strRenameApplicantAssetTypeKey]->getValue();
		}

    	$strRequireApplicantAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_INSTITUTION_NAME';
		$strRequireApplicantAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_NAME_ON_ACCOUNT';
		$strRequireApplicantAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ROUTING_NUMBER';
		$strRequireApplicantAssetTypeAccountNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ACCOUNT_NUMBER';
		$strRequireApplicantAssetTypeAddressFieldKey 			= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ADDRESS';
		$strRequireApplicantAssetTypeContactNameFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_CONTACT_NAME';
		$strRequireApplicantAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';

// 		if( true == array_key_exists( $strOverrideApplicantAssetTypeKey, $arrobjPropertyApplicationPreferences ) ) {
// 			$strRequireApplicantAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_INSTITUTION_NAME';
// 			$strRequireApplicantAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_NAME_ON_ACCOUNT';
// 			$strRequireApplicantAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ROUTING_NUMBER';
// 			$strRequireApplicantAssetTypeAccountNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ACCOUNT_NUMBER';
// 			$strRequireApplicantAssetTypeAddressFieldKey 			= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_ADDRESS';
// 			$strRequireApplicantAssetTypeContactNameFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_CONTACT_NAME';
// 			$strRequireApplicantAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objApplicantAssetType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';
// 		} else {
// 			$strRequireApplicantAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_ASSET_DEFAULT_INSTITUTION_NAME';
// 			$strRequireApplicantAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_ASSET_DEFAULT_NAME_ON_ACCOUNT';
// 			$strRequireApplicantAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_ASSET_DEFAULT_ROUTING_NUMBER';
// 			$strRequireApplicantAssetTypeAccountNumberFieldKey 		= 'REQUIRE_ASSET_DEFAULT_ACCOUNT_NUMBER';
// 			$strRequireApplicantAssetTypeAddressFieldKey 			= 'REQUIRE_ASSET_DEFAULT_ADDRESS';
// 			$strRequireApplicantAssetTypeContactNameFieldKey 		= 'REQUIRE_ASSET_DEFAULT_CONTACT_NAME';
// 			$strRequireApplicantAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_ASSET_DEFAULT_CONTACT_PHONE_NUMBER';
// 		}

        if( true == array_key_exists( 'INSTITUTION_NAME', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeInstitutionNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
          	$boolIsValid &= $this->valInstitutionName( $intApplicantAssetTypeId, $strSectionName );
        }

    	$boolIsValid &= $this->valAssetValue( $intApplicantAssetTypeId, $strSectionName );

        if( true == array_key_exists( 'NAME_ON_ACCOUNT', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeNameOnAccountFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valNameOnAccount( $intApplicantAssetTypeId, $strSectionName );
        }

        if( true == array_key_exists( 'ROUTING_NUMBER', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeRoutingNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valRoutingNumber( $intApplicantAssetTypeId, $strSectionName );
        }

        if( true == array_key_exists( 'ACCOUNT_NUMBER', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeAccountNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valAccountNumberEncrypted( $intApplicantAssetTypeId, $strSectionName );
        }

        if( true == array_key_exists( 'ADDRESS', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeAddressFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valAddress( $intApplicantAssetTypeId, $strSectionName );
           	$boolIsValid &= $this->valCity( $intApplicantAssetTypeId, $strSectionName, $intIsAlien );
           	$boolIsValid &= $this->valStateCode( $intApplicantAssetTypeId, $strSectionName, $intIsAlien );
        }

   		if( true == array_key_exists( 'CONTACT_NAME', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeContactNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valContactName( $intApplicantAssetTypeId, $strSectionName );
        }

        if( true == array_key_exists( 'CONTACT_PHONE', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) && true == array_key_exists( $strRequireApplicantAssetTypeContactPhoneNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {
           	$boolIsValid &= $this->valContactPhoneNumber( true, $intApplicantAssetTypeId, $strSectionName );

        } elseif( true == array_key_exists( 'CONTACT_PHONE', $arrstrApplicantAssetTypeCustomFields[$intApplicantAssetTypeId] ) ) {
        	$boolIsValid &= $this->valContactPhoneNumber( false, $intApplicantAssetTypeId, $strSectionName );
        }

		return $boolIsValid;
    }

    public function validate( $strAction, $objApplicantAssetType, $arrobjPropertyApplicationPreferences, $arrstrApplicantAssetTypeCustomFields, $intIsAlien ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRequiredFields( $objApplicantAssetType, $arrobjPropertyApplicationPreferences, $arrstrApplicantAssetTypeCustomFields, $intIsAlien );
				break;

            case VALIDATE_DELETE:
            	break;

            case 'applicant_assets':
            	$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApplicantId();
				$boolIsValid &= $this->valApplicantAssetTypeId();
				$boolIsValid &= $this->valRequiredFields( $objApplicantAssetType, $arrobjPropertyApplicationPreferences, $arrstrApplicantAssetTypeCustomFields, $intIsAlien );
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>