<?php

class CLeaseValidator {

	protected $m_objLease;
	protected $m_arrobjAllUnitSpecificLeaseIntervals;

    public function __construct() {
        return;
    }

    public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
    }

	/**
	* Validation Functions
	*
	*/

	public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->m_objLease->getId() ) || 0 >= ( int ) $this->m_objLease->getId() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Request:  Id required - CLease::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->m_objLease->getCid() ) || 0 >= ( int ) $this->m_objLease->getCid() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Request:  client id required - CLease::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valPropertyId( $boolIsTrigger = true ) {
        $boolIsValid = true;

        if( true == is_null( $this->m_objLease->getPropertyId() ) || 0 >= ( int ) $this->m_objLease->getPropertyId() ) {
        	if( false == $boolIsTrigger ) {
        		$boolIsValid = false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
        	} else {
	            $boolIsValid = false;
				trigger_error( 'Invalid Company Lease Request:  Property id required =  - CLease::valPropertyId()', E_USER_ERROR );
        	}
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey( $boolIsTrigger = true ) {
        $boolIsValid = true;

            if( false == $boolIsTrigger && false == valStr( $this->m_objLease->getRemotePrimaryKey() ) ) {
                $boolIsValid = false;
                $this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', __( 'Remote primary key is required.' ) ) );
            }
        return $boolIsValid;

    }

    public function valPropertyUnitId( $boolIsTrigger = true ) {
        $boolIsValid = true;

        if( false == is_null( $this->m_objLease->getPropertyUnitId() ) ) {

			if( 0 >= ( int ) $this->m_objLease->getPropertyUnitId() ) {
				if( false == $boolIsTrigger ) {
					$boolIsValid = false;
					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Property unit id is required.' ) ) );
				} else {
					$boolIsValid = false;
					trigger_error( 'Invalid Company Lease Request:  Property unit id must be greater than 0 - CLease::valPropertyUnitId()', E_USER_ERROR );
				}
			}

			if( 0 >= ( int ) $this->m_objLease->getUnitSpaceId() ) {
				if( false == $boolIsTrigger ) {
					$boolIsValid = false;
					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Unit Space Id must be present when property unit id is present.' ) ) );
				} else {
					$boolIsValid = false;
					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Select a unit.' ) ) );
					// trigger_error( 'Invalid Company Lease Request:  Unit Space Id must be present when property unit id is present - CLease::valPropertyUnitId()', E_USER_ERROR );
				}
			}

        }

        return $boolIsValid;
    }

    public function valUnitSpaceId( $boolIsTrigger = true ) {
        $boolIsValid = true;

        if( false == is_null( $this->m_objLease->getUnitSpaceId() ) ) {
			if( 0 >= ( int ) $this->m_objLease->getUnitSpaceId() ) {
				$boolIsValid = false;
				if( true == $boolIsTrigger ) {
					trigger_error( 'Invalid Company Lease Request:  Unit space id must be greater than 0 - CLease::valUnitSpaceId()', E_USER_ERROR );
				} else {
	                $this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Unit space id must be greater than 0.' ) ) );
                }
			}

			if( 0 >= ( int ) $this->m_objLease->getPropertyUnitId() ) {
				$boolIsValid = false;
				if( true == $boolIsTrigger ) {
					trigger_error( 'Invalid Company Lease Request:  Property unit id must be present when unit space id is present - CLease::valUnitSpaceId()', E_USER_ERROR );
				} else {
					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Property unit id must be present when unit space id is present.' ) ) );
				}
			}
        }

        return $boolIsValid;
    }

    public function valLeaseStartDate( $objLeaseIntervalForGivenPerNoticeDate = NULL, $boolIsRequired = false, $objDatabase = NULL, $arrmixDataToValidate = NULL ) {
    	if( 1 != $this->m_objLease->getIsView() ) return true;
        $boolIsValid 				= true;
        $strLeaseStartDate 			= $this->m_objLease->getLeaseStartDate();
        $boolIsRenewalUnitTransfer	= false;
		if( true == valObj( $objDatabase, 'CDatabase' ) && CLeaseIntervalType::MONTH_TO_MONTH == $this->m_objLease->getLeaseIntervalTypeId() ) {

			$boolIsRenewalUnitTransfer = \Psi\Eos\Entrata\CLeases::createService()->fetchRenewalTransferCountByLeaseIdByPropertyIdByCid( $this->m_objLease->getId(), $this->m_objLease->getPropertyId(), $this->m_objLease->getCid(), $objDatabase );
			 if( true == $boolIsRenewalUnitTransfer ) {
	         	$objContractualLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLastContractualLeaseIntervalByCidByLeaseId( $this->m_objLease->getCid(), $this->m_objLease->getId(), $objDatabase );
	         	if( true == valObj( $objContractualLeaseInterval, 'CLeaseInterval' ) ) {
	         		$strLeaseStartDate = $objContractualLeaseInterval->getLeaseStartDate();
	         	}
	         }
		}

        if( false == $boolIsRenewalUnitTransfer && true == valObj( $objLeaseIntervalForGivenPerNoticeDate, 'CLeaseInterval' ) ) {
        	$this->m_objLease->setLeaseStartDate( $objLeaseIntervalForGivenPerNoticeDate->getLeaseStartDate() );
        	$strLeaseStartDate = $objLeaseIntervalForGivenPerNoticeDate->getLeaseStartDate();
        }

        if( true == valStr( $this->m_objLease->getLeaseStartDate() ) ) {
        	$mixValidatedLeaseStartDate = CValidation::checkISODateFormat( $this->m_objLease->getLeaseStartDate(), $boolFormat = true );
        	if( false === $mixValidatedLeaseStartDate ) {
        		$boolIsValid = false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is not valid.' ) ) );
        	} else {
        		if( CLeaseIntervalType::MONTH_TO_MONTH != $this->m_objLease->getLeaseIntervalTypeId() ) {
					if( true == valStr( $this->m_objLease->getMoveOutDate() ) && strtotime( $strLeaseStartDate ) > strtotime( $this->m_objLease->getMoveOutDate() ) ) {
						$boolIsValid = false;
						$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be less than Move-out date.' ) ) );
					}
				}

        		if( true == $boolIsValid ) {
        			$this->m_objLease->setLeaseStartDate( date( 'm/d/Y', strtotime( $mixValidatedLeaseStartDate ) ) );
        		}
  	        }
        } elseif( true == $boolIsRequired ) {
        	$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valLeaseEndDate( $boolIsRequired = false, $objDatabase ) {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

        if( true == valStr( $this->m_objLease->getLeaseEndDate() ) ) {
        	$mixValidatedLeaseEndDate = CValidation::checkISODateFormat( $this->m_objLease->getLeaseEndDate(), $boolFormat = true );
        	if( false === $mixValidatedLeaseEndDate ) {
        		$boolIsValid &= false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date is not valid.' ) ) );
        	} else {
        		$this->m_objLease->setLeaseEndDate( date( 'm/d/Y', strtotime( $mixValidatedLeaseEndDate ) ) );
        	}
        } elseif( true == $boolIsRequired && CLeaseIntervalType::MONTH_TO_MONTH != $this->m_objLease->getLeaseIntervalTypeId() ) {
        	$boolIsValid &= false;
       		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date is required.' ) ) );
        }

        if( true == $boolIsRequired && true == valStr( $this->m_objLease->getLeaseEndDate() ) && $this->m_objLease->getLeaseStatusTypeId() != CLeaseStatusType::NOTICE && CLeaseIntervalType::MONTH_TO_MONTH == $this->m_objLease->getLeaseIntervalTypeId() && strtotime( date( 'Y/m/d' ) ) < strtotime( date( 'Y/m/d', strtotime( $this->m_objLease->getLeaseEndDate() ) ) ) ) {
        	$arrobjChildLeaseInterval = ( array ) \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalsByParentLeaseIntervalIdByCid( $this->m_objLease->getActiveLeaseIntervalId(), $this->m_objLease->getCid(), $objDatabase );
	        $objChildLeaseInterval  = reset( $arrobjChildLeaseInterval );

	        $boolIsFutureRenewalIntervalExist = false;
			if( true == valObj( $objChildLeaseInterval, 'CLeaseInterval' ) && CLeaseIntervalType::RENEWAL == $objChildLeaseInterval->getLeaseIntervalTypeId() && CLeaseStatusType::FUTURE == $objChildLeaseInterval->getLeaseStatusTypeId() ) {
				$boolIsFutureRenewalIntervalExist = true;
			}

			if( false == $boolIsFutureRenewalIntervalExist ) {
				$boolIsValid &= false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease with future end date can not be month to month.' ) ) );
			}
        }
        return $boolIsValid;
    }

  	public function valLeaseStartDateBeforeLeaseEndDate() {

    	if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

   		if( true == $boolIsValid && false == is_null( $this->m_objLease->getLeaseEndDate() ) ) {
        	if( true == is_null( $this->m_objLease->getLeaseStartDate() ) ) {
        		$boolIsValid = false;
        		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date required.' ) ) );
        	} elseif( strtotime( $this->m_objLease->getLeaseStartDate() ) > strtotime( $this->m_objLease->getLeaseEndDate() ) ) {
        		$boolIsValid = false;
        		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date must be greater than lease start date.' ) ) );
        	}
        }
        return $boolIsValid;
	}

    public function valMoveInDate() {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

        if( true == valStr( $this->m_objLease->getMoveInDate() ) ) {
        	$mixValidatedMoveInDate = CValidation::checkISODateFormat( $this->m_objLease->getMoveInDate(), $boolFormat = true );
        	if( false === $mixValidatedMoveInDate ) {
        		$boolIsValid = false;
       			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date is not valid.' ) ) );
        	} else {
        		$this->m_objLease->setMoveInDate( date( 'm/d/Y', strtotime( $mixValidatedMoveInDate ) ) );
        	}
        } elseif( COccupancyType::OTHER_INCOME != $this->m_objLease->getOccupancyTypeId() ) {
        	$boolIsValid = false;
       		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valMoveOutDate( $boolIsRequired = false ) {
		if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

        if( true == valStr( $this->m_objLease->getMoveOutDate() ) ) {
        	$mixValidatedMoveOutDate = CValidation::checkISODateFormat( $this->m_objLease->getMoveOutDate(), $boolFormat = true );
        	if( false === $mixValidatedMoveOutDate ) {
        		$boolIsValid = false;
       			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out date is not valid.' ) ) );
        	} else {
        		$this->m_objLease->setMoveOutDate( date( 'm/d/Y', strtotime( $mixValidatedMoveOutDate ) ) );
        	}
        } elseif( true == $boolIsRequired ) {
        	$boolIsValid = false;
       		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out date is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valMoveInDateOnAfterLeaseStartDate() {
        $boolIsValid = true;

		if( true == $boolIsValid && strtotime( $this->m_objLease->getLeaseStartDate() ) > strtotime( $this->m_objLease->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be on or after lease start date.' ) ) );
		}

        return $boolIsValid;
    }

    public function valMoveInDateBeforeLeaseEndDate() {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

		if( true == $boolIsValid && false == is_null( $this->m_objLease->getLeaseEndDate() )
			&& strtotime( $this->m_objLease->getLeaseEndDate() ) < strtotime( $this->m_objLease->getMoveInDate() ) ) {
				$boolIsValid = false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be on or before the lease end date.' ) ) );
		}

        return $boolIsValid;
    }

	public function valMoveInDateBeforeIndividualMoveOutDate( $objDatabase ) {

		$boolIsValid		= true;
		$arrobjLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByLeaseIdByCid( $this->m_objLease->getId(), $this->m_objLease->getCid(), $objDatabase );

		if( true == valArr( $arrobjLeaseProcess ) ) {
			foreach( $arrobjLeaseProcess as $objLeaseProcess ) {
				if( true == valId( $objLeaseProcess->getCustomerId() ) && false == is_null( $objLeaseProcess->getMoveOutDate() ) && strtotime( $objLeaseProcess->getMoveOutDate() ) < strtotime( $this->m_objLease->getMoveInDate() ) ) {
					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move out dates for one or more occupants precedes this move in date. The lease move in date must be before the move out date for any occupant.' ) ) );
					return false;
				}
			}
		}

		return $boolIsValid;
	}

    public function valMoveOutDateOnAfterLeaseStartAndMoveInDates() {
        $boolIsValid = true;

		if( true == $boolIsValid && strtotime( $this->m_objLease->getLeaseStartDate() ) > strtotime( $this->m_objLease->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date must be on or after lease start date.' ) ) );
		}
		if( true == $boolIsValid && strtotime( $this->m_objLease->getMoveInDate() ) > strtotime( $this->m_objLease->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date must be on or after move-in date.' ) ) );
		}

        return $boolIsValid;
    }

    public function valNoticeDate( $boolIsRequired = false ) {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

		if( true == valStr( $this->m_objLease->getNoticeDate() ) ) {
			$mixValidatedNoticeDate = CValidation::checkISODateFormat( $this->m_objLease->getNoticeDate(), $boolFormat = true );
			if( false === $mixValidatedNoticeDate ) {
				$boolIsValid = false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date is not valid.' ) ) );
			} else {
				$this->m_objLease->setNoticeDate( date( 'm/d/Y', strtotime( $mixValidatedNoticeDate ) ) );
			}
		} elseif( true == $boolIsRequired ) {
			$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date is required.' ) ) );
		}

        return $boolIsValid;
    }

	public function valNoticeDateBeforeMoveOutDate() {
		if( 1 != $this->m_objLease->getIsView() ) return true;

		$boolIsValid = true;

		if( true == $boolIsValid && strtotime( $this->m_objLease->getNoticeDate() ) > strtotime( $this->m_objLease->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Notice date must be less than the move-out date.' ) ) );
		}

        return $boolIsValid;
	}

	public function valLateFeePostedBy() {
		if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

        if( true == is_null( $this->m_objLease->getLateFeePostedBy() ) ) {
            $boolIsValid = false;
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_posted_by', __( 'Last fee posted by is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valLateFeePostedOn() {
		if( 1 != $this->m_objLease->getIsView() ) return true;

        $boolIsValid = true;

		if( true == valStr( $this->m_objLease->getLateFeePostedOn() ) ) {
			$mixValidatedLateFeePostedOn = CValidation::checkISODateFormat( $this->m_objLease->getLateFeePostedOn(), $boolFormat = true );
			if( false === $mixValidatedLateFeePostedOn ) {
				$boolIsValid = false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_posted_on', __( 'Late fee posted on date is not valid.' ) ) );
			} else {
				$this->m_objLease->setLateFeePostedOn( date( 'm/d/Y', strtotime( $mixValidatedLateFeePostedOn ) ) );
			}
		}

        return $boolIsValid;
    }

    public function valLeaseStartDateEndDate( $objDatabase ) {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

    	$boolIsValid = true;

		if( true == valObj( $objDatabase, 'CDatabase' ) && false == is_null( $this->m_objLease->getUnitSpaceId() )
			 && true == valStr( $this->m_objLease->getLeaseStartDate() ) && true == valStr( $this->m_objLease->getLeaseEndDate() ) ) {
    		$this->m_arrobjAllUnitSpecificLeaseIntervals = ( array ) $this->m_objLease->fetchAllUnitSpecificLeaseIntervalsOfActiveLeases( $objDatabase );
    		$objActiveLeaseInterval = $this->m_objLease->getOrFetchActiveLeaseInterval( $objDatabase );

    		if( true == valObj( $objActiveLeaseInterval, 'CLeaseInterval' ) ) {
    			$boolIsValid &= $objActiveLeaseInterval->valLeaseIntervalDates( $this->m_arrobjAllUnitSpecificLeaseIntervals );
    			if( false == $boolIsValid && true == valArr( $objActiveLeaseInterval->getErrorMsgs() ) ) {
    				foreach( $objActiveLeaseInterval->getErrorMsgs() as $objError ) {
    					$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $objError->getField(), $objError->getMessage() ) );
    				}
    			}
    		}
    	}

    	return $boolIsValid;
    }

    public function valNoticeDateOnAfterMoveInDate( $strAction = NULL ) {
    	if( 1 != $this->m_objLease->getIsView() ) return true;

    	$boolIsValid = true;

    	if( true == valStr( $this->m_objLease->getMoveInDate() ) && true == valStr( $this->m_objLease->getNoticeDate() )
    		&& strtotime( $this->m_objLease->getMoveInDate() ) > strtotime( $this->m_objLease->getNoticeDate() ) ) {
    		$boolIsValid = false;
    		if( 'move_in_adjustment' == $strAction ) {
    			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Move_in_date', __( 'Move-in date must be on or before notice date.' ) ) );
    		} else {
    			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date must be on or after move-in date.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valTransferStatusType() {
    	$boolIsValid = true;

    	if( $this->m_objLease->getLeaseStatusTypeId() != CLeaseStatusType::CURRENT ) {
       		$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_status_type_id', __( 'A lease cannot be scheduled for transfer unless it has a lease status of \'Current\'.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valMoveInAdjustmentPermissions( $objCompanyUser, $objDatabase ) {

    	$boolIsEditMoveInDate = false;

    	if( true == $objCompanyUser->getIsAdministrator() ) {
    		 $boolIsEditMoveInDate = true;
    	} else {
    		$arrobjCompanyUserPermission = [];
    		$arrobjCompanyUserPermission = ( array ) $objCompanyUser->fetchCompanyUserPermissionsByModuleIds( [ CModule::EDIT_MOVE_IN_DATE ], $objDatabase );

    		foreach( $arrobjCompanyUserPermission AS $objCompanyUserPermission ) {

    			if( true == valObj( $objCompanyUserPermission, 'CCompanyUserPermission' ) ) {
    				if( false == $objCompanyUserPermission->getIsInherited() ) {
    					if( CModule::EDIT_MOVE_IN_DATE == $objCompanyUserPermission->getModuleId() ) {
    						$boolIsEditMoveInDate = $objCompanyUserPermission->getIsAllowed();
    					}

    				} else {
    					$arrobjCompanyUserGroupsPermissions = $objCompanyUser->fetchCompanyGroupPermissions( $objDatabase );
    					$arrobjCompanyUserGroupsPermissions = rekeyObjects( 'moduleId', $arrobjCompanyUserGroupsPermissions );
    					if( true == valArr( $arrobjCompanyUserGroupsPermissions ) ) {
    						$arrobjCompanyUserGroupsPermissions = rekeyObjects( 'moduleId', $arrobjCompanyUserGroupsPermissions );

    							$boolIsEditMoveInDate = ( true == array_key_exists( CModule::EDIT_MOVE_IN_DATE, $arrobjCompanyUserGroupsPermissions ) ) ? true : false;
    					}
    					break;
    				}
    			}
    		}
    	}
    	$arrboolUserPermission = [];
    	$arrboolUserPermission[CModule::EDIT_MOVE_IN_DATE] = $boolIsEditMoveInDate;

    	return $arrboolUserPermission;
    }

	public function validateSuite( $objDatabase ) {
		$boolIsValid = true;
		if( ( COccupancyType::COMMERCIAL == $this->m_objLease->getOccupancyTypeId() ) ) {
			$intTotalActiveLeases = ( \Psi\Eos\Entrata\CCachedLeases::createService()->fetchTotalActiveLeasesCountByPropertyIdByPropertyUnitIdByCid( $this->m_objLease->getPropertyId(), $this->m_objLease->getPropertyUnitId(), $this->m_objLease->getCid(), $objDatabase, $this->m_objLease->getId() ) );
			if( $intTotalActiveLeases > 0 ) {
				$boolIsValid &= false;
				$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Selected Suite is not available' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valLeaseStatusBeforeRemoveGroupOrIndividual() {
		$boolIsValid = true;
		if( true == in_array( $this->m_objLease->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) ) {
			$this->m_objLease->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please refresh the page for updated Lease status.' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $objCompanyUser = NULL, $objLeaseIntervalForGivenPerNoticeDate = NULL, $boolIsStudentSemesterSelectionEnabled = false, $arrmixDataToValidate = NULL ) {

    	$boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsTrigger = ( COccupancyType::COMMERCIAL == $this->m_objLease->getOccupancyTypeId() ) ? false : true;
                $boolIsValid &= $this->valCid();
                $boolIsValid &= $this->valPropertyId( $boolIsTrigger );
				$boolIsValid &= $this->valPropertyUnitId( $boolIsTrigger );
				$boolIsValid &= $this->valUnitSpaceId( $boolIsTrigger );
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );
				$boolIsValid &= $this->valMoveInDate();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = false );
				$boolIsValid &= $this->valRemotePrimaryKey();
				$boolIsValid &= $this->validateSuite( $objDatabase );

				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					// $boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
				}
				break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate( $objLeaseIntervalForGivenPerNoticeDate, $boolIsRequired = false, $objDatabase, $arrmixDataToValidate );
				$boolIsValid &= $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );
				$boolIsValid &= $this->valMoveInDate();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = false );
				$boolIsValid &= $this->valNoticeDate();
				$boolIsValid &= $this->validateSuite( $objDatabase );

				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					if( COccupancyType::OTHER_INCOME != $this->m_objLease->getOccupancyTypeId() && true == valStr( $this->m_objLease->getLeaseEndDate() ) ) {
						$boolIsValid &= $this->valMoveInDateBeforeLeaseEndDate();
					}
					$boolIsValid &= $this->valNoticeDateOnAfterMoveInDate();
				}
				// $boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
				break;

            case 'update_move_out':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valMoveInDate();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					$boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
				}
				break;

			case 'update_move_out_notice':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = true );
				$boolIsValid &= $this->valNoticeDate( $boolIsRequired = true );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valNoticeDateOnAfterMoveInDate();
					$boolIsValid &= $this->valNoticeDateBeforeMoveOutDate();
				}
				break;

			case 'pre_insert':
				// The first page of add customer
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				break;

			case 'pre_lease_insert':
				// The second page of the add customer w/ new lease
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );
				$boolIsValid &= $this->valMoveInDate();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					$boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
				}
				break;

			case 'auto_create':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'application_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				if( false == $boolIsStudentSemesterSelectionEnabled ) $boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				break;

			case 'late_fee_post_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLateFeePostedBy();
				$boolIsValid &= $this->valLateFeePostedOn();
				break;

			// For Customer Moveout Notice
			case 'update_customer_move_out_notice':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				break;

			// For Customer Moveout Cancel Notice
			case 'cancel_move_out_notice':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate();
				break;

			case 'add_customer':
				$boolIsValid &= $this->valPropertyId( $boolIsTrigger = false );
				$boolIsValid &= $this->valPropertyUnitId( $boolIsTrigger = false );
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );
				$boolIsValid &= $this->valMoveInDate();
				if( true == $boolIsValid ) {
					// $boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
				}
				break;

			case 'pm_lease_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( $boolIsTrigger = false );
				$boolIsValid &= $this->valPropertyUnitId( $boolIsTrigger = false );
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= ( COccupancyType::OTHER_INCOME != $this->m_objLease->getOccupancyTypeId() ) ? $this->valLeaseEndDate( $boolIsRequired = true, $objDatabase ) : $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );
				$boolIsValid &= $this->valMoveInDate();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = false );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					$boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );
					$boolIsValid &= $this->valMoveInDateBeforeLeaseEndDate();
				}
				break;

			case 'pm_lease_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceId();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= ( COccupancyType::OTHER_INCOME != $this->m_objLease->getOccupancyTypeId() ) ? $this->valLeaseEndDate( $boolIsRequired = true, $objDatabase ) : $this->valLeaseEndDate( $boolIsRequired = false, $objDatabase );// commenting this only because it will be handled by lease intervals validate function
				$boolIsValid &= $this->valMoveInDate();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = false );
				$boolIsValid &= $this->valNoticeDate();

				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
					// $boolIsValid &= $this->valLeaseStartDateEndDate( $objDatabase );

					if( COccupancyType::OTHER_INCOME != $this->m_objLease->getOccupancyTypeId() ) {
						$boolIsValid &= $this->valMoveInDateBeforeLeaseEndDate();
					}

					$boolIsValid &= $this->valNoticeDateOnAfterMoveInDate();
				}
				break;

			case 'begin_transfer':
				$boolIsValid &= $this->valNoticeDate( $boolIsRequired = true );
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = true );
				$boolIsValid &= $this->valTransferStatusType();
				break;

			case 'insert_misc_income':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( false );
				$boolIsValid &= $this->valLeaseStartDate( $objLeaseIntervalForGivenPerNoticeDate = NULL, $boolIsRequired = true );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valLeaseStartDateBeforeLeaseEndDate();
				}
				break;

			case 'move_in_adjustment':
				$boolIsValid &= $this->valNoticeDateOnAfterMoveInDate( $strAction );
				$boolIsValid &= $this->valMoveInDateBeforeLeaseEndDate();
				$boolIsValid &= $this->valMoveInDateBeforeIndividualMoveOutDate( $objDatabase );
				break;

			case 'validate_move_in_transfer':
				$boolIsValid &= $this->valMoveInDateBeforeLeaseEndDate();
				$boolIsValid &= $this->valMoveInDateOnAfterLeaseStartDate();
				break;

            case 'validate_commercial_lease':
                $boolIsValid &= $this->valId();
                $boolIsValid &= $this->valCid();
                $boolIsValid &= $this->valPropertyId();
                $boolIsValid &= $this->valUnitSpaceId();
                $boolIsValid &= $this->valLeaseStartDate();
                break;

	        case 'validate_lease_status_for_bua':
		        $boolIsValid &= $this->valLeaseStatusBeforeRemoveGroupOrIndividual();
		        break;

	        default:
				$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

}
?>