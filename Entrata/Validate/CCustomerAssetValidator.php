<?php
use Psi\Libraries\I18n\PostalAddress\CPostalAddress;

class CCustomerAssetValidator {

	use TPostalAddressHelper;
	protected $m_objCustomerAsset;
	protected $m_boolValidateRequiredFields;

	const ADDRESS_POSTAL_CODE_FIELD     = 'postalCode';

	public function __construct() {
		$this->m_boolValidateRequiredFields = true;
		return;
	}

	public function setCustomerAsset( $objCustomerAsset ) {
		$this->m_objCustomerAsset = $objCustomerAsset;
	}

	public function setValidateRequiredFields( $boolValidateRequiredFields ) {
		$this->m_boolValidateRequiredFields = $boolValidateRequiredFields;
	}

 	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getCid() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Customer_id', __( 'Household member is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerAssetTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getCustomerAssetTypeId() ) || 0 == strlen( $this->m_objCustomerAsset->getCustomerAssetTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Customer_asset_type_id', __( 'Asset type is required.' ), '', [ 'label' => __( 'Asset Type' ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valInstitutionName( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getInstitutionName() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_institution_name' : 'institution_name';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} institution name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valMarketValue( $strAction = NULL, $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {

		if( 0 > $this->m_objCustomerAsset->getMarketValue() ) {
			if( false == is_null( $strAction ) && 'validate_resident_asset' === $strAction ) {
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'market_value', __( 'Market value is required.' ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			} else {
				$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
				$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_market_value' : 'market_value';
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} market value is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
			return false;
		}

		return true;

	}

	public function valAccountNumberEncrypted( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getAccountNumberEncrypted() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_account_number' : 'account_number';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} account number is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valRoutingNumber( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getRoutingNumber() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_routing_number' : 'routing_number';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} routing number is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valNameOnAccount( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getNameOnAccount() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_name_on_account' : 'name_on_account';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} name on acct is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valInstitutionStreetLine1( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getInstitutionStreetLine1() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_address' : 'address';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} address is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valInstitutionCity( $intCustomerAssetTypeId = 0, $strSectionName, $intIsAlien = NULL ) {
		$boolIsValid = true;

		if( 0 == $intIsAlien && true == is_null( $this->m_objCustomerAsset->getInstitutionCity() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_institution_city' : 'institution_city';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valInstitutionStateCode( $intCustomerAssetTypeId = 0, $strSectionName, $intIsAlien ) {
		$boolIsValid = true;

		if( 0 == $intIsAlien && true == is_null( $this->m_objCustomerAsset->getInstitutionStateCode() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_institution_state_code' : 'institution_state_code';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} state is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valContactName( $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objCustomerAsset->getContactName() ) && true == $this->m_boolValidateRequiredFields ) {
			$boolIsValid &= false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_contact_name' : 'contact_name';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} contact name is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}

		return $boolIsValid;
	}

	public function valContactPhoneNumber( $boolIsRequired = false, $intCustomerAssetTypeId = 0, $strSectionName = NULL ) {

		$boolIsValid = true;

		$objContactPhoneNumber = $this->createPhoneNumber( $this->m_objCustomerAsset->getContactPhoneNumber() );
		if( true == $boolIsRequired ) {
			if( ( true == is_null( $objContactPhoneNumber->getNumber() ) ) && true == $this->m_boolValidateRequiredFields ) {
				$boolIsValid &= false;
				$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
				$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_contact_phone_number' : 'contact_phone_number';
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} contact phone number is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		}

		if( true == valStr( $objContactPhoneNumber->getNumber() ) && false == $objContactPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$strSectionName = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strFieldName 	= ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_contact_phone_number' : 'contact_phone_number';
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,section_name} contact phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objContactPhoneNumber->getFormattedErrors(), [ 'section_name' => $strSectionName, 'region' => $objContactPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objContactPhoneNumber->getCountryCode() ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
		}
		return $boolIsValid;
	}

	public function valContactFaxNumber( $intCustomerAssetTypeId = 0,  $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = \Psi\CStringService::singleton()->strlen( $this->m_objCustomerAsset->getContactFaxNumber() );

		$strSectionName		 = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
		$strCustomerFieldName   = ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact fax';
		$strFieldName		   = ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_contact_fax_number' : 'contact_fax_number';

		if( true == $boolIsRequired && true == $this->m_boolValidateRequiredFields ) {
			if( true == is_null( $this->m_objCustomerAsset->getContactFaxNumber() ) || 0 == $intLength ) {
				$boolIsValid &= ( true == $this->m_boolValidateRequiredFields ) ? false : $boolIsValid;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$boolIsValid &= false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} number must be between 10 and 15 characters.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );

		} elseif( false == is_null( $this->m_objCustomerAsset->getContactFaxNumber() ) && false == ( CValidation::validateFullPhoneNumber( $this->m_objCustomerAsset->getContactFaxNumber(), false ) ) ) {

			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} number is required.', [ $strSectionName, $strCustomerFieldName ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valContactEmail( $strAction = NULL, $intCustomerAssetTypeId = 0, $strSectionName = NULL, $boolIsRequired = true, $strCustomerFieldName = NULL ) {

		$boolIsValid = true;
		$intLength = \Psi\CStringService::singleton()->strlen( $this->m_objCustomerAsset->getContactEmail() );

		if( false == is_null( $strAction ) && 'validate_resident_asset' == $strAction ) {
			if( true == valStr( $this->m_objCustomerAsset->getContactEmail() ) && false == CValidation::validateEmailAddresses( $this->m_objCustomerAsset->getContactEmail() ) ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->setContactEmail( NULL );
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email', __( 'Email address does not appear to be valid.' ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
			}
		} else {
			$strSectionName		 = ( 0 < strlen( $strSectionName ) )? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
			$strCustomerFieldName   = ( 0 < strlen( $strCustomerFieldName ) )? \Psi\CStringService::singleton()->strtolower( $strCustomerFieldName ) : 'contact email';
			$strFieldName		   = ( 0 < $intCustomerAssetTypeId )? 'asset_' . $intCustomerAssetTypeId . '_contact_email' : 'contact_email';

			if( true == $boolIsRequired && true == $this->m_boolValidateRequiredFields ) {
				if( true == is_null( $this->m_objCustomerAsset->getContactEmail() ) || 0 == $intLength ) {
					$boolIsValid &= ( true == $this->m_boolValidateRequiredFields ) ? false : $boolIsValid;
					$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
				}
			}

            if( 0 < $intLength && false == CValidation::validateEmailAddresses( $this->m_objCustomerAsset->getContactEmail() ) ) {
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Valid {%s,0} {%s,1} is required.', [ $strSectionName, $strCustomerFieldName ] ), 504, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ] ) );
	            $boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valRequiredFields( $objCustomerAssetType, $arrobjPropertyApplicationPreferences, $arrstrCustomerAssetTypeCustomFields ) {

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$boolIsValid = true;

		if( false == valObj( $objCustomerAssetType, 'CCustomerAssetType' ) ) {
			return $boolIsValid;
		}

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			$arrobjPropertyApplicationPreferences = [];
		}

		if( false == valArr( $arrstrCustomerAssetTypeCustomFields ) ) {
			return $boolIsValid;
		}

		if( false == array_key_exists( $objCustomerAssetType->getId(), $arrstrCustomerAssetTypeCustomFields ) ) {
			return $boolIsValid;
		}

		$strShowAssetTypeKey = 'HIDE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey();
		if( true == array_key_exists( $strShowAssetTypeKey, $arrobjPropertyApplicationPreferences ) ) {
			return $boolIsValid;
		}

		$intCustomerAssetTypeId 	= $objCustomerAssetType->getId();
		$strSectionName 			= $objCustomerAssetType->getName();

		$strRenameCustomerAssetTypeKey 		= 'RENAME_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey();
		// $strOverrideCustomerAssetTypeKey 	= 'OVERRIDE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey();

		if( true == array_key_exists( $strRenameCustomerAssetTypeKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences[$strRenameCustomerAssetTypeKey]->getValue() ) ) {
			$strSectionName 		= $arrobjPropertyApplicationPreferences[$strRenameCustomerAssetTypeKey]->getValue();
		}

		$strRequireCustomerAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_INSTITUTION_NAME';
		$strRequireCustomerAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_NAME_ON_ACCOUNT';
		$strRequireCustomerAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ROUTING_NUMBER';
		$strRequireCustomerAssetTypeAccountNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ACCOUNT_NUMBER';
		$strRequireCustomerAssetTypeAddressFieldKey 			= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ADDRESS';
		$strRequireCustomerAssetTypeContactNameFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_NAME';
		$strRequireCustomerAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';
		$strRequireCustomerAssetTypeContactFaxFieldKey		    = 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_FAX_NUMBER';
		$strRequireCustomerAssetTypeContactEmailFieldKey		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_EMAIL';

 		/*
 		 if( true == array_key_exists( $strOverrideCustomerAssetTypeKey, $arrobjPropertyApplicationPreferences ) ) {
	 			$strRequireCustomerAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_INSTITUTION_NAME';
	 			$strRequireCustomerAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_NAME_ON_ACCOUNT';
	 			$strRequireCustomerAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ROUTING_NUMBER';
	 			$strRequireCustomerAssetTypeAccountNumberFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ACCOUNT_NUMBER';
	 			$strRequireCustomerAssetTypeAddressFieldKey 			= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_ADDRESS';
	 			$strRequireCustomerAssetTypeContactNameFieldKey 		= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_NAME';
	 			$strRequireCustomerAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_APPLICANT_ASSET_TYPE_' . $objCustomerAssetType->getPreferenceKey() . '_CONTACT_PHONE_NUMBER';
	 	 } else {
	 			$strRequireCustomerAssetTypeInstitutionNameFieldKey 	= 'REQUIRE_ASSET_DEFAULT_INSTITUTION_NAME';
				$strRequireCustomerAssetTypeNameOnAccountFieldKey 		= 'REQUIRE_ASSET_DEFAULT_NAME_ON_ACCOUNT';
	 			$strRequireCustomerAssetTypeRoutingNumberFieldKey 		= 'REQUIRE_ASSET_DEFAULT_ROUTING_NUMBER';
	 			$strRequireCustomerAssetTypeAccountNumberFieldKey 		= 'REQUIRE_ASSET_DEFAULT_ACCOUNT_NUMBER';
	 			$strRequireCustomerAssetTypeAddressFieldKey 			= 'REQUIRE_ASSET_DEFAULT_ADDRESS';
	 			$strRequireCustomerAssetTypeContactNameFieldKey 		= 'REQUIRE_ASSET_DEFAULT_CONTACT_NAME';
	 			$strRequireCustomerAssetTypeContactPhoneNumberFieldKey 	= 'REQUIRE_ASSET_DEFAULT_CONTACT_PHONE_NUMBER';
	 	 }
 		 */

		if( true == array_key_exists( 'INSTITUTION_NAME', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeInstitutionNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
		  	$boolIsValid &= $this->valInstitutionName( $intCustomerAssetTypeId, $strSectionName );
		}

		$boolIsValid &= $this->valMarketValue( NULL, $intCustomerAssetTypeId, $strSectionName );

		if( true == array_key_exists( 'NAME_ON_ACCOUNT', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeNameOnAccountFieldKey, $arrobjPropertyApplicationPreferences ) ) {
		   	$boolIsValid &= $this->valNameOnAccount( $intCustomerAssetTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'ROUTING_NUMBER', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeRoutingNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {

			$boolIsValid &= $this->valRoutingNumber( $intCustomerAssetTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'ACCOUNT_NUMBER', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeAccountNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {
		   	$boolIsValid &= $this->valAccountNumberEncrypted( $intCustomerAssetTypeId, $strSectionName );
		}

		if( false == $objPostalAddressService->isValid( $this->m_objCustomerAsset->getPostalAddress() ) && true == array_key_exists( 'ADDRESS', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeAddressFieldKey, $arrobjPropertyApplicationPreferences ) && true == $this->m_boolValidateRequiredFields ) {

			$strFieldNamePrefix = 'customer_assets_' . $intCustomerAssetTypeId . '[postal_addresses][default]';

			$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : $this->m_objCustomerAsset->getDefaultSectionName();
				$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();
				foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
					if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->m_objCustomerAsset->getPostalAddress()->getPostalCode() ) ) ) {
						$arrmixExistingData = $objErrorMsg->getData();
						$arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::FINANCIAL ];
						$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
						continue;
					}
					unset( $arrobjErrorMsgs[$intKey] );
				}

			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, $this->m_objCustomerAsset, $strFieldNamePrefix );
				$boolIsValid = false;
			}

		}

   		if( true == array_key_exists( 'CONTACT_NAME', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeContactNameFieldKey, $arrobjPropertyApplicationPreferences ) ) {
		   	$boolIsValid &= $this->valContactName( $intCustomerAssetTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'CONTACT_PHONE', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeContactPhoneNumberFieldKey, $arrobjPropertyApplicationPreferences ) ) {
		   	$boolIsValid &= $this->valContactPhoneNumber( true, $intCustomerAssetTypeId, $strSectionName );

		} elseif( true == array_key_exists( 'CONTACT_PHONE', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) ) {
			$boolIsValid &= $this->valContactPhoneNumber( false, $intCustomerAssetTypeId, $strSectionName );
		}

		if( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intCustomerAssetTypeId, $strSectionName, true, $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_FAX_NUMBER', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && false == array_key_exists( $strRequireCustomerAssetTypeContactFaxFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerAsset->getContactFaxNumber() ) ) {
			$boolIsValid &= $this->valContactFaxNumber( $intCustomerAssetTypeId, $strSectionName, false, $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId]['CONTACT_FAX_NUMBER']['FIELD_NAME'] );
		}

		if( true == array_key_exists( 'CONTACT_EMAIL', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && true == array_key_exists( $strRequireCustomerAssetTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valContactEmail( NULL, $intCustomerAssetTypeId, $strSectionName, true, $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );

		} elseif( true == array_key_exists( 'CONTACT_EMAIL', $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId] ) && false == array_key_exists( $strRequireCustomerAssetTypeContactEmailFieldKey, $arrobjPropertyApplicationPreferences ) && 0 < strlen( $this->m_objCustomerAsset->getContactEmail() ) ) {
			$boolIsValid &= $this->valContactEmail( NULL, $intCustomerAssetTypeId, $strSectionName, false, $arrstrCustomerAssetTypeCustomFields[$intCustomerAssetTypeId]['CONTACT_EMAIL']['FIELD_NAME'] );
		}

		return $boolIsValid;
	}

	public function valAssetEffectiveDate() {

		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomerAsset->getAssetEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_effective', __( 'Effective date is required.' ) ) );
		} elseif( false === CValidation::checkISODateFormat( $this->m_objCustomerAsset->getAssetEffectiveDate(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_effective', __( 'Effective date must be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDateEnded( $boolValidateEndDate ) {

		$boolIsValid = true;

		if( true == $boolValidateEndDate || NULL != $this->m_objCustomerAsset->getDateEnded() ) {
			if( false == valStr( $this->m_objCustomerAsset->getDateEnded() ) ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'End date is required.' ) ) );
			} elseif( false === CValidation::checkISODateFormat( $this->m_objCustomerAsset->getDateEnded(), $boolFormat = true ) ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'End date must be in mm/dd/yyyy format.' ) ) );
			} elseif( true == valStr( $this->m_objCustomerAsset->getAssetEffectiveDate() ) && strtotime( $this->m_objCustomerAsset->getAssetEffectiveDate() ) > strtotime( $this->m_objCustomerAsset->getDateEnded() ) ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'End date should be greater than effective date.' ) ) );
			} elseif( true == valStr( $this->m_objCustomerAsset->getDateDivested() ) && strtotime( $this->m_objCustomerAsset->getDateDivested() ) > strtotime( $this->m_objCustomerAsset->getDateEnded() ) ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_ended', __( 'Dispoal date should be less than end date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateDivested() {

		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomerAsset->getDateDivested() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_divested', __( 'Disposal date is required.' ) ) );
		} elseif( false === CValidation::checkISODateFormat( $this->m_objCustomerAsset->getDateDivested(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_divested', __( 'Disposal date must be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCostToDispose() {
		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomerAsset->getMarketValue() ) && true == valStr( $this->m_objCustomerAsset->getCostToDispose() ) ) {
			if( 0 > $this->m_objCustomerAsset->getCostToDispose() ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost_to_dispose', __( 'Cost to sell or liquidate should be equal or greater than zero.' ) ) );
			} elseif( $this->m_objCustomerAsset->getMarketValue() <= $this->m_objCustomerAsset->getCostToDispose() ) {
				$boolIsValid = false;
				$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost_to_dispose', __( 'Cost to sell or liquidate should be less than market value.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDisposalAmount() {

		$boolIsValid = true;

		if( false == valStr( $this->m_objCustomerAsset->getDisposalAmount() ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disposal_amount', __( 'Sale price is required.' ) ) );
		} elseif( 0 > $this->m_objCustomerAsset->getDisposalAmount() ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'market_value', __( 'Sale price should be equal or greater than zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInterestRate() {
		$boolIsValid = true;

		if( true == valStr( $this->m_objCustomerAsset->getInterestRate() ) && ( $this->m_objCustomerAsset->getInterestRate() > 1 ) ) {
			$boolIsValid = false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interest_rate', __( 'Interest rate should be less than or equal to 100.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $boolIsPrimaryApplicant = true, $strSectionName = NULL, $arrobjPropertyPreferences = NULL ) {
		$boolIsValid = true;
		$strFieldName	= ( 0 < $this->m_objCustomerAsset->getCustomerAssetTypeId() )? 'asset_' . $this->m_objCustomerAsset->getCustomerAssetTypeId() . '_postal_code' : 'postal_code';

	    if( true == $this->m_boolValidateRequiredFields && 0 == strlen( trim( $this->m_objCustomerAsset->getInstitutionPostalCode() ) ) && true == valArr( $arrobjPropertyPreferences ) && ( ( true == $boolIsPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) || ( false == $boolIsPrimaryApplicant && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CO_APPLICANT_EMPLOYER_ZIP_CODE', $arrobjPropertyPreferences ) ) ) ) {
	        $boolIsValid &= false;
	        $this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s,0} Zip/Postal code is required.', [ $strSectionName ] ) ) );

	    } elseif( 0 < strlen( trim( $this->m_objCustomerAsset->getInstitutionPostalCode() ) ) && false == CApplicationUtils::validateZipCode( $this->m_objCustomerAsset->getInstitutionPostalCode(), NULL ) ) {
			$boolIsValid &= false;
			$this->m_objCustomerAsset->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Assets address zip code must be 5 to 10 characters.' ) ) );
		}

	    return $boolIsValid;
	}

	public function validate( $strAction, $boolValidateEndDate, $objCustomerAssetType = NULL, $arrobjPropertyApplicationPreferences = [], $arrstrCustomerAssetTypeCustomFields = [] ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRequiredFields( $objCustomerAssetType, $arrobjPropertyApplicationPreferences, $arrstrCustomerAssetTypeCustomFields );
				break;

			case VALIDATE_DELETE:
				break;

			case 'customer_assets':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerAssetTypeId();
				$boolIsValid &= $this->valRequiredFields( $objCustomerAssetType, $arrobjPropertyApplicationPreferences, $arrstrCustomerAssetTypeCustomFields );
				break;

			case 'validate_resident_asset':
				$boolIsValid &= $this->valAssetEffectiveDate();
				$boolIsValid &= $this->valDateEnded( $boolValidateEndDate );
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerAssetTypeId();
				$boolIsValid &= $this->valMarketValue( $strAction );
				$boolIsValid &= $this->valContactEmail( $strAction );

				if( true == $this->m_objCustomerAsset->getIsDisposed() ) {
					$boolIsValid &= $this->valDateDivested();
					$boolIsValid &= $this->valDisposalAmount();
				}

				$boolIsValid &= $this->valCostToDispose();
				$boolIsValid &= $this->valInterestRate();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}
?>