<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplatePropertyGroups
 * Do not add any new functions to this class.
 */

class CFeeTemplatePropertyGroups extends CBaseFeeTemplatePropertyGroups {

	public static function fetchFeeTemplatePropertyGroupsByFeeTypeIdByCid( $intFeeTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ftpg.*,
						p.id as property_id,
						p.property_name,
						ft.name AS fee_template_name
					FROM
						fee_template_property_groups ftpg
						JOIN fee_templates ft ON ( ft.cid = ftpg.cid AND ft.id = ftpg.fee_template_id )
						LEFT JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						LEFT JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						ftpg.cid = ' . ( int ) $intCid . '
						AND ft.fee_type_id = ' . ( int ) $intFeeTypeId . '
						AND ftpg.deleted_by IS NULL
						AND ftpg.deleted_on IS NULL';

		return self::fetchFeeTemplatePropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objClientDatabase, $boolIsFromViewFeeTemplatePropertyGroup = false ) {

		$strWhereCondition			= '';

		if( true == $boolIsFromViewFeeTemplatePropertyGroup ) {
			$strWhereCondition = ' AND ftpg.deleted_by IS NULL AND ftpg.deleted_on IS NULL';
		}

		$strSql = 'SELECT
						ftpg.*,
						p.id as property_id,
						p.property_name,
						p.is_disabled
					FROM
						fee_template_property_groups ftpg
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						ftpg.fee_template_id = ' . ( int ) $intFeeTemplateId . '
						AND ftpg.cid = ' . ( int ) $intCid . $strWhereCondition;

		return self::fetchFeeTemplatePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByPropertyIdsByFeeTypeIdByCid( $arrintPropertyIds, $intFeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ftpg.*,
						p.id as property_id,
						p.property_name
					FROM
						fee_template_property_groups ftpg
						JOIN fee_templates ft ON ( ft.cid = ftpg.cid AND ft.id = ftpg.fee_template_id )
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						ftpg.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ft.fee_type_id = ' . ( int ) $intFeeTypeId . '
						AND ftpg.deleted_by IS NULL
						AND ftpg.deleted_on IS NULL
						AND ftpg.cid = ' . ( int ) $intCid;

		return self::fetchFeeTemplatePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByPropertyIdsByFeeTemplateIdsByFeeTypeIdByCid( $arrintPropertyAndFeeTemplateIds, $intFeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyAndFeeTemplateIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ftpg.*,
						p.id as property_id,
						p.property_name
					FROM
						fee_template_property_groups ftpg
						JOIN fee_templates ft ON ( ft.cid = ftpg.cid AND ft.id = ftpg.fee_template_id )
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						( p.id, ft.id ) IN   ( ' . implode( ',', $arrintPropertyAndFeeTemplateIds ) . ' )
						AND ft.fee_type_id = ' . ( int ) $intFeeTypeId . '
						AND ftpg.deleted_by IS NULL
						AND ftpg.deleted_on IS NULL
						AND ftpg.cid = ' . ( int ) $intCid;

		return self::fetchFeeTemplatePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByPropertyIdByFeeTypeIdByCid( $intPropertyId, $intFeeTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ftpg.*,
						p.id as property_id,
						p.property_name
					FROM
						fee_template_property_groups ftpg
						JOIN fee_templates ft ON ( ft.cid = ftpg.cid AND ft.id = ftpg.fee_template_id )
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						ftpg.property_group_id = ' . ( int ) $intPropertyId . '
						AND ft.fee_type_id = ' . ( int ) $intFeeTypeId . '
						AND ftpg.cid = ' . ( int ) $intCid . '
						AND ftpg.deleted_by IS NULL
						AND ftpg.deleted_on IS NULL';

		return self::fetchFeeTemplatePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupIdsByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ftpg.property_group_id
					FROM
						fee_template_property_groups ftpg
						JOIN property_groups pg ON ( pg.cid = ftpg.cid AND ftpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						ftpg.fee_template_id = ' . ( int ) $intFeeTemplateId . '
						AND ftpg.cid = ' . ( int ) $intCid . '
						AND ftpg.deleted_by IS NULL AND ftpg.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>