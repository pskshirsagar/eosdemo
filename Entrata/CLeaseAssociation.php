<?php

class CLeaseAssociation extends CBaseLeaseAssociation {

	protected $m_intRateId;
	protected $m_intArCodeId;
	protected $m_intArTriggerId;
	protected $m_intLeaseTermId;
	protected $m_intAddOnGroupId;
	protected $m_intAddOnTypeId;
	protected $m_intLeasingTierSpecialId;

	protected $m_fltCurrentRentAmount;
	protected $m_fltRentAmount;
	protected $m_fltDepositeAmount;
	protected $m_fltReservationAmount;

	protected $m_intCompanyMediaFileId;
	protected $m_arrobjScheduledCharges;

	protected $m_fltRateAmount;

	/**
	 * Get Functions
	 */

	public function getCurrentRentAmount() {
		return $this->m_fltCurrentRentAmount;
	}

	public function getRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getDepositeAmount() {
		return $this->m_fltDepositeAmount;
	}

	public function getReservationAmount() {
		return $this->m_fltReservationAmount;
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getRateAmount() {
		return $this->m_fltRateAmount;
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getRateId() {
		return $this->m_intRateId;
	}

	public function getScheduledCharges() {
		return $this->m_arrobjScheduledCharges;
	}

	public function getLeasingTierSpecialId() {
		return $this->m_intLeasingTierSpecialId;
	}

	public function getAddOnGroupId() {
		return $this->m_intAddOnGroupId;
	}

	public function getAddOnTypeId() {
		return $this->m_intAddOnTypeId;
	}

	public function getCreatedCustomerId() {
		return $this->getDetailsField( 'created_customer_id' );
	}

	/**
	 *Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['lease_term_id'] ) && true == $boolDirectSet ) {
			$this->m_intLeaseTermId = trim( $arrmixValues['lease_term_id'] );
		} elseif( true == isset( $arrmixValues['lease_term_id'] ) ) {
			$this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		}

		if( true == isset( $arrmixValues['rate_amount'] ) && true == $boolDirectSet ) {
			$this->m_fltRateAmount = trim( $arrmixValues['rate_amount'] );
		} elseif( true == isset( $arrmixValues['rate_amount'] ) ) {
			$this->setRateAmount( $arrmixValues['rate_amount'] );
		}

		if( true == isset( $arrmixValues['company_media_file_id'] ) && true == $boolDirectSet ) {
			$this->m_intCompanyMediaFileId = trim( $arrmixValues['company_media_file_id'] );
		} elseif( true == isset( $arrmixValues['company_media_file_id'] ) ) {
			$this->setCompanyMediaFileId( $arrmixValues['company_media_file_id'] );
		}

		if( true == isset( $arrmixValues['ar_trigger_id'] ) && true == $boolDirectSet ) {
			$this->m_intArTriggerId = trim( $arrmixValues['ar_trigger_id'] );
		} elseif( true == isset( $arrmixValues['ar_trigger_id'] ) ) {
			$this->setArTriggerId( $arrmixValues['ar_trigger_id'] );
		}

		if( true == isset( $arrmixValues['ar_code_id'] ) && true == $boolDirectSet ) {
			$this->m_intArTriggerId = trim( $arrmixValues['ar_code_id'] );
		} elseif( true == isset( $arrmixValues['ar_code_id'] ) ) {
			$this->setArCodeId( $arrmixValues['ar_code_id'] );
		}

		if( true == isset( $arrmixValues['rate_id'] ) && true == $boolDirectSet ) {
			$this->m_intRateId = trim( $arrmixValues['rate_id'] );
		} elseif( true == isset( $arrmixValues['rate_id'] ) ) {
			$this->setRateId( $arrmixValues['rate_id'] );
		}

		if( true == isset( $arrmixValues['leasing_tier_special_id'] ) && true == $boolDirectSet ) {
			$this->m_intLeasingTierSpecialId = trim( $arrmixValues['leasing_tier_special_id'] );
		} elseif( true == isset( $arrmixValues['leasing_tier_special_id'] ) ) {
			$this->setLeasingTierSpecialId( $arrmixValues['leasing_tier_special_id'] );
		}

		if( true == isset( $arrmixValues['add_on_group_id'] ) && true == $boolDirectSet ) {
			$this->m_intAddOnGroupId = trim( $arrmixValues['add_on_group_id'] );
		} elseif( true == isset( $arrmixValues['add_on_group_id'] ) ) {
			$this->setAddOnGroupId( $arrmixValues['add_on_group_id'] );
		}

		if( true == isset( $arrmixValues['add_on_type_id'] ) && true == $boolDirectSet ) {
			$this->m_intAddOnTypeId = trim( $arrmixValues['add_on_type_id'] );
		} elseif( true == isset( $arrmixValues['add_on_type_id'] ) ) {
			$this->setAddOnTypeId( $arrmixValues['add_on_type_id'] );
		}

	}

	public function setCurrentRentAmount( $dblCurrentRentAmount ) {
		$this->m_fltCurrentRentAmount = $dblCurrentRentAmount;
	}

	public function setRentAmount( $dblRentAmount ) {
		$this->m_fltRentAmount = $dblRentAmount;
	}

	public function setDepositeAmount( $dblDepositeAmount ) {
		$this->m_fltDepositeAmount = $dblDepositeAmount;
	}

	public function setReservationAmount( $dblReservationAmount ) {
		$this->m_fltReservationAmount = $dblReservationAmount;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = $intLeaseTermId;
	}

	public function setRateAmount( $fltRateAmount ) {
		$this->m_fltRateAmount = $fltRateAmount;
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->m_intCompanyMediaFileId = $intCompanyMediaFileId;
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->m_intArTriggerId = $intArTriggerId;
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	public function setRateId( $intRateId ) {
		$this->m_intRateId = $intRateId;
	}

	public function setScheduledCharges( $arrobjScheduledCharges ) {
		$this->m_arrobjScheduledCharges = $arrobjScheduledCharges;
	}

	public function setLeasingTierSpecialId( $intLeasingTierSpecialId ) {
		$this->m_intLeasingTierSpecialId = CStrings::strToIntDef( $intLeasingTierSpecialId );
	}

	public function setAddOnGroupId( $intAddOnGroupId ) {
		$this->m_intAddOnGroupId = CStrings::strToIntDef( $intAddOnGroupId );
	}

	public function setAddOnTypeId( $intAddOnTypeId ) {
		$this->m_intAddOnTypeId = CStrings::strToIntDef( $intAddOnTypeId );
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->setDetailsField( 'created_customer_id', $intCreatedCustomerId );
	}

	/**
	 * Add Functions
	 */

	public function addScheduledCharge( $objScheduledCharge ) {
		$this->m_arrobjScheduledCharges[$objScheduledCharge->getId()] = $objScheduledCharge;
	}

	public static function createConcreteLeaseAssociation( $arrmixValues ) {

		if( false == valArr( $arrmixValues ) || false == array_key_exists( 'ar_origin_id', $arrmixValues ) ) {
			return new CLeaseAssociation();
		}

		switch( $arrmixValues['ar_origin_id'] ) {
			case CArOrigin::AMENITY:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::AMENITY ) . 'LeaseAssociation';
				break;

			case CArOrigin::PET:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::PET ) . 'LeaseAssociation';
				break;

			case CArOrigin::ADD_ONS:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::ADD_ONS ) . 'LeaseAssociation';
				break;

			case CArOrigin::RISK_PREMIUM:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::RISK_PREMIUM ) . 'LeaseAssociation';
				break;

			case CArOrigin::SPECIAL:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::SPECIAL ) . 'LeaseAssociation';
				break;

			case CArOrigin::MAINTENANCE:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::MAINTENANCE ) . 'LeaseAssociation';
				break;

			default:
				$strClassName = 'CLeaseAssociation';
				break;
		}

		return new $strClassName();
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}
}
?>