<?php

class CPropertyAppointmentHour extends CBasePropertyAppointmentHour {

	protected $m_arrstrDays;

	public function __construct() {
		parent::__construct();

		$this->m_arrstrDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		return;
	}

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Appointment hour id does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Appointment hour client does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Appointment hour property does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valTime() {

		$boolIsValid = true;
		$boolIsValid &= true;

		$strDay = '';
		if( true == isset ( $this->m_arrstrDays[$this->getDay()] ) ) {
			$strDay = $this->m_arrstrDays[$this->getDay()];
		}

		if( ( 0 == strlen( trim( $this->getBeginTime() ) ) && 0 != strlen( trim( $this->getCloseTime() ) ) ) || ( 0 == strlen( trim( $this->getCloseTime() ) ) && 0 != strlen( trim( $this->getBeginTime() ) ) ) ) {
			if( 0 == strlen( trim( $this->getBeginTime() ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '',  $strDay . ' appointment begin time can not be blank.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '',  $strDay . ' appointment close time can not be blank.' ) );
			}

			$boolIsValid &= false;
		}

		if( 0 < strlen( trim( $this->getBeginTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getBeginTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_time', $strDay . ' appointment begin time improperly formatted.' ) );
			$boolIsValid &= false;
		}

		if( 0 < strlen( trim( $this->getCloseTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getCloseTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_time', $strDay . ' appointment close time improperly formatted.' ) );
			$boolIsValid &= false;
		}

		$arrstrCloseTime = explode( ':', $this->getCloseTime() );
		$arrstrBeginTime = explode( ':', $this->getBeginTime() );

		if( 0 < strlen( trim( $this->getCloseTime() ) ) && 0 < strlen( trim( $this->getBeginTime() ) )
			&& true == valArr( $arrstrCloseTime ) && true == valArr( $arrstrBeginTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrCloseTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrBeginTime ) ) {

			$intBeginTime 	= ( ( int ) ( $arrstrBeginTime[0] * 60 ) + ( int ) $arrstrBeginTime[1] );
			$intCloseTime 	= ( ( int ) ( $arrstrCloseTime[0] * 60 ) + ( int ) $arrstrCloseTime[1] );

			if( $intBeginTime > $intCloseTime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strDay . ' appointment begin time is greater than the close time.' ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valDay() {
		$boolIsValid = true;

		if( true == is_null( $this->getDay() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day', 'Day is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTime();
				$boolIsValid &= $this->valDay();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>