<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountLoginTypes
 * Do not add any new functions to this class.
 */

class CGlAccountLoginTypes extends CBaseGlAccountLoginTypes {

	public static function fetchGlAccountLoginTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlAccountLoginType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGlAccountLoginType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlAccountLoginType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>