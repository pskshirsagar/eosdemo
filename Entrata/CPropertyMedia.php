<?php

class CPropertyMedia extends CBasePropertyMedia {

	protected $m_strFullsizeUri;
	protected $m_intCompanyMediaFolderId;
	protected $m_intMediaFileTypeId;
	protected $m_intMediaTypeId;
	protected $m_strFileName;
	protected $m_strExternalUri;
	protected $m_strMediaAlt;

	/**
	* Get Functions
	*
	*/

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	public function getCompanyMediaFolderId() {
		return $this->m_intCompanyMediaFolderId;
	}

	public function getMediaFileTypeId() {
		return $this->m_intMediaFileTypeId;
	}

	public function getMediaTypeId() {
		return $this->m_intMediaTypeId;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getExternalUri() {
		return $this->m_strExternalUri;
	}

	public function getMediaAlt() {
		return $this->m_strMediaAlt;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['fullsize_uri'] ) ) 	$this->setFullsizeUri( $arrstrValues['fullsize_uri'] );
		if( true == isset( $arrstrValues['company_media_folder_id'] ) ) 	$this->setCompanyMediaFolderId( $arrstrValues['company_media_folder_id'] );
		if( true == isset( $arrstrValues['media_file_type_id'] ) ) 	$this->setMediaFileTypeId( $arrstrValues['media_file_type_id'] );
		if( true == isset( $arrstrValues['media_type_id'] ) ) 	$this->setMediaTypeId( $arrstrValues['media_type_id'] );
		if( true == isset( $arrstrValues['file_name'] ) ) 	$this->setFileName( $arrstrValues['file_name'] );
		if( true == isset( $arrstrValues['external_uri'] ) ) 	$this->setExternalUri( $arrstrValues['external_uri'] );
		if( true == isset( $arrstrValues['media_alt'] ) ) 	$this->setMediaAlt( $arrstrValues['media_alt'] );
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->m_strFullsizeUri = $strFullsizeUri;
	}

	public function setCompanyMediaFolderId( $intCompanyMediaFolderId ) {
		$this->m_intCompanyMediaFolderId = $intCompanyMediaFolderId;
	}

	public function setMediaFileTypeId( $intMediaFileTypeId ) {
		$this->m_intMediaFileTypeId = $intMediaFileTypeId;
	}

	public function setMediaTypeId( $intMediaTypeId ) {
		$this->m_intMediaTypeId = $intMediaTypeId;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setExternalUri( $strExternalUri ) {
		$this->m_strExternalUri = $strExternalUri;
	}

	public function setMediaAlt( $strMediaAlt ) {
		$this->m_strMediaAlt = $strMediaAlt;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}
		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$intId = $this->getId();
		if( false == is_numeric( $intId ) || 1 > $intId ) {
			trigger_error( __( 'Invalid File Remove Request: Id not present.' ), E_USER_ERROR );
		}

		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>