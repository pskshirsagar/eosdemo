<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationDuplicates
 * Do not add any new functions to this class.
 */

class CApplicationDuplicates extends CBaseApplicationDuplicates {

	public static function fetchApplicationDuplicateByApplicantApplicationIdByIdByCid( $intApplicantApplicationId, $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT *
					FROM application_duplicates
    				WHERE applicant_application_id = ' . ( int ) $intApplicantApplicationId . '
    				AND id = ' . ( int ) $intId . '
    				AND cid = ' . ( int ) $intCid;

		return self::fetchApplicationDuplicate( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicationDuplicatesByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
        return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM %s WHERE applicant_application_id = %d AND cid = %d ORDER BY created_on DESC', 'application_duplicates', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
    }

    public static function fetchApplicationDuplicateByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT * FROM application_duplicates WHERE applicant_application_id = ' . ( int ) $intApplicantApplicationId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY created_on DESC LIMIT 1';

    	return self::fetchApplicationDuplicate( $strSql, $objDatabase );
    }

    public static function fetchApplicationDuplicateByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					application_duplicates
    				WHERE
    					applicant_application_id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
    				AND cid = ' . ( int ) $intCid;

    	return self::fetchApplicationDuplicates( $strSql, $objDatabase );
    }

    public static function fetchLatestApplicationDuplicatesByApplicationIdsByPsProductIdByCid( $arrintApplicationIds, $intPsProductId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
    	if( false == valArr( $arrintApplicationIds ) ) return;

    	$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

    	$strSubSql = 'SELECT
	    				MAX(c.id) as id,
    					c.cid
    				FROM
    					application_duplicates c
    					INNER JOIN applicant_applications aa ON ( c.applicant_application_id = aa.id AND c.cid = aa.cid AND c.ps_product_id = ' . ( int ) $intPsProductId . ' AND c.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . ' )
    					INNER JOIN applications a ON ( c.applicant_application_id = aa.id AND c.cid = aa.cid AND aa.application_id = a.id AND aa.cid = a.cid )
    					GROUP BY a.id, c.cid HAVING a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';

    	$strSql = 'SELECT
	    				c.*,
	    				date_part(\'day\', (current_date - c.contact_datetime ) ) as last_contact_days,
	    				a.id as application_id
    				FROM
    					application_duplicates c
    					INNER JOIN applicant_applications aa ON ( c.applicant_application_id = aa.id AND c.cid = aa.cid AND c.ps_product_id = ' . ( int ) $intPsProductId . ' AND c.cid = ' . ( int ) $intCid . $strCheckDeletedAASql . ' )
    					INNER JOIN applications a ON ( c.applicant_application_id = aa.id AND c.cid = aa.cid AND aa.application_id = a.id AND aa.cid = a.cid AND a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' ) AND a.cid = ' . ( int ) $intCid . ' )
    					INNER JOIN ( ' . $strSubSql . ' ) latest_contact ON ( latest_contact.id = c.id AND latest_contact.cid = c.cid )';

    	return self::fetchApplicationDuplicates( $strSql, $objDatabase );
    }

    public static function fetchApplicationDuplicatesByApplicationIdsByByContactTypeIdByCid( $arrintApplicationIds, $intContactTypeId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
    	if( false == valArr( $arrintApplicationIds ) ) return;

    	$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

    	$strSql = 'SELECT
						DISTINCT ON ( a.id ) application_id,
						c.id,
						c.cid,
						c.property_id, c.ps_product_id
    				FROM
    					applications a
    					JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
    					JOIN application_duplicates c ON ( c.applicant_application_id = aa.id AND c.cid = aa.cid )
    				WHERE
    					a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' ) AND a.cid = ' . ( int ) $intCid;

    	return self::fetchApplicationDuplicates( $strSql, $objDatabase );
    }

    public static function fetchApplicationDuplicateByApplicantIdByPropertyIdByPsProductIdByCid( $intApplicantId, $intPropertyId, $intPsProductId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
    	$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

    	$strSql = 'SELECT c.*, aa.id as applicant_application_id
    				FROM
    					application_duplicates c,
    					applicant_applications aa
    				WHERE
    					c.applicant_application_id = aa.id
    					AND c.cid = aa.cid
    					AND aa.applicant_id = ' . ( int ) $intApplicantId . '
    					AND c.property_id	= ' . ( int ) $intPropertyId . '
    					AND c.cid = ' . ( int ) $intCid . '
    					AND c.ps_product_id = ' . ( int ) $intPsProductId . '
    					' . $strCheckDeletedAASql . '
    					ORDER By c.id DESC LIMIT 1';

    	return self::fetchApplicationDuplicate( $strSql, $objDatabase );
    }

	public static function fetchApplicationDuplicateByLeadSourceIdByCid( $intLeadSourceId, $intCid,  $objClientDatabase ) {

		$strSql = 'SELECT id FROM application_duplicates WHERE cid = ' . ( int ) $intCid . ' AND lead_source_id = ' . ( int ) $intLeadSourceId . ' LIMIT 1';

    	$arrmixApplicationDuplicates = fetchData( $strSql, $objClientDatabase );

    	return ( 0 == \Psi\Libraries\UtilFunctions\count( $arrmixApplicationDuplicates ) ) ? false : true;
	}

	public static function fetchApplicationDuplicateByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT c.*
					FROM
						application_duplicates c
						JOIN applicant_applications aa ON ( aa.id = c.applicant_application_id AND c.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						aa.applicant_id = ' . ( int ) $intApplicantId . '
					AND aa.application_id = ' . ( int ) $intApplicationId . '
					AND aa.cid = ' . ( int ) $intCid . '
					ORDER BY id DESC LIMIT 1';

		return self::fetchApplicationDuplicate( $strSql, $objDatabase );
	}
}
?>