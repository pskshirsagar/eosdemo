<?php

class CScheduledCallCustomerType extends CBaseScheduledCallCustomerType {

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        }

        return $boolIsValid;
    }

    public function valLeaseStatusTypeId() {
        $boolIsValid = true;

        // Validation example
        if( true == is_null( $this->getLeaseStatusTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_status_type_id', '' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valLeaseStatusTypeId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>