<?php

class CCalendarEventType extends CBaseCalendarEventType {

	const DEFAULT_MAINTENANCE_WORK_ORDER	= 9;
	const DEFAULT_MAINTENANCE_INSPECTION	= 11;
	const DEFAULT_SERVICES_RESERVATION  	= 16;

	protected $m_strCalendarEventCategoryName;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['calendar_event_category_name'] ) ) $this->setCalendarEventCategoryName( $arrmixValues['calendar_event_category_name'] );

    }

    public function getCalendarEventCategoryName() {
    	return $this->m_strCalendarEventCategoryName;
    }

    public function setCalendarEventCategoryName( $strCalendarEventCategoryName ) {
    	$this->m_strCalendarEventCategoryName = $strCalendarEventCategoryName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultCalendarEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarEventCategoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>