<?php

class CArPaymentTransfer extends CBaseArPaymentTransfer {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldPaymentArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPaymentArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>