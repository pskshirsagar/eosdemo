<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetAllocations
 * Do not add any new functions to this class.
 */

class CAssetAllocations extends CBaseAssetAllocations {

	public static function fetchAssetAllocationsByCreditAssetTransactionIdsByCid( $arrintCreditAssetTransactionIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintCreditAssetTransactionIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						asset_allocations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND credit_asset_transaction_id IN ( ' . implode( ', ', $arrintCreditAssetTransactionIds ) . ' )';

		return self::fetchAssetAllocations( $strSql, $objClientDatabase );
	}
}
?>