<?php

use Psi\Eos\Entrata\CMaintenanceRequests;

class CIntegrationQueue extends CBaseIntegrationQueue {

	protected $m_boolIsShowIntegrationError;
	protected $m_strIntegrationDatabaseName;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsShowIntegrationError = false;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getParameters() {
		return str_replace( 'x00~', "\x00", parent::getParameters() );
	}

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getIntegrationDatabaseName() {
		return $this->m_strIntegrationDatabaseName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['database_name'] ) ) {
			$this->setIntegrationDatabaseName( $arrmixValues['database_name'] );
		}
	}

	public function setParameters( $strParameters ) {
		parent::setParameters( str_replace( "\x00", 'x00~', $strParameters ) );
	}

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function setIntegrationDatabaseName( $strIntegrationDatabaseName ) {
		return $this->m_strIntegrationDatabaseName = $strIntegrationDatabaseName;
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchIntegrationClient( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIdByCid( $this->getIntegrationClientId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationResult( $objDatabase ) {
		return CIntegrationResults::fetchIntegrationResultByIdByCid( $this->getIntegrationResultId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Database Functions
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// Integration result get cascade delete and we should not delete it.
		// If we delete it then respective files will stays under mounts folder without reference.
		if( false == is_null( $this->getIntegrationResultId() ) ) {
			$this->setIntegrationResultId( NULL );
			$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Process Functions
	 */

	public function process( $intCompanyUserId, $objDatabase, $objPaymentDatabase = NULL, $boolIsScript = false ) {

		if( true == is_null( $this->getReferenceNumber() ) || false == is_numeric( $this->getReferenceNumber() ) ) {
			return false;
		}

		switch( $this->getIntegrationServiceId() ) {

			case CIntegrationService::MODIFY_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {
					return $objArPayment->exportUpdatedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::MODIFY_REVERSED_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {
					return $objArPayment->exportUpdatedReversedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::REVERSE_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {

					$objArPayment->setIsShowIntegrationError( $this->getIsShowIntegrationError() );
					$boolIsValid = $objArPayment->exportReversedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );

					if( true == $this->getIsShowIntegrationError() && true == valArr( $objArPayment->getErrorMsgs() ) ) {
						$this->addErrorMsgs( $objArPayment->getErrorMsgs() );
					}

					return $boolIsValid;
				}
				break;

			case CIntegrationService::SEND_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {

					$objArPayment->setIsShowIntegrationError( $this->getIsShowIntegrationError() );
					$boolIsValid = $objArPayment->exportArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, NULL, true, $boolIsScript );

					if( true == $this->getIsShowIntegrationError() && true == valArr( $objArPayment->getErrorMsgs() ) ) {
						$this->addErrorMsgs( $objArPayment->getErrorMsgs() );
					}

					return $boolIsValid;
				}
				break;

			case CIntegrationService::RETURN_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {

					$objArPayment->setIsShowIntegrationError( $this->getIsShowIntegrationError() );
					$boolIsValid = $objArPayment->exportReturnedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );

					if( true == $this->getIsShowIntegrationError() && true == valArr( $objArPayment->getErrorMsgs() ) ) {
						$this->addErrorMsgs( $objArPayment->getErrorMsgs() );
					}

					return $boolIsValid;
				}
				break;

			case CIntegrationService::MODIFY_RETURNED_AR_PAYMENT:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {
					return $objArPayment->exportUpdatedReturnedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH:
				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objPaymentDatabase );
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {
					return $objArPayment->exportCloseReturnedPaymentBatch( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::CLOSE_PAYMENT_BATCH:
				$objSettlementDistribution = \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSettlementDistributionById( $this->getReferenceNumber(), $objPaymentDatabase );

				if( true == valObj( $objSettlementDistribution, 'CSettlementDistribution' ) ) {
					return $objSettlementDistribution->processClosePaymentBatchIntegrationClient( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::SEND_AR_TRANSACTIONS:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objLease, 'CLease' ) ) {
					return $objLease->exportArTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::SEND_INSURANCE_TRANSACTIONS:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objLease, 'CLease' ) ) {
					return $objLease->exportInsuranceTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::SEND_MAINTENANCE_REQUEST:
				// We should load the maintenance request from view.- NRW
				$objMaintenanceRequest = CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objMaintenanceRequest, 'CMaintenanceRequest' ) ) {
					return $objMaintenanceRequest->exportMaintenanceRequest( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS:
				// We should load the maintenance request from view.- NRW
				$objMaintenanceRequest = CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objMaintenanceRequest, 'CMaintenanceRequest' ) ) {
					return $objMaintenanceRequest->exportMaintenanceRequestAttachments( $intCompanyUserId, $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_MAINTENANCE_REQUEST:
				// We should load the maintenance request from view.- NRW
				$objMaintenanceRequest = CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objMaintenanceRequest, 'CMaintenanceRequest' ) ) {
					return $objMaintenanceRequest->exportUpdatedMaintenanceRequest( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::CLOSE_MAINTENANCE_REQUEST:
				// We should load the maintenance request from view.- NRW
				$objMaintenanceRequest = CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objMaintenanceRequest, 'CMaintenanceRequest' ) ) {
					return $objMaintenanceRequest->exportClosedMaintenanceRequest( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::SEND_GUEST_CARD:
			case CIntegrationService::SEND_APPLICATION:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					$boolIsValid = $objApplication->export( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );

					if( true == $boolIsValid && CIntegrationService::SEND_APPLICATION == $this->getIntegrationServiceId() ) {
						$objApplication->exportDocuments( $intCompanyUserId, $objDatabase );
					}

					return $boolIsValid;
				}
				break;

			case CIntegrationService::SEND_SCREENING_INFO:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportScreening( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::UPLOAD_APPLICATION_DOCUMENTS:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportDocuments( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::MODIFY_GUEST_CARD:
				$objApplicationDuplicate = CApplicationDuplicates::fetchApplicationDuplicateByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplicationDuplicate, 'CApplicationDuplicate' ) ) {
					return $objApplicationDuplicate->export( $intCompanyUserId, $objDatabase, $objPaymentDatabase, true );
				}
				break;

			case CIntegrationService::EXECUTE_LEASE:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportLease( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::EXECUTE_LEASE_RENEWAL:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportLeaseRenewal( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::UPDATE_APPLICATION:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( false == valObj( $objApplication, 'CApplication' ) || ( true == is_null( $objApplication->getGuestRemotePrimaryKey() ) && true == is_null( $objApplication->getAppRemotePrimaryKey() ) ) ) {
						return true;
					}

					return $objApplication->exportUpdate( $intCompanyUserId, $objDatabase, true );

				}
				break;

			case CIntegrationService::REVERSE_APPLICATION_CHARGES:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportReverseCharges( $intCompanyUserId, $objDatabase, $objPaymentDatabase );
				}
				break;

			case CIntegrationService::UPDATE_CUSTOMER:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objApplicantApplication = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( false == valObj( $objApplicantApplication, 'CApplicantApplication' ) ) {
						return false;
					}

					$objApplication = $objApplicantApplication->getOrFetchApplication( $objDatabase );
					$objApplicant   = $objApplicantApplication->getOrFetchApplicant( $objDatabase );

					if( false == valObj( $objApplication, 'CApplication' ) || false == valObj( $objApplicant, 'CApplicant' ) ) {
						return false;
					}

					$objApplication->setApplicantApplication( $objApplicantApplication );
					$objApplication->setApplicant( $objApplicant );

					$objProperty = $objApplication->getOrFetchProperty( $objDatabase );
					$objCustomer = $objApplicant->getOrFetchCustomer( $objDatabase );
					$objLease    = $objApplication->getOrFetchLease( $objDatabase );

					if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objProperty, 'CProperty' ) || false == valObj( $objLease, 'CLease' ) ) {
						return false;
					}

					return $objCustomer->export( $objProperty, $objDatabase, $objLease, $objApplication );
				}
				break;

			case CIntegrationService::SEND_EVENT:
				$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( false == valObj( $objEvent, 'CEvent' ) ) {
					return false;
				}

				return $objEvent->exportStack( $intCompanyUserId, $objDatabase, NULL, true );
				break;

			case CIntegrationService::UPDATE_EVENT:
				$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objEvent, 'CEvent' ) ) {
					return $objEvent->exportUpdate( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::SEND_UTILITY_TRANSACTIONS:
				$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$objUtilitiesDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::UTILITIES, CDatabaseUserType::PS_PROPERTYMANAGER );
					$boolIsValid          = $objProperty->exportUtilityTransactions( $intCompanyUserId, $objDatabase, $objUtilitiesDatabase, true );
					$objUtilitiesDatabase->close();

					return $boolIsValid;
				}
				break;

			case CIntegrationService::SEND_CUSTOMERS_ADDL_DATA:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {

					$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByFileIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( true == valObj( $objLease, 'CLease' ) ) {

						$objProperty = $objLease->getOrFetchProperty( $objDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {
							$arrobjFiles = CFiles::fetchSimpleFilesByIdsByCid( [ $this->getReferenceNumber() ], $this->getCid(), $objDatabase );

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::SEND_CUSTOMERS_ADDL_DATA, $intCompanyUserId, $objDatabase );

							$objGenericWorker->setFiles( $arrobjFiles );
							$objGenericWorker->setLease( $objLease );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setObjectStorageGateway( CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED ) );
							$objGenericWorker->setQueueSyncOverride( true );
							$objGenericWorker->setIntegrationQueueId( $this->getId() );

							return $objGenericWorker->process();
						}
					}

				}
				break;

			case CIntegrationService::UPDATE_LEASE:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

				if( true == valObj( $objLease, 'CLease' ) ) {
					return $objLease->exportUpdate( $intCompanyUserId, $objDatabase, true );
				}
				break;

			case CIntegrationService::UPLOAD_LEASE_DOCUMENTS:
			case CIntegrationService::UPLOAD_RENEWAL_LEASE_DOCUMENTS:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportLeaseDocuments( $intCompanyUserId, $this->getIntegrationServiceId(), $objDatabase, true );
				}
				break;

			case CIntegrationService::UPDATE_OFFER_ACCEPTED:
				$objApplicationInterest = CApplicationInterests::fetchApplicationInterestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				$objProperty            = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplicationInterest->getPropertyId(), $objApplicationInterest->getCid(), $objDatabase );
				if( true == valObj( $objApplicationInterest, 'CApplicationInterest' ) ) {
					return $objApplicationInterest->exportAcceptOffer( $intCompanyUserId, $objProperty, $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_OFFER_CANCELLED:
				$objApplicationInterest = CApplicationInterests::fetchApplicationInterestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				$objProperty            = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplicationInterest->getPropertyId(), $objApplicationInterest->getCid(), $objDatabase );
				if( true == valObj( $objApplicationInterest, 'CApplicationInterest' ) ) {
					return $objApplicationInterest->exportOfferCancelAcceptance( $intCompanyUserId, $objProperty, $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_MOVE_IN_DATE:
				$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $objApplication->getCid(), $objDatabase );
				if( true == valObj( $objApplication, 'CApplication' ) ) {
					return $objApplication->exportUpdateMoveIn( $intCompanyUserId, $objProperty, $objDatabase );
				}
				break;

			case CIntegrationService::SEND_GUEST_CARD_RECEIPT:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
					if( true == valObj( $objApplication, 'CApplication' ) ) {
						return $objApplication->exportGuestCardReceipt( $intCompanyUserId, $objDatabase, true );
					}
				}
				break;

			case CIntegrationService::SEND_CANCELLED_GUEST_CARD_RECEIPT:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
					if( true == valObj( $objApplication, 'CApplication' ) ) {
						return $objApplication->exportCancelledGuestCardReceipt( $intCompanyUserId, $objDatabase, true );
					}
				}
				break;

			case CIntegrationService::SEND_OFFER:
				$objApplicationInterest = CApplicationInterests::fetchApplicationInterestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objApplicationInterest, 'CApplicationInterest' ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $objApplicationInterest->getApplicationId(), $this->getCid(), $this->m_objDatabase );
					$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplicationInterest->getPropertyId(), $this->getCid(), $this->m_objDatabase );
					$objApplicationInterest->setApplication( $objApplication );

					return $objApplicationInterest->export( $intCompanyUserId, $objProperty, $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_OFFER_DECLINED:
				$objApplicationInterest = CApplicationInterests::fetchApplicationInterestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objApplicationInterest, 'CApplicationInterest' ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $objApplicationInterest->getApplicationId(), $this->getCid(), $this->m_objDatabase );
					$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplicationInterest->getPropertyId(), $this->getCid(), $objDatabase );
					$objApplicationInterest->setApplication( $objApplication );

					return $objApplicationInterest->exportUpdate( $intCompanyUserId, $objProperty, $objApplicationInterest->getReason(), $objDatabase, false );
				}
				break;

			case CIntegrationService::UPDATE_OFFER_EXPIRED:
				$objApplicationInterest = CApplicationInterests::fetchApplicationInterestByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objApplicationInterest, 'CApplicationInterest' ) ) {
					$objApplication = CApplications::fetchApplicationByIdByCid( $objApplicationInterest->getApplicationId(), $this->getCid(), $this->m_objDatabase );
					$objProperty    = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplicationInterest->getPropertyId(), $this->getCid(), $objDatabase );
					$objApplicationInterest->setApplication( $objApplication );

					return $objApplicationInterest->exportUpdate( $intCompanyUserId, $objProperty, $objApplicationInterest->getReason(), $objDatabase, true );
				}
				break;

			case CIntegrationService::UPDATE_MOVE_IN_STATUS:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objLease, 'CLease' ) ) {
					$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objLease->getPropertyId(), $this->getCid(), $objDatabase );

					return $objLease->exportMoveInStatus( $intCompanyUserId, $objProperty, date( 'M-d-Y' ), $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_MOVE_IN_CANCELLED_STATUS:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objLease, 'CLease' ) ) {
					$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objLease->getPropertyId(), $this->getCid(), $objDatabase );

					return $objLease->exportMoveInCancelledStatus( $intCompanyUserId, $objProperty, $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_OCCUPANCY_STATUS:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				if( true == valObj( $objLease, 'CLease' ) ) {
					$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objLease->getPropertyId(), $this->getCid(), $objDatabase );

					if( true == valObj( $objProperty, 'CProperty' ) ) {
						$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_OCCUPANCY_STATUS, $intCompanyUserId, $objDatabase );
						$objGenericWorker->setLease( $objLease );
						$objGenericWorker->setProperty( $objProperty );
						$objGenericWorker->setIntegrationQueueId( $this->getId() );

						return $objGenericWorker->process();
					}
				}
				break;

			case CIntegrationService::UPDATE_UNIT_TRANSFER_STATUS:
				$objLease    = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
				$objOldLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByTransferLeaseIdByCid( $objLease->getId(), $this->getCid(), $this->m_objDatabase );
				if( true == valObj( $objLease, 'CLease' ) ) {
					$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objLease->getPropertyId(), $this->getCid(), $objDatabase );

					return $objLease->exportProcessTransfer( $intCompanyUserId, $objProperty, $objOldLease->getMoveOutDate(), $objDatabase );
				}
				break;

			case CIntegrationService::UPDATE_ON_NOTICE_STATUS:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {

					$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( true == valObj( $objLease, 'CLease' ) ) {

						$objProperty = $objLease->getOrFetchProperty( $objDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_ON_NOTICE_STATUS, $intCompanyUserId, $objDatabase );

							$objGenericWorker->setLease( $objLease );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setIntegrationQueueId( $this->getId() );

							return $objGenericWorker->process();
						}
					}

				}
				break;

			case CIntegrationService::UPDATE_ON_NOTICE_CANCELLED_STATUS:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {

					$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( true == valObj( $objLease, 'CLease' ) ) {

						$objProperty = $objLease->getOrFetchProperty( $objDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_ON_NOTICE_CANCELLED_STATUS, $intCompanyUserId, $objDatabase );

							$objGenericWorker->setLease( $objLease );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setIntegrationQueueId( $this->getId() );

							return $objGenericWorker->process();
						}
					}

				}
				break;

			case CIntegrationService::UPDATE_MOVE_OUT_STATUS:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {

					$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( true == valObj( $objLease, 'CLease' ) ) {

						$objProperty = $objLease->getOrFetchProperty( $objDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_MOVE_OUT_STATUS, $intCompanyUserId, $objDatabase );

							$objGenericWorker->setLease( $objLease );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setIntegrationQueueId( $this->getId() );

							return $objGenericWorker->process();
						}
					}

				}
				break;

			case CIntegrationService::UPDATE_MOVE_OUT_CANCELLED_STATUS:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {

					$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );

					if( true == valObj( $objLease, 'CLease' ) ) {

						$objProperty = $objLease->getOrFetchProperty( $objDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_MOVE_OUT_CANCELLED_STATUS, $intCompanyUserId, $objDatabase );

							$objGenericWorker->setLease( $objLease );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setIntegrationQueueId( $this->getId() );

							return $objGenericWorker->process();
						}
					}

				}
				break;

			case CIntegrationService::SEND_QUOTE:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objApplication = Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
					$objProperty    = Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objApplication->getPropertyId(), $this->getCid(), $objDatabase );

					if( true == valObj( $objApplication, 'CApplication' ) ) {
						return $objApplication->exportQuote( $objProperty, $this->getCid(), $intCompanyUserId, $objDatabase );
					}
				}
				break;

			case CIntegrationService::SEND_RENTERS_INSURANCE_POLICIES:
			case CIntegrationService::UPDATE_RENTERS_INSURANCE_POLICIES:
				if( true == is_numeric( $this->getReferenceNumber() ) ) {
					$objResidentInsurancePolicy = \Psi\Eos\Entrata\CResidentInsurancePolicies::createService()->fetchResidentInsurancePolicyByIdByCid( $this->getReferenceNumber(), $this->getCid(), $objDatabase );
					if( true == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {
						$arrobjInsurancePolicyCustomers = \Psi\Eos\Entrata\CInsurancePolicyCustomers::createService()->fetchCustomInsurancePolicyCustomersByResidentInsurancePolicyIdByCid( $objResidentInsurancePolicy->getId(), $objResidentInsurancePolicy->getCid(), $objDatabase );
						if( true == valArr( $arrobjInsurancePolicyCustomers ) ) {
							$objInsurancePolicyCustomer = current( $arrobjInsurancePolicyCustomers );
							$objLease                   = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objInsurancePolicyCustomer->getLeaseId(), $objResidentInsurancePolicy->getCid(), $objDatabase );
							$objCustomer                = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $objInsurancePolicyCustomer->getCustomerId(), $objResidentInsurancePolicy->getCid(), $objDatabase );
							$objProperty                = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objResidentInsurancePolicy->getPropertyId(), $objResidentInsurancePolicy->getCid(), $objDatabase );
							if( true == valObj( $objCustomer, 'CCustomer' ) ) {
								$objCustomer->setInsurancePolicies( [ $objResidentInsurancePolicy ] );
								$boolIsInsuranceSuccess = $objCustomer->exportInsurance( $objCustomer, $objLease, $objProperty, $objDatabase );

								$objIntegrationClient = $this->fetchIntegrationClient( $objDatabase );
								if( true == $boolIsInsuranceSuccess && true == valObj( $objLease, 'CLease' ) && true == valObj( $objIntegrationClient, 'CIntegrationClient') && CIntegrationClientType::YARDI_RPORTAL == $objIntegrationClient->getIntegrationClientTypeId() ) {
									$objLease->exportInsuranceTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase, false );
								}

								return $boolIsInsuranceSuccess;
							}
						}
					}
				}
				break;

			default:
				trigger_error( 'Unable set reference id to integration queue for service id ( ' . $this->getIntegrationServiceId() . ')', E_USER_ERROR );
				break;
		}

		return false;
	}

}

?>