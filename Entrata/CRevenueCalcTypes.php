<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueCalcTypes
 * Do not add any new functions to this class.
 */

class CRevenueCalcTypes extends CBaseRevenueCalcTypes {

	public static function fetchRevenueCalcTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRevenueCalcType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRevenueCalcType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRevenueCalcType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>