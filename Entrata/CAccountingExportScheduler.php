<?php

class CAccountingExportScheduler extends CBaseAccountingExportScheduler {

	const GL_EXPORT_BOOK_TYPE_ACCRUAL	= 'accrual';
	const GL_EXPORT_BOOK_TYPE_CASH		= 'cash';
	const GL_EXPORT_BOOK_TYPE_BOTH		= 'accrual & cash';
	const SUMMARIZATION_BY_DATE			= 'date';
	protected $m_strEndOn;
	protected $m_intPropertyId;
	protected $m_boolIsValidScheduledEndOn = true;
	protected $m_strPropertyName;

	const LAST_DAY_OF_MONTH = '0';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['end_on'] ) ) {
			$this->setEndOn( $arrmixValues['end_on'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTransmissionVendorId() ) || 0 >= ( int ) $this->getTransmissionVendorId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_vendor_id', __( 'Transmission vendor id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAccountingExportFileFormatTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) || 0 >= ( int ) $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Frequency id required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is required.' ) ) );
		} elseif( false == is_numeric( $this->getFrequencyInterval() ) || 0 > $this->getFrequencyInterval() || 365 < $this->getFrequencyInterval() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && 'occurrences' == $this->getEndOn() ) {
			if( true == is_null( $this->getNumberOfOccurrences() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Number of occurrences is required.' ) ) );
			} elseif( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 == $this->getNumberOfOccurrences() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Invalid number of occurrences.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valWeekDays() {
		$boolIsValid = true;

		if( true == is_null( $this->getWeekDays() ) && ( CFrequency::WEEKLY == $this->getFrequencyId() || CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_days', __( 'Weekday is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMonthDays() {
		$boolIsValid = true;

		if( false == valStr( $this->getMonthDays() ) && CFrequency::MONTHLY == $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_days', __( 'Day(s) of month is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileNamePrefix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileNameSuffix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddresses() {
		$boolIsValid = true;
		if( false != valStr( $this->getEmailAddresses() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddresses() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_addresses', __( 'Invalid Email Address(es).' ) ) );
		}

		return $boolIsValid;
	}

	public function valLastPostedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid = true;
		if( true == valStr( $this->getEndOn() ) && false == is_null( $this->getScheduledEndOn() ) && ( 'date' == $this->getEndOn() || 'occurrences' == $this->getEndOn() ) ) {
			if( true == valStr( $this->getNextPostDate() ) && true == valStr( $this->getScheduledEndOn() ) && false == CValidation::checkDateFormat( $this->getNextPostDate(), true ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'Next Post date is invalid.' ) ) );
			} elseif( true == valStr( $this->getScheduledEndOn() ) && strtotime( $this->getScheduledEndOn() ) < strtotime( $this->getNextPostDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'The end date {%t, 0, DATE_NUMERIC_STANDARD} should be greater than or equal to the next occurrence date {%t, 1, DATE_NUMERIC_STANDARD}.', [ $this->getScheduledEndOn(), $this->getNextPostDate() ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valScheduledStartOn() {

		$boolIsValid = true;
		$intStartDateTime = strtotime( $this->getScheduledStartOn() );
		$strStartDate = date( 'm/d/Y', $intStartDateTime );

		if( false == valStr( $strStartDate ) ) {
			$boolIsValid &= false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'Scheduled start date is required.' ) ) );
			return $boolIsValid;
		}

		if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $strStartDate ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'Scheduled start date is invalid.' ) ) );
			return $boolIsValid;
		}

		if( false == CValidation::checkDateFormat( $strStartDate, true ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start date is invalid.' ) );
		} elseif( false == valId( $this->getId() ) && true == $boolIsValid && strtotime( date( 'm/d/Y' ) ) >= strtotime( $strStartDate ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'Scheduled start on date can\'t be the past or current date.' ) ) );
		}
		if( CFrequency::WEEKLY == $this->getFrequencyId() && date( 'w', $intStartDateTime ) != $this->getWeekDays() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'The week day of scheduled start on date should be same as selected week day.' ) ) );
		}
		if( CFrequency::MONTHLY == $this->getFrequencyId() ) {
			if( '0' == $this->getMonthDays() ) {
				if( date( 'm/t/Y', $intStartDateTime ) != date( 'm/d/Y', $intStartDateTime ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'Scheduled start on date should be the last day of selected month.' ) ) );
				}
			} else {
				if( date( 'd', $intStartDateTime ) != $this->getMonthDays() ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', __( 'The day of scheduled start on date should be same as selected month day.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valScheduledEndOn() {

		$boolIsValid = true;
		$strEndOnDate = date( 'm/d/Y', strtotime( $this->getScheduledEndOn() ) );
		$strStartDate = date( 'm/d/Y', strtotime( $this->getScheduledStartOn() ) );

		if( true == valStr( $this->getEndOn() ) && 'date' == $this->getEndOn() ) {
			if( false == valStr( $this->getScheduledEndOn() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', __( 'Scheduled end on date is required.' ) ) );
				return $boolIsValid;
			}

			if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $strEndOnDate ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', __( 'Scheduled end on date is invalid.' ) ) );

				return $boolIsValid;
			}

			if( false == CValidation::checkDateFormat( $strEndOnDate, true ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', __( 'Scheduled end on date is invalid.' ) ) );
			} elseif( strtotime( $strStartDate ) > strtotime( $strEndOnDate ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', __( 'Scheduled end on date should be greater than or equal to {%t, 0, DATE_NUMERIC_STANDARD}.', [ $strStartDate ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valStartingPostMonth() {

		$boolIsValid = true;
		$arrintStartingPostMonth = explode( '/', $this->getStartingPostMonth() );
		if( 2 == \Psi\Libraries\UtilFunctions\count( $arrintStartingPostMonth ) ) {
			$strStartingPostMonth = $arrintStartingPostMonth[0] . '/1/' . $arrintStartingPostMonth[1];
		}

		if( false == valStr( $this->getStartingPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'starting_post_month', __( 'Starting post month is required.' ) ) );
		} elseif( false == preg_match( '/^[0-9]{2}\/[0-9]{4}$/', $this->getStartingPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'starting_post_month', __( 'Starting post month of format mm/yyyy is required.' ) ) );
		} elseif( false == is_null( $strStartingPostMonth ) && false == CValidation::validateDate( $strStartingPostMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'starting_post_month', __( 'Starting post month is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIncludeAllUnexportedTransactions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncludeFutureTransactions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsZipFiles() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valFrequencyInterval();
				if( false == $this->getIncludeAllUnexportedTransactions() && false != in_array( $this->getTransmissionVendorId(), CTransmissionVendor::$c_arrintAllGlExportTypes ) ) {
					$boolIsValid &= $this->valStartingPostMonth();
				}
				$boolIsValid &= $this->valWeekDays();
				$boolIsValid &= $this->valMonthDays();
				$boolIsValid &= $this->valScheduledStartOn();
				$boolIsValid &= $this->valScheduledEndOn();
				$boolIsValid &= $this->valNumberOfOccurrences();
				$boolIsValid &= $this->valEmailAddresses();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getEndOn() {
		return $this->m_strEndOn;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setEndOn( $strEndOn ) {
		$this->m_strEndOn = $strEndOn;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function createPropertyAccountingExportScheduler() {

		$objPropertyAccountingExportScheduler = new CPropertyAccountingExportScheduler();

		$objPropertyAccountingExportScheduler->setCid( $this->getCid() );
		$objPropertyAccountingExportScheduler->setAccountingExportSchedulerId( $this->getId() );

		return $objPropertyAccountingExportScheduler;
	}

	/**
	 * TODO Remove this function once the task#2325918 implemented for AP, GL and Posi Pay exports.
	 */
	public function calculateScriptStartDate() {

		$boolTakeLastDayOfMonth		= false;
		$boolTakeNextDayFromMonth	= false;
		$strScriptStartDate			= NULL;
		$strPostDate				= $this->getScheduledStartOn();
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= ( true == valStr( $this->getMonthDays() ) ) ? explode( ',', $this->getMonthDays() ) : [];
		$arrmixDayNames				= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];
		$arrmixRelativeFormats		= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];

		if( false == is_numeric( $intFrequencyInterval ) ) {
			return NULL;
		}

		if( strtotime( $strPostDate ) < strtotime( 'today' ) ) {
			$strPostDate = date( 'm/d/Y', strtotime( ' + 1 days ' ) );
		}

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::YEARLY:
				$strScriptStartDate = $strPostDate;
				break;

			case CFrequency::WEEKLY:
				if( true == is_numeric( $this->getWeekDays() ) ) {
					$objStartDatetime	= new DateTime( $strPostDate );
					$objStartDatetime->modify( $arrmixDayNames[$this->getWeekDays()] );
					$strScriptStartDate	= $objStartDatetime->format( 'm/d/Y' );

					// if script start date is passed then calculate next day date
					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objStartDatetime->modify( 'next ' . $arrmixDayNames[$this->getWeekDays()] );
						$strScriptStartDate = $objStartDatetime->format( 'm/d/Y' );
					}
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {
					$intNextPostDay	= $arrintDaysOfMonth[0];
					$intDayKey		= array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey, $arrintDaysOfMonth ) ) {
							$intNextPostDay = $arrintDaysOfMonth[$intDayKey];
						}
						$strScriptStartDate 			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
						$strScriptStartDate 			= date( 'm/d/Y', strtotime( $strScriptStartDate ) );
						$boolTakeNextDayFromMonth		= true;
					} else {

						$arrstrPostDate = explode( '/', $strPostDate );
						$intDay = $arrstrPostDate[1];

						foreach( $arrintDaysOfMonth as $intDate ) {
							if( $intDate > $intDay ) {
								$intNextPostDay				= $intDate;
								$boolTakeNextDayFromMonth	= true;
								break;
							}
						}

						$strScriptStartDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

						if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
							$objNextPostMonthDatetime = new DateTime( $strScriptStartDate );
							$objNextPostMonthDatetime->modify( $intFrequencyInterval . ' month' );
							$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
						}
					}

					if( false == $boolTakeNextDayFromMonth && ( 0 == $intNextPostDay || ( false == $intDayKey && false != array_search( 0, $arrintDaysOfMonth ) ) ) ) {
						$boolTakeLastDayOfMonth = true;
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getWeekDays() ) ) {
					return NULL;
				}

				$strPostDate 		= date( $strPostDate );
				$strDayName 		= $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber	= $arrmixRelativeFormats[$intFrequencyInterval];

				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of this month' );
				$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );

				if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
					$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
					$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				}
				break;

			default:
		}// switch

		$arrstrNextPostDate = explode( '/', $strScriptStartDate );
		if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
			$boolTakeLastDayOfMonth = true;
		}

		if( true == $boolTakeLastDayOfMonth ) {

			$objDateTimeCurrentPostDate = new DateTime( $strPostDate );
			$objDateTimeCurrentPostDate->modify( 'last day of this month' );
			$strScriptStartDate = $objDateTimeCurrentPostDate->format( 'm/d/Y' );
		}

		return date( 'm/d/Y', strtotime( $strScriptStartDate ) );
	}

	/**
	 * TODO Remove this function once the task#2325918 implemented for AP, GL and Posi Pay exports.
	 */
	public function calculateNextPostDate( $objFrequency, $intTransmissionType = NULL ) {
		if( false == is_numeric( $this->getFrequencyInterval() ) ) {
			return NULL;
		}

		$intNextPostDay				= NULL;
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= explode( ',', $this->getMonthDays() );
		$strPostDate				= $this->getNextPostDate();
		if( false != in_array( $intTransmissionType, [ CTransmissionType::AP_EXPORT, CTransmissionType::GL_EXPORT, CTransmissionType::POSITIVE_PAY ] ) ) {
			$strPostDate				= $this->getLastPostedDate();
		}
		$arrmixRelativeFormats	= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];
		$arrmixDayNames			= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::WEEKLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' ' . $objFrequency->getCustomIntervalFrequencyLabelPlural() ) );
				break;

			case CFrequency::YEARLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' ' . $objFrequency->getCustomIntervalFrequencyLabelPlural() ) );

				if( date( 'm', strtotime( $strPostDate ) ) != date( 'm', strtotime( $strNextPostDate ) ) ) {

					$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
					$objDateTimeNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {

					$intDayKey = array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey + 1, $arrintDaysOfMonth ) ) {

							$intNextPostDay = $arrintDaysOfMonth[$intDayKey + 1];

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								}

							} else {

								$strNextPostDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

								$arrstrNextPostDate = explode( '/', $strNextPostDate );

								if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {

									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/01/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( 'last day of this month' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							}
						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

						}
					} else {

						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintDaysOfMonth ) ) {

							$intNextPostDay = current( $arrintDaysOfMonth );

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

							} else {

								$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
								$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
							}

						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
						}
					}
				}

				$objPostDate = new DateTime( $strPostDate );
				$objPostDate->modify( 'first day of this month' );

				$objNextPostDate = new DateTime( $strNextPostDate );
				$objNextPostDate->modify( 'first day of this month' );

				$objInterval = date_diff( $objPostDate, $objNextPostDate );
				if( $objInterval->format( '%m' ) != $intFrequencyInterval && $objInterval->format( '%m' ) != 0 ) {

					$intNextPostDay		= current( $arrintDaysOfMonth );
					$objNextPostDate	= new DateTime( $strNextPostDate );
					$objNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate	= $objNextPostDate->format( 'm/d/Y' );

					if( 0 != $intNextPostDay ) {

						$strNextPostDateCopy = $strNextPostDate;

						$strNextPostDate	= date( 'm', strtotime( $strNextPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strNextPostDate ) );
						$arrstrNextPostDate	= explode( '/', $strNextPostDate );

						if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
							$strNextPostDate = $strNextPostDateCopy;
						}
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				$strDayName = $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber = $arrmixRelativeFormats[$intFrequencyInterval];
				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
				$strNextPostDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				break;

			default:
		}
		return date( 'm/d/Y', strtotime( $strNextPostDate ) );
	}

	public function calculateEndDate( $objFrequency ) {

		if( false == is_numeric( $this->getNumberOfOccurrences() ) && 0 == $this->getNumberOfOccurrences() ) {
			return NULL;
		}

		$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() ) );
		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
			case CFrequency::YEARLY:
			case CFrequency::WEEKLY:
				$intNumberOfIntervalUnits	= ( $this->getNumberOfOccurrences() - 1 ) * $this->getFrequencyInterval();
				$strIntervalLabel			= $objFrequency->getCustomIntervalFrequencyLabelPlural();
				if( 0 < $intNumberOfIntervalUnits ) {
					$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() . ' +' . $intNumberOfIntervalUnits . ' ' . $strIntervalLabel ) );
				}
				break;

			case CFrequency::MONTHLY:
			case CFrequency::WEEKDAY_OF_MONTH:

				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getWeekDays() ) ) {
					return NULL;
				}

				if( CFrequency::MONTHLY == $this->getFrequencyId() && false == valStr( $this->getMonthDays() ) ) {
					return NULL;
				}

				$strScriptStartDate		= $this->getNextPostDate();
				$intNumberOfOccurrences	= $this->getNumberOfOccurrences();

				for( $intCounter = 2; $intCounter <= $intNumberOfOccurrences; $intCounter++ ) {

					$strEndDate = $this->calculateNextPostDate( $objFrequency );
					$this->setNextPostDate( $strEndDate );
				}
				$this->setNextPostDate( $strScriptStartDate );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strEndDate ) );
	}

	public function getCalculatedLastPostedDate() {
		if( ( false != in_array( $this->getFrequencyId(), CFrequency::$c_arrintPeriodFrequencies ) ) ) {
			$objDateTime = new DateTime( 'now' );
			return $objDateTime->format( 'm/d/Y' );
		} else {
			if( false == valStr( $this->getLastPostedDate() ) ) return $this->getScheduledStartOn();
		}

		$objDateTime	= new DateTime( date( 'm/d/Y', strtotime( $this->getLastPostedDate() ) ) );

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
				if( 1 == $this->getFrequencyInterval() ) {
					$objDateTime = new DateTime( 'now' );
				} else {
					$objDateTime->modify( '+' . $this->getFrequencyInterval() . ' days' );
				}
				break;

			case CFrequency::WEEKLY:
				$objDateTime->modify( '+' . $this->getFrequencyInterval() . ' weeks' );
				break;

			case CFrequency::MONTHLY:
				if( self::LAST_DAY_OF_MONTH == $this->getMonthDays() ) {
					$objDateTime->modify( 'last day of +' . $this->getFrequencyInterval() . ' months' );
				} else {
					$objDateTime->modify( '+' . $this->getFrequencyInterval() . ' months' );
				}
				break;

			default:
				// do nothing
				break;
		}

		return $objDateTime->format( 'm/d/Y' );
	}

	public function isResetLastPostedDate() {
		$arrstrTargetColumns = [ 'FrequencyId', 'WeekDays', 'MonthDays' ];
		return ( true == valArr( $this->getChangedColumns() ) && true == valArr( array_intersect( array_keys( $this->getChangedColumns() ), $arrstrTargetColumns ) ) ) ? true : false;
	}
}
?>