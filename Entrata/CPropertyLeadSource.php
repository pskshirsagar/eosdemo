<?php

class CPropertyLeadSource extends CBasePropertyLeadSource {

	protected $m_intIsAllowPhoneExtension;
	protected $m_intInternetListingServiceId;

	protected $m_strPropertyLeadSourceName;
	protected $m_strPropertyName;

	/**
	 * Get Functions
	 */

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function getPropertyLeadSourceName() {
		return $this->m_strPropertyLeadSourceName;
	}

	public function getIsAllowPhoneExtension() {
		return $this->m_intIsAllowPhoneExtension;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * @deprecated Doesn't handle international phone number formatting.
	 * @see        \i18n\CPhoneNumber::format() should be used.
	 */
	public function formatPhoneNumber() {

		$strTempPhoneNumber = '';

		$strPhoneNumber = preg_replace( '/\D/', '', $this->getPhoneNumber() );

		if( 0 < strlen( $strPhoneNumber ) ) {

			$intLength = \Psi\CStringService::singleton()->strlen( $strPhoneNumber ) - 6;

			$strTempPhoneNumber = \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 3 );
			$strTempPhoneNumber .= '-';
			$strTempPhoneNumber .= \Psi\CStringService::singleton()->substr( $strPhoneNumber, 3, 3 );
			$strTempPhoneNumber .= '-';
			$strTempPhoneNumber .= \Psi\CStringService::singleton()->substr( $strPhoneNumber, 6, $intLength );

		}

		return $strTempPhoneNumber;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['property_lead_source_name'] ) ) $this->setPropertyLeadSourceName( $arrmixValues['property_lead_source_name'] );
        if( true == isset( $arrmixValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrmixValues['internet_listing_service_id'] );
        if( true == isset( $arrmixValues['is_allow_phone_extension'] ) ) $this->setIsAllowPhoneExtension( $arrmixValues['is_allow_phone_extension'] );
        if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );

		return;
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->m_intInternetListingServiceId = $intInternetListingServiceId;
	}

	public function setPropertyLeadSourceName( $strPropertyLeadSourceName ) {
		$this->m_strPropertyLeadSourceName = $strPropertyLeadSourceName;
	}

	public function setIsAllowPhoneExtension( $intIsAllowPhoneExtension ) {
		$this->m_intIsAllowPhoneExtension = CStrings::strToIntDef( $intIsAllowPhoneExtension, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLeadSourceId( $objLeadSource, $objDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objLeadSource, 'CLeadSource' ) && false == is_null( $objLeadSource->getInternetListingServiceId() ) && CInternetListingService::SYNDICATION_ILS != $objLeadSource->getInternetListingServiceId() ) {

			$intConflictingPropertyLeadSourceCount = \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchConflictingPropertyLeadSourceCountByInternetListingServiceIdByCid( $this, $objLeadSource->getInternetListingServiceId(), $this->getCid(), $objDatabase );

			if( 0 < $intConflictingPropertyLeadSourceCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internet_listing_service_id', __( 'Lead source with same ILS already exists.' ), 504 ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valCallPhoneNumberId( $objDatabase ) {
		$boolIsValid = true;
		$strWhereSql = 'WHERE call_phone_number_id = ' . $this->getCallPhoneNumberId() . ' AND id <> ' . $this->getId() . ' AND deleted_on IS NULL';

		if( true == valId( $this->getCallPhoneNumberId() ) && 0 != \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceCount( $strWhereSql, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_phone_number_id', __( 'Vanity number is already associated to another lead source.' ), 504 ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolIsRequired = false ) {

		$boolIsValid = true;
		$strPhoneNumber   = $this->getPhoneNumber();
		$objPhoneNumber = $this->createPhoneNumber( $strPhoneNumber );

		$intPhoneLength	= strlen( $objPhoneNumber->getNumber() );
		if( true == $boolIsRequired ) {
			if( 0 == $intPhoneLength ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
			}
		}

		if( true == valStr( $objPhoneNumber->getNumber() ) && 0 < $intPhoneLength && false == $objPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == isset( $this->m_strEmailAddress ) || ( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'The email address does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objLeadSource = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLeadSourceId( $objLeadSource, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_call_phone_number_id':
				$boolIsValid &= $this->valCallPhoneNumberId( $objDatabase );
				break;

			case 'validate_phone_number':
				$boolIsValid &= $this->valPhoneNumber( false );
				break;

			case 'validate_email_address':
				$boolIsValid &= $this->valEmailAddress( false );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Validation Functions
	 */

	public function fetchLeadSourcesByIds( $arrintLeadSourceIds, $objDatabase ) {

		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdsByCid( $arrintLeadSourceIds, $this->getCid(), $objDatabase );
	}

	/**
	 * override base functions
	 */

	public function update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setPropertyLeadSourceOriginalValues( $objDatabase );

		if( true == $boolReturnSqlOnly ) {

			$strSql = '';
			$strSql .= parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = true );

			$strSql .= $this->insertTableLogs( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = true );

			return $strSql;

		} else {

			$boolIsValid = true;
			$boolIsValid &= parent::update( $intCompanyUserId, $objDatabase );

			if( false === $boolIsValid ) return false;

			$this->insertTableLogs( $intCompanyUserId, $objDatabase );

			return $boolIsValid;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'UPDATE public.property_lead_sources SET is_published = 0, deleted_on = now(), deleted_by = ' . ( int ) $intCurrentUserId . '  WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = '
			SELECT
				pls.*
			FROM
				property_lead_sources pls
				JOIN lead_sources ls ON ( ls.cid = pls.cid AND pls.lead_source_id = ls.id )
			WHERE
				pls.cid = ' . ( int ) $this->sqlCid() . '
				AND pls.property_id =  ' . $this->sqlPropertyId() . '
				AND pls.lead_source_id =  ' . ( int ) $this->sqlLeadSourceId() . ' LIMIT 1';

		$arrobjValues = \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSource( $strSql, $objDatabase );

		if( true == is_object( $arrobjValues ) ) {
			$strSql = 'UPDATE public.property_lead_sources SET is_published = 1, deleted_on = NULL, deleted_by = NULL  WHERE id = ' . ( int ) $arrobjValues->getId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';
		} else {
			$strSql = CBasePropertyLeadSource::insert( $intCurrentUserId, $objDatabase, true );
		}

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	/**
	 * Other Functions
	 */

	private function setPropertyLeadSourceOriginalValues( $objDatabase ) {

		$strSql = ' SELECT
						sms_keyword_id,
						phone_number,
						email_address,
						monthly_fixed_cost,
						per_lead_cost,
						per_lease_cost,
						one_time_cost,
						is_published
					 FROM
		 				property_lead_sources
		 			 WHERE
		 				id = ' . ( int ) $this->getId() . '
		 				AND cid = ' . ( int ) $this->getCid() . '
		 				AND property_id = ' . ( int ) $this->getPropertyId();

		$arrstrMixResponse = ( array ) fetchData( $strSql, $objDatabase );

		$this->setOriginalValues( $arrstrMixResponse[0] );
		$this->setSerializedOriginalValues( serialize( $arrstrMixResponse[0] ) );
	}

	public function insertTableLogs( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql                           = '';
		$strPropertyLeadSourceOldValue    = NULL;
		$strPropertyLeadSourceNewValue    = NULL;
		$strPropertyLeadSourceDiscription = NULL;

		if( $this->getOriginalValueByFieldName( 'sms_keyword_id' ) != $this->getSmsKeywordId() ) {
			$strPropertyLeadSourceOldValue .= 'sms_keyword_id::' . $this->getOriginalValueByFieldName( 'sms_keyword_id' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'sms_keyword_id::' . $this->getSmsKeywordId() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'phone_number' ) != $this->getPhoneNumber() ) {
			$strPropertyLeadSourceOldValue .= 'phone_number::' . $this->getOriginalValueByFieldName( 'phone_number' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'phone_number::' . $this->getPhoneNumber() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'email_address' ) != $this->getEmailAddress() ) {
			$strPropertyLeadSourceOldValue .= 'email_address::' . $this->getOriginalValueByFieldName( 'email_address' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'email_address::' . $this->getEmailAddress() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'monthly_fixed_cost' ) != $this->getMonthlyFixedCost() ) {
			$strPropertyLeadSourceOldValue .= 'monthly_fixed_cost::' . $this->getOriginalValueByFieldName( 'monthly_fixed_cost' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'monthly_fixed_cost::' . $this->getMonthlyFixedCost() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'per_lead_cost' ) != $this->getPerLeadCost() ) {
			$strPropertyLeadSourceOldValue .= 'per_lead_cost::' . $this->getOriginalValueByFieldName( 'per_lead_cost' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'per_lead_cost::' . $this->getPerLeadCost() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'per_lease_cost' ) != $this->getPerLeaseCost() ) {
			$strPropertyLeadSourceOldValue .= 'per_lease_cost::' . $this->getOriginalValueByFieldName( 'per_lease_cost' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'per_lease_cost::' . $this->getPerLeaseCost() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'one_time_cost' ) != $this->getOneTimeCost() ) {
			$strPropertyLeadSourceOldValue .= 'one_time_cost::' . $this->getOriginalValueByFieldName( 'one_time_cost' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'one_time_cost::' . $this->getOneTimeCost() . '~^~';
		}

		if( $this->getOriginalValueByFieldName( 'is_published' ) != $this->getIsPublished() ) {
			$strPropertyLeadSourceOldValue .= 'is_published::' . $this->getOriginalValueByFieldName( 'is_published' ) . '~^~';
			$strPropertyLeadSourceNewValue .= 'is_published::' . $this->getIsPublished() . '~^~';
		}

		if( false == is_null( $strPropertyLeadSourceOldValue ) || false == is_null( $strPropertyLeadSourceNewValue ) ) {
			$strPropertyLeadSourceDiscription = 'Property Lead Source record has been modified' . $strPropertyLeadSourceDiscription;
			$objTableLog                      = new CTableLog();

			$strReturnValue = $objTableLog->insert( $intCompanyUserId, $objDatabase, 'property_lead_sources', $this->getLeadSourceId(), 'UPDATE', $this->getCid(), $strPropertyLeadSourceOldValue, $strPropertyLeadSourceNewValue, $strPropertyLeadSourceDiscription, $boolReturnSqlOnly );

			if( true == $boolReturnSqlOnly ) {
				$strSql .= $strReturnValue;
			}
		}

		return ( true == $boolReturnSqlOnly ) ? $strSql : true;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}

?>