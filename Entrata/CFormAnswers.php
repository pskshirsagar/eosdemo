<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFormAnswers
 * Do not add any new functions to this class.
 */

class CFormAnswers extends CBaseFormAnswers {

	public static function fetchApplicationFormAnswersByFormIdByDocumentComponentIdsByCid( $intFormtId, $arrintDocumentComponentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentComponentIds ) ) return NULL;

		$strSql = 'SELECT * FROM
					 	form_answers
					WHERE
						cid = ' . ( int ) $intCid . '
					  	AND form_id = ' . ( int ) $intFormtId . '
					  	AND document_component_id IN ( ' . implode( ',', $arrintDocumentComponentIds ) . ' )';

		return self::fetchFormAnswers( $strSql, $objDatabase );
	}

	public static function fetchFormAnswersByFormIdsByCid( $arrintFormIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFormIds ) || false == valId( $intCid ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM
					 	form_answers
					WHERE
						cid = ' . ( int ) $intCid . '
					  	AND form_id IN ( ' . implode( ',', $arrintFormIds ) . ' )';

		return self::fetchFormAnswers( $strSql, $objDatabase );
	}

	public static function fetchCustomFormAnswersByFormIdByCid( $intFormtId, $intCid, $objDatabase ) {
		// To avoid 'tag' value in question field the check is applied : MSN

		$strSql = 'SELECT * FROM
					 	form_answers
					 WHERE
					 	cid = ' . ( int ) $intCid . '
					 	AND	form_id = ' . ( int ) $intFormtId . '
					 	AND question != \'tag\'';

		return self::fetchFormAnswers( $strSql, $objDatabase );
	}

	public static function fetchFormAnswersByApplicationIdByTagsByCid( $intApplicationId, $arrstrTags, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrstrTags ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$arrstrTags = array_map( 'addslashes', $arrstrTags );

		$strSql = 'SELECT
						fa.*
					FROM
						form_answers fa
						JOIN applicant_applications aa ON ( aa.application_form_id = fa.form_id AND aa.cid = fa.cid AND aa.application_id = ' . ( int ) $intApplicationId . $strCheckDeletedAASql . ' )
					WHERE
						fa.cid = ' . ( int ) $intCid . '
						AND fa.tag IN ( \'' . implode( '\',\'', $arrstrTags ) . '\' )';

		return self::fetchFormAnswers( $strSql, $objDatabase );
	}

	public static function fetchFormAnswersByDocumentIdCustomerIdByCid( $intDocumentId, $intCustomerId, $intCid, $objDatabase, $intMaintenanceRequestId = NULL ) {
        $strSql = 'SELECT 
                        fa.*
                   FROM
                        form_answers fa
                        JOIN forms f ON ( f.id = fa.form_id AND f.cid = fa.cid )
                    WHERE
                        fa.cid = ' . ( int ) $intCid . '
                        AND f.document_id = ' . ( int ) $intDocumentId . '
                        AND f.customer_id = ' . ( int ) $intCustomerId;
        
        if ( false == is_null( $intMaintenanceRequestId ) ) {
            $strSql .= ' AND f.maintenance_request_id = ' . ( int ) $intMaintenanceRequestId;
        }
        
        return self::fetchFormAnswers( $strSql, $objDatabase );
    }

}
?>