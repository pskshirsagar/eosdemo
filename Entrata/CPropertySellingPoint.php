<?php

class CPropertySellingPoint extends CBasePropertySellingPoint {

	protected $m_strCategoryName;
	protected $m_strAmenityName;
	protected $m_strFullSizeUri;

	/**
	 * construct functions
	 */

	/**
	 * Setter Getter functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['category_name'] ) ) $this->setCategoryName( $arrmixValues['category_name'] );
		if( true == isset( $arrmixValues['amenity_name'] ) ) $this->setAmenityName( $arrmixValues['amenity_name'] );
		if( true == isset( $arrmixValues['full_size_uri'] ) ) $this->setFullSizeUri( $arrmixValues['full_size_uri'] );
		return;
	}

	public function setCategoryName( $strCategoryName ) {
		$this->m_strCategoryName = $strCategoryName;
	}

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = $strAmenityName;
	}

	public function setFullSizeUri( $strFullSizeUri ) {
		$this->m_strFullSizeUri = $strFullSizeUri;
	}

	public function getCategoryName() {
		return $this->m_strCategoryName;
	}

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function getFullSizeUri() {
		return $this->m_strFullSizeUri;
	}

	/**
	 * Validate functions
	 */

	public function valPropertySellingPointCategoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertySellingPointCategoryId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_selling_point_category_id', __( 'Category is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateAssociationId() {
		$boolIsValid = true;

		if( false == valId( $this->getRateAssociationId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_selling_point_amenity_id', __( 'Amenity is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTitle( $boolIsValidateRateAssociationId = false ) {
		$boolIsValid = true;

		if( true == $boolIsValidateRateAssociationId && CPropertySellingPointCategory::FEATURED == $this->getPropertySellingPointCategoryId() ) {
			$boolIsValid &= $this->valRateAssociationId();
		} elseif( true == is_null( $this->getTitle() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertySellingPointCategoryId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_rate_association':
				$boolIsValid &= $this->valPropertySellingPointCategoryId();
				$boolIsValid &= $this->valTitle( $boolIsValidateRateAssociationId = true );
				$boolIsValid &= $this->valDescription();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>