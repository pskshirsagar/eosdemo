<?php

class CAddOnReservationStatusType extends CBaseAddOnReservationStatusType {

	const PENDING	= 1;
	const REJECTED	= 2;
	const APPROVED	= 3;
	const CANCELED	= 4;

	public static $c_arrstrAllAddOnReservationStatusTypes = [
		self::PENDING	=> 'Pending',
		self::REJECTED	=> 'Rejected',
		self::APPROVED	=> 'Approved',
		self::CANCELED	=> 'Canceled'
	];

	public static $c_arrstrActiveAddOnReservationStatusTypes = [
		self::PENDING,
		self::APPROVED
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
