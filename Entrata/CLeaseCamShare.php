<?php

class CLeaseCamShare extends CBaseLeaseCamShare {

	protected $m_strCamCalculationTypeName;
	protected $m_strGlAccountName;
	protected $m_strPropertyCamPoolName;

	protected $m_intLeaseId;
	protected $m_intPoolSquareFeet;
	protected $m_intGlAccountId;

	protected $m_fltLeaseCamShareAmount;
	protected $m_fltChargeAmount;
	protected $m_fltPropertyCamBudgetAmount;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == $boolDirectSet && true === isset( $arrmixValues['cam_calculation_type_name'] ) ) {
			$this->m_strCamCalculationTypeName = trim( $arrmixValues['cam_calculation_type_name'] );
		} elseif( true === isset( $arrmixValues['cam_calculation_type_name'] ) ) {
			$this->setCamCalculationTypeName( $arrmixValues['cam_calculation_type_name'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['cam_calculation_type_id'] ) ) {
			$this->m_intCamCalculationTypeId = trim( $arrmixValues['cam_calculation_type_id'] );
		} elseif( true === isset( $arrmixValues['cam_calculation_type_id'] ) ) {
			$this->setCamCalculationTypeId( $arrmixValues['cam_calculation_type_id'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['gl_account_id'] ) ) {
			$this->m_intGlAccountId = trim( $arrmixValues['gl_account_id'] );
		} elseif( true === isset( $arrmixValues['gl_account_id'] ) ) {
			$this->setGlAccountId( $arrmixValues['gl_account_id'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['gl_account_name'] ) ) {
			$this->m_strGlAccountName = trim( $arrmixValues['gl_account_name'] );
		} elseif( true === isset( $arrmixValues['gl_account_name'] ) ) {
			$this->setGlAccountName( $arrmixValues['gl_account_name'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['lease_cam_share_amount'] ) ) {
			$this->m_fltLeaseCamShareAmount = trim( $arrmixValues['lease_cam_share_amount'] );
		} elseif( true === isset( $arrmixValues['lease_cam_share_amount'] ) ) {
			$this->setLeaseCamShareAmount( $arrmixValues['lease_cam_share_amount'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['property_cam_budget_amount'] ) ) {
			$this->m_fltPropertyCamBudgetAmount = trim( $arrmixValues['property_cam_budget_amount'] );
		} elseif( true === isset( $arrmixValues['property_cam_budget_amount'] ) ) {
			$this->setPropertyCamBudgetAmount( $arrmixValues['property_cam_budget_amount'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['charge_amount'] ) ) {
			$this->m_fltChargeAmount = trim( $arrmixValues['charge_amount'] );
		} elseif( true === isset( $arrmixValues['charge_amount'] ) ) {
			$this->setChargeAmount( $arrmixValues['charge_amount'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['property_cam_pool_name'] ) ) {
			$this->m_strPropertyCamPoolName = trim( $arrmixValues['property_cam_pool_name'] );
		} elseif( true === isset( $arrmixValues['property_cam_pool_name'] ) ) {
			$this->setPropertyCamPoolName( $arrmixValues['property_cam_pool_name'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['pool_square_feet'] ) ) {
			$this->m_intPoolSquareFeet = trim( $arrmixValues['pool_square_feet'] );
		} elseif( true === isset( $arrmixValues['pool_square_feet'] ) ) {
			$this->setPoolSquareFeet( $arrmixValues['pool_square_feet'] );
		}

		if( true == $boolDirectSet && true === isset( $arrmixValues['lease_id'] ) ) {
			$this->m_intLeaseId = trim( $arrmixValues['lease_id'] );
		} elseif( true === isset( $arrmixValues['lease_id'] ) ) {
			$this->setLeaseId( $arrmixValues['lease_id'] );
		}

	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false === valId( $this->m_intPropertyId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLeaseCamPoolId() {
		$boolIsValid = true;
		if( false === valId( $this->m_intLeaseCamPoolId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_cam_pool_id', __( 'Lease CAM pool id is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCamCalculationTypeId() {
		$boolIsValid = true;
		if( false === valId( $this->m_intCamCalculationTypeId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cam_calculation_type_id', __( 'Rate type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valShareAmount() {
		$boolIsValid = true;
		if( false === is_numeric( $this->m_fltShareAmount ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'share_amount', __( 'Share amount is required.' ) ) );
		}
		if( 0 >= $this->m_fltShareAmount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'share_amount', __( 'Share amount should be greater than 0.' ) ) );
		} elseif( CCamCalculationType::PERCENT == $this->m_intCamCalculationTypeId && 100 < $this->m_fltShareAmount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'share_amount', __( 'Share amount percent should not be greater than 100.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCapAmount() {
		$boolIsValid = true;
		if( true === isset( $this->m_intCapAmount ) && ( 0 > $this->m_intCapAmount || 100 < $this->m_intCapAmount ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_amount', __( 'GL Account Reimbursement Limit should be between 0 to 100.' ) ) );
		}
		return $boolIsValid;
	}

	public function getCamCalculationTypeName() {
		return $this->m_strCamCalculationTypeName;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getLeaseCamShareAmount() {
		return $this->m_fltLeaseCamShareAmount;
	}

	public function getPropertyCamBudgetAmount() {
		return $this->m_fltPropertyCamBudgetAmount;
	}

	public function getPropertyCamPoolName() {
		return $this->m_strPropertyCamPoolName;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function getPoolSquareFeet() {
		return $this->m_intPoolSquareFeet;
	}

	public function setCamCalculationTypeName( string $strCamCalculationTypeName ) {
		return $this->m_strCamCalculationTypeName = $strCamCalculationTypeName;
	}

	public function setCamCalculationTypeId( $intCamCalculationTypeId ) {
		return $this->m_intCamCalculationTypeId = $intCamCalculationTypeId;
	}

	public function setGlAccountName( string $strGlAccountName ) {
		return $this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountId( int $intGlAccountId ) {
		return $this->m_intGlAccountId = $intGlAccountId;
	}

	public function setLeaseCamShareAmount( float $fltLeaseCamShareAmount ) {
		return $this->m_fltLeaseCamShareAmount = $fltLeaseCamShareAmount;
	}

	public function setPropertyCamBudgetAmount( float $fltPropertyCamBudgetAmount ) {
		return $this->m_fltPropertyCamBudgetAmount = $fltPropertyCamBudgetAmount;
	}

	public function setPropertyCamPoolName( string $strPropertyCamPoolName ) {
		return $this->m_strPropertyCamPoolName = $strPropertyCamPoolName;
	}

	public function setLeaseId( $intLeaseId ) {
		return $this->m_intLeaseId = $intLeaseId;
	}

	public function setChargeAmount( $fltChargeAmount ) {
		return $this->m_fltChargeAmount = $fltChargeAmount;
	}

	public function setPoolSquareFeet( $intPoolSquareFeet ) {
		return $this->m_intPoolSquareFeet = $intPoolSquareFeet;
	}

	public function validate( string $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLeaseCamPoolId();
				$boolIsValid &= $this->valCamCalculationTypeId();
				$boolIsValid &= $this->valShareAmount();
				$boolIsValid &= $this->valCapAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( false === valId( $intCurrentUserId ) || false === valObj( $objDatabase, 'CDatabase' ) ) return false;

		$this->createCamSharesActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_SHARE_DELETED, $objDatabase );

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->createCamSharesActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_SHARE_ADDED, $objDatabase );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objOriginalLeaseCamShare = $this->fetchOriginalLeaseCamShare( $objDatabase );
		$this->createCamSharesActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_SHARE_UPDATED, $objDatabase, $objOriginalLeaseCamShare, $this );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createCamSharesActivityLog( $objLeaseCamShare, $intCompanyUserId, $intEventSubTypeId, $objDatabase, $objOldReferenceData = NULL, $objNewReferenceData = NULL ) {
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $objLeaseCamShare->getCid(), $objDatabase );

		$arrobjPropertyCamPool = \Psi\Eos\Entrata\CLeaseCamPools::createService()->fetchLeaseCamPoolByIdByPropertyCamBudgetIdByPropertyIdByCId( $objLeaseCamShare->getLeaseCamPoolId(), $objLeaseCamShare->getPropertyCamBudgetId(), $objLeaseCamShare->getPropertyId(), $objLeaseCamShare->getCId(), $objDatabase );
		$arrobjPropertyCamPool = current( $arrobjPropertyCamPool );

		$arrstrLogFields = [ 'CamCalculationTypeId', 'ShareAmount', 'CapAmount', 'IsBaseYearFixed', 'PropertyCamPoolId', 'Details' ];
		$arrmixChanges   = CTenantLibrary::compareObject( $objOldReferenceData, $objNewReferenceData, $arrstrLogFields );

		if( CEventSubType::CAM_SHARE_UPDATED == $intEventSubTypeId && false == $arrmixChanges['lease_cam_share'] ) {
			return true;
		}

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $objLeaseCamShare ], CEventType::CAM_SHARE, $intEventSubTypeId );
		$objEvent->setCid( $objLeaseCamShare->getCid() );
		$objEvent->setPropertyId( $objLeaseCamShare->getPropertyId() );
		$objEvent->setLeaseId( $arrobjPropertyCamPool['lease_id'] );
		$objEvent->setEventDatetime( CEvent::getCurrentUtcTime() );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setUpdatedBy( $objCompanyUser->getId() );
		$objEvent->setUpdatedOn( 'NOW()' );
		$objEvent->setCreatedBy( $objCompanyUser->getId() );
		$objEvent->setCreatedOn( 'NOW()' );
		$objEvent->setPoolName( $arrobjPropertyCamPool['property_pool_name'] );
		$objEvent->setExpenseName( $arrobjPropertyCamPool['expense_name'] );
		$objEvent->setDataReferenceId( $this->getId() );

		$objEventLibraryDataObject->setData( $arrmixChanges );

		$objEventLibrary->buildEventDescription( $objLeaseCamShare );

		if( false === $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function fetchOriginalLeaseCamShare( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCamShares::createService()->fetchLeaseCamShareByIdByCId( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}