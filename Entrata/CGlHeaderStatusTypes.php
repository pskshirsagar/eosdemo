<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderStatusTypes
 * Do not add any new functions to this class.
 */
class CGlHeaderStatusTypes extends CBaseGlHeaderStatusTypes {

    public static function fetchGlHeaderStatusTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CGlHeaderStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchGlHeaderStatusType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CGlHeaderStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllGlHeaderStatusTypes( $objDatabase, $boolIsPublished = false ) {

    	$strSql = 'SELECT * FROM gl_header_status_types ORDER BY order_num';

    	if( false != $boolIsPublished ) {
    		$strSql = 'SELECT * FROM gl_header_status_types WHERE is_published = 1 ORDER BY id';
    	}

    	return self::fetchGlHeaderStatusTypes( $strSql, $objDatabase );
    }
}
?>