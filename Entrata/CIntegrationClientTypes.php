<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientTypes
 * Do not add any new functions to this class.
 */

class CIntegrationClientTypes extends CBaseIntegrationClientTypes {

	public static function fetchIntegrationClientTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CIntegrationClientType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedIntegrationClientTypes( $objDatabase ) {
		return self::fetchIntegrationClientTypes( 'SELECT * FROM integration_client_types WHERE is_published = 1 ORDER BY name', $objDatabase );
	}

	public static function fetchAllIntegrationClientTypes( $objDatabase ) {
		return self::fetchIntegrationClientTypes( 'SELECT * FROM integration_client_types ORDER BY name', $objDatabase );
	}

	public static function fetchIntegrationClientTypesByCid( $intClientid, $objDatabase, $boolIncludeDisabled = true, $boolReturnArray = false, $boolIncludeMigration = false, $boolIncludeEmh = false ) {

		$strSql	= 'SELECT
						DISTINCT ( id.integration_client_type_id ),
						ict.*
					FROM
						integration_client_types AS ict
						JOIN integration_databases id ON ( ict.id = id.integration_client_type_id )
					WHERE
						id.cid = ' . ( int ) $intClientid . '
						AND id.integration_client_status_type_id IN( ' . CIntegrationClientStatusType::ACTIVE . ', ' . CIntegrationClientStatusType::ON_HOLD . ' )';

		if( false == $boolIncludeMigration ) {
			$strSql .= ' AND ict.id NOT IN( ' . CIntegrationClientType::MIGRATION . ')';
		}

		if( false == $boolIncludeEmh ) {
			$strSql .= ' AND ict.id NOT IN( ' . CIntegrationClientType::EMH . ', ' . CIntegrationClientType::EMH_ONE_WAY . ' )';
		}

		if( false == $boolIncludeDisabled ) {
			$strSql .= ' AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED;
		}

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return parent::fetchIntegrationClientTypes( $strSql, $objDatabase );
	}

	public static function fetchIntegrationClientTypeIdsByCid( $intClientid, $objDatabase, $boolIncludeDisabled = true ) {

		$arrintIntegrationClientTypeIds = NULL;

		$strSql	= 'SELECT
						DISTINCT ict.id
					FROM
						integration_client_types AS ict
						JOIN integration_databases id ON ( ict.id = id.integration_client_type_id )
					WHERE
						id.cid = ' . ( int ) $intClientid . '
						AND ict.name NOT ILIKE \'%migrate%\'
						AND id.integration_client_status_type_id IN( ' . CIntegrationClientStatusType::ACTIVE . ', ' . CIntegrationClientStatusType::ON_HOLD . ' )';

		if( false == $boolIncludeDisabled ) {
			$strSql .= ' AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED;
		}

		$arrintTempIntegrationClientTypeIds = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintTempIntegrationClientTypeIds ) ) {

			$arrintIntegrationClientTypeIds = [];

			foreach( $arrintTempIntegrationClientTypeIds as $arrintChunkIntegrationClientIds ) {
				$arrintIntegrationClientTypeIds[$arrintChunkIntegrationClientIds['id']] = $arrintChunkIntegrationClientIds['id'];
			}
		}

		return $arrintIntegrationClientTypeIds;
	}

	public static function fetchIntegrationClientTypeByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ict.*
					FROM
						integration_client_types ict
						JOIN integration_databases id ON ( ict.id = id.integration_client_type_id AND id.cid = ' . ( int ) $intCid . ' )
						JOIN property_integration_databases pid ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
					WHERE
						pid.cid = ' . ( int ) $intCid . '
						AND pid.property_id = ' . ( int ) $intPropertyId;

		return self::fetchIntegrationClientType( $strSql, $objDatabase );
	}

	public static function fetchIntegrationClientTypeIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ict.id
					FROM
						integration_client_types ict
						JOIN integration_databases id ON ( ict.id = id.integration_client_type_id AND id.cid = ' . ( int ) $intCid . ' )
						JOIN property_integration_databases pid ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
					WHERE
						pid.cid = ' . ( int ) $intCid . '
						AND pid.property_id = ' . ( int ) $intPropertyId;

		return self::fetchIntegrationClientType( $strSql, $objDatabase );
	}

}
?>