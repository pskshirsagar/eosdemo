<?php

class CScheduledEmailTemplates extends CBaseScheduledEmailTemplates {

	public static function fetchScheduledEmailTemplate( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledEmailTemplate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchScheduledEmailTemplateById( $intId, $objDatabase ) {
		return self::fetchScheduledEmailTemplate( sprintf( 'SELECT * FROM scheduled_email_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>