<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldExceptions
 * Do not add any new functions to this class.
 */

class CCompanyMergeFieldExceptions extends CBaseCompanyMergeFieldExceptions {

	public static function fetchCompanyMergeFieldExceptionByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM company_merge_field_exceptions WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )  AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyMergeFieldExceptions( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldExceptionByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT *   FROM company_merge_field_exceptions WHERE   property_group_ids && array[ ' . implode( ',', $arrintPropertyIds ) . ' ] AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyMergeFieldExceptions( $strSql, $objDatabase );
	}

	public static function fetchCCompanyMergeFieldExceptionByExceptionNameByCid( $strExceptionName, $intCid, $objDatabase ) {

		$strSql = 'SELECT *  FROM company_merge_field_exceptions WHERE  exception_name LIKE \'' . $strExceptionName . '\' AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyMergeFieldException( $strSql, $objDatabase );

	}

    public static function fetchCompanyMergeFieldExcpetionsDataByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {

        if( false == valId( $intCompanyMergeFieldId ) ) return NULL;

        $strSql = 'SELECT
                        cmfe.id,
                        util_get_translated( \'exception_name\', cmfe.exception_name, cmfe.details ) as exception_name,
                        util_get_translated( \'default_value\', cmfe.default_value, cmfe.details ) as default_value,
                        cmfe.property_group_ids as property_group_ids,
                        cmfe.is_required,
                        cmfe.show_in_merge_display,
                        cmfe.allow_user_update,
                        cmfe.details->>\'is_follow_default\' as is_follow_default,
                        cmfe.view_expression->\'values\' as view_expression,
                        array_to_json( array_agg( pg.name ) ) as property_names
                   FROM
                        company_merge_field_exceptions as cmfe
                        JOIN company_merge_fields cmf ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )
                        JOIN property_groups pg ON ( pg.cid = cmfe.cid AND pg.id = ANY( cmfe.property_group_ids ) )  
                   WHERE
                        cmfe.cid = ' . ( int ) $intCid . '
                        AND cmfe.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
                  GROUP BY 
                        cmfe.id,
                        cmfe.exception_name,
                        util_get_translated( \'default_value\', cmfe.default_value, cmfe.details ),
                        cmfe.default_value,
                        cmfe.property_group_ids,
                        cmfe.is_required,
                        cmfe.show_in_merge_display,
                        cmfe.allow_user_update,
                        cmfe.details,
                        cmfe.view_expression
                   ORDER BY                       
                        ' . $objDatabase->getCollateSort( 'cmfe.exception_name', true );

        return fetchData( $strSql, $objDatabase );

    }

	public static function fetchCompanyMergeFieldExceptionNamesByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT
                        util_get_translated( \'exception_name\', cmfe.exception_name, cmfe.details ) as exception_name
                   FROM
                        company_merge_field_exceptions cmfe
                        JOIN company_merge_fields cmf ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )                        
                   WHERE
                        cmfe.cid = ' . ( int ) $intCid . '
                        AND cmf.id = \'' . $intCompanyMergeFieldId . '\'
                   ORDER BY
                        cmfe.id';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		$arrstrExceptionNames = [];

		if( true == valArr( $arrstrResult ) ) {
			foreach( $arrstrResult as $arrstrExceptionName ) {
				$arrstrExceptionNames[] = $arrstrExceptionName['exception_name'];
			}
		}

		return $arrstrExceptionNames;
	}

	public static function fetchCompanyMergeFieldExceptionsPropertyIdsByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT
                        array_to_json( cmfe.property_group_ids ) as property_group_ids
                   FROM
                        company_merge_field_exceptions cmfe
                        JOIN company_merge_fields cmf ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )                        
                   WHERE
                        cmfe.cid = ' . ( int ) $intCid . '
                        AND cmf.id = \'' . $intCompanyMergeFieldId . '\'
                   ORDER BY
                        cmfe.id';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrstrResult ) ) {
			foreach( $arrstrResult as $arrstrExceptionName ) {
				$arrintPropertyIds = array_merge( $arrintPropertyIds, json_decode( $arrstrExceptionName['property_group_ids'] ) );
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchCompanyMergeFieldExceptionsByCompanyMergeFieldIdsByCid( $arrintCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						cmfe.id,
						util_get_translated( \'exception_name\', cmfe.exception_name, cmfe.details ) as exception_name,
						cmfe.company_merge_field_id,
						cmf.name as company_merge_field_name,
						cmfe.default_value,
                        array_to_json( cmfe.property_group_ids ) as property_group_ids
                   FROM
                        company_merge_field_exceptions cmfe
                        JOIN company_merge_fields cmf ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )
                   WHERE
                        cmfe.cid = ' . ( int ) $intCid . '
                        AND cmf.id IN (' . implode( ',', $arrintCompanyMergeFieldId ) . ')
                   ORDER BY
                        cmfe.id';

		return fetchData( $strSql, $objDatabase );

	}

}
?>
