<?php

class CReportNewInstance extends CBaseReportNewInstance {

	const NAME_LIMIT = 240;

	private $m_intReportGroupInstanceId;
	private $m_intReportFilterId;
	private $m_strReportName;
	private $m_intReportTypeId;

	/** @var CReport */
	private $m_objReport;
	private $m_arrmixDownloadOptions;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['filters'] ) && false == valStr( $arrmixValues['filters'] ) ) {
			// Base setValues tries to trim filters, which fails for objects and arrays
			$this->setFilters( $arrmixValues['filters'] );
			unset( $arrmixValues['filters'] );
		}

		if( true == isset( $arrmixValues['details'] ) && true == valArr( $arrmixValues['details'], 0 ) ) {
			// Base setValues tries to trim details, which fails for objects and arrays
			$arrmixValues['details'] = json_encode( $arrmixValues['details'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['report_group_instance_id'] ) )	$this->setReportGroupInstanceId( $arrmixValues['report_group_instance_id'] );
		if( true == isset( $arrmixValues['report_filter_id'] ) ) 			$this->setReportFilterId( $arrmixValues['report_filter_id'] );
		if( true == isset( $arrmixValues['report_name'] ) )					$this->setReportName( $arrmixValues['report_name'] );
		if( true == isset( $arrmixValues['report_type_id'] ) )				$this->setReportTypeId( $arrmixValues['report_type_id'] );
		if( true == isset( $arrmixValues['download_options'] ) )			$this->setDownloadOptions( $arrmixValues['download_options'] );

		if( true == isset( $arrmixValues['report'] ) && true == valArr( $arrmixReport = ( valArr( $arrmixValues['report'] ) ? $arrmixValues['report'] : json_decode( $arrmixValues['report'], true ) ) ) ) {
			$objReport = new CReport();
			$objReport->setValues( $arrmixReport );
			$this->setReport( $objReport );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createReportHistory( CReportSchedule $objReportSchedule, DateTime $objToday, CDatabase $objDatabase, CCompanyUser $objCompanyUser, $arrmixReportFilterData ) {

		$strPacketName = '';
		if( $objReportSchedule->getIsScheduleForReportPacket() ) {
			$objReportGroup = $objReportSchedule->getOrFetchReportGroup( $objDatabase );
			if( valObj( $objReportGroup, CReportGroup::class ) ) {
				$strPacketName = $objReportGroup->getName();
			} else {
				$objReportNewGroup = $objReportSchedule->getOrFetchReportNewGroup( $objDatabase );
				$strPacketName = valObj( $objReportNewGroup, CReportNewGroup::class ) ? $objReportNewGroup->getName() : '';
			}
		}

		$objReportHistory = new CReportHistory();
		$objReportHistory->setCid( $this->getCid() );
		$objReportHistory->setReportId( $this->getReportId() < 0 ? NULL : $this->getReportId() );
		$objReportHistory->setReportScheduleTypeId( $objReportSchedule->getReportScheduleTypeId() );
		$objReportHistory->setReportNewInstanceId( $this->getId() < 0 ? NULL : $this->getId() );
		$objReportHistory->setReportNewGroupId( $objReportSchedule->getReportNewGroupId() );
		$objReportHistory->setReportVersionId( $this->getReportVersionId() < 0 ? NULL : $this->getReportVersionId() );
		$objReportHistory->setReportFilterId( $this->getReportFilterId() < 0 ? NULL : $this->getReportFilterId() );
		$objReportHistory->setReportScheduleId( $objReportSchedule->getId() < 0 ? NULL : $objReportSchedule->getId() );
		$objReportHistory->setReportGroupId( $objReportSchedule->getReportGroupId() );
		$objReportHistory->setFilters( json_encode( $this->getFilters() ) );
		$objReportHistory->setDownloadOptions( json_encode( $this->getDownloadOptions() ) );
		$objReportHistory->setRecipients( $objReportSchedule->fetchRecipientCache( $objDatabase, $objCompanyUser ) ?: '{}' );
		$objReportHistory->generateReportHistoryName( $this, $strPacketName, $objToday );
		$objReportHistory->setCreatedOn( $objToday->format( 'c' ) );
		$objReportHistory->setDetails( json_encode( $arrmixReportFilterData ) );

		return $objReportHistory;
	}

	public function toArrayWithDependencies() {
		return array_merge( parent::toArray(), [
			'report_group_instance_id'	=> $this->getReportFilterId(),
			'report_filter_id'			=> $this->getReportFilterId(),
			'report_name'				=> $this->getReportName(),
			'report_type_id'			=> $this->getReportTypeId(),
			'report'					=> valObj( $this->getReport(), CReport::class ) ? $this->getReport()->toArray() : NULL,
			'download_options'      => $this->getDownloadOptions(),
		] );
	}

	public function applyFilters( $strOutputType = NULL ) {
		// Set request data to match filters
		$_REQUEST['report_filter'] = $this->getFiltersArray()['report_filter'];

		if( true == isset( $this->getFiltersArray()['display_options'] ) ) {
			$_REQUEST['display_options'] = $this->getFiltersArray()['display_options'];
		}

		if( true == isset( $this->getDownloadOptions()[$strOutputType] ) ) {
			$_REQUEST['download_options'] = $this->getDownloadOptions()[$strOutputType];
		}
	}

	// region Getters

	public function getReportGroupInstanceId() {
		return $this->m_intReportGroupInstanceId;
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function getReportName() {
		return $this->m_strReportName;
	}

	public function getReport() {
		return $this->m_objReport;
	}

	public function getDownloadOptions() {
		return $this->m_arrmixDownloadOptions;
	}

	public function getFiltersArray() {
		return json_decode( json_encode( $this->getFilters() ), true ) ?: [];
	}

	public function getReportTypeId() {
		return $this->m_intReportTypeId;
	}

	// endregion

	// region Setters

	public function setReportGroupInstanceId( $intReportGroupInstanceId ) {
		$this->m_intReportGroupInstanceId = ( int ) $intReportGroupInstanceId;
		return $this;
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->m_intReportFilterId = $intReportFilterId;
		return $this;
	}

	public function setReportName( $strReportName ) {
		$this->m_strReportName = $strReportName;
		return $this;
	}

	public function setReport( CReport $objReport ) {
		$this->m_objReport = $objReport;
		return $this;
	}

	public function setDownloadOptions( $arrmixDownloadOptions ) {
		$this->m_arrmixDownloadOptions = $arrmixDownloadOptions;
		return $this;
	}

	public function setFilters( $mixFilters ) {
		if( true == valArr( $mixFilters, 0 ) ) {
			$mixFilters = json_encode( $mixFilters );
		}

		parent::setFilters( $mixFilters );
		return $this;
	}

	public function setFilterValue( $strFilterKey, $mixFilterValue ) {
		$arrmixFilters = $this->getFiltersArray();
		$arrmixFilters['report_filter'][$strFilterKey] = $mixFilterValue;
		$this->setFilters( json_encode( $arrmixFilters ) );
		return $this;
	}

	public function setReportTypeId( $intReportTypeId ) {
		$this->m_intReportTypeId = $intReportTypeId;
		return $this;
	}

	// endregion

	// region SQL Overrides

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : '(
				SELECT
					min(m.id)
				FROM
					modules m
					LEFT JOIN report_groups rg ON rg.module_id = m.id AND rg.cid = ' . $this->m_intCid . '
					LEFT JOIN report_instances ri ON ri.module_id = m.id AND ri.cid = ' . $this->m_intCid . '
					LEFT JOIN report_new_instances rni ON rni.module_id = m.id AND rni.cid = ' . $this->m_intCid . '
				WHERE
					m.id >= ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
					AND COALESCE( rg.id, ri.id, rni.id ) IS NULL
			)';
	}

	// endregion

	// region Validation

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {

		if( false != empty( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required' ) ) );
			return false;
		}

		if( self::NAME_LIMIT < mb_strlen( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is limited to {%d, 0} characters', [ self::NAME_LIMIT ] ) ) );
			return false;
		}

		// TODO: added this check particularly for Dashboard reports having type as SISENSE for now.
		if( NULL !== $objDatabase && valArr( \Psi\Eos\Entrata\CReportNewInstances::createService()->fetchDuplicateReportInstanceNames( $this, $objDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'A dashboard report with the name "{%s, 0}" already exists', [ $this->getName() ] ) ) );
			return false;
		}

		return true;
	}

	public function valDescription() {
		if( false != empty( $this->getDescription() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required' ) ) );
			return false;
		}
		return true;
	}

	public function valFilters() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		switch( $strAction ) {
			case VALIDATE_UPDATE:
				// TODO: validate ID
				// TODO: validate report id
				// TODO: validate report version id
				// TODO: validate filters
				return( $this->valName( $objDatabase ) && $this->valDescription() );

			case VALIDATE_INSERT:
				// TODO: validate report id
				// TODO: validate report version id
				// TODO: validate filters
				return( $this->valName( $objDatabase ) && $this->valDescription() );

			case VALIDATE_DELETE:
				return true;

			default:
				return false;
		}
	}

	// endregion

}
