<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessageTypes
 * Do not add any new functions to this class.
 */

class CCustomerMessageTypes extends CBaseCustomerMessageTypes {

	public static function fetchCustomerMessageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerMessageType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerMessageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerMessageType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>