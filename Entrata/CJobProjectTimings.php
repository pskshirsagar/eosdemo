<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectTimings
 * Do not add any new functions to this class.
 */

class CJobProjectTimings extends CBaseJobProjectTimings {

	public static function fetchJobProjectTimings( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CJobProjectTiming', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchJobProjectTiming( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CJobProjectTiming', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>