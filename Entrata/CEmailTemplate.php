<?php

class CEmailTemplate extends CBaseEmailTemplate {
	use TEosStoredObject;

	protected $m_intRecipientCount;
	public $m_strStoragePath;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledEmailTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailTemplateFolderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		$strName = preg_replace( '/[!@#$%^&*(),.?"\':{}|<>~`]/', '', $this->getName() );
		$strName = trim( $strName );

		if( \Psi\CStringService::singleton()->strlen( $this->getName() ) != \Psi\CStringService::singleton()->strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Only alphanumeric characters [0 to 9, A to Z ,'-' , '_'and a-z ] are allowed in name." ) ) );
			return false;
		}

		if( true == is_null( $strName ) || 2 > \Psi\CStringService::singleton()->strlen( $strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Template Name should contain at least 2 characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getSystemMessageTemplateSlotImagesByEmailTemplate( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;
		$arrstrSystemMessageTemplateSlotImages = [];

		$arrstrSystemMessageTemplateSlotNames 	= ( array ) \Psi\Eos\Entrata\CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $objDatabase );
		$arrmixSystemMessageTemplateSlotImages  = ( array ) \Psi\Eos\Entrata\CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesByEmailTemplateIdByCid( $this->getId(), $this->getCid(),  $objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageTemplateSlotNames ) ) {
				$strKey = $arrstrSystemMessageTemplateSlotNames[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = [
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target']
				];
			}
		}
		return $arrstrSystemMessageTemplateSlotImages;
	}

	public function getEmailAttachmentsByEmailTemplate( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;

		$arrmixFileAttachments = [];

		$arrobjStoredObjects = ( array ) CStoredObjects::fetchStoredObjectsByReferenceTableByReferenceIdByCid( 'public.email_templates', $this->getId(), $this->getCid(), $objDatabase );

		$intCounter = 1;
		foreach( $arrobjStoredObjects as $objStoredObject ) {
			$strKey = 'file' . $intCounter;
			$arrmixFileAttachments[$strKey] = [
				'id' => $objStoredObject->getId(),
				'title' => $objStoredObject->getTitle()
			];
			$intCounter++;
		}

		return $arrmixFileAttachments;

	}

	public function fetchEmailTemplateCountByNameByCid( $objDatabase ) {
		$strWhere = '';

		if( true == valId( $this->getId() ) ) {
			$strWhere = 'AND id <> ' . ( int ) $this->getId();
		}

		$strWhereSql = ' WHERE
					lower( name) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $this->getName() ) ) ) . '\'
					AND cid = ' . ( int ) $this->getCid() . $strWhere;

		return \Psi\Eos\Entrata\CEmailTemplates::createService()->fetchEmailTemplateCount( $strWhereSql, $objDatabase );
	}

	public function setStoragePath( $strStoragePath ) {
		$this->m_strStoragePath = $strStoragePath;
	}

	public function getStoragePath() {
		return $this->m_strStoragePath;
	}

	protected function calcStorageKey() {
		return $this->getStoragePath();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_OSG_BUCKET_COMMUNICATIONS;

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_FILES;

			default:
				return CONFIG_OSG_BUCKET_COMMUNICATIONS;
		}

	}

	public function buildFileData( $arrmixFileDetails, $objDatabase, $intFileIndex = NULL ) {

		if( false == is_numeric( $intFileIndex ) ) {
			$arrstrFileInfo = pathinfo( $arrmixFileDetails['name'] );
			$this->setTitle( $arrmixFileDetails['name'] );
		} else {
			$arrstrFileInfo = pathinfo( $arrmixFileDetails['name'][$intFileIndex] );
			$this->setTitle( $arrmixFileDetails['name'][$intFileIndex] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, CFileExtension::class ) ) {
			return false;
		}

		$strMimeType = $objFileExtension->getMimeType();

		$strFileName = uniqid( mt_rand(), true ) . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$this->setStoragePath( $this->getCid() . '/message_center/template_library_attachments/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/' . $strFileName );

		return true;
	}

	public function downloadEmailTemplateAttachment( $objStoredObject, $objObjectStorageGateway, $objDatabase ) {

		// This was ported from CStoredObject::downloadObject

		$arrmixGatewayRequest = $objStoredObject->createGatewayRequest( [ 'checkExists' => true ] );
		$arrmixGatewayRequest = $this->buildMountsFilePath( $arrmixGatewayRequest, $objStoredObject->getReferenceTable() );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		if( false == $objObjectStorageGatewayResponse['isExists'] ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'File is not present or has been deleted.' ) );
			return false;
		}

		$arrmixGatewayRequest = $objStoredObject->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );
		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $objStoredObject->getTitle();

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length:' . $objObjectStorageGatewayResponse['contentLength'] );
		header( 'Content-Type:' . $strMimeType );
		header( 'Content-Disposition: attachment; filename= "' . $strFileName . '"' );
		header( 'Content-Transfer-Encoding:binary' );

		if( 'text/html' == $strMimeType )
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

		echo CFileIo::fileGetContents( $strTempFullPath );
		CFileIo::deleteFile( $strTempFullPath );
		exit;

	}

	public function buildMountsFilePath( $arrmixGatewayRequest, $strReferenceTable ) {
		// We have added this code block for email template attachment till we completly able to move to cloud storage
		if( 'shared_file_system' == $arrmixGatewayRequest['storageGateway'] && false !== strstr( $strReferenceTable, 'email_templates' ) ) {
			$arrmixGatewayRequest['key'] = '/files'. $arrmixGatewayRequest['key'];
		}

		return $arrmixGatewayRequest;
	}

}
?>