<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessageSenderTypes
 * Do not add any new functions to this class.
 */

class CCustomerMessageSenderTypes extends CBaseCustomerMessageSenderTypes {

	public static function fetchCustomerMessageSenderTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CCustomerMessageSenderType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerMessageSenderType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CCustomerMessageSenderType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }
}
?>