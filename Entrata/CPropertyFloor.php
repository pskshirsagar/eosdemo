<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CPropertyFloors;

class CPropertyFloor extends CBasePropertyFloor {

	protected $m_strTaxId;

	protected $m_fltExistingTotalSquareFeet;
	protected $m_fltExistingUsableSquareFeet;
	protected $m_fltExistingRentableSquareFeet;

	public function getExistingTotalSquareFeet() {
		return $this->m_fltExistingTotalSquareFeet;
	}

	public function getExistingUsableSquareFeet() {
		return $this->m_fltExistingUsableSquareFeet;
	}

	public function getExistingRentableSquareFeet() {
		return $this->m_fltExistingRentableSquareFeet;
	}

	public function setExistingTotalSquareFeet( $fltTotalSquareFeet ) {
		$this->set( 'm_fltExistingTotalSquareFeet', CStrings::strToFloatDef( $fltTotalSquareFeet, NULL, false, 4 ) );
	}

	public function setExistingUsableSquareFeet( $fltUsableSquareFeet ) {
		$this->set( 'm_fltExistingUsableSquareFeet', CStrings::strToFloatDef( $fltUsableSquareFeet, NULL, false, 4 ) );
	}

	public function setExistingRentableSquareFeet( $fltRentableSquareFeet ) {
		$this->set( 'm_fltExistingRentableSquareFeet', CStrings::strToFloatDef( $fltRentableSquareFeet, NULL, false, 4 ) );
	}

	public function valFloorTitle( $objDatabase, $boolIsFromCommercial = false ) {
		$boolIsValid = true;

		if( true === is_null( $this->getFloorTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floor_title', __( 'Floor title required.' ) ) );
		}

		if( true == $boolIsFromCommercial ) {
			$intCountFloorTitles = CPropertyFloors::createService()->fetchPropertyFloorsCountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, false, $this->getFloorTitle(), $this->getId(),  $this->getPropertyBuildingId() );

			if( $intCountFloorTitles < 1 ) {
				return $boolIsValid;
			} else {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floor_title', __( 'Floor Title already exists.' ) ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valFloorSquareFeet( $strUnitOfMeasureForArea ) {
		$boolIsValid = true;

		if( false === is_numeric( $this->getTotalSquareFeet() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} required.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getUsableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getCamSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cam_square_feet', __( 'CAM {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Total rentable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet_total', __( 'The Usable {%s, 0} cannot exceed Rentable {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getRentableSquareFeet() > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Rentable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyFloorSquareFeet( $objDatabase, $strUnitOfMeasureForArea ) {
		$boolIsValid = true;

		$objPropertyBuilding  = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingByIdByCid( $this->getPropertyBuildingId(), $this->getCid(), $objDatabase );
		$arrmixPropertyFloors = rekeyArray( 'property_building_id', ( array ) \Psi\Eos\Entrata\CPropertyFloors::createService()->fetchSumOfTotalSqftOfPropertyFloorsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase ) );
		$arrmixPropertyFloor  = $arrmixPropertyFloors[$this->getPropertyBuildingId()];

		$intEditedTotalFloorSquareFeet    = 0;
		$intEditedUsableFloorSquareFeet   = 0;
		$intEditedRentableFloorSquareFeet = 0;
		if( $arrmixPropertyFloor['property_building_id'] == $this->getPropertyBuildingId() ) {
			$intEditedTotalFloorSquareFeet    = $this->getExistingTotalSquareFeet();
			$intEditedUsableFloorSquareFeet   = $this->getExistingUsableSquareFeet();
			$intEditedRentableFloorSquareFeet = $this->getExistingRentableSquareFeet();
		}

		// Floors Available Total SQFT
		$intTotalSquareFeet          = ( NULL != $objPropertyBuilding->getTotalSquareFeet() ) ? $objPropertyBuilding->getTotalSquareFeet() : 0;
		$fltFloorsAvailableTotalSqft = ( $intTotalSquareFeet - $arrmixPropertyFloor['floors_square_feet'] ) + $intEditedTotalFloorSquareFeet;

		// Floors Available Usable SQFT
		$intUsableSquareSquareFeet    = ( NULL != $objPropertyBuilding->getUsableSquareFeet() ) ? $objPropertyBuilding->getUsableSquareFeet() : 0;
		$fltFloorsAvailableUsableSqft = ( $intUsableSquareSquareFeet - $arrmixPropertyFloor['floors_usable_square_feet'] ) + $intEditedUsableFloorSquareFeet;

		// Floors Available Rentable SQFT
		$intRentableSquareSquareFeet    = ( NULL != $objPropertyBuilding->getRentableSquareFeet() ) ? $objPropertyBuilding->getRentableSquareFeet() : 0;
		$fltFloorsAvailableRentableSqft = ( $intRentableSquareSquareFeet - $arrmixPropertyFloor['floors_rentable_square_feet'] ) + $intEditedRentableFloorSquareFeet;

		if( false === is_null( $this->m_fltTotalSquareFeet ) && $this->m_fltTotalSquareFeet > $fltFloorsAvailableTotalSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Total {%s, 2} of this floor cannot exceed the Total {%s, 2} of {%s, 0} Building: {%f, 1, p:2}. The Total {%s, 2} of this floor must be equal to or less than the unallocated Total {%s, 2} of {%s, 0} Building: {%f, 3, p:2}.', [ $objPropertyBuilding->getBuildingName(), $objPropertyBuilding->getTotalSquareFeet(), $strUnitOfMeasureForArea, $fltFloorsAvailableTotalSqft ] ) ) );
			$boolIsValid = false;
		}

		if( false === is_null( $this->m_fltUsableSquareFeet ) && $this->m_fltUsableSquareFeet > $fltFloorsAvailableUsableSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Usable {%s, 2} of this floor cannot exceed the Usable {%s, 2} of {%s, 0} Building: {%f, 1, p:2}. The Usable {%s, 2} of this floor must be equal to or less than the unallocated Usable {%s, 2} of {%s, 0} Building: {%f, 3, p:2}.', [ $objPropertyBuilding->getBuildingName(), $objPropertyBuilding->getUsableSquareFeet(), $strUnitOfMeasureForArea, $fltFloorsAvailableUsableSqft ] ) ) );
			$boolIsValid = false;
		}

		if( false === is_null( $this->m_fltRentableSquareFeet ) && $this->m_fltRentableSquareFeet > $fltFloorsAvailableRentableSqft ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Rentable {%s, 2} of this floor cannot exceed the Rentable {%s, 2} of {%s, 0} Building: {%f, 1, p:2}. The Rentable {%s, 2} of this floor must be equal to or less than the unallocated Rentable {%s, 2} of {%s, 0} Building: {%f, 3, p:2}.', [ $objPropertyBuilding->getBuildingName(), $objPropertyBuilding->getRentableSquareFeet(), $strUnitOfMeasureForArea, $fltFloorsAvailableRentableSqft ] ) ) );
			$boolIsValid = false;
		}

		$arrmixFloorAllocatedSqftDetails = \Psi\Eos\Entrata\CCommercialSuites::createService()->fetchCustomCommercialSuitesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$arrmixFloorAllocatedSqftDetails = rekeyArray( 'property_floor_id', $arrmixFloorAllocatedSqftDetails );

		if( true === valArr( $arrmixFloorAllocatedSqftDetails[$this->getId()] ) && ( int ) $this->getTotalSquareFeet() < ( int ) $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_square_feet'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'The Total {%s, 1} of this floor cannot be less than allocated Suite Total {%s, 1}: {%f, 0, p:2}.', [ $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			$boolIsValid = false;
		}

		if( true === valArr( $arrmixFloorAllocatedSqftDetails[$this->getId()] ) && ( int ) $this->getUsableSquareFeet() < ( int ) $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_usable_square_feet'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 1} of this floor cannot be less than allocated Suite Usable {%s, 1}: {%f, 0, p:2}.', [ $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_usable_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			$boolIsValid = false;
		}

		if( true === valArr( $arrmixFloorAllocatedSqftDetails[$this->getId()] ) && ( int ) $this->getRentableSquareFeet() < ( int ) $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_rentable_square_feet'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Rentable {%s, 1} of this floor cannot be less than allocated Suite Rentable {%s, 1}: {%f, 0, p:2}.', [ $arrmixFloorAllocatedSqftDetails[$this->getId()]['suites_rentable_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valFloorNumber( $objDatabase ) {
		$boolIsValid = true;

		if( true === is_null( $this->getFloorNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floor_number', __( 'Floor number required' ) ) );
		}

		if( true === $boolIsValid && false === is_numeric( $this->getFloorNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floor_number', __( 'Invalid floor number' ) ) );
		}

		if( true === $boolIsValid && false === is_null( $this->getPropertyId() ) && false === is_null( $this->getFloorNumber() ) ) {

			$objPropertyFloor = CPropertyFloors::createService()->fetchPropertyFloorByFloorNumberByPropertyBuildingIdByPropertyIdByCid( $this->getFloorNumber(), $this->getPropertyBuildingId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true === valObj( $objPropertyFloor, 'CPropertyFloor' ) && $objPropertyFloor->getId() != $this->getId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Floor Number {%d,0} for this property building already exists.', [ $this->getFloorNumber() ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;

		if( true === is_null( $this->getPropertyBuildingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_building_id', __( 'Building required' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $strUnitOfMeasureForArea = 'square feet' ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFloorTitle( $objDatabase );
				$boolIsValid &= $this->valFloorNumber( $objDatabase );
				break;

			case 'validate_building_floor':
				$boolIsValid &= $this->valPropertyBuildingId();
				break;

			case 'VALIDATE_COMMERCIAL_FLOOR_INSERT':
			case 'VALIDATE_COMMERCIAL_FLOOR_UPDATE':
				$boolIsValid &= $this->valFloorTitle( $objDatabase, true );
				$boolIsValid &= $this->valFloorNumber( $objDatabase );
				$boolIsValid &= $this->valFloorSquareFeet( $strUnitOfMeasureForArea );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valPropertyFloorSquareFeet( $objDatabase, $strUnitOfMeasureForArea );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createFloorMedia() {
		$objFloorMedia = new CFloorMedia();
		$objFloorMedia->setCid( $this->m_intCid );
		$objFloorMedia->setPropertyFloorId( $this->m_intId );

		return $objFloorMedia;
	}

	public function createTaxIdAssociation() {
		$objTaxIdAssociation = new CTaxIdAssociation();
		$objTaxIdAssociation->setCid( $this->m_intCid );
		$objTaxIdAssociation->setPropertyId( $this->m_intPropertyId );
		$objTaxIdAssociation->setPropertyFloorId( $this->m_intId );

		return $objTaxIdAssociation;
	}

	/**
	 * get Functions
	 */

	public function getTaxId() {
		return $this->m_strTaxId;
	}

	public function getMaskedTaxId() {
		if( 4 == strlen( $this->getTaxId() ) ) {
			return $this->getTaxId();
		} else {
			return CEncryption::maskText( $this->getTaxId() );
		}
	}

	/**
	 * Set Functions
	 */

	public function setFloorNumber( $intFloorNumber ) {
		$this->m_intFloorNumber = trim( $intFloorNumber );
	}

	public function setTaxId( $strTaxId ) {
		$this->m_strTaxId = $strTaxId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_id'] ) ) $this->setTaxId( $arrmixValues['tax_id'] );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyMediaFile( $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $this->getCompanyMediaFileId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnits( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyFloorIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsCountByPropertyFloorIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloor( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorByFloorNumberByPropertyIdByCid( $this->getFloorNumber(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function disAssociateData( $intCompanyUserId, $objDatabase ) {
		$arrobjPropertyUnits = $this->fetchPropertyUnits( $objDatabase );

		if( true === valArr( $arrobjPropertyUnits ) ) {

			foreach( $arrobjPropertyUnits as $objPropertyUnit ) {
				$objPropertyUnit->setPropertyFloorId( NULL );

				if( false == $objPropertyUnit->update( $intCompanyUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByPropertyFloorIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFiles( $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyFloorIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFloorMedias( $objDatabase ) {
		return \Psi\Eos\Entrata\CFloorMedias::createService()->fetchFloorMediasByPropertyFloorIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	* Other Functions
	*/

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>