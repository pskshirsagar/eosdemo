<?php

class CSubsidyRecertificationType extends CBaseSubsidyRecertificationType {

	const BY_HOUSEHOLD		= 1;
	const BY_BUILDING		= 2;
	const BY_PROPERTY		= 3;
	const DO_NOT_PERFORM_ANNUAL_CERTIFICATIONS		= 4;
	const PERFORM_ANNUAL_CERTIFICATIONS				= 5;
	const PERFORM_1ST_YEAR_ANNUAL_CERTIFICATIONS	= 6;
	const PERFORM_CERTIFICATIONS_EVERY_3YEARS		= 7;
	const PERFORM_ANNUAL_STUDENT_CERTIFICATIONS		= 8;
	const FULL_CERTIFICATION		= 9;
	const INCOME_VERIFICATION		= 10;
	const DO_NOT_RECERTIFY		    = 11;

	public static $c_arrintHudSubsidyRecertificationTypes			= [ self::BY_HOUSEHOLD, self::BY_BUILDING, self::BY_PROPERTY ];
	public static $c_arrintTaxCreditSubsidyRecertificationTypes	= [ self::DO_NOT_PERFORM_ANNUAL_CERTIFICATIONS, self::PERFORM_ANNUAL_CERTIFICATIONS, self::PERFORM_1ST_YEAR_ANNUAL_CERTIFICATIONS, self::PERFORM_CERTIFICATIONS_EVERY_3YEARS, self::PERFORM_ANNUAL_STUDENT_CERTIFICATIONS ];
	public static $c_arrintTaxCreditHouseholdCompositionChangeSubsidyRecertificationTypes = [ self::FULL_CERTIFICATION, self::INCOME_VERIFICATION, self::DO_NOT_RECERTIFY ];

	public  static  $c_arrstrHouseholdCompositionChangeSubsidyRecertificationTypes = [
		self::FULL_CERTIFICATION => 'Full Certification',
		self::INCOME_VERIFICATION => 'Verify Income for New Member(s)',
		self::DO_NOT_RECERTIFY => 'Do Not Recertify'
	];

	public  static  $c_arrstrTaxCreditSubsidyRecertificationTypes = [
		self::PERFORM_ANNUAL_CERTIFICATIONS => 'Perform Annual Certifications',
		self::PERFORM_1ST_YEAR_ANNUAL_CERTIFICATIONS => 'Perform 1st Year Annual Certifications',
		self::PERFORM_CERTIFICATIONS_EVERY_3YEARS => 'Perform Certifications Every 3 Years',
		self::PERFORM_ANNUAL_STUDENT_CERTIFICATIONS => 'Perform Annual Student Certifications',
		self::DO_NOT_PERFORM_ANNUAL_CERTIFICATIONS => 'Do Not Perform Annual Certifications'
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>