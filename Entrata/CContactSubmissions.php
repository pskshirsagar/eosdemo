<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactSubmissions
 * Do not add any new functions to this class.
 */

class CContactSubmissions extends CBaseContactSubmissions {

	/**
	 * Fetch Functions
	 */
	public static function fetchPaginatedContactSubmissionsByPropertyIdsByCid( $intPageNumber, $intPageSize, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRemovedContactSubmissions = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$intOffset = ( 0 < $intPageNumber ) ? $intPageSize * ( $intPageNumber - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						cs.*,
						c.name_first,
						c.name_last
					 FROM
						contact_submissions cs
					 	LEFT JOIN customers c ON ( c.id = cs.customer_id AND c.cid = cs.cid )
					 WHERE
						cs.cid = ' . ( int ) $intCid . '
					 	AND cs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		$strSql .= ( true == $boolIsRemovedContactSubmissions ) ? ' AND cs.deleted_on IS NOT NULL ORDER BY cs.updated_on DESC' : ' AND cs.deleted_on IS NULL ORDER BY cs.id DESC';
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchContactSubmissions( $strSql, $objDatabase );
	}

	public static function fetchContactSubmissionsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsRemovedContactSubmissions = false, $boolIsCountContactSubmissions = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strWhere = ' WHERE cid = ' . ( int ) $intCid . ' AND property_id  IN (' . implode( ',', $arrintPropertyIds ) . ')';

		if( true == $boolIsCountContactSubmissions ) {
			$strWhere .= ' AND customer_message_id IS NULL AND deleted_on IS NULL';
		} else {
			if( true == $boolIsRemovedContactSubmissions ) {
				$strWhere .= ' AND deleted_on IS NOT NULL';
			} else {
				$strWhere .= ' AND deleted_on IS NULL';
			}
		}

		return self::fetchContactSubmissionCount( $strWhere, $objDatabase );
	}

	public static function fetchCustomContactSubmissionByIdByCid( $intContactSubmissionId, $intCid, $objDatabase ) {
		return self::fetchContactSubmission( sprintf( 'SELECT cs.* FROM %s AS cs WHERE id = %d AND cid = %d LIMIT 1', 'contact_submissions', ( int ) $intContactSubmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomContactSubmissionsByIdsByCid( $arrintContactSubmissionIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintContactSubmissionIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cs.*
					 FROM
						contact_submissions cs
					 WHERE
						cs.cid = ' . ( int ) $intCid . '
					 	AND cs.id IN (' . implode( ',', $arrintContactSubmissionIds ) . ')';

		return self::fetchContactSubmissions( $strSql, $objDatabase );
	}

	public static function fetchResidentsFollowUpsCountByDashboardFilterByEventSubTypeIdsByLeaseStatusTypeIdsByCid( $objDashboardFilter, $arrintEventSubTypeIds, $arrintLeaseStatusTypeIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}
		if( false == valArr( $arrintEventSubTypeIds ) ) {
			return NULL;
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= ' SELECT'
		 . ( true == $boolIsGroupByProperties ?
		  '
							COUNT( id ) AS residents_follow_up,
							property_id,
							property_name,
							priority'
		  :
		  '
							COUNT( id ) AS contact_submission_count,
							MAX( priority ) AS priority
						' ) . '
					 FROM
						(
							SELECT subQ.*
							FROM
								(
									SELECT
										DISTINCT ON (e.lease_interval_id)
										e.id,
										e.cid,
										e.lease_id,
										NULL AS lease_customer_lease_id,
										' . ( ( true == $boolIsGroupByProperties )
		  ? ' p.id AS property_id,
													p.property_name, '
		  : '' ) . '
										CASE
										WHEN TRUE = e.details ? \'overdue_on\' THEN
											CASE
												WHEN TRUE = trim ( e.details::json ->> \'overdue_on\' ) ~ \'^([0-9]+[.]?[0-9]*|[.][0-9]+)$\' THEN
												CASE
													WHEN NOW ( )::TIMESTAMP > TIMESTAMP \'epoch\' + trim ( e.details::json ->> \'overdue_on\' )::INTEGER * INTERVAL \'1 second\' AND COALESCE ( DATE_PART ( \'day\', TIMESTAMP \'epoch\' + trim ( e.details::json ->> \'overdue_on\' )::INTEGER * INTERVAL \'1 second\' - NOW ( )::TIMESTAMP )::INTEGER, 0 ) <> 0 THEN 3
													WHEN COALESCE ( DATE_PART ( \'day\', TIMESTAMP \'epoch\' + trim ( e.details::json ->> \'overdue_on\' )::INTEGER * INTERVAL \'1 second\' - NOW ( )::TIMESTAMP )::INTEGER, 0 ) = 0 AND
														( ABS( COALESCE ( DATE_PART ( \'hour\', TIMESTAMP \'epoch\' + trim ( e.details::json ->> \'overdue_on\' )::INTEGER * INTERVAL \'1 second\' - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 OR
															ABS( COALESCE ( DATE_PART ( \'minute\', TIMESTAMP \'epoch\' + trim ( e.details::json ->> \'overdue_on\' )::INTEGER * INTERVAL \'1 second\' - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 ) THEN 2
												ELSE 1
												END
											ELSE 
												CASE
													WHEN NOW ( )::TIMESTAMP > trim ( e.details::json ->> \'overdue_on\' )::TIMESTAMP AND COALESCE ( DATE_PART ( \'day\', trim ( e.details::json ->> \'overdue_on\' )::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) <> 0 THEN 3
													WHEN COALESCE ( DATE_PART ( \'day\', trim ( e.details::json ->> \'overdue_on\' )::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) = 0 AND
														( ABS( COALESCE ( DATE_PART ( \'hour\', trim ( e.details::json ->> \'overdue_on\' )::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 OR 
															ABS( COALESCE ( DATE_PART ( \'minute\', trim ( e.details::json ->> \'overdue_on\' )::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 ) THEN 2
												ELSE 1
												END
											END
										ELSE 
											CASE
												WHEN NOW ( )::TIMESTAMP > e.scheduled_datetime::TIMESTAMP AND COALESCE ( DATE_PART ( \'day\', e.scheduled_datetime::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) <> 0 THEN 3
												WHEN COALESCE ( DATE_PART ( \'day\', e.scheduled_datetime::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) = 0 AND 
													( ABS( COALESCE ( DATE_PART ( \'hour\', e.scheduled_datetime::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 OR 
														ABS( COALESCE ( DATE_PART ( \'minute\', e.scheduled_datetime::TIMESTAMP - NOW ( )::TIMESTAMP )::INTEGER, 0 ) ) > 0 ) THEN 2
											ELSE 1
										END
									END AS priority
									FROM
										events e
										JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . ' ) lp ON lp.property_id = e.property_id
										JOIN properties p ON ( p.cid = e.cid AND p.id = e.property_id )
										JOIN cached_leases cl ON ( cl.id = e.lease_id AND cl.cid = e.cid AND e.property_id = cl.property_id
										AND cl.lease_status_type_id IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::PAST . ',' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
										AND	CASE
												WHEN cl.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
												THEN cl.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' OR e.event_type_id = ' . CEventType::SPECIAL_QUALIFIED . '
												WHEN cl.lease_status_type_id = ' . CLeaseStatusType::PAST . '
												THEN e.event_type_id = ' . CEventType::EARLY_TERMINATION . '
											ELSE true
											END
										)
										JOIN event_types et ON ( et.id = e.event_type_id )
										LEFT JOIN cached_applications ca ON ( e.cid = ca.cid AND cl.id = ca.lease_id AND ca.property_id = e.property_id AND ca.lease_interval_id = e.lease_interval_id)
										LEFT JOIN event_sub_types est ON ( est.id = e.event_sub_type_id )
										LEFT JOIN event_results er ON ( er.cid = e.cid AND er.id = e.event_result_id AND is_system = true AND ( ' . CDefaultEventResult::COMPLETED . ' = ANY( er.default_event_result_ids ) OR ' . CDefaultEventResult::MISSED . ' = ANY( er.default_event_result_ids )  OR ' . CDefaultEventResult::FULFILLED . ' = ANY( er.default_event_result_ids )) )
									WHERE
										e.cid = ' . ( int ) $intCid . '
										AND CURRENT_DATE >= e.scheduled_datetime::DATE
										AND cl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
										AND CASE WHEN ca.mute_followups_until IS NOT NULL THEN CURRENT_DATE >= ca.mute_followups_until::date ELSE true END
										AND e.lease_id IS NOT NULL
										AND e.event_result_id IS NULL
										AND (
											e.event_sub_type_id IN (' . implode( ',', $arrintEventSubTypeIds ) . ')
											 OR
											( e.event_type_id = ' . CEventType::EMAIL_INCOMING . ' AND e.ps_product_id = ' . CPsProduct::MESSAGE_CENTER . ' AND e.is_resident = true )
											OR
											( e.event_sub_type_id IN ( ' . implode( ',', CEventSubType::$c_arrintActiveDashboardRenewalEventSubTypes ) . ' ) AND e.ps_product_id = ' . CPsProduct::MESSAGE_CENTER . ' )
											OR
											( e.event_type_id IN (' . implode( ',', CEventType::$c_arrintActiveDashboardEventTypes ) . ') AND e.event_sub_type_id IS NULL )
										)
										AND ( ( e.event_type_id IN (' . implode( ',', CEventType::$c_arrintActiveDashboardEventTypes ) . ') AND e.event_sub_type_id IS NULL   ) OR e.event_sub_type_id NOT IN ( ' . CEventSubType::MAINTENANCE_FOLLOW_UP . ', ' . CEventSubType::MAINTENANCE_MANUAL_FOLLOW_UP . '  ) )
										AND e.is_deleted = FALSE
									UNION
									SELECT
										subQ1.*
									FROM
										(
										SELECT
											DISTINCT ON (cs.id)
											cs.id,
											cs.cid,
											MAX ( lc.lease_id ) OVER ( PARTITION BY cs.customer_id ORDER BY lc.id DESC ) AS lease_id,
											lc.lease_id AS lease_customer_lease_id,
											' . ( ( true == $boolIsGroupByProperties )
		  ? ' p.id AS property_id,
														p.property_name, '
		  : '' ) . '
											CASE
												WHEN TRIM( dp.residents_follow_up ->> \'urgent_follow_up_type_ids\') <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up->>\'urgent_follow_up_type_ids\' )::integer[] ) THEN
													3
												WHEN TRIM ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' )::INTEGER [ ] ) THEN
													3
												WHEN TRIM( dp.residents_follow_up ->> \'important_follow_up_type_ids\') <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up->>\'important_follow_up_type_ids\' )::integer[] ) THEN
													2
												WHEN TRIM ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' )::INTEGER [] ) THEN
													2
												ELSE
													1
											END AS priority
										FROM
											contact_submissions cs
											JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '] ) lp ON lp.property_id = cs.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
											JOIN properties p ON ( p.id = cs.property_id AND p.cid = cs.cid )
											JOIN time_zones tz ON (p.time_zone_id = tz.id)
											JOIN lease_customers lc ON ( lc.cid = cs.cid AND lc.customer_id = cs.customer_id)
											LEFT JOIN dashboard_priorities dp ON ( dp.cid = cs.cid )
											LEFT JOIN contact_submission_types cst ON ( cst.cid = cs.cid AND cst.is_published = 1 AND  cst.id = cs.contact_submission_type_id AND cst.deleted_on IS NULL )
											LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = lc.cid )
											LEFT JOIN lease_intervals li ON ( li.cid = cs.cid AND li.id = cl.active_lease_interval_id AND li.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' ) )
											LEFT JOIN lease_processes lps ON ( cl.id = lps.lease_id AND cs.cid = lps.cid )
											LEFT JOIN property_preferences pp ON ( cs.property_id = pp.property_id AND cs.cid = pp.cid AND pp.key =\'PAST_RESIDENT_LOGIN_TOLERANCE_DAYS\' )
											LEFT JOIN cached_applications ca ON ( cl.cid = ca.cid AND cl.id = ca.lease_id AND ca.property_id = cl.property_id )
											LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
											LEFT JOIN customer_portal_settings cps ON ( cps.cid=cs.cid AND cps.customer_id=cs.customer_id )
										WHERE
											cs.cid = ' . ( int ) $intCid . '
											AND ( CASE
											WHEN cl.lease_status_type_id = ' . CLeaseStatusType::PAST . ' AND COALESCE( lps.move_out_date, li.lease_end_date ) + COALESCE( pp.value::INTEGER, ' . CPropertyPreference::PAST_RESIDENT_LOGIN_TOLERANCE_DAYS . ' ) > CURRENT_DATE
											THEN cl.id IS NULL OR cl.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintApprovedLeaseStatusTypeIds ) . ' )
                                            ELSE cl.id IS NULL OR cl.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ' )
										END )
											AND cs.deleted_on IS NULL
											AND cs.deleted_by IS NULL
											AND cs.customer_message_id IS NULL
											AND cs.id NOT IN(
															  SELECT
																  DISTINCT ON ( e.data_reference_id ) e.data_reference_id
															  FROM
																  events e
															  WHERE
																  e.data_reference_id IS NOT NULL
																  AND e.cid = ' . ( int ) $intCid . '
																  AND e.cid = cs.cid
																  AND e.property_id = cs.property_id
																  AND e.customer_id = cs.customer_id
																  AND e.event_type_id IN( ' . CEventType::CONTACT_SUBMISSION_CALL_OUTGOING . ', ' . CEventType::CONTACT_SUBMISSION_RESIDENT_VISIT . '  )
											)
										) AS subQ1
									WHERE
										subQ1.lease_id = subQ1.lease_customer_lease_id
									) AS subQ
							WHERE
								cid = ' . ( int ) $intCid
		 . $strPriorityWhere . '
							ORDER BY
								priority DESC
						) res_followups '
		 . ( true == $boolIsGroupByProperties
		  ? ' GROUP BY
										priority,
										property_id,
										property_name '
		  : '' );

		$arrmixData = fetchData( $strSql, $objDatabase );
		if( true == $boolIsGroupByProperties ) {
			return $arrmixData;
		}

		return ( true == valArr( $arrmixData ) ) ? $arrmixData[0] : [ 'contact_submission_count' => 0, 'priority' => 1 ];
	}

	public static function fetchContactSubmissionDetailsByIdByCid( $intContactSubmissionId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cs.id,
						cs.property_id,
						cs.name_last,
						cs.name_first,
						CASE 
							WHEN e.details::json ->>\'created_customer_id\' IS NULL
								THEN cs.email_address
							ELSE
								c.email_address
						END AS email_address,
						cs.phone_number,
						cs.office_number,
						cs.mobile_number,
						cs.move_out_date,
						cs.response_request_type_id,
						cs.subject,
						cs.message,
						cs.cid,
						cs.contact_submission_type_id,
						cs.list_items_id,
						cs.created_on,
						lc.lease_id,
						COALESCE ( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
						e.details::json ->>\'created_customer_id\' AS created_customer_id,
						cl.occupancy_type_id,
						func_format_customer_name ( c.name_first, c.name_last ) AS created_customer_name,
						cpn.phone_number AS created_customer_phone
					FROM
						contact_submissions cs
						JOIN lease_customers lc ON ( lc.cid = cs.cid AND lc.customer_id = cs.customer_id )
						LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = lc.cid )
						LEFT JOIN events e ON ( cs.cid = e.cid AND cs.customer_id = e.customer_id AND e.event_type_id = ' . CEventType::CONTACT_SUBMISSION . ' AND cs.created_on::timestamp = e.created_on::timestamp )
						LEFT JOIN customers c ON ( e.cid = c.cid AND c.id = ( e.details::json ->>\'created_customer_id\' )::integer )
						LEFT JOIN customer_phone_numbers cpn ON ( e.cid = cpn.cid AND cpn.customer_id = ( e.details::json ->>\'created_customer_id\' )::integer AND cpn.is_primary::integer = 1 )
					WHERE
						cs.id = ' . ( int ) $intContactSubmissionId . '
						AND cs.cid = ' . ( int ) $intCid . '
						AND ( cl.id IS NULL OR cl.lease_status_type_id IN( ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ', ' . CLeaseStatusType::PAST . ' ) )
						AND cs.deleted_on IS NULL
						AND cs.deleted_by IS NULL
						AND cs.customer_message_id IS NULL
					ORDER BY
						lc.lease_id DESC
					LIMIT 1';

		return self::fetchContactSubmission( $strSql, $objDatabase );
	}

	public static function fetchPaginatedResidentsFollowUpsByDashboardFilterByEventSubTypeIdsByLeaseStatusTypeIdsByCid( $objDashboardFilter, $arrintEventSubTypeIds, $arrintLeaseStatusTypeIds, $intCid, $objDatabase, $arrmixPaginationDetails = NULL, $boolIsReturnCount = false ) {
		if( false === valArr( $arrintEventSubTypeIds ) || false === valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		/*
		 On events table 'idx_events_partial_dashboard_contact_needed_temp_new' index exist and would need to be changed if following SQL is changed
		 */

		$strPaginationSql 			= ( true == valArr( $arrmixPaginationDetails ) ) ? ' ORDER BY ' . $arrmixPaginationDetails['sort_by'] . ' OFFSET ' . ( int ) $arrmixPaginationDetails['offset'] . ' LIMIT ' . ( int ) $arrmixPaginationDetails['page_limit'] : '';
		$strPriorityWhere 			= ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND e1.priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strCompanyEmployeeWhere 	= ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( e1.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR e1.leasing_agent_id IS NULL OR ( e1.company_employee_id IS NOT NULL AND e1.company_employee_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) ) )' : '';

		$strTempPropertiesSql       = self::preparePropertiesTempTable( $objDashboardFilter, $intCid );
		$strEventsSql 				= self::prepareContactNeededFromEvents( $arrintEventSubTypeIds, $arrintLeaseStatusTypeIds, $intCid, $boolIsReturnCount );
		$strContactSubmissionsSql 	= self::prepareContactNeededFromContactSubmissions( $intCid, $boolIsReturnCount );

		$strSql = 'SELECT
						concat (
						CASE
							WHEN ( e1.overdue_business_days = 0 ) THEN concat ( e1.overdue_buisness_time )
							WHEN ( e1.overdue_business_days <> 0 ) THEN
								CASE
									WHEN ( e1.overdue_business_days = 1 AND priority = 3 ) THEN concat ( \'Yesterday \', e1.overdue_buisness_time )
									WHEN ( e1.overdue_business_days = 1 AND priority = 1 ) THEN concat ( \'Tomorrow \', e1.overdue_buisness_time )
									ELSE concat ( e1.overdue_business_days, \' days\' )
							END
						END ) AS overdue_business_delay,
						e1.*
					FROM 
						( ( ' . $strEventsSql . ' ) 
							UNION 
							( ' . $strContactSubmissionsSql . ' ) ) AS e1
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
						' . $strCompanyEmployeeWhere . '
						' . $strPaginationSql;

		if( true == $boolIsReturnCount ) {
			$strSql = $strTempPropertiesSql . ' SELECT COUNT(*) AS contact_submission_count, MAX( cnt.priority ) AS priority FROM ( ' . $strSql . ') AS cnt';
			return fetchData( $strSql, $objDatabase );
		}

		$strSql = $strTempPropertiesSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	private static function preparePropertiesTempTable( $objDashboardFilter, $intCid ) {
		return '
			WITH temp_properties AS (
				SELECT
					lp.*,
					p.property_name,
					tz.time_zone_name
				FROM
					load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '] ) AS lp
					JOIN properties p ON p.cid = lp.cid AND p.id = lp.property_id
					JOIN time_zones tz ON tz.id = p.time_zone_id
					' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' WHERE lp.is_disabled = 0 ' : '' ) . '
			)';

	}

	private static function prepareContactNeededFromEvents( $arrintEventSubTypeIds, $arrintLeaseStatusTypeIds, $intCid, $boolIsReturnCount ) {
		$strSqlSelect = 'cne.id,
				cne.cid,
				ce.id AS company_employee_id,
				ca.leasing_agent_id,
				cne.lease_interval_id AS active_lease_interval_id,
				cne.priority,
				cne.overdue_business_days,
				cne.overdue_buisness_time,
				cl.primary_customer_id AS customer_id,
				\'\' AS cst_name,
				cl.name_first,
				cl.name_last,
				COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last ) AS agent_name,
				cl.primary_phone_number,
				\'\' AS email_address,
				cne.ps_product_id,
				cne.associated_event_id AS customer_message_id,
				cl.property_name,
				cne.data_reference_id,
				cne.lease_id,
				NULL::INTEGER AS lease_customer_lease_id,
				cne.created_on AS deleted_on,
				cne.scheduled_datetime AS created_on,
				COALESCE( util_get_system_translated( \'name\', est.name, est.details ),util_get_system_translated( \'name\', et.name, et.details ) ) AS name,
				cne.property_id,
				cne.notes,
				cne.event_sub_type_id,
				2 AS type,
				lce.event_type_id AS last_contacted_event_type_id,
				DATE_PART ( \'day\', NOW ( ) - ( lce.scheduled_datetime )::TIMESTAMP )::INTEGER AS last_contacted_days,
				DATE_PART ( \'day\', NOW ( ) - ( lae.scheduled_datetime )::TIMESTAMP )::INTEGER AS last_attempt_days,
				lce.scheduled_datetime::TIMESTAMP AS last_contacted_on,
				lae.event_type_id AS last_attempt_event_type_id,
				last_note_event.notes::TEXT AS last_note,
				lp.time_zone_name,
				CONCAT_WS( \' - \', lst.name, lit.name ) AS lease_status_name';

		if( true == $boolIsReturnCount ) {
			$strSqlSelect = 'cne.id,
							cne.cid,
							cne.lease_interval_id AS active_lease_interval_id,
							cne.priority,
							cne.overdue_business_days,
							cne.overdue_buisness_time,
							cne.lease_id,
							NULL::INTEGER AS lease_customer_lease_id,
							ca.leasing_agent_id,
							ce.id AS company_employee_id';
		}

		$strSql = '
			SELECT DISTINCT
				' . $strSqlSelect . '
			FROM (
					SELECT 
						DISTINCT ON (e.lease_interval_id) e.*,
						CASE
							WHEN true = e.details ? \'overdue_on\' THEN
								CASE
									WHEN NOW()::TIMESTAMP > trim( e.details::json ->>\'overdue_on\' )::TIMESTAMP THEN 3
									WHEN COALESCE( CURRENT_DATE - ( e.details::json ->>\'overdue_on\' )::DATE, 0 ) = 0 THEN 2
									ELSE 1
								END
							WHEN NOW()::TIMESTAMP > e.scheduled_datetime::TIMESTAMP THEN 3
							WHEN COALESCE( CURRENT_DATE - e.scheduled_datetime::DATE, 0 ) = 0 THEN 2
							ELSE 1
						END AS priority,
						CASE WHEN true = e.details ? \'overdue_on\' THEN ABS( COALESCE( CURRENT_DATE - (e.details::json ->> \'overdue_on\')::DATE, 0)) ELSE ABS(COALESCE(CURRENT_DATE - e.scheduled_datetime::DATE, 0)) END AS overdue_business_days,
						CASE WHEN true = e.details ? \'overdue_on\' THEN to_char(((( e.details::json ->> \'overdue_on\')::TIMESTAMP)::TIME),\'HH12:MIam\') ELSE to_char((e.scheduled_datetime)::TIME, \'HH12:MIam\') END AS overdue_buisness_time,
						( e.details -> \'auto_creation\' ->> \'triggering_event_id\' )::INTEGER AS triggering_event_id,
						e.id AS event_id,
						COALESCE( ( extract( DAY FROM trim(e.details::json ->> \'overdue_on\')::TIMESTAMP ) - extract( DAY FROM now()::TIMESTAMP ) ), 0 ) AS date_day_diff,
						e.id as eid
					FROM
						events e
						JOIN temp_properties tp ON ( tp.cid = e.cid AND tp.property_id = e.property_id )
					WHERE
						e.cid = ' . ( int ) $intCid . '
						AND e.is_deleted = FALSE
						AND e.lease_id IS NOT NULL
						AND e.event_result_id IS NULL
						AND CURRENT_DATE >= e.scheduled_datetime::DATE
						AND ( e.event_sub_type_id IN (' . implode( ',', $arrintEventSubTypeIds ) . ') 
								OR ( e.event_type_id = ' . CEventType::EMAIL_INCOMING . ' AND e.ps_product_id = ' . CPsProduct::MESSAGE_CENTER . ' AND e.is_resident = true ) 
								OR ( e.ps_product_id = ' . CPsProduct::MESSAGE_CENTER . ' AND e.event_sub_type_id IN (' . implode( ',', CEventSubType::$c_arrintActiveDashboardRenewalEventSubTypes ) . ') ) 
								OR ( e.event_type_id IN (' . implode( ',', CEventType::$c_arrintActiveDashboardEventTypes ) . ') AND e.event_sub_type_id IS NULL ) ) 
						AND (
								( e.event_type_id IN (' . implode( ',', CEventType::$c_arrintActiveDashboardEventTypes ) . ') AND e.event_sub_type_id IS NULL )
								OR e.event_sub_type_id NOT IN (' . CEventSubType::MAINTENANCE_FOLLOW_UP . ', ' . CEventSubType::MAINTENANCE_MANUAL_FOLLOW_UP . ')
							)
						ORDER BY 
							e.lease_interval_id,
							CASE
								WHEN true = e.details ? \'overdue_on\' THEN ( e.details::json ->> \'overdue_on\' )::TIMESTAMP
								ELSE e.scheduled_datetime::TIMESTAMP
							END DESC,
							e.id DESC
				) AS cne
				JOIN cached_leases cl ON cl.cid = cne.cid AND cl.id = cne.lease_id
							AND cne.property_id = cl.property_id
							AND cl.lease_status_type_id IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::PAST . ',' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
							AND CASE
									WHEN cl.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
									THEN cl.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' OR cne.event_type_id = ' . CEventType::SPECIAL_QUALIFIED . '
									WHEN cl.lease_status_type_id = ' . CLeaseStatusType::PAST . '
									THEN cne.event_type_id = ' . CEventType::EARLY_TERMINATION . '
								ELSE true
							END
				JOIN temp_properties lp ON ( lp.cid = cne.cid AND lp.property_id = cne.property_id )
				JOIN event_types et ON ( et.id = cne.event_type_id )
				LEFT JOIN event_sub_types est ON ( est.id = cne.event_sub_type_id )
				LEFT JOIN lease_customers lcs ON ( lcs.cid = cne.cid AND lcs.lease_id = cne.lease_id AND lcs.customer_id = cne.customer_id )
				LEFT JOIN lease_status_types lst ON ( lcs.lease_status_type_id = lst.id )
				LEFT JOIN lease_interval_types lit ON ( lit.id = cl.lease_interval_type_id )
				LEFT JOIN cached_applications ca ON ( cne.cid = ca.cid AND cl.id = ca.lease_id AND ca.property_id = cne.property_id AND ca.lease_interval_id = cl.active_lease_interval_id)
				LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
				LEFT JOIN LATERAL (
					SELECT
						ei.event_type_id,
						ei.scheduled_datetime
					FROM
						events ei
						LEFT JOIN event_results er ON ( er.cid = ei.cid AND er.id = ei.event_result_id )
						LEFT JOIN property_preferences pp1 ON ( ei.cid = pp1.cid AND ei.property_id = pp1.property_id AND pp1.key = \'DO_NOT_CONSIDER_LEASING_CENTER_COMMUNICATION_AS_FOLLOW_UPS\' AND pp1.value::INTEGER = 1 )
						LEFT JOIN property_preferences pp2 ON ( ei.cid = pp2.cid AND ei.property_id = pp2.property_id AND pp2.key = \'DO_NOT_CONSIDER_MESSAGE_CENTER_CORRESPONDENCE_AS_FOLLOW_UPS\' AND pp2.value::INTEGER = 1 )
					WHERE
						ei.cid = cne.cid
						AND ei.lease_interval_id = cne.lease_interval_id
						AND ei.event_type_id IN ( ' . implode( ',', CEventType::$c_arrintResidentSuccessfulContactEventTypeIds ) . ' )
						AND CASE
								WHEN ( ei.event_type_id = ' . CEventType::SCHEDULED_FOLLOWUP . ' AND ( ei.event_sub_type_id = ' . CEventSubType::MANUALLY_SCHEDULED_FOLLOW_UP . '  ) ) THEN ' . CDefaultEventResult::CANCELLED . ' = ANY ( er.default_event_result_ids ) OR ' . CDefaultEventResult::COMPLETED . ' = ANY ( er.default_event_result_ids ) OR ' . CDefaultEventResult::RESCHEDULED . ' = ANY ( er.default_event_result_ids )
								WHEN ei.event_type_id IN ( ' . implode( ',', CEventType::$c_arrintResidentDefaultSuccessfulContactEventTypeIds ) . ' ) THEN TRUE
								WHEN ( ei.event_type_id = ' . CEventType::CALL_INCOMING . ' AND ei.event_sub_type_id <> ' . CEventSubType::UNANSWERED_CALL . ' ) THEN TRUE
								WHEN ei.event_type_id IN ( ' . CEventType::CALL_OUTGOING . ' ) THEN ' . CDefaultEventResult::COMPLETED . ' = ANY ( er.default_event_result_ids ) END
						AND CASE WHEN pp1.id IS NOT NULL THEN COALESCE( ei.ps_product_id ,0 ) <> ' . CPsProduct::LEASING_CENTER . ' ELSE TRUE END
						AND CASE WHEN pp2.id IS NOT NULL THEN COALESCE( ei.ps_product_id ,0 ) <> ' . CPsProduct::MESSAGE_CENTER . ' ELSE TRUE END
						AND ei.is_deleted = FALSE
					ORDER BY
						ei.scheduled_datetime DESC, ei.id DESC
					LIMIT 1
				) AS lce ON TRUE
				LEFT JOIN LATERAL (
					SELECT
						ei.notes
					FROM
						events ei
					WHERE
						ei.cid = ' . ( int ) $intCid . '
						AND ei.event_type_id = ' . CEventType::NOTES . '
						AND ei.cid = cne.cid
						AND ei.lease_interval_id = cne.lease_interval_id
						AND ei.is_deleted = FALSE
					ORDER BY
						ei.id DESC
					LIMIT 1
				) AS last_note_event ON TRUE
				LEFT JOIN LATERAL(
					SELECT
						ei.event_type_id,
						ei.scheduled_datetime
					FROM
						events ei
						LEFT JOIN event_results er ON ( er.cid = ei.cid AND er.id = ei.event_result_id AND is_system = TRUE )
						LEFT JOIN property_preferences pp1 ON ( ei.cid = pp1.cid AND ei.property_id = pp1.property_id AND pp1.key = \'DO_NOT_CONSIDER_LEASING_CENTER_COMMUNICATION_AS_FOLLOW_UPS\' AND pp1.value::INTEGER = 1 )
						LEFT JOIN property_preferences pp2 ON ( ei.cid = pp2.cid AND ei.property_id = pp2.property_id AND pp2.key = \'DO_NOT_CONSIDER_MESSAGE_CENTER_CORRESPONDENCE_AS_FOLLOW_UPS\' AND pp2.value::INTEGER = 1 )
					WHERE
						ei.cid = cne.cid
						AND ei.lease_interval_id = cne.lease_interval_id
						AND ei.event_type_id IN ( ' . implode( ',', CEventType::$c_arrintSuccessfulResidentAttemptContactEventTypeIds ) . ' )
						AND ei.is_deleted = FALSE
						AND CASE WHEN pp1.id IS NOT NULL THEN COALESCE( ei.ps_product_id ,0 ) <> ' . CPsProduct::LEASING_CENTER . ' ELSE TRUE END
						AND CASE WHEN pp2.id IS NOT NULL THEN COALESCE( ei.ps_product_id ,0 ) <> ' . CPsProduct::MESSAGE_CENTER . ' ELSE TRUE END
					ORDER BY
						ei.scheduled_datetime DESC, ei.id DESC
					LIMIT 1
				) AS lae ON TRUE
			WHERE
				/*CASE WHEN ca.mute_followups_until IS NOT NULL THEN CURRENT_DATE >= ca.mute_followups_until::DATE ELSE true END*/
				cl.occupancy_type_id IN (' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ')
				AND CURRENT_DATE >= COALESCE( ca.mute_followups_until::DATE, CURRENT_DATE )';

		return $strSql;
	}

	private static function prepareContactNeededFromContactSubmissions( $intCid, $boolIsReturnCount ) {
		$strSqlSelect = 'cs.id,
					cs.cid,
					ce.id AS company_employee_id,
					ca.leasing_agent_id,
					cl.active_lease_interval_id,
					CASE
						WHEN TRIM ( dp.residents_follow_up ->> \'urgent_follow_up_type_ids\' ) <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up ->> \'urgent_follow_up_type_ids\' )::INTEGER [ ] ) THEN 3
						WHEN TRIM ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' )::INTEGER [ ] ) THEN 3
						WHEN TRIM ( dp.residents_follow_up ->> \'important_follow_up_type_ids\' ) <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up ->> \'important_follow_up_type_ids\' )::INTEGER [ ] ) THEN 2
						WHEN TRIM ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' )::INTEGER [ ] ) THEN 2
					  ELSE 1
					END AS overdue_business_priority,
					ABS( COALESCE( CURRENT_DATE - cs.contact_datetime::DATE , 0 ) ) AS overdue_business_days,
					to_char( ( cs.contact_datetime::DATE )::TIMESTAMP, \'HH12:MIam\' ) AS overdue_buisness_time,
					cs.customer_id,
					cst.name cst_name,
					cs.name_first,
					cs.name_last,
					COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last ) AS agent_name,
					cs.phone_number,
					CASE WHEN cs.email_address IS NULL THEN cps.username ELSE cs.email_address END as email_address,
					cs.ps_product_id,
					cs.customer_message_id,
					lp.property_name,
					MAX ( lc.id ) OVER ( PARTITION BY cs.customer_id ORDER BY lc.id DESC ) AS lease_customer_id,
					MAX ( lc.lease_id ) OVER ( PARTITION BY cs.customer_id ORDER BY lc.id DESC ) AS lease_id,
					lc.lease_id AS lease_customer_lease_id,
					cs.deleted_on,
					cs.created_on,
					cs.subject,
					cs.property_id,
					cs.message,
					cs.company_owner_id,
					1 AS type,
					0 AS last_contacted_event_type_id,
					DATE_PART ( \'day\', NOW() - ( cs.contact_datetime )::TIMESTAMP )::INTEGER AS last_contacted_days,
					NULL::INTEGER AS last_attempt_days,
					NULL::TIMESTAMP AS last_contacted_on,
					NULL::INTEGER AS last_attempt_event_type_id,
					NULL::TEXT AS last_note,
					lp.time_zone_name,
					CONCAT_WS( \' - \', lst.name, lit.name ) AS lease_status_name';

		if( true == $boolIsReturnCount ) {
			$strSqlSelect = 'cs.id,
					cs.cid,
					cl.active_lease_interval_id,
					CASE
						WHEN TRIM ( dp.residents_follow_up ->> \'urgent_follow_up_type_ids\' ) <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up ->> \'urgent_follow_up_type_ids\' )::INTEGER [ ] ) THEN 3
						WHEN TRIM ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'urgent_contact_submission_type_ids\' )::INTEGER [ ] ) THEN 3
						WHEN TRIM ( dp.residents_follow_up ->> \'important_follow_up_type_ids\' ) <> \'\' AND cs.ps_product_id = ANY ( ( dp.residents_follow_up ->> \'important_follow_up_type_ids\' )::INTEGER [ ] ) THEN 2
						WHEN TRIM ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' ) <> \'\' AND cs.contact_submission_type_id = ANY ( ( dp.residents_follow_up ->> \'important_contact_submission_type_ids\' )::INTEGER [ ] ) THEN 2
					  ELSE 1
					END AS overdue_business_priority,
					ABS( COALESCE( CURRENT_DATE - cs.contact_datetime::DATE , 0 ) ) AS overdue_business_days,
					to_char( ( cs.contact_datetime::DATE )::TIMESTAMP, \'HH12:MIam\' ) AS overdue_buisness_time,
					MAX ( lc.lease_id ) OVER ( PARTITION BY cs.customer_id ORDER BY lc.id DESC ) AS lease_id,
					lc.lease_id AS lease_customer_lease_id,
					ca.leasing_agent_id,
					ce.id AS company_employee_id';
		}

		$strSql = '
			SELECT
				*
			FROM
				( SELECT DISTINCT ON ( cs.id )
					' . $strSqlSelect . '
				FROM
					contact_submissions cs
					JOIN temp_properties lp ON ( lp.cid = cs.cid AND lp.property_id = cs.property_id )
					JOIN lease_customers lc ON ( lc.cid = cs.cid AND lc.customer_id = cs.customer_id )
					LEFT JOIN dashboard_priorities dp ON ( dp.cid = cs.cid )
					LEFT JOIN contact_submission_types cst ON ( cst.cid = cs.cid AND cst.is_published = 1 AND cst.id = cs.contact_submission_type_id AND cst.deleted_on IS NULL )
					LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = lc.cid )
					LEFT JOIN lease_intervals li ON ( li.cid = cs.cid AND li.id = cl.active_lease_interval_id AND li.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' ) )
					LEFT JOIN lease_processes lps ON ( cl.id = lps.lease_id AND cs.cid = lps.cid )
					LEFT JOIN lease_status_types lst ON ( lc.lease_status_type_id = lst.id )
					LEFT JOIN lease_interval_types lit ON ( lit.id = cl.lease_interval_type_id )
					LEFT JOIN property_preferences pp ON ( cs.property_id = pp.property_id AND cs.cid = pp.cid AND pp.key =\'PAST_RESIDENT_LOGIN_TOLERANCE_DAYS\' )
					LEFT JOIN cached_applications ca ON ( cl.cid = ca.cid AND cl.id = ca.lease_id AND ca.property_id = cl.property_id )
					LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
					LEFT JOIN customer_portal_settings cps ON ( cps.cid=cs.cid AND cps.customer_id=cs.customer_id )
				WHERE
					cs.cid = ' . ( int ) $intCid . '
					AND ( CASE
							WHEN cl.lease_status_type_id = ' . CLeaseStatusType::PAST . ' AND COALESCE( lps.move_out_date, li.lease_end_date ) + COALESCE( pp.value::INTEGER, ' . CPropertyPreference::PAST_RESIDENT_LOGIN_TOLERANCE_DAYS . ' ) > CURRENT_DATE
							THEN cl.id IS NULL OR cl.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintApprovedLeaseStatusTypeIds ) . ' )
							ELSE cl.id IS NULL OR cl.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ' )
						END )
					AND cs.deleted_on IS NULL
					AND cs.deleted_by IS NULL
					AND cs.customer_message_id IS NULL
					AND cs.id NOT IN ( SELECT DISTINCT ON ( e.data_reference_id ) e.data_reference_id
								  FROM
									  events e
								  WHERE
									  e.data_reference_id IS NOT NULL
									  AND e.cid = ' . ( int ) $intCid . '
									  AND e.cid = cs.cid
									  AND e.property_id = cs.property_id
									  AND ( e.customer_id = cs.customer_id OR e.customer_id IN ( SELECT customer_id FROM customer_connections where connected_customer_id = cs.customer_id ) )
									  AND e.event_type_id IN( ' . CEventType::CONTACT_SUBMISSION_CALL_OUTGOING . ', ' . CEventType::CONTACT_SUBMISSION_RESIDENT_VISIT . ', ' . CEventType::TENANT_VISIT . ' ) 
					) 
			) AS cs1
		WHERE
			cs1.lease_id = cs1.lease_customer_lease_id';

		return $strSql;
	}
	public static function fetchCustomerContactSubmissionByIdByCid( $intContactSubmissionId, $intCid, $objDatabase, $boolIsEmailInsertion = false ) {

		if( true == $boolIsEmailInsertion ) {
			$strSqlCondition = 'coalesce(c.name_last,cs.name_last)';
		}else{
			$strSqlCondition = 'c.name_last';
		}

		$strSql = 'SELECT
	  	cs.id,
			cs.property_id,
			CASE 
				WHEN e.details::json ->>\'created_customer_id\' IS NULL
					THEN cs.email_address
				ELSE
					c.email_address
			END AS email_address,
			cs.phone_number,
			cs.office_number,
			cs.mobile_number,
			cs.move_out_date,
			cs.response_request_type_id,
			cs.subject,
			cs.message,
			cs.customer_message_id,
			cs.contact_submission_type_id,
			cs.company_employee_id,
			cs.created_on,
			cs.cid,
			cs.ps_product_id,
			cs.ap_payee_id,
			CASE 
				WHEN e.details::json ->>\'created_customer_id\' IS NULL
					THEN cs.customer_id
				ELSE
					c.id
			END AS customer_id,
			CASE 
				WHEN e.details::json ->>\'created_customer_id\' IS NULL
					THEN cs.name_first
				ELSE
					c.name_first
			END AS name_first,
			CASE 
				WHEN e.details::json ->>\'created_customer_id\' IS NULL
					THEN cs.name_last
				ELSE
				'.$strSqlCondition.'
			END AS name_last
		FROM
			contact_submissions cs
			LEFT JOIN events e ON ( cs.cid = e.cid AND cs.customer_id = e.customer_id AND e.event_type_id = ' . CEventType::CONTACT_SUBMISSION . ' AND cs.created_on::timestamp = e.created_on::timestamp )
			LEFT JOIN customers c ON ( e.cid = c.cid AND c.id = ( e.details::json ->>\'created_customer_id\' )::integer )
		WHERE
			cs.id = ' . ( int ) $intContactSubmissionId . '
			AND cs.cid = ' . ( int ) $intCid . '
		LIMIT 1';

		return self::fetchContactSubmission( $strSql, $objDatabase );
	}


}
?>