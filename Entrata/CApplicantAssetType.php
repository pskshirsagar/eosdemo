<?php

class CApplicantAssetType extends CBaseApplicantAssetType {

	const CERTIFICATES_OF_DEPOSIT  	= 1;
	const STOCKS_OR_BONDS 		   	= 2;
	const CHECKING_ACCOUNTS		   	= 3;
	const SAVINGS_ACCOUNTS		   	= 4;
	const IRS_OR_RETIREMENT_ACCOUNTS = 5;
	const MUTUAL_FUNDS 				= 6;
	const TRUST_ACCOUNTS 			= 7;
	const CASH_OR_SAFETY_DEPOSIT	= 8; // Cash on hand or Safety Deposit Box
	const LIFE_INSURANCE			= 9;
	const PERSONAL_PROPERTY			= 10; // Personal Property Held as an Investment
	const REAL_ESTATE_SALE			= 11;
	const REAL_ESTATE_RENT			= 12;
	const OTHER_CURRENT_ASSET		= 13;

	protected $m_strApplicantAssetTypeCustomName;

	/**
	 * Get Functions
	 */

	public function getApplicantAssetTypeCustomName() {
		return ( 0 < strlen( $this->m_strApplicantAssetTypeCustomName ) ) ? $this->m_strApplicantAssetTypeCustomName : $this->getName();
	}

	/**
	 * Set Functions
	 */

	public function setApplicantAssetTypeCustomName( $strApplicantAssetTypeCustomeName ) {
		if( 0 < strlen( trim( $strApplicantAssetTypeCustomeName ) ) ) {
			$this->m_strApplicantAssetTypeCustomName = CStrings::strTrimDef( $strApplicantAssetTypeCustomeName, 50, NULL, true );
		} else {
			$this->m_strApplicantAssetTypeCustomName = $this->getName();
		}
	}

}
?>