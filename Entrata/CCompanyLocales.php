<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyLocales
 * Do not add any new functions to this class.
 */

class CCompanyLocales extends CBaseCompanyLocales {

	public static function fetchCompanyLocaleCodesByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						locale_code
					FROM 
						company_locales
					WHERE cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyLanguagesByCid( $intCid, $objDatabase, $boolSkipDefault = false ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strJoinClause = ( true == $boolSkipDefault ) ? ' JOIN clients c ON ( cl.cid = c.id AND ( cl.locale_code != c.locale_code OR c.locale_code IS NULL ) ) ' : '';

		$strSql = 'SELECT 
						l.locale_code,
						l.iso_language_name,
						CASE
							WHEN l.iso_region_code IS NOT NULL THEN 
								l.iso_language_name || \'(\' || l.iso_region_code || \')\'
						ELSE l.iso_language_name
						END AS language_name
					FROM 
						company_locales cl
						JOIN locales l on (cl.locale_code = l.locale_code AND l.is_published)
						' . $strJoinClause . '
						WHERE 
							cl.cid = ' . ( int ) $intCid . '
						ORDER BY 
							l.locale_code';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLocaleCodeAndCompanyRegionByCid( $intCid, $objDatabase, $boolSkipDefault = false ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strJoinClause = ( true == $boolSkipDefault ) ? ' JOIN clients c ON ( cl.cid = c.id AND ( cl.locale_code != c.locale_code OR c.locale_code IS NULL ) ) ' : '';

		$strSql = 'SELECT 
						l.locale_code,
						l.iso_region_name
					FROM 
						company_locales cl
						JOIN locales l on (cl.locale_code = l.locale_code AND l.is_published)
						' . $strJoinClause . '
						WHERE 
							cl.cid = ' . ( int ) $intCid . '
						ORDER BY 
							l.locale_code';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyLocalesByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cl.id,
						cl.locale_code,
						l.iso_language_name,
						l.iso_region_name,
						CASE WHEN cl.locale_code = c.locale_code THEN true ELSE false END AS is_default_language,
						count ( pl.locale_code ) AS property_count
					FROM
						company_locales cl
						JOIN locales l ON ( l.locale_code = cl.locale_code )
						JOIN clients c ON ( cl.cid = c.id ) 
						LEFT JOIN property_locales AS pl ON ( pl.locale_code = cl.locale_code AND cl.cid = pl.cid )
					WHERE
						cl.cid = ' . ( int ) $intCid . '
					GROUP BY
						cl.id,
						cl.locale_code,
						l.iso_language_name,
						l.iso_region_name,
						is_default_language
					ORDER BY
						is_default_language DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyLocaleCodeCountByCidByLocaleCode( $intCid, $strLocaleCode, $objDatabase ) {

		if( !valId( $intCid ) || !valStr( $strLocaleCode ) ) {
			return NULL;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND locale_code = \'' . addslashes( $strLocaleCode ) . '\'';

		return self :: fetchCompanyLocaleCount( $strWhere, $objDatabase );
	}

	public static function fetchCompanyLocaleCodeAndLanguageByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'select 
						cl.locale_code AS locale_code,
						l.iso_language_name AS language_name
					from 
						company_locales cl
						LEFT JOIN locales l ON( cl.locale_code = l.locale_code )
					where 
							cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyLocaleCodeCountByCid( $intCid, $objDatabase ) {

		if( !valId( $intCid ) ) {
			return NULL;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $intCid;

		return self :: fetchCompanyLocaleCount( $strWhere, $objDatabase );
	}

}

?>