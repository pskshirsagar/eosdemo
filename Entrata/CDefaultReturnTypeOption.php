<?php

class CDefaultReturnTypeOption extends CBaseDefaultReturnTypeOption {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReturnTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncrementReturnCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFeeGenerating() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}

?>