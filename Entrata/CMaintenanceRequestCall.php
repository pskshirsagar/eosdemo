<?php

class CMaintenanceRequestCall extends CBaseMaintenanceRequestCall {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCallId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>