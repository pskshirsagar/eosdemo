<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoneygramLocations
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMoneygramLocations extends CBaseMoneygramLocations {

	public static function fetchPaginatedMoneygramLocations( $intPageNumber, $intPageSize, $objMoneygramLocationsSearch, $objDatabase ) {

		$arrstrAndSearchParameters = [];
		$strAdditionalJoins = '';

		if( false == is_null( $objMoneygramLocationsSearch->getPostalCode() ) ) {
			$arrstrAndSearchParameters[] = ' ml.postal_code LIKE \'\%' . $objMoneygramLocationsSearch->getPostalCode() . '\%\'';
		} else {
			$arrstrAndSearchParameters[] = ( ' ml.postal_code LIKE \'\%' . $objMoneygramLocationsSearch->getPostalCode() . '\%\' or ( 1 = 1 ) ' );

		}

		$strSql = 'SELECT
						ml.*
					FROM
						moneygram_locations ml ' . $strAdditionalJoins . '
					WHERE
						' . implode( ' AND ', $arrstrAndSearchParameters ) . '
					ORDER BY
						ml.name ASC OFFSET ' . ( int ) ( ( $intPageNumber - 1 ) * $intPageSize ) . '
					LIMIT
						' . ( int ) $intPageSize;

		return self::fetchMoneygramLocations( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAdminArPaymentsTotalCount( $objMoneygramLocationsSearch, $objDatabase ) {

		$arrstrAndSearchParameters = [];
		$strAdditionalJoins = '';

		if( false == is_null( $objMoneygramLocationsSearch->getPostalCode() ) ) {
			$arrstrAndSearchParameters[] = ' ml.postal_code LIKE \'\%' . $objMoneygramLocationsSearch->getPostalCode() . '\%\'';
		} else {
			$arrstrAndSearchParameters[] = ( ' ml.postal_code LIKE \'\%' . $objMoneygramLocationsSearch->getPostalCode() . '\%\' or ( 1 = 1 ) ' );

		}

		$strSql = 'SELECT
						count(ml.id)
					FROM
						moneygram_locations ml ' . $strAdditionalJoins . '
					WHERE
						' . implode( ' AND ', $arrstrAndSearchParameters );

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchNeighborhoodMoneygramLocationsByPostalCodeByMiles( $strLatitude, $strLongitude, $objDatabase, $intNoOfRecordsToReturn = 3, $intMilesWithin = 10 ) {

		if( true == is_null( $strLatitude ) || true == is_null( $strLongitude ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
                    FROM
						(
							SELECT
								*,
								trunc((
									3958.75 * ACos( Sin( ' . addslashes( $strLatitude ) . ' / 57.2958 ) * Sin( latitude::Float / 57.2958 ) + Cos(' . addslashes( $strLatitude ) . ' / 57.2958 ) * Cos( latitude::Float / 57.2958 ) * Cos( longitude::Float / 57.2958 - ' . addslashes( $strLongitude ) . ' / 57.2958 ) )
								)::numeric, 1 ) as miles
							FROM
								 moneygram_locations ml
							WHERE
								 ml.latitude IS NOT NULL
								 AND ml.latitude <> \'\'
								 AND ml.longitude IS NOT NULL
								 AND ml.longitude <> \'\'
								 AND ml.is_blacklisted <> 1
						) as sub_query
					WHERE
						miles <= ' . ( float ) $intMilesWithin . '
                    ORDER by
						miles ASC
					LIMIT
						' . ( int ) $intNoOfRecordsToReturn . ';';

		return self::fetchMoneygramLocations( $strSql, $objDatabase );
	}

    public static function fetchMoneygramLocationByAddressByCityByStateByName( $strAddress, $strCity, $strStateCode, $strName, $objClientDatabase ) {
        $strSql = 'SELECT 
						*
					FROM
						moneygram_locations
					WHERE
						address = \'' . pg_escape_string( $strAddress ) . '\'
						AND city = \'' . pg_escape_string( $strCity ) . '\'
						AND state_code = \'' . pg_escape_string( $strStateCode ) . '\'
						AND name = \'' . pg_escape_string( $strName ) . '\'
					LIMIT 1;';

        return self::fetchMoneygramLocation( $strSql, $objClientDatabase );
    }

    public static function deleteMoneyGramLocationsByUnmatchedIds( $arrintMoneygramLocationsToKeep,  $objClientDatabase ) {

        if( 0 == \Psi\Libraries\UtilFunctions\count( $arrintMoneygramLocationsToKeep ) ) {
            return false;
        }

        $strSql = 'DELETE 
                    FROM 
                        moneygram_locations 
                    WHERE 
                        id NOT IN (' . ( implode( ',', $arrintMoneygramLocationsToKeep ) ) . ');';

        if( false == $objClientDatabase->execute( $strSql ) ) {
            return false;
        }

        return true;
    }

}
?>