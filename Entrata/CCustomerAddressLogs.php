<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAddressLogs
 * Do not add any new functions to this class.
 */

class CCustomerAddressLogs extends CBaseCustomerAddressLogs {

	public static function fetchLatestCustomerAddressLogIdByCustomerAddressIdByCid( $intCustomerAddressId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cal.id
				   FROM
						customer_address_logs cal
				   WHERE
						cal.cid = ' . ( int ) $intCid . '
                        AND cal.customer_address_id = ' . ( int ) $intCustomerAddressId . '
				   ORDER BY 
					    cal.id DESC
				   LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestCustomerAddressLogIdsByCustomerIdsByAddressTypeIdsByCid( $arrintCustomerIds, $arrintAddressTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT	
						*
					FROM
						(
					      SELECT 
					      cal.cid,
					      cal.id,
					      cal.customer_id,
					      cal.customer_address_id,
					      cal.address_type_id,
					      ca.deleted_on,
					      rank() OVER(
					      PARTITION 
					      BY 
					          cal.cid,
					          cal.customer_id,
					          cal.address_type_id
					      ORDER BY 
					          cal.id DESC    
					      ) as rank 
					  FROM
					      customer_addresses ca
					      JOIN customer_address_logs cal ON ( ca.cid = cal.cid AND ca.id = cal.customer_address_id ) 
					  WHERE
					      cal.cid = ' . ( int ) $intCid . '
					      AND cal.customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
					      AND cal.address_type_id in ( ' . sqlIntImplode( $arrintAddressTypeIds ) . ' )
					    ) as temp 
					WHERE 
						rank = 1   
	';

		return fetchData( $strSql, $objDatabase );
	}

}
?>