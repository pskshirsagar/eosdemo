<?php

class CForm extends CBaseForm {

	protected $m_strDocumentTitle;

	/**
	 * Create Functions
	 */

    public function createFormAnswer() {

    	$objFormAnswer = new CFormAnswer();
    	$objFormAnswer->setCid( $this->m_intCid );
    	$objFormAnswer->setFormId( $this->getId() );

    	return $objFormAnswer;
    }

    /**
     * Get Functions
     */

	public function getDocumentTitle() {
		return $this->m_strDocumentTitle;
	}

    /**
     * Set Functions
     */

	public function setDocumentTitle( $strDocumentTitle ) {
		$this->m_strDocumentTitle = CStrings::strTrimDef( $strDocumentTitle, 240, NULL, true );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['document_title'] ) ) $this->setDocumentTitle( $arrValues['document_title'] );

        return;
    }

	/**
	 * Validate Functions
	 */

    public function valDocumentId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valFormStatusTypeId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;

        // Validation example

        if( true == is_null( $this->getNameFirst() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valNameMiddle() {
        $boolIsValid = true;

        // Validation example

        if( true == is_null( $this->getNameMiddle() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', 'Middle name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;

        // Validation example

        if( true == is_null( $this->getNameLast() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDigitalSignature() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valReferrerUrl() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valNameFirst();
            	$boolIsValid &= $this->valNameMiddle();
            	$boolIsValid &= $this->valNameLast();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Fetch Functions
	 */

	public function fetchFormAnswers( $objDatabase ) {

		return CFormAnswers::fetchFormAnswersByFormIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDocument( $objDatabase ) {
		return CDocuments::fetchDocumentByIdByCid( $this->getDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDocument( $objDatabase ) {
		return CDocuments::fetchApplicationDocumentByIdByCid( $this->getDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomer( $objDatabase ) {

    	return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
    }

    public function fetchApplicant( $objDatabase ) {

    	return CApplicants::fetchSimpleApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $objDatabase );
    }

    public function fetchProperty( $objDatabase ) {

        $this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
        return $this->m_objProperty;
    }

    public function fetchClient( $objDatabase ) {

        $this->m_objClient = CClients::fetchClientById( $this->getCid(), $objDatabase );
        return $this->m_objClient;
    }

    public function fetchApplicationFormAnswersByDocumentComponentIds( $arrintDocumentComponentIds, $objDatabase ) {
    	return CFormAnswers::fetchApplicationFormAnswersByFormIdByDocumentComponentIdsByCid( $this->getId(), $arrintDocumentComponentIds, $this->getCid(), $objDatabase );
    }

    public function fetchApplicationFormAnswers( $objDatabase ) {
    	return CFormAnswers::fetchFormAnswersByFormIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
    }

}
?>