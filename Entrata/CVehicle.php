<?php

class CVehicle extends CBaseVehicle {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Vehicle name is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valStartingMileage() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLicensePlateNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsActive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPersonal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyVehicleMaintenanceRequests( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
    		$arrintMaintenanceRequestsIds = ( array ) \Psi\Eos\Entrata\CMaintenanceRequestMileages::createService()->fetchCustomMaintenanceRequestsByVehicleIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintMaintenanceRequestsIds ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_id', __( 'Maintenance request(s) exists for the Vehicle.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {

    	require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

    	$boolIsValid = true;
        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valName();
        		break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName();
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valCompanyVehicleMaintenanceRequests( $objDatabase );
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intUserId );
    	$this->setDeletedOn( ' NOW() ' );
    	$this->setUpdatedBy( $intUserId );
    	$this->setUpdatedOn( ' NOW() ' );

    	if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
    		return true;
    	}
    }

}
?>