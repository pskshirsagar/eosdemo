<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCalls
 * Do not add any new functions to this class.
 */

class CScheduledCalls extends CBaseScheduledCalls {

	public static function fetchPaginatedNewScheduledCallsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objPagination, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON( sc.id )
   						sc.*,
						cu.username
					FROM
						scheduled_calls sc
						JOIN property_scheduled_calls psc ON ( sc.cid = psc.cid AND sc.id = psc.scheduled_call_id )
						LEFT JOIN company_users cu ON ( cu.cid = sc.cid AND cu.id = sc.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						sc.cid = ' . ( int ) $intCid;

		$strSql .= ( true == valArr( $arrintPropertyIds ) ) ? ' AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';

		$strSql .= ' AND sc.deleted_by IS NULL
					 AND sc.deleted_on IS NULL
					ORDER BY
						sc.id DESC
					OFFSET ' . $objPagination->getOffset() . '
					LIMIT ' . $objPagination->getPageSize();

		return self::fetchScheduledCalls( $strSql, $objDatabase );
	}

	public static function fetchPaginatedNewScheduledCallsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( DISTINCT sc.id )
					FROM
						scheduled_calls sc
						JOIN property_scheduled_calls psc ON ( sc.cid = psc.cid AND sc.id = psc.scheduled_call_id )
					WHERE
						sc.cid = ' . ( int ) $intCid;

		$strSql .= ( true == valArr( $arrintPropertyIds ) ) ? ' AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : '';

		$strSql .= ' AND sc.deleted_by IS NULL
					 AND sc.deleted_on IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	public static function fetchCustomUnpostedScheduledCalls( $objDatabase ) {

		$strSql = 'SELECT
    					sc.*,
    					sct.name
					FROM
	           	 	   	scheduled_calls sc
						JOIN scheduled_call_chunks scc ON ( scc.cid = sc.cid AND scc.scheduled_call_id = sc.id )
						LEFT JOIN scheduled_call_types sct ON ( sc.scheduled_call_type_id = sct.id )
					WHERE
						( CURRENT_DATE >= sc.start_date AND ( sc.end_date IS NULL OR CURRENT_DATE <= sc.end_date ) )
						AND (
								( sc.last_sent_on IS NULL )
								OR ( sc.last_sent_on IS NOT NULL AND sc.call_frequency_id = ' . CCallFrequency::DAILY . ' AND NOW() > ( sc.last_sent_on + INTERVAL \'1 day\' ) )
								OR ( sc.last_sent_on IS NOT NULL AND sc.call_frequency_id = ' . CCallFrequency::WEEKLY . ' AND NOW() > ( sc.last_sent_on + INTERVAL \'1 week\' ) )
								OR ( sc.last_sent_on IS NOT NULL AND sc.call_frequency_id = ' . CCallFrequency::MONTHLY . ' AND NOW() > ( sc.last_sent_on + INTERVAL \'1 month\' ) )
								OR ( sc.last_sent_on IS NOT NULL AND sc.call_frequency_id = ' . CCallFrequency::QUARTERLY . ' AND NOW() > ( sc.last_sent_on + INTERVAL \'3 months\' ) )
								OR ( sc.last_sent_on IS NOT NULL AND sc.call_frequency_id = ' . CCallFrequency::ANNUALLY . ' AND NOW() > ( sc.last_sent_on + INTERVAL \'1 year\' ) ) )
						AND	CURRENT_TIME >= sc.begin_send_time
						AND	CURRENT_TIME <= sc.end_send_time
						AND	sc.approved_on IS NOT NULL
						AND	sc.deleted_on IS NULL';

		return self::fetchScheduledCalls( $strSql, $objDatabase );
	}

}
?>