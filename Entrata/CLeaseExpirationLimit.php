<?php

class CLeaseExpirationLimit extends CBaseLeaseExpirationLimit {

    public function valMonth() {
        $boolIsValid = true;

        if( true == is_null( $this->getMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', 'Month is required.' ) );
        } elseif( false == is_numeric( $this->getMonth() ) || 1 > $this->getMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', 'Invalid Month.' ) );
        } elseif( date( 'Y' ) == $this->getYear() && date( 'm' ) > $this->getMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', 'Past Month.' ) );
        }

        return $boolIsValid;
    }

    public function valYear() {
        $boolIsValid = true;

        if( true == is_null( $this->getYear() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year is required.' ) );
        } elseif( false == is_numeric( $this->getYear() ) || date( 'Y' ) > $this->getYear() ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Invalid or past year.' ) );
        }

        return $boolIsValid;
    }

    public function valLeaseExpirationTolerance() {
        $boolIsValid = true;

        if( true == is_null( $this->getLeaseExpirationTolerance() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_expiration_tolerance', ' Allowable lease expiration tolerance is required.' ) );
        } elseif( false == is_numeric( $this->getLeaseExpirationTolerance() ) || 1 > $this->getLeaseExpirationTolerance() ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_expiration_tolerance', ' Invalid lease expiration tolerance.' ) );
        } elseif( $this->getLeaseExpirationCount() > $this->getLeaseExpirationTolerance() ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_expiration_tolerance', ' Lease expiration tolerance should not be less than lease expiration occurred.' ) );
        }

        return $boolIsValid;
    }

    public function valLeaseExpirationCount() {
        $boolIsValid = true;

        if( false == is_null( $this->getLeaseExpirationCount() ) && $this->getLeaseExpirationCount() > $this->getLeaseExpirationTolerance() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_expiration_count', 'Lease expiration count should not be greater than allowable lease expiration.' ) );
        }

        return $boolIsValid;
    }

    public function valDuplicityOfLeaseExpirationLimit( $objDatabase ) {

    	$boolIsValid = true;

		$objLeaseExpirationLimit = \Psi\Eos\Entrata\CLeaseExpirationLimits::createService()->fetchLeaseExpirationLimitByPropertyIdByYearByMonthByCid( $this->getPropertyId(), $this->getYear(), $this->getMonth(), $this->getCid(), $objDatabase );

		if( true == valObj( $objLeaseExpirationLimit, 'CLeaseExpirationLimit' ) && ( true == is_null( $this->getId() ) || $this->getId() != $objLeaseExpirationLimit->getId() ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Lease expiration for year ' . $this->getYear() . ', month ' . $this->getMonth() . ' is already set.' ) );
		}

		return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valYear();
            	$boolIsValid &= $this->valMonth();
            	$boolIsValid &= $this->valLeaseExpirationTolerance();
            	$boolIsValid &= $this->valDuplicityOfLeaseExpirationLimit( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>