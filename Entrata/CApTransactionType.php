<?php

class CApTransactionType extends CBaseApTransactionType {

	const STANDARD					    = 1;
	const STANDARD_REIMBURSEMENT	    = 2;
	const BILL_BACK_REIMBURSEMENT	    = 3;
	const STANDARD_REIMBURSEMENT_NEW    = 9;
	const CHECK						    = 101;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'STANDARD',					self::STANDARD );
		$objSmarty->assign( 'STANDARD_REIMBURSEMENT',	self::STANDARD_REIMBURSEMENT );
		$objSmarty->assign( 'BILL_BACK_REIMBURSEMENT',	self::BILL_BACK_REIMBURSEMENT );
		$objSmarty->assign( 'CHECK',					self::CHECK );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>