<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyConciergeServices
 * Do not add any new functions to this class.
 */

class CCompanyConciergeServices extends CBaseCompanyConciergeServices {

	public static function fetchCompanyConciergeServiceByIdByPropertyIdByCid( $intCompanyConciergeServiceId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyConciergeService( sprintf( 'SELECT ccs.* FROM %s AS ccs , %s AS pcs WHERE pcs.company_concierge_service_id::integer = ccs.id::integer AND pcs.cid = ccs.cid AND ccs.cid = %d AND pcs.property_id = %d AND ccs.id = %d', 'company_concierge_services', 'property_concierge_services', ( int ) $intCid, ( int ) $intPropertyId, ( int ) $intCompanyConciergeServiceId ), $objDatabase );
	}

	public static function fetchCompanyConciergeServicesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsPublished = NULL ) {

		if( false == is_null( $boolIsPublished ) ) {
			$strSql = sprintf( 'SELECT ccs.* FROM %s ccs, %s pcs WHERE pcs.company_concierge_service_id = ccs.id AND pcs.cid = ccs.cid AND pcs.cid = %d AND pcs.property_id = %d  AND ccs.is_published = %d ORDER BY pcs.order_num', 'company_concierge_services', 'property_concierge_services', ( int ) $intCid, ( int ) $intPropertyId, ( int ) $boolIsPublished );
		} else {
			$strSql = sprintf( 'SELECT ccs.* FROM %s ccs, %s pcs WHERE pcs.company_concierge_service_id = ccs.id AND pcs.cid = ccs.cid AND pcs.cid = %d AND pcs.property_id = %d ORDER BY pcs.order_num', 'company_concierge_services', 'property_concierge_services', ( int ) $intCid, ( int ) $intPropertyId );
		}

		return self::fetchCompanyConciergeServices( $strSql, $objDatabase );
	}

	public static function fetchCompanyConciergeServicesCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsPublished = NULL ) {
		if( false == is_null( $boolIsPublished ) ) {
			$strSql = sprintf( 'SELECT count(ccs.id) FROM company_concierge_services AS ccs, property_concierge_services AS pcs WHERE pcs.company_concierge_service_id::integer = ccs.id::integer AND pcs.cid = ccs.cid AND pcs.property_id = %d AND ccs.cid = %d AND ccs.is_published = %d', ( int ) $intPropertyId, ( int ) $intCid, ( int ) $boolIsPublished );
		} else {
			$strSql = sprintf( 'SELECT count(ccs.id) FROM company_concierge_services AS ccs, property_concierge_services AS pcs WHERE pcs.company_concierge_service_id::integer = ccs.id::integer AND pcs.cid = ccs.cid AND pcs.property_id = %d AND ccs.cid = %d', ( int ) $intPropertyId, ( int ) $intCid );
		}

		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}
}
?>