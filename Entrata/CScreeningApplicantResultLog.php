<?php

class CScreeningApplicantResultLog extends CBaseScreeningApplicantResultLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantResultId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantScreenTypeResult() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valScreeningRecommendationTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getScreeningApplicantScreenTypeResultByScreenType( $intScreenTypeId ) {
		if( false == valObj( $this->getScreeningApplicantScreenTypeResult(), 'stdClass' ) || false == valId( $intScreenTypeId ) || false == property_exists( $this->getScreeningApplicantScreenTypeResult(), $intScreenTypeId ) ) return;
		return $this->getScreeningApplicantScreenTypeResult()->$intScreenTypeId;
	}

	public function checkScreenTypeResultByScreenTypeIdByKey( $intScreenTypeId, $strStatusTypeIdKey ) {
		if( ( true == valObj( $this->getScreeningApplicantScreenTypeResult(), 'stdClass' ) )
		    && ( true == property_exists( $this->getScreeningApplicantScreenTypeResult(), $intScreenTypeId ) )
		    && ( CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW != $this->getScreeningApplicantScreenTypeResult()->$intScreenTypeId->$strStatusTypeIdKey ) ) {

			return true;
		}

		return false;
	}

}
?>