<?php

class CViolationType extends CBaseViolationType {

	const VIOLATION_TYPE_PET = 1;
	const VIOLATION_TYPE_UNAUTHORIZED_GUEST = 2;
	const VIOLATION_TYPE_EXTERIOR_VIOLATION = 3;
	const VIOLATION_TYPE_CRIMINAL_ACTIVITY  = 4;
	const VIOLATION_TYPE_SMOKING            = 5;
	const VIOLATION_TYPE_TRASH              = 6;
	const VIOLATION_TYPE_NOISE              = 7;
	const VIOLATION_TYPE_INTERIOR_VIOLATION = 8;
	const VIOLATION_TYPE_SAFETY_HAZARD      = 9;
	const VIOLATION_TYPE_GENERAL_LEASE_VIOLATION = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getViolationTypeIds() {
		return [
			SELF::VIOLATION_TYPE_PET                     => [ 'id' => '1', 'name' => __( 'Pet' ) ],
			SELF::VIOLATION_TYPE_UNAUTHORIZED_GUEST      => [ 'id' => '2', 'name' => __( 'Unauthorized Guest' ) ],
			SELF::VIOLATION_TYPE_EXTERIOR_VIOLATION      => [ 'id' => '3', 'name' => __( 'Exterior Violation' ) ],
			SELF::VIOLATION_TYPE_CRIMINAL_ACTIVITY       => [ 'id' => '4', 'name' => __( 'Criminal Activity' ) ],
			SELF::VIOLATION_TYPE_SMOKING                 => [ 'id' => '5', 'name' => __( 'Smoking' ) ],
			SELF::VIOLATION_TYPE_TRASH                   => [ 'id' => '6', 'name' => __( 'Trash' ) ],
			SELF::VIOLATION_TYPE_NOISE                   => [ 'id' => '7', 'name' => __( 'Noise' ) ],
			SELF::VIOLATION_TYPE_INTERIOR_VIOLATION      => [ 'id' => '8', 'name' => __( 'Interior Violation' ) ],
			SELF::VIOLATION_TYPE_SAFETY_HAZARD           => [ 'id' => '9', 'name' => __( 'Safety Hazard' ) ],
			SELF::VIOLATION_TYPE_GENERAL_LEASE_VIOLATION => [ 'id' => '10', 'name' => __( 'General Lease Violation' ) ]
		];
	}

}
?>