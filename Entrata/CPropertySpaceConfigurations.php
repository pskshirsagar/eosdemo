<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySpaceConfigurations
 * Do not add any new functions to this class.
 */

class CPropertySpaceConfigurations extends CBasePropertySpaceConfigurations {

	public static function fetchActivePropertySpaceConfigurationCountBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
			if( false == is_numeric( $intSpaceConfigurationId ) ) return NULL;

			$strWhereSql = ' WHERE space_configuration_id = ' . ( int ) $intSpaceConfigurationId . '
						AND cid = ' . ( int ) $intCid . '
						LIMIT 1';

		return self::fetchPropertySpaceConfigurationCount( $strWhereSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsSpaceConfigurationIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						space_configuration_id
					FROM
						property_space_configurations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . ' 
					ORDER BY order_num';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrintSpaceConfigurationIds = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrintSpaceConfigurationIds[] = $arrmixValue['space_configuration_id'];
			}
		}

		return $arrintSpaceConfigurationIds;
	}

	public static function fetchCustomPropertySpaceConfigurationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintSkipSpaceConfigurationIds = [] ) {
		$strWhere = '';

		if( true == valArr( $arrintSkipSpaceConfigurationIds ) ) {
			$strWhere = ' AND space_configuration_id NOT IN(' . implode( ',', $arrintSkipSpaceConfigurationIds ) . ' ) ';
		}

		$strSql = 'SELECT
						*
					FROM
						property_space_configurations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						' . $strWhere . '
					ORDER BY
						order_num';

		return self::fetchPropertySpaceConfigurations( $strSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsDataByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolShowOnWebsite = false, $strOrderByField = NULL, $boolHideConventional = false ) {

		$strWhereCondition = '';

		if( true == $boolShowOnWebsite ) {
			$strWhereCondition .= ' AND psc.show_in_entrata = true AND psc.show_on_website = true';
		}

		if( true == $boolHideConventional ) {
			$strWhereCondition .= ' AND sc.id <> ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . ' ';
		}

		$strSql = 'SELECT
						sc.id,
						sc.name
					FROM
						property_space_configurations psc
						JOIN space_configurations sc ON ( sc.cid = psc.cid AND sc.id = psc.space_configuration_id AND sc.deleted_on IS NULL )
					WHERE
						psc.cid = ' . ( int ) $intCid . '
						AND psc.property_id = ' . ( int ) $intPropertyId .
						$strWhereCondition;

		if( true == valStr( $strOrderByField ) ) {
			$strSql .= ' ORDER BY ' . $strOrderByField;
		} else {
			$strSql .= ' ORDER BY psc.space_configuration_id';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsDataByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT sc.id,
						sc.name,
						psc.property_id,
						psc.cid
					FROM property_space_configurations psc
						JOIN space_configurations sc ON (sc.cid = psc.cid AND sc.id = psc.space_configuration_id AND sc.deleted_on IS NULL )
					WHERE psc.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedPropertiesBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase, $boolIsReturnObjects = false, $boolCheckForOccupancyTypes = false, $arrintOccupancyTypeIds = NULL ) {

		$strJoinSql        = '';
		$strWhereCondition = '';

		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strWhereCondition .= ' AND p.occupancy_type_ids && ARRAY[' . implode( ',', $arrintOccupancyTypeIds ) . ']';
		} elseif( true == $boolCheckForOccupancyTypes ) {
			$strJoinSql        = ' JOIN space_configurations sc ON ( sc.cid = psc.cid AND sc.id = psc.space_configuration_id )';
			$strWhereCondition .= ' AND p.occupancy_type_ids && sc.occupancy_type_ids::INTEGER[]';
		}

		$strSql = 'SELECT DISTINCT
						psc.id,
						psc.cid,
						psc.property_id,
						p.property_name,
						p.is_disabled,
						psc.space_configuration_id
				   FROM
						property_space_configurations psc
						JOIN properties p ON p.cid = psc.cid AND p.id = psc.property_id
						LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . \CPsProduct::ENTRATA . ' )
						' . $strJoinSql . '
				   WHERE
						psc.space_configuration_id = ' . ( int ) $intSpaceConfigurationId . '
						AND psc.cid = ' . ( int ) $intCid . '
						AND ( ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.id IS NOT NULL ) OR p.property_type_id = ' . CPropertyType::MILITARY . ' )
						' . $strWhereCondition . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'p.property_name' );

		if( true == $boolIsReturnObjects ) {
			return self::fetchPropertySpaceConfigurations( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertySpaceConfigurationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strSql = 'select 
						sc.id,
						sc.name,
						psc.property_id,
						sc.unit_space_count
					FROM property_space_configurations psc
						JOIN space_configurations sc ON (psc.cid = sc.cid AND
						psc.space_configuration_id = sc.id)
					WHERE sc.id <> ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . ' 
						AND sc.cid = ' . ( int ) $intCid . ' 
						AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND sc.deleted_by IS NULL 
						AND sc.deleted_on IS NULL;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsByLeaseStartWindowIdByFloorPlanIdByPropertyIdByCid( $intLeaseStartWindowId, $intFloorPlanId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseStartWindowId ) || false == valId( $intFloorPlanId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return array();
		}

		$strSql = 'SELECT
						*
					FROM
					(
						SELECT
						    sc.id,
						    sc.name,
						    sc.unit_space_count,
						    psc.order_num,
						    SUM ( CASE
						            WHEN lsw.id = ' . ( int ) $intLeaseStartWindowId . ' AND us.property_floorplan_id = ' . ( int ) $intFloorPlanId . ' THEN 1
						            ELSE 0
						          END ) AS number_of_unit_spaces
						FROM
						    property_space_configurations psc
						    JOIN space_configurations sc ON ( sc.cid = psc.cid AND sc.id = psc.space_configuration_id )
						    JOIN unit_space_unions usu ON ( sc.cid = usu.cid AND sc.id = usu.space_configuration_id )
						    JOIN unit_spaces us ON ( usu.cid = us.cid AND usu.unit_space_id = us.id )
						    JOIN lease_start_windows lsw ON ( usu.cid = lsw.cid AND lsw.property_id = psc.property_id AND usu.lease_term_id = lsw.lease_term_id AND usu.lease_start_window_id = lsw.id )
						WHERE
						    sc.cid = ' . ( int ) $intCid . '
						    AND psc.property_id = ' . ( int ) $intPropertyId . '
						    AND sc.is_default = FALSE
						    AND us.unit_exclusion_reason_type_id = 1
						    AND us.deleted_on IS NULL
						    AND lsw.end_date >= CURRENT_DATE
						GROUP BY
						    psc.space_configuration_id,
						    sc.id,
						    sc.name,
						    sc.unit_space_count,
						    psc.order_num
						ORDER BY
							psc.order_num
					) sub
					WHERE
						sub.number_of_unit_spaces > 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strWhereSql = '	AS psc
							JOIN space_configurations sc ON ( sc.id = psc.space_configuration_id AND sc.cid = psc.cid )
						WHERE
							psc.cid = ' . ( int ) $intCid . '
							AND psc.property_id = ' . ( int ) $intPropertyId . '
							AND sc.id <> ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . '
							AND sc.deleted_by IS NULL
							AND sc.deleted_on IS NULL';
		return self::fetchPropertySpaceConfigurationCount( $strWhereSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationOrderNumByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}
		$strSql = 'SELECT 
						psc.property_id,
						max ( psc.order_num ) as order_num
					FROM 
						property_space_configurations psc
					WHERE 
						psc.cid = ' . ( int ) $intCid . ' 
						AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						psc.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsBySpaceConfigurationIdsByPropertyIdByCid( $arrintSpaceConfigurationIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpaceConfigurationIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						property_space_configurations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . ' 
						AND space_configuration_id IN ( ' . implode( ',', $arrintSpaceConfigurationIds ) . ' )';

		return self::fetchPropertySpaceConfigurations( $strSql, $objDatabase );
	}

}
?>