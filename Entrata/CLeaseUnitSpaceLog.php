<?php

class CLeaseUnitSpaceLog extends CBaseLeaseUnitSpaceLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectivePeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOriginalPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMoveInDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMoveOutDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStartsWithLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndsWithLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSlotNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPostMonthIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEffectiveDateIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }
}
?>