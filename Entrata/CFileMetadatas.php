<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMetadatas
 * Do not add any new functions to this class.
 */

class CFileMetadatas extends CBaseFileMetadatas {

	// we are not fetching "ESA" and "LEASE" document because they not supposed to visible on front end.
	public static $c_arrstrFileSystemCodeForUpdateFileMetadata = [
		'AAL', 'APP', 'ARTRANS', 'CRP', 'FMOS',
		'GC', 'LA', 'LD', 'LTNTC', 'MA', 'OEL',
		'OTHER', 'OTR', 'PCF', 'PCL', 'POE', 'POI',
		'POLICY', 'PRES', 'PS', 'QUOTE', 'REPORT',
		'ROA', 'ROS', 'RS', 'SIGNED', 'STC', 'UTS',
		'WAIT', 'WO'
	];

	public static function fetchFileTagsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						id,
						file_id,
						jsonb_object_keys(multival_data) as tag
					FROM
						file_metadatas fm
					WHERE
						fm.cid = ' . ( int ) $intCid . '
						AND fm.file_id = ' . ( int ) $intFileId . '';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomFileMetadataByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
					FROM
						file_metadatas
					WHERE
						file_id = ' . ( int ) $intFileId . '
						AND cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchFileMetadata( $strSql, $objDatabase );
	}

	public static function fetchCustomFileMetadatasByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return fetchData( sprintf( 'SELECT (jsonb_each(data)).key, (jsonb_each(data)).value FROM file_metadatas WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasForAugmentationByCid( $intCid, $objDatabase, $intRecordLimit = NULL ) {
		$strSql = 'SELECT
						fm.id,
						fm.cid,
						fm.entity_id,
						fm.entity_type_id,
						fm.entity_name,
						f.id as file_id,
						f.property_id,
						fm.data->>\'external_lease_id\' as external_lease_id,
						fm.data->>\'external_esignature_id\' as external_esignature_id,
						fm.data->>\'external_application_id\' as external_application_id,
						fm.data->>\'external_document_name\' as external_document_name,
						COALESCE( fm.data->>\'sync_attempt_count\', \'0\' ) as sync_attempt_count,
						fm.data
					FROM
						file_metadatas fm
						JOIN files f ON ( f.id = fm.file_id AND f.cid = fm.cid )
					WHERE
						fm.cid = ' . ( int ) $intCid . '
						AND CASE
							WHEN fm.data ? \'external_lease_id\' AND fm.data->>\'external_lease_id\' ~ \'[0-9]+$\' THEN TRUE
							WHEN fm.data ? \'external_esignature_id\' AND fm.data->>\'external_esignature_id\' ~ \'[0-9]+$\' THEN TRUE
							WHEN fm.data ? \'external_application_id\' AND fm.data->>\'external_application_id\' ~ \'[0-9]+$\' THEN TRUE
							ELSE FALSE
						END
						AND ( fm.data ->> \'force_remote_sync\' = \'1\' OR ( (fm.data->>\'remote_details_synced_on\') IS NULL AND f.file_name IS NOT NULL AND f.file_path IS NOT NULL ) )
						AND ( (fm.data->>\'sync_attempt_count\') IS NULL OR ( fm.data->>\'sync_attempt_count\' )::INTEGER < 3 )
						AND ( (fm.data->>\'next_sync_datetime\') IS NULL OR ( fm.data->>\'next_sync_datetime\' )::TIMESTAMP < NOW() )
						AND fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\'
					ORDER BY f.property_id, fm.id desc';

		if( true == valStr( $intRecordLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intRecordLimit;
		}

		return self::fetchFileMetadatas( $strSql, $objDatabase );
	}

	public static function fetchFileMetadatasByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						fm.*
					FROM
						file_metadatas fm
						JOIN files f ON ( f.id = fm.file_id AND f.cid = fm.cid )
					WHERE
						fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\'
						AND fm.data->>\'property_id\' = \'' . $intPropertyId . '\'
						AND fm.cid = ' . ( int ) $intCid;

		return self::fetchFileMetadatas( $strSql, $objDatabase );
	}

	public static function fetchUploadedFromDocStorageByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						file_id,
						data->>\'uploaded_from_doc_storage\'  as uploaded_from_doc_storage
					FROM
						file_metadatas
					WHERE
						file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql,  $objDatabase );
	}

	public static function fetchFileMetadatasByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						file_metadatas
					WHERE
						file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchFileMetadatas( $strSql, $objDatabase );
	}

	public static function fetchFileMetadatasDataByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						id,
						cid,
						file_id,
						property_id,
						data->\'external_source_system\',
						data
					FROM
						file_metadatas
					WHERE
						file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrmixFileMetadatas = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFileMetadatas ) ) {
			return rekeyArray( 'file_id', $arrmixFileMetadatas );
		}
	}

	public static function fetchFileMetadatasByDatakeysByFileIdByCid( $arrstrDatakeys, $intFileId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrDatakeys ) ) return NULL;

		$strCondition = '';

		foreach( $arrstrDatakeys as $intKey => $strDatakey ) {
			$strCondition .= ' fm.data->>\'' . $strDatakey . '\' as ' . $strDatakey;
			$strCondition .= ( ( \Psi\Libraries\UtilFunctions\count( $arrstrDatakeys ) - 1 ) != $intKey ) ? ', ' : '';
		}

		$strSql = ' SELECT
						' . $strCondition . '
					FROM
						file_metadatas fm
					WHERE
						fm.file_id = ' . ( int ) $intFileId . '
						AND fm.cid = ' . ( int ) $intCid;

		$arrmixDataRows = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixDataRows ) ) return NULL;

		return $arrmixDataRows[0];
	}

	public static function fetchCustomClientsForInsertFileMetadata( $objDatabase ) {
		$arrstrSqlEntityType = self::getEntityTypeToInsertFileMetadata();

		$strSqlEntityType = ' AND ( ' . implode( ' OR ', $arrstrSqlEntityType ) . ') ';

		$strSql = 'SELECT
						DISTINCT f.cid
					FROM files f
						 JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
						 JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
						 LEFT JOIN file_metadatas fm ON ( fa.file_id = fm.file_id AND fa.cid = fm.cid )
					WHERE
						  fa.deleted_on IS NULL
						  AND fm.id IS NULL
						  AND ( f.property_id IS NOT NULL OR fa.property_id IS NOT NULL )
						  AND ( ( ft.system_code IN ( \'' . implode( '\',\'', self::$c_arrstrFileSystemCodeForUpdateFileMetadata ) . '\' ) ) 
								OR ( ft.is_system = 0 AND ft.system_code IS NULL ) 
							)
						  AND ft.is_published = 1 
						  AND ( ' . implode( ' OR ', $arrstrSqlEntityType ) . ') ';

		$arrmixData = ( array ) fetchData( $strSql, $objDatabase );

		$arrintClientIds = [];
		foreach( $arrmixData as $strData ) {
			$arrintClientIds[$strData['cid']] = $strData['cid'];
		}

		return $arrintClientIds;

	}

	public static function fetchFileAssociationForInsertFileMetadataByCidByEntityTypeId( $intCid, $intEntityTypeId, $objDatabase, $boolReturnCount = false, $intRowFetchLimit = 500 ) {

		$strSqlEntityType 	= '';
		$strSqlLimit 		= ' LIMIT ' . ( int ) $intRowFetchLimit;

		switch( $intEntityTypeId ) {

			case ( CEntityType::LEAD ):
				$strSqlEntityType = ' AND ( applicant_id IS NOT NULL AND application_id IS NOT NULL )';
				break;

			case ( CEntityType::RESIDENT ):
				$strSqlEntityType = ' AND ( customer_id IS NOT NULL AND lease_id IS NOT NULL )';
				break;

			case ( CEntityType::EMPLOYEE ):
				$strSqlEntityType = ' AND ( company_employee_id IS NOT NULL AND lease_id IS NULL AND application_id IS NULL )';
				break;

			case ( CEntityType::VENDOR ):
				$strSqlEntityType = ' AND ( ap_payee_id IS NOT NULL AND lease_id IS NULL AND application_id IS NULL )';
				break;

			default:
				$arrstrSqlEntityType = self::getEntityTypeToInsertFileMetadata();

				$strSqlEntityType = ' AND ( ' . implode( ' OR ', $arrstrSqlEntityType ) . ') ';
				$strSqlLimit = '';
		}

		$arrstrFields = [
			'f.cid',
			'f.id as file_id',
			'f.title',
			'f.file_type_id',
			'fa.customer_id',
			'fa.lease_id',
			'fa.application_id',
			'fa.applicant_id',
			'fa.company_employee_id',
			'fa.ap_payee_id'
		];

		if( true == $boolReturnCount ) {
			$arrstrFields = [ 'count( DISTINCT f.id )' ];
		}

		$strSql = 'SELECT
						' . implode( ',', $arrstrFields ) . '
					FROM files f
					     JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid )
					     JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid )
					     LEFT JOIN file_metadatas fm ON ( fa.file_id = fm.file_id AND fa.cid = fm.cid )
					WHERE
						  f.cid = ' . ( int ) $intCid . '
						  AND fa.deleted_on IS NULL
						  AND fm.id IS NULL
						  AND ( f.property_id IS NOT NULL OR fa.property_id IS NOT NULL )
					      AND ( ( ft.system_code IN ( \'' . implode( '\',\'', self::$c_arrstrFileSystemCodeForUpdateFileMetadata ) . '\' ) ) OR ( ft.is_system = 0 AND ft.system_code IS NULL ) )
					      AND ft.is_published = 1
					      ' . $strSqlEntityType . ' ' . $strSqlLimit;

		if( true == $boolReturnCount ) {
			$arrmixData = fetchData( $strSql, $objDatabase );
    		return ( false == is_null( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : NULL;
		}

		return $strSql;
	}

	public static function fetchLeaseCustomerToInsertFileMetadata( $intCid, $intRowFetchLimit, $objDatabase ) {
		$strSubSql = self::fetchFileAssociationForInsertFileMetadataByCidByEntityTypeId( $intCid, CEntityType::RESIDENT, $objDatabase, $boolReturnCount = false, $intRowFetchLimit = 500 );

		$strSql = 'SELECT
						fa.cid,
						fa.file_id,
						fa.title as document_name,
						fa.file_type_id as document_type_id,
						cl.id as entity_id,
						' . CEntityType::RESIDENT . ' as entity_type_id,
						CONCAT( c.name_first, \' \', c.name_last ) as entity_name,
						lc.lease_id,
						array_agg( c2.id order by c2.id ) AS array_customer_id,
						array_agg( c2.name_first order by c2.id ) AS array_person_first_name,
						array_agg( c2.name_last order by c2.id ) AS array_person_last_name,
						c.email_address as primary_email_address,
						cl.building_name,
						cl.is_multi_slot,
						cl.lease_end_date,
						cl.lease_interval_type,
						cl.lease_interval_type_id,
						cl.lease_start_date,
						cl.lease_status_type,
						cl.lease_status_type_id,
						cl.move_in_date,
						cl.move_out_date,
						cl.notice_date,
						cl.primary_customer_id,
						cl.primary_phone_number,
						cl.property_name,
						cl.property_unit_id,
						cl.termination_list_type_id,
						cl.unit_number_cache,
						cl.unit_space_id
					FROM cached_leases cl
						JOIN lease_customers lc ON ( lc.customer_id = cl.primary_customer_id AND lc.cid = cl.cid and cl.id = lc.lease_id )
						JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
						JOIN ( ' . $strSubSql . ') as fa ON ( fa.cid = lc.cid AND fa.lease_id = lc.lease_id )
						LEFT JOIN customers c2 ON ( c2.cid = fa.cid AND c2.id = fa.customer_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
					GROUP BY
						fa.cid,
						fa.file_id,
						document_name,
						document_type_id,
						cl.id,
						entity_name,
						lc.lease_id,
						c.id,
						primary_email_address,
						cl.building_name,
						cl.is_multi_slot,
						cl.lease_end_date,
						cl.lease_interval_type,
						cl.lease_interval_type_id,
						cl.lease_start_date,
						cl.lease_status_type,
						cl.lease_status_type_id,
						cl.move_in_date,
						cl.move_out_date,
						cl.notice_date,
						cl.primary_customer_id,
						cl.primary_phone_number,
						cl.property_name,
						cl.property_unit_id,
						cl.termination_list_type_id,
						cl.unit_number_cache,
						cl.unit_space_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationsToInsertFileMetadata( $intCid, $intRowFetchLimit, $objDatabase ) {

		$strSubSql = self::fetchFileAssociationForInsertFileMetadataByCidByEntityTypeId( $intCid, CEntityType::LEAD, $objDatabase, false, $intRowFetchLimit );

		$strSql = 'SELECT
						fa.cid,
						fa.file_id,
						fa.title as document_name,
						fa.file_type_id as document_type_id,
						fa.application_id as application_id,
						ca.id as entity_id,
						' . CEntityType::LEAD . ' as entity_type_id,
						CONCAT( a.name_first, \' \', a.name_last ) as entity_name,
						array_agg(a2.id order by a2.id ) as array_applicant_id,
						array_agg(a2.name_first order by a2.id ) as array_person_first_name,
						array_agg(a2.name_last order by a2.id ) as array_person_last_name,
						a.email_address as primary_email_address,
						aa.lease_signed_on,
						aa.lease_status_type_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.building_name,
						ca.lease_end_date,
						ca.lease_id,
						ca.lease_interval_id,
						ca.lease_interval_type_id,
						ca.lease_start_date,
						ca.primary_applicant_id,
						ca.primary_phone_number,
						ca.property_name,
						ca.property_unit_id
					FROM cached_applications ca
						JOIN applicant_applications aa on ( aa.applicant_id = ca.primary_applicant_id and aa.cid = ca.cid and ca.id = aa.application_id )
						JOIN applicants a on ( a.cid = aa.cid and a.id = aa.applicant_id )
						JOIN ( ' . $strSubSql . ') as fa ON ( fa.cid = aa.cid AND fa.application_id = aa.application_id )
						LEFT JOIN applicants a2 on ( a2.cid = fa.cid and a2.id = fa.applicant_id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
					GROUP BY
						  fa.cid,
						  fa.file_id,
						  fa.title,
						  fa.file_type_id,
						  fa.application_id,
						  ca.id,
						  entity_name,
						  a.email_address,
						  aa.lease_signed_on,
						  aa.lease_status_type_id,
						  ca.application_stage_id,
						  ca.application_status_id,
						  ca.building_name,
						  ca.lease_end_date,
						  ca.lease_id,
						  ca.lease_interval_id,
						  ca.lease_interval_type_id,
						  ca.lease_start_date,
						  ca.primary_applicant_id,
						  ca.primary_phone_number,
						  ca.property_name,
						  ca.property_unit_id
					';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesToInsertFileMetadata( $intCid,  $intRowFetchLimit, $objDatabase ) {

		$strSubSql = self::fetchFileAssociationForInsertFileMetadataByCidByEntityTypeId( $intCid, CEntityType::EMPLOYEE, $objDatabase, false, $intRowFetchLimit );

		$strSql = 'SELECT
						fa.cid,
						fa.file_id,
						fa.title as document_name,
						fa.file_type_id as document_type_id,
						ce.id as entity_id,
						' . CEntityType::EMPLOYEE . ' as entity_type_id,
						CONCAT( ce.name_first, \' \', ce.name_last ) as entity_name,
						ce.name_first as person_first_name1,
						ce.name_last as person_last_name1
					FROM company_employees ce
						JOIN ( ' . $strSubSql . ') as fa ON ( fa.cid = ce.cid AND fa.company_employee_id = ce.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeesToInsertFileMetadata( $intCid,  $intRowFetchLimit, $objDatabase ) {
		$strSubSql = self::fetchFileAssociationForInsertFileMetadataByCidByEntityTypeId( $intCid, CEntityType::VENDOR, $objDatabase, false, $intRowFetchLimit );

		$strSql = 'SELECT
						fa.cid,
						fa.file_id,
						fa.title as document_name,
						fa.file_type_id as document_type_id,
						ap.id as entity_id,
						' . CEntityType::VENDOR . ' as entity_type_id,
						ap.company_name as entity_name,
						ap.company_name as person_first_name1
					FROM ap_payees ap
						JOIN ( ' . $strSubSql . ') as fa ON ( fa.cid = ap.cid AND fa.ap_payee_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountApplicantApplicationsToUpdateFileMetadata( $intCid, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;

		$strSql = 'SELECT COUNT( DISTINCT f.id ) as count
					FROM file_metadatas fm
					     JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					     JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					     JOIN cached_applications ca ON ( ca.id = fa.application_id and ca.cid = fa.cid )
					     JOIN applicant_applications aa ON ( ca.primary_applicant_id = aa.applicant_id and ca.id = aa.application_id and ca.cid = aa.cid )
					     JOIN applicants a ON ( a.id = aa.applicant_id and a.cid = aa.cid )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.applicant_id IS NOT NULL AND fa.application_id IS NOT NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ca.updated_on, aa.updated_on, a.updated_on )
					';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == is_null( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : NULL;
	}

	public static function fetchApplicantApplicationsToUpdateFileMetadata( $intCid, $intLimit, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;
		if( false == valStr( $intLimit ) ) 	return NULL;

		$strSql = 'SELECT
						fm.id,
						fa.cid,
						fa.file_id,
						array_agg(a2.id order by a2.id ) as array_applicant_id,
						array_agg(a2.name_first order by a2.id ) as array_person_first_name,
						array_agg(a2.name_last order by a2.id ) as array_person_last_name,
						fa.application_id as application_id,
						f.title as document_name,
						f.file_type_id as document_type_id,
						ca.id as entity_id,
						' . CEntityType::LEAD . ' as entity_type_id,
						CONCAT( a.name_first, \' \', a.name_last ) as entity_name,
						a.email_address as primary_email_address,
						aa.lease_signed_on,
						aa.lease_status_type_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.building_name,
						ca.lease_end_date,
						ca.lease_id,
						ca.lease_interval_id,
						ca.lease_interval_type_id,
						ca.lease_start_date,
						ca.primary_applicant_id,
						ca.primary_phone_number,
						ca.property_name,
						ca.property_unit_id
					FROM file_metadatas fm
						JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
						JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
						JOIN cached_applications ca ON ( ca.id = fa.application_id and ca.cid = fa.cid )
						JOIN applicant_applications aa ON ( ca.primary_applicant_id = aa.applicant_id and ca.id = aa.application_id and ca.cid = aa.cid )
						JOIN applicants a ON ( a.id = aa.applicant_id and a.cid = aa.cid )
						LEFT JOIN applicants a2 on ( a2.cid = fa.cid and a2.id = fa.applicant_id )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.applicant_id IS NOT NULL AND fa.application_id IS NOT NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ca.updated_on, aa.updated_on, a.updated_on )
					GROUP BY
						fm.id,
						fa.cid,
						fa.file_id,
						f.title,
						f.file_type_id,
						fa.application_id,
						ca.id,
						a.name_first,
						a.name_last,
						a.email_address,
						aa.lease_signed_on,
						aa.lease_status_type_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.building_name,
						ca.lease_end_date,
						ca.lease_id,
						ca.lease_interval_id,
						ca.lease_interval_type_id,
						ca.lease_start_date,
						ca.primary_applicant_id,
						ca.primary_phone_number,
						ca.property_name,
						ca.property_unit_id
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountLeaseCustomersToUpdateFileMetadata( $intCid, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;

		$strSql = 'SELECT COUNT( DISTINCT f.id  ) as count
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN lease_customers lc on ( lc.cid = fa.cid AND lc.customer_id = fa.customer_id AND lc.lease_id = fa.lease_id )
					    JOIN cached_leases cl on ( cl.cid = fa.cid and cl.id = fa.lease_id )
						JOIN customers c ON ( c.id = lc.customer_id and c.cid = lc.cid )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.customer_id IS NOT NULL AND fa.lease_id IS NOT NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, cl.updated_on, lc.updated_on, c.updated_on )';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == is_null( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : NULL;
	}

	public static function fetchLeaseCustomersToUpdateFileMetadata( $intCid, $intLimit, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;
		if( false == valStr( $intLimit ) ) 	return NULL;

		$strSql = 'SELECT
						fm.id,
						fa.cid,
						fa.file_id,
						f.title as document_name,
						f.file_type_id as document_type_id,
						cl.id as entity_id,
						' . CEntityType::RESIDENT . ' as entity_type_id,
						CONCAT( c.name_first, \' \', c.name_last ) as entity_name,
						lc.lease_id,
						array_agg( c2.id order by c2.id ) AS array_customer_id,
						array_agg( c2.name_first order by c2.id ) AS array_person_first_name,
						array_agg( c2.name_last order by c2.id ) AS array_person_last_name,
						c.email_address as primary_email_address,
						cl.building_name,
						cl.is_multi_slot,
						cl.lease_end_date,
						cl.lease_interval_type,
						cl.lease_interval_type_id,
						cl.lease_start_date,
						cl.lease_status_type,
						cl.lease_status_type_id,
						cl.move_in_date,
						cl.move_out_date,
						cl.notice_date,
						cl.primary_customer_id,
						cl.primary_phone_number,
						cl.property_name,
						cl.property_unit_id,
						cl.termination_list_type_id,
						cl.unit_number_cache,
						cl.unit_space_id
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN lease_customers lc on ( lc.cid = fa.cid AND lc.customer_id = fa.customer_id AND lc.lease_id = fa.lease_id )
					    JOIN cached_leases cl on ( cl.cid = fa.cid and cl.id = fa.lease_id )
						JOIN customers c ON ( c.id = lc.customer_id and c.cid = lc.cid )
						LEFT JOIN customers c2 ON ( c2.id = lc.customer_id and c2.cid = lc.cid )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.customer_id IS NOT NULL AND fa.lease_id IS NOT NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, cl.updated_on, lc.updated_on, c.updated_on )
					GROUP BY
						fm.id,
						fa.cid,
						fa.file_id,
						document_name,
						document_type_id,
						entity_id,
						c.name_first,
						c.name_last,
						lc.lease_id,
						cl.id,
						primary_email_address,
						cl.building_name,
						cl.is_multi_slot,
						cl.lease_end_date,
						cl.lease_interval_type,
						cl.lease_interval_type_id,
						cl.lease_start_date,
						cl.lease_status_type,
						cl.lease_status_type_id,
						cl.move_in_date,
						cl.move_out_date,
						cl.notice_date,
						cl.primary_customer_id,
						cl.primary_phone_number,
						cl.property_name,
						cl.property_unit_id,
						cl.termination_list_type_id,
						cl.unit_number_cache,
						cl.unit_space_id
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountCompanyEmployeesToUpdateFileMetadata( $intCid, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;

		$strSql = 'SELECT COUNT( DISTINCT f.id ) as count
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN company_employees ce on ( ce.cid = fa.cid AND fa.company_employee_id = ce.id )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.company_employee_id IS NOT NULL )
					    AND ( fa.customer_id IS NULL AND fa.lease_id IS NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ce.updated_on )';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == is_null( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : NULL;
	}

	public static function fetchCompanyEmployeesToUpdateFileMetadata( $intCid, $intLimit, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;
		if( false == valStr( $intLimit ) ) 	return NULL;

		$strSql = 'SELECT
						fm.id,
						fa.cid,
					    fa.file_id,
					    f.title as document_name,
					    f.file_type_id as document_type_id,
					    ce.id as entity_id,
					    ' . CEntityType::EMPLOYEE . ' as entity_type_id,
					    CONCAT( ce.name_first, \' \', ce.name_last ) as entity_name,
					    ce.name_first as person_first_name1,
					    ce.name_last as person_last_name1
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN company_employees ce on ( ce.cid = fa.cid AND fa.company_employee_id = ce.id )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.company_employee_id IS NOT NULL )
					    AND ( fa.customer_id IS NULL AND fa.lease_id IS NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ce.updated_on )
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountApPayeesToUpdateFileMetadata( $intCid, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;

		$strSql = 'SELECT COUNT( DISTINCT f.id ) as count
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN ap_payees ap on ( ap.cid = fa.cid AND fa.ap_payee_id = ap.id )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.ap_payee_id IS NOT NULL )
					    AND ( fa.customer_id IS NULL AND fa.lease_id IS NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ap.updated_on )';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == is_null( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : NULL;
	}

	public static function fetchApPayeesToUpdateFileMetadata( $intCid, $intLimit, $objDatabase ) {

		if( false == valStr( $intCid ) ) 	return NULL;
		if( false == valStr( $intLimit ) ) 	return NULL;

		$strSql = 'SELECT
						fm.id,
						fa.cid,
					    fa.file_id,
					    f.title as document_name,
					    f.file_type_id as document_type_id,
					    ap.id as entity_id,
					    ' . CEntityType::VENDOR . ' as entity_type_id,
					    ap.company_name as entity_name,
						ap.company_name as person_first_name1
					FROM file_metadatas fm
					    JOIN files f ON ( fm.file_id = f.id AND fm.cid = f.cid  )
					    JOIN file_associations fa ON ( fa.file_id = f.id AND fa.cid = f.cid )
					    JOIN ap_payees ap on ( ap.cid = fa.cid AND fa.ap_payee_id = ap.id )
					WHERE fm.cid = ' . ( int ) $intCid . '
						AND f.property_id IS NOT NULL
					    AND ( fa.ap_payee_id IS NOT NULL )
					    AND ( fa.customer_id IS NULL AND fa.lease_id IS NULL )
					    AND ( fa.application_id IS NULL AND fa.applicant_id IS NULL )
					    AND fm.updated_on < GREATEST( f.updated_on, fa.updated_on, ap.updated_on )
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomFileMetadatasForJobCostingByFileTypeIdByCid( $strFileTypeSystemCode, $intCid, $objDatabase, $intJobId = NULL, $intApContractId = NULL, $intDrawRequestId = NULL ) {
		if( ( false == valId( $intJobId ) && false == valId( $intApContractId ) ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT fm.file_id
					FROM
						file_metadatas fm
						JOIN files f ON ( f.id = fm.file_id AND f.cid = fm.cid )
						JOIN file_types ft ON( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'' . addslashes( $strFileTypeSystemCode ) . '\')
						JOIN clients c ON ( fm.cid = c.id )
					WHERE
						fm.cid = ' . ( int ) $intCid;

		if( true == valId( $intJobId ) ) {
			$strSql .= ' AND fm.data->>\'job_id\' = \'' . ( int ) $intJobId . '\'';
			if( true == valId( $intDrawRequestId ) ) {
				$strSql .= ' AND fm.data->>\'draw_request_id\' = \'' . ( int ) $intDrawRequestId . '\'';
			}
		} elseif( true == valId( $intApContractId ) ) {
			$strSql .= ' AND fm.data->>\'ap_contract_id\' = \'' . ( int ) $intApContractId . '\'';
		}

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * @param $arrstrSqlEntityType
	 * @return array
	 */
	protected static function getEntityTypeToInsertFileMetadata() {
		$arrstrSqlEntityType = [];

		$arrstrSqlEntityType[] = ' ( applicant_id IS NOT NULL AND application_id IS NOT NULL )';
		$arrstrSqlEntityType[] = ' ( customer_id IS NOT NULL AND lease_id IS NOT NULL )';
		$arrstrSqlEntityType[] = ' ( company_employee_id IS NOT NULL AND lease_id IS NULL AND application_id IS NULL )';
		$arrstrSqlEntityType[] = ' ( ap_payee_id IS NOT NULL AND lease_id IS NULL AND application_id IS NULL )';

		return $arrstrSqlEntityType;
	}

}
?>