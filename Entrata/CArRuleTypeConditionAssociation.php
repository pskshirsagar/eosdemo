<?php

class CArRuleTypeConditionAssociation extends CBaseArRuleTypeConditionAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArRuleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArRuleConditionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>