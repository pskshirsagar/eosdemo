<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUtilities
 * Do not add any new functions to this class.
 */

class CUtilities extends CBaseUtilities {

	public static function fetchUtilitiesWithIsResponsibleByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $objApplication = NULL ) {

		$strWhereCond = ' ';
		if( true == valObj( $objApplication, 'CApplication' ) ) {
			$strFloorPlanCond = $strUnitTypeCond = $strUnitSpaceCond = '';

			$strWhereCond = ' ( pu.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND pu.ar_cascade_reference_id = ' . ( int ) $intPropertyId . ' )';

			if( true == valId( $objApplication->getPropertyFloorPlanId() ) ) {
				$strFloorPlanCond = '( pu.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND pu.ar_cascade_reference_id = ' . $objApplication->getPropertyFloorPlanId() . ' )';
			}

			if( true == valId( $objApplication->getUnitTypeId() ) ) {
				$strUnitTypeCond = '( pu.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND pu.ar_cascade_reference_id = ' . $objApplication->getUnitTypeId() . ' )';
			} elseif( true == valStr( $strFloorPlanCond ) ) {
				$strUnitTypeCond = '( pu.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND pu.ar_cascade_reference_id IN ( SELECT id FROM unit_types WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND property_floorplan_id = ' . $objApplication->getPropertyFloorPlanId() . ' ) )';
			}

			if( true == valId( $objApplication->getUnitSpaceId() ) ) {
				$strUnitSpaceCond = '( pu.ar_cascade_id = ' . CArCascade::SPACE . ' AND pu.ar_cascade_reference_id = ' . ( int ) $objApplication->getUnitSpaceId() . ' )';
			}

			if( true == valStr( $strFloorPlanCond ) || true == valStr( $strUnitTypeCond ) || true == valStr( $strUnitSpaceCond ) ) {
				$strWhereCond .= ( true == valStr( $strFloorPlanCond ) ) ? ' OR ' . $strFloorPlanCond : $strFloorPlanCond;
				$strWhereCond .= ( true == valStr( $strUnitTypeCond ) ) ? ' OR ' . $strUnitTypeCond : $strUnitTypeCond;
				$strWhereCond .= ( true == valStr( $strUnitSpaceCond ) ) ? ' OR ' . $strUnitSpaceCond : $strUnitSpaceCond;
			}

			$strWhereCond  = ' AND ( ' . $strWhereCond . ' ) ';
		}

		$strSql = 'SELECT
					    DISTINCT u.id,
					    COALESCE ( pu.responsibility_description, u.name ) AS NAME
					FROM
					    utilities u
					    JOIN property_utilities pu ON ( pu.cid = u.cid AND pu.utility_id = u.id )
					WHERE 
						pu.cid = ' . ( int ) $intCid . '
						AND pu.property_id = ' . ( int ) $intPropertyId . '
						AND pu.is_responsible = true
						AND pu.show_responsibility_on_portal = true
						AND u.utility_type_id <> ' . CUtilityType::LEASING . ' 
						AND u.is_published = true ' . $strWhereCond;

		return self::fetchUtilities( $strSql, $objDatabase );
	}

	public static function fetchUtilitiesByCid( $intCid, $objDatabase, $boolSkipCustomUtilities = false ) {

		$strSql = 'SELECT * FROM utilities WHERE cid = ' . ( int ) $intCid . ' AND is_published = true';

		if( true == $boolSkipCustomUtilities ) {
			$strSql .= ' AND utility_type_id <> ' . CUtilityType::LEASING;
		}

		$strSql .= ' ORDER BY id';

		return self::fetchUtilities( $strSql, $objDatabase );
	}

	public static function fetchAllUtilitiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM utilities WHERE cid = ' . ( int ) $intCid . ' ORDER BY id';

		return self::fetchUtilities( $strSql, $objDatabase );
	}

	public static function fetchUtilitiesByIdsByCids( $arrintUtilityIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintUtilityIds ) || false == valArr( $arrintCids ) ) return;

		$strSql = 'SELECT
						*
					FROM
						utilities
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND id IN ( ' . implode( ',', $arrintUtilityIds ) . ' )';

		return self::fetchUtilities( $strSql, $objClientDatabase );
	}

}
?>