<?php

class CPeriod extends CBasePeriod {

	protected $m_boolIsPostAr;
	protected $m_boolIsPostToCurrentApPeriod;

	protected $m_intApHeaderId;
	protected $m_intApDetailId;
	protected $m_intFeeTemplateId;
	protected $m_intManagementFeeId;
	protected $m_intIsDisabledProperty;
	protected $m_intFeeTemplatePropertyGroupId;
	protected $m_intIncludeSecurityDeposits;
	protected $m_intIncludePaymentsInTransit;
	protected $m_intFeeCalculationTypeId;

	protected $m_fltManagementFeeAmount;

	protected $m_strErrors;
	protected $m_strPropertyName;
	protected $m_strArPostMonth;
	protected $m_strApPostMonth;
	protected $m_strGlPostMonth;
	protected $m_strInvoiceDate;
	protected $m_strFeeTemplateName;
	protected $m_strInvoicePostMonth;
	protected $m_strIntercompanyArEntity;
	protected $m_strManagementFeeStatus;

	protected $m_objFee;

	const CACHE_REBUILD_TYPE_DIFFERENTIAL			= 1;
	const CACHE_REBUILD_TYPE_AS_OF_DATE 			= 2;
	const CACHE_REBUILD_TYPE_DATE_RANGE 			= 3;
	const CACHE_REBUILD_TYPE_OPEN_PERIODS 			= 4;
	const CACHE_REBUILD_TYPE_TOTAL_REBUILD 			= 5;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getFeeTemplateName() {
		return $this->m_strFeeTemplateName;
	}

	public function getManagementFeeStatus() {
		return $this->m_strManagementFeeStatus;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getFeeTemplateId() {
		return $this->m_intFeeTemplateId;
	}

	public function getManagementFeeId() {
		return $this->m_intManagementFeeId;
	}

	public function getIsDisabledProperty() {
		return $this->m_intIsDisabledProperty;
	}

	public function getManagementFeeAmount() {
		return $this->m_fltManagementFeeAmount;
	}

	public function getArPostMonth() {
		return $this->m_strArPostMonth;
	}

	public function getApPostMonth() {
		return $this->m_strApPostMonth;
	}

	public function getGlPostMonth() {
		return $this->m_strGlPostMonth;
	}

	public function getStatus( $strLedger ) {

		if( false == in_array( $strLedger, [ 'Ar', 'Ap', 'Gl' ] ) ) {
			return;
		}
		$strGetLockedOn		= 'get' . $strLedger . 'LockedOn';
		$strGetPostMonth	= 'get' . $strLedger . 'PostMonth';

		if( false == is_null( $this->$strGetLockedOn() ) ) {
			return __( 'Locked {%t, 0, DATE_NUMERIC_STANDARD}', [ $this->$strGetLockedOn() ] );
		} elseif( strtotime( $this->getPostMonth() ) < strtotime( $this->$strGetPostMonth() ) ) {
			return __( 'Open' );
		} elseif( strtotime( $this->getPostMonth() ) == strtotime( $this->$strGetPostMonth() ) ) {
			return __( 'Current' );
		} else {
			return '-';
		}
	}

	public function getFormattedPostMonth() {
		return ( new Datetime( parent::getPostMonth() ) )->format( 'm/Y' );
	}

	public function getIntercompanyArEntity() {
		return $this->m_strIntercompanyArEntity;
	}

	public function getInvoicePostMonth() {
		return $this->m_strInvoicePostMonth;
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function getIncludePaymentsInTransit() {
		return $this->m_intIncludePaymentsInTransit;
	}

	public function getIncludeSecurityDeposits() {
		return $this->m_intIncludeSecurityDeposits;
	}

	public function getIsPostAr() {
		return $this->m_boolIsPostAr;
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function getIsPostToCurrentApPeriod() {
		return $this->m_boolIsPostToCurrentApPeriod;
	}

	public function getFeeTemplatePropertyGroupId() {
		return $this->m_intFeeTemplatePropertyGroupId;
	}

	public function getFeeCalculationTypeId() {
		return $this->m_intFeeCalculationTypeId;
	}

	public function getFee() {
		return $this->m_objFee;
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setFeeTemplateName( $strFeeTemplateName ) {
		$this->m_strFeeTemplateName = $strFeeTemplateName;
	}

	public function setManagementFeeStatus( $strManagementFeeStatus ) {
		$this->m_strManagementFeeStatus = $strManagementFeeStatus;
	}

	public function setPostMonth( $strPostMonth ) {

		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = CStrings::strToIntDef( $intApHeaderId, NULL, false );
	}

	public function setFeeTemplateId( $intFeeTemplateId ) {
		$this->m_intFeeTemplateId = CStrings::strToIntDef( $intFeeTemplateId, NULL, false );
	}

	public function setManagementFeeId( $intManagementFeeId ) {
		$this->m_intManagementFeeId = CStrings::strToIntDef( $intManagementFeeId, NULL, false );
	}

	public function setIsDisabledProperty( $intIsDisabledProperty ) {
		$this->m_intIsDisabledProperty = CStrings::strToIntDef( $intIsDisabledProperty, NULL, false );
	}

	public function setManagementFeeAmount( $fltManagementFeeAmount ) {
		$this->m_fltManagementFeeAmount = CStrings::strToFloatDef( $fltManagementFeeAmount, NULL, false, 2 );
	}

	public function setArPostMonth( $strArPostMonth ) {
		$this->m_strArPostMonth = CStrings::strTrimDef( $strArPostMonth, -1, NULL, true );
	}

	public function setApPostMonth( $strApPostMonth ) {
		$this->m_strApPostMonth = CStrings::strTrimDef( $strApPostMonth, -1, NULL, true );
	}

	public function setGlPostMonth( $strGlPostMonth ) {
		$this->m_strGlPostMonth = CStrings::strTrimDef( $strGlPostMonth, -1, NULL, true );
	}

	public function setIntercompanyArEntity( $strIntercompanyArEntity ) {
		$this->m_strIntercompanyArEntity = $strIntercompanyArEntity;
	}

	public function setInvoicePostMonth( $strInvoicePostMonth ) {
		$this->m_strInvoicePostMonth = $strInvoicePostMonth;
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->m_strInvoiceDate = $strInvoiceDate;
	}

	public function setIncludePaymentsInTransit( $intIncludePaymentsInTransit ) {
		$this->m_intIncludePaymentsInTransit = $intIncludePaymentsInTransit;
	}

	public function setIncludeSecurityDeposits( $intIncludeSecurityDeposits ) {
		$this->m_intIncludeSecurityDeposits = $intIncludeSecurityDeposits;
	}

	public function setIsPostAr( $boolIsPostAr ) {
		$this->m_boolIsPostAr = CStrings::strToBool( $boolIsPostAr );
	}

	public function setApDetailId( $intApDetailId ) {
		$this->m_intApDetailId = CStrings::strToIntDef( $intApDetailId, NULL, false );
	}

	public function setIsPostToCurrentApPeriod( $boolIsPostToCurrentApPeriod ) {
		$this->m_boolIsPostToCurrentApPeriod = CStrings::strToBool( $boolIsPostToCurrentApPeriod );
	}

	public function setFeeTemplatePropertyGroupId( $intFeeTemplatePropertyGroupId ) {
		$this->m_intFeeTemplatePropertyGroupId = $intFeeTemplatePropertyGroupId;
	}

	public function setFeeCalculationTypeId( $intFeeCalculationTypeId ) {
		$this->m_intFeeCalculationTypeId = $intFeeCalculationTypeId;
	}

	public function setFee( $objFee ) {
		$this->m_objFee = $objFee;
	}

	public function setFeeByDetail( $strDetails ) {
		if( true == valStr( $strDetails ) && true == valId( $this->getFeeCalculationTypeId() ) ) {
			$objFee = new CFee();
			$objFee->setId( $this->getManagementFeeId() );
			$objFee->setFeeCalculationTypeId( $this->getFeeCalculationTypeId() );
			$objFee->setDetails( $strDetails );
			$objFee->setErrors( $this->getErrors() );
			$objFee->setValues( [] );
			$this->m_objFee = $objFee;
		}
	}

	public function setErrors( $strErrors ) {
		$this->set( 'm_strErrors', CStrings::strTrimDef( $strErrors, -1, NULL, true ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) )                   $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['fee_template_name'] ) )               $this->setFeeTemplateName( $arrmixValues['fee_template_name'] );
		if( true == isset( $arrmixValues['management_fee_id'] ) )               $this->setManagementFeeId( $arrmixValues['management_fee_id'] );
		if( true == isset( $arrmixValues['is_disabled_property'] ) )            $this->setIsDisabledProperty( $arrmixValues['is_disabled_property'] );
		if( true == isset( $arrmixValues['management_fee_amount'] ) )           $this->setManagementFeeAmount( $arrmixValues['management_fee_amount'] );
		if( true == isset( $arrmixValues['ap_header_id'] ) )                    $this->setApHeaderId( $arrmixValues['ap_header_id'] );
		if( true == isset( $arrmixValues['fee_template_id'] ) )                 $this->setFeeTemplateId( $arrmixValues['fee_template_id'] );
		if( true == isset( $arrmixValues['management_fee_status'] ) )           $this->setManagementFeeStatus( $arrmixValues['management_fee_status'] );
		if( true == isset( $arrmixValues['ar_post_month'] ) )                   $this->setArPostMonth( $arrmixValues['ar_post_month'] );
		if( true == isset( $arrmixValues['ap_post_month'] ) )                   $this->setApPostMonth( $arrmixValues['ap_post_month'] );
		if( true == isset( $arrmixValues['gl_post_month'] ) )                   $this->setGlPostMonth( $arrmixValues['gl_post_month'] );
		if( true == isset( $arrmixValues['intercompany_ar_entity'] ) )          $this->setIntercompanyArEntity( $arrmixValues['intercompany_ar_entity'] );
		if( true == isset( $arrmixValues['invoice_post_month'] ) )              $this->setInvoicePostMonth( $arrmixValues['invoice_post_month'] );
		if( true == isset( $arrmixValues['invoice_date'] ) )                    $this->setInvoiceDate( $arrmixValues['invoice_date'] );
		if( true == isset( $arrmixValues['include_payments_in_transit'] ) )     $this->setIncludePaymentsInTransit( $arrmixValues['include_payments_in_transit'] );
		if( true == isset( $arrmixValues['include_security_deposits'] ) )       $this->setIncludeSecurityDeposits( $arrmixValues['include_security_deposits'] );
		if( true == isset( $arrmixValues['is_post_ar'] ) )                      $this->setIsPostAr( $arrmixValues['is_post_ar'] );
		if( true == isset( $arrmixValues['ap_detail_id'] ) )                    $this->setApDetailId( $arrmixValues['ap_detail_id'] );
		if( true == isset( $arrmixValues['is_post_to_current_ap_period'] ) )    $this->setIsPostToCurrentApPeriod( $arrmixValues['is_post_to_current_ap_period'] );
		if( true == isset( $arrmixValues['fee_template_property_group_id'] ) )  $this->setFeeTemplatePropertyGroupId( $arrmixValues['fee_template_property_group_id'] );
		if( true == isset( $arrmixValues['fee_calculation_type_id'] ) )         $this->setFeeCalculationTypeId( $arrmixValues['fee_calculation_type_id'] );
		if( true == isset( $arrmixValues['errors'] ) )                          $this->setErrors( $arrmixValues['errors'] );
		if( true == isset( $arrmixValues['details'] ) )                         $this->setFeeByDetail( $arrmixValues['details'] );
		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlStartDate( $objPeriodClosingsFilter ) {

		$boolIsValid = true;

		if( ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getGlEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getGlPostMonth() ) )
			|| ( 0 == $objPeriodClosingsFilter->getGeneralLedger() && 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getArEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getArPostMonth() ) )
			|| ( 0 == $objPeriodClosingsFilter->getGeneralLedger() && 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getApEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getApPostMonth() ) )
			|| ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable()
				&& true == CValidation::valDate( $this->getGlEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getGlPostMonth() )
				&& true == CValidation::valDate( $this->getArEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getArPostMonth() )
				&& true == CValidation::valDate( $this->getApEndDate() ) && strtotime( $this->getPostMonth() ) < strtotime( $this->getApPostMonth() ) ) ) {

			if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
				$arrstrCategories[] = 'AR';
			}

			if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
				$arrstrCategories[] = 'AP';
			}

			if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
				$arrstrCategories[] = 'GL';
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, categories} is already advanced for {%s, property_name}.', [ 'categories' => implode( ' and ', $arrstrCategories ), 'property_name' => $this->getPropertyName() ] ) ) );

		} elseif( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getPostMonth() ) > strtotime( $this->getArPostMonth() ) ) {
			// Actual date or string 'NOW()' are valid inputs.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'AR and AP must be advanced for {%s, 0}.', [ $this->getPropertyName() ] ) ) );

		} elseif( 1 == $objPeriodClosingsFilter->getGeneralLedger()
					&& ( ( true == CValidation::validateDate( $this->getArStartDate() ) && strtotime( $this->getGlStartDate() ) < strtotime( $this->getArStartDate() ) )
							|| ( true == CValidation::validateDate( $this->getApStartDate() ) && strtotime( $this->getGlStartDate() ) < strtotime( $this->getApStartDate() ) ) ) ) {

			$boolIsValid = false;
			$strMaxDate = strtotime( $this->getArStartDate() ) > strtotime( $this->getApStartDate() ) ? $this->getArStartDate() : $this->getApStartDate();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL end date should not be less than AR or AP end date {%t, max_date, DATE_NUMERIC_STANDARD} for {%s, property_name}.', [ 'max_date' => strtotime( '-1 days', strtotime( $strMaxDate ) ), 'property_name' => $this->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valArEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlLockedOn( $objPeriodClosingsFilter ) {

		$boolIsValid = true;

		if( ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getGlLockedOn() ) )
			|| ( 0 == $objPeriodClosingsFilter->getGeneralLedger() && 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getArLockedOn() ) )
			|| ( 0 == $objPeriodClosingsFilter->getGeneralLedger() && 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getApLockedOn() ) )
			|| ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && true == CValidation::valDate( $this->getGlLockedOn() ) && true == CValidation::valDate( $this->getArLockedOn() ) && true == CValidation::valDate( $this->getApLockedOn() ) ) ) {

			if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
				$arrstrCategories[] = 'AR';
			}

			if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
				$arrstrCategories[] = 'AP';
			}

			if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
				$arrstrCategories[] = 'GL';
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, categories} is already locked for {%s, property_name}.', [ 'categories' => implode( '/', $arrstrCategories ), 'property_name' => $this->getPropertyName() ] ) ) );

		} elseif( ( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getArPostMonth() ) )
					|| ( 1 == $objPeriodClosingsFilter->getAccountsPayable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getApPostMonth() ) )
					|| ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getPostMonth() ) == strtotime( $this->getGlPostMonth() ) ) ) {

			if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getArPostMonth() ) ) {
				$arrstrCategories[] = 'AR';
			}

			if( 1 == $objPeriodClosingsFilter->getAccountsPayable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getApPostMonth() ) ) {
				$arrstrCategories[] = 'AP';
			}

			if( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getPostMonth() ) == strtotime( $this->getArPostMonth() ) ) {
				$arrstrCategories[] = 'GL';
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Current {%s, categories} period(s) can not be locked for {%s, property_name}.', [ 'categories' => implode( '/', $arrstrCategories ), 'property_name' => $this->getPropertyName() ] ) ) );

		} elseif( 1 == $objPeriodClosingsFilter->getGeneralLedger()
					&& ( false == valStr( $this->getArLockedOn() ) || false == valStr( $this->getApLockedOn() ) ) ) {
			// Actual date or string 'NOW()' are valid inputs.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Lock AR and AP first for {%s, 0}.', [ $this->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInitialImport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLowestPostMonth( $strMessage ) {

		$boolIsValid = true;

		if( true == valStr( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'First {%s, message} period for {%t, post_month, DATE_NUMERIC_POSTMONTH} of {%s, property_name}.', [ 'message' => $strMessage, 'post_month' => $this->getPostMonth(), 'property_name' => $this->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valManagementFeePostMonth() {

		$boolIsValid = true;

		if( false == valStr( $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBulkLockPeriods( $objPeriodClosingsFilter ) {

		$boolIsValid		= true;
		$arrstrCategories	= [];

		if( ( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getArPostMonth() ) )
		    || ( 1 == $objPeriodClosingsFilter->getAccountsPayable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getApPostMonth() ) )
		    || ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getPostMonth() ) == strtotime( $this->getGlPostMonth() ) ) ) {
			if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getArPostMonth() ) ) {
				$arrstrCategories[] = 'AR';
				$arrstrPostMonth[] = $this->getPostMonth();
			}

			if( 1 == $objPeriodClosingsFilter->getAccountsPayable() && strtotime( $this->getPostMonth() ) == strtotime( $this->getApPostMonth() ) ) {
				$arrstrCategories[] = 'AP';
				$arrstrPostMonth[] = $this->getPostMonth();
			}

			if( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getPostMonth() ) == strtotime( $this->getGlPostMonth() ) ) {
				$arrstrCategories[] = 'GL';
				$arrstrPostMonth[] = $this->getPostMonth();
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The Current {%s, categories} post month is {%t, post_month, DATE_NUMERIC_POSTMONTH}. You cannot lock a current period.', [ 'categories' => implode( '/', $arrstrCategories ), 'post_month' => implode( '/', array_unique( $arrstrPostMonth ) ) ] ) ) );

		} elseif( 1 == $objPeriodClosingsFilter->getGeneralLedger() && 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable()
		           && ( false == valStr( $this->getArLockedOn() ) || false == valStr( $this->getApLockedOn() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL cannot be locked for a period where AR or AP is unlocked.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDate( $objPeriodClosingsFilter ) {

		$boolIsValid = true;

		if( ( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && strtotime( $this->getArEndDate() ) > strtotime( date( 'm/d/Y' ) ) )
			|| ( 1 == $objPeriodClosingsFilter->getAccountsPayable() && strtotime( $this->getApEndDate() ) > strtotime( date( 'm/d/Y' ) ) )
			|| ( 1 == $objPeriodClosingsFilter->getGeneralLedger() && strtotime( $this->getGlEndDate() ) > strtotime( date( 'm/d/Y' ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Locked periods cannot have an end date in the future.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objPeriodClosingsFilter = NULL, $strMessage = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valGlStartDate( $objPeriodClosingsFilter );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_lowest_period':
				$boolIsValid &= $this->valLowestPostMonth( $strMessage );
				break;

			case 'validate_lock_period':
				$boolIsValid &= $this->valGlLockedOn( $objPeriodClosingsFilter );
				break;

			case 'validate_management_fee':
				$boolIsValid &= $this->valManagementFeePostMonth();
				break;

			case 'validate_bulk_lock_period':
				$boolIsValid &= $this->valBulkLockPeriods( $objPeriodClosingsFilter );
				$boolIsValid &= $this->valEndDate( $objPeriodClosingsFilter );
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchGlDetailsByPeriodId( $objClientDatabase ) {
		return CGlDetails::fetchGlDetailsByPeriodIdByGlHeaderTypeIdsByCid( $this->getId(), [ CGlHeaderType::ACCRUAL_MONTH_END, CGlHeaderType::CASH_MONTH_END ], $this->getCid(), $objClientDatabase, false );
	}

	public function fetchEventsByReferenceId( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByCid( $this->getId(), CEventReferenceType::PERIOD, $this->getCid(), $objClientDatabase );
	}

	public function fetchPendingMoveInLeases( $objClientDatabase, $strEndDate = NULL ) {
		return \Psi\Eos\Entrata\CCachedLeases::createService()->fetchPendingMoveInLeaseDetailsByPeriodIdByCid( $this->getId(), $strEndDate, $this->getCid(), $objClientDatabase );
	}

	public function fetchPendingMoveOutLeases( $objClientDatabase, $strEndDate = NULL ) {
		return \Psi\Eos\Entrata\CCachedLeases::createService()->fetchPendingMoveOutLeaseDetailsByPeriodIdByCid( $this->getId(), $strEndDate, $this->getCid(), $objClientDatabase );
	}

	public function fetchLeaseTransfers( $objClientDatabase, $strEndDate = NULL ) {
		return \Psi\Eos\Entrata\CCachedLeases::createService()->fetchPaginatedTransferLeasesWithCustomerDetailsByCid( $this->getCid(), $objClientDatabase, 0, NULL, NULL, NULL, 0, $this->getId(), $strEndDate );
	}

	public function fetchPendingFinancialMoveOutLeases( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCachedLeases::createService()->fetchPendingFinancialMoveOutLeaseDetailsByPeriodByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApprovedApHeaders( $intCompanyUser, $objClientDatabase, $boolIsAdministrator = false, $strEndDate = NULL ) {
		return CApHeaders::fetchPaginatedApHeadersByApFinancialStatusTypeIdByCompanyUserIdByCid( CApFinancialStatusType::PENDING, $intCompanyUser, $this->getCid(), $objClientDatabase, NULL, NULL, $boolIsAdministrator, $this->getId(), $strEndDate );
	}

	public function fetchApprovedInvoices( $intCompanyUserId, $objClientDatabase, $boolIsAdministrator = false ) {
		return CApHeaders::fetchPaginatedApHeadersForDashboardByPropertyIdsByCompanyUserIdByCid( 1, [ $this->getPropertyId() ], $intCompanyUserId, $this->getCid(), NULL, $objClientDatabase, NULL, $boolIsAdministrator, $this->getId() );
	}

	public function fetchApplicantRents( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchApplicantRentTransactionsByPostMonthByPropertyIdByCid( $this->getPostMonth(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchDepositInterestsByEndDate( $strEndDate, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchDepositInterestPostingLeasesByPropertyIdByCid( $this->getPropertyId(), $strEndDate, $this->getCid(), $objClientDatabase );
	}

	public function fetchRefundInterests( $objClientDatabase ) {
		return CApHeaders::fetchApHeadersByPeriodIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Other Function
	 */

	public static function insertPeriods( $intCid, $intPropertyId, $strPostMonth, $intUserId, $objClientDatabase ) {
		return CPeriods::fetchPeriods( 'SELECT * FROM load_period_id_by_post_month( ' . ( int ) $intCid . ', ' . ( int ) $intPropertyId . ', \'' . $strPostMonth . '\', TRUE, ' . ( int ) $intUserId . ' )', $objClientDatabase );
	}

}
?>