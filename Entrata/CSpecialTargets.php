<?php

class CSpecialTargets extends CBaseSpecialTargets {

    public static function fetchSpecialTargets( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CSpecialTarget', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchSpecialTarget( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSpecialTarget', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>