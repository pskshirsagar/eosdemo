<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultClassifiedCategories
 * Do not add any new functions to this class.
 */

class CDefaultClassifiedCategories extends CBaseDefaultClassifiedCategories {

	public static function fetchDefaultClassifiedCategories( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultClassifiedCategory::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultClassifiedCategory( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultClassifiedCategory::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>