<?php

class COneTimeLink extends CBaseOneTimeLink {

	protected $m_strUsername;
	protected $m_strPassword;

	const EXPIRY_DAYS = 14;

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( $arrmixValues['username'] );
		if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( $arrmixValues['password'] );

		return;
	}
}
?>