<?php

class CSharedFilter extends CBaseSharedFilter {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilterTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActions() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSharedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSharedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;

		}

		return $boolIsValid;
	}

}
?>