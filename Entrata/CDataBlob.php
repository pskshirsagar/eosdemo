<?php

class CDataBlob extends CBaseDataBlob {

	const LOOKUP_TYPE_REASSCOCIATE_PAYMENT					= 'reassociate_payment';
	const LOOKUP_TYPE_CUSTOMERS_FOR_CHECK21_BATCHES 		= 'customers_for_check21_batches';
	const LOOKUP_TYPE_CALL_CENTER_WORK_ORDER_RESIDENTS 		= 'call_center_work_order_residents';

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataBlobTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataField1() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataField2() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataField3() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBlob() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>