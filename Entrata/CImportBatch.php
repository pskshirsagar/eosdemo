<?php

class CImportBatch extends CBaseImportBatch {

    /**
     * Validation Functions
     */

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'You must provide an client id.' ) );
        }

        return $boolIsValid;
    }

    public function valImportSourceId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intImportSourceId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_source_id', 'You must provide an import source id.' ) );
        }

        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCompanyEmployeeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', 'You must provide an company employee id.' ) );
        }

        return $boolIsValid;
    }

    public function valDuplicateImportBatch( $objDatabase ) {

    	$boolIsValid = true;

    	if( true == is_numeric( $this->getCid() ) ) {
    		$objImportBatch = CImportBatches::fetchUnProcessedImportBatchByCid( $this->getCid(), $objDatabase );

    		if( true == valObj( $objImportBatch, 'CImportBatch' ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Duplicate import batch please process current added batch.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	// $boolIsValid &= $this->valImportSourceId();
            	// $boolIsValid &= $this->valCompanyEmployeeId();
            	$boolIsValid &= $this->valDuplicateImportBatch( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchPropertyImportBatches( $objDatabase ) {
		return CPropertyImportBatches::fetchPropertyImportBatchesByCidByImportBatchId( $this->getCid(), $this->getId(), $objDatabase );
    }
}
?>