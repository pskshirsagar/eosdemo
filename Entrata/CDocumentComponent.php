<?php

class CDocumentComponent extends CBaseDocumentComponent {

	protected $m_arrobjDocumentComponents;
	protected $m_arrobjDocumentParameters;
	protected $m_arrobjFormAnswers;

	protected $m_arrstrDocumentParameters;
	protected $m_arrstrErrorMsgs;
	// protected $m_arrstrErrorData;
	protected $m_arrstrFormData;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjDocumentComponents 			= [];
        $this->m_arrobjDocumentParameters 			= [];
        $this->m_arrobjFormAnswers 					= [];

        $this->m_arrstrSimpleDocumentParameters		= [];
        $this->m_arrstrFormData 					= [];
        $this->m_arrstrErrorMsgs 					= [];

        return;
    }

    public function valComponentType() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valSize() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    /**
     * Add Functions
     */

    public function addDocumentParameter( $objDocumentParameter ) {
    	$this->m_arrobjDocumentParameters[$objDocumentParameter->getKey()] = $objDocumentParameter;
    	$this->m_arrstrSimpleDocumentParameters[$objDocumentParameter->getKey()] = $objDocumentParameter->getValue();
    }

    public function addFormAnswer( $objFormAnswer ) {
    	$this->m_arrobjFormAnswers[$objFormAnswer->getQuestion()] = $objFormAnswer;
    	$this->m_arrstrFormData[$objFormAnswer->getQuestion()] = $objFormAnswer->getAnswer();

    	if( true == valArr( $objFormAnswer->getErrorMsgs() ) ) {
    		// We only allow one error message per customer application answer
    		foreach( $objFormAnswer->getErrorMsgs() as $objErrorMsg ) {
    			$this->m_arrstrErrorMsgs[str_replace( 'require_', '', $objFormAnswer->getQuestion() )] = $objErrorMsg->getMessage();
    			continue;
    		}
    	}
    }

    public function addDocumentComponent( $objDocumentComponent ) {
    	$this->m_arrobjDocumentComponents[$objDocumentComponent->getId()] = $objDocumentComponent;
    }

    /**
     * Get Functions
     */

    public function getDocumentComponents() {
    	return $this->m_arrobjDocumentComponents;
    }

    // This is a function designed to be agnostic to which type of component parameters are being called
    // For example, the grid engine coudl get component parameters, website parameters and document parameters
    // with one common function.

    public function getDocumentParameters() {
    	return $this->m_arrobjDocumentParameters;
    }

    // This array stores an equally agnostic array of string data that prevents the use of if isset statements
    // in the grid engine templates

    public function getSimpleDocumentParameters() {
    	return $this->m_arrstrSimpleDocumentParameters;
    }

    // This function is required so grid engine can run the same regardless of whether it is rendering an application or website page.

    public function getFormData() {
    	return $this->m_arrstrFormData;
    }

	public function getErrorMsgs() {
    	return $this->m_arrstrErrorMsgs;
    }

    public function getFormAnswers() {
    	return $this->m_arrobjFormAnswers;
    }

    /**
     * Create Functions
     */

    public function createDocumentParameter() {

    	$objDocumentParameter = new CDocumentParameter();
    	$objDocumentParameter->setCid( $this->m_intCid );
    	$objDocumentParameter->setDocumentId( $this->m_intDocumentId );
    	$objDocumentParameter->setDocumentComponentId( $this->m_intId );

    	return $objDocumentParameter;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchDocumentParameters( $objDatabase ) {

        return \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByDocumentComponentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

}
?>