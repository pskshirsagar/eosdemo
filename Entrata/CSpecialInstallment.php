<?php

class CSpecialInstallment extends CBaseSpecialInstallment {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSpecialId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeStartDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeEndDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEarlyChargeMonths() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseBillingMonthAsPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostAllAtOnce() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>