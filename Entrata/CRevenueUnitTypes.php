<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypes
 * Do not add any new functions to this class.
 */

class CRevenueUnitTypes extends CBaseRevenueUnitTypes {

	const DAILY_AVAILABILITY_CHANGE_RATE = 0.0001;
	const BASE_RENT                      = 500;

	public static function fetchMissingRevenueUnitTypes( $objDatabase ) {

		$strSql = 'SELECT
						ut.cid,
						ut.property_id,
						ut.id AS unit_type_id,
						\'today\'::DATE AS effective_date,
						\'12/31/2099\'::DATE AS expiry_date,
						COALESCE( NULLIF( ROUND( us.demand_thirty_day / 30, 4 ), 0 ), ' . self::DAILY_AVAILABILITY_CHANGE_RATE . ' ) AS daily_availability_change_rate,
						COALESCE( ca.avg_executed_base_rent, ' . self::BASE_RENT . ' ) AS base_rent,
						cf.min_market_rent AS min_advertised_rent,
						cf.max_market_rent AS max_advertised_rent,
						cf.avg_market_rent AS avg_advertised_rent,
						ca.min_executed_rent,
						ca.max_executed_rent,
						ca.avg_executed_rent,
						ROUND( us.demand_thirty_day )::INTEGER AS demand_thirty_day,
						ROUND( us.demand_thirty_day * 2 )::INTEGER AS demand_sixty_day,
						ROUND( us.demand_thirty_day * 3 )::INTEGER AS demand_ninety_day,
						NOW() AS updated_on,
						NOW() AS created_on
					FROM
						unit_types AS ut
						JOIN property_products AS pp ON ut.cid = pp.cid AND ut.property_id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::PRICING . '
						JOIN property_preferences AS prefs ON ut.cid = prefs.cid AND ut.property_id = prefs.property_id AND prefs.key = \'PRICING_LEASES\' AND prefs.value <> \'None\'
						JOIN LATERAL ( SELECT COUNT(*) / 12 * 0.8 AS demand_thirty_day FROM unit_spaces WHERE ut.cid = cid AND ut.id = unit_type_id AND is_marketed AND deleted_on IS NULL ) AS us ON TRUE
						LEFT JOIN cached_floorplans AS cf ON ut.cid = cf.cid AND ut.property_floorplan_id = cf.property_floorplan_id AND cf.occupancy_type_id = 1 AND effective_date = \'today\'
						JOIN property_charge_settings pcs on cf.cid = pcs.cid and pcs.property_id = cf.property_id
						JOIN lease_terms lt on pcs.cid = lt.cid and pcs.lease_term_structure_id = lt.lease_term_structure_id AND cf.lease_term_id = lt.id AND lt.term_month = ' . CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM . '
						LEFT JOIN revenue_unit_types AS rut ON ut.cid = rut.cid AND ut.id = rut.unit_type_id
						LEFT JOIN LATERAL (
						    SELECT 
						        MIN(executed_monthly_rent_total) AS min_executed_rent
						        ,MAX(executed_monthly_rent_total) AS max_executed_rent
						        ,AVG(executed_monthly_rent_total) AS avg_executed_rent
						        ,AVG(executed_monthly_rent_base) AS avg_executed_base_rent
						    FROM 
						        cached_applications
						    WHERE
						        ut.cid = cid
						        AND ut.id = unit_type_id
						        AND NOT is_deleted
						        AND lease_approved_on BETWEEN \'today\'::DATE - INTERVAL \'90 days\' AND \'today\'::DATE
						        AND cancelled_on IS NULL
						) AS ca ON TRUE
					WHERE
						ut.deleted_on IS NULL
						AND rut.daily_availability_change_rate IS NULL';

		return self::fetchRevenueUnitTypes( $strSql, $objDatabase );
	}

}
?>