<?php

class CSystemMessageType extends CBaseSystemMessageType {

	const EMAIL 		= 1;
	const SMS			= 2;
	const NOTIFICATION 	= 3;
	const WECHAT        = 4;

	public static $c_arrstrSystemMessageTypes = [
		self::EMAIL 		=> 'Email',
		self::SMS 			=> 'SMS',
		self::NOTIFICATION 	=> 'Notification'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>