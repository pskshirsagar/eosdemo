<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAddendas
 * Do not add any new functions to this class.
 */

class CDocumentAddendas extends CBaseDocumentAddendas {

	// From Company Application Documents Module File

	public static function fetchDocumentAddendaByCidByDocumentId( $intCid, $intDocumentId, $objDatabase ) {
		$strSql = 'SELECT * FROM document_addendas WHERE cid = ' . ( int ) $intCid . ' AND document_id = ' . ( int ) $intDocumentId;

		return self::fetchDocumentAddenda( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendaByIdByCompanyApplicationIdByCid( $intCompanyApplicationId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_addendas WHERE company_application_id = ' . ( int ) $intCompanyApplicationId . ' AND cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId;

		return self::fetchDocumentAddenda( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendaByDocumentAddendaIdByIdByCid( $intDocumentAddendaId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_addendas WHERE document_addenda_id = ' . ( int ) $intDocumentAddendaId . ' AND cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId;
		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendasByCidByDocumentId( $intCid, $intDocumentId, $objDatabase, $boolIsOptional = NULL, $intLeaseCustomerId = NULL ) {

		$strSql = 'SELECT
						*
					FROM
						document_addendas
					WHERE
						cid = ' . ( int ) $intCid . '
						AND document_id = ' . ( int ) $intDocumentId . '
						AND archived_on IS NULL
						AND deleted_by IS NULL ';
		if( 0 === $boolIsOptional ) {
			$strSql .= 'AND ( is_optional = ' . ( int ) $boolIsOptional . ' OR is_optional IS NULL )';
		} elseif( 1 === $boolIsOptional ) {
			$strSql .= 'AND is_optional = ' . ( int ) $boolIsOptional;
		}

		switch( $intLeaseCustomerId ) {
			case CCustomerType::PRIMARY:
				$strSql .= ' AND is_for_primary_applicant = 1 ';
				break;

			case CCustomerType::RESPONSIBLE:
				$strSql .= ' AND is_for_co_applicant = 1 ';
				break;

			case CCustomerType::GUARANTOR:
				$strSql .= ' AND is_for_co_signer = 1 ';
				break;

			default:
				$boolIsValid = true;
				break;
		}

		$strSql .= ' ORDER BY order_num ';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendaByDocumentIdByMasterDocumentIdByCid( $intDocumentId, $intMasterDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_addendas WHERE document_id = ' . ( int ) $intDocumentId . ' AND cid = ' . ( int ) $intCid . ' AND master_document_id = ' . ( int ) $intMasterDocumentId;
	   	return self::fetchDocumentAddenda( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendaByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
   		 $strSql = 'SELECT * FROM document_addendas da WHERE document_id = ' . ( int ) $intDocumentId . ' AND cid = ' . ( int ) $intCid;
			return self::fetchDocumentAddenda( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendasByCidByDocumentIds( $intCid, $arrintDocumentIds, $objDatabase, $boolIsCheckedMasterDocumentId = false ) {
		$strSql = 'SELECT
						*
				   FROM document_addendas
				   WHERE cid = ' . ( int ) $intCid . '
				   		AND document_id IN (' . implode( ',', $arrintDocumentIds ) . ')';

		if( true == $boolIsCheckedMasterDocumentId ) {
			$strSql .= ' AND master_document_id IS NULL
						 AND is_published = 1
						 AND archived_on IS NULL';
		}

		$strSql .= ' AND deleted_by IS NULL
				   ORDER BY order_num, created_on';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendasByCidByCompanyApplicationId( $intCid, $intCompanyApplicationId, $objDatabase ) {
		$strSql = '	SELECT
						da.*
					FROM
						document_addendas da
						JOIN documents d ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
						da.company_application_id =  ' . ( int ) $intCompanyApplicationId . '
						AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
						AND d.document_sub_type_id IN ( ' . CDocumentSubType::APPLICATION_POLICY_DOCUMENT . ', ' . CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT . ' )
					 	AND da.cid = ' . ( int ) $intCid . '
					 	AND da.deleted_by IS NULL
					ORDER BY da.order_num, da.created_on';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchChildDocumentAddendaHavingMaxOrderNumByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						da.order_num
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid  )
					WHERE
						da.master_document_id = ' . ( int ) $intDocumentId . '
						AND da.cid = ' . ( int ) $intCid . '
						AND da.archived_on IS NULL
						AND da.deleted_by IS NULL
						ORDER BY da.order_num DESC LIMIT 1';

		$arrintOrderNums = fetchData( $strSql, $objDatabase );

		$intMaxOrderNum = NULL;

		if( true == valArr( $arrintOrderNums ) ) {
			foreach( $arrintOrderNums as $arrintOrderNum ) {
				$intMaxOrderNum = $arrintOrderNum['order_num'];
			}
		}

		return $intMaxOrderNum;
	}

	public static function fetchDocumentAddendasByCidByIds( $intCid, $arrintDocumentAddendaIds, $objDatabase ) {

		if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT * FROM document_addendas WHERE cid = ' . ( int ) $intCid . ' AND id IN (' . implode( ',', $arrintDocumentAddendaIds ) . ') AND deleted_by IS NULL ORDER BY order_num, created_on';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchMaxChildDocumentAddendaOrderNumByMasterDocumentIdByCid( $intMasterDocumentId, $intCid, $objDatabase, $boolHasExternalKey = NULL ) {
		$strSql = ' SELECT max(order_num) FROM document_addendas WHERE master_document_id = ' . ( int ) $intMasterDocumentId . ' AND cid = ' . ( int ) $intCid;

	   	if( true === $boolHasExternalKey ) {
			$strSql .= ' AND external_key IS NOT NULL ';
		} elseif( false === $boolHasExternalKey ) {
			$strSql .= ' AND external_key IS NULL ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['max'] ) && 0 < $arrstrData[0]['max'] ) {
			return $arrstrData[0]['max'];
		} else {
			return 0; // we want to make sure that master lease document and guarantor's master document should be at 1st and 2nd respectively. [KAL]
		}
	}

	public static function fetchLeaseAddendaCountByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
					da.master_document_id,
					count(da.id) As addenda_count
				FROM document_addendas da JOIN documents d on ( d.id = da.document_id and d.cid = da.cid )
				WHERE
					da.cid = ' . ( int ) $intCid . '
					AND da.master_document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
					AND da.is_hidden = false
					AND da.is_published = 1
					AND da.archived_on IS NULL
					AND da.deleted_by IS NULL
					AND d.deleted_on IS NULL
					AND d.archived_on IS NULL
				GROUP BY(da.master_document_id) ';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		$arrmixDocumentsAddendaCount = [];

		if( true == valArr( $arrintResponses ) ) {
			foreach( $arrintResponses as $arrintResponse ) {
				$arrmixDocumentsAddendaCount[$arrintResponse['master_document_id']] = $arrintResponse['addenda_count'];
			}
		}

		return $arrmixDocumentsAddendaCount;
	}

	public static function fetchLeaseDocumentAddendaByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase, $boolIsDocumentTemplate = false, $intMasterDocumentId = NULL ) {

		$strSql = 'SELECT
						*
					FROM
						document_addendas
					WHERE document_id = ' . ( int ) $intDocumentId . '
						  AND cid = ' . ( int ) $intCid;

					if( true == $boolIsDocumentTemplate ) {
						if( false == is_null( $intMasterDocumentId ) ) {
							$strSql .= ' AND master_document_id = ' . ( int ) $intMasterDocumentId;
						} else {
							$strSql .= ' AND master_document_id IS NULL ';
						}
					}

					$strSql .= ' AND deleted_on IS NULL
							    AND archived_on IS NULL';

		return self::fetchDocumentAddenda( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentAddendasByMasterDocumentIdByCid( $intMasterDocumentId, $intCid, $objDatabase, $boolOnlyChildDocumentAddendas = false ) {

		$strCondition = ' AND da.master_document_id = ' . ( int ) $intMasterDocumentId;

		if( true == $boolOnlyChildDocumentAddendas ) {
			$strCondition .= ' AND da.document_id != ' . ( int ) $intMasterDocumentId;
		}

		$strSql = 'SELECT
						da.*
					FROM
						document_addendas da
 					    JOIN documents d ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid;
		$strSql .= $strCondition;
		$strSql .= ' AND d.deleted_by IS NULL
					 AND d.archived_on IS NULL
				     AND da.deleted_by IS NULL
					 AND da.archived_on IS NULL
				ORDER BY da.order_num';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentAddendasByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase, $boolOnlyAssociatedDocumentAddendas = false ) {

		$strCondition = ' AND da.document_id = ' . ( int ) $intDocumentId;

		if( true == $boolOnlyAssociatedDocumentAddendas ) {
			$strCondition .= ' AND da.master_document_id IS NOT NULL';
		}

		$strSql = 'SELECT
						da.*
					FROM
						document_addendas da
 					    JOIN documents d ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid;
		$strSql .= $strCondition;
		$strSql .= ' AND d.deleted_by IS NULL AND da.deleted_on IS NULL';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchAssociatedDocumentAddendasByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) )

		$strSql = 'SELECT
						da.*
					FROM
						document_addendas da
 					    JOIN documents d ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND da.document_id IN (' . implode( ',', $arrintDocumentIds ) . ' )
						AND da.master_document_id IS NOT NULL
						AND d.deleted_by IS NULL AND da.deleted_on IS NULL';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentAddendasByMasterDocumentIdByOrderNumByCid( $intMasterDocumentId, $intOrderNum, $strPosition, $intCid, $objDatabase, $boolOnlyChildDocumentAddendas = false ) {

		$strCondition = ' AND da.master_document_id = ' . ( int ) $intMasterDocumentId;

		if( true == $boolOnlyChildDocumentAddendas ) {
			$strCondition .= ' AND da.document_id != ' . ( int ) $intMasterDocumentId;
		}

		if( '' != $strPosition && 'Before' == $strPosition ) {
			$strCondition .= ' AND da.order_num >=' . ( int ) $intOrderNum;
		} elseif( '' != $strPosition && 'After' == $strPosition ) {
			$strCondition .= ' AND da.order_num > ' . ( int ) $intOrderNum;
		}

		$strSql = 'SELECT
						da.*
					FROM
						document_addendas da
 					    JOIN documents d ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid;
		$strSql .= $strCondition;
		$strSql .= ' AND d.deleted_by IS NULL
					 AND d.archived_on IS NULL
				     AND da.deleted_by IS NULL
					 AND da.archived_on IS NULL
				ORDER BY da.order_num ASC';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchBluemoonLeaseDocumentAddendasByMasterDocumentIdByCid( $intMasterDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						da.*
					FROM
						document_addendas da
						JOIN documents d ON da.document_id = d.id AND da.cid = d.cid
					WHERE
						da.cid = ' . ( int ) $intCid . '
						AND da.master_document_id = ' . ( int ) $intMasterDocumentId . '
						AND d.deleted_by IS NULL
    					AND da.deleted_by IS NULL
					 	AND da.archived_on IS NULL
				     	AND da.external_key IS NOT NULL';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

	public static function fetchDocumentAddendaByDocumentIdsByMasterDocumentIdByCid( $arrintDocumentIds, $intCid, $objDatabase, $intMasterDocumentId ) {

		$strSql = 'SELECT
						*
					FROM
						document_addendas
					WHERE document_id IN (' . implode( ',', $arrintDocumentIds ) . ' )
						  AND cid = ' . ( int ) $intCid . '
						  AND master_document_id = ' . ( int ) $intMasterDocumentId . '
						  AND deleted_on IS NULL
						  AND archived_on IS NULL';

		return self::fetchDocumentAddendas( $strSql, $objDatabase );
	}

}
?>