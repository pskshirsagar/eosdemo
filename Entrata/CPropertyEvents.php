<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEvents
 * Do not add any new functions to this class.
 */

class CPropertyEvents extends CBasePropertyEvents {

	/**
	 * Fetch Functions
	 */

	public static function fetchPropertyEventByCompanyEventIdByPropertyIdByCid( $intCompanyEventId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT pe.* FROM property_events pe, company_events ce WHERE ce.id = pe.company_event_id AND ce.cid = pe.cid AND ce.id = ' . ( int ) $intCompanyEventId . ' AND pe.property_id = ' . ( int ) $intPropertyId . ' AND ce.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchPropertyEvent( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyEventsByCidByCompanyEventIdsByFieldNames( $arrintCompanyEventIds, $intCid, $arrstrFieldNames, $objDatabase ) {

		if ( false == valArr( $arrintCompanyEventIds ) )	return NULL;
		if ( false == valArr( $arrstrFieldNames ) ) 		return NULL;

		$arrstrReorderedPropertyEvents = [];

		$strSql	= 'SELECT ' . implode( ',', $arrstrFieldNames ) . ' FROM property_events WHERE company_event_id IN ( ' . implode( ',', $arrintCompanyEventIds ) . ' ) AND cid = ' . ( int ) $intCid;
		$arrstrPropertyEvents = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrPropertyEvents ) ) {
			foreach( $arrstrPropertyEvents as $arrstrPropertyEvent ) {
				$arrstrReorderedPropertyEvents[$arrstrPropertyEvent['id']] = $arrstrPropertyEvent;
			}
		}

		return $arrstrReorderedPropertyEvents;
	}

	public static function fetchPropertyEventsByCidByCompanyEventIds( $arrintCompanyEventIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintCompanyEventIds ) )	return NULL;

		$strSql = 'SELECT
						*
					FROM property_events
					WHERE cid = ' . ( int ) $intCid . '
						AND company_event_id IN  (' . implode( ',', $arrintCompanyEventIds ) . ')';

		return self::fetchPropertyEvents( $strSql, $objDatabase );

	}

}
?>