<?php

class CPropertyGroupDocument extends CBasePropertyGroupDocument {

	protected $m_intPropertyId;
	protected $m_strBlueMoonKey;
	protected $m_strPropertyName;

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getBlueMoonKey() {
    	return $this->m_strBlueMoonKey;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrValues['property_id'] ) )				$this->setPropertyId( $arrValues['property_id'] );
    	if( true == isset( $arrValues['blue_moon_key'] ) )				$this->setBlueMoonKey( $arrValues['blue_moon_key'] );
    	if( true == isset( $arrValues['property_name'] ) )				$this->setPropertyName( $arrValues['property_name'] );
    	return;
    }

    public function setBlueMoonKey( $strBlueMoonKey ) {
    	$this->m_strBlueMoonKey = $strBlueMoonKey;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentAssociationTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>