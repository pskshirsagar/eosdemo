<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationSubmissions
 * Do not add any new functions to this class.
 */

class CApplicantApplicationSubmissions extends CBaseApplicantApplicationSubmissions {

	public static function fetchApplicantApplicationSubmissionsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						applicant_application_submissions
					WHERE
						applicant_application_id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
					AND cid =  ' . ( int ) $intCid;

		return self::fetchApplicantApplicationSubmissions( $strSql, $objDatabase );
	}

}
?>