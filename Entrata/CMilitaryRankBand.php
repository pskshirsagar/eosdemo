<?php

class CMilitaryRankBand extends CBaseMilitaryRankBand {

	protected $m_intAssociatedUnitCount;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryOfficerStructureId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['associated_unit_count'] ) ) {
			$this->setAssociatedUnitCount( $arrmixValues['associated_unit_count'] );
		}
	}

	public function getAssociatedUnitCount() {
		return $this->m_intAssociatedUnitCount;
	}

	public function setAssociatedUnitCount( $intAssociatedUnitCount ) {
		$this->m_intAssociatedUnitCount = $intAssociatedUnitCount;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createMilitaryRankBandUnitSpace() {
		$objMilitaryRankBandUnitSpace = new CMilitaryRankBandUnitSpace();
		$objMilitaryRankBandUnitSpace->setCid( $this->m_intCid );
		$objMilitaryRankBandUnitSpace->setRankBandId( $this->m_intId );
		return $objMilitaryRankBandUnitSpace;
	}

}
?>