<?php

class CGlAccount extends CBaseGlAccount {

	const LOOKUP_TYPE_GL_ACCOUNTS_FOR_IC	= 'gl_accounts_for_ic';
	const LOOKUP_TYPE_AP_PAYEES_GL_ACCOUNT	= 'ap_payees_gl_account';

	const SYSTEM_CODE_RETAINED_EARNINGS				= 'RE';
	const SYSTEM_CODE_ACCOUNTS_RECEIVABLE			= 'AR';
	const SYSTEM_CODE_ACCOUNTS_PAYABLE				= 'AP';
	const SYSTEM_CODE_CASH_IN_BANK_OPERATING		= 'CIBO';
	const SYSTEM_CODE_CASH_IN_BANK_RESIDENTPAY		= 'CIBRP';
	const SYSTEM_CODE_SECURITY_DEPOSIT_LIABILITY	= 'SDL';
	const SYSTEM_CODE_RENT_OR_LEASE_INCOME			= 'RNTLSEINC';
	const SYSTEM_CODE_PAYMENTS_IN_TRANSIT			= 'PIT';
	const SYSTEM_CODE_PREPAID_RENT_LIABILITY		= 'PRL';
	const SYSTEM_CODE_REFUNDS_IN_TRANSIT			= 'RIT';

	protected $m_intGlTreeId;
	protected $m_intGlGroupId;
	protected $m_intGlTreeTypeId;
	protected $m_intParentGlGroupId;
	protected $m_intOriginGlGroupId;
	protected $m_intDefaultGlGroupId;
	protected $m_intRestrictedGlTreeId;
	protected $m_intGlGroupTypeId;
	protected $m_intHideIfZero;
	protected $m_intOrderNum;
	protected $m_intApCodeId;
	protected $m_intPropertyId;

	protected $m_strAccountName;
	protected $m_strDescription;
	protected $m_strGlGroupName;
	protected $m_strAccountNumber;
	protected $m_strSecondaryNumber;
	protected $m_strGlAccountTypeName;
	protected $m_strGlChartSystemCode;
	protected $m_strAccountNumberPattern;
	protected $m_strAccountNumberDelimiter;
	protected $m_strFormattedAccountNumber;

	/**
	 * Get Functions
	 */

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function getGlGroupId() {
		return $this->m_intGlGroupId;
	}

	public function getGlTreeTypeId() {
		return $this->m_intGlTreeTypeId;
	}

	public function getParentGlGroupId() {
		return $this->m_intParentGlGroupId;
	}

	public function getOriginGlGroupId() {
		return $this->m_intOriginGlGroupId;
	}

	public function getDefaultGlGroupId() {
		return $this->m_intDefaultGlGroupId;
	}

	public function getRestrictedGlTreeId() {
		return $this->m_intRestrictedGlTreeId;
	}

	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	public function getHideIfZero() {
		return $this->m_intHideIfZero;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function getGlGroupName() {
		return $this->m_strGlGroupName;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function getGlAccountTypeName() {
		return $this->m_strGlAccountTypeName;
	}

	public function getGlChartSystemCode() {
		return $this->m_strGlChartSystemCode;
	}

	public function getAccountNumberPattern() {
		return $this->m_strAccountNumberPattern;
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	public function getFormattedAccountNumber() {
		return $this->m_strFormattedAccountNumber;
	}

	public function getUnFormattedAccountNumber() {
		return str_replace( $this->getAccountNumberDelimiter(), '', $this->getAccountNumber() );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setGlTreeId( $intGlTreeId ) {
		$this->m_intGlTreeId = CStrings::strToIntDef( $intGlTreeId, NULL, false );
	}

	public function setGlGroupId( $intGlGroupId ) {
		$this->m_intGlGroupId = CStrings::strToIntDef( $intGlGroupId, NULL, false );
	}

	public function setGlTreeTypeId( $intGlTreeTypeId ) {
		$this->m_intGlTreeTypeId = CStrings::strToIntDef( $intGlTreeTypeId, NULL, false );
	}

	public function setParentGlGroupId( $intParentGlGroupId ) {
		$this->m_intParentGlGroupId = CStrings::strToIntDef( $intParentGlGroupId, NULL, false );
	}

	public function setOriginGlGroupId( $intOriginGlGroupId ) {
		$this->m_intOriginGlGroupId = CStrings::strToIntDef( $intOriginGlGroupId, NULL, false );
	}

	public function setDefaultGlGroupId( $intDefaultGlGroupId ) {
		$this->m_intDefaultGlGroupId = CStrings::strToIntDef( $intDefaultGlGroupId, NULL, false );
	}

	public function setRestrictedGlTreeId( $intRestrictedGlTreeId ) {
		$this->m_intRestrictedGlTreeId = CStrings::strToIntDef( $intRestrictedGlTreeId, NULL, false );
	}

	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->m_intGlGroupTypeId = CStrings::strToIntDef( $intGlGroupTypeId, NULL, false );
	}

	public function setHideIfZero( $intHideIfZero ) {
		$this->m_intHideIfZero = CStrings::strToIntDef( $intHideIfZero, NULL, false );
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
	}

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = CStrings::strToIntDef( $intApCodeId, NULL, false );
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = CStrings::strTrimDef( $strAccountName, 50, NULL, true );
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
	}

	public function setGlGroupName( $strGlGroupName ) {
		$this->m_strGlGroupName = CStrings::strTrimDef( $strGlGroupName, 50, NULL, true );
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = CStrings::strTrimDef( $strAccountNumber, 80, NULL, true );
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->m_strSecondaryNumber = CStrings::strTrimDef( $strSecondaryNumber, 50, NULL, true );
	}

	public function setGlAccountTypeName( $strName ) {
		$this->m_strGlAccountTypeName = CStrings::strTrimDef( $strName, 50, NULL, true );
	}

	public function setGlChartSystemCode( $strGlChartSystemCode ) {
		$this->m_strGlChartSystemCode = CStrings::strTrimDef( $strGlChartSystemCode, 10, NULL, true );
	}

	public function setAccountNumberPattern( $strAccountNumberPattern ) {
		$this->m_strAccountNumberPattern = CStrings::strTrimDef( $strAccountNumberPattern, 50, NULL, true );
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		$this->m_strAccountNumberDelimiter = CStrings::strTrimDef( $strAccountNumberDelimiter, 50, NULL, true );
	}

	public function setFormattedAccountNumber( $strFormattedAccountNumber ) {
		$this->m_strFormattedAccountNumber = CStrings::strTrimDef( $strFormattedAccountNumber, 80, NULL, true );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['gl_tree_id'] ) && $boolDirectSet ) $this->m_intGlTreeId = trim( $arrmixValues['gl_tree_id'] );
			elseif ( isset( $arrmixValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrmixValues['gl_tree_id'] );
		if( isset( $arrmixValues['gl_group_id'] ) && $boolDirectSet ) $this->m_intGlGroupId = trim( $arrmixValues['gl_group_id'] );
			elseif ( isset( $arrmixValues['gl_group_id'] ) ) $this->setGlGroupId( $arrmixValues['gl_group_id'] );
		if( isset( $arrmixValues['gl_tree_type_id'] ) && $boolDirectSet ) $this->m_intGlTreeTypeId = trim( $arrmixValues['gl_tree_type_id'] );
			elseif ( isset( $arrmixValues['gl_tree_type_id'] ) ) $this->setGlTreeTypeId( $arrmixValues['gl_tree_type_id'] );
		if( isset( $arrmixValues['parent_gl_group_id'] ) && $boolDirectSet ) $this->m_intParentGlGroupId = trim( $arrmixValues['parent_gl_group_id'] );
			elseif ( isset( $arrmixValues['parent_gl_group_id'] ) ) $this->setParentGlGroupId( $arrmixValues['parent_gl_group_id'] );
		if( isset( $arrmixValues['origin_gl_group_id'] ) && $boolDirectSet ) $this->m_intOriginGlGroupId = trim( $arrmixValues['origin_gl_group_id'] );
			elseif ( isset( $arrmixValues['origin_gl_group_id'] ) ) $this->setOriginGlGroupId( $arrmixValues['origin_gl_group_id'] );
		if( isset( $arrmixValues['default_gl_group_id'] ) && $boolDirectSet ) $this->m_intDefaultGlGroupId = trim( $arrmixValues['default_gl_group_id'] );
			elseif ( isset( $arrmixValues['default_gl_group_id'] ) ) $this->setDefaultGlGroupId( $arrmixValues['default_gl_group_id'] );
		if( isset( $arrmixValues['restricted_gl_tree_id'] ) && $boolDirectSet ) $this->m_intRestrictedGlTreeId = trim( $arrmixValues['restricted_gl_tree_id'] );
			elseif ( isset( $arrmixValues['restricted_gl_tree_id'] ) ) $this->setRestrictedGlTreeId( $arrmixValues['restricted_gl_tree_id'] );
		if( isset( $arrmixValues['gl_group_type_id'] ) && $boolDirectSet ) $this->m_intGlGroupTypeId = trim( $arrmixValues['gl_group_type_id'] );
			elseif ( isset( $arrmixValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrmixValues['gl_group_type_id'] );
		if( isset( $arrmixValues['hide_if_zero'] ) && $boolDirectSet ) $this->m_intHideIfZero = trim( $arrmixValues['hide_if_zero'] );
			elseif ( isset( $arrmixValues['hide_if_zero'] ) ) $this->setHideIfZero( $arrmixValues['hide_if_zero'] );
		if( isset( $arrmixValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrmixValues['order_num'] );
			elseif ( isset( $arrmixValues['order_num'] ) ) $this->setOrderNum( $arrmixValues['order_num'] );
		if( isset( $arrmixValues['ap_code_id'] ) && $boolDirectSet ) $this->m_intApCodeId = trim( $arrmixValues['ap_code_id'] );
			elseif ( isset( $arrmixValues['ap_code_id'] ) ) $this->setApCodeId( $arrmixValues['ap_code_id'] );
		if( isset( $arrmixValues['account_name'] ) && $boolDirectSet ) $this->m_strAccountName = trim( stripcslashes( $arrmixValues['account_name'] ) );
			elseif ( isset( $arrmixValues['account_name'] ) ) $this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_name'] ) : $arrmixValues['account_name'] );
		if( isset( $arrmixValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrmixValues['description'] ) );
			elseif ( isset( $arrmixValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['description'] ) : $arrmixValues['description'] );
		if( isset( $arrmixValues['gl_group_name'] ) && $boolDirectSet ) $this->m_strGlGroupName = trim( stripcslashes( $arrmixValues['gl_group_name'] ) );
			elseif ( isset( $arrmixValues['gl_group_name'] ) ) $this->setGlGroupName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_group_name'] ) : $arrmixValues['gl_group_name'] );
		if( isset( $arrmixValues['account_number'] ) && $boolDirectSet ) $this->m_strAccountNumber = trim( stripcslashes( $arrmixValues['account_number'] ) );
			elseif ( isset( $arrmixValues['account_number'] ) ) $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number'] ) : $arrmixValues['account_number'] );
		if( isset( $arrmixValues['secondary_number'] ) && $boolDirectSet ) $this->m_strSecondaryNumber = trim( stripcslashes( $arrmixValues['secondary_number'] ) );
			elseif ( isset( $arrmixValues['secondary_number'] ) ) $this->setSecondaryNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['secondary_number'] ) : $arrmixValues['secondary_number'] );
		if( isset( $arrmixValues['gl_account_type_name'] ) && $boolDirectSet ) $this->m_strGlAccountTypeName = trim( stripcslashes( $arrmixValues['gl_account_type_name'] ) );
			elseif ( isset( $arrmixValues['gl_account_type_name'] ) ) $this->setGlAccountTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_account_type_name'] ) : $arrmixValues['gl_account_type_name'] );
		if( isset( $arrmixValues['gl_chart_system_code'] ) && $boolDirectSet ) $this->m_strGlChartSystemCode = trim( stripcslashes( $arrmixValues['gl_chart_system_code'] ) );
			elseif ( isset( $arrmixValues['gl_chart_system_code'] ) ) $this->setGlChartSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_chart_system_code'] ) : $arrmixValues['gl_chart_system_code'] );
		if( isset( $arrmixValues['account_number_pattern'] ) && $boolDirectSet ) $this->m_strAccountNumberPattern = trim( stripcslashes( $arrmixValues['account_number_pattern'] ) );
			elseif ( isset( $arrmixValues['account_number_pattern'] ) ) $this->setAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number_pattern'] ) : $arrmixValues['account_number_pattern'] );
		if( isset( $arrmixValues['account_number_delimiter'] ) && $boolDirectSet ) $this->m_strAccountNumberDelimiter = trim( stripcslashes( $arrmixValues['account_number_delimiter'] ) );
			elseif ( isset( $arrmixValues['account_number_delimiter'] ) ) $this->setAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number_delimiter'] ) : $arrmixValues['account_number_delimiter'] );
		if( isset( $arrmixValues['formatted_account_number'] ) && $boolDirectSet ) $this->m_strFormattedAccountNumber = trim( stripcslashes( $arrmixValues['formatted_account_number'] ) );
			elseif ( isset( $arrmixValues['formatted_account_number'] ) ) $this->setFormattedAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['formatted_account_number'] ) : $arrmixValues['formatted_account_number'] );
		if( isset( $arrmixValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrmixValues['property_id'] );
		elseif ( isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlChartId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCashFlowTypeId() {

		$boolIsValid = true;

		if( false == valStr( $this->getCashFlowTypeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cash_flow_type_id', __( 'Cash flow type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateGlAccountUsageTypeId( $objDatabase ) {

		$boolIsValid = true;

		if( false == is_numeric( $this->getGlAccountUsageTypeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_usage_type_id', __( 'GL Account Usage Type is required.' ) ) );

		} elseif( true == valObj( $objDatabase, 'CDatabase' ) ) {

			$objGlAccount = CGlAccounts::fetchCustomGlAccountByIdByCid( $this->getCid(), $this->getId(), $objDatabase );

			if( true == valObj( $objGlAccount, 'CGlAccount' ) && $this->getGlAccountUsageTypeId() == $objGlAccount->getGlAccountUsageTypeId() ) {
				return $boolIsValid;
			}

			$arrmixPropertyGlSettings = ( array ) CPropertyGlSettings::fetchDependentPropertyNamesForGlAccountUsageTypeValidationByGlAccountIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixPropertyGlSettings ) ) {

				$arrstrProperties = [];
				foreach( $arrmixPropertyGlSettings as $arrstrProperty ) {
					$arrstrProperties[] = ' \'' . $arrstrProperty['property_name'] . '\'';
				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_usage_type_id', __( 'You may not change the GL Account Usage Type, it is being used in \'Financial Settings\' of Property Settings ( Properties {%s, 0} ).', [ implode( ',', $arrstrProperties ) ] ) ) );
				$boolIsValid = false;
			}

			$intGlSettingCount = CGlSettings::fetchGlSettingsCountForGlAccountUsageTypeValidationByGlAccountIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			if( 0 < $intGlSettingCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_usage_type_id', __( 'You may not change the GL Account Usage Type, it is being used in \'Financial Settings\' of Company Settings.' ) ) );
				$boolIsValid = false;
			}

			$intPropertyBankAccountCount = CPropertyBankAccounts::fetchPropertyBankAccountsCountByGlAccountIdNotByGlAccountUsageTypeIdByCid( $this->getId(), $this->getGlAccountUsageTypeId(), $this->getCid(), $objDatabase );
			if( 0 < $intPropertyBankAccountCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_usage_type_id', __( 'You can not change GL Account Usage Type, it is being used in \'Bank Accounts\'.' ) ) );
				$boolIsValid = false;
			}

			$intFeeTemplateCount = \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchFeeTemplatesCountByGlAccountIdNotByGLAccountUsageTypeIdByCid( $this->getId(), $this->getGlAccountUsageTypeId(), $this->getCid(), $objDatabase );

			if( 0 < $intFeeTemplateCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_usage_type_id', __( 'You can not change GL Account Usage Type, it is being used in \'Management fee template(s)\'.' ) ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->validateGlAccountUsageTypeId( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_balance_sheet_insert':
			case 'validate_balance_sheet_update':
				$boolIsValid &= $this->valCashFlowTypeId();
				$boolIsValid &= $this->validateGlAccountUsageTypeId( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>