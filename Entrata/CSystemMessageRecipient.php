<?php

class CSystemMessageRecipient extends CBaseSystemMessageRecipient {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerEmailAddresses() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreferredLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>