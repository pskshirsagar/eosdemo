<?php

class CCallFrequency extends CBaseCallFrequency {

	const DAILY 	  	= 1;
	const WEEKLY	  	= 2;
	const MONTHLY  		= 3;
	const QUARTERLY 	= 4;
	const ANNUALLY 		= 5;
	const ONCE 			= 6;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>