<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportVersionChanges
 * Do not add any new functions to this class.
 */

class CReportVersionChanges extends CBaseReportVersionChanges {

	public static function fetReportVersionChangesByDefaultReportVersionIds( $arrintDefaultReportVersionIds, $objDatabase ) {

		if( false == valArr( $arrintDefaultReportVersionIds ) ) {
			return [];
		}
		$strSql = '
			SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
			SELECT 
				rvc.id,
				rvc.report_dataset_id,
				util_get_translated( \'name\', rd.name, rd.details ) AS name,
				CASE
					WHEN rvc.changes IS NULL
					THEN rvc.details ->> \'release_notes\'
					ELSE util_get_translated( \'changes\', rvc.changes, rvc.details )
				END AS release_notes,
				CASE
					WHEN rvc.release_date IS NULL
					THEN ( rvc.details ->> \'release_date\' )::date
					ELSE rvc.release_date
				END AS release_date,
				rvc.default_report_version_id,
				rvc.details ->> \'task_id\' as task_id
			FROM 
				report_version_changes rvc
				LEFT JOIN report_datasets rd ON ( rvc.report_dataset_id = rd.id AND rvc.default_report_version_id = rd.default_report_version_id )
			WHERE
				rvc.default_report_version_id IN ( ' . implode( ',', $arrintDefaultReportVersionIds ) . ' )
				AND rvc.deleted_by IS NULL
			ORDER BY
				rvc.report_dataset_id,
				rvc.release_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportVersionChangeMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				report_version_changes';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveReportVersionChangesByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		$strSql = '
			SELECT
				*
			FROM
				report_version_changes
			WHERE
				default_report_version_id = ' . ( int ) $intDefaultReportVersionId . '
				AND deleted_by IS NULL';

		return self::fetchObjects( $strSql, 'CReportVersionChange', $objDatabase );
	}

}
?>