<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTransTypes
 * Do not add any new functions to this class.
 */

class CScheduledPaymentTransTypes extends CBaseScheduledPaymentTransTypes {

	public static function fetchScheduledPaymentTransTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScheduledPaymentTransType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScheduledPaymentTransType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledPaymentTransType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllSchdeuledPaymentTransTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM scheduled_payment_trans_types';
		return self::fetchScheduledPaymentTransTypes( $strSql, $objDatabase );
	}
}
?>