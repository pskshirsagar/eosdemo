<?php

class CPetTypesOld extends CBasePetTypesOld {

	public static function fetchPetTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPetTypeOld', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPetType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPetTypeOld', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedPetTypes( $objDatabase ) {
		$strSql = 'SELECT  * FROM pet_types_old where is_published = 1';

		return self::fetchPetTypes( $strSql, $objDatabase );
	}
}
?>