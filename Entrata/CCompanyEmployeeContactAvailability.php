<?php

class CCompanyEmployeeContactAvailability extends CBaseCompanyEmployeeContactAvailability {

	protected $m_strAvailabilityTypeName;

	/**
	 * Setter Getter functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['availability_type_name'] ) ) $this->setAvailabilityTypeName( $arrmixValues['availability_type_name'] );
		return;
	}

	public function setAvailabilityTypeName( $strAvailabilityTypeName ) {
		$this->m_strAvailabilityTypeName = $strAvailabilityTypeName;
	}

	public function getAvailabilityTypeName() {
		return $this->m_strAvailabilityTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeContactId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeContactAvailabilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeekDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>