<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyStandardResponses
 * Do not add any new functions to this class.
 */

class CPropertyStandardResponses extends CBasePropertyStandardResponses {

	public static function fetchCustomPropertyStandardResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_standard_responses
					 WHERE
						id = ' . ( int ) $intId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyStandardResponse( $strSql, $objDatabase );
	}

	public static function fetchPropertyStandardResponsesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						property_standard_responses
					WHERE
						id IN (' . implode( ',', $arrintIds ) . ')
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num,
						created_on';

		return self::fetchPropertyStandardResponses( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPropertyStandardResponsesByCid( $intPageNo, $intPageSize, $intCid, $objDatabase, $intPropertyId = NULL ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = 'SELECT * FROM
						property_standard_responses
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		$strSql .= ( 0 < $intPropertyId ) ? ' AND property_id = ' . ( int ) $intPropertyId : '';
		$strSql .= ' ORDER BY
						order_num,
						created_on
					 OFFSET
						' . ( int ) $intOffset . '
					 LIMIT ' . $intLimit;

		return self::fetchPropertyStandardResponses( $strSql, $objDatabase );
	}

	public static function fetchPropertyStandardResponsesCountByCid( $intCid, $objDatabase, $intPropertyId = NULL ) {
		$strWhere = ' WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL	';

		$strWhere .= ( 0 < $intPropertyId ) ? ' AND property_id = ' . ( int ) $intPropertyId : '';

		return CPropertyStandardResponses::fetchPropertyStandardResponseCount( $strWhere, $objDatabase );
	}

	public static function fetchPropertyStandardResponsesCountByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {
		$strWhere = ' WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return CPropertyStandardResponses::fetchPropertyStandardResponseCount( $strWhere, $objClientDatabase );
	}

	public static function fetchPropertyStandardResponseHavingMaxOrderNumByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_standard_responses
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND order_num IS NOT NULL
					ORDER BY
						order_num DESC
					LIMIT 1';

		return self::fetchPropertyStandardResponse( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyStandardResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						*,
						CASE
							 WHEN
								 message ILIKE \'%<html%\' OR
								 message ILIKE \'%<body%\' OR
								 message ILIKE \'%<link%\' OR
								 message ILIKE \'%<style%\' OR
								 message ILIKE \'%<table%\' OR
								 message ILIKE \'%<div%\' OR
								 message ILIKE \'%<span%\' OR
								 message ILIKE \'%<a%\' OR
								 message ILIKE \'%<img%\' OR
								 message ILIKE \'%<ul%\' OR
								 message ILIKE \'%<dl%\' OR
								 message ILIKE \'%<form%\' OR
								 message ILIKE \'%<iframe%\' THEN 1
						 	ELSE 0
						 END as is_html_message
					FROM
						property_standard_responses
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchPropertyStandardResponses( $strSql, $objDatabase );
	}

}
?>