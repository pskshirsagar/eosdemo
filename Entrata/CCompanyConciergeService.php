<?php

class CCompanyConciergeService extends CBaseCompanyConciergeService {

	protected $m_arrobjPropertyConciergeServices;
	protected $m_arrintRequestedPropertyAssociations;
	protected $m_strCompanyMediaFileName;

	/**
	 * Get Functions
	 */

	public function getPropertyConciergeServicesOnFailure() {
		$arrobjFailedPropertyConciergeServices = [];
		if( true == valArr( $this->m_arrintRequestedPropertyAssociations ) ) {
			foreach( $this->m_arrintRequestedPropertyAssociations as $intPropertyId ) {
				$objFailedPropertyConciergeService = new CPropertyConciergeService();
				$objFailedPropertyConciergeService->setPropertyId( $intPropertyId );
				$objFailedPropertyConciergeService->setCid( $this->getCid() );
				$arrobjFailedPropertyConciergeServices[] = $objFailedPropertyConciergeService;
			}
		}
		return $arrobjFailedPropertyConciergeServices;
	}

	public function getRequestedPropertyAssociations() {
		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function getCompanyMediaFileName() {
		return $this->m_strCompanyMediaFileName;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyConciergeServices( $arrobjPropertyConciergeServices ) {
		if( true == valArr( $arrobjPropertyConciergeServices ) && false == empty( $arrobjPropertyConciergeServices ) ) {
			$this->m_arrobjPropertyConciergeServices = $arrobjPropertyConciergeServices;
		}
	}

	public function setRequestedPropertyAssociations( $arrintRequestProperty ) {
		$this->m_arrintRequestedPropertyAssociations = [];
		if( true == isset( $arrintRequestProperty ) && true == valArr( $arrintRequestProperty ) ) {
			foreach( $arrintRequestProperty as $intPropertyId ) {
				$this->m_arrintRequestedPropertyAssociations[] = $intPropertyId;
			}
		}
		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function setCompanyMediaFileName( $strCompanyMediaFileName ) {
		$this->m_strCompanyMediaFileName = $strCompanyMediaFileName;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrstrValues['company_media_file_name'] ) )	$this->setCompanyMediaFileName( $arrstrValues['company_media_file_name'] );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyConciergeServices( $objDatabase ) {

		return CPropertyConciergeServices::fetchPropertyConciergeServicesByCompanyConciergeServiceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function validateRequestedAssociations() {
		// It is required
		if( NULL == $this->m_arrintRequestedPropertyAssociations || false == valArr( $this->m_arrintRequestedPropertyAssociations ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'At least one property is required.' ) );
			$boolIsValid = false;
		}
		// It has to have at least one requested property
		if( true == empty( $this->m_arrintRequestedPropertyAssociations ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'At least one property is required.' ) );
			$boolIsValid = false;
		}
		return true;
	}

	public function valId() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_intId ) {
			trigger_error( 'Id is required - CCompanyConciergeService', E_USER_ERROR );
			return false;
		}
		// Must be greater than zero
		if( 1 > $this->m_intId ) {
			trigger_error( 'Id has to be greater than zero - CCompanyConciergeService', E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		// It is required
		if( NULL == $this->m_intCid ) {
			trigger_error( 'A Client Id is required - CCompanyConciergeService', E_USER_ERROR );
			return false;
		}
		// Must be greater than zero
		if( 1 > $this->m_intCid ) {
			trigger_error( 'A Client Id has to be greater than zero - CCompanyConciergeService', E_USER_ERROR );
			return false;
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		// It is required

		if( false == isset ( $this->m_strName ) || 1 > strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Concierge service name is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPriceAmount() {
		$boolIsValid = true;

		if( true == isset( $this->m_fltPriceAmount ) && 0 > ( float ) $this->m_fltPriceAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Price amount cannot be negative.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( false == isset( $this->m_strDescription ) ) {
		//	$boolIsValid = false;
		//	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( false == isset( $this->m_intIsPublished ) ) {
		//	$boolIsValid = false;
		//	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
		// }

		return $boolIsValid;
	}

	public function valEmailAddress() {

		$boolIsValid = true;

		if( false == isset ( $this->m_strEmailAddress ) || false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Your email address was missing or was formatted improperly.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valUser( $intCompanyUserIsAdministrator = 0 ) {
		$boolIsValid = true;

		if( 1 == $intCompanyUserIsAdministrator ) {
			return $boolIsValid;
		} elseif( true == is_null( $this->getCreatedBy() ) || true == is_null( $this->getUpdatedBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'company_concierge_service', __( 'User was not set and must be set to update or delete record' ) ) );
			$boolIsValid = false;
		} elseif( $this->getCreatedBy() != $this->getUpdatedBy() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'company_concierge_service', __( 'You do not have permissions to edit or remove this concierge service.  You can, however, edit the properties it is associated to.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intCompanyUserIsAdministrator = 0 ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// DB required fields
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPriceAmount();
				break;

			case VALIDATE_UPDATE:
				// DB required fields
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPriceAmount();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valUser( $intCompanyUserIsAdministrator );
		   		break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

 	public function addRequestedPropertyAssociation( $intPropertyId ) {
		if( true == is_numeric( $intPropertyId ) && 0 < $intPropertyId ) {
			$this->m_arrintRequestedPropertyAssociations[] = $intPropertyId;
		}
	}

	public function addPropertyConciergeService( $objPropertyConciergeService ) {
		if( true == valObj( $objPropertyConciergeService, 'CPropertyConciergeService' ) ) {
			$this->m_arrobjPropertyConciergeServices[$objPropertyConciergeService->getId()] = $objPropertyConciergeService;
		}
	}

	public function addNewPropertyConciergeServices( $objDatabase, $intCompanyUserId ) {

		if( false == valArr( $this->m_arrintRequestedPropertyAssociations ) || true == empty( $this->m_arrintRequestedPropertyAssociations ) ) {
			trigger_error( 'Invalid Company Concierge Service Request: Attempt made to add property concierge services without associations - CCompanyConciergeService::addNewPropertyConciergeServices()', E_USER_ERROR );
		}

		foreach( $this->m_arrintRequestedPropertyAssociations as $intPropertyId ) {
			$objNewPropertyConciergeService = new CPropertyConciergeService();
			$objNewPropertyConciergeService->setCompanyConciergeServiceId( $this->getId() );
			$objNewPropertyConciergeService->setPropertyId( $intPropertyId );
			$objNewPropertyConciergeService->setCid( $this->getCid() );

			if( false == $objNewPropertyConciergeService->insertOrUpdate( $objDatabase, $intCompanyUserId ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to associate all properties.' ) );
				return false;
			}
			$this->addPropertyConciergeService( $objNewPropertyConciergeService );
		}
		return true;
	}

	public function removeExistingPropertyConciergeServices( $objDatabase, $intCompanyUserId ) {
		$arrobjExistingAssociations = $this->getPropertyConciergeServices( $objDatabase );

		foreach( $arrobjExistingAssociations as $objPropertyConciergeService ) {
			if( false == $objPropertyConciergeService->delete( $objDatabase, $intCompanyUserId ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the association to property with id = ' . $objPropertyConciergeService->getId() ) );
				return false;
			}
		}
		return true;
	}

	public function fetchCompanyMediaFileByMediaType( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByCompanyConciergeServiceIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

}
?>