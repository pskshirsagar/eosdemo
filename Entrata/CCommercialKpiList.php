<?php

class CCommercialKpiList extends CBaseCommercialKpiList {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You must enter a list name.' ) ) );
		}

		if( true == valStr( $this->getName() ) ) {
			$intCommercialKpiListsCount = Psi\Eos\Entrata\CCommercialKpiLists::createService()->fetchCommercialKpiListCountExceptByIdByNameByCid( $this->getId(), $this->getCid(), $this->getName(), $objDatabase );
			if( $intCommercialKpiListsCount > 0 ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
