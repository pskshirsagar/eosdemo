<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyRoutes
 * Do not add any new functions to this class.
 */

class CPropertyRoutes extends CBasePropertyRoutes {

	public static function fetchPropertyRoutesByRouteTypeIdByRouteIdIdByCid( $intRouteTypeId, $intRouteId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						property_routes
					WHERE
						route_type_id = ' . ( int ) $intRouteTypeId . '
						AND route_id = ' . ( int ) $intRouteId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyRoutes( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyRoutesByRouteTypeIdByPropertyIdsByCid( $intRouteTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						p.id as property_id,
						p.property_name,
						r.id AS route_id,
						CASE WHEN r.id IS NOT NULL THEN 1 ELSE 0 END AS has_route
					FROM
						properties p
						LEFT JOIN property_routes pr ON ( pr.cid = p.cid AND pr.property_id = p.id AND pr.route_type_id = ' . ( int ) $intRouteTypeId . ' )
						LEFT JOIN routes r ON ( r.cid = pr.cid AND r.id = pr.route_id AND r.is_published = 1 )
						LEFT JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pgs.activate_standard_posting = true
					ORDER BY
						p.property_name, has_route';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyRoutesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						p.id as property_id,
						p.property_name,
						array_to_string( array_agg( pr.route_type_id ), \', \' ) as route_type_ids
					FROM
						properties p
						LEFT JOIN property_routes pr ON ( pr.cid = p.cid AND pr.property_id = p.id )
						LEFT JOIN routes r ON ( r.cid = pr.cid AND r.id = pr.route_id AND r.route_type_id = pr.route_type_id AND r.is_published = 1 )
						LEFT JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled = 0
						AND pgs.activate_standard_posting = true
					GROUP BY
						p.id, p.property_name
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyRoutesByRouteTypeIdByPropertyIdsByCid( $intRouteTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM
						property_routes
					WHERE
						route_type_id = ' . ( int ) $intRouteTypeId . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyRoutes( $strSql, $objDatabase );
	}

	public static function fetchPropertyRoutesByRouteIdByPropertyIdsByCid( $intRouteId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM
						property_routes
					WHERE
						route_id = ' . ( int ) $intRouteId . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyRoutes( $strSql, $objDatabase );
	}

	public static function fetchPropertyRoutesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM
						property_routes
					WHERE
						property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyRoutes( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyRoutes( int $intPropertyId, int $intCompanyUserId, int $intCid, CDatabase $objDatabase ) {

		$strSql = 'SELECT
						r.id,
						apr.cid,
						pr.property_id,
						COALESCE( r.id, 0 ) AS route_id,
						COALESCE( rc.id, 0 ) AS rule_condition_id,
						COALESCE( rr.id, 0 ) AS route_rule_id,
						COALESCE( rr.is_default_rule, 0 ) AS is_default_rule,
						COALESCE( rr.order_num, 0 ) AS order_num,
						CASE WHEN rc.id IS NULL THEN 1 ELSE 0 END AS is_failed_route,
						CASE WHEN 1 = rr.use_all_conditions THEN 1 ELSE 0 END AS use_all_conditions,
						rc.route_rule_type_id,
						rc.condition_details,
						rc.maximum_amount,
						rc.minimum_amount,
						cug.company_group_id as company_group_id
					FROM
						property_routes pr
						JOIN approval_preferences apr ON (  pr.cid = apr.cid AND pr.route_type_id = apr.route_type_id )
						JOIN routes r ON ( r.cid = pr.cid AND r.id = pr.route_id AND r.route_type_id = pr.route_type_id )
						JOIN route_rules rr ON ( rr.cid = r.cid AND rr.route_id = r.id AND rr.deleted_on IS NULL AND rr.route_type_id = r.route_type_id )
						LEFT JOIN rule_conditions rc ON ( rc.cid = rr.cid AND rc.route_id = rr.route_id AND rc.route_rule_id = rr.id AND rc.deleted_by IS NULL AND rc.deleted_on IS NULL )
						LEFT JOIN route_rule_associations rra ON ( rra.route_rule_type_id = rc.route_rule_type_id AND rra.route_type_id = rr.route_type_id )
						LEFT JOIN route_rule_types rrt ON ( rrt.id = rra.route_rule_type_id AND rrt.is_published = 1 )
						LEFT JOIN company_user_groups cug ON ( cug.cid = apr.cid AND cug.company_user_id = ' . ( int ) $intCompanyUserId . ' AND (rc.condition_details::json->>0)::text::INT = cug.company_group_id ) 
					WHERE
						pr.cid = ' . ( int ) $intCid . '
						AND pr.route_type_id = ' . ( int ) CRouteType::SCHEDULED_CHARGES . ' 
						AND apr.is_enabled
						AND pr.property_id = ' . ( int ) $intPropertyId . ' 
					ORDER BY
						order_num DESC';

		return fetchData( $strSql, $objDatabase );

	}

}
?>