<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CMarketingMediaAssociations;

class CFileBatch extends CBaseFileBatch {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_strCreatedByName;
	protected $m_strPropertyName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function getChunkFilesByFileName( $strFileName ) {
		$arrmixPathInfo = pathinfo( $strFileName );

		$strExtension = $arrmixPathInfo['extension'];
		$intFileCount = \Psi\CStringService::singleton()->preg_replace( '/[^0-9]/', '', $arrmixPathInfo['extension'] );

		$arrstrFileName = [];

		$arrstrFileName[0] = $arrmixPathInfo['filename'] . '.zip';

		for( $intCounter = 1; $intCounter <= $intFileCount; $intCounter++ ) {
			$arrstrFileName[$intCounter] = $arrmixPathInfo['filename'] . '.z' . \Psi\CStringService::singleton()->str_pad( $intCounter, \Psi\CStringService::singleton()->strlen( $strExtension ) - 1, '0', STR_PAD_LEFT );
		}

		return $arrstrFileName;
	}

	public function setSearchFilterByKey( $strKey, $arrmixValue ) {
		$arrmixSearchFilter = json_decode( $this->getSearchFilter(), true );
		$arrmixSearchFilter[$strKey] = $arrmixValue;
		$this->setSearchFilter( json_encode( $arrmixSearchFilter ) );
	}

	public function getSearchFilterByKey( $strKey ) {
		// this is fix for old legacy code which was storing data in php serialize format
		$arrmixSearchFilter = @unserialize( base64_decode( $strSerialize ) );

		if( $arrmixSearchFilter === false && $strSerialize !== 'b:0;' ) {
			$arrmixSearchFilter = json_decode( $this->getSearchFilter(), true );
		}

		if( false == valArr( $arrmixSearchFilter ) || false == array_key_exists( $strKey, $arrmixSearchFilter ) ) {
			return NULL;
		}
		return $arrmixSearchFilter[$strKey];
	}

	public function fetchCompanyLogo( $objClientDatabase ) {
		$arrobjLogoMarketingMediaAssociation	= CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCid(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE,  CMarketingMediaSubType::COMPANY_LOGO, $this->getCid(), $objClientDatabase );

		return $arrobjLogoMarketingMediaAssociation;
	}

	public function getImportDetails() {
		return $this->getDetailsField( [ 'import_details' ] );
	}

	public function setImportDetails( $arrmixImportDetails ) {
		return $this->setDetailsField( [ 'import_details' ], $arrmixImportDetails );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );
		if( true == isset( $arrmixValues['created_by_name'] ) )	$this->setCreatedByName( $arrmixValues['created_by_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )	$this->setPropertyName( $arrmixValues['property_name'] );
		return;
	}

	public function setCreatedByName( $strCreatedByName ) {
		$this->m_strCreatedByName = $strCreatedByName;
	}

	public function getCreatedByName() {
		return $this->m_strCreatedByName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getImportSource() {
		return $this->getDetailsField( [ 'import_details', 'import_source' ] );
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		$strFileName = str_replace( '.zip', '', $this->buildArchiveFileName() );
		if( true == valStr( $strReferenceTag ) && 'pdf_preview' == $strReferenceTag ) {
			$strStorageKey = $strFileName . '.pdf';
			return $this->getArchiveFilePath() . $strStorageKey;
		} elseif( true == valStr( $strReferenceTag ) && 'zip' != $strReferenceTag ) {
			$strStorageKey = $strFileName . '.z' . \Psi\CStringService::singleton()->str_pad( $strReferenceTag, strlen( $strReferenceTag ) - 1, '0', STR_PAD_LEFT );

		} else {
			$strStorageKey = $strFileName . '.zip';
		}

		$strStorageKey = rtrim( $this->getArchiveFilePath(), '/' ) . '/' . $strStorageKey;

		return $strStorageKey;

	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_FILES;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

		}

	}

	protected function getConvertedAndCleanFolderName( $strValue ) {
		$strValue = convertNonAsciiCharactersToAscii( trim( $strValue ) );
		$strValue = preg_replace( '/[^a-zA-Z0-9_\-\s]/', '', $strValue );
		$strValue = \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( $strValue ) );
		return  preg_replace( '/\s+/', '_', $strValue );
	}

	public function buildArchiveFileName() {
		return $this->getConvertedAndCleanFolderName( $this->getBatchName() ) . '.zip';
	}

	public function buildArchiveFilePath( $strExportType ) {

		$strExportPath = '';

		switch( $strExportType ) {
			case CFileExportType::ONE_TIME_EXPORT:
				$strExportPath = $this->getCid() . '/company/doc_exports/' . date( 'Y' ) . '/' . date( 'm' ) . date( 'd' ) . '/' . $this->getId() . '/';
				break;

			case CFileExportType::DOCUMENT_MANAGEMENT_EXPORT:
				$strExportPath   = $this->getCid() . '/company/batches/' . date( 'Y' ) . '/' . date( 'm' ) . date( 'd' ) . '/' . $this->getId() . '/';
				break;

			default:
				// Do nothing
				break;
		}

		return $strExportPath;
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}

?>