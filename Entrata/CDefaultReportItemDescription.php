<?php

class CDefaultReportItemDescription extends CBaseDefaultReportItemDescription {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportItemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemKey( $strAction, $objDatabase ) {
		$boolIsValid = true;
		$strSql  = ' WHERE default_cid = 1 AND report_item_type_id = ' . ( int ) $this->getReportItemTypeId() . ' AND item_key = \'' . $this->getItemKey() . '\'  AND deleted_by IS NULL AND deleted_on IS NULL';

		if( VALIDATE_UPDATE == $strAction ) {
			$strSql .= ' AND id != ' . $this->getId();
		}

		if( 0 < \Psi\Eos\Entrata\CDefaultReportItemDescriptions::createService()->fetchDefaultReportItemDescriptionCount( $strSql, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key already exists.' ) );
		}

		if( false == isset( $this->m_strItemKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
		}
		return $boolIsValid;
	}

	public function valItemName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strItemName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valItemDescription() {
		$boolIsValid = true;

		if( false == isset( $this->m_strItemDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valItemName();
				$boolIsValid &= $this->valItemKey( $strAction, $objDatabase );
				$boolIsValid &= $this->valItemDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>