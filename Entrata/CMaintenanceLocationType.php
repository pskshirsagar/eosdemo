<?php

class CMaintenanceLocationType extends CBaseMaintenanceLocationType {

	const PROPERTY 	    = 1;
	const UNIT 		    = 2;
	const UNIT_SPACE    = 3;    // This is not a table field, used in scheduled tasks as constant only. Task #919120.

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function getMaintenanceLocationTypeNameById( $intId ) {

    	$strTemplateTypeName = NULL;

    	switch( $intId ) {

    		case CMaintenanceLocationType::PROPERTY:
    			$strTemplateTypeName = __( 'PROPERTY' );
    			break;

    		case CMaintenanceLocationType::UNIT:
    			$strTemplateTypeName = __( 'UNIT' );
    			break;

    		default:
    			// default case
    			break;
    	}

    	return $strTemplateTypeName;
    }

	public static $c_arrstrMaintenanceLocationTypes = [
		self::UNIT => 'Unit',
		self::UNIT_SPACE => 'Unit Space',
		self::PROPERTY => 'Property Location'
	];

}
?>