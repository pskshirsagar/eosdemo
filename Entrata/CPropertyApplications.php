<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplications
 * Do not add any new functions to this class.
 */

class CPropertyApplications extends CBasePropertyApplications {

	public static function fetchPropertyApplicationsByCompanyApplicationIdByPropertyIdsByCid( $intCompanyApplicationId, $arrintPropertyIds, $intCid, $objDatabase ) {

	 	$strSql = 'SELECT * FROM property_applications WHERE company_application_id = ' . ( int ) $intCompanyApplicationId . '  AND cid = ' . ( int ) $intCid . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

	 	return self::fetchPropertyApplications( $strSql, $objDatabase );
    }

    public static function fetchCustomPropertyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
    					pa.*, ' .
	              ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ?
		              'util_get_translated( \'title\',ca.title, ca.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' )' : 'ca.title' ) . ' AS title
    				 FROM
    				 	property_applications pa
    				 	JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
    		        WHERE
						pa.property_id = ' . ( int ) $intPropertyId . '
						AND pa.cid = ' . ( int ) $intCid . '
    				 	AND ca.deleted_on IS NULL
    				 	AND ca.is_published = 1
    			  	ORDER BY
    			  		pa.order_num';

        return self::fetchPropertyApplications( $strSql, $objDatabase );
    }

	public static function fetchAllPropertyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intOccupancyTypeId = NULL ) {
		$strWhere = '';
		if( false == is_null( $intOccupancyTypeId ) ) {
			$strWhere = ( COccupancyType::MILITARY == $intOccupancyTypeId ) ? ' AND ca.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : ' AND ca.occupancy_type_id <> ' . COccupancyType::MILITARY;
		}

    	$strSql = 'SELECT
    						pa.*,
    						util_get_translated( \'title\', ca.title, ca.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as title,
    						ca.occupancy_type_id,
    						ca.is_published as web_visible
    				 FROM
    				 		property_applications pa

    				 JOIN 	company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
    		        WHERE
    				 		pa.property_id = ' . ( int ) $intPropertyId . $strWhere . '
    				 		AND pa.cid = ' . ( int ) $intCid . '
    				 		AND ca.deleted_on IS NULL
    			  ORDER BY
    			  			pa.order_num';

        return self::fetchPropertyApplications( $strSql, $objDatabase );
    }

	public static function fetchPropertyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valStr( $intCid ) || false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
						pa.*,
    					ca.title,
    					ca.occupancy_type_id,
    					ca.is_published as web_visible
					FROM
    					property_applications pa
						JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
    		        WHERE
    					pa.cid = ' . ( int ) $intCid . '
    					AND pa.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
    				 	AND ca.deleted_on IS NULL
					ORDER BY
						pa.property_id, pa.order_num';

        return self::fetchPropertyApplications( $strSql, $objDatabase );
    }

	public static function fetchPropertyApplicationsCountByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {

		$strSql = 'SELECT COUNT (ca.id), p.id as property_id
					FROM properties AS p
						INNER JOIN website_properties AS wp ON ( p.id = wp.property_id AND p.cid = wp.cid )
						LEFT OUTER JOIN property_applications AS pa ON ( pa.property_id = p.id AND pa.cid = p.cid )
						INNER JOIN company_applications AS ca ON ( ca.id = pa.company_application_id AND ca.cid = pa.cid AND ca.is_published = 1 AND ca.deleted_on IS NULL )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.cid = ' . ( int ) $intCid . '
						AND wp.hide_on_prospect_portal != 1 AND p.is_disabled <> 1
					GROUP BY p.id';

		 $arrmixPropertyApplications = fetchData( $strSql, $objDatabase );

		 if( true == valArr( $arrmixPropertyApplications ) ) {
		 	$arrmixTempPropertyApplications = [];

		 	foreach( $arrmixPropertyApplications as $arrmixPropertyApplication ) {
		 		$intPropertyId = $arrmixPropertyApplication['property_id'];
		 		$arrmixTempPropertyApplications[$intPropertyId] = $arrmixPropertyApplication['count'];
		 	}

		 	$arrmixPropertyApplications = $arrmixTempPropertyApplications;

		 	return $arrmixPropertyApplications;
		 } else {
			return NULL;
		 }
	}

	public static function fetchPropertyApplicationWithOccupancyTypeIdByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ca.*,pa.*
					FROM
						property_applications pa
						JOIN company_applications ca ON ( ca.cid = pa.cid AND ca.id = pa.company_application_id )
					WHERE
						pa.id = ' . ( int ) $intId . '
						AND pa.cid = ' . ( int ) $intCid;

		return self::fetchPropertyApplication( $strSql, $objDatabase );
	}

    public static function fetchCustomPropertyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

        if( false == valStr( $intCid ) || false == valArr( $arrintPropertyIds ) ) return NULL;

        $strSql = 'SELECT
						pa.id,
						pa.property_id,
						pa.company_application_id as online_application,
    					ca.title as name
					FROM
    					property_applications pa
						JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
    		        WHERE
    					pa.cid = ' . ( int ) $intCid . '
    					AND pa.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
    				 	AND ca.deleted_on IS NULL
					ORDER BY
						pa.property_id, pa.order_num';

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchPropertyApplicationsAndPreferencesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolCheckKeys = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == $boolCheckKeys ) {
			$strCaseCondition = '( CASE WHEN
									pap.property_id IS NOT NULL
									OR( pap1.property_id IS NOT NULL
									AND pap2.property_id IS NOT NULL
									AND pap3.property_id IS NOT NULL
									AND pap4.property_id IS NOT NULL)
								 THEN ca.title ELSE NULL END ) AS key ';

			$strJoinCondition = 'LEFT JOIN property_application_preferences pap ON( pa.cid = pap.cid AND pap.key=\'HIDE_ADDITIONAL_INFO_PERSONAL_INFO\' AND pa.property_id = pap.property_id AND pa.company_application_id = pap.company_application_id )
								 LEFT JOIN property_application_preferences pap1 ON( pa.cid = pap1.cid AND pap1.key=\'HIDE_ADDITIONAL_INFO_PERSONAL_INFO_EYE_COLOR\' AND pa.property_id = pap1.property_id AND pa.company_application_id = pap1.company_application_id )
								 LEFT JOIN property_application_preferences pap2 ON( pa.cid = pap2.cid AND pap2.key=\'HIDE_ADDITIONAL_INFO_PERSONAL_INFO_HEIGHT\' AND pa.property_id = pap2.property_id AND pa.company_application_id = pap2.company_application_id )
								 LEFT JOIN property_application_preferences pap3 ON( pa.cid = pap3.cid AND pap3.key=\'HIDE_ADDITIONAL_INFO_PERSONAL_INFO_MARITAL_STATUS\' AND pa.property_id = pap3.property_id AND pa.company_application_id = pap3.company_application_id )
								 LEFT JOIN property_application_preferences pap4 ON( pa.cid = pap4.cid AND pap4.key=\'HIDE_ADDITIONAL_INFO_PERSONAL_INFO_WEIGHT\' AND pa.property_id = pap4.property_id AND pa.company_application_id = pap4.company_application_id )';
		} else {
			$strCaseCondition = '( CASE WHEN pap.property_id IS NOT NULL THEN ca.title ELSE NULL END ) AS key';
			$strJoinCondition = 'LEFT JOIN property_application_preferences pap ON( pa.cid = pap.cid AND pa.property_id = pap.property_id AND pa.company_application_id = pap.company_application_id )';
		}

		$strSql = 'SELECT
						DISTINCT pa.id,pa.property_id,ca.title AS name,
						' . $strCaseCondition . '
					FROM
						property_applications pa
						LEFT JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
						' . $strJoinCondition . '
					WHERE
						pa.cid = ' . ( int ) $intCid . '
						AND pa.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' ) AND ca.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationsAndPreferencesByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRequiredKeys = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		if( true == $boolIsRequiredKeys ) {
			$strCaseCondition = '( CASE WHEN ( pap1.property_id IS NULL AND pap2.property_id IS NOT NULL ) OR ( pap1.property_id IS NOT NULL AND pap2.property_id IS NULL ) THEN ca.title ELSE NULL END ) AS key';
			$strJoinCondition = 'LEFT JOIN property_application_preferences pap1
								ON ( pa.cid = pap1.cid AND pap1.key = \'' . $arrstrKeys[0] . '\' AND pa.property_id = pap1.property_id AND pa.company_application_id = pap1.company_application_id )
								LEFT JOIN property_application_preferences pap2
								ON ( pa.cid = pap2.cid AND pap2.key = \'' . $arrstrKeys[1] . '\' AND pa.property_id = pap2.property_id AND pa.company_application_id = pap2.company_application_id )';
		} else {
			$strJoinCondition = '';
			$strCaseCondition = '( CASE WHEN pap.property_id IS NOT NULL THEN 1 ELSE NULL END ) AS key';
		}

		$strSql = 'SELECT
						DISTINCT pa.id,
						pa.property_id,
						p.property_id AS parent,
						pa.company_application_id,
						ca.title AS name,
						ca.occupancy_type_id,
						' . $strCaseCondition . '
					FROM
						property_applications pa
						LEFT JOIN properties p ON (pa.property_id = p.id AND pa.cid = p.cid)
						LEFT JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
						' . $strJoinCondition . '
						LEFT JOIN property_application_preferences pap ON( pa.cid = pap.cid AND  pap.key IN (\'' . implode( "','", $arrstrKeys ) . '\') AND pa.property_id = pap.property_id AND pa.company_application_id = pap.company_application_id )
					WHERE
						pa.cid = ' . ( int ) $intCid . '
						AND pa.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ca.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPropertyIdsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						property_id 
					FROM 
						property_applications 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND company_application_id = ' . ( int ) $intCompanyApplicationId;

		$arrintPropertyApplicationPropertyIds = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintPropertyApplicationPropertyIds ) ) {
			$arrintTempPropertyApplications = [];

			foreach( $arrintPropertyApplicationPropertyIds as $arrintPropertyApplicationPropertyId ) {
				$arrintTempPropertyApplications[$arrintPropertyApplicationPropertyId['property_id']] = $arrintPropertyApplicationPropertyId['property_id'];
			}

			$arrintPropertyApplicationPropertyIds = $arrintTempPropertyApplications;

			return $arrintPropertyApplicationPropertyIds;
		} else {
			return NULL;
		}
	}

	public static function fetchPropertyApplicationByOccupancyTypeIdByPropertyIdByCid( $intOccupancyId, $intPropertyId, $intCid, $objDatabase, $boolIsRequiredRelationships = false ) {

		$strWhere = '';
		if( true == $boolIsRequiredRelationships ) {
			$strWhere = ' AND pa.customer_relationships IS NOT NULL';
		}

		$strSql = 'SELECT
						pa.*,
						ca.title
					FROM
						property_applications pa
						JOIN company_applications ca ON ( ca.cid = pa.cid AND ca.id = pa.company_application_id )
					WHERE
						pa.cid = ' . ( int ) $intCid . '
						AND pa.property_id = ' . ( int ) $intPropertyId . '
						AND ca.occupancy_type_id = ' . ( int ) $intOccupancyId . '
						AND ca.deleted_on IS NULL' . $strWhere;

		return self::fetchPropertyApplications( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationByCompanyApplicationIdsByPropertyIdByCid( $arrintCompnayApplicationIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompnayApplicationIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						property_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND company_application_id IN ( ' . implode( ',', $arrintCompnayApplicationIds ) . ' )';

		return self::fetchPropertyApplications( $strSql, $objDatabase );

	}

}
?>