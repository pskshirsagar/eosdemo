<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMenuIcons
 * Do not add any new functions to this class.
 */

class CMenuIcons extends CBaseMenuIcons {

	public static function fetchMenuIcons( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMenuIcon', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchMenuIcon( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMenuIcon', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>