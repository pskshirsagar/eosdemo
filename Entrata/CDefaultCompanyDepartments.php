<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCompanyDepartments
 * Do not add any new functions to this class.
 */

class CDefaultCompanyDepartments extends CBaseDefaultCompanyDepartments {

	public static function fetchDefaultCompanyDepartments( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultCompanyDepartment::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultCompanyDepartment( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultCompanyDepartment::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>