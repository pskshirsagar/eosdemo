<?php

class CImportGroup extends CBaseImportGroup {

	const FINANCIAL 			= 1;
	const PROPERTY 				= 2;
	const REVIEW 				= 3;
	const SUMMARY 				= 4;

	public static $c_arrstrImportGroups = [
		self::FINANCIAL		=> 'financial',
		self::PROPERTY		=> 'property',
		self::REVIEW		=> 'review',
		self::SUMMARY		=> 'summary'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>