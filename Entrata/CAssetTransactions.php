<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTransactions
 * Do not add any new functions to this class.
 */

class CAssetTransactions extends CBaseAssetTransactions {

	public static function fetchActiveAssetTransactionsByApDetailIdByAssetTransactionTypeIdsByCid( $intPoApDetalid, $arrintAssetTransactionTypeIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						at.*,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
					FROM
						asset_transactions at
						JOIN company_users cu ON ( at.cid = cu.cid AND cu.id = at.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND cu.company_employee_id = ce.id )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.po_ap_detail_id = ' . ( int ) $intPoApDetalid . '
						AND at.asset_transaction_type_id IN ( ' . implode( ', ', $arrintAssetTransactionTypeIds ) . ' )
						AND at.asset_transaction_type_id != ' . CAssetTransactionType::CONSUME_INVENTORY . '
					ORDER BY at.id ASC';

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchActiveAssetTransactionsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						at.id,
						at.display_quantity AS quantity
					FROM
						asset_transactions at
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchAssetTransactionsByAssetIdByCid( $intAssetId, $intCid, $objClientDatabase, $arrintAssetTransactionTypeIds = [], $boolIsFromMaintenanceMaterial = false ) {

		if( false == valId( $intAssetId ) ) return NULL;

		$strCondition		= NULL;
		$arrstrConditions	= [];

		if( true == valArr( $arrintAssetTransactionTypeIds ) ) {
			$arrstrConditions[] = 'at.asset_transaction_type_id IN ( ' . sqlIntImplode( $arrintAssetTransactionTypeIds ) . ' )';
		}

		if( true == valArr( $arrstrConditions ) ) {
			$strCondition = ' AND ' . implode( ' AND ', $arrstrConditions );
		}

		$strSql = 'SELECT
						at.*,
						ah.id AS ap_header_id,
						ah.header_number,
						uom.name AS unit_of_measure_name,
						att.name AS transaction_type,
						mr.id AS maintenance_request_id,
						' . self::getSelectSqlConsumptionUnitCost() . ' AS consumption_unit_cost
					FROM
						asset_transactions at
						JOIN asset_transaction_types att ON ( at.asset_transaction_type_id = att.id )
						JOIN unit_of_measures uom ON ( uom.cid = at.cid AND uom.id = at.unit_of_measure_id )
						LEFT JOIN maintenance_request_materials mrm ON ( mrm.cid = at.cid AND mrm.id = at.maintenance_request_material_id )
						LEFT JOIN maintenance_requests mr ON ( mr.cid = mrm.cid AND mr.id = mrm.maintenance_request_id AND mr.deleted_on IS NULL )
						LEFT JOIN ap_details ad ON ( ad.cid = at.cid AND ad.ap_code_id = at.ap_code_id AND ad.id = at.po_ap_detail_id )
						LEFT JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.deleted_by IS NULL AND ah.deleted_on IS NULL )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_id = ' . ( int ) $intAssetId . '
						' . $strCondition . '
					ORDER BY
						at.post_date DESC';

		if( true == $boolIsFromMaintenanceMaterial ) {
			$strSql .= ' LIMIT 1';
			return self::fetchAssetTransaction( $strSql, $objClientDatabase );
		} else {
			return self::fetchAssetTransactions( $strSql, $objClientDatabase );
		}
	}

	public static function fetchNonConsumedAssetTransactionsByApCodeIdByAssetLocationIdByPropertyIdByCid( $intApCodeId, $intAssetLocationId, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApCodeId ) || false == valId( $intAssetLocationId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						at.*,
						ac.unit_of_measure_id AS ap_codes_unit_of_measure_id,
						' . self::getSelectSqlConsumptionUnitCost() . ' AS consumption_unit_cost
					FROM
						asset_transactions at
						JOIN ap_codes ac ON ( ac.cid = at.cid AND ac.id = at.ap_code_id )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_transaction_type_id <> ' . CAssetTransactionType::CONSUME_INVENTORY . '
						AND at.ap_code_id = ' . ( int ) $intApCodeId . '
						AND at.asset_location_id = ' . ( int ) $intAssetLocationId . '
						AND at.property_id = ' . ( int ) $intPropertyId . '
						ORDER BY at.id ASC';

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchNonConsumedAssetTransactionsByApCodeIdsByAssetLocationIdsByPropertyIdByCid( $arrintApCodeIds, $arrintAssetLocationIds, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApCodeIds ) || false == valArr( $arrintAssetLocationIds ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						at.*,
						ac.unit_of_measure_id AS ap_codes_unit_of_measure_id,
						' . self::getSelectSqlConsumptionUnitCost() . ' AS consumption_unit_cost
					FROM
						asset_transactions at
						JOIN ap_codes ac ON ( ac.cid = at.cid AND ac.id = at.ap_code_id )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_transaction_type_id <> ' . CAssetTransactionType::CONSUME_INVENTORY . '
						AND at.ap_code_id IN ( ' . implode( ', ', $arrintApCodeIds ) . ' )
						AND at.asset_location_id IN ( ' . implode( ', ', $arrintAssetLocationIds ) . ' )
						AND at.property_id = ' . ( int ) $intPropertyId;

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchAssetTransactionsByMaintenanceRequestMaterialIdByCid( $intMaintenanceRequestMaterialId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_transactions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND asset_transaction_id IS NULL
						AND maintenance_request_material_id = ' . ( int ) $intMaintenanceRequestMaterialId;

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchAssetTransactionsWithHistoryByAssetIdByCid( $intAssetId, $intCid, $objClientDatabase ) {

		if( false == valId( $intAssetId ) ) return NULL;

		$strSql = 'WITH company_user_names AS (

						SELECT
							cu.id,
							ce.name_first,
							ce.name_last
						FROM
							company_users cu
							LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						WHERE
							cu.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
							AND deleted_on IS NULL
							AND is_disabled = 0
					)

					SELECT
						\'' . CAssetTransaction::PURCHASED . '\' AS action,
						at.created_on AS action_date,
						NULL AS maintenance_location_name,
						NULL AS unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						NULL AS maintenance_request_id,
						NULL AS event_id,
						1 AS order_num
					FROM
						asset_transactions at
						JOIN company_user_names cun ON ( cun.id = at.created_by )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_id = ' . ( int ) $intAssetId . '
						AND at.asset_transaction_type_id IN ( ' . CAssetTransactionType::MANUAL_ADJUSTMENT . ', ' . CAssetTransactionType::RECEIVE_INVENTORY . ' )

					UNION

					SELECT
						CASE
							WHEN e.event_type_id = \'' . CEventType::ASSET_EDITED . '\' THEN \'' . CAssetTransaction::ASSET_EDITED . '\'
							ELSE \'' . CAssetTransaction::PLACED_IN_SERVICE . '\'
						END AS action,
						e.created_on AS action_date,
						e.event_handle AS maintenance_location_name,
						ad.unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						NULL AS maintenance_request_id,
						e.id AS event_id,
						2 AS order_num
					FROM
						asset_details ad
						JOIN event_references er ON ( ad.cid = er.cid AND ad.asset_id = er.reference_id AND er.event_reference_type_id = ' . CEventReferenceType::FIXED_ASSETS . '  )
						JOIN events e ON ( er.cid = e.cid AND er.event_id = e.id AND e.event_type_id IN ( ' . CEventType::ASSET_PLACED_IN_SERVICE . ',' . CEventType::ASSET_EDITED . ' )  )
						JOIN company_user_names cun ON ( cun.id = e.created_by )
						LEFT JOIN unit_spaces us ON ( ad.cid = us.cid AND ad.unit_space_id = us.id )
						LEFT JOIN maintenance_locations ml ON ( ad.cid = ml.cid AND ad.maintenance_location_id = ml.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.asset_id = ' . ( int ) $intAssetId . '

					UNION

					SELECT
						CASE
							WHEN at_offset.id IS NULL OR ( at_offset.id IS NOT NULL AND at.id < at_offset.id ) THEN \'' . CAssetTransaction::RETIRED . '\'
							ELSE \'' . CAssetTransaction::UNRETIRED . '\'
						END AS action,
						at.created_on AS action_date,
						NULL AS maintenance_location_name,
						NULL AS unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						NULL AS maintenance_request_id,
						NULL AS event_id,
						3 AS order_num
					FROM
						asset_transactions at
						LEFT JOIN asset_transactions at_offset ON ( at.cid = at_offset.cid AND at.id = at_offset.asset_transaction_id )
						JOIN company_user_names cun ON ( cun.id = at.created_by )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_transaction_type_id = ' . CAssetTransactionType::ASSET_RETIRE . '
						AND at.asset_id = ' . ( int ) $intAssetId . '

					UNION
					
					SELECT
						\'' . CAssetTransaction::RETURNED . '\' AS action,
						at.created_on AS action_date,
						NULL AS maintenance_location_name,
						NULL AS unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						NULL AS maintenance_request_id,
						NULL AS event_id,
						3 AS order_num
					FROM
						asset_transactions at
						LEFT JOIN asset_transactions at_offset ON ( at.cid = at_offset.cid AND at.id = at_offset.asset_transaction_id )
						JOIN company_user_names cun ON ( cun.id = at.created_by )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_transaction_type_id = ' . CAssetTransactionType::RETURN_INVENTORY . '
						AND at.asset_id = ' . ( int ) $intAssetId . '

					UNION

					SELECT
						\'' . CAssetTransaction::TRANSFERRED . '\' AS action,
						e.created_on AS action_date,
						e.event_handle AS maintenance_location_name,
						ad.unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						NULL AS maintenance_request_id,
						e.id AS event_id,
						3 AS order_num
					FROM
						asset_details ad
						JOIN event_references er ON ( ad.cid = er.cid AND ad.asset_id = er.reference_id AND er.event_reference_type_id = ' . CEventReferenceType::FIXED_ASSETS . '  )
						JOIN events e ON ( er.cid = e.cid AND er.event_id = e.id AND e.event_type_id = ' . CEventType::ASSET_TRANSFER . '  )
						JOIN company_user_names cun ON ( cun.id = e.created_by )
						LEFT JOIN unit_spaces us ON ( ad.cid = us.cid AND ad.unit_space_id = us.id )
						LEFT JOIN maintenance_locations ml ON ( ad.cid = ml.cid AND ad.maintenance_location_id = ml.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.asset_id = ' . ( int ) $intAssetId . '

					UNION

					( SELECT
						\'' . CAssetTransaction::WORK_ORDER . '\' AS action,
						e.updated_on AS action_date,
						NULL AS maintenance_location_name,
						ad.unit_space_id,
						CASE
							WHEN cun.name_first IS NOT NULL OR cun.name_last IS NOT NULL THEN ARRAY_TO_STRING ( ARRAY [ cun.name_first, cun.name_last ], \' \' )
							ELSE \'PSI User\'
						END AS action_user_name,
						e.data_reference_id::text AS maintenance_request_id,
						NULL AS event_id,
						3 AS order_num
					FROM
						asset_details ad
						JOIN event_references er ON ( ad.cid = er.cid AND ad.asset_id = er.reference_id AND er.event_reference_type_id = ' . CEventReferenceType::WORK_ORDER . ' )
						JOIN events e ON ( er.cid = e.cid AND er.event_id = e.id AND e.event_type_id = ' . CEventType::WORK_ORDER_UPDATED . ' AND e.event_sub_type_id = ' . CEventSubType::WORKORDER_ASSET_ASSOCIATED . ' )
						JOIN company_user_names cun ON ( cun.id = e.created_by )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.asset_id = ' . ( int ) $intAssetId . ' ORDER BY e.updated_on DESC )
					ORDER BY
						order_num DESC,
						action_date DESC';

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function fetchCountOfReceivedQuantityByPoApDetailIdsByCid( $arrintPoApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPoApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						po_ap_detail_id,
						SUM( display_quantity ) AS display_quantity
					FROM
						asset_transactions
					WHERE
						cid  = ' . ( int ) $intCid . '
						AND invoice_ap_detail_id IS NULL
						AND po_ap_detail_id IN ( ' . implode( ',', $arrintPoApDetailIds ) . ' )
					GROUP BY
						po_ap_detail_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchNonConsumedAssetTransactionsByAssetIdsByCid( $arrintAssetIds, $intCid, $objClientDatabase ) {
		if( false == ( $arrintAssetIds = getIntValuesFromArr( $arrintAssetIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						at.*
					FROM
						asset_transactions at
						JOIN ap_codes ac ON ( ac.cid = at.cid AND ac.id = at.ap_code_id )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.asset_id IN ( ' . sqlIntImplode( $arrintAssetIds ) . ' )
						AND at.cached_remaining > 0';

		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

	public static function getSelectSqlConsumptionUnitCost() {
		return 'at.unit_cost / at.quantity * at.display_quantity';
	}

	public static function fetchActiveAssetTransactionsByApDetailIdsByAssetTransactionTypeIdsByCid( $arrintPoApDetailIds, $arrintAssetTransactionTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPoApDetailIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						at.*,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name,
						al.name as asset_location_name
					FROM
						asset_transactions at
						JOIN company_users cu ON ( at.cid = cu.cid AND cu.id = at.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND cu.company_employee_id = ce.id )
						LEFT JOIN asset_locations al ON (al.cid = at.cid AND at.asset_location_id = al.id)
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.po_ap_detail_id IN ( ' . implode( ',', $arrintPoApDetailIds ) . ' )
						AND at.asset_transaction_type_id IN ( ' . implode( ', ', $arrintAssetTransactionTypeIds ) . ' )
					ORDER BY at.id ASC';
		return self::fetchAssetTransactions( $strSql, $objClientDatabase );
	}

}
?>