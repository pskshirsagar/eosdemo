<?php

class CTransmissionCondition extends CBaseTransmissionCondition {

	protected $m_strTransmissionConditionName;
	protected $m_boolIsInSemKeyword;
	protected $m_boolIsFeatured;

	protected $m_objProperty;

	/**
	 * Get Functions
	 */

	public function getTransmissionConditionName() {
		return $this->m_strTransmissionConditionName;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	/**
	 * Set Functions
	 */

	public function setTransmissionConditionName( $strTransmissionConditionName ) {
		$this->m_strTransmissionConditionName = $strTransmissionConditionName;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	/**
	 * Val Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionConditionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantApplicationTransmissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGuarantorApplicantApplicationTransmissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGuarantorApplicantApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepositArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentApplicationRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSatisfiedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSatisfiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// added default case
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrValues['transmission_condition_name'] ) && $boolDirectSet ) $this->m_strTransmissionConditionName = trim( $arrValues['transmission_condition_name'] );
		elseif ( isset( $arrValues['transmission_condition_name'] ) ) $this->setTransmissionConditionName( $arrValues['transmission_condition_name'] );

		return;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchApplication( $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function loadTransmisssionConditionName( $arrobjPropertyPreferences ) {

		$strTransmissionConditionName = CTransmissionConditionType::transmissionConditionTypeIdToStr( $this->getTransmissionConditionTypeId() );

		if( CTransmissionConditionType::INCREASE_DEPOSIT == $this->getTransmissionConditionTypeId() ) {

			if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY'] ) && 0 < $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue() ) {
				$strTransmissionConditionName .= ' by ';

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
					$strTransmissionConditionName .= '$';
				}

				$strTransmissionConditionName .= number_format( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue(), 2 );

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
					$strTransmissionConditionName .= '%';
				}

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE'] ) && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
					$strTransmissionConditionName .= '% of rent';
				}
			}
		}

		// This is a temporary fix as per task id# 204687
		if( CTransmissionConditionType::INCREASE_DEPOSIT_2 == $this->getTransmissionConditionTypeId() ) {

			if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2'] ) && 0 < $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue() ) {
				$strTransmissionConditionName .= ' by ';

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
					$strTransmissionConditionName .= '$';
				}

				$strTransmissionConditionName .= number_format( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue(), 2 );

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
					$strTransmissionConditionName .= '%';
				}

				if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
					$strTransmissionConditionName .= '% of rent';
				}
			}
		}

		if( CTransmissionConditionType::INCREASE_RENT == $this->getTransmissionConditionTypeId() ) {

			if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_BY'] ) && 0 < $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue() ) {
				$strTransmissionConditionName .= ' by ';

				if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE']->getValue() ) {
					$strTransmissionConditionName .= '$';
				}

				$strTransmissionConditionName .= number_format( $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue(), 2 );

				if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE']->getValue() ) {
					$strTransmissionConditionName .= '%';
				}
			}
		}

		// This is a temporary fix as per task id# 204687
		if( CTransmissionConditionType::INCREASE_RENT_2 == $this->getTransmissionConditionTypeId() ) {

			if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_BY_2'] ) && 0 < $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue() ) {
				$strTransmissionConditionName .= ' by ';

				if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2']->getValue() ) {
					$strTransmissionConditionName .= '$';
				}

				$strTransmissionConditionName .= number_format( $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue(), 2 );

				if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2']->getValue() ) {
					$strTransmissionConditionName .= '%';
				}
			}
		}

		if( CTransmissionConditionType::CUSTOM_SCREENING_CONDITION == $this->getTransmissionConditionTypeId() ) {
			$strTransmissionConditionName = $arrobjPropertyPreferences['CUSTOM_SCREENING_CONDITION_2']->getValue();
		}

		$this->setTransmissionConditionName( $strTransmissionConditionName );

		return $strTransmissionConditionName;
	}

	public function processTransmissionCondition( $arrobjPropertyPreferences, $intCompanyUserId, $objDatabase, $boolIsScheduledChargeProcessed = false, $boolIsTransmissionFailed = false ) {

		if( false == valObj( $this->m_objProperty, 'CProperty' ) ) {
			trigger_error( 'Failed to load property object.', E_USER_ERROR );
		}

		switch( $this->getTransmissionConditionTypeId() ) {

			case CTransmissionConditionType::INCREASE_DEPOSIT:
				$this->setConditionDatetime( 'NOW()' );
				$this->setSatisfiedBy( $intCompanyUserId );
				$this->setSatisfiedOn( 'NOW()' );

				if( true == $boolIsScheduledChargeProcessed )
				break;

				$objRentArCode 	= $this->m_objProperty->getOrFetchRentArCode( $objDatabase );
				$objApplication = $this->fetchApplication( $objDatabase );

				$intRentChargeCodeId 	= ( true == valObj( $objRentArCode, 'CArCode' ) ) ? $objRentArCode->getId() : NULL;
				$intChargeCodeId 		= ( true == isset( $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT'] ) && 0 < $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT']->getValue() ) ? $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT']->getValue() : NULL;
				$objArCode 				= ( 0 < ( int ) $intChargeCodeId ) ? \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $intChargeCodeId, $this->getCid(), $objDatabase ) : NULL;

				$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), CArTrigger::$c_arrintProbableSecDepArTriggerIdsForRV, $this->getCid(), $objDatabase );

				$objDepositScheduledCharge 	= NULL;
				$fltTotalMoveinFee 			= 0;
				$boolIsCurrentDeposit 		= false;

				foreach( $arrobjScheduledCharges as $objScheduledCharge ) {

					if( 0 < $intRentChargeCodeId && $intRentChargeCodeId == $objScheduledCharge->getArCodeId() && CArTrigger::MONTHLY == $objScheduledCharge->getArTriggerId() && true == array_key_exists( 'INCREASE_SECURITY_DEPOSIT_TYPE', $arrobjPropertyPreferences ) && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
						$objDepositScheduledCharge = $objScheduledCharge;
					}

					if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() && true == in_array( $objScheduledCharge->getArTriggerId(), CArTrigger::$c_arrintProbableSecDepArTriggerIdsForRV ) && 'PERCENTAGE_OF_RENT' != $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
						if( CArTrigger::MOVE_IN == $objScheduledCharge->getArTriggerId() ) {
							$arrintScheduledChargeMaxAmountTypeMoveIn[ceil( $objScheduledCharge->getChargeAmount() )] = $objScheduledCharge;
							$objDepositScheduledCharge = $arrintScheduledChargeMaxAmountTypeMoveIn[max( array_keys( $arrintScheduledChargeMaxAmountTypeMoveIn ) )];
							break;

						} elseif( CArTrigger::APPLICATION_COMPLETED == $objScheduledCharge->getArTriggerId() ) {
							$arrintScheduledChargeMaxAmountTypePrimaryApp[ceil( $objScheduledCharge->getChargeAmount() )] = $objScheduledCharge;
							$objDepositScheduledCharge = $arrintScheduledChargeMaxAmountTypePrimaryApp[max( array_keys( $arrintScheduledChargeMaxAmountTypePrimaryApp ) )];
						}
					}

					$fltTotalMoveinFee += $objScheduledCharge->getChargeAmount();
				}

				if( false == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) && true == valArr( $arrobjScheduledCharges ) ) {
					$boolIsCurrentDeposit = true;
					$objDepositScheduledCharge = current( $arrobjScheduledCharges );

					if( false == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) ) {
						$objDepositScheduledCharge = array_pop( array_reverse( $arrobjScheduledCharges ) );
					}
				}

				if( true == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY'] ) && 0 < $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue() ) {

					$fltIncreasedDepositAmount = 0;

					if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) {
						$fltIncreasedDepositAmount = ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue();
					}

					if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE'] ) && ( 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() || 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) ) {

						if( false == $boolIsCurrentDeposit ) {
							$fltIncreasedDepositAmount = ( $objDepositScheduledCharge->getChargeAmount() * ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue() ) / 100;
						} else {
							$fltIncreasedDepositAmount = ( $fltTotalMoveinFee * ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY']->getValue() ) / 100;
						}
					}

					if( 0 < $fltIncreasedDepositAmount ) {

						$objNewDepositScheduledCharge = clone $objDepositScheduledCharge;

						$objNewDepositScheduledCharge->setId( NULL );
						$objNewDepositScheduledCharge->setChargeAmount( $fltIncreasedDepositAmount );
						$objNewDepositScheduledCharge->setArCodeId( NULL );
						$objNewDepositScheduledCharge->setArOriginReferenceId( NULL );
						$objNewDepositScheduledCharge->setLastPostedOn( date( 'm/d/Y' ) );

						if( ( true == array_key_exists( 'INCREASE_SECURITY_DEPOSIT_TYPE', $arrobjPropertyPreferences ) && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE']->getValue() ) || CArTrigger::MOVE_IN != $objNewDepositScheduledCharge->getArTriggerId() ) {
							$objNewDepositScheduledCharge->setArTriggerId( CArTrigger::MOVE_IN );
						}

						if( 0 < $intChargeCodeId ) {
							$objNewDepositScheduledCharge->setArCodeId( $intChargeCodeId );
						}

						$objDatabase->begin();

						$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $objApplication->getPropertyId(), $this->getCid(), $objDatabase );

						$objArTransaction = $objNewDepositScheduledCharge->createArTransaction( date( 'm/d/Y' ), date( 'm/d/Y' ), $objNewDepositScheduledCharge->getChargeAmount(), $objPropertyGlSetting, $objDatabase );

						if( true == valObj( $objArCode, 'CArCode' ) ) {
							$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
						}

						$objArTransaction->setPropertyId( $objApplication->getPropertyId() );
						$objArTransaction->setLeaseId( $objApplication->getLeaseId() );
						$objArTransaction->setCustomerId( $objApplication->getApplicantApplicationId() );
						$objArTransaction->getOrFetchPostMonth( $objDatabase );

						if( false == $objNewDepositScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						if( false == $objArTransaction->postCharge( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
							break;
						}

						$objDatabase->commit();

						$this->setDepositArTransactionId( $objArTransaction->getId() );
					}
				}
				break;

			// This is a temporary fix as per task id# 204687
			case CTransmissionConditionType::INCREASE_DEPOSIT_2:
				$this->setConditionDatetime( 'NOW()' );
				$this->setSatisfiedBy( $intCompanyUserId );
				$this->setSatisfiedOn( 'NOW()' );

				if( true == $boolIsScheduledChargeProcessed )
					break;

				$objRentArCode 			= $this->m_objProperty->getOrFetchRentArCode( $objDatabase );
				$objApplication 		= $this->fetchApplication( $objDatabase );

				$intRentChargeCodeId 	= ( true == valObj( $objRentArCode, 'CArCode' ) ) ? $objRentArCode->getId() : NULL;
				$intChargeCodeId 		= ( true == isset( $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT_2'] ) && 0 < $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT_2']->getValue() ) ? $arrobjPropertyPreferences['CHARGE_CODE_SECURITY_DEPOSIT_2']->getValue() : NULL;
				$objArCode 				= ( 0 < ( int ) $intChargeCodeId ) ? \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $intChargeCodeId, $this->getCid(), $objDatabase ) : NULL;

				$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), array_merge( CArTrigger::$c_arrintRecurringArTriggers, [ CArTrigger::MOVE_IN ] ), $this->getCid(), $objDatabase );

				$objDepositScheduledCharge 	= NULL;
				$fltTotalMoveinFee 			= 0;
				$boolIsCurrentDeposit 		= false;

				foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
					if( 0 < $intRentChargeCodeId && $intRentChargeCodeId == $objScheduledCharge->getArCodeId() && CArTrigger::MONTHLY == $objScheduledCharge->getArTriggerId() && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
						$objDepositScheduledCharge = $objScheduledCharge;
					}

					if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() && CArTrigger::MOVE_IN == $objScheduledCharge->getArTriggerId() && 'PERCENTAGE_OF_RENT' != $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
						$objDepositScheduledCharge = $objScheduledCharge;
					}

					$fltTotalMoveinFee += $objScheduledCharge->getChargeAmount();
				}

				if( false == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) && valArr( $arrobjScheduledCharges ) ) {
					$boolIsCurrentDeposit = true;
					$objDepositScheduledCharge = current( $arrobjScheduledCharges );

					if( false == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) ) {
						$objDepositScheduledCharge = array_pop( array_reverse( $arrobjScheduledCharges ) );
					}
				}

				if( true == valObj( $objDepositScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2'] ) && 0 < $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue() ) {

					$fltIncreasedDepositAmount = 0;

					if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
						$fltIncreasedDepositAmount = ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue();
					}

					if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && ( 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() || 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) ) {

						if( false == $boolIsCurrentDeposit ) {
							$fltIncreasedDepositAmount = ( $objDepositScheduledCharge->getChargeAmount() * ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue() ) / 100;
						} else {
							$fltIncreasedDepositAmount = ( $fltTotalMoveinFee * ( float ) $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue() ) / 100;
						}
					}

					if( 0 < $fltIncreasedDepositAmount ) {

						$objNewDepositScheduledCharge = clone $objDepositScheduledCharge;

						$objNewDepositScheduledCharge->setId( NULL );
						$objNewDepositScheduledCharge->setChargeAmount( $fltIncreasedDepositAmount );
						$objNewDepositScheduledCharge->setArCodeId( NULL );
						$objNewDepositScheduledCharge->setArOriginReferenceId( NULL );
						$objNewDepositScheduledCharge->setLastPostedOn( date( 'm/d/Y' ) );

						if( true == isset( $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2'] ) && 'PERCENTAGE_OF_RENT' == $arrobjPropertyPreferences['INCREASE_SECURITY_DEPOSIT_TYPE_2']->getValue() ) {
							$objNewDepositScheduledCharge->setArTriggerId( CArTrigger::MOVE_IN );
						}

						if( 0 < $intChargeCodeId ) {
							$objNewDepositScheduledCharge->setArCodeId( $intChargeCodeId );
						}

						$objDatabase->begin();

						$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $objApplication->getPropertyId(), $this->getCid(), $objDatabase );

						$objArTransaction = $objNewDepositScheduledCharge->createArTransaction( date( 'm/d/Y' ), date( 'm/d/Y' ), $objNewDepositScheduledCharge->getChargeAmount(), $objPropertyGlSetting, $objDatabase );

						if( true == valObj( $objArCode, 'CArCode' ) ) {
							$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
						}

						$objArTransaction->setPropertyId( $objApplication->getPropertyId() );
						$objArTransaction->setLeaseId( $objApplication->getLeaseId() );
						$objArTransaction->setCustomerId( $objApplication->getApplicantApplicationId() );
						$objArTransaction->getOrFetchPostMonth( $objDatabase );

						if( false == $objNewDepositScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						if( false == $objArTransaction->postCharge( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
							break;
						}

						$objDatabase->commit();

						$this->setDepositArTransactionId( $objArTransaction->getId() );
					}
				}
				break;

			case CTransmissionConditionType::INCREASE_RENT:
				$this->setConditionDatetime( 'NOW()' );
				$this->setSatisfiedBy( $intCompanyUserId );
				$this->setSatisfiedOn( 'NOW()' );

				if( true == $boolIsScheduledChargeProcessed ) {
					break;
				}

				$intChargeCodeId = ( true == isset( $arrobjPropertyPreferences['CHARGE_CODE_RENT'] ) && 0 < $arrobjPropertyPreferences['CHARGE_CODE_RENT']->getValue() ) ? $arrobjPropertyPreferences['CHARGE_CODE_RENT']->getValue() : NULL;

				$objApplication 		= $this->fetchApplication( $objDatabase );

				$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), [ CArTrigger::MONTHLY ], $this->getCid(), $objDatabase );

				$objRentScheduledCharge = NULL;
				$fltTotalRecurringFee 	= 0;
				$boolIsCurrentRent 		= false;

				foreach( $arrobjScheduledCharges as $objScheduledCharge ) {

					if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() ) {
						$objRentScheduledCharge = $objScheduledCharge;
					}

					$fltTotalRecurringFee += $objScheduledCharge->getChargeAmount();
				}

				if( false == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && valArr( $arrobjScheduledCharges ) ) {
					$boolIsCurrentRent = true;
					$objRentScheduledCharge = current( $arrobjScheduledCharges );
				}

				if( true == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreferences['INCREASE_RENT_BY'] ) && 0 < $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue() ) {

					$fltIncreasedRentAmount = 0;

					if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE']->getValue() ) {
						$fltIncreasedRentAmount = ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue();
					}

					if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE']->getValue() ) {

						if( false == $boolIsCurrentRent ) {
							$fltIncreasedRentAmount = ( $objRentScheduledCharge->getChargeAmount() * ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue() ) / 100;
						} else {
							$fltIncreasedRentAmount = ( $fltTotalRecurringFee * ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY']->getValue() ) / 100;
						}
					}

					if( 0 < $fltIncreasedRentAmount ) {

						$objNewRentScheduledCharge = clone $objRentScheduledCharge;

						if( 0 < $intChargeCodeId ) {
							$objNewRentScheduledCharge->setArCodeId( $intChargeCodeId );
						}

						if( 0 < $objNewRentScheduledCharge->getArOriginReferenceId() ) {
							$objNewRentScheduledCharge->setId( NULL );
						}

						if( 0 < $objNewRentScheduledCharge->getId() ) {
							$objNewRentScheduledCharge->setChargeAmount( $fltIncreasedRentAmount + $objRentScheduledCharge->getChargeAmount() );
						} else {
							$objNewRentScheduledCharge->setChargeAmount( $fltIncreasedRentAmount );
						}

						if( $intChargeCodeId == $objRentScheduledCharge->getArCodeId() && false == $objNewRentScheduledCharge->update( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						} elseif( $intChargeCodeId != $objRentScheduledCharge->getArCodeId() && false == $objNewRentScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						$this->setRentScheduledChargeId( $objNewRentScheduledCharge->getId() );
					}
				}
				break;

			// This is a temporary fix as per task id# 204687

			case CTransmissionConditionType::INCREASE_RENT_2:
				$this->setConditionDatetime( 'NOW()' );
				$this->setSatisfiedBy( $intCompanyUserId );
				$this->setSatisfiedOn( 'NOW()' );

				if( true == $boolIsScheduledChargeProcessed ) {
					break;
				}

				$intChargeCodeId = ( true == isset( $arrobjPropertyPreferences['CHARGE_CODE_RENT_2'] ) && 0 < $arrobjPropertyPreferences['CHARGE_CODE_RENT_2']->getValue() ) ? $arrobjPropertyPreferences['CHARGE_CODE_RENT_2']->getValue() : NULL;

				$objApplication 		= $this->fetchApplication( $objDatabase );

				$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), CArTrigger::$c_arrintRecurringArTriggers, $this->getCid(), $objDatabase );

				$objRentScheduledCharge = NULL;
				$fltTotalRecurringFee 	= 0;
				$boolIsCurrentRent 		= false;

				foreach( $arrobjScheduledCharges as $objScheduledCharge ) {

					if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() ) {
						$objRentScheduledCharge = $objScheduledCharge;
					}

					$fltTotalRecurringFee += $objScheduledCharge->getChargeAmount();
				}

				if( false == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && valArr( $arrobjScheduledCharges ) ) {
					$boolIsCurrentRent = true;
					$objRentScheduledCharge = current( $arrobjScheduledCharges );
				}

				if( true == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreferences['INCREASE_RENT_BY_2'] ) && 0 < $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue() ) {

					$fltIncreasedRentAmount = 0;

					if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2'] ) && 'AMOUNT' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2']->getValue() ) {
						$fltIncreasedRentAmount = ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue();
					}

					if( true == isset( $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2'] ) && 'PERCENTAGE' == $arrobjPropertyPreferences['INCREASE_RENT_TYPE_2']->getValue() ) {

						if( false == $boolIsCurrentRent ) {
							$fltIncreasedRentAmount = ( $objRentScheduledCharge->getChargeAmount() * ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue() ) / 100;
						} else {
							$fltIncreasedRentAmount = ( $fltTotalRecurringFee * ( float ) $arrobjPropertyPreferences['INCREASE_RENT_BY_2']->getValue() ) / 100;
						}
					}

					if( 0 < $fltIncreasedRentAmount ) {

						$objNewRentScheduledCharge = clone $objRentScheduledCharge;

						if( 0 < $intChargeCodeId ) {
							$objNewRentScheduledCharge->setArCodeId( $intChargeCodeId );
						}

						if( 0 < $objNewRentScheduledCharge->getArOriginReferenceId() ) {
							$objNewRentScheduledCharge->setId( NULL );
						}

						if( 0 < $objNewRentScheduledCharge->getId() ) {
							$objNewRentScheduledCharge->setChargeAmount( $fltIncreasedRentAmount + $objRentScheduledCharge->getChargeAmount() );
						} else {
							$objNewRentScheduledCharge->setChargeAmount( $fltIncreasedRentAmount );
						}

						if( $intChargeCodeId == $objRentScheduledCharge->getArCodeId() && false == $objNewRentScheduledCharge->update( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						} elseif( $intChargeCodeId != $objRentScheduledCharge->getArCodeId() && false == $objNewRentScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						$this->setRentScheduledChargeId( $objNewRentScheduledCharge->getId() );
					}
				}
				break;

			case CTransmissionConditionType::CUSTOM_SCREENING_CONDITION;
					$this->setConditionDatetime( 'NOW()' );
					$this->setSatisfiedBy( $intCompanyUserId );
					$this->setSatisfiedOn( 'NOW()' );
					if( true == $boolIsScheduledChargeProcessed ) {
						break;
					}
					if( false == $this->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
						trigger_error( 'Failed to update the transmission condition object.', E_USER_ERROR );
					}
				break;

			case CTransmissionConditionType::REQUIRE_GUARANTOR:
				if( 0 < $this->getGuarantorApplicantApplicationId() ) {
					$objApplicantApplicationTransmission = CApplicantApplicationTransmissions::fetchLastApplicantApplicationTransmissionByApplicantApplicationIdByLastTransmissionIdByCid( $this->getGuarantorApplicantApplicationId(), $this->getTransmissionId(), $this->getCid(), $objDatabase );

					if( true == valObj( $objApplicantApplicationTransmission, 'CApplicantApplicationTransmission' ) ) {
						if( false == $boolIsTransmissionFailed ) {
							$this->setConditionDatetime( 'NOW()' );
							$this->setGuarantorApplicantApplicationTransmissionId( $objApplicantApplicationTransmission->getId() );
							$this->setSatisfiedBy( $intCompanyUserId );
							$this->setSatisfiedOn( 'NOW()' );
						}
					} else {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'No transmission found with the selected guarrantor.' ) );
					}
				}
				break;

			default:
				// added default case
				break;
		}
	}

	public function revertTransmissionConditions( $arrobjSatisfiedTransmissionCondition, $arrobjPropertyPreference, $arrobjProperty, $intCompanyUserId, $objDatabase ) {

			if( false == valObj( $arrobjSatisfiedTransmissionCondition, 'CTransmissionCondition' ) ) return;

			switch( $arrobjSatisfiedTransmissionCondition->getTransmissionConditionTypeId() ) {

				case CTransmissionConditionType::INCREASE_RENT:
					$intChargeCodeId 		= ( true == isset( $arrobjPropertyPreference['CHARGE_CODE_RENT'] ) && 0 < $arrobjPropertyPreference['CHARGE_CODE_RENT']->getValue() ) ? $arrobjPropertyPreference['CHARGE_CODE_RENT']->getValue() : NULL;

					$objApplication = $this->fetchApplication( $objDatabase );

					$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), CArTrigger::$c_arrintRecurringArTriggers, $this->getCid(), $objDatabase );

					$objRentScheduledCharge = NULL;
					$flatDecreasedChargeAmount = 0;

					foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
						if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() ) {
							$objRentScheduledCharge = $objScheduledCharge;
						}
					}

					if( true == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreference['INCREASE_RENT_BY'] ) && 0 < $arrobjPropertyPreference['INCREASE_RENT_BY']->getValue() ) {

						$intScheduledChargeId	= $arrobjSatisfiedTransmissionCondition->getRentScheduledChargeId();

						if( true == isset( $arrobjPropertyPreference['INCREASE_RENT_TYPE'] ) && 'AMOUNT' == $arrobjPropertyPreference['INCREASE_RENT_TYPE']->getValue() ) {
							$flatDecreasedChargeAmount = $objRentScheduledCharge->getChargeAmount() - ( float ) $arrobjPropertyPreference['INCREASE_RENT_BY']->getValue();
						}

						if( true == isset( $arrobjPropertyPreference['INCREASE_RENT_TYPE'] ) && 'PERCENTAGE' == $arrobjPropertyPreference['INCREASE_RENT_TYPE']->getValue() ) {
							$flatDecreasedChargeAmount = $this->revertScheduledCharge( $objRentScheduledCharge->getChargeAmount(), $arrobjPropertyPreference['INCREASE_RENT_BY']->getValue() );
						}
					}

					$objDatabase->begin();

					if( 0 < $flatDecreasedChargeAmount ) {

						$objNewRentScheduledCharge = clone $objRentScheduledCharge;

						$objNewRentScheduledCharge->setId( $objRentScheduledCharge->getId() );
						$objNewRentScheduledCharge->setChargeAmount( $flatDecreasedChargeAmount );

						if( false == $objNewRentScheduledCharge->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}
					}

					$objDatabase->commit();
					break;

				// This is a temporary fix as per task id# 204687

				case CTransmissionConditionType::INCREASE_RENT_2:
					$intChargeCodeId = ( true == isset( $arrobjPropertyPreference['CHARGE_CODE_RENT_2'] ) && 0 < $arrobjPropertyPreference['CHARGE_CODE_RENT_2']->getValue() ) ? $arrobjPropertyPreference['CHARGE_CODE_RENT_2']->getValue() : NULL;

					$objApplication = $this->fetchApplication( $objDatabase );

					$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), CArTrigger::$c_arrintRecurringArTriggers, $this->getCid(), $objDatabase );

					$objRentScheduledCharge = NULL;
					$flatDecreasedChargeAmount = 0;

					foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
						if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() ) {
							$objRentScheduledCharge = $objScheduledCharge;
						}
					}

					if( true == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreference['INCREASE_RENT_BY_2'] ) && 0 < $arrobjPropertyPreference['INCREASE_RENT_BY_2']->getValue() ) {

						// need to delete this
						$intScheduledChargeId	= $arrobjSatisfiedTransmissionCondition->getRentScheduledChargeId();

						if( true == isset( $arrobjPropertyPreference['INCREASE_RENT_TYPE_2'] ) && 'AMOUNT' == $arrobjPropertyPreference['INCREASE_RENT_TYPE_2']->getValue() ) {
							$flatDecreasedChargeAmount = $objRentScheduledCharge->getChargeAmount() - ( float ) $arrobjPropertyPreference['INCREASE_RENT_BY_2']->getValue();
						}

						if( true == isset( $arrobjPropertyPreference['INCREASE_RENT_TYPE_2'] ) && 'PERCENTAGE' == $arrobjPropertyPreference['INCREASE_RENT_TYPE_2']->getValue() ) {
							$flatDecreasedChargeAmount = $this->revertScheduledCharge( $objRentScheduledCharge->getChargeAmount(), $arrobjPropertyPreference['INCREASE_RENT_BY_2']->getValue() );
						}
					}

					$objDatabase->begin();

					if( 0 < $flatDecreasedChargeAmount ) {

						$objNewRentScheduledCharge = clone $objRentScheduledCharge;

						$objNewRentScheduledCharge->setId( $objRentScheduledCharge->getId() );
						$objNewRentScheduledCharge->setChargeAmount( $flatDecreasedChargeAmount );

						if( false == $objNewRentScheduledCharge->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}
					}

					$objDatabase->commit();
					break;

				case CTransmissionConditionType::INCREASE_DEPOSIT:
					$intChargeCodeId = ( true == isset( $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT'] ) && 0 < $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT']->getValue() ) ? $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT']->getValue() : NULL;

					$objApplication = $this->fetchApplication( $objDatabase );

					$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), [ CArTrigger::MOVE_IN ], $this->getCid(), $objDatabase );

					$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByIdByCid( $arrobjSatisfiedTransmissionCondition->getDepositArTransactionId(), $this->getCid(), $objDatabase );

					if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
						trigger_error( 'Failed to load the transaction object for application id: ' . $this->getApplicationId() . ' for client id: ' . $this->getCid() . ' for transmission condition id: ' . $arrobjSatisfiedTransmissionCondition->getId(), E_USER_WARNING );
					}

					$objDeleteScheduledCharge 	= NULL;
					$intScheduledChargeId		= 0;

					foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
						if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() && false == is_null( $objScheduledCharge->getLastPostedOn() ) ) {
							$objDeleteScheduledCharge = $objScheduledCharge;
							$intScheduledChargeId	  = $objScheduledCharge->getId();
						}
					}

					if( true == valObj( $objDeleteScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreference['INCREASE_SECURITY_DEPOSIT_BY'] ) && 0 < $arrobjPropertyPreference['INCREASE_SECURITY_DEPOSIT_BY']->getValue() ) {

						$objDatabase->begin();

						// deleting records from scheduled charges table.
						$objDeleteScheduledCharge->setId( $intScheduledChargeId );

						if( false == $objDeleteScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						// removing records from ar transactions table.
						if( true == valObj( $objArTransaction, 'CArTransaction' ) && 1 != $objArTransaction->getIsDeleted() ) {
							$objPropertyGlSetting 	= $arrobjProperty->getOrFetchPropertyGlSetting( $objDatabase );
							$boolRapidDelete 		= ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) ? false : true;

							$objArTransaction->delete( $intCompanyUserId, $objDatabase, NULL, false, $boolRapidDelete );
						}

						$objDatabase->commit();

					}
					break;

				// This is a temporary fix as per task id# 204687
				case CTransmissionConditionType::INCREASE_DEPOSIT_2:
					$intChargeCodeId = ( true == isset( $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT_2'] ) && 0 < $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT_2']->getValue() ) ? $arrobjPropertyPreference['CHARGE_CODE_SECURITY_DEPOSIT_2']->getValue() : NULL;

					$objApplication = $this->fetchApplication( $objDatabase );

					$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), [ CArTrigger::MOVE_IN ], $this->getCid(), $objDatabase );

					$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByIdByCid( $arrobjSatisfiedTransmissionCondition->getDepositArTransactionId(), $this->getCid(), $objDatabase );

					if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
						trigger_error( 'Failed to load the transaction object for application id: ' . $this->getApplicationId() . ' for client id: ' . $this->getCid() . ' for transmission condition id: ' . $arrobjSatisfiedTransmissionCondition->getId(), E_USER_WARNING );
					}

					$objDeleteScheduledCharge 	= NULL;
					$intScheduledChargeId		= 0;

					foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
						if( 0 < $intChargeCodeId && $intChargeCodeId == $objScheduledCharge->getArCodeId() && false == is_null( $objScheduledCharge->getLastPostedOn() ) ) {
							$objDeleteScheduledCharge = $objScheduledCharge;
							$intScheduledChargeId	  = $objScheduledCharge->getId();
						}
					}

					if( true == valObj( $objDeleteScheduledCharge, 'CScheduledCharge' ) && true == isset( $arrobjPropertyPreference['INCREASE_SECURITY_DEPOSIT_BY_2'] ) && 0 < $arrobjPropertyPreference['INCREASE_SECURITY_DEPOSIT_BY_2']->getValue() ) {

						$objDatabase->begin();

						// deleting records from scheduled charges table.
						$objDeleteScheduledCharge->setId( $intScheduledChargeId );

						if( false == $objDeleteScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $this->getErrorMsgs() );
							break;
						}

						// removing records from ar transactions table.
						if( true == valObj( $objArTransaction, 'CArTransaction' ) && 1 != $objArTransaction->getIsDeleted() ) {
							$objPropertyGlSetting 	= $arrobjProperty->getOrFetchPropertyGlSetting( $objDatabase );
							$boolRapidDelete 		= ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) ? false : true;

							$objArTransaction->delete( $intCompanyUserId, $objDatabase, NULL, false, $boolRapidDelete );
						}

						$objDatabase->commit();
					}
					break;

				default:
					// Need to add comment
					break;
			}
	}

	public function revertScheduledCharge( $fltScheduledChargeAmount, $intIncreaseRentBy ) {
		$fltOriginalScheduledCharge = ( 100 / ( 100 + $intIncreaseRentBy ) ) * $fltScheduledChargeAmount;
		return $fltOriginalScheduledCharge;
	}

	public function processAutoSatisfyRequireGuarantorTransmissionCondition( $objApplication, $arrobjPropertyPreferences, $objDatabase ) {

		$arrobjLatestGuarantorApplicantApplicationTransmissions = $objApplication->fetchLatestApplicantApplicationTransmissionsByCustomerTypeIds( [ CCustomerType::GUARANTOR ], $objDatabase );

		$arrobjLatestGuarantorApplicantApplicationTransmissions = rekeyObjects( 'TransmissionResponseTypeId', $arrobjLatestGuarantorApplicantApplicationTransmissions );

		if( false == valArr( $arrobjLatestGuarantorApplicantApplicationTransmissions ) ) return true;

		$boolIsScheduledChargeProcessed = false;

		if( true == array_key_exists( CTransmissionResponseType::APPROVED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) || true == array_key_exists( CTransmissionResponseType::CONDITIONALLY_APPROVED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) || true == array_key_exists( CTransmissionResponseType::DENIED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) ) {

			if( true == array_key_exists( CTransmissionResponseType::APPROVED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) ) {
				$this->setGuarantorApplicantApplicationId( $arrobjLatestGuarantorApplicantApplicationTransmissions[CTransmissionResponseType::APPROVED]->getApplicantApplicationId() );
			} elseif( true == array_key_exists( CTransmissionResponseType::CONDITIONALLY_APPROVED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) ) {
				$this->setGuarantorApplicantApplicationId( $arrobjLatestGuarantorApplicantApplicationTransmissions[CTransmissionResponseType::CONDITIONALLY_APPROVED]->getApplicantApplicationId() );
			} elseif( true == array_key_exists( CTransmissionResponseType::DENIED, $arrobjLatestGuarantorApplicantApplicationTransmissions ) ) {
				$this->setGuarantorApplicantApplicationId( $arrobjLatestGuarantorApplicantApplicationTransmissions[CTransmissionResponseType::DENIED]->getApplicantApplicationId() );
			}

			$this->processTransmissionCondition( $arrobjPropertyPreferences, SYSTEM_USER_ID, $objDatabase, $boolIsScheduledChargeProcessed );

			if( false == $this->update( SYSTEM_USER_ID, $objDatabase ) ) {
				trigger_error( 'Failed to update the transmission condition object.', E_USER_ERROR );
				return false;
			}

		} else {

			$this->setSatisfiedBy( NULL );
			$this->setSatisfiedOn( NULL );
			$this->setGuarantorApplicantApplicationId( NULL );
			$this->setGuarantorApplicantApplicationTransmissionId( NULL );
			$this->setConditionDatetime( NULL );

			$this->processTransmissionCondition( $arrobjPropertyPreferences, SYSTEM_USER_ID, $objDatabase, $boolIsScheduledChargeProcessed, $boolIsTransmissionFailed = true );

			if( false == $this->update( SYSTEM_USER_ID, $objDatabase ) ) {
				trigger_error( 'Failed to update the transmission condition object.', E_USER_ERROR );
				return false;
			}

		}

		return true;
	}

}
?>