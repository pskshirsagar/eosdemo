<?php

class COldRenewalOffer extends CBaseOldRenewalOffer {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyRenewalOfferSettingId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyLeasePeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConcessionArCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOfferDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMinLeaseTerm() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaxLeaseTerm() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncentive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncentiveDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncentiveAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConcessionAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>