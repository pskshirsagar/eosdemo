<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxIdTypes
 * Do not add any new functions to this class.
 */

class CTaxIdTypes extends CBaseTaxIdTypes {

	public static function fetchTaxIdTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CTaxIdType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaxIdType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CTaxIdType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaxIdTypesByAssignTypes( $arrstrTaxIdTypeAssignTypes, $objDatabase ) {
		if( false == valArr( $arrstrTaxIdTypeAssignTypes ) ) {
			return;
		}

		$strSql = '	SELECT 
						* 
					FROM
					    tax_id_types 
					WHERE
					    is_published = TRUE
					    AND assign_type IN ( ' . sqlStrImplode( $arrstrTaxIdTypeAssignTypes ) . ' )';

		return self::fetchTaxIdTypes( $strSql, $objDatabase );
	}

	public function fetchPublishedTaxIdTypes( $objDatabase, $boolIsPropertyIntegrated = false ) {

		$strIntegrationCheck = ( true == $boolIsPropertyIntegrated ) ? 'AND t.id = ' . CTaxIdType::SSN : '';

		$strSql = 'SELECT 
						t.*,
                        c.name as country_name
					FROM 
						tax_id_types t
						JOIN countries c ON ( t.country_code = c.code )
					WHERE
						t.is_published = true
					    AND COALESCE( t.assign_type, \'' . CTaxIdAssignType::CUSTOMER . '\') <> \'' . CTaxIdAssignType::PROPERTY . '\'
					    ' . $strIntegrationCheck . '
					    ORDER BY t.order_num';

		return parent::fetchTaxIdTypes( $strSql, $objDatabase );

	}

	public static function fetchAllTaxIdTypes( $objDatabase ) {
			$strSql = 'SELECT * FROM tax_id_types';
			return self::fetchTaxIdTypes( $strSql, $objDatabase );
	}

	public static function fetchTaxIdTypeNameById( $intTaxIdType, $objDatabase ) {

		$strSql = '	SELECT 
						name 
					FROM
					    tax_id_types 
					WHERE
					    id = ' . ( int ) $intTaxIdType;

		return self::fetchColumn( $strSql, 'name', $objDatabase );
	}

	// Fetch Tax Type Id Abbreviation By Id

	public static function fetchTaxIdTypeAbbreviationById( $intTaxIdType, CDatabase $objDatabase ) {

		$strSql = '	SELECT
						abbreviation
					FROM
					    tax_id_types
					WHERE
					    id = ' . ( int ) $intTaxIdType;

		return self::fetchColumn( $strSql, 'abbreviation', $objDatabase );
	}

}
?>