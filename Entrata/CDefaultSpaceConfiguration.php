<?php

class CDefaultSpaceConfiguration extends CBaseDefaultSpaceConfiguration {

	const SPACE_CONFIGURATION_CONVENTIONAL	= 1; // Conventional
	const SPACE_CONFIGURATION_PRIVATE 		= 2; // Private
	const SPACE_CONFIGURATION_SHARED 		= 3; // Shared
	const SPACE_CONFIGURATION_OTHER 		= 4; // Other
	const SPACE_CONFIGURATION_ENTIRE_UNIT 	= 5; // Entire Unit

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsStudent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>
