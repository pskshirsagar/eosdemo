<?php

class CImportMapping extends CBaseImportMapping {

	protected $m_strChargeCodeName;
	protected $m_strArCodeId;
	protected $m_strLeadSourceName;
	protected $m_strLeadSourceId;
	protected $m_strLeaseTermName;
	protected $m_strLeaseTermId;
	protected $m_strLeasingAgentName;
	protected $m_strLeasingAgentId;
	protected $m_strMoveOutReasonName;
	protected $m_strMoveOutReasonId;
	protected $m_strOccupancyTypeId;
	protected $m_strPetTypeName;
	protected $m_strPetTypeId;
	protected $m_strInsurancePolicyStatusTypeName;
	protected $m_strInsurancePolicyStatusTypeId;
	protected $m_strMaintenanceProblemName;
	protected $m_strMaintenanceProblemId;
	protected $m_strMaintenanceLocationName;
	protected $m_strMaintenanceLocationId;
	protected $m_strPropertyListItemName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportSourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMigrationTokenId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocalDataKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChildSourceKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocalAdditionalKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getChargeCodeName() {
		return $this->m_strChargeCodeName;
	}

	public function setChargeCodeName( $strChargeCodeName ) {
		$this->m_strChargeCodeName = $strChargeCodeName;
	}

	public function getArCodeId() {
		return $this->m_strArCodeId;
	}

	public function setArCodeId( $strArCodeId ) {
		$this->m_strArCodeId = $strArCodeId;
	}

	public function getLeadSourceName() {
		return $this->m_strLeadSourceName;
	}

	public function setLeadSourceName( $strLeadSourceName ) {
		$this->m_strLeadSourceName = $strLeadSourceName;
	}

	public function getLeadSourceId() {
		return $this->m_strLeadSourceId;
	}

	public function setLeadSourceId( $strLeadSourceId ) {
		$this->m_strLeadSourceId = $strLeadSourceId;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function getLeaseTermId() {
		return $this->m_strLeaseTermId;
	}

	public function setLeaseTermId( $strLeaseTermId ) {
		$this->m_strLeaseTermId = $strLeaseTermId;
	}

	public function getLeasingAgentName() {
		return $this->m_strLeasingAgentName;
	}

	public function setLeasingAgentName( $strLeaseTermName ) {
		$this->m_strLeasingAgentName = $strLeaseTermName;
	}

	public function getLeasingAgentId() {
		return $this->m_strLeasingAgentId;
	}

	public function setLeasingAgentId( $strLeaseTermId ) {
		$this->m_strLeasingAgentId = $strLeaseTermId;
	}

	public function getMoveOutReasonName() {
		return $this->m_strMoveOutReasonName;
	}

	public function setMoveOutReasonName( $strMoveOutReasonName ) {
		$this->m_strMoveOutReasonName = $strMoveOutReasonName;
	}

	public function getMoveOutReasonId() {
		return $this->m_strMoveOutReasonId;
	}

	public function setMoveOutReasonId( $strMoveOutReasonId ) {
		$this->m_strMoveOutReasonId = $strMoveOutReasonId;
	}

	public function getOccupancyTypeId() {
		return $this->m_strOccupancyTypeId;
	}

	public function setOccupancyTypeId( $strOccupancyTypeId ) {
		$this->m_strOccupancyTypeId = $strOccupancyTypeId;
	}

	public function getPetTypeName() {
		return $this->m_strPetTypeName;
	}

	public function setPetTypeName( $strPetTypeName ) {
		$this->m_strPetTypeName = $strPetTypeName;
	}

	public function getPetTypeId() {
		return $this->m_strPetTypeId;
	}

	public function setPetTypeId( $strPetTypeId ) {
		$this->m_strPetTypeId = $strPetTypeId;
	}

	public function getInsurancePolicyStatusTypeName() {
		return $this->m_strInsurancePolicyStatusTypeName;
	}

	public function setInsurancePolicyStatusTypeName( $strInsurancePolicyStatusTypeName ) {
		$this->m_strInsurancePolicyStatusTypeName = $strInsurancePolicyStatusTypeName;
	}

	public function getInsurancePolicyStatusTypeId() {
		return $this->m_strInsurancePolicyStatusTypeId;
	}

	public function setInsurancePolicyStatusTypeId( $strInsurancePolicyStatusTypeId ) {
		$this->m_strInsurancePolicyStatusTypeId = $strInsurancePolicyStatusTypeId;
	}

	public function getMaintenanceProblemName() {
		return $this->m_strMaintenanceProblemName;
	}

	public function setMaintenanceProblemName( $strMaintenanceProblemName ) {
		$this->m_strMaintenanceProblemName = $strMaintenanceProblemName;
	}

	public function getMaintenanceProblemId() {
		return $this->m_strMaintenanceProblemId;
	}

	public function setMaintenanceProblemId( $strMaintenanceProblemId ) {
		$this->m_strMaintenanceProblemId = $strMaintenanceProblemId;
	}

	public function getMaintenanceLocationName() {
		return $this->m_strMaintenanceLocationName;
	}

	public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
		$this->m_strMaintenanceLocationName = $strMaintenanceLocationName;
	}

	public function getMaintenanceLocationId() {
		return $this->m_strMaintenanceLocationId;
	}

	public function setMaintenanceLocationId( $strMaintenanceLocationId ) {
		$this->m_strMaintenanceLocationId = $strMaintenanceLocationId;
	}

	public function getPropertyListItemName() {
		return $this->m_strPropertyListItemName;
	}

	public function setPropertyListItemName( $strMaintenanceLocationId ) {
		$this->m_strPropertyListItemName = $strMaintenanceLocationId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['charge_code_name'] ) ) $this->setChargeCodeName( $arrmixValues['charge_code_name'] );
		if( true == isset( $arrmixValues['ar_code_id'] ) ) $this->setArCodeId( $arrmixValues['ar_code_id'] );
		if( true == isset( $arrmixValues['lead_source_name'] ) ) $this->setLeadSourceName( $arrmixValues['lead_source_name'] );
		if( true == isset( $arrmixValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrmixValues['lead_source_id'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		if( true == isset( $arrmixValues['leasing_agent_name'] ) ) $this->setLeasingAgentName( $arrmixValues['leasing_agent_name'] );
		if( true == isset( $arrmixValues['leasing_agent_id'] ) ) $this->setLeasIngAgentId( $arrmixValues['leasing_agent_id'] );
		if( true == isset( $arrmixValues['move_out_reason_name'] ) ) $this->setMoveOutReasonName( $arrmixValues['move_out_reason_name'] );
		if( true == isset( $arrmixValues['move_out_reason_id'] ) ) $this->setMoveOutReasonId( $arrmixValues['move_out_reason_id'] );
		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		if( true == isset( $arrmixValues['pet_type_name'] ) ) $this->setPetTypeName( $arrmixValues['pet_type_name'] );
		if( true == isset( $arrmixValues['pet_type_id'] ) ) $this->setPetTypeId( $arrmixValues['pet_type_id'] );
		if( true == isset( $arrmixValues['insurance_policy_status_type_name'] ) ) $this->setInsurancePolicyStatusTypeName( $arrmixValues['insurance_policy_status_type_name'] );
		if( true == isset( $arrmixValues['insurance_policy_status_type_id'] ) ) $this->setInsurancePolicyStatusTypeId( $arrmixValues['insurance_policy_status_type_id'] );
		if( true == isset( $arrmixValues['maintenance_problem_name'] ) ) $this->setMaintenanceProblemName( $arrmixValues['maintenance_problem_name'] );
		if( true == isset( $arrmixValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrmixValues['maintenance_problem_id'] );
		if( true == isset( $arrmixValues['maintenance_location_name'] ) ) $this->setMaintenanceLocationName( $arrmixValues['maintenance_location_name'] );
		if( true == isset( $arrmixValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrmixValues['maintenance_location_id'] );
		if( true == isset( $arrmixValues['property_list_item_name'] ) ) $this->setPropertyListItemName( $arrmixValues['property_list_item_name'] );
	}

}
?>