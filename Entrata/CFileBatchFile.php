<?php

class CFileBatchFile extends CBaseFileBatchFile {

	protected $m_strResidentFolderPath;
	protected $m_strResidentFolder;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );
		if( true == isset( $arrmixValues['resident_folder_path'] ) )	$this->setResidentFolderPath( $arrmixValues['resident_folder_path'] );
		if( true == isset( $arrmixValues['resident_folder'] ) )	$this->setResidentFolder( $arrmixValues['resident_folder'] );
		return;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function setResidentFolderPath( $strResidentFolderPath ) {
		$this->m_strResidentFolderPath = $strResidentFolderPath;
	}

	public function getResidentFolderPath() {
		return $this->m_strResidentFolderPath;
	}

	public function setResidentFolder( $strResidentFolder ) {
		$this->m_strResidentFolder = $strResidentFolder;
	}

	public function getResidentFolder() {
		return $this->m_strResidentFolder;
	}

}
?>