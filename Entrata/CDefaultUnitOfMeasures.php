<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultUnitOfMeasures
 * Do not add any new functions to this class.
 */

class CDefaultUnitOfMeasures extends CBaseDefaultUnitOfMeasures {

	public static function fetchDefaultUnitOfMeasures( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultUnitOfMeasure::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultUnitOfMeasure( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultUnitOfMeasure::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>