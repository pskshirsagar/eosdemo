<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilterDescriptions
 * Do not add any new functions to this class.
 */

class CReportFilterDescriptions extends CBaseReportFilterDescriptions {

	public static function fetchReportFilterDescriptionsByDefaultReportVersionIds( $arrintDefaultReportVersionIds, $strOrderByType, $objDatabase ) {
		return self::fetchReportFilterDescriptions( sprintf( 'SELECT * FROM report_filter_descriptions WHERE default_report_version_id IN ( ' . implode( ',', $arrintDefaultReportVersionIds ) . ' ) AND deleted_by IS NULL ORDER BY ' . $strOrderByType ), $objDatabase );
	}

	public static function fetMaxOrderNumReportFilterDescriptionByDefaultreportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		$strSql = 'SELECT 
						MAX(order_num) AS  MAX_ORDER_NUM 
					FROM 
						report_filter_descriptions 
					WHERE 
						default_report_version_id = ' . ( int ) $intDefaultReportVersionId . ' 
						AND deleted_by IS NULL';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportFilterDescriptionMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				report_filter_descriptions';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportFilterDescriptionByDefaultReportVersionIdByName( $strName, $intDefaultReportVersionId, $objDatabase ) {
		$strSql = 'SELECT 
						id 
					FROM 
						report_filter_descriptions
					WHERE 
						name = \'' . $strName . '\'
						AND default_report_version_id =  ' . ( int ) $intDefaultReportVersionId . ' 
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportFilterDescriptionsByIds( $arrintReportFilterDescriptionIds, $objDatabase ) {
		if( !valArr( $arrintReportFilterDescriptionIds ) ) return [];

		$strSql = 'SELECT 
						*
					FROM
						report_filter_descriptions
					WHERE 
						id IN ( ' . implode( ',', $arrintReportFilterDescriptionIds ) . ' )';

		return self::fetchReportFilterDescriptions( $strSql, $objDatabase );
	}

	public static function fetchReportFiltersByFilterCount( CDatabase $objDatabase, bool $boolCompanyReport = false ) {
		$strWhereCondition = $strJoinCondition = '';
		if( $boolCompanyReport ) {
			$strJoinCondition = ' JOIN report_versions rv ON rv.default_report_version_id = drv.id JOIN report_new_instances rni ON rni.report_version_id = rv.id ';
			$strWhereCondition = ' WHERE rni.deleted_by is NULL ';
		}

		$strSql = '
			SELECT
				DISTINCT 
				filter_list.filter_name AS item_name,
				filter_list.filter_key AS item_key,
				filter_list.filter_count
			FROM
				(
				SELECT
					DISTINCT rfd.filter_key,
					MAX( util_get_translated( \'name\', rfd.name, rfd.details ) ) OVER ( PARTITION BY rfd.filter_key ) AS filter_name,
					count( drv.default_report_id ) OVER ( PARTITION BY rfd.filter_key ) AS filter_count
				FROM
					report_filter_descriptions rfd
					JOIN default_report_versions drv ON drv.id = rfd.default_report_version_id ' . $strJoinCondition . $strWhereCondition . '
				) AS filter_list
			ORDER BY
				filter_list.filter_count DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportFilterLabelByFilterKey( $strKey, $objDatabase ) {
		if( false == valStr( $strKey ) ) {
			return NULL;
		}
		$strSql = '
			SELECT 
				util_get_system_translated( \'name\', name, details ) AS label
			FROM 
				report_filter_descriptions
			where 
				filter_key = \'' . $strKey . '\'
				AND INITCAP( name ) = name
			LIMIT 1';
		return fetchData( $strSql, $objDatabase );
	}

}
?>