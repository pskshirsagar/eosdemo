<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COccupancyTypes
 * Do not add any new functions to this class.
 */

class COccupancyTypes extends CBaseOccupancyTypes {

	public static function fetchOccupancyTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'COccupancyType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOccupancyType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'COccupancyType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllOccupancyTypes( $objDatabase, $strOrderBy = 'order_num', $intAllowLeaseAssociation = 1 ) {
		$strSql = ' SELECT * FROM occupancy_types ';
		if( false == is_null( $intAllowLeaseAssociation ) ) {
			$strSql .= ' WHERE allow_lease_association = 1  ';
		}
		$strSql .= ' ORDER BY ' . $strOrderBy;

		return self::fetchOccupancyTypes( $strSql, $objDatabase );
	}

	public static function fetchOccupancyTypesByIds( $objDatabase, $arrintIds = NULL ) {
		$strSql = '  SELECT
						*
					 FROM
						occupancy_types ';

		if( true == valArr( $arrintIds ) ) {
			$strSql .= ' WHERE
							id IN ( ' . implode( ', ', $arrintIds ) . ' ) ';
		}

		$strSql .= 'ORDER BY 
						order_num';

		return self::fetchOccupancyTypes( $strSql, $objDatabase );
	}

	public static function fetchOccupancyTypesByAllowUnitAssociation( $intAllowUnitAssociation, $objDatabase, $strOrderBy = 'order_num' ) {
		$strSql = 'SELECT * FROM occupancy_types WHERE allow_unit_association = ' . ( int ) $intAllowUnitAssociation . ' ORDER BY ' . $strOrderBy;
		return parent::fetchOccupancyTypes( $strSql, $objDatabase );
	}

	public static function fetchAllOccupancyTypeIds( $objDatabase ) {
		$strSql = 'SELECT ot.id FROM occupancy_types ot';
		return array_keys( rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchAllOccupancyTypeAssociations( $objDatabase, $strOrderBy = 'name', $intAllowLeaseAssociation = 1, $intAllowUnitAssociation = 1 ) {

		$strSql = ' SELECT * FROM occupancy_types ';

		if( false == is_null( $intAllowLeaseAssociation ) || false == is_null( $intAllowUnitAssociation ) ) {
			$strSql .= ' WHERE allow_lease_association = 1  
							OR allow_unit_association = 1';
		}

		$strSql .= ' ORDER BY ' . $strOrderBy;

		return self::fetchOccupancyTypes( $strSql, $objDatabase );
	}

	public static function fetchAllOccupancyTypeByExcludingOtherIncome( $objDatabase, $strOrderBy = 'name', $intAllowLeaseAssociation = 1 ) {

		$strSql = ' SELECT * 
					FROM 
						occupancy_types ot
					WHERE
						ot.id <> ' . COccupancyType::OTHER_INCOME;

		if( false == is_null( $intAllowLeaseAssociation ) ) {
			$strSql .= ' AND allow_lease_association = 1  ';
		}

		$strSql .= ' ORDER BY ' . $strOrderBy;

		return self::fetchOccupancyTypes( $strSql, $objDatabase );
	}

	public static function fetchOccupancyTypesNameByIds( $objDatabase, $arrintOccupanceTypeIds ) {
		if( false == valArr( $arrintOccupanceTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT id, name FROM occupancy_types ot WHERE ot.id IN ( ' . implode( ', ', $arrintOccupanceTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOccupancyTypeNamesByPropertyId( $objDatabase, $intPropertyId ) {

		if( !valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						STRING_AGG( ot.name, \', \' ) AS occupancy_type_names
					FROM
						occupancy_types AS ot
						JOIN properties AS p ON ( ot.id = ANY ( p.occupancy_type_ids ) )
					WHERE
						p.id = ' . $intPropertyId . '
						AND p.occupancy_type_ids::text IS NOT NULL';

		$arrstrOccupancyTypes = fetchData( $strSql, $objDatabase );

		return $arrstrOccupancyTypes[0]['occupancy_type_names'];

	}

	public static function fetchOccupancyTypesByPropertyIdsByOccupancyTypeIdsByCid( $arrintPropertyIds, $arrintOccupancyTypeIds, $intCid, $objDatabase, $strOrderBy = 'order_num' ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		( true == valArr( $arrintOccupancyTypeIds ) ) ? $strWhereSql = ' AND ot.id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )' : $strWhereSql = '';

		$strSql = 'SELECT
						DISTINCT ot.*
					FROM
						occupancy_types AS ot
						JOIN properties AS p ON ( p.cid = ' . ( int ) $intCid . ' AND ot.id = ANY ( p.occupancy_type_ids ) )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereSql;

		$strSql .= ' ORDER BY ' . $strOrderBy;

		return parent::fetchOccupancyTypes( $strSql, $objDatabase );

	}

	public static function fetchOccupancyTypesByPropertyId( $objDatabase, $intPropertyId ) {

		if( !valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ot.*
					FROM
						occupancy_types AS ot
						JOIN properties AS p ON ( ot.id = ANY ( p.occupancy_type_ids ) )
					WHERE
						p.id = ' . $intPropertyId . '
						AND p.occupancy_type_ids::text IS NOT NULL';

		return parent::fetchOccupancyTypes( $strSql, $objDatabase );
	}

}
?>