<?php

class CPropertyInspectionForm extends CBasePropertyInspectionForm {

	protected $m_intInspectionMaintenanceProblemId;
	protected $m_intInspectionMaintenanceLocationId;

    public function setInspectionMaintenanceProblemId( $intInspectionMaintenanceProblemId ) {
    	$this->m_intInspectionMaintenanceProblemId = $intInspectionMaintenanceProblemId;
    }

    public function getInspectionMaintenanceProblemId() {
    	return $this->m_intInspectionMaintenanceProblemId;
    }

	public function setInspectionMaintenanceLocationId( $intInspectionMaintenanceLocationId ) {
    	$this->m_intInspectionMaintenanceLocationId = $intInspectionMaintenanceLocationId;
    }

    public function getInspectionMaintenanceLocationId() {
    	return $this->m_intInspectionMaintenanceLocationId;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, true, $boolDirectSet );
    	if( true == isset( $arrValues['maintenance_problem_id'] ) ) $this->setInspectionMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
    	if( true == isset( $arrValues['maintenance_location_id'] ) ) $this->setInspectionMaintenanceLocationId( $arrValues['maintenance_location_id'] );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>