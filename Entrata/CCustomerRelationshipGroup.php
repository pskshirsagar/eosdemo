<?php

class CCustomerRelationshipGroup extends CBaseCustomerRelationshipGroup {

	protected $m_arrobjCustomerRelationships;

	const DEFAULT_CUSTOMER_RELATIONSHIP_GROUP_ID = 1;

	// Need to verify this.
	const AFFORDABLE_CUSTOMER_RELATIONSHIP_GROUP_ID = 2049;

	/**
	 * Get Functions
	 */

	public function getCustomerRelationships() {
		return $this->m_arrobjCustomerRelationships;
	}

	public function getAllowEditCustomerRelationshipGroup() {
		if( COccupancyType::AFFORDABLE == $this->getOccupancyTypeId() )
			return false;
		return true;
	}

	/**
	 * Set Functions
	 */

	public function setCustomerRelationships( $arrobjCustomerRelationships ) {
		$this->m_arrobjCustomerRelationships = $arrobjCustomerRelationships;
	}

	/**
	 * Add Functions
	 */

	public function addCustomerRelationship( $objCustomerRelationship ) {
		$this->m_arrobjCustomerRelationships[$objCustomerRelationship->getId()] = $objCustomerRelationship;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>