<?php

class CSubsidyContractUnitSpace extends CBaseSubsidyContractUnitSpace {

	protected $m_strUnitNumberCache;
	protected $m_strUnitNumber;
	protected $m_strHeadOfHouseholdId;
	protected $m_strNameLast;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strBirthDate;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strZip5;
	protected $m_strZip4;
	protected $m_strMobilityDisability;
	protected $m_strHearingDisability;
	protected $m_strVisualDisability;
	protected $m_strTaxCreditBin;
	protected $m_intTransactionTypeId;

	protected $m_intNumberOfBedrooms;

	/**
	 * Get Set Functions
	 *
	 */
	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true );
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setHeadOfHouseholdId( $strHeadOfHouseholdId ) {
		$this->m_strHeadOfHouseholdId = $strHeadOfHouseholdId;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->m_strNameMiddle = $strNameMiddle;
	}

	public function setBirthDate( $strBirthDate ) {
		$this->m_strBirthDate = $strBirthDate;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = $strStreetLine2;
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = $strStreetLine3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setZip5( $strZip5 ) {
		$this->m_strZip5 = $strZip5;
	}

	public function setZip4( $strZip4 ) {
		$this->m_strZip4 = $strZip4;
	}

	public function setMobilityDisability( $strMobilityDisability ) {
		$this->m_strMobilityDisability = $strMobilityDisability;
	}

	public function setHearingDisability( $strHearingDisability ) {
		$this->m_strHearingDisability = $strHearingDisability;
	}

	public function setVisualDisability( $strVisualDisability ) {
		$this->m_strVisualDisability = $strVisualDisability;
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->m_intNumberOfBedrooms = $intNumberOfBedrooms;
	}

	public function setTaxCreditBin( $strTaxCreditBin ) {
		$this->m_strTaxCreditBin = $strTaxCreditBin;
	}

	public function setTransactionTypeId( $intTransactionTypeId ) {
		$this->m_intTransactionTypeId = $intTransactionTypeId;
	}

	// getters

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getHeadOfHouseholdId() {
		return $this->m_strHeadOfHouseholdId;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getZip5() {
		return $this->m_strZip5;
	}

	public function getZip4() {
		return $this->m_strZip4;
	}

	public function getMobilityDisability() {
		return $this->m_strMobilityDisability;
	}

	public function getHearingDisability() {
		return $this->m_strHearingDisability;
	}

	public function getVisualDisability() {
		return $this->m_strVisualDisability;
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function getTaxCreditBin() {
		return $this->m_strTaxCreditBin;
	}

	public function getTransactionTypeId() {
		return $this->m_intTransactionTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_number_cache'] ) : $arrmixValues['unit_number_cache'] );

		if( array_key_exists( 'unit_number', $arrmixValues ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}
		if( array_key_exists( 'head_of_household_id', $arrmixValues ) ) {
			$this->setHeadOfHouseholdId( $arrmixValues['head_of_household_id'] );
		}
		if( array_key_exists( 'name_last', $arrmixValues ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}
		if( array_key_exists( 'name_first', $arrmixValues ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}
		if( array_key_exists( 'name_middle', $arrmixValues ) ) {
			$this->setNameMiddle( $arrmixValues['name_middle'] );
		}
		if( array_key_exists( 'birth_date', $arrmixValues ) ) {
			$this->setBirthDate( $arrmixValues['birth_date'] );
		}
		if( array_key_exists( 'street_line1', $arrmixValues ) ) {
			$this->setStreetLine1( $arrmixValues['street_line1'] );
		}
		if( array_key_exists( 'street_line2', $arrmixValues ) ) {
			$this->setStreetLine2( $arrmixValues['street_line2'] );
		}
		if( array_key_exists( 'street_line3', $arrmixValues ) ) {
			$this->setStreetLine3( $arrmixValues['street_line3'] );
		}
		if( array_key_exists( 'city', $arrmixValues ) ) {
			$this->setCity( $arrmixValues['city'] );
		}
		if( array_key_exists( 'state_code', $arrmixValues ) ) {
			$this->setStateCode( $arrmixValues['state_code'] );
		}
		if( array_key_exists( 'zip_5', $arrmixValues ) ) {
			$this->setZip5( $arrmixValues['zip_5'] );
		}
		if( array_key_exists( 'zip_4', $arrmixValues ) ) {
			$this->setZip4( $arrmixValues['zip_4'] );
		}
		if( array_key_exists( 'mobility_disability', $arrmixValues ) ) {
			$this->setMobilityDisability( $arrmixValues['mobility_disability'] );
		}
		if( array_key_exists( 'hearing_disability', $arrmixValues ) ) {
			$this->setHearingDisability( $arrmixValues['hearing_disability'] );
		}
		if( array_key_exists( 'visual_disability', $arrmixValues ) ) {
			$this->setVisualDisability( $arrmixValues['visual_disability'] );
		}
		if( array_key_exists( 'number_of_bedrooms', $arrmixValues ) ) {
			$this->setNumberOfBedrooms( $arrmixValues['number_of_bedrooms'] );
		}
		if( array_key_exists( 'tax_credit_bin', $arrmixValues ) ) {
			$this->setTaxCreditBin( $arrmixValues['tax_credit_bin'] );
		}

		if( array_key_exists( 'tax_credit_bin', $arrmixValues ) ) {
			$this->setTaxCreditBin( $arrmixValues['tax_credit_bin'] );
		}

		if( array_key_exists( 'transaction_type_id', $arrmixValues ) ) {
			$this->setTransactionTypeId( $arrmixValues['transaction_type_id'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityAllowanceAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>