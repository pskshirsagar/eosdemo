<?php

class CReimbursementMethod extends CBaseReimbursementMethod {

	const NO_REIMBURSEMENT								= 1;
	const REIMBURSEMENT_METHOD_USE_SUBLEDGERS			= 2;
	const STANDARD_REIMBURSEMENT_DO_NOT_USE_SUBLEDGERS	= 3;
	const BILLBACK										= 4;
	const REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW		= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>