<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyVehicles
 * Do not add any new functions to this class.
 */

class CPropertyVehicles extends CBasePropertyVehicles {

    public static function fetchAllVehiclesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
    	$strSql = ' SELECT
 						v.*,
						COALESCE( pv.property_id, NULL ) as property_id
 				   	FROM
 						vehicles v
 				   		LEFT OUTER JOIN property_vehicles pv ON ( v.id = pv.vehicle_id AND v.cid = pv.cid AND pv.property_id = ' . ( int ) $intPropertyId . ' )
 				   	WHERE
 				   		v.cid = ' . ( int ) $intCid . '
 				   		AND v.is_active = 1
 				   		AND v.is_personal = 0
 				   		AND v.deleted_on IS NULL
 				   		AND pv.property_id IS NULL
 				   	ORDER BY 
 				   	    v.name';

    	return CVehicles::fetchVehicles( $strSql, $objDatabase );
	}

	public static function fetchPropertyVehiclesByIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
 					v.*,
 					pv.property_id
 				   	FROM
 						property_vehicles pv
 				   		LEFT JOIN vehicles v ON ( v.cid = pv.cid AND v.id = pv.vehicle_id )
 				   	WHERE
 				   		v.cid = ' . ( int ) $intCid . '
 				   		AND pv.property_id = ' . ( int ) $intPropertyId . '
 				   		AND v.is_active = 1
 				   		AND v.is_personal = 0
 				   		AND v.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
    }

    public static function fetchPropertyVehiclesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
                        *
					FROM
                        (
 						    SELECT DISTINCT ON (pv.vehicle_id) ' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', v.name, v.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'v.name' ) . ' AS name,
		                        v.starting_mileage,
		                        v.license_plate_number,
		                        v.is_active,
		                        v.is_personal,
		                        pv.*,
		                        (
		                            SELECT
										COUNT(DISTINCT ( pvh.property_id ) )
		                            FROM
										property_vehicles pvh
		                            WHERE
										pvh.vehicle_id = pv.vehicle_id
										AND pvh.cid = pv.cid
										AND	pvh.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
		                        ) AS properties_count
	                        FROM
	                            property_vehicles pv
	                            JOIN vehicles v ON ( v.cid = pv.cid AND v.id = pv.vehicle_id )
	                        WHERE
	                            v.cid = ' . ( int ) $intCid . '
	                            AND pv.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
	                            AND v.is_active = 1
	                            AND v.is_personal = 0
	                            AND v.deleted_on IS NULL
 				   	        GROUP BY
	                            pv.id,
	                            pv.cid,
	                            pv.vehicle_id,
						        v.name,
						        v.starting_mileage,
						        v.license_plate_number,
						        v.is_active,
						        v.is_personal,
						        v.details
						) AS a
					ORDER BY
                        NAME';

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchAllCompanyVehiclesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;
    	$strSql = 'SELECT
 						v.*,
						COALESCE( pv.property_id, NULL ) as property_id
 				   	FROM
 						vehicles v
 				   		LEFT OUTER JOIN property_vehicles pv ON ( v.id = pv.vehicle_id AND v.cid = pv.cid AND pv.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
 				   	WHERE
 				   		v.cid = ' . ( int ) $intCid . '
 				   		AND v.is_active = 1
 				   		AND v.is_personal = 0
 				   		AND v.deleted_on IS NULL
 				   		AND pv.property_id IS NULL';

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchPropertyVehiclesByVehicleIdByPropertyIdsByCid( $intVehicleId, $arrintPropertyIds, $intCid, $objDatabase, $boolIsReturnArray = false ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
    					pv.*
    				FROM
    					property_vehicles pv
    				WHERE
    					pv.cid = ' . ( int ) $intCid . '
    					AND pv.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
    					AND pv.vehicle_id = ' . ( int ) $intVehicleId;

    	if( true == $boolIsReturnArray ) {
    		return fetchData( $strSql, $objDatabase );
     	}

        return parent::fetchPropertyVehicles( $strSql, $objDatabase );
    }

    public static function fetchUnassociatedPropertyIdsByVehicleIdByPropertyIdsByCid( $intVehicleId, $arrintPropertyIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
 						p.id
 				   	FROM
 						properties p
 				   		LEFT JOIN property_vehicles pv ON ( p.cid = pv.cid and p.id = pv.property_id and pv.vehicle_id = ' . ( int ) $intVehicleId . ' )
 				   	WHERE
 				   		p.cid = ' . ( int ) $intCid . '
 				   		AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
 				   		AND pv.id IS NULL';

    	return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );
	}

}
?>