<?php

class CSystemConfig extends CBaseSystemConfig {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>