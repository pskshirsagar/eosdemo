<?php

class CPropertyArRule extends CBasePropertyArRule {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCodeTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTriggerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArActionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStoreLeaseTerms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStoreLeaseStartWindows() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStoreUnitOptions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseNonStandardFrequencies() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseEffectiveDates() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseRateOffsets() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseArTiers() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseArMultipliers() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUsePercentages() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseUnitOptions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseMinMaxes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequireCustomerRelationship() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSeparateProspectVsRenewal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowDeactivationDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequireTriggerTypeCodes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequireTriggerCodes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEnabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>