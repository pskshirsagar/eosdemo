<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantResults
 * Do not add any new functions to this class.
 */
class CScreeningApplicantResults extends CBaseScreeningApplicantResults {

	public static function fetchScreeningApplicantResultsByScreeningApplicationRequestIdByApplicationIdByCid( $intScreeningApplicationRequestId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_applicant_results
					WHERE
						 screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						 AND application_id = ' . ( int ) $intApplicationId . '
						 AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicantResults( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = '	SELECT
						sar.*,
						a.id as applicant_id,
       					a.name_first as first_name,
       					a.name_last as last_name,
       					aa.customer_type_id
					FROM
						screening_applicant_results sar
						JOIN applicant_applications aa ON ( aa.applicant_id = sar.applicant_id AND sar.cid = aa.cid )
     					JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE
						sar.application_id = ' . ( int ) $intApplicationId . '
						AND sar.cid = ' . ( int ) $intCid . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND aa.deleted_on IS NULL';

		return parent::fetchScreeningApplicantResults( $strSql, $objDatabase );
	}

	public static function fetchTotalUnScreenedorUnApprovedApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( aa.id ) AS applicant_count
				 	 FROM
						applicant_applications aa
						LEFT JOIN screening_applicant_results sar ON ( aa.applicant_id = sar.applicant_id AND sar.cid = aa.cid AND aa.deleted_on IS NULL AND sar.screening_recommendation_type_id = 1 )
					 WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )' . '
						AND sar.id is null';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrData[0]['applicant_count'] ) ) {
			return $arrstrData[0]['applicant_count'];
		}

		return NULL;
	}

	public static function fetchScreeningApplicantResultByScreeningApplicationRequestByApplicationIdByApplicantIdByCid( $intScreeningApplicationRequestId, $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
							*
					FROM
						screening_applicant_results
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						AND applicant_id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicantResult( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByApplicationIdByApplicantIdsByCid( $intApplicationId, $arrintApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return;
		$strSql = 'SELECT
						*
					FROM
						screening_applicant_results
					WHERE
						applicant_id IN( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND application_id =  ' . ( int ) $intApplicationId . '
						AND request_status_type_id <>' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicantResults( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantResultsCountByApplicationIdByCustomerTypeIdsByScreeningRecommendationTypeIdByCid( $intApplicationId, $arrintCustomerTypeIds, $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( aa.id ) AS applicant_count
				 	 FROM
						applicant_applications aa
						JOIN screening_applicant_results sar ON ( aa.applicant_id = sar.applicant_id AND sar.cid = aa.cid AND aa.deleted_on IS NULL AND sar.screening_recommendation_type_id = ' . ( int ) $intScreeningRecommendationTypeId . ' )
					 WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrData[0]['applicant_count'] ) ) {
			return $arrstrData[0]['applicant_count'];
		}

		return NULL;
	}

	public static function fetchLatestScreeningApplicantResultByCustomerTypeIdByScreeningApplicationRequestIdByCid( $intCustomerTypeId, $intScreeningApplicationRequestId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						sar.*
					FROM
						screening_applicant_results sar						
						JOIN applicant_applications aa ON ( aa.application_id = sar.application_id AND aa.applicant_id = sar.applicant_id AND aa.cid = sar.cid AND aa.customer_type_id = ' . ( int ) $intCustomerTypeId . ' ) 
					WHERE
						sar.screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						AND sar.cid = ' . ( int ) $intCid . '						
						AND aa.deleted_on IS NULL
						AND aa.deleted_by IS NULL					
					ORDER BY sar.created_on DESC
					LIMIT 1';

		return self::fetchScreeningApplicantResult( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_applicant_results
					WHERE
						applicant_id = ' . ( int ) $intApplicantId . ' 
						AND application_id =  ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicantResult( $strSql, $objDatabase );
	}

	public static function fetchTotalScreenedApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( aa.id ) AS applicant_count
				 	 FROM
						applicant_applications aa
						LEFT JOIN screening_applicant_results sar ON ( aa.applicant_id = sar.applicant_id AND aa.application_id = sar.application_id AND sar.cid = aa.cid AND aa.deleted_on IS NULL   )
					 WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.deleted_by is NULL
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )' . '
						AND sar.id is null';

		return self::fetchColumn( $strSql, 'applicant_count', $objDatabase );
	}

	public static function fetchCountUnScreenedorUnApprovedApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, $arrintCustomerTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( aa.id ) AS applicant_count
				 	 FROM
						applicant_applications aa
						LEFT JOIN screening_applicant_results sar ON (aa.applicant_id = sar.applicant_id AND aa.application_id = sar.application_id AND sar.cid = aa.cid )
					 WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )' . ' 
						AND aa.deleted_on IS NULL 
						AND ( sar.screening_recommendation_type_id != 2 or sar.id is null )';

		return self::fetchColumn( $strSql, 'applicant_count', $objDatabase );
	}

	public static function fetchCountUnScreenedorUnApprovedApplicantsCountByApplicationIdByCustomerTypeIdsByScreeningRecommendationTypeIdByCid( $intApplicationId, $arrintCustomerTypeIds, $arrintScreeningRecommendationTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( aa.id ) AS applicant_count
				 	 FROM
						applicant_applications aa
						LEFT JOIN screening_applicant_results sar ON (aa.applicant_id = sar.applicant_id AND aa.application_id = sar.application_id AND sar.cid = aa.cid )
					 WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )' . ' 
						AND aa.deleted_on IS NULL 
						AND ( sar.screening_recommendation_type_id IN ( ' . implode( ',', $arrintScreeningRecommendationTypeIds ) . ' )' . '  or sar.id is null )';

		return self::fetchColumn( $strSql, 'applicant_count', $objDatabase );
	}

	public static function fetchScreenedApplicantCountByApplicationIdByApplicantByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intApplicantId ) || false == valId( $intCid ) )  return NULL;

		$strSql = 'SELECT
						COUNT ( id ) AS total_count
				 	 FROM
						screening_applicant_results 
					 WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_id = ' . ( int ) $intApplicantId . '
						AND application_id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'total_count', $objDatabase );
	}

	public static function fetchCustomScreeningApplicantResultsByScreeningApplicationRequestIdByApplicationIdByCid( $intScreeningApplicationRequestId, $intApplicationId, $intCid, $objDatabase ) {

		if( !valId( $intScreeningApplicationRequestId ) || !valId( $intApplicationId ) || !valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						array_to_string( array_agg( DISTINCT( error_details->>\'error_description\' ) ),\', \' ) as error_message, applicant_id
					FROM
						screening_applicant_results sar,
						jsonb_array_elements( additional_details ) error_details
					WHERE
						 screening_application_request_id = ' . ( int ) $intScreeningApplicationRequestId . '
						 AND application_id = ' . ( int ) $intApplicationId . '
						 AND cid = ' . ( int ) $intCid . '
					GROUP BY applicant_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>