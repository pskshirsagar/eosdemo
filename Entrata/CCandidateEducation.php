<?php

class CCandidateEducation extends CBaseCandidateEducation {

    public function valId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateEducationTypeId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateEducationTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_education_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCity() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getStateCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAverageGpa() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getAverageGpa() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'average_gpa', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStartDate() {
        $boolIsValid = true;

			/**
			 * Validation example
			 */

        // if( true == is_null( $this->getStartDate() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEndDate() {
        $boolIsValid = true;

			/**
			 * Validation example
			 */

        // if( true == is_null( $this->getEndDate() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDegreeDescription() {
        $boolIsValid = true;

			/**
			 * Validation example
			 */

        // if( true == is_null( $this->getDegreeDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'degree_description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSubjectOfSpecialization() {
        $boolIsValid = true;

			/**
			 * Validation example
			 */

        // if( true == is_null( $this->getSubjectOfSpecialization() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject_of_specialization', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDegreeCompleted() {
        $boolIsValid = true;

		    /**
		     * Validation example
		     */

        // if( true == is_null( $this->getDegreeCompleted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'degree_completed', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

			/**
			 * Validation example
			 */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
          		break;

          	default:
          		$boolIsValid = true;
          		break;
        }

        return $boolIsValid;
    }
}
?>