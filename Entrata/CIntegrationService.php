<?php

class CIntegrationService extends CBaseIntegrationService {

	const  MODIFY_AR_PAYMENT                            = 1;
	const  MODIFY_AR_PAYMENTS                           = 2;
	const  RETRIEVE_AR_CODE                             = 3;
	const  RETRIEVE_AR_CODES                            = 4;
	const  RETRIEVE_CUSTOMER                            = 5;
	const  RETRIEVE_CUSTOMERS                           = 6;
	const  RETRIEVE_AR_TRANSACTION                      = 7;
	const  RETRIEVE_AR_TRANSACTIONS                     = 8;
	const  RETRIEVE_MAINTENANCE_REQUEST                 = 9;
	const  RETRIEVE_MAINTENANCE_REQUESTS                = 10;
	const  RETRIEVE_PROPERTY                            = 11;
	const  RETRIEVE_PROPERTIES                          = 12;
	const  RETRIEVE_COMPANY_USER                        = 13;
	const  RETRIEVE_COMPANY_USERS                       = 14;
	const  RETRIEVE_LEAD_SOURCE                         = 15;
	const  RETRIEVE_LEAD_SOURCES                        = 16;
	const  RETRIEVE_MAINTENANCE_PROBLEM                 = 17;
	const  RETRIEVE_MAINTENANCE_PROBLEMS                = 18;
	const  RETRIEVE_PROPERTY_UNIT                       = 19;
	const  RETRIEVE_PROPERTY_UNITS                      = 20;
	const  REVERSE_AR_PAYMENT                           = 21;
	const  REVERSE_AR_PAYMENTS                          = 22;
	const  SEND_AR_CODE                                 = 23;
	const  SEND_AR_CODES                                = 24;
	const  SEND_AR_TRANSACTION                          = 25;
	const  SEND_AR_TRANSACTIONS                         = 26;
	const  SEND_LEAD_SOURCE                             = 27;
	const  SEND_LEAD_SOURCES                            = 28;
	const  SEND_MAINTENANCE_REQUEST                     = 29;
	const  SEND_MAINTENANCE_REQUESTS                    = 30;
	const  SEND_APPLICATION                             = 31;
	const  SEND_APPLICATIONS                            = 32;
	const  SEND_AR_PAYMENT                              = 33;
	const  SEND_AR_PAYMENTS                             = 34;
	const  SEND_GUEST_CARD                              = 35;
	const  SEND_GUEST_CARDS                             = 36;
	const  RETRIEVE_MAINTENANCE_PRIORITY                = 37;
	const  RETRIEVE_MAINTENANCE_PRIORITIES              = 38;
	const  RETRIEVE_MAINTENANCE_STATUS                  = 39;
	const  RETRIEVE_MAINTENANCE_STATUSES                = 40;
	const  MODIFY_REVERSED_AR_PAYMENT                   = 41;
	const  MODIFY_REVERSED_AR_PAYMENTS                  = 42;
	const  CLOSE_PAYMENT_BATCH                          = 43;
	const  RETURN_AR_PAYMENT                            = 44;
	const  RETURN_AR_PAYMENTS                           = 53;
	const  RETRIEVE_CUSTOMER_CREDIT_DATA                = 45;
	const  RETRIEVE_CUSTOMERS_CREDIT_DATA               = 46;
	const  MODIFY_EXTERNAL_REFERENCE                    = 47;
	const  MODIFY_EXTERNAL_REFERENCES                   = 48;
	const  GET_PING                                     = 49;
	const  GET_PINGS                                    = 50;
	const  UPDATE_MAINTENANCE_REQUEST                   = 51;
	const  UPDATE_MAINTENANCE_REQUESTS                  = 52;
	const  MODIFY_RETURNED_AR_PAYMENT                   = 54;
	const  MODIFY_RETURNED_AR_PAYMENTS                  = 55;
	const  UPDATE_CUSTOMER                              = 56;
	const  UPDATE_CUSTOMERS                             = 57;
	const  CLOSE_MAINTENANCE_REQUEST                    = 58;
	const  CLOSE_MAINTENANCE_REQUESTS                   = 59;
	const  RETRIEVE_AR_PAYMENT                          = 60;
	const  RETRIEVE_AR_PAYMENTS                         = 61;
	const  RETRIEVE_LEASE_EXPIRATION_LIMIT              = 62;
	const  RETRIEVE_LEASE_EXPIRATION_LIMITS             = 63;
	const  RETRIEVE_UNIT_TYPE_AVAILABILITY_AND_PRICING  = 64;
	const  RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING = 65;
	const  CLOSE_RETURNED_PAYMENT_BATCH                 = 66;
	const  RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE          = 67;
	const  RETRIEVE_PROPERTY_SPECIAL                    = 68;
	const  RETRIEVE_PROPERTY_SPECIALS                   = 69;
	const  RETRIEVE_CLOSED_MAINTENANCE_REQUEST          = 70;
	const  RETRIEVE_CLOSED_MAINTENANCE_REQUESTS         = 71;
	const  RETRIEVE_AP_PAYEES                           = 72;
	const  RETRIEVE_AP_PAYEE                            = 73;
	const  UPDATE_AP_PAYEE                              = 74;
	const  RETRIEVE_AP_TRANSACTIONS                     = 75;
	const  SEND_AP_PAYMENT                              = 76;
	const  REVERSE_AP_PAYMENT                           = 77;
	const  UPDATE_UTILITY                               = 78;
	const  SEND_SCREENING_INFO                          = 79;
	const  MODIFY_GUEST_CARD                            = 80;
	const  RETRIEVE_CUSTOMER_WITH_LEDGER                = 81;
	const  RETRIEVE_DIFFERENTIAL_DATA                   = 82;
	const  RETRIEVE_UNIT_OPTIMIZED_PRICING              = 83;
	const  RETRIEVE_UNITS_OPTIMIZED_PRICING             = 84;
	const  RETRIEVE_RENTABLE_ITEM                       = 85;
	const  RETRIEVE_RENTABLE_ITEMS                      = 86;
	const  UPDATE_LEAD_2_LEASE                          = 87;
	const  UPDATE_LEAD_TRACKING_SOLUTIONS               = 88;
	const  SEND_UTILITY_TRANSACTION                     = 89;
	const  SEND_UTILITY_TRANSACTIONS                    = 90;
	const  RETRIEVE_RENEWING_CUSTOMER                   = 91;
	const  RETRIEVE_RENEWING_CUSTOMERS                  = 92;
	const  EXECUTE_LEASE                                = 93;
	const  UPDATE_APPLICATION                           = 95;
	const  REVERSE_UTILITY_TRANSACTION                  = 96;
	const  REVERSE_UTILITY_TRANSACTIONS                 = 97;
	const  SEND_AP_TRANSACTIONS                         = 98;
	const  RETRIEVE_CUSTOMERS_ADDL_DATA                 = 99;
	const  SEND_EVENT                                   = 100;
	const  UPDATE_EVENT                                 = 101;
	const  RETRIEVE_GL_DETAILS                          = 102;
	const  RETRIEVE_GL_ACCOUNTS                         = 103;
	const  RETRIEVE_GL_ACCOUNT                          = 104;
	const  RETRIEVE_LEADS                               = 106;
	const  RETRIEVE_HISTORICAL_DATA                     = 107;
	const  RETRIEVE_SCREENING_INFO                      = 108;
	const  RETRIEVE_COMPANY_OWNERS                      = 109;
	const  RETRIEVE_OWNER_TRANSACTIONS                  = 110;
	const  RETRIEVE_LEASE_AND_GL_BALANCES               = 111;
	const  RETRIEVE_GUEST_CARDS                         = 112;
	const  RETRIEVE_LEASING_AGENTS                      = 113;
	const  RETRIEVE_LEASING_AGENT                       = 114;
	const  REBUILD_GL                                   = 115;
	const  RETRIEVE_DIFFERENTIAL_MAINTENANCE_REQUESTS   = 116;
	const  SEND_CUSTOMERS_ADDL_DATA                     = 117;
	const  EXECUTE_LEASE_RENEWAL                        = 118;
	const  RETRIEVE_DIFFERENTIAL_CUSTOMERS              = 119;
	const  RETRIEVE_AP_PAYEES_TRANSACTIONS              = 120;
	const  RETRIEVE_AR_TRANSACTIONS_AND_DEPOSITS        = 121;
	const  REVERSE_APPLICATION_CHARGES                  = 122;
	const  RETRIEVE_UNIT_TYPES_OPTIMIZED_PRICING        = 123;
	const  UPLOAD_LEASE_DOCUMENTS                       = 124;
	const  VENDOR_PROP_RESTRICTIONS                     = 125;
	const  UPLOAD_APPLICATION_DOCUMENTS                 = 126;
	const  RETRIEVE_MAINTENANCE_LOCATIONS               = 127;
	const  REAPPLY_AR_PAYMENT                           = 128;
	const  RETRIEVE_PROPERTY_FLOORPLANS                 = 129;
	const  UPDATE_LEASE                                 = 130;
	const  RETRIEVE_UTILITY_SETTINGS                    = 132;
	const  UPDATE_UTILITY_SETTINGS                      = 133;
	const  SEND_INSURANCE_TRANSACTIONS                  = 134;
	const  UPDATE_RENEWAL_APPLICATION                   = 135;
	const  SEND_QUOTE                                   = 136;
	const  RETRIEVE_RESIDENT_ADDRESS                    = 137;
	const  RETRIEVE_PROPERTY_UNIT_PRICING               = 138;
	const  RETRIEVE_PROPERTY_UNITS_PRICING              = 139;
	const  RETRIEVE_OCCUPANT_DEMOGRAPHICS               = 140;
	const  RETRIEVE_PROSPECT                            = 141;
	const  UPDATE_PROSPECT                              = 142;
	const  RETRIEVE_PAST_CUSTOMERS                      = 143;
	const  UPLOAD_RENEWAL_LEASE_DOCUMENTS               = 144;
	const  RETRIEVE_CANCELLED_GUEST_CARDS               = 145;
	const  SEND_GUEST_CARD_RECEIPT                      = 146;
	const  SEND_CANCELLED_GUEST_CARD_RECEIPT            = 147;
	const  SEND_GUEST_CARD_CANCELLED                    = 148;
	const  SEND_OFFER                                   = 149;
	const  UPDATE_OFFER_DECLINED                        = 150;
	const  UPDATE_OFFER_EXPIRED                         = 151;
	const  UPDATE_OFFER_ACCEPTED                        = 152;
	const  UPDATE_OFFER_CANCELLED                       = 153;
	const  UPDATE_MOVE_IN_DATE                          = 154;
	const  UPDATE_MOVE_IN_STATUS                        = 155;
	const  UPDATE_MOVE_IN_CANCELLED_STATUS              = 156;
	const  UPDATE_UNIT_TRANSFER_STATUS                  = 157;
	const  UPDATE_ON_NOTICE_STATUS                      = 158;
	const  UPDATE_ON_NOTICE_CANCELLED_STATUS            = 159;
	const  UPDATE_MOVE_OUT_STATUS                       = 160;
	const  UPDATE_MOVE_OUT_CANCELLED_STATUS             = 161;
	const  CREATE_SEQUENCE                              = 162;
	const  CLOSE_SEQUENCE                               = 163;
	const  TERMINATE_SEQUENCE                           = 164;
	const  UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS       = 165;
	const  OPEN_PAYMENT_BATCH                           = 166;
	const  RETRIEVE_LEAD_CONTACTS                       = 167;
	const  RETRIEVE_INTEGRATION_VERSIONS                = 168;
	const  RETRIEVE_LEASE_CHARGES                       = 169;
	const  RETRIEVE_PROPERTY_AVAILABLE_UNITS            = 170;
	const  RETRIEVE_APPLICATION_AR_CODES                = 171;
	const  ADD_NEW_CUSTOMER_TO_GUESTCARD                = 172;
	const  RETRIEVE_LEASE_OFFERS                        = 173;
	const  RETRIEVE_RENEWAL_LEASE_CHARGES               = 174;
	const  REVIEW_RECEIPT_BATCH                         = 175;
	const  RETRIEVE_MAINTENANCE_LOCATION_MAPPING_DATA	= 176;
	const  RETRIEVE_MAINTENANCE_PROBLEM_MAPPING_DATA	= 177;
	const  RETRIEVE_RESIDENT_INSURANCE_MAPPING_DATA		= 178;
	const  RETRIEVE_RENTERS_INSURANCE_POLICIES          = 179;
	const  RETRIEVE_MAINTENANCE_PRIORITY_MAPPING_DATA	= 180;
	const  RETRIEVE_MAINTENANCE_STATUS_MAPPING_DATA		= 181;
	const  SEND_RENTERS_INSURANCE_POLICIES              = 182;
	const  UPDATE_RENTERS_INSURANCE_POLICIES            = 183;
	const  CANCEL_RECEIPT_BATCH                         = 184;
	const  RETRIEVE_FLOORPLANS                          = 185;
	const  RETRIEVE_UNITTYPES                           = 186;
	const  RETRIEVE_UNITS_AVAILABILITY_AND_PRICING      = 187;
	const  UPDATE_OCCUPANCY_STATUS                      = 188;

	// Dont confiuse here this is the default call time limit to break the calls
	// const DEFAULT_CALL_TIME_LIMT 						= 90;

	public static $c_arrintIntegrationServicesCallTimeLimit = [
		CIntegrationService::UPDATE_CUSTOMER                              => 25,
		CIntegrationService::CLOSE_MAINTENANCE_REQUEST                    => 25,
		CIntegrationService::MODIFY_AR_PAYMENTS                           => 25,                    // None of the instance found for this call
		CIntegrationService::RETRIEVE_AR_CODE                             => 25,                    // None of the instance found for this call
		CIntegrationService::RETRIEVE_AR_TRANSACTION                      => 25,
		CIntegrationService::RETRIEVE_CUSTOMER                            => 30,
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUEST                 => 30,
		CIntegrationService::RETRIEVE_COMPANY_USER                        => 30,
		CIntegrationService::MODIFY_AR_PAYMENT                            => 30,
		CIntegrationService::RETRIEVE_LEAD_SOURCE                         => 30,                    // None of the instance found for this call
		CIntegrationService::RETRIEVE_LEAD_SOURCES                        => 30,
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEM                 => 30,                    // None of the instance found for this call
		CIntegrationService::RETRIEVE_PROPERTY_UNIT                       => 30,
		CIntegrationService::SEND_AR_TRANSACTIONS                         => 30,
		CIntegrationService::SEND_MAINTENANCE_REQUEST                     => 30,
		CIntegrationService::UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS       => 30,
		CIntegrationService::SEND_APPLICATION                             => 30,
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITY                => 30,
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUS                  => 30,
		CIntegrationService::UPDATE_MAINTENANCE_REQUEST                   => 30,
		CIntegrationService::RETRIEVE_PROPERTY_SPECIALS                   => 30,
		CIntegrationService::RETRIEVE_CLOSED_MAINTENANCE_REQUESTS         => 30,
		CIntegrationService::RETRIEVE_AP_PAYEE                            => 30,
		CIntegrationService::RETRIEVE_AP_TRANSACTIONS                     => 30,
		CIntegrationService::SEND_AP_PAYMENT                              => 30,
		CIntegrationService::RETRIEVE_RESIDENT_ADDRESS                    => 30,
		CIntegrationService::RETRIEVE_AR_TRANSACTIONS                     => 40,
		CIntegrationService::RETRIEVE_PROPERTY                            => 40,
		CIntegrationService::UPDATE_EVENT                                 => 50,
		CIntegrationService::RETRIEVE_LEADS                               => 50,
		CIntegrationService::EXECUTE_LEASE                                => 50,
		CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING             => 50,
		CIntegrationService::REVERSE_AR_PAYMENT                           => 60,
		CIntegrationService::REVERSE_AR_PAYMENTS                          => 60,
		CIntegrationService::SEND_GUEST_CARD                              => 60,
		CIntegrationService::RETURN_AR_PAYMENT                            => 60,
		CIntegrationService::RETRIEVE_CUSTOMERS_ADDL_DATA                 => 60,
		CIntegrationService::SEND_EVENT                                   => 60,
		CIntegrationService::SEND_QUOTE                                   => 60,
		CIntegrationService::RETRIEVE_AR_CODES                            => 80,
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUESTS                => 90,
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEMS                => 90,
		CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS                  => 90,
		CIntegrationService::RETRIEVE_AP_PAYEES                           => 100,
		CIntegrationService::RETRIEVE_COMPANY_USERS                       => 120,
		CIntegrationService::RETRIEVE_RENTABLE_ITEMS                      => 130,
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITIES              => 140,
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUSES                => 140,
		CIntegrationService::RETRIEVE_PROPERTIES                          => 150,
		CIntegrationService::RETRIEVE_PROPERTY_UNITS                      => 300,
		CIntegrationService::RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING => 150,
		CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE          => 150,
		CIntegrationService::RETRIEVE_CUSTOMER_WITH_LEDGER                => 150,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_DATA                   => 150,
		CIntegrationService::RETRIEVE_RENTABLE_ITEMS                      => 150,
		CIntegrationService::RETRIEVE_CUSTOMERS                           => 300,
		CIntegrationService::SEND_AR_PAYMENT                              => 300,
		CIntegrationService::CLOSE_PAYMENT_BATCH                          => 300,
		CIntegrationService::OPEN_PAYMENT_BATCH                           => 30
	];

	public static $c_arrintOptimumIntegrationServicesCallTimeLimit = [
		CIntegrationService::RETRIEVE_CUSTOMER             => [
			CIntegrationClientType::YARDI     => 10,
			CIntegrationClientType::MRI       => 5,
			CIntegrationClientType::AMSI      => 5,
			CIntegrationClientType::JENARK    => 5,
			CIntegrationClientType::REAL_PAGE => 5
		],
		CIntegrationService::RETRIEVE_AR_TRANSACTIONS      => [
			CIntegrationClientType::YARDI     => 10,
			CIntegrationClientType::MRI       => 5,
			CIntegrationClientType::AMSI      => 5,
			CIntegrationClientType::REAL_PAGE => 5
		],
		CIntegrationService::RETRIEVE_CUSTOMER_WITH_LEDGER => [
			CIntegrationClientType::YARDI => 10,
			CIntegrationClientType::MRI   => 5,
			CIntegrationClientType::AMSI  => 5
		],
	];

	public static $c_arrintIntegrationServicesToBeQueued = [

		CIntegrationService::SEND_AR_PAYMENT,
		CIntegrationService::MODIFY_AR_PAYMENT,
		CIntegrationService::RETURN_AR_PAYMENT,
		CIntegrationService::MODIFY_RETURNED_AR_PAYMENT,
		CIntegrationService::REVERSE_AR_PAYMENT,
		CIntegrationService::MODIFY_REVERSED_AR_PAYMENT,
		CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH,
		CIntegrationService::CLOSE_PAYMENT_BATCH,
		CIntegrationService::SEND_AR_TRANSACTION,
		CIntegrationService::SEND_AR_TRANSACTIONS,
		CIntegrationService::SEND_UTILITY_TRANSACTIONS,
		CIntegrationService::SEND_MAINTENANCE_REQUEST,
		CIntegrationService::UPDATE_MAINTENANCE_REQUEST,
		CIntegrationService::CLOSE_MAINTENANCE_REQUEST,
		CIntegrationService::SEND_APPLICATION,
		CIntegrationService::SEND_GUEST_CARD,
		CIntegrationService::MODIFY_GUEST_CARD,
		CIntegrationService::SEND_SCREENING_INFO,
		CIntegrationService::UPDATE_APPLICATION,
		CIntegrationService::UPDATE_CUSTOMER,
		CIntegrationService::EXECUTE_LEASE,
		CIntegrationService::SEND_EVENT,
		CIntegrationService::SEND_QUOTE,
		CIntegrationService::UPDATE_EVENT,
		CIntegrationService::SEND_CUSTOMERS_ADDL_DATA,
		CIntegrationService::EXECUTE_LEASE_RENEWAL,
		CIntegrationService::REVERSE_APPLICATION_CHARGES,
		CIntegrationService::UPLOAD_APPLICATION_DOCUMENTS,
		CIntegrationService::SEND_INSURANCE_TRANSACTIONS,
		CIntegrationService::UPDATE_PROSPECT,
		CIntegrationService::UPLOAD_LEASE_DOCUMENTS,
		CIntegrationService::UPLOAD_RENEWAL_LEASE_DOCUMENTS,
		CIntegrationService::UPDATE_MOVE_IN_STATUS,
		CIntegrationService::UPDATE_MOVE_IN_CANCELLED_STATUS,
		CIntegrationService::UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS,
		CIntegrationService::UPDATE_OFFER_ACCEPTED,
		CIntegrationService::UPDATE_OFFER_CANCELLED,
		CIntegrationService::UPDATE_MOVE_IN_DATE,
		CIntegrationService::SEND_CANCELLED_GUEST_CARD_RECEIPT,
		CIntegrationService::SEND_GUEST_CARD_RECEIPT,
		CIntegrationService::SEND_OFFER,
		CIntegrationService::UPDATE_OFFER_DECLINED,
		CIntegrationService::UPDATE_OFFER_EXPIRED,
		CIntegrationService::UPDATE_UNIT_TRANSFER_STATUS,
		CIntegrationService::UPDATE_ON_NOTICE_STATUS,
		CIntegrationService::UPDATE_ON_NOTICE_CANCELLED_STATUS,
		CIntegrationService::UPDATE_MOVE_OUT_STATUS,
		CIntegrationService::UPDATE_MOVE_OUT_CANCELLED_STATUS,
		CIntegrationService::ADD_NEW_CUSTOMER_TO_GUESTCARD,
		CIntegrationService::SEND_RENTERS_INSURANCE_POLICIES,
		CIntegrationService::UPDATE_RENTERS_INSURANCE_POLICIES,
		CIntegrationService::UPDATE_OCCUPANCY_STATUS
	];

	public static $c_arrintFinancialIntegrationServiceIds = [
		CIntegrationService::MODIFY_AR_PAYMENT,
		CIntegrationService::MODIFY_AR_PAYMENTS,
		CIntegrationService::SEND_AR_TRANSACTION,
		CIntegrationService::SEND_AR_TRANSACTIONS,
		CIntegrationService::SEND_UTILITY_TRANSACTIONS,
		CIntegrationService::SEND_AR_PAYMENT,
		CIntegrationService::SEND_AR_PAYMENTS,
		CIntegrationService::MODIFY_REVERSED_AR_PAYMENT,
		CIntegrationService::MODIFY_REVERSED_AR_PAYMENTS,
		CIntegrationService::RETURN_AR_PAYMENT,
		CIntegrationService::REVERSE_AR_PAYMENT,
		CIntegrationService::CLOSE_PAYMENT_BATCH,
		CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH,
		CIntegrationService::SEND_INSURANCE_TRANSACTIONS,
		CIntegrationService::OPEN_PAYMENT_BATCH
	];

	public static $c_arrintIntegrationServiceNameById = [
		CIntegrationService::MODIFY_AR_PAYMENT                            => 'modifyArPayment',
		CIntegrationService::MODIFY_AR_PAYMENTS                           => 'modifyArPayments',
		CIntegrationService::RETRIEVE_AR_CODE                             => 'retrieveChargeCode',
		CIntegrationService::RETRIEVE_AR_CODES                            => 'retrieveChargeCodes',
		CIntegrationService::RETRIEVE_CUSTOMER                            => 'retrieveCustomer',
		CIntegrationService::RETRIEVE_CUSTOMERS                           => 'retrieveCustomers',
		CIntegrationService::RETRIEVE_PAST_CUSTOMERS                      => 'retrievePastCustomersOnVacantUnits',
		CIntegrationService::RETRIEVE_AR_TRANSACTION                      => 'retrieveLeaseTransaction',
		CIntegrationService::RETRIEVE_AR_TRANSACTIONS                     => 'retrieveArTransactions',
		CIntegrationService::RETRIEVE_LEAD_CONTACTS                       => 'retrieveLeadContacts',
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUEST                 => 'retrieveMaintenanceRequest',
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUESTS                => 'retrieveMaintenanceRequests',
		CIntegrationService::RETRIEVE_PROPERTY                            => 'retrieveProperty',
		CIntegrationService::RETRIEVE_PROPERTIES                          => 'retrieveProperties',
		CIntegrationService::RETRIEVE_COMPANY_USER                        => 'retrieveCompanyUser',
		CIntegrationService::RETRIEVE_COMPANY_USERS                       => 'retrieveCompanyUsers',
		CIntegrationService::RETRIEVE_LEAD_SOURCE                         => 'retrieveLeadSource',
		CIntegrationService::RETRIEVE_LEAD_SOURCES                        => 'retrieveLeadSources',
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEM                 => 'retrieveMaintenanceProblem',
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEMS                => 'retrieveMaintenanceProblems',
		CIntegrationService::RETRIEVE_PROPERTY_UNIT                       => 'retrievePropertyUnit',
		CIntegrationService::RETRIEVE_PROPERTY_UNITS                      => 'retrievePropertyUnits',
		CIntegrationService::REVERSE_AR_PAYMENT                           => 'reverseArPayment',
		CIntegrationService::REVERSE_AR_PAYMENTS                          => 'reverseArPayments',
		CIntegrationService::SEND_AR_CODE                                 => 'sendChargeCode',
		CIntegrationService::SEND_AR_CODES                                => 'sendChargeCodes',
		CIntegrationService::SEND_AR_TRANSACTION                          => 'sendLeaseTransaction',
		CIntegrationService::SEND_AR_TRANSACTIONS                         => 'sendArTransactions',
		CIntegrationService::SEND_LEAD_SOURCE                             => 'sendLeadSource',
		CIntegrationService::SEND_LEAD_SOURCES                            => 'sendLeadSources',
		CIntegrationService::SEND_MAINTENANCE_REQUEST                     => 'sendMaintenanceRequest',
		CIntegrationService::SEND_MAINTENANCE_REQUESTS                    => 'sendMaintenanceRequests',
		CIntegrationService::SEND_APPLICATION                             => 'sendApplication',
		CIntegrationService::SEND_APPLICATIONS                            => 'sendApplications',
		CIntegrationService::SEND_AR_PAYMENT                              => 'sendArPayment',
		CIntegrationService::REAPPLY_AR_PAYMENT                           => 'reApplyArPayment',
		CIntegrationService::SEND_AR_PAYMENTS                             => 'sendArPayments',
		CIntegrationService::SEND_GUEST_CARD                              => 'sendGuestCard',
		CIntegrationService::SEND_GUEST_CARDS                             => 'sendGuestCards',
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITY                => 'retrieveMaintenancePriority',
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITIES              => 'retrieveMaintenancePriorities',
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUS                  => 'retrieveMaintenanceStatus',
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUSES                => 'retrieveMaintenanceStatusTypes',
		CIntegrationService::MODIFY_REVERSED_AR_PAYMENT                   => 'modifyReversedArPayment',
		CIntegrationService::MODIFY_REVERSED_AR_PAYMENTS                  => 'modifyReversedArPayments',
		CIntegrationService::CLOSE_PAYMENT_BATCH                          => 'closePaymentBatch',
		CIntegrationService::RETURN_AR_PAYMENT                            => 'returnArPayment',
		CIntegrationService::RETURN_AR_PAYMENTS                           => 'returnArPayments',
		CIntegrationService::RETRIEVE_CUSTOMER_CREDIT_DATA                => 'retrieveCustomerCreditData',
		CIntegrationService::RETRIEVE_CUSTOMERS_CREDIT_DATA               => 'retrieveCustomersCreditData',
		CIntegrationService::MODIFY_EXTERNAL_REFERENCE                    => 'modifyExternalReference',
		CIntegrationService::MODIFY_EXTERNAL_REFERENCES                   => 'modifyExternalReferences',
		CIntegrationService::GET_PING                                     => 'getPing',
		CIntegrationService::GET_PINGS                                    => 'getPings',
		CIntegrationService::UPDATE_MAINTENANCE_REQUEST                   => 'updateMaintenanceRequest',
		CIntegrationService::UPDATE_MAINTENANCE_REQUESTS                  => 'updateMaintenanceRequests',
		CIntegrationService::MODIFY_RETURNED_AR_PAYMENT                   => 'modifyReturnedArPayment',
		CIntegrationService::MODIFY_RETURNED_AR_PAYMENTS                  => 'modifyReturnedArPayments',
		CIntegrationService::UPDATE_CUSTOMER                              => 'updateCustomer',
		CIntegrationService::UPDATE_CUSTOMERS                             => 'updateCustomers',
		CIntegrationService::CLOSE_MAINTENANCE_REQUEST                    => 'closeMaintenanceRequest',
		CIntegrationService::CLOSE_MAINTENANCE_REQUESTS                   => 'closeMaintenanceRequests',
		CIntegrationService::RETRIEVE_AR_PAYMENT                          => 'retrieveArPayment',
		CIntegrationService::RETRIEVE_AR_PAYMENTS                         => 'retrieveArPayments',
		CIntegrationService::RETRIEVE_LEASE_EXPIRATION_LIMIT              => 'retrieveLeaseExpirationLimit',
		CIntegrationService::RETRIEVE_LEASE_EXPIRATION_LIMITS             => 'retrieveLeaseExpirationLimits',
		CIntegrationService::RETRIEVE_UNIT_TYPE_AVAILABILITY_AND_PRICING  => 'retrieveUnitTypeAvailabilityAndPricing',
		CIntegrationService::RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING => 'retrieveUnitTypesAvailabilityAndPricing',
		CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH                 => 'closeReturnedPaymentBatch',
		CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE          => 'retrieveCustomersWithBalanceDue',
		CIntegrationService::RETRIEVE_PROPERTY_SPECIAL                    => 'retrievePropertySpecial',
		CIntegrationService::RETRIEVE_PROPERTY_SPECIALS                   => 'retrievePropertySpecials',
		CIntegrationService::RETRIEVE_CLOSED_MAINTENANCE_REQUEST          => 'retrieveClosedMaintenanceRequest',
		CIntegrationService::RETRIEVE_CLOSED_MAINTENANCE_REQUESTS         => 'retrieveClosedMaintenanceRequests',
		CIntegrationService::RETRIEVE_AP_PAYEES                           => 'retrieveApPayees',
		CIntegrationService::RETRIEVE_AP_PAYEE                            => 'retrieveApPayee',
		CIntegrationService::UPDATE_AP_PAYEE                              => 'updateApPayee',
		CIntegrationService::RETRIEVE_AP_TRANSACTIONS                     => 'retrieveApTransactions',
		CIntegrationService::SEND_AP_PAYMENT                              => 'sendApPayment',
		CIntegrationService::REVERSE_AP_PAYMENT                           => '',
		CIntegrationService::UPDATE_UTILITY                               => '',
		CIntegrationService::SEND_SCREENING_INFO                          => 'sendScreeningInfo',
		CIntegrationService::MODIFY_GUEST_CARD                            => 'modifyGuestCard',
		CIntegrationService::RETRIEVE_CUSTOMER_WITH_LEDGER                => 'retrieveCustomerWithLedger',
		CIntegrationService::RETRIEVE_DIFFERENTIAL_DATA                   => 'retrieveDifferentialData',
		CIntegrationService::RETRIEVE_UNIT_OPTIMIZED_PRICING              => 'retrieveUnitOptimizedPricing',
		CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING             => 'retrieveUnitsOptimizedPricing',
		CIntegrationService::RETRIEVE_RENTABLE_ITEM                       => 'retrieveRentableItem',
		CIntegrationService::RETRIEVE_RENTABLE_ITEMS                      => 'retrieveRentableItems',
		CIntegrationService::UPDATE_LEAD_2_LEASE                          => 'updateLead2Lease',
		CIntegrationService::UPDATE_LEAD_TRACKING_SOLUTIONS               => 'updateLeadTrackingSolutions',
		CIntegrationService::SEND_UTILITY_TRANSACTION                     => 'sendUtilityTransaction',
		CIntegrationService::SEND_UTILITY_TRANSACTIONS                    => 'sendUtilityTransactions',
		CIntegrationService::RETRIEVE_RENEWING_CUSTOMER                   => 'retrieveRenewingCustomer',
		CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS                  => 'retrieveRenewingCustomers',
		CIntegrationService::EXECUTE_LEASE                                => 'executeLease',
		CIntegrationService::UPDATE_APPLICATION                           => 'updateApplication',
		CIntegrationService::REVERSE_UTILITY_TRANSACTION                  => 'reverseUtilityTransaction',
		CIntegrationService::REVERSE_UTILITY_TRANSACTIONS                 => 'reverseUtilityTransactions',
		CIntegrationService::SEND_AP_TRANSACTIONS                         => 'sendApTransactions',
		CIntegrationService::RETRIEVE_CUSTOMERS_ADDL_DATA                 => 'retrieveCustomersAddlData',
		CIntegrationService::SEND_EVENT                                   => 'sendEvent',
		CIntegrationService::UPDATE_EVENT                                 => 'updateEvent',
		CIntegrationService::RETRIEVE_GL_DETAILS                          => 'retrieveGlDetails',
		CIntegrationService::RETRIEVE_GL_ACCOUNTS                         => 'retrieveGlAccounts',
		CIntegrationService::RETRIEVE_GL_ACCOUNT                          => 'retrieveGlAccount',
		CIntegrationService::RETRIEVE_LEADS                               => 'retrieveLeads',
		CIntegrationService::RETRIEVE_HISTORICAL_DATA                     => 'retrieveHistoricalData',
		CIntegrationService::RETRIEVE_SCREENING_INFO                      => 'retrieveScreeningInfo',
		CIntegrationService::RETRIEVE_COMPANY_OWNERS                      => 'retrieveCompanyOwners',
		CIntegrationService::RETRIEVE_OWNER_TRANSACTIONS                  => 'retrieveOwnerTransactions',
		CIntegrationService::RETRIEVE_LEASE_AND_GL_BALANCES               => 'retrieveLeaseAndGlBalances',
		CIntegrationService::RETRIEVE_GUEST_CARDS                         => 'retrieveGuestCards',
		CIntegrationService::RETRIEVE_LEASING_AGENTS                      => 'retrieveLeasingAgents',
		CIntegrationService::RETRIEVE_LEASING_AGENT                       => 'retrieveLeasingAgent',
		CIntegrationService::REBUILD_GL                                   => 'rebuildGl',
		CIntegrationService::RETRIEVE_DIFFERENTIAL_MAINTENANCE_REQUESTS   => 'retrieveDifferentialMaintenanceRequests',
		CIntegrationService::SEND_CUSTOMERS_ADDL_DATA                     => 'sendCustomerAddlData',
		CIntegrationService::EXECUTE_LEASE_RENEWAL                        => 'executeLeaseRenewal',
		CIntegrationService::RETRIEVE_DIFFERENTIAL_CUSTOMERS              => 'retrieveDifferentialCustomers',
		CIntegrationService::RETRIEVE_AP_PAYEES_TRANSACTIONS              => 'retrieveApPayeesTransactions',
		CIntegrationService::RETRIEVE_AR_TRANSACTIONS_AND_DEPOSITS        => 'retrieveArTransactionsAndDeposits',
		CIntegrationService::REVERSE_APPLICATION_CHARGES                  => 'reverseApplicationCharges',
		CIntegrationService::RETRIEVE_UNIT_TYPES_OPTIMIZED_PRICING        => 'retrieveUnitTypesOptimizedPricing',
		CIntegrationService::UPLOAD_LEASE_DOCUMENTS                       => 'uploadLeaseDocuments',
		CIntegrationService::RETRIEVE_MAINTENANCE_LOCATIONS               => 'retrieveMaintenanceLocations',
		CIntegrationService::RETRIEVE_PROPERTY_FLOORPLANS                 => 'retrievePropertyFloorplans',
		CIntegrationService::UPLOAD_APPLICATION_DOCUMENTS                 => 'uploadApplicationDocuments',
		CIntegrationService::UPDATE_RENEWAL_APPLICATION                   => 'updateRenewalApplication',
		CIntegrationService::RETRIEVE_UTILITY_SETTINGS                    => 'retrieveUtilitySettings',
		CIntegrationService::UPDATE_UTILITY_SETTINGS                      => 'updateUtilitySettings',
		CIntegrationService::UPDATE_LEASE                                 => 'updateLease',
		CIntegrationService::SEND_QUOTE                                   => 'sendQuote',
		CIntegrationService::RETRIEVE_RESIDENT_ADDRESS                    => 'retrieveResidentAddress',
		CIntegrationService::RETRIEVE_PROSPECT                            => 'prospectSearch',
		CIntegrationService::UPDATE_PROSPECT                              => 'updateProspect',
		CIntegrationService::RETRIEVE_PROPERTY_AVAILABLE_UNITS            => 'retrievePropertyAvailableUnitsAndPricing',
		CIntegrationService::UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS       => 'UploadMaintenanceRequestAttachments',
		CIntegrationService::OPEN_PAYMENT_BATCH                           => 'openPaymentBatch',
		CIntegrationService::RETRIEVE_INTEGRATION_VERSIONS                => 'retrieveIntegrationVersions',
		CIntegrationService::RETRIEVE_LEASE_CHARGES				          => 'retrieveLeaseCharges',
		CIntegrationService::RETRIEVE_APPLICATION_AR_CODES                => 'retrieveApplicationChargeCodes',
		CIntegrationService::REVIEW_RECEIPT_BATCH                         => 'reviewReceiptBatch',
		CIntegrationService::SEND_RENTERS_INSURANCE_POLICIES              => 'sendRentersInsurancePolicies',
		CIntegrationService::UPDATE_RENTERS_INSURANCE_POLICIES            => 'updateRentersInsurancePolicies',
		CIntegrationService::CANCEL_RECEIPT_BATCH                         => 'cancelReceiptBatch',
		CIntegrationService::RETRIEVE_FLOORPLANS                          => 'retrieveFloorplans',
		CIntegrationService::RETRIEVE_UNITTYPES                           => 'retrieveUnitTypes',
		CIntegrationService::RETRIEVE_UNITS_AVAILABILITY_AND_PRICING      => 'retrieveUnitsAvailabilityAndPricing'
	];

	public static $c_arrintIntegrationServicesToBeAMQPQueued = [
		CIntegrationService::SEND_CUSTOMERS_ADDL_DATA,
		CIntegrationService::SEND_MAINTENANCE_REQUEST,
		CIntegrationService::UPDATE_MAINTENANCE_REQUEST,
		CIntegrationService::CLOSE_MAINTENANCE_REQUEST,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_DATA,
		CIntegrationService::RETRIEVE_GUEST_CARDS,
		CIntegrationService::RETRIEVE_AP_PAYEES,
		CIntegrationService::RETRIEVE_AP_PAYEES_TRANSACTIONS,
		CIntegrationService::RETRIEVE_AP_TRANSACTIONS,
		CIntegrationService::RETRIEVE_PROPERTY_UNIT,
		CIntegrationService::RETRIEVE_PROPERTY_UNITS,
		CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING,
		CIntegrationService::RETRIEVE_COMPANY_USER,
		CIntegrationService::RETRIEVE_COMPANY_USERS,
		CIntegrationService::EXECUTE_LEASE,
		CIntegrationService::EXECUTE_LEASE_RENEWAL,
		CIntegrationService::UPDATE_LEASE,
		CIntegrationService::RETRIEVE_CLOSED_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITIES,
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEMS,
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUSES,
		CIntegrationService::RETRIEVE_AR_CODES,
		CIntegrationService::RETRIEVE_LEAD_SOURCES,
		CIntegrationService::RETRIEVE_PROPERTY,
		CIntegrationService::RETRIEVE_PROPERTY_SPECIALS,
		CIntegrationService::RETRIEVE_RENTABLE_ITEMS,
		CIntegrationService::RETRIEVE_PROPERTIES,
		CIntegrationService::RETRIEVE_LEASING_AGENTS,
		CIntegrationService::RETRIEVE_PROPERTY_FLOORPLANS,
		CIntegrationService::RETRIEVE_CUSTOMERS_ADDL_DATA,
		CIntegrationService::SEND_AR_TRANSACTIONS,
		CIntegrationService::RETRIEVE_CUSTOMERS,
		CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_CUSTOMERS,
		CIntegrationService::SEND_EVENT,
		CIntegrationService::UPDATE_EVENT,
		CIntegrationService::MODIFY_GUEST_CARD,
		CIntegrationService::SEND_GUEST_CARD,
		CIntegrationService::SEND_SCREENING_INFO,
		CIntegrationService::UPDATE_APPLICATION,
		CIntegrationService::UPLOAD_APPLICATION_DOCUMENTS,
		CIntegrationService::RETURN_AR_PAYMENT,
		CIntegrationService::REVERSE_AR_PAYMENT,
		CIntegrationService::SEND_AR_PAYMENT,
		CIntegrationService::SEND_AR_PAYMENTS
	];

	public static $c_arrintSyncTypeServicesScheduledToBeAMQPQueued = [
		CIntegrationService::RETRIEVE_DIFFERENTIAL_DATA,
		CIntegrationService::RETRIEVE_GUEST_CARDS,
		CIntegrationService::RETRIEVE_AP_PAYEES,
		CIntegrationService::RETRIEVE_AP_PAYEES_TRANSACTIONS,
		CIntegrationService::RETRIEVE_AP_TRANSACTIONS,
		CIntegrationService::RETRIEVE_PROPERTY_UNIT,
		CIntegrationService::RETRIEVE_PROPERTY_UNITS,
		CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING,
		CIntegrationService::RETRIEVE_COMPANY_USER,
		CIntegrationService::RETRIEVE_COMPANY_USERS,
		CIntegrationService::RETRIEVE_CLOSED_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITIES,
		CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEMS,
		CIntegrationService::RETRIEVE_MAINTENANCE_REQUESTS,
		CIntegrationService::RETRIEVE_MAINTENANCE_STATUSES,
		CIntegrationService::RETRIEVE_AR_CODES,
		CIntegrationService::RETRIEVE_LEAD_SOURCES,
		CIntegrationService::RETRIEVE_PROPERTY,
		CIntegrationService::RETRIEVE_PROPERTY_SPECIALS,
		CIntegrationService::RETRIEVE_RENTABLE_ITEMS,
		CIntegrationService::RETRIEVE_PROPERTIES,
		CIntegrationService::RETRIEVE_LEASING_AGENTS,
		CIntegrationService::RETRIEVE_PROPERTY_FLOORPLANS,
		CIntegrationService::RETRIEVE_CUSTOMERS_ADDL_DATA,
		CIntegrationService::RETRIEVE_CUSTOMERS,
		CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS,
		CIntegrationService::RETRIEVE_DIFFERENTIAL_CUSTOMERS

	];
}

?>