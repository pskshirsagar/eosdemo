<?php

class CImportRequestReport extends CBaseImportRequestReport {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportEntityReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsChecked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerifiedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>