<?php

class CCustomerDataType extends CBaseCustomerDataType {
	const INCOME				= 1;
	const ASSET					= 2;
	const EXPENSE				= 3;
	const SSN					= 4;
	const CITIZENSHIP			= 5;
	const BIRTHDATE				= 6;
	const DISABILITY			= 7;
	const VETERAN				= 8;
	const JOINT_DEPENDENT		= 9;
	const STUDENT				= 10;
	const ASSISTANCE_ANIMAL		= 11;
	const DISPLACEMENT			= 12;
	const DISASTER				= 13;
	const POLICE_OFFICER		= 14;
	const NAME_FIRST			= 15;
	const NAME_LAST				= 16;
	const USERNAME				= 17;
	const PRIMARY_PHONE			= 18;
	const PRIMARY_LEAD_SOURCE	= 19;
	const OCCUPANTS				= 20;
	const PETS					= 21;
	const APPOINTMENT			= 22;
	const CUSTOMER_ADDRESS		= 23;

	public static $c_arrintCustomerDataTypeIds = [ self::INCOME, self::ASSET, self::EXPENSE, self::SSN, self::CITIZENSHIP, self::BIRTHDATE, self::DISABILITY, self::VETERAN, self::JOINT_DEPENDENT, self::STUDENT, self::ASSISTANCE_ANIMAL, self::DISPLACEMENT, self::DISASTER, self::POLICE_OFFICER ];
	public static $c_arrintMemberStepCustomerDataTypeIds = [ self::SSN, self::CITIZENSHIP, self::BIRTHDATE, self::DISABILITY, self::VETERAN, self::STUDENT, self::DISASTER, self::POLICE_OFFICER, self::JOINT_DEPENDENT ];
	public static $c_arrintPetStepCustomerDataTypeIds = [ self::ASSISTANCE_ANIMAL ];
	public static $c_arrintFinancialStepCustomerDataTypeIds = [ self::INCOME, self::ASSET,self::EXPENSE ];
	public static $c_arrintHouseHoldStepCustomerDataTypeIds = [ self::DISPLACEMENT ];
	public static $c_arrintCustomerDataTypeIdsWithoutVerificationType = [ self::VETERAN, self::DISASTER, self::POLICE_OFFICER, self::JOINT_DEPENDENT ];

	public static $c_arrstrCustomerDataTypeIdToStr = [
		self::INCOME 				=> 'Income',
		self::ASSET 				=> 'Asset',
		self::EXPENSE 				=> 'Expense',
		self::SSN 					=> 'SSN',
		self::CITIZENSHIP 			=> 'Citizenship',
		self::BIRTHDATE 			=> 'Birth Date',
		self::DISABILITY 			=> 'Disability',
		self::VETERAN 				=> 'Veteran',
		self::JOINT_DEPENDENT 		=> 'Joint Dependent',
		self::STUDENT 				=> 'Student',
		self::ASSISTANCE_ANIMAL 	=> 'Pet/Assistance Animal',
		self::DISPLACEMENT 			=> 'Displacement',
		self::DISASTER 				=> 'Disaster',
		self::POLICE_OFFICER		=> 'Police/Security Officer',
	];

	public static $c_arrstrLeasingCenterCustomerDataTypes = [
		'customer_data_type_first_name'				=> self::NAME_FIRST,
		'customer_data_type_last_name'				=> self::NAME_LAST,
		'customer_data_type_username'				=> self::USERNAME,
		'customer_data_type_primary_phone'			=> self::PRIMARY_PHONE,
		'customer_data_type_primary_lead_source'	=> self::PRIMARY_LEAD_SOURCE,
		'customer_data_type_occupants'				=> self::OCCUPANTS,
		'customer_data_type_pets'					=> self::PETS,
		'customer_data_type_appointment'			=> self::APPOINTMENT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationPreferenceKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function getFileType( $intCustomerDataTypeId ) {

		switch( $intCustomerDataTypeId ) {

			case CCustomerDataType::INCOME:
				return CFileType::SYSTEM_CODE_PROOF_OF_INCOME;

			case CCustomerDataType::ASSET:
				return CFileType::SYSTEM_CODE_PROOF_OF_ASSETS;

			case CCustomerDataType::EXPENSE:
				return CFileType::SYSTEM_CODE_PROOF_OF_EXPENSES;

			case CCustomerDataType::SSN:
				return CFileType::SYSTEM_CODE_SSN_VERIFICATION;

			case CCustomerDataType::CITIZENSHIP:
				return CFileType::SYSTEM_CODE_CITIZENSHIP_VERIFICATION;

			case CCustomerDataType::BIRTHDATE:
				return CFileType::SYSTEM_CODE_DOB_VERIFICATION;

			default:
				return CFileType::SYSTEM_CODE_VERIFICATION_DOCUMENT;
		}
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>