<?php

class CClub extends CBaseClub {

	protected $m_boolIsClubMember;
	protected $m_boolIsClubCreator;

	protected $m_intCustomerClubId;
	protected $m_intMembersCount;

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPropertyName;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClubMember = false;
		$this->m_boolIsClubCreator = false;
	}

	/**
	 * Get Functions
	 */

	public function getIsClubMember() {
		return $this->m_boolIsClubMember;
	}

	public function getIsClubCreator() {
		return $this->m_boolIsClubCreator;
	}

	public function getCustomerClubId() {
		return $this->m_intCustomerClubId;
	}

	public function getMembersCount() {
		return $this->m_intMembersCount;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * Set Functions
	 */

	public function setIsClubMember( $boolIsClubMember ) {
		$this->m_boolIsClubMember = $boolIsClubMember;
	}

	public function setIsClubCreator( $boolIsClubCreator ) {
		$this->m_boolIsClubCreator = $boolIsClubCreator;
	}

	public function setCustomerClubId( $intCustomerClubId ) {
		$this->m_intCustomerClubId = $intCustomerClubId;
	}

	public function setMembersCount( $intMembersCount ) {
		$this->m_intMembersCount = $intMembersCount;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst 	= $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast 	= $strNameLast;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	 public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_club_member'] ) )		$this->setIsClubMember( $arrmixValues['is_club_member'] );
		if( true == isset( $arrmixValues['is_club_creator'] ) )	$this->setIsClubCreator( $arrmixValues['is_club_creator'] );
		if( true == isset( $arrmixValues['customer_club_id'] ) )	$this->setCustomerClubId( $arrmixValues['customer_club_id'] );
		if( true == isset( $arrmixValues['members_count'] ) )		$this->setMembersCount( $arrmixValues['members_count'] );
		if( true == isset( $arrmixValues['name_first'] ) )			$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )			$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['property_name'] ) )		$this->setPropertyName( $arrmixValues['property_name'] );
	 }

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Club name is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Club name is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strDescription ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'description', __( 'Club description is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > strlen( $this->m_strDescription ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'description', __( 'Club description is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_approve':
			case 'validate_reject':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valName();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCustomerClubs( $objDatabase ) {
		return CCustomerClubs::fetchCustomerClubsByClubIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerClubByCustomerId( $intCustomerId, $objDatabase ) {
		return CCustomerClubs::fetchCustomerClubByCustomerIdByClubIdByCid( $intCustomerId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaType( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByClubIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchMembersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objCustomerCommunityFilter = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereCondition			= NULL;
		$arrintLeaseStatusTypeIds	= [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ];

		if( true == valObj( $objCustomerCommunityFilter, 'CCustomerCommunityFilter' ) && true == valStr( $objCustomerCommunityFilter->getResidentName() ) ) {
			$arrstrExplodedResidentNames = explode( ' ', trim( $objCustomerCommunityFilter->getResidentName() ) );
			if( true == valArr( $arrstrExplodedResidentNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrExplodedResidentNames ) ) {
				$arrstrOrParameters = [];
				foreach( $arrstrExplodedResidentNames as $strExplodedResidentName ) {
					if( true === strrchr( $strExplodedResidentName, '\'' ) ) {
						$strFilteredExplodedResidentName = $strExplodedResidentName;
					} else {
						$strFilteredExplodedResidentName = '\' || \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strExplodedResidentName ) ) . '\' || \'';
					}
					$arrstrOrParameters[] = $strFilteredExplodedResidentName;
				}

				$arrstrOrParameters = array_filter( $arrstrOrParameters );

				if( true == valArr( $arrstrOrParameters ) ) {
					$strWhereCondition = 'AND ( c.name_first ILIKE E\'%' . implode( '%\' OR c.name_first ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
					$strWhereCondition .= ' AND ( c.name_last ILIKE E\'%' . implode( '%\' OR c.name_last ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
				}
			} else {
				$strWhereCondition = 'AND ( lower( c.name_first ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' OR lower( c.name_last ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' )';
			}
		}

		$strSql = '
					SELECT
						c.*,
						cs.value as is_opt_out_of_apartmates,
						cc.join_datetime as customer_event_created_on,
						cpn.phone_number
					FROM
						lease_customers as lc
						JOIN leases as l ON l.id = lc.lease_id AND l.cid = lc.cid
						JOIN customers as c ON c.id = lc.customer_id AND c.cid = lc.cid
						LEFT JOIN customer_settings cs ON c.id = cs.customer_id AND c.cid = cs.cid AND cs.key = \'opt_out_of_apartmates\'
						LEFT JOIN customer_clubs cc ON c.id = cc.customer_id AND c.cid = cc.cid AND cc.club_id = ' . ( int ) $this->getId() . '
						LEFT JOIN customer_phone_numbers cpn ON c.id = cpn.customer_id AND c.cid = cpn.cid AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL AND cpn.deleted_on IS NULL
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND	l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND	lc.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND ( c.id IN ( cc.customer_id ) OR c.id = ' . ( int ) $this->getCustomerId() . ' )' . $strWhereCondition . '
					ORDER BY
						lc.lease_status_type_id';

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomers( $strSql, $objDatabase );
	}

	public function deleteClubData( $intCurrentUserId, $objDatabase ) {
		$strSql = ' UPDATE
						public.clubs
					SET '
						. 'rejection_comment = ' . $this->sqlRejectionComment() . ','
						. ' deleted_by = ' . $this->sqlDeletedBy() . ','
						. ' deleted_on = ' . $this->sqlDeletedOn() . ','
						. ' updated_by = ' . ( int ) $intCurrentUserId . ','
						. ' updated_on = NOW()
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
			return true;
		}
		return false;
	}

}
?>