<?php

class CApplicantEmailAddress extends CBaseApplicantEmailAddress {

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getApplicantId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailAddressTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getEmailAddressTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getEmailAddress() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>