<?php

class CDocumentParameter extends CBaseDocumentParameter {

	protected  $m_arrstrAllowedScriptTagKeys;

	public function __construct() {
		parent::__construct();

		$this->m_arrstrAllowedScriptTagKeys = [ 'text' ];
	}

	/**
	 * Set Functions
	 */

	public function setValue( $strValue ) {
		if( false == is_null( $this->getKey() ) && true == in_array( $this->getKey(), $this->m_arrstrAllowedScriptTagKeys ) ) {
			$this->m_strValue = CStrings::strTrimDef( $strValue, -1, NULL, true, true );
		} else {
			parent::setValue( $strValue );
		}
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>