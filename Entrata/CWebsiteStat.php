<?php

class CWebsiteStat extends CBaseWebsiteStat {

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valWebsiteId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getWebsiteId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valStatDate() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getStatDate())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'stat_date', '' ));
        // }

        return $boolIsValid;
    }

    public function valStandardUniques() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getStandardUniques())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_uniques', '' ));
        // }

        return $boolIsValid;
    }

    public function valStandardViews() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getStandardViews())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_views', '' ));
        // }

        return $boolIsValid;
    }

    public function valMobileUniques() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getMobileUniques())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_uniques', '' ));
        // }

        return $boolIsValid;
    }

    public function valMobileViews() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getMobileViews())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_views', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function insertOrUpdateStandardWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        if( true == is_null( $this->getId() ) ) {
            return $this->insertStandardWebsite( $intCurrentUserId, $objDatabase );
        } else {
            return $this->updateStandardWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase );
        }
    }

	public function insertStandardWebsite( $intCurrentUserId, $objDatabase ) {
        $objDataset = $objDatabase->createDataset();

        $intId = $this->getId();

        if( true == is_null( $intId ) ) {
            $strSql = 'SELECT nextval( \'website_stats_id_seq\'::text ) AS id';

            if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

                $objDataset->cleanup();
                return false;
            }

            $arrValues = $objDataset->fetchArray();
            $this->setId( $arrValues['id'] );

            $objDataset->cleanup();
        }

        $strSql = 'INSERT INTO website_stats( id, cid, website_id, stat_date, standard_uniques, standard_views, updated_by, updated_on, created_by, created_on ) VALUES ( ' .
                    $this->sqlId() . ', ' .
                    $this->sqlCid() . ', ' .
                    $this->sqlWebsiteId() . ', now(), 1, 1, ' .
                    ( int ) $intCurrentUserId . ', now(), ' .
                    ( int ) $intCurrentUserId . ', now() )';

        if( false == $objDataset->execute( $strSql ) ) {
            // Reset id on error
            $this->setId( $intId );

            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
            // Reset id on error
            $this->setId( $intId );

            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

    public function updateStandardWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        $objDataset = $objDatabase->createDataset();

        $strSql = 'UPDATE website_stats SET';

        if( true == $boolIsUniqueStat ) {
        	$strSql .= ' standard_uniques = standard_uniques + 1, ';
        }

        $strSql .= ' standard_views = standard_views + 1
				  WHERE
				    id = ' . ( int ) $this->sqlId() . '
				    AND cid = ' . ( int ) $this->sqlCid();

        if( false == $objDataset->execute( $strSql ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update website stat record. The following error was reported.' ) );
            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

	public function insertOrUpdateMobileWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        if( true == is_null( $this->getId() ) ) {
            return $this->insertMobileWebsite( $intCurrentUserId, $objDatabase );
        } else {
            return $this->updateMobileWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase );
        }
    }

	public function insertMobileWebsite( $intCurrentUserId, $objDatabase ) {
        $objDataset = $objDatabase->createDataset();

        $intId = $this->getId();

        if( true == is_null( $intId ) ) {
            $strSql = 'SELECT nextval( \'website_stats_id_seq\'::text ) AS id';

            if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

                $objDataset->cleanup();
                return false;
            }

            $arrValues = $objDataset->fetchArray();
            $this->setId( $arrValues['id'] );

            $objDataset->cleanup();
        }

        $strSql = 'INSERT INTO website_stats( id, cid, website_id, stat_date, mobile_uniques, mobile_views, updated_by, updated_on, created_by, created_on ) VALUES ( ' .
                    $this->sqlId() . ', ' .
                    $this->sqlCid() . ', ' .
                    $this->sqlWebsiteId() . ', now(), 1, 1, ' .
                    ( int ) $intCurrentUserId . ', now(), ' .
                    ( int ) $intCurrentUserId . ', now() )';

        if( false == $objDataset->execute( $strSql ) ) {
            // Reset id on error
            $this->setId( $intId );

            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert website stat record. The following error was reported.' ) );
            // Reset id on error
            $this->setId( $intId );

            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

    public function updateMobileWebsite( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        $objDataset = $objDatabase->createDataset();

        $strSql = 'UPDATE website_stats SET';

        if( true == $boolIsUniqueStat ) {
        	$strSql .= ' mobile_uniques = mobile_uniques + 1, ';
        }

        $strSql .= ' mobile_views = mobile_views + 1
				  WHERE
				    id = ' . ( int ) $this->sqlId() . '
				   AND cid =' . ( int ) $this->sqlCid();

        if( false == $objDataset->execute( $strSql ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update website stat record. The following error was reported.' ) );
            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }
}
?>