<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCashFlowTypes
 * Do not add any new functions to this class.
 */

class CCashFlowTypes extends CBaseCashFlowTypes {

	public static function fetchCashFlowTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCashFlowType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCashFlowType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCashFlowType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCashFlowTypes( $objClientDatabase ) {
		return self::fetchCashFlowTypes( 'SELECT * FROM cash_flow_types', $objClientDatabase );
	}
}
?>