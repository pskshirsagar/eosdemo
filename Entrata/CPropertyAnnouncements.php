<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAnnouncements
 * Do not add any new functions to this class.
 */

class CPropertyAnnouncements extends CBasePropertyAnnouncements {

	public static function fetchPropertyAnnouncementByAnnouncementIdByPropertyIdByCid( $intAnnouncementId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT pa.* FROM property_announcements pa, announcements a WHERE a.id = pa.announcement_id AND a.cid = pa.cid AND a.id = ' . ( int ) $intAnnouncementId . ' AND pa.property_id = ' . ( int ) $intPropertyId . ' AND a.cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchPropertyAnnouncement( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyAnnouncementsByAnnouncementIdByCidByFieldNames( $intAnnouncementId, $intCid, $arrstrFieldName, $objDatabase ) {
		if( false == valArr( $arrstrFieldName ) ) return NULL;

		$strSql = 'SELECT DISTINCT ' . implode( ',', $arrstrFieldName ) . ' FROM property_announcements pa, announcements a WHERE a.id = pa.announcement_id AND a.cid = pa.cid AND a.id = ' . ( int ) $intAnnouncementId . '  AND a.cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyAnnouncementsCountByAnnouncementIdByPropertyIdByCid( $intAnnouncementId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count ( pa.id ) as count
					FROM
						property_announcements pa
						JOIN announcements a ON ( a.id = pa.announcement_id AND a.cid = pa.cid )
					WHERE
						pa.announcement_id = ' . ( int ) $intAnnouncementId . '
						AND pa.property_id = ' . ( int ) $intPropertyId . '
						AND pa.cid = ' . ( int ) $intCid;

		$arrstrCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrstrCount[0]['count'] ) ) ? $arrstrCount[0]['count'] : 0;
	}

	public static function fetchPropertyAnnouncementIdsByAnnouncementIdByCId( $intAnnouncementId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pa.id,
						pa.property_id
					FROM
						property_announcements pa
						JOIN announcements a ON ( a.id = pa.announcement_id AND a.cid = pa.cid )
					WHERE
						pa.announcement_id = ' . ( int ) $intAnnouncementId . '
						AND pa.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteBulkPropertyAnnouncementsByIds( $arrintPropertyAnnouncementsIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyAnnouncementsIds ) ) return;
		$strSql = 'DELETE
					FROM
						property_announcements
					WHERE
						id IN (' . implode( ',', $arrintPropertyAnnouncementsIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return executeSql( $strSql, $objDatabase );
	}

}
?>