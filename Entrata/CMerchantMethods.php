<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMerchantMethods
 * Do not add any new functions to this class.
 */

class CMerchantMethods extends CBaseMerchantMethods {

    // Here we pull merchant account methods, but map them into the merchant methods objects from the works database

    public static function fetchPseudoMerchantAccountMethodsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantMethods( 'SELECT company_merchant_account_id as merchant_account_id, * FROM merchant_account_methods WHERE cid = ' . ( int ) $intCid, $objDatabase );
    }

	public static function fetchPseudoMerchantAccountMethodsByCompanyMerchantAccountIdsByCid( $arrintCompanyMerchantAccountIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) {
			return NULL;
		}
		return self::fetchMerchantMethods( 'SELECT company_merchant_account_id as merchant_account_id, * FROM merchant_account_methods WHERE company_merchant_account_id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' ) AND cid = ' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchMerchantMethodByMerchantAccountIdByCidByPaymentMediumIdByPaymentTypeId( $intMerchantAccountId, $intCid, $intPaymentMediumId, $intPaymentTypeId, $objDatabase ) {
		$strSql = 'SELECT 
							* 
					FROM 
							merchant_methods 
					WHERE 
							merchant_account_id = ' . ( int ) $intMerchantAccountId . '
							AND cid = ' . ( int ) $intCid . ' 
							AND payment_medium_id = ' . ( int ) $intPaymentMediumId . '
							AND payment_type_id = ' . ( int ) $intPaymentTypeId . '
							AND deleted_by IS NULL
							AND deleted_on IS NULL';

		return self::fetchMerchantMethod( $strSql, $objDatabase );
	}

}
?>