<?php

class CCalendarEventReminder extends CBaseCalendarEventReminder {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarEventId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReminderTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntervalTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntervalCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReminderDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>