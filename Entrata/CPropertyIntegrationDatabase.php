<?php

class CPropertyIntegrationDatabase extends CBasePropertyIntegrationDatabase {

	protected $m_intPropertyUnitsCount;
	protected $m_intIntegrationClientTypeId;
	protected $m_strIntegrationClientTypeName;

    /**
     * Set Functions
     */

    // Added temprory will be remove once CUpdatePropertyUnitsForIntegratedProperties is executed once

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['property_units_count'] ) ) $this->setPropertyUnitsCount( $arrmixValues['property_units_count'] );
		if( true == isset( $arrmixValues['integration_client_type_id'] ) ) $this->setIntegrationClientTypeId( $arrmixValues['integration_client_type_id'] );
		if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
    }

    public function setIntegrationClientTypeId( $strIntegrationClientTypeId ) {
		$this->m_intIntegrationClientTypeId = $strIntegrationClientTypeId;
    }

	public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
    }

    // Added temprory will be remove once CUpdatePropertyUnitsForIntegratedProperties is executed once

    public function setPropertyUnitsCount( $intPropertyUnitsCount ) {
     	 $this->m_intPropertyUnitsCount = $intPropertyUnitsCount;
    }

    /**
     * Get Functions
     */

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
    }

	public function getIntegrationClientTypeName() {
		return $this->m_strIntegrationClientTypeName;
    }

    // Added temprory will be remove once CUpdatePropertyUnitsForIntegratedProperties is executed once

    public function getPropertyUnitsCount() {
     	 return $this->m_intPropertyUnitsCount;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == $boolReturnSqlOnly ) {
    		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
	    	if( true == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
	    		$objIntegrationDatabase = \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByIdByCid( $this->getIntegrationDatabaseId(), $this->getCid(), $objDatabase );
	    		$objPropertyGlSetting 	= CPropertyGlSettings::fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

	    		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MIGRATION, CIntegrationClientType::EMH ] ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {
	    			$objPropertyGlSetting->setActivateStandardPosting( false );

	    			if( false == $objPropertyGlSetting->update( $intCurrentUserId, $objDatabase ) ) {
	    				trigger_error( 'Unable to update activate_standard_posting for cid : ' . $this->sqlCid() . ', property_id : ' . $this->sqlPropertyId(), E_USER_WARNING );
	    			}
	    		}

	    		return true;
	    	}
    	}
    }

    /**
     * Validation Functions
     */

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIntegrationDatabaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getIntegrationDatabaseId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_database_id', 'Integration database is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valIntegrationDatabaseId();
            	$boolIsValid &= $this->valPropertyId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>