<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSettingKeys
 * Do not add any new functions to this class.
 */

class CApplicationSettingKeys extends CBaseApplicationSettingKeys {

	public static function fetchPublishedApplicationSettingKeys( $objDatabase ) {
		return parent::fetchApplicationSettingKeys( 'select ask.* from application_setting_keys ask where ask.is_published = TRUE', $objDatabase );
	}

}
?>