<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentDependencies
 * Do not add any new functions to this class.
 */

class CDocumentDependencies extends CBaseDocumentDependencies {

//     public static function fetchDocumentDependencies( $strSql, $objDatabase ) {
//         return self::fetchCachedObjects( $strSql, 'CDocumentDependency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
//     }

    public static function fetchDocumentDependency( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDocumentDependency', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDocumentDependenciesByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintDocumentIds ) ) return NULL;

    	$strSql = ' SELECT
						*
					FROM
						document_dependencies
					WHERE
						document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND cid = ' . ( int ) $intCid;

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

    public static function fetchDocumentDependencyByDocumentIdByDocumentAddendaIdCid( $intDocumentId, $intDocumentAddendaId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT * FROM document_dependencies WHERE document_id = ' . ( int ) $intDocumentId . ' AND document_addenda_id = ' . ( int ) $intDocumentAddendaId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id ASC';

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

    public static function fetchDocumentDependenciesByDocumentAddendaIdsByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

    	$strSql = ' SELECT
						*
					FROM
						document_dependencies
					WHERE
						document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						order_num';

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

    public static function fetchDocumentDependenciesByDocumentAddendaIdsByReferenceIdsByDocumentDependencyTypeIdsByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase, $arrintReferenceIds = NULL, $arrintDocumentDependencyTypeIds = NULL, $boolIncludeVehicles = true ) {

    	if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

    	$strSql = ' SELECT
						*
					FROM
						document_dependencies
					WHERE
						document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND cid = ' . ( int ) $intCid;

    	if( true == valArr( $arrintReferenceIds ) ) {
    		if( true == $boolIncludeVehicles ) {
    			$strSql .= ' AND ( ( \'{ ' . implode( ',', $arrintReferenceIds ) . ' }\' && reference_ids ) OR reference_ids IS NULL )';
    		} else {
    			$strSql .= ' AND ( \'{ ' . implode( ',', $arrintReferenceIds ) . ' }\' && reference_ids )';
    		}
    	}

    	if( true == valArr( $arrintDocumentDependencyTypeIds ) ) {
    		$strSql .= ' AND document_dependency_type_id IN ( ' . implode( ',', $arrintDocumentDependencyTypeIds ) . ' )';
    	}

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

    public static function fetchDocumentDependenciesByMasterDocumentIdByCid( $intMasterDocumentId, $intCid, $objDatabase ) {

    	$strSql = ' SELECT
						dd.*
					FROM
						document_dependencies dd
    					JOIN document_addendas da ON ( dd.document_id = da.document_id AND dd.document_addenda_id = da.id AND dd.cid = da.cid )
					WHERE
						dd.cid = ' . ( int ) $intCid . '
						AND da.deleted_on IS NULL
						AND da.archived_on IS NULL
						AND da.master_document_id = ' . ( int ) $intMasterDocumentId . '
						ORDER BY dd.order_num';

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

    public static function fetchDocumentDependenciesByIncludeDocumentAddendaIdsByCid( $arrintIncludeDocumentAddendaIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintIncludeDocumentAddendaIds ) ) return NULL;

    	$strSql = ' SELECT
						*
					FROM
						document_dependencies
					WHERE
						include_document_addenda_id IN ( ' . implode( ',', $arrintIncludeDocumentAddendaIds ) . ' )
						AND cid = ' . ( int ) $intCid;

    	return self::fetchDocumentDependencies( $strSql, $objDatabase );
    }

	public static function fetchDocumentDependenciesByReferenceIdByDocumentDependencyTypeIdByCid( $intReferenceId, $intDocumentDependencyTypeId, $intCid, $objDatabase ) {

		if( false == valId( $intReferenceId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						document_dependencies
					WHERE
						document_dependency_type_id = ' . ( int ) $intDocumentDependencyTypeId . ' 
						AND ( \'{ ' . ( int ) $intReferenceId . ' }\' && reference_ids )
						AND cid = ' . ( int ) $intCid;

		return self::fetchDocumentDependencies( $strSql, $objDatabase );

	}

}
?>