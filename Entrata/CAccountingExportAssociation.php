<?php

class CAccountingExportAssociation extends CBaseAccountingExportAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountingExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>