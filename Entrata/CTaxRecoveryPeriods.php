<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxRecoveryPeriods
 * Do not add any new functions to this class.
 */

class CTaxRecoveryPeriods extends CBaseTaxRecoveryPeriods {

	public static function fetchTaxRecoveryPeriods( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaxRecoveryPeriod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );

	}

	public static function fetchTaxRecoveryPeriod( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaxRecoveryPeriod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );

	}

	public static function fetchAllTaxRecoveryPeriods( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						tax_recovery_periods';

		return self::fetchTaxRecoveryPeriods( $strSql, $objDatabase );
	}
}
?>