<?php

class CCompanyGroupPropertyGroup extends CBaseCompanyGroupPropertyGroup {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions Get Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCompanyGroupPropertyGroupsFields() {
		return [
			'company_group_property_groups' => __( 'Property/Property Groups' )
		];
	}

}
?>