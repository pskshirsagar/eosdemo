<?php

class CViolationWarningType extends CBaseViolationWarningType {
	const WRITTEN = 1;
	const VERBAL = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getViolationWarningTypes() {
		return [
			SELF::WRITTEN => [ 'id' => '1', 'name' => __( 'Written' ) ],
			SELF::VERBAL   => [ 'id' => '2', 'name' => __( 'Verbal' ) ]
		];
	}

}
?>