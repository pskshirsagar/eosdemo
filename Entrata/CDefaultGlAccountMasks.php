<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlAccountMasks
 * Do not add any new functions to this class.
 */

class CDefaultGlAccountMasks extends CBaseDefaultGlAccountMasks {

	public static function fetchDefaultGlAccountMasksByGlAccountTypeIds( $arrintGlAccountTypeIds, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						dgam.*,
						ga.gl_account_type_id,
						util_get_system_translated( \'name\', gat.name, gat.details ) AS gl_account_type_name,
						dgt.account_number_pattern,
						dgt.account_number_delimiter
					FROM
						default_gl_account_masks dgam
						JOIN default_gl_accounts ga ON ( ga.id = dgam.default_gl_account_id )
						JOIN default_gl_trees dgt ON ( dgam.default_gl_tree_id = dgt.id )
						JOIN gl_account_types gat ON ( gat.id = ga.gl_account_type_id)
					WHERE
						ga.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
					ORDER BY
						gl_account_type_name,
						dgam.account_number,
						LOWER( dgam.name ),
						ga.gl_account_type_id,
						dgam.id';

		return self::fetchDefaultGlAccountMasks( $strSql, $objClientDatabase );
	}

}
?>