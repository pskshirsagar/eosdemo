<?php

class CMaritalStatusType extends CBaseMaritalStatusType {

	const SINGLE					= 1;
	const MARRIED					= 2;
	const DIVORCED					= 3;
	const WIDOWED					= 4;
	const SEPERATED					= 5;

	public static $c_arrintMaritalStausCodesForTic = [
		self::SINGLE    => 2,
		self::MARRIED   => 1,
		self::DIVORCED  => 4,
		self::WIDOWED   => 3,
		self::SEPERATED => 5
	];

	public static function getBluemoonMaritalStatusType( $intMaritalStatusTypeId ) {
		$strMaritalStatus = '';

		switch( $intMaritalStatusTypeId ) {
			case self::MARRIED:
				$strMaritalStatus = 'married';
				break;

			case self::DIVORCED:
				$strMaritalStatus = 'divorced';
				break;

			case self::WIDOWED:
				$strMaritalStatus = 'widowed';
				break;

			case self::SEPERATED:
				$strMaritalStatus = 'seperated';
				break;

			default:
				$strMaritalStatus = 'single';
				break;
		}

		return $strMaritalStatus;
	}

}
?>