<?php

class CApFormulaType extends CBaseApFormulaType {

	const PERCENT				= 1;
	const NO_OF_UNITS			= 2;
	const SQUARE_FOOTAGE		= 3;
	const NO_OF_SPACES			= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>