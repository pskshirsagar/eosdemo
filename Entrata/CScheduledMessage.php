<?php

class CScheduledMessage extends CBaseScheduledMessage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledMessageType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledMessageFilterIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageOwnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastQueuedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>