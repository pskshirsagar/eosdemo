<?php
class CTransmission extends CBaseTransmission {

	use TEosStoredObject;

	protected $m_intIsUpdateFromInsert;

	protected $m_strRequestContent;
	protected $m_strResponseContent;
	protected $m_strErrorDescription;
	protected $m_strDescription;
    protected $m_strCrimeStatus;
    protected $m_strAlertMessage;
    protected $m_strCrimeMessage;
    protected $m_strScoreXDecision;
    protected $m_strScoreXStatus;
    protected $m_strTransmissionVendorName;
    protected $m_strCompanyTransmissionVendorName;
    protected $m_strDataSent;
    protected $m_strDataReceived;
    protected $m_strTitle;

	protected $m_objSerialisedRequest;
	protected $m_objSerialisedResponse;

	protected $m_intCompanyUserId;

	protected $m_boolIsContentLoaded;

	protected $m_objObjectStorage;
	protected $m_strStorageGatewayName;

    public function __construct() {
        parent::__construct();

		$this->m_strDataSent 			= NULL;
		$this->m_strDataReceived		= NULL;
		$this->m_boolIsContentLoaded	= false;

        return;
    }

    public function setDefaults() {
		$this->m_strTransmissionDatetime = date( 'm/d/Y H:i:s' );
    }

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );
        if( true == isset( $arrstrValues['company_transmission_vendor_name'] ) ) 	$this->setCompanyTransmissionVendorName( $arrstrValues['company_transmission_vendor_name'] );
        if( true == isset( $arrstrValues['transmission_vendor_name'] ) ) 			$this->setTransmissionVendorName( $arrstrValues['transmission_vendor_name'] );
    }

	/**
	 * Get Functions
	 */

    public function getTransmissionVendorName() {
    	return $this->m_strTransmissionVendorName;
    }

    public function getCompanyTransmissionVendorName() {
    	return $this->m_strCompanyTransmissionVendorName;
    }

    public function getCompanyUserId() {
    	return $this->m_intCompanyUserId;
    }

    public function getIsContentLoaded() {
		return $this->m_boolIsContentLoaded;
	}

	public function loadContents( $objDatabase ) {

		$arrmixFileNames                 = explode( ',', $this->getRequestFileName() );
		$strRentSummaryFileName          = NULL;
		$strAvailableUnitFileName        = NULL;
		$strLeaseTermRentFileName        = NULL;
		$strRenewalLeaseTermRentFileName = NULL;
		$strLroFileName                  = NULL;

		if( true == valArr( $arrmixFileNames ) ) {
			foreach( $arrmixFileNames as $mixFileName ) {
				if( true == \Psi\CStringService::singleton()->strpos( $mixFileName, 'RentSummary.csv' ) ) {
					$strRentSummaryFileName = $mixFileName;
				} elseif( true == \Psi\CStringService::singleton()->strpos( $mixFileName, 'AvailableUnits.csv' ) ) {
					$strAvailableUnitFileName = $mixFileName;
				} elseif( true == \Psi\CStringService::singleton()->strpos( $mixFileName, 'RenewalLeaseTermRent.csv' ) ) {
					$strRenewalLeaseTermRentFileName = $mixFileName;
				} elseif( true == \Psi\CStringService::singleton()->strpos( $mixFileName, 'LeaseTermRent.csv' ) ) {
					$strLeaseTermRentFileName = $mixFileName;
				} elseif( true == \Psi\CStringService::singleton()->strpos( $mixFileName, 'LRO' ) ) {
					$strLroFileName = $mixFileName;
				}
			}
		}

		$arrstrFileNames = [ '_result.txt', NULL ];
		if( CTransmissionVendor::YIELDSTAR == $this->getTransmissionVendorId() ) {
			$arrstrFileNames = array_merge( $arrstrFileNames, [ '_AvailableUnits.csv', '_LeaseTermRent.csv', '_RenewalLeaseTermRent.csv', '_RentSummary.csv' ] );
		}

		foreach( $arrstrFileNames as $strFileName ) {
			$arrmixStoredObjects[] = $this->fetchStoredObject( $objDatabase, $strFileName )->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS', 'noEncrypt' => true ] );
		}

		if( false == valArr( $arrmixStoredObjects ) ) {
			return false;
		}

		foreach( $arrmixStoredObjects as $arrmixStoredObject ) {

			$arrmixGatewayResponse = $this->getOrLoadObjectStorage()->getObject( $arrmixStoredObject );

			if( true == $arrmixGatewayResponse->hasErrors() ) {
				continue;
			}

			list( $strDataSent, $strDataReceived ) = explode( 'PSI_SEPARATOR', $arrmixGatewayResponse['data'] );

			if( CTransmissionVendor::YIELDSTAR == $this->getTransmissionVendorId() ) {
				if( true == in_array( $this->getResponse(), [ 'retrieveLeaseTermRent', 'getAvailableUnits', 'retrieveRenewalLeaseTermRents', 'getRentSummary' ] ) ) {
					$this->m_strDataSent     = $strDataSent;
					$this->m_strDataReceived = $strDataReceived;
				} else {
					// Yieldstar unique case
					if( false !== strpos( $arrmixStoredObject['key'], '_AvailableUnits.csv' ) ) {
						$this->m_strDataReceived .= "\n\n" . 'File Name: ' . $strAvailableUnitFileName . "\n\n" . $strDataReceived;
					}
					if( false !== strpos( $arrmixStoredObject['key'], '_LeaseTermRent.csv' ) ) {
						$this->m_strDataReceived .= "\n\n" . 'File Name: ' . $strLeaseTermRentFileName . "\n\n" . $strDataReceived;
					}
					if( false !== strpos( $arrmixStoredObject['key'], '_RenewalLeaseTermRent.csv' ) ) {
						$this->m_strDataReceived .= "\n\n" . 'File Name: ' . $strRenewalLeaseTermRentFileName . "\n\n" . $strDataReceived;
					}
					if( false !== strpos( $arrmixStoredObject['key'], '_RentSummary.csv' ) ) {
						$this->m_strDataReceived .= "\n\n" . 'File Name: ' . $strRentSummaryFileName . "\n\n" . $strDataReceived;
					}
				}
			} else {
				$this->m_strDataSent     = $strDataSent;
				$this->m_strDataReceived = $strDataReceived;
			}
		}

		if( true == $this->getIsFailed() && false == valStr( $this->m_strDataReceived ) ) {
			$this->m_strDataReceived = $this->getMessage();
		}

		if( CTransmissionVendor::LRO == $this->getTransmissionVendorId() ) {
			$this->m_strDataReceived = "\n\n" . ' ' . $strLroFileName . ' ' . "\n\n" . $this->m_strDataReceived;
		}

		$intCompanyUserId     = 0;
		$arrintCompanyUserIds = [];

		// Functionality ONLY for SCREENING
		if( CTransmissionType::SCREENING == $this->getTransmissionTypeId() ) {
			$intCompanyUserId = ( false == is_null( $this->getCompanyUserId() ) ) ? $this->getCompanyUserId() : SYSTEM_USER_ID;

			// User Ids, who are allowed to see the original sensitive information as well.
			// DIR 	> 1719 : Santosh H
			// DEV 	> 1302 : Sonal J, 1511 : Sachin M.
			// QA 	> 3379 : Sandip P, 4123 : Nikhil R

			$arrintCompanyUserIds = [ 1302, 1511, 1719, 3379, 4123 ];

			if( false == in_array( $intCompanyUserId, $arrintCompanyUserIds ) ) {
				if( true == valStr( $this->m_strDataSent ) ) {
					$this->handleEncryptSensitiveScreeningInfo( $this->m_strDataSent, $strResponseFile = NULL, $boolIsRequestContent = true, $boolIsResponseContent = false );
					$this->m_strDataSent = $this->m_objSerialisedRequest;
				}

				if( true == valStr( $this->m_strDataReceived ) ) {
					$this->handleEncryptSensitiveScreeningInfo( $strRequestFile = NULL, $this->m_strDataReceived, $boolIsRequestContent = false, $boolIsResponseContent = true );
					$this->m_strDataReceived = $this->m_objSerialisedResponse;
				}
			}
		}

		$this->setIsContentLoaded( true );
	}

	public function getRequestContent( $objDatabase = NULL ) {
		if( true == valStr( $this->m_strRequestContent ) ) {
			return $this->m_strRequestContent;
		}

		if( false == $this->getIsContentLoaded() ) {
			// handling database error gracefully. Using the already loaded db from the object if database is not provided.
			if( NULL == $objDatabase ) {
				$objDatabase = $this->m_objDatabase;
			}
			$this->loadContents( $objDatabase );
		}

		return $this->m_strDataSent;
	}

	public function getResponseContent( $objDatabase = NULL ) {
		if( true == valStr( $this->m_strResponseContent ) ) {
			return $this->m_strResponseContent;
		}

		if( false == $this->getIsContentLoaded() ) {
			// handling database error gracefully. using the already loaded db from the object if database is not provided.
			if( NULL == $objDatabase ) {
				$objDatabase = $this->m_objDatabase;
			}
			$this->loadContents( $objDatabase );
		}

		return $this->m_strDataReceived;
	}

	public function getErrorDescription() {
		return $this->m_strErrorDescription;
	}

    public function getDescription() {
    	return $this->m_strDescription;
    }

    public function getCrimeStatus() {
		return $this->m_strCrimeStatus;
    }

    public function getAlertMessage() {
		return $this->m_strAlertMessage;
    }

    public function getCrimeMessage() {
		return $this->m_strCrimeMessage;
    }

    public function getScoreXDecision() {
		return $this->m_strScoreXDecision;
    }

    public function getScoreXStatus() {
		return $this->m_strScoreXStatus;
    }

	public function getTitle() {
		return $this->m_strTitle;
	}

	/**
	 * @return mixed
	 */
	public function getStorageGatewayName() {
		return $this->m_strStorageGatewayName;
	}

	/**
	 * Set Functions
	 */
	public function setIsContentLoaded( $boolIsContentLoaded ) {
		$this->m_boolIsContentLoaded = $boolIsContentLoaded;
	}

    public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
    }

	public function setTransmissionVendorName( $strTransmissionVendorName ) {
    	$this->m_strTransmissionVendorName = $strTransmissionVendorName;
    }

    public function setCompanyTransmissionVendorName( $strCompanyTransmissionVendorName ) {
    	$this->m_strCompanyTransmissionVendorName = $strCompanyTransmissionVendorName;
    }

	public function setRequestContent( $strRequestContent ) {
		$this->m_strRequestContent = CStrings::strTrimDef( $strRequestContent, -1, NULL, true );
	}

	public function setResponseContent( $strResponseContent ) {
		$this->m_strResponseContent = CStrings::strTrimDef( $strResponseContent, -1, NULL, true );
	}

	public function setErrorDescription() {

		$objSimpleXml = $this->getUnSerializedData( $this->getResponseContent() );
		if( NULL == $objSimpleXml ) return;
		switch( $this->getTransmissionVendorId() ) {

			case CTransmissionVendor::SAFERENT:
				if( 0 != $objSimpleXml->Error->Error_Code && true == isset( $objSimpleXml->Error->Error_Description ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->Error->Error_Description;
				}
				break;

			case CTransmissionVendor::ONSITE:
				if( true == isset( $objSimpleXml->ApplicantScreening->Response->ErrorDescription ) && 0 < \Psi\CStringService::singleton()->strlen( $objSimpleXml->ApplicantScreening->Response->ErrorDescription ) ) {
					$this->m_strErrorDescription = $objSimpleXml->ApplicantScreening->Response->ErrorDescription;
				}
				break;

			case CTransmissionVendor::FIRST_ADVANTAGE:
				if( true == isset( $objSimpleXml->Response->ErrorDescription ) && 0 < \Psi\CStringService::singleton()->strlen( $objSimpleXml->Response->ErrorDescription ) ) {
					$this->m_strErrorDescription = $objSimpleXml->Response->ErrorDescription;
				}
				break;

			case CTransmissionVendor::RESIDENT_CHECK:
				if( true == isset( $objSimpleXml->Response->ErrorDescription ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->Response->ErrorDescription;
				}
				break;

			case CTransmissionVendor::RENT_GROW:
				if( true == isset( $objSimpleXml->Response->ErrorDescription ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->Response->ErrorDescription;
				}
				break;

			case CTransmissionVendor::TRANSUNION_CREDIT_RETRIEVER_CR6:
				if( 1 != $objSimpleXml->status->statusCode && true == isset( $objSimpleXml->status->statusMessage ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->status->statusMessage;
				}
				break;

			case CTransmissionVendor::VERIFIRST:
				if( true == isset( $objSimpleXml->VBSScreeningResponse->ErrorResponse->Failed->Error ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->VBSScreeningResponse->ErrorResponse->Failed->Error->Message . ' : ' . $objSimpleXml->VBSScreeningResponse->ErrorResponse->Failed->Error->Data;
				} elseif( true == isset( $objSimpleXml->VBSScreeningResponse->VeriguideOrderResponse->Failed->Error ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->VBSScreeningResponse->VeriguideOrderResponse->Failed->Error->Message;
				}
				break;

			case CTransmissionVendor::LEASINGDESK:
				if( true == isset( $objSimpleXml->Result->ErrorDescription ) ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->Result->ErrorDescription;
				} elseif( $objSimpleXml->attributes()->ErrorDescription ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->attributes()->ErrorDescription;
				}
				break;

			case CTransmissionVendor::RENTAL_HISTORY_REPORTS:
				if( true == isset( $objSimpleXml->Status ) && 1 == $objSimpleXml->Status ) {
					$this->m_strErrorDescription = ( string ) $objSimpleXml->Message;
				}
				break;

			case CTransmissionVendor::LEASINGDESK_MITS:
			case CTransmissionVendor::REALID:
			case CTransmissionVendor::GENERIC_SCREENING_LIBRARY:
				if( true == isset( $objSimpleXml->response->errordescription ) || isset( $this->m_objSimpleXml->Response->ErrorDescription ) ) {
					$strErrorDesc = ( isset( $this->m_objSimpleXml->Response->ErrorDescription ) ) ? trim( $this->m_objSimpleXml->Response->ErrorDescription ) : trim( $this->m_objSimpleXml->response->errordescription );
					$this->m_strErrorDescription = ( string ) $strErrorDesc;
				}
				break;

			default:
		}

		$this->m_strErrorDescription = str_replace( chr( 13 ), ' ', $this->m_strErrorDescription ); // remove carriage returns
		$this->m_strErrorDescription = str_replace( chr( 10 ), ' ', $this->m_strErrorDescription ); // remove line feeds
	}

	public function setDescription( $objSimpleXml ) {

    	if( false == is_object( $objSimpleXml ) ) {
    		$this->m_strDescription = NULL;
    	}

    	$strDescription = '';

    	switch( $this->getTransmissionVendorId() ) {
    		case CTransmissionVendor::SAFERENT:
    			if( 0 == $objSimpleXml->Error_Code ) {
					foreach( $objSimpleXml->Applicant as $objApplicant ) {

						if( $objApplicant->ApplicantID != $this->getRemotePrimaryKey() ) {
							continue;
						}

						if( $objApplicant->Messages ) {
							if( $objApplicant->Messages->CreditMessages ) {
								$strDescription .= $objApplicant->Messages->CreditMessages->CreditMessage . '<br/>';
							}

							if( $objApplicant->Messages->SSNMessages ) {
								$strDescription .= $objApplicant->Messages->SSNMessages->SSNMessage . '<br/>';
							}

							if( $objApplicant->Messages->CrimCHECKMessages ) {
								$strDescription .= $objApplicant->Messages->CrimCHECKMessages->CrimCHECKMessage . '<br/>';
							}
						}
					}
				}
    			break;

    		default:
    	}

    	$this->m_strDescription = $strDescription;
    }

   	public function setTransmissionReportData( $objTransmission, $objDatabase ) {
    	if( false == valObj( $objTransmission, 'CTransmission' ) ) {
    		return;
    	}

    	$objApplicant = $this->fetchApplicant( $objDatabase );

    	if( false == valObj( unserialize( $objTransmission->getResponseContent() ), 'stdClass' ) ) {
	    	$objSimpleXml = $this->getSimpleXmlObject( $objTransmission->getResponseContent() );

    	} else {
    		$objSimpleXml = unserialize( $objTransmission->getResponseContent() );
    	}

    	$this->retriveTransmissionReportData( $objSimpleXml, $objApplicant );
    }

    public function setAlertMessage( $strAlertMessage ) {
		$this->m_strAlertMessage = $strAlertMessage;
    }

    public function setCrimeStatus( $strCrimeStatus ) {
		$this->m_strCrimeStatus = $strCrimeStatus;
    }

    public function setCrimeMessage( $strCrimeMessage ) {
		$this->m_strCrimeMessage = $strCrimeMessage;
    }

    public function setScoreXDecision( $strScoreXDecision ) {
		$this->m_strScoreXDecision = $strScoreXDecision;
    }

    public function setScoreXStatus( $strScoreXStatus ) {
		$this->m_strScoreXStatus = $strScoreXStatus;
    }

	public function setObjectStorage( $objObjectStorage ) {
		$this->m_objObjectStorage = $objObjectStorage;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setStorageGatewayName( $strStorageGatewayName ) {
		$this->m_strStorageGatewayName = $strStorageGatewayName;
	}

	public function retriveTransmissionReportData( $objSimpleXml, $objApplicant = NULL ) {

    	if( CTransmissionResponseType::INSUFFICIENT_INFO == $this->getTransmissionResponseTypeId() ) {
			$this->setScoreXStatus( 'Insufficient Info.' );
		}

    	if( false == is_object( $objSimpleXml ) ) {
    		return;
    	}

    	switch( $this->getTransmissionVendorId() ) {
    		case CTransmissionVendor::SAFERENT:
    			$arrintValidErrorNumbers  = [ 100, 101, 102, 103, 201, 202, 203, 205, 206, 207, 208, 209, 210, 211, 212, 213, 301, 400, 500, 501, 600, 601, 602, 604, 605, 606, 607, 609, 610, 611, 700 ];

    			if( 0 == $objSimpleXml->Error_Code ) {
					foreach( $objSimpleXml->Applicant as $objApplicant ) {
						if( $objApplicant->ApplicantID != $this->getRemotePrimaryKey() ) {
							continue;
						}

						if( $objApplicant->CriminalSearchResult ) {
							if( $objApplicant->CriminalSearchResult->CriminalRecommendation ) {
								 if( 0 < \Psi\CStringService::singleton()->strlen( $objApplicant->CriminalSearchResult->CriminalRecommendation['code'] ) ) {
								 	$this->setCrimeStatus( $objApplicant->CriminalSearchResult->CriminalRecommendation['code'] );
								 	$this->setCrimeMessage( $objApplicant->CriminalSearchResult->CriminalRecommendation );
								 }
							}
						}

						if( $objApplicant->Messages ) {
							if( $objApplicant->Messages->CreditMessages ) {
								if( true == in_array( $objApplicant->Messages->CreditMessages->CreditMessage['id'], $arrintValidErrorNumbers ) ) {
									if( 0 < \Psi\CStringService::singleton()->strlen( $objApplicant->Messages->CreditMessages->CreditMessage ) ) {
										$this->setAlertMessage( ' - ' . $objApplicant->Messages->CreditMessages->CreditMessage );
									}
								}
							}

							if( $objApplicant->Messages->SSNMessages ) {
								if( true == in_array( $objApplicant->Messages->SSNMessages->SSNMessage['id'], $arrintValidErrorNumbers ) ) {
									if( 0 < \Psi\CStringService::singleton()->strlen( $this->getAlertMessage() ) ) $strAlertMessage = $this->getAlertMessage() . '<br>';
									if( 0 < \Psi\CStringService::singleton()->strlen( $objApplicant->Messages->SSNMessages->SSNMessage ) ) {
										$this->setAlertMessage( $strAlertMessage . ' - ' . $objApplicant->Messages->SSNMessages->SSNMessage );
									}
								}
							}

							if( $objApplicant->Messages->OtherMessages ) {
								if( true == in_array( $objApplicant->Messages->OtherMessages->OtherMessage['id'], $arrintValidErrorNumbers ) ) {
									if( 0 < \Psi\CStringService::singleton()->strlen( $this->getAlertMessage() ) ) $strAlertMessage = $this->getAlertMessage() . '<br>';
									if( 0 < \Psi\CStringService::singleton()->strlen( $objApplicant->Messages->OtherMessages->OtherMessage ) ) {
										$this->setAlertMessage( $strAlertMessage . ' - ' . $objApplicant->Messages->OtherMessages->OtherMessage );
									}
								}
							}

							if( $objApplicant->Messages->CrimCHECKMessages ) {
								if( true == in_array( $objApplicant->Messages->CrimCHECKMessages->CrimCHECKMessage['id'], $arrintValidErrorNumbers ) ) {
									if( 0 < \Psi\CStringService::singleton()->strlen( $this->getAlertMessage() ) ) $strAlertMessage .= '<br>';
									$this->setAlertMessage( $objApplicant->Messages->CrimCHECKMessages->CrimCHECKMessage );
								}
							}
						}

						if( $objApplicant->Score ) {
							if( $objApplicant->Score->ScoreDescription ) {
								$this->setScoreXDecision( $objApplicant->Score->ScoreDescription );
							}

							if( $objApplicant->Score->ScoreCategory ) {
								$this->setScoreXStatus( $objApplicant->Score->ScoreCategory );
							}
						}
						$strScoreXStatus = trim( $this->getScoreXStatus() );
						if( true == empty( $strScoreXStatus ) && $objApplicant->Score_Status ) {
							$this->setScoreXStatus( $objApplicant->Score_Status );
						}
					}
				}
    			break;

			case CTransmissionVendor::ONSITE:
				if( true == isset( $objSimpleXml->ApplicantScreening->Response->ApplicationDecision ) && 0 < \Psi\CStringService::singleton()->strlen( $objSimpleXml->ApplicantScreening->Response->ApplicationDecision ) ) {
					$this->setScoreXStatus( $objSimpleXml->ApplicantScreening->Response->ApplicationDecision );
				}

				( true == isset( $objSimpleXml->ApplicantScreening->Credit ) && false == valArr( $objSimpleXml->ApplicantScreening->Credit ) ) ? $objSimpleXml->ApplicantScreening->Credit = [ $objSimpleXml->ApplicantScreening->Credit ] : NULL;

				if( true == isset( $objSimpleXml->ApplicantScreening->Credit ) ) {
					foreach( $objSimpleXml->ApplicantScreening->Credit as $objStdCredit ) {

						if( $objApplicant->getId() != $objStdCredit->ApplicantIdentifier ) {
							continue;
						}

						if( true == isset( $objStdCredit->CreditCommentText1 ) && 0 < \Psi\CStringService::singleton()->strlen( $objStdCredit->CreditCommentText1 ) ) {
							$this->setAlertMessage( $objStdCredit->CreditCommentText1 );
						}
					}
				}
				break;

			default:
    	}

    	return;
    }

	/**
	 * Fetch Functions
	 */

	public function fetchApplicantApplicationTransmissions( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchSimpleApplicantApplicationTransmissionsByTransmissionIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

    public function fetchApplicant( $objDatabase ) {
    	return CApplicants::fetchApplicantByApplicantApplicationIdByCid( $this->getApplicantApplicationId(), $this->getCid(), $objDatabase );
    }

	public function fetchTransmissionType( $objDatabase ) {
		return	\Psi\Eos\Entrata\CTransmissionTypes::createService()->fetchTransmissionTypeById( $this->getTransmissionTypeId(), $objDatabase );
	}

	public function fetchTransmissionVendor( $objDatabase ) {
		return	\Psi\Eos\Entrata\CTransmissionVendors::createService()->fetchTransmissionVendorById( $this->getTransmissionVendorId(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }

	/**
	 * Database Functions
	 */

	/**
	 * @param  int    $intCurrentUserId
	 * @param  \CDatabase    $objDatabase
	 * @param bool $boolReturnSqlOnly
	 * @return bool|string
	 */
	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$mixResult = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( true == $boolReturnSqlOnly ) {
			return $mixResult;
		}
		if( false == $mixResult ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			return false;
		}

		$this->setUseAmazonFileStorage( true );
		$this->setStorageGatewayName( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );

		$boolStoreResult = true;
		$objSystemConfig = \Psi\Eos\Entrata\CSystemConfigs::createService()->fetchSystemConfigByKey( 'DISABLE_INTEGRATION_DEBUG_MODE', $objDatabase );
		if( true == valObj( $objSystemConfig, 'CSystemConfig', 'Value', 1 ) || ( false == valStr( $this->m_strRequestContent ) && false == valStr( $this->m_strResponseContent ) ) ) {
			$boolStoreResult = false;
		}
		// check if logging is enabled or not. Do nothing if logging is disabled.
		if( false == $boolStoreResult ) {
			if( true == $this->getUseAmazonFileStorage() ) {
				$this->setUseAmazonFileStorage( false );
				$this->update( $intCurrentUserId, $objDatabase );
			}

			return true;
		}

		// store log file to aws
		if( false != $this->getUseAmazonFileStorage() && false != $this->storeLogFile( $intCurrentUserId, $objDatabase ) ) {
			return true;
		}
		// store log file to non backup mounts if failed to store on AWS or UseAmazonFileStorage = false.
		if( true == $this->getUseAmazonFileStorage() ) {
			$this->setUseAmazonFileStorage( false );
			$this->update( $intCurrentUserId, $objDatabase );
		}

		$this->setStorageGatewayName( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM );
		$this->storeLogFile( $intCurrentUserId, $objDatabase );

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$this->deleteTransmissionLogFile();

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function buildStoragePath() {
		// TODO Need to comfirm with Sandeep about updating the NBM path with container name.
		return getNonBackupMountsPath( $this->getCid(), CONFIG_OSG_BUCKET_SYSTEM_TRANSMISSIONS ) . date( '/Y/m/d/', strtotime( $this->getTransmissionDatetime() ) );
	}

	public function getSimpleXmlObject( $strXml ) {
		if( \Psi\CStringService::singleton()->strstr( $strXml, '&' ) ) {
			$strXml = str_replace( '&', '&amp;', $strXml );
		}

		$objSimpleXml = simplexml_load_string( $strXml, NULL, LIBXML_NOERROR );

		return $objSimpleXml;
	}

	// Added this function to remove notice of undefined offset.

	public function getUnSerializedData( $strSerializedData ) {
		if( false == $this->isSerializedData( $strSerializedData ) ) {
			if( false == valStr( $strSerializedData ) ) return NULL;
			$strSerializedData = $this->getSimpleXmlObject( $this->getResponseContent() );
			return $strSerializedData;
		}

		$strUnSerializedData = unserialize( $strSerializedData );

		if( false == valObj( $strUnSerializedData, 'stdClass' ) ) {
			$strUnSerializedData = $this->getSimpleXmlObject( $strSerializedData );
		}

		if( false == is_object( $strUnSerializedData ) ) return NULL;

		return $strUnSerializedData;
	}

	public function isSerializedData( $strSerializedData ) {

		if( false == valStr( $strSerializedData ) ) return false;

		// to check if serializedData is NULL
		if( 'N;' == $strSerializedData ) return true;

		if( false == preg_match( '/^([adObis]):/', $strSerializedData, $arrstrMatches ) ) return false;

		switch( $arrstrMatches[1] ) {

			case 'a':
			case 'O':
			case 's':
				if( true == preg_match( '/^{$arrstrMatches[1]}:[0-9]+:.*[;}]\$/s', $strSerializedData ) )
				return true;
				break;

			case 'b':
			case 'i':
			case 'd':
				if( true == preg_match( '/^{$arrstrMatches[1]}:[0-9.E-]+;\$/', $strSerializedData ) )
				return true;
				break;

			default:
				// added default
		}

		return false;
	}

	public function formatDate( $strDate ) {
		$arrstrDateSplit 		= preg_split( '[-]', $strDate );
		$strFormattedDate 	= $arrstrDateSplit[0] . '/' . $arrstrDateSplit[2] . '/' . $arrstrDateSplit[1];

		return $strFormattedDate;
	}

	public function handleEncryptSensitiveScreeningInfo( $strRequestFile, $strResponseFile, $boolIsRequestContent, $boolIsResponseContent ) {

		if( true == in_array( $this->getTransmissionVendorId(), CTransmissionVendor::$c_arrintTransmissionVendorIdsHavingSensitiveScreeningInfo ) ) {
			// Convert xml string to object
			$objSimpleXml 			= $this->getSimpleXmlObject( $strRequestFile );
			$objResponseSimpleXml 	= $this->getSimpleXmlObject( $strResponseFile );

		} elseif( true == in_array( $this->getTransmissionVendorId(), [ CTransmissionVendor::VERIFIRST, CTransmissionVendor::ONSITE, CTransmissionVendor::FIRST_ADVANTAGE, CTransmissionVendor::SCREENING_REPORTS, CTransmissionVendor::BACKGROUND_INVESTIGATIONS ] ) ) {
			// unserialise the content
			$objUnSerialise 		= @unserialize( $strRequestFile );
			$objResponseUnserialise = @unserialize( $strResponseFile );
		}

		switch( $this->getTransmissionVendorId() ) {

			case CTransmissionVendor::RESIDENT_VERIFY:
			case CTransmissionVendor::TAZ_WORKS:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						if( 'submit' == $objSimpleXml->BackgroundSearchPackage['action'] ) {

							$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->BackgroundSearchPackage->PersonalData->DemographicDetail->GovernmentId ) ) ? $objSimpleXml->BackgroundSearchPackage->PersonalData->DemographicDetail->GovernmentId : '' ) );
							$strDateOfBirth 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->BackgroundSearchPackage->PersonalData->DemographicDetail->DateOfBirth ) ), 0 );
							$strPhoneNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->BackgroundSearchPackage->Screenings->Screening->ContactInfo->Telephone ), 0 );

							$objSimpleXml->BackgroundSearchPackage->PersonalData->DemographicDetail->GovernmentId 		= $strSocSecNumber;
							$objSimpleXml->BackgroundSearchPackage->PersonalData->DemographicDetail->DateOfBirth 		= $strDateOfBirth;
							$objSimpleXml->BackgroundSearchPackage->Screenings->Screening->ContactInfo->Telephone		= $strPhoneNumber;

							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						}

						// need to handle check status
						if( 'searchstatus' == $objSimpleXml->BackgroundSearchPackage['action'] || 'status' == $objSimpleXml->BackgroundSearchPackage['action'] ) {
							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						}
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::RENT_GROW:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate ) ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber ) );

							$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate 			= $strBirthDate;
							$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber 			= $strSocSecNumber;
						}

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objResponseSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate ) ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber ) );

							$objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate 		= $strBirthDate;
							$objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber 		= $strSocSecNumber;
						}

						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::RENTAL_HISTORY_REPORTS:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->Birthdate ) ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->SSN ) );
							$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->DriversLicense ), 0 );
							$strHomePhone		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->HomePhone ), 0 );
							$strCellPhone 		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->CellPhone ), 0 );

							$objSimpleXml->Applicant[$intApplicantCount]->Birthdate 		= $strBirthDate;
							$objSimpleXml->Applicant[$intApplicantCount]->SSN 				= $strSocSecNumber;
							$objSimpleXml->Applicant[$intApplicantCount]->DriversLicense	= $strLicenseNumber;
							$objSimpleXml->Applicant[$intApplicantCount]->HomePhone			= $strHomePhone;
							$objSimpleXml->Applicant[$intApplicantCount]->CellPhone			= $strCellPhone;
						}
						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::RESIDENT_CHECK:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {

							$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );
							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
								$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
								$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
								$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->Information->DriversLicenseID ) );

								$objSimpleXml->Applicant[$intApplicantCount]->Information->Birthdate 		= $strBirthDate;
								$objSimpleXml->Applicant[$intApplicantCount]->Information->SocSecNumber 	= $strSocSecNumber;
								$objSimpleXml->Applicant[$intApplicantCount]->Information->DriversLicenseID	= $strLicenseNumber;
							}

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::SAFERENT:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$objCompanyUser 	= unserialize( $_SESSION['resident_works'][$this->getCid()]['company_user'] );

						if( ( true == isset( $objSimpleXml->User_Info->Web_Account ) || true == isset( $objSimpleXml->Property_Info->Web_Account ) ) && ( true == valObj( $objCompanyUser, 'CCompanyUser' ) && false == $objCompanyUser->getIsPSIUser() ) ) {

							if( true == isset( $objSimpleXml->User_Info->Web_Account ) ) {
								$strUserInfoWebUserId			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->User_Info->Web_Account->Web_UserID ), 0 );
								$strUserInfoWebPassword			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->User_Info->Web_Account->Web_Password ), 0 );
								$objSimpleXml->User_Info->Web_Account->Web_UserID		= $strUserInfoWebUserId;
								$objSimpleXml->User_Info->Web_Account->Web_Password		= $strUserInfoWebPassword;
							} elseif( true == isset( $objSimpleXml->Property_Info->Web_Account->Web_UserID ) ) {
								$strPropertyInfoWebUserId		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Property_Info->Web_Account->Web_UserID ), 0 );
								$strPropertyInfoWebPassword		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Property_Info->Web_Account->Web_Password ), 0 );
								$objSimpleXml->Property_Info->Web_Account->Web_UserID	= $strPropertyInfoWebUserId;
								$objSimpleXml->Property_Info->Web_Account->Web_Password	= $strPropertyInfoWebPassword;
							}

							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						} elseif( true == isset( $objSimpleXml->Applicant_Information ) ) {
							$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant_Information );

							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
								$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant_Information[$intApplicantCount]->SocialNum_DateOfBirth->Date_of_Birth ), 0 );
								$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant_Information[$intApplicantCount]->SocialNum_DateOfBirth->Social_Sec_Num ) );
								$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant_Information[$intApplicantCount]->DL_Details->DL_Number ), 0 );
								$strPhoneNumber		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant_Information[$intApplicantCount]->Contact->EveningTime_PhoneNum ), 0 );

								$objSimpleXml->Applicant_Information[$intApplicantCount]->SocialNum_DateOfBirth->Date_of_Birth	= $strBirthDate;
								$objSimpleXml->Applicant_Information[$intApplicantCount]->SocialNum_DateOfBirth->Social_Sec_Num	= $strSocSecNumber;
								$objSimpleXml->Applicant_Information[$intApplicantCount]->DL_Details->DL_Number					= $strLicenseNumber;
								$objSimpleXml->Applicant_Information[$intApplicantCount]->Contact->EveningTime_PhoneNum			= $strPhoneNumber;
							}

							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						} elseif( true == $boolIsRequestContent && true == isset( $objSimpleXml->Applicant->ApplicantID ) ) {
							// need to handle 2nd call
							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						} else {
							$this->m_objSerialisedRequest = $strRequestFile;
						}
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						if( true == isset( $objResponseSimpleXml->ApplicantList->Applicant->CustomApplicantID ) ) {
							$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objResponseSimpleXml->Applicant_Information );

							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
								$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseSimpleXml->ApplicantList->Applicant[$intApplicantCount]->SocialNum_DateOfBirth->Date_of_Birth ), 0 );
								$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseSimpleXml->ApplicantList->Applicant[$intApplicantCount]->SocialNum_DateOfBirth->Social_Sec_Num ) );

								$objResponseSimpleXml->ApplicantList->Applicant[$intApplicantCount]->SocialNum_DateOfBirth->Date_of_Birth 	= $strBirthDate;
								$objResponseSimpleXml->ApplicantList->Applicant[$intApplicantCount]->SocialNum_DateOfBirth->Social_Sec_Num	= $strSocSecNumber;
							}

							$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
						} elseif( true == $boolIsResponseContent && true == isset( $objResponseSimpleXml->Applicant->Applicant_CustomerID ) ) {
							// need to handle 2nd call
							$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
						} else {
							$this->m_objSerialisedResponse = $strResponseFile;
						}
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::LEASINGDESK:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Application->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Application->Applicant[$intApplicantCount]->DateOfBirth ) ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Application->Applicant[$intApplicantCount]->SSN ) );
							$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Application->Applicant[$intApplicantCount]->IDNumber ), 0 );

							$objSimpleXml->Application->Applicant[$intApplicantCount]->DateOfBirth			= $strBirthDate;
							$objSimpleXml->Application->Applicant[$intApplicantCount]->SSN					= $strSocSecNumber;
							$objSimpleXml->Application->Applicant[$intApplicantCount]->IDNumber				= $strLicenseNumber;
						}
						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::SOUND_SCREENING:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
							$strHomePhone		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->Information->HomePhone ), 0 );

							$objSimpleXml->Applicant[$intApplicantCount]->Information->Birthdate		= $strBirthDate;
							$objSimpleXml->Applicant[$intApplicantCount]->Information->SocSecNumber		= $strSocSecNumber;
							$objSimpleXml->Applicant[$intApplicantCount]->Information->HomePhone		= $strHomePhone;
						}

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::FABCO:
			case CTransmissionVendor::AMRENT:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate				= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate ) ), 0 );
							$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber ) );
							$strDriversLicenceNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID ), 0 );

							$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate					= $strBirthDate;
							$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber				= $strSocSecNumber;
							$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID 			= $strDriversLicenceNumber;
						}
						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objResponseSimpleXml->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate				= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate ) ), 0 );
							$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber ) );
							$strDriversLicenceNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID ) ) ? $objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID : '' ), 0 );

							$objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate				= $strBirthDate;
							$objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber			= $strSocSecNumber;
							$objResponseSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID 		= $strDriversLicenceNumber;
						}

						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::FIRST_ADVANTAGE:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objUnSerialise, 'stdClass' ) ) {
						$objReSerialiseObject 	= simplexml_load_string( $objUnSerialise->xml, NULL, LIBXML_NOERROR );

						if( true == valObj( $objReSerialiseObject, 'SimpleXMLElement' ) ) {
							$intApplicantsCount 	= \Psi\Libraries\UtilFunctions\count( $objReSerialiseObject->Applicant );

							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
								$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objReSerialiseObject->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
								$strSocSecNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objReSerialiseObject->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
								$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objReSerialiseObject->Applicant[$intApplicantCount]->Information->DriversLicenseID ) ) ? $objReSerialiseObject->Applicant[$intApplicantCount]->Information->DriversLicenseID :'' ),  0 );

								$objReSerialiseObject->Applicant[$intApplicantCount]->Information->Birthdate			= $strBirthDate;
								$objReSerialiseObject->Applicant[$intApplicantCount]->Information->SocSecNumber			= $strSocSecNumber;
								$objReSerialiseObject->Applicant[$intApplicantCount]->Information->DriversLicenseID 	= $strLicenseNumber;
							}

							$this->m_objSerialisedRequest = $objReSerialiseObject->asXML();
						}
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					$objResponseUnserialise = simplexml_load_string( @unserialize( $strResponseFile ) );

					if( true == is_object( $objResponseUnserialise ) ) {
						$intApplicantsCount 	= \Psi\Libraries\UtilFunctions\count( $objResponseUnserialise->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseUnserialise->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
							$strSocSecNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
							$strLicenseNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->Applicant[$intApplicantCount]->Information->DriversLicenseID ), 0 );

							$objResponseUnserialise->Applicant[$intApplicantCount]->Information->Birthdate			= $strBirthDate;
							$objResponseUnserialise->Applicant[$intApplicantCount]->Information->SocSecNumber		= $strSocSecNumber;
							$objResponseUnserialise->Applicant[$intApplicantCount]->Information->DriversLicenseID	= $strLicenseNumber;
						}
						$this->m_objSerialisedResponse = $objResponseUnserialise->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::SCREENING_REPORTS:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objUnSerialise, 'stdClass' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objUnSerialise->ApplicantScreening->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
							$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
							$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone ) ) ? $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone : '' ), 0 );
							$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone ) ) ? $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone : '' ), 0 );

							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate 			= $strBirthDate;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber 		= $strSocSecNumber;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone			= $strHomePhone;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone			= $strWorkPhone;
						}

						$this->m_objSerialisedRequest = serialize( $objUnSerialise );
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseUnserialise, 'stdClass' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objResponseUnserialise->ApplicantScreening->Applicant );

						if( 1 == $intApplicantsCount ) {
							$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseUnserialise->ApplicantScreening->Applicant->Information->Birthdate ) ), 0 );
							$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->ApplicantScreening->Applicant->Information->SocSecNumber ) );
							$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone : '' ), 0 );
							$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone : '' ), 0 );

							$objResponseUnserialise->ApplicantScreening->Applicant->Information->Birthdate 			= $strBirthDate;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->SocSecNumber 		= $strSocSecNumber;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone			= $strHomePhone;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone			= $strWorkPhone;
						}

						if( $intApplicantsCount > 1 ) {
							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {

								$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
								$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
								$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone : '' ), 0 );
								$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone : '' ), 0 );

								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate 			= $strBirthDate;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber 		= $strSocSecNumber;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone			= $strHomePhone;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone			= $strWorkPhone;
							}
						}
						$this->m_objSerialisedResponse = serialize( $objResponseUnserialise );
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::ONSITE:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objUnSerialise, 'stdClass' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objUnSerialise->ApplicantScreening->Applicant );

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
							$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
							$strDriversLicenceNumber = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID ) ) ? $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID : '' ), 0 );
							$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone ) ) ? $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone : '' ), 0 );
							$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone ) ) ? $objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone : '' ), 0 );

							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate 			= $strBirthDate;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber 		= $strSocSecNumber;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID	= $strDriversLicenceNumber;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone			= $strHomePhone;
							$objUnSerialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone			= $strWorkPhone;
						}
						$this->m_objSerialisedRequest = serialize( $objUnSerialise );
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseUnserialise, 'stdClass' ) ) {
						$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objResponseUnserialise->ApplicantScreening->Applicant );

						if( 1 == $intApplicantsCount ) {
							$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseUnserialise->ApplicantScreening->Applicant->Information->Birthdate ) ), 0 );
							$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->ApplicantScreening->Applicant->Information->SocSecNumber ) );
							$strDriversLicenceNumber = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant->Information->DriversLicenseID ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant->Information->DriversLicenseID : '' ), 0 );
							$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone : '' ), 0 );
							$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone : '' ), 0 );

							$objResponseUnserialise->ApplicantScreening->Applicant->Information->Birthdate 			= $strBirthDate;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->SocSecNumber 		= $strSocSecNumber;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->DriversLicenseID	= $strDriversLicenceNumber;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->HomePhone			= $strHomePhone;
							$objResponseUnserialise->ApplicantScreening->Applicant->Information->WorkPhone			= $strWorkPhone;
						}

						if( $intApplicantsCount > 1 ) {
							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {

								$strBirthDate 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate ) ), 0 );
								$strSocSecNumber 		 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber ) );
								$strDriversLicenceNumber = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID : '' ), 0 );
								$strHomePhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone : '' ), 0 );
								$strWorkPhone 			 = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone ) ) ? $objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone : '' ), 0 );

								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->Birthdate 			= $strBirthDate;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->SocSecNumber 		= $strSocSecNumber;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->DriversLicenseID	= $strDriversLicenceNumber;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->HomePhone			= $strHomePhone;
								$objResponseUnserialise->ApplicantScreening->Applicant[$intApplicantCount]->Information->WorkPhone			= $strWorkPhone;
							}
						}
						$this->m_objSerialisedResponse = serialize( $objResponseUnserialise );
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::VERIFIRST:
				if( true == $boolIsRequestContent ) {
					if( true == is_object( $objUnSerialise ) ) {
						$intApplicantsCount = ( true == isset( $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants ) ) ? \Psi\Libraries\UtilFunctions\count( $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants ) : 0;

						for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {
							$strBirthDate 		= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->DateOfBirth ) ) ? strtotime( $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->DateOfBirth ) : '' ), 0 );
							$strSocSecNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->SSN ) );
							$strPhoneNumber 	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->Phone ) );

							$objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->DateOfBirth 	= $strBirthDate;
							$objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->SSN 			= $strSocSecNumber;
							$objUnSerialise->VBSScreeningRequest->VeriguideOrder->Request->Applicants[$intApplicantCount]->Phone		= $strPhoneNumber;
						}

						$this->m_objSerialisedRequest = serialize( $objUnSerialise );
					} else {
						$this->m_objSerialisedRequest = $strRequestFile;
					}
				}

				if( true == $boolIsResponseContent ) {
					$this->m_objSerialisedResponse = ( true == is_object( $objResponseUnserialise ) ) ? serialize( $objResponseUnserialise ) : $strResponseFile;
				}
				break;

			case CTransmissionVendor::NTN:
				if( true == $boolIsRequestContent ) {
						if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {
							$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->Applicant );

							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {

								$strDateOfBirth 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate ) ), 0 );
								$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber ) ) ? $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber : '' ) );
								$strDriversLicenceNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID ) ) ? $objSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID : '' ), 0 );

								$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->Birthdate			= $strDateOfBirth;
								$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->SocSecNumber		= $strSocSecNumber;
								$objSimpleXml->Applicant[$intApplicantCount]->AS_Information->DriversLicenseID	= $strDriversLicenceNumber;
							}

							$this->m_objSerialisedRequest = $objSimpleXml->asXML();
						} else {
								$this->m_objSerialisedRequest = $strRequestFile;
						}
				}

				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::TRANSUNION_CREDIT_RETRIEVER_CR6:
					if( true == $boolIsRequestContent ) {
						if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {

							$intApplicantsCount = \Psi\Libraries\UtilFunctions\count( $objSimpleXml->application->applicants->applicant );

							for( $intApplicantCount = 0; $intApplicantCount < $intApplicantsCount; $intApplicantCount++ ) {

								$strDateOfBirth 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->application->applicants->applicant[$intApplicantCount]->dateOfBirth ) ), 0 );
								$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->application->applicants->applicant[$intApplicantCount]->ssn ) ) ? $objSimpleXml->application->applicants->applicant[$intApplicantCount]->ssn : '' ) );

								$objSimpleXml->application->applicants->applicant[$intApplicantCount]->dateOfBirth			= $strDateOfBirth;
								$objSimpleXml->application->applicants->applicant[$intApplicantCount]->ssn		   			= $strSocSecNumber;
							}

							$this->m_objSerialisedRequest = $objSimpleXml->asXML();

						} else {
							$this->m_objSerialisedRequest = $strRequestFile;
						}
					}

					if( true == $boolIsResponseContent ) {
						if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
							$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
						} else {
							$this->m_objSerialisedResponse = $strResponseFile;
						}
					}
				break;

			case CTransmissionVendor::LEASINGDESK_MITS:
				if( true == $boolIsRequestContent ) {
					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {

						$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->SocSecNumber ) ) ? $objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->SocSecNumber : '' ) );
						$strDateOfBirth 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->Birthdate ) ), 0 );
						$strDriversLicenceNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->DriversLicenseID ) ) ? $objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->DriversLicenseID : '' ), 0 );

						$objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->SocSecNumber 		= $strSocSecNumber;
						$objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->Birthdate 		= $strDateOfBirth;
						$objSimpleXml->ProcessApplication->ApplicantScreening->Applicant->AS_Information->DriversLicenseID	= $strDriversLicenceNumber;

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					}
				}
				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::BCS:
			case CTransmissionVendor::REALID:
			case CTransmissionVendor::GENERIC_SCREENING_LIBRARY:
				if( true == $boolIsRequestContent ) {

					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {

						$strSocSecNumber       = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicants->Applicant->AS_Information->SocSecNumber ) ) ? $objSimpleXml->Applicants->Applicant->AS_Information->SocSecNumber : '' ) );
						$strDateOfBirth       = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicants->Applicant->AS_Information->Birthdate ) ), 0 );
						$strDriversLicenceNumber  = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicants->Applicant->AS_Information->DriversLicenseID ) ) ? $objSimpleXml->Applicants->Applicant->AS_Information->DriversLicenseID : '' ), 0 );

						$objSimpleXml->Applicants->Applicant->AS_Information->SocSecNumber     = $strSocSecNumber;
						$objSimpleXml->Applicants->Applicant->AS_Information->Birthdate     = $strDateOfBirth;
						$objSimpleXml->Applicants->Applicant->AS_Information->DriversLicenseID  = $strDriversLicenceNumber;

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					}
				}
				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			case CTransmissionVendor::THE_SCREENING_PROS:
				if( true == $boolIsRequestContent ) {

					if( true == valObj( $objSimpleXml, 'SimpleXMLElement' ) ) {

						$strSocSecNumber 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicant->AS_Information->SocSecNumber ) ) ? $objSimpleXml->Applicant->AS_Information->SocSecNumber : '' ) );
						$strDateOfBirth 			= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', strtotime( $objSimpleXml->Applicant->AS_Information->Birthdate ) ), 0 );
						$strDriversLicenceNumber	= CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', ( true == isset( $objSimpleXml->Applicant->AS_Information->DriversLicenseID ) ) ? $objSimpleXml->Applicant->AS_Information->DriversLicenseID : '' ), 0 );

						$objSimpleXml->Applicant->AS_Information->SocSecNumber 		= $strSocSecNumber;
						$objSimpleXml->Applicant->AS_Information->Birthdate 		= $strDateOfBirth;
						$objSimpleXml->Applicant->AS_Information->DriversLicenseID	= $strDriversLicenceNumber;

						$this->m_objSerialisedRequest = $objSimpleXml->asXML();
					}
				}
				if( true == $boolIsResponseContent ) {
					if( true == valObj( $objResponseSimpleXml, 'SimpleXMLElement' ) ) {
						$this->m_objSerialisedResponse = $objResponseSimpleXml->asXML();
					} else {
						$this->m_objSerialisedResponse = $strResponseFile;
					}
				}
				break;

			default:
		}
	}

	public function storeLogFile( $intCurrentUserId, $objDatabase, $strFileName = NULL ) {

		$objSystemConfig = \Psi\Eos\Entrata\CSystemConfigs::createService()->fetchSystemConfigByKey( 'DISABLE_INTEGRATION_DEBUG_MODE', $objDatabase );

		if( true == valObj( $objSystemConfig, 'CSystemConfig', 'Value', 1 ) || ( false == valStr( $this->m_strRequestContent ) && false == valStr( $this->m_strResponseContent ) ) ) {
			if( true == $this->getUseAmazonFileStorage() ) {
				$this->setUseAmazonFileStorage( false );
				$this->update( $intCurrentUserId, $objDatabase );
			}

			return true;
		}

		$boolIsValid = true;

		// Force reset the Gateway to shared file system for development environment.
		if( 'production' != CConfig::get( 'environment' ) && ( false == valStr( $this->getStorageGatewayName() ) || CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 == $this->getStorageGatewayName() ) ) {
			$this->setStorageGatewayName( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM );

			if( true == $this->getUseAmazonFileStorage() ) {
				$this->setUseAmazonFileStorage( false );
			}
		}

		$this->m_strResponseContent = str_replace( 'PSI_SEPARATOR', '', $this->m_strResponseContent );
		$strTransmissionResult      = $this->m_strRequestContent . 'PSI_SEPARATOR' . $this->m_strResponseContent;

		$strFileName = $strFileName ?? '_result.txt';

		$strExpiresOn = NULL;
		$strTitle     = 'Property Id:' . $this->getPropertyId();

		switch( $this->getTransmissionVendorId() ) {
			case CTransmissionVendor::LRO:
				$strExpiresOn = date( 'm/d/Y', strtotime( '+16 days' ) );
				$strTitle     .= ' LRO log file';
				break;

			case CTransmissionVendor::YIELDSTAR:
				$strExpiresOn = date( 'm/d/Y', strtotime( '+16 days' ) );
				$strTitle     .= ' YS log file:' . $strFileName;
				break;

			default:
				$strExpiresOn = date( 'm/d/Y', strtotime( '+91 days' ) );
				break;
		}

		$this->setTitle( $strTitle );

		$arrmixRequest = $this->createPutGatewayRequest( [
			'storageGateway' => $this->getStorageGatewayName(),
			'data'           => $strTransmissionResult,
			'expires'        => $strExpiresOn,
			'noEncrypt'      => true,
			'mountSystem'    => 'NON_BACKUP_MOUNTS'
		], $strFileName );

		$arrmixPutObjectResponse = $this->getOrLoadObjectStorage()->putObject( $arrmixRequest );
		if( false == is_null( $arrmixPutObjectResponse ) && true == $arrmixPutObjectResponse->isSuccessful() ) {
			$this->setObjectStorageGatewayResponse( $arrmixPutObjectResponse )->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, $strFileName );
		} else {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getOrLoadObjectStorage() {
		if( false == is_null( $this->m_objObjectStorage ) ) {
			return $this->m_objObjectStorage;
		}

		$this->m_objObjectStorage = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );
		$this->m_objObjectStorage->initialize();
		return $this->m_objObjectStorage;
	}

	public function deleteTransmissionLogFile() {
		$strStoragePath			= $this->buildStoragePath() . $this->getId();
		$boolIsSuccess			= true;
		$arrstrFileNames		= [ '_result.txt', '_RentSummary.csv', '_AvailableUnits.csv', '_RenewalLeaseTermRent.csv', '_LeaseTermRent.csv' ];

		foreach( $arrstrFileNames as $strFileName ) {
			$strInputFilePathResult	= $strStoragePath . $strFileName;

			if( true == CFileIo::fileExists( $strInputFilePathResult ) && false == CFileIo::deleteFile( $strInputFilePathResult ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to delete ' . $strFileName . ' file for transmission id : ' . $this->getId() ) );
				$boolIsSuccess = false;
			}
		}

		return $boolIsSuccess;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CONFIG_OSG_BUCKET_SYSTEM_TRANSMISSIONS;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		// Do not add cid in the key for SHARED_FILE_SYSTEM Gateway. Otherwise additional cid folder will be created under container's folder.
		$strKey = date( 'Y/m/d/', strtotime( $this->getTransmissionDatetime() ) ) . $this->getId() . $strReferenceTag;
		if( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 == $this->getStorageGatewayName() ) {
			$strKey = $this->getCid() . '/' . $strKey;
		}

		return $strKey;
	}

}
?>