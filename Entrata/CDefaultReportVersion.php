<?php

class CDefaultReportVersion extends CBaseDefaultReportVersion {

	protected $m_arrmixDefinition;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMajor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitleAddendum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefinition( $intReportTypeId ) {
		return $this->valClass( $intReportTypeId ) && $this->valValidationCallbackFunction();
	}

	public function valClass( $intReportTypeId ) {
		$boolIsValid = true;
		if( CReportType::CONTROLLER != $intReportTypeId && ( false == valStr( $this->getDefinitionField( 'class' ) ) || false == class_exists( $this->getClass() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'definition', 'File name is required.</br>' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValidationCallbackFunction() {
		$boolIsValid = true;
		if( true == valArr( $this->getValidationCallbacks() ) ) {
			$objReportsValidation = new CReportsValidation( '', '', '', '' );
			foreach( $this->getValidationCallbacks() as $strFunction => $arrstrArgs ) {
				if( false == method_exists( $objReportsValidation, $strFunction ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'validation_callbacks', 'Validation callback function "' . $strFunction . '" does not exist.' ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valExpiration() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLatest() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault( $arrstrReportVersionsData, $boolReportIsPublished, $intReportTypeId, $intVersionsCount ) {
		$boolIsValid = true;
		if( !json_decode( $this->getDefinition(), true )['is_published'] && $this->getIsDefault() && $boolReportIsPublished ) {
			foreach( $arrstrReportVersionsData as $arrstrReportVersionData ) {
				if( $this->getId() == $arrstrReportVersionData['id'] && !$arrstrReportVersionData['definition']['is_published'] ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_default', 'Unpublished version cannot be selected as default.</br>' ) );
					$boolIsValid = false;
					break;
				}
			}
		}

		if( ( valArr( json_decode( $this->getDefinition(), true )['validation_callbacks']['isAllowedClient'] ) ) && $this->getIsDefault() && CReportType::SYSTEM == $intReportTypeId && 1 != $intVersionsCount ) {
			foreach( $arrstrReportVersionsData as $arrstrReportVersionData ) {
				if( $this->getId() == $arrstrReportVersionData['id'] && valArr( $arrstrReportVersionData['definition']['validation_callbacks'][0]['args'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_default', 'Versions that are set to Entrata Only or that are Client Specific cannot be set as the default.</br>' ) );
					$boolIsValid = false;
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function valRunOnReplica( $arrstrReportVersionsData ) {
		$boolIsValid = true;

		if( json_decode( $this->getDefinition(), true )['class'] && CReportVersion::NEVER != $this->getRunOnReplicas() && $this->getIsDefault() ) {
			foreach( $arrstrReportVersionsData as $arrstrReportVersionData ) {
				if( $this->getId() == $arrstrReportVersionData['id'] ) {
					$objClassName = new $arrstrReportVersionData['definition']['class']();
					if( $objClassName->getNeedsCacheRebuild() ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'run_on_replicas', 'This report calls the cache rebuild function and cannot be run on read-only replicas (ROs).</br>' ) );
						$boolIsValid = false;
						break;
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intReportTypeId, $arrstrReportVersionsData, $objDatabase, $intVersionsCount, $boolReportIsPublished = false ) {
		unset( $objDatabase );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( !$this->getIsDraft() ) {
					$boolIsValid &= $this->valDefinition( $intReportTypeId );
					$boolIsValid &= $this->valDescription();
					$boolIsValid &= $this->valRunOnReplica( $arrstrReportVersionsData );
				}
				$boolIsValid &= $this->valIsDefault( $arrstrReportVersionsData, $boolReportIsPublished, $intReportTypeId, $intVersionsCount );
				break;

			case VALIDATE_UPDATE:
				if( !$this->getIsDraft() ) {
					if( true == $boolReportIsPublished && true == $this->getDefinitionField( 'is_published' ) ) {
						$boolIsValid &= $this->valDefinition( $intReportTypeId );
					}
					$boolIsValid &= $this->valDescription();
					$boolIsValid &= $this->valRunOnReplica( $arrstrReportVersionsData );
				}
				$boolIsValid &= $this->valIsDefault( $arrstrReportVersionsData, $boolReportIsPublished, $intReportTypeId, $intVersionsCount );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Getters
	 */

	public function getDefinitionData() {
		return $this->m_arrmixDefinition;
	}

	public function getDefinitionField( $strFieldName ) {
		if( false == isset( $this->m_arrmixDefinition ) ) {
			$this->decodeDefinition();
		}

		return ( true == isset( $this->m_arrmixDefinition[$strFieldName] ) ? $this->m_arrmixDefinition[$strFieldName] : NULL );
	}

	public function getClass() {
		return $this->getDefinitionField( 'class' );
	}

	public function getRunOnReplicas() {
		return $this->getDefinitionField( 'run_on_replicas' ) ?: CReportVersion::NEVER;
	}

	public function getDescription() {
		return $this->getDefinitionField( 'description' );
	}

	public function getUrl() {
		return $this->getDefinitionField( 'url' );
	}

	public function getValidationCallbacks() {
		return $this->getDefinitionField( 'validation_callbacks' ) ?: [];
	}

	public function getAllowOnlyAdmin() {
		return $this->getDefinitionField( 'allow_only_admin' );
	}

	public function getAllowOnlyPsiAdmin() {
		return $this->getDefinitionField( 'allow_only_psi_admin' );
	}

	public function getIsSchedulable() {
		return $this->getDefinitionField( 'is_schedulable' );
	}

	public function getIsPublished() {
		return $this->getDefinitionField( 'is_published' );
	}

	public function getIsDraft() {
		return $this->getDefinitionField( 'is_draft' );
	}

	/*
	 * Setters
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		// We need to handle the definition before the parent method, so that escape sequences are not removed from the JSON and it is never direct set
		if( true == isset( $arrmixValues['definition'] ) ) {
			$this->setDefinition( $arrmixValues['definition'] );
			unset( $arrmixValues['definition'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setDefinition( $strDefinition ) {
		$this->m_strDefinition = $strDefinition;
		$this->decodeDefinition();
	}

	protected function setDefinitionField( $strFieldName, $mixValue ) {
		$this->m_arrmixDefinition[$strFieldName] = $mixValue;
		$this->encodeDefinition();
		return $this;
	}

	public function setClass( $strClass ) {
		return $this->setDefinitionField( 'class', $strClass );
	}

	public function setRunOnReplicas( $strRunOnReplicas ) {
		return $this->setDefinitionField( 'run_on_replicas', $strRunOnReplicas );
	}

	public function setDescription( $strDescription ) {
		return $this->setDefinitionField( 'description', $strDescription );
	}

	public function setUrl( $strUrl ) {
		return $this->setDefinitionField( 'url', $strUrl );
	}

	public function setReportXml( $strReportXml ) {
		return $this->setDefinitionField( 'report_xml', $strReportXml );
	}

	public function setAllowOnlyAdmin( $boolAllowOnlyAdmin ) {
		return $this->setDefinitionField( 'allow_only_admin', $boolAllowOnlyAdmin ? true : false );
	}

	public function setAllowOnlyPsiAdmin( $boolAllowOnlyPsiAdmin ) {
		return $this->setDefinitionField( 'allow_only_psi_admin', $boolAllowOnlyPsiAdmin ? true : false );
	}

	public function setIsSchedulable( $boolIsSchedulable ) {
		return $this->setDefinitionField( 'is_schedulable', $boolIsSchedulable ? true : false );
	}

	public function setIsPublished( $boolIsPublished ) {
		return $this->setDefinitionField( 'is_published', $boolIsPublished ? true : false );
	}

	public function setIsDraft( $boolIsDraft ) {
		return $this->setDefinitionField( 'is_draft', $boolIsDraft ? true : false );
	}

	public function setValidationCallbacks( array $arrmixValidationCallbacks ) {
		return $this->setDefinitionField( 'validation_callbacks', ( array ) $arrmixValidationCallbacks );
	}

	public function addValidationCallback( $strFunctionName, $arrmixArguments ) {
		$this->m_arrmixDefinition['validation_callbacks'][$strFunctionName] = $arrmixArguments;
		$this->encodeDefinition();
		return $this;
	}

	public function clearValidationCallbacks() {
		return $this->setDefinitionField( 'validation_callbacks', [] );
	}

	/*
	 * Other Functions
	 */

	protected function decodeDefinition() {
		$this->m_arrmixDefinition = json_decode( $this->getDefinition(), true );
	}

	protected function encodeDefinition() {
		$this->m_strDefinition = json_encode( $this->m_arrmixDefinition );
	}

}
