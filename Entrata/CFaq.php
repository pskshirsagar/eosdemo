<?php

class CFaq extends CBaseFaq {

	protected $m_strPsProductName;
	protected $m_strQuestion;

	public function valPsProductId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPsProductId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_type_id', 'Product is required.' ) );
        }

        return $boolIsValid;
    }

	public function valQuestion() {
        $boolIsValid = true;

		$strQuestion	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getQuestion() ) );
    	$strQuestion = strip_tags( html_entity_decode( nl2br( trim( $strQuestion ) ), ENT_QUOTES ) );
   		if( 0 >= strlen( $strQuestion ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required.' ) );
    	}

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
	        case VALIDATE_UPDATE:
	        	$boolIsValid &= $this->valPsProductId();
            	$boolIsValid &= $this->valQuestion();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>