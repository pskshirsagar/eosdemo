<?php

class CArOrigin extends CBaseArOrigin {

	const BASE                 = 1;
	const AMENITY              = 2;
	const PET                  = 3;
	const ADD_ONS              = 4;
	const RISK_PREMIUM         = 5;
	const SPECIAL              = 6;
	const MAINTENANCE          = 7;
	const REIMBURSABLE_EXPENSE = 8;
	const LEASE_VIOLATION      = 9;

	public static $c_arrintGeneralArOrigins = [ self::BASE, self::AMENITY, self::PET, self::ADD_ONS, self::SPECIAL ];
	public static $c_arrintStudentPropertyArOrigins = [ self::BASE, self::SPECIAL ];
	public static $c_arrintCommercialPropertyArOrigins = [ self::BASE, self::AMENITY, self::PET, self::ADD_ONS, self::SPECIAL, self::REIMBURSABLE_EXPENSE ];

	public static $c_arrintDelinquencyArOrigins = [ self::BASE, self::AMENITY, self::SPECIAL ];
	public static $c_arrintArOriginIdsWithoutAddOns     = [ self::BASE, self::AMENITY, self::PET, self::SPECIAL, self::RISK_PREMIUM, self::MAINTENANCE, self::REIMBURSABLE_EXPENSE ];

	public static $c_arrintArOriginIdsExcludedInOtherCharges = [ self::AMENITY, self::ADD_ONS ];

	protected $m_arrstrArOrignNames;

	/**
	 * Validate function
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function  getIdToCamelizeName( $intArOriginId ) {
		switch( $intArOriginId ) {
			case self::AMENITY:
				return 'Amenity';
				break;

			case self::PET:
				return 'Pet';
				break;

			case self::ADD_ONS:
				return 'AddOn';
				break;

			case self::RISK_PREMIUM:
				return 'RiskPremium';
				break;

			case self::SPECIAL:
				return 'Special';
				break;

			case self::MAINTENANCE:
				return 'Maintenance';
				break;

			case self::BASE:
				return 'Base';
				break;

			default:
				// do nothing
				break;
		}
	}

	/**
	 * Add Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getArOrignNames() {
		$this->loadArOrignNames();

		return $this->m_arrstrArOrignNames;
	}

	public function loadArOrignNames() {

		if( false == empty( $this->m_arrstrArOrignNames ) ) {
			return;
		}

		$this->m_arrstrArOrignNames = [
			self::AMENITY		=> __( 'Amenity' ),
			self::PET			=> __( 'Pet' ),
			self::ADD_ONS		=> __( 'AddOn' ),
			self::RISK_PREMIUM	=> __( 'Risk Premium' ),
			self::SPECIAL		=> __( 'Special' ),
			self::MAINTENANCE	=> __( 'Maintenance' ),
			self::BASE			=> __( 'Base' )
		];
	}

}
?>