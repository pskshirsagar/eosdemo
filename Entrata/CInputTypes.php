<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInputTypes
 * Do not add any new functions to this class.
 */

class CInputTypes extends CBaseInputTypes {

    public static function fetchInputTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CInputType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchInputType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CInputType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchInputTypesByOrderNum( $objDatabase ) {
    	$strSql = 'SELECT * FROM input_types where is_published = 1 ORDER BY order_num';
    	return parent::fetchCachedObjects( $strSql, 'CInputType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>