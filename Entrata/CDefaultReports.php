<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReports
 * Do not add any new functions to this class.
 */

class CDefaultReports extends CBaseDefaultReports {

	public static function fetchDefaultReports( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultReport', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultReport', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	/**
	 * Used only in CHistoricalWaitlistReportsEntryScript
	 *
	 * @param $strReportName
	 * @param $objDatabase
	 * @return null
	 */
	public static function fetchDefaultReportIdByName( $strReportName, $objDatabase ) {
		$strSql = '
			SELECT
				dr.id
			FROM
				default_reports dr
			WHERE
				dr.name = \'' . $strReportName . '\'';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchDefaultReportMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) AS id
			FROM
				default_reports';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllDefaultReportsByDefaultReportGroupId( $intDefaultReportGroupId, $objDatabase ) {

		if( NULL !== $intDefaultReportGroupId ) {
			$strAllReportSql = 'WHERE
				dr.default_report_group_id =' . ( int ) $intDefaultReportGroupId . '';
			$strOrderBySql = 'ORDER BY
				dr.title';
		} else {
			$strAllReportSql = '';
			$strOrderBySql = 'ORDER BY
				dr.title';
		}

		$strSql = '
			SELECT
				dr.*,
				drg.parent_module_id AS report_tab_id,
				concat ( drv_latest.major, \'.\', drv_latest.minor ) AS latest_version,
				concat ( drv_default.major, \'.\', drv_default.minor ) AS default_version,
				drv_default.definition->>\'allow_only_psi_admin\' AS default_entrata_only,
				drv_default.definition->>\'is_published\' AS default_is_published,
				util_get_translated( \'name\', rt.name, rt.details ) AS report_type
			FROM
				default_reports dr
				JOIN default_report_versions drv_latest ON ( drv_latest.default_report_id = dr.id AND drv_latest.is_latest = TRUE )
				LEFT JOIN default_report_versions drv_default ON ( drv_default.default_report_id = drv_latest.default_report_id AND drv_default.is_default = TRUE )
				LEFT JOIN default_report_groups drg ON ( drg.id = dr.default_report_group_id )
				LEFT JOIN report_types rt ON ( rt.id = dr.report_type_id )
			' . $strAllReportSql
			 . $strOrderBySql;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNextDefaultReportId( $objDatabase ) {
		$strSql = 'SELECT 
						MAX(id) + 1 AS id 
					FROM 
						default_reports';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportByName( $strReportName, $objDatabase ) {
		$strSql = '
			SELECT
				dr.id
			FROM
				default_reports dr
			WHERE
				dr.name = \'' . $strReportName . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportVersionsByDefaultReportGroupId( $intDefaultReportGroupId, $objDatabase ) {
		$strSql = '
			SELECT
				dr.*,
				drv.id AS default_report_version_id,
				drv.major,
				drv.minor
			FROM
				default_reports dr
				JOIN default_report_versions drv ON( dr.id = drv.default_report_id )
			WHERE
				dr.default_report_group_id =' . ( int ) $intDefaultReportGroupId . '
				AND drv.is_latest = true
			ORDER BY
				dr.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						default_report_versions drv
						JOIN default_reports dr ON ( drv.default_report_id = dr.id )
					WHERE
						drv.id = ' . ( int ) $intDefaultReportVersionId;

		return parent::fetchDefaultReport( $strSql, $objDatabase );
	}

	public static function fetchAllDefaultReports( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						default_reports dr
					ORDER BY name, id DESC';

		return self::fetchDefaultReports( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportsByIds( $arrintDefaultReportIds, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						default_reports dr
					WHERE
						id IN ( ' . implode( ', ', $arrintDefaultReportIds ) . ' )
					ORDER BY name, id DESC';

		return self::fetchDefaultReports( $strSql, $objDatabase );
	}

}
