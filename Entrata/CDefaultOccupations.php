<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultOccupations
 * Do not add any new functions to this class.
 */

class CDefaultOccupations extends CBaseDefaultOccupations {

	public static function fetchDefaultOccupation( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultOccupation', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>