<?php

class CArRule extends CBaseArRule {

	const ACCELERATED_FULL_TERM = 'accelerated_full_term';
	const ACCELERATED_RENT_DURATION_DAY = 'accelerated_days';
	const ACCELERATED_RENT_DURATION_MONTH = 'accelerated_month';
	const ACCELERATED_RENT_DURATION_DATE = 'accelerated_date';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArRuleSetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseAllConditions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyToDetails() {
		$boolIsValid = true;
		$arrmixApplyToDetails = json_decode( $this->getApplyToDetails(), true );

		if( true == valArr( $arrmixApplyToDetails ) && true == array_key_exists( 'key', $arrmixApplyToDetails ) ) {
			switch( $arrmixApplyToDetails['key'] ) {
				case self::ACCELERATED_RENT_DURATION_DAY:
					$intDays = $arrmixApplyToDetails[self::ACCELERATED_RENT_DURATION_DAY];

					if( false == valStr( $intDays ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'apply_to_detail', __( 'Please enter days.' ) ) );
						break;
					}

					if( 0 > $intDays ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'apply_to_detail', __( 'Days should be greater than zero.' ) ) );
						break;
					}

					if( !( ( string ) round( $intDays ) == $intDays && false === \Psi\CStringService::singleton()->strpos( $intDays, '.' ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Days should not be decimal.' ) ) );
						break;
					}
					break;

				case self::ACCELERATED_RENT_DURATION_MONTH:
					$intMonth = $arrmixApplyToDetails[self::ACCELERATED_RENT_DURATION_MONTH];
					if( false == valStr( $intMonth ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'apply_to_detail', __( 'Please select set max duration month.' ) ) );
						break;
					}
					break;

				case self::ACCELERATED_RENT_DURATION_DATE:
					$strDate = $arrmixApplyToDetails[self::ACCELERATED_RENT_DURATION_DATE];
					if( false == valStr( $strDate ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'apply_to_detail', __( 'Please select set max duration date.' ) ) );
						break;
					}
					break;

				default:
					//
					break;
			}
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valApplyToDetails();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>