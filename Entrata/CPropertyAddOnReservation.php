<?php

class CPropertyAddOnReservation extends CBasePropertyAddOnReservation {

	protected $m_strAmenityName;
	protected $m_strReservationStatusType;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPropertyName;
	protected $m_strRpDescription;
	protected $m_strUnitNumber;
	protected $m_strBuildingName;
	protected $m_strTimeZoneName;
	protected $m_strReservationStartFormatted;
	protected $m_strReservationEndFormatted;
	protected $m_strFullSizeUri;
	protected $m_strUnitNumberCache;
	protected $m_strAmenityAvailability;

	protected $m_intReservationFeeOccurrenceTypeId;
	protected $m_intIsReservedByDay;
	protected $m_intFileId;
	protected $m_intFileExtensionId;
	protected $m_intLeaseId;
	protected $m_intPropertyAmenityAvailabilityId;
	protected $m_intAmenityId;

	protected $m_fltRateAmount;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateAssociationId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intRateAssociationId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Amenity is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > \Psi\CStringService::singleton()->strlen( $this->m_intRateAssociationId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Amenity is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerId( $boolIsCustomer, $boolIsApproveReject ) {
		$boolIsValid = true;
		if( false == $boolIsCustomer || true == $boolIsApproveReject ) {
			return true;
		}
		if( true == is_null( $this->m_intCustomerId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Resident is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valName( $boolIsCustomer, $boolIsApproveReject ) {
		$boolIsValid = true;
		if( true == $boolIsCustomer || true == $boolIsApproveReject ) {
			return true;
		}

		if( false == valStr( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Name is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReservationStart() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strReservationStart ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Date and time is required.' ) ) );
			$boolIsValid = false;
		} elseif( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strReservationStart ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Date and time is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valReservationEnd() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityReservationStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsCustomer = false, $boolIsApproveReject = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRateAssociationId();
				if( false == $boolIsApproveReject ) {
					$boolIsValid &= $this->valReservationStart();
				}
				$boolIsValid &= $this->valCustomerId( $boolIsCustomer, $boolIsApproveReject );
				$boolIsValid &= $this->valName( $boolIsCustomer, $boolIsApproveReject );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * get Functions
	 */

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function getAmenityId() {
		return $this->m_intAmenityId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getReservationStatusType() {
		return $this->m_strReservationStatusType;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getRateAmount() {
		return $this->m_fltRateAmount;
	}

	public function getReservationFeeOccurrenceTypeId() {
		return $this->m_intReservationFeeOccurrenceTypeId;
	}

	public function getIsReservedByDay() {
		return $this->m_intIsReservedByDay;
	}

	public function setAmenityAvailability( $strAmenityAvailability ) {
		$this->set( 'm_strAmenityAvailability', CStrings::strTrimDef( $strAmenityAvailability, -1, NULL, true ) );
	}

	public function getAmenityAvailability() {
		return $this->m_strAmenityAvailability;
	}

	public function getRpDescription() {
		return $this->m_strRpDescription;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getPropertyAmenityAvailabilityId() {
		return $this->m_intPropertyAmenityAvailabilityId;
	}

	public function getTimeZoneName() {
		return $this->m_strTimeZoneName;
	}

	public function getReservationStartFormatted() {
		return $this->m_strReservationStartFormatted;
	}

	public function getReservationEndFormatted() {
		return $this->m_strReservationEndFormatted;
	}

	public function getFullSizeUri() {
		return $this->m_strFullSizeUri;
	}

	/**
	 * set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['amenity_name'] ) ) $this->setAmenityName( $arrmixValues['amenity_name'] );
		if( true == isset( $arrmixValues['amenity_id'] ) ) $this->setAmenityId( $arrmixValues['amenity_id'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['reservation_status_type'] ) ) $this->setReservationStatusType( $arrmixValues['reservation_status_type'] );
		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['rate_amount'] ) ) $this->setRateAmount( $arrmixValues['rate_amount'] );
		if( true == isset( $arrmixValues['reservation_fee_occurrence_type_id'] ) ) $this->setReservationFeeOccurrenceTypeId( $arrmixValues['reservation_fee_occurrence_type_id'] );
		if( true == isset( $arrmixValues['is_reserved_by_day'] ) ) $this->setIsReservedByDay( $arrmixValues['is_reserved_by_day'] );
		if( true == isset( $arrmixValues['amenity_availability'] ) ) $this->setAmenityAvailability( $arrmixValues['amenity_availability'] );
		if( true == isset( $arrmixValues['rp_description'] ) ) $this->setRpDescription( $arrmixValues['rp_description'] );
		if( true == isset( $arrmixValues['file_id'] ) ) $this->setFileId( $arrmixValues['file_id'] );
		if( true == isset( $arrmixValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrmixValues['file_extension_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['property_amenity_availability_id'] ) ) $this->setPropertyAmenityAvailabilityId( $arrmixValues['property_amenity_availability_id'] );
		if( true == isset( $arrmixValues['time_zone_name'] ) ) $this->setTimeZoneName( $arrmixValues['time_zone_name'] );
		if( true == isset( $arrmixValues['reservation_start_formatted'] ) ) $this->setReservationStartFormatted( $arrmixValues['reservation_start_formatted'] );
		if( true == isset( $arrmixValues['reservation_end_formatted'] ) ) $this->setReservationEndFormatted( $arrmixValues['reservation_end_formatted'] );
		if( true == isset( $arrmixValues['fullsize_uri'] ) ) $this->setFullSizeUri( $arrmixValues['fullsize_uri'] );

		return;
	}

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = $strAmenityName;
	}

	public function setAmenityId( $intAmenityId ) {
		$this->m_intAmenityId = $intAmenityId;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setReservationStatusType( $strReservationStatusType ) {
		return $this->m_strReservationStatusType = $strReservationStatusType;
	}

	public function setNameFirst( $strNameFirst ) {
		return $this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		return $this->m_strNameLast = $strNameLast;
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		return $this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	public function setBuildingName( $strBuildingName ) {
		return $this->m_strBuildingName = $strBuildingName;
	}

	public function setRateAmount( $fltRateAmount ) {
		$this->m_fltRateAmount = CStrings::strToFloatDef( $fltRateAmount, NULL, false );
	}

	public function setReservationFeeOccurrenceTypeId( $intReservationFeeOccurrenceTypeId ) {
		$this->m_intReservationFeeOccurrenceTypeId = CStrings::strToIntDef( $intReservationFeeOccurrenceTypeId, NULL, false );
	}

	public function setIsReservedByDay( $intIsReservedByDay ) {
		$this->m_intIsReservedByDay = CStrings::strToIntDef( $intIsReservedByDay, NULL, false );
	}

	public function setFileId( $intFileId ) {
		$this->m_intFileId = $intFileId;
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->m_intFileExtensionId = $intFileExtensionId;
	}

	public function setRpDescription( $strRpDescription ) {
		$this->m_strRpDescription = $strRpDescription;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setPropertyAmenityAvailabilityId( $intPropertyAmenityAvailabilityId ) {
		$this->m_intPropertyAmenityAvailabilityId = $intPropertyAmenityAvailabilityId;
	}

	public function setTimeZoneName( $strTimeZoneName ) {
		return $this->m_strTimeZoneName = $strTimeZoneName;
	}

	public function setReservationStartFormatted( $strReservationStartFormatted ) {
		$this->m_strReservationStartFormatted = $strReservationStartFormatted;
	}

	public function setReservationEndFormatted( $strReservationEndFormatted ) {
		$this->m_strReservationEndFormatted = $strReservationEndFormatted;
	}

	public function setFullSizeUri( $strFullSizeUri ) {
		$this->m_strFullSizeUri = $strFullSizeUri;
	}

	public function getReservationHours() {
		$objStartDate = date_create( $this->getReservationStart() );
		$objEndDate = date_create( $this->getReservationEnd() );

		/** @var \DateInterval $objDateInterval */
		$objDateInterval = $objStartDate->diff( $objEndDate );
		return $objStartDate < $objEndDate ? $objDateInterval->days * 24 + $objDateInterval->h : 0;
	}

	public function getReservationDays() {
		$objStartDate = date_create( $this->getReservationStart() );
		$objEndDate = date_create( $this->getReservationEnd() );

		/** @var \DateInterval $objDateInterval */
		$objDateInterval = $objStartDate->diff( $objEndDate );

		$intReservationDays = ( $objDateInterval->days == 0 ) ? 1 : $objDateInterval->days;
		return $objStartDate < $objEndDate ? $intReservationDays : 0;
	}

}
?>
