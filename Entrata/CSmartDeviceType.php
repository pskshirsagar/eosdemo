<?php

class CSmartDeviceType extends CBaseSmartDeviceType {

	const DOOR_LOCK                   = 1;
	const THERMOSTAT                  = 2;
	const DIMMER_LIGHT                = 3;
	const WATER_SENSOR                = 4;
	const LIGHT_SWITCH                = 5;
	const WALL_OUTLET                 = 6;
	const NEST_THERMOSTAT             = 7;
	const HONEYWELL_THERMOSTAT        = 8;
	const SMOKE_DETECTOR              = 9;
	const WATER_VALVE                 = 10;
	const HIGH_VOLTAGE_SHUTOFF_SWITCH = 11;
	const ECOBEE_THERMOSTAT           = 12;
	const CONTACT_SENSOR              = 13;
	const MULTI_SENSOR                = 14;
	const ENERGY_METER                = 15;
	const WINDOW_SHADES               = 16;
	const NEST_THERMOSTAT_MDU         = 18;
	const FAN_SWITCH                  = 19;
	const DIMMER_FAN                  = 20;
	const ZWAVE_GARAGE_DOOR           = 21;
	const WIFI_GARAGE_DOOR            = 22;
	const ECOBEE_SMARTBUILDINGS_THERMOSTAT = 23;
	const ALEXA_FOR_RESIDENTIAL            = 24;

	const OVEN            = 25;
	const REFRIGERATOR    = 26;
	const DISHWASHER      = 27;
	const WASHING_MACHINE = 28;
	const DRYER           = 29;
	const MICROWAVE       = 30;

	public static $c_arrintZWaveSmartDeviceTypes = [ self::DOOR_LOCK, self::THERMOSTAT, self::DIMMER_LIGHT, self::WATER_SENSOR, self::LIGHT_SWITCH, self::WALL_OUTLET, self::SMOKE_DETECTOR, self::WATER_VALVE, self::HIGH_VOLTAGE_SHUTOFF_SWITCH, self::CONTACT_SENSOR, self::MULTI_SENSOR, self::ENERGY_METER, self::WINDOW_SHADES, self::FAN_SWITCH, self::DIMMER_FAN, self::ZWAVE_GARAGE_DOOR ];
	public static $c_arrintWifiSmartDeviceTypes  = [ self::NEST_THERMOSTAT, self::HONEYWELL_THERMOSTAT, self::ECOBEE_THERMOSTAT, self::NEST_THERMOSTAT_MDU, self::WIFI_GARAGE_DOOR, self::ECOBEE_SMARTBUILDINGS_THERMOSTAT, self::ALEXA_FOR_RESIDENTIAL ];
	public static $c_arrintOtherSmartDevices     = [ self::WALL_OUTLET, self::SMOKE_DETECTOR, self::HIGH_VOLTAGE_SHUTOFF_SWITCH, self::CONTACT_SENSOR, self::MULTI_SENSOR, self::ENERGY_METER, self::WINDOW_SHADES, self::FAN_SWITCH, self::DIMMER_FAN ];
	public static $c_arrintThermostatDevices     = [ self::THERMOSTAT, self::NEST_THERMOSTAT, self::HONEYWELL_THERMOSTAT, self::ECOBEE_THERMOSTAT, self::NEST_THERMOSTAT_MDU, self::ECOBEE_SMARTBUILDINGS_THERMOSTAT  ];
	public static $c_arrintFanDevices            = [ self::FAN_SWITCH, self::DIMMER_FAN ];

	const ALEXA_FOR_RESIDENTIAL_TEXT = 'Alexa For Residentials';

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>