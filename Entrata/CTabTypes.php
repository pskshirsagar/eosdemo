<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabTypes
 * Do not add any new functions to this class.
 */

class CTabTypes extends CBaseTabTypes {

	public static function fetchTabTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTabType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTabType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTabType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllTabTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM tab_types';

		return self::fetchTabTypes( $strSql, $objDatabase );
	}
}
?>