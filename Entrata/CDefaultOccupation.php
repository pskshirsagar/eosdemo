<?php

class CDefaultOccupation extends CBaseDefaultOccupation {

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getSystemCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsSystem() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublised() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsPublised() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_publised', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>