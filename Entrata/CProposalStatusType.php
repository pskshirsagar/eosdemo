<?php

class CProposalStatusType extends CBaseProposalStatusType {

	const PLANNING	= 1;
	const BIDDING	= 2;
	const APPROVED	= 3;
	const CANCELLED	= 4;

	public static $c_arrstrProposalStatusTypes = [
		self::PLANNING	=> 'Planning',
		self::BIDDING	=> 'Bidding',
		self::APPROVED	=> 'Approved',
		self::CANCELLED	=> 'Cancelled'
	];

	public static $c_arrstrActiveProposalStatusTypes = [
		self::PLANNING	=> 'Planning',
		self::BIDDING	=> 'Bidding'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>