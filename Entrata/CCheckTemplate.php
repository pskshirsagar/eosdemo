<?php

class CCheckTemplate extends CBaseCheckTemplate {

	protected $m_intIsCheckedCustomText;

	/**
	 * Get Functions
	 */

	public function getIsCheckedCustomText() {
		return $this->m_intIsCheckedCustomText;
	}

	/**
	 * Set Functions
	 */

	public function setIsCheckedCustomText( $intIsCheckedCustomText ) {
		$this->m_intIsCheckedCustomText = $intIsCheckedCustomText;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $objClientDatabase ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Check layout name is required.' ) );
			return false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND id <> ' . ( int ) $this->getId() . '
						AND LOWER( name ) LIKE \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->getName() ) ) . '\'';

		if( 0 < CCheckTemplates::fetchCheckTemplateCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Check layout name already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTopMargin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeftMargin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintStubCheckNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintStubBankInfo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCenterCompanyInfo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCenterBankInfo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valThreeChecksPerPage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAboveStubs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrePrinted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomText() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomText() ) && 1 == $this->getIsCheckedCustomText() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_text', 'Custom text is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valCustomText();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createCheckComponent() {

		$objCheckComponent = new CCheckComponent();
		$objCheckComponent->setDefaults();
		$objCheckComponent->setCid( $this->getCid() );
		$objCheckComponent->setCheckTemplateId( $this->getId() );

		return $objCheckComponent;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCheckComponents( $objClientDatabase ) {
		return CCheckComponents::fetchSimpleCheckComponentsByCheckTemplateIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

}
?>