<?php
class CCustomerExpenseLog extends CBaseCustomerExpenseLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerExpenseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpenseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpenseTypeReasonId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerExpenseLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectivePeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnabledCustomerIncomeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpenseEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateEnded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostMonthIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>