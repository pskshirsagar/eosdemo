<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklists
 * Do not add any new functions to this class.
 */

class CChecklists extends CBaseChecklists {

	public static function fetchCustomChecklistsByChecklistTypeIdByCid( $intChecklistTypeId, $intCid, $objClientDatabase, $arrintPropertyId = [] ) {
		$strSqlJoin = '';
		if( false == is_numeric( $intCid ) || false == is_numeric( $intChecklistTypeId ) ) return NULL;

		if( true == valArr( $arrintPropertyId ) ) {
			$strSqlJoin = ' JOIN property_checklists pch ON ( pch.cid = c.cid AND pch.checklist_id = c.id AND pch.property_id IN (' . implode( ',', $arrintPropertyId ) . ') ) ';
		}

		$strSql = '	SELECT
					    c.id AS checklistId,
						c.is_enabled,
						' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', c.name, c.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'c.name' ) . ' AS checklistName,
						' . ( false == empty( CLocaleContainer::createService()->getDefaultLocaleCode() ) ? 'util_get_translated( \'name\', c.name, c.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' )' : '' ) . ' AS default_locale_checklistName,
					    ct.id AS triggerId,
					    ct.name AS triggerName,
					    ci.checklistItemCount,
					    pc.propertyChecklistCount
					FROM
					    checklists c
					    JOIN checklist_triggers ct ON ( ct.id = c.checklist_trigger_id AND ct.checklist_type_id = c.checklist_type_id )
					    JOIN occupancy_types ot ON ( ot.id = ANY ( c.occupancy_type_ids ) )' .
		                $strSqlJoin
		                . 'JOIN
					    (
					      SELECT
					          count ( id ) AS checklistItemCount,
					          cid,
					          checklist_id
					      FROM
					          checklist_items
						  WHERE
							  deleted_on IS NULL
					      GROUP BY
					          cid,
					          checklist_id
					    ) AS ci ON ( ci.cid = c.cid AND ci.checklist_id = c.id )
					   LEFT JOIN
					    (
					      SELECT
					          count ( DISTINCT pc.id ) AS propertyChecklistCount,
					          pc.cid,
					          pc.checklist_id
					      FROM
					          property_checklists pc
					          JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ]::INT [ ], ARRAY[' . CPsProduct::ENTRATA . '], TRUE ) lp ON ( lp.property_id = pc.property_id AND pc.cid = lp.cid )
					      GROUP BY
					          pc.cid,
					          pc.checklist_id
					    ) AS pc ON ( pc.cid = c.cid AND pc.checklist_id = c.id )
					WHERE
					    c.cid = ' . ( int ) $intCid . '
					    AND c.checklist_type_id = ' . ( int ) $intChecklistTypeId . '
					    AND c.deleted_by IS NULL
					GROUP BY
					    c.id,
					    c.is_enabled,
					    c.name,
					    c.details,
					    ci.checklistItemCount,
					    pc.propertyChecklistCount,
					    triggerId,
					    triggerName
					ORDER BY
					    ' . $objClientDatabase->getCollateSort( 'ct.name', true, true ) . ', 
					    c.name ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchChecklistIdByDefaultChecklistIdByPropertyIdByCid( $intDefaultChecklistId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
					    c.id,
					    pc.id as property_checklist_id
					FROM
					    checklists c
					    LEFT JOIN property_checklists pc ON ( c.cid = pc.cid AND c.id = pc.checklist_id AND pc.property_id = ' . ( int ) $intPropertyId . ' )
					WHERE
					    c.default_checklist_id = ' . ( int ) $intDefaultChecklistId . '
					    AND c.cid = ' . ( int ) $intCid . '
		                AND c.deleted_by IS NULL ';

		$arrmixPropertyChecklist = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixPropertyChecklist ) ) return current( $arrmixPropertyChecklist );
	}

	public static function fetchCustomChecklistsByChecklistTypeIdByChecklistTriggerIdByPropertyIdByCid( $intChecklistTypeId, $intTriggerId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                      c.id,
                      c.name,
                      c.occupancy_type_ids,
                      pc.property_id,
                      p.occupancy_type_ids AS property_occupancy_type_ids,
                      CASE
                        WHEN c.occupancy_type_ids && p.occupancy_type_ids THEN 1
                        ELSE 0
                      END AS has_required_occupancy_type_id
                  FROM
                      checklists c
                      JOIN property_checklists pc ON ( c.cid = pc.cid AND c.id = pc.checklist_id )
                      JOIN properties p ON ( p.cid = pc.cid AND p.id = pc.property_id )
                  WHERE
                      pc.property_id              = ' . ( int ) $intPropertyId . '
                      AND pc.cid                  = ' . ( int ) $intCid . '
                      AND c.checklist_type_id     = ' . ( int ) $intChecklistTypeId . '
                      AND c.checklist_trigger_id  = ' . ( int ) $intTriggerId . '
                  ORDER BY
                      has_required_occupancy_type_id DESC,
                      c.occupancy_type_ids';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChecklistIdByChecklistTypeIdByChecklistTriggerIdByPropertyIdByCid( $intChecklistTypeId, $intTriggerId, $intPropertyId, $intCid, $objDatabase, $intLeaseCustomerId = NULL, $intLeaseId = NULL, $intCustomerId = NULL ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		if( true == valId( $intLeaseCustomerId ) ) {
			$strWhere = ' AND lc.id = ' . ( int ) $intLeaseCustomerId;
		} elseif( true == valId( $intLeaseId ) && true == valId( $intCustomerId ) ) {
			$strWhere = ' AND lc.lease_id    = ' . ( int ) $intLeaseId . ' AND lc.customer_id = ' . ( int ) $intCustomerId;
		} else {
			return NULL;
		}

		$strSql  = 'SELECT
					    c.id as checklist_id
					FROM
					    checklists c
					    JOIN lease_checklists lch ON c.cid = lch.cid AND c.id = lch.checklist_id
					    JOIN cached_leases cl ON cl.cid = lch.cid AND cl.id = lch.lease_id AND cl.active_lease_interval_id = lch.lease_interval_id
					    JOIN property_checklists pc ON pc.cid = lch.cid and pc.checklist_id = lch.checklist_id and pc.property_id = cl.property_id
					    JOIN lease_customers lc ON lc.cid = lch.cid AND lc.lease_id = lch.lease_id
					WHERE
					    lc.property_id              = ' . ( int ) $intPropertyId . '
					    AND lc.cid                  = ' . ( int ) $intCid . '
					    AND cl.occupancy_type_id    = ANY ( c.occupancy_type_ids )
					    AND c.checklist_type_id     = ' . ( int ) $intChecklistTypeId . '
					    AND c.checklist_trigger_id  = ' . ( int ) $intTriggerId . '
					    AND c.deleted_by IS NULL' .
		                $strWhere;

		return parent::fetchColumn( $strSql, 'checklist_id', $objDatabase );
	}

	public static function fetchChecklistIdByChecklistFilterByPropertyIdByCid( $objChecklistFilter, $intPropertyId, $intCid, $objDatabase, $intCustomerTypeId = NULL ) {

		if( false == valObj( $objChecklistFilter, 'CChecklistsFilter' ) || false == valId( $objChecklistFilter->getChecklistTriggerId() ) || false == valId( $objChecklistFilter->getOccupancyTypeId() ) || false == valId( $objChecklistFilter->getChecklistTypeId() ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strJoinSql = $strWhereSql = '';
		if( true == valId( $objChecklistFilter->getOrganizationContractId() ) ) {
			$strJoinSql = ' JOIN organization_contract_checklists occ ON( pc.cid = occ.cid AND pc.checklist_id = occ.checklist_id AND pc.property_id = occ.property_id AND occ.organization_contract_id = ' . ( int ) $objChecklistFilter->getOrganizationContractId() . ' AND occ.deleted_by IS NULL AND occ.deleted_on IS NULL ) ';
			if( true == valId( $intCustomerTypeId ) ) {
				$strJoinSql  .= ' JOIN organization_contracts oc ON ( occ.cid = oc.cid AND occ.organization_contract_id = oc.id ) ';
				$strWhereSql .= ' AND ( oc.organization_contract_financial_responsibility_type_id IN ( ' . COrganizationContractFinancialResponsibilityType::GROUP_IS_HUNDRED_PERCENT_RESPONSIBLE . ', ' . COrganizationContractFinancialResponsibilityType::SHARED . ' ) AND ' . ( int ) $intCustomerTypeId . ' <> ' . CCustomerType::PRIMARY . ' ) ';
			}
		}

		$strSql = 'SELECT
						c.id as checklist_id
					FROM
						checklists c
						JOIN property_checklists pc ON ( pc.cid = c.cid AND c.id = pc.checklist_id AND pc.property_id = ' . ( int ) $intPropertyId . ' )
						' . $strJoinSql . '
					WHERE c.deleted_by IS NULL
						AND c.deleted_on IS NULL
						AND c.checklist_trigger_id = ' . ( int ) $objChecklistFilter->getChecklistTriggerId() . '
						AND c.checklist_type_id = ' . ( int ) $objChecklistFilter->getChecklistTypeId() . '
						AND ' . ( int ) $objChecklistFilter->getOccupancyTypeId() . ' = ANY ( c.occupancy_type_ids )
						AND c.cid = ' . ( int ) $intCid . $strWhereSql;

		return parent::fetchColumn( $strSql, 'checklist_id', $objDatabase );

	}

}
?>