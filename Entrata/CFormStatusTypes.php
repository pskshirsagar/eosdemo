<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFormStatusTypes
 * Do not add any new functions to this class.
 */

class CFormStatusTypes extends CBaseFormStatusTypes {

	public static function fetchFormStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFormStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchFormStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFormStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>