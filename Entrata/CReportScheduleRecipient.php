<?php

class CReportScheduleRecipient extends CBaseReportScheduleRecipient {

	const NON_ENTRATA_USER_REFERENCE_ID = 0;
	const NON_ENTRATA = 'non_entrata';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportScheduleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportScheduleRecipientTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intReportScheduleRecipientTypeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_schedule_recipient_type_id', 'Report schedule recipient type id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valReferenceId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intReferenceId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_id', 'Reference id is required.' ) );
        }

        return $boolIsValid;
    }

	public function valDetails() {
			$boolIsValid = true;
			return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valReportScheduleRecipientTypeId();
        		$boolIsValid &= $this->valReferenceId();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>