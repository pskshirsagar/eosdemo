<?php

class CApplicationInterestSkip extends CBaseApplicationInterestSkip {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationInterestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSkipReason() {
        $boolIsValid = true;
        if( true == is_null( $this->getSkipReason() ) ) {
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valApplicationId();
            	$boolIsValid &= $this->valSkipReason();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>