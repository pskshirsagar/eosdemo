<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentPrintingOrderTypes
 * Do not add any new functions to this class.
 */

class CApPaymentPrintingOrderTypes extends CBaseApPaymentPrintingOrderTypes {

	public static function fetchApPaymentPrintingOrderTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApPaymentPrintingOrderType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchApPaymentPrintingOrderType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApPaymentPrintingOrderType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveApPaymentPrintingOrderTypes( $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payment_printing_order_types
					WHERE
						is_published = 1
					ORDER BY 
						order_num';

		return self::fetchApPaymentPrintingOrderTypes( $strSql, $objClientDatabase );
	}

}
?>