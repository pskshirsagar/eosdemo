<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyOnSiteContacts
 * Do not add any new functions to this class.
 */

class CPropertyOnSiteContacts extends CBasePropertyOnSiteContacts {

	public static function fetchPropertyOnSiteContactByPropertyIdContactTypeIdByCid( $intPropertyId, $intContactTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContact( sprintf( 'SELECT * FROM %s WHERE property_id::int = %d::int AND on_site_contact_type_id::int = %d::int AND cid::int = %d::int LIMIT 1', 'property_on_site_contacts', ( int ) $intPropertyId, ( int ) $intContactTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactsByPropertyIdsContactTypeIdByCid( $arrintPropertyIds, $intContactTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContacts( sprintf( 'SELECT * FROM %s WHERE property_id::int IN ( %s ) AND on_site_contact_type_id::int = %d::int AND cid::int = %d::int', 'property_on_site_contacts', implode( ',', $arrintPropertyIds ), ( int ) $intContactTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactByPropertyIdContactTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intPropertyId, $intContactTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContact( sprintf( 'SELECT * FROM %s WHERE property_id::int = %d::int AND on_site_contact_type_id::int = %d::int AND lower(remote_primary_key) = \'%s\' AND cid::int = %d::int LIMIT 1', 'property_on_site_contacts', ( int ) $intPropertyId, ( int ) $intContactTypeId, \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strRemotePrimaryKey ) ) ), ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactsByPropertyIdsByContactTypeIdByCid( $arrintPropertyIds, $intContactTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						property_on_site_contacts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND on_site_contact_type_id = ' . ( int ) $intContactTypeId;

		return self::fetchPropertyOnSiteContacts( $strSql, $objDatabase );
	}

}
?>