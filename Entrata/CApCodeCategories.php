<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApCodeCategories
 * Do not add any new functions to this class.
 */

class CApCodeCategories extends CBaseApCodeCategories {

	public static function fetchApCodeCategoriesByIdsByCid( $arrintCatalogCatagoryIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintCatalogCatagoryIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_code_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ', ', array_filter( $arrintCatalogCatagoryIds ) ) . ' )
						AND deleted_by IS NULL';

		return self::fetchApCodeCategories( $strSql, $objClientDatabase );
	}

	public static function fetchApCodeCategoryByIdByCid( $intCatalogCatagoryId, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intCatalogCatagoryId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_code_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intCatalogCatagoryId . '
						AND deleted_by IS NULL';

		return self::fetchApCodeCategory( $strSql, $objClientDatabase );
	}

	public static function fetchAllApCodeCategoriesByCid( $intCid, $objClientDatabase, $intApcodeTypeId = NULL, $strOrderBy ='name' ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSubSql = ( false == is_null( $intApcodeTypeId ) ) ? 'AND ap_code_type_id = ' . ( int ) $intApcodeTypeId : ' AND ap_code_type_id <> 3';

		$strSql = 'SELECT
						*
					FROM
						ap_code_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL ' . $strSubSql . '
					ORDER BY ' . $strOrderBy;

		return self::fetchApCodeCategories( $strSql, $objClientDatabase );
	}

	public static function fetchAllApCodeCategoriesByApCodeTypeIdByCid( $intApCodeTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApCodeTypeId ) ) return NULL;

		$strSql = 'SELECT
						apc.id,
						apc.name,
						apc.is_disabled,
						count( ac.id ) as ap_code_count
					FROM
						ap_code_categories apc
						LEFT JOIN ap_codes ac ON( ac.cid = apc.cid AND ac.ap_code_category_id = apc.id AND ac.is_disabled = false AND ac.deleted_on IS NULL )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_code_type_id = ' . ( int ) $intApCodeTypeId . '
						AND apc.deleted_by IS NULL
					GROUP BY
						apc.id,
						apc.cid
					ORDER BY apc.id';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchApCodeCategoryByIdByCidByApCodeTypeId( $intId, $intCid, $intApCodeTypeId, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intId ) || false == valId( $intApCodeTypeId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_code_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						AND ap_code_type_id = ' . ( int ) $intApCodeTypeId . '
						AND deleted_by IS NULL';

		return self::fetchApCodeCategory( $strSql, $objClientDatabase );
	}

	public static function fetchJobCodeCategoriesCountByJobTypeByCid( $intJobType, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ac.ap_code_category_id as category_id,
						count( ad.id) AS associated_job_code_count
					FROM
						ap_codes ac
						LEFT JOIN ap_details ad ON( ad.ap_code_id = ac.id AND ad.cid = ac.cid )
					WHERE
						ac.ap_code_type_id = ' . ( int ) $intJobType . '
						AND ac.cid = ' . ( int ) $intCid . '
						AND ac.deleted_by IS NULL
					GROUP BY
						category_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>