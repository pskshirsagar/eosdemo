<?php

class CImportTemplate extends CBaseImportTemplate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourcePropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompetitorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToken() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasResidents() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSimpleCsvUpload() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusTypes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmountDue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>