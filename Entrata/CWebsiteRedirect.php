<?php

use Psi\Eos\Entrata\CWebsiteRedirects;

class CWebsiteRedirect extends CBaseWebsiteRedirect {

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valOldUrl( $objDatabase = NULL ) {

    	$boolIsValid = true;

    	if( true == is_null( $this->getOldUrl() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_url', __( 'Old url is required.' ) ) );
    		return $boolIsValid;
    	} elseif( false == CValidation::checkUrl( $this->getOldUrl() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_url', __( 'Old url does not appear to be valid.' ) ) );
    		return $boolIsValid;
    	} elseif( true == is_object( $objDatabase ) ) {
			$objWebsiteRedirect = CWebsiteRedirects::createService()->fetchNewUrlByOldUrlByCid( $this->getOldUrl(), $this->m_intCid, $objDatabase );

			if( true == is_object( $objWebsiteRedirect ) && $objWebsiteRedirect->getId() != $this->getId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Old url already exists.' ) ) );
				return false;
			}

			$objWebsiteRedirect = CWebsiteRedirects::createService()->fetchExistingNewUrlByOldUrlByCid( $this->getOldUrl(), $this->m_intCid, $objDatabase );

			if( true == is_object( $objWebsiteRedirect ) && $objWebsiteRedirect->getId() != $this->getId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Old url already exists as new url.' ) ) );
				return false;
			}

		}
        return $boolIsValid;
    }

    public function valNewUrl( $objDatabase = NULL ) {

        $boolIsValid = true;

        if( true == is_null( $this->getNewUrl() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_url', __( 'New url is required.' ) ) );
        } elseif( false == CValidation::checkUrl( $this->getNewUrl() ) ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_url', __( 'New url does not appear to be valid.' ) ) );
        } elseif( 0 == \Psi\CStringService::singleton()->strcasecmp( $this->getNewUrl(), $this->getOldUrl() ) ) {
 			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_url', __( 'New url can not be same as that of old url.' ) ) );
        } else {
        	if( true == is_object( $objDatabase ) ) {
	        	$objWebsiteRedirect = CWebsiteRedirects::createService()->fetchExistingOldUrlByNewUrlByCid( $this->getNewUrl(), $this->m_intCid, $objDatabase );
	 			if( true == is_object( $objWebsiteRedirect ) && $objWebsiteRedirect->getId() != $this->getId() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'New url already exists as old url.' ) ) );
					$boolIsValid = false;
	 			}
        	}
        }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valOldUrl( $objDatabase );
            	$boolIsValid &= $this->valNewUrl( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>