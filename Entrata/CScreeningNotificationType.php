<?php

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;

class CScreeningNotificationType extends CBaseScreeningNotificationType {
	const DECISION_DENY                     = 1;
	const DECISION_DENY_GUARANTOR           = 2;
	const DECISION_APPROVE_WITH_CONDITION   = 3;

	const CONDITIONAL_GUARANTOR_FAIL_AA = 4; // this notification generates adverse action letters
	const CONDITIONAL_GUARANTOR_PASS    = 5;
	const CONDITIONAL_GUARANTOR_FAIL    = 7;

	const APPLICANT_CONDITIONALLY_APPROVED  = 8;

	protected $m_intCurrentUserId;
	protected $m_intQuickResponseId;
	protected $m_intCustomerTypeId;
	protected $m_intScreeningDecisionTypeId;

	protected $m_arrobjScreeningNotifications;
	protected $m_arrobjScreeningApplicantResults;

	protected $m_objProperty;
	protected $m_objApplication;
	protected $m_objDocumentManager;
	protected $m_objScreeningApplicationRequest;

	protected $m_boolExcludeAALetterConditions;
    protected $m_boolExcludeNotificationHeader;
    protected $m_boolExcludeNotificationFooter;

	private static $c_arrintNotificationCustomerTypeAssociation = [ self::DECISION_DENY_GUARANTOR => [ CCustomerType::PRIMARY ] ];

    public static $c_arrintScreeningDecisionNotificationType = [ CScreeningDecisionType::APPROVE_WITH_CONDITIONS => self::DECISION_APPROVE_WITH_CONDITION, CScreeningDecisionType::DENIED => self::DECISION_DENY ];

	public function __construct() {
		parent::__construct();
		$this->m_boolExcludeAALetterConditions = false;
        $this->m_boolExcludeNotificationHeader = false;
        $this->m_boolExcludeNotificationFooter = false;
		return;
	}

	public function setQuickResponseId( $intQuickResponseId ) {
		$this->m_intQuickResponseId = $intQuickResponseId;
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setExcludeAALetter( $boolExcludeAALetter ) {
		$this->m_boolExcludeAALetterConditions = $boolExcludeAALetter;
	}

	public function getQuickResponseId() {
		return $this->m_intQuickResponseId;
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['quick_response_id'] ) ) $this->setQuickResponseId( $arrmixValues['quick_response_id'] );
		if( isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningMessageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	private function validateRequest( $objDatabase ) {
		$boolIsValid = true;

		switch( $this->getScreeningMessageTypeId() ) {
			case CScreeningMessageType::SCREENING_NOTIFICATION:
					if( self::APPLICANT_CONDITIONALLY_APPROVED == $this->getId() && false == valArr( $this->m_arrobjScreeningNotifications ) ) {
						$boolIsValid = true;
						break;
					} elseif( false == valArr( $this->m_arrobjScreeningNotifications ) ) {
						$boolIsValid = false;
						break;
					}

					switch( $this->getId() ) {
						case self::CONDITIONAL_GUARANTOR_FAIL:
							if( false == $this->m_objScreeningApplicationRequest->compareScreeningApplicantResult( CCustomerType::GUARANTOR, CScreeningRecommendationType::FAIL, $objDatabase ) ) {
								$boolIsValid = false;
								break 2;
							}
							break;

						case self::CONDITIONAL_GUARANTOR_PASS:
							if( false == $this->m_objScreeningApplicationRequest->compareScreeningApplicantResult( CCustomerType::GUARANTOR, CScreeningRecommendationType::PASS, $objDatabase ) ) {
								$boolIsValid = false;
								break 2;
							}
							break;

						default:
							// handle this.
					}
				break;

			default:
				// handle this
				break;
		}

		return $boolIsValid;
	}

	private function loadCommonData( $objDatabase ) {
		$this->m_objApplication     = $this->m_objScreeningApplicationRequest->fetchApplication( $objDatabase );
		$this->m_objProperty        = ( true == valObj( $this->m_objApplication, 'CApplication' ) ) ? $this->m_objApplication->getOrFetchProperty( $objDatabase ) : NULL;

		$this->m_arrobjScreeningNotifications = $this->loadScreeningNotifications( $objDatabase );
        $objCompanyPreferenceExcludeConditionsAALetter = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'EXCLUDE_CONDITIONS_AA_LETTER', $this->m_objScreeningApplicationRequest->getCid(), $objDatabase );
        if( true == valObj( $objCompanyPreferenceExcludeConditionsAALetter, 'CCompanyPreference' ) && true == $objCompanyPreferenceExcludeConditionsAALetter->getValue() ) {
            $this->m_boolExcludeNotificationHeader = true;
            $this->m_boolExcludeNotificationFooter = true;
        }

		if( CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() ) {
			$this->m_objApplication->loadDocumentManager( $objDatabase, $this->m_intCurrentUserId );

			$this->m_objDocumentManager              = $this->m_objApplication->getDocumentManager();
			$this->m_arrobjScreeningApplicantResults = $this->m_objScreeningApplicationRequest->fetchScreeningApplicantResults( $objDatabase );

			if( ( true == $this->m_boolExcludeNotificationHeader && CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $this->m_intScreeningDecisionTypeId ) || ( false == $this->m_objProperty->checkIsAALetterRequired() ) ) {
				$this->m_boolExcludeAALetterConditions = true;
			}
		}
	}

	public function processScreeningNotification( $intCurrentUserId, $objScreeningApplicationRequest, $arrobjApplicants, $arrobjApplicantApplications, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants = false ) {
		$this->m_intCurrentUserId               = $intCurrentUserId;
		$this->m_objScreeningApplicationRequest = $objScreeningApplicationRequest;
		$this->m_intScreeningDecisionTypeId     = ( self::CONDITIONAL_GUARANTOR_FAIL_AA == $this->getId() ) ? CScreeningDecisionType::DENIED : $objScreeningApplicationRequest->getScreeningDecisionTypeId();

		$this->loadCommonData( $objDatabase );
		if( false == $this->validateRequest( $objDatabase ) ) return;

		switch( $this->getScreeningMessageTypeId() ) {
			case CScreeningMessageType::ADVERSE_ACTION_LETTER:
				$this->generateAdverseActionLetters( $arrobjApplicantApplications, $objDatabase );
				$arrmixScreeningNotifications = $this->generateScreeningNotification( $arrobjApplicants, $objDatabase, $boolOfferConditionsToApplicants );
				$this->sendEmails( $arrmixScreeningNotifications, $arrobjApplicants, $objDatabase, $objEmailDatabase );
				break;

			case CScreeningMessageType::SCREENING_NOTIFICATION:
				$this->sendScreeningNotifications( $arrobjApplicants, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants );
				break;

			default:
				// add comments
				break;
		}

		return true;
	}

	private function getTemplate( $intCustomerTypeId, $objDatabase ) {
		$objQuickResponse = NULL;

		$objScreeningNotification = ( true == valArr( $this->m_arrobjScreeningNotifications ) && true == array_key_exists( $intCustomerTypeId, $this->m_arrobjScreeningNotifications ) ) ? getArrayElementByKey( $intCustomerTypeId, $this->m_arrobjScreeningNotifications ) : NULL;

		if( true == valObj( $objScreeningNotification, 'CScreeningNotification' ) ) {
			if( CScreeningNotification::DEFAULT_TEMPLATE == $objScreeningNotification->getQuickResponseId() || true == is_null( $objScreeningNotification->getQuickResponseId() ) ) {
				$objQuickResponse = $this->loadDefaultTemplate();
			} else {
				$objQuickResponse = \Psi\Eos\Entrata\CQuickResponses::createService()->fetchQuickResponseByIdByCid( $objScreeningNotification->getQuickResponseId(), $this->m_objScreeningApplicationRequest->getCid(), $objDatabase );
				if( false == valObj( $objQuickResponse, 'CQuickResponse' ) || ( true == valObj( $objQuickResponse, 'CQuickResponse' ) && false == is_null( $objQuickResponse->getDeletedBy() ) && false == is_null( $objQuickResponse->getDeletedOn() ) ) ) {
					$objQuickResponse = $this->loadDefaultTemplate();
				}
			}
		} elseif( CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() || self::APPLICANT_CONDITIONALLY_APPROVED == $this->getId() ) {
			$objQuickResponse = $this->loadDefaultTemplate();
		}

		return $objQuickResponse;
	}

	private function getSubject( $objProperty, $objQuickResponse ) {
		return str_replace( '*PROPERTY_NAME*', $objProperty->getPropertyName(), $objQuickResponse->getSubject() );
	}

	private function loadDefaultTemplate() {
		$objSmarty        = new CPsSmarty( PATH_INTERFACES_COMMON, false );
		$objQuickResponse = new CQuickResponse();

		if( false == $this->m_objProperty->checkIsAALetterRequired() && CScreeningNotificationType::DECISION_DENY == $this->getId() ) {
			$objQuickResponse->setMessage( $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/ca_screening_notification_' . str_replace( ' ', '_', \Psi\CStringService::singleton()->strtolower( CScreeningNotificationType::getTypeNameByTypeId( $this->getId() ) ) ) . '.tpl' ) );
		} else {
			$objQuickResponse->setMessage( $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/screening_notification_' . str_replace( ' ', '_', \Psi\CStringService::singleton()->strtolower( CScreeningNotificationType::getTypeNameByTypeId( $this->getId() ) ) ) . '.tpl' ) );
		}

		$objQuickResponse->setSubject( '*PROPERTY_NAME* : ' . CScreeningNotificationType::getTypeNameByTypeId( $this->getId() ) );
		return $objQuickResponse;
	}

	private function generateScreeningNotification( $arrobjApplicants, $objDatabase, $boolOfferConditionsToApplicants ) {
		$objMergeFieldLibrary = new CMergeFieldLibrary( $this->m_objApplication->getCid(), NULL, $objDatabase );

		$arrmixScreeningConditionDetails = [];

		if( CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $this->m_intScreeningDecisionTypeId ) {
			// If conditions are offered it fetches all the condition
			// if not then satisfied condition only
			$boolFetchSatisfiedCOnditionOnly = ( true == $boolOfferConditionsToApplicants ) ? false : true;
			$arrmixScreeningConditionDetails = $this->m_objApplication->getScreeningApplicationConditionDetails( $objDatabase, $boolFetchSatisfiedCOnditionOnly );
		}

		$strWebsiteUrl                = $this->m_objApplication->generateRVWebsiteUrl( $objDatabase );
		$boolShowConditionsInAALetter = CScreeningUtils::determineShowConditionSection( $this->m_objApplication, $objDatabase );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );
		CScreeningDecisionType::assignSmartyConstants( $objSmarty );
		CScreeningMessageType::assignSmartyConstants( $objSmarty );

		$objSmarty->assign( 'media_library_uri',		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH );

		$objSmarty->assign_by_ref( 'screening_package_conditions',	$arrmixScreeningConditionDetails );
		$objSmarty->assign_by_ref( 'screening_decision_type_id',	$this->m_intScreeningDecisionTypeId );

		if( CScreeningDecisionType::DENIED == $this->m_intScreeningDecisionTypeId ) {
			$arrmixScreeningApplicantDenialReasons = $this->fetchScreeningApplicantDenialReasons();
		}

		foreach( $arrobjApplicants as $objApplicant ) {
			$objTemplate = $this->getTemplate( $objApplicant->getCustomerTypeId(), $objDatabase );

			// We receieve this blank object when notification are not configured to send to current applicant..
			if( false == valObj( $objTemplate, 'CQuickResponse' ) ) {
				continue;
			}

			$strTemplate = $objTemplate->getMessage();

			$this->m_objApplication->setApplicantId( $objApplicant->getId() );
			$objMergeFieldLibrary->loadApplicationsMergeFields( [ $this->m_objApplication ], $strTemplate );

			if( false !== \Psi\CStringService::singleton()->stripos( $strTemplate, '*CRIMINAL_DENIAL_TEXT*' ) ) {
				if( CScreeningDecisionType::DENIED != $this->m_intScreeningDecisionTypeId ) {
					$strTemplate = str_replace( '*CRIMINAL_DENIAL_TEXT*', '', $strTemplate );
				} else {
					if( true == valArr( $arrmixScreeningApplicantDenialReasons ) && true == array_key_exists( $objApplicant->getId(), $arrmixScreeningApplicantDenialReasons ) ) {
						$arrmixApplicantDenialReasons = getArrayElementByKey( $objApplicant->getId(), $arrmixScreeningApplicantDenialReasons );
						$arrmixApplicantDenialReasons = ( true == valArr( $arrmixApplicantDenialReasons ) ) ? rekeyArray( 'screen_type_id', $arrmixApplicantDenialReasons ) : NULL;
						if( true == valArr( $arrmixApplicantDenialReasons ) && true == array_key_exists( CScreenType::CRIMINAL, $arrmixApplicantDenialReasons ) ) {
							$strTemplate = str_replace( '*CRIMINAL_DENIAL_TEXT*', $this->getDefaultCriminalDenialTemplate(), $strTemplate );
						} else {
							$strTemplate = str_replace( '*CRIMINAL_DENIAL_TEXT*', ' ', $strTemplate );
						}
					} else {
						$strTemplate = str_replace( '*CRIMINAL_DENIAL_TEXT*', ' ', $strTemplate );
					}
				}
			}

			$arrstrReasons = $this->m_objScreeningApplicationRequest->getScreeningDecisionReasonNamesArray();
			if( CScreeningDecisionType::DENIED == $this->m_intScreeningDecisionTypeId ) {
				if( ( false == valArr( $arrstrReasons ) || CScreeningDecisionType::DENIED != $this->m_objScreeningApplicationRequest->getScreeningDecisionTypeId() ) && true == valArr( $arrmixScreeningApplicantDenialReasons ) && true == array_key_exists( $objApplicant->getId(), $arrmixScreeningApplicantDenialReasons ) ) {
					$arrmixApplicantDenialReasons = getArrayElementByKey( $objApplicant->getId(), $arrmixScreeningApplicantDenialReasons );
					$arrmixApplicantDenialReasons = ( true == valArr( $arrmixApplicantDenialReasons ) ) ? rekeyArray( 'decision_reason_name', $arrmixApplicantDenialReasons ) : NULL;
                    $arrstrReasons = ( true == valArr( $arrmixApplicantDenialReasons ) ) ? array_keys( $arrmixApplicantDenialReasons ) : '';
				}
			}

			$strEmailBody = CScreeningUtils::replaceQuickResponseMedia( $strTemplate, $this->m_objApplication, $this->m_objApplication->getOrFetchClient( $objDatabase ), $objDatabase );
			$strEmailBody = $objMergeFieldLibrary->getProcessedContent( $this->m_objApplication->getMergeFields(), $strEmailBody );

			$strQueryString 		  = '/customer[id]/' . $objApplicant->getCustomerId() . '/screening_request[id]/' . $this->m_objScreeningApplicationRequest->getId();
			$strScreeningConditionUrl = $strWebsiteUrl . 'Apartments/module/application_screening/action/view_screening_conditions/property[id]/' . $this->m_objApplication->getPropertyId() . $strQueryString;

			$boolShowViewConditionsButtonInAALetter = false;
			if( true == $boolShowConditionsInAALetter && false == ( CCustomerType::GUARANTOR == $objApplicant->getCustomerTypeId() ) && true == ( CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() && true == valObj( $this->m_objScreeningApplicationRequest->getScreeningConditionOffer( $objDatabase ), 'CScreeningConditionOffer' ) ) ) {
				$boolShowViewConditionsButtonInAALetter = true;
			}
			if( false == $this->m_objProperty->checkIsAALetterRequired() && CScreeningNotificationType::DECISION_DENY == $this->getId() ) {
				$this->m_boolExcludeNotificationHeader = true;
			}

            $objSmarty->assign( 'conditional_reasons',	    ( true == valArr( $arrstrReasons ) ) ? $arrstrReasons : '' );
			$objSmarty->assign( 'applicant_name',	        $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() );
			$objSmarty->assign( 'email_body',               $strEmailBody );
			$objSmarty->assign( 'apply_condition_page_url',	$strScreeningConditionUrl );
			$objSmarty->assign( 'exclude_condition',            $this->m_boolExcludeAALetterConditions );
            $objSmarty->assign( 'exclude_header',               $this->m_boolExcludeNotificationHeader );
            $objSmarty->assign( 'exclude_footer',               $this->m_boolExcludeNotificationFooter );
			$objSmarty->assign( 'show_view_condition_button',   $boolShowViewConditionsButtonInAALetter );
			$objSmarty->assign( 'screening_message_type_id', $this->getScreeningMessageTypeId() );

			$arrstrHtmlEmailOutput[$objApplicant->getId()]['notification']          = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/offered_adverse_action_letter_email.tpl' );
			$arrstrHtmlEmailOutput[$objApplicant->getId()]['notification_subject']  = $this->getSubject( $this->m_objApplication->getProperty(), $objTemplate );
		}

		return $arrstrHtmlEmailOutput;
	}

	private function createEmailAttachment( $intCurrentUserId, $intCid, $intApplicantId, $objDatabase, $objEmailDatabase ) {
		$objFileType = CFileTypes::fetchFileTypeBySystemCodeByCid( CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER, $intCid, $objDatabase );
		$arrobjFiles = CFiles::fetchFilesByApplicationIdByApplicantIdsByFileTypeIdByCid( $this->m_objApplication->getId(), [ $intApplicantId ], $objFileType->getId(), $intCid, $objDatabase );
		$objFile 	 = ( true == valArr( $arrobjFiles ) ) ? current( $arrobjFiles ) : NULL;

		if( false == valObj( $objFile, 'CFile' ) ) {
			trigger_error( 'Failed to load file object for applicant id : ' . ( int ) $intApplicantId . ' And cid : ' . ( int ) $intCid, E_USER_ERROR );
		}

		$this->m_objApplication->loadObjectStorageGateway();

		$objStoredObject                    = $objFile->fetchStoredObject( $this->m_objDatabase );
		$arrmixRequest                      = $objStoredObject->createGatewayRequest( [ 'checkExists' => true ] );
		$arrmixObjectStorageGatewayResponse = $this->m_objApplication->getObjectStorageGateway()->getObject( $arrmixRequest );

		if( false == $arrmixObjectStorageGatewayResponse['isExists'] ) {
			trigger_error( __( 'Failed to load file for applicant id : ' ) . ( int ) $intApplicantId . ' ' . __( 'application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$arrmixRequest               = $objStoredObject->createGatewayRequest();
		$arrmixObjectStorageResponse = $this->m_objApplication->getObjectStorageGateway()->getObject( $arrmixRequest );

		if( true == $arrmixObjectStorageResponse->hasErrors() ) {
			trigger_error( __( 'Failed to get file data for applicant id : ' ) . ( int ) $intApplicantId . ' ' . __( 'application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$objPrince = CPrinceFactory::createPrince();

		if( false == valObj( $objPrince, 'Prince' ) ) {
			trigger_error( 'Failed to load prince library object', E_USER_ERROR );
		}

		$strFilePath = PATH_MOUNTS_FILES . $intApplicantId . '/' . time() . '/';
		$strTempPath = CDocumentManagerUtils::getDocumentManagementNonBackupMountsPath( $strFilePath, $intCid );
		CFileIo::recursiveMakeDir( $strTempPath );
		$strLocalFile = $strTempPath . 'AdverseActionLetter.pdf';

		$objPrince->setBaseURL( PATH_COMMON );
		$objPrince->setHTML( 1 );
		$objPrince->convert_string_to_file( $arrmixObjectStorageResponse['data'], $strLocalFile );

		$strAttachmentPath      = dirname( $strLocalFile );
		$strAttachmentFileName  = basename( $strLocalFile );

		$objEmailAttachment = new CEmailAttachment();
		$objEmailAttachment->setTitle( $strAttachmentFileName );
		$objEmailAttachment->setFileName( $strAttachmentFileName );
		$objEmailAttachment->setFilePath( $strAttachmentPath );

		return $objEmailAttachment;
	}

	private function generateAdverseActionLetters( $arrobjApplicantApplications, $objDatabase ) {
		if( true == $this->m_boolExcludeAALetterConditions ) return;

		$arrobjApplicantApplications     = rekeyObjects( 'applicantId', $arrobjApplicantApplications );
		$arrmixScreeningApplicantLetters = $this->fetchAdverseActionLetters( array_keys( $arrobjApplicantApplications ), $objDatabase );

		if( false == valArr( $arrmixScreeningApplicantLetters ) ) {
			trigger_error( 'Failed to fetch adverse action letter for screening application request id : ' . $this->m_objScreeningApplicationRequest->getId() . ' And decision type : ' . $this->m_intScreeningDecisionTypeId, E_USER_WARNING );
			return;
		}

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == array_key_exists( $objApplicantApplication->getApplicantId(), $arrmixScreeningApplicantLetters ) ) continue;

			$strLetterTemplate = getArrayElementByKey( $objApplicantApplication->getApplicantId(), $arrmixScreeningApplicantLetters );
			$strLetterTemplate = base64_decode( $strLetterTemplate );

			$objApplicantApplication->generateDocumentFile( $strLetterTemplate, $this->m_objApplication, $objDatabase, $objApplicantApplication->getApplicantId() );
		}
	}

	private function sendEmails( $arrstrScreeningNotifications, $arrobjApplicants, $objDatabase, $objEmailDatabase, $boolAttachAALetter = false ) {
		$arrobjApplicantSystemEmails	= [];
		$arrobjManagerSystemEmails		= [];

		if( true == valObj( $this->m_objApplication, 'CApplication' ) ) {
			$arrobjPropertyPreferences 	= ( true == valObj( $this->m_objProperty, 'CProperty' ) ) ? ( array ) $this->m_objProperty->getOrFetchPropertyPreferences( $objDatabase ) : NULL;
			$objPropertyEmailRule		= \Psi\Eos\Entrata\CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $this->m_objProperty->getId(), $objDatabase );
			$arrstrPropertyEmailAddresses = $this->m_objProperty->fetchApplicationNotificationEmails( $objDatabase );
		}

		$boolAttachAALetter = true;

		$boolAttachAALetter = ( ( CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $this->m_intScreeningDecisionTypeId && true == $this->m_boolExcludeAALetterConditions ) || ( false == $this->m_objProperty->checkIsAALetterRequired() ) ) ? false : true;

		$objSystemEmail = new CSystemEmail();

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			$objSystemEmail->setPropertyId( $this->m_objProperty->getId() );
		}

		$objSystemEmail->setCid( $this->m_objScreeningApplicationRequest->getCid() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCREENING_EMAILS );

		$strFromEmailAddress = ( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) ? $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() : CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );

		$arrobjEventLibrary = [];

        $arrobjScreeningApplicantResults = \Psi\Eos\Entrata\CScreeningApplicantResults::createService()->fetchScreeningApplicantResultsByScreeningApplicationRequestIdByCid( $this->m_objScreeningApplicationRequest->getId(), $this->m_objScreeningApplicationRequest->getCid(), $this->m_objDatabase );
        $arrobjScreeningApplicantResultApplicants	= ( array ) rekeyObjects( 'ApplicantId', $arrobjScreeningApplicantResults );

        $arrintScreeningApplicantResults = array_keys( $arrobjScreeningApplicantResults );
        $arrobjExistingScreeningApplicantNotifications = \Psi\Eos\Entrata\CScreeningApplicantNotifications::createService()->fetchPublishedScreeningApplicantNotificationsByScreeningApplicantResultIdsByCidByPropertyIdByScreeningNotificationTypeId( $arrintScreeningApplicantResults, $this->m_objScreeningApplicationRequest->getCid(), $this->m_objProperty->getId(), $this->getId(), $objDatabase );

        $arrobjScreeningApplicantNotifications = [];

		foreach( $arrobjApplicants as $objApplicant ) {
			if( ( CScreeningMessageType::SCREENING_NOTIFICATION == $this->getScreeningMessageTypeId() && true == valArr( $this->m_arrobjScreeningNotifications ) && false == array_key_exists( $objApplicant->getCustomerTypeId(), $this->m_arrobjScreeningNotifications ) ) || true == is_null( $objApplicant->getUsername() ) ) {
				continue;
			}

			// added logic to skip if the notification is already sent
			if( CScreeningEventType::CONTINGENTLY_APPROVED == $this->getScreeningEventTypeId() ) {
				$intScreeningApplicantContingentNotificationCount = \Psi\Eos\Entrata\CScreeningApplicantNotifications::createService()->fetchTotalPublishedScreeningApplicantNotificationsByApplicantIdByCidByPropertyIdByScreeningNotiticationTypeId( $objApplicant->getId(), $this->m_objScreeningApplicationRequest->getCid(), $this->m_objProperty->getId(), $this->getId(), $objDatabase );
				if( 0 < $intScreeningApplicantContingentNotificationCount ) {
					continue;
				}
			}

			$objCurrentSystemEmail = clone $objSystemEmail;
            $objCurrentSystemEmail->setId( $objCurrentSystemEmail->fetchNextId( $objEmailDatabase ) );
			$objCurrentSystemEmail->setSubject( $arrstrScreeningNotifications[$objApplicant->getId()]['notification_subject'] );

			$objCurrentSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objCurrentSystemEmail->setHtmlContent( $arrstrScreeningNotifications[$objApplicant->getId()]['notification'] );
			$objCurrentSystemEmail->setApplicantId( $objApplicant->getId() );

			$arrobjApplicantSystemEmails[] = $objCurrentSystemEmail;

			$arrobjEventLibrary[$objApplicant->getId()] = $objApplicant->createEmailOutgoingEvent( $this->m_intCurrentUserId, $arrstrScreeningNotifications[$objApplicant->getId()]['notification_subject'], $this->m_objApplication, $this->m_objDatabase );

			if( true == valArr( $arrstrPropertyEmailAddresses ) ) {
				foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
					if( false == valStr( $strEmailAddress ) ) continue;

					$objManagerSystemEmail = clone $objSystemEmail;

					$objManagerSystemEmail->setSubject( $arrstrScreeningNotifications[$objApplicant->getId()]['notification_subject'] );
					$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );
					$objManagerSystemEmail->setHtmlContent( $arrstrScreeningNotifications[$objApplicant->getId()]['notification'] );
					$objManagerSystemEmail->setCompanyEmployeeId( $this->m_intCurrentUserId );
					$objManagerSystemEmail->setApplicantId( $objApplicant->getId() );

					$arrobjManagerSystemEmails[] = $objManagerSystemEmail;
				}
			}

			// Add entry in the screening_applicant_notifications table for Contingent Mail and AA Letter
			if( true == valArr( $arrobjScreeningApplicantResultApplicants ) ) {
				$objScreeningApplicantResult = $arrobjScreeningApplicantResultApplicants[$objApplicant->getId()];

				if( true == valObj( $objScreeningApplicantResult, 'CScreeningApplicantResult' ) && false == valArr( $arrobjExistingScreeningApplicantNotifications ) ) {
					$objScreeningApplicantNotification = new CScreeningApplicantNotification();
					$objScreeningApplicantNotification->setCid( $objScreeningApplicantResult->getCid() );
					$objScreeningApplicantNotification->setPropertyId( $objScreeningApplicantResult->getPropertyId() );
					$objScreeningApplicantNotification->setScreeningApplicantResultId( $objScreeningApplicantResult->getId() );
					$objScreeningApplicantNotification->setScreeningNotificationTypeId( $this->getId() );

					$arrobjScreeningApplicantNotifications[] = $objScreeningApplicantNotification;
				}
			}
        }

		switch( NULL ) {
			default:
				$objDatabase->begin();

				$intEmailSuccessCount = 0;

				$arrobjApplicantSystemEmails	= ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjApplicantSystemEmails, $objPropertyEmailRule );
				$arrobjManagerSystemEmails		= ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjManagerSystemEmails, $objPropertyEmailRule );

				if( false == valArr( $arrobjApplicantSystemEmails ) ) {
					break;
				}

				$arrintApplicantSystemEmailIds = [];

				foreach( $arrobjApplicantSystemEmails as $objSystemEmail ) {

					$intEmailSuccessCount ++;

					$arrintApplicantSystemEmailIds[$objSystemEmail->getApplicantId()] = $objSystemEmail->getId();

					if( true == $boolAttachAALetter && CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() ) {
						$objEmailAttachment = $this->createEmailAttachment( $this->m_intCurrentUserId, $objSystemEmail->getCid(), $objSystemEmail->getApplicantId(), $objDatabase, $objEmailDatabase );

						if( false == valObj( $objEmailAttachment, 'CEmailAttachment' ) ) {
							trigger_error( 'Failed to attach adverse action to the email for applicant id : ' . $objSystemEmail->getApplicantId(), E_USER_ERROR );
						}

						$objSystemEmail->addEmailAttachment( $objEmailAttachment );
					}

					if( false == $objSystemEmail->insert( $this->m_intCurrentUserId, $objEmailDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'System Email record failed to insert.', NULL ) );
						break;
					}

					if( true == valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
						unlink( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() );
					}
				}

				if( CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() && true == valArr( $this->m_arrobjScreeningApplicantResults ) && true == $boolAttachAALetter ) {
					$strSql = '';

					foreach( $this->m_arrobjScreeningApplicantResults as $objScreeningApplicantResult ) {
						$objScreeningApplicantResult->setAdverseActionLetterSentOn( 'NOW()' );

						$strSql .= $objScreeningApplicantResult->update( $this->m_intCurrentUserId, $objDatabase, true );
					}

					if( true == valStr( $strSql ) && false == valArr( executeSql( $strSql, $objDatabase ) ) ) {
						$objDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update screening applicant result.', NULL ) );
					}
				}

				if( true == valArr( $arrobjManagerSystemEmails ) ) {
					foreach( $arrobjManagerSystemEmails as $objSystemEmail ) {

						if( true == $boolAttachAALetter && CScreeningMessageType::ADVERSE_ACTION_LETTER == $this->getScreeningMessageTypeId() ) {
							$objEmailAttachment = $this->createEmailAttachment( $this->m_intCurrentUserId, $objSystemEmail->getCid(), $objSystemEmail->getApplicantId(), $objDatabase, $objEmailDatabase );

							if( false == valObj( $objEmailAttachment, 'CEmailAttachment' ) ) {
								trigger_error( 'Failed to attach adverse action to the email for applicant id : ' . $objSystemEmail->getApplicantId(), E_USER_ERROR );
							}

							$objSystemEmail->addEmailAttachment( $objEmailAttachment );
						}

						if( false == $objSystemEmail->insert( $this->m_intCurrentUserId, $objEmailDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'System Email record failed to insert.', NULL ) );
							break;
						}

						if( true == valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
							unlink( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() );
						}
					}
				}

				if( true == valArr( $arrobjEventLibrary ) && true == valArr( $arrintApplicantSystemEmailIds ) ) {
					foreach( $arrobjEventLibrary as $intApplicantId => $objEventLibrary ) {
						$objEventLibrary->getEventLibraryDataObject()->getEvent()->setDataReferenceId( $arrintApplicantSystemEmailIds[$intApplicantId] );
						$objEventLibrary->buildEventDescription( $objEventLibrary->getEventLibraryDataObject()->getApplication() );
						$objEventLibrary->setIsExportEvent( true );

						if( false == $objEventLibrary->insertEvent( $this->m_intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
							$objDatabase->rollback();
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert activity log.', NULL ) );
							break;
						}
					}
				}

                if( true == valArr( $arrobjScreeningApplicantNotifications ) ) {
                    if( false == \Psi\Eos\Entrata\CScreeningApplicantNotifications::createService()->bulkInsert( $arrobjScreeningApplicantNotifications, $this->m_intCurrentUserId, $objDatabase ) ) {
                        $objDatabase->rollback();
                        $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to add Screening Applicant Notification for applicant id' . $objApplicant->getId(), NULL ) );
                        break;
                    }
				}
				$objDatabase->commit();
		}

		return;
	}

	private function fetchAdverseActionLetters( $arrintApplicantIds, $objDatabase ) {
		$objResidentVerifyLibrary = new CResidentVerifyLibrary();
		return $objResidentVerifyLibrary->generateAdverseActionLetters( $this->m_intCurrentUserId, $arrintApplicantIds, $this->m_objScreeningApplicationRequest, $this->m_objApplication, $this->m_objProperty, $objDatabase );
	}

	private function sendScreeningNotifications( $arrobjApplicants, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants ) {
		$arrmixScreeningNotifications = $this->generateScreeningNotification( $arrobjApplicants, $objDatabase, $boolOfferConditionsToApplicants );
		$this->sendEmails( $arrmixScreeningNotifications, $arrobjApplicants, $objDatabase, $objEmailDatabase );

		return true;
	}

	private function loadScreeningNotifications( $objDatabase ) {
		$arrobjScreeningNotifications = CScreeningNotifications::fetchScreeningNotificationsByScreeningNotificationTypeIdByPropertyIdByCid( $this->getId(), $this->m_objApplication->getPropertyId(), $this->m_objApplication->getCid(), $objDatabase );
		if( true == valArr( $arrobjScreeningNotifications ) ) {
			$arrobjScreeningNotifications = rekeyObjects( 'CustomerTypeId', $arrobjScreeningNotifications );
		}
		return $arrobjScreeningNotifications;
	}

	private function checkSendNotification( $intApplicantId ) {
		if( false == valArr( $this->m_arrobjScreeningApplicantResults ) ) return false;
		return ( true == array_key_exists( $intApplicantId, $this->m_arrobjScreeningApplicantResults ) && false == is_null( $this->m_arrobjScreeningApplicantResults[$intApplicantId]->getAdverseActionLetterSentOn() ) ) ? false : true;
	}

	private function fetchScreeningApplicantDenialReasons() {
		$objResidentVerifyLibrary = new CResidentVerifyLibrary();
		$arrmixScreeningApplicantDenialReasons = $objResidentVerifyLibrary->fetchScreeningApplicantDenialReaons( $this->m_objScreeningApplicationRequest );

		if( false == valArr( $arrmixScreeningApplicantDenialReasons ) ) return NULL;

		foreach( $arrmixScreeningApplicantDenialReasons as $intKey => $arrmixScreeningDenialReason ) {
			$intScreeningDecisionReasonTypeId   = CScreeningDecisionReasonType::getScreeningDecisionReasonTypeIdByScreenTypeId( $arrmixScreeningDenialReason['screen_type_id'] );

			if( CScreeningDecisionReasonType::PRECISE_ID == $intScreeningDecisionReasonTypeId ) {
				$arrmixScreeningApplicantDenialReasons[$intKey]['decision_reason_name'] = 'Id Verification';
			} else {
				$arrmixScreeningApplicantDenialReasons[$intKey]['decision_reason_name'] = CScreeningDecisionReasonType::getScreeningDecisionReasonNameById( $intScreeningDecisionReasonTypeId );
			}

		}

		return rekeyArray( 'custom_applicant_id', $arrmixScreeningApplicantDenialReasons, false, true );
	}

	// As of now, this function will work for lincon only..
	// In future, may be some other clients may also ask for this

	private function getDefaultCriminalDenialTemplate() {
		$objSmarty        = new CPsSmarty( PATH_INTERFACES_COMMON, false );
		return $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/screening_notification_lincon_custom_criminal_text.tpl' );
	}

}
?>
