<?php

class CReviewType extends CBaseReviewType {

	protected $m_strReviewTypeIds;

	const RESIDENT						= 1;
	const EMPLOYEE						= 2;
	const CLIENT						= 3;
	const VACANCY						= 4;
	const APARTMENTRATINGS				= 5;
	const YELP							= 6;
	const CITYSEARCH					= 7;
	const RESIDENT_RATINGS_AND_REVIEW	= 8;
	const APARTMENTRATINGS_CHATMETER	= 9;
	const FACEBOOK_REVIEWS				= 21;
	const MODERN_MESSAGE				= 41;
	const YAHOO						    = 19;
	const YAHOOCA						= 43;
	const YELLOWPAGES					= 16;
	const YELLOWPAGESCA					= 44;
	const GOOGLE					    = 13;
	const YELP_EXTERNAL					= 10;
	const FACEBOOK_RECOMMENDATIONS		= 47;
	const FOURSQUARE                    = 12;
	const WECARE						= 48;
	const CORPORATE_INQUIRY				= 49;
	const APARTMENTS					= 42;

	public static $c_arrintEntrataReviewTypeIds = [
		self::RESIDENT,
		self::EMPLOYEE,
		self::CLIENT,
		self::MODERN_MESSAGE,
		'' . self::VACANCY . ',' . self::RESIDENT_RATINGS_AND_REVIEW . '',
		self::APARTMENTRATINGS,
		self::YELP,
		self::CITYSEARCH
	];

	public static $c_arrintResponseCharacterLimits = [
		CReviewType::APARTMENTRATINGS_CHATMETER => [ 'min' => 0, 'max' => 1500 ],
		CReviewType::APARTMENTS => [ 'min' => 50, 'max' => 10000 ]
	];

	public function setReviewTypeIds( $strReviewTypeIds ) {
		$this->m_strReviewTypeIds = CStrings::strTrimDef( $strReviewTypeIds, -1, NULL, true );
	}

	public function getReviewTypeIds() {
		return $this->m_strReviewTypeIds;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['review_type_ids'] ) ) {
			$this->setReviewTypeIds( $arrstrValues['review_type_ids'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDirectReply() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSource() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisplayName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasReviewTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisplayNameByReviewTypeId( $intReviewTypeId ) {

		switch( $intReviewTypeId ) {
			case self::RESIDENT:
			case self::EMPLOYEE:
			case self::CLIENT:
			case self::RESIDENT_RATINGS_AND_REVIEW:
				return 'Entrata';
				break;

			case self::VACANCY:
				return 'Vacancy';
				break;

			case self::APARTMENTRATINGS:
			case self::APARTMENTRATINGS_CHATMETER:
				return 'ApartmentRatings';
				break;

			case self::YELP:
			case self::YELP_EXTERNAL:
				return 'Yelp';
				break;

			case self::CITYSEARCH:
				return 'Citysearch';
				break;

			case self::FACEBOOK_REVIEWS:
				return 'Facebook Reviews';
				break;

			case self::MODERN_MESSAGE:
				return 'Modern Message';
				break;

			case self::YAHOO:
				return 'Yahoo';
				break;

			case self::YAHOOCA:
				return 'Yahooca';
				break;

			case self::YELLOWPAGES:
			case self::YELLOWPAGESCA:
				return 'YellowPages';
				break;

			case self::GOOGLE:
				return 'Google';
				break;

			case self::FACEBOOK_RECOMMENDATIONS:
				return 'Facebook Recommendations';
				break;

			case self::FOURSQUARE:
				return 'FourSquare';
				break;

			case self::WECARE:
				return 'WeCare';
				break;

			case self::CORPORATE_INQUIRY:
				return 'Corporate Inquiry';
				break;

			case self::APARTMENTS:
				return 'Apartments';
				break;

			default:
				// default case
				break;
		}
	}

}
?>