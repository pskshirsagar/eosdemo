<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerContactTypes
 * Do not add any new functions to this class.
 */

class CCustomerContactTypes extends CBaseCustomerContactTypes {

	public static function fetchCustomerContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCustomerContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCustomerContactTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM public.customer_contact_types';
		return self::fetchCustomerContactTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomerContactTypesByIds( $arrintIds, $objDatabase, $boolSortForEmail = false ) {

		if( false == valArr( $arrintIds ) ) return NULL;
		$strOrderBy = ( true == $boolSortForEmail ) ? ' ORDER BY array_position(array[' . CCustomerContactType::BILLING . ',' . CCustomerContactType::PRIMARY . ',' . CCustomerContactType::LEGAL . ',' . CCustomerContactType::MAINTENANCE . '], cct.id) ' : '';
		$strSql = 'SELECT
						cct.*
					FROM
						customer_contact_types cct
					WHERE
						cct.id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
					' . $strOrderBy;
		return self::fetchCustomerContactTypes( $strSql, $objDatabase );
	}

}
?>