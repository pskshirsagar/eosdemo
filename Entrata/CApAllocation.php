<?php

class CApAllocation extends CBaseApAllocation {

	protected $m_arrintApHeaderIds;
	protected $m_arrobjProperties;
	protected $m_arrobjPropertyGlSettings;
	protected $m_arrobjCompanyUserPreferences;
	protected $m_arrobjApDetails;

	protected $m_objGlHeader;
	protected $m_objApDetail;

	protected $m_strHeaderNumber;
	protected $m_strPaymentNumber;
	protected $m_strReversePostMonth;
	protected $m_strReversePostDate;

	// Get Functions

	public function getApHeaderIds() {
		return $this->m_arrintApHeaderIds;
	}

	public function getFormattedAllocationDatetime() {
		if( true == is_null( $this->m_strAllocationDatetime ) ) {
			return NULL;
		}
		return date( 'm/d/Y', strtotime( $this->m_strAllocationDatetime ) );
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getApDetails() {
		return $this->m_arrobjApDetails;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getPropertyGlSettings() {
		return $this->m_arrobjPropertyGlSettings;
	}

	public function getCompanyUserPreferences() {
		return $this->m_arrobjCompanyUserPreferences;
	}

	public function getReversePostMonth() {
		return $this->m_strReversePostMonth;
	}

	public function getReversePostDate() {
		return $this->m_strReversePostDate;
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function getPaymentNumber() {
		return $this->m_strPaymentNumber;
	}

	public function getGlHeader() {
		return $this->m_objGlHeader;
	}

	public function getApDetail() {
		return $this->m_objApDetail;
	}

	// Set functions
	// Override setPostMonth function. It will acccept date in dd/yyyy format and set it to dd/01/yyyy format

	public function setPostMonth( $strPostMonth ) {

		if( false == is_null( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$this->m_strPostMonth = $strPostMonth;
		}
	}

	public function setApDetails( $arrobjApDetails ) {
		$this->m_arrobjApDetails = $arrobjApDetails;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setPropertyGlSettings( $arrobjPropertyGlSettings ) {
		$this->m_arrobjPropertyGlSettings = $arrobjPropertyGlSettings;
	}

	public function setCompanyUserPreferences( $arrobjCompanyUserPreferences ) {
		$this->m_arrobjCompanyUserPreferences = $arrobjCompanyUserPreferences;
	}

	public function setReversePostMonth( $strReversePostMonth ) {

		if( true == valStr( $strReversePostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strReversePostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strReversePostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$this->m_strReversePostMonth = $strReversePostMonth;
		}

		return $this->m_strReversePostMonth;
	}

	public function setReversePostDate( $strReversePostDate ) {
		$this->m_strReversePostDate = CStrings::strTrimDef( $strReversePostDate, -1, NULL, true );
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->m_strHeaderNumber = CStrings::strTrimDef( $strHeaderNumber, 2000, NULL, true );
	}

	public function setPaymentNumber( $strPaymentNumber ) {
		$this->m_strPaymentNumber = CStrings::strTrimDef( $strPaymentNumber, 40, NULL, true );
	}

	public function setGlHeader( $objGlHeader ) {
		$this->m_objGlHeader = $objGlHeader;
	}

	public function setApDetail( $objApDetail ) {
		$this->m_objApDetail = $objApDetail;
	}

	public function setApHeaderIds( $arrintApHeaderIds ) {
		$this->m_arrintApHeaderIds = $arrintApHeaderIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['header_number'] ) ) {
			$this->setHeaderNumber( $arrmixValues['header_number'] );
		}
		if( true == isset( $arrmixValues['payment_number'] ) ) {
			$this->setPaymentNumber( $arrmixValues['payment_number'] );
		}

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAllocationDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getAllocationDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_datetime', 'Allocation date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeApDetailId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChargeApDetailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_ap_detail_id', 'Debit AP detail is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCreditApDetailId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCreditApDetailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_ap_detail_id', 'Credit AP detail is required.' ) );
		} elseif( $this->getChargeApDetailId() == $this->getCreditApDetailId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_ap_detail_id', 'Debit AP detail id should not be equal to Credit AP detail id.' ) );
		}

		return $boolIsValid;
	}

	public function valAllocationAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getAllocationAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', 'Allocation amount is required.' ) );
		} elseif( 0 >= $this->getAllocationAmount() && true == is_null( $this->getOriginApAllocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', 'Invalid allocation amount.' ) );
		}

		return $boolIsValid;
	}

	public function valAllocationAmountForNegativeAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getAllocationAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', 'Allocation amount is required.' ) );
		} elseif( 0 <= $this->getAllocationAmount() && true == is_null( $this->getOriginApAllocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_amount', 'Invalid allocation amount.' ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth( $intModule, $objCompanyUser, $boolIsReverse = false ) {

		$boolIsValid				= true;
		$strMessage					= NULL;
		$arrstrGlLockDateProperties	= [];
		$arrstrOutOfRangeProperties	= [];

		$strPostMonth = ( false == $boolIsReverse ) ? $this->getPostMonth() : $this->getReversePostMonth();

		if( false == is_null( $this->getLumpApHeaderId() ) ) {
			return $boolIsValid;
		}

		if( false == valStr( $strPostMonth ) ) {
			$boolIsValid &= false;

			$strMessage   = ( false == $boolIsReverse ) ? 'Post month is required.' : 'A reverse post month is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $strPostMonth ) ) {
			$boolIsValid &= false;

			$strMessage   = ( false == $boolIsReverse ) ? 'Post month of format mm/yyyy is required.' : 'The reverse post month format is mm/yyyy.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		}

		if( true == is_null( $objCompanyUser ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Company user not found.' ) );
		}

		if( true == is_null( $intModule ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Module id not found.' ) );
		}

		if( false == valArr( $this->m_arrobjApDetails ) || false == valArr( $this->m_arrobjPropertyGlSettings ) || false == valArr( $this->m_arrobjProperties ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Either line items or property or its GL settings is not set.' ) );
			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == $boolIsReverse && ( true == strtotime( $strPostMonth ) < strtotime( $this->getPostMonth() ) ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'The reverse post month may not be less than the check post month (' . date( 'm/Y', strtotime( $this->getPostMonth() ) ) . ').' ) );
		}

		$arrobjPropertyGlSettings	= rekeyObjects( 'PropertyId', $this->m_arrobjPropertyGlSettings );
		$arrobjApDetails			= rekeyObjects( 'PropertyId', $this->m_arrobjApDetails );

		foreach( $arrobjApDetails as $intPropertyId => $objApDetail ) {

			$objPropertyGlSetting = ( true == array_key_exists( $intPropertyId, $arrobjPropertyGlSettings ) )? $arrobjPropertyGlSettings[$intPropertyId] : NULL;

			// If no property gl setting available then skip iteration
			if( true == is_null( $objPropertyGlSetting ) ) {
				continue;
			}

			// Validate for GL lock period
			if( true == array_key_exists( $intPropertyId, $this->m_arrobjProperties ) && ( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) || strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) ) {
				$arrstrGlLockDateProperties[] = $this->m_arrobjProperties[$intPropertyId]->getPropertyName();
			}

			// If logged in user is administrator then no need to check post month's range permission
			if( 1 == $objCompanyUser->getIsAdministrator() ) {
				continue;
			}

			$intPastPostMonth	= strtotime( $objPropertyGlSetting->getApPostMonth() );
			$intFuturePostMonth	= strtotime( $objPropertyGlSetting->getApPostMonth() );

			// Create range of default AP post month based on user permission
			foreach( ( array ) $this->m_arrobjCompanyUserPreferences as $objCompanyUserPreference ) {

				if( false == is_null( $objCompanyUserPreference->getValue() ) && 0 < $objCompanyUserPreference->getValue() ) {

					if( CModule::CHECKS_POST_MONTH == $intModule ) {
						// Subtract months to ap_post_month
						if( CCompanyUserPreference::CHECKS_PAST_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intPastPostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' -' . $objCompanyUserPreference->getValue() . ' month' );
						}

						// Add months to ap_post_month
						if( CCompanyUserPreference::CHECKS_FUTURE_POST_MONTH == $objCompanyUserPreference->getKey() ) {
							$intFuturePostMonth = strtotime( date( 'm/d/Y', strtotime( $objPropertyGlSetting->getApPostMonth() ) ) . ' +' . $objCompanyUserPreference->getValue() . ' month' );
						}
					}
				}
			}

			// Validate for range of default AP post month based on user permissions
			if( true == array_key_exists( $intPropertyId, $this->m_arrobjProperties ) && ( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) ) {
				$arrstrOutOfRangeProperties[] = $this->m_arrobjProperties[$intPropertyId]->getPropertyName();
			}
		}

		if( true == valArr( $arrstrOutOfRangeProperties ) ) {

			$boolIsValid &= false;
			$arrstrOutOfRangeProperties = array_unique( $arrstrOutOfRangeProperties );

			if( CModule::CHECKS_POST_MONTH == $intModule ) {
				$strMessage = 'Post month is not in range for property(s) \'' . implode( '\', \'', $arrstrOutOfRangeProperties ) . '\'.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', $strMessage ) );
		}

		if( true == valArr( $arrstrGlLockDateProperties ) ) {

			$boolIsValid &= false;
			$arrstrGlLockDateProperties = array_unique( $arrstrGlLockDateProperties );

			if( CModule::CHECKS_POST_MONTH == $intModule ) {
				$strMessage = 'Post month is in GL locked period for property(s) \'' . implode( '\', \'', $arrstrGlLockDateProperties ) . '\'.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valPostDate( $intModule, $boolIsReverse = false ) {

		$boolIsValid	= true;
		$strMessage		= NULL;

		if( false == is_null( $this->getLumpApHeaderId() ) ) {
			return $boolIsValid;
		}

		$strPostDate	= ( false == $boolIsReverse ) ? $this->getPostDate() : $this->getReversePostDate();
		$strPostDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $strPostDate ] );

		if( false == valStr( $strPostDate ) ) {
			$boolIsValid &= false;

			$strMessage   = ( false == $boolIsReverse ) ? 'Payment date is required.' : 'A reverse date is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );

		} elseif( false == CValidation::checkDateFormat( $strPostDate ) ) {
			$boolIsValid &= false;

			$strMessage   = ( false == $boolIsReverse ) ? 'Check date should be in mm/dd/yyyy format.' : 'The reverse date format is mm/dd/yyyy.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		} elseif( true == $boolIsReverse && true == valStr( $strPostDate ) && strtotime( $strPostDate ) < strtotime( $this->getPostDate() ) ) {

			$boolIsValid &= false;
			$strMessage   = 'The reverse date may not be less than the payment date ( ' . $this->getPostDate() . ' ).';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intModule = NULL, $objCompanyUser = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAllocationDatetime();
				$boolIsValid &= $this->valChargeApDetailId();
				$boolIsValid &= $this->valCreditApDetailId();
				$boolIsValid &= $this->valAllocationAmount();
				break;

			case 'ap_allocation_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAllocationDatetime();
				$boolIsValid &= $this->valChargeApDetailId();
				$boolIsValid &= $this->valCreditApDetailId();
				$boolIsValid &= $this->valAllocationAmountForNegativeAmount();
				break;

			case 'ap_payment_summary':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAllocationDatetime();
				$boolIsValid &= $this->valAllocationAmountForNegativeAmount();
				$boolIsValid &= $this->valPostDate( $intModule );
				// $boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				break;

			case 'credit_memo_ap_allocation_insert':
			case 'auto_ap_allocation_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAllocationDatetime();
				$boolIsValid &= $this->valChargeApDetailId();
				$boolIsValid &= $this->valCreditApDetailId();
				$boolIsValid &= $this->valAllocationAmountForNegativeAmount();
				$boolIsValid &= $this->valPostDate( $intModule );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser );
				break;

			case 'credit_memo_ap_allocation_reverse':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPostDate( $intModule, true );
				$boolIsValid &= $this->valPostMonth( $intModule, $objCompanyUser, true );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>
