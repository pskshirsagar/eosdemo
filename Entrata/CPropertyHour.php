<?php

class CPropertyHour extends CBasePropertyHour {

	const 	DISPLAY_OLD_FORMAT = true;
	const 	MAX_PROPERTY_HOUR_DAYS = 8;
	const 	LUNCH_HOUR_INTERVAL = 10;

	protected $m_intIsUpdated;

	protected $m_arrstrDays;

	protected $m_strLunchStartTime;
	protected $m_strLunchEndTime;
	protected $m_strPropertyHolidayName;

	public function __construct() {
		parent::__construct();

		$this->m_arrstrDays = [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		return;
	}

	/**
	 * Other Functions
	 */
	public function getFormattedOpenTime() {
		$arrintHourMins	= explode( ':', $this->m_strOpenTime );
		$strTime = '';

		if( true == array_key_exists( 0, $arrintHourMins ) ) {

			if( 12 < $arrintHourMins[0] ) {
				$strTime = ( ( int ) $arrintHourMins[0] - 12 );

				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' PM';

			} elseif( 12 == $arrintHourMins[0] ) {
				$strTime = ( ( int ) $arrintHourMins[0] );

				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' PM';

			} else {
				$strTime = ( ( int ) $arrintHourMins[0] );

				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' AM';
			}
		}

		return $strTime;
	}

	public function getFormattedCloseTime() {
		$arrintHourMins	= explode( ':', $this->m_strCloseTime );

		$strTime = '';

		if( true == array_key_exists( 0, $arrintHourMins ) ) {

			if( 12 < $arrintHourMins[0] ) {
				$strTime = ( ( int ) $arrintHourMins[0] - 12 );

				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' PM';

			} elseif( 12 == $arrintHourMins[0] ) {
				$strTime = ( ( int ) $arrintHourMins[0] );

				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' PM';

			} else {

				$strTime = ( ( int ) $arrintHourMins[0] );
				if( true == array_key_exists( 1, $arrintHourMins ) ) {
					$strTime .= ':' . $arrintHourMins[1];
				}

				$strTime .= ' AM';
			}
		}

		return $strTime;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['open_time'] ) ) $this->setOpenTime( mb_substr( $arrmixValues['open_time'], 0, 5 ) );
		if( true == isset( $arrmixValues['close_time'] ) ) $this->setCloseTime( mb_substr( $arrmixValues['close_time'], 0, 5 ) );
		if( true == isset( $arrmixValues['lunch_start_time'] ) ) $this->setLunchStartTime( $arrmixValues['lunch_start_time'] );
		if( true == isset( $arrmixValues['lunch_end_time'] ) ) $this->setLunchEndTime( $arrmixValues['lunch_end_time'] );
		if( true == isset( $arrmixValues['property_holiday_name'] ) ) $this->setPropertyHolidayName( $arrmixValues['property_holiday_name'] );

		return;
	}

	public function setIsUpdated( $intIsUpdated ) {
		$this->m_intIsUpdated = $intIsUpdated;
	}

	public function setLunchStartTime( $strLunchStartTime ) {
		$this->m_strLunchStartTime = $strLunchStartTime;
	}

	public function setLunchEndTime( $strLunchEndTime ) {
		$this->m_strLunchEndTime = $strLunchEndTime;
	}

	public function setPropertyHolidayName( $strPropertyHolidayName ) {
		$this->m_strPropertyHolidayName = $strPropertyHolidayName;
	}

	/**
	 * Get Functions
	 */

	public function getIsUpdated() {
		return $this->m_intIsUpdated;
	}

	public function getLunchStartTime() {
		return $this->m_strLunchStartTime;
	}

	public function getLunchEndTime() {
		return $this->m_strLunchEndTime;
	}

	public function getPropertyHolidayName() {
		return $this->m_strPropertyHolidayName;
	}

	public static function getPropertyOpenHoursByMaintenanceRequestResponseDetailsByCid( $arrmixMaintenanceRequestResponseDetails, $intCid, $objDatabase ) {
		if( false == valArr( $arrmixMaintenanceRequestResponseDetails ) ) {
			return NULL;
		}

		$strValues = '';

		foreach( $arrmixMaintenanceRequestResponseDetails as $arrmixMaintenanceRequestDetail ) {
			if( '' != $strValues ) {
				$strValues .= ', ';
			}

			$strValues .= '( ' . ( int ) $intCid . ', 
                          ' . ( int ) $arrmixMaintenanceRequestDetail['property_id'] . ', 
                          ' . ( int ) $arrmixMaintenanceRequestDetail['maintenance_request_id'] . ',
                           \'' . $arrmixMaintenanceRequestDetail['creation_datetime'] . '\', 
                           \'' . $arrmixMaintenanceRequestDetail['response_datetime'] . '\', 
                           ' . ( int ) $arrmixMaintenanceRequestDetail['property_hour_type_id'] . ', 
                           ' . ( int ) $arrmixMaintenanceRequestDetail['maintenance_hour_type_id'] . ' )';
		}

		$strSql = ' WITH maintenance_request_response_details (
						cid,
						property_id,
						maintenance_request_id,
						creation_datetime,
						response_datetime,
						property_hour_type_id,
						maintenance_hour_type_id
				   ) AS ( VALUES ' . $strValues . ' )
				   SELECT
						maintenance_request_id,
						CASE 
 	                        WHEN ' . CMaintenanceHourType::ALL_HOURS . ' <> maintenance_hour_type_id 
 	                            THEN round( round( calc_open_hours( cid, property_id, creation_datetime::TIMESTAMP, response_datetime::TIMESTAMP, property_hour_type_id, maintenance_hour_type_id ) ::NUMERIC, 2 ) * 60 ) 
 	                        ELSE
 	                            DATE_PART(\'day\', response_datetime::TIMESTAMP - creation_datetime::TIMESTAMP ) * 24 * 60 +
                                DATE_PART(\'hour\', response_datetime::TIMESTAMP  - creation_datetime::TIMESTAMP ) * 60+
                                DATE_PART(\'minute\', response_datetime::TIMESTAMP  - creation_datetime::TIMESTAMP )
 	                    END AS open_hours
                   FROM
                        maintenance_request_response_details
					';

		$arrintResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResponseData ) ) {
			$arrintResponseData = rekeyArray( 'maintenance_request_id', $arrintResponseData );
		}

		return $arrintResponseData;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyHourTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyHolidayId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfficeHours() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCloseTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valByAppointmentOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOldFormat() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTime();
				$boolIsValid &= $this->valDay();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valTime() {
		$boolIsValid = true;
		$boolIsValid &= true;

		$strDay = '';
		if( true == isset( $this->m_arrstrDays[$this->getDay()] ) ) {
			$strDay = $this->m_arrstrDays[$this->getDay()];
		}

		if( ( 0 == mb_strlen( trim( $this->getOpenTime() ) ) && 0 != mb_strlen( trim( $this->getCloseTime() ) ) ) || ( 0 == mb_strlen( trim( $this->getCloseTime() ) ) && 0 != mb_strlen( trim( $this->getOpenTime() ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'If either {%s, 0} open time or close time is set, the corresponding open or close time must also be set.', [ $strDay ] ) ) );
			$boolIsValid &= false;
		}

		if( 0 < mb_strlen( trim( $this->getOpenTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getOpenTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'open_time', __( '{%s, 0} Open time improperly formatted.', [ $strDay ] ) ) );
			$boolIsValid &= false;
		}

		if( 0 < mb_strlen( trim( $this->getCloseTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getCloseTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_time', __( '{%s, 0} Close time improperly formatted.', [ $strDay ] ) ) );
			$boolIsValid &= false;
		}

		$arrstrCloseTime = explode( ':', $this->getCloseTime() );
		$arrstrOpenTime = explode( ':', $this->getOpenTime() );

		if( 0 < mb_strlen( trim( $this->getCloseTime() ) ) && 0 < mb_strlen( trim( $this->getOpenTime() ) ) && true == valArr( $arrstrCloseTime ) && true == valArr( $arrstrOpenTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrCloseTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrOpenTime ) ) {

			$intOpenTime 	= ( ( int ) ( $arrstrOpenTime[0] * 60 ) + ( int ) $arrstrOpenTime[1] );
			$intCloseTime 	= ( ( int ) ( $arrstrCloseTime[0] * 60 ) + ( int ) $arrstrCloseTime[1] );

			if( false == is_numeric( $arrstrOpenTime[0] ) || false == is_numeric( $arrstrOpenTime[1] ) || false == is_numeric( $arrstrCloseTime[0] ) || false == is_numeric( $arrstrCloseTime[1] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} Please enter valid time. ', [ $strDay ] ) ) );
				$boolIsValid &= false;
			} elseif( $intOpenTime > $intCloseTime ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '{%s, 0} Open time is greater than the close time.', [ $strDay ] ) ) );
						$boolIsValid &= false;
			} elseif( $intOpenTime == $intCloseTime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Maintenance Open and close time cannot be the same.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

}
?>