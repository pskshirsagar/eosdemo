<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplications
 * Do not add any new functions to this class.
 */
class CApplications extends CBaseApplications {

	public static function fetchPaginatedApplicationsByPropertyIdsByPsProductIdsByApplicationsFilterByCid( $arrintPropertyIds, $arrintPsProductIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	    = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql   = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesAsSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND deleted_on IS NULL' : '';

		$arrstrWhereParameters = [];

		// -1 means outside the core query, 1 means inside the core query, and 0 means no join at all
		$intJoinLeases = ( true == in_array( CLeaseIntervalType::RENEWAL, ( array ) $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) ? - 1 : 0;
		$intJoinLeases = ( -1 == $intJoinLeases && 'lease_end_date' == $objApplicationsFilter->getSortBy() ) ? 1 : - 1;

		$intJoinPropertyUnits 		= -1;
		$intJoinPropertyFloorplans 	= 0;

		$intJoinEvents = ( true == valArr( $arrintPsProductIds ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) ? - 1 : 0;

		$intOffset 	= ( 0 < ( int ) $objApplicationsFilter->getPageNumber() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNumber() - 1 ) : 0;
		$intLimit 	= ( 0 < $objApplicationsFilter->getCountPerPage() ) ? $objApplicationsFilter->getCountPerPage() : 25;
		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) ) {
			$arrstrQuickSearchWhereSqls = [];

			$arrstrQuickSearchItems	= explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );
			foreach( $arrstrQuickSearchItems as $strQuickSearchItem ) {
				if( true == valStr( $strQuickSearchItem ) ) {
					$arrstrQuickSearchWhereSqls[] = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR CAST( app.id AS text ) LIKE \'%' . addslashes( $strQuickSearchItem ) . '%\' OR lower( appt.phone_number ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) ) ';
				}
			}

			if( true == valArr( $arrstrQuickSearchWhereSqls ) ) {
				$arrstrWhereParameters[] = ' ( ' . implode( 'OR', $arrstrQuickSearchWhereSqls ) . ' ) ';
			}
		}

		if( true == valArr( $objApplicationsFilter->getApplicationIds() ) ) 			$arrstrWhereParameters[] = 'app.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getLeadSourceIds() ) ) 				$arrstrWhereParameters[] = 'app.lead_source_id IN ( ' . implode( ',', $objApplicationsFilter->getLeadSourceIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getPsProductIds() ) ) 				$arrstrWhereParameters[] = 'app.ps_product_id IN ( ' . implode( ',', $objApplicationsFilter->getPsProductIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getCompanyEmployeeIds() ) ) 		$arrstrWhereParameters[] = 'app.leasing_agent_id IN ( ' . implode( ',', $objApplicationsFilter->getCompanyEmployeeIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getPropertyFloorplanIds() ) ) 		$arrstrWhereParameters[] = 'app.property_floorplan_id IN ( ' . implode( ',', $objApplicationsFilter->getPropertyFloorplanIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) 		$arrstrWhereParameters[] = 'li_app.lease_interval_type_id IN ( ' . implode( ',', $objApplicationsFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getCompanyapplicationIds() ) ) 		$arrstrWhereParameters[] = 'app.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';

		if( 0 < ( int ) $objApplicationsFilter->getDesiredRentMin() ) 					$arrstrWhereParameters[] = 'app.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		if( 0 < ( int ) $objApplicationsFilter->getDesiredRentMax() ) 					$arrstrWhereParameters[] = 'app.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';

		if( true == valArr( $objApplicationsFilter->getPropertyIds() ) ) {
			$arrstrWhereParameters[] = 'app.property_id IN ( ' . implode( ',', $objApplicationsFilter->getPropertyIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 'Min' != $objApplicationsFilter->getApplicationStartDate() && true == CValidation::validateDate( $objApplicationsFilter->getApplicationStartDate() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', app.application_datetime ) >= \'' . $objApplicationsFilter->getApplicationStartDate() . '\' ';
		if( 'Max' != $objApplicationsFilter->getApplicationEndDate() && true == CValidation::validateDate( $objApplicationsFilter->getApplicationEndDate() ) ) 		$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', app.application_datetime ) <= \'' . $objApplicationsFilter->getApplicationEndDate() . '\' ';

		if( 'Min' != $objApplicationsFilter->getMoveInDateMin() && true == CValidation::validateDate( $objApplicationsFilter->getMoveInDateMin() ) )	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', li.lease_start_date ) >= \'' . $objApplicationsFilter->getMoveInDateMin() . '\' ';
		if( 'Max' != $objApplicationsFilter->getMoveInDateMax() && true == CValidation::validateDate( $objApplicationsFilter->getMoveInDateMax() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', li.lease_start_date ) <= \'' . $objApplicationsFilter->getMoveInDateMax() . '\' ';

		if( true == valArr( $objApplicationsFilter->getApplicationStageStatusIds() ) ) {
			$arrstrWhereParameters[] = 'ass.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationStageStatusIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
		}

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );
		if( true == valArr( $objApplicationsFilter->getCompanyEmployeeIds() ) && true == $objApplicationsFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhereParameters[] = ' app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationsFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' app.leasing_agent_id IS NULL ';
		}

		// We may require this code in future when we enhance the search leads.
		if( false == is_null( $objApplicationsFilter->getLastContactMinDays() ) && '' != trim( $objApplicationsFilter->getLastContactMinDays() ) && '0' != trim( $objApplicationsFilter->getLastContactMinDays() ) && -1 == $intJoinEvents ) {
			$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getLastContactMaxDays() ) && '' != trim( $objApplicationsFilter->getLastContactMaxDays() ) && '0' != trim( $objApplicationsFilter->getLastContactMaxDays() ) && -1 == $intJoinEvents ) {
			$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getDesiredBedrooms() ) && '' != trim( $objApplicationsFilter->getDesiredBedrooms() ) ) {
			$intJoinPropertyFloorplans 	= 1;

			$arrstrBedroomsWhereSqls 	= [];
			$arrintDesiredBedrooms 		= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) 	$arrstrBedroomsWhereSqls[] = ' app.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBedrooms ) ) 		$arrstrBedroomsWhereSqls[] = ' app.desired_bedrooms > 5 ';

			if( true == valArr( $arrstrBedroomsWhereSqls ) ) $arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBedroomsWhereSqls ) . ' ) ';
		}
		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;
			$arrintDesiredBathrooms		= explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			$arrstrBathroomsWhereSqls = [];
			if( true == valArr( $arrintDesiredBathrooms ) ) 	$arrstrBathroomsWhereSqls[] = ' app.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBathrooms ) ) 			$arrstrBathroomsWhereSqls[] = ' app.desired_bathrooms > 5 ';

			if( true == valArr( $arrstrBathroomsWhereSqls ) ) 	$arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBathroomsWhereSqls ) . ' ) ';
		}
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$intUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhereParameters[] = 'li_app.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == valArr( $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhereParameters[] = 'li_app.lease_interval_type_id IN ( ' . implode( ',', $objApplicationsFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( li_app.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.app_remote_primary_key IS NOT NULL )
												OR
											( li_app.lease_interval_type_id = ' . CLeaseIntervalType::GUEST_CARD . ' AND app.guest_remote_primary_key IS NOT NULL ) ) ';

			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( li_app.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.app_remote_primary_key IS NULL )
												OR
											( li_app.lease_interval_type_id = ' . CLeaseIntervalType::GUEST_CARD . ' AND app.guest_remote_primary_key IS NULL ) ) ';
			}
		}

		$strEventsJoinSql = ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = app.last_event_id AND e.cid = app.cid ) ';

		$strEventFields = 'e.id as event_id,
							e.event_datetime,
							e.event_type_id,
							DATE_TRUNC(\'day\',  NOW() - e.event_datetime ) as last_contact_days,';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						app.*,
						CASE WHEN us.space_number IS NOT NULL AND ( 1 < ( SELECT COUNT(id) FROM unit_spaces WHERE property_unit_id = app.property_unit_id AND cid = app.cid ' . $strCheckDeletedUnitSpacesAsSql . ' ) )
							THEN pu.unit_number || \'-\' || us.space_number
							ELSE pu.unit_number
						END AS unit_number,
						' . ( ( -1 == $intJoinEvents ) ? $strEventFields : '' ) . '
						' . ( ( 1 == $intJoinLeases ) ? 'li.lease_end_date,' : '' ) . '
						' . ( ( -1 == $intJoinLeases ) ? 'li.lease_end_date,' : '' ) . '
						CASE WHEN TRUE = app.is_pet_policy_conflicted
							AND ( 0 = ( SELECT
											count(pap.id)
										FROM
											property_application_preferences as pap
										WHERE
											pap.cid = ' . ( int ) $intCid . '
											AND pap.property_id = app.property_id
											AND pap.company_application_id = app.company_application_id
											AND pap.cid = app.cid
											AND pap.key = \'HIDE_OPTION_PET\' LIMIT 1 ) ) THEN 1 ELSE 0
						END AS is_pet_policy_conflicted
					FROM
						(
							SELECT
								appt.id AS applicant_id,
								appt.name_last,
								appt.name_first,
								appt.phone_number,
								appt.mobile_number,
								appt.work_number,
								appt.username as email_address,
								app.id as id,
								app.cid,
								app.property_id,
								app.company_application_id,
								app.application_datetime,
								app.property_floorplan_id,
								app.property_unit_id,
								app.unit_space_id,
								app.lead_source_id,
								app.leasing_agent_id,
								app.application_stage_id,
								app.application_status_id,
								app.last_event_id,
								app.is_pet_policy_conflicted,
								app.lease_id,
								li_app.lease_interval_type_id,
								app.ps_product_id,
								app.screening_id,
								app.guest_remote_primary_key,
								app.app_remote_primary_key,
								app.created_on,
								app.application_completed_on,
								app.lease_completed_on,
								app.application_approved_on,
								aa.application_step_id,
								aa.application_document_id,
								' . ( ( 1 == $intJoinEvents ) ? $strEventFields : '' ) . '
								' . ( ( 1 == $intJoinLeases ) ? 'li.lease_end_date as lease_end_date,' : '' ) . '
								aa.application_form_id,
								aa.lease_generated_on ';

		$strSql .= ' FROM
						applications app
						JOIN lease_intervals li_app ON ( li_app.cid = app.cid AND li_app.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li_app.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';

		$strSql .= ( 1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid )
											LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.cid = l.cid AND li.id = l.active_lease_interval_id ) ' : '';

		$strSql .= ( 1 == $intJoinPropertyUnits ) ? ' LEFT JOIN property_units pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' ) ' : '';
		$strSql .= ( 1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE  app.cid = ' . ( int ) $intCid;

		if( 0 == $intJoinLeases ) {
			$strSql .= ' AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;
		}

		if( true == valArr( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters ) . ' ';
		}

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'name'						=> 'appt.name_last || appt.name_first',
			'status'					=> 'app.application_status_id',
			'property'					=> 'app.property_id',
			'source'					=> 'app.lead_source_id',
			'application_datetime'		=> 'app.application_datetime',
			'application_approved_on'	=> 'app.application_approved_on',
			'application_completed_on'	=> 'app.application_completed_on',
			'lease_completed_on'		=> 'app.lease_completed_on',
			'lease_interval_type'		=> 'li_app.lease_interval_type_id',
			'lease_end_date'			=> 'li.lease_end_date'
		];

		$strOrderBy 		= ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'app.application_datetime' );
		$strSortDirection 	= ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';

		$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNumber() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql .= ' ) AS app
					LEFT JOIN unit_spaces us ON ( us.id = app.unit_space_id AND us.cid = app.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					LEFT JOIN property_units pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' ) ';

		$strSql .= ( -1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid )
												LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( -1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicationsCountByPropertyIdsByPsProductIdsByApplicationFilterByCid( $arrintPropertyIds, $arrintPsProductIds, $objApplicationFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrstrWhereParameters 			= [];

		$boolJoinApplicantSql 			= false;
		$boolJoinPropertyFloorplanSql	= false;
		$boolJoinPropertyUnitSql		= false;
		$boolIsRenewal					= false;
		$boolJoinEvents					= false;

		if( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) && true == in_array( CLeaseIntervalType::RENEWAL, $objApplicationFilter->getLeaseIntervalTypeIds() ) ) {
			$boolIsRenewal = true;
		}
		$strQuickSearch = trim( $objApplicationFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) ) {
			$arrstrQuickSearchWhereSqls = [];
			$boolJoinApplicantSql 		= true;

			$arrstrQuickSearchItems	= explode( ' ', trim( $objApplicationFilter->getQuickSearch() ) );
			foreach( $arrstrQuickSearchItems as $strQuickSearchItem ) {
				if( true == valStr( $strQuickSearchItem ) ) {
					$arrstrQuickSearchWhereSqls[] = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR CAST( a.id AS text ) LIKE \'%' . addslashes( $strQuickSearchItem ) . '%\' OR lower( cpn.phone_number ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) ) ';
				}
			}

			if( true == valArr( $arrstrQuickSearchWhereSqls ) ) {
				$arrstrWhereParameters[] = ' ( ' . implode( 'OR', $arrstrQuickSearchWhereSqls ) . ' ) ';
			}
		}

		if( true == valArr( $objApplicationFilter->getApplicationIds() ) ) 			$arrstrWhereParameters[] = 'a.id IN ( ' . implode( ',', $objApplicationFilter->getApplicationIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getLeadSourceIds() ) ) 			$arrstrWhereParameters[] = 'a.lead_source_id IN ( ' . implode( ',', $objApplicationFilter->getLeadSourceIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getPsProductIds() ) ) 			$arrstrWhereParameters[] = 'a.ps_product_id IN ( ' . implode( ',', $objApplicationFilter->getPsProductIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getCompanyEmployeeIds() ) ) 		$arrstrWhereParameters[] = 'a.leasing_agent_id IN ( ' . implode( ',', $objApplicationFilter->getCompanyEmployeeIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getPropertyFloorplanIds() ) ) 	$arrstrWhereParameters[] = 'a.property_floorplan_id IN ( ' . implode( ',', $objApplicationFilter->getPropertyFloorplanIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) ) 			$arrstrWhereParameters[] = 'li.lease_interval_type_id IN ( ' . implode( ',', $objApplicationFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getCompanyApplicationIds() ) ) 	$arrstrWhereParameters[] = 'a.company_application_id IN( ' . implode( ',', $objApplicationFilter->getCompanyApplicationIds() ) . ' )';

		if( 0 < ( int ) $objApplicationFilter->getDesiredRentMin() ) 					$arrstrWhereParameters[] = 'a.desired_rent_min >= ' . ( int ) $objApplicationFilter->getDesiredRentMin() . ' ';
		if( 0 < ( int ) $objApplicationFilter->getDesiredRentMax() ) 					$arrstrWhereParameters[] = 'a.desired_rent_max <= ' . ( int ) $objApplicationFilter->getDesiredRentMax() . ' ';

		if( true == valArr( $objApplicationFilter->getPropertyIds() ) ) {
			$arrstrWhereParameters[] = 'a.property_id IN ( ' . implode( ',', $objApplicationFilter->getPropertyIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = 'a.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 'Min' != $objApplicationFilter->getApplicationStartDate() && true == CValidation::validateDate( $objApplicationFilter->getApplicationStartDate() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', a.application_datetime ) >= \'' . $objApplicationFilter->getApplicationStartDate() . '\' ';
		if( 'Max' != $objApplicationFilter->getApplicationEndDate() && true == CValidation::validateDate( $objApplicationFilter->getApplicationEndDate() ) ) 		$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', a.application_datetime ) <= \'' . $objApplicationFilter->getApplicationEndDate() . '\' ';

		if( 'Min' != $objApplicationFilter->getMoveInDateMin() && true == CValidation::validateDate( $objApplicationFilter->getMoveInDateMin() ) )	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', li.lease_start_date ) >= \'' . $objApplicationFilter->getMoveInDateMin() . '\' ';
		if( 'Max' != $objApplicationFilter->getMoveInDateMax() && true == CValidation::validateDate( $objApplicationFilter->getMoveInDateMax() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', li.lease_start_date ) <= \'' . $objApplicationFilter->getMoveInDateMax() . '\' ';

		if( true == valArr( $objApplicationFilter->getApplicationStageStatusIds() ) ) {
			$arrstrWhereParameters[] = 'ass.id IN ( ' . implode( ',', $objApplicationFilter->getApplicationStageStatusIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = '( application_stage_id, application_status_id ) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
		}

		$strCompanyEmployees = trim( $objApplicationFilter->getCompanyEmployees() );
		if( true == valArr( $objApplicationFilter->getCompanyEmployeeIds() ) && true == $objApplicationFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ( a.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR a.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhereParameters[] = ' a.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' a.leasing_agent_id IS NULL ';
		}

		// We may require this code in future when we enhance the search leads.
		if( true == valArr( $arrintPsProductIds ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) {
			if( false == is_null( $objApplicationFilter->getLastContactMinDays() ) && '' != trim( $objApplicationFilter->getLastContactMinDays() ) && '0' != trim( $objApplicationFilter->getLastContactMinDays() ) && -1 == $intJoinEvents ) {
				$boolJoinEvents = true;
				$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationFilter->getLastContactMinDays() . ' ';
			}

			if( false == is_null( $objApplicationFilter->getLastContactMaxDays() ) && '' != trim( $objApplicationFilter->getLastContactMaxDays() ) && '0' != trim( $objApplicationFilter->getLastContactMaxDays() ) && -1 == $intJoinEvents ) {
				$boolJoinEvents = true;
				$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationFilter->getLastContactMaxDays() . ' ';
			}
		}

		$strDesiredBedrooms = trim( $objApplicationFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;

			$arrstrBedroomsWhereSqls 	= [];
			$arrintDesiredBedrooms		= explode( ',', $objApplicationFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) $arrstrBedroomsWhereSqls[] = ' a.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBedrooms ) ) 		$arrstrBedroomsWhereSqls[] = ' a.desired_bedrooms > 5 ';

			if( true == valArr( $arrstrBedroomsWhereSqls ) ) $arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBedroomsWhereSqls ) . ' ) ';
		}

		$strDesiredBathrooms = trim( $objApplicationFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;
			$arrintDesiredBathrooms		= explode( ',', $objApplicationFilter->getDesiredBathrooms() );

			$arrstrBathroomsWhereSqls = [];
			if( true == valArr( $arrintDesiredBathrooms ) ) 	$arrstrBathroomsWhereSqls[] = ' a.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBathrooms ) ) 			$arrstrBathroomsWhereSqls[] = ' a.desired_bathrooms > 5 ';

			if( true == valArr( $arrstrBathroomsWhereSqls ) ) 	$arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBathroomsWhereSqls ) . ' ) ';
		}

		$strDesiredRentMin = trim( $objApplicationFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationFilter->getDesiredRentMax() );

		$strDesiredOccupants = trim( $objApplicationFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'pu.max_occupants = ' . ( int ) $objApplicationFilter->getDesiredOccupants() . ' ';
		}

		$strPropertyFloorplans = trim( $objApplicationFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhereParameters[] = 'a.property_floorplan_id IN ( ' . $objApplicationFilter->getPropertyFloorplans() . ' ) ';
		}
		$intUnitNumbers = trim( $objApplicationFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationFilter->getUnitNumbers() ) . '%\' ) ';
		}

		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhereParameters[] = 'li.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhereParameters[] = 'li.lease_interval_type_id IN ( ' . implode( ',', $objApplicationFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND a.app_remote_primary_key IS NOT NULL ) ) ';

			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND a.app_remote_primary_key IS NULL ) ) ';
			}
		}

		$strJoinSql = '';

		if( true == $boolJoinApplicantSql ) 							$strJoinSql .= ' INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) LEFT JOIN customer_phone_numbers cpn ON ( appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )';
		if( true == $boolJoinApplicantSql && true == $boolIsRenewal ) 	$strJoinSql .= ' LEFT JOIN leases l ON ( a.lease_id = l.id AND a.cid = l.cid ) ';
		if( true == $boolJoinPropertyFloorplanSql )						$strJoinSql .= ' LEFT JOIN property_floorplans pfp ON ( pfp.id = a.property_floorplan_id AND pfp.cid = a.cid ' . $strCheckDeletedFloorPlansSql . '  ) ';
		if( true == $boolJoinPropertyUnitSql )							$strJoinSql .= ' LEFT JOIN property_units pu ON ( pu.id = a.property_unit_id AND pu.cid = a.cid ' . $strCheckDeletedUnitsSql . ' ) ';
		if( true == $boolJoinEvents )									$strJoinSql .= ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = a.last_event_id AND e.cid = a.cid ) ';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						COUNT( a.id )
					FROM
						applications a
						JOIN lease_intervals li ON ( li.id = a.lease_interval_id AND li.cid = a.cid )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = a.application_stage_id AND ass.application_status_id = a.application_status_id AND ass.lease_interval_type_id = a.lease_interval_type_id)
						INNER JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' ) '
				. $strJoinSql . '
					WHERE
						a.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchCustomApplicationByIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						a.*,
						ap.id as applicant_id,
						aa.id as applicant_application_id,
						pfp.floorplan_name,
						CASE WHEN (cp.key)::text = \'ENABLE_LEAD_SCORING\' AND (cp.value)::numeric = 2 THEN 
								ROUND( util.util_mapto_range(( a.details::json->>\'lead_score\' )::text::numeric, ( a.details::json->>\'lead_score_min\' )::text::numeric, ( a.details::json->>\'lead_score_max\' )::text::numeric, 0.5, 0.0, 1.0), 1 )
							ELSE
								ROUND( util.util_mapto_range(( a.details::json->>\'lead_score\' )::text::numeric, 0.1, 5.0, 0.5, 0.0, 1.0), 1 )
							END
					     AS lead_score
					FROM
						applications a
						JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants ap ON ( ap.id = aa.applicant_id AND ap.cid = aa.cid )
						LEFT JOIN property_floorplans pfp ON ( pfp.id = a.property_floorplan_id AND pfp.cid = a.cid )
						LEFT JOIN company_preferences cp ON (cp.cid = a.cid AND cp.key = \'ENABLE_LEAD_SCORING\' )
					WHERE
						a.id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND a.cid = ' . ( int ) $intCid;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM applications WHERE cid = ' . ( int ) $intCid . ' AND lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM applications WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicantIdByPropertyIdsByHistoricalDaysByApplicationStageStatusIdsByLeaseIntervalTypeIdsByCid( $intApplicantId, $arrintPropertyIds, $intHistoricalDays, $arrintApplicationStageStatusIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $arrintExcludeOccupancyTypes = [] ) {
		if( false == valArr( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return;
		if( false == valArr( $arrintLeaseIntervalTypeIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strSqlWhereOccupancyType = ( true == valArr( $arrintExcludeOccupancyTypes ) ) ? ' AND app.occupancy_type_id NOT IN ( ' . sqlintImplode( $arrintExcludeOccupancyTypes ) . ' ) ' : '';
		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN properties p ON ( ( p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.cid = ' . ( int ) $intCid . ') OR  ( p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )  AND p.cid = ' . ( int ) $intCid . '))
						LEFT JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN company_applications ca ON ( app.company_application_id = ca.id AND app.cid = ca.cid)
						LEFT JOIN property_applications pa ON ( ( ( pa.property_id = p.id AND pa.cid = p.cid )  OR ( pa.property_id = p.property_id AND pa.cid = p.cid ) ) AND pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						a.id::int = ' . ( int ) $intApplicantId . '
						AND a.cid::int = ' . ( int ) $intCid . '
						AND app.property_id = p.id
						AND app.cid = p.cid
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND
						(
								app.application_stage_id = ' . CApplicationStage::LEASE . '
								OR
								(
										( app.application_stage_id, app.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
										AND
										(
												ca.id IS NULL
												OR
												( pa.is_published = 1 AND pa.id IS NOT NULL )
										)
								)
						) ' . $strSqlWhereOccupancyType;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $intPropertyId = NULL, $boolFetchDeniedApplications = false, $boolIncludeDeletedAA = false, $intOccupancyTypeId = NULL ) {

		if( false == valArr( $arrintLeaseIntervalTypeIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) return;
		if( false == valArr( $arrintPropertyIds ) ) return;

		if( false == $boolFetchDeniedApplications ) {
		    $strFetchDeniedApplications = ' AND app.cancellation_list_type_id <> ' . CListType::LEAD_DENIAL_REASONS;
		}

		$strOccupancyTypeIdCheck = '';

		if( true == valId( $intOccupancyTypeId ) ) {
			$strOccupancyTypeIdCheck = 'AND app.occupancy_type_id = ' . $intOccupancyTypeId;
		}

	    $strFetchCancelledArchivedApplications = ' AND ( ( app.application_status_id IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) ' . $strFetchDeniedApplications . '
	    													AND CASE WHEN \'BLOCK_REOPEN_ALL_LEADS\' = 	pp.value THEN app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )

																	 WHEN \'BLOCK_REOPEN_APPLICATION\' = pp.value
																		THEN CASE WHEN app.application_stage_id = ' . CApplicationStage::APPLICATION . '
																			THEN app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
																		ELSE TRUE
																	 END

						                                             WHEN \'BLOCK_REOPEN_GUEST_CARD\' = pp.value
						                                             THEN CASE WHEN app.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '
						                                                       THEN app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						                                                       ELSE TRUE
						                                                  END
						                                              ELSE TRUE
						                                          END
				                                     	  ) OR app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
				                                     	)';

		$strCheckDeletedAASql		= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		// we need to load the application for which the applicant has already applied and order those - NRW
		if( 0 < $intPropertyId ) {

			$strSql = '	SELECT
							app.*,
							CASE WHEN ' . ( int ) $intPropertyId . ' = app.property_id THEN 1 ELSE 0 END AS display_order
						FROM
							applications app
							JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
							JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid )
							LEFT JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'BLOCK_AUTO_REOPENING_OF_CANCELLED_ARCHIVED_LEADS\' )
						WHERE
							a.id = ' . ( int ) $intApplicantId . '
							AND a.cid = ' . ( int ) $intCid . '
							AND app.combined_application_id IS NULL
							AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::FUTURE . ',' . CLeaseStatusType::CURRENT . ',' . CLeaseStatusType::NOTICE . ')
							AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strFetchCancelledArchivedApplications . '
							AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
							AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							' . $strOccupancyTypeIdCheck . '
						ORDER BY display_order DESC, app.created_on DESC LIMIT 1';

		} else {

			$strSql = '	SELECT
							app.*
						FROM
							applications app
							JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
							JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid )
							LEFT JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'BLOCK_AUTO_REOPENING_OF_CANCELLED_ARCHIVED_LEADS\' )
						WHERE
								a.id = ' . ( int ) $intApplicantId . '
							AND	a.cid = ' . ( int ) $intCid . '
							AND app.combined_application_id IS NULL
							AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strFetchCancelledArchivedApplications . '
							AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
							AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							' . $strOccupancyTypeIdCheck . '
						ORDER BY app.created_on DESC LIMIT 1';
		}

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintPropertyIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.applicant_id = ' . ( int ) $intApplicantId . $strCheckDeletedAASql . ' )
						JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN lease_intervals li ON ( li.cid = app.cid and li.id = app.lease_interval_id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					ORDER BY app.created_on DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsCountByApplicantIdByFullSitePropertyIdsByCid( $intApplicantId, $arrintFullSitePropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( array_filter( $arrintFullSitePropertyIds ) ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						COUNT( app.id )
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.applicant_id = ' . ( int ) $intApplicantId . $strCheckDeletedAASql . ' )
						JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ( p.id IN ( ' . implode( ',', $arrintFullSitePropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintFullSitePropertyIds ) . ' ) )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchApplicationByIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*,
						aa.id as applicant_application_id
					FROM
						applications app,
						applicants ap,
						applicant_applications aa
					WHERE
						app.id = aa.application_id
						AND app.cid = aa.cid
						AND ap.id = aa.applicant_id
						AND ap.cid = aa.cid
						AND ap.id::int = ' . ( int ) $intApplicantId . '
						AND app.id = ' . ( int ) $intApplicationId . '
						AND app.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicantIdByPropertyIdByCid( $intApplicantId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN applicants a ON ( a.id = app.primary_applicant_id )
					WHERE
						a.id = ' . ( int ) $intApplicantId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
	                    app.created_on DESC                        
                    LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app,
						applicants appt,
						applicant_applications aa
					WHERE
						app.cid = aa.cid
						AND app.id = aa.application_id
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.customer_id::int = ' . ( int ) $intCustomerId . '
						AND app.cid::int = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						ORDER BY id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.lease_id = app.lease_id AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id::int = ' . ( int ) $intLeaseId . '
						AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
						ORDER BY id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicationsCountByPropertyIdsByApplicationFilterByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrstrWhere = [];

		$boolJoinApplicantSql 			= false;
		$boolJoinPropertyFloorplanSql	= false;
		$boolJoinPropertyUnitSql		= false;
		$boolIsRenewal					= false;
		$boolJoinEvents					= false;

		if( true == valArr( $arrintLeaseIntervalTypeIds ) && true == in_array( CLeaseIntervalType::RENEWAL, $arrintLeaseIntervalTypeIds ) ) {
			$boolIsRenewal = true;
		}
		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) && ( 'Find a Lead ...' != trim( $objApplicationsFilter->getQuickSearch() ) ) ) {

			$boolJoinApplicantSql = true;

			$arrstrLeadNames = explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );

			if( true == valArr( $arrstrLeadNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrLeadNames ) ) {
				foreach( $arrstrLeadNames as $strLeadName ) {
					$strName = addslashes( $strLeadName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$strCondition = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName = array_pop( $arrstrLeadNames );
				$strCondition = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( app.id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR lower( appt.phone_number ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}

			$arrstrWhere[] = $strCondition;
		}

		if( true == is_null( $objApplicationsFilter->getPageNo() ) && true == is_null( $objApplicationsFilter->getCountPerPage() ) && false == is_null( $objApplicationsFilter->getLeadIds() ) ) {
			$arrstrWhere[] = 'app.id IN ( ' . $objApplicationsFilter->getLeadIds() . ' ) ';// used only for selected downloads
		}

		if( false == is_null( $objApplicationsFilter->getExcludingApplicationIds() ) ) {
			$arrstrWhere[] = 'app.id NOT IN ( ' . implode( ',', $objApplicationsFilter->getExcludingApplicationIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationsFilter->getApplicationId() ) ) {
			$arrstrWhere[] = 'app.id = ' . ( int ) $objApplicationsFilter->getApplicationId();
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		} else {
			$arrstrWhere[] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 99 != $objApplicationsFilter->getApplicationStageStatuses() ) {
			$strApplicationStageStatuses = trim( $objApplicationsFilter->getApplicationStageStatuses() );
			if( false == empty( $strApplicationStageStatuses ) ) {
				$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
			} elseif( true == is_null( $objApplicationsFilter->getApplicationId() ) ) {
				$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
			}
		}
		$strLeadSources = trim( $objApplicationsFilter->getLeadSources() );
		if( false == empty( $strLeadSources ) ) {
			$arrstrWhere[] = 'app.lead_source_id IN ( ' . $objApplicationsFilter->getLeadSources() . ' ) ';
		}
		$strPsProducts = trim( $objApplicationsFilter->getPsProducts() );
		if( false == empty( $strPsProducts ) ) {
			$arrstrWhere[] = 'app.ps_product_id IN ( ' . $objApplicationsFilter->getPsProducts() . ' ) ';
		}

		// We may require this code in future when we enhance the search leads .
		$strLastContactMinDays = trim( $objApplicationsFilter->getLastContactMinDays() );
		if( false == empty( $strLastContactMinDays ) && true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) {
			$boolJoinEvents = true;
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
		}

		$strLastContactMaxDays = trim( $objApplicationsFilter->getLastContactMaxDays() );
		if( false == empty( $strLastContactMaxDays ) && true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) {
			$boolJoinEvents = true;
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
		}
		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );

		if( true == $objApplicationsFilter->getConsiderLeasingAgent() ) {

			if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
			} elseif( false == empty( $strCompanyEmployees ) ) {
				$arrstrWhere[] = ' app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
			} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( app.leasing_agent_id IS NULL ) ';
			}
		}

		if( false == is_null( $objApplicationsFilter->getMoveInDateMin() ) && false == is_null( $objApplicationsFilter->getMoveInDateMax() ) && 'Min' != $objApplicationsFilter->getMoveInDateMin() && 'Max' != $objApplicationsFilter->getMoveInDateMax() ) {
			$intMoveInDateMin = strtotime( $objApplicationsFilter->getMoveInDateMin() );
			$intMoveInDateMax = strtotime( $objApplicationsFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrstrWhere[] = 'li.lease_start_date >= \'' . date( 'Y-m-d', $intMoveInDateMin ) . '\'';
				$arrstrWhere[] = 'li.lease_start_date <= \'' . date( 'Y-m-d', $intMoveInDateMax ) . '\'';
			}
		}
		$strDesiredBedrooms = trim( $objApplicationsFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {

			$boolJoinPropertyFloorplanSql = true;

			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' pfp.number_of_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'pfp.number_of_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;

		}
		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {

			$boolJoinPropertyFloorplanSql = true;

			$boolGreaterThanSix 	= false;
			$arrintDesiredBathrooms	= [];
			$arrintDesiredBathrooms = explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array then we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' pfp.number_of_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'pfp.number_of_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		// Removing join from property_units because we are not getting min/max rent from property units table.
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		if( false == empty( $strDesiredRentMax ) && $strDesiredRentMin < $strDesiredRentMax ) {
			$arrstrWhere[] = 'app.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'app.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		} elseif( false == empty( $strDesiredRentMin ) && true == empty( $strDesiredRentMax ) ) {
			$arrstrWhere[] = 'app.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		} elseif( $strDesiredRentMin != 0 && $strDesiredRentMin == $strDesiredRentMax ) {
			$arrstrWhere[] = 'app.desired_rent_min = ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'app.desired_rent_max = ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		}
		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$boolJoinPropertyUnitSql = true;

			$arrstrWhere[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$strPropertyFloorplans = trim( $objApplicationsFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhere[] = 'app.property_floorplan_id IN ( ' . $objApplicationsFilter->getPropertyFloorplans() . ' ) ';
		}
		$intUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$boolJoinPropertyUnitSql = true;

			$arrstrWhere[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.app_remote_primary_key is NOT NULL';
				} elseif( CLeaseIntervalType::GUEST_CARD == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.guest_remote_primary_key is NOT NULL';
				} elseif( NULL == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = '( app.app_remote_primary_key is NOT NULL OR app.guest_remote_primary_key is NOT NULL )';
				}
			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.app_remote_primary_key is NULL AND app.guest_remote_primary_key is NULL';
				} elseif( CLeaseIntervalType::GUEST_CARD == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.guest_remote_primary_key is NULL';
				} else {
					$arrstrWhere[] = '( app.app_remote_primary_key is NULL AND app.guest_remote_primary_key is NULL )';
				}
			}
		}

		if( false == is_null( $objApplicationsFilter->getApplicationStartDate() ) && false == is_null( $objApplicationsFilter->getApplicationEndDate() ) && 'Min' != $objApplicationsFilter->getApplicationStartDate() && 'Max' != $objApplicationsFilter->getApplicationEndDate() ) {
			$intApplicationStartDate = strtotime( $objApplicationsFilter->getApplicationStartDate() );
			$intApplicationEndDate = strtotime( $objApplicationsFilter->getApplicationEndDate() );
			if( $intApplicationStartDate <= $intApplicationEndDate ) {
				$arrstrWhere[] = 'app.application_datetime >= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationStartDate() ) ) . ' 00:00:00\'';
				$arrstrWhere[] = 'app.application_datetime <= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationEndDate() ) ) . ' 23:59:59\'';
			}
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'app.application_completed_on IS NOT NULL';
		}

		if( false == is_null( $objApplicationsFilter->getCompanyapplicationIds() ) ) {
			$arrstrWhere[] = 'app.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';
		}

		$strJoinSql = '';
		if( true == $boolJoinApplicantSql || true == $objApplicationsFilter->getIsFollowup() ) {
			$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

			$strJoinSql .= ' INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
							INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';
		}

		if( true == $boolJoinApplicantSql && true == $boolIsRenewal ) {
			$strJoinSql	.= ' LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid ) ';
		}

		if( true == $boolJoinPropertyFloorplanSql ) {
			$strJoinSql .= ' LEFT JOIN property_floorplans pfp ON ( pfp.id = app.property_floorplan_id AND pfp.cid = app.cid ' . $strCheckDeletedFloorPlansSql . ' ) ';
		}

		if( true == $boolJoinPropertyUnitSql ) {
			$strJoinSql .= ' LEFT JOIN property_units pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' ) ';
		}

		// There's no reason to
		if( true == $boolJoinEvents ) {
			$strJoinSql .= ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = app.last_event_id AND e.cid = app.cid ) ';
		}

		$strSql = '	SELECT
						COUNT( app.id )
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li.lease_interval_type_id)'
						. $strJoinSql . '
						WHERE
							app.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 		 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql 	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesAsSql	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND deleted_on IS NULL' : '';

		// -1 means outside the core query, 1 means inside the core query, and 0 means no join at all
		$intJoinLeases = ( true == in_array( CLeaseIntervalType::RENEWAL, ( array ) $arrintLeaseIntervalTypeIds ) ) ? - 1 : 0;
		$intJoinLeases = ( - 1 == $intJoinLeases && 'lease_end_date' == $objApplicationsFilter->getSortBy() ) ? 1 : - 1;

		$intJoinPropertyUnits = - 1;
		$intJoinPropertyFloorplans = 0;
		$intJoinEvents = ( true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && ( true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) || true == in_array( CPsProduct::ENTRATA, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) ) ? - 1 : 0;

		$arrstrWhere = [];

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationsFilter->getPageNo() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objApplicationsFilter->getCountPerPage();
		}

		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );

		if( false == empty( $strQuickSearch ) ) {
			$arrstrLeadNames = explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );

			if( true == valArr( $arrstrLeadNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrLeadNames ) ) {
				foreach( $arrstrLeadNames as $strLeadName ) {
					$strName = addslashes( $strLeadName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$strCondition = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName = array_pop( $arrstrLeadNames );
				$strCondition = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( app.id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR appt.phone_number LIKE \'%' . addslashes( $strName ) . '%\' OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( app.lease_id AS text ) LIKE ( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}

			$arrstrWhere[] = $strCondition;
		}

		if( true == is_null( $objApplicationsFilter->getPageNo() ) && true == is_null( $objApplicationsFilter->getCountPerPage() ) && false == is_null( $objApplicationsFilter->getLeadIds() ) ) {
			$arrstrWhere[] = 'app.id IN ( ' . $objApplicationsFilter->getLeadIds() . ' ) ';// used only for selected downloads
		}

		if( true == is_numeric( $objApplicationsFilter->getApplicationId() ) ) {
			$arrstrWhere[] = 'app.id = ' . ( int ) $objApplicationsFilter->getApplicationId();
		}

		if( false == is_null( $objApplicationsFilter->getExcludingApplicationIds() ) ) {
			$arrstrWhere[] = 'app.id NOT IN ( ' . implode( ',', $objApplicationsFilter->getExcludingApplicationIds() ) . ' ) ';
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
			$strEventProperties = $objApplicationsFilter->getProperties();
		} else {
			$arrstrWhere[] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
			$strEventProperties = implode( ',', $arrintPropertyIds );
		}

		if( ( true == is_null( $objApplicationsFilter->getQuickSearch() ) || false == is_numeric( $objApplicationsFilter->getQuickSearch() ) ) && 99 != ( int ) $objApplicationsFilter->getApplicationStageStatuses() ) {
			$strApplicationStageStatuses = trim( $objApplicationsFilter->getApplicationStageStatuses() );

			if( true == $objApplicationsFilter->getIsPartiallyGeneratedLease() ) {

				$strPartiallyGeneratedLeaseWhere = '( (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ] ] ) . ' ) ';
				$strPartiallyGeneratedLeaseWhere .= ' OR ( (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' ) ';
				$strPartiallyGeneratedLeaseWhere .= ' AND 0 < ( SELECT count ( fa.id ) FROM file_associations fa JOIN files f ON ( fa.file_id = f.id AND f.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND f.file_upload_date IS NULL ) JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'LEASE\') WHERE fa.application_id = app.id ) )';
				$strPartiallyGeneratedLeaseWhere .= ' )';
				$arrstrWhere[] = $strPartiallyGeneratedLeaseWhere;
			} elseif( false == empty( $strApplicationStageStatuses ) ) {
				$arrstrWhere[] = 'ass.id IN ( ' . $objApplicationsFilter->getApplicationStageStatuses() . ' ) ';
			} elseif( true == is_null( $objApplicationsFilter->getApplicationId() ) ) {
				$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
			}
		}
		$strLeadSources = trim( $objApplicationsFilter->getLeadSources() );
		if( false == empty( $strLeadSources ) ) {
			$arrstrWhere[] = 'app.lead_source_id IN ( ' . $objApplicationsFilter->getLeadSources() . ' ) ';
		}
		$strPsProducts = trim( $objApplicationsFilter->getPsProducts() );
		if( false == empty( $strPsProducts ) ) {
			$arrstrWhere[] = 'app.ps_product_id IN ( ' . $objApplicationsFilter->getPsProducts() . ' ) ';
		}

		// We may require this code in future when we enhance the search leads .
		$strLastContactMinDays = trim( $objApplicationsFilter->getLastContactMinDays() );
		if( false == empty( $strLastContactMinDays ) ) {
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
			$intJoinEvents = 1;
		}

		$strLastContactMaxDays = trim( $objApplicationsFilter->getLastContactMaxDays() );
		if( false == empty( $strLastContactMaxDays ) ) {
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
			$intJoinEvents = 1;
		}

		if( 'contact_days' == $objApplicationsFilter->getSortBy() ) {
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getMoveInDateMin() ) && false == is_null( $objApplicationsFilter->getMoveInDateMax() ) && 'Min' != $objApplicationsFilter->getMoveInDateMin() && 'Max' != $objApplicationsFilter->getMoveInDateMax() ) {
			$intMoveInDateMin = strtotime( $objApplicationsFilter->getMoveInDateMin() );
			$intMoveInDateMax = strtotime( $objApplicationsFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrstrWhere[] = 'li_app.lease_start_date >= \'' . date( 'Y-m-d', $intMoveInDateMin ) . '\'';
				$arrstrWhere[] = 'li_app.lease_start_date <= \'' . date( 'Y-m-d', $intMoveInDateMax ) . '\'';
			}
		}

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );

		if( true == $objApplicationsFilter->getConsiderLeasingAgent() ) {

			if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
			} elseif( false == empty( $strCompanyEmployees ) ) {
				$arrstrWhere[] = ' app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
			} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( app.leasing_agent_id IS NULL ) ';
			}
		}

		$strDesiredBedrooms = trim( $objApplicationsFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {

			$intJoinPropertyFloorplans 		= 1;
			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' app.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' app.desired_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {

			$intJoinPropertyFloorplans 	= 1;
			$boolGreaterThanSix 		= false;
			$arrintDesiredBathrooms		= [];
			$arrintDesiredBathrooms 	= explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array the we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' app.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'app.desired_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		// Removing join from property_units table because we are not getting min/max rent from property units table.
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		if( false == empty( $strDesiredRentMax ) && $strDesiredRentMin < $strDesiredRentMax ) {
			$arrstrWhere[] = 'app.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'app.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		} elseif( false == empty( $strDesiredRentMin ) && true == empty( $strDesiredRentMax ) ) {
			$arrstrWhere[] = 'app.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		} elseif( $strDesiredRentMin != 0 && $strDesiredRentMin == $strDesiredRentMax ) {
			$arrstrWhere[] = 'app.desired_rent_min = ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'app.desired_rent_max = ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		}
		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhere[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$strPropertyFloorplans = trim( $objApplicationsFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhere[] = 'app.property_floorplan_id IN ( ' . $objApplicationsFilter->getPropertyFloorplans() . ' ) ';
		}
		$strUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $strUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhere[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li_app.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'li_app.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.app_remote_primary_key is NOT NULL';
				} elseif( CLeaseIntervalType::GUEST_CARD == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.guest_remote_primary_key is NOT NULL';
				} elseif( NULL == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = '( app.app_remote_primary_key is NOT NULL OR app.guest_remote_primary_key is NOT NULL )';
				}
			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.app_remote_primary_key is NULL AND app.guest_remote_primary_key is NULL';
				} elseif( CLeaseIntervalType::GUEST_CARD == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'app.guest_remote_primary_key is NULL';
				} else {
					$arrstrWhere[] = '( app.app_remote_primary_key is NULL AND app.guest_remote_primary_key is NULL )';
				}
			}
		}

		if( false == is_null( $objApplicationsFilter->getApplicationStartDate() ) && false == is_null( $objApplicationsFilter->getApplicationEndDate() ) && 'Min' != $objApplicationsFilter->getApplicationStartDate() && 'Max' != $objApplicationsFilter->getApplicationEndDate() ) {
			$intApplicationStartDate = strtotime( $objApplicationsFilter->getApplicationStartDate() );
			$intApplicationEndDate = strtotime( $objApplicationsFilter->getApplicationEndDate() );
			if( $intApplicationStartDate <= $intApplicationEndDate ) {
				$arrstrWhere[] = 'app.application_datetime >= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationStartDate() ) ) . ' 00:00:00\'';
				$arrstrWhere[] = 'app.application_datetime <= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationEndDate() ) ) . ' 23:59:59\'';

			} elseif( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {

				$objApplicationsFilter->validate( 'validate_dates' );

				$objApplicationsFilter->changeDateFormat();
			}
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'app.application_completed_on IS NOT NULL';
		}

		if( true == valArr( $objApplicationsFilter->getApplicationIds() ) ) {
			$arrstrWhere[] = 'app.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationIds() ) . ')';
		}

		if( false == is_null( $objApplicationsFilter->getCompanyapplicationIds() ) ) {
			$arrstrWhere[] = 'app.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';
		}

		$strEventsJoinSql = ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = app.last_event_id AND e.cid = app.cid ) ';

		$strEventFields = 'e.id as event_id,
							e.event_datetime,
							e.event_type_id,
							DATE_TRUNC(\'day\',  NOW() - e.event_datetime ) as last_contact_days,';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*,
						CASE WHEN us.space_number IS NOT NULL AND ( 1 < ( SELECT COUNT(id) FROM unit_spaces WHERE property_unit_id = app.property_unit_id AND cid = app.cid  AND cid = ' . ( int ) $intCid . $strCheckDeletedUnitSpacesAsSql . ' ) )
							THEN pu.unit_number || \'-\' || us.space_number
							ELSE pu.unit_number
						END AS unit_number,
						' . ( ( -1 == $intJoinEvents ) ? $strEventFields : '' ) . '
						' . ( ( 1 == $intJoinLeases ) ? 'app.lease_end_date,' : '' ) . '
						' . ( ( -1 == $intJoinLeases ) ? 'li.lease_end_date,' : '' ) . '

						CASE
							WHEN TRUE = app.is_pet_policy_conflicted
							AND ( 0 = (	SELECT
												count(pap.id)
											FROM
												property_application_preferences AS pap
											WHERE
												pap.cid = ' . ( int ) $intCid . '
												AND pap.property_id = app.property_id
												AND pap.company_application_id = app.company_application_id
												AND pap.cid = app.cid
												AND pap.key = \'HIDE_OPTION_PET\' LIMIT 1 ) ) THEN 1 ELSE 0
						END AS is_pet_policy_conflicted
					FROM
						(
							SELECT
								appt.id AS applicant_id,
								appt.name_last,
								appt.name_first,
								appt.phone_number,
								appt.mobile_number,
								appt.work_number,
								appt.username as email_address,
								app.id as id,
								app.cid,
								app.property_id,
								app.company_application_id,
								app.application_datetime,
								app.property_floorplan_id,
								app.property_unit_id,
								app.unit_space_id,
								app.lead_source_id,
								app.leasing_agent_id,
								app.application_stage_id,
								app.application_status_id,
								app.last_event_id,
								app.is_pet_policy_conflicted,
								app.lease_id,
								app.ps_product_id,
								app.screening_id,
								app.guest_remote_primary_key,
								app.app_remote_primary_key,
								app.created_on,
								app.application_completed_on,
								app.lease_completed_on,
								app.application_approved_on,
								aa.application_step_id,
								aa.application_document_id,
								' . ( ( 1 == $intJoinEvents ) ? $strEventFields : '' ) . '
								' . ( ( 1 == $intJoinLeases ) ? 'li.lease_end_date as lease_end_date,' : '' ) . '
								aa.application_form_id,
								aa.lease_generated_on,
								CASE
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::HOME . ' THEN appt.phone_number
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' THEN appt.work_number
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' THEN appt.mobile_number
								END AS primary_phone_number,
								ad.primary_phone_number_type_id';

		$strSql .= ' FROM
						applications app
						JOIN lease_intervals li_app ON ( li_app.cid = app.cid AND li_app.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN applicant_details ad ON ( ad.cid = appt.cid AND ad.applicant_id = appt.id ) ';

		$strSql .= ( 1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid )
											LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( 1 == $intJoinPropertyUnits ) ? ' LEFT JOIN property_units pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' ) ' : '';
		$strSql .= ( 1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE app.cid = ' . ( int ) $intCid;
		if( 0 == $intJoinLeases ) {
			$strSql .= ' AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'name'						=> 'appt.name_last || appt.name_first',
			'status'					=> 'app.application_status_id',
			'property'					=> 'app.property_id',
			'source'					=> 'app.lead_source_id',
			'application_datetime'		=> 'app.application_datetime',
			'application_approved_on'	=> 'app.application_approved_on',
			'application_completed_on'	=> 'app.application_completed_on',
			'lease_completed_on'		=> 'app.lease_completed_on',
			'lease_interval_type'		=> 'li_app.lease_interval_type_id',
			'lease_end_date'			=> 'li.lease_end_date',
			'contact_days'				=> 'last_contact_days'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'app.application_datetime' );

		$strSortDirection = ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';

		$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql .= ' ) AS app
					LEFT JOIN unit_spaces us ON ( us.id = app.unit_space_id AND us.cid = app.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					LEFT JOIN property_units pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' ) ';

		$strSql .= ( -1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid )
												LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( -1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusWiseOpenApplicationCountByPropertyIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

		$strSql = '	SELECT
						ass.id as application_stage_status_id,
						count(app.id)
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON( li.lease_interval_type_id = ass.lease_interval_type_id AND app.application_stage_id = ass.application_stage_id AND app.application_status_id =ass.application_status_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND ass.id NOT IN ( ' . implode( ',', CApplicationStageStatus::$c_arrintNonActiveApplicationStageStatusIds ) . ' ) ' . $strCompanyEmployeeSql . '
					GROUP BY
						ass.id';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrintApplicationStatuesTypesData = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrintApplicationStatuesTypesData[$arrmixValue['application_stage_status_id']] = $arrmixValue['count'];
			}
		}

		return $arrintApplicationStatuesTypesData;
	}

	public static function fetchEventStatisticsByPropertyIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

		$strSql = '	SELECT
						count(id) as total_lead_count,
						sum ( is_lease_approved ) as lease_approved_count,
						sum ( has_site_visit ) as site_visit_count,
						( sum ( time_to_first_contact ) / count(id)) as time_to_first_contact,
						sum ( is_contacted ) as contacted_count
					FROM (
							SELECT
								app.id,
								EXTRACT( EPOCH FROM COALESCE( first_contact.event_datetime, NOW() ) - app.application_datetime ) AS time_to_first_contact,
								CASE
									WHEN first_site_visit.lease_interval_id IS NOT NULL
									THEN 1
									ELSE 0
								END AS has_site_visit,
								CASE
									WHEN ' . CApplicationStage::LEASE . '  = app.application_stage_id AND ' . CApplicationStatus::STARTED . '  = app.application_status_id
									THEN 1
									ELSE 0
								END AS is_lease_approved,
								CASE
									WHEN first_contact.lease_interval_id IS NOT NULL
									THEN 1
									ELSE 0
								END AS is_contacted
							FROM
								applications app
								LEFT JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
								LEFT JOIN (
											SELECT
												e.event_datetime,
												e.lease_interval_id,
												e.cid,
												RANK() OVER( PARTITION BY e.lease_interval_id ORDER BY e.event_datetime ASC )
											FROM
												events e
											WHERE
												e.cid = ' . ( int ) $intCid . '
												AND e.event_datetime > ( CURRENT_DATE - 31 )
												AND e.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
												AND e.lease_interval_id IS NOT NULL
												AND e.event_type_id IN ( ' . implode( ',', [ CEventType::EMAIL_OUTGOING, CEventType::CALL_OUTGOING, CEventType::SMS_OUTGOING, CEventType::ONLINE_CHAT, CEventType::ONSITE_VISIT ] ) . ' )
								) first_contact ON ( first_contact.lease_interval_id = li.id AND first_contact.cid = app.cid AND first_contact.rank = 1 )
								LEFT JOIN (
											SELECT
												e.event_datetime,
												e.lease_interval_id,
												e.cid,
												RANK() OVER ( PARTITION BY e.lease_interval_id ORDER BY e.event_datetime ASC )
											FROM
												events e
											WHERE
												e.cid = ' . ( int ) $intCid . '
												AND e.event_datetime > ( CURRENT_DATE - 31 )
												AND e.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
												AND e.lease_interval_id IS NOT NULL
												AND e.event_type_id = ' . CEventType::ONSITE_VISIT . '
											) first_site_visit ON ( first_site_visit.lease_interval_id = li.id AND first_site_visit.cid = app.cid AND first_site_visit.rank = 1 )
							WHERE
								app.cid = ' . ( int ) $intCid . '
								AND app.application_datetime > ( CURRENT_DATE - 31 )
								AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strCompanyEmployeeSql . '
						) as sub';
		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrmixEventStatisticsData = [];

		$arrmixEventStatisticsData['total_lead_count'] 				= ( true == isset ( $arrmixValues[0]['total_lead_count'] ) ) ? $arrmixValues[0]['total_lead_count'] : 0;
		$arrmixEventStatisticsData['lease_approved_count'] 			= ( true == isset ( $arrmixValues[0]['lease_approved_count'] ) ) ? $arrmixValues[0]['lease_approved_count'] : 0;
		$arrmixEventStatisticsData['site_visit_count'] 				= ( true == isset ( $arrmixValues[0]['site_visit_count'] ) ) ? $arrmixValues[0]['site_visit_count'] : 0;
		$arrmixEventStatisticsData['contacted_count'] 				= ( true == isset ( $arrmixValues[0]['contacted_count'] ) ) ? $arrmixValues[0]['contacted_count'] : 0;

		if( true == is_numeric( $arrmixValues[0]['time_to_first_contact'] ) ) {
			$intDays = 0;
			$intHours = ( $arrmixValues[0]['time_to_first_contact'] / 3600 );
			if( $intHours > 23 ) {
				$intDays = $intHours / 24;
				$intHours = $intHours % 24;
			}

			$strDaysLabel = ( 1 == $intDays ) ? ' Day ' : ' Days ';
			$strHoursLabel = ( 1 == $intHours ) ? ' Hour ' : ' Hours ';

			if( 0 == $intDays ) {
				$arrmixEventStatisticsData['time_to_first_contact'] = number_format( $intHours, 0 ) . 'h';
				$arrmixEventStatisticsData['time_to_first_contact_long'] = number_format( $intHours, 0 ) . $strHoursLabel;
			} else {
				$arrmixEventStatisticsData['time_to_first_contact'] = number_format( $intDays, 0 ) . 'd';
				$arrmixEventStatisticsData['time_to_first_contact_long'] = number_format( $intDays, 0 ) . $strDaysLabel . number_format( $intHours, 0 ) . $strHoursLabel;
			}
		} else {
			$arrmixEventStatisticsData['time_to_first_contact'] = NULL;
			$arrmixEventStatisticsData['time_to_first_contact_long'] = NULL;
		}

		return $arrmixEventStatisticsData;
	}

	public static function fetchApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						DISTINCT ( app.id ),
						appt.id as applicant_id,
						appt.name_last,
						appt.name_first,
						CASE WHEN cpn.phone_number_type_id = ' . CPhoneNumberType::HOME . ' THEN cpn.phone_number END  AS phone_number,
						app.property_id,
						app.property_unit_id,
						app.property_floorplan_id,
						app.originating_lead_source_id,
						app.converting_lead_source_id,
						app.leasing_agent_id,
						app.lease_id,
						app.primary_applicant_id,
						app.lease_interval_id,
						app.application_stage_id,
						app.application_status_id,
						app.guest_remote_primary_key,
						app.app_remote_primary_key,
						app.application_datetime,
						app.created_on,
						aa.application_step_id,
						aa.application_document_id,
						aa.application_form_id,
						aa.cid
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = app.cid )
						LEFT JOIN lease_intervals li ON ( li.cid = app.cid and li.id = app.lease_interval_id )
						LEFT JOIN customer_phone_numbers cpn ON ( appt.customer_id = cpn.customer_id AND appt.cid = cpn.cid AND cpn.deleted_by IS NULL AND cpn.deleted_on IS NULL )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id is NOT NULL
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )'
						. $strCheckDeletedAASql;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByIdsByCid( $intPropertyId, $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchFirstPrimaryApplicationByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						DISTINCT ( app.id ), appt.id as applicant_id, appt.name_last, appt.name_first
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid)
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						' . $strCheckDeletedAASql . '
					ORDER BY app.id DESC limit 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.id,app.created_on,aa.additional_info, acnt.id as applicant_id, app.lease_id, app.primary_applicant_id
					FROM
						applicants acnt
						JOIN applicant_applications aa ON ( acnt.id = aa.applicant_id AND acnt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE
						acnt.customer_id = ' . ( int ) $intCustomerId . '
						AND acnt.cid = ' . ( int ) $intCid . '
					ORDER BY aa.created_on DESC';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchLeaseAgreementIssuedApplicationByCustomerIdByPropertyIdsByCid( $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN file_associations fa ON( fa.application_id = app.id AND fa.applicant_id = appt.id AND fa.cid = app.cid )
						JOIN files f ON ( f.id = fa.file_id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' AND fa.cid = ft.cid )
						JOIN lease_intervals li ON ( app.cid = li.cid AND app.lease_id = li.lease_id AND li.id = app.lease_interval_id AND li.lease_status_type_id <> ' . CLeaseStatusType::PAST . ' )
					WHERE
						aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND appt.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ',' . CLeaseIntervalType::TRANSFER . ',' . CLeaseIntervalType::APPLICATION . ' )

						AND CASE WHEN app.application_stage_id = ' . CApplicationStage::LEASE . ' AND app.application_status_id = ' . CApplicationStatus::APPROVED . ' THEN
							app.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							true
						END
							ORDER BY app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchSimpleLeaseAgreementIssuedApplicationsByCustomerIdByPropertyIdsByCid( $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						app.id,
						app.cid,
						li.lease_interval_type_id,
						app.application_datetime,
						app.details
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND app.id = aa.application_id AND aa.deleted_on IS NULL )
						JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
						JOIN file_associations fa ON(  fa.cid = app.cid AND fa.application_id = app.id AND fa.applicant_id = appt.id )
						JOIN files f ON ( fa.cid = f.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( fa.cid = ft.cid AND ft.id = f.file_type_id AND ft.system_code = \'' . \CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
						JOIN lease_intervals li ON( app.cid = li.cid AND app.lease_id = li.lease_id AND li.id = app.lease_interval_id )
						JOIN file_signatures fs ON( fa.cid = fs.cid AND fa.id = fs.file_association_id )
					WHERE
						appt.cid = ' . ( int ) $intCid . '
						AND f.file_upload_date IS NOT NULL
						AND aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id IN ( ' . implode( ',', CLeaseIntervalType::$c_arrintLeaseSigningLeaseIntervalTypes ) . ' )
						AND li.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintLeaseSigningLeaseStatusTypes ) . ' )
						AND CASE WHEN app.application_stage_id = ' . \CApplicationStage::LEASE . ' AND app.application_status_id = ' . \CApplicationStatus::APPROVED . ' THEN
							app.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							TRUE
						END
						ORDER BY app.created_on DESC';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchUnexportedApplicationsByInternetListingServiceIdByApplicationDatetimeByCid( $intInternetListingServiceId, $strApplicationDatetime = NULL, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if ( false == is_numeric( $intInternetListingServiceId ) ) return NULL;
		if ( false == is_numeric( $intCid ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (app.id)
						app.*,
						aa.application_step_id
					FROM
						applications app
						LEFT OUTER JOIN export_records er ON ( app.id::int = er.reference_number::int AND app.cid::int = er.cid AND er.table_key = \'applications\' AND er.export_type_key = \'' . ( string ) $intInternetListingServiceId . '\' )
						LEFT OUTER JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid::int = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
					WHERE
						er.id IS NULL
						AND app.internet_listing_service_id::int = ' . ( int ) $intInternetListingServiceId . '::int
						AND app.cid::int = ' . ( int ) $intCid . '::int';
					if( false == is_null( $strApplicationDatetime ) ) {
							$strSql .= ' AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date = \'' . $strApplicationDatetime . '\'::date ';
					}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApartmentsComApplicationsByCid( $intPageNo, $intPageSize, $objDatabase, $objApplicationsFilter, $boolIncludeDeletedAA = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' WHERE aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT DISTINCT ON ( app.created_on, mc.company_name, app.id )
						app.*,
						appt.name_last,
						appt.name_first
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN clients mc ON ( mc.id = ' . ( int ) $objApplicationsFilter->getCid() . ' AND app.cid = mc.id )
					' . $strCheckDeletedAASql;

		$strSql .= ' ORDER BY
						app.created_on DESC,
						mc.company_name ASC,
						app.id ASC

						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApartmentsComApplicationsCountByCid( $objDatabase, $objApplicationsFilter, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' WHERE aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						count( apps.id )
					FROM
						applications apps
						JOIN applicant_applications aa ON ( apps.id = aa.application_id AND apps.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ')
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN clients mc ON ( mc.id = ' . ( int ) $objApplicationsFilter->getCid() . ' apps.cid = mc.id )
					' . $strCheckDeletedAASql;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchApplicationsByIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolIncludePrimaryApplicantDetails = false, $boolIncludeLeaseDocument = false, $strOrderBy = '', $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;
		$arrintApplicationIds = array_filter( $arrintApplicationIds );
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						app.*';

		if( true == $boolIncludePrimaryApplicantDetails ) {
			$strSql .= ' , appt.id as applicant_id
						, appt.name_last
						, appt.name_first
						, appt.username as email_address';
		}

		if( true == $boolIncludePrimaryApplicantDetails || true == $boolIncludeLeaseDocument ) {
			$strSql .= ' , aa.id AS applicant_application_id
						, aa.lease_document_id ';
		}

		$strSql .= ' FROM
						applications app ';

		if( true == $boolIncludePrimaryApplicantDetails || true == $boolIncludeLeaseDocument ) {
			$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

			$strSql .= ' INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' ) ';
		}

		if( true == $boolIncludePrimaryApplicantDetails ) {
			$strSql .= ' INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';
		}

		if( false !== \Psi\CStringService::singleton()->strpos( $strOrderBy, 'lease_interval_type_id' ) ) {
			$strSql .= 'JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leas_interval_id ) ';
		}

		$strSql .= ' WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ')';

		if( true == $boolIncludePrimaryApplicantDetails ) {
			if( 'name' == $strOrderBy ) {
				$strSql .= ' ORDER BY appt.name_first, appt.name_last';
			} elseif( 'name DESC' == $strOrderBy ) {
				$strSql .= ' ORDER BY name_last || name_first DESC';
			} elseif( 'name ASC' == $strOrderBy ) {
				$strSql .= ' ORDER BY name_last || name_first ASC';
			} elseif( false !== \Psi\CStringService::singleton()->strpos( $strOrderBy, 'lease_interval_type_id' ) ) {
				$strSql .= ' ORDER BY li.' . $strOrderBy;
			} elseif( false == empty( $strOrderBy ) ) {
				$strSql .= ' ORDER BY app. ' . $strOrderBy;
			} else {
				$strSql .= ' ORDER BY app.id DESC';
			}
		}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByIntegrationDatabaseIdByPropertyIdsByCid( $intIntegrationDatabaseId, $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN property_integration_databases pid ON( app.property_id = pid.property_id AND app.cid = pid.cid )
					WHERE
						pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND pid.cid = ' . ( int ) $intCid .
						( ( true == valArr( $arrintPropertyIds ) ) ? ' AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : '' );

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStatusCountsByPropertyIdsByLeasingAgentIdByCid( $arrintPropertyIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase, $intCurrentEmployeeId = NULL ) {

		if ( false == valArr( $arrintPropertyIds ) ) return 0;

		$arrintLeaseIntervalTypes = [ CLeaseIntervalType::APPLICATION ];

		$strSql = '	SELECT
						ass.id as application_stage_status_id,
						is_lease_partially_generated,
						count(app.id)
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leas_interval_id )
						JOIN application_stage_statuses ass ON( li.lease_interval_type_id = ass.lease_interval_type_id AND app.application_stage_id = ass.application_stage_id AND app.application_status_id =ass.application_status_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND (app.application_stage_id,app.application_status_id) NOT IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::CANCELLED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ] ] ) . ' )
						AND ( CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . ' THEN
									li.lease_interval_type_id in ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )
							ELSE
									li.lease_interval_type_id <> 7
							END ) ';

					if( false == valArr( $arrintCompanyEmployeeIds ) ) {

						$strSql .= ' AND( CASE WHEN ass.id NOT IN ( ' . CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED . ' )
										THEN leasing_agent_id IS NULL
										ELSE true
									END ) ';

					} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintCompanyEmployeeIds ) && false == is_null( $intCurrentEmployeeId ) && true == array_key_exists( $intCurrentEmployeeId, $arrintCompanyEmployeeIds ) ) {

						$strSql .= ' AND( CASE WHEN ass.id NOT IN ( ' . CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED . ' )
										THEN leasing_agent_id IS NULL OR leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
										ELSE true
									END ) ';
					} else {

						$strSql .= ' AND ( leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) OR leasing_agent_id IS NULL )';
					}

		$strSql .= ' GROUP BY
					ass.id, is_lease_partially_generated
				ORDER BY
					ass.id';

		$arrstrOpenApplicationCounts = fetchData( $strSql, $objDatabase );

		$arrstrFormattedOpenApplicationCounts = [];
		if( true == valArr( $arrstrOpenApplicationCounts ) ) {
			foreach( $arrstrOpenApplicationCounts as $arrstrOpenApplicationCount ) {

				if( 1 == $arrstrOpenApplicationCount['is_lease_partially_generated'] ) {
					$arrstrFormattedOpenApplicationCounts[CApplicationStageStatus::PROSPECT_STAGE3_APPROVED]['count'] += $arrstrOpenApplicationCount['count'];
					continue;
				}

				$arrstrFormattedOpenApplicationCounts[$arrstrOpenApplicationCount['application_stage_status_id']] = [ 'count' => $arrstrOpenApplicationCount['count'], 'status' => '' ];
			}
		}

		return $arrstrFormattedOpenApplicationCounts;
	}

	public static function fetchApplicationByPropertyIdByApplicantApplicationIdByCid( $intPropertyId, $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.id = ' . ( int ) $intApplicantApplicationId . ' AND app.cid = aa.cid AND aa.cid = ' . ( int ) $intCid . ' AND app.id = aa.application_id)
					WHERE
						app.property_id = ' . ( int ) $intPropertyId . $strCheckDeletedAASql;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicationsByApplicantApplicationIdsByPsProductIdByUserIdsByCids( $arrintApplicantApplicationIds, $intPsProductId, $arrintUserIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) || false == valId( $intPsProductId ) || false == valArr( $arrintUserIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						app.id,
						app.cid,
						app.property_id,
						app.created_by,
						aa.id AS applicant_application_id,
						app.lease_id,
						app.lease_generated_on
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid )
					WHERE
						aa.id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND aa.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND app.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND app.ps_product_id = ' . ( int ) $intPsProductId . '
						AND aa.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						property_id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse[0]['property_id'];
	}

	public static function fetchNonIntegratedApplicationsByPropertyIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $arrintPropertyIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leasE_interval_id )
					WHERE
						app.cid =' . ( int ) $intCid . '
						AND app.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						And li.lease_interval_type_id=' . ( int ) $intLeaseIntervalTypeId . '
						AND ( app.application_stage_id, app.application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app.app_remote_primary_key IS NULL
						AND app.created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedApplicationIdsByPropertyIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $arrintPropertyIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = ' SELECT
						app.property_unit_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leasE_interval_id )
					WHERE
						app.cid =' . ( int ) $intCid . '
						AND app.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						And li.lease_interval_type_id=' . ( int ) $intLeaseIntervalTypeId . '
						AND ( app.application_stage_id, app.application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app.app_remote_primary_key IS NULL
						AND app.created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		$arrmixApplicationIds = fetchData( $strSql, $objDatabase );

		$arrmixTempApplicationIds = [];

		if( true == valArr( $arrmixApplicationIds ) ) {
			foreach( $arrmixApplicationIds as $arrmixApplicationId ) {
				$arrmixTempApplicationIds[$arrmixApplicationId['property_unit_id']] = $arrmixApplicationId['property_unit_id'];
			}
		}

		return $arrmixTempApplicationIds;
	}

	public static function fetchNonIntegratedApplicationsByPropertyIdByPropertyUnitIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $intPropertyId, $intPropertyUnitId,  $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) ) return NULL;

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leasE_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.property_unit_id = ' . ( int ) $intPropertyUnitId . '
						And li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ( app.application_stage_id, app.application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app.app_remote_primary_key IS NULL
						AND app.created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolFetchDeniedApplications = true ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeniedApplications = ( false == $boolFetchDeniedApplications ) ? ' AND application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) ' : '';
		$strSql = '	SELECT
						*
					FROM
						applications
					WHERE
						id IN ( ' . implode( ',', $arrintApplicationIds ) . ')
						AND cid = ' . ( int ) $intCid . $strCheckDeniedApplications;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	/**
	 * @deprecated Deprecating this function as it would give wrong results because of id IN () AND cid IN () condition.
	 * It needs to be (id, cid) IN () - SRS
	 */
	public static function fetchCustomApplicationsByApplicationIdsByCids( $arrintApplicationIds, $arrintCids, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						applications
					WHERE
						id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchApplicationByLeadSourceIdByCid( $intLeadSourceId, $intCid, $objClientDatabase ) {
		$strSql = '	SELECT
						id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( originating_lead_source_id = ' . ( int ) $intLeadSourceId . ' OR converting_lead_source_id = ' . ( int ) $intLeadSourceId . ' )
					LIMIT 1';

		$arrmixApplications = fetchData( $strSql, $objClientDatabase );

		return ( 0 == \Psi\Libraries\UtilFunctions\count( $arrmixApplications ) ) ? false : true;
	}

	public static function fetchApplicationIdsByLeadSourceIdsByPropertyIdByCid( $arrintLeadSourceIds, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintLeadSourceIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						id,
						originating_lead_source_id,
						converting_lead_source_id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( originating_lead_source_id IN ( ' . implode( ',', $arrintLeadSourceIds ) . ' ) OR converting_lead_source_id IN ( ' . implode( ',', $arrintLeadSourceIds ) . ' )  )
						AND property_id = ' . ( int ) $intPropertyId;

		$arrmixApplications = fetchData( $strSql, $objClientDatabase );

		$arrintApplicationIdsKeyedByLeadSourceId = [];
		foreach( $arrmixApplications as $arrmixApplication ) {

			if( true == valStr( $arrmixApplication['originating_lead_source_id'] ) ) {
				$arrintApplicationIdsKeyedByLeadSourceId[$arrmixApplication['originating_lead_source_id']][$arrmixApplication['id']] = $arrmixApplication['id'];
			}

			if( true == valStr( $arrmixApplication['converting_lead_source_id'] ) ) {
				$arrintApplicationIdsKeyedByLeadSourceId[$arrmixApplication['converting_lead_source_id']][$arrmixApplication['id']] = $arrmixApplication['id'];
			}
		}

		return $arrintApplicationIdsKeyedByLeadSourceId;
	}

	public static function fetchApplicationCountByLeadSourceIdsByPropertyIdByCidKeyedByLeadSourceIds( $arrintLeadSourceIds, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintLeadSourceIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						count( a.id ),
						originating_lead_source_id,
						converting_lead_source_id
					FROM
						applications a
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND ( originating_lead_source_id IN ( ' . implode( ',', $arrintLeadSourceIds ) . ' ) OR converting_lead_source_id IN ( ' . implode( ',', $arrintLeadSourceIds ) . ' )  )
					GROUP BY
						originating_lead_source_id,
						converting_lead_source_id';

		$arrmixApplications = fetchData( $strSql, $objClientDatabase );

		$arrintApplicationCountKeyedByLeadSourceId = [];
		foreach( $arrmixApplications as $arrmixApplication ) {
			$arrintApplicationCountKeyedByLeadSourceId[$arrmixApplication['originating_lead_source_id']] += $arrmixApplication['count'];

			if( $arrmixApplication['originating_lead_source_id'] != $arrmixApplication['converting_lead_source_id'] ) {
				$arrintApplicationCountKeyedByLeadSourceId[$arrmixApplication['converting_lead_source_id']] += $arrmixApplication['count'];
			}
		}

		return $arrintApplicationCountKeyedByLeadSourceId;
	}

	public static function fetchApplicationsByUsernameByCid( $strUsername, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == empty( $strUsername ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						*
					FROM
						applicant_applications aa
						INNER JOIN applications app ON ( aa.cid = app.cid AND aa.application_id = app.id )
					WHERE
						aa.applicant_id = ( SELECT id FROM applicants WHERE lower(username) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC LIMIT 1 ) AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $intPropertyId = NULL, $arrintApplicationStatusIds = NULL ) {
		$strWhereSql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strWhereSql .= ( true == valId( $intPropertyId ) ) ? ' AND app.property_id = ' . ( int ) $intPropertyId : '';
		$strWhereSql .= ( true == valArr( $arrintApplicationStatusIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintApplicationStatusIds ) ) ? ' AND app.application_status_id IN ( ' . implode( ', ', $arrintApplicationStatusIds ) . ' ) ' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid)
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.id::int = ' . ( int ) $intApplicantId . ' )
					WHERE
						app.cid::int = ' . ( int ) $intCid . $strWhereSql . '
						ORDER BY app.id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationIdByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						app.id
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid)
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.id::int = ' . ( int ) $intApplicantId . ' )
					WHERE
						app.cid::int = ' . ( int ) $intCid . '
						ORDER BY app.id DESC LIMIT 1';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) ) return $arrmixData[0]['id'];

		return NULL;
	}

	public static function fetchApplicationsByCallIdsByCid( $arrintCallIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCallIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCallIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND call_id IS NOT NULL
						AND call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )';
		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchTotalGuestCardsCountByStatisticsEmailFilterByCid( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						app.property_id,
						app.originating_lead_source_id AS lead_source_id,
						app.ps_product_id,
						count ( DISTINCT( app.id )) lead_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
						AND app.ps_product_id = ' . CPsProduct::PROSPECT_PORTAL . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND app.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
						AND app.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						app.property_id,
						app.originating_lead_source_id,
						app.ps_product_id
					ORDER BY
						app.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalApplicationsCountByStatisticsEmailFilterByCid( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						app.property_id,
						app.application_stage_id,
						app.application_status_id,
						app.application_completed_on,
						count ( DISTINCT ( app.id ) ) as applications_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.leas_interval_id )
					WHERE
						app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id = ' . ( int ) $objStastisticsEmailFilter->getLeaseIntervalTypeId() . '
						AND app.ps_product_id = ' . CPsProduct::PROSPECT_PORTAL . '
						AND app.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
						AND app.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						app.property_id,
						app.application_stage_id,
						app.application_status_id,
						app.application_completed_on
					ORDER BY
						app.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalApplicationsCountByStatisticsEmailFilterByPsProductIdsByCid( $objStastisticsEmailFilter, $arrintPsProductIds, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						app.property_id,
						to_char(app.application_datetime, \'MM/DD/YYYY\') as xaxis,
						li.lease_interval_type_id,
						count ( DISTINCT ( app.id ) ) as application_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND app.internet_listing_service_id = ' . CInternetListingService::CRAIGS_LIST . '
						AND app.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND app.craigslist_post_id IS NOT NULL
						AND li.lease_interval_type_id IN ( ' . implode( ',', $objStastisticsEmailFilter->getLeaseIntervalTypeIds() ) . ' )
						AND app.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
						AND app.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						app.property_id,
						xaxis,
						li..lease_interval_type_id
					ORDER BY
						app.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByApplicationIdsByCallFilterByCid( $arrintApplicationIds, $objCallsFilter, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicationIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintApplicationIds ) ) return;

		$arrstrWhere 	= [];
		$strGenericData = trim( $objCallsFilter->getGenericData() );

		if( ( false == empty( $strGenericData ) ) && ( 'Find a Call ...' != trim( $objCallsFilter->getGenericData() ) ) ) {
			$arrstrApplicantNames = explode( ' ', trim( $objCallsFilter->getGenericData() ) );

			if( true == valArr( $arrstrApplicantNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrApplicantNames ) ) {
				foreach( $arrstrApplicantNames as $arrstrApplicantName ) {
					$strName					= addslashes( $arrstrApplicantName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$arrstrWhere[] = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName		= array_pop( $arrstrApplicantNames );
				$arrstrWhere[]	= '( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( app.call_id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR lower( cpn.phone_number ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						appt.id AS applicant_id, appt.name_last, appt.name_first, cpn.phone_number,
						app.id as id, app.property_id, app.property_unit_id,
				--app.lead_source_id,
				app.lease_id, app.ps_product_id, app.call_id,
						aa.id AS applicant_application_id
					FROM
						applications app
						INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
						INNER JOIN applicants appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = appt.customer_id AND cpn.cid = appt.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id IN (' . implode( ',', $arrintApplicationIds ) . ')
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND cpn.deleted_by IS NULL';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchApplications( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicantApplicationIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintApplicantApplicationIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						appt.id AS applicant_id, appt.name_last, appt.name_first, cpn.phone_number,
						app.id as id, app.property_id, app.property_unit_id,
				--app.lead_source_id,
				app.lease_id, app.ps_product_id, app.call_id,
						aa.id AS applicant_application_id
					FROM
						applications app
						INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
						INNER JOIN applicants appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = appt.customer_id AND cpn.cid = appt.cid AND cpn.phone_number_type_id = ' . CPhoneNumberType::HOME . ' )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND aa.id IN (' . implode( ',', $arrintApplicantApplicationIds ) . ')
						AND cpn.deleted_on IS NULL';

		return self::fetchApplications( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByCustomerIdByLeaseIdsByCid( $intCustomerId, $arrintLeaseIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintLeaseIds ) ) return false;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.customer_id = ' . ( int ) $intCustomerId . ' AND appt.cid = ' . ( int ) $intCid . ' )
					WHERE
						app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ( li.lease_interval_type_id, app.application_stage_id, app.application_status_id ) NOT IN ( ( ' . CLeaseIntervalType::TRANSFER . ' ,' . CApplicationStage::APPLICATION . ' , ' . CApplicationStatus::STARTED . ' ) )
						AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsIdsByCustomerIdByLeaseIdsByCid( $intCustomerId, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return false;

		$arrmixUnusedParameters = [ $intCustomerId ];

		$strSql = '	SELECT
						id
					FROM
						applications
					WHERE
						lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrintApplicationIds = ( array ) fetchData( $strSql, $objDatabase );
		$arrintNewApplicationIds = [];
		foreach( $arrintApplicationIds as $arrintApplicationId ) {
			$arrintNewApplicationIds[] = $arrintApplicationId['id'];
		}

		return $arrintNewApplicationIds;
	}

	public static function fetchApplicationsByIntegrationDatabaseIdByLeaseRemotePrimaryKeysByLeaseIntervalTypeIdsByLeaseStatusTypeIdsByCid( $intIntegrationDatabaseId, $arrstrLeaseRemotePrimaryKeys, $intCid, $arrintLeaseIntervalTypeIds, $arrintLeaseStatusTypeIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrstrLeaseRemotePrimaryKeys ) || false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintLeaseStatusTypeIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*,
						aa.offer_sent_on,
						l.remote_primary_key
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN integration_databases id ON ( app.cid = id.cid AND id.id = ' . ( int ) $intIntegrationDatabaseId . ' )
						JOIN leases l ON( l.id = app.lease_id AND l.cid = app.cid AND l.remote_primary_key IN( \'' . implode( "','", $arrstrLeaseRemotePrimaryKeys ) . '\' ) )
						JOIN lease_intervals li ON( li.id = app.lease_interval_id and li.cid = app.cid AND li.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) AND li.lease_interval_type_id IN( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) )
					WHERE
						app.cid = ' . ( int ) $intCid
						. $strCheckDeletedAASql;

		return CApplications::fetchApplications( $strSql, $objDatabase );
	}

	public static function loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds, $strCheckDeletedUnitsSql = '', $strCheckDeletedFloorPlansSql = '', $intCid = NULL, $boolIsSmsTextMessage = false ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrTableJoins = [
			'leases'							=> 'LEFT JOIN leases AS l ON ( l.id = app.lease_id AND l.cid = app.cid AND l.property_id = app.property_id )',
			'application_stage_statuses'		=> 'JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = li.lease_interval_type_id AND ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id )',
			'customer_pets' 				    => 'LEFT JOIN customer_pets cp ON ( cp.customer_id = appt.customer_id AND cp.cid = appt.cid )',
			'lease_associations'			    => 'LEFT JOIN lease_associations la ON ( cp.cid = la.cid AND la.lease_id = app.lease_id AND la.lease_interval_id = app.lease_interval_id AND la.ar_origin_id = ' . CArOrigin::PET . ' AND cp.id = la.ar_origin_object_id AND app.lease_id = la.lease_id AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . ' )',
			'pet_types'						    => 'LEFT JOIN pet_types pt ON ( pt.id = cp.pet_type_id AND pt.cid = cp.cid )',
			'events'							=> 'JOIN events AS e ON ( e.cid = app.cid AND e.lease_interval_id = app.lease_interval_id AND e.property_id = app.property_id ) ',
			'property_units'					=> 'LEFT JOIN ( SELECT pu.unit_number, pu.property_building_id, pu.property_floor_id, pu.id, pu.cid, pu.deleted_on FROM property_units pu WHERE pu.cid =' . ( int ) $intCid . ') AS pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' )',
			'property_buildings'				=> 'LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )',
			'company_employees'					=> 'LEFT JOIN company_employees AS ce ON ( ce.id = app.leasing_agent_id AND ce.cid = app.cid )',
			'property_floorplans'				=> 'LEFT JOIN property_floorplans AS pfp ON ( pfp.id = app.property_floorplan_id AND pfp.cid = app.cid ' . $strCheckDeletedFloorPlansSql . ' )',
			'scheduled_email_address_filters'	=> 'LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid AND seaf.cid = app.cid )',
			'event_results'						=> 'JOIN event_results er ON ( e.cid = er.cid AND e.event_result_id = er.id )'
		];

		$arrmixSqlFilters 	= [
			'select'	=> [],
			'join' 		=> [],
			'where' 	=> []
		];

		$strSqlCondition = '';
		$strProperties = trim( $objScheduledEmailFilter->getProperties() );

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrmixSqlFilters['where'][] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		} elseif( true == valStr( $strProperties ) ) {
			$arrmixSqlFilters['where'][] = 'app.property_id IN ( ' . trim( $objScheduledEmailFilter->getProperties(), ',' ) . ' )';
		}

		$strStatusTypes = trim( $objScheduledEmailFilter->getStatusTypes() );
		if( true == valStr( $strStatusTypes ) && 99 != ( int ) $strStatusTypes ) {
			$arrmixSqlFilters['where'][]							= 'ass.id IN ( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		} elseif( 99 != ( int ) $strStatusTypes ) {
			$arrmixSqlFilters['where'][]							= 'ass.id IN ( ' . implode( ',', $objScheduledEmailFilter->getAllActiveStatusTypeIds() ) . ' )';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		} elseif( 99 == ( int ) $strStatusTypes ) {
			$strSql = 'SELECT id FROM application_stage_statuses WHERE application_stage_id IN ( ' . CApplicationStage::LEASE . ' ) AND application_status_id IN ( ' . CApplicationStatus::APPROVED . ' ) AND lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION;
			$arrmixSqlFilters['where'][]							= 'ass.id NOT IN ( ' . $strSql . ' )';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		}

		if( true == valId( $objScheduledEmailFilter->getIsExcludeRecipient() ) && 1 == $objScheduledEmailFilter->getIsExcludeRecipient() ) {
			$arrmixSqlFilters['where'][] = 'appt.username IS NOT NULL';
		}

		if( true == valStr( $objScheduledEmailFilter->getLocaleCode() ) ) {
			$strLocaleCode = $objScheduledEmailFilter->getLocaleCode();
			$arrmixSqlFilters['where'][] = 'c.preferred_locale_code = \'' . $strLocaleCode . '\'';
		}

		// Building filter
		$arrmixSqlFilters['join']['property_units']		= $arrstrTableJoins['property_units'];

		$strBuildings = trim( $objScheduledEmailFilter->getPropertyBuildings() );
		if( true == valStr( $strBuildings ) ) {
			$arrmixSqlFilters['select'][] = 'pb.building_name';
			$arrmixSqlFilters['where'][] = 'pb.id IN ( ' . $objScheduledEmailFilter->getPropertyBuildings() . ' ) ';
			$arrmixSqlFilters['join']['property_buildings'] = $arrstrTableJoins['property_buildings'];
		}

		$strLeasingAgents = trim( $objScheduledEmailFilter->getLeasingAgents() );
		if( true == valStr( $strLeasingAgents ) ) {
			$arrmixSqlFilters['where'][] = 'app.leasing_agent_id IN ( ' . $objScheduledEmailFilter->getLeasingAgents() . ' ) ';
		}

		// floor plan filter
		$strPropertyFloorplans = trim( $objScheduledEmailFilter->getPropertyFloorplans() );
		if( true == valStr( $strPropertyFloorplans ) ) {
			$arrmixSqlFilters['where'][] = 'app.property_floorplan_id IN ( ' . $objScheduledEmailFilter->getPropertyFloorplans() . ' ) ';
		}

		// floor filter
		$strPropertyFloors = trim( $objScheduledEmailFilter->getPropertyFloors() );
		if( true == valStr( $strPropertyFloors ) ) {
			$arrmixSqlFilters['where'][] = 'pu.property_floor_id IN ( ' . $strPropertyFloors . ' ) ';
		}

		// Traffic type filter
		$strEventTypes = trim( $objScheduledEmailFilter->getEventTypes() );
		if( true == valStr( $strEventTypes ) ) {
			$arrmixSqlFilters['where'][] = 'e.event_type_id IN ( ' . $strEventTypes . ' ) AND e.is_deleted = false AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION;
			$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
		}

		// Pets filter
		if( true == valStr( $objScheduledEmailFilter->getPetTypes() ) ) {
			$arrmixPetType = explode( ',', $objScheduledEmailFilter->getPetTypes() );

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixPetType ) && false !== array_search( 'none', $arrmixPetType ) ) {
				$intIndex = array_search( 'none', $arrmixPetType );

				if( 'none' == $arrmixPetType[$intIndex] ) {
					unset( $arrmixPetType[$intIndex] );

					$arrmixSqlFilters['where'][] = '( cp.customer_id IS NULL OR ( cp.pet_type_id IN (' . implode( ',', $arrmixPetType ) . ') AND la.deleted_on IS NULL ) OR
														( la.deleted_on IS NOT NULL AND la.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND la.customer_id NOT IN ( SELECT customer_id FROM lease_associations WHERE cid = ' . ( int ) $objScheduledEmailFilter->getCid() . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND deleted_on IS NULL ) )
													)';
					$arrmixSqlFilters['join']['customer_pets'] = $arrstrTableJoins['customer_pets'];
					$arrmixSqlFilters['join']['lease_associations'] = $arrstrTableJoins['lease_associations'];
					$arrmixSqlFilters['join']['pet_types'] = $arrstrTableJoins['pet_types'];
				}

			} elseif( false !== array_search( 'none', $arrmixPetType ) ) {

				$arrmixSqlFilters['where'][] .= '( cp.customer_id IS NULL OR ( la.deleted_by IS NOT NULL AND la.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND la.customer_id NOT IN ( SELECT customer_id FROM lease_associations WHERE cid = ' . ( int ) $objScheduledEmailFilter->getCid() . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND deleted_on IS NULL ) ) )';
				$arrmixSqlFilters['join']['customer_pets'] = $arrstrTableJoins['customer_pets'];
				$arrmixSqlFilters['join']['lease_associations'] = $arrstrTableJoins['lease_associations'];
				$arrmixSqlFilters['join']['pet_types'] = $arrstrTableJoins['pet_types'];

			} else {

				$arrmixSqlFilters['where'][] = 'la.ar_origin_reference_id IN ( ' . implode( ',', $arrmixPetType ) . ' )';
				$arrmixSqlFilters['where'][] .= 'la.deleted_by IS NULL';
				$arrmixSqlFilters['where'][] .= 'la.deleted_on IS NULL';
				$arrmixSqlFilters['join']['customer_pets'] = $arrstrTableJoins['customer_pets'];
				$arrmixSqlFilters['join']['lease_associations'] = $arrstrTableJoins['lease_associations'];
				$arrmixSqlFilters['join']['pet_types'] = $arrstrTableJoins['pet_types'];

			}
		}

		$strLeaseIntervalTypes = trim( $objScheduledEmailFilter->getLeaseIntervalTypes() );
		if( true == valStr( $strLeaseIntervalTypes ) ) {
			$arrmixLeaseIntervalTypes = explode( ',', $strLeaseIntervalTypes );

			$arrmixAllLeaseIntervalTypes = [];

			foreach( $arrmixLeaseIntervalTypes as &$arrmixLeaseIntervalType ) {
				if( CLeaseIntervalType::RENEWAL == $arrmixLeaseIntervalType || CLeaseIntervalType::LEASE_MODIFICATION == $arrmixLeaseIntervalType ) {
					continue;
				} else {
					$arrmixAllLeaseIntervalTypes[] = $arrmixLeaseIntervalType;
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixAllLeaseIntervalTypes ) ) {
				$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id IN ( ' . implode( ',', $arrmixAllLeaseIntervalTypes ) . ' )';
			} else {
				$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' ';
			}
		} else {
			$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' ';
		}

		$strUnitNumber = trim( $objScheduledEmailFilter->getUnitNumber() );

		if( true == valStr( $strUnitNumber ) ) {
			$arrstrUnitNumberWhere 	= [];

			$arrstrUnitNumbers 		= str_replace( '\\', '',  $strUnitNumber );
			$arrstrUnitNumbers		= explode( ',', str_replace( '*', '%', str_replace( '%', '\%', $arrstrUnitNumbers ) ) );

			if( false != \Psi\CStringService::singleton()->strpos( $strUnitNumber, ',' ) ) {
				$strUnitNumber 		= str_replace( ' ', '', str_replace( '*', '%', str_replace( '%', '\%', $strUnitNumber ) ) );
				$arrstrUnitNumbers 	= explode( ',', $strUnitNumber );
			}

			if( false != \Psi\CStringService::singleton()->strpos( $strUnitNumber, ' ' ) && false === \Psi\CStringService::singleton()->strpos( $strUnitNumber, '-' ) ) {
				$strUnitNumber 		= str_replace( '*', '%', str_replace( '%', '\%', $strUnitNumber ) );
				$arrstrUnitNumbers 	= explode( ' ', $strUnitNumber );
			}

			foreach( $arrstrUnitNumbers as $strUnitNumber ) {
				$strUnitNumber 		= trim( $strUnitNumber );

				if( false !== \Psi\CStringService::singleton()->strpos( $strUnitNumber, '-' ) ) {
					if( 1 == substr_count( $strUnitNumber, '-' ) ) {
						list( $strRangeStart, $strRangeEnd ) 	= explode( '-', $strUnitNumber, 2 );
						$strRangeStart 							= preg_replace( '/[^a-zA-Z0-9\.]+/', '', $strRangeStart );
						$strRangeEnd 							= preg_replace( '/[^a-zA-Z0-9\.]+/', '', $strRangeEnd );
					} else {
						$strRangeStart = '';
						$strRangeEnd = '';
					}

					if( true == valStr( $strRangeStart ) && true == valStr( $strRangeEnd ) ) {
						if( true == is_numeric( $strRangeStart ) && true == is_numeric( $strRangeEnd ) ) {
							$arrstrUnitNumberWhere[] = 'regexp_replace( pu.unit_number, E\'[^0-9.\-]\', \'\', \'g\' ) = pu.unit_number AND CAST ( regexp_replace( pu.unit_number, E\'[^0-9.]\', \'\', \'g\' ) AS numeric ) BETWEEN ' . $strRangeStart . ' AND ' . $strRangeEnd . ' ';
						} else {

							// This block gets alphanumeric range
							$arrstrUnitNumberWhere	= [];
							$strUnitNumbeRangeQuery	= '';
							$arrstrUnitRange		= explode( '-', $strUnitNumber );
							$arrstrUnitRange[0]		= trim( $arrstrUnitRange[0] );
							$arrstrUnitRange[1]		= trim( $arrstrUnitRange[1] );

							preg_match( '/[0-9]+/', $arrstrUnitRange[0], $arrintStartNumber );
							preg_match( '/[0-9]+/', $arrstrUnitRange[1], $arrintEndNumber );
							preg_match( '/[a-zA-Z]+/', $arrstrUnitRange[0], $arrstrStartCharacter );
							preg_match( '/[a-zA-Z]+/', $arrstrUnitRange[1], $arrstrEndCharacter );

							// fetch result based on start range start with alphabet and  end range start with number and vice versa Ex. 100A-200 , 100-200A
							if( ( true == is_numeric( $arrstrUnitRange[0] ) && true == ctype_alnum( $arrstrUnitRange[1] ) ) || ( true == is_numeric( $arrstrUnitRange[1] ) && true == ctype_alnum( $arrstrUnitRange[0] ) ) ) {
								$arrstrTableJoins['property_units'] = 'LEFT JOIN ( SELECT ( regexp_matches ( pu.unit_number, \'^[0-9]+\' ) ) [ 1 ] AS int_unit_number, ( regexp_matches ( pu.unit_number, \'[a-zA-Z]+\' ) ) [ 1 ] AS unit_character, pu.unit_number, pu.property_building_id, pu.id, pu.cid, pu.deleted_on FROM property_units pu WHERE pu.cid =' . ( int ) $intCid . ') AS pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' )';
								$arrmixSqlFilters['join']['property_units'] = $arrstrTableJoins['property_units'];
								$strUnitNumbeRangeQuery = 'int_unit_number::INTEGER BETWEEN ' . ( int ) $arrintStartNumber[0] . ' AND ' . ( int ) $arrintEndNumber[0];
								$arrstrUnitNumberWhere[] = $strUnitNumbeRangeQuery;
								break;
							}

							$intStartRangeLength	= \Psi\CStringService::singleton()->strlen( $arrstrUnitRange[0] );
							$intEndRangeLength		= \Psi\CStringService::singleton()->strlen( $arrstrUnitRange[1] );
							$strStartRange			= \Psi\CStringService::singleton()->substr( $arrstrUnitRange[0], 0, $intStartRangeLength - 1 );
							$strEndRange			= \Psi\CStringService::singleton()->substr( $arrstrUnitRange[1], 0, $intEndRangeLength - 1 );

							// fetch result based on start with alphabet or start with number Ex. 100A-105D , A100-D105
							if( true == is_numeric( $strStartRange ) && true == is_numeric( $strEndRange ) ) {

								$arrstrTableJoins['property_units'] = 'LEFT JOIN ( SELECT ( regexp_matches ( pu.unit_number, \'^[0-9]+\' ) ) [ 1 ] AS int_unit_number, ( regexp_matches ( pu.unit_number, \'[a-zA-Z]+$\' ) ) [ 1 ] AS unit_character, pu.unit_number, pu.property_building_id, pu.id, pu.cid, pu.deleted_on FROM property_units pu WHERE pu.cid =' . ( int ) $intCid . ') AS pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' )';
								$arrmixSqlFilters['join']['property_units'] = $arrstrTableJoins['property_units'];
								$strUnitNumbeRangeQuery = 'int_unit_number::INTEGER BETWEEN ' . ( int ) $arrintStartNumber[0] . ' AND ' . ( int ) $arrintEndNumber[0] . ' AND CASE WHEN int_unit_number::INTEGER = ' . ( int ) $arrintStartNumber[0] . ' THEN LOWER( pu.unit_character ) >= \'' . \Psi\CStringService::singleton()->strtolower( $arrstrStartCharacter[0] ) . '\' WHEN int_unit_number::INTEGER = ' . ( int ) $arrintEndNumber[0] . ' THEN LOWER( pu.unit_character ) <= \'' . \Psi\CStringService::singleton()->strtolower( $arrstrEndCharacter[0] ) . '\' ELSE TRUE END ';
							} else {
								$strStartRange	= \Psi\CStringService::singleton()->substr( $arrstrUnitRange[0], 0, 1 );
								$strEndRange	= \Psi\CStringService::singleton()->substr( $arrstrUnitRange[1], 0, 1 );
								if( true == ctype_alpha( $strStartRange ) && true == ctype_alpha( $strEndRange ) ) {
									$arrstrTableJoins['property_units'] = 'LEFT JOIN ( SELECT ( regexp_matches ( pu.unit_number, \'[0-9]+$\' ) ) [ 1 ] AS int_unit_number, ( regexp_matches ( pu.unit_number, \'^[a-zA-Z]+\' ) ) [ 1 ] AS unit_character, pu.unit_number, pu.property_building_id, pu.id, pu.cid, pu.deleted_on FROM property_units pu WHERE pu.cid =' . ( int ) $intCid . ') AS pu ON ( pu.id = app.property_unit_id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' )';
									$arrmixSqlFilters['join']['property_units'] = $arrstrTableJoins['property_units'];
									$strUnitNumbeRangeQuery = ' int_unit_number::INTEGER BETWEEN ' . ( int ) $arrintStartNumber[0] . ' AND ' . ( int ) $arrintEndNumber[0] . ' AND LOWER( pu.unit_character ) >= \'' . \Psi\CStringService::singleton()->strtolower( $arrstrStartCharacter[0] ) . '\' AND LOWER( pu.unit_character ) <= \'' . \Psi\CStringService::singleton()->strtolower( $arrstrEndCharacter[0] ) . '\' ';
								} else {
									$arrstrUnitNumberWhere[] = 'pu.unit_number = \'#$#$\' AND us.space_number = \'#$#$\' ';
									break;
								}
							}
							$arrstrUnitNumberWhere[] = $strUnitNumbeRangeQuery;
						}
						continue;
					}

					if( true == valStr( $strRangeStart ) ) {
						$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $strRangeStart ) . '\' ';
					}

					if( true == valStr( $strRangeEnd ) ) {
						$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $strRangeEnd ) . '\' ';
					}

					// if both are blank show no result
					if( false == valStr( $strRangeStart ) && false == valStr( $strRangeEnd ) ) {
						$arrstrUnitNumberWhere[] = 'pu.unit_number = \'#$#$\' AND us.space_number = \'#$#$\' ';
					}
				} else {
					$strUnitNumber = addslashes( $strUnitNumber );
					$strUnitNumber = str_replace( '-', '', $strUnitNumber );

					if( true == is_numeric( $strUnitNumber ) && false == \Psi\CStringService::singleton()->stripos( $strUnitNumber, 'e' ) ) {
						$strUnitNumber = preg_replace( '/[^0-9\.]+/', '', $strUnitNumber );
						$arrstrUnitNumberWhere[] = 'regexp_replace( pu.unit_number, E\'[^0-9.\-]\', \'\', \'g\' ) = pu.unit_number AND CAST ( regexp_replace( pu.unit_number, E\'[^0-9.]\', \'\', \'g\' ) AS numeric ) = ' . $strUnitNumber;
					} else {
						$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $strUnitNumber ) . '\' ';
					}
				}
			}

			if( true == valArr( $arrstrUnitNumberWhere ) ) {
				$arrmixSqlFilters['where'][] = 'pu.unit_number <> \'\'';
				$arrmixSqlFilters['where'][] = ' ( ' . implode( ' OR ', $arrstrUnitNumberWhere ) . ' ) ';
			}
		}

		$arrmixSqlFilters['join']['property_buildings']	= $arrstrTableJoins['property_buildings'];
		$arrmixSqlFilters['join']['company_employees']	= $arrstrTableJoins['company_employees'];
		$arrmixSqlFilters['join']['property_floorplans']	= $arrstrTableJoins['property_floorplans'];
		$arrmixSqlFilters['join']['scheduled_email_address_filters']	= $arrstrTableJoins['scheduled_email_address_filters'];

		$strLeadSources = trim( $objScheduledEmailFilter->getLeadSources() );
		if( true == valStr( $strLeadSources ) ) {
			$arrmixSqlFilters['where'][] = 'app.originating_lead_source_id IN ( ' . $objScheduledEmailFilter->getLeadSources() . ' )';
		}

		$strProducts = trim( $objScheduledEmailFilter->getProducts() );
		if( true == valStr( $strProducts ) ) {
			$arrmixSqlFilters['where'][] = 'app.ps_product_id IN ( ' . $objScheduledEmailFilter->getProducts() . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getDesiredRentMin() ) ) {
			$arrmixSqlFilters['where'][] = 'app.desired_rent_min >= ' . ( int ) $objScheduledEmailFilter->getDesiredRentMin();
		}

		if( true == valStr( $objScheduledEmailFilter->getDesiredRentMax() ) ) {
			$arrmixSqlFilters['where'][] = 'app.desired_rent_max <= ' . ( int ) $objScheduledEmailFilter->getDesiredRentMax();
		}

		if( true == valStr( $objScheduledEmailFilter->getApplicationTypes() ) ) {
			$arrmixSqlFilters['where'][] = 'app.company_application_id IN ( ' . $objScheduledEmailFilter->getApplicationTypes() . ' )';
		}

		if( true == $objScheduledEmailFilter->getShowBlockedRecipients() && true == valArr( $objScheduledEmailFilter->getBlockedRecipientIds() ) ) {
			$arrmixSqlFilters['where'][] = 'appt.id NOT IN ( ' . implode( ', ', $objScheduledEmailFilter->getBlockedRecipientIds() ) . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getCustomerTypes() ) ) {
			$arrmixSqlFilters['where'][] = 'aa.customer_type_id IN ( ' . $objScheduledEmailFilter->getCustomerTypes() . ' ) ';
		} else {
			$arrmixSqlFilters['where'][] = 'aa.customer_type_id NOT IN ( ' . CCustomerType::GUARANTOR . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getMoveInDateMin() ) && true == valStr( $objScheduledEmailFilter->getMoveInDateMax() ) ) {
			$intMoveInDateMin	= strtotime( $objScheduledEmailFilter->getMoveInDateMin() );
			$intMoveInDateMax	= strtotime( $objScheduledEmailFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrmixSqlFilters['where'][] = 'li.lease_start_date >= \'' . date( 'Y-m-d', strtotime( $objScheduledEmailFilter->getMoveInDateMin() ) ) . ' 00:00:00\'';
				$arrmixSqlFilters['where'][] = 'li.lease_start_date <= \'' . date( 'Y-m-d', strtotime( $objScheduledEmailFilter->getMoveInDateMax() ) ) . ' 23:59:59\'';
			} else {
				$objScheduledEmailFilter->validate( 'validate_move_in_dates' );
			}
		}

		if( true == valStr( $objScheduledEmailFilter->getDaysCreated() ) && 0 < $objScheduledEmailFilter->getDaysCreated() ) {
			$arrmixSqlFilters['where'][] = ' DATE( app.application_datetime ) BETWEEN ( DATE ( CURRENT_DATE - INTERVAL \'' . $objScheduledEmailFilter->getDaysCreated() . ' day\' ) ) AND CURRENT_DATE';
		} else {
			if( true == valStr( $objScheduledEmailFilter->getApplicationCreatedOnDateMin() ) && true == valStr( $objScheduledEmailFilter->getApplicationCreatedOnDateMax() ) ) {
				$intApplicationCreatedOnDateMin	= strtotime( $objScheduledEmailFilter->getApplicationCreatedOnDateMin() );
				$intApplicationCreatedOnDateMax	= strtotime( $objScheduledEmailFilter->getApplicationCreatedOnDateMax() );

				if( $intApplicationCreatedOnDateMin <= $intApplicationCreatedOnDateMax ) {
					$arrmixSqlFilters['where'][] = 'app.application_datetime >= \'' . date( 'Y-m-d', $intApplicationCreatedOnDateMin ) . ' 00:00:00\'';
					$arrmixSqlFilters['where'][] = 'app.application_datetime <= \'' . date( 'Y-m-d', $intApplicationCreatedOnDateMax ) . ' 23:59:59\'';
				}
			}
		}

		$strDesiredBedrooms = trim( $objScheduledEmailFilter->getDesiredBedrooms() );
		if( true == valStr( $strDesiredBedrooms ) ) {

			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objScheduledEmailFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' pfp.number_of_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' pfp.number_of_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrmixSqlFilters['where'][] = $strCondition;
		}

		$strDesiredBathrooms = trim( $objScheduledEmailFilter->getDesiredBathrooms() );
		if( true == valStr( $strDesiredBathrooms ) ) {

			$boolGreaterThanSix 	= false;
			$arrintDesiredBathrooms	= [];
			$arrintDesiredBathrooms	= explode( ',', $objScheduledEmailFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array then we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' pfp.number_of_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' pfp.number_of_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrmixSqlFilters['where'][] = $strCondition;
		}

		if( true == $objScheduledEmailFilter->getShowEventRecipients() && false == is_null( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) ) {

			$strIntervalCondition = ( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) ? ' + ' : ' - ';

			switch( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) {
				case CScheduledEmailEventType::BIRTHDAY:
					$arrmixSqlFilters['where'][] = 'date_part( \'day\', appt.birth_date ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part(\'day\', CURRENT_DATE )
										AND date_part( \'month\', appt.birth_date ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part( \'month\', CURRENT_DATE )';
					break;

				case CScheduledEmailEventType::MOVE_IN_DATE:
					$arrmixSqlFilters['where'][] = 'li.lease_start_date IS NOT NULL AND ( DATE_TRUNC( \'day\', li.lease_start_date ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::LEAD_CREATED_DATE:
					if( 0 < $objScheduledEmailFilter->getDaysFromEvent() ) {
						$arrmixSqlFilters['where'][] = 'app.application_datetime IS NOT NULL AND ( DATE_TRUNC( \'day\', app.application_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					} else {
						if( false == is_null( $objScheduledEmailFilter->getLastSentOn() ) ) {
							$arrmixSqlFilters['where'][] = ' ( app.application_datetime BETWEEN ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getLastSentOn() ) ) . '\' ) AND ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getLastSentOn() ) ) . '\' + INTERVAL \'200 minutes\' ) )';
						} else {
							$arrmixSqlFilters['where'][] = ' ( app.application_datetime BETWEEN ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getConfirmedOn() ) ) . '\' ) AND ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getConfirmedOn() ) ) . '\' + INTERVAL \'200 minutes\' ) )';
						}
					}
					break;

				case CScheduledEmailEventType::APPLICATION_LAST_UPDATE:
					$arrmixSqlFilters['where'][] = ' ( DATE_TRUNC( \'day\', app.updated_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::LEASE_LAST_UPDATE:
					$arrmixSqlFilters['join']['leases'] = $arrstrTableJoins['leases'];
					$arrmixSqlFilters['where'][] = 'l.updated_on IS NOT NULL AND ( DATE_TRUNC( \'day\', l.updated_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::TOUR_COMPLETED:
					$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
					$arrmixSqlFilters['join']['event_results']	= $arrstrTableJoins['event_results'];
					$arrmixSqlFilters['where'][] = 'app.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . ' AND DATE( e.event_datetime ) <= CURRENT_DATE AND e.is_deleted IS FALSE AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND e.event_type_id = ' . CEventType::TOUR . ' AND e.event_datetime IS NOT NULL AND ( DATE_TRUNC( \'day\', e.event_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() ) AND ' . CDefaultEventResult::COMPLETED . ' = ANY( er.default_event_result_ids ) ';
					break;

				case CScheduledEmailEventType::TOUR_MISSED:
					$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
					$arrmixSqlFilters['join']['event_results']	= $arrstrTableJoins['event_results'];
					$arrmixSqlFilters['where'][] = 'app.application_stage_id <= ' . CApplicationStage::APPLICATION . ' AND DATE( e.event_datetime ) <= CURRENT_DATE AND e.is_deleted IS FALSE AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND e.event_type_id = ' . CEventType::TOUR . ' AND e.event_datetime IS NOT NULL AND ( DATE_TRUNC( \'day\', e.event_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() ) AND ' . CDefaultEventResult::MISSED . ' = ANY( er.default_event_result_ids ) ';
					break;

				case CScheduledEmailEventType::APPOINTMENT_SCHEDULED:
					$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND DATE( e.scheduled_datetime ) >= CURRENT_DATE AND e.is_deleted IS FALSE AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND e.event_type_id = ' . CEventType::SCHEDULED_APPOINTMENT . ' AND e.lease_interval_id IS NOT NULL AND ( DATE_TRUNC( \'day\', e.scheduled_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() ) ';
					break;

				case CScheduledEmailEventType::LEASE_STARTED:
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.application_stage_id = ' . CApplicationStage::LEASE . ' AND app.application_status_id = ' . CApplicationStatus::STARTED . ' AND ( DATE_TRUNC( \'day\', app.lease_generated_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::APPLICATION_STARTED:
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND app.application_status_id = ' . CApplicationStatus::STARTED . ' AND ( DATE_TRUNC( \'day\', app.application_started_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::APPLICATION_PARTIALLY_COMPLETED:
					$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . 'AND e.event_type_id = ' . CEventType::APPLICATION_PROGRESS . ' AND e.new_stage_id = ' . CApplicationStage::APPLICATION . ' AND e.new_status_id = ' . CApplicationStatus::PARTIALLY_COMPLETED . ' AND ( DATE_TRUNC( \'day\', e.event_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::APPLICATION_COMPLETED:
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND app.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND ( DATE_TRUNC( \'day\', app.application_completed_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::LEASE_PARTIALLY_COMPLETED:
					$arrmixSqlFilters['join']['events']	= $arrstrTableJoins['events'];
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . 'AND e.event_type_id = ' . CEventType::LEASE_PROGRESS . ' AND e.new_stage_id = ' . CApplicationStage::LEASE . ' AND e.new_status_id = ' . CApplicationStatus::PARTIALLY_COMPLETED . ' AND ( DATE_TRUNC( \'day\', e.event_datetime ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				case CScheduledEmailEventType::LEASE_COMPLETED:
					$arrmixSqlFilters['where'][] = 'li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND app.application_stage_id = ' . CApplicationStage::LEASE . ' AND app.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND ( DATE_TRUNC( \'day\', app.lease_completed_on ) ' . $strIntervalCondition . ' INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					break;

				default:
					$boolIsValid = false;
					break;
			}
		}

		return $arrmixSqlFilters;
	}

	public static function fetchSimpleRecipientsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $objDatabase, $strSelectedFields, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false, $boolIsSmsTextMessage = false ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valArr( $arrintPropertyIds ) || false == valStr( $strSelectedFields ) ) return NULL;

		$strSelectColumns = '';
		switch( $strSelectedFields ) {
			case 'recipients':
				$strSelectColumns = 'DISTINCT ON ( appt.id, app.property_id, app.property_unit_id ) appt.id AS recipients';
				break;

			case 'count':
				$strSelectColumns = 'COUNT ( DISTINCT appt.id )';
				break;

			default:
				return NULL;
		}

		$strCheckDeletedUnitsSql 		= ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql	= ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		if( false == valStr( $strSelectColumns ) ) return NULL;

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds, $strCheckDeletedUnitsSql, $strCheckDeletedFloorPlansSql, $objScheduledEmailFilter->getCid(), $boolIsSmsTextMessage );

		if( true == $boolIsSmsTextMessage ) {
			$arrmixSqlFilters['join']['customer_phone_numbers'] = 'JOIN customer_phone_numbers cpn ON ( cpn.customer_id = c.id AND cpn.cid = c.cid AND cpn.deleted_by IS NULL AND cpn.phone_number_type_id = 4 AND cpn.phone_number IS NOT NULL)';
		}

		$strSql = '	SELECT
						' . $strSelectColumns . '
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN customers AS c ON ( c.id = appt.customer_id AND c.cid = appt.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_preferences pp ON ( pp.cid = app.cid AND pp.property_id = app.property_id AND pp.key = \'COMPLIANCE_ALLOW_OPT_OUT_TRANSACTIONAL\' AND value = \'1\' )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						app.cid = ' . ( int ) $objScheduledEmailFilter->getCid() . '
						AND ( CASE WHEN pp.id IS NULL THEN TRUE ELSE seaf.applicant_id IS NULL END ) ' . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		switch( $strSelectedFields ) {
			case 'recipients':
				$arrmixAllRecipients = [];
				foreach( $arrmixResponseData as $arrmixReponse ) {
					$arrmixAllRecipients[] = $arrmixReponse['recipients'];
				}
				return $arrmixAllRecipients;

			case 'count':
				if( true == isset( $arrmixResponseData[0]['count'] ) )
					return $arrmixResponseData[0]['count'];

				return 0;

			default:
				return NULL;
		}
	}

	public static function fetchLatestApplicationByLeaseIdByLeaseIntervalTypeIdByCid( $intLeaseId, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						app.*,
						pf.floorplan_name as floorplan_name
					FROM
						applications app
						JOIN lease_intervals li ON ( app.lease_interval_id = li.id AND li.cid = app.cid )
						LEFT JOIN property_floorplans pf ON ( app.cid = pf.cid  AND pf.id = app.property_floorplan_id )
					WHERE
						app.lease_id = ' . ( int ) $intLeaseId . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND pf.cid = ' . ( int ) $intCid . '
					ORDER BY
						app.id DESC
					LIMIT 1 ';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByScheduledEmailFilterByCid( $intPropertyId, $objScheduledEmailFilter, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$strCheckDeletedUnitsSql 		= ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql	= ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, [ $intPropertyId ],  $strCheckDeletedUnitsSql, $strCheckDeletedFloorPlansSql, $intCid );

		$strSql = 'SELECT
						app.id,
						app.lease_id,
						appt.id AS recipient_id,
						aa.application_id,
						appt.name_first,
						appt.name_last
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						seaf.applicant_id IS NULL
						AND app.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		if( 0 < $objScheduledEmailFilter->getRecipientCount() ) {
			$strSql .= ' LIMIT ' . $objScheduledEmailFilter->getRecipientCount();
		}

		return ( array ) fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchApplicationsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolShowAllRecords = true, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false, $boolIsRestrictBlockedRecipients = false, $boolIsSmsTextMessage = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		if( false == is_null( $objScheduledEmailFilter->getPageNo() ) && false == is_null( $objScheduledEmailFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objScheduledEmailFilter->getPageNo() ) ? $objScheduledEmailFilter->getCountPerPage() * ( $objScheduledEmailFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objScheduledEmailFilter->getCountPerPage();
		}

		$strCheckDeletedUnitsSql 		= ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql	= ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds, $strCheckDeletedUnitsSql, $strCheckDeletedFloorPlansSql, $intCid );

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'customer_name_last'		=> 'appt.name_last',
			'customer_email_address'	=> 'appt.username',
			'status'					=> 'app.application_status_id',
			'property'					=> 'app.property_id',
			'property_name'				=> 'p.property_name',
			'source'					=> 'app.originating_lead_source_id',
			'created_on'				=> 'app.created_on',
			'unit_number'				=> 'pu.unit_number'
		];

		if( true == $boolIsRestrictBlockedRecipients ) {
			$arrmixSqlFilters['join'][] = 'LEFT JOIN LATERAL(
																SELECT id FROM
																(
																	SELECT
																		id,
																		regexp_split_to_array ( blocked_recipients, \',\' ) AS blocked_recipients
																	FROM
																		scheduled_email_filters
																	WHERE
																		id = ' . ( int ) $objScheduledEmailFilter->getId() . '
																 ) AS blocked_recipient_lists
																 WHERE
																		appt.id::TEXT = ANY( blocked_recipient_lists.blocked_recipients )
															) AS sef ON ( true )';

			$arrmixSqlFilters['where'][] = 'sef.id IS NULL AND appt.username IS NOT NULL';
		}

		$strSortBy			= ( true == isset( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) ? $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] : 'appt.id' );
		$strSortDirection	= ( false == is_null( $objScheduledEmailFilter->getSortDirection() ) ) ? $objScheduledEmailFilter->getSortDirection() : 'DESC';

		$strSql = 'SELECT
						DISTINCT ON ( appt.id, ' . $strSortBy . ', app.property_id, app.property_unit_id )
						app.*,
						appt.id AS applicant_id,
						appt.customer_id,
						appt.name_last,
						appt.name_first,
						appt.username as email_address,
						aa.id AS applicant_application_id,
						p.property_name,
						aptd.primary_phone_number_type_id,
						cpn.phone_number,
						cpn.phone_number_type_id,
						app.application_stage_id,
						app.application_status_id,
						app.originating_lead_source_id,
						app.property_id,
						app.application_datetime as created_on,
						li.lease_start_date,
						pu.unit_number,
						ce.name_first AS leasing_agent_first_name,
						ce.name_last AS leasing_agent_last_name,
						pfp.floorplan_name,
						pb.building_name ' . ( ( true == valArr( $arrmixSqlFilters['select'] ) ) ? ', ' . implode( ',', $arrmixSqlFilters['select'] ) : '' ) . '
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_preferences pp ON ( pp.cid = app.cid AND pp.property_id = app.property_id AND pp.key = \'COMPLIANCE_ALLOW_OPT_OUT_TRANSACTIONAL\' AND value = \'1\' )
						LEFT JOIN LATERAL (
							SELECT
								*
							FROM
								customer_phone_numbers
							WHERE
								cid = ' . ( int ) $intCid . '
								AND customer_id = appt.customer_id
								AND deleted_by IS NULL
								AND deleted_on IS NULL
							ORDER BY 
								CASE WHEN is_primary = true THEN \'01/01/1900\'::TIMESTAMP
								ELSE
									created_on
								END  ASC
								LIMIT 1
						) as cpn ON TRUE
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						( CASE WHEN pp.id IS NULL THEN TRUE ELSE seaf.applicant_id IS NULL END )
						AND app.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		if( false == is_null( $strSortBy ) && false == is_null( $strSortDirection ) ) {
			$strSql .= ' ORDER BY ' . addslashes( $strSortBy ) . ' ' . addslashes( $strSortDirection );
		}

		if( false == $boolShowAllRecords ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchApplications( $strSql, $objDatabase, false );
	}

	public static function fetchCountApplicationsAsResidentByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds );

		$strJoinCustomer = ( false == array_key_exists( 'customers', $arrmixSqlFilters['join'] ) ) ? 'JOIN customers AS c ON ( c.id = appt.customer_id AND c.cid = appt.cid )' : '';

		$strSql = 'SELECT
						COUNT( DISTINCT( appt.username ) ) as count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						JOIN customer_portal_settings AS cps ON ( cps.username = appt.username AND appt.cid = cps.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . ' ' . $strJoinCustomer . '
						JOIN lease_customers lc ON ( c.id = lc.customer_id AND c.cid = lc.cid )
					WHERE
						seaf.applicant_id IS NULL
						AND appt.username IS NOT NULL
						AND lc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ' , ' . CLeaseStatusType::NOTICE . ' )
						AND app.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchApplicationByPropertyIdByIdByCid( $intPropertyId, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM applications WHERE property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByLeaseIdsByLeaseIntervalTypeIdByCid( $intPropertyId, $arrintLeaseIds, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = ' SELECT app.* FROM applications app JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id ) WHERE app.property_id = ' . ( int ) $intPropertyId . ' AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . ' AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByLeaseIdsByLeaseIntervalTypeIdsByCid( $intPropertyId, $arrintLeaseIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase ) {
		if( false == valStr( $intPropertyId ) || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalTypeIds ) ) return NULL;

		$strSql = ' SELECT app.* FROM applications app JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id ) WHERE app.property_id = ' . ( int ) $intPropertyId . ' AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND li.lease_interval_type_id IN( ' . implode( ', ', $arrintLeaseIntervalTypeIds ) . ' ) AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByGuestRemotePrimaryKeyByPropertyIdByByCid( $strRemotePrimaryKey, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valStr( $intPropertyId ) || false == valStr( $strRemotePrimaryKey ) ) return NULL;

		$strSql = 'SELECT * FROM applications WHERE lease_id IS NULL AND guest_remote_primary_key = \'' . $strRemotePrimaryKey . '\' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByEventTypeIdByDataReferenceIdByCid( $intEventTypeId, $intDataReferenceId, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN events e ON ( e.cid = app.cid AND e.lease_interval_id = app.lease_interval_id AND e.event_type_id = ' . ( int ) $intEventTypeId . ' )
					WHERE
						e.lease_interval_id IS NOT NULL
						AND e.data_reference_id = ' . ( int ) $intDataReferenceId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND e.is_deleted = false';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByDataReferenceIdsByCid( $arrintDataReferenceIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDataReferenceIds ) ) return NULL;
		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN events e ON ( e.cid = app.cid AND e.lease_interval_id = app.lease_interval_id )
					WHERE
						e.data_reference_id IN ( ' . implode( ',', $arrintDataReferenceIds ) . ' )
						AND app.cid = ' . ( int ) $intCid;

		return ( array ) self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicationsByUsernameByCid( $strUsername, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql  = 'SELECT
						ap.id as applicant_id,
						ap.name_first,
						ap.name_last,
						aa.application_id,
						ass.stage_name || ass.status_name as application_status ,
						app.property_id,
						ct.name as customer_type
					FROM
						applicant_applications aa
						JOIN applications app ON ( aa.cid = app.cid AND app.id = aa.application_id )
			 			JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicants ap ON ( aa.cid = ap.cid AND ap.id = aa.applicant_id )
						JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = li.lease_interval_type_id AND ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id )
						JOIN customer_types ct ON ( aa.customer_type_id = ct.id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
						AND ass.id <> ' . CApplicationStageStatus::PROSPECT_STAGE4_APPROVED . '
						AND lower( ap.username ) LIKE \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '%\'
						AND aa.customer_type_id IN ( ' . implode( ',', CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) . ' )' . $strCheckDeletedAASql . ' LIMIT 2';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationFilterByCid( $objApplicationFilter, $intCid, $objDatabase, $boolIsReturnId = false, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$intOffset 				= NULL;
		$intMaxSelectionLimit 	= 1000;
		$intLimit  				= NULL;

		if( false == is_null( $objApplicationFilter->getPageNo() ) && false == is_null( $objApplicationFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationFilter->getPageNo() ) ? $objApplicationFilter->getCountPerPage() * ( $objApplicationFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= $objApplicationFilter->getCountPerPage();
		}
		$strApplicationStageStatuses = trim( $objApplicationFilter->getApplicationStageStatuses() );
		if( false == empty( $strApplicationStageStatuses ) ) {
			$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		}
		$strProperties = trim( $objApplicationFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationFilter->getProperties() . ' ) ';
		}

		if( false == is_null( $objApplicationFilter->getCompanyEmployees() ) ) {
			$arrstrWhere[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
		} else {
			$arrstrWhere[] = ' ( app.leasing_agent_id IS NULL ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSelect = 'appt.id AS applicant_id,

						appt.name_last,
						appt.name_first,
						appt.phone_number,
						appt.mobile_number,
						appt.work_number,
						appt.username AS email_address,
						app.id AS id,
						app.property_id,
						app.application_datetime,
						app.application_stage_id,
						app.application_status_id,
						app.lease_id,
						app.created_on,
						app.application_completed_on,
						li.lease_end_date,
						pu.unit_number AS unit_number,
						CASE
							WHEN ( lc.lease_status_type_id IN  ( ' . CLeaseStatusType::CURRENT . ' , ' . CLeaseStatusType::NOTICE . '  ) AND li.lease_end_date  <= CURRENT_TIMESTAMP  ) THEN
								1
							ELSE
								li.lease_interval_type_id
						END::integer AS month_to_month';

		if( true == $boolIsReturnId ) {

			$intLimit	= $intMaxSelectionLimit;
			$strSelect	= ' app.id AS id ';

		}

		$strSql  = 'SELECT
						' . $strSelect . '
					FROM
						applications app
						JOIN lease_intervals li_app ON ( li_app.cid = app.cid AND li_app.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li_app.lease_interval_type_id)
						JOIN properties p ON ( app.property_id = p.id AND app.cid = p.cid )
						JOIN applicant_applications aa ON ( aa.application_id = app.id
															AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
															AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )

						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.cid = app.cid )
						JOIN applicant_details ad ON ( appt.id = ad.applicant_id AND ad.cid = appt.cid )

						LEFT JOIN leases l ON ( app.lease_id = l.id AND l.cid = app.cid )
						LEFT JOIN lease_customers lc ON ( l.cid = lc.cid
														AND l.id = lc.lease_id
														AND lc.customer_id = l.primary_customer_id )

						LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid )

						LEFT JOIN property_units pu ON ( app.property_unit_id = pu.id AND pu.cid = app.cid ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrOrderBy = [
			'name_last'			=> 'appt.name_last',
			'property_name'		=> 'p.property_name',
			'month_to_month'	=> 'month_to_month'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationFilter->getSortBy()] : '' );
		$strSortDirection = ( false == is_null( $objApplicationFilter->getSortDirection() ) ) ? $objApplicationFilter->getSortDirection() : 'ASC';

		if( true == valStr( $strOrderBy ) ) {
			if( 'p.property_name' == $strOrderBy ) {

				$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection ) . ' ,pu.unit_number ' . addslashes( $strSortDirection );

			} else {
				$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );
			}
		}

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}
		$arrmixApplications = fetchData( $strSql, $objDatabase );
		return $arrmixApplications;

	}

	public static function fetchApplicationsCountByApplicationFilterByCid( $objApplicationFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strApplicationStageStatuses = trim( $objApplicationFilter->getApplicationStageStatuses() );
		if( false == empty( $strApplicationStageStatuses ) ) {
			$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		}
		$strProperties = trim( $objApplicationFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationFilter->getProperties() . ' ) ';
		}

		if( false == is_null( $objApplicationFilter->getCompanyEmployees() ) ) {
			$arrstrWhere[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
		} else {
			$arrstrWhere[] = ' ( app.leasing_agent_id IS NULL ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql  = 'SELECT
						COUNT( appt.id )
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND  )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li.lease_interval_type_id)
						JOIN applicant_applications aa ON (	aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = 1 ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN applicant_details ad ON ( appt.id = ad.applicant_id AND appt.cid = ad.cid )
						LEFT JOIN leases l ON ( app.lease_id = l.id AND app.cid = l.cid )
					WHERE
						app.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchCustomCCPaymentPendingApplications( $objDatabase, $intApplicationId = NULL, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN application_payments ap ON ( ap.application_id = app.id AND ap.cid = app.cid )
						JOIN ar_payments arp ON ( arp.id = ap.ar_payment_id AND arp.cid = ap.cid )
						JOIN applicant_applications aa ON ( aa.applicant_id = ap.applicant_id AND aa.application_id = ap.application_id AND aa.cid = ap.cid ' . $strCheckDeletedAASql . ' )
						JOIN clients mc ON app.cid = mc.id
						JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid AND p.is_disabled = 0 )
					WHERE
						arp.payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' )
						AND aa.completed_on IS NULL
						AND app.application_completed_on IS NULL
						AND ( (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ') OR app.requires_capture = TRUE )
						AND to_char( app.updated_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'60\' )::date
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT;

		$strSql .= ( 0 < $intApplicationId ) ? ' AND app.id = ' . ( int ) $intApplicationId : '';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchCustomMoneygramPaymentPendingApplications( $objDatabase, $intApplicationId = NULL, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants a ON a.id = aa.applicant_id AND a.cid = aa.cid
						RIGHT JOIN ar_payments arp ON arp.lease_id = app.lease_id AND arp.customer_id = a.customer_id AND arp.cid = app.cid
						JOIN clients mc ON app.cid = mc.id
						JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid AND p.is_disabled = 0 )
					WHERE
						arp.payment_type_id = ' . CPaymentType::EMONEY_ORDER . '
						AND arp.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND (
							--If application is in Application Approved / Lease Approved status then completed_on would be set
							( aa.completed_on IS NOT NULL AND app.application_completed_on IS NOT NULL AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ] ] ) . ' ) )
							OR ( aa.completed_on IS NULL AND app.application_completed_on IS NULL AND ( ( app.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND app.application_status_id = ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ) OR app.requires_capture = TRUE ) )
						)
						AND to_char( app.updated_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'60\' )::date
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT;

		$strSql .= ( 0 < $intApplicationId ) ? ' AND app.id = ' . ( int ) $intApplicationId : '';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchFutureRenewalApplicationByLeaseIdByLeaseEndDateByCid( $intLeaseId, $strLeaseEndDate, $intCid, $objDatabase ) {

		if( false == valStr( $strLeaseEndDate ) ) return NULL;

		$strSql = ' SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON( app.cid = li.cid AND app.lease_id = li.lease_id AND li.id = app.lease_interval_id AND li.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
					WHERE
						app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND app.application_stage_id = ' . ( int ) CApplicationStage::LEASE . '
						AND app.application_status_id = ' . ( int ) CApplicationStatus::APPROVED . '
						AND li.lease_start_date > \'' . date( 'Y-m-d', strtotime( $strLeaseEndDate ) ) . ' 00:00:00\'
						AND app.lease_approved_on IS NOT NULL
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN file_associations fa ON( app.id = fa.application_id AND app.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND fa.file_id = ' . ( int ) $intFileId . ' ) LIMIT 1';
		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByApplicantIdByPropertyIdsByCid( $intApplicantId, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						app.*
					FROM applications app
						INNER JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE app.cid = ' . ( int ) $intCid . '
						AND aa.applicant_id = ' . ( int ) $intApplicantId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY app.id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchExtendedRenewalOfferCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false === valId( $intLeaseId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return 0;
		}

		$strSql = '	SELECT
						COUNT(app.*)
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id AND li.lease_id = app.lease_id )
						JOIN leases l ON( l.cid = li.cid AND l.id = li.lease_id AND l.active_lease_interval_id <> li.id )
					WHERE
						app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND li.lease_status_type_id IN( ' . sqlIntImplode( [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE ] ) . ' )
						AND (app.application_stage_id, app.application_status_id) NOT IN ( ' . sqlIntMultiImplode( array( CApplicationStage::PRE_APPLICATION => array( CApplicationStatus::ON_HOLD ), CApplicationStage::APPLICATION => array( CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ), CApplicationStage::LEASE => array( CApplicationStatus::CANCELLED ) ) ) . ')';

		$arrmixQueryResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixQueryResult ) ) return $arrmixQueryResult[0]['count'];
		return 0;
	}

	public static function fetchOtherGuestCardCountsByStatisticsEmailFilter( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						app.property_id,
						app.originating_lead_source_id AS lead_source_id,
						app.ps_product_id,
						count ( DISTINCT( app.id )) lead_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '	AND
						app.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND
						app.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '	AND
						li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '	AND
						app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND
						app.created_on::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date AND
						app.created_on::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						app.property_id,
						app.originating_lead_source_id,
						app.ps_product_id
					ORDER BY
						app.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchInsuranceMissingApplicationsByApplicationFilterByCid( $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );
		if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
			$arrstrWhere[] = ' ( app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR app.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhere[] = ' app.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
			$arrstrWhere[] = ' ( app.leasing_agent_id IS NULL ) ';
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						app.*,
						appt.name_last,
						appt.name_first,
						appt.email_address,
						appt.phone_number,
						p.property_name,
						pu.unit_number,
						us.space_number,
						rip.id as policy_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						LEFT JOIN properties p ON ( app.property_id = p.id AND app.cid = p.cid )
						LEFT JOIN property_units pu ON ( app.property_unit_id = pu.id AND app.cid = pu.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN unit_spaces us ON ( app.unit_space_id = us.id AND app.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN insurance_policy_customers ipc ON( app.lease_id = ipc.lease_id AND app.id = ipc.application_id AND app.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )';
		$strSql .= ' WHERE
						rip.confirmed_on IS NULL
						AND app.application_stage_id = ' . ( int ) CApplicationStage::LEASE . '
						AND app.application_status_id = ' . ( int ) CApplicationStatus::COMPLETED . '
						AND app.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByTransmissionVendorIdByApplicationsFilterByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $intTransmissionVendorId, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrWhere = [];

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationsFilter->getPageNo() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objApplicationsFilter->getCountPerPage();
		}

		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		} else {
			$arrstrWhere[] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'app.application_completed_on IS NOT NULL';
		}

		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						*
					FROM
						(
						SELECT
							DISTINCT ON ( TEMP . id ) TEMP . *
						FROM
							(
								SELECT
									app.id,
									aat.transmission_response_type_id,
									appt.id AS applicant_id,
									appt.name_last,
									appt.name_first,
									appt.phone_number,
									appt.mobile_number,
									appt.work_number,
									appt.username AS email_address,
									app.cid,
									app.property_id,
									app.application_datetime,
									app.created_on,
									app.application_completed_on,
									app.application_approved_on,
									app.application_stage_id,
									app.application_status_id,
									sar.screening_decision_type_id,
									aa.customer_type_id,
									CASE
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::HOME . ' THEN appt.phone_number
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' THEN appt.work_number
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' THEN appt.mobile_number
									END AS primary_phone_number,
									ad.primary_phone_number_type_id
								FROM
									applications app
									JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
									JOIN applicant_applications aa ON ( app.id = aa.application_id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
									JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
									JOIN applicant_details ad ON ( ad.cid = appt.cid AND ad.applicant_id = appt.id )
									JOIN applicant_application_transmissions aat ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid AND aat.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . ' )
									JOIN screening_application_requests sar ON ( app.id = sar.application_id AND app.cid = sar.cid AND sar.screening_decision_type_id ISNULL )';

		$strSql .= ' WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND app.application_status_id = ' . CApplicationStatus::COMPLETED . '
						AND ( (
								SELECT
									SUM( CASE WHEN customer_type_id = ' . CCustomerType::GUARANTOR . ' THEN CASE WHEN ( SELECT MAX( id ) FROM applicant_application_transmissions WHERE application_id = app.id AND cid = ' . ( int ) $intCid . ' AND applicant_application_id = aa1.id AND  transmission_response_type_id = ' . CTransmissionResponseType::IN_PROGRESS . ' ) IS null THEN 0 ELSE 1 END ELSE 1 END ) as cnt
								FROM
									applicant_applications aa1
								WHERE
									aa1.application_id = app.id
									AND aa1.cid = ' . ( int ) $intCid . '
									AND aa1.customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
						) =
						(
						SELECT count( id ) cnt
							FROM (
									SELECT aat.id,aat.transmission_response_type_id,
											MAX(aat.id) OVER( PARTITION BY applicant_application_id ) as
											max_application_id
									FROM applicant_application_transmissions aat
											JOIN applicant_applications aa ON (aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
									WHERE
										aat.application_id = app.id
										AND aat.cid = ' . ( int ) $intCid . '
										AND customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
							) as sub
							WHERE id = max_application_id  AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						) ) ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ' . ') TEMP ';
		}

		$strSql .= ' WHERE
						customer_type_id = ' . CCustomerType::PRIMARY . '
						AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						AND application_approved_on IS NULL
						AND cid = ' . ( int ) $intCid;

		$arrstrOrderBy = [
			'lead_id'					=> 'id',
			'completed_on'				=> 'completed_on',
			'name'						=> 'name_last || name_first',
			'application_datetime'		=> 'application_datetime'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'app.application_datetime' );
		$strSortDirection = ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';
		$strSql .= ') temp_final ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchApplications( $strSql, $objDatabase );

	}

	public static function fetchLastContractualApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN lease_intervals li ON ( li.cid = ca.cid AND li.property_id = ca.property_id AND li.lease_id = ca.lease_id AND ca.lease_interval_id = li.id AND li.lease_start_date <= ca.lease_start_date )
					WHERE
						li.cid = ' . ( int ) $intCid . '
						AND li.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND li.lease_interval_type_id NOT IN (' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
						AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::CANCELLED . ' )
					ORDER BY
						li.lease_start_date DESC
					LIMIT 1';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchResidentVerifyTransmissionVendorApplicationsByApplicationFilterByCid( $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$arrstrWhere = [];

		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		}
		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'app.application_completed_on IS NOT NULL';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						count ( DISTINCT ( TEMP . id ) ) AS count
					FROM
						(
						SELECT
							app.id,
							aat.transmission_response_type_id,
							app.application_approved_on,
							aa.customer_type_id,
							app.cid
						FROM
							applications app
							JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
							JOIN applicant_applications aa ON ( app.id = aa.application_id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
							JOIN applicant_application_transmissions aat ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid AND aat.transmission_vendor_id = 21 )
							JOIN screening_application_requests sar ON ( app.id = sar.application_id AND app.cid = sar.cid AND sar.screening_decision_type_id ISNULL )';

		$strSql .= ' WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND app.application_status_id = ' . CApplicationStatus::COMPLETED . '
						AND ( (
								SELECT
									SUM( CASE WHEN customer_type_id = ' . CCustomerType::GUARANTOR . ' THEN CASE WHEN ( SELECT MAX( id ) FROM applicant_application_transmissions aat WHERE aat.cid = ' . ( int ) $intCid . ' AND aat.application_id = app.id AND aat.applicant_application_id = aa1.id AND  aat.transmission_response_type_id = ' . CTransmissionResponseType::IN_PROGRESS . ' ) IS null THEN 0 ELSE 1 END ELSE 1 END ) as cnt
								FROM
									applicant_applications aa1
								WHERE
									aa1.application_id = app.id
									AND aa1.cid = ' . ( int ) $intCid . '
									AND aa1.customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
						) =
						(
						SELECT count( id ) cnt

							FROM (
									SELECT aat.id,aat.transmission_response_type_id,
											MAX(aat.id) OVER( PARTITION BY applicant_application_id ) as
											max_application_id
									FROM applicant_application_transmissions aat
											JOIN applicant_applications aa ON (aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
									WHERE
										aat.application_id = app.id
										AND aat.cid = ' . ( int ) $intCid . '
										AND customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
							) as sub
							WHERE id = max_application_id  AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						) ) ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ' . ' AND li.lease_interval_type_id = ' . CLeaseIntervalTypeType::APPLICANT . ' ) TEMP ';
		}

		$strSql .= ' WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_type_id = ' . CCustomerType::PRIMARY . '
						AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						AND application_approved_on IS NULL ';

		$arrstrData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
		return 0;

	}

	public static function fetchExentdedRenewalOfferApplicationsByPropertyIdByLeaseIdsByCid( $intPropertyId, $arrintLeaseIds = [], $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intPropertyId ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN leasE_intervals ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND app.application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND app.application_status_id = ' . CApplicationStatus::STARTED;

						if( false != valArr( $arrintLeaseIds ) ) {
							$strSql .= ' AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
						}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByCallIdByCid( $intCallId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM applications WHERE cid = ' . ( int ) $intCid . ' AND call_id = ' . ( int ) $intCallId . ' LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseId, $intCid, $objDatabase, $arrintApplicationStageStatusIds = NULL ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;
		if( true == is_null( $arrintApplicationStageStatusIds ) )  return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( app.application_stage_id = ass.application_stage_id AND app.application_status_id = ass.application_status_id AND li.lease_interval_type_id = ass.lease_interval_type_id  )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND ass.id IN ( ' . implode( ',', $arrintApplicationStageStatusIds ) . ' )
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchRenewalApplicationsByLeaseIdsByApplicationStageStatusIdsByCid( $arrintLeaseIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*,
						us.unit_number_cache As unit_number_cache,
						pf.floorplan_name
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN unit_spaces us ON ( app.cid = us.cid AND us.id = app.unit_space_id )
						LEFT JOIN property_floorplans pf ON ( app.cid = pf.cid AND pf.property_id = app.property_id AND app.property_floorplan_id = pf.id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND (app.application_stage_id,app.application_status_id) IN( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchTransferQuoteApplicationsByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*,
						us.unit_number_cache As unit_number_cache,
						pf.floorplan_name,
						li.lease_interval_type_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN unit_spaces us ON ( app.cid = us.cid AND us.id = app.unit_space_id )
						LEFT JOIN property_floorplans pf ON ( app.cid = pf.cid AND pf.property_id = app.property_id AND app.property_floorplan_id = pf.id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . '
						AND (app.application_stage_id,app.application_status_id) IN( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND app.lease_id = ' . ( int ) $intLeaseId;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchTransferQuoteApplicationByLeaseIdByApplicationStageIdByApplicationStatusIdsByCid( $intLeaseId, $intApplicationStageId,  $arrintApplicationStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intApplicationStageId ) || false == valArr( $arrintApplicationStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*,
						us.unit_number_cache As unit_number_cache,
						pf.floorplan_name,
						li.lease_interval_type_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN unit_spaces us ON ( app.cid = us.cid AND us.id = app.unit_space_id )
						LEFT JOIN property_floorplans pf ON ( app.cid = pf.cid AND pf.property_id = app.property_id AND app.property_floorplan_id = pf.id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . '
						AND app.application_stage_id = ' . ( int ) $intApplicationStageId . '
                        AND app.application_status_id IN ( ' . sqlIntImplode( $arrintApplicationStatusIds ) . ' )
						AND app.lease_id = ' . ( int ) $intLeaseId . '
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByRoommateIdByCid( $intRoommateId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intRoommateId ) ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						roommates r
						JOIN roommate_groups rg ON ( r.roommate_group_id = rg.id AND r.cid = rg.cid )
						JOIN applications app ON (rg.application_id = app.id AND rg.cid = app.cid )
					WHERE
						r.id =' . ( int ) $intRoommateId . '
						AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByApplicantIdByLeaseTermIdByPropertyFloorplanIdByPropertyIdByCid( $intApplicantId, $intLeaseTermId, $intPropertyFloorplanId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == is_numeric( $intApplicantId ) || false == is_numeric( $intLeaseTermId ) || false == is_numeric( $intPropertyFloorplanId ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strCheckDeletedAASql       = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedAASqlJoin   = ( false == $boolIncludeDeletedAA ) ? ' JOIN applicant_applications aa ON ( a.id = aa.application_id AND a.cid = aa.cid )' : '';

		$strSql = 'SELECT
						a.*
					FROM
						applications a
						' . $strCheckDeletedAASqlJoin . '
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.id =' . ( int ) $intApplicantId . ')
						JOIN lease_intervals li ON ( li.cid = a.cid AND li.id = a.lease_interval_id AND ( ( li.lease_id = a.lease_id ) OR ( li.lease_id IS NULL AND a.lease_id IS NULL ) ) )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.application_status_id IN( ' . CApplicationStatus::COMPLETED . ',' . CApplicationStatus::APPROVED . ',' . CApplicationStatus::STARTED . ',' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
						AND li.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND a.property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . $strCheckDeletedAASql;

		$strSql .= ' ORDER BY id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $intLeaseId, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsFromMidLease = false ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.lease_id =' . ( int ) $intLeaseId . '
						AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		if( true == $boolIsFromMidLease ) {
			$strSql .= ' ORDER BY app.id DESC LIMIT 1';
		}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdsByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $arrintLeaseIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsFromMidLease = false ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
            app.*
          FROM
            applications app
			JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
          WHERE
            app.cid = ' . ( int ) $intCid . '
            AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
            AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
            AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		if( true == $boolIsFromMidLease ) {
			$strSql .= ' ORDER BY app.id DESC LIMIT 1';
		}

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchRenewalOpenApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						app.*,
						COUNT( sc.id ) AS scheduled_charges_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN scheduled_charges sc ON( sc.cid = app.cid AND sc.property_id = app.property_id AND sc.lease_id = app.lease_id AND sc.lease_interval_id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND app.lease_id IN (' . implode( ',', $arrintLeaseIds ) . ' )
						AND app.application_stage_id =' . CApplicationStage::APPLICATION . '
						AND app.application_status_id =' . CApplicationStatus::STARTED . '
					GROUP BY
						app.id,
						app.cid
					HAVING
						COUNT( sc.id ) = 0';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchRenewalOpenApplicationByIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						li.lease_interval_type_id,
						li.lease_term_id,
						li.lease_start_date,
						li.lease_end_date
					FROM
						applications a
						JOIN lease_intervals li ON ( li.cid = a.cid AND li.id = a.lease_interval_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND a.id = ' . ( int ) $intApplicationId . '
						AND a.application_stage_id =' . ( int ) CApplicationStage::APPLICATION . '
						AND a.application_status_id =' . ( int ) CApplicationStatus::STARTED;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationWithLeaseIntervalDataByIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						li.lease_term_id,
						li.lease_start_date,
						li.lease_end_date,
						li.lease_start_window_id,
						li.late_fee_formula_id,
						le.move_in_date
					FROM
						applications a
						JOIN lease_intervals li ON ( li.cid = a.cid AND li.id = a.lease_interval_id )
						LEFT JOIN lease_processes le ON ( le.cid = a.cid AND le.lease_id = a.lease_id AND le.customer_id IS NULL )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id = ' . ( int ) $intApplicationId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationWithLeaseIntervalDataByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						li.lease_term_id,
						li.lease_start_date,
						li.lease_end_date,
						li.lease_start_window_id,
						li.late_fee_formula_id,
						li.is_immediate_move_in
					FROM
						applications a
						JOIN lease_intervals li ON ( li.cid = a.cid AND li.id = a.lease_interval_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchTransferOpenApplicationsByIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$arrintApplicationStatuses = [ CApplicationStatus:: PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ];

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						LEFT JOIN scheduled_charges sc ON ( li.cid = sc.cid AND li.id = sc.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
						AND app.id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND sc.id IS NULL
						AND app.application_stage_id =' . CApplicationStage::APPLICATION . '
						AND app.application_status_id IN ( ' . sqlIntImplode( $arrintApplicationStatuses ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsCountByLeaseIntervalTypeIdByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $intLeaseId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						count( app.id )
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.lease_id =' . ( int ) $intLeaseId . '
						AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		$arrmixApplicationCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixApplicationCount ) ) return $arrmixApplicationCount[0]['count'];

		return 0;
	}

	public static function fetchApplicationsByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchApplications( $strSql, $objDatabase );

	}

	public static function fetchApplicationsByLeaseIdsByPropertyIdByCustomerIdByCid( $arrintLeaseIds, $intPropertyId, $intCustomerId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						a.*,
						aa.id as applicant_application_id
					FROM
						applications a
						JOIN applicant_applications aa ON ( a.cid = aa.cid AND a.id = aa.application_id )
						JOIN applicants app ON ( app.cid = aa.cid AND app.id = aa.applicant_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND app.customer_id = ' . ( int ) $intCustomerId . '
						AND a.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );

	}

	/**
	 * Function used for searching and filtering the waitlist by applicant
	 *
	 * @param int $intApplicationId
	 * @param array $arrintPropertyFloorplanIds
	 * @param array $arrintPropertyIds
	 * @param int $intCid
	 * @param CDatabase $objDatabase
	 * @param bool $boolIncludeDeletedAA
	 * @param bool $boolIncludeDeletedFloorPlans
	 * @return array
	 */

	public static function fetchApplicationsByApplicationIdByPropertyFloorplanIdsByPropertyIdsByCid( $intApplicationId, $arrintPropertyFloorplanIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedFloorPlans = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						apl.id applicant_id,
						app.id application_id,
						apl.name_first,
						apl.name_last,
						app.lease_id,
						app.screening_approved_on,
						app.application_completed_on,
						li.lease_start_date,
						apl.username AS email_address,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS phone_number,
						pf.floorplan_name,
						af.property_floorplan_id af_property_floorplan_id,
						app.property_floorplan_id ap_property_floorplan_id,
						app.desired_bedrooms,
						app.desired_bathrooms,
						COALESCE ( sub_art.balance, 0 ) AS balance,
						CASE
						WHEN 0 >= COALESCE ( sub_art.balance, 0 ) THEN 0
						ELSE sub_art.balance
						END balance_for_sorting
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li.lease_interval_type_id)
						JOIN wait_lists wl ON ( wl.cid = app.cid AND wl.property_group_id = app.property_id AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						LEFT JOIN property_floorplans pf ON ( pf.cid = app.cid ' . $strCheckDeletedFloorPlansSql . ' )
						LEFT JOIN application_floorplans af ON ( af.cid = app.cid AND af.application_id = app.id AND af.property_floorplan_id = pf.id )
						JOIN applicant_applications aa ON ( aa.cid = app.cid AND aa.application_id = app.id ' . $strCheckDeletedAASql . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN lease_customers lc ON ( lc.cid = apl.cid AND lc.customer_id = apl.customer_id AND lc.lease_id = app.lease_id )
						LEFT JOIN
								(
								SELECT
									COALESCE ( sum ( art.transaction_amount ), 0 ) AS balance,
									art.cid,
									art.lease_id
								FROM
									ar_transactions art
								WHERE
									art.cid = ' . ( int ) $intCid . '
									AND art.ar_code_type_id IN ( ' . implode( ', ', array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT ] ) ) . ' )
									AND art.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								GROUP BY
									art.cid,
									art.lease_id
								) sub_art ON ( sub_art.cid = app.cid AND sub_art.lease_id = app.lease_id )
					WHERE
							app.cid = ' . ( int ) $intCid . '
							AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND ass.id >= wl.application_stage_status_id
							AND app.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
							AND app.application_completed_on IS NOT NULL
							AND app.property_unit_id IS NULL
							AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
							AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '';

		if( true == valArr( $arrintPropertyFloorplanIds ) ) {

			$strSql .= 'AND ( app.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' )
								OR af.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' ) )
							AND ( ( app.property_floorplan_id IS NOT NULL OR af.property_floorplan_id IS NOT NULL )
								OR ( app.desired_bathrooms IS NOT NULL AND app.desired_bedrooms IS NOT NULL ) )
							AND ( app.property_floorplan_id = pf.id OR af.property_floorplan_id = pf.id )';

		} elseif( false == is_null( $intApplicationId ) ) {

			$strSql .= 'AND app.id = ' . ( int ) $intApplicationId;
		}

		$strSql .= '
					ORDER BY
						balance_for_sorting,
						app.application_completed_on,
						li.lease_start_date,
						apl.name_first,
						apl.name_last';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByFloorPlanGroupIdsByPropertyIdsByCid( $arrintFloorplanGroupIds, $objWaitlistFilter, $strGroupType = NULL, $objDatabase ) {
		if( false == valArr( $arrintFloorplanGroupIds ) ) return NULL;

		$strPropertyIdCondition = ( true == valArr( $objWaitlistFilter->getParentChildPropertyIds() ) ) ? implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) : $objWaitlistFilter->getPropertyId();

		$strSortBy = ( 'applications_count' == $objWaitlistFilter->getSortColumn() ) ? ( 'desc' == $objWaitlistFilter->getSortOrder() ? ' ORDER BY applications_count DESC ' : ' ORDER BY applications_count ASC ' ) : '';

		$strSql = 'SELECT
						fg.id,
						count( DISTINCT app.id ) AS applications_count
					FROM
						floorplan_groups fg
						JOIN property_floorplan_groups pfg ON ( fg.cid = pfg.cid AND fg.id = pfg.floorplan_group_id )
						JOIN application_floorplans af ON ( pfg.cid = af.cid AND pfg.property_id = af.property_id AND pfg.property_floorplan_id = af.property_floorplan_id )
						JOIN applications app ON ( app.cid = af.cid AND app.property_id = af.property_id AND app.id = af.application_id )
						JOIN properties p ON ( p.cid = app.cid and p.id = app.property_id)
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id AND li.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . ' )
						JOIN wait_lists wl ON ( wl.cid = app.cid AND ( wl.property_group_id = p.id or wl.property_group_id = p.property_id ) AND wl.is_active IS TRUE AND app.occupancy_type_id = wl.occupancy_type_id )
						JOIN wait_list_applications wla ON ( wl.cid = wla.cid AND wl.id = wla.wait_list_id AND wla.wait_start_on IS NOT NULL AND wla.wait_end_on IS NULL AND app.id = wla.application_id )
					WHERE
						fg.cid = ' . ( int ) $objWaitlistFilter->getcId() . '
						AND pfg.property_id IN ( ' . $strPropertyIdCondition . ' )
						AND fg.group_type = \'' . $strGroupType . '\'
						AND fg.is_published = 1
						AND app.is_deleted = false
						AND app.occupancy_type_id = ' . ( int ) $objWaitlistFilter->getOccupancyTypeId() . '
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND app.property_unit_id IS NULL
					GROUP BY fg.id ' . $strSortBy;

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Function used for searching and filtering the waitlist by floorplan
	 *
	 * @param array $arrintRequiredPropertyFloorPlanIds
	 * @param array $arrintPropertyIds
	 * @param int $intCid
	 * @param CDatabase $objDatabase
	 * @param bool $boolIsFromBulkAssignUnits
	 * @param array $arrintParentChildPropertyIds
	 * @param bool $boolIncludeDeletedAA
	 * @param bool $boolIncludeDeletedFloorPlans
	 * @return array
	 */

	public static function fetchApplicationsByPropertyFloorPlanIdsByPropertyIdsByCid( $objDatabase, $objWaitlistFilter, $boolIsFromFloorplanWaitlistTab = false, $boolIsFromRenewal = false ) {

		if( false == valArr( $objWaitlistFilter->getPropertyFloorplanIds() ) ) return NULL;

		$strCheckDeletedAASql = ( false == $objWaitlistFilter->getIncludeDeletedAA() ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $objWaitlistFilter->getIncludeDeletedFloorPlans() ) ? ' AND pf.deleted_on IS NULL' : '';

		if( $objWaitlistFilter->getOccupancyTypeId() == COccupancyType::AFFORDABLE ) {
			$strConditionalJoin  = 'LEFT JOIN
                    (
                        SELECT
                            cid, customer_id, array_to_string( array_agg( sdt.name ), \',\') AS subsidy_disability_type_name
                        FROM
                            customer_subsidy_disability_types csdt
                        JOIN
                            subsidy_disability_types sdt ON ( csdt.subsidy_disability_type_id = sdt.id )
                        WHERE
                            csdt.cid = ' . ( int ) $objWaitlistFilter->getcId() . '
                        GROUP BY
                            customer_id, cid
                    ) sub_csdt ON ( sub_csdt.cid = app.cid and sub_csdt.customer_id = apl.customer_id ) ';
			$strConditionalSubAppsSelect = ', sub_csdt.subsidy_disability_type_name';
		}
		$strSqlConditionalSapceOptions = '';
		if( COccupancyType::MILITARY == $objWaitlistFilter->getOccupancyTypeId() ) {
			$strConditionalJoin .= 'LEFT JOIN customer_military_details cmd ON ( cmd.cid = apl.cid AND cmd.customer_id = apl.customer_id )
									LEFT JOIN space_configurations sc ON( sc.cid = app.cid AND sc.id = app.desired_space_configuration_id AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL )';
			$strConditionalSubAppsSelect .= ', cmd.eligibility_date';
			$strSqlConditionalSapceOptions .= ', sc.unit_space_count,sc.name as space_option_title';
		}

		$strCustomername = ' COALESCE ( apl.name_first || \' \', \'\' ) || apl.name_last applicant_name, ';

		if( true == $boolIsFromRenewal ) {
			$strCustomername = ' COALESCE ( apl.name_first || \' \', \'\' ) || apl.name_last || \' \' || COALESCE ( apl.name_last_matronymic, \'\') AS applicant_name, ';
		}

		$strPropertyIdCondition = ( true == valArr( $objWaitlistFilter->getParentChildPropertyIds() ) ) ? implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) : $objWaitlistFilter->getPropertyId();
		$strSql = 'SELECT
						app.id AS application_id,
						apl.id AS applicant_id,
						app.lease_id,
						CASE WHEN sar.id IS NOT NULL THEN
								sar.screening_recommendation_type_id
							WHEN aat.id is NOT NULL THEN
								aat.transmission_response_type_id
							ELSE
								0
						END AS screening_recommendation_type_id,
						' . $strCustomername . '
						app.application_completed_on,
						wla.wait_start_on,
						li.lease_start_date,
						COALESCE( ( select move_in_date from lease_processes lp where lp.cid = li.cid and lp.lease_id = li.lease_id and lp.customer_id is null ), li.lease_start_date ) as move_in_date,
						apl.username AS email_address,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS phone_number,
						pf.id AS property_floorplan_id,
						pf.floorplan_name,
						app.desired_bedrooms,
						app.desired_bathrooms,
						COALESCE ( sub_at.balance, 0 ) AS balance,
						CASE
						WHEN 0 >= COALESCE ( sub_at.balance, 0 ) THEN 0
						ELSE sub_at.balance
						END balance_for_sorting,
						wlp.name AS priority,
						app.wait_list_points AS point_value,
						ROW_NUMBER() OVER (PARTITION BY app.id ORDER BY app.id DESC) rn
						' . $strConditionalSubAppsSelect . '
						' . $strSqlConditionalSapceOptions . '
					FROM
						cached_applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.lease_interval_type_id = li.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.cid = app.cid AND aa.application_id = app.id ' . $strCheckDeletedAASql . ' AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN properties p ON ( p.cid = app.cid and p.id = app.property_id)
						JOIN wait_lists wl ON ( wl.cid = app.cid AND ( wl.property_group_id = p.id or wl.property_group_id = p.property_id ) AND wl.is_active IS TRUE AND app.occupancy_type_id = wl.occupancy_type_id )
						JOIN wait_list_applications wla ON ( wl.cid = wla.cid AND wl.id = wla.wait_list_id AND wla.wait_start_on IS NOT NULL AND wla.wait_end_on IS NULL AND app.id = wla.application_id )
						JOIN load_properties( ARRAY[ ' . ( int ) $objWaitlistFilter->getCid() . ' ]::INTEGER[], ARRAY[ ' . $strPropertyIdCondition . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						LEFT JOIN screening_application_requests sar ON ( sar.cid = app.cid AND app.id = sar.application_id AND sar.screening_recommendation_type_id IS NOT NULL )
						LEFT JOIN LATERAL ( SELECT wlp.name,wlp.value FROM wait_list_points wlp WHERE wlp.cid = wla.cid AND wla.wait_list_point_id = wlp.id ) AS wlp ON TRUE
						LEFT JOIN LATERAL ( SELECT MAX( t.id ) as id FROM transmissions t WHERE t.cid = app.cid AND app.id = t.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' ) AS t ON TRUE
                        LEFT JOIN LATERAL ( SELECT MAX( aat.id ) as id, aat.transmission_response_type_id FROM applicant_application_transmissions aat WHERE aat.cid = app.cid AND app.id = aat.application_id AND aat.applicant_application_id = aa.id AND t.id = aat.transmission_id GROUP BY aat.id, aat.transmission_response_type_id ) AS aat ON TRUE
						LEFT JOIN application_floorplans af ON ( af.cid = app.cid AND af.application_id = app.id )
						LEFT JOIN property_floorplans pf ON ( pf.cid = app.cid AND ( pf.id = app.property_floorplan_id OR pf.id = af.property_floorplan_id ) ' . $strCheckDeletedFloorPlansSql . ' ) ' . $strConditionalJoin;
		if( true == $objWaitlistFilter->getIsFromBulkAssignUnits() && false == is_null( $strPropertyIdCondition ) ) {
			$strSql .= 'LEFT JOIN
											(
											SELECT
												ai.application_id,
												ai.cid,
												ai.offer_submitted_on,
												ai.event_type_id,
												rank () OVER ( PARTITION BY ai.application_id ORDER BY ai.offer_submitted_on DESC NULLS LAST ) AS rank
											FROM
												application_interests ai
											WHERE
													ai.cid = ' . ( int ) $objWaitlistFilter->getcId() . '
													AND ai.property_id IN ( ' . $strPropertyIdCondition . ' )
											) sub_ai ON ( sub_ai.cid = app.cid AND sub_ai.application_id = app.id AND sub_ai.rank = 1 )';
		}
		$strSql .= 'LEFT JOIN
								(
								SELECT
									COALESCE ( sum ( at.transaction_amount ), 0 ) AS balance,
									at.cid,
									at.lease_id
								FROM
									ar_transactions at
								WHERE
									at.cid = ' . ( int ) $objWaitlistFilter->getcId() . '
									AND at.ar_code_type_id IN ( ' . implode( ', ', array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT ] ) ) . ' )
									AND at.property_id IN ( ' . $strPropertyIdCondition . ' )
								GROUP BY
									at.cid,
									at.lease_id
								) sub_at ON ( sub_at.cid = app.cid AND sub_at.lease_id = app.lease_id )
					WHERE
						app.cid = ' . ( int ) $objWaitlistFilter->getcId() . '
						AND app.occupancy_type_id = ' . ( int ) $objWaitlistFilter->getOccupancyTypeId() . '
						AND app.property_id IN ( ' . $strPropertyIdCondition . ' )
						AND ( CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_PARTIALLY_COMPLETED . ' THEN ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . ' ELSE ass.id END >= wl.application_stage_status_id OR wla.is_exception IS TRUE )
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND CASE WHEN aat.id IS NOT NULL AND sar.id IS NULL THEN
                        	 aat.id = (
	        	                SELECT
							          MAX ( aat1.id ) OVER ( PARTITION BY aat1.applicant_application_id ) AS max_transmission_id
							      FROM
							          applicant_application_transmissions aat1
						      WHERE
							          aat1.cid = app.cid
							          AND aat1.application_id = app.id
							          AND aat1.applicant_application_id = aa.id
                                      LIMIT 1
	                	         )
                           ELSE
                           		TRUE
                         END
						AND app.property_unit_id IS NULL
						AND app.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
						AND ( pf.id IN ( ' . implode( ',', $objWaitlistFilter->getPropertyFloorplanIds() ) . ' ) )
						AND ( ( app.property_floorplan_id IS NOT NULL
							OR pf.id IS NOT NULL )
							OR ( app.desired_bathrooms IS NOT NULL
							AND app.desired_bedrooms IS NOT NULL ) )';

		if( true == $objWaitlistFilter->getIsFromBulkAssignUnits() && false == is_null( $strPropertyIdCondition ) ) {
			$strSql .= ' AND ( sub_ai.event_type_id IS NULL
										OR sub_ai.event_type_id <> ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . ' ) ';
		}

		if( false == valStr( $objWaitlistFilter->getSortOrder() ) || false == valStr( $objWaitlistFilter->getSortColumn() ) ) {
			if( COccupancyType::MILITARY == $objWaitlistFilter->getOccupancyTypeId() ) {
				$strSql .= ' ORDER BY point_value DESC NULLS LAST, wlp.value DESC, li.lease_start_date,cmd.eligibility_date NULLS LAST';
			} else {
				$strSql .= ' ORDER BY app.wait_list_position NULLS LAST';
			}

		} elseif( true == valStr( $objWaitlistFilter->getSortOrder() ) && true == valStr( $objWaitlistFilter->getSortColumn() ) ) {
			$strSortColumn = $objWaitlistFilter->getSortColumn();
			$strSortOrder = ( 'asc' == $objWaitlistFilter->getSortOrder() ) ? $objWaitlistFilter->getSortOrder() . ' NULLS FIRST ' : $objWaitlistFilter->getSortOrder() . ' NULLS LAST ';
			$strMilitary = COccupancyType::MILITARY == $objWaitlistFilter->getOccupancyTypeId() ? ', cmd.eligibility_date ' . $strSortOrder : '';
			switch( $strSortColumn ) {
				case 'applicant_name':
				case 'sar.screening_recommendation_type_id':
					$strSql .= ' ORDER BY ' . $strSortColumn . ' ' . $strSortOrder . ', priority ' . $strSortOrder . ', li.lease_start_date ' . $strSortOrder . $strMilitary . ', wla.wait_start_on ' . $strSortOrder;
					break;

				case 'priority':
					$strSql .= ' ORDER BY priority ' . $strSortOrder . ', li.lease_start_date ' . $strSortOrder . $strMilitary . ', wla.wait_start_on ' . $strSortOrder;
					break;

				case 'cmd.eligibility_date':
					$strSql .= ' ORDER BY cmd.eligibility_date ' . $strSortOrder . ', priority ' . $strSortOrder . ', li.lease_start_date ' . $strSortOrder . ', wla.wait_start_on ' . $strSortOrder;
					break;

				case 'li.lease_start_date':
					$strSql .= ' ORDER BY li.lease_start_date ' . $strSortOrder . ', priority ' . $strSortOrder . $strMilitary . ', wla.wait_start_on ' . $strSortOrder;
					break;

				case 'wla.wait_start_on':
					$strSql .= ' ORDER BY wla.wait_start_on ' . $strSortOrder . ', priority ' . $strSortOrder . ', li.lease_start_date ' . $strSortOrder . $strMilitary;
					break;

				case 'serial':
					$strSql .= ' ORDER BY point_value DESC NULLS LAST, wlp.value DESC, li.lease_start_date' . $strMilitary;
					break;

				case 'floorplan_name':
					$strSql .= ' ORDER BY floorplan_name ' . $strSortOrder;
					break;

				case 'number_of_bedrooms':
				case 'number_of_bathrooms':
					$strSql .= ' ORDER BY ' . $strSortColumn . ' ' . $objWaitlistFilter->getSortOrder();
					break;

				default:
					if( 'floorplan' !== $objWaitlistFilter->getSortColumn() && 'id' !== $objWaitlistFilter->getSortColumn() && 'applications_count' !== $objWaitlistFilter->getSortColumn() && 'min_rent' !== $objWaitlistFilter->getSortColumn() ) {
							$strSql .= ' ORDER BY ' . $objWaitlistFilter->getSortColumn() . ' ' . $objWaitlistFilter->getSortOrder() . ' NULLS LAST, point_value DESC NULLS LAST,cmd.eligibility_date NULLS LAST';
					} else {
						$strSql .= ' ORDER BY app.wait_list_position NULLS LAST';
					}
					break;
			}
		}
		$strSql .= ', wla.wait_start_on ';
		if( false == $boolIsFromFloorplanWaitlistTab ) {
			$strSql = ' SELECT * FROM ( ' . $strSql . ') sub_query_result WHERE sub_query_result.rn = 1';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaMissingApplicationIdsByPropertyIdsByModulePermissionByCid( $arrintPropertyIds, $arrintSelectedCompanyEmployeeIds, $intCid, $objDatabase, $intCurrentEmployeeId = NULL ) {

	if( false == valArr( $arrintPropertyIds ) ) return [];

		$arrstrWhere = [];

		if( true == ValArr( $arrintPropertyIds ) ) {
			$arrstrWhere[] = 'app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql = ' SELECT
						DISTINCT( app.id )
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN property_transmission_vendors ptv ON( ptv.property_id = app.property_id AND ptv.cid = app.cid )
						JOIN company_transmission_vendors ctv ON( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )';
		$strSql .= 'WHERE
						app.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY;

		if( false == valArr( $arrintSelectedCompanyEmployeeIds ) ) {

			$strSql .= ' AND( CASE WHEN app.application_stage_id <> ' . CApplicationStage::LEASE . ' AND app.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN app.leasing_agent_id IS NULL
							ELSE true
						END ) ';

		} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintSelectedCompanyEmployeeIds ) && false == is_null( $intCurrentEmployeeId ) && true == array_key_exists( $intCurrentEmployeeId, $arrintSelectedCompanyEmployeeIds ) ) {
			$strSql .= ' AND( CASE WHEN app.application_stage_id <> ' . CApplicationStage::LEASE . ' AND app.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN app.leasing_agent_id IS NULL OR app.leasing_agent_id IN ( ' . implode( ',', $arrintSelectedCompanyEmployeeIds ) . ' )
							ELSE true
						END ) ';
		} else {
			$strSql .= ' AND ( app.leasing_agent_id IN ( ' . implode( ',', $arrintSelectedCompanyEmployeeIds ) . ' ) OR app.leasing_agent_id IS NULL AND li.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' ))';
		}

		$strSql .= ' AND app.application_stage_id = ' . CApplicationStage::LEASE . ' AND app.application_status_id = ' . CApplicationStatus::COMPLETED;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrintApplicationIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintApplicationIds ) ) return [];

		$arrintRekeyedApplicationIds = rekeyArray( 'id', $arrintApplicationIds );

		$arrintScreeningCriteriaMissingApplicantionIds 		  = [];
		$intTotalLatestApplicantApplicationTransmissionsCount = 0;
		$intTotalApplicantsCount							  = 0;
		$arrobjTransmissionConditions						  = [];
		$arrobjRekeyedTransmissionConditions				  = [];

		$arrintTotalLatestApplicantApplicationTransmissionCounts = ( array ) CApplicantApplicationTransmissions::fetchTotalLatestApplicantApplicationTransmissionsCountByApplicationIdsByCustomerTypeIdsByCid( array_keys( $arrintRekeyedApplicationIds ), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrintTotalApplicantCounts								 = ( array ) CApplicants::fetchTotalApplicantsCountByApplicationIdsByCustomerTypeIdsByCid( array_keys( $arrintRekeyedApplicationIds ), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrobjTransmissionConditions							 = ( array ) CTransmissionConditions::fetchTransmissionConditionsByApplicationIdsByCid( array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase );

		$arrstrEvents 		 									 = \Psi\Eos\Entrata\CEvents::createService()->fetchLatestScreeningEventsByEventTypeIdByApplicationIdsByCid( CEventType::SCREENING_DECISION, array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase );
		$arrstrRekeyedEvents 									 = ( true == valArr( $arrstrEvents ) ) ? rekeyArray( 'event_reference_id', $arrstrEvents ) : [];

		$arrobjRekeyedTransmissionConditions					 = rekeyObjects( 'ApplicationId', $arrobjTransmissionConditions, true );

		$arrmixApplicationConditionSetDetails 				 	 = rekeyArray( 'application_id', ( array ) CScreeningApplicationConditionSets::fetchScreeningApplicationConditionSetsByApplicationIdsByCid( array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase ) );

		$arrintApplicationIds = array_keys( $arrintRekeyedApplicationIds );

		foreach( $arrintApplicationIds as $intApplicationId ) {

			$intTotalLatestApplicantApplicationTransmissionsCount = ( true == array_key_exists( $intApplicationId, $arrintTotalLatestApplicantApplicationTransmissionCounts ) ) ? $arrintTotalLatestApplicantApplicationTransmissionCounts[$intApplicationId]['count'] : 0;
			$intTotalApplicantsCount							  = ( true == array_key_exists( $intApplicationId, $arrintTotalApplicantCounts ) ) ? $arrintTotalApplicantCounts[$intApplicationId]['applicant_count'] : 0;

			if( 0 != $intTotalLatestApplicantApplicationTransmissionsCount ) {

				if( $intTotalLatestApplicantApplicationTransmissionsCount != $intTotalApplicantsCount ) {
					$arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;

				} else {
					if( true == array_key_exists( $intApplicationId, $arrmixApplicationConditionSetDetails ) ) {
						$boolIsConditionSetSatisfied = ( true == array_key_exists( 'satisfied_on', $arrmixApplicationConditionSetDetails[$intApplicationId] ) && false == is_null( $arrmixApplicationConditionSetDetails[$intApplicationId]['satisfied_on'] ) ) ? true : false;

						if( false == $boolIsConditionSetSatisfied ) $arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;

					} else {
						$arrobjTransmissionConditions = ( true == valArr( $arrobjRekeyedTransmissionConditions ) && true == array_key_exists( $intApplicationId, $arrobjRekeyedTransmissionConditions ) ) ? $arrobjRekeyedTransmissionConditions[$intApplicationId] : [];

						if( true == valArr( $arrobjTransmissionConditions ) ) {
							$objApplication = new CApplication();

							$boolIsTransmissionConditionsSatisfied = $objApplication->getIsTransmissionConditionsSatisfied( $arrobjTransmissionConditions );

							// if transmission conditions are selected, but not satisfied then skip that application from approving its lease

							if( false == $boolIsTransmissionConditionsSatisfied ) {
								$arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;
							}
 						}
					}
				}
			}
		}

		return $arrintScreeningCriteriaMissingApplicantionIds;
	}

	public static function fetchActiveApplicationByIdByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicationId, $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.applicant_id = ' . ( int ) $intApplicantId . $strCheckDeletedAASql . ' )
						JOIN properties p ON ( p.id = app.property_id AND p.cid = app.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						AND to_char( COALESCE( app.application_datetime, app.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND app.id = ' . ( int ) $intApplicationId . '
					ORDER BY app.created_on DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApprovedApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsExcludeLeaseModification = false ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						' . ( ( true == $boolIsExcludeLeaseModification ) ? ' JOIN lease_intervals li ON ( li.cid = app.cid AND li.lease_id = app.lease_id AND li.id = app.lease_interval_id ) ' : '' ) . '
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.application_status_id != ' . CApplicationStatus::CANCELLED . '
						' . ( ( true == $boolIsExcludeLeaseModification ) ? ' AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '' : '' ) . '
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsIncludeCancelledApplication = true ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						' . ( ( false == $boolIsIncludeCancelledApplication ) ? ' AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . '' : '' ) . '
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;
		if( 0 >= $intLeaseIntervalId ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

    public static function fetchLastActiveApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

        if( false == valId( $intCid ) ) return NULL;
        if( false == valId( $intLeaseId ) ) return NULL;

        $strSql = 'SELECT
						app.*
					FROM
					    cached_leases cl 
					    JOIN lease_intervals li ON ( li.cid = cl.cid AND li.lease_id = cl.id AND li.lease_start_date <= cl.lease_start_date AND li.lease_status_type_id NOT IN (' . \CLeaseStatusType::APPLICANT . ',' . \CLeaseStatusType::CANCELLED . ' ) )
						JOIN applications app ON ( app.lease_id = cl.id AND app.cid = cl.cid )					 		
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND cl.id = ' . ( int ) $intLeaseId . '
						AND app.application_status_id = ' . \CApplicationStatus::APPROVED .  '
						AND app.application_stage_id = ' . \CApplicationStage::LEASE .  '
					ORDER BY
						li.lease_start_date DESC, app.created_on DESC
					LIMIT 1';

        return self::fetchApplication( $strSql, $objDatabase );

    }

	public static function fetchApplicationPropertyFloorplanIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						property_floorplan_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'property_floorplan_id', $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $intLeaseId, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intLeaseIntervalTypeId ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $arrintLeaseIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintLeaseIds ) || false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.lease_id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ' )
						AND ( app.application_stage_id, app.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' ) ';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsFieldsByIdsByCid( $arrintApplicationIds, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						app.id,
						ce.name_first || \' \' || ce.name_last as leasing_agent_name,
						pu.unit_number,
						pb.building_name,
						pfp.floorplan_name,
						li.lease_start_date
					FROM
						applications app
						JOIN lease_intervals li ON ( app.cid = li.cid AND app.lease_interval_id = li.id )
						LEFT JOIN company_employees ce ON ( app.cid = ce.cid AND app.leasing_agent_id = ce.id )
						LEFT JOIN unit_spaces us ON ( app.cid = us.cid AND app.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN property_units pu ON ( us.cid = pu.cid AND us.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( app.property_floorplan_id = pfp.id AND app.cid = pfp.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id IN ( ' . implode( ', ', $arrintApplicationIds ) . ' )';

		$arrmixData			= fetchData( $strSql, $objClientDatabase );
		$arrmixFinalData	= [];

		foreach( $arrmixData as $arrmixChunk ) {
			$arrmixFinalData[$arrmixChunk['id']] = $arrmixChunk;
		}

		return $arrmixFinalData;
	}

	public static function fetchApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						apps.*
					FROM
						leases l
						JOIN
							(
							SELECT
								rank() OVER ( PARTITION BY app.lease_id ORDER BY app.id ),
								app.*
							FROM
								applications app
							WHERE
								app.cid = ' . ( int ) $intCid . '
								AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
							) apps ON ( apps.cid = l.cid AND apps.lease_id = l.id AND apps.rank = 1 )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdsByCid( $arrintLeaseIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						apps.*
					FROM
						leases l
						JOIN
							(
							SELECT
								rank() OVER ( PARTITION BY app.lease_id ORDER BY app.id ),
								app.*
							FROM
								applications app
								JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
							WHERE
								app.cid = ' . ( int ) $intCid . '
								AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
								AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							) apps ON ( apps.cid = l.cid AND apps.lease_id = l.id AND apps.rank = 1 )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function filterScreeningFailedApplicationIdsByApplicationIdsByModulePermissionByCid( $arrintApplicationIds, $boolCanApproveApplicationWithoutScreening, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return [];

		$strSql = 'SELECT
						app.id,
						app.property_id
					FROM
						applications app
						JOIN property_transmission_vendors ptv ON ( ptv.property_id = app.property_id AND ptv.cid = app.cid )
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						AND app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';

		$arrmixRvEnabledApplicationDetails = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRvEnabledApplicationDetails ) ) return [];

		$arrintRvEnabledPropertyIds         = getArrayFieldValuesByFieldName( 'property_id', $arrmixRvEnabledApplicationDetails );
		$arrmixRvEnabledApplicationDetails  = rekeyArray( 'id', $arrmixRvEnabledApplicationDetails );
		// we need array_keys for multiple functions as an argument.
		$arrintRvEnabledApplicationIds      = array_keys( $arrmixRvEnabledApplicationDetails );

		$arrmixPropertyPreferences = CPropertyPreferences::fetchCustomPropertyPreferencesByPropertyIdsByKeysByCid( $arrintRvEnabledPropertyIds, [ 'SCREEN_AFTER_LEASE_COMPLETED' ], $intCid, $objDatabase );
		$arrintScreeningFailedApplicationIds = [];

		$intTotalLatestApplicantApplicationTransmissionsCount = 0;
		$intTotalApplicantsCount							  = 0;
		$arrobjTransmissionConditions						  = [];
		$arrobjRekeyedTransmissionConditions				  = [];

		$arrobjLatestApplicantApplicationTransmissions 	= rekeyObjects( 'ApplicationId', ( array ) CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionsByApplicationIdsByCustomerTypeIdsByCid( $arrintRvEnabledApplicationIds, [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT, CCustomerType::GUARANTOR ], $intCid, $objDatabase ), true );
		$arrintTotalApplicantCounts 					= ( array ) CApplicants::fetchTotalApplicantsCountByApplicationIdsByCustomerTypeIdsByCid( $arrintRvEnabledApplicationIds, CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrobjTransmissionConditions					= ( array ) CTransmissionConditions::fetchTransmissionConditionsByApplicationIdsByCid( $arrintRvEnabledApplicationIds, $intCid, $objDatabase );

		$arrmixApplicationConditionSetDetails 			= rekeyArray( 'application_id', ( array ) CScreeningApplicationConditionSets::fetchScreeningApplicationConditionSetsByApplicationIdsByCid( $arrintRvEnabledApplicationIds, $intCid, $objDatabase ) );

		$arrobjRekeyedTransmissionConditions			= rekeyObjects( 'ApplicationId', $arrobjTransmissionConditions, true );
		foreach( $arrmixRvEnabledApplicationDetails as $intApplicationId => $arrintRvEnabledApplicationId ) {

			$intTotalLatestApplicantApplicationTransmissionsCount = ( true == array_key_exists( $intApplicationId, $arrobjLatestApplicantApplicationTransmissions ) ) ? \Psi\Libraries\UtilFunctions\count( $arrobjLatestApplicantApplicationTransmissions[$intApplicationId] ) : 0;
			$intTotalApplicantsCount							  = ( true == array_key_exists( $intApplicationId, $arrintTotalApplicantCounts ) ) ? $arrintTotalApplicantCounts[$intApplicationId]['applicant_count'] : 0;

			if( 0 == $intTotalLatestApplicantApplicationTransmissionsCount ) {
				if( true == valArr( $arrmixPropertyPreferences ) && true == array_key_exists( $arrintRvEnabledApplicationId['property_id'], $arrmixPropertyPreferences ) ) {
					return [];
				}

				// if not screened then check for permission approve application without screening
				if( false == $boolCanApproveApplicationWithoutScreening ) {
					$arrintScreeningFailedApplicationIds[] = $intApplicationId;
				}

			} else {
				if( $intTotalLatestApplicantApplicationTransmissionsCount != $intTotalApplicantsCount ) {
					$arrintScreeningFailedApplicationIds[] = $intApplicationId;

				} else {
					if( true == array_key_exists( $intApplicationId, $arrmixApplicationConditionSetDetails ) ) {
						$boolIsConditionSetSatisfied = ( true == array_key_exists( 'satisfied_on', $arrmixApplicationConditionSetDetails[$intApplicationId] ) && false == is_null( $arrmixApplicationConditionSetDetails[$intApplicationId]['satisfied_on'] ) ) ? true : false;

						if( false == $boolIsConditionSetSatisfied ) $arrintScreeningFailedApplicationIds[] = $intApplicationId;

					} else {
						$arrobjTransmissionConditions = ( true == valArr( $arrobjRekeyedTransmissionConditions ) && true == array_key_exists( $intApplicationId, $arrobjRekeyedTransmissionConditions ) ) ? $arrobjRekeyedTransmissionConditions[$intApplicationId] : [];

						if( true == valArr( $arrobjTransmissionConditions ) ) {
							$objApplication = new CApplication();

							$boolIsTransmissionConditionsSatisfied = $objApplication->getIsTransmissionConditionsSatisfied( $arrobjTransmissionConditions );

							// if transmission conditions are selected, but not satisfied then skip that application from approving
							if( false == $boolIsTransmissionConditionsSatisfied ) {
								$arrintScreeningFailedApplicationIds[] = $intApplicationId;
							}
						} else {

							// If screening recommendation is not pass, then skip application from approving
							$arrobjPrimaryCoApplicantsLatestApplicantApplicationTransmissions = ( true == array_key_exists( $intApplicationId, $arrobjLatestApplicantApplicationTransmissions ) ) ? $arrobjLatestApplicantApplicationTransmissions[$intApplicationId] : [];
							if( CScreeningRecommendationType::PASS != CResidentVerifyScreeningController::loadPrimaryCoapplicantsApplicationScreeningStatusId( $arrobjPrimaryCoApplicantsLatestApplicantApplicationTransmissions ) ) {
								$arrintScreeningFailedApplicationIds[] = $intApplicationId;
							}
						}
					}
				}
			}
		}
		return $arrintScreeningFailedApplicationIds;
	}

	public static function fetchApplicationsByEventTypeIdByLeaseIntervalTypeIdByPsProductIdByPropertyIdsByCidByFormFilter( $intEventTypeId, $intLeaseIntervalTypeId, $intProductId, $arrintPropertyIds, $intCid, $objFormFilter, $objDatabase ) {
		if ( false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintPropertyIds ) || false == valObj( $objFormFilter, 'CFormFilter' ) ) return NULL;

		$strSql = 'SELECT
						app.id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN events e ON ( e.cid = app.cid AND e.lease_interval_id = app.lease_interval_id AND e.event_type_id = ' . ( int ) $intEventTypeId . ' )
						JOIN properties p ON ( p.cid = app.cid AND p.id = app.property_id )
						JOIN time_zones tz ON ( tz.id = p.time_zone_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.ps_product_id = ' . ( int ) $intProductId . '
						AND e.created_by < \' 10000 \'
						AND ' . $objFormFilter->getFilterObject( 'period' )->getDateComparisonSql( 'DATE_TRUNC( \'day\', app.application_datetime ) AT TIME ZONE tz.time_zone_name ', NULL, 'property_id' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByCallIdsByUserIdsByLeaseIntervalTypeIdByPsProductIdByPropertyIdsByCidByDate( $arrintCallIds, $arrintUserIds, $intLeaseIntervalTypeId, $intProductId, $arrintPropertyIds, $intCid, $objFormFilter, $objDatabase ) {
		if ( false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintUserIds ) || false == valArr( $arrintCallIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT( app.id ) AS application_id,
						app.call_id,
						app.property_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN properties p ON ( p.cid = app.cid AND p.id = app.property_id )
						JOIN time_zones tz ON ( tz.id = p.time_zone_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.ps_product_id = ' . ( int ) $intProductId . '
						--AND app.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' ) // Leasing center team is working on why guest cards are not created by leasing agent.
						AND app.call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )
						AND ' . $objFormFilter->getFilterObject( 'period' )->getDateComparisonSql( 'DATE_TRUNC( \'day\', app.application_datetime ) AT TIME ZONE tz.time_zone_name ', NULL, 'property_id' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdsByCallFilter( $arrintLeaseIntervalTypeIds, $objCallFilter, $objDatabase ) {
		if ( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valStr( $objCallFilter->getCids() ) || false == valStr( $objCallFilter->getCallIds() ) || false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		if( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_WEEKLY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'week\', app.application_datetime + INTERVAL \'6 days\' )::date AS application_datetime_format, ';
		} elseif( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_DAILY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'day\', app.application_datetime )::date AS application_datetime_format, ';
		} elseif( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_MONTHLY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'month\', app.application_datetime )::date AS application_datetime_format, ';
		}

		$strSql = 'SELECT
						DISTINCT( app.id ),
						app.lease_interval_id,
						' . $strSelect . '
						 COUNT( app.id ) AS application_count
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid IN ( ' . $objCallFilter->getCids() . ' )
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND app.ps_product_id IN ( ' . $objCallFilter->getPsProductIds() . ' )
						AND app.call_id IN ( ' . $objCallFilter->getCallIds() . ' )
						AND app.application_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'
					GROUP BY
						application_datetime_format,
						app.id,
						app.lease_interval_id
					ORDER BY
						application_datetime_format ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationCountByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						app.property_id,
						COUNT( id ) AS guest_cards_inserted_count
					FROM
						applications app
					WHERE
						app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND app.cid = ' . ( int ) $intCid . '
					GROUP BY app.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationCountByApplicationIdsByApplicationStageStatusIdsByCid( $arrintApplicationIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.property_id,
						COUNT( app.id ) AS leases_generated_count
					FROM
						applications app
					WHERE
						app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND app.cid = ' . ( int ) $intCid . '
						AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					GROUP BY app.property_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByExcludingApplicationStageStatusIdsByCid( $intLeaseId, $intLeaseIntervalTypeId, $arrintExcludingApplicationStageStatusIds, $intCid, $objDatabase, $arrintLeaseStatusTypes = [] ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intLeaseIntervalTypeId ) || false == valArr( $arrintExcludingApplicationStageStatusIds ) ) return NULL;

		$strLeaseStatusTypeCondition = NULL;
		if( true == valArr( $arrintLeaseStatusTypes ) ) {
			$strLeaseStatusTypeCondition = ' AND li.lease_status_type_id IN ( ' . sqlIntImplode( $arrintLeaseStatusTypes ) . ' ) ';
		}

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN leases l ON( l.cid = app.cid AND l.property_id = app.property_id AND l.id = app.lease_id )
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id AND li.lease_id = app.lease_id )
					WHERE
						li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . $strLeaseStatusTypeCondition . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND l.active_lease_interval_id <> li.id
						AND (app.application_stage_id,app.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintExcludingApplicationStageStatusIds ) . ' )
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByAppRemotePrimaryKeyOrLeaseIntervalIdByPropertyIdByCid( $strAppRemotePrimaryKey, $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = 'lower( app.app_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strAppRemotePrimaryKey ) ) . '\'';

		if( true == valStr( $intLeaseIntervalId ) ) $strCondition = '( lower( app.app_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strAppRemotePrimaryKey ) ) . '\' OR app.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ')';

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND ' . $strCondition;

		$strSql .= ' ORDER BY app.id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByGuestRemotePrimaryKeyOrLeaseIntervalIdByPropertyIdByCid( $strGuestRemotePrimaryKey, $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = 'lower( app.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\'';

		if( true == valStr( $intLeaseIntervalId ) ) $strCondition = '( lower( app.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\' OR app.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ')';

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND ' . $strCondition;

		$strSql .= ' ORDER BY app.id DESC LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdsByCid( $intLeaseIntervalTypeId, $arrintLeaseIds, $intCid, $objDatabase, $arrintLeaseIntervalIds = NULL ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ' .
						( ( true == valArr( $arrintLeaseIntervalIds ) ) ? 'AND app.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ')' : '' ) . '
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByIdsByCustomerTypeIdsByPropertyIdByCid( $arrintApplicationIds, $arrintCustomerTypeIds, $intPropertyId, $intCid, $boolIncludeOnlyPendingSigners = false, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						apps.*,
						aa.id as applicant_application_id,
						a.id as applicant_id,
						a.name_first,
						a.name_last,
						a.username as email_address,
						a.mobile_number as phone_number,
						c.preferred_locale_code
					FROM
						applications apps
						JOIN applicant_applications aa ON ( aa.application_id = apps.id AND aa.cid = apps.cid )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN customers c ON ( a.customer_id = c.id AND a.cid = c.cid )
					WHERE
						apps.cid = ' . ( int ) $intCid . '
						AND apps.property_id = ' . ( int ) $intPropertyId . '
						AND apps.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND aa.deleted_by IS NULL
						AND aa.deleted_on IS NULL ';

		if( true == $boolIncludeOnlyPendingSigners ) {
			$strSql .= ' AND aa.lease_signed_on IS NULL';
		}

		return self::fetchApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchApplicationCountByPropertyGroupIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCidGroupedByApplicationStageStatusId( $arrintPropertyGroupIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND a.leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

        $arrintNonActiveApplicationStatusIds = [
            CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
            CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
            CApplicationStage::LEASE                => [ CApplicationStatus::PARTIALLY_COMPLETED ],
        ];

        $strSql = 'SELECT
						ass.id as application_stage_status_id,
						count( app.id ) as total_prospects,
						count( app_1.id ) as new_prospects
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) as lp ON ( lp.property_id = app.property_id AND lp.is_disabled = 0 )
						JOIN application_stage_statuses ass ON ( app.application_stage_id = ass.application_stage_id AND app.application_status_id = ass.application_status_id AND li.lease_interval_type_id = ass.lease_interval_type_id  )
						LEFT JOIN applications app_1 ON ( app.id = app_1.id AND app_1.cid = app.cid AND DATE_TRUNC ( \'day\', app.application_datetime ) BETWEEN ( CURRENT_DATE - 30 ) AND CURRENT_DATE )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND DATE_TRUNC( \'day\', app.application_datetime ) BETWEEN \'' . '01/01/2004' . '\'  AND \'' . '12/31/2024' . '\'
						AND (app.application_stage_id,app.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintNonActiveApplicationStatusIds ) . ' )
						' . $strCompanyEmployeeSql . '
					GROUP BY
						ass.id';

		$arrmixApplicationCounts = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixApplicationCounts ) ) {

			$arrmixApplicationCounts = rekeyArray( 'application_stage_status_id', $arrmixApplicationCounts );

		}

		return $arrmixApplicationCounts;
	}

	public static function fetchApplicationWithPropertyFloorPlansByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						app.id,
						aa.id as applicant_application_id,
						app.cid,
						a.id as applicant_id,
						a.name_first,
						a.name_last,
						a.username as email_address,
						app.desired_bedrooms AS desired_bedrooms,
						app.desired_bathrooms AS desired_bathrooms,
						li.lease_start_date AS move_in_date,
						lt.term_month AS lease_term,
						app.created_on AS created_on,
						appt.name_first AS name_first,
						appt.name_last AS name_last,
						appt.phone_number AS phone_number,
						appt.email_address AS email_address,
						pf.floorplan_name AS floorplan_name
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN lease_terms lt ON ( lt.cid = li.cid AND lt.id = li.lease_term_id )
						JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						LEFT JOIN property_floorplans pf ON ( app.property_floorplan_id = pf.id AND app.cid = pf.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						app.cid	= ' . ( int ) $intCid . '
						AND app.id = ' . ( int ) $intApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalIdByFieldNamesByCid( $intLeaseId, $intLeaseIntervalId, $arrstrFieldNames, $intCid, $objDatabase, $boolOnlyPrimaryApplicant = false ) {
		if( false == valArr( $arrstrFieldNames ) ) return NULL;

		$strSql = 'SELECT
						' . implode( ', ', $arrstrFieldNames ) . '
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND app.id = aa.application_id )
						JOIN lease_terms lt ON ( app.cid = lt.cid AND li.lease_term_id = lt.id )
						LEFT JOIN lease_start_windows lsw ON ( app.cid = lsw.cid AND li.lease_start_window_id = lsw.id AND lsw.lease_term_id = lt.id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.lease_interval_id = ' . ( int ) $intLeaseIntervalId;
		$strSql .= ( true == $boolOnlyPrimaryApplicant ) ? ' AND aa.customer_type_id = 1 ' : '';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApplicationsByLeaseIdByLeaseIntervalIdsByCid( $intLeaseId, $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = '	SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
					ORDER BY
						app.created_on DESC';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsWithoutLeaseRenewalPdfsByApplicationStageStatusIdsByCid( $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintApplicationStageStatusIds ) || false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = '	SELECT
						app.*,						aa.offer_sent_on					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN quotes q ON ( app.cid = q.cid AND app.id = q.application_id AND q.cancelled_on IS NULL )
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND aa.application_id = app.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN file_associations fa ON ( fa.cid = app.cid AND fa.application_id = app.id AND fa.cid = ' . ( int ) $intCid . ' AND fa.deleted_by IS NULL AND fa.quote_id = q.id )
						LEFT JOIN files f ON ( f.cid = fa.cid AND f.id = fa.file_id AND f.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN file_types ft ON ( ft.cid = f.cid AND ft.id = f.file_type_id AND ft.system_code = \'ROS\' AND ft.cid = ' . ( int ) $intCid . ' )
					WHERE
						li.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND fa.id IS NULL
						AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchLatestScheduleTransferApplicationByTransferLeaseIdLeaseIntervalTypeIdByByCid( $intTransferLeaseId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intTransferLeaseId ) return NULL;

		$strSql = '
					SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intTransferLeaseId . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
					ORDER BY
						app.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );

	}

	public static function fetchLatestRenewalApplicationIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						app.id AS application_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )

					WHERE
						app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					ORDER BY
						app.id  DESC
					LIMIT
						1';

		return self::fetchColumn( $strSql, 'application_id', $objDatabase );

	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByLeaseStatusTypeIdsByExcludingApplicationStageStatusIds( $intLeaseId, $intLeaseIntervalTypeId, $arrintLeaseStatusTypeIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						app.id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND li.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND (app.application_stage_id,app.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					LIMIT
						1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyUnitIdByApplicationStageStatusIdsByCid( $intPropertyUnitId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_unit_id = ' . ( int ) $intPropertyUnitId . '
						AND ( app.application_stage_id, app.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsWithPropertyFloorplanIdsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND property_floorplan_id IS NOT NULL';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStausIdsByLeasesIdsByPropertyIdByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valId( $intCid ) ) return false;

		$strSql	= '	SELECT
						app.lease_id,
						ass.id,
						ass.application_stage_id as stage_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN leases l ON ( l.cid = app.cid AND l.id = app.lease_id AND l.active_lease_interval_id = app.lease_interval_id )
						JOIN application_stage_statuses ass ON( li.lease_interval_type_id = ass.lease_interval_type_id AND app.application_stage_id = ass.application_stage_id AND app.application_status_id =ass.application_status_id )
					WHERE
						app.cid	= ' . ( int ) $intCid . '
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdByCid( $arrintTransferLeaseIds, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintTransferLeaseIds ) ) return NULL;

		$strSql = ' SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id IN ( ' . implode( ',', $arrintTransferLeaseIds ) . ' )
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
					ORDER BY
						app.created_on DESC';

		return self::fetchApplications( $strSql, $objDatabase );

	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalIdsByCid( $arrintLeaseIds, $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						app.*,
						li.lease_start_date,
						li.lease_end_date,
						li.lease_interval_type_id,
						li.lease_term_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND li.id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
					ORDER BY
						app.created_on DESC';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageIdIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						application_stage_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'application_stage_id', $objDatabase );
	}

	public static function fetchApplicationsByIdsByLeaseIdsByCidByLeaseIntervalTypeId( $arrintApplicationIds, $arrintLeaseIds, $intCid, $objDatabase, $intLeaseIntervalTypeId = CLeaseIntervalType::RENEWAL ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintApplicationIds ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND app.lease_id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ' )
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStatusIdIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						application_status_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'application_status_id', $objDatabase );
	}

	public static function fetchApplicantsByApplicatonIdsFloorplanIdByUnitSpaceIdByCid( $arrintApplicationIds, $intPropertyFloorplanId, $intUnitSpaceId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$arrintArCodeTypeIds = array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT, CArCodeType::REFUND ] );

		$strSql = 'SELECT
						app.cid,
						ROW_NUMBER() OVER (ORDER BY apl.id) AS number,
						app.id application_id,
						app.lease_id,
						app.property_id,
						app.application_completed_on,
						li.lease_start_date,
						app.screening_approved_on,
						apl.id AS applicant_id,
						apl.name_first || \' \' || COALESCE ( apl.name_middle, \'\' ) || apl.name_last AS applicant_name,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
						pf.id AS property_floorplan_id,
						pf.floorplan_name,
						pf.number_of_bedrooms,
						pf.number_of_bathrooms,
						COALESCE ( sub_art.balance, 0 ) AS actual_balance
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( aa.cid = app.cid AND aa.application_id = app.id )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN lease_customers lc ON ( lc.cid = apl.cid AND lc.customer_id = apl.customer_id AND lc.lease_id = app.lease_id )
						LEFT JOIN application_floorplans af ON ( af.cid = app.cid AND af.application_id = app.id )
						LEFT JOIN property_floorplans pf ON ( pf.cid = app.cid AND ( pf.id = af.property_floorplan_id OR pf.id = app.property_floorplan_id ) AND pf.deleted_on IS NULL )
						LEFT JOIN
						(
							SELECT
								sum ( art.transaction_amount ) AS balance,
								art.cid,
								art.lease_id
							FROM
								ar_transactions art
							WHERE
								art.cid = ' . ( int ) $intCid . '
								AND art.ar_code_type_id IN  ( ' . implode( ',', $arrintArCodeTypeIds ) . ' )
							GROUP BY
								art.cid,
								art.lease_id
						) sub_art ON ( sub_art.cid = app.cid AND sub_art.lease_id = app.lease_id )
						LEFT JOIN
						(
							SELECT
								ai.application_id,
								ai.cid,
								ai.event_type_id,
								rank() OVER ( PARTITION BY ai.application_id ORDER BY ai.offer_submitted_on DESC NULLS LAST ) AS rank
							FROM
								application_interests ai
							WHERE
								ai.cid = ' . ( int ) $intCid . '
								AND ai.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						) sub_ai ON ( sub_ai.cid = app.cid AND sub_ai.application_id = app.id AND sub_ai.rank = 1 )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND app.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND pf.id = ' . ( int ) $intPropertyFloorplanId . '
						AND app.application_completed_on IS NOT NULL
						AND app.unit_space_id IS NULL
						AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
						AND sub_ai.event_type_id IS NULL
						AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByUnitSpaceIdByIntervalByIdByCid( $intPropertyId, $intUnitSpaceId, $intApplicationId, $strInterval, $intCid, $objDatabase ) {

		$strSql = ' SELECT
    					*
					FROM
    					applications
					WHERE
    					CID = ' . ( int ) $intCid . '
    					AND property_id = ' . ( int ) $intPropertyId . '
    					AND unit_space_id = ' . ( int ) $intUnitSpaceId . '
    					AND id <> ' . ( int ) $intApplicationId . '
    					AND created_on > ( NOW ( ) - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByCallFilter( $intLeaseIntervalTypeId, $objCallFilter, $objDatabase ) {
		if( false == valId( $intLeaseIntervalTypeId ) || false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT( app.id )
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $objCallFilter->getCid() . '
						AND li.lease_interval_type_id =  ' . ( int ) $intLeaseIntervalTypeId . '
						AND app.ps_product_id = ' . ( int ) $objCallFilter->getPsProductIds() . '
						AND app.call_id IN ( ' . $objCallFilter->getCallIds() . ' )';

		if( true == valId( $objCallFilter->getPropertyId() ) ) {
			$strSql .= ' AND app.property_id = ' . ( int ) $objCallFilter->getPropertyId();
		}

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND DATE_TRUNC( \'day\', app.application_datetime ) BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						 AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						aa.application_id
					FROM
						applicant_applications aa
						JOIN applicants a ON aa.applicant_id = a.id AND aa.cid = a.cid
					WHERE
						a.customer_id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalApplicationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT app.* FROM applications app JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id ) WHERE app.lease_id = %d AND app.cid = %d AND li.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ')', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRenewalApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}
		 $strSql = 'SELECT
						a.*
					FROM
						applications a
						JOIN lease_intervals li ON (li.cid = a.cid AND li.lease_id = a.lease_id AND li.id = a.lease_interval_id)
					WHERE
						a.lease_id = ' . ( int ) $intLeaseId . '
						AND a.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND a.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
						AND cancelled_on is NULL
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchCachedApplicationByCustomFieldsByCustomerIdByPropertyIdsByCid( $arrmixCustomFields, $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrmixCustomFields ) ) return NULL;

		$strSql = '	SELECT
						app.' . implode( ',', $arrmixCustomFields ) . '
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						JOIN applicant_applications aa ON ( app.id = aa.application_id AND app.cid = aa.cid )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN file_associations fa ON( fa.application_id = app.id AND fa.applicant_id = appt.id AND fa.cid = app.cid )
						JOIN files f ON ( f.id = fa.file_id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' AND fa.cid = ft.cid )
					WHERE
						aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND appt.cid = ' . ( int ) $intCid . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND li.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ',' . CLeaseIntervalType::TRANSFER . ',' . CLeaseIntervalType::APPLICATION . ' )
						AND CASE WHEN app.application_stage_id = ' . CApplicationStage::LEASE . ' AND app.application_status_id = ' . CApplicationStatus::APPROVED . ' THEN
							app.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							true
						END
							ORDER BY app.created_on DESC
					LIMIT 1 ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['id'] : '';

	}

	public static function fetchLatestApplicationByCustomFieldsByByCustomerIdByCid( $arrmixCustomFields, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valArr( $arrmixCustomFields ) )  return NULL;

		$strSql = '	SELECT
						app.' . implode( ',', $arrmixCustomFields ) . '
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND app.id = aa.application_id )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.customer_id::int = ' . ( int ) $intCustomerId . ' )
					WHERE
						app.cid::int = ' . ( int ) $intCid . '
						ORDER BY app.id DESC LIMIT 1 ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['lease_id'] : '';

	}

	public static function fetchApplicationWithPropertyFloorplanIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						property_floorplan_id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND property_floorplan_id IS NOT NULL
					ORDER BY id ASC LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['property_floorplan_id'] : NULL;
	}

	public static function fetchApplicationDataByLeaseIdByActiveLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						app.id,
						app.primary_applicant_id
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id = ' . ( int ) $intLeaseId . '
						AND app.lease_interval_id = ' . ( int ) $intLeaseIntervalId;
		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchCustomerIdsByLeaseIntervalIdByCustomerTypeIdsByPropertyIdByCid( $intLeaseIntervalId, $arrintCustomerTypeIds, $intPropertyId, $intCid, $objDatabase ) {

	    if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

	    $strSql = ' SELECT
					   a.customer_id
    				FROM
    					applications app
    					JOIN applicant_applications aa ON ( aa.cid = app.cid AND aa.application_id = app.id )
    					JOIN applicants a ON ( aa.cid = a.cid AND aa.applicant_id = a.id )
    				WHERE
    					app.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
    					AND app.cid = ' . ( int ) $intCid . '
    					AND app.property_id = ' . ( int ) $intPropertyId . '
    					AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' );';

	    $arrmixValues = ( array ) fetchData( $strSql, $objDatabase );

	    $arrintCustomerIds = [];

	    foreach( $arrmixValues as $arrmixValue ) {
	        $arrintCustomerIds[] = $arrmixValue['customer_id'];
	    }

	    return $arrintCustomerIds;
	}

	public static function fetchApplicationDataByApplicationIdByCid( $intApplicationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT
						lease_id,
						property_id
					FROM
						applications
					WHERE
						id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;
		$arrstrData = fetchData( $strSql, $objClientDatabase );
		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchApplicationDetailsByLeaseIntervalIdByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

			$strSql = 'SELECT
							app.property_floorplan_id,
							li.lease_start_window_id,
							app.space_configuration_id,
							app.lease_interval_id,
							app.id AS application_id
						FROM
							applications app
							JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
						WHERE
							app.cid = ' . ( int ) $intCid . '
							AND app.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )';

	    return rekeyArray( 'lease_interval_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchLastContractualApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN lease_intervals li ON ( li.cid = ca.cid AND li.property_id = ca.property_id AND li.lease_id = ca.lease_id AND ca.lease_interval_id = li.id AND li.lease_start_date <= ca.lease_start_date )
					WHERE
						li.cid = ' . ( int ) $intCid . '
						AND li.lease_id = ' . ( int ) $intLeaseId . '
						AND li.lease_interval_type_id NOT IN (' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
						AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::CANCELLED . ' )
					ORDER BY
						li.lease_start_date DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function cancelUnUsedApplicationsByApplicationIdByCid( $intLeaseId, $intCid, $objDatabase, $intApplicationId ) {

		if( false === valId( $intLeaseId ) || false === valId( $intCid ) ) return NULL;

		$intApplicationId = ( true === valId( $intApplicationId ) ) ? ( int ) $intApplicationId : 'NULL::INTEGER';

		$strSql = 'SELECT cancel_unused_applications( ' . ( int ) $intCid . ', ' . $intApplicationId . ' , ' . ( int ) $intLeaseId . ' ) AS count ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdApplicationStageIdByApplicationStatusIdByCid( $intPropertyId, $intApplicationStageId, $intApplicationStatusId, $intCid, $objDatabase, $strLeadDate = NULL ) {

		if( true == empty( $intPropertyId ) || true == empty( $intCid ) || true == empty( $intApplicationStageId ) || true == empty( $intApplicationStatusId ) ) return NULL;

		$strWhere = ( true == valStr( $strLeadDate ) ) ? ' AND app.updated_on <= \'' . $strLeadDate . ' 00:00:00\'' : '';

		$strSql = '
					WITH application_leases AS (
												SELECT
													DISTINCT cl.id
												FROM
													cached_leases cl
													LEFT JOIN ar_transactions sub_at ON( sub_at.cid = cl.cid AND sub_at.property_id = cl.property_id AND sub_at.lease_id = cl.id AND ( sub_at.ar_code_type_id = ' . CArCodeType::DEPOSIT . ' OR sub_at.ar_trigger_id = ' . CArTrigger::BAD_DEBT_WRITE_OFF . ' ) )
													LEFT JOIN ap_details ad ON (sub_at.cid = ad.cid AND sub_at.id = ad.refund_ar_transaction_id AND sub_at.property_id = ad.property_id )
												WHERE
													cl.cid = ' . ( int ) $intCid . '
													AND cl.property_id IN ( ' . ( int ) $intPropertyId . ' )
													AND cl.lease_status_type_id = ' . CLeaseStatusType::CANCELLED . '
													AND cl.total_balance = 0
													AND sub_at.lease_id IS NULL
													AND ad.id IS NULL
					)
					SELECT
						app.id,
						app.name_first,
						app.name_last,
						cl.id as lease_id
					FROM
						cached_applications app
						JOIN application_stages aps ON ( aps.id = app.application_stage_id )
						JOIN application_statuses aas ON ( aas.id = app.application_status_id )
						LEFT JOIN lease_intervals li ON ( li.cid = app.cid AND app.lease_interval_id = li.id )
						LEFT JOIN cached_leases cl ON ( cl.cid = li.cid AND cl.active_lease_interval_id = li.id  )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.is_deleted = false
						' . $strWhere . '
						AND ( app.lease_id IS NULL OR cl.id IN (
																SELECT
																	id
																FROM
																	application_leases
																)
							)
						AND app.application_stage_id = ' . ( int ) $intApplicationStageId . '
					    AND app.application_status_id = ' . ( int ) $intApplicationStatusId;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByApplicationStageStatusIdsByCid( $intPropertyId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		 $strSql = 'SELECT
						aa.id, aa.applicant_id, aa.application_id, app.application_status_id, app.lease_interval_id
					FROM
						applicant_applications aa
						JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.app_remote_primary_key IS NULL
						AND app.guest_remote_primary_key IS NOT NULL
						AND app.application_datetime > ' . '\'' . date( 'Y-m-d H:i:s', strtotime( '-90 days' ) ) . '\'' . '
						AND ( app.application_stage_id, app.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		$arrintApplicantIds = [];
		$arrintApplicantApplicationsIds = [];
		$arrintApplicationIds = [];
		$arrintApplicantAndApplicationAndApplicantApplicationIds = [];
		$arrintLeaseIntervalIds = [];

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrintApplicantIds[] = $arrmixResponse['applicant_id'];
				$arrintApplicantApplicationsIds[] = $arrmixResponse['id'];
				$arrintApplicationIds[] = $arrmixResponse['application_id'];
				$arrintLeaseIntervalIds[] = $arrmixResponse['lease_interval_id'];
			}
		}
		$arrintApplicantAndApplicationAndApplicantApplicationIds['applicant_id'] = $arrintApplicantIds;
		$arrintApplicantAndApplicationAndApplicantApplicationIds['id'] = $arrintApplicantApplicationsIds;
		$arrintApplicantAndApplicationAndApplicantApplicationIds['application_id'] = $arrintApplicationIds;
		$arrintApplicantAndApplicationAndApplicantApplicationIds['lease_interval_id'] = $arrintLeaseIntervalIds;

		return $arrintApplicantAndApplicationAndApplicantApplicationIds;
	}

	public static function fetchLatestSubsidyApprovedApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						a.*
					FROM
						applications a
						JOIN subsidy_certifications sc ON ( sc.application_id = a.id AND sc.cid = a.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_id = ' . ( int ) $intLeaseId . '
						AND a.application_status_id != ' . CApplicationStatus::CANCELLED . '
					ORDER BY
						a.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByCallIdsByCallFilterByCid( $arrintCallIds, $objCallsFilter, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintCallIds ) ) return;

		$arrstrWhere 	= [];
		$strGenericData = trim( $objCallsFilter->getGenericData() );

		if( ( false == empty( $strGenericData ) ) && ( 'Find a Call ...' != trim( $objCallsFilter->getGenericData() ) ) ) {
			$arrstrApplicantNames = explode( ' ', trim( $objCallsFilter->getGenericData() ) );

			if( true == valArr( $arrstrApplicantNames ) ) {
				foreach( $arrstrApplicantNames as $arrstrApplicantName ) {
					$strName					= addslashes( $arrstrApplicantName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$arrstrWhere[] = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName		= array_pop( $arrstrApplicantNames );
				$arrstrWhere[]	= '( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( app.call_id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR lower( cpn.phone_number ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						appt.id AS applicant_id, appt.name_last, appt.name_first, cpn.phone_number,
						app.id as id, app.property_id, app.property_unit_id,
						app.lease_id, app.ps_product_id, app.call_id,
						aa.id AS applicant_application_id
					FROM
						applications app
						INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' )
						INNER JOIN applicants appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = appt.customer_id AND cpn.cid = appt.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.call_id IN (' . implode( ',', $arrintCallIds ) . ')
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND cpn.deleted_by IS NULL';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchApplications( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    app.*
					FROM
					    applications app
					    JOIN leases l ON ( app.lease_id = l.id AND app.lease_interval_id = l.active_lease_interval_id AND app.cid = l.cid )
					WHERE
					    app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
					    AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicantIdByPropertyIdsByCid( $intApplicantId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN applicants a ON ( a.id = app.primary_applicant_id )
					WHERE
						a.id = ' . ( int ) $intApplicantId . '
						AND app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchOlderApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $strApplicationDate, $objDatabase ) {
		if( false == valStr( $intCid ) || false == valStr( $strApplicationDate ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = ' SELECT
						app.*
					FROM
						applications app
					WHERE
						app.property_id = ' . ( int ) $intPropertyId . '
						AND app.cid = ' . ( int ) $intCid . '
						AND app.application_stage_id = ' . ( int ) CApplicationStage::PRE_APPLICATION . '
						AND application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND app.application_datetime < \'' . date( 'Y-m-d', strtotime( $strApplicationDate ) ) . ' 00:00:00\'';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationPropertyIdByApplicantUserNameByCid( $strUserName, $intCid, $objDatabase ) {

		if( false == is_null( $strUserName ) && 0 < strlen( trim( $strUserName ) ) ) {
			$strUserNameCondition = '\'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUserName ) ) . '\'';
		}

		$strSql = 'SELECT
			app.property_id
		FROM
			applications app
			JOIN applicant_applications aa ON ( aa.cid = app.cid AND aa.application_id = app.id )
			JOIN  applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
        WHERE a.username = ' . $strUserNameCondition . ' AND app.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'property_id', $objDatabase );

	}

	public static function fetchPreviousApplicationByApplicationIdByLeaseIdByCid( $intApplicationId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applications a
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_id = ' . ( int ) $intLeaseId . '
						AND a.id <> ' . ( int ) $intApplicationId . '
						AND a.is_deleted = FALSE
					ORDER BY a.id DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByQuoteIdsByPropertyIdByCid( $arrintQuoteIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintQuoteIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' ) ';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchRenewalApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsGuestCard = false ) {

		$strWhere = ' AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL;
		if( true == $boolIsGuestCard ) {
			$strWhere = ' AND app.application_stage_id = ' . ( int ) CApplicationStage::PRE_APPLICATION;
		}

		$strSql = 'SELECT
						app.*
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						' . $strWhere;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStatusIdByLeaseIdByLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intLeaseIntervalId ) || false == valId( $intCid ) ) {
			return;
		}
		$strSql = 'SELECT
						a.application_status_id
					FROM
						applications a
					WHERE
						a.lease_id = ' . ( int ) $intLeaseId . '
						AND a.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND a.cid = ' . ( int ) $intCid;
		$arrstrData = fetchData( $strSql, $objDatabase );
		return $arrstrData[0]['application_status_id'];
	}

	public static function fetchPaginatedApplicationsByPropertyIdByCid( $intCid, $objPagination, $objLeasingBoardFilter, $objDatabase, $boolIsFromExport, $boolIsCalculateCount = false ) {

		if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			return;
		}

		$strFilterSql   = '';
		$strSearchSql   = '';

		if( false == $boolIsCalculateCount ) {
			$strSortSql     = '';
			$strOffsetLimit = '';

			if( false == $boolIsFromExport ) {
				$strOffsetLimit = ' OFFSET ' . ( int ) $objPagination->getOffset() .
				                  ' LIMIT ' . $objPagination->getPageSize();
			}

			$strSortDirection = $objLeasingBoardFilter->getSortDirection();
			$strSortBy        = $objLeasingBoardFilter->getSortBy();

			if( true == valStr( $strSortBy ) ) {
				if( $strSortBy == 'unit_name' ) {
					$strSortBy = ' unit_name ';
				} elseif( $strSortBy == 'move_in_date' ) {
					$strSortBy = ' li.lease_start_date ';
				} else {
					$strSortBy = ' days_left ';
				}

				$strSortSql = ' ORDER BY ' . $strSortBy . ' ' . $strSortDirection;
			}
		}

		if( true == valArr( $objLeasingBoardFilter->getPropertyBuildingIds() ) ) {
			$strFilterSql .= ' AND us.property_building_id IN ( ' . implode( ',', $objLeasingBoardFilter->getPropertyBuildingIds() ) . ' ) ';
		}

		if( true == valArr( $objLeasingBoardFilter->getPropertyFloorplanIds() ) ) {
			$strFilterSql .= ' AND a.property_floorplan_id IN ( ' . implode( ',', $objLeasingBoardFilter->getPropertyFloorplanIds() ) . ' ) ';
		}

		if( true == valArr( $objLeasingBoardFilter->getPropertyLeaseStartWindowIds() ) ) {
			$strFilterSql .= ' AND li.lease_start_window_id IN ( ' . implode( ',', $objLeasingBoardFilter->getPropertyLeaseStartWindowIds() ) . ' ) ';
		}

		if( true == valStr( $objLeasingBoardFilter->getMinMoveInDate() ) ) {
			$strFilterSql .= ' AND li.lease_start_date >= \'' . date( 'm/d/Y', strtotime( $objLeasingBoardFilter->getMinMoveInDate() ) ) . '\'';
		}

		if( true == valStr( $objLeasingBoardFilter->getMaxMoveInDate() ) ) {
			$strFilterSql .= ' AND li.lease_start_date <= \'' . date( 'm/d/Y', strtotime( $objLeasingBoardFilter->getMaxMoveInDate() ) ) . '\'';
		}

		if( true == valId( $objLeasingBoardFilter->getSelectedStatusId() ) ) {
			if( $objLeasingBoardFilter->getSelectedStatusId() == CApplicationStageStatus::PROSPECT_STAGE3_STARTED ) {
				$strFilterSql .= ' AND ( a.application_stage_id = ' . CApplicationStage::LEASE . ' OR  ( a.application_stage_id = ' . CApplicationStage::APPLICATION . '  AND a.application_status_id >= ' . CApplicationStatus::STARTED . ' AND a.application_status_id <= ' . CApplicationStatus::APPROVED . ' ) ) ';
			} elseif( $objLeasingBoardFilter->getSelectedStatusId() == CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED ) {
				$strFilterSql .= ' AND ( a.application_stage_id = ' . CApplicationStage::LEASE . ' OR  ( a.application_stage_id = ' . CApplicationStage::APPLICATION . '  AND a.application_status_id >= ' . CApplicationStatus::COMPLETED . ' ) ) ';
			} elseif( $objLeasingBoardFilter->getSelectedStatusId() == CApplicationStageStatus::PROSPECT_STAGE4_STARTED ) {
				$strFilterSql .= ' AND ( a.application_stage_id = ' . CApplicationStage::LEASE . '  AND a.application_status_id >= ' . CApplicationStatus::STARTED . ' AND a.application_status_id <= ' . CApplicationStatus::APPROVED . ' ) ';
			} elseif( $objLeasingBoardFilter->getSelectedStatusId() == CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED ) {
				$strFilterSql .= ' AND ( a.application_stage_id = ' . CApplicationStage::LEASE . '  AND a.application_status_id = ' . CApplicationStatus::COMPLETED . ' ) ';
			}
		}

		if( true == valStr( $objLeasingBoardFilter->getResidentOrUnitName() ) ) {

			$strSearchSql = ' AND ( ap.name_first ILIKE \'%' . $objLeasingBoardFilter->getResidentOrUnitName() . '%\'
			                        OR ap.name_last ILIKE \'%' . $objLeasingBoardFilter->getResidentOrUnitName() . '%\'
			                        OR us.building_name ILIKE \'%' . $objLeasingBoardFilter->getResidentOrUnitName() . '%\'
			                        OR us.unit_number_cache ILIKE \'%' . $objLeasingBoardFilter->getResidentOrUnitName() . '%\'
			                      )';
		}

		$strSql = '
			SELECT
                l.id AS lease_id,
                a.id AS application_id,
                a.lease_interval_id,
                a.application_stage_id,
                a.application_status_id,
                ap.customer_id,
                concat ( ap.name_last, \', \', ap.name_first ) AS application_name,
                CASE
                    WHEN us.building_name IS NOT NULL THEN concat ( us.building_name, \'-\', us.unit_number_cache )
                    ELSE ' . $objDatabase->getCollateSort( 'us.unit_number_cache' ) . '
                END AS unit_name,
                li.lease_start_date AS move_in_date,
                ( li.lease_start_date::DATE - TO_CHAR ( NOW ( ), \'mm/dd/YYYY\' )::DATE ) AS days_left,
                us.property_building_id,
                a.property_floorplan_id,
                li.lease_start_window_id,
                sar.screening_decision_type_id AS is_screened
			FROM
                applications a
                LEFT JOIN leases l ON ( l.cid = a.cid AND l.property_id = a.property_id AND l.id = a.lease_id )
                JOIN lease_intervals li ON ( li.cid = a.cid AND li.id = a.lease_interval_id AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' )
                JOIN applicant_applications aa ON ( aa.cid = a.cid AND aa.application_id = a.id )
                JOIN applicants ap ON ( ap.cid = aa.cid AND ap.id = aa.applicant_id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
                LEFT JOIN unit_spaces us ON ( us.cid = a.cid AND us.property_id = a.property_id AND us.id = a.unit_space_id )
                LEFT JOIN screening_application_requests sar ON ( a.cid = sar.cid AND a.id = sar.application_id )
			WHERE
				a.cid             = ' . ( int ) $intCid . '
		        AND a.property_id = ' . ( int ) $objLeasingBoardFilter->getPropertyId() . '
				AND ( ( a.application_stage_id IN ( ' . CApplicationStage::PRE_APPLICATION . ' , ' . CApplicationStage::APPLICATION . ' ) AND a.application_status_id <= ' . CApplicationStatus::APPROVED . ' ) OR ( a.application_stage_id = ' . CApplicationStage::LEASE . ' AND a.application_status_id < ' . CApplicationStatus::APPROVED . ' ) OR ( a.application_stage_id = ' . CApplicationStage::PRE_QUALIFICATION . ' AND a.application_status_id <= ' . CApplicationStatus::APPROVED . ' ) ) ';

		$strSql .= $strFilterSql;
		$strSql .= $strSearchSql;

		if( false == $boolIsCalculateCount ) {
			$strSql .= $strSortSql;
			$strSql .= $strOffsetLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLastContactDateByPropertyIdByCidByIds( $strArchiveDate, $intPropertyId, $intCid, $objDatabase, $arrintApplicationIds = NULL ) {

		if( false == valStr( $strArchiveDate ) && false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}

		$strConditionSql = $strJoinSql = '';
		if( true == valStr( $strArchiveDate ) ) {
			$strJoinSql .= ' JOIN events e ON ( e.cid = a.cid AND a.last_event_id = e.id )';
			$strConditionSql .= ' AND e.event_datetime::date <= \'' . $strArchiveDate . '\'::date';
		} elseif( true == valArr( $arrintApplicationIds ) ) {
			$strConditionSql .= ' AND a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ')';
		}

		$strSql = ' SELECT
					a.*
					FROM
						applications a
						' . $strJoinSql . '
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND a.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '
						AND a.application_status_id <> ' . CApplicationStatus::ON_HOLD . '
						' . $strConditionSql;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchActiveApprovedApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						a.*
					FROM
						applications a
						JOIN subsidy_certifications sc ON ( a.cid = sc.cid AND a.id = sc.application_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_id = ' . ( int ) $intLeaseId . '
						AND a.application_status_id = ' . CApplicationStatus::APPROVED . '
						AND sc.effective_date <= CURRENT_DATE
						AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::FINALIZED . '
						AND sc.deleted_on IS NULL
						AND sc.deleted_by IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						AND a.is_deleted IS FALSE
					ORDER BY
						a.created_on DESC,
						a.id DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByGuestRemotePrimaryKeysByPropertyIdByByCid( $arrintRemotePrimaryKeys, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valArr( $arrintRemotePrimaryKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND ( guest_remote_primary_key IN ( \'' . implode( '\',\'', $arrintRemotePrimaryKeys ) . '\') )
						AND application_stage_id = ' . ( int ) CApplicationStage::PRE_APPLICATION;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchLeaseIdsByLeaseIntervalTypeIdsByLeaseIdsByApplicationStageStatusIdsByCid( $arrintLeaseIntervalTypeIds, $arrintLeaseIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						app.lease_id
					FROM
						applications app
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ( app.application_stage_id, app.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		$arrintResults = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResults ) ) return array_keys( rekeyArray( 'lease_id', $arrintResults ) );
		return $arrintResults;
	}

	public static function fetchActiveApplicationsCountByIdPropertyUnitIdByUnitSpaceIdByPropertyIdByCid( $intApplicationId, $intPropertyUnitId, $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase, $intApplicationStageStautsId = NULL ) {

		if( true == is_null( $intApplicationId ) || true == is_null( $intPropertyUnitId ) || true == is_null( $intUnitSpaceId ) || true == is_null( $intPropertyId ) || true == is_null( $intCid ) ) return NULL;

		$strJoinSql = '';

		if( false == is_null( $intApplicationStageStautsId ) ) {
			$strJoinSql = ' JOIN application_stage_statuses ass ON ( ass.application_stage_id = app.application_stage_id AND ass.application_status_id = app.application_status_id AND ass.id >= ' . ( int ) $intApplicationStageStautsId . ' ) ';
		}

		$strSql = 'SELECT
						COUNT(*)
					FROM
						applications app
						' . $strJoinSql . '
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.property_unit_id = ' . ( int ) $intPropertyUnitId . '
						AND app.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND app.id <> ' . ( int ) $intApplicationId . '
						AND app.application_status_id <> ' . CApplicationStatus::CANCELLED;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;

	}

	public static function fetchPrimaryApplicationsByPropertyIdByCid( $intPropertyId,  $intCid, $objDatabase, $boolIsLimit = false ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						app.id,
						app.lease_id,
						appt.id AS recipient_id,
						aa.application_id,
						appt.name_first,
						appt.name_last
					FROM
						applications app
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND app.application_status_id <> ' . CApplicationStatus::CANCELLED;

		if( true == $boolIsLimit ) {
			$strSql .= ' LIMIT 50';
		}

		return ( array ) fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchApplicationsCancellationListTypeIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						app.cancellation_list_type_id
					FROM
						applications app
						JOIN leases l ON ( l.cid = app.cid AND l.id = app.lease_id )
						JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = l.active_lease_interval_id AND li.id = app.lease_interval_id )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.id = ' . ( int ) $intLeaseId;

		return parent::fetchColumn( $strSql, 'cancellation_list_type_id', $objDatabase );
	}

	public static function fetchApplicationsByCallIdsByCidByPropertyIds( $arrintCallIds, $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintCallIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCallIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND call_id IS NOT NULL
						AND call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusFromLogsByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						application_stage_id as stage,
						application_status_id as status
					FROM
						cached_application_logs
					WHERE
						application_status_id != ' . CApplicationStatus::CANCELLED . '
						AND cid = ' . ( int ) $intCid . '
						AND application_id  = ' . ( int ) $intId . '
					ORDER BY
						id DESC
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationsBySearchKeywordByPropertyIdByCid( $strSearchKeyword, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						a.id AS application_id,
						a.property_id,
						a.lease_id,
						a.application_stage_id,
						a.application_status_id,
						ap.customer_id,
						ap.name_first || \' \' || ap.name_last AS full_name,
						ap.name_first,
						ap.name_last,
						us.unit_number_cache AS unit_number,
						us.space_number,
						pb.building_name,
						COALESCE( pb.building_name || \' \' || us.unit_number_cache, us.unit_number_cache ) AS building_unit_number,
						lc.id AS lease_customer_id,
						lst.name AS lease_status_type
					FROM
						applications a
						JOIN applicant_applications aa ON (  aa.cid = a.cid AND aa.application_id = a.id )
						JOIN applicants ap ON ( ap.cid = aa.cid AND ap.id = aa.applicant_id )
						JOIN unit_spaces us ON ( us.cid = a.cid AND us.property_id = a.property_id AND us.id = a.unit_space_id )
						JOIN leases l ON ( l.cid = a.cid AND l.id = a.lease_id )
						JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_id = ap.customer_id )
						JOIN lease_status_types lst ON ( lst.id = lc.lease_status_type_id )
						LEFT JOIN property_buildings pb ON ( pb.cid = us.cid AND pb.property_id = us.property_id AND pb.id = us.property_building_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND ( ap.name_first ILIKE \'%' . $strSearchKeyword . '%\' OR ap.name_last ILIKE \'%' . $strSearchKeyword . '%\'
							OR us.building_name ILIKE \'%' . $strSearchKeyword . '%\' OR us.unit_number_cache ILIKE \'%' . $strSearchKeyword . '%\'
						)
						AND ( a.application_stage_id, a.application_status_id ) IN  ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintActiveApplicationStatusIds ) . ' )
						AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicantNameByEmailByPropertyIdByApplicationStageStatusIdsByCid( $strNameFirst, $strNameLast, $strEmail, $intPropertyId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valArr( $arrintApplicationStageStatusIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
            app.*
          FROM
            applicants appt
            JOIN applicant_applications aa ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
             JOIN applications app ON ( aa.application_id = app.id AND aa.cid = app.cid )
          WHERE
            app.cid = ' . ( int ) $intCid . '
            AND app.property_id = ' . ( int ) $intPropertyId . '
            AND app.app_remote_primary_key IS NULL
            AND app.guest_remote_primary_key IS NOT NULL
            AND app.application_datetime > ' . '\'' . date( 'Y-m-d H:i:s', strtotime( '-30 days' ) ) . '\'' . '
            AND ( app.application_stage_id, app.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
            AND lower( trim( appt.name_first) ) ILIKE E\'' . addslashes( $strNameFirst ) . '\'
            AND lower( trim( appt.name_last) ) ILIKE E\'' . addslashes( $strNameLast ) . '\'
            AND  ( lower( trim( appt.email_address ) ) ILIKE E\'' . addslashes( $strEmail ) . '\' OR lower( trim( appt.username ) ) ILIKE E\'' . addslashes( $strEmail ) . '\' )
            ORDER BY app.application_stage_id , app.created_on desc
            LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationIdsByApplicantIdsByPropertyIdByCid( $arrintApplicationIds, $arrintApplicantIds, $intPropertyId,  $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valArr( $arrintApplicationIds ) || false == valArr( $arrintApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						app.id,
						app.lease_id,
						appt.id AS recipient_id,
						aa.application_id,
						appt.name_first,
						appt.name_last
					FROM
						applications app
						JOIN applicant_applications AS aa ON ( aa.application_id = app.id AND aa.cid = app.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = app.property_id AND p.cid = app.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.id in (' . implode( ',', $arrintApplicationIds ) . ')
						AND appt.id in (' . implode( ',', $arrintApplicantIds ) . ')';

		return ( array ) fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchApplicationsByIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase, $strOrderBy = '', $boolIncludeDeletedAA = false ) {

		if( false == valId( $intApplicationId ) || false == valId( $intApplicantId ) ) return NULL;

		$strSql = 'SELECT
						app.*
						, appt.id as applicant_id
						, appt.name_last
						, appt.name_first
						, appt.username as email_address
						, aa.id AS applicant_application_id
					FROM
						applications app ';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql .= ' INNER JOIN applicant_applications aa ON ( aa.application_id = app.id AND aa.cid = app.cid ' . $strCheckDeletedAASql . ' ) ';

		$strSql .= ' INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';

		$strSql .= ' WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id = ' . $intApplicationId . '
						AND appt.id =  ' . $intApplicantId;

		if( 'name' == $strOrderBy ) {
			$strSql .= ' ORDER BY appt.name_first, appt.name_last';
		} elseif( 'name DESC' == $strOrderBy ) {
			$strSql .= ' ORDER BY name_last || name_first DESC';
		} elseif( 'name ASC' == $strOrderBy ) {
			$strSql .= ' ORDER BY name_last || name_first ASC';
		} elseif( false !== \Psi\CStringService::singleton()->strpos( $strOrderBy, 'lease_interval_type_id' ) ) {
			$strSql .= ' ORDER BY li.' . $strOrderBy;
		} elseif( false == empty( $strOrderBy ) ) {
			$strSql .= ' ORDER BY app. ' . $strOrderBy;
		} else {
			$strSql .= ' ORDER BY app.id DESC';
		}

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLeadsCountByInternetListingServiceIds( $arrintInternetListingServiceIds, $objDatabase ) {
		if( false == valArr( $arrintInternetListingServiceIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						a.internet_listing_service_id,
						count( a.id ) AS ils_lead_count,
						a.cid
					FROM
						applications a
						JOIN applicant_applications aa ON ( a.id = aa.application_id AND a.cid = aa.cid )
					WHERE
						aa.is_lead = 1
						AND ( a.ps_product_id = ' . CPsProduct::ILS_PORTAL . ' OR a.ps_product_id = ' . CPsProduct::GUEST_CARD_PARSING . ' )
						AND a.internet_listing_service_id IN ( ' . implode( ',', $arrintInternetListingServiceIds ) . ' )
						AND a.application_datetime > now() - \'30day\'::interval
						AND a.application_datetime <= now()
					GROUP BY
						a.internet_listing_service_id,
						a.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByGuestRemotePrimaryKeyByPropertyIdByCid( $strGuestRemotePrimaryKey, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND lower( app.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\'
						AND app.app_remote_primary_key IS NULL';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchLeaseApprovedApplicationsCountByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(1) as application_count
					FROM
						applications a
						JOIN lease_intervals li ON ( a.cid = li.cid AND a.lease_interval_id = li.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId . '
						AND li.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND li.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
						AND a.lease_approved_on IS NOT NULL
						AND li.lease_status_type_id = ' . CLeaseStatusType::FUTURE;

		return self::fetchColumn( $strSql, 'application_count', $objDatabase );
	}

	public static function fetchCustomApplicationsByLeaseIdsByPropertyIdByCid( $arrintLeaseId, $intPropertyId, $intCid, $objDatabase, $intApplicationDate, $intLeaseDate ) {

		$strWhereSql = $strSelectSQL = '';
		if( true == valArr( $arrintLeaseId ) ) {
			$strWhereSql = 'AND l.id IN ( ' . implode( ',', $arrintLeaseId ) . ' )';
		}

		if( true == valId( $intApplicationDate ) ) {
			$strSelectSQL = 'CASE
								WHEN li.lease_start_date > NOW() THEN NOW() - INTERVAL \'' . $intApplicationDate . ' DAY\'
								ELSE li.lease_start_date - INTERVAL \'' . $intApplicationDate . ' DAY\'
							END as application_date,';
		}

		if( true == valId( $intLeaseDate ) ) {
			$strSelectSQL .= 'CASE
								WHEN li.lease_start_date > NOW() THEN NOW() - INTERVAL \'' . $intLeaseDate . ' DAY\'
								ELSE li.lease_start_date - INTERVAL \'' . $intLeaseDate . ' DAY\'
							END as lease_date,';
		}

		$strSql = 'select
						l.id as lease_id,
					    li.id as lease_interval_id,
					    a.id as application_id,
					    aa.id as applicant_application_id,
					    li.lease_start_date,
					    ' . $strSelectSQL . '
					    a.application_stage_id,
					    a.application_status_id,
					    a.application_datetime,
					    a.application_completed_on,
					    a.application_approved_on,
					    a.lease_completed_on,
					    a.lease_approved_on,
					    aa.started_on as aa_started_on,
					    aa.approved_on as aa_approved_on,
					    aa.completed_on as aa_completed_on,
					    aa.lease_generated_on as aa_lease_generated_on,
					    aa.lease_signed_on
					FROM leases l
					     JOIN lease_intervals li ON (l.cid = li.cid AND l.id = li.lease_id)
					     JOIN applications a ON (a.cid = li.cid AND a.lease_interval_id = li.id)
					     JOIN applicant_applications aa ON (aa.cid = a.cid AND aa.application_id = a.id )
					WHERE l.cid = ' . ( int ) $intCid . '
						  AND l.property_id = ' . ( int ) $intPropertyId . '
						  AND ( l.created_by IN ( 18, 61 ) OR l.updated_by IN ( 18, 61 ) ) AND ( li.created_by IN ( 18, 61 ) OR li.updated_by IN ( 18, 61 ) ) AND ( a.created_by IN ( 18, 61 ) OR a.updated_by IN ( 18, 61 ) ) AND ( aa.created_by IN ( 18, 61 ) OR aa.updated_by IN ( 18, 61 ) )
						  ' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveGuestCardApplications( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						ca.id, ca.cid, ca.property_id, ca.name_first, ca.name_last, ca.lease_start_date, ca.lease_end_date
					FROM
						cached_applications ca
						JOIN lease_intervals li ON ( li.cid = ca.cid AND li.id = ca.lease_interval_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '	AND
						ca.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND
						ca.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '	AND
						li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '	AND
						ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByAppRemotePrimaryKeyByIntegrationDatabaseIdByCid( $strLeaseRemotePrimaryKey, $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		$strSql = $strSql = 'SELECT
						a.*
					FROM
						applications a
						INNER JOIN property_integration_databases as pid ON ( a.property_id = pid.property_id AND a.cid = pid.cid )
					WHERE
						( lower(a.app_remote_primary_key) = \'' . \Psi\CStringService::singleton()->strtolower( $strLeaseRemotePrimaryKey ) . '\' )
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND pid.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationCancellationDetailsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						a.cancellation_list_type_id,
						a.cancellation_list_item_id
					FROM
						applications a
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return getArrayElementByKey( 0, fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchActiveApplicationsByLeaseIdsByCidForRpc( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND app.application_status_id != ' . CApplicationStatus::CANCELLED;
		return self::fetchApplications( $strSql, $objDatabase );
	}

	public function fetchApplicationsByLeasingAgentIdByPropertyIdByCid( $intLeasingAgentId, $intPropertyId, $intCid, $objDatabase, $boolIsCount = false ) {

		if( false == valId( $intLeasingAgentId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSelectField = ' app.* ';
		if( true == $boolIsCount ) {
			$strSelectField = ' count( app.id ) ';
		}

		$strSql = 'SELECT
						' . $strSelectField . '
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.leasing_agent_id = ' . ( int ) $intLeasingAgentId;

		if( true == $boolIsCount ) {
			$arrstrData = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
			return 0;
		}
		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationStageStatusIdsByUserIdByPropertyIdByCid( $arrintApplicationStageId, $arrintApplicationStatusId, $intUserId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageId ) || false == valArr( $arrintApplicationStatusId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						app.*
					FROM
						applications app
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.property_id = ' . ( int ) $intPropertyId . '
						AND app.application_stage_id IN ( ' . implode( ',', $arrintApplicationStageId ) . ' )
						AND app.application_status_id IN ( ' . implode( ',', $arrintApplicationStatusId ) . ' )
						AND ( app.created_by = ' . ( int ) $intUserId . ' OR app.updated_by = ' . ( int ) $intUserId . ' ) ';

		return self::fetchApplications( $strSql, $objDatabase );

	}

	public static function fetchCustomApplicationsByLeaseIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintPropertyIds, $intCid, $objDatabase, $boolFetchActiveApplicationOnly = false ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strJoinLeaseIntervals = 'JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id AND li.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' )';

		if( true == $boolFetchActiveApplicationOnly ) {
			$strJoinLeaseIntervals = 'JOIN lease_intervals li ON ( li.cid = app.cid AND li.id = app.lease_interval_id AND l.active_lease_interval_id = li.id )';
		}

		$strSql = 'SELECT 
						app.* 
					FROM applications app
						JOIN leases l ON ( app.cid = l.cid AND app.property_id = l.property_id AND app.lease_id = l.id )
						' . $strJoinLeaseIntervals . '
					WHERE
						l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND
						app.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND 
						app.cid = ' . ( int ) $intCid;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationLeasingAgentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT 
						DISTINCT ON (a.leasing_agent_id) a.id,
						a.leasing_agent_id,
						ce.name_first,
						ce.name_last       
					FROM
						company_employees ce 
						JOIN  applications a ON ( a.leasing_agent_id = ce.id AND a.cid = ce.cid )
					WHERE 
						a.cid = ' . ( int ) $intCid . '
						AND a.property_id = ' . ( int ) $intPropertyId;

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicationIdByApplicantApplicationIdByCid( $intApplicationId, $intApplicantApplicationId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intApplicantApplicationId ) ) return;

		$strSql = 'SELECT
						app.*,
						aa.id,
						c.preferred_locale_code
					FROM
						applications app
						JOIN applicant_applications aa ON ( app.cid = aa.cid AND app.id = aa.application_id AND aa.deleted_on IS NULL )
						JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
						JOIN customers c ON ( appt.customer_id = c.id AND appt.cid = c.cid )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						AND app.id = ' . ( int ) $intApplicationId . '
						AND aa.id = ' . ( int ) $intApplicantApplicationId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public function fetchApplicationByPrimaryResidentDetailsByPropertyIdByCid( $strFirstName, $strLastName, $strEmailAddress, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valStr( $strEmailAddress ) ) {
			return NULL;
		}
		$strSql = 'SELECT
					    ca.*
					FROM
					    cached_applications ca
					    INNER JOIN applicant_applications aap ON( ca.id = aap.application_id AND ca.cid = aap.cid )
					    INNER JOIN applicants apt ON( aap.applicant_id = apt.id AND aap.cid = apt.cid )
					WHERE
					    ca.cid = ' . ( int ) $intCid . '
					    AND ca.property_id = ' . ( int ) $intPropertyId . '
					    AND ca.guest_remote_primary_key IS NULL
					    AND aap.customer_type_id = ' . CCustomerType::PRIMARY . '
					    AND lower(apt.name_first) ILIKE \'%' . trim( strtolower( $strFirstName ) ) . '%\'
					    AND lower(apt.name_last) ILIKE \'%' . trim( strtolower( $strLastName ) ) . '%\'
						AND( lower(apt.email_address ) ILIKE \'%' . trim( strtolower( $strEmailAddress ) ) . '%\' OR lower(apt.username ) ILIKE \'%' . strtolower( $strEmailAddress ) . '%\' )
	                    ORDER BY ca.created_on DESC LIMIT 1;';

		return self::fetchApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByCustomerIdByLeaseStatusTypeIdByCid( $intLeaseId, $intCustomerId, $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT
						a.*
					FROM
						applications a
						LEFT JOIN applicant_applications aa ON ( a.id = aa.application_id AND a.cid = aa.cid ) 
						LEFT JOIN applicants apt ON ( aa.applicant_id = apt.id AND aa.cid = apt.cid )
						LEFT JOIN lease_customers lc ON ( lc.cid = apt.cid AND lc.lease_id = a.lease_id AND lc.customer_id = apt.customer_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.lease_id = ' . ( int ) $intLeaseId . '
						AND apt.customer_id = ' . ( int ) $intCustomerId . '
						AND lc.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . '
						AND a.application_stage_id = ' . CApplicationStage::LEASE . '
						AND a.application_status_id IN ( ' . CApplicationStatus::STARTED . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ', ' . CApplicationStatus::APPROVED . ' ) 
					ORDER BY
						apt.created_on DESC
					LIMIT 1';

		return self::fetchApplication( $strSql, $objDatabase );
	}

	public static function getIsReferrerApplicationByIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return;
		}

		$strSql = 'SELECT
					    CASE
					      WHEN ar.id IS NULL AND r.id IS NULL THEN 0
					      ELSE 1
					    END AS is_referrer_associated,
					    CASE
					      WHEN r.enable_portal_access IS TRUE AND ast.id <= r.interaction_cutoff_stage_status_id THEN 0
					      ELSE 1
					    END AS is_referrer_application
					FROM
					    cached_applications ca
					    JOIN application_referrers ar ON ( ar.cid = ca.cid AND ar.application_id = ca.id AND ar.deleted_by IS NULL AND ar.deleted_on IS NULL )
					    JOIN referrers r ON ( r.cid = ar.cid AND r.id = ar.referrer_id AND r.deleted_by IS NULL AND r.deleted_on IS NULL )
					    JOIN application_stage_statuses ast ON ( ast.lease_interval_type_id = ca.lease_interval_type_id AND ast.application_stage_id = ca.application_stage_id AND ast.application_status_id = ca.application_status_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
					    AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.id = ' . ( int ) $intApplicationId;

		$arrmixReferrerData = ( array ) fetchData( $strSql, $objDatabase );

		return ( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixReferrerData ) &&
		         true == $arrmixReferrerData[0]['is_referrer_associated'] &&
		         false == $arrmixReferrerData[0]['is_referrer_application'] );
	}

	public static function fetchApplicationByLeaseRemotePrimaryKeyByApplicantFullNameByPropertyIdByCid( $strRemotePrimaryKey, $strNameFirst, $strNameLast, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strRemotePrimaryKey ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                 ca.* 
               FROM
                 applicants a
              JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid  )
              JOIN cached_applications ca ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
              WHERE
              lower(a.name_first) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ) . '\'
               AND lower(a.name_last) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ) . '\'
                AND ca.app_remote_primary_key = \'' . $strRemotePrimaryKey . '\'
              AND ca.property_id = ' . ( int ) $intPropertyId . '
              AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
              AND a.cid = ' . ( int ) $intCid . '
              ORDER BY a.created_on DESC LIMIT 1';

		return CApplications::fetchObject( $strSql, 'CApplication', $objDatabase );
	}

	public static function fetchActiveApplicationByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		if( false == valId( $intScreeningId ) || false == valId( $intCid ) ) {
			return;
		}

		$strSql = '	SELECT
						app.*
					FROM
						applications app
						JOIN screening_application_requests sar ON ( sar.application_id = app.id AND sar.cid = app.cid AND sar.remote_screening_id = ' . ( int ) $intScreeningId . ' )
					WHERE
						app.cid = ' . ( int ) $intCid . '
						and sar.remote_screening_id = ' . ( int ) $intScreeningId;

		return self::fetchApplication( $strSql, $objDatabase );
	}

}
?>