<?php

class CArTriggerType extends CBaseArTriggerType {

	const APPLICATION_OR_LEASING	= 1;
	const ONE_TIME_LEASE_CHARGES	= 2;
	const RECURRING_LEASE_CHARGES	= 3;
	const CACHEABLE					= 4;
	const UTILITY 					= 5;
	const MAINTENANCE				= 6;
	const RESERVED					= 7;
	const SYSTEM					= 9;

	public static $c_arrintScheduledChargeCArTriggerTypes               		= [ self::APPLICATION_OR_LEASING, self::ONE_TIME_LEASE_CHARGES, self::RECURRING_LEASE_CHARGES ];
	public static $c_arrintConditionalAndOneTimeScheduledChargeCArTriggerTypes	= [ CArTriggerType::APPLICATION_OR_LEASING, CArTriggerType::ONE_TIME_LEASE_CHARGES, CArTriggerType::CACHEABLE ];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>