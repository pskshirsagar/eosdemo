<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryVendorsRegions
 * Do not add any new functions to this class.
 */

class CAncillaryVendorsRegions extends CBaseAncillaryVendorsRegions {

	public static function fetchAncillaryVendorsRegions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAncillaryVendorsRegion', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAncillaryVendorsRegion( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAncillaryVendorsRegion', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>