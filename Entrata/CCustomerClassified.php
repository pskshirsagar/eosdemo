<?php

class CCustomerClassified extends CBaseCustomerClassified {

	protected $m_intTermsAndConditions;
	protected $m_intCustomerId;
	protected $m_intClassifiedAttachmentId;

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strUnitNumber;
	protected $m_strPropertyName;
	protected $m_strFileName;
	protected $m_strFilePath;

	protected $m_strApprovalNotificationEmailAddress;

	/**
	* Get Functions
	*
	*/

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	 public function getTermsAndConditions() {
		return $this->m_intTermsAndConditions;
	 }

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getApprovalNotificationEmailAddress() {
		return $this->m_strApprovalNotificationEmailAddress;
	}

	public function getClassifiedAttachmentId() {
		return $this->m_intClassifiedAttachmentId;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	/**
	* Set Functions
	*
	*/

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst 	= $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast 	= $strNameLast;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setTermsAndConditions( $intTermsAndConditions ) {
		$this->m_intTermsAndConditions = $intTermsAndConditions;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber 	= $strUnitNumber;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName 	= $strPropertyName;
	}

	public function setApprovalNotificationEmailAddress( $strApprovalNotificationEmailAddress ) {
		$this->m_strApprovalNotificationEmailAddress = $strApprovalNotificationEmailAddress;
	}

	public function setClassifiedAttachmentId( $intClassifiedAttachmentId ) {
		$this->m_intClassifiedAttachmentId = $intClassifiedAttachmentId;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['name_first'] ) ) 				$this->setNameFirst( $arrValues['name_first'] );
		if( true == isset( $arrValues['name_last'] ) ) 					$this->setNameLast( $arrValues['name_last'] );
		if( true == isset( $arrValues['terms_and_conditions'] ) ) 		$this->setTermsAndConditions( $arrValues['terms_and_conditions'] );
		if( true == isset( $arrValues['unit_number'] ) ) 				$this->setUnitNumber( $arrValues['unit_number'] );
		if( true == isset( $arrValues['property_name'] ) ) 				$this->setPropertyName( $arrValues['property_name'] );
		if( true == isset( $arrValues['customer_id'] ) ) 				$this->setCustomerId( $arrValues['customer_id'] );
		if( true == isset( $arrValues['classified_attachment_id'] ) ) 	$this->setClassifiedAttachmentId( $arrValues['classified_attachment_id'] );
		if( true == isset( $arrValues['file_name'] ) ) 					$this->setFileName( $arrValues['file_name'] );
		if( true == isset( $arrValues['file_path'] ) ) 					$this->setFilePath( $arrValues['file_path'] );
		return;
	}

	/**
	* Create Functions
	*
	*/

	public function createCustomerClassifiedAttachment() {

		$objCustomerClassifiedAttachment = new CCustomerClassifiedAttachment();

		$objCustomerClassifiedAttachment->setDefaults();
		$objCustomerClassifiedAttachment->setCid( $this->getCid() );

		return $objCustomerClassifiedAttachment;
	}

	/**
	* Fetch Functions
	*
	*/

	public function fetchCustomer( $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {

		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyClassifiedCategory( $objDatabase ) {

		return CCompanyClassifiedCategories::fetchCompanyClassifiedCategoryByIdByCid( $this->m_intCompanyClassifiedCategoryId, $this->m_intCid, $objDatabase );
	}

	public function fetchClassifiedAttachments( $objDatabase ) {

		return CCustomerClassifiedAttachments::fetchClassifiedAttachmentsByClassifiedIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	* Validate Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyClassifiedCategoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyClassifiedCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_classified_category_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$strMessage = __( 'Title is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valPost() {
		$boolIsValid = true;

		if( true == is_null( $this->getPost() ) ) {
			$strMessage = __( 'Message is required' );
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {

		$boolIsValid = true;

		if( false == is_null( $this->getStartDate() ) && 1 != CValidation::checkDate( $this->getStartDate() ) ) {
			$strMessage = __( 'Start date must be in mm/dd/yyyy form.' );
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getEndDate() ) && 1 !== CValidation::checkDate( $this->getEndDate() ) ) {
			$strMessage = __( 'End date must be in mm/dd/yyyy form.' );
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strMessage ) );
		}

		if( false == is_null( $this->getEndDate() ) && 1 == CValidation::checkDate( $this->getEndDate() ) && ( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) ) {
			$boolIsValid = false;
			$strMessage = __( 'End date must be greater than start date.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valTermsAndConditions() {

		$boolIsValid = true;

		if( true == is_null( $this->getTermsAndConditions() ) ) {
			$boolIsValid = false;
			$strMessage = __( 'You must agree to the terms and conditions.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCompanyClassifiedCategoryId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPost();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valTermsAndConditions();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_approve':
			case 'validate_reject':
			case 'validate_archive':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function deleteCustomerClassified( $intUserId, $objDatabase ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intUserId, $objDatabase );
	}

}
?>