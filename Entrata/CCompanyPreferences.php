<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyPreferences
 * Do not add any new functions to this class.
 */

class CCompanyPreferences extends CBaseCompanyPreferences {

	public static function fetchAllCompanyPreferencesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM company_preferences WHERE cid = ' . ( int ) $intCid;

		$arrobjCompanyPreferences = self::fetchCompanyPreferences( $strSql, $objDatabase );
		$arrobjRekeyedCompanyPreferences = [];

		if( true == valArr( $arrobjCompanyPreferences ) ) {
			foreach( $arrobjCompanyPreferences as $objCompanyPreference ) {
				$arrobjRekeyedCompanyPreferences[trim( $objCompanyPreference->getKey() )] = $objCompanyPreference;
			}
		}

		return $arrobjRekeyedCompanyPreferences;
	}

	public static function fetchCachedAllCompanyPreferencesByCid( $intCid, $objDatabase ) {
		$arrmixData = CCache::fetchObject( 'Company_Preferences_' . $intCid );

		if( false === $arrmixData || true == is_null( $arrmixData ) ) {
			$strSql = 'SELECT
							cp.key, cp.value
						FROM
							company_preferences cp
						WHERE
							cp.cid = ' . ( int ) $intCid;

			$arrmixResults = ( array ) fetchData( $strSql, $objDatabase );
			foreach( $arrmixResults as $arrmixResult ) {
				$arrmixData[$arrmixResult['key']] = $arrmixResult['value'];
			}

			CCache::storeObject( 'Company_Preferences_' . $intCid, $arrmixData, $intSpecificLifetime = 120, [] );
		}

		return $arrmixData;
	}

	public static function fetchCachedCompanyPreferenceByKeyByCid( $strKey, $intCid, $objDatabase ) {
		$objCompanyPreference = CCache::fetchObject( 'Company_Preferences_' . $intCid . '_' . $strKey );

		if( false === $objCompanyPreference ) {
			$objCompanyPreference = self::fetchCompanyPreferenceByKeyByCid( $strKey, $intCid, $objDatabase );
			CCache::storeObject( 'Company_Preferences_' . $intCid . '_' . $strKey, $objCompanyPreference, $intSpecificLifetime = 120, [] );
		}

		return $objCompanyPreference;
	}

	public static function fetchCompanyPreferenceByKeyByCid( $strKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM company_preferences WHERE key = \'' . $strKey . '\' AND cid = ' . ( int ) $intCid;
		return self::fetchCompanyPreference( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferenceByKeyByValueByCid( $strKey, $strValue, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM company_preferences WHERE key = \'' . $strKey . '\' AND value = \'' . $strValue . '\' AND cid = ' . ( int ) $intCid;
		return self::fetchCompanyPreference( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesValueByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {

		$arrstrCompanyPreferences = [];
		if( false == valArr( $arrstrKeys ) ) return $arrstrCompanyPreferences;

		$strSql = 'SELECT
						key,
						value
					FROM
						company_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		$arrmixCompanyPreferences = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixCompanyPreferences ) ) {
			foreach( $arrmixCompanyPreferences as $arrstrCompanyPreference ) {
				$arrstrCompanyPreferences[$arrstrCompanyPreference['key']] = $arrstrCompanyPreference['value'];
			}
		}

		return $arrstrCompanyPreferences;
	}

	public static function fetchCustomIntegratedCompanyPreferenceByKeys( $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						cp.*
					FROM
						company_preferences cp
						JOIN integration_databases id ON cp.cid = id.cid
					WHERE
						cp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
						AND cp.value IS NOT NULL ';

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferenceByKeysByCid( $arrstrKeys, $intCid, $objDatabase, $boolDisplayNullPreferences = false ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		if( false == $boolDisplayNullPreferences ) {
			$strSql .= ' AND value IS NOT NULL';
		}

		$strSql .= ' AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesByKeyByCids( $strKey, $arrintCids, $objClientDatabase ) {

		if( false == valStr( $strKey ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						cid IN ( ' . implode( ', ', $arrintCids ) . ' )
						AND key = \'' . $strKey . '\'';

		return self::fetchCompanyPreferences( $strSql, $objClientDatabase, false );
	}

	public static function fetchCompanyPreferencesByKeysByCids( $arrstrKeys, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintCids ) ) return NULL;
		$strKeys = '\'{' . implode( ',', $arrstrKeys ) . '}\'';

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND key = ANY( ' . $strKeys . '::text[] )';

		return self::fetchCompanyPreferences( $strSql, $objClientDatabase, false );
	}

	public static function fetchDocumentNamingConventionFormatByEntityTypeIdByCid( $intEntityTypeId, $intCid, $objClientDatabase ) {
		$strDocumentNamingConventionFormat = '';

		switch( $intEntityTypeId ) {
			case CEntityType::RESIDENT:
			case CEntityType::LEAD:
				$strKey = 'DOCUMENT_MANAGEMENT_LEAD_AND_RESIDENT_FILE_NAME_FORMAT';
				break;

			case CEntityType::VENDOR:
				$strKey = 'DOCUMENT_MANAGEMENT_VENDOR_FILE_NAME_FORMAT';
				break;

			case CEntityType::EMPLOYEE:
				$strKey = 'DOCUMENT_MANAGEMENT_EMPLOYEE_FILE_NAME_FORMAT';
				break;

			case CEntityType::PROPERTY:
				$strKey = 'DOCUMENT_MANAGEMENT_PROPERTY_FILE_NAME_FORMAT';
				break;

			default:
				$strKey = '';
				break;
		}

		$objDocumentNamingPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( $strKey, $intCid, $objClientDatabase );

		if( true == valObj( $objDocumentNamingPreference, 'CCompanyPreference' ) ) {
			$strDocumentNamingConventionFormat = $objDocumentNamingPreference->getValue();
		}

		return $strDocumentNamingConventionFormat;
	}

	public static function fetchCompanyPreferencesByKeys( $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesCustomTagsByKeyByCids( $arrstrKeys, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						key
					FROM
						company_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND value = \'1\'
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesCustomTagByKeyByCid( $arrstrKeys, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						key
					FROM
						company_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCustomCompanyPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						cp.id AS id,
						cp.key
					FROM
						company_preferences cp
					WHERE
						cp.cid = ' . ( int ) $intCid . '
						AND cp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesByKeyByValues( $strKey, $strValue,  $objDatabase, $strStartedBefore ) {

		$strSql = 'SELECT
						*
					FROM
						company_preferences
					WHERE
						key = \'' . $strKey . '\' 
						AND value = \'' . $strValue . '\'';

		if( true == valStr( $strStartedBefore ) ) {
			$strSql .= ' AND updated_on < \'' . $strStartedBefore . '\'::date';
		}

		return self::fetchCompanyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyPreferencesUseRetentionByKeyByCid( $strKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						value
					FROM
						company_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\' LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>