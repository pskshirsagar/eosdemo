<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyErrorCodes
 * Do not add any new functions to this class.
 */

class CSubsidyErrorCodes extends CBaseSubsidyErrorCodes {

	public static function fetchSubsidyErrorCodeByErrorCodes( $arrmixErrorCodes, $objDatabase ) {
		if( false == valArr( $arrmixErrorCodes ) ) return NULL;
		$intErrorCount = 0;

		$strSql = 'SELECT
						sce.*
					FROM
						subsidy_error_codes sce
					WHERE ';
		$strSql .= ' sce.error_code IN(';
		foreach( $arrmixErrorCodes as $strErrorCode ) {
			$strSql .= '\'' . $strErrorCode . '\'';
			if( ++$intErrorCount != \Psi\Libraries\UtilFunctions\count( $arrmixErrorCodes ) ) {
				$strSql .= ',';
			}
		}
		$strSql .= ')';

		return self::fetchSubsidyErrorCodes( $strSql, $objDatabase );
	}

}
?>