<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifieds
 * Do not add any new functions to this class.
 */

class CCustomerClassifieds extends CBaseCustomerClassifieds {

	public static function fetchCustomerClassifiedsWithCustomerNameByCompanyClassifiedCategoryIdByPropertyIdByCid( $intCompanyClassifiedCategoryId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cc.*, c.name_first, c.name_last,
						cca.id as classified_attachment_id, cca.file_name, cca.file_path
					FROM
					 	customer_classifieds cc
		  				LEFT OUTER JOIN customers c ON cc.customer_id = c.id AND cc.cid = c.cid
						LEFT JOIN customer_classified_attachments cca ON( cc.id = cca.customer_classified_id AND cc.cid = cca.cid AND cca.deleted_on IS NULL)
				    WHERE
				    	cc.company_classified_category_id = ' . ( int ) $intCompanyClassifiedCategoryId . '
				      	AND cc.property_id = ' . ( int ) $intPropertyId . '
				      	AND cc.cid = ' . ( int ) $intCid . '
				      	AND ( cc.hide_on IS NULL OR cc.hide_on > now() )
				      	AND cc.deleted_on IS NULL
                        AND cc.deleted_by IS NULL
					ORDER BY
				      	cc.updated_on desc';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchCustomerClassifiedByIdByPropertyIdByCid( $intCustomerClassifiedId, $intPropertyId, $intCid, $objDatabase ) {
		if ( false == valId( $intCustomerClassifiedId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						cc.*, c.name_first, c.name_last, c.id as customer_id,
						p.property_name,
						cca.id as classified_attachment_id, cca.file_name, cca.file_path
					FROM
						customer_classifieds cc
						JOIN properties p ON ( cc.property_id = p.id AND cc.cid = p.cid )
						LEFT OUTER JOIN customers c ON cc.customer_id = c.id AND cc.cid = c.cid
						LEFT JOIN customer_classified_attachments cca ON( cc.id = cca.customer_classified_id AND cc.cid = cca.cid AND cca.deleted_on IS NULL)
					WHERE
						cc.id = ' . ( int ) $intCustomerClassifiedId . '
						AND cc.property_id = ' . ( int ) $intPropertyId . '
						AND	cc.cid = ' . ( int ) $intCid;

		return self::fetchCustomerClassified( $strSql, $objDatabase );
	}

	public static function fetchSimpleCustomerClassifiedByIdByPropertyIdByCid( $intCustomerClassifiedId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cc.id,
						cc.customer_id,
						cc.property_id,
						cc.cid,
						cc.title,
						cc.created_by,
						CASE
							WHEN cc.customer_id IS NOT NULL AND c.email_address IS NOT NULL
							THEN c.email_address
							WHEN cu.username IS NOT NULL
							THEN cu.username
							WHEN pp.id IS NOT NULL
							THEN pp.value
							ELSE NULL
						END AS email_address,
						CASE
							WHEN cc.customer_id IS NOT NULL
							THEN c.name_first
							WHEN cu.id IS NOT NULL
							THEN cu.username
							ELSE NULL
						END AS seller
					FROM
						customer_classifieds cc
						JOIN properties p ON ( cc.property_id = p.id AND cc.cid = p.cid AND cc.id = ' . ( int ) $intCustomerClassifiedId . ' AND cc.cid = ' . ( int ) $intCid . ' )
						JOIN company_users cu ON ( cc.created_by = cu.id AND cc.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN customers c ON ( cc.customer_id = c.id AND cc.cid = c.cid )
						LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'CLASSIFIED_APPROVAL_NOTIFICATION_EMAIL\')
					WHERE
						cc.id = ' . ( int ) $intCustomerClassifiedId . '
						AND cc.property_id = ' . ( int ) $intPropertyId . '
						AND	cc.cid = ' . ( int ) $intCid;

		// return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );
		$arrmixData = fetchData( $strSql, $objDatabase );
		return $arrmixData[0];
	}

	public static function fetchSimpleClassifiedByCustomerClassifiedIdByPropertyIdByCidByCustomerId( $intCustomerClassifiedId, $intPropertyId, $intCid, $intCustomerId, $objDatabase ) {
		if( true == is_null( $intCustomerClassifiedId ) || true == is_null( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						cc.id,
						cc.title,
						cc.customer_id,
						cc.cid,
						cc.property_id,
						cc.hide_on,
						cc.amount,
						cc.post,
						cc.show_on,
						p.property_name,
						ccc.name as category_name,
						ccc.id as company_classified_category_id,
						CASE
							WHEN cc.hide_on IS NOT NULL AND cc.hide_on < NOW()
							THEN 1
							ELSE 0
						END AS is_mark_as_sold,
						( round ( EXTRACT ( EPOCH FROM NOW() - cc.created_on ) / 3600 / 24 ) ) as age,
						CASE WHEN ( cc.approved_on IS NULL ) 
						THEN true
						ELSE false
						END AS awaiting_approval
					FROM
						customer_classifieds cc
						JOIN properties p ON ( cc.property_id = p.id AND cc.cid = p.cid )
						LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'ALLOW_RESIDENT_TO_ADD_CLASSIFIED\' )
						JOIN company_classified_categories ccc ON ( cc.company_classified_category_id = ccc.id AND cc.cid = ccc.cid  )
					WHERE
						cc.id = ' . ( int ) $intCustomerClassifiedId . '
						AND cc.property_id = ' . ( int ) $intPropertyId . '
						AND	cc.cid = ' . ( int ) $intCid . '
						AND cc.deleted_on IS NULL
						AND cc.deleted_by IS NULL
						AND CASE
								WHEN pp.id IS NULL
								THEN ( cc.customer_id != ' . ( int ) $intCustomerId . ' OR cc.customer_id IS NULL )
								ELSE 1 = 1
							END';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData[0];
	}

	public static function fetchPaginatedUnapprovedCustomerClassifiedsByPropertyIdsByCid( $objPagination, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRejectedClassifieds= false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSqlOffsetLimit = '';
    	if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

    		$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
    		$intLimit = ( int ) $objPagination->getPageSize();
    		$strSqlOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
    	}

    	$strSqlRejected = ( true == $boolIsRejectedClassifieds ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSql = 'SELECT
						cc.*, c.name_first, c.name_last
					FROM
					 	customer_classifieds cc
		  				LEFT OUTER JOIN customers c ON cc.customer_id = c.id AND cc.cid = c.cid
				    WHERE
				      	cc.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
				      	AND cc.cid = ' . ( int ) $intCid . '
				      	AND cc.customer_id IS NOT NULL
				      	AND approved_by IS NULL
				      	AND deleted_on IS NULL
						AND deleted_by IS NULL
				      	' . $strSqlRejected . '
					ORDER BY
				      	cc.updated_on DESC ' . $strSqlOffsetLimit;

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedCustomerClassifiedsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsRejectedClassifieds= false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlRejected = ( true == $boolIsRejectedClassifieds ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSql = 'SELECT
						count(cc.id)
					FROM
					 	customer_classifieds cc
				    WHERE
				      	cc.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cc.cid = ' . ( int ) $intCid . '
				      	AND approved_by IS NULL' . $strSqlRejected;

		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchCustomerClassifiedsByIdsByCid( $arrintCustomerClassifiedIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerClassifiedIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_classifieds
					WHERE
						id IN ( ' . implode( ',', $arrintCustomerClassifiedIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
					ORDER BY
						created_on';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchUnSoldCustomerClassifiedsByCompanyCategoryIdsByPropertyIdByCid( $arrintCompanyClassifiedCategoryIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyClassifiedCategoryIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_classifieds
					WHERE
						company_classified_category_id IN ( ' . implode( ',', $arrintCompanyClassifiedCategoryIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND hide_on > NOW()
					ORDER BY
						id';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerClassifiedsByPropertyIdsByCidByCustomerId( $arrintPropertytIds, $intCid, $intCustomerId, $objDatabase, $intLimit = NULL, $intCompanyClassifiedCategoryId = NULL ) {
		if( false == valArr( $arrintPropertytIds ) ) return NULL;

		$strSql = 'SELECT
						cc.id,
						cc.post,
						cc.title,
						cc.amount,
						cc.customer_id,
						cc.property_id,
						cc.company_classified_category_id,
						( round ( EXTRACT ( EPOCH FROM NOW() - cc.created_on ) / 3600 / 24 ) ) as age
					FROM
						customer_classifieds cc
						JOIN properties p ON ( ( cc.property_id = p.id OR cc.property_id = p.property_id ) AND cc.cid = p.cid )
						LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'ALLOW_RESIDENT_TO_ADD_CLASSIFIED\' )
						JOIN property_products prop_prod ON (
							p.cid = prop_prod.cid
							AND prop_prod.cid = ' . ( int ) $intCid . '
							AND prop_prod.ps_product_id = ' . CPsProduct::RESIDENT_PORTAL . '
							AND ( prop_prod.property_id IS NULL OR prop_prod.property_id IN ( ' . implode( ',', $arrintPropertytIds ) . ' ) )
						)
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertytIds ) . ' )
						AND cc.cid = ' . ( int ) $intCid . '
						AND cc.is_published = 1
						AND cc.deleted_on IS NULL
						AND cc.deleted_by IS NULL
						AND ( cc.hide_on IS NULL OR cc.hide_on > NOW() )
						AND cc.approved_by IS NOT NULL';

			if( true == isset( $intCompanyClassifiedCategoryId ) ) {
				$strSql .= ' AND cc.company_classified_category_id = ' . ( int ) $intCompanyClassifiedCategoryId;
			}

			// TODO - We should decide and discuss if this is really needed.
			$strSql .= ' AND CASE
								WHEN pp.id IS NULL
								THEN ( cc.customer_id != ' . ( int ) $intCustomerId . ' OR cc.customer_id IS NULL )
								ELSE 1 = 1
							END
					GROUP BY
						cc.id,
						cc.post,
						cc.title,
						cc.amount,
						cc.customer_id,
						cc.property_id,
						cc.company_classified_category_id,
						cc.created_on
					ORDER BY
						cc.created_on DESC';

		if( true == isset( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnSoldCustomerClassifiedsByCompanyClassifiedCategoryIdByCid( $intCompanyClassifiedCategoryId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						customer_classifieds
					WHERE
						company_classified_category_id = ' . ( int ) $intCompanyClassifiedCategoryId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND hide_on > NOW()';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchCustomerClassifiedsByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intCid, $objDatabase ) {
		if ( true == is_null( $intCustomerId ) || true == is_null( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_classifieds
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND approved_by IS NOT NULL
					ORDER BY
						created_on DESC';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );
	}

	public static function fetchPaginatedUnapprovedCustomerClassifiedsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT * FROM (
						SELECT
							CASE WHEN TRIM( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' )::int THEN
								  3
								 WHEN TRIM( dp.resident_portal_classifieds->>\'important_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'important_classified_requested_since\' )::int THEN
								  2
								 ELSE
								  1
							END AS priority,
							cc.id,
							cl.id AS lease_id,
							cc.customer_id AS customer_id,
							cc.title AS classified,
							ccc.name AS category,
							cc.created_on AS requested,
							func_format_customer_name( c.name_first, c.name_last ) as customer_name,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
							cl.property_name AS property,
							MAX(cl.id) OVER( PARTITION BY cc.id ORDER BY cl.id DESC ) AS max_lease_id,
							cc.property_id AS cc_property_id , lc.property_id AS lc_property_id
						FROM
						 	customer_classifieds cc
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = cc.property_id )
							JOIN company_classified_categories ccc ON ( ccc.id = cc.company_classified_category_id AND cc.cid = ccc.cid )
							JOIN customers c ON ( c.id = cc.customer_id AND c.cid = cc.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = cc.cid )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = cc.cid )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = cc.cid )
					    WHERE
					      	cc.cid = ' . ( int ) $intCid . '
					      	AND cc.approved_by IS NULL
					      	AND cc.customer_id IS NOT NULL
							AND cc.deleted_on IS NULL
					      	AND cc.is_archived != 1
	    			) classifiedsubquery
			      	WHERE
	    				classifiedsubquery.lease_id = classifiedsubquery.max_lease_id
	    				' . $strPriorityWhere . '
	    			ORDER BY ' . $strSortBy . '
			      	OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedCustomerClassifiedCountByDashboardByCid( $arrintSelectedPropertyGroupIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT count(*) FROM (
						SELECT
							CASE WHEN TRIM( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' )::int THEN
								  3
								 WHEN TRIM( dp.resident_portal_classifieds->>\'important_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'important_classified_requested_since\' )::int THEN
								  2
								 ELSE
								  1
							END AS priority,
							cc.id,
							cl.id AS lease_id,
							cc.customer_id AS customer_id,
							cc.title AS classified,
							ccc.name AS category,
							cc.created_on AS requested,
							func_format_customer_name( c.name_first, c.name_last ) as customer_name,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
							cl.property_name AS property,
							MAX(cl.id) OVER( PARTITION BY cc.id ORDER BY cl.id DESC ) AS max_lease_id
						FROM
						 	customer_classifieds cc
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintSelectedPropertyGroupIds ) . ' ] ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = cc.property_id )
							JOIN company_classified_categories ccc ON ( ccc.id = cc.company_classified_category_id AND cc.cid = ccc.cid )
							JOIN customers c ON ( c.id = cc.customer_id AND c.cid = cc.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = cc.cid )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = cc.cid )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = cc.cid )
					    WHERE
					      	cc.cid = ' . ( int ) $intCid . '
					      	AND cc.approved_by IS NULL
					      	AND cc.customer_id IS NOT NULL
							AND cc.deleted_on IS NULL
					      	AND cc.is_archived != 1
	    			) classifiedsubquery
			      	WHERE
	    				classifiedsubquery.lease_id = classifiedsubquery.max_lease_id';
		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchUnapprovedCustomerClassifiedscountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT count(classifiedsubquery) AS ' .
			( false == $boolIsGroupByProperties ?
				' unapproved_classified_count,
						MAX'
				:
				'resident_classifieds,
						property_id,
						property_name,
					' ) .
						'( priority ) AS priority
					FROM (
							SELECT
        		                rank() OVER( PARTITION BY cc.id ORDER BY cl.id DESC ),
								CASE WHEN TRIM( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'urgent_classified_requested_since\' )::int THEN
									  3
									 WHEN TRIM( dp.resident_portal_classifieds->>\'important_classified_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - cc.created_on ) >= ( dp.resident_portal_classifieds->>\'important_classified_requested_since\' )::int THEN
									  2
									 ELSE
									  1
								END AS priority,
								cl.property_id,
								p.property_name
							FROM
								( 	SELECT
										*
									FROM
										customer_classifieds cc
									WHERE
										cc.cid = ' . ( int ) $intCid . '
						      			AND cc.approved_by IS NULL
						      			AND cc.customer_id IS NOT NULL
						      			AND cc.deleted_on IS NULL
										AND cc.is_archived != 1' .
							      			( false == $boolIsGroupByProperties ?
							      				' OFFSET 0 LIMIT 100'
							      				: '' ) . '
								) AS cc
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.property_id = cc.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								JOIN properties p ON ( p.cid = cc.cid AND p.id = cc.property_id )
								JOIN customers c ON ( c.id = cc.customer_id AND c.cid = cc.cid )
								JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = cc.cid )
								LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = cc.cid )
								LEFT JOIN dashboard_priorities dp ON ( dp.cid = cc.cid )
				    		WHERE
						      	cc.cid = ' . ( int ) $intCid . '
							GROUP BY
					      		cl.id,
						      	cc.id,
          						cc.created_on,
          						dp.resident_portal_classifieds ->> \'urgent_classified_requested_since\',
          						dp.resident_portal_classifieds ->> \'important_classified_requested_since\',
						      	cl.propertY_id,
								p.property_name
					) classifiedsubquery
				    WHERE
						classifiedsubquery.rank = 1
						' . $strPriorityWhere . ' ' .
					( true == $boolIsGroupByProperties ?
					' GROUP BY
							property_id,
							property_name,
							priority'
								: '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClassifiedCategoriesByFilterByClassifiedCategoryIdByPropertyIdByCid( $objCustomerCommunityFilter, $intCompanyClassifiedCategoryId, $intPropertyId, $intCid, $objDatabase ) {

		$strWhereCondition = NULL;

		if( true == valStr( $objCustomerCommunityFilter->getResidentName() ) ) {
			$arrstrExplodedResidentNames = explode( ' ', trim( $objCustomerCommunityFilter->getResidentName() ) );
			if( true == valArr( $arrstrExplodedResidentNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrExplodedResidentNames ) ) {
				$arrstrOrParameters = [];
				foreach( $arrstrExplodedResidentNames as $strExplodedResidentName ) {
					if( true === strrchr( $strExplodedResidentName, '\'' ) ) {
						$strFilteredExplodedResidentName = $strExplodedResidentName;
					} else {
						$strFilteredExplodedResidentName = '\' || \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strExplodedResidentName ) ) . '\' || \'';
					}
					array_push( $arrstrOrParameters, $strFilteredExplodedResidentName );
				}

				$arrstrOrParameters = array_filter( $arrstrOrParameters );

				if( true == valArr( $arrstrOrParameters ) ) {
					$strWhereCondition = 'AND ( c.name_first ILIKE E\'%' . implode( '%\' OR c.name_first ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
					$strWhereCondition .= ' AND ( c.name_last ILIKE E\'%' . implode( '%\' OR c.name_last ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
					$strWhereCondition .= ' AND ( cc.title ILIKE E\'%' . implode( '%\' OR cc.title ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
				}
			} else {
				$strWhereCondition = 'AND ( lower( c.name_first ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' OR lower( c.name_last ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' OR lower( cc.title ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerCommunityFilter->getResidentName() ) ) ) . '%\' )';
			}
		}

		$strSql = 'SELECT
						cc.*, c.name_first, c.name_last,
						cca.id as classified_attachment_id, cca.file_name, cca.file_path
					FROM
					 	customer_classifieds cc
		  				LEFT OUTER JOIN customers c ON cc.customer_id = c.id AND cc.cid = c.cid
						LEFT JOIN customer_classified_attachments cca ON( cc.id = cca.customer_classified_id AND cc.cid = cca.cid AND cca.deleted_on IS NULL)
				    WHERE
				    	cc.company_classified_category_id = ' . ( int ) $intCompanyClassifiedCategoryId . '
				      	AND cc.property_id = ' . ( int ) $intPropertyId . '
				      	AND cc.cid = ' . ( int ) $intCid . '
				      	AND ( cc.hide_on IS NULL OR cc.hide_on > now() )
				      	' . $strWhereCondition . '
					ORDER BY
				      	cc.updated_on desc';

		return self::fetchCustomerClassifieds( $strSql, $objDatabase );

	}

	public static function fetchCustomCustomerClassifiedsByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intCid, $objDatabase ) {

		if ( true == is_null( $intCustomerId ) || true == is_null( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT 
						cc.id,
						cc.cid,
						cc.customer_id,
						cc.company_classified_category_id,
						util_get_translated( \'post\', cc.post, cc.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS post,
						util_get_translated( \'title\', cc.title, cc.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS title,
						cc.amount,
						cc.show_on,
						cc.hide_on,
						ccc.name as category_name,
						CASE WHEN ( cc.approved_on IS NULL ) 
						THEN true
						ELSE false
						END AS awaiting_approval
					FROM
						customer_classifieds cc
					JOIN company_classified_categories ccc 
						ON ( cc.company_classified_category_id = ccc.id AND cc.cid = ccc.cid  )
					WHERE
						cc.customer_id = ' . ( int ) $intCustomerId . '
						AND cc.property_id = ' . ( int ) $intPropertyId . '
						AND cc.cid = ' . ( int ) $intCid . '
						AND cc.is_published = 1
						AND ( cc.hide_on IS NULL OR cc.hide_on > NOW() )
						And cc.deleted_on IS NULL
					ORDER BY
						cc.created_on DESC';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData;
	}

	public static function fetchCustomerClassifiedsByPropertyIdsByCidByCustomerId( $arrintPropertytIds, $intCid, $intCutomerId, $objDatabase, $intPageNo = 0, $intPageSize = 0, $arrintCategoryId = 0 ) {
		if( false == valArr( $arrintPropertytIds ) ) return NULL;
		$strSql = 'SELECT
						cc.id,
						util_get_translated( \'post\', cc.post, cc.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS post,
						util_get_translated( \'title\', cc.title, cc.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS title,
						cc.amount,
						cc.customer_id,
						cc.property_id,
						cc.show_on,
						cc.hide_on,
						ccc.name,
						cc.company_classified_category_id,
						( round ( EXTRACT ( EPOCH FROM NOW() - cc.created_on ) / 3600 / 24 ) ) as age,
						CASE WHEN ( ccf.id IS NOT NULL ) 
						THEN true
						ELSE false
						END AS is_favorite,
						CASE WHEN ( cc.approved_on IS NULL ) 
						THEN true
						ELSE false
						END AS awaiting_approval
					FROM
						customer_classifieds cc
						LEFT JOIN customer_classified_favorites ccf ON ( ccf.customer_classified_id = cc.id AND ccf.customer_id = ' . ( int ) $intCutomerId . ' AND ccf.deleted_on IS NULL )
						JOIN properties p ON ( ( cc.property_id = p.id OR cc.property_id = p.property_id ) AND cc.cid = p.cid )
						JOIN property_products prop_prod ON (
							p.cid = prop_prod.cid
							AND prop_prod.cid = ' . ( int ) $intCid . '
							AND prop_prod.ps_product_id = ' . CPsProduct::RESIDENT_PORTAL . '
							AND ( prop_prod.property_id IS NULL OR prop_prod.property_id IN ( ' . implode( ',', $arrintPropertytIds ) . ' ) )
						)
						JOIN company_classified_categories ccc ON ( cc.company_classified_category_id = ccc.id AND cc.cid = ccc.cid  )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertytIds ) . ' )
						AND cc.cid = ' . ( int ) $intCid . '
						AND cc.is_published = 1
						AND cc.deleted_on IS NULL
						AND cc.deleted_by IS NULL
						AND ( cc.hide_on IS NULL OR cc.hide_on > NOW() )
						AND cc.deleted_by IS NULL
						AND cc.deleted_on IS NULL';
		if( true == valArr( $arrintCategoryId ) ) {
			$strSql .= ' AND cc.company_classified_category_id IN ( ' . implode( ',', $arrintCategoryId ) . ' ) ';
		}
		$strSql .= ' ORDER BY cc.created_on DESC';

		$strSqlOffsetLimit = '';
		if( true == valId( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit = ( int ) $intPageSize;
			$strSqlOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql .= $strSqlOffsetLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerClassifiedByIdByPropertyIdByCid( $intCustomerClassifiedId, $intPropertyId, $intCid, $intCustomerId, $objDatabase ) {
		if ( false == valId( $intCustomerClassifiedId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						cc.*
					FROM
						customer_classifieds cc
						LEFT JOIN customer_classified_attachments cca ON( cc.id = cca.customer_classified_id AND cc.cid = cca.cid AND cca.deleted_on IS NULL )
					WHERE
						cc.id = ' . ( int ) $intCustomerClassifiedId . '
						AND cc.deleted_on IS NULL
						AND cc.deleted_by IS NULL
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND cc.property_id = ' . ( int ) $intPropertyId . '
						AND	cc.cid = ' . ( int ) $intCid . '
						AND cc.deleted_on IS NULL
						AND cc.deleted_by IS NULL';

		return self::fetchCustomerClassified( $strSql, $objDatabase );
	}

}
?>
