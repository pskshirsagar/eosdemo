<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallFrequencies
 * Do not add any new functions to this class.
 */

class CCallFrequencies extends CBaseCallFrequencies {

	public static function fetchCallFrequencies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCallFrequency( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllCallFrequencies( $objDatabase ) {
		$strSql = 'SELECT * FROM call_frequencies ORDER BY order_num, LOWER( name )';

		return self::fetchCallFrequencies( $strSql, $objDatabase );

    }

}
?>