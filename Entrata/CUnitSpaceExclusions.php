<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceExclusions
 * Do not add any new functions to this class.
 */

class CUnitSpaceExclusions extends CBaseUnitSpaceExclusions {

	public static function fetchUnitSpaceExclusionsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						usx.*,
						util_get_system_translated( \'name\', uert.name, uert.details ) AS unit_exclusion_reason_type_name,
						util_get_system_translated( \'name\', uert.name, uert.details ) AS name,
						pgs.group_excluded_units_in_gpr
					FROM
						unit_space_exclusions usx
						JOIN property_gl_settings pgs ON ( pgs.cid = usx.cid AND pgs.property_id = usx.property_id )
						JOIN unit_exclusion_reason_types uert ON ( uert.id = usx.unit_exclusion_reason_type_id )
					WHERE
						usx.cid = ' . ( int ) $intCid . '
						AND usx.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchUnitSpaceExclusions( $strSql, $objClientDatabase );
	}

	public static function fetchUnitSpaceExclusionsNameAndExclusionsTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCondition = ' WHERE uert.id <> ' . CUnitExclusionReasonType::NOT_EXCLUDED . ' ';

		$strSql = 'SELECT
						uert.name,
						use1.details,
						CASE WHEN use1.custom_name IS NULL THEN util_get_system_translated( \'name\', uert.name, uert.details ) ELSE util_get_translated( \'custom_name\', use1.custom_name, use1.details )
						END as custom_name,
						CASE WHEN use1.id IS NOT NULL AND use1.is_marketed = 1 THEN 1 ELSE 0
						END as is_marketed,
						CASE WHEN use1.id IS NOT NULL AND use1.is_reportable IS TRUE THEN TRUE ELSE FALSE
						END as is_reportable,
						CASE WHEN use1.id IS NOT NULL AND use1.is_considered_occupied IS TRUE THEN TRUE ELSE FALSE
						END as is_considered_occupied,
						uert.id as unit_exclusion_reason_type_id
					FROM
						unit_exclusion_reason_types uert
						LEFT JOIN unit_space_exclusions use1 ON ( uert.id = use1.unit_exclusion_reason_type_id AND use1.cid = ' . ( int ) $intCid . ' AND use1.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					' . $strCondition . '
					ORDER BY
						uert.order_num ASC';

		return self::fetchUnitSpaceExclusions( $strSql, $objClientDatabase );
	}

	public static function fetchUnitSpaceExclusionCountForBreakOutExcludedUnitsByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						COUNT( usx.id ) AS count
					FROM
						unit_space_exclusions usx
						JOIN property_gl_settings pgs ON ( usx.cid = pgs.cid AND usx.property_id = pgs.property_id )
					WHERE
						usx.cid = ' . ( int ) $intCid . '
						AND usx.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND pgs.group_excluded_units_in_gpr = 1';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchUnitSpaceExclusionsByUnitExclusionReasonTypeIdByPropertyIdsByCid( $intUnitExclusionReasonTypeId, $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						unit_space_exclusions
					WHERE
						unit_exclusion_reason_type_id = ' . ( int ) $intUnitExclusionReasonTypeId . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND CID = ' . ( int ) $intCid;

		return parent::fetchObjects( $strSql, 'CUnitSpaceExclusion', $objClientDatabase );
	}

}
?>