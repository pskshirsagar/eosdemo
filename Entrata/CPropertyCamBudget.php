<?php

class CPropertyCamBudget extends CBasePropertyCamBudget {

	protected $m_strCamExpenseName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues );
		if( isset( $arrmixValues['cam_expense_name'] ) ) $this->setCamExpenseName( ( $boolStripSlashes ) ? stripslashes( $arrmixValues['cam_expense_name'] ) : $arrmixValues['cam_expense_name'] );

		if( isset( $arrmixValues['account_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strAccountName', trim( $arrmixValues['account_name'] ) );
		} elseif( isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
	}

	public function getCamExpenseName() {
		return $this->m_strCamExpenseName;
	}

	public function setCamExpenseName( $strCamExpenseName ) {
		$this->m_strCamExpenseName = $strCamExpenseName;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false === valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false === valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPropertyCamPoolId() {
		return true;
	}

	public function valPropertyCamPeriodId() {
		return true;
	}

	public function valGlAccountId() {
		$boolIsValid = true;
		if( false === valId( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL Account is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valBudgetAmount() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getBudgetAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_amount', __( 'CAM budget is required.' ) ) );
		}

		// Allowing to add budget with zero annual amount
		if( 0 > $this->getBudgetAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_amount', __( 'CAM budget cannot be negative.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valBudgetAmount();
				$boolIsValid &= $this->valGlAccountId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

}
?>