<?php

class CPropertyBuildingMedia extends CBasePropertyBuildingMedia {

	public function createCompanyMediaFile( $intMediaTypeId ) {
		$objCompanyMediaFile = new CCompanyMediaFile();
		$objCompanyMediaFile->setCid( $this->getCid() );
		$objCompanyMediaFile->setMediaTypeId( $intMediaTypeId );

		return $objCompanyMediaFile;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>