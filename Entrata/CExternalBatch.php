<?php

class CExternalBatch extends CBaseExternalBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMerchantAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributeOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepositDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalBatchNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBatchNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchCreatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchClosedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>