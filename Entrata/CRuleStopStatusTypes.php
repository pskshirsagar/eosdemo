<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRuleStopStatusTypes
 * Do not add any new functions to this class.
 */

class CRuleStopStatusTypes extends CBaseRuleStopStatusTypes {

	public static function fetchRuleStopStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRuleStopStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>