<?php

class CDashboardPriority extends CBaseDashboardPriority {

	const DASHBOARD_PRIORITY_NORMAL 	= 1;
	const DASHBOARD_PRIORITY_IMPORTANT 	= 2;
	const DASHBOARD_PRIORITY_URGENT		= 3;

	public static $c_arrintDashboardPriorities		= [ self::DASHBOARD_PRIORITY_NORMAL, self::DASHBOARD_PRIORITY_IMPORTANT, self::DASHBOARD_PRIORITY_URGENT ];
	public static $c_arrmixDashboardPriorityValues	= [ self::DASHBOARD_PRIORITY_NORMAL => 'normal', self::DASHBOARD_PRIORITY_IMPORTANT => 'important', self::DASHBOARD_PRIORITY_URGENT => 'urgent' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsApplications() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsInvoices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsReversals() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsEsignDocs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsRenewalOffers() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsTransfers() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsPurchaseOrders() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsChecks() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsUtilities() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsFinancialMoveOuts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalsTransactions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsAssignAgents() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsNeverContacted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsFollowUp() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsNotProgressing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsResidentReferrals() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsAnsweredCalls() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadsUnknownEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantsFollowUp() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantsNotProgressing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantsGenerateLeases() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantsMissingInsurance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsFollowUp() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsMoveInReviews() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsMoveIns() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsEvictions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsMoveOuts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsRenewals() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsTransfers() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsRentableItems() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsInsurance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsAdditionalSettings() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentsNotProgressing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentPortalAmenityReservations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentPortalClassifieds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentPortalClubs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentPortalConcierge() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentPortalEvents() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceAssignWorkOrders() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceWorkOrders() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRecurringWorkOrders() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceMakeReadies() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceInspections() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceInspectionReviews() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialPayments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialCheck21() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialDeposits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialNotices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialCollections() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialDeliveryErrors() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialMoveOuts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialAcceleratedRentCredits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>