<?php

class CMaintenanceRequestType extends CBaseMaintenanceRequestType {

	const MAKE_READY      = 1;
	const SERVICE_REQUEST = 2;
	const RECURRING       = 3;
	const FEATURE_UPGRADE = 4;
	const RENOVATION      = 5;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenanceRequestPrefix() {
		if( isset( $this->m_arrmixMaintenanceRequestPrefix ) )
			return $this->m_arrmixMaintenanceRequestPrefix;

		$this->m_arrmixMaintenanceRequestPrefix = [
			self::MAKE_READY      => [
				'prefix'    => __( 'MR' ),
				'type_name' => __( 'Make Ready' ),
				'prefix_en'    => 'MR',
			],
			self::SERVICE_REQUEST => [
				'prefix'    => __( 'SR' ),
				'type_name' => __( 'Service Request' ),
				'prefix_en'    => 'SR',
			],
			self::RECURRING       => [
				'prefix'    => __( 'RE' ),
				'type_name' => __( 'Recurring' ),
				'prefix_en'    => 'RE',
			],
			self::FEATURE_UPGRADE => [
				'prefix'    => __( 'FU' ),
				'type_name' => __( 'Feature Upgrade' ),
				'prefix_en'    => 'FU',
			],
			self::RENOVATION => [
				'prefix'    => __( 'RT' ),
				'type_name' => __( 'Renovation' ),
				'prefix_en'    => 'RT',
			]
		];
		return $this->m_arrmixMaintenanceRequestPrefix;
	}

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public static function getMaintenanceRequestTypeNameById( $intId ) {

    	$strTemplateTypeName = NULL;

    	switch( $intId ) {

    		case CMaintenanceRequestType::MAKE_READY:
    			$strTemplateTypeName = __( 'Make Ready' );
    			break;

    		case CMaintenanceRequestType::SERVICE_REQUEST:
    			$strTemplateTypeName = __( 'Service Request' );
    			break;

    		case CMaintenanceRequestType::RECURRING:
    			$strTemplateTypeName = __( 'Recurring' );
    			break;

    		case CMaintenanceRequestType::FEATURE_UPGRADE:
    			$strTemplateTypeName = __( 'Feature Upgrade' );
    			break;

		    case CMaintenanceRequestType::RENOVATION:
			    $strTemplateTypeName = __( 'Renovation' );
			    break;

    		default:
    			// default case
    			break;
    	}

    	return $strTemplateTypeName;
    }

	public static function getMaintenanceRequestTypes() {
		return [ CMaintenanceRequestType::MAKE_READY      => __( 'Make Ready' ),
		         CMaintenanceRequestType::SERVICE_REQUEST => __( 'Service Request' ),
		         CMaintenanceRequestType::RECURRING       => __( 'Recurring' ),
		         CMaintenanceRequestType::FEATURE_UPGRADE => __( 'Feature Upgrade' ),
		         CMaintenanceRequestType::RENOVATION      => __( 'Renovation' )
		];
	}

}
?>