<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFields
 * Do not add any new functions to this class.
 */
class CCompanyMergeFields extends CBaseCompanyMergeFields {

	public static function fetchCompanyMergeFieldsWithCompanyMergeFieldValuesByMergeFieldTypeIdByCid( $arrintMergeFieldTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintMergeFieldTypeId ) ) return NULL;

		$strSql = 'SELECT
						cmf.*,
						(
							SELECT
									(array_to_string(array_agg( vals.value ), \', \') ) merge_field_values
								FROM
									jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean)
						) As merge_field_values
				FROM
					company_merge_fields cmf
				WHERE
					cmf.cid = ' . ( int ) $intCid . '
					AND cmf.deleted_on IS NULL
					AND cmf.deleted_by IS NULL
					AND cmf.merge_field_type_id IN ( ' . implode( ',', $arrintMergeFieldTypeId ) . ' )
				ORDER BY cmf.id';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByNamesByCid( $arrstrCompanyMergeFieldNames, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrCompanyMergeFieldNames ) ) return NULL;

		$strSql = 'SELECT
						cmf.id, cmf.cid, cmf.name, cmf.description, cmf.is_multiselect, cmf.merge_field_type_id, cmf.view_expression, cmf.original_company_merge_field_id, cmf.updated_by, cmf.updated_on, 	
						cmf.created_by, cmf.created_on, cmf.deleted_by, cmf.deleted_on, cmf.merge_field_group_id, cmf.default_merge_field_id, cmf.block_company_merge_field_id, cmf.default_value, 	
						cmf.is_required, cmf.show_in_merge_display, cmf.allow_user_update, cmf.is_block, cmf.details,	
						bcmf.name as block_name,	
						vals.value as default_value, 
						COALESCE(util_get_translated( \'title\', dmf.title, dmf.details ), util_get_translated( \'title\', cmf.title, cmf.details )) as title
					FROM
						company_merge_fields cmf
						LEFT JOIN jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean) ON vals.is_default
						LEFT JOIN company_merge_fields bcmf ON ( bcmf.cid = cmf.cid AND bcmf.id = cmf.block_company_merge_field_id )
						LEFT JOIN default_merge_fields dmf ON ( cmf.default_merge_field_id = dmf.id )
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.name IN ( \'' . implode( '\', \'', $arrstrCompanyMergeFieldNames ) . '\' )
					';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldByNameByCid( $strCompanyMergeFieldName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cmf.*,
						vals.value as default_value
					FROM
						company_merge_fields cmf
						LEFT JOIN jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean) ON vals.is_default
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.name IN ( \'' . $strCompanyMergeFieldName . '\' )
						AND cmf.deleted_by IS NULL
						AND cmf.deleted_on IS NULL';

		return self::fetchCompanyMergeField( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByIdsByCid( $arrintCompanyMergeFieldIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyMergeFieldIds ) ) return NULL;

		$strSql = 'SELECT
						cmf.*,
						vals.value as default_value
					FROM
						company_merge_fields cmf
						LEFT JOIN jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean) ON vals.is_default
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.id IN ( ' . implode( ',', $arrintCompanyMergeFieldIds ) . ' )
					ORDER BY id';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldHistoryByOriginalCompanyMergeFieldIdByCid( $intOriginalCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valId( $intOriginalCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT
						cmf.*,
						CONCAT ( ce.name_first, \' \', ce.name_last) as created_by_user,
						(
							SELECT
									(array_to_string(array_agg( vals.value ), \', \') ) merge_field_values
								FROM
									jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean)
						) As merge_field_values
				FROM
					company_merge_fields cmf
					LEFT JOIN company_users cu ON ( cu.cid = cmf.cid AND cu.id = cmf.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
				WHERE
					cmf.cid = ' . ( int ) $intCid . '
					AND ( cmf.id = ' . ( int ) $intOriginalCompanyMergeFieldId . ' OR cmf.original_company_merge_field_id = ' . ( int ) $intOriginalCompanyMergeFieldId . ' )
				ORDER BY cmf.id';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByCidBySearchFilter( $intCid, $objDatabase, $arrstrSearchFilter, $strSortBy = 'name', $strSortDirection = 'asc' ) {

		$strWhereCondition = $strJoin = '';

		if( true == valStr( $arrstrSearchFilter['merge_field_name'] ) ) {
			$strWhereCondition .= 'AND ( LOWER( cmf.name ) LIKE \'%' . \Psi\CStringService::singleton()->strtolower( $arrstrSearchFilter['merge_field_name'] ) . '%\' OR LOWER( util_get_translated( \'title\', cmf.title, cmf.details ) ) LIKE \'%' . \Psi\CStringService::singleton()->strtolower( $arrstrSearchFilter['merge_field_name'] ) . '%\' )';
		}

		// if( true == valId( $arrstrSearchFilter['template_id'] ) ) {
		//	$strWhereCondition .= 'AND ' . $arrstrSearchFilter['template_id'] . '\' = ANY( array_agg( cmfd.id ) )';
		// }

		if( true == valStr( $arrstrSearchFilter['template_name'] ) ) {
			// OR d.details->\'marketing_name\' iLIKE \'%' . $arrstrSearchFilter['template_name'] . '%\'
			$strJoin = 'JOIN company_merge_field_documents cmfd1 ON ( cmf.cid = cmfd1.cid AND cmf.id = cmfd1.company_merge_field_id AND cmfd1.deleted_on IS NULL )
					    JOIN documents d ON ( cmfd1.cid = d.cid ANd cmfd1.document_id = d.id AND d.deleted_on IS NULL AND d.name iLIKE \'%' . $arrstrSearchFilter['template_name'] . '%\' )';
		}

		if( true == valArr( $arrstrSearchFilter['merge_field_group_ids'] ) ) {
			$strWhereCondition .= 'AND cmf.merge_field_group_id IN ( ' . implode( ',', $arrstrSearchFilter['merge_field_group_ids'] ) . ' )';
		}

		if( true == valArr( $arrstrSearchFilter['html_input_types'] ) ) {
			$strWhereCondition .= 'AND cmf.details->>\'html_input_type\' IN ( \'' . implode( '\', \'', $arrstrSearchFilter['html_input_types'] ) . '\' )';
		}

		if( 1 == $arrstrSearchFilter['is_system'] ) {
			$strWhereCondition .= 'AND cmf.default_merge_field_id IS NOT NULL';
		} else {
			$strWhereCondition .= 'AND cmf.default_merge_field_id IS NULL';
		}

		$strSql = 'WITH temp_company_merge_field_documents AS (
						SELECT 
							cmf.cid,
						    cmf.id as company_merge_field_id,
						    d.name as document_name
						FROM company_merge_fields cmf
						     JOIN company_merge_field_documents cmfd ON ( cmf.cid = cmfd.cid AND  cmf.id = cmfd.company_merge_field_id AND cmfd.deleted_on IS NULL )
						     LEFT JOIN documents d ON ( d.cid = cmfd.cid AND d.id = cmfd.document_id AND d.deleted_on IS NULL AND d.archived_on IS NULL )
						WHERE cmf.cid = ' . ( int ) $intCid . ' AND
						      cmf.deleted_on IS NULL AND
						      cmf.deleted_by IS NULL AND
						      cmf.block_company_merge_field_id IS NULL
						      ' . $strWhereCondition . '
				), temp_templates_count AS
				( 
					 SELECT 
				        cid, 
				        company_merge_field_id, 
				        count( DISTINCT document_name ) as templates_count 
				     FROM 
				        temp_company_merge_field_documents 
				     GROUP BY  
				        cid, 
				        company_merge_field_id 
				)
				SELECT * FROM ( SELECT
						cmf.cid,
					    cmf.id,
						COALESCE(util_get_translated( \'title\', dmf.title, dmf.details ), util_get_translated( \'title\', cmf.title, cmf.details )) as title,						
					    cmf.name,
					    cmf.details->>\'html_input_type\' as html_input_type,
					    cmf.data_type as merge_field_data_type,
					    cmf.merge_field_type_id as merge_field_type_id,
					     CASE
					        WHEN ( (CAST( cmf.details->>\'html_input_type\' AS TEXT )) = \'checkbox\' AND ( CAST (cmf.default_value AS TEXT )) = \'0\' )
					         THEN \'unchecked\'
					         WHEN ( (CAST( cmf.details->>\'html_input_type\' AS TEXT )) = \'checkbox\'  AND ( CAST (cmf.default_value AS TEXT )) = \'1\' )
					         THEN \'checked\'
					         ELSE
					         util_get_translated( \'default_value\', cmf.default_value, cmf.details )
					         END
					         as custom_default_value,
					    cmf.is_required,
					    cmf.allow_user_update,
					    cmf.show_in_merge_display,
					    cmf.is_block,
					    util_get_translated( \'name\', mfg.name, mfg.details ) as merge_field_group_name,
					    util_get_translated( \'name\', mfsg.name, mfsg.details ) as merge_field_sub_group_name,
					    count( DISTINCT cmfe.id ) as exceptions_count,
					    COALESCE ( ttc.templates_count, 0 ) as templates_count
					FROM
						company_merge_fields cmf
					    JOIN merge_field_groups mfg ON ( cmf.merge_field_group_id = mfg.id )
					    LEFT JOIN merge_field_sub_groups mfsg ON ( cmf.merge_field_sub_group_id = mfsg.id )    
					     ' . $strJoin . ' 
					    LEFT JOIN company_merge_field_exceptions cmfe ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )
					    LEFT JOIN temp_templates_count ttc ON (cmf.cid = ttc.cid AND cmf.id = ttc.company_merge_field_id )
					    LEFT JOIN default_merge_fields dmf ON ( cmf.default_merge_field_id = dmf.id )
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.deleted_on IS NULL
						AND cmf.deleted_by IS NULL
						AND cmf.block_company_merge_field_id IS NULL
					 ' . $strWhereCondition . '
					GROUP BY
						cmf.cid,
					    cmf.id,
						cmf.title,
						cmf.name,
					    cmf.details->>\'html_input_type\',
					    cmf.data_type,
					    cmf.default_value,
					    cmf.is_required,
					    cmf.allow_user_update,
					    util_get_translated( \'name\', mfg.name, mfg.details ),
					    util_get_translated( \'name\', mfsg.name, mfsg.details ),
					    ttc.templates_count,
					    mfg.name,
					    mfsg.name,
					    dmf.title,
                        dmf.details ) as t';
		if( 'merge_field_type' == $strSortBy ) {
			$strSql .= ' ORDER BY COALESCE ( t.html_input_type , t.merge_field_data_type::name ) ' . $strSortDirection;
		} else if( 'title' == $strSortBy ) {
			$strSql .= ' ORDER BY ' . $objDatabase->getCollateSort( 'title', false ) . ' ' . $strSortDirection;
		} else if( 't.name' == $strSortBy || 't.merge_field_group_name' == $strSortBy ) {
			$strSql .= ' ORDER BY ' . $objDatabase->getCollateSort( $strSortBy, true ) . ' ' . $strSortDirection;
		} else {
			$strSql .= ' ORDER BY  (' . $strSortBy . ' ) ' . $strSortDirection;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldDataByIdByCid( $intId, $intCid, $objDatabase ) {

		if( false == valId( $intId ) ) return NULL;

		$strSql = 'select
                        cmf.cid,
					    cmf.id,
						util_get_translated( \'title\', cmf.title, cmf.details ) as title,
					    cmf.name,
					    util_get_translated( \'default_value\', cmf.default_value, cmf.details ) as default_value,
					    cmf.is_required,
					    cmf.allow_user_update,
					    cmf.show_in_merge_display,
					    cmf.default_merge_field_id,
					    cmf.merge_field_type_id as merge_field_type_id,
					    mft.name as merge_field_type,
					    cmf.view_expression->\'values\' as view_expression,
					    cmf.details->\'max_length\' as max_length,
					    cmf.is_block,
					    mfg.name as merge_field_group_name,
					    mfsg.name as merge_field_sub_group_name,
					    mfg.id as merge_field_group_id,
					    mfsg.id as merge_field_sub_group_id,
                        mfg.is_external,
						cmf.details->>\'html_input_type\' as html_input_type,
						cmf.details->>\'has_formatting\' as has_formatting,
						cmf.details->>\'round_up_value\' as round_up_value,
						cmf.data_type as data_type,
						mft.name as merge_field_type,
						(cmf.details->>\'range\')::jsonb->>\'start\' as start_range,
						(cmf.details->>\'range\')::jsonb->>\'end\' as end_range

					FROM
                        company_merge_fields cmf
                        JOIN merge_field_groups mfg ON ( cmf.merge_field_group_id = mfg.id )
                        LEFT JOIN merge_field_sub_groups mfsg ON ( cmf.merge_field_sub_group_id = mfsg.id )
                        LEFT JOIN merge_field_types mft ON ( cmf.merge_field_type_id = mft.id )
                  WHERE
                        cmf.id = ' . ( int ) $intId . '
						AND cmf.cid = ' . ( int ) $intCid . '
						AND cmf.deleted_on IS NULL
						AND cmf.deleted_by IS NULL';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) && true == array_key_exists( 0, $arrmixResult ) ) {
			$arrmixReturnResult = $arrmixResult[0];
		}

		return $arrmixReturnResult;
	}

	public static function fetchSimpleCompanyMergeFieldsByNamesByCid( $arrstrCompanyMergeFieldNames, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrCompanyMergeFieldNames ) ) return NULL;

		$strSql = 'SELECT
						cmf.*
					FROM
						company_merge_fields cmf
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.name IN ( \'' . implode( '\', \'', $arrstrCompanyMergeFieldNames ) . '\' )
						AND is_block = 0
						AND block_company_merge_field_id IS NULL';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchBlockCompanyMergeFieldsByNamesByCid( $arrstrBlockCompanyMergeFieldNames, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrBlockCompanyMergeFieldNames ) ) return NULL;

		$strSql = 'SELECT
						cmf.*
					FROM
						company_merge_fields cmf
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.name IN ( \'' . implode( '\', \'', $arrstrBlockCompanyMergeFieldNames ) . '\' )
						AND is_block = 1';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchBlockChildCompanyMergeFieldsByNamesByCid( $arrstrBlockChildCompanyMergeFieldNames, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrBlockChildCompanyMergeFieldNames ) ) return NULL;

		$strSql = 'SELECT
						cmf.*,
						bcmf.name as block_name
					FROM
						company_merge_fields cmf
						LEFT JOIN company_merge_fields bcmf ON ( bcmf.cid = cmf.cid AND bcmf.id = cmf.block_company_merge_field_id )
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.name IN ( \'' . implode( '\', \'', $arrstrBlockChildCompanyMergeFieldNames ) . '\' )
						AND cmf.block_company_merge_field_id IS NOT NULL';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsDataByExcludingGroupIdsByExcludingMergeFieldNamesByCid( $intCid, $objDatabase, $arrintExcludeMergeFieldGroups = NULL, $arrstrExcludeMergeFields = NULL ) {

		$strCondition = '';

		if( true == valArr( $arrintExcludeMergeFieldGroups ) ) {
			$strCondition .= ' AND mfg.id NOT IN (' . implode( ',', $arrintExcludeMergeFieldGroups ) . ')';
		}

		if( true == valArr( $arrstrExcludeMergeFields ) ) {
			$strCondition .= ' AND cmf.name NOT IN (\'' . implode( '\',\'', $arrstrExcludeMergeFields ) . '\' )';
		}

		$strSql = 'SELECT
		              	cmf.id,
						util_get_translated( \'name\', mfg.name, mfg.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS merge_field_group_name,
						cmf.name,
						cmf.is_block,
						cmf.block_company_merge_field_id,
						mfg.id as merge_field_group_id
                    FROM
		              	company_merge_fields cmf
				    	JOIN merge_field_groups mfg ON ( mfg.id = cmf.merge_field_group_id AND mfg.is_external = 0 )
				    WHERE
				        cmf.cid  = ' . ( int ) $intCid . '
				        AND cmf.deleted_on IS NULL
				        ' . $strCondition . '
                    ORDER BY
						mfg.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByCidByDocumentIds( $intCid, $objDatabase, $arrintDocumentIds ) {

		if( false == valId( $intCid ) || false == valArr( $arrintDocumentIds ) ) return [];

		$strSql = 'WITH temp_company_merge_field_documents AS (
						SELECT 
							cmf.cid,
						    cmf.id as company_merge_field_id,
						    d.name as document_name
						FROM company_merge_fields cmf
						     JOIN company_merge_field_documents cmfd ON ( cmf.cid = cmfd.cid AND  cmf.id = cmfd.company_merge_field_id AND cmfd.deleted_on IS NULL )
						     LEFT JOIN documents d ON ( d.cid = cmfd.cid AND d.id = cmfd.document_id AND d.deleted_on IS NULL AND d.archived_on IS NULL )
						WHERE cmf.cid = ' . ( int ) $intCid . ' AND
						      cmf.deleted_on IS NULL AND
						      cmf.deleted_by IS NULL AND
						      cmf.block_company_merge_field_id IS NULL
				), temp_templates_count AS
				( 
					 SELECT 
				        cid, 
				        company_merge_field_id, 
				        count( DISTINCT document_name ) as templates_count 
				     FROM 
				        temp_company_merge_field_documents 
				     GROUP BY 
				        cid, 
				        company_merge_field_id 
				)
				SELECT
						cmf.cid,
					    cmf.id,
					    cmf.default_merge_field_id,
						COALESCE(util_get_translated( \'title\', dmf.title, dmf.details ), util_get_translated( \'title\', cmf.title, cmf.details )) as title,
					    cmf.name,
					    cmf.details->>\'html_input_type\' as html_input_type,
					    cmf.merge_field_type_id as merge_field_type_id,
					    cmf.data_type as merge_field_data_type,
					    util_get_translated( \'default_value\', cmf.default_value, cmf.details ) as default_value,
					    cmf.is_required,
					    cmf.is_block,
					    cmf.allow_user_update,
					    cmf.show_in_merge_display,
					    mfg.name as merge_field_group_name,   
					    count( DISTINCT cmfe.id ) as exceptions_count,
					    COALESCE ( ttc.templates_count, 0 ) as templates_count
					FROM
						company_merge_fields cmf    
					    JOIN merge_field_groups mfg ON ( cmf.merge_field_group_id = mfg.id )
					    
					    JOIN company_merge_field_documents cmfd1 ON ( cmf.cid = cmfd1.cid AND cmf.id = cmfd1.company_merge_field_id AND cmfd1.deleted_on IS NULL )
					    JOIN documents d ON ( cmfd1.cid = d.cid ANd cmfd1.document_id = d.id AND d.deleted_on IS NULL AND d.id IN(' . implode( ',', $arrintDocumentIds ) . ') )
					    LEFT JOIN company_merge_field_exceptions cmfe ON ( cmf.cid = cmfe.cid AND cmf.id = cmfe.company_merge_field_id )
					    LEFT JOIN temp_templates_count ttc ON (cmf.cid = ttc.cid AND cmf.id = ttc.company_merge_field_id )
					    LEFT JOIN default_merge_fields dmf ON ( cmf.default_merge_field_id = dmf.id )
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.deleted_on IS NULL
						AND cmf.deleted_by IS NULL
						AND cmf.block_company_merge_field_id IS NULL
					GROUP BY
						cmf.cid,
					    cmf.id,
					    cmf.default_merge_field_id,
						cmf.title,
					    cmf.name,
					    cmf.details->>\'html_input_type\',
					    cmf.data_type,
					    cmf.default_value,
					    cmf.is_required,
					    cmf.allow_user_update,
					    mfg.name,					    
					    ttc.templates_count,
					    dmf.title,
						dmf.details
					ORDER BY
						cmf.title';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyMergeFieldsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cmf.id,
						cmf.name,
						cmf.is_block,
						cmf.block_company_merge_field_id
					FROM
						company_merge_fields cmf
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.deleted_by IS NULL
						AND cmf.deleted_on IS NULL
						AND cmf.is_block = 0
						AND cmf.block_company_merge_field_id IS NULL';

		return self::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchParentChildCompanyMergeFieldsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cmf.name as name,
						bcmf.name as block_name,
						( REPLACE ( cmf.name, \'_\', \' \' ) )  as title
					FROM
						company_merge_fields bcmf
						LEFT JOIN company_merge_fields cmf ON ( cmf.cid = bcmf.cid AND bcmf.id  = cmf.block_company_merge_field_id )
					WHERE 
					cmf.cid = ' . ( int ) $intCid . ' AND bcmf.is_block = 1
					    AND bcmf.name IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExternalBluemoonCompanyMergeFieldsByDocumentIds( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintDocumentIds ) ) return [];

		$strSql = 'SELECT
	                   cmf.id as company_merge_field_id,
	                   cmf.name,
	                   cmf.is_required,
	                   cmf.allow_user_update,
	                   cmf.show_in_merge_display,
	                   util_get_translated( \'default_value\', cmf.default_value, cmf.details ) as default_value,
	                   cmfd.document_id,
	                   emf.external_merge_field_name,
	                   emf.data_type	                   
	               FROM
	                   company_merge_fields cmf
	                   JOIN company_merge_field_documents cmfd ON ( cmf.cid = cmfd.cid AND cmf.id = cmfd.company_merge_field_id AND cmfd.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ') AND cmfd.deleted_on IS NULL )
	                   JOIN external_merge_fields emf ON ( cmf.default_merge_field_id = ANY( emf.default_merge_field_ids ) OR cmf.name = emf.external_merge_field_name )
	               WHERE
	                    cmf.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsWithCompanyMergeFieldValuesByHtmlInputTypeByCid( $arrstrHtmlInputType, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrHtmlInputType ) ) return NULL;

		$strSql = 'SELECT
						cmf.*,
						(
							SELECT
									(array_to_string(array_agg( vals.value ), \', \') ) merge_field_values
								FROM
									jsonb_to_recordset( COALESCE(cmf.view_expression->\'values\',\'[{"value":"","is_default":0}]\') ) as vals( value text, is_default boolean)
						) As merge_field_values
				FROM
					company_merge_fields cmf
				WHERE
					cmf.cid = ' . ( int ) $intCid . '
					AND cmf.deleted_on IS NULL
					AND cmf.deleted_by IS NULL
					AND cmf.details->>\'html_input_type\' like \'%' . implode( ',', $arrstrHtmlInputType ) . '%\'
				ORDER BY cmf.id';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function fetchBlockChildCompanyMergeFieldsByMergeFieldIdByCid( $intMergeFieldId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cmf.name 
					FROM
						company_merge_fields cmf
						
					WHERE
						cmf.cid = ' . ( int ) $intCid . '
						AND cmf.block_company_merge_field_id = ' . ( int ) $intMergeFieldId . '
						AND cmf.deleted_on IS NULL
						AND cmf.deleted_by IS NULL';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

	public static function addTenantechCompanyMergeFieldsByMasterDocumentIdByStateCodeByExternalFormKey( int $intMasterDocumentId, array $arrstrStateCode, array $arrstrExternalFormKey, int $intCompanyUserId, int $intCid, CDatabase $objDatabase ) {
		$strSql = 'SELECT * FROM func_sync_tenantech_company_merge_fields(' . $intCid . '::INTEGER, ' .
		          $intMasterDocumentId . '::INTEGER, ' .
		          $intCompanyUserId . '::INTEGER, ' .
		          'ARRAY[\'' . implode( '\',\'', $arrstrStateCode ) . '\']::TEXT[],' .
		          'ARRAY[\'' . implode( '\',\'', $arrstrExternalFormKey ) . '\']::TEXT[]' . ')';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByDefaultMergeFieldIdsByCid( $arrintDefaultMerfeFieldIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDefaultMerfeFieldIds ) ) return NULL;
		$strSql = 'SELECT *
				   FROM
				        company_merge_fields cmf
				   WHERE
				        cmf.cid = ' . ( int ) $intCid . '
				        AND cmf.default_merge_field_id IN ( ' . implode( ',', $arrintDefaultMerfeFieldIds ) . ' )
				        AND cmf.deleted_on IS NULL ';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );

	}

	public static function fetchDefaultBlockCompanyMergeFieldsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						cmf.*
				   FROM
				        company_merge_fields cmf
				   WHERE
				        cmf.cid = ' . ( int ) $intCid . '
				        AND cmf.is_block = 1
				        AND cmf.default_merge_field_id IS NOT NULL
				        AND cmf.deleted_on IS NULL';

		return parent::fetchCompanyMergeFields( $strSql, $objDatabase );
	}

}
?>