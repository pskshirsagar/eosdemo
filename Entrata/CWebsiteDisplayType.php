<?php

class CWebsiteDisplayType extends CBaseWebsiteDisplayType {

	const CLASSIC			= 1;
	const MODERN			= 2;
	const LIMITED			= 3;
	const LEASING_TABLET	= 4;
	const PREMIUM			= 6;

	public static $c_arrstrWebsiteDisplayTypeName = [
		CWebsiteDisplayType::CLASSIC 		=> 'classic',
		CWebsiteDisplayType::MODERN 		=> 'modern',
		CWebsiteDisplayType::LIMITED 		=> 'limited',
		CWebsiteDisplayType::LEASING_TABLET => 'leasing_tablet',
		CWebsiteDisplayType::PREMIUM 		=> 'premium'
	];

}
?>