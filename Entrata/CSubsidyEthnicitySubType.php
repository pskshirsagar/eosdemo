<?php

class CSubsidyEthnicitySubType extends CBaseSubsidyEthnicitySubType {

	const PUERTO_RICAN 								= 1;
	const CUBAN 									= 2;
	const MEXICAN_AMERICAN_CHICANO 					= 3;
	const ANOTHER_HISPANIC_LATINO_OR_SPANISH_ORIGIN = 4;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyEthnicityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSubsidyEthnicityHispanicSubTypeId( $strSubsidyEthnicityHispanicSubType ) {

		switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyEthnicityHispanicSubType ) ) {
			case 'puerto rican':
				$intSubsidyEthnicityHispanicSubTypeId = self::PUERTO_RICAN;
				break;

			case 'cuban':
				$intSubsidyEthnicityHispanicSubTypeId = self::CUBAN;
				break;

			case 'mexican american chicano':
			case 'mexican':
			case 'american':
			case 'chicano':
				$intSubsidyEthnicityHispanicSubTypeId = self::MEXICAN_AMERICAN_CHICANO;
				break;

			case 'another hispanic latino or spanish origin':
			case 'another hispanic latino':
			case 'spanish origin':
			case 'hispanic latino':
				$intSubsidyEthnicityHispanicSubTypeId = self::ANOTHER_HISPANIC_LATINO_OR_SPANISH_ORIGIN;
				break;

			default:
				$intSubsidyEthnicityHispanicSubTypeId = NULL;
				break;
		}

		return $intSubsidyEthnicityHispanicSubTypeId;
	}

}
?>