<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CDocument extends CBaseDocument {
	use Psi\Libraries\EosFoundation\TEosStoredObject;
	protected $m_strCompanyMediaFileName;
	protected $m_strFileType;
	protected $m_strTempFileName;
	protected $m_strFileError;
	protected $m_strExternalKey;
	protected $m_strLastSyncOn;
	protected $m_strCreatedByUserName;
	protected $m_strFileTypeName;
	protected $m_strSystemCode;

	protected $m_arrobjDocuments;
	protected $m_arrobjDocumentComponents;
	protected $m_arrobjDocumentPreferences;
	protected $m_arrobjPropertyGroupDocuments;
	protected $m_arrobjOwnerDocuments;
	protected $m_arrobjDocumentParameters;
	protected $m_arrobjFormAnswers;
	protected $m_arrobjPropertyDocumentFeeTypes;
	protected $m_arrobjDocumentPlugins;
	protected $m_arrobjDocumentLibraries;
	protected $m_arrobjDocumentMergeFields;

	protected $m_objOldDocument;
	protected $m_objDocumentAddenda;

	protected $m_boolIsPrint;
	protected $m_boolShowSecureData;
	protected $m_boolShowInPrimaryNav;
	protected $m_boolShowInSecondaryNav;
	protected $m_boolShowInTertiaryNav;
	protected $m_boolShowInMobilePortal;
	protected $m_boolAttachToEmail;
	protected $m_boolIsForPrimaryApplicant;
	protected $m_boolIsForCoApplicant;
	protected $m_boolIsForCoSigner;
	protected $m_boolRequireConfirmation;
	protected $m_boolShowInPortal;
	protected $m_boolRequireSign;
	protected $m_boolRequireCountersign;
	protected $m_boolLimitCountersign;
	protected $m_boolIsOptional;
	protected $m_boolIsAutoGenerate;
	protected $m_boolIsDefaultSelected;
	protected $m_boolDocumentAddendaIsPublished;
	protected $m_boolIsHidden;
	protected $m_boolIsChangePdfVersion;
	protected $m_boolIsRequired;
	protected $m_boolExcludePdfVersionValidation;

	protected $m_intIsApplyToGuarantor;
	protected $m_intIsApplyToApplicant;
	protected $m_intIsHousehold;
	protected $m_intExcludeOtherSignersInfo;

	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_intPropertyGroupId;
	protected $m_intParentDocumentId;
	protected $m_intDocumentAddendaId;
	protected $m_intCompanyApplicationId;
	protected $m_intDocumentAddendaTypeId;
	protected $m_intFileId;
	protected $m_intDocumentAddendaOrderNum;
	protected $m_intRequireSign;
	protected $m_intMasterDocumentId;

	protected $m_intLateNoticePriority;
	protected $m_intPropertiesCount;

	protected $m_arrstrAllowedFileTypes = [ 'application/msword', 'application/pdf' ];
	protected $m_arrintCountersignCompanyGroupIds = [];

	protected $m_objObjectStorageGatewayResponse;

	public function __construct() {
		parent::__construct();

		$this->m_intIsHousehold = '1';
		$this->m_intIsDynamic   = '0';
	}

	/**
	 * Set Functions
	 */

	public function setCreatedByUserName( $strCreatedByUserName ) {
		$this->m_strCreatedByUserName = $strCreatedByUserName;
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->m_intWebsiteId = $intWebsiteId;
	}

	public function setSeoUri( $strSeoUri ) {
		$this->set( 'm_strSeoUri', CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( $strSeoUri ), -1, NULL, true, true ) );
	}

	public function setDocumentAddendaId( $intDocumentAddendaId ) {
		$this->m_intDocumentAddendaId = $intDocumentAddendaId;
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->m_intCompanyApplicationId = $intCompanyApplicationId;
	}

	public function setAttachToEmail( $boolAttachToEmail ) {
		$this->m_boolAttachToEmail = $boolAttachToEmail;
	}

	public function setIsForPrimaryApplicant( $boolIsForPrimaryApplicant ) {
		$this->m_boolIsForPrimaryApplicant = $boolIsForPrimaryApplicant;
	}

	public function setIsForCoApplicant( $boolIsForCoApplicant ) {
		$this->m_boolIsForCoApplicant = $boolIsForCoApplicant;
	}

	public function setIsForCoSigner( $boolIsForCoSigner ) {
		$this->m_boolIsForCoSigner = $boolIsForCoSigner;
	}

	public function setDocumentAddendaOrderNum( $intDocumentAddendaOrderNum ) {
		$this->m_intDocumentAddendaOrderNum = $intDocumentAddendaOrderNum;
	}

	public function setRequireConfirmation( $boolRequireConfirmation ) {
		$this->m_boolRequireConfirmation = $boolRequireConfirmation;
	}

	public function setRequireSign( $boolRequireSign ) {
		$this->m_boolRequireSign = $boolRequireSign;
	}

	public function setShowInPortal( $boolShowInPortal ) {
		$this->m_boolShowInPortal = CStrings::strToBool( $boolShowInPortal );
	}

	public function setRequireCountersign( $boolRequireCountersign ) {
		$this->m_boolRequireCountersign = CStrings::strToBool( $boolRequireCountersign );
	}

	public function setLimitCountersign( $boolLimitCountersign ) {
		$this->m_boolLimitCountersign = $boolLimitCountersign;
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->m_boolIsOptional = $boolIsOptional;
	}

	public function setIsAutoGenerate( $boolIsAutoGenerate ) {
		$this->m_boolIsAutoGenerate = CStrings::strToBool( $boolIsAutoGenerate );
	}

	public function setIsDefaultSelected( $boolIsDefaultSelected ) {
		$this->m_boolIsDefaultSelected = $boolIsDefaultSelected;
	}

	public function setDocumentAddendaIsPublished( $boolDocumentAddendaIsPublished ) {
		$this->m_boolDocumentAddendaIsPublished = $boolDocumentAddendaIsPublished;
	}

	public function setIsHousehold( $intIsHousehold ) {
		$this->m_intIsHousehold = CStrings::strToIntDef( $intIsHousehold, NULL, false );
	}

	public function setExcludeOtherSignersInfo( $intExcludeOtherSignersInfo ) {
		$this->m_intExcludeOtherSignersInfo = CStrings::strToIntDef( $intExcludeOtherSignersInfo, NULL, false );
	}

	public function setDocumentAddendaTypeId( $intDocumentAddendaTypeId ) {
		$this->m_intDocumentAddendaTypeId = $intDocumentAddendaTypeId;
	}

	public function setFileId( $intFileId ) {
		$this->m_intFileId = $intFileId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = $intPropertyGroupId;
	}

	public function setParentDocumentId( $intParentDocumentId ) {
		$this->m_intParentDocumentId = $intParentDocumentId;
	}

	public function setMasterDocumentId( $intMasterDocumentId ) {
		$this->m_intMasterDocumentId = $intMasterDocumentId;
	}

	public function setDocuments( $arrobjDocuments ) {
		$this->m_arrobjDocuments = $arrobjDocuments;
	}

	public function setDocumentMergeFields( $arrobjDocumentMergeFields ) {
		$this->m_arrobjDocumentMergeFields = $arrobjDocumentMergeFields;
	}

	public function setLateNoticePriority( $intLateNoticePriority ) {
		$this->m_intLateNoticePriority = $intLateNoticePriority;
	}

	public function setCountersignCompanyGroupIds( $arrintCountersignCompanyGroupIds ) {
		$this->m_arrintCountersignCompanyGroupIds = CStrings::strToArrIntDef( $arrintCountersignCompanyGroupIds, NULL );
	}

	public function setIsHidden( $boolIsHidden ) {
		$this->m_boolIsHidden = $boolIsHidden;
	}

	public function setIsChangePdfVersion( $boolIsChangePdfVersion ) {
		$this->m_boolIsChangePdfVersion = $boolIsChangePdfVersion;
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->m_boolIsRequired = $boolIsRequired;
	}

	public function setPropertiesCount( $intPropertiesCount ) {
		$this->m_intPropertiesCount = $intPropertiesCount;
	}

	public function setExcludePdfVersionValidation( $boolExcludePdfVersionValidation ) {
		$this->m_boolExcludePdfVersionValidation = $boolExcludePdfVersionValidation;
	}

	public function setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse ) {
		$this->m_objObjectStorageGatewayResponse = $objObjectStorageGatewayResponse;
	}

	/**
	 * Create Functions
	 */

	public function createOwnerDocument() {

		$objOwnerDocument = new COwnerDocument();
		$objOwnerDocument->setDefaults();
		$objOwnerDocument->setCid( $this->getCid() );
		$objOwnerDocument->setDocumentId( $this->getId() );

		return $objOwnerDocument;
	}

	public function createPropertyGroupDocument() {

		$objPropertyGroupDocument = new CPropertyGroupDocument();
		$objPropertyGroupDocument->setCid( $this->getCid() );
		$objPropertyGroupDocument->setDocumentId( $this->getId() );

		return $objPropertyGroupDocument;
	}

	public function createPropertyApplication() {

		$objPropertyApplication = new CPropertyApplication();
		$objPropertyApplication->setCid( $this->m_intCid );
		$objPropertyApplication->setDocumentId( $this->m_intId );

		return $objPropertyApplication;
	}

	public function createDocumentComponent() {

		$objDocumentComponent = new CDocumentComponent();
		$objDocumentComponent->setCid( $this->m_intCid );
		$objDocumentComponent->setDocumentId( $this->m_intId );

		return $objDocumentComponent;
	}

	public function createDocumentPreference() {

		$objDocumentPreference = new CDocumentPreference();
		$objDocumentPreference->setCid( $this->getCid() );
		$objDocumentPreference->setDocumentId( $this->getId() );

		return $objDocumentPreference;
	}

	public function createDocumentTemplateOrder() {

		$objDocumentTemplateOrder = new CDocumentTemplateOrder();
		$objDocumentTemplateOrder->setCid( $this->getCid() );
		$objDocumentTemplateOrder->setDocumentTemplateId( $this->getDocumentTemplateId() );
		$objDocumentTemplateOrder->setDocumentId( $this->getId() );

		return $objDocumentTemplateOrder;
	}

	public function createDocumentAddenda() {

		$objDocumentAddenda = new CDocumentAddenda();
		$objDocumentAddenda->setDocumentId( $this->getId() );
		$objDocumentAddenda->setCid( $this->getCid() );

		return $objDocumentAddenda;
	}

	public function createDocument() {
		$objDocument = new CDocument();

		$objDocument->setCid( $this->getCid() );
		$objDocument->setDocumentTypeId( $this->getDocumentTypeId() );
		$objDocument->setDocumentSubTypeId( $this->getDocumentSubTypeId() );
		$objDocument->setDocumentId( $this->getDocumentId() );
		$objDocument->setDocumentTemplateId( $this->getDocumentTemplateId() );

		return $objDocument;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setDocumentId( $this->getId() );

		return $objFileAssociation;
	}

	/**
	 * Add Functions
	 */

	public function addDocument( $objDocument ) {
		$this->m_arrobjDocuments[$objDocument->getId()] = $objDocument;
	}

	public function addPropertyDocumentFeeType( $objPropertyDocumentFeeType ) {
		$this->m_arrobjPropertyDocumentFeeTypes[$objPropertyDocumentFeeType->getId()] = $objPropertyDocumentFeeType;
	}

	public function addDocumentMergeField( $objDocumentMergeField ) {
		$this->m_arrobjDocumentMergeFields[$objDocumentMergeField->getId()] = $objDocumentMergeField;
	}

	public function addDocumentPreference( $objDocumentPreference ) {
		$this->m_arrobjDocumentPreferences[$objDocumentPreference->getId()] = $objDocumentPreference;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['company_media_file_name'] ) ) {
			$this->setCompanyMediaFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_media_file_name'] ) : $arrmixValues['company_media_file_name'] );
		}
		if( true == isset( $arrmixValues['show_in_primary_nav'] ) ) {
			$this->setShowInPrimaryNav( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['show_in_primary_nav'] ) : $arrmixValues['show_in_primary_nav'] );
		}
		if( true == isset( $arrmixValues['show_in_secondary_nav'] ) ) {
			$this->setShowInSecondaryNav( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['show_in_secondary_nav'] ) : $arrmixValues['show_in_secondary_nav'] );
		}
		if( true == isset( $arrmixValues['show_in_tertiary_nav'] ) ) {
			$this->setShowInTertiaryNav( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['show_in_tertiary_nav'] ) : $arrmixValues['show_in_tertiary_nav'] );
		}
		if( true == isset( $arrmixValues['show_in_mobile_portal'] ) ) {
			$this->setShowInMobilePortal( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['show_in_mobile_portal'] ) : $arrmixValues['show_in_mobile_portal'] );
		}

		if( true == isset( $arrmixValues['company_application_id'] ) ) {
			$this->setCompanyApplicationId( $arrmixValues['company_application_id'] );
		}
		if( true == isset( $arrmixValues['document_addenda_id'] ) ) {
			$this->setDocumentAddendaId( $arrmixValues['document_addenda_id'] );
		}
		if( true == isset( $arrmixValues['attach_to_email'] ) ) {
			$this->setAttachToEmail( $arrmixValues['attach_to_email'] );
		}
		if( true == isset( $arrmixValues['is_for_primary_applicant'] ) ) {
			$this->setIsForPrimaryApplicant( $arrmixValues['is_for_primary_applicant'] );
		}
		if( true == isset( $arrmixValues['is_for_co_applicant'] ) ) {
			$this->setIsForCoApplicant( $arrmixValues['is_for_co_applicant'] );
		}
		if( true == isset( $arrmixValues['is_for_co_signer'] ) ) {
			$this->setIsForCoSigner( $arrmixValues['is_for_co_signer'] );
		}
		if( true == isset( $arrmixValues['is_optional'] ) ) {
			$this->setIsOptional( $arrmixValues['is_optional'] );
		}
		if( true == isset( $arrmixValues['is_auto_generate'] ) ) {
			$this->setIsAutoGenerate( $arrmixValues['is_auto_generate'] );
		}
		if( true == isset( $arrmixValues['document_addenda_order_num'] ) ) {
			$this->setDocumentAddendaOrderNum( $arrmixValues['document_addenda_order_num'] );
		}
		if( true == isset( $arrmixValues['require_confirmation'] ) ) {
			$this->setRequireConfirmation( $arrmixValues['require_confirmation'] );
		}
		if( true == isset( $arrmixValues['file_id'] ) ) {
			$this->setFileId( $arrmixValues['file_id'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['property_group_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_group_id'] );
		}
		if( true == isset( $arrmixValues['parent_document_id'] ) ) {
			$this->setParentDocumentId( $arrmixValues['parent_document_id'] );
		}
		if( true == isset( $arrmixValues['external_key'] ) ) {
			$this->setExternalKey( $arrmixValues['external_key'] );
		}
		if( true == isset( $arrmixValues['last_sync_on'] ) ) {
			$this->setLastSyncOn( $arrmixValues['last_sync_on'] );
		}
		if( true == isset( $arrmixValues['is_default_selected'] ) ) {
			$this->setIsDefaultSelected( $arrmixValues['is_default_selected'] );
		}
		if( true == isset( $arrmixValues['is_household'] ) ) {
			$this->setIsHousehold( $arrmixValues['is_household'] );
		}
		if( true == isset( $arrmixValues['exclude_other_signers_info'] ) ) {
			$this->setExcludeOtherSignersInfo( $arrmixValues['exclude_other_signers_info'] );
		}
		if( true == isset( $arrmixValues['require_sign'] ) ) {
			$this->setRequireSign( $arrmixValues['require_sign'] );
		}
		if( true == isset( $arrmixValues['show_in_portal'] ) ) {
			$this->setShowInPortal( $arrmixValues['show_in_portal'] );
		}
		if( true == isset( $arrmixValues['file_type_name'] ) ) {
			$this->setFileTypeName( $arrmixValues['file_type_name'] );
		}
		if( true == isset( $arrmixValues['system_code'] ) ) {
			$this->setSystemCode( $arrmixValues['system_code'] );
		}
		if( true == isset( $arrmixValues['created_by_username'] ) ) {
			$this->setCreatedByUserName( $arrmixValues['created_by_username'] );
		}
		if( true == isset( $arrmixValues['document_addenda_is_published'] ) ) {
			$this->setDocumentAddendaIsPublished( $arrmixValues['document_addenda_is_published'] );
		}
		if( true == isset( $arrmixValues['master_document_id'] ) ) {
			$this->setMasterDocumentId( $arrmixValues['master_document_id'] );
		}
		if( true == isset( $arrmixValues['countersign_company_group_ids'] ) ) {
			$this->setCountersignCompanyGroupIds( $arrmixValues['countersign_company_group_ids'] );
		}
		if( true == isset( $arrmixValues['require_countersign'] ) ) {
			$this->setRequireCountersign( $arrmixValues['require_countersign'] );
		}
		if( true == isset( $arrmixValues['limit_countersign'] ) ) {
			$this->setLimitCountersign( $arrmixValues['limit_countersign'] );
		}
		if( true == isset( $arrmixValues['is_hidden'] ) ) {
			$this->setIsHidden( $arrmixValues['is_hidden'] );
		}
		if( true == isset( $arrmixValues['properties_count'] ) ) {
			$this->setPropertiesCount( $arrmixValues['properties_count'] );
		}
	}

	public function setCompanyMediaFileName( $strCompanyMediaFileName ) {
		$this->m_strCompanyMediaFileName = CStrings::strTrimDef( $strCompanyMediaFileName, 50, NULL, true );
	}

	public function setOwnerDocuments( $arrobjOwnerDocuments ) {
		$this->m_arrobjOwnerDocuments = $arrobjOwnerDocuments;
	}

	public function setPropertyGroupDocuments( $arrobjPropertyGroupDocuments ) {
		$this->m_arrobjPropertyGroupDocuments = $arrobjPropertyGroupDocuments;
	}

	public function setFormAnswers( $arrobjFormAnswers ) {
		$this->m_arrobjFormAnswers = $arrobjFormAnswers;
	}

	public function setIsPrint( $boolIsPrint ) {
		$this->m_boolIsPrint = $boolIsPrint;
	}

	public function setShowSecureData( $boolShowSecureData ) {
		$this->m_boolShowSecureData = $boolShowSecureData;
	}

	public function setShowInPrimaryNav( $boolShowInPrimaryNav ) {
		$this->m_boolShowInPrimaryNav = $boolShowInPrimaryNav;
	}

	public function setShowInSecondaryNav( $boolShowInSecondaryNav ) {
		$this->m_boolShowInSecondaryNav = $boolShowInSecondaryNav;
	}

	public function setShowInTertiaryNav( $boolShowInTertiaryNav ) {
		$this->m_boolShowInTertiaryNav = $boolShowInTertiaryNav;
	}

	public function setShowInMobilePortal( $boolShowInMobilePortal ) {
		$this->m_boolShowInMobilePortal = $boolShowInMobilePortal;
	}

	public function setFileType( $strFileType ) {
		$this->m_strFileType = $strFileType;
	}

	public function setFileTypeName( $strFileTypeName ) {
		$this->m_strFileTypeName = $strFileTypeName;
	}

	public function setSystemCode( $strSystemCode ) {
		$this->m_strSystemCode = $strSystemCode;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setExternalKey( $strExternalKey ) {
		$this->m_strExternalKey = $strExternalKey;
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->m_strLastSyncOn = $strLastSyncOn;
	}

	public function setIsApplyToGuarantor( $intIsApplyToGuarantor ) {
		$this->m_intIsApplyToGuarantor = $intIsApplyToGuarantor;
	}

	public function setIsApplyToApplicant( $intIsApplyToApplicant ) {
		$this->m_intIsApplyToApplicant = $intIsApplyToApplicant;
	}

	public function setOldDocument( $objOldDocument ) {
		$this->m_objOldDocument = $objOldDocument;
	}

	public function setDocumentAddenda( $objDocumentAddenda ) {
		$this->m_objDocumentAddenda = $objDocumentAddenda;
	}

	public function setFileData( $arrstrFileData, $strDocumentFileName = NULL, $strCategoryName = NULL ) {

		if( true == isset( $arrstrFileData['tmp_name'] ) ) {
			$this->setTempFileName( $arrstrFileData['tmp_name'] );
		}
		if( true == isset( $arrstrFileData['error'] ) ) {
			$this->setFileError( $arrstrFileData['error'] );
		}
		if( true == isset( $arrstrFileData['title'] ) ) {
			$this->setTitle( $arrstrFileData['title'] );
		}
		if( true == isset( $arrstrFileData['name'] ) ) {
			$this->setFileName( $arrstrFileData['name'] );
		}
		if( true == isset( $arrstrFileData['type'] ) ) {
			$this->setFileType( $arrstrFileData['type'] );
		}

		$this->setFilePath( $this->buildDocumentFilePath( $strDocumentFileName, $strCategoryName ) );
	}

	/**
	 * Get Functions
	 */

	public function getCompanyMediaFileName() {
		return $this->m_strCompanyMediaFileName;
	}

	public function getCreatedByUserName() {
		return $this->m_strCreatedByUserName;
	}

	public function getDocumentLibraries() {
		return $this->m_arrobjDocumentLibraries;
	}

	public function getDocumentPlugins() {
		return $this->m_arrobjDocumentPlugins;
	}

	public function getDocumentComponents() {
		return $this->m_arrobjDocumentComponents;
	}

	public function getDocumentParameters() {
		return $this->m_arrobjDocumentParameters;
	}

	public function getOwnerDocuments() {
		return $this->m_arrobjPropertyGroupDocuments;
	}

	public function getPropertyGroupDocuments() {
		return $this->m_arrobjPropertyGroupDocuments;
	}

	public function getDocuments() {
		return $this->m_arrobjDocuments;
	}

	public function getPropertyDocumentFeeTypes() {
		return $this->m_arrobjPropertyDocumentFeeTypes;
	}

	public function getFormAnswers() {
		return $this->m_arrobjFormAnswers;
	}

	public function getDocumentSeoName() {
		return \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^a-zA-Z0-9\s_' . "'" . '\-]+/', '/(\s|-)+/', '/[_]/' ], [ '-', '-', '-' ], $this->getName() ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function getShowInPrimaryNav() {
		return $this->m_boolShowInPrimaryNav;
	}

	public function getShowInSecondaryNav() {
		return $this->m_boolShowInSecondaryNav;
	}

	public function getShowInTertiaryNav() {
		return $this->m_boolShowInTertiaryNav;
	}

	public function getShowInMobilePortal() {
		return $this->m_boolShowInMobilePortal;
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function getFileTypeName() {
		return $this->m_strFileTypeName;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getExternalKey() {
		return $this->m_strExternalKey;
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function getIsApplyToGuarantor() {
		return $this->m_intIsApplyToGuarantor;
	}

	public function getIsApplyToApplicant() {
		return $this->m_intIsApplyToApplicant;
	}

	public function getOldDocument() {
		return $this->m_objOldDocument;
	}

	public function getDocumentAddenda() {
		return $this->m_objDocumentAddenda;
	}

	public function getDocumentAddendaId() {
		return $this->m_intDocumentAddendaId;
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function getAttachToEmail() {
		return $this->m_boolAttachToEmail;
	}

	public function getIsForPrimaryApplicant() {
		return $this->m_boolIsForPrimaryApplicant;
	}

	public function getIsForCoApplicant() {
		return $this->m_boolIsForCoApplicant;
	}

	public function getIsForCoSigner() {
		return $this->m_boolIsForCoSigner;
	}

	public function getIsAutoGenerate() {
		return $this->m_boolIsAutoGenerate;
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function getIsDefaultSelected() {
		return $this->m_boolIsDefaultSelected;
	}

	public function getRequireConfirmation() {
		return $this->m_boolRequireConfirmation;
	}

	public function getRequireSign() {
		return $this->m_boolRequireSign;
	}

	public function getShowInPortal() {
		return $this->m_boolShowInPortal;
	}

	public function getRequireCountersign() {
		return $this->m_boolRequireCountersign;
	}

	public function getLimitCountersign() {
		return $this->m_boolLimitCountersign;
	}

	public function getDocumentAddendaIsPublished() {
		return $this->m_boolDocumentAddendaIsPublished;
	}

	public function getIsHousehold() {
		return $this->m_intIsHousehold;
	}

	public function getExcludeOtherSignersInfo() {
		return $this->m_intExcludeOtherSignersInfo;
	}

	public function getDocumentAddendaOrderNum() {
		return $this->m_intDocumentAddendaOrderNum;
	}

	public function getDocumentAddendaTypeId() {
		return $this->m_intDocumentAddendaTypeId;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function getParentDocumentId() {
		return $this->m_intParentDocumentId;
	}

	public function getMasterDocumentId() {
		return $this->m_intMasterDocumentId;
	}

	public function getCountersignCompanyGroupIds() {
		return $this->m_arrintCountersignCompanyGroupIds;
	}

	public function getIsHidden() {
		return $this->m_boolIsHidden;
	}

	public function getIsChangePdfVersion() {
		return $this->m_boolIsChangePdfVersion;
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function getPropertiesCount() {
		return $this->m_intPropertiesCount;
	}

	public function getLeaseDocumentAppliedToLable( $boolFromApplication = true ) {

		if( 1 == $this->getIsForPrimaryApplicant() && 1 == $this->getIsForCoApplicant() && 1 == $this->getIsForCoSigner() ) {
			return $strLable = ( true == $boolFromApplication ) ? __( 'Applicants & Guarantors' ) : __( 'Residents & Guarantors' );
		} elseif( 1 == $this->getIsForPrimaryApplicant() && 1 == $this->getIsForCoApplicant() ) {
			return $strLable = ( true == $boolFromApplication ) ? __( 'Applicants Only' ) : __( 'Residents Only' );
		} elseif( 1 == $this->getIsForPrimaryApplicant() && 1 == $this->getIsForCoSigner() ) {
			return $strLable = ( true == $boolFromApplication ) ? __( 'Primary Applicants & Guarantors' ) : __( 'Primary Residents & Guarantors' );
		} elseif( 1 == $this->getIsForCoApplicant() && 1 == $this->getIsForCoSigner() ) {
			return __( 'Co-Applicants & Guarantors' );
		} elseif( 1 == $this->getIsForPrimaryApplicant() ) {
			return $strLable = ( true == $boolFromApplication ) ? __( 'Primary Applicants Only' ) : __( 'Primary Residents Only' );
		} elseif( 1 == $this->getIsForCoApplicant() ) {
			return __( 'Co-Applicant Only' );
		} elseif( 1 == $this->getIsForCoSigner() ) {
			return __( 'Guarantor Only' );
		}

	}

	public function getExternalLeaseDocuments( $arrobjLeaseDocuments ) {

		$arrobjExternalLeaseDocuments = [];

		if( NULL == $this->getTransmissionVendorId() ) {
			return $arrobjExternalLeaseDocuments;
		}

		if( false == valArr( $arrobjLeaseDocuments ) ) {
			return $arrobjLeaseDocuments;
		}

		foreach( $arrobjLeaseDocuments as $objDocument ) {
			if( false == is_null( $objDocument->getTransmissionVendorId() ) ) {
				$arrobjExternalLeaseDocuments[$objDocument->getId()] = $objDocument;
			}
		}

		return $arrobjExternalLeaseDocuments;
	}

	public function getMasterGuarantorDocument( $arrobjChildDocuments ) {

		if( true == $this->getIsBluemoon() || false == valArr( $arrobjChildDocuments ) ) {
			return NULL;
		}

		foreach( $arrobjChildDocuments as $objDocument ) {
			if( CDocumentAddendaType::MASTER_GUARANTOR == $objDocument->getDocumentAddendaTypeId() ) {
				return $objDocument;
			}
		}

		return NULL;
	}

	public function getDocumentMergeFields() {
		return $this->m_arrobjDocumentMergeFields;
	}

	public function getLateNoticePriority() {
		return $this->m_intLateNoticePriority;
	}

	public function getDocumentPreferences() {
		return $this->m_arrobjDocumentPreferences;
	}

	public function getFileNameExtension() {

		$intPosDot = \Psi\CStringService::singleton()->strrpos( $this->getFileName(), '.' );
		if( $intPosDot === false ) {
			return $this->getFileName();
		}

		return \Psi\CStringService::singleton()->substr( $this->getFileName(), $intPosDot + 1 );
	}

	public function getExcludePdfVersionValidation() {
		return $this->m_boolExcludePdfVersionValidation;
	}

	public function getObjectStorageGatewayResponse() {
		return $this->m_objObjectStorageGatewayResponse;
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchDocumentPreferences( $objDatabase ) {

		if( false == valArr( $this->m_arrobjDocumentPreferences ) ) {
			$this->m_arrobjDocumentPreferences = $this->fetchDocumentPreferences( $objDatabase );
		}

		return $this->m_arrobjDocumentPreferences;
	}

	public function getOrFetchLeaseDocuments( $objDatabase, $boolIsOptional = NULL, $boolIsPublished = true ) {

		if( false == valArr( $this->m_arrobjDocuments ) ) {
			$this->m_arrobjDocuments = $this->fetchLeaseDocuments( $objDatabase, $boolIsOptional, $boolIsPublished );
		}

		return $this->m_arrobjDocuments;
	}

	public function getOrFetchDocumentMergeFields( $objDatabase ) {
		if( false == valArr( $this->m_arrobjDocumentMergeFields ) ) {
			$this->m_arrobjDocumentMergeFields = $this->fetchDocumentMergeFields( $objDatabase );
		}

		return $this->m_arrobjDocumentMergeFields;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchClient( $objAdminDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchDocumentTemplates( $objAdminDatabase ) {
		return CDocumentTemplates::fetchDocumentTemplatesByDocumentTypeIdByDocumentSubTypeId( $this->getDocumentTypeId(), $this->getDocumentSubTypeId(), $objAdminDatabase );
	}

	public function fetchForms( $objDatabase ) {
		return CForms::fetchFormsByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchFilteredForms( $objGraphFilter, $objDatabase ) {
		return CForms::fetchFilteredFormsByDocumentIdByCid( $this->m_intId, $this->getCid(), $objGraphFilter, $objDatabase );
	}

	public function fetchSwitchDocumentByDirection( $strDirection, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase ) {
		return CDocuments::fetchSwitchDocumentByDirectionByCid( $this, $strDirection, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase );
	}

	public function fetchSwitchOwnerDocumentByDirection( $strDirection, $arrintOwnerIds, $objDatabase ) {
		return CDocuments::fetchSwitchOwnerDocumentByDirectionByCid( $this, $strDirection, $arrintOwnerIds, $objDatabase );
	}

	public function fetchSwitchDocumentByDirectionByDocumentInclusionTypeId( $strDirection, $arrintDocumentInclusionTypeId, $objDatabase ) {
		return CDocuments::fetchSwitchDocumentByDirectionByDocumentInclusionTypeIdByCid( $this, $strDirection, $arrintDocumentInclusionTypeId, $objDatabase );
	}

	public function fetchDisplacedDocumentByDirection( $strDirection, $objWebsite, $objDatabase ) {
		return CDocuments::fetchDisplacedDocumentByDirectionByCid( $strDirection, $this, $objWebsite, $objDatabase );
	}

	public function fetchApplications( $objDatabase ) {
		return CApplications::fetchApplicationsByDocumentId( $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupDocumentsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyGroupDocuments::fetchPropertyGroupDocumentsByDocumentIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyGroupDocumentsByPropertyGroupIds( $arrintPropertyGroupIds, $objDatabase ) {
		return CPropertyGroupDocuments::fetchPropertyGroupDocumentsByDocumentIdByPropertyGroupIdsByCid( $this->getId(), $arrintPropertyGroupIds, $this->getCid(), $objDatabase );
	}

	public function fetchOwnerDocumentsByOwnerIds( $arrintOwnerIds, $objDatabase ) {
		return COwnerDocuments::fetchOwnerDocumentsByDocumentIdOwnerIdsByCid( $this->getId(), $arrintOwnerIds, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentsByCidByDocumentInclusionTypeId( $intCid, $intDocumentInclusionTypeId, $objDatabase ) {
		return CDocuments::fetchDocumentsByDocumentInclusionTypeIdByDocumentIdByCid( $intDocumentInclusionTypeId, $this->getId(), $intCid, $objDatabase );
	}

	public function fetchOwnerDocumentsByCidOwnerIds( $intCid, $arrintOwnerIds, $objDatabase ) {
		return COwnerDocuments::fetchOwnerDocumentsByCidDocumentIdOwnerIds( $intCid, $arrintOwnerIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupDocumentsNotInPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyGroupDocuments::fetchPropertyGroupDocumentsByDocumentIdNotInPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentComponents( $objDatabase ) {
		$this->m_arrobjDocumentComponents = CDocumentComponents::fetchCustomDocumentComponentsByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );

		return $this->m_arrobjDocumentComponents;
	}

	public function fetchDocumentParametersByDocumentComponentIds( $arrintDocumentComponentIds, $boolIsCustomPage = false, $objDatabase ) {
		$this->m_arrobjDocumentParameters = \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByDocumentComponentIdsByDocumentIdByCid( $arrintDocumentComponentIds, $this->m_intId, $this->getCid(), $boolIsCustomPage, $objDatabase );

		return $this->m_arrobjDocumentParameters;
	}

	public function fetchDocumentParameters( $objDatabase ) {
		$this->m_arrobjDocumentParameters = \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );

		return $this->m_arrobjDocumentParameters;
	}

	public function fetchPropertyGroupDocumentByPropertyIdByDocumentAssociationTypeId( $intPropertyId, $intDocumentAssociationTypeId, $objDatabase ) {
		return CPropertyGroupDocuments::fetchPropertyGroupDocumentByDocumentIdByPropertyIdByDocumentAssociationTypeIdByCid( $this->m_intId, $intPropertyId, $intDocumentAssociationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyGroupDocuments( $objDatabase ) {
		return CPropertyGroupDocuments::fetchPropertyGroupDocumentsByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentComponentsByComponentTypes( $arrstrComponentTypes, $objDatabase ) {
		return CDocumentComponents::fetchDocumentComponentsByDocumentIdByComponentTypesByCid( $this->m_intId, $arrstrComponentTypes, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentComponentById( $intDocumentComponentId, $objDatabase ) {
		return CDocumentComponents::fetchDocumentComponentByDocumentIdByDocumentComponentIdByCid( $this->m_intId, $intDocumentComponentId, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\CFileAssociations::createService()->fetchFileAssociationsByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentPreferenceByKey( $strDocumentPreferenceKey, $objDatabase ) {
		return CDocumentPreferences::fetchDocumentPreferenceByKeyByDocumentIdByCid( $this->m_intId, $strDocumentPreferenceKey, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentPreferences( $objDatabase ) {
		return CDocumentPreferences::fetchDocumentPreferenceByDocumentIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentAddenda( $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendaByDocumentIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseDocuments( $objDatabase, $boolIsOptional = NULL, $boolIsPublished = true, $boolHasExternalKey = NULL ) {
		$boolIncludeArchived = ( false == is_null( $this->getArchivedOn() ) ) ? true : false;

		return CDocuments::fetchLeaseDocumentsByDocumentIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsOptional, $boolIsPublished, $boolHasExternalKey, $boolIncludeArchived );
	}

	public function fetchLeaseDocumentsByIds( $arrintDocumentIds, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		$boolIncludeArchived = ( false == is_null( $this->getArchivedOn() ) ) ? true : false;

		return CDocuments::fetchLeaseDocumentsByIdsByDocumentIdsByCid( $arrintDocumentIds, [ $this->getId() ], $this->getCid(), $objDatabase, $boolIncludeArchived );
	}

	public function fetchDocuments( $objDatabase, $boolIncludeMasterDocument = false ) {
		return CDocuments::fetchCustomDocumentsByDocumentIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeMasterDocument );
	}

	public function fetchDocumentById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchDocumentByIdByDocumentIdByCid( $intDocumentId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDocumentAddendaByMasterDocumentId( $intMasterDocumentId, $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendaByDocumentIdByMasterDocumentIdByCid( $this->getId(), $intMasterDocumentId, $this->getCid(), $objDatabase );
	}

	public function fetchChildDocumentAddendaHavingMaxOrderNum( $objDatabase ) {
		return CDocumentAddendas::fetchChildDocumentAddendaHavingMaxOrderNumByDocumentIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaxChildDocumentAddendaOrderNum( $objDatabase, $boolHasExternalKey = NULL ) {
		return CDocumentAddendas::fetchMaxChildDocumentAddendaOrderNumByMasterDocumentIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolHasExternalKey );
	}

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDocumentTemplateId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDocumentTemplateId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_template_id', __( 'Document template is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDocumentTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDocumentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_type_id', __( 'Document type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDocumentSubTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDocumentSubTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_sub_type_id', __( 'Document sub type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyMediaFileId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyMediaFileId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_media_file_id', __( 'Document file is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valParentDocumentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getParentDocumentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_document_id', __( 'Parent document is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileExtensionId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		if( true == is_null( $this->getTitle() ) || 0 == strlen( trim( $this->getTitle() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL, $boolIsWebsiteDocument = false, $intDocumentInclusionType = 0 ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) || 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {

			if( false == $boolIsWebsiteDocument ) {

				if( CDocumentInclusionType::COMPANY == $intDocumentInclusionType ) {
					$intConfilictingDocumentCount = CDocuments::fetchConflictingDocumentCount( $this, $boolIsValidateSeoUri = false, $objDatabase, $boolCheckDeletedBy = true, $intIsArchived = NULL, $intDocumentInclusionType );
				} else {
					$intConfilictingDocumentCount = CDocuments::fetchConflictingPropertyGroupDocumentCount( $this, $boolIsValidateSeoUri = false, $objDatabase, $boolCheckDeletedBy = true, $intDocumentInclusionType );
				}

				if( 0 < $intConfilictingDocumentCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
					$boolIsValid = false;
				}
			} else {

				$intConfilictingWebsiteDocumentCount = \Psi\Eos\Entrata\CWebsiteDocuments::createService()->fetchConflictingWebsiteDocumentCountByIdByWebsiteIdByNameByCid( $this->getId(), $this->getWebsiteId(), $this->getName(), $this->getCid(), $objDatabase, $this->getDocumentId() );

				if( 0 < $intConfilictingWebsiteDocumentCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Page name is already in use for this website.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valLeaseDocumentName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) || 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {
			if( true == valObj( $this->getOldDocument(), 'CDocument' ) ) {
				$arrintExcludingDocumentIds[] = $this->getOldDocument()->getId();
				$objOldDocumentName           = CDocuments::fetchDocumentNameByFieldNameByCidById( $this->getOldDocument()->getId(), $this->getCid(), $objDatabase );
			} else {
				$objOldDocumentName = CDocuments::fetchDocumentNameByFieldNameByCidById( $this->getId(), $this->getCid(), $objDatabase );
			}

			$arrintExcludingDocumentIds[] = $this->getId();

			$intConfilictingDocumentCount = NULL;

			if( $this->getName() != $objOldDocumentName && false == $this->getIsBluemoon() ) {
				$intConfilictingDocumentCount = CDocuments::fetchDuplicateDocumentNameCountByExcludingDocumentIdsByFileTypeIdByCid( $arrintExcludingDocumentIds, $this, $objDatabase, $this->getDocumentTypeId(), $this->getDocumentSubTypeId(), NULL, $boolCheckDeletedBy = true, $boolCheckIsArchived = true );
			}

			if( 0 < $intConfilictingDocumentCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valSystemDocumentName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) || 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {
			if( true == valObj( $this->getOldDocument(), 'CDocument' ) ) {
				$arrintExcludingDocumentIds[] = $this->getOldDocument()->getId();
				$objOldDocumentName           = CDocuments::fetchDocumentNameByFieldNameByCidById( $this->getOldDocument()->getId(), $this->getCid(), $objDatabase );
			} else {
				$objOldDocumentName = CDocuments::fetchDocumentNameByFieldNameByCidById( $this->getId(), $this->getCid(), $objDatabase );
			}

			$arrintExcludingDocumentIds[] = $this->getId();

			$intConfilictingDocumentCount = CDocuments::fetchDuplicateDocumentNameCountByExcludingDocumentIdsByFileTypeIdByCid( $arrintExcludingDocumentIds, $this, $objDatabase, $this->getDocumentTypeId(), $this->getDocumentSubTypeId(), $this->getFileTypeId(), $boolCheckDeletedBy = true, $boolCheckIsArchived = true );

			if( 0 < $intConfilictingDocumentCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valSeoUri( $objDatabase = NULL, $boolIsWebsiteDocument = false, $intDocumentInclusionType = 0 ) {
		$boolIsValid = true;

		if( false == is_null( $objDatabase ) ) {

			if( false == valStr( $this->getSeoUri() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'SEO Uri is required.' ) ) );

				return $boolIsValid;
			}

			if( true == $boolIsWebsiteDocument ) {
				$intConfilictingDocumentCount = CDocuments::fetchConflictingWebsiteDocumentCount( $this, $objDatabase );
			} elseif( CDocumentInclusionType::COMPANY == $intDocumentInclusionType ) {
				$intConfilictingDocumentCount = CDocuments::fetchConflictingDocumentCount( $this, $boolIsValidateSeoUri = true, $objDatabase, $boolCheckDeletedBy = true, $intIsArchived = NULL, $intDocumentInclusionType );
			} else {
				$intConfilictingDocumentCount = CDocuments::fetchConflictingPropertyGroupDocumentCount( $this, $boolIsValidateSeoUri = true, $objDatabase, $boolCheckDeletedBy = true, $intDocumentInclusionType );
			}

			if( 0 < $intConfilictingDocumentCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'seo_uri', __( 'SEO Uri is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valDocumentRedirectUrl() {
		$boolIsValid = true;

		if( true == valStr( $this->getRedirectUrl() ) && false == CValidation::checkUrl( $this->getRedirectUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link_uri', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valKeywords() {
		$boolIsValid = true;

		if( true == is_null( $this->getKeywords() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keywords', '' ) );
		}

		return $boolIsValid;
	}

	public function valContent() {
		$boolIsValid = true;

		if( true == is_null( $this->getContent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'content', '' ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Please select a file to upload.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( true == is_null( $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', '' ) );
		}

		return $boolIsValid;
	}

	public function valLinkUri() {
		$boolIsValid = true;

		if( true == is_null( $this->getLinkUri() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link_uri', __( 'Url is required.' ) ) );
		} elseif( false == CValidation::checkUrl( $this->m_strLinkUri, false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link_uri', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

	public function valActivateOn() {
		$boolIsValid = true;

		$strDate = date( 'm/d/Y', strtotime( $this->getActivateOn() ) );

		if( false == CValidation::validateDate( $strDate ) && false == is_null( $this->getActivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Activate date is not a valid date.' ) ) );
		}

		if( true == $boolIsValid && true == is_null( $this->getId() ) && false == is_null( $this->getActivateOn() ) && strtotime( $this->getActivateOn() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Activate date cannot be less than current date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeactivateOn() {
		$boolIsValid = true;

		$strDate = date( 'm/d/Y', strtotime( $this->getDeactivateOn() ) );

		if( false == CValidation::validateDate( $strDate ) && false == is_null( $this->getDeactivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivate_on', __( 'Deactivate date is not a valid date.' ) ) );
		}

		if( false == is_null( $this->getDeactivateOn() ) && true == is_null( $this->getActivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Activate date is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( false == is_null( $this->getActivateOn() ) && false == is_null( $this->getDeactivateOn() ) ) {
				if( strtotime( $this->getDeactivateOn() ) <= strtotime( $this->getActivateOn() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivate_on', __( 'Deactivate date should be greater than activate date.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPublishDateRange() {
		$boolIsValid = true;

		$strStartDate = date( 'm/d/Y', strtotime( $this->getActivateOn() ) );
		$strEndDate   = date( 'm/d/Y', strtotime( $this->getDeactivateOn() ) );

		if( false == CValidation::validateDate( $strStartDate ) && false == is_null( $this->getActivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Start date is not a valid date.' ) ) );
		}

		if( true == $boolIsValid && true == is_null( $this->getId() ) && false == is_null( $this->getActivateOn() ) && strtotime( $this->getActivateOn() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Start date cannot be less than current date.' ) ) );
		}

		// check end date
		if( false == CValidation::validateDate( $strEndDate ) && false == is_null( $this->getDeactivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivate_on', __( 'End date is not a valid date.' ) ) );
		}

		if( false == is_null( $this->getDeactivateOn() ) && true == is_null( $this->getActivateOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'activate_on', __( 'Start date is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( false == is_null( $this->getActivateOn() ) && false == is_null( $this->getDeactivateOn() ) ) {
				if( strtotime( $this->getDeactivateOn() ) <= strtotime( $this->getActivateOn() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivate_on', __( 'End date should be greater than start date.' ) ) );
				}
			}
		}

		return $boolIsValid;

	}

	public function valPropertyGroupDocuments() {
		$boolIsValid = true;

		if( ( false == valArr( $this->m_arrobjPropertyGroupDocuments ) ) && false == is_numeric( $this->getDocumentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_documents', __( 'At least one property must be associated to this document.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLateNoticeIsPublished( $objDatabase, $arrintPropertyIds ) {

		$boolIsValid                            = true;
		$arrintSelectedPropertyIds              = [];
		$arrintPropertyIdsWithPublishedDocument = [];

		if( 1 == $this->getIsPublished() ) {

			$objDocumentParameter = NULL;

			if( true == valStr( $this->getId() ) ) {
				$arrobjDocumentParameters = ( array ) \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByCidByDocumentIdByKey( $this->getCid(), $this->getId(), 'text', $objDatabase );
				$objDocumentParameter     = array_shift( $arrobjDocumentParameters );
			}

			if( true == is_null( $objDocumentParameter )
			    || ( true == valObj( $objDocumentParameter, 'CDocumentParameter' )
			         && ( false == valStr( trim( $objDocumentParameter->getValue() ) )
			              || '<br />' == $objDocumentParameter->getValue()
			              || '&nbsp;' == $objDocumentParameter->getValue() ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot publish a document that has no content.' ) ) );

				return $boolIsValid;
			}

			$arrobjDocuments = ( array ) CDocuments::fetchPublishedLateNoticeDocumentsByDocumentTypeIdByDocumentSubTypeIdsByPropertyGroupIdsByCid( $this->getDocumentTypeId(), [ $this->getDocumentSubTypeId() ], $arrintPropertyIds, $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjDocuments ) ) {

				$arrobjDocuments = rekeyArray( 'property_id', $arrobjDocuments );

				foreach( $arrintPropertyIds as $intPropertyId ) {
					if( true == array_key_exists( $intPropertyId, $arrobjDocuments )
					    && true == $arrobjDocuments[$intPropertyId]['is_published']
					    && $this->getId() != $arrobjDocuments[$intPropertyId]['document_id'] ) {
						$arrintPropertyIdsWithPublishedDocument[] = $intPropertyId;
						$boolIsValid                              = false;
					}
				}
			}

			$arrstrPropertiesNames = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyNamesByPropertyIdsByCid( $arrintPropertyIdsWithPublishedDocument, $this->getCid(), $objDatabase );

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'Propert' . ( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrPropertiesNames ) ? 'ies ' : 'y ' ) . implode( ', ', $arrstrPropertiesNames ) . ' already associated with published document.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valOwnerDocuments() {
		$boolIsValid = true;

		if( ( false == valArr( $this->m_arrobjOwnerDocuments ) ) && false == is_numeric( $this->getDocumentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_documents', __( 'At least one owner must be associated to this document.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeletionOfNonPermissionedDocuments( $arrintPropertyIds, $objDatabase ) {
		$boolIsValid = true;

		$arrobjPropertyGroupDocuments = $this->fetchPropertyGroupDocumentsNotInPropertyIds( $arrintPropertyIds, $objDatabase );

		if( true == valArr( $arrobjPropertyGroupDocuments ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_documents', __( 'You cannot delete a document associated to non-permissioned properties.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseDocumentFile() {

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), $this->m_arrstrAllowedFileTypes ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unrecognized file type. Please choose .docx or .pdf files only.' ) ) );

			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%d,0} B', [ PHP_MAX_POST ] ) ) );

			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );

			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );

			return false;
		}
		if( 'application/msword' == $this->getFileType() ) {
			return $this->valDocxDocument();
		} else {
			if( true == $this->getIsChangePdfVersion() ) {
				return true;
			}

			return $this->valPdfDocument();
		}

	}

	public function valSystemDocumentFile() {

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), [ 'application/msword', 'application/pdf' ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unrecognized file type. Please choose .docx or .pdf files only.' ) ) );

			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%d,0} B', [ PHP_MAX_POST ] ) ) );

			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );

			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );

			return false;
		}
		if( 'application/msword' == $this->getFileType() ) {
			return $this->valDocxDocument();
		} else {
			if( true == $this->getIsChangePdfVersion() ) {
				return true;
			}

			return $this->valPdfDocument();
		}

	}

	public function valReportTemplateFile() {

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), [ 'application/msword', 'application/pdf' ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unrecognized file type. Please choose .docx or .pdf files only.' ) ) );

			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%d,0} B', [ PHP_MAX_POST ] ) ) );

			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );

			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid {%s,0} file.', [ $this->getFileType() ] ) ) );

			return false;
		}

		return true;
	}

	public function valPolicyDocument() {

		if( false == in_array( $this->getFileType(), [ 'application/msword', 'application/pdf' ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Only .PDF and .DOCX documents are supported at this time. Please convert your document accordingly and re-upload.' ) ) );

			return false;
		}

		if( isset( $_SERVER['CONTENT_LENGTH'] ) && $_SERVER['CONTENT_LENGTH'] > 6291456 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document must be no larger than 6MB. Please modify your document accordingly and re-upload.' ) ) );

			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%d,0} B', [ PHP_MAX_POST ] ) ) );

			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );

			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid.' ) ) );

			return false;
		}

		if( 'application/msword' == $this->getFileType() ) {
			return $this->valDocxDocument();
		} else {
			return $this->valPdfDocument();
		}
	}

	public function valDocxDocument() {

		$resZip = zip_open( $this->getTempFileName() );

		if( !$resZip || is_numeric( $resZip ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );

			return false;
		}

		$boolValidDocx = false;

		while( $resZipEntry = zip_read( $resZip ) ) {

			// As .emf image file containing .docx file creates converted .pdf heavy in size, we have added validation. Once we get way to make it workable, will remove this validation. [CKS/NNV]
			if( 'emf' == \Psi\CStringService::singleton()->strtolower( pathinfo( ( string ) zip_entry_name( $resZipEntry ), PATHINFO_EXTENSION ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document contains at least one .EMF image. Such images are not supported and create unworkable .PDF documents. Please replace your .EMF with a supported image type (e.g., .JPG, .PNG, .BMP, .GIF) and re-upload your document' ) ) );

				return false;
			}

			if( false == zip_entry_open( $resZip, $resZipEntry ) ) {
				continue;
			}

			if( 'word/document.xml' == zip_entry_name( $resZipEntry ) ) {
				$boolValidDocx = true;
				continue;
			}
			zip_entry_close( $resZipEntry );
		}

		if( true == $boolValidDocx ) {
			return true;
		}

		$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );

		return false;
	}

	public function valPdfDocument() {

		if( true == $this->getExcludePdfVersionValidation() ) {

			$strUploadPath = CObjectStorageUtils::getTemporaryPath( PATH_MOUNTS_FILES, $this->getCid() );

			if( false == CFileIo::isDirectory( $strUploadPath ) ) {
				if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );

					return false;
				}
			}

			if( false == CFileIo::copyFile( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );

				return false;
			}

			$objPdfProcessor = CPdfProcessor::getInstance( CPdfProcessor::LIBRARY_TYPE_TCPDF );
			try {
				$objPdfProcessor->setSourceFile( $strUploadPath . $this->getFileName() );
			} catch( CPdfProcessorException $objPdfProcessorException ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This document cannot be signed electronically. Possible reasons include: it may not contain any valid text, it may contain cross-reference streams, the document may be protected or encrypted. Please recreate the document and try again.' ) ) );

				return false;
			} finally {
				CFileIo::deleteFile( $strUploadPath . $this->getFileName() );
			}

			return true;
		}

		$resFile = CFileIo::fileOpen( $this->getTempFileName(), 'rb' );

		if( true != $resFile ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid pdf file.' ) ) );

			return false;
		}

		fseek( $resFile, 0 );
		\Psi\CStringService::singleton()->preg_match( '/\d\.\d/', CFileIo::fileRead( $resFile, 20 ), $arrmixMatch );

		CFileIo::fileClose( $resFile );

		$intPdfVersion = 0;

		if( true == isset( $arrmixMatch[0] ) ) {
			$intPdfVersion = $arrmixMatch[0];
		}

		if( 1.4 < ( float ) $intPdfVersion ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'This document is not a valid version PDF file.' ) ) );

			return false;
		}

		try {
			$objZendPdf = Zend_Pdf::load( $this->getTempFileName() );
		} catch( Zend_Pdf_Exception $objZendPdfException ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This document cannot be signed electronically. Possible reasons include: it may not contain any valid text, it may contain cross-reference streams, the document may be protected or encrypted. Please recreate the document and try again.' ) ) );

			return false;
		}

		return true;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		if( true == is_null( $this->getArTriggerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_trigger_id', __( 'Trigger is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileTypeId() ) ) {
			$boolIsValid = false;
			if( true == $this->getIsSystem() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', __( 'Template type is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', __( 'File type is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCompanyApplicationIds() {
		$boolIsValid = true;

		$arrintCompanyApplicationIds = $this->getCompanyApplicationIds();

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintCompanyApplicationIds ) ) {
			if( -1 == $arrintCompanyApplicationIds[0] && CDocumentSubType::APPLICATION == $this->getDocumentSubTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_application_ids', __( 'Associated Application is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPacketType() {
		$boolIsValid = true;

		if( true == is_null( $this->getDocumentSubTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_sub_type_id', __( 'Packet type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseIntervalTypeIds() {
		$boolIsValid = true;

		if( CDocumentSubType::LEASE == $this->getDocumentSubTypeId() && false == valArr( $this->getLeaseIntervalTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_sub_type_id', __( 'Lease Type(s) are required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrintPropertyIds = NULL, $boolIsFromTemplateUsageUpdate = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case 'prospect_portal_insert':
			case 'prospect_portal_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valSeoUri( $objDatabase );
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				$boolIsValid &= $this->valDocumentRedirectUrl();
				break;

			case VALIDATE_INSERT:
			case 'corporate_portal_insert':
			case 'corporate_portal_update':
			case 'merge_document_insert':
			case 'merge_document_update':

				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valSeoUri( $objDatabase );

				if( true == $this->getIsPublished() ) {
					$boolIsValid &= $this->valActivateOn();
					$boolIsValid &= $this->valDeactivateOn();
				}

				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'resident_portal_insert':
			case 'resident_portal_update':
			case 'employee_portal_insert':
			case 'employee_portal_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();

				if( true == $this->getIsPublished() ) {
					$boolIsValid &= $this->valPublishDateRange();
				}

				$boolIsValid &= $this->valSeoUri( $objDatabase );
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'owner_portal_insert':
			case 'owner_portal_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();

				if( true == $this->getIsPublished() ) {
					$boolIsValid &= $this->valPublishDateRange();
				}

				$boolIsValid &= $this->valSeoUri( $objDatabase );
				$boolIsValid &= $this->valPropertyGroupDocuments();
				$boolIsValid &= $this->valDocumentRedirectUrl();
				break;

			case 'employee_portal_doument_insert':
			case 'employee_portal_doument_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'company_document_insert':
			case 'company_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, false, CDocumentInclusionType::COMPANY );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();

				if( true == $this->getIsPublished() ) {
					$boolIsValid &= $this->valPublishDateRange();
				}
				$boolIsValid &= $this->valSeoUri( $objDatabase, false, CDocumentInclusionType::COMPANY );
				$boolIsValid &= $this->valDocumentRedirectUrl();
				break;

			case 'owner_document_insert':
			case 'owner_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, false, CDocumentInclusionType::OWNERS );
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();

				if( true == $this->getIsPublished() ) {
					$boolIsValid &= $this->valPublishDateRange();
				}
				$boolIsValid &= $this->valSeoUri( $objDatabase, false, CDocumentInclusionType::OWNERS );
				$boolIsValid &= $this->valOwnerDocuments();
				$boolIsValid &= $this->valDocumentRedirectUrl();
				break;

			case 'apply_document_template':
				$boolIsValid &= $this->valDocumentTemplateId();
				break;

			case 'online_application_insert':
			case 'online_application_update':
			case 'lease_agreement_insert':
			case 'lease_agreement_update':
			case 'merge_document_insert':
			case 'merge_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'auto_responder_insert':
			case 'auto_responder_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valDocumentSubTypeId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'website_document_insert':
			case 'website_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, $boolIsWebsiteDocument = true );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valSeoUri( $objDatabase, $boolIsWebsiteDocument = true );
				$boolIsValid &= $this->valDocumentRedirectUrl();
				break;

			case 'resident_portal_delete':
				$boolIsValid &= $this->valDeletionOfNonPermissionedDocuments( $arrintPropertyIds, $objDatabase );
				break;

			case 'copy_application_document':
			case 'insert_architect_document':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valTitle();
				break;

			case 'main_lease_document_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'lease_document_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				$boolIsValid &= $this->valFileName();
				// $boolIsValid &= $this->valFileError();
				$boolIsValid &= $this->valLeaseDocumentFile();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				break;

			case 'main_lease_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				$boolIsValid &= $this->valPropertyGroupDocuments();
				break;

			case 'report_template_update':
			case 'lease_document_details_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				// $boolIsValid &= $this->valArTriggerId( $objDatabase );
				// $boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				$boolIsValid &= $this->valCompanyApplicationIds();
				$boolIsValid &= $this->valPacketType();
				$boolIsValid &= $this->valLeaseIntervalTypeIds();
				break;

			case 'lease_document_tamplate_details_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				// $boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				if( true == $boolIsFromTemplateUsageUpdate ) {
					$boolIsValid &= $this->valFileTypeId();
				}
				break;

			case 'upload_document':
				$boolIsValid &= $this->valFileName();
				// $boolIsValid &= $this->valFileError();
				$boolIsValid &= $this->valLeaseDocumentFile();
				break;

			case 'insert_update_policy_document':
				$boolIsValid &= $this->valName();
				break;

			case 'insert_report_template_document':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				break;

			case 'insert_lease_document':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				// $boolIsValid &= $this->valArTriggerId( $objDatabase );
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				$boolIsValid &= $this->valCompanyApplicationIds();
				$boolIsValid &= $this->valPacketType();
				$boolIsValid &= $this->valLeaseIntervalTypeIds();
				break;

			case 'insert_lease_document_template':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valLeaseDocumentFile();
				$boolIsValid &= $this->valFileName();
				if( 1 == $this->getIsAdhoc() ) {
					$boolIsValid &= $this->valFileTypeId();
				}
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				break;

			case 'update_lease_document':
				break;

			case 'delete_report_template_document':
			case 'delete_lease_document':
				break;

			case 'delete_main_lease_document':
				break;

			case 'insert_master_guarantor_lease_document':
			case 'update_master_guarantor_lease_document':
				$boolIsValid &= $this->valName();
				break;

			case 'late_notice_insert':
			case 'late_notice_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDocumentSubTypeId();
				$boolIsValid &= $this->valLateNoticeIsPublished( $objDatabase, $arrintPropertyIds );
				break;

			case VALIDATE_DELETE:
				break;

			case 'upload_policy_document':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valPolicyDocument();
				break;

			case 'report_template_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valLeaseDocumentName( $objDatabase );
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valReportTemplateFile();
				$boolIsValid &= $this->valActivateOn();
				$boolIsValid &= $this->valDeactivateOn();
				break;

			case 'upload_report_template_document':
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valReportTemplateFile();
				break;

			case 'insert_system_document_template':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valSystemDocumentName( $objDatabase );
				$boolIsValid &= $this->valSystemDocumentFile();
				$boolIsValid &= $this->valFileTypeId();
				break;

			case 'update_system_document_template':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentTypeId();
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valSystemDocumentName( $objDatabase );
				$boolIsValid &= $this->valFileTypeId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function loadDocumentLibrariesAndComponents( $objDatabase ) {

		if( false == is_null( $this->getDocumentSubTypeId() ) && ( 0 < $this->getDocumentSubTypeId() ) && ( CDocumentSubType::ONLINE_RENTAL_APPLICATION_FORM != $this->getDocumentSubTypeId() ) ) {
			$this->m_arrobjDocumentLibraries = \Psi\Eos\Entrata\CDocumentLibraries::createService()->fetchDocumentLibrariesByDocumentTypeIdByDocumentSubTypeId( $this->getDocumentTypeId(), $this->getDocumentSubTypeId(), $objDatabase );
		} else {
			$this->m_arrobjDocumentLibraries = \Psi\Eos\Entrata\CDocumentLibraries::createService()->fetchDocumentLibrariesByDocumentTypeId( $this->getDocumentTypeId(), $objDatabase );
		}

		$this->m_arrobjDocumentPlugins = \Psi\Eos\Entrata\CDocumentPlugins::createService()->fetchDocumentPluginsByHandle( $objDatabase );

		if( CDocumentSubType::SOCIAL_EVENT == $this->getDocumentSubTypeId() ) {
			krsort( $this->m_arrobjDocumentPlugins );
		}

		CObjectModifiers::createService()->nestObjects( $this->m_arrobjDocumentPlugins, $this->m_arrobjDocumentLibraries );
		  return $this->m_arrobjDocumentLibraries;
	}

	public function nestDocumentComponents() {

		if( true == valArr( $this->m_arrobjDocumentComponents ) ) {
			foreach( $this->m_arrobjDocumentComponents as $objDocumentComponent ) {
				if( false == is_null( $objDocumentComponent->getDocumentComponentId() ) && true == valObj( $this->m_arrobjDocumentComponents[$objDocumentComponent->getDocumentComponentId()], 'CDocumentComponent' ) ) {
					$this->m_arrobjDocumentComponents[$objDocumentComponent->getDocumentComponentId()]->addDocumentComponent( $objDocumentComponent );
				}
			}
		}
	}

	public function nestFormAnswers() {
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjFormAnswers, $this->m_arrobjDocumentComponents );
	}

	public function getUsage() {
		if( true == $this->getIsPacket() && true == $this->getIsAdhoc() ) {
			return __( 'Ad hoc and Packets' );
		}

		if( true == $this->getIsPacket() ) {
			return __( 'Packets Only' );
		}

		if( true == $this->getIsAdhoc() ) {
			return __( 'Ad hoc Only' );
		}
	}

	// This function applies a document template to an existing document.

	public function applyDocumentTemplate( $intDocumentTemplateId, $intCompanyUserId, $objDatabase, $objAdminDatabase ) {

		$objDocumentTemplate = CDocumentTemplates::fetchDocumentTemplateById( $intDocumentTemplateId, $objAdminDatabase );

		if( true == valObj( $objDocumentTemplate, 'CDocumentTemplate' ) ) {

			$arrobjDocumentTemplateComponents = $objDocumentTemplate->fetchDocumentTemplateComponents( $objAdminDatabase );
			$arrobjDocumentTemplateParameters = $objDocumentTemplate->fetchDocumentTemplateParameters( $objAdminDatabase );

			$objDocumentTemplate->nestDocumentTemplateComponentsAndDocumentTemplateParameters();
			$arrobjDocumentTemplateComponents = $objDocumentTemplate->getDocumentTemplateComponents();

			// Loop on document template components and params and insert onto this document.

			if( true == valArr( $arrobjDocumentTemplateComponents ) ) {
				foreach( $arrobjDocumentTemplateComponents as $objParentDocumentTemplateComponent ) {

					$objParentDocumentComponent = $objParentDocumentTemplateComponent->createDocumentComponent( $this );

					if( false == $objParentDocumentComponent->validate( VALIDATE_INSERT ) || false == $objParentDocumentComponent->insert( $intCompanyUserId, $objDatabase ) ) {
						return false;
					}

					// Loop on sub components
					if( true == valArr( $objParentDocumentTemplateComponent->getDocumentTemplateComponents() ) ) {
						foreach( $objParentDocumentTemplateComponent->getDocumentTemplateComponents() as $objChildDocumentTemplateComponent ) {

							$objChildDocumentComponent = $objChildDocumentTemplateComponent->createDocumentComponent( $this );
							$objChildDocumentComponent->setDocumentComponentId( $objParentDocumentComponent->getId() );

							if( false == $objChildDocumentComponent->validate( VALIDATE_INSERT ) || false == $objChildDocumentComponent->insert( $intCompanyUserId, $objDatabase ) ) {
								return false;
							}

							// Loop on sub component's parameters
							if( true == valArr( $objChildDocumentTemplateComponent->getDocumentTemplateParameters() ) ) {
								foreach( $objChildDocumentTemplateComponent->getDocumentTemplateParameters() as $objDocumentTemplateParameter ) {
									$objDocumentParameter = $objDocumentTemplateParameter->createDocumentParameter( $objChildDocumentComponent );

									if( false == $objDocumentParameter->validate( VALIDATE_INSERT ) || false == $objDocumentParameter->insert( $intCompanyUserId, $objDatabase ) ) {
										return false;
									}
								}
							}
						}
					}

					// Loop on parent parameters
					if( true == valArr( $objParentDocumentTemplateComponent->getDocumentTemplateParameters() ) ) {
						foreach( $objParentDocumentTemplateComponent->getDocumentTemplateParameters() as $objDocumentTemplateParameter ) {
							$objDocumentParameter = $objDocumentTemplateParameter->createDocumentParameter( $objParentDocumentComponent );

							if( false == $objDocumentParameter->validate( VALIDATE_INSERT ) || false == $objDocumentParameter->insert( $intCompanyUserId, $objDatabase ) ) {
								return false;
							}
						}
					}

				}
			}

			// SET DOCUMENT TEMPLATE ID  FOR THE DOCUMENT
			$intOldDocumentTemplateId = $this->getDocumentTemplateId();

			$this->setDocumentTemplateId( $objDocumentTemplate->getId() );

			if( false == $this->validate( 'apply_document_template', $objDatabase, NULL, $objAdminDatabase ) || false == $this->update( $intCompanyUserId, $objDatabase, $objAdminDatabase ) ) {
				return false;
			}

			// INSERT THE DOCUMENT TEMPLATE ORDERS
			if( $intOldDocumentTemplateId != $objDocumentTemplate->getId() ) {

				$objCompanyPricing = CCompanyPricings::fetchCompanyPricingByCidByCompanyPricingTypeId( $this->getCid(), CCompanyPricingType::RESIDENT_LEASE, $objAdminDatabase );

				if( true == valObj( $objCompanyPricing, 'CCompanyPricing' ) ) {
					if( true == is_null( $objCompanyPricing->getAccountId() ) ) {
						// fetch the default account
						$objManamgementCompany = $this->fetchClient( $objDatabase );

						if( false == valObj( $objManamgementCompany, 'CClient' ) ) {
							return false;
						}

						$objPrimaryAccount = $objManamgementCompany->fetchPrimaryAccount( $objAdminDatabase );

						if( false == valObj( $objPrimaryAccount, 'CAccount' ) ) {
							return false;
						}

						$objCompanyPricing->setAccountId( $objPrimaryAccount->getId() );
					}

					$arrobjPropertyDocuments = $this->fetchPropertyDocuments( $objDatabase );

					if( true == valArr( $arrobjPropertyDocuments ) ) {

						$objDocumentTemplateOrder           = $this->createDocumentTemplateOrder();
						$arrobjDocumentTemplateOrdersInsert = NULL;

						foreach( $arrobjPropertyDocuments as $objPropertyDocument ) {
							$objDocumentTemplateOrderInsert = clone $objDocumentTemplateOrder;
							$objDocumentTemplateOrderInsert->setPropertyId( $objPropertyDocument->getPropertyId() );
							$objDocumentTemplateOrderInsert->setAccountId( $objCompanyPricing->getAccountId() );
							$objDocumentTemplateOrderInsert->setCompanyPricingId( $objCompanyPricing->getId() );
							$objDocumentTemplateOrderInsert->setFeeAmount( $objCompanyPricing->getAmount() );

							$arrobjDocumentTemplateOrdersInsert[] = $objDocumentTemplateOrderInsert;
						}

						if( true == valArr( $arrobjDocumentTemplateOrdersInsert ) ) {
							foreach( $arrobjDocumentTemplateOrdersInsert as $objDocumentTemplateOrderInsert ) {
								if( false == $objDocumentTemplateOrderInsert->insert( SYSTEM_USER_ID, $objAdminDatabase ) ) {
									return false;
								}
							}
						}
					}
				}
			}
		}

		return true;
	}

	public function loadGridEngineData( $objDatabase, $boolIsCustomPage = false ) {
		$this->fetchDocumentComponents( $objDatabase );

		if( true == valArr( $this->m_arrobjDocumentComponents ) ) {
			$this->m_arrobjDocumentParameters = $this->fetchDocumentParametersByDocumentComponentIds( array_keys( $this->m_arrobjDocumentComponents ), $boolIsCustomPage, $objDatabase );
		} else {
			$this->m_strContent = NULL;

			return;
		}

		$this->nestDocumentComponents();
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjDocumentParameters, $this->m_arrobjDocumentComponents );
		$this->nestFormAnswers();
	}

	public function generateSeoUrl( $objDatabase ) {

		if( true == is_null( $this->getDocumentId() ) ) {
			return $this->getDocumentSeoName();
		}

		$arrstrDocumentSeoUrls              = [];
		$intCount                           = 0;
		$strDoccumentSeoUrl                 = '';
		$arrstrDocumentSeoUrls[$intCount++] = $this->getDocumentSeoName();

		$intDocumentId = $this->getDocumentId();

		while( 1 ) {
			$intCount++;

			$objParentDocument = CDocuments::fetchDocumentByIdByCid( $intDocumentId, $this->getCid(), $objDatabase );

			if( false == valObj( $objParentDocument, 'CDocument' ) ) {
				break;
			}

			$intDocumentId = $objParentDocument->getDocumentId();

			$arrstrDocumentSeoUrls[$intCount++] = $objParentDocument->getDocumentSeoName();

			if( true == is_null( $objParentDocument->getDocumentId() ) ) {
				break;
			}
		}

		if( true == valArr( $arrstrDocumentSeoUrls ) ) {
			arsort( $arrstrDocumentSeoUrls );
			$strDoccumentSeoUrl = implode( '/', $arrstrDocumentSeoUrls );
		}

		return $strDoccumentSeoUrl;
	}

	public function loadGridEngineDisplayContent( $objDatabase, $boolIsPopup = 0, $boolIsResponsive = 0, $boolIsDisplayAnchor = 0, $boolIsCustomPage = false, $boolIsRP40 = false ) {

		$boolShowSecureData = false;

		$this->loadGridEngineData( $objDatabase, $boolIsCustomPage );

		if( true == valArr( $this->m_arrobjDocumentComponents ) ) {

			$arrobjStates = \Psi\Eos\Entrata\CStates::createService()->fetchAllStates( $objDatabase );

			$objGridEngineControl = new CGridEngineControl();
			$objGridEngineControl->setDocumentComponents( $this->m_arrobjDocumentComponents );
			$objGridEngineControl->setIsPrint( $this->m_boolIsPrint );
			$objGridEngineControl->setShowSecureData( $this->m_boolShowSecureData );
			$objGridEngineControl->setStates( $arrobjStates );
			$objGridEngineControl->setDocumentSubTypeId( $this->m_intDocumentSubTypeId );
			$objGridEngineControl->setIsResponsive( $boolIsResponsive );

			$this->m_strContent = $objGridEngineControl->generateDisplayHtml( $boolIsPopup, $boolIsDisplayAnchor, $boolIsRP40 );
		}
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function deleteDocumentParameters( $objDatabase ) {
		$strSql = 'DELETE FROM document_parameters WHERE document_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid();
		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function deleteDocumentComponents( $objDatabase ) {

		$strSql = 'DELETE FROM document_components WHERE document_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND document_component_id IS NOT NULL';
		fetchData( $strSql, $objDatabase );

		$strSql = 'DELETE FROM document_components WHERE document_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND document_component_id IS NULL';
		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function buildFileData( $objDatabase, $strDocumentFileName = 'lease_document_file', $strCategoryName = NULL ) {

		if( true == is_null( $_FILES[$strDocumentFileName]['name'] ) || 0 == strlen( trim( $_FILES[$strDocumentFileName]['name'] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );

			return false;
		}

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		$arrstrFileInfo = pathinfo( $_FILES[$strDocumentFileName]['name'] );
		$strExtension   = ( true == isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
		$strFileName    = date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $this->getId() . '.' . $strExtension;

		// Getting the correct mime-type
		$strMimeType      = '';
		$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$arrstrFileData = [ 'tmp_name' => $_FILES[$strDocumentFileName]['tmp_name'], 'error' => $_FILES[$strDocumentFileName]['error'], 'title' => $_FILES[$strDocumentFileName]['name'], 'name' => $strFileName, 'type' => $strMimeType ];

		$this->setFileData( $arrstrFileData, $strDocumentFileName, $strCategoryName );

		return true;
	}

	public function buildLeaseTemplateFileData( $objDatabase, $strTempFileName = NULL, $strDocumentFileName = 'lease_document_file', $strCategoryName = NULL ) {

		if( true == is_null( $strTempFileName ) || 0 == strlen( trim( $strTempFileName ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );

			return false;
		}

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		$arrstrFileInfo = pathinfo( $strTempFileName );
		$strExtension   = ( true == isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
		$strFileName    = date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $this->getId() . '.' . $strExtension;

		// Getting the correct mime-type
		$strMimeType      = '';
		$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$arrstrFileData = [ 'tmp_name' => PATH_NON_BACKUP_MOUNTS_GLOBAL_TMP . $strTempFileName, 'error' => '', 'title' => $strTempFileName, 'name' => $strFileName, 'type' => $strMimeType ];

		$this->setFileData( $arrstrFileData, $strDocumentFileName, $strCategoryName );

		return true;
	}

	public function buildDocumentFilePath( $strDocumentFileName = NULL, $strCategoryName = NULL ) {

		if( false == is_null( $strDocumentFileName ) && 'policy_document_file' == $strDocumentFileName ) {
			return 'policy_documents/rental_applications/' . $this->getId() . '/';

		} elseif( false == is_null( $strDocumentFileName ) && 'report_template_document_file' == $strDocumentFileName && false == is_null( $strCategoryName ) ) {
			return 'report_templates/' . $strCategoryName . '/';
		} elseif( false == is_null( $strDocumentFileName ) && 'system_template_file' == $strDocumentFileName ) {
			return 'system_templates/' . $this->getFileTypeId() . '/' . $this->getId() . '/';
		} elseif( 'employee_portal_documents' == $strCategoryName ) {
			return $this->getCid() . '/company/employee_portal_documents/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/';
		} else {
			if( true == valStr( $this->getSystemCode() ) && ( CFileType::SYSTEM_CODE_LEASE_DOCUMENT == $this->getSystemCode() ) ) {
				$strPath = $this->getCid() . '/company/lease-document-templates/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/';
			} elseif( true == valStr( $this->getSystemCode() ) && ( CFileType::SYSTEM_CODE_POLICY == $this->getSystemCode() ) && ( CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT == $this->getDocumentSubTypeId() ) ) {
                $strPath = $this->getCid() . '/rv_rental_application_document/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getCompanyApplicationId() . '/';
            } else {
				$strPath = 'leases/' . $this->getId() . '/';
			}

			return $strPath;
		}
	}

	public function uploadFile( $strDocumentFileName = NULL, $strCategoryName = NULL ) {

		$boolIsValid = true;

		$this->setFilePath( $this->buildDocumentFilePath( $strDocumentFileName, $strCategoryName ) );
		$strUploadPath = $this->getFullFilePath();

		if( false == is_dir( $strUploadPath ) && false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
			$boolIsValid = false;
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function encryptUploadedObject( $objObjectStorageGateway, $strDocumentFileName = NULL, $strCategoryName = NULL, $boolNoEncrypt = false, $strContainer = PATH_MOUNTS_DOCUMENTS ) {

		$strUploadPath = CObjectStorageUtils::getTemporaryPath( PATH_MOUNTS_DOCUMENTS, $this->getCid() );

		if( false == is_dir( $strUploadPath ) && false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );

			return false;
		}

		if( false == CFileIo::fileExists( $this->getTempFileName() ) || false == CFileIo::moveFile( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File Not Found.' ) ) );

			return false;
		}

		$strUploadFilePath = $strUploadPath . $this->getFileName();

		if( true == $this->getIsChangePdfVersion() && 1.4 < $this->getPdfVersion( $strUploadPath ) ) {

			$strTempConvertedPdfFilePath = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_DOCUMENTS, $this->getCid() ) . '/' . $objObjectStorageGateway->calcTemporaryFileName( 'pdf' );

			$strCommand = 'gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' . $strTempConvertedPdfFilePath . ' ' . $strUploadPath . $this->getFileName();

			if( 'development' == CONFIG_ENVIRONMENT ) {
				$strCommand = CConfig::get( 'gs_script_exe' ) . ' -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' . $strTempConvertedPdfFilePath . ' ' . $strUploadPath . $this->getFileName();
			}

			shell_exec( $strCommand );

			if( true == CFileIo::fileExists( $strTempConvertedPdfFilePath ) ) {
				$strUploadFilePath = $strTempConvertedPdfFilePath;
			} else {
				return false;
			}
		}

		$strDocumentContent = CFileIo::fileGetContents( $strUploadFilePath );
		CFileIo::deleteFile( $strUploadFilePath );

		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => $strDocumentContent ] );

		if( true == valStr( $strContainer ) && PATH_MOUNTS_DOCUMENTS != $strContainer ) {
			$arrmixGatewayRequest['container'] = $strContainer;
		}

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt document' ) ) );

			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );

		return true;
	}

	public function getPdfVersion( $strUploadPath ) {

		$strUploadFilePath = $strUploadPath . $this->getFileName();

		$resFile = CFileIo::fileOpen( $strUploadFilePath, 'rb' );

		fseek( $resFile, 0 );
		\Psi\CStringService::singleton()->preg_match( '/\d\.\d/', CFileIo::fileRead( $resFile, 20 ), $arrmixMatch );
		CFileIo::fileClose( $resFile );

		$intPdfVersion = 0;
		if( true == isset( $arrmixMatch[0] ) ) {
			$intPdfVersion = $arrmixMatch[0];
		}

		return ( float ) $intPdfVersion;
	}

	public function getFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath();
	}

	public function downLoadFile() {

		$strFullpath = $this->getFullFilePath() . $this->getFileName();

		if( false == CFileIo::fileExists( $strFullpath ) ) {
			trigger_error( __( 'File is not present or has been deleted.' ), E_USER_ERROR );
			exit;
		}

		if( 'application/pdf' == $this->getFileType() ) {
			header( 'Pragma:public' );
			header( 'Expires:0' );
			header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control:public' );
			header( 'Content-Description:File Transfer' );
			header( 'Content-Type: application/pdf' );
			header( 'Content-Disposition: attachment; filename="' . $this->getName() . date( '_M_d_Y_g_i_a', time() ) . '.pdf"' );
			echo CFileIo::fileGetContents( $strFullpath );
			exit();
		} else {
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length: ' . CFileIo::getFileSize( $strFullpath ) );
			header( 'Content-Disposition: attachment; filename= ' . $this->getFileName() );
			header( 'Content-Transfer-Encoding:binary' );
			echo CFileIo::fileGetContents( $strFullpath );
			exit();
		}
	}

	public function copyObject( $intCompanyUserId, $objObjectStorageGateway, $strFolderPath = PATH_MOUNTS_DOCUMENTS, $boolDeleteAfterCopy = true ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to get IObjectStorage.' ) ) );
			return false;
		}

		if( false == valObj( $this->m_objOldDocument, 'CDocument' ) ) {
			return false;
		}

		$arrmixGatewayRequest  = $this->m_objOldDocument->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );

		$arrobjResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		if( true == $arrobjResponse->isSuccessful() ) {
			$arrmixPutGatewayRequest = $this->createPutGatewayRequest( [ 'data' => file_get_contents( $arrobjResponse['outputFile'] ) ] );
			$objResponse = $objObjectStorageGateway->putObject( $arrmixPutGatewayRequest );
		}

		if( true == $objResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objResponse );

		if( true == $boolDeleteAfterCopy ) {
			$arrmixGatewayRequest  = $this->m_objOldDocument->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
			$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );

			if( true == $objDeleteObjectResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not deleted.' ) ) );
				return false;
			} else {
				$this->deleteStoredObject( $intCompanyUserId, $this->m_objDatabase );
			}
		}

		return true;
	}

	public function moveFile( $intCompanyUserId, $objObjectStorageGateway ) {

		if( false == $this->copyObject( $intCompanyUserId, $objObjectStorageGateway, $strFolderPath = PATH_MOUNTS_DOCUMENTS, $boolDeleteAfterCopy = true ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to delete old file.' ) ) );

			return false;
		}

		return true;
	}

	public function makeCopyAndArchive( $objDatabase, $boolCopyFileData = true ) {
		$objNewDocument = clone $this;

		$objNewDocument->setId( $objNewDocument->fetchNextId( $objDatabase ) );

		if( true == $boolCopyFileData ) {
			$arrstrFileNameExtension = explode( '.', $this->getFileName() );

			$strFileName = NULL;
			if( true == valArr( $arrstrFileNameExtension ) ) {
				$strFileName = date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $objNewDocument->getId() . '.' . $arrstrFileNameExtension[1];
			}

			$objFileType   = CFileTypes::fetchFileTypeByIdByCid( $this->getFileTypeId(), $this->getCid(), $this->m_objDatabase );
			$strSystemCode = ( true == valObj( $objFileType, 'CFileType' ) ) ? $objFileType->getSystemCode() : NULL;
			$objNewDocument->setSystemCode( $strSystemCode );

			$objNewDocument->setFileName( $strFileName );
			$objNewDocument->setFilePath( $objNewDocument->buildDocumentFilePath() );
		}

		$objNewDocument->setOldDocument( $this );

		$this->setArchivedOn( 'now()' );

		return $objNewDocument;
	}

	public function createDocumentMergeField() {

		$objDocumentMergeField = new CDocumentMergeField();
		$objDocumentMergeField->setDocumentId( $this->m_intId );
		$objDocumentMergeField->setCid( $this->m_intCid );

		$objDocumentMergeField->setAllowUserUpdate( 1 );
		$objDocumentMergeField->setAllowAdminUpdate( 1 );
		$objDocumentMergeField->setForceOriginUpdate( 1 );
		$objDocumentMergeField->setShowInMergeDisplay( 1 );
		$objDocumentMergeField->setIsRequired( 0 );

		return $objDocumentMergeField;
	}

	public function checkIsHavingRequiredDocuments( $objDatabase ) {

		if( CDocumentType::PACKET == $this->getDocumentTypeId() ) {
			return true;
		}

		$arrobjExternalLeaseDocuments = $this->fetchLeaseDocuments( $objDatabase, $boolIsOptional = NULL, $boolIsPublished = true, $boolHasExternalKey = true );

		if( false == valArr( $arrobjExternalLeaseDocuments ) ) {
			return false;
		}

		foreach( $arrobjExternalLeaseDocuments as $objDocument ) {
			if( false == $objDocument->getIsOptional() ) {
				return true;
			} elseif( true == $objDocument->getIsDefaultSelected() ) {
				return true;
			}
		}

		return false;
	}

	public function checkPdfMergeDocument( $intDocumentAddendaId, $boolIsSignatureMergeField, $objDatabase ) {

		if( true == is_null( $intDocumentAddendaId ) ) {
			return false;
		}

		$strCondition = '';

		if( 'pdf' != \Psi\CStringService::singleton()->strtolower( $this->getFileNameExtension() ) ) {
			return false;
		}

		if( true == $boolIsSignatureMergeField ) {
			$strCondition = ' AND merge_field_group_id = ' . CMergeFieldGroup::SIGNATURE_AND_INITIALS;
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND document_id = ' . ( int ) $this->getId() . ' AND document_addenda_id = ' . ( int ) $intDocumentAddendaId . $strCondition;

		$intDocumentMergeFieldCount = CDocumentMergeFields::fetchDocumentMergeFieldCount( $strWhere, $objDatabase );

		if( 0 == $intDocumentMergeFieldCount ) {
			return false;
		}

		return true;
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true, true ) );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolSuccess = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolSuccess && false == $boolReturnSqlOnly ) {
			$boolSuccess &= $this->insertStoredObject( $intCurrentUserId, $objDatabase );
		}
		return $boolSuccess;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		return $this->getFilePath() . $this->getFileName();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return ( false !== strpos( $this->getFilePath(), 'employee_portal_documents' ) ) ? PATH_MOUNTS_FILES : PATH_MOUNTS_DOCUMENTS;

			default:
				if( true == in_array( $this->getSystemCode(), [
						CFileType::SYSTEM_CODE_LEASE_PACKET,
						CFileType::SYSTEM_CODE_SIGNED,
						CFileType::SYSTEM_CODE_PARTIALLY_SIGNED,
						CFileType::SYSTEM_CODE_LEASE_ADDENDUM,
						CFileType::SYSTEM_CODE_LEASE_DOCUMENT,
						CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE,
						CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET,
						CFileType::SYSTEM_CODE_VIOLATION_MANAGER,
						CFileType::SYSTEM_CODE_MOVE_IN_STATEMENT,
						CFileType::SYSTEM_CODE_TENANT_STATEMENT,
						CFileType::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT,
                        CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID,
                        CFileType::SYSTEM_CODE_LEASE_ESA,
                        CFileType::SYSTEM_CODE_MILITARY_DD_1746
					] )
					|| ( false !== strpos( $this->getFilePath(), 'employee_portal_documents' )
					|| ( $this->getSystemCode() == CFileType::SYSTEM_CODE_POLICY && $this->getDocumentSubTypeId() == CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT ) ) ) {
					return CONFIG_OSG_BUCKET_DOCUMENTS;
				} elseif( ( $this->getSystemCode() == CFileType::SYSTEM_CODE_POLICY && $this->getDocumentSubTypeId() == CDocumentSubType::DOCUMENT_TEMPLATE )
							|| ( $this->getDocumentSubTypeId() == CDocumentSubType::APPLICATION_POLICY_DOCUMENT ) ) {
					return CONFIG_OSG_BUCKET_LEASING;
				} else {
					return PATH_MOUNTS_DOCUMENTS;
				}
		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function getDocumentFromCloud( $objObjectStorageGateway ) {
		$strCloudPath = str_replace( PATH_MOUNTS_LEASES, PATH_MOUNTS_DOCUMENTS . PATH_MOUNTS_TEMPLATES, $this->getFilePath() );
		$strFilePath = $this->getFilePath();
		$this->setFilePath( $strCloudPath );
		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		$this->setFilePath( $strFilePath );

		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
		return $arrobjObjectStorageResponse;
	}

	public function downloadObject( $objObjectStorageGateway ) {

		$arrmixGatewayRequest = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile'  => 'temp' ] );
		$arrmixStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		if( true == $arrmixStorageGatewayResponse->hasErrors() ) {
			$arrmixStorageGatewayResponse = $this->getDocumentFromCloud( $objObjectStorageGateway );
			if( true == $arrmixStorageGatewayResponse->hasErrors() ) {
				trigger_error( __( 'File is not present or has been deleted.' ), E_USER_ERROR );
				exit;
			}
		}

		$strFullPath = $arrmixStorageGatewayResponse['outputFile'];

		if( 'application/pdf' == $this->getFileType() ) {
			header( 'Pragma:public' );
			header( 'Expires:0' );
			header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control:public' );
			header( 'Content-Description:File Transfer' );
			header( 'Content-Type: application/pdf' );
			header( 'Content-Disposition: attachment; filename="' . $this->getName() . date( '_M_d_Y_g_i_a', time() ) . '.pdf"' );
			echo CFileIo::fileGetContents( $strFullPath );

		} else {
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length: ' . CFileIo::getFileSize( $strFullPath ) );
			header( 'Content-Disposition: attachment; filename= ' . $this->getFileName() );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Transfer-Encoding:binary' );
			echo CFileIo::fileGetContents( $strFullPath );
		}

		CFileIo::deleteFile( $strFullPath );
		exit();
	}

	public function buildDocumentName() {

		$strFileName = '';
		if( ( false == is_null( $this->getDocumentSubTypeId() ) && CDocumentSubType::APPLICATION_POLICY_DOCUMENT == $this->getDocumentSubTypeId() ) || ( false == is_null( $this->getFileTypeSystemCode() ) && CFileType::SYSTEM_CODE_POLICY == $this->getFileTypeSystemCode() ) ) {
			$arrstrFileInfo = pathinfo( $this->getFileName() );
			$strExtension   = ( true == isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
			$strFileName    = date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $this->getId() . '.' . $strExtension;
		}
		return $strFileName;
	}

}

?>