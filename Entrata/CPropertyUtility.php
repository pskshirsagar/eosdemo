<?php

class CPropertyUtility extends CBasePropertyUtility {

	protected $m_intUtilityTypeId;
	protected $m_strUtilityName;
	protected $m_strUtilityFormulaName;
	protected $m_strAmenityName;
	protected $m_strVendorName;
	protected $m_boolUtilityIsPublished;

	protected $m_boolIsLeaseResponsible;

    /**
     * Get functions
     */

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

    public function getUtilityName() {
    	return $this->m_strUtilityName;
    }

    public function getUtilityFormulaName() {
    	return $this->m_strUtilityFormulaName;
    }

    public function getAmenityName() {
    	return $this->m_strAmenityName;
    }

    public function getVendorName() {
    	return $this->m_strVendorName;
    }

    public function getUtilityIsPublished() {
    	return $this->m_boolUtilityIsPublished;
    }

    public function getIsLeaseResponsible() {
    	return $this->m_boolIsLeaseResponsible;
    }

    /**
     * Set functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrmixValues['utility_type_id'] );
		if( true == isset( $arrmixValues['utility_name'] ) ) $this->setUtilityName( $arrmixValues['utility_name'] );
		if( true == isset( $arrmixValues['utility_formula_name'] ) ) $this->setUtilityFormulaName( $arrmixValues['utility_formula_name'] );
		if( true == isset( $arrmixValues['amenity_name'] ) ) $this->setAmenityName( $arrmixValues['amenity_name'] );
		if( true == isset( $arrmixValues['vendor_name'] ) ) $this->setVendorName( $arrmixValues['vendor_name'] );
		if( true == isset( $arrmixValues['lease_responsible'] ) ) $this->setIsLeaseResponsible( $arrmixValues['lease_responsible'] );
		if( true == isset( $arrmixValues['utility_is_published'] ) ) $this->setUtilityIsPublished( $arrmixValues['utility_is_published'] );
    }

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->m_intUtilityTypeId = $intUtilityTypeId;
	}

    public function setUtilityName( $strUtilityName ) {
    	$this->m_strUtilityName = $strUtilityName;
    }

    public function setUtilityFormulaName( $strUtilityFormulaName ) {
    	$this->m_strUtilityFormulaName = $strUtilityFormulaName;
    }

    public function setAmenityName( $strAmenityName ) {
    	$this->m_strAmenityName = $strAmenityName;
    }

    public function setVendorName( $strVendorName ) {
    	$this->m_strVendorName = $strVendorName;
    }

    public function setUtilityIsPublished( $boolUtilityIsPublished ) {
    	$this->m_boolUtilityIsPublished = $boolUtilityIsPublished;
    }

    public function setIsLeaseResponsible( $boolIsLeaseResponsible ) {
    	$this->m_boolIsLeaseResponsible = $boolIsLeaseResponsible;
    }

	/**
	 * Validate functions
	 */

    public function valArCascadeReferenceId() {
        $boolIsValid = true;
        if( false == is_numeric( $this->getArCascadeReferenceId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_cascade_reference_id', __( 'Please select unit type/space.' ) ) );
        }
        return $boolIsValid;
    }

    public function valUtilityId() {
        $boolIsValid = true;
        if( false == is_numeric( $this->getUtilityId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_id', __( 'Please select utility.' ) ) );
        }
        return $boolIsValid;
    }

    public function valUtilityFormulaId() {
        $boolIsValid = true;
        if( false == is_numeric( $this->getUtilityFormulaId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_formula_id', __( 'Please select billing type.' ) ) );
        }
        return $boolIsValid;
    }

    public function valMinimumCostPerSpace() {
        $boolIsValid = true;
        if( false == is_null( $this->getMinimumCostPerSpace() ) && false == is_numeric( $this->getMinimumCostPerSpace() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_cost_per_space', __( 'Please enter valid low est.' ) ) );
        }
        return $boolIsValid;
    }

    public function valAverageCostPerSpace() {
        $boolIsValid = true;
        if( false == is_null( $this->getAverageCostPerSpace() ) && false == is_numeric( $this->getAverageCostPerSpace() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'average_cost_per_space', __( 'Please enter valid avg est.' ) ) );
        }
        return $boolIsValid;
    }

    public function valMaximumCostPerSpace() {
        $boolIsValid = true;
        if( false == is_null( $this->getMaximumCostPerSpace() ) && false == is_numeric( $this->getMaximumCostPerSpace() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_cost_per_space', __( 'Please enter valid high est.' ) ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valArCascadeReferenceId();
        		$boolIsValid &= $this->valUtilityId();
        		$boolIsValid &= $this->valUtilityFormulaId();
        		$boolIsValid &= $this->valMinimumCostPerSpace();
        		$boolIsValid &= $this->valMaximumCostPerSpace();
        		$boolIsValid &= $this->valAverageCostPerSpace();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>