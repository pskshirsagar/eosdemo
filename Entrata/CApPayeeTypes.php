<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeTypes
 * Do not add any new functions to this class.
 */

class CApPayeeTypes extends CBaseApPayeeTypes {

	public static function fetchApPayeeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApPayeeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchApPayeeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApPayeeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedApPayeeTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						ap_payee_types
					WHERE
						is_published = 1';

		return self::fetchApPayeeTypes( $strSql, $objDatabase );
	}

}
?>