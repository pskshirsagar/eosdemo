<?php

class CApPayeePunchoutCatalog extends CBaseApPayeePunchoutCatalog {

	const PUNCHOUT_CATALOG_CXML_KEY        = 'cxml';
	const PUNCHOUT_SETUP_REQUEST_URL       = 'PunchoutSetupRequest URL';
	const IDENTITY_LABEL                   = 'Catalog Identity';
	const SHARED_SECRET_LABEL              = 'Catalog SharedSecret';
	const PUNCHOUT_SETUP_REQUEST_URL_LABEL = 'Catalog URL';
	const ORDER_REQUEST_URL_LABEL          = 'Catalog OrderRequest URL';
	const TO_CREDENTIAL_IDENTITY           = 'To Credential Identity';
	const TO_CREDENTIAL_DOMAIN             = 'To Credential Domain';
	const IDENTITY                         = 'Identity';
	const SHARED_SECRET                    = 'SharedSecret';
	const ORDER_REQUEST_URL                = 'OrderRequest URL';
	const DUNS                             = 'DUNS';
	const NETWORK_ID                       = 'NetworkID';

	public static $c_arrstrApPayeePunchoutCatalogKeys = [
		self::IDENTITY                   => self::IDENTITY_LABEL,
		self::SHARED_SECRET              => self::SHARED_SECRET_LABEL,
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL          => self::ORDER_REQUEST_URL_LABEL,
		self::TO_CREDENTIAL_IDENTITY     => self::TO_CREDENTIAL_IDENTITY,
	];

	public static $c_arrstrAllApPayeePunchoutCatalogKeys = [
		self::IDENTITY                   => self::IDENTITY_LABEL,
		self::SHARED_SECRET              => self::SHARED_SECRET_LABEL,
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL          => self::ORDER_REQUEST_URL_LABEL,
		self::TO_CREDENTIAL_IDENTITY     => self::TO_CREDENTIAL_IDENTITY,
		self::TO_CREDENTIAL_DOMAIN => self::TO_CREDENTIAL_DOMAIN,
	];

	public static $c_arrstrToCredentialDomainValues = [
		self::DUNS => self::DUNS,
		self::NETWORK_ID => self::NETWORK_ID
	];

	public static $c_arrstrUrlKeys = [
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL => self::ORDER_REQUEST_URL_LABEL
	];

	protected $m_arrstrMissingRequiredFields;

	protected $m_arrintPropertyGroupIds;

	public function getMissingRequiredFields() {
		return $this->m_arrstrMissingRequiredFields;
	}

	public function getPropertyGroupIds() {
		return $this->m_arrintPropertyGroupIds;
	}

	public function setPropertyGroupIds( $arrintPropertyGroupIds ) {
		$this->m_arrintPropertyGroupIds = $arrintPropertyGroupIds;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		if( false == Psi\Libraries\UtilFunctions\valId( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->m_arrstrMissingRequiredFields['Vendor'] = 'Vendor';
		}
		return $boolIsValid;
	}

	public function valIsSetupCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		$strInValidKeys = NULL;
		if( true == Psi\Libraries\UtilFunctions\valArr( $arrmixDetails = json_decode( json_encode( $this->getDetails() ), true ) )
		    && true == isset( $arrmixDetails[self::PUNCHOUT_CATALOG_CXML_KEY] ) ) {
			foreach( $arrmixDetails[self::PUNCHOUT_CATALOG_CXML_KEY] as $strKey => $strValue ) {

				if( false == Psi\Libraries\UtilFunctions\valStr( $strValue ) && true == isset( self::$c_arrstrAllApPayeePunchoutCatalogKeys[$strKey] ) ) {

				$this->m_arrstrMissingRequiredFields[self::$c_arrstrAllApPayeePunchoutCatalogKeys[$strKey]] = self::$c_arrstrAllApPayeePunchoutCatalogKeys[$strKey];
					$boolIsValid &= false;
				}

				// check the key is of type URL then validate the URL format
				if( true == Psi\Libraries\UtilFunctions\valStr( $strValue ) && true == Psi\Libraries\UtilFunctions\valArr( preg_grep( '/' . $strKey . '/i', array_keys( self::$c_arrstrUrlKeys ) ) ) && false == CValidation::checkUrl( $strValue ) ) {
					$boolIsValid    &= false;
					$strInValidKeys .= self::$c_arrstrUrlKeys[$strKey] . ', ';
				}
			}

			if( false == $boolIsValid && true == Psi\Libraries\UtilFunctions\valStr( $strInValidKeys ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'A valid URL is required for {%s, 0}.', [ rtrim( $strInValidKeys, ', ' ) ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupId() {

		$boolIsValid = true;
		if( false == Psi\Libraries\UtilFunctions\valArr( $this->getPropertyGroupIds() ) ) {
			$boolIsValid = true;
			$this->m_arrstrMissingRequiredFields['Property'] = 'Property';
		}
		return $boolIsValid;
	}

	public function valApPayeePunchoutCatalogs( $objDatabase ) {
		$intApPayeePunchoutCatalogsCount = ( int ) \Psi\Eos\Entrata\CApPayeePunchoutCatalogs::createService()->fetchEnabledApPayeePunchoutCatalogsCountByApPayeeIdByCid( $this->getApPayeeId(), $this->getCid(), $objDatabase, $this->getId() );

		if( 1 <= $intApPayeePunchoutCatalogsCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Punchout catalog is already exist for vendor.' ) ) );
			return false;
		}

		return true;
	}

	public function builtRequiredFieldErrorMessage() {

		if( true == Psi\Libraries\UtilFunctions\valArr( $this->getMissingRequiredFields() ) ) {
			$strRequiredFields = implode( ', ', $this->getMissingRequiredFields() );
			$strMessage = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->getMissingRequiredFields() ) ) ? ' are ' : ' is ';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( ' {%s, 0} {%s, 1} required.', [ $strRequiredFields, $strMessage ] ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valPropertyGroupId();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->builtRequiredFieldErrorMessage();
				$boolIsValid &= $this->valApPayeePunchoutCatalogs( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>