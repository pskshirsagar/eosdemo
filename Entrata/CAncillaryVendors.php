<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryVendors
 * Do not add any new functions to this class.
 */

class CAncillaryVendors extends CBaseAncillaryVendors {

	public static function fetchAllAncillaryVendors( $objClientDatabase ) {

		$strSql = 'SELECT
						id,
						name
					FROM
						ancillary_vendors';

		return self::fetchAncillaryVendors( $strSql, $objClientDatabase );
	}

}
?>