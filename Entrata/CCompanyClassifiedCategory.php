<?php

class CCompanyClassifiedCategory extends CBaseCompanyClassifiedCategory {
	const NAME_COMMUNITY_FEEDBACK = 'Community Feedback';

	protected $m_intAdsCount;

	public function getCustomerClassifiedsCount() {
		return $this->m_intCustomerClassifiedsCount;
	}

	public function getIsAdminRestricted() {
		return $this->m_intIsAdminRestricted;
	}

	public function setCustomerClassifiedsCount( $intCustomerClassifiedsCount ) {
		$this->m_intCustomerClassifiedsCount = $intCustomerClassifiedsCount;
	}

	public function setIsAdminRestricted( $intIsAdminRestricted ) {
		$this->m_intIsAdminRestricted = $intIsAdminRestricted;
	}

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrintValues['customer_classifieds_count'] ) ) $this->setCustomerClassifiedsCount( $arrintValues['customer_classifieds_count'] );
		if( true == isset( $arrintValues['is_admin_restricted'] ) ) $this->setIsAdminRestricted( $arrintValues['is_admin_restricted'] );

		if( isset( $arrintValues['ads_count'] ) && $boolDirectSet ) {
			$this->m_intAdsCount = trim( $arrintValues['ads_count'] );
		} elseif( isset( $arrintValues['ads_count'] ) ) {
			$this->setAdsCount( $arrintValues['ads_count'] );
		}

		return;
	}

	public function setAdsCount( $intAdsCount ) {
		$this->m_intAdsCount = CStrings::strToIntDef( $intAdsCount, NULL, false );
	}

	public function getAdsCount() {
		return $this->m_intAdsCount;
	}

	public function fetchPropertyClassifiedCategoryByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyClassifiedCategories::fetchPropertyClassifiedCategoryByCompanyClassifiedCategoryIdByPropertyIdByCid( $this->m_intId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function valCode() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strCode ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', __( 'Code is required.' ), 502 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {

		$boolIsValid = true;
		// It is required

		if( false == valStr( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ), 502 ) );
			$boolIsValid = false;
		}

		$intCompanyClassifiedCategoryCount = CCompanyClassifiedCategories::fetchDuplicateCompanyClassifiedCategoryCountByIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

		if( 0 < $intCompanyClassifiedCategoryCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>