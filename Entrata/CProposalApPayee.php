<?php

class CProposalApPayee extends CBaseProposalApPayee {

	protected $m_strName;
	protected $m_strCompanyName;

	/**
	* Get Functions
	*/

	public function getName() {
		return $this->m_strName;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
	}

}
?>