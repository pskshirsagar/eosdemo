<?php

class CApplicationStatusType extends CBaseApplicationStatusType {

    const INFO_REQUESTED 					= 1; // Guest Card

	const PRE_SCREENING 					= 3; // Application
    const PRE_SCREENING_PENDING 			= 4; // Application
	const PRE_SCREENING_APPROVED 			= 5; // Application

	const APPLICATION_OPEN 					= 6; // Application
	const CO_APPLICANT_PENDING 				= 7; // Application
	const COMPLETED							= 8; // Application
	const APPLICATION_APPROVED				= 9; // Application

	const RENEWAL_APPROVAL_PENDING			= 12; // Renewal
	const RENEWAL_OFFER_EXTENDED			= 13; // Renewal
	const RENEWAL_OFFER_ACCEPTED			= 14; // Renewal

	const LEASE_PENDING						= 15; // Lease
	const LEASE_PARTIALLY_SIGNED			= 16; // Lease
	const LEASE_COMPLETED					= 17; // Lease
	const LEASE_APPROVED					= 18; // Lease

	const ON_HOLD							= 19; // Inactive
	const CANCELLED							= 20; // Inactive
	const DENIED							= 21; // Inactive - This should not be a status type (should be a cancellation type)
	const RENEWAL_APPROVAL_DENIED			= 22; // Inactive - This should not be a status type (should be a cancellation type)

	public static $c_arrintCombineLeadApplicationStatusTypIds = [
		self::INFO_REQUESTED,
		self::PRE_SCREENING,
		self::PRE_SCREENING_PENDING,
		self::PRE_SCREENING_APPROVED,
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED ];

	public static $c_arrintNonActiveApplicationStatusTypeIds 	= [
		self::ON_HOLD,
	    self::CANCELLED,
		self::DENIED,
		self::RENEWAL_APPROVAL_DENIED,
	  	self::LEASE_APPROVED ];

	public static $c_arrintUnfulfilledNonActiveApplicationStatusTypeIds 	= [
		self::ON_HOLD,
		self::CANCELLED,
	 	self::DENIED,
	 	self::RENEWAL_APPROVAL_DENIED ];

	public static $c_arrintRentalApplicationGroupStatusTypeIds = [
		self::INFO_REQUESTED,
		self::PRE_SCREENING,
		self::PRE_SCREENING_PENDING,
		self::PRE_SCREENING_APPROVED,
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED ];

	public static $c_arrintPreScreeningStatusTypeIds 			= [
		self::PRE_SCREENING,
		self::PRE_SCREENING_PENDING,
		self::PRE_SCREENING_APPROVED ];

	public static $c_arrintRenewalGroupStatusTypeIds 			= [
		self::RENEWAL_APPROVAL_PENDING,
		self::RENEWAL_OFFER_EXTENDED,
		self::RENEWAL_OFFER_ACCEPTED,
		self::RENEWAL_APPROVAL_DENIED ];

	public static $c_arrintLeaseGroupStatusTypeIds 			= [
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED ];

	public static $c_arrintLeaseSignPendingStatusTypeIds = [
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED ];

	public static $c_arrintValidPostApplicationCompletedStatusTypeIds = [
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED ];

	public static $c_arrintSmsNotificationOnApplicationStatusTypeIds = [
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::CANCELLED,
		self::DENIED,
		self::LEASE_PARTIALLY_SIGNED ];

	public static $c_arrintValidBluemoonApplicationIntegrationStatusTypeIds = [
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED ];

	public static $c_arrintWaitlistApplicationStatusTypeIds = [
		self::COMPLETED,
 		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED
	];

	public static $c_arrintActiveLeasePendingModificationStatusTypeIds = [
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED
	];

	public static $c_arrintInactiveLeasePendingModificationStatusTypeIds = [
		self::APPLICATION_OPEN,
		self::CANCELLED
	];

	public static $c_arrintLockDownApplicationStatusTypeIds = [
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED
	];

	public static $c_arrintActiveLeaseApplicationStatusTypeIds	= [
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::RENEWAL_OFFER_EXTENDED,
		self::RENEWAL_OFFER_ACCEPTED,
		self::RENEWAL_APPROVAL_PENDING
	];

	public static $c_arrintValidPaymentStepCompletedStatusTypeIds	= [
		self::PRE_SCREENING,
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::APPLICATION_APPROVED,
		self::LEASE_APPROVED
	];

	public static $c_arrintStartedApplicationStatusTypeIds = [
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED,
		self::ON_HOLD,
		self::CANCELLED,
		self::DENIED
	];

	public static $c_arrintCompletedApplicationStatusTypeIds = [
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED
	];

	public static $c_arrintLeaseSignedLeadsApplicationStatusTypeIds = [
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED
	];

	public static $c_arrintBulkUnitAssignmentApplicationStatustypes = [
		self::APPLICATION_APPROVED		=> 'Application Approved',
		self::RENEWAL_OFFER_ACCEPTED 	=> 'Renewal Offer Accepted',
		self::LEASE_PENDING 			=> 'Lease Pending',
		self::LEASE_PARTIALLY_SIGNED 	=> 'Lease Partially Signed',
		self::LEASE_COMPLETED 			=> 'Lease Completed',
		self::LEASE_APPROVED 			=> 'Lease Approved'
	];

	public static $c_arrintCustomLeadApplicationStatusTypIds = [
		self::INFO_REQUESTED,
		self::PRE_SCREENING,
		self::PRE_SCREENING_PENDING,
		self::PRE_SCREENING_APPROVED,
		self::APPLICATION_OPEN,
		self::CO_APPLICANT_PENDING,
		self::COMPLETED,
		self::APPLICATION_APPROVED,
		self::RENEWAL_APPROVAL_PENDING,
		self::RENEWAL_OFFER_EXTENDED,
		self::RENEWAL_OFFER_ACCEPTED,
		self::LEASE_PENDING,
		self::LEASE_PARTIALLY_SIGNED,
		self::LEASE_COMPLETED ];

	public static $c_arrintLeadApplicationStatusTypeIds = [
		self::INFO_REQUESTED,
		self::APPLICATION_OPEN,
		self::COMPLETED,
		self::CO_APPLICANT_PENDING,
		self::APPLICATION_APPROVED,
		self::LEASE_PENDING,
		self::LEASE_COMPLETED,
		self::LEASE_APPROVED
	];

	public static function applicationStatusTypeIdToStr( $intUnitSpaceStatusTypeId ) {
		switch( $intUnitSpaceStatusTypeId ) {
			case self::INFO_REQUESTED:
				return __( 'Info Requested' );
				break;

			case self::PRE_SCREENING:
				return __( 'Pre Qualification' );
				break;

			case self::PRE_SCREENING_PENDING:
				return __( 'Pre Qualification Pending' );
				break;

			case self::PRE_SCREENING_APPROVED:
				return __( 'Pre Qualification Approved' );
				break;

			case self::APPLICATION_OPEN:
				return __( 'Application Open' );
				break;

			case self::CO_APPLICANT_PENDING:
				return __( 'Waiting for Co-Applicants' );
				break;

			case self::COMPLETED:
				return __( 'Application Completed' );
				break;

			case self::APPLICATION_APPROVED:
				return __( 'Application Approved' );
				break;

			case self::RENEWAL_APPROVAL_PENDING:
				return __( 'Renewal Offer Approval Pending' );
				break;

			case self::RENEWAL_OFFER_EXTENDED:
				return __( 'Renewal Offer Extended' );
				break;

			case self::RENEWAL_OFFER_ACCEPTED:
				return __( 'Renewal Offer Accepted' );
				break;

			case self::LEASE_PENDING:
				return __( 'Lease Pending' );
				break;

			case self::LEASE_PARTIALLY_SIGNED:
				return __( 'Lease Partially Signed' );
				break;

			case self::LEASE_COMPLETED:
				return __( 'Lease Completed' );
				break;

			case self::LEASE_APPROVED:
				return __( 'Lease Approved' );
				break;

			case self::ON_HOLD:
				return __( 'Archived' );
				break;

			case self::CANCELLED:
				return __( 'Cancelled' );
				break;

			case self::DENIED:
				return __( 'Denied' );
				break;

			case self::RENEWAL_APPROVAL_DENIED:
				return __( 'Renewal Offer Approval Denied' );
				break;

			default:
				// default case
				break;
		}
	}

	public function getCustomDescription( $objApplicantApplication, $objApplication ) {

		$strApplicationStatusDescription = '';

		switch( $this->getId() ) {
			case self::APPLICATION_OPEN:
				if( 1 == $objApplication->getRequiresCapture() ) {
					$strApplicationStatusDescription = __( 'Your application has been submitted, waiting for payment confirmation.' );
				} else {
					$strApplicationStatusDescription = __( 'Your application has been started, but not completed' );
				}
				break;

			case self::CO_APPLICANT_PENDING:
				if( CLeaseCustomerType::PRIMARY == $objApplicantApplication->getLeaseCustomerTypeId() ) {
					$strApplicationStatusDescription = __( 'Your information is complete, we are now waiting for others to complete their information.' );
				} else {
					$strApplicationStatusDescription = __( 'In order to process this rental application, we need to gather some information from you.' );
				}
				break;

			case self::PRE_SCREENING_PENDING:
				$strApplicationStatusDescription = __( 'Thank you for submitting your information, we are currently processing your request.' );
				break;

			case self::PRE_SCREENING_APPROVED:
				$strApplicationStatusDescription = __( 'Congratulations, your request has been processed.' );
				break;

			case self::COMPLETED:
				$strApplicationStatusDescription = __( 'Your application has been completed, waiting for property approval.' );
				break;

			case self::APPLICATION_APPROVED:
				$strApplicationStatusDescription = __( 'Congratulations, your application has been approved.' );
				break;

			case self::LEASE_PENDING:
			case self::LEASE_PARTIALLY_SIGNED:
				if( true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) ) {
					return;
				}

				if( false == is_null( $objApplicantApplication->getExpirationDate() ) && ( strtotime( $objApplicantApplication->getExpirationDate() ) < strtotime( date( 'm/d/Y' ) ) ) ) {
					$strApplicationStatusDescription = __( 'Your lease offer has expired. Please contact the leasing office for further assistance.' );
				} elseif( CLeaseCustomerType::PRIMARY == $objApplicantApplication->getLeaseCustomerTypeId() ) {
					if( true == is_null( $objApplicantApplication->getLeaseSignedOn() ) ) {
						$strApplicationStatusDescription = __( 'Your lease is ready to be reviewed and signed.' );
					} else {
						$strApplicationStatusDescription = __( 'You have completely signed your lease, we are now waiting for others to sign.' );
					}
				} else {
					if( true == is_null( $objApplicantApplication->getLeaseSignedOn() ) ) {
						$strApplicationStatusDescription = __( 'Your lease is ready to be reviewed and signed.' );
					} else {
						$strApplicationStatusDescription = __( 'You have completely signed your lease, we are now waiting for others to sign.' );
					}
				}
				break;

			case self::LEASE_COMPLETED:
				$strApplicationStatusDescription = __( 'Waiting for the property to sign and approve your lease.' );
				break;

			case self::LEASE_APPROVED:
				$strApplicationStatusDescription = __( 'Your lease is completely signed and approved.' );
				break;

			default:
				// default case
				break;
		}

		return ( 0 < strlen( $strApplicationStatusDescription ) ) ? $strApplicationStatusDescription : $this->getDescription();
	}

}
?>