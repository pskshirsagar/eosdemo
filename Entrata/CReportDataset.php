<?php

class CReportDataset extends CBaseReportDataset {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatasetKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $strAction, $objDatabase ) {
		$boolIsValid = true;
		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Dataset Name is required.</br>' ) );
		} else {
			$strWhere = '
					WHERE
						name = \'' . pg_escape_string( $this->m_strName ) . '\'
						AND dataset_key = \'' . pg_escape_string( $this->m_strDatasetKey ) . '\'';

			$strWhere .= ' AND deleted_by IS NULL AND default_report_version_id = ' . $this->m_intDefaultReportVersionId;

			if( VALIDATE_UPDATE == $strAction ) {
				$strWhere .= ' AND id != ' . $this->m_intId;
			}

			if( false == is_null( $objDatabase ) ) {
				$intExistingDatasetCount = \Psi\Eos\Entrata\CReportDatasets::createService()->fetchReportDatasetCount( $strWhere, $objDatabase );
				if( 0 < $intExistingDatasetCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Dataset with ' . $this->m_strName . ' Name and ' . $this->m_strDatasetKey . ' Key already exists.' ) );
				}

			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>