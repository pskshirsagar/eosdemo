<?php

class CCustomerPhoneNumber extends CBaseCustomerPhoneNumber {

    /**
     * Validation Functions
     */

	protected $m_objI18nPhoneNumber;

	/**
	 * Get Functions
	 */

	public function getI18nPhoneNumber() {
		return $this->m_objI18nPhoneNumber;
	}

	/**
	 * Set Functions
	 */

	public function setI18nPhoneNumber( $objI18nPhoneNumber ) {
		$this->m_objI18nPhoneNumber = $objI18nPhoneNumber;
	}

	public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) ) {
            $boolIsValid = false;
            trigger_error( 'Invalid Customer Phone Number Request:  Id required - CCustomerPhoneNumber::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Phone Number Request:  Management id required - CCustomerPhoneNumber::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Phone Number Request:  Customer Id required - CCustomerPhoneNumber::valCustomerId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valPhoneNumberTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intPhoneNumberTypeId ) || false == valId( ( int ) $this->m_intPhoneNumberTypeId ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Phone Number Request:  Phone Number Type Id required - CCustomerPhoneNumber::valPhoneNumberTypeId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;

	    $objI18nPhoneNumber = $this->getI18nPhoneNumber();
	    if( true == valObj( $objI18nPhoneNumber, 'i18n\CPhoneNumber' ) ) {
		    if( false == $objI18nPhoneNumber->isValid( false ) ) {
			    $this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPhoneNumber->getFormattedErrors() ] ) ) );
			    return false;
		    }
	    } else {
		    if( false == isset( $this->m_strPhoneNumber ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Customer phone number is required' ) ) );
		    }
	    }

        return $boolIsValid;
    }

    public function valExtension() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_strExtension )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function valCustomerPhoneNumber( $strSecondaryPhoneNumber, $boolIsReq = false ) {

		$objI18nPhoneNumber = $this->getI18nPhoneNumber();
		if( true == valObj( $objI18nPhoneNumber, 'i18n\CPhoneNumber' ) ) {
			if( false == $objI18nPhoneNumber->isValid( $boolIsReq ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPhoneNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$intSecondaryNumberLength	= \Psi\CStringService::singleton()->strlen( $strSecondaryPhoneNumber );

			if( '' != $strSecondaryPhoneNumber && 0 < $intSecondaryNumberLength && ( 10 > $intSecondaryNumberLength || 15 < $intSecondaryNumberLength ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number must be between 10 and 15 characters.' ), '' ) );
				return false;
			}

			if( '' != $strSecondaryPhoneNumber && false == ( CValidation::validateFullPhoneNumber( $strSecondaryPhoneNumber, false ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Valid secondary phone number is required.' ), 504 ) );
				return false;
			}
		}

		return true;
	}

	// TODO: This file is not appropriate place to this function. Need to place it at another common place of Phone Number project files

	public function insertOrUpdateCustomerPhoneNumber( $arrobjCustomerPhoneNumber, $intCompanyUser, $intCid, $objDatabase, $boolIsDelete = true, $boolIsOnlyAdditionalPhoneNumber = false ) {

		$boolIsValid = true;
		$boolIsPrimary = false;
		$objExistingPrimaryCustomerPhoneNumber = NULL;
		$arrobjReopenCustomerPhoneNumber = [];
		$intCustomerId = $arrobjCustomerPhoneNumber[0]->getCustomerId();
		$strNewPrimaryPhoneNumber = '';
		$intNewPrimaryCustomerPhoneNumberId = '';

		$arrobjCustomerPhoneNumber         = ( array ) rekeyObjects( 'PhoneNumber', $arrobjCustomerPhoneNumber );
		$arrobjExistingCustomerPhoneNumber = ( array ) rekeyObjects( 'PhoneNumber', \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchAllCustomerPhoneNumbersByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) );

		if( true == valArr( $arrobjCustomerPhoneNumber ) && true == array_key_exists( '', $arrobjCustomerPhoneNumber ) ) {
			unset( $arrobjCustomerPhoneNumber[''] );
		}

		foreach( $arrobjCustomerPhoneNumber AS $strPhoneNumber => $objCustomerPhoneNumber ) {
			unset( $arrobjCustomerPhoneNumber[$strPhoneNumber] );
			$strPhoneNumber = preg_replace( '/[^0-9]*/', '', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
			$arrobjCustomerPhoneNumber[$strPhoneNumber] = $objCustomerPhoneNumber;

			if( true == valStr( $objCustomerPhoneNumber->getPhoneNumber() ) && true == $objCustomerPhoneNumber->getIsPrimary() ) {
				$boolIsPrimary = true;
				$strNewPrimaryPhoneNumber = preg_replace( '/[^0-9]*/', '', $objCustomerPhoneNumber->getPhoneNumber() );
			}
		}

		foreach( $arrobjExistingCustomerPhoneNumber AS $strPhoneNumber => $objExistingCustomerPhoneNumber ) {
			unset( $arrobjExistingCustomerPhoneNumber[$strPhoneNumber] );
			$strPhoneNumber = preg_replace( '/[^0-9]*/', '', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
			if( $strNewPrimaryPhoneNumber ==  $strPhoneNumber ) {
				$intNewPrimaryCustomerPhoneNumberId = $objExistingCustomerPhoneNumber->getId();
			}
			$arrobjExistingCustomerPhoneNumber[$strPhoneNumber] = $objExistingCustomerPhoneNumber;
		}

		foreach( $arrobjCustomerPhoneNumber AS $strPhoneNumber => $objCustomerPhoneNumber ) {

			if( true == array_key_exists( $strPhoneNumber, $arrobjExistingCustomerPhoneNumber ) ) {
				$objCustomerPhoneNumber->setDeletedOn( NULL );
				$objCustomerPhoneNumber->setDeletedBy( NULL );
				$objCustomerPhoneNumber->setId( $arrobjExistingCustomerPhoneNumber[$strPhoneNumber]->getId() );

				if( true == valId( $arrobjExistingCustomerPhoneNumber[$strPhoneNumber]->getDeletedBy() ) || $objCustomerPhoneNumber->getIsPrimary() != $arrobjExistingCustomerPhoneNumber[$strPhoneNumber]->getIsPrimary() || $objCustomerPhoneNumber->getPhoneNumberTypeId() != $arrobjExistingCustomerPhoneNumber[$strPhoneNumber]->getPhoneNumberTypeId() ) {
					array_push( $arrobjReopenCustomerPhoneNumber, $objCustomerPhoneNumber );
				}

				$objExistingPrimaryCustomerPhoneNumber = $objCustomerPhoneNumber;
				unset( $arrobjCustomerPhoneNumber[$strPhoneNumber] );
				unset( $arrobjExistingCustomerPhoneNumber[$strPhoneNumber] );
			}
		}

		if( false == $boolIsPrimary && false == $boolIsOnlyAdditionalPhoneNumber ) {

			if( true == valArr( $arrobjCustomerPhoneNumber ) ) {
				$arrobjCustomerPhoneNumber[array_keys( $arrobjCustomerPhoneNumber )[0]]->setIsPrimary( true );
				$boolIsPrimary = true;
			} elseif( true == valArr( $arrobjReopenCustomerPhoneNumber ) ) {
				$arrobjReopenCustomerPhoneNumber[array_keys( $arrobjReopenCustomerPhoneNumber )[0]]->setIsPrimary( true );
				$boolIsPrimary = true;
			} elseif( false == valArr( $arrobjReopenCustomerPhoneNumber ) && false == valArr( $arrobjCustomerPhoneNumber ) ) {

				if( true == valObj( $objExistingPrimaryCustomerPhoneNumber, CCustomerPhoneNumber::class ) ) {
					$objExistingPrimaryCustomerPhoneNumber->setIsPrimary( true );
					array_push( $arrobjReopenCustomerPhoneNumber, $objExistingPrimaryCustomerPhoneNumber );
					$boolIsPrimary = true;
				}
			}

		}

		$arrobjExistingPrimaryPhoneNumbers = ( array ) \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchExistingPrimaryCustomerPhoneNumbersByCustomerIdByIdByCid( $intCustomerId, $intNewPrimaryCustomerPhoneNumberId, $intCid, $objDatabase );

		if( true == valArr( $arrobjExistingPrimaryPhoneNumbers ) ) {
			foreach( $arrobjExistingPrimaryPhoneNumbers as $objExistingPrimaryPhoneNumber ) {
				$objExistingPrimaryPhoneNumber->setIsPrimary( false );
			}

			if( false == \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->bulkUpdate( $arrobjExistingPrimaryPhoneNumbers, [ 'is_primary', 'updated_by', 'updated_on' ], $intCompanyUser, $objDatabase ) ) {
				$boolIsValid = false;
			}
		}

		if( true == valArr( $arrobjCustomerPhoneNumber ) ) {
			if( false == \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->bulkInsert( $arrobjCustomerPhoneNumber, $intCompanyUser, $objDatabase ) ) {
				$boolIsValid = false;
			}
		}

		if( true == valArr( $arrobjReopenCustomerPhoneNumber ) ) {
			if( false == \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->bulkUpdate( $arrobjReopenCustomerPhoneNumber, [ 'deleted_on', 'deleted_by', 'is_primary', 'phone_number_type_id' ], $intCompanyUser, $objDatabase ) ) {
				$boolIsValid = false;
			}
		}

		foreach( ( array ) $arrobjExistingCustomerPhoneNumber AS $strPhoneNumber => $objCustomerPhoneNumber ) {

			if( false == is_null( $objCustomerPhoneNumber->getDeletedOn() ) && false == is_null( $objCustomerPhoneNumber->getDeletedBy() ) && false == $objCustomerPhoneNumber->getIsPrimary() ) {
				unset( $arrobjExistingCustomerPhoneNumber[$strPhoneNumber] );
				continue;
			}
			if( true == $boolIsDelete && true == is_null( $objCustomerPhoneNumber->getDeletedOn() ) && true == is_null( $objCustomerPhoneNumber->getDeletedBy() ) ) {
				$objCustomerPhoneNumber->setDeletedOn( 'NOW()' );
				$objCustomerPhoneNumber->setDeletedBy( $intCompanyUser );
			}
			if( true == $boolIsPrimary && true == $objCustomerPhoneNumber->getIsPrimary() ) {
				$objCustomerPhoneNumber->setIsPrimary( false );
			}
		}

		if( true == valArr( $arrobjExistingCustomerPhoneNumber ) && false == \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->bulkUpdate( $arrobjExistingCustomerPhoneNumber, [ 'deleted_on', 'deleted_by', 'is_primary' ], $intCompanyUser, $objDatabase ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateCustomerPhoneNumbers( $arrobjCustomerPhoneNumbers, $boolIsRequired = true ) {
		$arrintCustomerPhoneNumber = [];
		$boolIsValid = true;
		$boolIsRequire = false;
		$boolIsDuplicate = false;
		$boolIsPrimary = false;
		$boolIsPostedPhoneNumber = false;

		foreach( $arrobjCustomerPhoneNumbers AS $objCustomerPhoneNumber ) {

			$objI18nPhoneNumber = $objCustomerPhoneNumber->getI18nPhoneNumber();
			if( true == valObj( $objI18nPhoneNumber, \i18n\CPhoneNumber::class ) ) {
				if( false == $objI18nPhoneNumber->isValid( false ) ) {
					$this->addErrorMsg( new \Psi\Libraries\UtilErrorMsg\CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode() ] ) ) );
					return false;
				}
			}

			if( false == $boolIsDuplicate && true == valStr( $objCustomerPhoneNumber->getPhoneNumber() ) && true == in_array( $objCustomerPhoneNumber->getPhoneNumber(), $arrintCustomerPhoneNumber ) ) {
				$this->addErrorMsg( new \Psi\Libraries\UtilErrorMsg\CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'The same number cannot be used more than once.' ) ) );
				$boolIsDuplicate = true;
				$boolIsValid = false;
			}

			if( true == $boolIsRequired && false == $boolIsRequire && false == valStr( $objCustomerPhoneNumber->getPhoneNumber() ) ) {
				$this->addErrorMsg( new \Psi\Libraries\UtilErrorMsg\CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
				$boolIsValid = false;
				$boolIsRequire = true;
			}

			if( true == valStr( $objCustomerPhoneNumber->getPhoneNumber() ) && true == $objCustomerPhoneNumber->getIsPrimary() ) {
				$boolIsPrimary = true;
			}

			if( true == valStr( $objCustomerPhoneNumber->getPhoneNumber() ) ) {
				$boolIsPostedPhoneNumber = true;
			}

			$arrintCustomerPhoneNumber[] = $objCustomerPhoneNumber->getPhoneNumber();
		}

		if( true == $boolIsPostedPhoneNumber && false == $boolIsPrimary && true == $boolIsRequired ) {
			$this->addErrorMsg( new \Psi\Libraries\UtilErrorMsg\CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Please add a primary phone number.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// TODO: This file is not appropriate place to this function. Need to place it at another common place of Phone Number project files

	public function loadCustomerPhoneNumbers( $arrmixCustomerPhoneNumbers, $intCustomerId, $intCid ) {

		require_once( PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php' );

		$arrobjCustomerPhoneNumbers = [];

		foreach( $arrmixCustomerPhoneNumbers AS $intCustomerPhoneNumberKey => $arrmixCustomerPhoneNumber ) {

			$objCustomerPhoneNumber = new \CCustomerPhoneNumber();
			$objCustomerPhoneNumber->setCid( $intCid );
			$objCustomerPhoneNumber->setCustomerId( $intCustomerId );
			$objCustomerPhoneNumber->setPhoneNumber( $arrmixCustomerPhoneNumber['phone_number'] );
			$objCustomerPhoneNumber->setIsPrimary( $arrmixCustomerPhoneNumber['is_primary'] );
			$objCustomerPhoneNumber->setPhoneNumberTypeId( $arrmixCustomerPhoneNumber['phone_number_type'] );
			$objCustomerPhoneNumber->setI18nPhoneNumber( \i18n\CPhoneNumber::create( $arrmixCustomerPhoneNumber['phone_number'] ) );

			array_push( $arrobjCustomerPhoneNumbers, $objCustomerPhoneNumber );
		}

		return $arrobjCustomerPhoneNumbers;
	}

}
?>