<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoutes
 * Do not add any new functions to this class.
 */

class CRoutes extends CBaseRoutes {

	public static function fetchCustomRouteByIdByCid( $intId, $intCid, $objClientDatabase ) {
		return self::fetchRoute( 'SELECT * FROM routes WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId, $objClientDatabase );
	}

	public static function fetchRoutesByIsPublishedByCid( $intIsPublished, $intCid, $objClientDatabase ) {
		return self::fetchRoutes( 'SELECT * FROM routes WHERE cid = ' . ( int ) $intCid . ' AND is_published = ' . ( int ) $intIsPublished, $objClientDatabase );
	}

	public static function fetchRouteByRuleStopIdByRouteTypeIdByCid( $intRuleStopId, $intRouteTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT r.* FROM
						routes r
						JOIN rule_stops rs ON ( rs.cid = r.cid AND rs.route_id = r.id )
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rs.id = ' . ( int ) $intRuleStopId;

		return self::fetchRoute( $strSql, $objDatabase );
	}

	public static function fetchSimpleRoutesWithAssociatedCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intHideDisabled ) {

		$strSql = 'WITH associated_count AS (
					SELECT
						pr.cid,
						pr.route_id,
						pr.route_type_id,
						COUNT( DISTINCT pr.property_id ) as assigned_count
					FROM
						property_routes pr
						LEFT JOIN property_gl_settings pgs ON ( pr.cid = pgs.cid AND pr.property_id = pgs.property_id )
					WHERE
						pr.cid = ' . ( int ) $intCid .
						( true == valArr( $arrintPropertyIds ) ? ' AND pr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : ' AND 1 != 1 ' ) . '
						AND pgs.activate_standard_posting = TRUE
					GROUP BY
						pr.cid,
						pr.route_id,
						pr.route_type_id
				)

				SELECT
					r.id,
					r.name,
					r.route_type_id,
					r.is_published,
					COALESCE( ac.assigned_count, 0 ) AS assigned_count
				FROM
					routes r
					LEFT JOIN associated_count ac ON ( ac.cid = r.cid AND ac.route_id = r.id AND ac.route_type_id = r.route_type_id )
				WHERE
					r.cid = ' . ( int ) $intCid . ' ' .
					( 1 == $intHideDisabled ? ' AND r.is_published = 1 ' : '' ) . '
				ORDER BY
					r.is_published DESC, r.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRoutesByRouteTypeIdByRouteIdsByCid( $intRouteTypeId, $arrintRouteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintRouteIds ) ) return;

		$strSql = 'SELECT
						r.*
					FROM
						routes r
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND r.id IN ( ' . implode( ',', $arrintRouteIds ) . ' ) ';

		return self::fetchRoutes( $strSql, $objDatabase );
	}

	public static function fetchConflictingRoutesByNameByIdByRouteTypeIdByCid( $strName, $intId, $intRouteTypeId, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) ) return;

		$strSql = 'SELECT
						r.*
					FROM
						routes r
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND lower( r.name ) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strName ) ) ) . '\'';

		$strSql .= ( true == valId( $intId ) ) ? ' AND r.id <> ' . ( int ) $intId : ' ';

		return self::fetchRoutes( $strSql, $objDatabase );
	}

	public static function fetchRoutesByArApprovalRequestIdByRouteTypeIdByCid( int $intArApprovalRequestId, int $intRouteType = CRouteType::TRANSACTIONS, int $intCid, CDatabase $objDatabase, $arrintRuleStopStatusTypeIds = [] ) {

		$strWhereCondition = ( true == valArr( $arrintRuleStopStatusTypeIds ) ) ? ' AND rsr.rule_stop_status_type_id IN ( ' . implode( ',', $arrintRuleStopStatusTypeIds ) . ') ' : '';

		$strSql = ' SELECT
						DISTINCT r.id,
						r.name,
						r.allow_notes_on_approval, 
						r.require_notes_on_rejected_returned
					 FROM
						routes r
						JOIN rule_stops rs ON ( r.cid = rs.cid AND r.id = rs.route_id )
						JOIN rule_stop_results rsr ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id)
						JOIN ar_approval_requests apr ON ( apr.cid = rsr.cid AND apr.id = rsr.reference_id AND apr.route_type_id = ' . ( int ) $intRouteType . ' )
					WHERE 
						apr.cid = ' . ( int ) $intCid . '
						AND apr.id = ' . ( int ) $intArApprovalRequestId . '
						' . $strWhereCondition . '
						AND apr.is_archived = FALSE;';

		return self::fetchRoutes( $strSql, $objDatabase );

	}

	public static function fetchRouteDetailsByIdsByCid( $arrintRouteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintRouteIds ) ) return NULL;

		$strSql = 'SELECT
						r.id,
						r.name,
						CASE WHEN r.allow_notes_on_approval THEN 1 ELSE 0 END AS allow_notes_on_approval,
						CASE WHEN r.require_notes_on_rejected_returned THEN 1 ELSE 0 END AS require_notes_on_rejected_returned
					FROM
						routes r
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN( ' . sqlIntImplode( $arrintRouteIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>