<?php

class CReviewResponseTemplate extends CBaseReviewResponseTemplate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubject( $objDatabase = NULL, $intReviewResponseTemplateId = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getSubject() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', 'Name is required.' ) );
		} else {
			$objPreExistingReviewResponseTemplate = CReviewResponseTemplates::fetchPreExistingReviewResponseTemplateBySubject( $this->getSubject(), $objDatabase, $intReviewResponseTemplateId );

			if( true == valObj( $objPreExistingReviewResponseTemplate, 'CReviewResponseTemplate' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', 'Name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valResponse() {
		$boolIsValid = true;
		if( false == valStr( $this->getResponse() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'response', 'Response is required.' ) );
		}
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intReviewResponseTemplateId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSubject( $objDatabase, $intReviewResponseTemplateId );
				$boolIsValid &= $this->valResponse();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>