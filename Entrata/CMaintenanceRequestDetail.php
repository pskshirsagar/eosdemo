<?php

class CMaintenanceRequestDetail extends CBaseMaintenanceRequestDetail {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMainPhoneNumberTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAltPhoneNumberTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMainMessageOperatorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAltMessageOperatorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProblemDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAdditionalInfo() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLocationSpecifics() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPermissionToEnter() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAlarmCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestedDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledStartDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEndDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActualStartDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompletedDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEstimatedHours() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActualHours() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCostPerHour() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPartsDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPartsCost() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEquipmentDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEquipmentCost() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAdditionalCost() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileName() {
    	$boolIsValid = true;
	   	return $boolIsValid;
    }

    public function valFilePath() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valCustomerNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMainPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAltPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSmsConfirmedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsBillable() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valMaintenanceExceptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequestedAm() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setCustomerNameFirstEncrypted( $strCustomerNameFirstEncrypted ) {
    	$this->m_strCustomerNameFirstEncrypted = $strCustomerNameFirstEncrypted;
    }

    public function setCustomerNameLastEncrypted( $strCustomerNameLastEncrypted ) {
    	$this->m_strCustomerNameLastEncrypted = $strCustomerNameLastEncrypted;
    }

    public function setMainPhoneNumberEncrypted( $strMainPhoneNumberEncrypted ) {
    	$this->m_strMainPhoneNumberEncrypted = $strMainPhoneNumberEncrypted;
    }

    public function setAltPhoneNumberEncrypted( $strAltPhoneNumberEncrypted ) {
    	$this->m_strAltPhoneNumberEncrypted = $strAltPhoneNumberEncrypted;
    }

    public function setCustomerNameFirst( $strCustomerNameFirst, $strLocaleCode = NULL ) {
	    $this->setTranslated( 'm_strCustomerNameFirst', CStrings::strTrimDef( $strCustomerNameFirst, 50, NULL, true ), $strLocaleCode );
	    if( true == valStr( $this->m_strCustomerNameFirst ) ) {
		    $this->setCustomerNameFirstEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strCustomerNameFirst, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
	    }
    }

    public function setCustomerNameLast( $strCustomerNameLast, $strLocaleCode = NULL ) {
	    $this->setTranslated( 'm_strCustomerNameLast', CStrings::strTrimDef( $strCustomerNameLast, 50, NULL, true ), $strLocaleCode );
	    if( true == valStr( $this->m_strCustomerNameLast ) ) {
		    $this->setCustomerNameLastEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strCustomerNameLast, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
	    }
    }

    public function setMainPhoneNumber( $strMainPhoneNumber ) {
    	$this->m_strMainPhoneNumber = CStrings::strTrimDef( $strMainPhoneNumber, 30, NULL, true );
	    if( true == valStr( $this->m_strMainPhoneNumber ) ) {
		    $this->setMainPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strMainPhoneNumber, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
	    }
    }

    public function setAltPhoneNumber( $strAltPhoneNumber ) {
    	$this->m_strAltPhoneNumber = CStrings::strTrimDef( $strAltPhoneNumber, 30, NULL, true );
	    if( true == valStr( $this->m_strAltPhoneNumber ) ) {
		    $this->setAltPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strAltPhoneNumber, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
	    }
    }

}
?>