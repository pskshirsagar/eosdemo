<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionResponseTypes
 * Do not add any new functions to this class.
 */

class CTransmissionResponseTypes extends CBaseTransmissionResponseTypes {

	public static function fetchTransmissionResponseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransmissionResponseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransmissionResponseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransmissionResponseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllTransmissionResponseTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM transmission_response_types';

		return self::fetchTransmissionResponseTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedTransmissionResponseTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						transmission_response_types
					WHERE
						is_published = 1';

		return self::fetchTransmissionResponseTypes( $strSql, $objDatabase );
	}
}
?>