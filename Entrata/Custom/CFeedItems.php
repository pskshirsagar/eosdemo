<?php

class CFeedItems {

	use \Psi\Libraries\Container\TContainerized;

	public function fetchFeedItemsByPropertyIdsByCustomerIdByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intPage = 0, $intLimit = 100 ) {
		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintPropertyIds ) || false == \Psi\Libraries\UtilFunctions\valId( $intCustomerId ) || false == \Psi\Libraries\UtilFunctions\valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						feed_items.*
					FROM (
							SELECT
								\'Event\' AS type,
								ce.id as id,
								row_to_json( row(
									util_get_translated( \'name\', ce.name, ce.details ),
									util_get_translated( \'description\', ce.description, ce.details ),
									CASE
										WHEN ce.date_start IS NOT NULL
											THEN ce.date_start::TEXT
									END,
									CASE
										WHEN ce.date_end IS NOT NULL
											THEN ce.date_end::TEXT
									END,
									ce.company_media_file_id,
									CASE
										WHEN ce.location IS NOT NULL
											THEN util_get_translated( \'location\', ce.location, ce.details )
										WHEN ( ce.street_line1 IS NOT NULL AND ce.city IS NOT NULL )
											THEN util_get_translated( \'street_line1\', ce.street_line1, ce.details ) || \', \' || util_get_translated( \'city\', ce.city, ce.details )
										WHEN ce.street_line1 IS NOT NULL
											THEN util_get_translated( \'street_line1\', ce.street_line1, ce.details )
										WHEN ce.city IS NOT NULL
											THEN util_get_translated( \'city\', ce.city, ce.details )
									END,
									util_get_translated( \'driving_directions\', ce.driving_directions, ce.details ),
									CASE
										WHEN pp2.id IS NOT NULL
											THEN 0
										WHEN ce.customer_id IS NULL
											THEN COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id )
										ELSE COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) + 1
									END,
									CASE
										WHEN EXISTS (
											SELECT id FROM customer_events cce WHERE cce.company_event_id = ce.id AND cce.cid = ce.cid AND cce.customer_id = ' . ( int ) $intCustomerId . '
										) THEN true
										ELSE false
									END,
									ce.customer_id,
									CASE
									  WHEN ce.is_published::integer = 1
										 THEN TRUE
									  ELSE FALSE
									END,
									CASE
									  WHEN ( ce.is_published::integer = 0 AND ce.deleted_by IS NULL AND ce.deleted_on IS NULL )
										 THEN TRUE
									  ELSE FALSE
									END,
									ce.deleted_on,
									ce.time_start::TEXT,
									ce.time_end::TEXT
								 ) )::TEXT AS details,
								 ce.created_on AS created_on,
								 ce.date_start::timestamptz  AS date_start
							FROM
								company_events AS ce
								JOIN property_events AS pe ON( pe.company_event_id = ce.id AND pe.cid = ce.cid )
								LEFT JOIN property_preferences pp ON ( pp.property_id = pe.property_id AND pp.cid = pe.cid AND pp.key = \'ALLOW_RESIDENT_TO_ADD_EVENT\' )
								LEFT JOIN property_preferences pp1 ON ( pp1.property_id = pe.property_id AND pp1.cid = pe.cid AND pp1.key = \'TURN_OFF_EVENTS\' )
								LEFT JOIN property_preferences pp2 ON ( pp2.property_id = pe.property_id AND pp2.cid = pe.cid AND pp2.key = \'HIDE_RSVP_NUMBER_EVENTS_RP\' )
								LEFT JOIN (
									SELECT
										DISTINCT ON ( lc.customer_id, compevnt.id ) lc.customer_id as custid,
										compevnt.id as company_event_id,
										compevnt.cid
									FROM
										company_events AS compevnt
										JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
										JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
										JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
									WHERE
										l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
										AND compevnt.cid = ' . ( int ) $intCid . '
								) AS attndcnt ON( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )
							WHERE
								ce.cid = ' . ( int ) $intCid . '
								AND pe.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ce.is_published = 1
								AND ce.created_on >= NOW() - interval \'365 days\'
								AND CASE
										WHEN pp.id IS NULL
											THEN ( ce.customer_id != ' . ( int ) $intCustomerId . ' OR ce.customer_id IS NULL )
										ELSE 1 = 1
									END
								AND pp1.id IS NULL
								AND ce.deleted_by IS NULL
								AND ce.date_start IS NOT NULL
						UNION
							SELECT
								\'Announcement\' AS type,
								a.id as id,
								row_to_json( row(
									util_get_translated( \'title\', a.title, a.details ),
									util_get_translated( \'announcement\', a.announcement, a.details ),
									CASE
										WHEN a.date_start IS NOT NULL
											THEN a.date_start::TEXT
									END,
									CASE
										WHEN a.date_end IS NOT NULL
											THEN a.date_end::TEXT
									END,
									a.company_media_file_id,
									a.is_pinned
								) )::TEXT AS details,
								( CASE 
							        WHEN a.date_start IS NOT NULL 
							        THEN concat( a.date_start, \' \', date_part(\'hour\', a.created_on), \':\',  date_part(\'minute\', a.created_on), \':\',  date_part(\'second\', a.created_on))::timestamptz
							        ELSE
							            a.created_on::timestamp
							        END    
								)::date AS created_on,
								CASE WHEN a.date_start IS NOT NULL THEN a.date_start::timestamptz ELSE a.created_on END AS date_start
							FROM
								announcements a
								JOIN property_announcements pa ON ( pa.announcement_id::integer = a.id::integer AND pa.cid = a.cid )
							WHERE
								a.cid = ' . ( int ) $intCid . '
								AND pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND a.announcement_type_id IN ( ' . CAnnouncementType::PROPERTY . ', ' . CAnnouncementType::COMPANY . ' )
								AND ( ( a.date_start IS NOT NULL AND a.date_start >= NOW() - interval \'365 days\' ) OR a.created_on IS NOT NULL )
								AND a.is_published = 1
								AND CASE
									WHEN ( a.frequency_id IS NULL OR a.frequency_id = ' . CFrequency::DAILY . ' )
										THEN a.date_start <= NOW()::date AND ( a.date_end >= NOW()::date OR a.date_end IS NULL )
									WHEN( a.frequency_id = ' . CFrequency::ONCE . ' )
										THEN a.date_start IS NOT NULL AND NOW()::date = a.date_start
									WHEN( a.frequency_id = ' . CFrequency::WEEKLY . ' )
										THEN date_part( \'dow\', NOW() ) = date_part( \'dow\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
									WHEN(a.frequency_id = ' . CFrequency::MONTHLY . ')
										THEN date_part( \'day\', NOW() ) = date_part( \'day\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
									ELSE TRUE
								END
						UNION
							SELECT
								\'Classified\' AS type,
								cc.id as id,
								row_to_json( row(
									util_get_translated( \'title\', cc.title, cc.details ),
									util_get_translated( \'post\', cc.post, cc.details ),
									util_get_translated( \'name\', ccc.name, cc.details ),
									cc.company_classified_category_id,
									cc.amount,
									cc.customer_id,
									cc.show_on,
									cc.hide_on,
									util_get_translated( \'name_first\', c.name_first, cc.details ) || \' \' || util_get_translated( \'name_last\', c.name_last, cc.details )
								) )::TEXT AS details,
								cc.created_on AS created_on,
								cc.created_on  AS date_start
							FROM
								customer_classifieds cc
								JOIN properties p ON( ( cc.property_id = p.id OR cc.property_id = p.property_id ) AND cc.cid = p.cid )
								LEFT JOIN customers c ON( cc.cid = c.cid AND cc.customer_id = c.id )
								LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'TURN_OFF_CLASSIFIEDS\' )
								JOIN property_products prop_prod ON(
									p.cid = prop_prod.cid
									AND prop_prod.cid = ' . ( int ) $intCid . '
									AND prop_prod.ps_product_id = ' . CPsProduct::RESIDENT_PORTAL . '
									AND ( prop_prod.property_id IS NULL OR prop_prod.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
								)
								JOIN company_classified_categories ccc ON ( cc.company_classified_category_id = ccc.id AND cc.cid = ccc.cid  )
							WHERE
								p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND cc.cid = ' . ( int ) $intCid . '
								AND cc.is_published = 1
								AND ( cc.hide_on IS NULL OR cc.hide_on > NOW() )
								AND cc.approved_by IS NOT NULL 
								AND ( cc.customer_id != ' . ( int ) $intCustomerId . ' OR cc.customer_id IS NULL )
								AND pp.id IS NULL
								AND cc.deleted_by IS NULL
								AND cc.created_on >= NOW() - interval \'365 days\'
							GROUP BY
								cc.id,
								cc.post,
								cc.title,
								cc.amount,
								cc.customer_id,
								cc.property_id,
								cc.cid,
								cc.company_classified_category_id,
								cc.created_on,
								c.name_first,
								c.name_last,
								ccc.name
						) as feed_items
					ORDER BY
						to_char( feed_items.date_start, \'YYYY-MM-DD\') DESC
					OFFSET ' . ( int ) $intPage . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

}
?>
