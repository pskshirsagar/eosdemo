<?php

class CAmenityLeaseAssociations extends CLeaseAssociations {

	public static function fetchAmenityLeaseAssociationsByLeaseIdByLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase, $boolIsFetchDeleted = true, $arrintArCascadeIds = [], $arrintArCascadeReferenceIds = [] ) {

		if( true == is_null( $intLeaseId ) || false == is_numeric( $intLeaseId ) ) 	return NULL;
		if( true == is_null( $intLeaseIntervalId ) || false == is_numeric( $intLeaseIntervalId ) ) 	return NULL;

		$strSql = '	SELECT
						la.*,
						a.name as amenity_name
					FROM
						lease_associations la
						JOIN amenities a ON ( a.cid = la.cid AND a.id = la.ar_origin_reference_id )
					WHERE
						la.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintArCascadeIds ) && true == valArr( $arrintArCascadeReferenceIds ) ) {
			$strSql .= ' AND la.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
						 AND la.ar_cascade_reference_id IN ( ' . implode( ',', $arrintArCascadeReferenceIds ) . ' ) ';
		}

		if( false == $boolIsFetchDeleted ) {
			$strSql .= ' AND la.deleted_by IS NULL';
		}

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityLeaseAssociationsByPropertyIdByArCascadeIdByApartmentAmenityIdsByCid( $intPropertyId, $intArCascadeId, $arrintAmenityIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintAmenityIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						lease_associations
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ar_cascade_id= ' . ( int ) $intArCascadeId . '
						AND cid = ' . ( int ) $intCid . '
						AND ar_origin_reference_id IN ( ' . implode( ',', $arrintAmenityIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityLeaseAssociationsByPropertyIdByArCascadeIdByLeaseIdsByCid( $intPropertyId, $intArCascadeId, $arrintLeaseIds, $intCid, $objDatabase, $arrintArCascadeReferenceIds = NULL ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strWhere = ( true == valArr( $arrintArCascadeReferenceIds ) ) ? ' AND la.ar_cascade_reference_id IN ( ' . implode( ', ', $arrintArCascadeReferenceIds ) . ' ) ' : '';
		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
						JOIN cached_leases cl ON ( cl.cid = la.cid AND cl.property_id = la.property_id AND cl.active_lease_interval_id = la.lease_interval_id )
					WHERE
						la.property_id = ' . ( int ) $intPropertyId . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND la.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND la.deleted_by IS NULL
						AND la.deleted_on IS NULL ' . $strWhere;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

}
?>