<?php

use Psi\Eos\Entrata\CReportSchedules;

class CReportScheduledTask extends CScheduledTask {

	const TASK_DETAIL_REPORT_SCHEDULE_ID = 'report_schedule_id';

	/** @var DateTime */
	private $m_objToday;

	private $m_boolVerbose				= false;

	protected $m_intCompanyUserId;

	protected $m_arrstrLogDetails		= [];
	protected $m_arrstrErrorLogDetails	= [];
	private $m_boolSkipDelivery			= false;
	private $m_arrmixRecipients			= NULL;

	// region Task Execution Methods

	private function initDependencyContainer( $objDatabase ) {

		$objReportSchedule = $this->fetchReportSchedule( $objDatabase );

		$this->getDependencyContainer()['intCid'] = $this->getCid();
		$this->getDependencyContainer()['intCompanyUser'] = $this->m_intCompanyUserId;
		$this->getDependencyContainer()['objDatabase'] = $objDatabase;
		$this->getDependencyContainer()['objReportSchedule'] = $objReportSchedule;
		$this->getDependencyContainer()['debug_logger'] = function() {
			return new \Psi\Libraries\Logger\CNullLogger();
		};
		$this->getDependencyContainer()['document_manager'] = function( $objContainer ) {
			return CDocumentManagerFactory::createDocumentManager( CDocumentManagerFactory::DOCUMENT_MANAGER_SECURE_FILE_SYSTEM,
				[ $objContainer['intCid'], $objContainer['intCompanyUser'], $objContainer['objDatabase'], NULL, $objContainer['debug_logger'] ] );
		};
	}

	public function execute( $intCompanyUserId, $objDatabase, $objApplication = NULL, $objEvent = NULL, $boolVerbose = false, $strToday = NULL, $boolIsEventTrigger = false, $intPropertyId = NULL ) {
		unset( $objApplication );
		unset( $objEvent );

		$this->setCompanyUserId( $intCompanyUserId );

		// TODO: inject this from the app layer, init in separate method
		$objDependencyContainer = new Pimple\Container();
		$this->setDependencyContainer( $objDependencyContainer );
		$this->initDependencyContainer( $objDatabase );

		// Check which report system is in use
		$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'USE_NEW_REPORTING', $this->getCid(), $this->getDatabase() );
		$boolIsSchedule = $this->doesScheduleMatchReportSystem( valObj( $objCompanyPreference, 'CCompanyPreference' ) ? $objCompanyPreference->getValue() : 0 );
		if( false == $boolIsSchedule ) {
			$this->log( 'Skipping report schedule ' . $this->getReportSchedule()->getId() . ' because it does not match the currently selected report system.' );
			return $this->getLogDetails();
		}

		// shows why we skipped the scheduled task.
		$objClient = CClients::fetchClientById( $this->getCid(), $this->getDatabase() );
		$arrintCompanyStatusTypesToSkip = [ CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::TERMINATED ];
		if( in_array( $objClient->getCompanyStatusTypeId(), $arrintCompanyStatusTypesToSkip, true ) ) {
			$this->log( ' Skipping report scheduled task ' . $this->getId() . ' because the client status is "' . CCompanyStatusType::C_ARRSTRGACOMPANYSTATUSTYPES[$objClient->getCompanyStatusTypeId()] . '"' );
			return $this->getLogDetails();
		}

		// Set up an error handler that will convert all errors to exceptions, so that we can catch them without aborting the scheduled task script
		$mixOldErrorHandler = set_error_handler( [ $this, 'handleError' ] );

		try {
			$this->m_objToday = new DateTime( $strToday );

			$this->m_boolVerbose = $boolVerbose;

			if( !$this->checkValidScheduleObject() ) {
				// This schedule is corrupted, and cannot run as is
				$this->log( ' Unable to fetch report_schedule. cid: ' . $this->getCid() . ', scheduled_task_id: ' . $this->getId() );

				// We could end these schedules, but I'd rather have them continue to show in the logs until we figure out what's going on [dmerriman, 10/3/2018]
				// $this->setEndOn( date( 'Y-m-d H:i:s' ) );
				// $this->update( $this->getCompanyUserId(), $this->getDatabase() );
				return $this->getLogDetails();
			}

			$this->getReportSchedule()->setSendEmail( true )->setIsTemporary( false );

			if( !$this->isScheduleActive() ) {
				$this->log( ' Report schedule has been deleted; done.' );

				// If the schedule has been deleted, we should end the scheduled task so that we can ignore it in subsequent runs
				$this->setEndOn( date( 'Y-m-d H:i:s' ) );
				$this->update( $this->getCompanyUserId(), $this->getDatabase() );
				return $this->getLogDetails();
			}

			if( $this->isScheduleEventTriggered() && !$boolIsEventTrigger ) {
				$this->log( ' Skipping event-driven report schedule; done.' );
				return $this->getLogDetails();
			}

			$this->processSchedule( $intCompanyUserId, $objDatabase, $boolVerbose = false, $strToday = NULL, $boolIsEventTrigger, $intPropertyId );

		} catch( Exception $objException ) {
			$this->log( ' ***ERROR*** An error occurred while running the schedule:' );
			$this->log( 'In CReportScheduledTask:: ERROR: ' . $objException->getMessage() );
			$this->prettyPrintException( $objException );

			// If an error occurred at this point, something went wrong with the scheduler itself, not just a particular report
			// By marking the execution failed, the next run on date will not be updated, and the report schedule will be picked up in the next batch to run again
			$this->setExecutionFailed( true );

			$this->m_arrstrErrorLogDetails[] = [
				'error' => $objException
			];
		} finally {
			set_error_handler( $mixOldErrorHandler );
		}

		return $this->getLogDetails();
	}

	public function checkValidScheduleObject() : bool {
		return valObj( $this->getReportSchedule(), 'CReportSchedule' );
	}

	public function isScheduleActive() : bool {
		return $this->getReportSchedule()->getIsActive();
	}

	public function isScheduleEventTriggered() : bool {
		return CScheduleType::EVENT_TRIGGER == $this->getScheduleTypeId();
	}

	public function updateLockedSchedule( $objLockedSchedule, $arrstrCorrelationIds, $intCompanyUserId ) {
		$objLockedSchedule->cleanUpRuns();
		$objLockedSchedule->beginReportScheduleRun( $arrstrCorrelationIds );
		$objLockedSchedule->update( $intCompanyUserId, $this->getDatabase() );
	}

	public function processSchedule( $intCompanyUserId, $objDatabase, $boolVerbose = false, $strToday = NULL, $boolIsEventTrigger = false, $intPropertyId = NULL ) {
		// Beginning a transaction and locking the schedule to prevent any messages which finish before other
		// messages are sent from updating the details field until all messages have been sent and all correlation
		// ids have been added. This is an unlikely event, but it is extremely important that we don't let it happen.

		$strLastRunOn = $this->getLastRunOn();
		$strNextRunOn = $this->getNextRunOn();

		$this->getDatabase()->begin();

		$objLockedSchedule = CReportSchedules::createService()->fetchReportScheduleForUpdate( $this->getCid(), $this->getReportSchedule()->getId(), $this->getDatabase() );
		$this->getReportSchedule()->setReportInstances( $this->getReportSchedule()->getOrFetchReportInstances( $this->getDatabase() ) );

		$strRunKey = $objLockedSchedule->generateRunKey();
		$intMessageCount = 0;
		$arrstrCorrelationIds = [];
		$arrmixRecipientGroups = $this->fetchRecipientGroups( $objDatabase );

		// To minimize the size of the report message, we are setting report schedule's details field as NULL. After sending message, we are setting details field to its previous value
		$arrmixScheduleDetails = json_decode( json_encode( $this->getReportSchedule()->getDetails() ), true );
		$arrmixFilterOverrides = $this->getReportSchedule()->getFilterOverrides();
		$this->getReportSchedule()->setDetails( json_encode( '' ) );

		foreach( $arrmixRecipientGroups as $arrmixRecipientGroup ) {
			// TODO: Figure out why this needs to be inside the loop; it fails to send more than one message when it's outside
			$objReportsExchange = new CReportsExchange();
			$objReportsMessage = new CReportsMessage();
			$objReportsMessage
				->setCid( $this->getCid() )
				->setCompanyUserId( $this->getReportSchedule()->getCompanyUserId() )
				->setClusterName( CCluster::getClusterNameByClusterId( $this->getDatabase()->getClusterId() ) )
				->setReportSchedule( $this->getReportSchedule() )
				->setFilterOverrides( $arrmixFilterOverrides )
				->setToday( $strToday )
				->setVerbose( $boolVerbose )
				->setDatabaseId( $this->getDatabase()->getId() )
				->setStatusTtl( 14400 )// 4 hours
				->setContext( CReportsLibrary::CONTEXT_SCHEDULED )
				->setRecipients( $arrmixRecipientGroup )
				->setRunKey( $strRunKey )
				->setIsCacheDatasetData( true )
				->setMessageProperties( [ 'correlation_id' => $objReportsMessage->getCorrelationId() ] );

			if( $boolIsEventTrigger ) {
				$objReportsMessage
					->setFilterRestrictions( [
						'property_group_ids' => [ $intPropertyId ]
					] );
			} else {
				$objReportsMessage
					->setTaskLastRunOn( $strLastRunOn )
					->setTaskNextRunOn( $strNextRunOn );
			}

			$arrstrCorrelationIds[] = $objReportsMessage->getCorrelationId();
			$objReportsExchange->send( $objReportsMessage, [], $objReportsMessage->getRoutingKey() );
			$objReportsExchange->close();
			$this->log( ' Successfully sent report message for cid: ' . $this->getReportSchedule()->getCid() . ', report schedule id: ' . $this->getReportSchedule()->getId() . ', correlation id: ' . $objReportsMessage->getCorrelationId() . ', recipients: ' . json_encode( $arrmixRecipientGroup ) . PHP_EOL );
			$intMessageCount++;
		}

		$this->getReportSchedule()->setDetails( json_encode( $arrmixScheduleDetails ) );
		$this->updateLockedSchedule( $objLockedSchedule, $arrstrCorrelationIds, $intCompanyUserId );

		$this->getDatabase()->commit();

		$this->log( ' Sent ' . ( int ) $intMessageCount . ' total messages.' );
	}

	protected function doesScheduleMatchReportSystem( $intReportingCompanyPreference ) {
		// Run all schedules where New group id, new instance id, old group id and filter id are NULL.
		if( valObj( $this->getReportSchedule(), 'CReportSchedule' ) ) {
			if( NULL == $this->getReportSchedule()->getReportGroupId() && NULL == $this->getReportSchedule()->getReportFilterId() && NULL == $this->getReportSchedule()->getReportNewGroupId() && NULL == $this->getReportSchedule()->getReportNewInstanceId() ) {
				return true;
			}

			if( 0 == ( int ) ( $intReportingCompanyPreference ) ) {
				if( NULL == $this->getReportSchedule()->getReportGroupId() && NULL == $this->getReportSchedule()->getReportFilterId() ) {
					return false;
				}
			} else if( NULL == $this->getReportSchedule()->getReportNewGroupId() && NULL == $this->getReportSchedule()->getReportNewInstanceId() ) {
				return false;
			}
		}

		return true;
	}

	// endregion

	// region CScheduledTask Overrides

	// @TODO Move to CScheduledTask

	public function parseScheduleDetails() {
		$this->getScheduleType();
	}

	public function parseTaskDetails() {
	}

	// @TODO Move to CScheduledTask

	public function composeScheduleDetails() {
		$this->setScheduleDetails( $this->getScheduleType()->composeDetails() );
	}

	// @TODO Move to CScheduledTask

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	// endregion

	// region Database Wrapper Methods

	/**
	 * @param $objDatabase
	 * @return CReportSchedule
	 */

	public function fetchReportSchedule( $objDatabase ) {
		$objReportSchedule = CReportSchedules::createService()->fetchReportSchedulesByScheduledTaskIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objReportSchedule, 'CReportSchedule' ) ) {
			$this->setReportScheduleId( $objReportSchedule->getId() );
		}

		return $objReportSchedule;
	}

	public function fetchRecipientGroups( $objDatabase ) {
		$arrmixRecipientGroups = $this->getReportSchedule()->fetchRecipientsByUniquePropertyGroups( $objDatabase );

		if( valArr( $this->getRecipients() ) ) {
			$arrmixNewRecipientGroups = [];
			foreach( $this->getRecipients() as $arrmixRecipientSpec ) {
				foreach( $arrmixRecipientGroups as $strGroupKey => $arrmixRecipientGroup ) {
					foreach( $arrmixRecipientGroup as $strRecipientKey => $arrmixRecipient ) {
						if( $this->matchRecipient( $arrmixRecipient, $arrmixRecipientSpec ) ) {
							$arrmixNewRecipientGroups[$strGroupKey][$strRecipientKey] = $arrmixRecipient;
						}
					}
				}
			}
			$arrmixRecipientGroups = $arrmixNewRecipientGroups;
		}

		if( $this->getSkipDelivery() ) {
			foreach( $arrmixRecipientGroups as $strGroupKey => $arrmixRecipientGroup ) {
				foreach( $arrmixRecipientGroup as $strRecipientKey => $arrmixRecipient ) {
					// Modify the company user id to be negative so that report histories are not saved
					if( isset( $arrmixRecipient['company_user_id'] ) ) {
						$arrmixRecipientGroups[$strGroupKey][$strRecipientKey]['company_user_id'] = - abs( $arrmixRecipient['company_user_id'] );
					}

					// Modify the email address so that emails are not sent
					if( isset( $arrmixRecipient['email_address'] ) ) {
						$arrmixRecipientGroups[$strGroupKey][$strRecipientKey]['email_address'] = $arrmixRecipient['email_address'] . '.lcl';
					}
				}
			}
		}

		return $arrmixRecipientGroups;
	}

	protected function matchRecipient( $arrmixRecipient, $arrmixRecipientSpec ) {
		$arrmixTest = array_intersect_key( $arrmixRecipient, $arrmixRecipientSpec );
		return $arrmixTest == $arrmixRecipientSpec;
	}

	// endregion

	// region Logging and Error Handling

	public function handleError( $intErrNo, $strErrStr, $strErrFile, $strErrLine ) {

		if( false == ( error_reporting() & $intErrNo ) ) {
			// The server is set to ignore this type of error
			return false;
		}

		switch( $intErrNo ) {

			case E_ERROR:
			case E_WARNING:
			case E_PARSE:
			case E_CORE_ERROR:
			case E_CORE_WARNING:
			case E_COMPILE_ERROR:
			case E_COMPILE_WARNING:
			case E_USER_ERROR:
			case E_USER_WARNING:
			case E_RECOVERABLE_ERROR:
				// Script-halting errors will generate exceptions, so we can catch them and continue the script
				throw new ErrorException( $strErrStr, 0, $intErrNo, $strErrFile, $strErrLine );

			case E_NOTICE:
			case E_USER_NOTICE:
			case E_STRICT:
			case E_DEPRECATED:
			case E_USER_DEPRECATED:
			default:
				// Log all non-serious errors, just so we can see what's happening, but otherwise ignore them
				$this->log( ' ***NOTICE*** ' . ( int ) $intErrNo . ' ' . $strErrStr . ' ' . $strErrFile . ' ' . $strErrLine );
				return true;
		}
	}

	protected function prettyPrintException( Exception $objException ) {
		$this->log( '   Error in {' . $objException->getFile() . '}({' . $objException->getLine() . '})' );
		$this->log( '   Message: {' . $objException->getMessage() . '}' );
		$this->log( '   Stack trace:' );

		foreach( $objException->getTrace() as $intIndex => $arrmixStackFrame ) {
			$strFile = ( true == isset( $arrmixStackFrame['file'] ) ? $arrmixStackFrame['file'] : '' );
			$strLine = ( true == isset( $arrmixStackFrame['line'] ) ? '(' . $arrmixStackFrame['line'] . ')' : '' );
			$strClassAndFunction = implode( '::', array_filter( [ isset( $arrmixStackFrame['class'] ) ? $arrmixStackFrame['class'] : '', $arrmixStackFrame['function'] ] ) );
			$this->log( '     #' . $intIndex . ' ' . $strFile . ' ' . $strLine . ' ' . $strClassAndFunction );
		}
	}

	protected function log( $strMessage ) {
		if( true == $this->m_boolVerbose ) {
			echo "\n" . $strMessage;
		}
		$this->m_arrstrLogDetails[] = $strMessage;
	}

	// endregion

	// region Getters

	public function getReportScheduleId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'report_schedule_id' );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getLogDetails() {
		return $this->m_arrstrLogDetails;
	}

	public function getErrorLogDetails() {
		return $this->m_arrstrErrorLogDetails;
	}

	public function getSkipDelivery() {
		return $this->m_boolSkipDelivery;
	}

	public function getRecipients() {
		return $this->m_arrmixRecipients;
	}

	/**
	 * @return CDatabase
	 */

	public function getDatabase() {
		return $this->getDependencyContainer()['objDatabase'];
	}

	/**
	 * @return CReportSchedule
	 */

	public function getReportSchedule() {
		return $this->getDependencyContainer()['objReportSchedule'];
	}

	// endregion

	// region Setters

	public function setReportScheduleId( $intReportScheduleId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'report_schedule_id', $intReportScheduleId );
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = ( int ) $intCompanyUserId;
	}

	public function setSkipDelivery( $boolSkipDelivery ) {
		$this->m_boolSkipDelivery = $boolSkipDelivery;
		return $this;
	}

	public function setRecipients( $arrmixRecipients ) {
		$this->m_arrmixRecipients = $arrmixRecipients;
		return $this;
	}

	// endregion

}
