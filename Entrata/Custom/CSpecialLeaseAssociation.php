<?php

class CSpecialLeaseAssociation extends CLeaseAssociation {

	protected $m_intSpecialTypeId;

	protected $m_strSpecialName;
	protected $m_strSpecialDescription;

	/**
	 * Get Functions
	 */

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	public function getSpecialName() {
		return $this->m_strSpecialName;
	}

	public function getSpecialDescription() {
		return $this->m_strSpecialDescription;
	}

	/**
	 * Set functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['special_type_id'] ) && true == $boolDirectSet ) {
			$this->m_intSpecialTypeId = trim( $arrmixValues['special_type_id'] );
		} elseif( true == isset( $arrmixValues['special_type_id'] ) ) {
			$this->setSpecialTypeId( $arrmixValues['special_type_id'] );
		}

		if( true == isset( $arrmixValues['special_name'] ) && true == $boolDirectSet ) {
			$this->m_strSpecialName = trim( $arrmixValues['special_name'] );
		} elseif( true == isset( $arrmixValues['special_name'] ) ) {
			$this->setSpecialName( $arrmixValues['special_name'] );
		}

		if( true == isset( $arrmixValues['special_description'] ) && true == $boolDirectSet ) {
			$this->m_strSpecialDescription = trim( $arrmixValues['special_description'] );
		} elseif( true == isset( $arrmixValues['special_description'] ) ) {
			$this->setSpecialDescription( $arrmixValues['special_description'] );
		}
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		$this->m_intSpecialTypeId = $intSpecialTypeId;
	}

	public function setSpecialName( $strSpecialName ) {
		$this->m_strSpecialName = $strSpecialName;
	}

	public function setSpecialDescription( $strSpecialDescription ) {
		$this->m_strSpecialDescription = $strSpecialDescription;
	}

	/**
	 * Create functions
	 */

}
?>