<?php

class CApplicationScheduledTask extends CScheduledTask {

	protected $m_intNumDays;
	protected $m_intIntentToApplyWithinMonths;
	protected $m_intPsProductId;
	protected $m_intComposeEmailId;
	protected $m_intSystemEmailTypeId;
	protected $m_intBccEmailAddressCount;
	protected $m_intDependantOn;
	protected $m_intListTypeId;
	protected $m_intListItemId;
	protected $m_intOverdueBusinessHours;
	protected $m_intReminderBusinessHours;
	protected $m_intIsConsiderNotProgressing;
	protected $m_intMappingEventTypeId;
	protected $m_intMappingEventSubTypeId;
	protected $m_intMappingEventResultId;
	protected $m_intMappingNewStatusId;
	protected $m_intMappingNewStageId;
	protected $m_intSystemMessageTemplateIdCount;
	protected $m_intNumDaysCount;
	protected $m_intListItemIdCount;
	protected $m_intIsEnableEmailCount;
	protected $m_intIsEnableManualContactCount;
	protected $m_intManualContactInstructionsCount;

	protected $m_intSubjectCount;
	protected $m_intOverdueBusinessHoursCount;
	protected $m_intReminderBusinessHoursCount;
	protected $m_intListTypeIdCount;
	protected $m_intAttachmentCount;

	protected $m_strEmailSubject;
	protected $m_strCcEmailAddress;
	protected $m_strApplicantType;
	protected $m_strEventTriggerType;
	protected $m_strSmsMessage;
	protected $m_strScheduledTaskIds;
	protected $m_strComposeEmailIds;
	protected $m_strSystemMessageKey;
	protected $m_strSmsSystemMessageKey;

	protected $m_boolIsDeleted;
	protected $m_boolIsVerbose;
	protected $m_boolIsEnableEmail;
	protected $m_boolIsEnableSubject;
	protected $m_boolIsEnableManualContact;
	protected $m_boolIsEnableManualContactAfterNDays;
	protected $m_boolIsEnableSms;
	protected $m_boolIsEntrataDefault;

	protected $m_objProperty;
	protected $m_objEmailDatabase;
	protected $m_objPaymentDatabase;

	protected $m_arrobjApplications;
	protected $m_arrobjScheduledTasks;

	protected $m_arrintPsProductIds;
	protected $m_arrintMappingPsProductIds;
	protected $m_arrintApplicationIds;

	protected $m_arrmixNewLeadEventProductsMapping;

	const APPLICANT_TYPE_PRIMARY 					= 'primary';
	const APPLICANT_TYPE_CO_APPLICANTS 				= 'co_applicants';
	const APPLICANT_TYPE_ALL_APPLICANTS 			= 'all_applicants';
	const APPLICANT_TYPE_PENDING_CO_APPLICANTS 		= 'pending_co_applicants';
	const APPLICANT_TYPE_ALL_SIGNERS 				= 'all_signers';
	const APPLICANT_TYPE_PENDING_SIGNERS 			= 'pending_signers';

	const EVENT_TRIGGER_TYPE_IMMEDIATE 				= 'immediate';
	const EVENT_TRIGGER_TYPE_AFTER 					= 'after';
	const EVENT_TRIGGER_TYPE_BEFORE 				= 'before';
	const EVENT_TRIGGER_TYPE_DAY_OF_EVENT 			= 'day_of_event';

	const NUM_DAYS 									= 'num_days';
	const INTENT_TO_APPLY_WITHIN_MONTHS 			= 'intent_to_apply_within_months';
	const DEPENDANT_ON								= 'dependant_on';
	const LAST_SCHEDULED_EVENT						= 'last_scheduled_event';
	const PS_PRODUCT_ID 							= 'ps_product_id';
	const MAPPING_PS_PRODUCT_IDS 					= 'ps_product_ids';
	const IS_ENABLE_MANUAL_CONTACT 					= 'is_enable_manual_contact';
	const IS_ENABLE_MANUAL_CONTACT_AFTER_N_DAYS 	= 'is_enable_manual_contact_after_n_days';
	const IS_CANCEL_LEAD 							= 'is_cancel_lead';
	const APPLICANT_TYPE 							= 'applicant_type';
	const IS_ENABLE_EMAIL 							= 'is_enable_email';
	const IS_ENTRATA_DEFAULT						= 'is_entrata_default';
	const IS_ENABLE_SUBJECT 						= 'is_enable_subject';
	const COMPOSE_EMAIL_ID 							= 'compose_email_id';
	const EMAIL_SUBJECT 							= 'email_subject';
	const EVENT_TRIGGER_TYPE 						= 'event_trigger_type';
	const MANUAL_CONTACT_INSTRUCTIONS 				= 'manual_contact_instructions';
	const IS_ENABLE_SMS 							= 'is_enable_sms';
	const IS_SEND_EMAIL_TO_PROPERTY 				= 'is_send_email_to_property';

	const SMS_MESSAGE 								= 'sms_message';
	const OVERDUE_BUSINESS_HOURS 					= 'overdue_business_hours';
	const REMINDER_BUSINESS_HOURS 					= 'reminder_business_hours';
	const IS_CONSIDER_NOT_PROGRESSING 				= 'is_consider_not_progressing';
	const LIST_ITEM_ID 							    = 'list_item_id';
	const LIST_TYPE_ID 								= 'list_type_id';
	const MAPPING_EVENT_TYPE_ID 					= 'mapping_event_type_id';
	const MAPPING_EVENT_SUB_TYPE_ID 				= 'mapping_event_sub_type_id';
	const MAPPING_EVENT_RESULT_ID 					= 'mapping_event_result_id';
	const MAPPING_NEW_STATUS_ID 					= 'mapping_new_status_id';
	const MAPPING_NEW_STAGE_ID 						= 'mapping_new_stage_id';
	const SYSTEM_EMAIL_TYPE_ID 						= 'system_email_type_id';
	const SYSTEM_MESSAGE_KEY 						= 'system_message_key';
	const SMS_SYSTEM_MESSAGE_KEY 					= 'sms_system_message_key';

	public static $c_arrstrApplicationApplicantTypes = [
		self::APPLICANT_TYPE_PRIMARY,
		self::APPLICANT_TYPE_CO_APPLICANTS,
		self::APPLICANT_TYPE_PENDING_CO_APPLICANTS,
		self::APPLICANT_TYPE_ALL_APPLICANTS
	];

	public static $c_arrstrLeaseApplicantTypes = [
		self::APPLICANT_TYPE_ALL_SIGNERS,
		self::APPLICANT_TYPE_PENDING_SIGNERS,
		self::APPLICANT_TYPE_PRIMARY
	];

	/**
	 * Get Functions
	 */

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getNumDays() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'num_days' );
	}

	public function getIntentToApplyWithinMonths() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'intent_to_apply_within_months' );
	}

	public function getDependantOn() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'dependant_on' );
	}

	public function getIsEnableManualContact() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact' );
	}

	public function getIsEnableManualContactAfterNDays() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact_after_n_days' );
	}

	public function getPsProductId() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'ps_product_id' );
	}

	public function getApplicantType() {
		return $this->m_strApplicantType;
	}

	public function getIsEnableEmail() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_email' );
	}

	public function getIsEnableSubject() {
		return ( int ) $this->m_boolIsEnableSubject;
	}

	public function getComposeEmailId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'compose_email_id' );
	}

	public function getEventTriggerType() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type' );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getPsProductIds() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'ps_product_ids' ) ?? [];
	}

	public function getLegacyPsProductIds() {
		return $this->m_arrintPsProductIds;
	}

	public function getMappingPsProductIds() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'ps_product_ids' );
	}

	public function getApplicationIds() {
		return $this->m_arrintApplicationIds;
	}

	public function getApplications() {
		return $this->m_arrobjApplications;
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getCcEmailAddress() {
		return $this->m_strCcEmailAddress;
	}

	public function getIsEnableSms() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_sms' );
	}

	public function getSmsMessage() {
		return $this->m_strSmsMessage;
	}

	public function getOverdueBusinessHours() {
		// not typecasted into int as validation isn't working
		return $this->getJsonbFieldValue( 'TaskDetails', 'overdue_business_hours' );
	}

	public function getReminderBusinessHours() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'reminder_business_hours' );
	}

	public function getIsConsiderNotProgressing() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_consider_not_progressing' );
	}

	public function getListItemId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'list_item_id' );
	}

	public function getListTypeId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'list_type_id' );
	}

	public function getMappingEventTypeId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'mapping_event_type_id' );
	}

	public function getMappingEventSubTypeId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'mapping_event_sub_type_id' );
	}

	public function getMappingEventResultId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'mapping_event_result_id' );
	}

	public function getMappingNewStatusId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'mapping_new_status_id' );
	}

	public function getMappingNewStageId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'mapping_new_status_id' );
	}

	public function getSystemEmailTypeId() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'system_email_type_id' );
	}

	public function getScheduledTaskIds() {
		return $this->m_strScheduledTaskIds;
	}

	public function getNumDaysCount() {
		return $this->m_intNumDaysCount;
	}

	public function getListItemIdCount() {
		return $this->m_intListItemIdCount;
	}

	public function getIsEnableEmailCount() {
		return $this->m_intIsEnableEmailCount;
	}

	public function getIsEnableManualContactCount() {
		return $this->m_intIsEnableManualContactCount;
	}

	public function getManualContactInstructionsCount() {
		return $this->m_intManualContactInstructionsCount;
	}

	public function getSubjectCount() {
		return $this->m_intSubjectCount;
	}

	public function getOverdueBusinessHoursCount() {
		return $this->m_intOverdueBusinessHoursCount;
	}

	public function getReminderBusinessHoursCount() {
		return $this->m_intReminderBusinessHoursCount;
	}

	public function getListTypeIdCount() {
		return $this->m_intListTypeIdCount;
	}

	public function getSystemMessageTemplateIdCount() {
		return $this->m_intSystemMessageTemplateIdCount;
	}

	public function getAttachmentCount() {
		return $this->m_intAttachmentCount;
	}

	public function getBccEmailAddressCount() {
		return $this->m_intBccEmailAddressCount;
	}

	public function getComposeEmailIds() {
		return $this->m_strComposeEmailIds;
	}

	public function getSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'system_message_key' );
	}

	public function getSmsSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'sms_system_message_key' );
	}

	public function getIsEntrataDefault() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_entrata_default' );
	}

	/**
	 * Set Functions
	 */

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setNumDays( $intNumDays ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'num_days', $intNumDays );
	}

	public function setIntentToApplyWithinMonths( $intMonths ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'intent_to_apply_within_months', $intMonths );
	}

	public function setDependantOn( $strDependantOn ) {
		$strDependantOn = ( false == is_null( $strDependantOn ) ) ?  $strDependantOn : self::LAST_SCHEDULED_EVENT;
		$this->setJsonbFieldValue( 'ScheduleDetails', 'dependant_on', $strDependantOn );
	}

	public function setIsEnableManualContact( $boolIsEnableManualContact ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact', $boolIsEnableManualContact );
	}

	public function setIsEnableManualContactAfterNDays( $boolIsEnableManualContactAfterNDays ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact_after_n_days', $boolIsEnableManualContactAfterNDays );
	}

	public function setPsProductId( $intPsProductId ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'ps_product_id', $intPsProductId );
	}

	public function setApplicantType( $strApplicantType ) {
		$this->m_strApplicantType = $strApplicantType;
	}

	public function setIsEnableEmail( $boolIsEnableEmail ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_email', $boolIsEnableEmail );
	}

	public function setIsEnableSubject( $boolIsEnableSubject ) {
		$this->m_boolIsEnableSubject = $boolIsEnableSubject;
	}

	public function setComposeEmailId( $intComposeEmailId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'compose_email_id', $intComposeEmailId );
	}

	public function setEventTriggerType( $strEventTriggerType ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type', $strEventTriggerType );
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->m_boolIsDeleted = $boolIsDeleted;
	}

	public function setPsProductIds( $arrintPsProductIds ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'ps_product_ids', $arrintPsProductIds );
	}

	public function setLegacyPsProductIds( $arrintPsProductIds ) {
		$this->m_arrintPsProductIds = $arrintPsProductIds;
	}

	public function setMappingPsProductIds( $arrintMappingPsProductIds ) {
		$this->m_arrintMappingPsProductIds = $arrintMappingPsProductIds;
	}

	public function setApplicationIds( $arrintApplicationIds ) {
		$this->m_arrintApplicationIds = CStrings::strToArrIntDef( $arrintApplicationIds, NULL );
	}

	public function setApplications( $arrobjApplications ) {
		$this->m_arrobjApplications = $arrobjApplications;
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setCcEmailAddress( $strCcEmailAddress ) {
		$this->m_strCcEmailAddress = $strCcEmailAddress;
	}

	public function setIsEnableSms( $boolIsEnableSms ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_sms', $boolIsEnableSms );
	}

	public function setSmsMessage( $strSmsMessage ) {
		$this->m_strSmsMessage = $strSmsMessage;
	}

	public function setOverdueBusinessHours( $intOverdueBusinessHours ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'overdue_business_hours', $intOverdueBusinessHours );
	}

	public function setReminderBusinessHours( $intReminderBusinessHours ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'reminder_business_hours', $intReminderBusinessHours );
	}

	public function setIsConsiderNotProgressing( $intIsConsiderNotProgressing ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_consider_not_progressing', $intIsConsiderNotProgressing );
	}

	public function setListItemId( $intListItemId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'list_item_id', $intListItemId );
	}

	public function setListTypeId( $intListTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'list_type_id', $intListTypeId );
	}

	public function setMappingEventTypeId( $intMappingEventTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'mapping_event_type_id', $intMappingEventTypeId );
	}

	public function setMappingEventSubTypeId( $intMappingEventSubTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'mapping_event_sub_type_id', $intMappingEventSubTypeId );
	}

	public function setMappingEventResultId( $intMappingEventResultId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'mapping_event_result_id', $intMappingEventResultId );
	}

	public function setMappingNewStatusId( $intMappingNewStatusId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'mapping_new_status_id', $intMappingNewStatusId );
	}

	public function setMappingNewStageId( $intMappingNewStageId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'mapping_new_stage_id', $intMappingNewStageId );
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_email_type_id', $intSystemEmailTypeId );
	}

	public function setScheduledTaskIds( $strScheduledTaskIds ) {
		$this->m_strScheduledTaskIds = preg_replace( '/\s+/', '', $strScheduledTaskIds );
	}

	public function setNumDaysCount( $intNumDaysCount ) {
		$this->m_intNumDaysCount = $intNumDaysCount;
	}

	public function setListItemIdCount( $intListItemIdCount ) {
		$this->m_intListItemIdCount = $intListItemIdCount;
	}

	public function setIsEnableEmailCount( $intIsEnableEmailCount ) {
		$this->m_intIsEnableEmailCount = $intIsEnableEmailCount;
	}

	public function setIsEnableManualContactCount( $intIsEnableManualContactCount ) {
		$this->m_intIsEnableManualContactCount = $intIsEnableManualContactCount;
	}

	public function setManualContactInstructionsCount( $intManualContactInstructionsCount ) {
		$this->m_intManualContactInstructionsCount = $intManualContactInstructionsCount;
	}

	public function setSubjectCount( $intSubjectCount ) {
		$this->m_intSubjectCount = $intSubjectCount;
	}

	public function setOverdueBusinessHoursCount( $intOverDueBusinessHoursCount ) {
		$this->m_intOverdueBusinessHoursCount = $intOverDueBusinessHoursCount;
	}

	public function setReminderBusinessHoursCount( $intReminderBusinessHoursCount ) {
		$this->m_intReminderBusinessHoursCount = $intReminderBusinessHoursCount;
	}

	public function setListTypeIdCount( $intListTypeIdCount ) {
		$this->m_intListTypeIdCount = $intListTypeIdCount;
	}

	public function setSystemMessageTemplateIdCount( $intSystemMessageTemplateIdCount ) {
		$this->m_intSystemMessageTemplateIdCount = $intSystemMessageTemplateIdCount;
	}

	public function setAttachmentCount( $intAttachmentCount ) {
		$this->m_intAttachmentCount = $intAttachmentCount;
	}

	public function setComposeEmailIds( $strComposeEmailIds ) {
		$this->m_strComposeEmailIds = preg_replace( '/\s+/', '', $strComposeEmailIds );
	}

	public function setBccEmailAddressCount( $intBccEmailAddressCount ) {
		$this->m_intBccEmailAddressCount = $intBccEmailAddressCount;
	}

	public function setSystemMessageKey( $strSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_message_key', $strSystemMessageKey );
	}

	public function setSmsSystemMessageKey( $strSmsSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'sms_system_message_key', $strSmsSystemMessageKey );
	}

	public function setIsEntrataDefault( $boolIsEntrataDefault ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_entrata_default', $boolIsEntrataDefault );
	}

	/**
	 * Other Functions
	 */

	public function getPaymentDatabase() {
		if( false == valObj( $this->m_objPaymentDatabase, 'CDatabase' ) ) {
			$this->m_objPaymentDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::PAYMENT, CDatabaseUserType::PS_DEVELOPER );
		}

		return $this->m_objPaymentDatabase;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['id'] ) && $boolDirectSet ) {
			$this->m_intId = trim( $arrmixValues['id'] );
		} elseif( isset( $arrmixValues['id'] ) ) {
			$this->setId( trim( $arrmixValues['id'] ) );
		}

		if( isset( $arrmixValues['num_days'] ) && $boolDirectSet ) {
			$this->m_intNumDays = trim( $arrmixValues['num_days'] );
		} elseif( isset( $arrmixValues['num_days'] ) ) {
			$this->setNumDays( $arrmixValues['num_days'] );
		}

		if( isset( $arrmixValues['intent_to_apply_within_months'] ) && $boolDirectSet ) {
			$this->m_intIntentToApplyWithinMonths = trim( $arrmixValues['intent_to_apply_within_months'] );
		} elseif( isset( $arrmixValues['intent_to_apply_within_months'] ) ) {
			$this->setIntentToApplyWithinMonths( $arrmixValues['intent_to_apply_within_months'] );
		}

		if( isset( $arrmixValues['dependant_on'] ) && $boolDirectSet ) {
			$this->m_intDependantOn = trim( $arrmixValues['dependant_on'] );
		} elseif( isset( $arrmixValues['dependant_on'] ) ) {
			$this->setDependantOn( $arrmixValues['dependant_on'] );
		}

		if( isset( $arrmixValues['is_enable_email'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableEmail = trim( $arrmixValues['is_enable_email'] );
		} elseif( isset( $arrmixValues['is_enable_email'] ) ) {
			$this->setIsEnableEmail( $arrmixValues['is_enable_email'] );
		}
		if( isset( $arrmixValues['is_enable_subject'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableEmail = trim( $arrmixValues['is_enable_subject'] );
		} elseif( isset( $arrmixValues['is_enable_subject'] ) ) {
			$this->setIsEnableSubject( $arrmixValues['is_enable_subject'] );
		}
		if( isset( $arrmixValues['compose_email_id'] ) && $boolDirectSet ) {
			$this->m_intComposeEmailId = trim( $arrmixValues['compose_email_id'] );
		} elseif( isset( $arrmixValues['compose_email_id'] ) ) {
			$this->setComposeEmailId( $arrmixValues['compose_email_id'] );
		}
		if( isset( $arrmixValues['email_subject'] ) && $boolDirectSet ) {
			$this->m_strEmailSubject = trim( $arrmixValues['email_subject'] );
		} elseif( isset( $arrmixValues['email_subject'] ) ) {
			$this->setEmailSubject( $arrmixValues['email_subject'] );
		}
		if( isset( $arrmixValues['cc_email_address'] ) && $boolDirectSet ) {
			$this->m_strCcEmailAddress = trim( $arrmixValues['cc_email_address'] );
		} elseif( isset( $arrmixValues['cc_email_address'] ) ) {
			$this->setCcEmailAddress( $arrmixValues['cc_email_address'] );
		}
		if( isset( $arrmixValues['ps_product_id'] ) && $boolDirectSet ) {
			$this->m_intPsProductId = trim( $arrmixValues['ps_product_id'] );
		} elseif( isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}

		if( isset( $arrmixValues['event_trigger_type'] ) && $boolDirectSet ) {
			$this->m_intEventTriggerType = trim( $arrmixValues['event_trigger_type'] );
		} elseif( isset( $arrmixValues['event_trigger_type'] ) ) {
			$this->setEventTriggerType( $arrmixValues['event_trigger_type'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableManualContact = trim( $arrmixValues['is_enable_manual_contact'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact'] ) ) {
			$this->setIsEnableManualContact( $arrmixValues['is_enable_manual_contact'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact_after_n_days'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableManualContactAfterNDays = trim( $arrmixValues['is_enable_manual_contact_after_n_days'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact_after_n_days'] ) ) {
			$this->setIsEnableManualContactAfterNDays( $arrmixValues['is_enable_manual_contact_after_n_days'] );
		}

		if( isset( $arrmixValues['is_deleted'] ) && $boolDirectSet ) {
			$this->m_boolIsDeleted = trim( $arrmixValues['is_deleted'] );
		} elseif( isset( $arrmixValues['is_deleted'] ) ) {
			$this->setIsDeleted( $arrmixValues['is_deleted'] );
		}

		if( isset( $arrmixValues['applicant_type'] ) && $boolDirectSet ) {
			$this->m_strApplicantType = trim( $arrmixValues['applicant_type'] );
		} elseif( isset( $arrmixValues['applicant_type'] ) ) {
			$this->setApplicantType( $arrmixValues['applicant_type'] );
		} elseif( isset( $arrmixValues['application_ids'] ) ) {
			$this->setApplicationIds( $arrmixValues['application_ids'] );
		}

		if( isset( $arrmixValues['is_enable_sms'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableSms = trim( $arrmixValues['is_enable_sms'] );
		} elseif( isset( $arrmixValues['is_enable_sms'] ) ) {
			$this->setIsEnableSms( $arrmixValues['is_enable_sms'] );
		}

		if( isset( $arrmixValues['is_send_email_to_property'] ) && $boolDirectSet ) {
			$this->m_booliIsSendEmailToProperty = trim( $arrmixValues['is_send_email_to_property'] );
		} elseif( isset( $arrmixValues['is_send_email_to_property'] ) ) {
			$this->setIsSendEmailToProperty( $arrmixValues['is_send_email_to_property'] );
		}

		if( isset( $arrmixValues['sms_message'] ) && $boolDirectSet ) {
			$this->m_strSmsMessage = trim( $arrmixValues['sms_message'] );
		} elseif( isset( $arrmixValues['sms_message'] ) ) {
			$this->setSmsMessage( $arrmixValues['sms_message'] );
		}

		if( isset( $arrmixValues['overdue_business_hours'] ) && $boolDirectSet ) {
			$this->m_intOverdueBusinessHours = trim( $arrmixValues['overdue_business_hours'] );
		} elseif( isset( $arrmixValues['overdue_business_hours'] ) ) {
			$this->setOverdueBusinessHours( $arrmixValues['overdue_business_hours'] );
		}

		if( isset( $arrmixValues['reminder_business_hours'] ) && $boolDirectSet ) {
			$this->m_intReminderBusinessHours = trim( $arrmixValues['reminder_business_hours'] );
		} elseif( isset( $arrmixValues['reminder_business_hours'] ) ) {
			$this->setReminderBusinessHours( $arrmixValues['reminder_business_hours'] );
		}

		if( isset( $arrmixValues['is_consider_not_progressing'] ) && $boolDirectSet ) {
			$this->m_intIsConsiderNotProgressing = trim( $arrmixValues['is_consider_not_progressing'] );
		} elseif( isset( $arrmixValues['is_consider_not_progressing'] ) ) {
			$this->setIsConsiderNotProgressing( $arrmixValues['is_consider_not_progressing'] );
		}

		if( isset( $arrmixValues['list_item_id'] ) && $boolDirectSet ) {
			$this->m_intListItemId = trim( $arrmixValues['list_item_id'] );
		} elseif( isset( $arrmixValues['list_item_id'] ) ) {
			$this->setListItemId( $arrmixValues['list_item_id'] );
		}

		if( isset( $arrmixValues['list_type_id'] ) && $boolDirectSet ) {
			$this->m_intListTypeId = trim( $arrmixValues['list_type_id'] );
		} elseif( isset( $arrmixValues['list_type_id'] ) ) {
			$this->setListTypeId( $arrmixValues['list_type_id'] );
		}

		if( isset( $arrmixValues['mapping_event_type_id'] ) && $boolDirectSet ) {
			$this->m_intMappingEventTypeId = trim( $arrmixValues['mapping_event_type_id'] );
		} elseif( isset( $arrmixValues['mapping_event_type_id'] ) ) {
			$this->setMappingEventTypeId( $arrmixValues['mapping_event_type_id'] );
		}
		if( isset( $arrmixValues['mapping_event_sub_type_id'] ) && $boolDirectSet ) {
			$this->m_intMappingEventSubTypeId = trim( $arrmixValues['mapping_event_sub_type_id'] );
		} elseif( isset( $arrmixValues['mapping_event_sub_type_id'] ) ) {
			$this->setMappingEventSubTypeId( $arrmixValues['mapping_event_sub_type_id'] );
		}

		if( isset( $arrmixValues['mapping_event_result_id'] ) && $boolDirectSet ) {
			$this->m_intMappingEventResultId = trim( $arrmixValues['mapping_event_result_id'] );
		} elseif( isset( $arrmixValues['mapping_event_sub_type_id'] ) ) {
			$this->setMappingEventResultId( $arrmixValues['mapping_event_result_id'] );
		}

		if( isset( $arrmixValues['mapping_new_status_id'] ) && $boolDirectSet ) {
			$this->m_intMappingNewStatusId = trim( $arrmixValues['mapping_new_status_id'] );
		} elseif( isset( $arrmixValues['mapping_new_status_id'] ) ) {
			$this->setMappingNewStatusId( $arrmixValues['mapping_new_status_id'] );
		}

		if( isset( $arrmixValues['mapping_new_stage_id'] ) && $boolDirectSet ) {
			$this->m_intMappingNewStageId = trim( $arrmixValues['mapping_new_stage_id'] );
		} elseif( isset( $arrmixValues['mapping_new_stage_id'] ) ) {
			$this->setMappingNewStageId( $arrmixValues['mapping_new_stage_id'] );
		}

		if( isset( $arrmixValues['system_email_type_id'] ) && $boolDirectSet ) {
			$this->m_intSystemEmailTypeId = trim( $arrmixValues['system_email_type_id'] );
		} elseif( isset( $arrmixValues['system_email_type_id'] ) ) {
			$this->setSystemEmailTypeId( $arrmixValues['system_email_type_id'] );
		}

		if( isset( $arrmixValues['scheduled_task_ids'] ) && $boolDirectSet ) {
			$this->m_strScheduledTaskIds = trim( $arrmixValues['scheduled_task_ids'] );
		} elseif( isset( $arrmixValues['scheduled_task_ids'] ) ) {
			$this->setScheduledTaskIds( $arrmixValues['scheduled_task_ids'] );
		}

		if( isset( $arrmixValues['num_days_count'] ) && $boolDirectSet ) {
			$this->m_intNumDaysCount = trim( $arrmixValues['num_days_count'] );
		} elseif( isset( $arrmixValues['num_days_count'] ) ) {
			$this->setNumDaysCount( $arrmixValues['num_days_count'] );
		}

		if( isset( $arrmixValues['list_item_id_count'] ) && $boolDirectSet ) {
			$this->m_intListItemIdCount = trim( $arrmixValues['list_item_id_count'] );
		} elseif( isset( $arrmixValues['list_item_id_count'] ) ) {
			$this->setListItemIdCount( $arrmixValues['list_item_id_count'] );
		}

		if( isset( $arrmixValues['is_enable_email_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableEmailCount = trim( $arrmixValues['is_enable_email_count'] );
		} elseif( isset( $arrmixValues['is_enable_email_count'] ) ) {
			$this->setIsEnableEmailCount( $arrmixValues['is_enable_email_count'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableManualContactCount = trim( $arrmixValues['is_enable_manual_contact_count'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact_count'] ) ) {
			$this->setIsEnableManualContactCount( $arrmixValues['is_enable_manual_contact_count'] );
		}

		if( isset( $arrmixValues['manual_contact_instructions_count'] ) && $boolDirectSet ) {
			$this->m_intManualContactInstructionsCount = trim( $arrmixValues['manual_contact_instructions_count'] );
		} elseif( isset( $arrmixValues['manual_contact_instructions_count'] ) ) {
			$this->setManualContactInstructionsCount( $arrmixValues['manual_contact_instructions_count'] );
		}

		if( isset( $arrmixValues['subject_count'] ) && $boolDirectSet ) {
			$this->m_intSubjectCount = trim( $arrmixValues['subject_count'] );
		} elseif( isset( $arrmixValues['subject_count'] ) ) {
			$this->setSubjectCount( $arrmixValues['subject_count'] );
		}

		if( isset( $arrmixValues['overdue_business_hours_count'] ) && $boolDirectSet ) {
			$this->m_intOverdueBusinessHoursCount = trim( $arrmixValues['overdue_business_hours_count'] );
		} elseif( isset( $arrmixValues['overdue_business_hours_count'] ) ) {
			$this->setOverdueBusinessHoursCount( $arrmixValues['overdue_business_hours_count'] );
		}

		if( isset( $arrmixValues['reminder_business_hours_count'] ) && $boolDirectSet ) {
			$this->m_intReminderBusinessHoursCount = trim( $arrmixValues['reminder_business_hours_count'] );
		} elseif( isset( $arrmixValues['reminder_business_hours_count'] ) ) {
			$this->setReminderBusinessHoursCount( $arrmixValues['reminder_business_hours_count'] );
		}

		if( isset( $arrmixValues['list_type_id_count'] ) && $boolDirectSet ) {
			$this->m_intListTypeIdCount = trim( $arrmixValues['list_type_id_count'] );
		} elseif( isset( $arrmixValues['list_type_id_count'] ) ) {
			$this->setListTypeIdCount( $arrmixValues['list_type_id_count'] );
		}
		if( isset( $arrmixValues['compose_email_ids'] ) && $boolDirectSet ) {
			$this->m_strComposeEmailIds = trim( $arrmixValues['compose_email_ids'] );
		} elseif( isset( $arrmixValues['compose_email_ids'] ) ) {
			$this->setComposeEmailIds( $arrmixValues['compose_email_ids'] );
		}

		if( isset( $arrmixValues['system_message_template_id_count'] ) && $boolDirectSet ) {
			$this->m_intSystemMessageTemplateIdCount = trim( $arrmixValues['system_message_template_id_count'] );
		} elseif( isset( $arrmixValues['system_message_template_id_count'] ) ) {
			$this->setSystemMessageTemplateIdCount( $arrmixValues['system_message_template_id_count'] );
		}
		if( isset( $arrmixValues['bcc_email_address_count'] ) && $boolDirectSet ) {
			$this->m_intBccEmailAddressCount = trim( $arrmixValues['bcc_email_address_count'] );
		} elseif( isset( $arrmixValues['bcc_email_address_count'] ) ) {
			$this->setBccEmailAddressCount( $arrmixValues['bcc_email_address_count'] );
		}
		if( isset( $arrmixValues['system_message_key'] ) && $boolDirectSet ) {
			$this->m_strSystemMessageKey = trim( $arrmixValues['system_message_key'] );
		} elseif( isset( $arrmixValues['system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['system_message_key'] );
		}
		if( isset( $arrmixValues['sms_system_message_key'] ) && $boolDirectSet ) {
			$this->m_strSmsSystemMessageKey = trim( $arrmixValues['sms_system_message_key'] );
		} elseif( isset( $arrmixValues['sms_system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['sms_system_message_key'] );
		}
		if( isset( $arrmixValues['is_entrata_default'] ) && $boolDirectSet ) {
			$this->m_boolIsEntrataDefault = trim( $arrmixValues['is_entrata_default'] );
		} elseif( isset( $arrmixValues['is_entrata_default'] ) ) {
			$this->setIsEntrataDefault( $arrmixValues['is_entrata_default'] );
		}
	}

	public function validate( $strAction, $objClient = NULL, $objDatabase = NULL, $boolValidateGroupCount = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( self::EVENT_TRIGGER_TYPE_IMMEDIATE != trim( $this->getEventTriggerType() ) && self::EVENT_TRIGGER_TYPE_DAY_OF_EVENT != trim( $this->getEventTriggerType() ) ) {
					if( false == is_numeric( $this->getNumDays() ) || 0 > $this->getNumDays() ) {
						$boolIsValid = false;
					}
				}
				break;

			default:
				// default action will go here
				break;
		}

		return $boolIsValid;
	}

	public function execute( $intCompanyUserId, $objDatabase, $objApplication = NULL, $objEvent = NULL, $boolVerbose = false ) {
		$this->m_boolIsVerbose = $boolVerbose;
		if( $this->m_boolIsVerbose ) {
			echo "\n -----------------------------------------------------------------------------------------------------------------";
			echo "\n |" . 'Processing parent scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ] ';
			echo "\n -----------------------------------------------------------------------------------------------------------------";
		}

		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return;
		}

		$this->m_arrmixNewLeadEventProductsMapping = [
			CPsProduct::PROSPECT_PORTAL => [ CPsProduct::FACEBOOK_INTEGRATION ],
			CPsProduct::ENTRATA    		=> [ CPsProduct::ENTRATA_PAAS, CPsProduct::SITE_TABLET, CPsProduct::API_SERVICES ],
			CPsProduct::ILS_PORTAL      => [ CPsProduct::VACANCY, CPsProduct::GUEST_CARD_PARSING ]
		];

		// Check product permissions. [KAL]
		$arrintPermissionedPsProductIds	= [
			CPsProduct::LEAD_MANAGEMENT,
			CPsProduct::MESSAGE_CENTER,
			CPsProduct::PROSPECT_PORTAL,
			CPsProduct::ILS_PORTAL,
			CPsProduct::CRAIGSLIST_POSTING,
			CPsProduct::CALL_TRACKER,
			CPsProduct::LEASING_CENTER,
			CPsProduct::ENTRATA,
			CPsProduct::SITE_TABLET,
			CPsProduct::VACANCY,
			CPsProduct::FACEBOOK_INTEGRATION,
			CPsProduct::API_SERVICES
		];

		$this->m_arrintPsProductIds = \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPsProductPermissionsByPsProductIdsByPropertyIdsByCid( $arrintPermissionedPsProductIds, [ $this->getPropertyId() ], $this->getCid(), $objDatabase );

		if( false == valArr( $this->m_arrintPsProductIds )
		    || ( ( false == array_key_exists( CPsProduct::LEAD_MANAGEMENT, $this->m_arrintPsProductIds ) )
		         && ( false == array_key_exists( CPsProduct::MESSAGE_CENTER, $this->m_arrintPsProductIds ) ) ) ) {

			if( $this->m_boolIsVerbose ) {
				echo "\n -";
				echo "\n No permissioned products are associated to property.";
				echo "\n -";
			}
			return;
		}

	}

	public function loadApplications( $objDatabase ) {

		if( true == $this->m_boolIsVerbose ) {
			echo "\n Loading applications...";
		}

		$this->m_arrobjApplications = $this->fetchApplications( $objDatabase );

		if( true == $this->m_boolIsVerbose ) {
			if( true == valArr( $this->m_arrobjApplications ) ) {
				echo "\n application(s) loaded successfuly.";
			} else {
				echo "\n No applications found.";
			}
		}
	}

	public function fetchApplications( $objDatabase ) {
		$strSql = 'SELECT
						app.*,
						CURRENT_DATE - DATE( e.event_datetime ) as last_contact_days
					FROM
						cached_applications app
						JOIN temp_crm_application_scheduled_tasks ast ON ( app.id = ast.application_id AND app.property_id = ast. property_id AND app.cid = ast.cid )
		 				LEFT JOIN events e ON ( e.cid = ' . ( int ) $this->getCid() . ' AND e.id = app.last_event_id AND e.cid = app.cid )
					WHERE
						app.cid = ' . $this->getCid();

		return CApplications::fetchApplications( $strSql, $objDatabase );
	}

	public function cancelLease( $objApplication, $intCompanyUserId, $objDatabase ) {

		$objLeaseExecutionLibrary = new CLeaseExecutionLibrary();

		$objClient 		= CClients::fetchClientById( $this->getCid(), $objDatabase );
		$objCompanyUser	= \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) || false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return;
		}

		$objLeaseExecutionLibrary->setClient( $objClient );
		$objLeaseExecutionLibrary->setDatabase( $objDatabase );
		$objLeaseExecutionLibrary->setApplication( $objApplication );
		$objLeaseExecutionLibrary->setCompanyUserId( $intCompanyUserId );

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objLeaseExecutionLibrary->cancelLease() ) {
					$objDatabase->rollback();
					return false;
				}

				$objDatabase->commit();
		}

		return true;
	}

	public function cancelApplication( $objApplication, $intCompanyUserId, $intListItemId, $objDatabase ) {

		$objClient = CClients::fetchClientById( $objApplication->getCid(), $objDatabase );
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $objApplication->getCid(), $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) || false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return;
		}

		$objApplicationDataObject = new CApplicationDataObject();

		$objApplicationDataObject->m_objClient 	= $objClient;
		$objApplicationDataObject->m_objCompanyUser 		= $objCompanyUser;
		$objApplicationDataObject->m_intCompanyUserId		= $intCompanyUserId;
		$objApplicationDataObject->m_objDatabase 			= $objDatabase;
		$objApplicationDataObject->m_objApplication  		= $objApplication;
		$objApplicationDataObject->m_arrstrEventData 		= [];

		if( true == valId( $intListItemId ) ) {
			$objApplicationDataObject->m_arrstrEventData['list_item_id'] = $intListItemId;
		}

		$objApplicationSystemLibrary = new CApplicationSystemLibrary();

		$objApplicationSystemLibrary->setApplicationDataObject( $objApplicationDataObject );

		$intOldApplicationStageId 	= $objApplication->getApplicationStageId();
		$intOldApplicationStatusId 	= $objApplication->getApplicationStatusId();

		$arrobjDeletingLeaseCustomers 	= [];
		$arrobjTransmissions 			= [];

		if( CLeaseIntervalType::LEASE_MODIFICATION == $objApplication->getLeaseIntervalTypeId() ) {
			//@TODO Test it thoroughly in second phase refactoring
			$arrobjDeletingLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByLeaseIdByLeaseStatusTypeIdsByCid( $objApplication->getLeaseId(), [ CLeaseStatusType::APPLICANT ], $objApplication->getCid(), $objDatabase );
			$arrobjTransmissions = CTransmissions::fetchTransmissionsByApplicationIdsByCid( [ $objApplication->getId() ], $objApplication->getCid(), $objDatabase );
		}

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objApplicationSystemLibrary->updateApplicationStatus( CApplicationStage::APPLICATION, CApplicationStatus::CANCELLED ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjDeletingLeaseCustomers ) && false == \Psi\Eos\Entrata\CLeaseCustomers::createService()->bulkDelete( $arrobjDeletingLeaseCustomers, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjTransmissions ) ) {
					foreach( $arrobjTransmissions as $objTransmission ) {
						if( false == $objTransmission->delete( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							break 2;
						}
					}
				}

				if( false == $this->cancelAddOnLeaseAssociations( $objApplication, $objDatabase, $intCompanyUserId ) ) {
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();

				// integration call needs to move in application lib
				$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

				if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::AMSI != $objIntegrationDatabase->getIntegrationClientTypeId() ) {
					$this->processIntegrationClient( $objApplication, $objClient, $intCompanyUserId, $objDatabase, $intOldApplicationStageId, $intOldApplicationStatusId );
				}
				return true;
		}
		return false;
	}

	public function handleArchiveApplication( $objApplication, $intCompanyUserId, $intListItemId, $objDatabase ) {

		$objClient = CClients::fetchClientById( $objApplication->getCid(), $objDatabase );
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $objApplication->getCid(), $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) || false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return;
		}

		$objApplicationDataObject 	 = new CApplicationDataObject();

		$objApplicationDataObject->m_objClient				= $objClient;
		$objApplicationDataObject->m_objCompanyUser	 		= $objCompanyUser;
		$objApplicationDataObject->m_intCompanyUserId		= $intCompanyUserId;
		$objApplicationDataObject->m_objDatabase 			= $objDatabase;
		$objApplicationDataObject->m_objApplication  		= $objApplication;
		$objApplicationDataObject->m_arrstrEventData = [];

		if( true == valId( $intListItemId ) ) {
			$objApplicationDataObject->m_arrstrEventData['list_item_id'] = $intListItemId;
		}

		$objApplicationSystemLibrary = new CApplicationSystemLibrary();
		$objApplicationSystemLibrary->setApplicationDataObject( $objApplicationDataObject );

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objApplicationSystemLibrary->archiveApplication() ) {
					$this->addErrorMsgs( $objApplicationSystemLibrary->getErrorMsgs() );
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();
				return true;
		}

		return false;
	}

	public function cancelAddOnLeaseAssociations( $objApplication, $objDatabase, $intCompanyUserId ) {

		$objAddOnLeaseAssociations = ( array ) \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByLeaseIntervalIdsByCid( [ $objApplication->getLeaseIntervalId() ], $objApplication->getCid(), $objDatabase, $arrintExcludeLeaseStatusTypeIds = [ CLeaseStatusType::PAST ] );

		$arrintDeletingAddOnLeaseAssociationIds = [];

		foreach( $objAddOnLeaseAssociations as $objAddOnLeaseAssociation ) {
			$objAddOnLeaseAssociation->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
			$arrintDeletingAddOnLeaseAssociationIds[] = $objAddOnLeaseAssociation->getId();
			if( false == $objAddOnLeaseAssociation->delete( $intCompanyUserId, $objDatabase, $boolIsSoftDelete = true, $boolIsDeleteSchedueledCharges = true ) ) {
				return false;
			}
		}

		return true;
	}

	public function validateScheduledTask( $objScheduledTask, $objApplication, $objDatabase ) {
		// validation for new leads
		if( false == is_null( $objScheduledTask->getPsProductId() ) && CPsProduct::ENTRATA != $objApplication->getPsProductId() && CPsProduct::API_SERVICES != $objApplication->getPsProductId() ) {
			if( CPsProduct::PROSPECT_PORTAL == $objScheduledTask->getPsProductId() || CPsProduct::ILS_PORTAL == $objScheduledTask->getPsProductId() ) {
				if( false == array_key_exists( $objScheduledTask->getPsProductId(), $this->getLegacyPsProductIds() ) ) {
					if( false == valArr( array_intersect( $this->m_arrmixNewLeadEventProductsMapping[$objScheduledTask->getPsProductId()], $this->getLegacyPsProductIds() ) ) ) {
						return false;
					}
				}
			} else {
				if( false == array_key_exists( $objScheduledTask->getPsProductId(), $this->getLegacyPsProductIds() ) && CPsProduct::ENTRATA != $objScheduledTask->getPsProductId() ) {
					return false;
				} else {
					if( CPsProduct::ENTRATA == $objScheduledTask->getPsProductId() && false == array_key_exists( CPsProduct::ENTRATA, $this->getLegacyPsProductIds() ) && false == array_key_exists( CPsProduct::LEAD_MANAGEMENT, $this->getLegacyPsProductIds() ) ) {
						return false;
					}
				}
			}
		}

		if( ( CEventType::SCHEDULED_FOLLOWUP != $objScheduledTask->getEventTypeId() || CEventSubType::NEW_LEAD_FOLLOW_UP != $objScheduledTask->getEventSubTypeId() ) && false == is_null( $objScheduledTask->getPsProductId() ) && CPsProduct::ENTRATA != $objScheduledTask->getPsProductId() && CPsProduct::API_SERVICES != $objScheduledTask->getPsProductId() && false == array_key_exists( $objScheduledTask->getPsProductId(), $this->getLegacyPsProductIds() ) ) {
			return false;
		}

		if( CEventType::SCHEDULED_FOLLOWUP == $objScheduledTask->getEventTypeId() && CEventSubType::APPLICATION_STARTED_FOLLOW_UP == $objScheduledTask->getEventSubTypeId() && CApplicationStage::APPLICATION == $objApplication->getApplicationStageId() && CApplicationStatus::STARTED == $objApplication->getApplicationStatusId() ) {

			if( false == array_key_exists( CPsProduct::PROSPECT_PORTAL, $this->getLegacyPsProductIds() ) ) {
				return;
			}

			$boolIsValid = false;

			$arrintApplicantIds         = CApplicants::fetchApplicantIdsByApplicationIdByCid( $objApplication->getId(), $objApplication->getCid(), $objDatabase );
			$intAuthenticationLogsCount = CAuthenticationLogs::fetchAuthenticationLogsCountByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $objApplication->getId(), $objApplication->getCid(), $objDatabase );

			if( 0 < $intAuthenticationLogsCount ) {
				$boolIsValid = true;
			}

			return $boolIsValid;
		}

		return true;
	}

	public function processIntegrationClient( $objApplication, $objClient, $intCompanyUserId, $objDatabase, $intOldApplicationStageId = NULL, $intOldApplicationStatusId = NULL ) {

		if( false == valObj( $objApplication, 'CApplication' ) ) {
			return true;
		}

		if( false == valStr( $this->m_objProperty->getRemotePrimaryKey() ) ) {
			return true;
		}

		if( false == in_array( $objApplication->getLeaseIntervalTypeId(), [ CLeaseIntervalType::APPLICATION ] ) ) {
			return true;
		}

		// Archived leads should not call integration - NRW
		if( true == valStr( $objApplication->getRemotePrimaryKey() ) && CApplicationStatus::ON_HOLD == $objApplication->getApplicationStatusId() ) {
			return true;
		}

		if( false == valObj( $this->getPaymentDatabase(), 'CDatabase' ) ) {
			$this->displayMessage( 'Payment service is currently unavailable, Please try after some time', '', '', '', '', 1 );
			exit;
		}

		$boolIsValid = true;

		// Handling Applications
		// 1. Update the incomplete application into external system if it is integrated as guest card.
		$intApplicationStageId	= ( true == is_null( $intOldApplicationStageId ) ) ? $objApplication->getApplicationStageId() : $intOldApplicationStageId;
		$intApplicationStatusId = ( true == is_null( $intOldApplicationStatusId ) ) ? $objApplication->getApplicationStatusId() : $intOldApplicationStatusId;

		if( ( false == valStr( $objApplication->getAppRemotePrimaryKey() )
		    && CApplicationStage::APPLICATION > $intApplicationStageId ) || ( CApplicationStage::APPLICATION == $intApplicationStageId && CApplicationStatus::PARTIALLY_COMPLETED > $intApplicationStatusId ) ) {

			if( true == valStr( $objApplication->getGuestRemotePrimaryKey() ) ) {
				return $objApplication->exportUpdate( $intCompanyUserId, $objDatabase );
			} else {
				return true;
			}
		}

		// 2. Exporting/updating completed application.
		$strFunction = ( false == valStr( $objApplication->getRemotePrimaryKey() ) ) ? 'export' : 'exportUpdate';
		if( false == $objApplication->$strFunction( $intCompanyUserId, $objDatabase ) ) return false;

		// 3. If application was completed before this but not integrated then it might be integrated at #2. Here are updating the decision into external system.
		if( ( 'export' == $strFunction
		    && CApplicationStage::APPLICATION < $objApplication->getApplicationStageId() ) || ( CApplicationStage::APPLICATION == $objApplication->getApplicationStageId() && CApplicationStatus::COMPLETED < $objApplication->getApplicationStatusId() ) ) {
			$objApplication->refreshField( 'app_remote_primary_key', 'applications', $objDatabase );

			if( true == valStr( $objApplication->getRemotePrimaryKey() ) ) {
				$objApplication->exportUpdate( $intCompanyUserId, $objDatabase );
			}
		}

		// 4. Exporting application charges and payments.
		$arrobjPropertyPreferences = $this->m_objProperty->getPropertyPreferences();

		if( false == valArr( $arrobjPropertyPreferences ) || false == isset( $arrobjPropertyPreferences['DONT_EXPORT_APPLICATION_PAYMENTS'] ) || false == isset( $arrobjPropertyPreferences['DONT_EXPORT_APPLICATION_CHARGES'] ) ) {
			$arrobjPropertyPreferences 	= $this->m_objProperty->fetchPropertyPreferencesByKeysKeyedByKey( [ 'DONT_EXPORT_APPLICATION_PAYMENTS', 'DONT_EXPORT_APPLICATION_CHARGES' ], $objDatabase, true );
		}

		$objPropertyPreference = getArrayElementByKey( 'DONT_EXPORT_APPLICATION_CHARGES', $arrobjPropertyPreferences );

		if( CLeaseIntervalType::APPLICATION == $objApplication->getLeaseIntervalTypeId() && false == valObj( $objPropertyPreference, 'CPropertyPreference', 'Value' ) ) {

			$objLease = $objApplication->fetchLease( $objDatabase );

			if( false == valObj( $objLease, 'CLease', 'RemotePrimaryKey' ) ) return true;

			$arrobjArTransactions = $objApplication->fetchNonIntegratedApplicationCharges( $objDatabase );

			if( false == valArr( $arrobjArTransactions ) ) return true;

			// Process Send Charges/Company Lease Transactions Integration Client
			if( false == $objApplication->exportCharges( $intCompanyUserId, $objDatabase, $this->getPaymentDatabase(), false ) ) return false;

			$objPropertyPreference = getArrayElementByKey( 'DONT_EXPORT_APPLICATION_PAYMENTS', $arrobjPropertyPreferences );

			if( true == valObj( $objPropertyPreference, 'CPropertyPreference', 'Value' ) ) return true;

			$arrobjApplicantApplications = $objApplication->getOrFetchApplicantApplications( $objDatabase );

			if( true == valArr( $arrobjApplicantApplications ) ) return true;

			foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
				$arrobjArPayments = $objApplicantApplication->fetchArPayments( $objDatabase );
				$arrobjAdminArPayments = [];

				if( true == valArr( $arrobjArPayments ) ) {
					$arrobjAdminArPayments = ( array ) $objClient->fetchArPaymentsByIds( array_keys( $arrobjArPayments ), $this->getPaymentDatabase() );
				}

				if( false == valArr( $arrobjAdminArPayments ) ) continue;

				foreach( $arrobjAdminArPayments as $objAdminArPayment ) {
					if( CPaymentStatusType::CAPTURED == $objAdminArPayment->getPaymentStatusTypeId() ) {
						$boolIsValid &= $objAdminArPayment->exportArPayment( $intCompanyUserId, $objDatabase, $this->getPaymentDatabase() );
					}
				}
			}
		}

		return $boolIsValid;
	}

}

?>
