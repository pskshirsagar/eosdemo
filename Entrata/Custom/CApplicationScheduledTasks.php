<?php

class CApplicationScheduledTasks extends \Psi\Eos\Entrata\CScheduledTasks {

	public static function loadCrmApplicationScheduledTasks( $intCid, $intPropertyId, $intId, $objDatabase ) {
		$strSql = 'SELECT * FROM load_crm_application_scheduled_tasks(' . $intCid . '::INTEGER,' . $intPropertyId . '::INTEGER,' . $intId . '::INTEGER )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationScheduledTasksByPropertyIdByScheduledTaskIdIdByCid( $intPropertyId, $intScheduledTaskId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					   	id,
						cid,
						property_id,
					    scheduled_task_id,
					    event_type_id,
					    event_sub_type_id,
					    schedule_type_id,
						scheduled_task_type_id,
					    task_details,
					    schedule_details,
					    updated_on,
			    		updated_by,
			    		created_on,
			    		created_by,
					    array_agg( application_id ) as application_ids
					FROM
						temp_crm_application_scheduled_tasks
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
					    AND scheduled_task_id = ' . ( int ) $intScheduledTaskId . '
					    AND cid = ' . ( int ) $intCid . '
					GROUP BY
					    id,
						cid,
						property_id,
					    scheduled_task_id,
					    event_type_id,
					    event_sub_type_id,
					    schedule_type_id,
						scheduled_task_type_id,
					    task_details,
					    schedule_details,
					    updated_on,
			    		updated_by,
			    		created_on,
			    		created_by';

		return self::fetchScheduledTasks( $strSql, $objDatabase );
	}

	public static function fetchApplicationScheduledTasksByScheduledTaskEmailSearchFilter( $objScheduledTaskSearchFilter, $objDatabase ) {
		$arrstrWhere = [];

		if( true == valStr( $objScheduledTaskSearchFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'st.property_id IN ( ' . $objScheduledTaskSearchFilter->getPropertyIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_sub_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventTypeIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTriggerTypes() ) ) {
			$arrstrWhere[] = 'st.schedule_details ->> \'event_trigger_type\' IN ( \'' . $objScheduledTaskSearchFilter->getEventTriggerTypes() . '\')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getSubject() ) ) {
			$arrstrWhere[] = 'ste.subject LIKE \'%' . addslashes( trim( $objScheduledTaskSearchFilter->getSubject() ) ) . '%\'';
		}

		$strSql = 'SELECT
						st.id,
						st.cid,
						st.property_id,
						st.task_details ->> \'compose_email_id\' as compose_email_id,
						st.schedule_details ->> \'num_days\' as num_days,
						st.schedule_details ->> \'event_trigger_type\' as event_trigger_type,
						ste.subject as email_subject,
						st.event_sub_type_id as event_id,
						p.property_name
					FROM
						scheduled_tasks st
						JOIN scheduled_task_emails ste ON ( st.cid = ste.cid AND ( st. task_details ->> \'compose_email_id\' )::INTEGER = ste.id )
						JOIN properties p ON ( st.cid = p.cid AND st.property_id = p.id )
					WHERE
						st.cid = ' . ( int ) $objScheduledTaskSearchFilter->getCid() . '
						AND st.deleted_by IS NULL
						AND st.deleted_on IS NULL 
						AND ( st.task_details ? \'compose_email_id\'
						AND ( st.task_details ->> \'compose_email_id\' ) IS NOT NULL )
						AND st.task_details ->> \'compose_email_id\' != \'\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( true == is_numeric( $objScheduledTaskSearchFilter->getOffset() ) && true == is_numeric( $objScheduledTaskSearchFilter->getPageSize() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objScheduledTaskSearchFilter->getOffset() . ' LIMIT ' . ( int ) $objScheduledTaskSearchFilter->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountApplicationScheduledTasksByScheduledTaskEmailSearchFilter( $objScheduledTaskSearchFilter, $objDatabase ) {
		$arrstrWhere = [];

		if( true == valStr( $objScheduledTaskSearchFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'st.property_id IN ( ' . $objScheduledTaskSearchFilter->getPropertyIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventTypeIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTriggerTypes() ) ) {
			$arrstrWhere[] = 'st.schedule_details ->> \'event_trigger_type\' IN ( \'' . $objScheduledTaskSearchFilter->getEventTriggerTypes() . '\')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getSubject() ) ) {
			$arrstrWhere[] = 'ste.subject LIKE \'%' . addslashes( trim( $objScheduledTaskSearchFilter->getSubject() ) ) . '%\'';
		}

		$strSql = 'SELECT
						COUNT( st.id )
					FROM
						scheduled_tasks st
						JOIN scheduled_task_emails ste ON ( st.cid = ste.cid AND ( st. task_details ->> \'compose_email_id\' )::INTEGER = ste.id )
						JOIN event_sub_types est ON ( st.event_sub_type_id = est.id )
						JOIN properties p ON ( st.cid = p.cid AND st.property_id = p.id )
					WHERE
						st.cid = ' . ( int ) $objScheduledTaskSearchFilter->getCid() . '
						AND st.deleted_by IS NULL
						AND st.deleted_on IS NULL 
						AND ( st.task_details ? \'compose_email_id\'
						AND ( st.task_details ->> \'compose_email_id\' ) IS NOT NULL )
						AND st.task_details ->> \'compose_email_id\' != \'\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchApplicationScheduledTasksByScheduledTaskTypeIdsByCids( $arrintScheduledTaskTypeIds, $arrIntCids, $objDatabase ) {
		$strSql = 'SELECT
						st.*
					FROM
						scheduled_tasks st
						JOIN properties p ON ( st.cid = p.cid AND st.property_id = p.id AND p.is_disabled <> 1 AND p.termination_date IS NULL)
					WHERE
						st.cid IN ( ' . implode( ',', $arrIntCids ) . ' )
						AND scheduled_task_type_id IN ( ' . implode( ',', $arrintScheduledTaskTypeIds ) . ' )
						AND st.deleted_by IS NULL
						AND st.deleted_on IS NULL
						AND st.scheduled_task_id IS NOT NULL
					ORDER BY
						st.cid, st.property_id, st.order_num';

		return self::fetchScheduledTasks( $strSql, $objDatabase );
	}

}
?>
