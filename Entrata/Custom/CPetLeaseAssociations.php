<?php

class CPetLeaseAssociations extends CLeaseAssociations {

	public static function fetchPetLeaseAssociationsByArOriginObjectIdsByLeaseIdByLeaseIntervalIdByCid( $arrintArOriginObjectIds, $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArOriginObjectIds ) || true == is_null( $intLeaseId ) || true == is_null( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( cp.cid = la.cid AND la.ar_origin_object_id = cp.id )
						JOIN pet_types pt ON ( pt.cid = cp.cid AND cp.pet_type_id = pt.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.ar_origin_object_id IN ( ' . implode( ',', $arrintArOriginObjectIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByArOriginObjectIdsByLeaseIdByLeaseIntervalIdByCustomerIdByCid( $arrintArOriginObjectIds, $intLeaseId, $intLeaseIntervalId, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArOriginObjectIds ) || ( true == is_null( $intLeaseId ) && true == is_null( $intLeaseIntervalId ) ) ) return NULL;

		$strCustomerIdSql 		= ( true == valId( $intCustomerId ) ) ? ' AND la.customer_id = ' . ( int ) $intCustomerId . ' ' : ' ';

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( la.cid = cp.cid AND la.ar_origin_object_id = cp.id )
						JOIN pet_types pt ON ( cp.cid = pt.cid AND cp.pet_type_id = pt.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND COALESCE( la.lease_id = ' . ( int ) $intLeaseId . ', la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' )
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.ar_origin_object_id IN ( ' . implode( ',', $arrintArOriginObjectIds ) . ' )
						AND la.deleted_by IS NULL
						' . $strCustomerIdSql . ' ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchCountPetLeaseAssociationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						count(id)
					FROM
						lease_associations
					WHERE
						cid::int=' . ( int ) $intCid . '
						AND ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND quote_id = ' . ( int ) $intQuoteId;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
	}

	public static function fetchPetLeaseAssociationsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintQuoteIds ) ) return NULL;

		$strSql = ' SELECT
						la.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name						
					FROM
						lease_associations la
						JOIN pet_types pt ON pt.cid = la.cid AND pt.id = la.ar_origin_reference_id
					WHERE
						la.cid::int=' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
						AND la.deleted_by IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByArCascadeIdbyArCascadeReferenceIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceId, $intArOriginId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						lease_associations
					WHERE
						cid::int=' . ( int ) $intCid . '
						AND ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
						AND ar_origin_id = ' . ( int ) $intArOriginId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationByArOriginObjectIdByLeaseIntervalIdByCid( $intArOriginObjectId, $intLeaseIntervalId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_object_id = ' . ( int ) $intArOriginObjectId . '
						AND la.is_selected_quote IS TRUE 
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchTransferQuotePetLeaseAssociationByArOriginObjectIdByLeaseIntervalIdByQuoteIdByCid( $intArOriginObjectId, $intLeaseIntervalId, $intQuoteId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_object_id = ' . ( int ) $intArOriginObjectId . '
						AND la.quote_id = ' . ( int ) $intQuoteId . '
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public function fetchPetLeaseAssociationByLeaseIdByLeaseIntervalIdByArOriginIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						la.*,
						cp.name as pet_name,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name
						
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( cp.cid = la.cid AND la.ar_origin_object_id = cp.id )
						JOIN pet_types pt ON ( pt.cid = cp.cid AND cp.pet_type_id = pt.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByLeaseIntervalIdExcludingQuoteIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( cp.cid = la.cid AND la.ar_origin_object_id = cp.id )
						JOIN pet_types pt ON ( pt.cid = cp.cid AND cp.pet_type_id = pt.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.is_selected_quote IS TRUE                         
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchCountPetLeaseAssociationsByLeaseIdByCustomerIdByPropertyIdByCid( $intLeaseId, $intCustomerId, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = ' SELECT
						count(id)
					FROM
						lease_associations
					WHERE
						cid =' . ( int ) $intCid . '
						AND ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND ( lease_id = ' . ( int ) $intLeaseId . '
						OR customer_id = ' . ( int ) $intCustomerId . ' )
						AND deleted_by IS NULL ';

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
	}

	public static function fetchPetLeaseAssociationsByPropertyIdsByLeaseIntervalIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolIncludeIsSelectedQuote = true, $intCustomerId = NULL ) {

		if( false === valArr( $arrintLeaseIntervalIds ) || false === valArr( $arrintPropertyIds ) ) return NULL;

		$strCustomerIdCondition = '';
		$strCustomerIdCondition .= ( false == is_null( $intCustomerId ) ) ? ' AND cp.customer_id = ' . ( int ) $intCustomerId . ' ' : '';

		$strSqlConditions = '';

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';
		$strSqlConditions .= ( true == $boolIncludeIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' : '';

		$strSql = 'SELECT
						la.*,
						cp.name AS pet_name,
						(
							SELECT
								(c.name_first || \' \' || c.name_last)
							FROM
								customers c
							WHERE
								c.id = cp.customer_id
								AND c.cid = cp.cid
						) AS customer_name,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( cp.cid = la.cid AND cp.id = la.ar_origin_object_id ' . $strCustomerIdCondition . ' )
						JOIN pet_types pt ON ( pt.cid = cp.cid AND pt.id = cp.pet_type_id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND la.lease_status_type_id IN( ' . sqlIntImplode( [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) . ' )
						AND ( la.move_out_date IS NULL OR la.move_out_date > CURRENT_DATE )
						AND la.lease_interval_id IN ( ' . sqlIntImplode( $arrintLeaseIntervalIds ) . ' ) ' . $strSqlConditions;

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchRenewalPetLeaseAssociationsByPropertyIdByLeaseIntervalIdsByPetTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $intCid, $objDatabase, $boolIsFetchDeleted = false ) {

		if( false === valId( $intPropertyId ) || false === valId( $intCid ) || false === valArr( $arrintLeaseIntervalIds ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSqlConditions = ( true == $boolIsFetchDeleted ) ? '' : ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL';

		$strSql = ' SELECT
						la.*,
						cp.name AS pet_name,
						( c.name_first || c.name_last ) as customer_name,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS memo
					FROM
						lease_associations la
						JOIN pet_types pt ON ( pt.cid = la.cid AND pt.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . ' )
						JOIN customer_pets cp ON ( cp.cid = la.cid AND cp.id = la.ar_origin_object_id )
						JOIN customers c ON ( c.id = cp.customer_id AND c.cid = cp.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.lease_interval_id IN ( ' . sqlIntImplode( $arrintLeaseIntervalIds ) . ' )
						' . $strSqlConditions;

		return parent::fetchLeaseAssociations( $strSql, $objDatabase, false );
	}

	public static function fetchLeaseAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
					    la.*
					FROM
					    lease_associations la
					    JOIN customer_pets cp ON ( cp.id = la.ar_origin_object_id AND cp.cid = la.cid )
					WHERE
					    la.cid = ' . ( int ) $intCid . '
					    AND la.lease_id = ' . ( int ) $intLeaseId . '
					    AND la.ar_origin_id = ' . CArOrigin::PET . '
					    AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( true == is_null( $intLeaseId ) || true == is_null( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						sub.*
					FROM (
					       SELECT 
								la.*,
								rank() OVER(PARTITION BY la.cid, la.ar_origin_object_id ORDER BY li.lease_start_date DESC) AS rank
					       FROM customer_pets cp
					            JOIN lease_associations la ON ( la.cid = cp.cid AND la.ar_origin_object_id = cp.id )
					            JOIN cached_leases l ON ( l.cid = la.cid AND l.id = la.lease_id )
					            join lease_intervals li on (li.cid = l.cid and li.id = la.lease_interval_id)
					       WHERE
					            cp.cid = ' . ( int ) $intCid . '
					            AND l.id = ' . ( int ) $intLeaseId . '
					            AND la.ar_origin_id = ' . CArOrigin::PET . '
					            AND la.is_selected_quote = TRUE
					            AND la.deleted_on IS NULL
					            AND la.deleted_by IS NULL
					            AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					            AND la.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					            AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '
					            AND CASE
					               WHEN l.lease_status_type_id IN ( ' . implode( ',', [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE ] ) . ' ) THEN
					                 li.lease_status_type_id IN ( ' . implode( ',', [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE ] ) . ' )
					               ELSE li.lease_status_type_id IN ( ' . implode( ',', [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::PAST ] ) . ' )
					             END
					     ) sub
					WHERE 
						sub.rank = 1';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

}
?>