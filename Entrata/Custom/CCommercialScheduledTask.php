<?php
/**
 * Created by PhpStorm.
 * User: sbhaware01
 * Date: 6/3/2019
 * Time: 12:09 PM
 */

class CCommercialScheduledTask extends CScheduledTask {
	protected $m_boolIsEnableSubject;
	protected $m_boolIsDeleted;
	protected $m_boolIsVerbose;
	protected $m_boolIsResident;
	protected $m_boolIsInsuranceAgent = false;

	protected $m_strEmailSubject;
	protected $m_strSmsMessage;
	protected $m_strScheduledTaskIds;

	protected $m_intEventDaysCount;
	protected $m_intIsEnableEmailCount;
	protected $m_intIsEnableManualContactCount;
	protected $m_intManualContactInstructionsCount;
	protected $m_intBccEmailAddressCount;
	protected $m_intSubjectCount;
	protected $m_intSystemMessageTemplateIdCount;
	protected $m_intOverdueBusinessHoursCount;
	protected $m_intAttachmentCount;

	protected $m_arrintPsProductIds;

	const EVENT_TRIGGER_TYPE_IMMEDIATE			= 'immediate';
	const EVENT_TRIGGER_TYPE_AFTER				= 'after';
	const EVENT_TRIGGER_TYPE_BEFORE				= 'before';
	const EVENT_TRIGGER_TYPE_DAY_OF_EVENT		= 'day_of_event';

	/**
	 * Get Functions
	 *
	 */

	public function getNumDays() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'num_days' );
	}

	public function getEventDays() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'event_days' );
	}

	public function getEventTriggerType() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type' );
	}

	public function getLeaseIntervalTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'lease_interval_type_id' );
	}

	public function getPsProductId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'ps_product_id' );
	}

	public function getIsEnableEmail() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_email' );
	}

	public function getMaintenanceStatusTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'maintenance_status_type_id' );
	}

	public function getIsEnableManualContact() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact' );
	}

	public function getSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'system_message_key' );
	}

	public function getSmsSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'sms_system_message_key' );
	}

	public function getSystemEmailTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'system_email_type_id' );
	}

	public function getIsEnableSms() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_sms' );
	}

	public function getOverdueBusinessHours() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'overdue_business_hours' );
	}

	public function getIsEntrataDefault() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_entrata_default' );
	}

	public function getIsTriggerOnStatusChange() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_trigger_on_status_change' );
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getIsEnableSubject() {
		return ( int ) $this->m_boolIsEnableSubject;
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getSmsMessage() {
		return $this->m_strSmsMessage;
	}

	public function getScheduledTaskIds() {
		return $this->m_strScheduledTaskIds;
	}

	public function getEventDaysCount() {
		return $this->m_intEventDaysCount;
	}

	public function getIsEnableEmailCount() {
		return $this->m_intIsEnableEmailCount;
	}

	public function getIsEnableManualContactCount() {
		return $this->m_intIsEnableManualContactCount;
	}

	public function getManualContactInstructionsCount() {
		return $this->m_intManualContactInstructionsCount;
	}

	public function getBccEmailAddressCount() {
		return $this->m_intBccEmailAddressCount;
	}

	public function getSubjectCount() {
		return $this->m_intSubjectCount;
	}

	public function getSystemMessageTemplateIdCount() {
		return $this->m_intSystemMessageTemplateIdCount;
	}

	public function getOverdueBusinessHoursCount() {
		return $this->m_intOverdueBusinessHoursCount;
	}

	public function getAttachmentCount() {
		return $this->m_intAttachmentCount;
	}

	/**
	 * Set Functions
	 *
	 */
	public function setEventDays( $intEventDays ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'event_days', $intEventDays );
	}

	public function setEventTriggerType( $strEventTriggerType ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type', $strEventTriggerType );
	}

	public function setNumDays( $intNumDays ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'num_days', $intNumDays );
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'lease_interval_type_id', $intLeaseIntervalTypeId );
	}

	public function setPsProductId( $intPsProductId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'ps_product_id', $intPsProductId );
	}

	public function setIsEnableEmail( $boolIsEnableEmail ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_email', $boolIsEnableEmail );
	}

	public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'maintenance_status_type_id', $intMaintenanceStatusTypeId );
	}

	public function setIsEnableManualContact( $boolIsEnableManualContact ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact', $boolIsEnableManualContact );
	}

	public function setSystemMessageKey( $strSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_message_key', $strSystemMessageKey );
	}

	public function setSmsSystemMessageKey( $strSmsSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'sms_system_message_key', $strSmsSystemMessageKey );
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_email_type_id', $intSystemEmailTypeId );
	}

	public function setIsEnableSms( $boolIsEnableSms ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_sms', $boolIsEnableSms );
	}

	public function setScheduledTaskIds( $strScheduledTaskIds ) {
		$this->m_strScheduledTaskIds = preg_replace( '/\s+/', '', $strScheduledTaskIds );
	}

	public function setOverdueBusinessHours( $intOverdueBusinessHours ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'overdue_business_hours', $intOverdueBusinessHours );
	}

	public function setIsEntrataDefault( $boolIsEntrataDefault ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_entrata_default', $boolIsEntrataDefault );
	}

	public function setIsTriggerOnStatusChange( $boolIsTriggerOnStatusChange ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_trigger_on_status_change', $boolIsTriggerOnStatusChange );
	}

	public function setIsEnableSubject( $boolIsEnableSubject ) {
		$this->m_boolIsEnableSubject = $boolIsEnableSubject;
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->m_boolIsDeleted = $boolIsDeleted;
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setSmsMessage( $strSmsMessage ) {
		$this->m_strSmsMessage = $strSmsMessage;
	}

	public function setEventDaysCount( $intEventDaysCount ) {
		$this->m_intEventDaysCount = $intEventDaysCount;
	}

	public function setIsEnableEmailCount( $intIsEnableEmailCount ) {
		$this->m_intIsEnableEmailCount = $intIsEnableEmailCount;
	}

	public function setIsEnableManualContactCount( $intIsEnableManualContactCount ) {
		$this->m_intIsEnableManualContactCount = $intIsEnableManualContactCount;
	}

	public function setManualContactInstructionsCount( $intManualContactInstructionsCount ) {
		$this->m_intManualContactInstructionsCount = $intManualContactInstructionsCount;
	}

	public function setBccEmailAddressCount( $intBccEmailAddressCount ) {
		$this->m_intBccEmailAddressCount = $intBccEmailAddressCount;
	}

	public function setSubjectCount( $intSubjectCount ) {
		$this->m_intSubjectCount = $intSubjectCount;
	}

	public function setSystemMessageTemplateIdCount( $intSystemMessageTemplateIdCount ) {
		$this->m_intSystemMessageTemplateIdCount = $intSystemMessageTemplateIdCount;
	}

	public function setOverdueBusinessHoursCount( $intOverDueBusinessHoursCount ) {
		$this->m_intOverdueBusinessHoursCount = $intOverDueBusinessHoursCount;
	}

	public function setAttachmentCount( $intAttachmentCount ) {
		$this->m_intAttachmentCount = $intAttachmentCount;
	}

	/**
	 * Other Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['lease_interval_type_id'] ) && $boolDirectSet ) {
			$this->m_intLeaseIntervalTypeId = trim( $arrmixValues['lease_interval_type_id'] );
		} elseif( isset( $arrmixValues['lease_interval_type_id'] ) ) {
			$this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		}

		if( isset( $arrmixValues['is_enable_subject'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableSubject = trim( $arrmixValues['is_enable_subject'] );
		} elseif( isset( $arrmixValues['is_enable_subject'] ) ) {
			$this->setIsEnableSubject( $arrmixValues['is_enable_subject'] );
		}

		if( isset( $arrmixValues['is_deleted'] ) && $boolDirectSet ) {
			$this->m_boolIsDeleted = trim( $arrmixValues['is_deleted'] );
		} elseif( isset( $arrmixValues['is_deleted'] ) ) {
			$this->setIsDeleted( $arrmixValues['is_deleted'] );
		}

		if( isset( $arrmixValues['sms_message'] ) && $boolDirectSet ) {
			$this->m_strSmsMessage = trim( $arrmixValues['sms_message'] );
		} elseif( isset( $arrmixValues['sms_message'] ) ) {
			$this->setSmsMessage( $arrmixValues['sms_message'] );
		}

		if( isset( $arrmixValues['scheduled_task_ids'] ) && $boolDirectSet ) {
			$this->m_strScheduledTaskIds = trim( $arrmixValues['scheduled_task_ids'] );
		} elseif( isset( $arrmixValues['scheduled_task_ids'] ) ) {
			$this->setScheduledTaskIds( $arrmixValues['scheduled_task_ids'] );
		}

		if( isset( $arrmixValues['event_days_count'] ) && $boolDirectSet ) {
			$this->m_intEventDaysCount = trim( $arrmixValues['event_days_count'] );
		} elseif( isset( $arrmixValues['event_days_count'] ) ) {
			$this->setEventDaysCount( $arrmixValues['event_days_count'] );
		}

		if( isset( $arrmixValues['is_enable_email_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableEmailCount = trim( $arrmixValues['is_enable_email_count'] );
		} elseif( isset( $arrmixValues['is_enable_email_count'] ) ) {
			$this->setIsEnableEmailCount( $arrmixValues['is_enable_email_count'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableManualContactCount = trim( $arrmixValues['is_enable_manual_contact_count'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact_count'] ) ) {
			$this->setIsEnableManualContactCount( $arrmixValues['is_enable_manual_contact_count'] );
		}

		if( isset( $arrmixValues['manual_contact_instructions_count'] ) && $boolDirectSet ) {
			$this->m_intManualContactInstructionsCount = trim( $arrmixValues['manual_contact_instructions_count'] );
		} elseif( isset( $arrmixValues['manual_contact_instructions_count'] ) ) {
			$this->setManualContactInstructionsCount( $arrmixValues['manual_contact_instructions_count'] );
		}

		if( isset( $arrmixValues['bcc_email_address_count'] ) && $boolDirectSet ) {
			$this->m_intBccEmailAddressCount = trim( $arrmixValues['bcc_email_address_count'] );
		} elseif( isset( $arrmixValues['bcc_email_address_count'] ) ) {
			$this->setBccEmailAddressCount( $arrmixValues['bcc_email_address_count'] );
		}

		if( isset( $arrmixValues['subject_count'] ) && $boolDirectSet ) {
			$this->m_intSubjectCount = trim( $arrmixValues['subject_count'] );
		} elseif( isset( $arrmixValues['subject_count'] ) ) {
			$this->setSubjectCount( $arrmixValues['subject_count'] );
		}

		if( isset( $arrmixValues['system_message_template_id_count'] ) && $boolDirectSet ) {
			$this->m_intSystemMessageTemplateIdCount = trim( $arrmixValues['system_message_template_id_count'] );
		} elseif( isset( $arrmixValues['system_message_template_id_count'] ) ) {
			$this->setSystemMessageTemplateIdCount( $arrmixValues['system_message_template_id_count'] );
		}

		if( isset( $arrmixValues['overdue_business_hours_count'] ) && $boolDirectSet ) {
			$this->m_intOverdueBusinessHoursCount = trim( $arrmixValues['overdue_business_hours_count'] );
		} elseif( isset( $arrmixValues['overdue_business_hours_count'] ) ) {
			$this->setOverdueBusinessHoursCount( $arrmixValues['overdue_business_hours_count'] );
		}

		if( isset( $arrmixValues['num_days'] ) ) {
			$this->setNumDays( $arrmixValues['num_days'] );
		}

		if( isset( $arrmixValues['event_days'] ) ) {
			$this->setEventDays( $arrmixValues['event_days'] );
		}

		if( isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}

		if( isset( $arrmixValues['is_enable_email'] ) ) {
			$this->setIsEnableEmail( $arrmixValues['is_enable_email'] );
		}

		if( isset( $arrmixValues['event_trigger_type'] ) ) {
			$this->setEventTriggerType( $arrmixValues['event_trigger_type'] );
		}

		if( isset( $arrmixValues['event_sub_type_id'] ) ) {
			$this->setEventSubTypeId( $arrmixValues['event_sub_type_id'] );
		}

		if( isset( $arrmixValues['maintenance_status_type_id'] ) ) {
			$this->setMaintenanceStatusTypeId( $arrmixValues['maintenance_status_type_id'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact'] ) ) {
			$this->setIsEnableManualContact( $arrmixValues['is_enable_manual_contact'] );
		}

		if( isset( $arrmixValues['system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['system_message_key'] );
		}

		if( isset( $arrmixValues['sms_system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['sms_system_message_key'] );
		}

		if( isset( $arrmixValues['system_email_type_id'] ) ) {
			$this->setSystemEmailTypeId( $arrmixValues['system_email_type_id'] );
		}

		if( isset( $arrmixValues['overdue_business_hours'] ) ) {
			$this->setOverdueBusinessHours( $arrmixValues['overdue_business_hours'] );
		}

		if( isset( $arrmixValues['is_enable_sms'] ) ) {
			$this->setIsEnableSms( $arrmixValues['is_enable_sms'] );
		}

		if( isset( $arrmixValues['is_entrata_default'] ) ) {
			$this->setIsEntrataDefault( $arrmixValues['is_entrata_default'] );
		}

		if( isset( $arrmixValues['is_trigger_on_status_change'] ) ) {
			$this->setIsTriggerOnStatusChange( $arrmixValues['is_trigger_on_status_change'] );
		}
	}

	public function validate( $strAction, $objClient = NULL, $objDatabase = NULL, $boolValidateGroupCount = false ) {
		$boolIsValid = true;
		$objClient = $objClient;
		$objDatabase = $objDatabase;
		$boolValidateGroupCount = $boolValidateGroupCount;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( self::EVENT_TRIGGER_TYPE_IMMEDIATE != trim( $this->getEventTriggerType() ) && self::EVENT_TRIGGER_TYPE_DAY_OF_EVENT != trim( $this->getEventTriggerType() ) ) {
					if( false == is_numeric( $this->getEventDays() ) || 0 >= $this->getEventDays() || true == is_float( $this->getEventDays() ) ) {
						$this->addErrorMsg( __( 'Enter positive numeric values only.' ) );
						$boolIsValid = false;
					}
				}

				if( true == valId( $this->getIsEnableManualContact() ) && false == valId( $this->getOverdueBusinessHours() ) ) {
					$this->addErrorMsg( __( 'Consider Overdue After must be a positive value.' ) );
					$boolIsValid = false;
				}
				break;

			default:
				// default action will go here
				break;
		}

		return $boolIsValid;
	}

	public function execute( $intCompanyUserId, $objDatabase, $objRecipient = NULL, $boolVerbose = false ) {
		$this->m_boolIsVerbose 	= $boolVerbose;

		// Use this function for print log.
		$this->log( '-----------------------------------------------------------------------------------------------------------------' );
		$this->log( 'Processing scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ] ' );
		$this->log( '-----------------------------------------------------------------------------------------------------------------' );

		// Check property product permission
		$arrintPermissionedPsProductIds	= [ CPsProduct::MESSAGE_CENTER, CPsProduct::ENTRATA_COMMERCIAL, CPsProduct::ENTRATA ];

		if( false == valId( $this->getPropertyId() ) ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'No property id found for this scheduled task so skipping task from processing.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );

			return;
		}

		if( true == \Psi\Eos\Entrata\CProperties::createService()->fetchIsPropertyMigratedByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase ) ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'Property is in migration mode. So communication from contact points is disabled.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );

			return;
		}

		$this->m_arrintPsProductIds 	= \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPsProductPermissionsByPsProductIdsByPropertyIdsByCid( $arrintPermissionedPsProductIds, [ $this->getPropertyId() ], $this->getCid(), $objDatabase );

		if( false == valArr( $this->m_arrintPsProductIds ) || ( false == array_key_exists( CPsProduct::MESSAGE_CENTER, $this->m_arrintPsProductIds ) && self::EVENT_TRIGGER_TYPE_BEFORE != $this->getEventTriggerType() ) ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'Message center has been disabled for this property.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );

			return;
		}

		if( 0 == $this->getIsEnableEmail() ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'Either system message id not found or email feature is disabled, so skipping task from processing.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );

			return;
		}

		$intDaysInterval = 0;

		// For immediate scheduled tasks
		if( false == is_null( $objRecipient ) ) {
			$this->m_boolIsResident = ( valObj( $objRecipient, CCustomerConnection::class ) || valObj( $objRecipient, CLeaseCustomer::class ) );
			$arrobjRecipients[] = $objRecipient;
		} else {

			if( self::EVENT_TRIGGER_TYPE_IMMEDIATE == $this->getEventTriggerType() ) {
				echo 'immediate';

				return;
			}

			if( true == is_numeric( $this->getEventDays() ) ) {
				$intDaysInterval = ( self::EVENT_TRIGGER_TYPE_BEFORE == $this->getEventTriggerType() ) ? - $this->getEventDays() : $this->getEventDays();
			}

			switch( $this->getEventTypeId() ) {

				case CEventType::LEASE_START:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchLeaseStartCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::MOVE_IN:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchMoveInCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::MOVE_OUT:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchMoveOutCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::LEASE_END:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchLeaseExpirationCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::COMMERCIAL_INSURANCE_POLICY:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchInsuranceExpirationCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::QUOTE:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchRenewalOptionClosedCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					$this->m_boolIsResident = true;
					break;

				case CEventType::RENEWAL_OPTION_OPEN:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchRenewalOptionOpenCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					$this->m_boolIsResident = true;
					break;

				case CEventType::COMMERCIAL_LEASE:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchRentCommencementCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					$this->m_boolIsResident = true;
					break;

				case CEventType::COMMERCIAL_UNINSURED_TENANTS:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchInsuranceExpiredCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					$this->m_boolIsResident = true;
					break;

				case CEventType::RENT_ESCALATIONS:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchRentEscalationsCommercialCustomerConnectionsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
					$this->m_boolIsResident = true;
					break;

				case CEventType::EXPIRING_INSURANCE_AGENT_NOTIFICATION:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCommercialInsurancePolicies::createService()->fetchExpiredCommercialInsurancePoliciesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval, true );
					$this->m_boolIsInsuranceAgent = true;
					break;

				case CEventType::EXPIRED_INSURANCE_AGENT_NOTIFICATION:
					$arrobjRecipients = ( array ) \Psi\Eos\Entrata\CCommercialInsurancePolicies::createService()->fetchExpiredCommercialInsurancePoliciesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					$this->m_boolIsInsuranceAgent = true;
					break;

				default:
					$arrobjRecipients = [];
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjRecipients ) ) {
			$this->processMessage( $arrobjRecipients, $intCompanyUserId, $objDatabase );

			$arrobjRecipients = NULL;
			unset( $arrobjRecipients );
		}
	}

	public function processMessage( $arrobjRecipients, $intCompanyUserId, $objDatabase ) {
		if( 1 == $this->getIsEnableEmail() && true == valStr( $this->getSystemMessageKey() ) ) {

			$objSendSystemMessage = new \Psi\Libraries\SystemMessages\CSendSystemMessage();
			$objSendSystemMessage->setDatabase( $objDatabase );
			$objSendSystemMessage->setPropertyId( $this->getPropertyId() );
			$objSendSystemMessage->setCid( $this->getCid() );
			$objSendSystemMessage->setMetaData( 'system_email_type_id', CSystemEmailType::EVENT_SCHEDULER_EMAIL );
			$objSendSystemMessage->setKey( $this->getSystemMessageKey() );
			$objSendSystemMessage->setScheduledTaskId( $this->getId() );
			$objSendSystemMessage->setMetaData( 'is_commercial', true );

			$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchSimpleClientById( $this->getCid(), $objDatabase );

			if( true == valObj( $objClient, CClient::class ) && CCompanyStatusType::CLIENT != $objClient->getCompanyStatusTypeId() ) {
				$objSendSystemMessage->setSystemMessagePriority( CSystemMessage::SYSTEM_MESSAGE_QUEUE_TEST_CLIENT_PRIORITY );
			} else if( CEventTriggerScheduleType::IMMEDIATE_VALUE == $this->getEventTriggerType() ) {
				$objSendSystemMessage->setSystemMessagePriority( CSystemMessage::SYSTEM_MESSAGE_QUEUE_IMMEDIATE_PRIORITY );
			}

			if( true == $this->m_boolIsResident ) {
				$objSendSystemMessage->setSystemMessageAudienceId( CSystemMessageAudience::RESIDENT );
				$this->processResidentMessageEmail( $arrobjRecipients, $intCompanyUserId, $objSendSystemMessage );
			} elseif( true == $this->m_boolIsInsuranceAgent ) {
				$objSendSystemMessage->setSystemMessageAudienceId( CSystemMessageAudience::INSURANCE_AGENT );
				$this->processInsuranceAgentMessageEmail( $arrobjRecipients, $intCompanyUserId, $objDatabase );
			} else {
				$objSendSystemMessage->setSystemMessageAudienceId( CSystemMessageAudience::LEAD );
				$this->processLeadMessageEmail( $arrobjRecipients, $intCompanyUserId, $objSendSystemMessage, $objDatabase );
			}
		}
	}

	public function processResidentMessageEmail( $arrobjCustomers, $intCompanyUserId, $objSendSystemMessage ) {

		if( true == valId( ( int ) $this->getSystemEmailTypeId() ) && false == $this->getIsSendEmailToResident() ) {
			return;
		}

		$arrmixSystemMessageMetaData = ( array ) $this->getSystemMessageMetaData();

		$arrobjEmailAttachments = ( array ) $this->createFileAttachments();

		foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
			$objSendSystemMessage->addEmailAttachment( $objEmailAttachment );
		}

		foreach( $arrobjCustomers as $objCustomer ) {

			$objSendQueueSystemMessage = clone $objSendSystemMessage;
			$objSendQueueSystemMessage->setMetaData( 'lease_id', $objCustomer->getLeaseId() );
			$objSendQueueSystemMessage->setMetaData( 'property_id', $objCustomer->getPropertyId() );
			$objSendQueueSystemMessage->setPreferredLocaleCode( $objCustomer->getPreferredLocaleCode() );
			if( true == valObj( $objCustomer, CCustomerConnection::class ) ) {
				$objSendQueueSystemMessage->setMetaData( 'lease_customer_id', $objCustomer->getLeaseCustomerId() );
				$objSendQueueSystemMessage->setMetaData( 'customer_id', $objCustomer->getCustomerId() );
			} else {
				$objSendQueueSystemMessage->setMetaData( 'lease_customer_id', $objCustomer->getCustomerId() );
				$objSendQueueSystemMessage->setMetaData( 'application_id', $objCustomer->getApplicationId() );
			}

			if( true == array_key_exists( 'application_id', $arrmixSystemMessageMetaData ) ) {
				$objSendQueueSystemMessage->setMetaData( 'application_id', $arrmixSystemMessageMetaData['application_id'] );
			}

			if( false == $objSendQueueSystemMessage->execute( $intCompanyUserId ) ) {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'Unable to process system message for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			} else {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'System Message process successfully for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			}
		}

		if( true == $this->removeFileAttachments() ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'Temporary file attachments are removed.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
		}
	}

	public function processLeadMessageEmail( $arrobjApplications, $intCompanyUserId, $objSendSystemMessage, $objDatabase ) {

		if( true == valId( ( int ) $this->getSystemEmailTypeId() ) && false == $this->getIsSendEmailToProspect() ) {
			return;
		}

		foreach( $arrobjApplications as $objApplication ) {
			$objSendQueueSystemMessage = clone $objSendSystemMessage;
			$objSendQueueSystemMessage->setMetaData( 'applicant_application_id', $objApplication->getApplicantApplicationId() );
			$objSendQueueSystemMessage->setMetaData( 'application_id', $objApplication->getId() );
			$objSendQueueSystemMessage->setMetaData( 'property_id', $objApplication->getPropertyId() );
			$objSendQueueSystemMessage->setPreferredLocaleCode( $objApplication->getPreferredLocaleCode() );

			$boolIsRepresentativePrimaryContact = ( bool ) ( $objApplication->getIsRepresentativePrimaryContact() ?? false );

			if( true == $boolIsRepresentativePrimaryContact && true == valId( ( int ) $objApplication->getRepresentativeCustomerContactId() ) ) {
				$objCustomerConnection = \Psi\Eos\Entrata\CCustomerConnections::createService()->fetchCustomerConnectionsByCustomerIdByCid( ( int ) $objApplication->getRepresentativeCustomerContactId(), $objApplication->getCid(), $objDatabase );
				if( true === valObj( $objCustomerConnection, 'CCustomerConnection' ) && true == valStr( $objCustomerConnection->getEmailAddress() ) ) {
					$objSendQueueSystemMessage->setMetaData( 'lead_representative_email_address', $objCustomerConnection->getEmailAddress() );
					$objSendQueueSystemMessage->setMetaData( 'primary_customer_contact_id', $objCustomerConnection->getId() );
				}
			}

			if( false == $objSendQueueSystemMessage->execute( $intCompanyUserId ) ) {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'Unable to process system message for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			} else {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'System Message process successfully for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			}

		}

		if( true == $this->removeFileAttachments() ) {
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			$this->log( 'Temporary file attachments are removed.' );
			$this->log( '-----------------------------------------------------------------------------------------------------------------' );
		}
	}

	public function processInsuranceAgentMessageEmail( $arrobjCommercialInsurancePolicies, $intCompanyUserId, $objDatabase ) {

		$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setClient( $objClient );
		$objSystemMessageLibrary->setDatabase( $objDatabase );
		$objSystemMessageLibrary->setSystemMessageAudienceIds( [ CSystemMessageAudience::INSURANCE_AGENT ] );
		$objSystemMessageLibrary->setCompanyUserId( $intCompanyUserId );
		$objSystemMessageLibrary->setIsResident( false );
		$objSystemMessageLibrary->setSystemEmailTypeId( CSystemEmailType::EVENT_SCHEDULER_EMAIL );
		$objSystemMessageLibrary->setSystemMessageKey( $this->getSystemMessageKey() );
		$objSystemMessageLibrary->setPropertyId( $this->getPropertyId() );

		foreach( $arrobjCommercialInsurancePolicies as $objCommercialInsurancePolicy ) {
			if( false == CValidation::validateEmailAddresses( trim( $objCommercialInsurancePolicy->getAgentEmail() ) ) ) {
				continue;
			}

			$objClonedSystemMessageLibrary = clone $objSystemMessageLibrary;
			$objClonedSystemMessageLibrary->setPropertyId( $objCommercialInsurancePolicy->getPropertyId() );
			$objClonedSystemMessageLibrary->setToEmailAddresses( [ $objCommercialInsurancePolicy->getAgentEmail() ] );
			$objClonedSystemMessageLibrary->setExtraMetaData( [ 'lease_id' => $objCommercialInsurancePolicy->getLeaseId(), 'property_id' => $objCommercialInsurancePolicy->getPropertyId(), 'commercial_insurance_policy_id' => $objCommercialInsurancePolicy->getId() ] );

			if( false == $objClonedSystemMessageLibrary->execute() ) {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'Unable to process system message for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			} else {
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
				$this->log( 'System Message process successfully for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getId() . ' ]' );
				$this->log( '-----------------------------------------------------------------------------------------------------------------' );
			}
		}
	}

	public function getEventCaption() {

		switch( $this->getEventSubTypeId() ) {
			case CEventSubType::LEASE_EXPIRED_FOLLOW_UP:
				return $this->getEventTriggerCaption() . ' Lease Expiration';
				break;

			case CEventSubType::RENT_ESCALATIONS:
				return $this->getEventTriggerCaption() . ' Rent Step Start';
				break;

			case CEventSubType::EXPIRING_INSURANCE:
				return $this->getEventTriggerCaption() . ' Insurance Expiration';
				break;

			default:
				return '';
		}
	}

	public function getEventTriggerCaption() {

		switch( $this->getEventTriggerType() ) {
			case CLeaseCustomerScheduledTask::EVENT_TRIGGER_TYPE_AFTER:
				return 'Day(s) After';
				break;

			case CLeaseCustomerScheduledTask::EVENT_TRIGGER_TYPE_IMMEDIATE:
				return 'Immediately After';
				break;

			case CLeaseCustomerScheduledTask::EVENT_TRIGGER_TYPE_BEFORE:
				return 'Day(s) Before';
				break;

			case CLeaseCustomerScheduledTask::EVENT_TRIGGER_TYPE_DAY_OF_EVENT:
				return 'Day of';
				break;

			default:
				return '';
		}
	}

	private function log( $strMessage ) {
		if( true == $this->m_boolIsVerbose ) {
			echo $strMessage . "\n";
		}
	}

}
