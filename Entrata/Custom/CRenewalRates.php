<?php

use Psi\Eos\Entrata\CRateLogs;

class CRenewalRates extends CRateLogs {

	public static function fetchPetRatesByLease( $objLease, $objDatabase, $intArOriginReferenceId = NULL ) {

		$strSqlCondition = ( true == valId( $intArOriginReferenceId ) ) ? ' AND pr.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId : '';

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $objLease->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $objLease->getPropertyId() ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $objLease->getUnitSpaceId() ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::PET ] );
		$objRateLogsFilter->setLoadOptionalRates( true );

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		$strSql = 'SELECT
						pr.*
					FROM
						prospect_rates pr
					WHERE
						pr.cid = ' . ( int ) $objLease->getCid() . $strSqlCondition;

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase );
	}

	public static function fetchMarketRatesByLease( $objLease, $objDatabase ) {

		// Here the market value of the other charges will be fetched only when the
		// use_lease_terms_for_others is turned on in the property_ar_origin_rules
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $objLease->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $objLease->getPropertyId() ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $objLease->getUnitSpaceId() ] );
		$objRateLogsFilter->setLeaseIntervalIds( [ $objLease->getActiveLeaseIntervalId() ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		$strSql = 'SELECT
						pr.*
					FROM
						prospect_rates pr
					WHERE
						pr.cid = ' . ( int ) $objLease->getCid() . '
						AND CASE
							WHEN 0 < pr.lease_term_id
								THEN pr.lease_term_id = ' . ( int ) $objLease->getLeaseTermId() . '
							ELSE
								TRUE
							END';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase );
	}

	public static function fetchAllRenewalRentByLease( $objLease, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		$strWhereCondittion = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $objLease->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $objLease->getPropertyId() ] );
		$objRateLogsFilter->setUnitTypeIds( [ $objLease->getUnitTypeId() ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $objLease->getUnitSpaceId() ] );
		$objRateLogsFilter->setArTriggerIds( [ CArTrigger::MONTHLY ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE, CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setLeaseIntervalIds( [ $objLease->getActiveLeaseIntervalId() ] );
		$objRateLogsFilter->setPropertyRatesOnly( true );
		$objRateLogsFilter->setIsRenewal( true );
		$objRateLogsFilter->setMoveInDate( self::calculateRenewalMoveInDate( $objLease ) );

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						rr.*,
						lt.name AS lease_term_name
					FROM
						renewal_rates rr
						JOIN lease_terms lt ON( lt.cid = rr.cid AND lt.id = rr.lease_term_id )
					WHERE
						lt.cid = ' . ( int ) $objLease->getCid() . '
						AND lt.deleted_by IS NULL
						AND lt.deleted_on IS NULL
						AND lt.is_renewal IS TRUE' . $strWhereCondittion;

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase );
	}

	public static function fetchAllRenewalRentByLeasesByCid( $arrobjLeases, $intCid, $objDatabase, $objOfferTemplate = NULL ) {

		self::fetchRenewalRatesByLeasesByCid( $arrobjLeases, $intCid, $objDatabase );

		$arrobjRenewalTierSpecialRates = self::fetchRenewalSpecialsByLeasesByCid( $arrobjLeases, $intCid, $objDatabase, $strRenewalTransferLeaseEndCondition = 'cl.lease_end_date', $boolIncludeCommercialLeaseTerms = false, $objOfferTemplate = NULL );

		$arrintLeaseWiseLeaseStartWindows	= [];
		$arrobjTempRenewalRates				= [];

		if( true == valArr( $arrobjRenewalTierSpecialRates ) ) {

			foreach( $arrobjRenewalTierSpecialRates As $intKey => $objRateLog ) {

				if( false == array_key_exists( $objRateLog->getLeaseId(), $arrobjTempRenewalRates ) ) {
					$arrobjTempRenewalRates[$objRateLog->getLeaseId()]			= [];
					$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers']	= [];
				}

				if( true == valId( $objRateLog->getLeaseId() ) && false == array_key_exists( 'tiers', $arrobjTempRenewalRates[$objRateLog->getLeaseId()] ) ) {
					$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers']		= [];
					$arrintLeasesWithTierSpecialRates[$objRateLog->getLeaseId()]	= $objRateLog->getLeaseId();
				}

				if( true == valId( $objRateLog->getArOriginReferenceId() ) && false == array_key_exists( $objRateLog->getArOriginReferenceId(), $arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'] ) ) {
					$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'][$objRateLog->getArOriginReferenceId()] = [];
				}

				$strRateKey = $objRateLog->getLeaseTermId();
				if( true == valId( $objRateLog->getLeaseStartWindowId() ) ) {
					$strRateKey .= '_' . $objRateLog->getLeaseStartWindowId();
				}

				if( true == valId( $objRateLog->getArOriginReferenceId() )
					&& true === valArr( $arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'] )
					&& false == array_key_exists( $strRateKey, $arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'][$objRateLog->getArOriginReferenceId()] ) ) {
					$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'][$objRateLog->getArOriginReferenceId()][$strRateKey] = [];
				}

				$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['tiers'][$objRateLog->getArOriginReferenceId()][$strRateKey][$objRateLog->getArCodeId()] = $objRateLog;

				$intLeaseId = $objRateLog->getLeaseId();
				if( false == array_key_exists( $intLeaseId, $arrintLeaseWiseLeaseStartWindows ) ) {
					$arrintLeaseWiseLeaseStartWindows[$intLeaseId]['lease_start_windows']	= [];
					$arrintLeaseWiseLeaseStartWindows[$intLeaseId]['is_student_tier']		= false;
				}

				if( true == valId( $objRateLog->getLeaseStartWindowId() ) ) {

					if( CSpecialGroupType::CONVENTIONAL_LEASING != $objRateLog->getSpecialGroupTypeId() ) {
						$arrintLeaseWiseLeaseStartWindows[$intLeaseId]['lease_start_windows'][$objRateLog->getLeaseStartWindowId()] = $objRateLog->getLeaseStartWindowId();
						$arrintLeaseWiseLeaseStartWindows[$intLeaseId]['is_student_tier'] = true;
					}
				}
			}

			$arrobjRenewalTierSpecialRates = NULL;
		}

		$arrobjRenewalBaseRates = self::fetchRenewalBaseRentsByLeasesByCid( $arrobjLeases, $intCid, $objDatabase, $boolLeftJoinForRates = true );

		if( false == valArr( $arrobjRenewalBaseRates ) ) return [];

		if( false === valArr( $arrobjTempRenewalRates ) ) $arrobjTempRenewalRates = [];

		foreach( $arrobjRenewalBaseRates As $intKey => $objRateLog ) {

			if( false == valId( $objRateLog->getRateId() ) ) continue;

			if( false == array_key_exists( $objRateLog->getLeaseId(), $arrobjTempRenewalRates ) ) {
				$arrobjTempRenewalRates[$objRateLog->getLeaseId()] = [];
			}

			if( true == valId( $objRateLog->getLeaseId() ) && false == array_key_exists( 'base_rent', $arrobjTempRenewalRates[$objRateLog->getLeaseId()] ) ) {
				$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['base_rent'] = [];
			}

			$strRateKey = $objRateLog->getArCodeId() . '_' . $objRateLog->getLeaseTermId();
			if( true == valId( $objRateLog->getLeaseStartWindowId() ) ) {
				$strRateKey .= '_' . $objRateLog->getLeaseStartWindowId();
			}

			if( true == valId( $objRateLog->getLeaseStartWindowId() )
				&& true == array_key_exists( $objRateLog->getLeaseId(), $arrintLeaseWiseLeaseStartWindows ) ) {
				$arrmixLeaseStartWindows = $arrintLeaseWiseLeaseStartWindows[$objRateLog->getLeaseId()];
				$boolIsStudentTier       = $arrmixLeaseStartWindows['is_student_tier'];
				$arrintLeaseStartWindows = $arrmixLeaseStartWindows['lease_start_windows'];
				if( true == $boolIsStudentTier
					&& false == array_key_exists( $objRateLog->getLeaseStartWindowId(), $arrintLeaseStartWindows ) ) continue;
			}

			$arrobjTempRenewalRates[$objRateLog->getLeaseId()]['base_rent'][$strRateKey] = $objRateLog;
		}

		$arrobjRenewalBaseRates = NULL;

		return $arrobjTempRenewalRates;

	}

	public static function calculateRenewalMoveInDate( $objLease ) {

		if( CLeaseIntervalType::MONTH_TO_MONTH == $objLease->getLeaseIntervalTypeId() ) {
			return $strMoveInDate = date( 'Y-m-d' );
		} elseif( false == valId( $objLease->getTransferredFromLeaseId() )
					&& true == valStr( $objLease->getLeaseEndDate() )
					&& true == CValidation::validateDate( $objLease->getLeaseEndDate() ) ) {

			return $strMoveInDate = date( 'Y-m-d', strtotime( '+1 day', strtotime( $objLease->getLeaseEndDate() ) ) );
		} elseif( true == valId( $objLease->getTransferredFromLeaseId() )
					&& true == valStr( $objLease->getMoveInDate() )
					&& true == CValidation::validateDate( $objLease->getMoveInDate() ) ) {

			return $strMoveInDate = date( 'Y-m-d', strtotime( $objLease->getMoveInDate() ) );
		}

		return $strMoveInDate = date( 'Y-m-d' );
	}

	public static function fetchRenewalRatesByLeasesByCid( $arrobjLeases, $intCid, $objDatabase ) {

		$arrintLeaseIntervalIds 	= ( array ) getObjectFieldValuesByFieldName( 'ActiveLeaseIntervalId', $arrobjLeases );
		$arrintPropertyIds			= ( array ) getObjectFieldValuesByFieldName( 'PropertyId', $arrobjLeases );

		$arrintOriginIds = [ CArOrigin::BASE, CArOrigin::SPECIAL ];

		$arrintArTriggerIds = [
			CArTrigger::PRE_QUALIFICATION,
			CArTrigger::APPLICATION_COMPLETED,
			CArTrigger::APPLICATION_APPROVAL,
			CArTrigger::LEASE_COMPLETED,
			CArTrigger::LEASE_APPROVAL,
			CArTrigger::MOVE_IN,
			CArTrigger::LAST_MONTH_OF_OCCUPANCY,
			CArTrigger::NOTICE,
			CArTrigger::MOVE_OUT,
			CArTrigger::END_OF_LEASE_TERM,
			CArTrigger::END_OF_CALENDAR_YEAR,
			CArTrigger::FINAL_STATEMENT,
			CArTrigger::RENEWAL_LEASE_APPROVAL,
			CArTrigger::RENEWAL_START,
			CArTrigger::MONTHLY,
			CArTrigger::DAILY,
			CArTrigger::WEEKLY,
			CArTrigger::NIGHTLY,
			CArTrigger::EARLY_TERMINATION,
			CArTrigger::RETURN_ITEM_FEE,
			CArTrigger::MONTH_TO_MONTH_BEGIN,
			CArTrigger::PROCESS_TRANSFER,
			CArTrigger::SCHEDULE_TRANSFER,
			CArTrigger::WORK_ORDER_FEES,
			CArTrigger::RENEWAL_OFFER_ACCEPTED
		];

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setArTriggerIds( $arrintArTriggerIds );
		$objRateLogsFilter->setArOriginIds( $arrintOriginIds );
		$objRateLogsFilter->setLeaseIntervalIds( $arrintLeaseIntervalIds );
		$objRateLogsFilter->setPublishedUnitsOnly( false );
		$objRateLogsFilter->setIsRenewal( true );
		$objRateLogsFilter->setIsEntrata( true );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		return true;
	}

	public static function fetchMonthToMonthFeeRatesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						DISTINCT ON ( l.id )
						pr.*,
						l.id AS lease_id,
						rounder( pr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ) AS rate_amount
					FROM
						prospect_rates pr
						JOIN leases l ON ( l.cid = pr.cid AND l.property_id = pr.property_id )
						JOIN cached_applications ca ON ( ca.cid = pr.cid AND ca.property_id = pr.property_id AND l.active_lease_interval_id = ca.lease_interval_id AND l.id = ca.lease_id )
						JOIN property_charge_settings  pcs ON ( pcs.cid = l.cid AND pcs.property_id = l.property_id )
						JOIN ar_codes ac ON ( ac.cid = pcs.cid AND ac.id = pr.ar_code_id )
					WHERE
						pr.cid = ' . ( int ) $intCid . '
						AND l.id =  ' . ( int ) $intLeaseId . '
						AND pr.ar_trigger_id = ' . CArTrigger::MONTH_TO_MONTH_BEGIN . '
						AND CASE
							WHEN 0 < pr.lease_term_id
								THEN pr.lease_term_id = ca.lease_term_id
							ELSE
								TRUE
							END';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchMonthToMonthRentRatesByLeaseIdByCid( $objLease, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						DISTINCT ON ( cl.id )
						rr.*,
						cl.id AS lease_id,
						rounder( rr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ) AS rate_amount
					FROM
						renewal_rates rr
						JOIN property_gl_settings pgs ON ( rr.cid = pgs.cid AND pgs.property_id = rr.property_id AND pgs.rent_ar_code_id IS NOT NULL )
						JOIN cached_leases cl ON ( cl.cid = rr.cid AND cl.property_id = rr.property_id AND rr.lease_interval_id = cl.active_lease_interval_id )
						JOIN property_charge_settings pcs ON ( pcs.cid = cl.cid AND pcs.property_id = cl.property_id AND pcs.month_to_month_type_id = ' . ( int ) CMonthToMonthType::UPDATE_ONLY_BASE_RENT_TO_MARKET . ' )
						JOIN cached_applications ca ON ( ca.cid = rr.cid AND ca.property_id = rr.property_id AND cl.active_lease_interval_id = ca.lease_interval_id AND cl.id = ca.lease_id )
						JOIN ar_codes ac ON( ac.cid = ca.cid AND ac.id = rr.ar_code_id )
					WHERE
						rr.cid = ' . ( int ) $intCid . '
						AND cl.id = ' . ( int ) $objLease->getId() . '
						AND rr.month_to_month_multiplier > 0
						AND rr.rate_amount > 0
						AND rr.ar_code_id = pgs.rent_ar_code_id
						AND CASE
								WHEN pcs.default_mtm_multiplier_lease_term_id > 0 AND rr.lease_term_id > 0
									THEN rr.lease_term_id = pcs.default_mtm_multiplier_lease_term_id
								WHEN rr.lease_term_id > 0
									THEN rr.lease_term_id = ca.lease_term_id
								ELSE TRUE
							END';

		if( self::checkForRenewalTransfer( [ $objLease ] )
			&& true === valObj( $objLease, 'CLease' )
			&& true === valId( $objLease->getUnitSpaceId() ) ) {
			$strSql .= ' AND rr.unit_space_id = ' . $objLease->getUnitSpaceId();
		}

		$arrobjRateLogs = CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );

		if( true == valArr( $arrobjRateLogs ) ) return $arrobjRateLogs;

		$strUnitSpaceCondition = ' pr.unit_space_id = cl.unit_space_id';

		if( self::checkForRenewalTransfer( [ $objLease ] )
			&& true === valObj( $objLease, 'CLease' )
			&& true === valId( $objLease->getUnitSpaceId() ) ) {
			$strUnitSpaceCondition = ' pr.unit_space_id = ' . $objLease->getUnitSpaceId();
		}

		$strSql = 'SELECT
						DISTINCT ON ( cl.id )
						pr.*,
						cl.id AS lease_id,
						rounder( pr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ) AS rate_amount
					FROM
						prospect_rates pr
						JOIN property_gl_settings pgs ON ( pr.cid = pgs.cid AND pgs.property_id = pr.property_id AND pgs.rent_ar_code_id IS NOT NULL )
						JOIN cached_leases cl ON ( cl.cid = pr.cid AND cl.property_id = pr.property_id AND ' . $strUnitSpaceCondition . ' )						JOIN property_charge_settings pcs ON ( pcs.cid = cl.cid AND pcs.property_id = cl.property_id AND pcs.month_to_month_type_id = ' . ( int ) CMonthToMonthType::UPDATE_ONLY_BASE_RENT_TO_MARKET . ' )
						JOIN cached_applications ca ON ( ca.cid = pr.cid AND ca.property_id = pr.property_id AND cl.active_lease_interval_id = ca.lease_interval_id AND cl.id = ca.lease_id )
						JOIN ar_codes ac ON( ac.cid = ca.cid AND ac.id = pr.ar_code_id )
					WHERE
						pr.cid = ' . ( int ) $intCid . '
						AND cl.id = ' . ( int ) $objLease->getId() . '
						AND pr.month_to_month_multiplier > 0
						AND pr.ar_code_id = pgs.rent_ar_code_id
						AND CASE
								WHEN pcs.default_mtm_multiplier_lease_term_id > 0 AND pr.lease_term_id > 0
									THEN pr.lease_term_id = pcs.default_mtm_multiplier_lease_term_id
								WHEN pr.lease_term_id > 0
									THEN pr.lease_term_id = ca.lease_term_id
								ELSE TRUE
							END';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchRenewalBaseRentsByLeasesByCid( $arrobjLeases, $intCid, $objDatabase, $boolLeftJoinForRates = true, $boolIncludeCommercialLeaseTerms = false, $boolIsFromScheduledTransfer = false, $boolAllowLeaseExpirationLimit = false, $boolIsCrossPropertyTransfer = false, $objProperty = NULL, $strRenewalStartDate = NULL ) {

		if( false === valArr( $arrobjLeases ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) return [];

		$arrintLeaseIds = array_keys( $arrobjLeases );

		$arrintPropertyIds = ( array ) getObjectFieldValuesByFieldName( 'PropertyId', $arrobjLeases );
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		// Custom renewal transfer conditions
		$boolIsRenewalTransfer  = self::checkForRenewalTransfer( $arrobjLeases );
		$strRenewalTransferUnitSpaceCondition = NULL;
		if( true === $boolIsRenewalTransfer && false === $boolIsFromScheduledTransfer ) {
			$strRenewalTransferUnitSpaceCondition = self::getRenewalTransferCondition( $arrobjLeases, true );
		}

		$strSqlRenewalTransferLeftJoinCondition = ( true === $boolIsRenewalTransfer ) ? ' LEFT ' : '';
		$strRatesTableName = ( true === $boolIsRenewalTransfer ) ? ' prospect_rates ' : ' renewal_rates ';

		$strScheduledTransferLeaseTermCondition = NULL;
		$strScheduledTransferLeaseStartWindowCondition = NULL;
		$strPropertyRentChargeCodeCondition = NULL;
		$strCompareSpaceConfiguration        = ' ca.space_configuration_id ';
		$strCompareDesiredSpaceConfiguration = ' ca.desired_space_configuration_id ';
		$strPropertyTypeCondition            = ' rr.is_student_property = 1 ';

		$strRenewalLeaseTermJoinCondition		= ' AND lt.is_renewal IS TRUE ';
		$strLeaseTermDeletedCondition			= ' AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL AND lt.is_disabled IS FALSE ';
		$strLeaseStartWindowDeletedCondition	= ' AND lsw.deleted_by IS NULL AND lsw.deleted_on IS NULL AND lsw.is_active IS TRUE ';
		$strLeaseTermStructureCondition			= ' AND pcs.lease_term_structure_id = lt.lease_term_structure_id ';
		$strLeaseStartStructureCondition		= ' AND lsw.lease_start_structure_id = pcs.lease_start_structure_id ';
		$strPropertyColumn						= 'cl.property_id';
		$strNewPropertyCondition				= 'AND paor.property_id = cl.property_id';
		$strPropertyGlSettingsCondition			= 'AND pcs.property_id = pgs.property_id';
		$strLeaseEndDate						= 'cl.lease_end_date + 1';
		if( true == valStr( $strRenewalStartDate ) ) {
			$strLeaseEndDate = '\'' . $strRenewalStartDate . '\'::DATE';
		}
		if( true == $boolIsFromScheduledTransfer ) {
			$strRenewalLeaseTermJoinCondition = NULL;
			$strLeaseTermDeletedCondition = NULL;
			$strLeaseStartWindowDeletedCondition = NULL;
			if( true == $boolIsCrossPropertyTransfer ) {
				$strLeaseTermStructureCondition  = '';
				$strLeaseStartStructureCondition = '';
				$strPropertyColumn				 = 'rr.property_id';
				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$strNewPropertyCondition = 'AND paor.property_id =' . $objProperty->getId() . ' ';
					$strPropertyGlSettingsCondition = 'AND pgs.property_id =' . $objProperty->getId() . ' ';
				}
			}
			$arrintLeaseTermIds = ( array ) getObjectFieldValuesByFieldName( 'LeaseTermId', $arrobjLeases );
			if( true === valArr( array_filter( $arrintLeaseTermIds ) ) ) {
				$strScheduledTransferLeaseTermCondition = ' AND lt.id IN( ' . sqlIntImplode( $arrintLeaseTermIds ) . ' ) ';
			} else {
				$strScheduledTransferLeaseTermCondition = ' AND lt.term_month IN( ' . CLeaseTerm::LEASE_TERM_STANDARD . ' ) ';
			}
			$arrintLeaseStartWindowIds = ( array ) getObjectFieldValuesByFieldName( 'LeaseStartWindowId', $arrobjLeases );
			if( true === valArr( array_filter( $arrintLeaseStartWindowIds ) ) ) {
				$strScheduledTransferLeaseStartWindowCondition = ' AND lsw.id IN( ' . sqlIntImplode( $arrintLeaseStartWindowIds ) . ' ) ';
			}

			if( true == valId( current( $arrobjLeases )->getSpaceConfigurationId() ) ) {
				$strCompareDesiredSpaceConfiguration = current( $arrobjLeases )->getSpaceConfigurationId();
				$strCompareSpaceConfiguration        = current( $arrobjLeases )->getSpaceConfigurationId();
			}

			$strPropertyTypeCondition = ' rr.is_student_property = 1 ';
		}

		$strUnitTypeCase = '';
		$strPropertyFloorPlanCase = '';
		$strUnitTypeJoinCondition = '';
		$strUnitTypeIdCondition = '';
		$strSpaceConfigurationIdColumn = 'ca.space_configuration_id';

		$strUnitSpaceJoinCondition = 'JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = cl.unit_space_id )';
		$strUnitSpaceColumn = 'COALESCE( rr.unit_space_id, cl.unit_space_id ) AS unit_space_id, ';

		if( true === $boolIsRenewalTransfer || true === $boolIsFromScheduledTransfer ) {
			$objLease = reset( $arrobjLeases );
			if( true === valId( $objLease->getUnitSpaceId() ) ) {
				$strUnitSpaceColumn = $objLease->getUnitSpaceId() . ' AS unit_space_id, ';
				$strUnitSpaceJoinCondition = 'JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = ' . $objLease->getUnitSpaceId() . ' )';
			}
		}

		$strSpaceConfigurationCase = 'CASE
										WHEN paor.use_space_configurations_for_rent IS TRUE AND ' . $strPropertyTypeCondition . '
										THEN CASE
												WHEN ca.space_configuration_id > 0 THEN rr.space_configuration_id = ' . $strCompareSpaceConfiguration . '
												WHEN ca.desired_space_configuration_id > 0 THEN rr.space_configuration_id = ' . $strCompareDesiredSpaceConfiguration . '
												WHEN ca.space_configuration_id = 0 OR ca.space_configuration_id IS NULL
													THEN ( rr.space_configuration_id = 0 OR rr.space_configuration_id IS NULL )
												ELSE FALSE
											END
										ELSE TRUE
									END';

		$strPropertyFloorPlanColumn = 'COALESCE( rr.property_floorplan_id, us.property_floorplan_id ) AS property_floorplan_id';
		$strUnitTypeColumn = 'COALESCE( rr.unit_type_id, us.unit_type_id ) AS unit_type_id';
		$strUnitSpaceColumn = '	' . $strUnitSpaceColumn . ' ';

		if( true === $boolIsRenewalTransfer
			&& false === $boolIsFromScheduledTransfer
			&& true === current( $arrobjLeases )->getIsStudentSemesterSelectionEnabled() ) {
			if( true == valId( current( $arrobjLeases )->getSpaceConfigurationId() ) ) {
				$strSpaceConfigurationCase     = ' rr.space_configuration_id = ' . current( $arrobjLeases )->getSpaceConfigurationId() . ' ';
				$strSpaceConfigurationIdColumn = current( $arrobjLeases )->getSpaceConfigurationId() . ' AS space_configuration_id ';
			}

			if( true === valId( current( $arrobjLeases )->getUnitTypeId() ) ) {
				$strUnitTypeIdCondition = ' AND ut.id = ' . current( $arrobjLeases )->getUnitTypeId() . ' ';
			}

			$strUnitTypeCase = ' AND ( COALESCE( rr.unit_type_id, 0 ) = 0 OR rr.unit_type_id = ' . current( $arrobjLeases )->getUnitTypeId() . ' ) ';
			$strPropertyFloorPlanCase = ' AND ( COALESCE( rr.property_floorplan_id, 0 ) = 0 OR rr.property_floorplan_id = ' . current( $arrobjLeases )->getPropertyFloorPlanId() . ' ) ';
			$strUnitSpaceJoinCondition = '';
			$strPropertyFloorPlanColumn = 'COALESCE( rr.property_floorplan_id, ut.property_floorplan_id ) AS property_floorplan_id';
			$strUnitTypeColumn = 'COALESCE( rr.unit_type_id, ut.id ) AS unit_type_id';
			$strUnitSpaceColumn = '';
			$strUnitTypeJoinCondition = ' JOIN unit_types ut ON ( ut.cid = cl.cid AND ut.property_id = cl.property_id AND ut.property_floorplan_id = ' . current( $arrobjLeases )->getPropertyFloorPlanId() . ' ' . $strUnitTypeIdCondition . ' ) ';
		}

		$arrintCustomLeaseStatusTypeIds = CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes;

		// checking for renewal transfer
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjLeases ) ) {
			$objLease = reset( $arrobjLeases );

			if( true == valObj( $objLease, 'CLease' ) && true == $objLease->getIsRenewalTransfer() ) {
				$arrintCustomLeaseStatusTypeIds = CLeaseStatusType::$c_arrintRenewalTransferActivatedLeaseStatusTypes;
			}
		}

		$strLeftJoinRates = ( true == $boolLeftJoinForRates ) ? 'LEFT' : '';

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					WITH ple As ( SELECT
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance,
									round ( count ( pu.id ) * lem.expiration_tolerance / 100 ) pu_count
								FROM
									property_gl_settings pgs
									JOIN lease_expiration_months lem ON ( pgs.cid =lem.cid AND lem.lease_expiration_structure_id = pgs.lease_expiration_structure_id )
									LEFT JOIN property_units pu ON ( pu.cid = lem.cid AND pu.property_id = pgs.property_id AND pu.deleted_by IS NULL AND pu.deleted_on IS NULL )
								WHERE
									pgs.cid = ' . ( int ) $intCid . '
									AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								GROUP BY
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance
									ORDER BY
									lem.month ASC )
					SELECT
						r.*,
						CASE
						WHEN r.pu_count IS NOT NULL AND r.lease_expiring_count >= r.pu_count THEN 1
						ELSE 0
						END AS exceeds_expiration_limit
					FROM
					(
						SELECT
							rr.id,
							cl.cid,
							cl.id AS lease_id,
							' . $strPropertyColumn . ',
							' . $strPropertyFloorPlanColumn . ',
							' . $strUnitTypeColumn . ',
							' . $strUnitSpaceColumn . '
							rr.rate_id,
							COALESCE( rr.ar_formula_id, ' . CArFormula::FIXED_AMOUNT . ' ) AS ar_formula_id,
							rr.ar_formula_reference_id,
							COALESCE( rr.ar_cascade_id ) AS ar_cascade_id,
							COALESCE( rr.ar_cascade_reference_id, ' . CArCascade::SPACE . ' ) AS ar_cascade_reference_id,
							COALESCE( rr.ar_origin_id, ' . CArOrigin::BASE . ' ) AS ar_origin_id,
							rr.ar_origin_reference_id,
							rr.ar_origin_object_id,
							COALESCE( rr.ar_trigger_id, ' . CArTrigger::MONTHLY . ' ) AS ar_trigger_id,
							COALESCE( rr.ar_code_type_id, ' . CArCodeType::RENT . ' ) AS ar_code_type_id,
							COALESCE( rr.ar_code_id, pgs.rent_ar_code_id ) AS ar_code_id,
							' . $strSpaceConfigurationIdColumn . ',
							lt.id AS lease_term_id,
							CASE
								WHEN rr.lease_start_window_id > 0 AND pp.value = \'1\' THEN rr.lease_start_window_id
								WHEN lsw.id > 0 THEN lsw.id
							END AS lease_start_window_id,
							lt.term_month AS lease_term_months,
							COALESCE( rounder( rr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ), 0 ) AS rate_amount,
							rr.month_to_month_multiplier,
							COALESCE( rounder( rr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ), 0 ) AS normalized_amount,
							rr.normalized_percent,
							rr.rate_increase_increment,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL THEN lsw.renewal_start_date
								ELSE lsw.start_date
							END AS window_start_date,
							lsw.end_date AS window_end_date,
							rr.rate_interval_start,
							rr.rate_interval_occurances,
							rr.rate_interval_offset,
							COALESCE( rr.is_renewal, lt.is_renewal ),
							COALESCE( rr.is_optional, false ) AS is_optional,
							COALESCE( rr.show_on_website, lt.show_on_website ) AS show_on_website,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/ YYYY\')||\')\'
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.renewal_start_date,\'MM/DD/ YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								ELSE util_get_translated( \'name\', lt.name, lt.details )
							END AS lease_term_name,
							ac.name AS ar_code_name,
							(
								SELECT
									COUNT( cli.id )
								FROM
									cached_leases cli
								WHERE cli.cid = cl.cid AND
									cli.property_id = cl.property_id AND
									cli.id != cl.id AND
									cli.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ' ) AND
									CASE
										WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' THEN
											DATE_TRUNC( \'month\',lsw.end_date ) = DATE_TRUNC( \'month\', cli.lease_end_date + 1 )
										ELSE
											DATE_TRUNC(\'MONTH\', CASE
																	WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . '
																		THEN CURRENT_DATE
																	ELSE ' . $strLeaseEndDate . '
																 END + CAST ( lt.term_month || \'MONTHS -1DAY\' AS INTERVAL ) ) = DATE_TRUNC( \'month\', cli.lease_end_date )
									END
							) AS lease_expiring_count,
							CASE 
								WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' 
									THEN CURRENT_DATE
								ELSE ' . $strLeaseEndDate . '
								END  + CAST( lt.term_month || \'MONTHS -1DAY\' AS INTERVAL  ) as lease_term_expiry_date,
							ple.pu_count
						FROM
							cached_leases cl
							JOIN property_charge_settings pcs ON ( pcs.cid = cl.cid AND pcs.property_id = cl.property_id AND cl.id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintLeaseIds ) . ' ]::INTEGER [ ]))
							JOIN lease_terms lt ON ( lt.cid = pcs.cid ' . $strLeaseTermStructureCondition . $strLeaseTermDeletedCondition . $strRenewalLeaseTermJoinCondition . ' AND lt.is_unset IS FALSE ' . $strComercialCondition . ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) )
							JOIN property_ar_origin_rules paor ON ( paor.cid = cl.cid ' . $strNewPropertyCondition . ' AND paor.ar_origin_id = ' . CArOrigin::BASE . ' )
						' . $strSqlRenewalTransferLeftJoinCondition . ' ' . $strUnitSpaceJoinCondition . '  ' . $strUnitTypeJoinCondition . '
							JOIN property_gl_settings pgs ON ( pcs.cid = pgs.cid ' . $strPropertyGlSettingsCondition . ' )
							LEFT JOIN LATERAL (
												SELECT
													ca.space_configuration_id,
													ca.desired_space_configuration_id
												FROM
													lease_intervals ili
													JOIN cached_applications ca ON ( ili.cid = ca.cid AND ca.lease_interval_id = ili.id )
												WHERE
													ili.lease_id = cl.id
													AND ili.cid = cl.cid
													AND ili.lease_interval_type_id NOT IN (' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
													AND ili.lease_status_type_id IN ( ' . sqlIntImplode( $arrintCustomLeaseStatusTypeIds ) . ' )
													AND ili.lease_start_date <= cl.lease_start_date
												ORDER BY ili.lease_start_date DESC
												LIMIT 1
											) as ca ON TRUE

							LEFT JOIN lease_start_windows lsw ON (	lsw.cid = pcs.cid
																	' . $strLeaseStartStructureCondition . '
																	AND lt.id = lsw.lease_term_id
																	AND lsw.property_id = cl.property_id ' . $strLeaseStartWindowDeletedCondition . ' )

							' . $strLeftJoinRates . ' JOIN ' . $strRatesTableName . ' rr ON ( rr.cid = cl.cid ' . self::getRenewalTransferCondition( $arrobjLeases, true ) . ' AND CASE
													WHEN paor.use_lease_terms_for_rent IS TRUE THEN
														rr.lease_term_id = lt.id
													ELSE TRUE
													END
													' . $strPropertyRentChargeCodeCondition . '
													AND rr.ar_origin_id = ' . CArOrigin::BASE . '
													AND rr.ar_code_type_id = ' . CArCodeType::RENT . '
													AND ( rr.ar_origin_reference_id IS NULL OR rr.ar_origin_reference_id = 0 )
													AND rr.ar_trigger_id IN ( ' . sqlIntImplode( CArTrigger::$c_arrintRecurringArTriggers ) . ')
													AND ' . $strSpaceConfigurationCase . '
													' . $strUnitTypeCase . '
													' . $strPropertyFloorPlanCase . '
													AND CASE
															WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND rr.lease_start_window_id IS NOT NULL AND COALESCE( rr.lease_start_window_id,0 ) > 0
																THEN rr.lease_start_window_id = lsw.id
															ELSE TRUE
														END )

							LEFT JOIN property_preferences pp ON ( pp.cid = cl.cid AND pp.property_id = cl.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' )
							LEFT JOIN lease_processes lp ON ( lp.cid = cl.cid AND lp.transfer_lease_id =  cl.id AND lp.transferred_on IS NULL )
							LEFT JOIN ple ON ( ple.cid = cl.cid AND ple.property_id = cl.property_id AND ple.month = date_part ( \'month\', CASE
						                                                                                                   WHEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' THEN CASE
						                                                                                                                                                                      WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN CURRENT_DATE
						                                                                                                                                                                      ELSE cl.lease_end_date + 1
						                                                                                                                                        END  + ( lt.term_month || \'MONTHS -1 DAY\' )::INTERVAL
						                                                                                                   ELSE DATE ( date_trunc ( \'month\', lsw.end_date ) )
						                                                                                                 END ) )
							' . $strLeftJoinRates . ' JOIN ar_codes ac ON ( ac.cid = rr.cid AND ac.id = rr.ar_code_id )
						WHERE
							cl.cid = ' . ( int ) $intCid . '
							' . $strRenewalTransferUnitSpaceCondition . ' 
							AND CASE
									WHEN lsw.start_date IS NOT NULL
										THEN DATE (lsw.end_date) > ' . ( ( true == $boolIsRenewalTransfer ) ? ' cl.lease_start_date ' : ' COALESCE( cl.lease_end_date , CURRENT_DATE ) + 1 ' ) . '
									ELSE TRUE
								END
							AND CASE
									WHEN lsw.start_date IS NOT NULL
										THEN DATE ( lsw.end_date) > CASE
																		WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . '
																			THEN CURRENT_DATE
																		ELSE ' . ( ( true == $boolIsRenewalTransfer ) ? ' cl.lease_start_date ' : ' cl.lease_end_date ' ) . '
																	END
									ELSE TRUE
								END
							AND CASE
									WHEN pp.value = \'1\'
										THEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.id IS NOT NULL
									ELSE lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
								END ' . $strScheduledTransferLeaseTermCondition . $strScheduledTransferLeaseStartWindowCondition . '
						ORDER BY
						    lsw.renewal_start_date,
							lt.term_month
							 ) As r ';
		if( false === $boolAllowLeaseExpirationLimit ) {
			$strSql .= ' WHERE 
							CASE
								WHEN r.pu_count IS NOT NULL AND r.lease_expiring_count >= r.pu_count THEN FALSE
								ELSE TRUE
							END';
		}

		$arrobjRenewalBaseRates = CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );

		return $arrobjRenewalBaseRates;
	}

	public static function fetchRenewalSpecialsByLeasesByCid( $arrobjLeases, $intCid, $objDatabase, $strRenewalTransferLeaseEndCondition = 'cl.lease_end_date', $boolIncludeCommercialLeaseTerms = false, $objOfferTemplate = NULL ) {

		$arrintLeaseIds			= array_keys( $arrobjLeases );

		$boolIsRenewalTransfer					= self::checkForRenewalTransfer( $arrobjLeases );
		$objLease = current( $arrobjLeases );
		if( true == valId( $objLease->getTransferredFromLeaseId() ) ) {
			$objTransferredLease = $objLease->fetchTransferredLease( $objDatabase );
			if( true == valObj( $objTransferredLease, 'CLease' ) ) {
				$strRenewalTransferLeaseEndCondition = ( CLeaseIntervalType::MONTH_TO_MONTH == $objTransferredLease->getLeaseIntervalTypeId() ) ? 'cl.lease_start_date' : '\'' . $objTransferredLease->getLeaseEndDate() . '\'::DATE';
			}
		}

		$strUnitSpaceJoinCondition = 'JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = cl.unit_space_id )';

		$strUnitTypeJoinCondition = '';
		$strUnitTypeSelect = 'us.unit_type_id, us.property_floorplan_id ';

		$strUnitTypeCase = 'us.unit_type_id = ANY( ot.unit_type_ids )';

		if( true == $boolIsRenewalTransfer ) {
			$objLease = reset( $arrobjLeases );

			if( true === valId( $objLease->getUnitSpaceId() ) ) {
				$strUnitSpaceJoinCondition = 'LEFT JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = ' . $objLease->getUnitSpaceId() . ' )';
			} else if( true == valId( $objLease->getUnitTypeId() ) ) {
				$strUnitSpaceJoinCondition = '';
				$strUnitTypeJoinCondition = ' JOIN unit_types ut ON ( ut.cid = cl.cid AND ut.property_id = cl.property_id AND ut.property_floorplan_id = ' . $objLease->getPropertyFloorPlanId() . ' AND ut.id = ' . $objLease->getUnitTypeId() . ' )';
				$strUnitTypeCase = 'ut.id = ANY( ot.unit_type_ids )';
				$strUnitTypeSelect = 'ut.unit_type_id, ut.property_floorplan_id ';
			}
		}

		$strComercialCondition		= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strCondition = ' WHERE sub.rank = 1';

		if( true == valObj( $objOfferTemplate, 'COfferTemplate' ) ) {
			$strCondition = ' AND rr.special_group_id = ' . $objOfferTemplate->getId();
		}

		if( false == valArr( $arrintLeaseIds ) ) {
			return [];
		}

		$strSql = 'WITH lease_wise_applicable_offers AS (
						SELECT *
							FROM (
									SELECT cl.id,
										cl.cid,
										cl.property_id,
										cl.unit_space_id,
										cl.lease_start_date,
										cl.lease_end_date,
										COALESCE(cl.lease_end_date + 1, CURRENT_DATE) As renewal_start_date,
										( ' . $strRenewalTransferLeaseEndCondition . ' - CURRENT_DATE) AS lease_offer_offset,
										ca.space_configuration_id,
										ca.desired_space_configuration_id,
										' . $strUnitTypeSelect . ',
										ot.unit_type_ids,
										ot.lease_start_window_ids,
										ot.move_out_start_date,
										ot.move_out_end_date,
										ot.occupancy_type_id AS offer_template_occupancy_type_id,
										ot.offer_template_type_id,
										o.offer_template_id,
										o.start_date,
										o.end_date,
										o.days_offset,
										COALESCE( (
													SELECT o2.days_offset
													FROM offers o2
													WHERE	o2.cid = o.cid AND
															o2.offer_template_id = o.offer_template_id
													ORDER BY o2.days_offset DESC
													LIMIT 1
												), 0) AS max_days_offset,
										oi.name,
										o.id AS offer_id,
										CASE
											WHEN s.min_days_to_lease_start = 0 THEN 0
										ELSE
											COALESCE( (
																SELECT o1.days_offset
																FROM offers o1
																WHERE	o1.cid = o.cid AND
																		o1.offer_template_id = o.offer_template_id AND
																		o1.deleted_by IS NULL AND
																		o1.days_offset < o.days_offset
																ORDER BY o1.days_offset DESC
																LIMIT 1
												) + 1, 0)
										END	AS min_days_to_lease_start,
										COALESCE( s.quantity_remaining,s.quantity_budgeted ) AS max_offer_count,
										s.id AS offer_special_id,
										CASE
											WHEN ot.unit_type_ids IS NOT NULL THEN 3
											ELSE 1
										END AS ar_cascade_id
   						FROM
   							cached_leases cl
   							LEFT JOIN LATERAL
   								(
   									SELECT
   										ca.space_configuration_id,
										ca.desired_space_configuration_id
   									FROM
   										lease_intervals ili
   										JOIN cached_applications ca ON(ili.cid = ca.cid AND ca.lease_interval_id = ili.id)
   									WHERE
   										ili.lease_id = cl.id AND
   										ili.cid = cl.cid AND
   										ili.lease_interval_type_id NOT IN(' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ') AND
   										ili.lease_status_type_id IN(' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ', ' . CLeaseStatusType::PAST . ' ) AND
   										ili.lease_start_date <= cl.lease_start_date
   									ORDER BY
   										ili.lease_start_date DESC
   									LIMIT 1
   								) AS ca ON TRUE
   								' . $strUnitSpaceJoinCondition . '
   								' . $strUnitTypeJoinCondition . '
   							JOIN offer_templates ot ON(ot.cid = cl.cid AND ot.property_id = cl.property_id)
   							JOIN offers o ON(cl.cid = o.cid AND ot.id = o.offer_template_id)
   							JOIN offer_items oi ON(cl.cid = o.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL)
   							JOIN specials s ON(s.cid = oi.cid AND s.id = oi.ar_origin_reference_id AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ')
   							WHERE
   								cl.cid = ' . ( int ) $intCid . '
   								AND cl.id = ANY(ARRAY [ ' . sqlIntImplode( $arrintLeaseIds ) . ' ]::INTEGER [ ])
   								AND ot.deleted_by IS NULL
   								AND ot.expiry_date >= CURRENT_DATE
   								AND (   ot.unit_type_ids IS NULL
   										OR ' . $strUnitTypeCase . '
   									)
   								AND (COALESCE(cl.lease_end_date, CURRENT_DATE) BETWEEN ot.move_out_start_date AND ot.move_out_end_date OR ot.occupancy_type_id = ' . COccupancyType::CONVENTIONAL . ' )
   								/* Temporary SQL fixed done under task #2164704 */
   								AND
   									CASE
   										WHEN ( ot.offer_template_type_id IN (\'DATE BASED TIERS\', \'DATE BASED TIERS WITH CAPS\', \'AUTO ADVANCING DATE BASED TIERS\', \'CAP BASED TIERS\') )
   										THEN ( COALESCE(cl.lease_end_date, CURRENT_DATE) BETWEEN ot.move_out_start_date AND ot.move_out_end_date )
   										ELSE TRUE
   									END
   								AND ot.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
   								AND ot.is_published IS TRUE
   								AND o.deleted_by IS NULL
   								AND o.is_active IS TRUE
   								AND (o.end_date > CURRENT_DATE OR o.end_date IS NULL)
   								AND	((ot.offer_template_type_id IN (\'DATE BASED TIERS WITH CAPS\', \'AUTO ADVANCING DATE BASED TIERS\', \'CAP BASED TIERS\')
   									AND	o.max_offer_count IS NOT NULL AND o.max_offer_count > 0)
   									OR ( ot.offer_template_type_id IS NOT NULL AND ot.offer_template_type_id IN (\'DATE BASED TIERS\', \'AUTO ADVANCING DATE BASED TIERS\', \'AUTOMATIC RENEWALS\', \'DATE BASED TIERS WITH CAPS\') )
   									OR ot.offer_template_type_id IS NULL)
   								AND
   									CASE
										WHEN ( ( ot.offer_template_type_id IN ( \'AUTO ADVANCING DATE BASED TIERS\', \'CAP BASED TIERS\' ) OR NULL = ot.offer_template_type_id  ) AND s.quantity_remaining IS NOT NULL )
										THEN s.quantity_remaining > 0
										ELSE TRUE
									END
   				 		) AS lease_offers
   				  WHERE CASE
   							WHEN offer_template_type_id = \'CAP BASED TIERS\' AND lease_offers.lease_end_date >= CURRENT_DATE THEN TRUE
   							WHEN ( offer_template_type_id IS NULL OR offer_template_type_id = \'AUTOMATIC RENEWALS\' )
   							THEN ( lease_offer_offset BETWEEN min_days_to_lease_start AND days_offset
   								OR  ( lease_offer_offset > days_offset AND lease_offer_offset <= max_days_offset ) )
   							ELSE start_date <= lease_offers.lease_end_date
						END
				)
				SELECT *
					FROM (
						SELECT rr.id,
							lwao.cid,
							lwao.property_id,
							lwao.property_floorplan_id,
							lwao.unit_type_id,
							rr.unit_space_id,
							rr.rate_id,
							rr.ar_formula_id,
							rr.ar_formula_reference_id,
							rr.ar_cascade_id,
							rr.ar_cascade_reference_id,
							' . CArOrigin::SPECIAL . ' As ar_origin_id,
							lwao.offer_special_id AS ar_origin_reference_id,
							lwao.offer_id,
							rr.ar_origin_object_id,
							rr.ar_trigger_id,
							rr.ar_code_type_id,
							rr.ar_code_id,
							rr.lease_association_id,
							lt.term_month AS lease_term_months,
							COALESCE(rounder(rr.rate_amount, COALESCE(ac.round_type_id, pcs.round_type_id)), 0) AS rate_amount,
							rr.month_to_month_multiplier,
							rounder(rr.normalized_amount, COALESCE(ac.round_type_id, pcs.round_type_id)) AS normalized_amount,
							rr.normalized_percent,
							rr.rate_increase_increment,
							rr.rate_interval_start,
							rr.rate_interval_occurances,
							rr.rate_interval_offset,
							ac.name AS ar_code_name,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
								ELSE util_get_translated( \'name\', lt.name, lt.details )
							END AS lease_term_name,
							lt.id as lease_term_id,
							CASE
								WHEN rr.lease_start_window_id > 0 AND pp.value = \'1\' THEN rr.lease_start_window_id
								ELSE lsw.id
							END AS lease_start_window_id,
							lwao.id AS lease_id,
							(lwao.lease_end_date - CURRENT_DATE) AS expiry_days,
							CASE
								WHEN lwao.offer_template_type_id IS NULL OR lwao.offer_template_type_id = \'AUTOMATIC RENEWALS\' THEN lwao.name || \' DAYS BEFORE EXP\'
								ELSE lwao.name
							END AS special_name,
							lwao.offer_special_id as special_id,
							lwao.days_offset AS special_max_days_to_lease_start,
							lwao.min_days_to_lease_start AS special_min_days_to_lease_start,
							lwao.start_date AS special_start_date,
							lwao.end_date AS special_end_date,
							lwao.offer_template_type_id AS special_group_type_id,
							dense_rank() OVER(PARTITION BY lwao.cid, lwao.unit_space_id
						ORDER BY
							COALESCE(rr.ar_cascade_id, lwao.ar_cascade_id) DESC NULLS LAST) AS rank
						FROM
							lease_wise_applicable_offers lwao
							JOIN property_ar_origin_rules paor ON(paor.cid = lwao.cid AND paor.property_id = lwao.property_id AND paor.ar_origin_id = ' . CArOrigin::SPECIAL . ')
							JOIN property_charge_settings pcs ON(pcs.cid = lwao.cid AND pcs.property_id = lwao.property_id)
							JOIN lease_terms lt ON(lt.cid = lwao.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL
								AND lt.is_disabled IS FALSE AND lt.is_unset IS FALSE AND lt.is_renewal IS TRUE ' . $strComercialCondition . ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) )
							LEFT JOIN lease_start_windows lsw ON(lsw.cid = pcs.cid AND lsw.lease_start_structure_id = pcs.lease_start_structure_id AND lt.id = lsw.lease_term_id AND
								lsw.is_active IS TRUE AND lsw.deleted_by IS NULL AND lsw.deleted_on IS NULL AND lsw.property_id = lwao.property_id AND lsw.id = ANY (
								lwao.lease_start_window_ids))
							LEFT JOIN property_preferences pp ON(
									pp.cid = lwao.cid
									AND pp.property_id = lwao.property_id
									AND pp.key = \'ENABLE_SEMESTER_SELECTION\'
								)
								LEFT JOIN renewal_rates rr ON( rr.cid = lwao.cid
																AND CASE
																		WHEN rr.unit_space_id IS NOT NULL THEN rr.unit_space_id = lwao.unit_space_id
																		ELSE rr.unit_type_id = lwao.unit_type_id
																	END
																AND CASE
																		WHEN paor.use_lease_terms_for_rent IS TRUE THEN rr.lease_term_id = lt.id
																		ELSE TRUE
																	END
																AND rr.ar_origin_id = ' . CArOrigin::SPECIAL . '
																AND rr.ar_origin_reference_id = lwao.offer_special_id
																AND CASE
												 						WHEN paor.use_space_configurations_for_rent IS TRUE
																			AND rr.is_student_property = 1
																			AND lwao.space_configuration_id > 0
																		THEN rr.space_configuration_id = lwao.space_configuration_id
																		WHEN paor.use_space_configurations_for_rent IS TRUE
																			AND rr.is_student_property = 1
																			AND lwao.desired_space_configuration_id > 0
																		THEN rr.space_configuration_id = lwao.desired_space_configuration_id
																		WHEN paor.use_space_configurations_for_rent IS TRUE
																			AND rr.is_student_property = 1
																			AND ( lwao.space_configuration_id = 0 OR lwao.space_configuration_id IS NULL )
																		THEN ( rr.space_configuration_id = 0 OR rr.space_configuration_id IS NULL )
																		ELSE
																			TRUE
																	END
																AND CASE WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
																AND rr.lease_start_window_id IS NOT NULL
																AND rr.lease_start_window_id > 0 THEN rr.lease_start_window_id = lsw.id ELSE TRUE END )
								LEFT JOIN ar_codes ac ON (ac.id = rr.ar_code_id AND ac.cid = rr.cid)
						WHERE
							((( pp.value = \'1\'
								AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
								AND lsw.id IS NOT NULL)
								OR lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' )
							AND (lsw.id IS NULL OR (lsw.end_date > lwao.renewal_start_date))
							AND ((lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
									AND lsw.id IS NOT NULL )
								 OR lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' ))
								 OR (
                             ( pp.value = \'1\'
								AND lt.lease_term_type_id = 2
                                AND lwao.offer_template_type_id = \'AUTOMATIC RENEWALS\'
                             ) )
					) As sub
					' . $strCondition . '
				 Order By offer_id
			';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchInstallmentPlansByPropertyIdsByLeaseTermIdsByLeaseStartWindowIdsByCid( $arrintPropertyIds, $arrintLeaseTermIds, $arrintLeaseStartWindowIds= [], $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $arrintSpaceConfigurationIds = [] ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintLeaseTermIds ) ) return [];
		$strSqlLeaseStartWindowJoin = '';
		$strLeaseStartWindowCondition = '';
		$strLeaseStartWindowWhereCondition = '';
		if( true == valArr( $arrintLeaseStartWindowIds ) ) {
			$strSqlLeaseStartWindowJoin        = ' JOIN lease_start_windows lsw ON ( 	lsw.cid = ip.cid
															AND lsw.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
															AND lsw.lease_term_id = lt.id
															AND lsw.is_active IS TRUE
															AND lsw.deleted_by IS NULL
															AND lsw.deleted_on IS NULL
															AND lsw.id IN ( ' . sqlIntImplode( $arrintLeaseStartWindowIds ) . ' )
														) ';
			$strLeaseStartWindowCondition      = ' AND CASE
														WHEN
															paor.use_lease_start_windows_for_other IS TRUE
														THEN
															pr.lease_start_window_id = lsw.id
														ELSE
															TRUE
														END ';
			$strLeaseStartWindowWhereCondition = ' AND ip.lease_start_window_id IN ( ' . sqlIntImplode( $arrintLeaseStartWindowIds ) . ' ) ';
		}
		$strCommercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strSpaceConfigurationWhr = ' TRUE ';
		if( true == valArr( $arrintSpaceConfigurationIds ) ) {
			$strSpaceConfigurationWhr           = ' pr.space_configuration_id IN( ' . implode( ',', $arrintSpaceConfigurationIds ) . ' )';
		}

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ ( int ) $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE, CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setLoadOptionalRates( true );
		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						ip.id as installment_plan_id,
						ip.property_id,
						ip.name as installment_plan_name,
						ip.description as installment_plan_description,
						ip.lease_term_id,
						ip.lease_start_window_id,
						pr.id AS rate_log_id,
						CASE WHEN pr.ar_formula_id <> ' . CArFormula::FIXED_AMOUNT . ' THEN
							rounder( pr.rate_increase_increment, COALESCE( ac.round_type_id, pcs.round_type_id ) )
						ELSE
							rounder( pr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) )
						END as rate_amount,
						pr.ar_formula_id,
						ip.special_id,
						paor.use_space_configurations_for_rent,
						pr.space_configuration_id
					FROM
						installment_plans ip
						JOIN property_ar_origin_rules paor ON ( ip.cid = paor.cid AND ip.property_id = paor.property_id AND paor.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN lease_terms lt ON ( 	ip.cid = lt.cid
													AND ip.lease_term_id = lt.id
													AND lt.is_renewal IS TRUE
													AND lt.is_disabled IS FALSE
													AND lt.show_on_website IS TRUE
													AND lt.deleted_on IS NULL
													AND lt.deleted_by IS NULL
													AND lt.id IN ( ' . sqlIntImplode( $arrintLeaseTermIds ) . ' ) ' . $strCommercialCondition . '
													AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
												)
												' . $strSqlLeaseStartWindowJoin . '
						JOIN property_charge_settings pcs ON ( pcs.cid = ip.cid AND pcs.property_id = ip.property_id )
						LEFT JOIN specials s ON ( ip.cid = s.cid AND ip.special_id = s.id AND s.special_type_id = ' . CSpecialType::INSTALLMENT_PLAN . ' )
						LEFT JOIN prospect_rates pr ON ( 	ip.cid = pr.cid
												AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . '
												AND ip.special_id = pr.ar_origin_reference_id
												AND pr.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
												' . $strLeaseStartWindowCondition . '
												AND CASE
														WHEN
															paor.use_space_configurations_for_rent IS TRUE
														THEN
															' . $strSpaceConfigurationWhr . '
														ELSE
															TRUE
														END
												AND pr.property_id = ip.property_id
											)
						LEFT JOIN ar_codes ac ON ( ac.cid = pcs.cid AND ac.id = pr.ar_code_id )
					WHERE
						ip.cid = ' . ( int ) $intCid . '
						AND ip.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND ip.lease_term_id IN ( ' . sqlIntImplode( $arrintLeaseTermIds ) . ' )
						' . $strLeaseStartWindowWhereCondition . '
						AND ip.deleted_by IS NULL
						AND ip.deleted_on IS NULL
					GROUP BY
                        ip.id ,
						ip.name ,
                        ip.property_id,
						ip.description,
						ip.lease_term_id,
						ip.lease_start_window_id,
						pr.id ,
						pr.ar_formula_id ,
						pr.rate_increase_increment,
                        ac.round_type_id,
                        pcs.round_type_id ,
						pr.rate_amount,
						pr.ar_formula_id,
						ip.special_id,
						paor.use_space_configurations_for_rent,
						pr.space_configuration_id
					ORDER BY
						ip.id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRatesByUnitSpaceIdsByLeaseTermIdsByPropertyIds( $intPropertyId, $intUnitSpaceId, $intLeaseTermId, $intLeaseStartWindowId, $intSpaceConfigurationId, $strMoveInDate, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ ( int ) $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );
		$objRateLogsFilter->setArTriggerIds( [ CArTrigger::MOVE_OUT, CArTrigger::MOVE_IN ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );
		$objRateLogsFilter->setLoadOptionalRates( true );
		$objRateLogsFilter->setMoveInDate( $strMoveInDate );
		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						*
					FROM
						prospect_rates pr
					WHERE
						CASE
							WHEN
								pr.space_configuration_id > 0
							THEN
								pr.space_configuration_id = ' . ( int ) $intSpaceConfigurationId . '
							ELSE
								pr.space_configuration_id = 0
							END
						AND CASE
								WHEN
									pr.lease_term_id > 0
								THEN
									pr.lease_term_id = ' . ( int ) $intLeaseTermId . '
								ELSE
									pr.lease_term_id = 0
								END
						AND CASE
								WHEN
									pr.lease_start_window_id > 0
								THEN
									pr.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
								ELSE
									pr.lease_start_window_id = 0
								END';
		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchRenewalGiftIncentivesByLeasesByOfferIdsByCid( $arrobjLeases, $arrintOfferIds, $intCid, $objDatabase, $arrintLeaseTermIds, $arrintLeaseStartWindowIds ) {

		$arrintLeaseIds = array_keys( $arrobjLeases );

		$strSqlRenewalTransferLeftJoinCondition = ( true == self::checkForRenewalTransfer( $arrobjLeases ) ) ? ' LEFT ' : '';

		if ( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintOfferIds ) ) return [];

		$strSqlLeaseStartWindowCondition = '';

		if( true == valArr( $arrintLeaseStartWindowIds ) ) {
			$strSqlLeaseStartWindowCondition = ' AND lsw.id IN ( ' . sqlIntImplode( $arrintLeaseStartWindowIds ) . ' )';
		}

		/** @FIXME This query needs some love. I would recommend creating at a temp table with the wheres/joins (not including LEFT) to reduce the dataset as quickly as possible, and only do left joins that do not have any WHERE condition after that. This will help the optimizer do its job simply & quickly. This came to my attention in NewRelic under slow queries. -MLewis */

		$strSql = 'WITH offers_having_gift_insentive AS (
						SELECT
							o.id,
							o.cid
						FROM
							offers o
							JOIN offer_templates ot ON (ot.cid = o.cid AND ot.deleted_by IS NULL)
							JOIN offer_items oi ON (oi.cid = o.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL)
							JOIN specials s ON (s.cid = o.cid AND oi.ar_origin_reference_id = s.id AND s.special_type_id IN ( ' . CSpecialType::GIFT_INCENTIVE . ', ' . CSpecialType::STANDARD . ' ) AND s.deleted_by IS NULL AND s.is_active IS TRUE)
						WHERE o.cid = ' . ( int ) $intCid . ' AND
							o.deleted_by IS NULL AND
							o.is_active IS TRUE AND
							o.id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )
				)
				SELECT
					*
				FROM (
						SELECT 
							s.id AS gift_incentive_id,
							s.name,
							o.cid,
							s.special_type_id,
							o.max_offer_count AS quantity_remaining,
							s.description,
							st.name AS special_type_name,
							ois.ar_origin_reference_id AS tier_special_id,
							cl.id AS lease_id,
							rounder( rr.rate_amount, COALESCE( ac.round_type_id, pcs.round_type_id ) ) AS rate_amount,
							rr.ar_trigger_id,
							rr.ar_origin_id,
							rr.ar_code_id,
							rr.ar_code_type_id,
							rr.ar_origin_reference_id,
							rr.ar_cascade_id,
							rr.ar_cascade_reference_id,
							rr.ar_formula_id,
							rr.ar_formula_reference_id,
							rr.rate_id,
							rr.id AS rate_log_id,
							CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
							THEN lt.name ||\' ( \'||to_char(lsw.start_date,\'MM/DD/ YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/ YYYY\')||\' )\'
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
							THEN lt.name ||\' ( \'||to_char(lsw.renewal_start_date,\'MM/DD/ YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/ YYYY\')||\' )\'
							ELSE lt.name
						END AS lease_term_months,
							lt.id As lease_term_id,
							lt.term_month,
							CASE
								WHEN rr.lease_start_window_id > 0
								THEN rr.lease_start_window_id
								ELSE lsw.id
							END AS lease_start_window_id,
							ra.hide_description,
							NULL AS lease_association_id,
							NULL AS scheduled_charge_id,
							cl.property_id,
							ROW_NUMBER() OVER (partition BY s.id, o.id, cl.id, lt.id, rr.ar_code_id, lsw.id ORDER BY s.id) AS rnum
						FROM
							offers o
							JOIN offers_having_gift_insentive owgi ON (o.cid = owgi.cid AND o.id = owgi.id)
							JOIN offer_items ois ON (o.cid = ois.cid AND ois.offer_id = o.id AND ois.deleted_by IS NULL)
							JOIN specials ss ON (ss.cid = ois.cid AND ois.ar_origin_reference_id = ss.id AND ss.special_type_id = ' . CSpecialType::RENEWAL_TIER . ' AND ss.deleted_by IS NULL AND ss.is_active IS TRUE)
							JOIN cached_leases cl ON (cl.cid = o.cid AND cl.id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintLeaseIds ) . ' ]::INTEGER [ ] ))
							JOIN offer_templates ot ON (ot.cid = cl.cid AND ot.property_id = cl.property_id AND ot.deleted_by IS NULL AND ot.id = o.offer_template_id)
							' . $strSqlRenewalTransferLeftJoinCondition . ' JOIN unit_spaces us ON (cl.cid = us.cid AND us.id = cl.unit_space_id)
							JOIN offer_items oi ON (o.cid = oi.cid AND oi.offer_id = o.id AND oi.deleted_by IS NULL)
							JOIN specials s ON (s.cid = oi.cid AND oi.ar_origin_reference_id = s.id AND s.special_type_id IN ( ' . CSpecialType::GIFT_INCENTIVE . ', ' . CSpecialType::STANDARD . ' ) AND s.deleted_by IS NULL AND s.is_active IS TRUE)
							JOIN property_charge_settings pcs ON (pcs.cid = cl.cid AND pcs.property_id = cl.property_id)
							JOIN special_types st ON (st.id = s.special_type_id)
							LEFT JOIN property_ar_origin_rules paor ON (paor.cid = cl.cid AND paor.property_id = cl.property_id AND paor.ar_origin_id = ' . CArOrigin::SPECIAL . ')
							LEFT JOIN LATERAL (
												SELECT
													ca.*
												FROM
													lease_intervals ili
													JOIN cached_applications ca ON ( ili.cid = ca.cid AND ca.lease_interval_id = ili.id )
												WHERE
													ili.lease_id = cl.id
													AND ili.cid = cl.cid
													AND ili.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
													AND ili.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ', ' . CLeaseStatusType::PAST . ' )
													AND ili.lease_start_date <= cl.lease_start_date
												ORDER BY
													ili.lease_start_date DESC
													LIMIT 1
											) AS ca ON TRUE
							LEFT JOIN renewal_rates rr ON (
																rr.cid = s.cid
																AND rr.ar_origin_reference_id = s.id
																AND CASE
																		WHEN paor.use_space_configurations_for_other IS TRUE AND rr.is_student_property = 1
																			THEN
																				CASE
																					WHEN ca.space_configuration_id > 0
																						THEN rr.space_configuration_id = ca.space_configuration_id
																					WHEN ca.desired_space_configuration_id > 0
																						THEN rr.space_configuration_id = ca.desired_space_configuration_id
																					WHEN ca.space_configuration_id = 0 OR ca.space_configuration_id IS NULL
																						THEN ( rr.space_configuration_id = 0 OR rr.space_configuration_id IS NULL )
																					ELSE FALSE
																				END
																			ELSE TRUE
																	END
															)
							LEFT JOIN lease_terms lt ON (
															lt.cid = s.cid
															AND pcs.lease_term_structure_id = lt.lease_term_structure_id
															AND CASE
																	WHEN rr.lease_term_id > 0 THEN rr.lease_term_id = lt.id
																	ELSE TRUE
																END
															AND lt.deleted_by IS NULL
															AND lt.deleted_on IS NULL
															AND lt.is_disabled IS FALSE
															AND lt.is_unset IS FALSE
															AND lt.is_renewal IS TRUE
															AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' 
															AND lt.id IN ( ' . sqlIntImplode( $arrintLeaseTermIds ) . ' )
															AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
														)
							LEFT JOIN lease_start_windows lsw ON (
																	lsw.cid = s.cid
																	AND pcs.lease_start_structure_id = lsw.lease_start_structure_id
																	AND CASE
																			WHEN rr.lease_start_window_id > 0 THEN rr.lease_start_window_id = lsw.id
																			ELSE TRUE
																		END
																	AND lt.id = lsw.lease_term_id
																	AND lsw.is_active IS TRUE
																	AND lsw.deleted_by IS NULL
																	AND lsw.deleted_on IS NULL
																	AND lsw.property_id = cl.property_id
																	' . $strSqlLeaseStartWindowCondition . ' 
																)
							LEFT JOIN rate_associations ra ON (
																	ra.cid = s.cid
																	AND ra.property_id = cl.property_id
																	AND ra.ar_cascade_id =' . CArCascade::PROPERTY . '
																	AND ra.ar_cascade_reference_id = cl.property_id
																	AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
																	AND ra.ar_origin_reference_id = s.id
																)
							LEFT JOIN ar_codes ac ON ( ac.cid = pcs.cid AND ac.id = rr.ar_code_id )
						WHERE 
							o.cid = ' . ( int ) $intCid . ' AND
							o.deleted_by IS NULL AND
							o.is_active IS TRUE AND 
							s.is_active IS TRUE
							AND CASE
										WHEN s.special_type_id = ' . CSpecialType::STANDARD . ' THEN rr.id IS NOT NULL
										ELSE TRUE
									END
					) As sub
				WHERE
					sub.rnum = 1
				ORDER BY
					lease_id,
					special_type_id DESC,
					tier_special_id,
					name,
					term_month';

		$arrmixGiftIncentives = fetchData( $strSql, $objDatabase );

		return $arrmixGiftIncentives;
	}

	public static function getRenewalStandardSpecialRatesByOfferIdsByCid( $arrintOfferIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintOfferIds ) ) return [];

		$strSql = 'SELECT
						rr.*,
						CASE WHEN rr.lease_term_id = 0 THEN lt.id ELSE rr.lease_term_id END As lease_term_id,
						CASE WHEN rr.lease_start_window_id = 0 THEN lsw.id ELSE rr.lease_start_window_id END As lease_start_window_id
					FROM
						offers o
						JOIN offer_templates ot ON ( o.cid = ot.cid AND o.offer_template_id = ot.id )
						JOIN offer_items oi ON ( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
						JOIN specials s ON (oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.deleted_by IS NULL AND s.special_type_id = ' . CSpecialType::STANDARD . '  )
						JOIN special_types st ON (st.id = s.special_type_id)
						JOIN renewal_rates rr ON ( rr.cid = s.cid
													AND rr.ar_origin_reference_id = oi.ar_origin_reference_id
													AND rr.ar_code_type_id = ' . CArCodeType::OTHER_INCOME . '
													AND rr.ar_trigger_id IN ( ' . sqlIntImplode( [ CArTrigger::RENEWAL_LEASE_APPROVAL, CArTrigger::MONTHLY, CArTrigger::RENEWAL_OFFER_ACCEPTED, CArTrigger::NIGHTLY, CArTrigger::WEEKLY, CArTrigger::DAILY ] ) . ' ) )
						JOIN lease_terms lt ON (   lt.cid = s.cid
												AND lt.deleted_by IS NULL
												AND lt.deleted_on IS NULL
												AND lt.is_disabled IS FALSE
												AND lt.is_unset IS FALSE
												AND lt.is_renewal IS TRUE
												AND CASE WHEN rr.lease_term_id > 0
													THEN rr.lease_term_id = lt.id
												 	ELSE TRUE
												 	END
													)
						LEFT JOIN lease_start_windows lsw ON ( 	lsw.cid = s.cid
																AND lt.id = lsw.lease_term_id
																AND lsw.is_active IS TRUE
																AND lsw.deleted_by IS NULL
																AND lsw.deleted_on IS NULL
																AND lsw.property_id = rr.property_id
																AND
																	CASE
																		WHEN rr.lease_start_window_id > 0
																		THEN rr.lease_start_window_id = lt.id
																		ELSE TRUE
																	END
															 )
						WHERE
							o.cid = ' . ( int ) $intCid . '
							AND o.deleted_by IS NULL
							AND o.id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )';

		$arrobjRenewalSpecialRates = CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );

		if( false === valArr( $arrobjRenewalSpecialRates ) ) return [];

		$arrobjTempRenewalSpecialRates = [];

		foreach( $arrobjRenewalSpecialRates As $objRateLog ) {

			if( false === valArr( $arrobjTempRenewalSpecialRates[$objRateLog->getArOriginReferenceId()] ) )
				$arrobjTempRenewalSpecialRates[$objRateLog->getArOriginReferenceId()] = [];

			$strRateKey = $objRateLog->getLeaseTermId();

			if( true === valId( $objRateLog->getLeaseStartWindowId() ) ) {
				$strRateKey .= '_' . $objRateLog->getLeaseStartWindowId();
			}

			if( true === valId( $objRateLog->getSpaceConfigurationId() ) ) {
				$strRateKey .= '_' . $objRateLog->getSpaceConfigurationId();
			}

			if( false === valArr( $arrobjTempRenewalSpecialRates[$objRateLog->getArOriginReferenceId()][$strRateKey] ) ) {
				$arrobjTempRenewalSpecialRates[$objRateLog->getArOriginReferenceId()][$strRateKey] = [];
			}

			$arrobjTempRenewalSpecialRates[$objRateLog->getArOriginReferenceId()][$strRateKey][$objRateLog->getArTriggerId()] = $objRateLog;

		}

		return $arrobjTempRenewalSpecialRates;
	}

	public static function getRenewalTransferCondition( $arrobjLeases, $boolIsRenewal = true ) {

		// table alias name
		$strTableAlias = 'rr';

		if( false == $boolIsRenewal ) $strTableAlias = 'pr';

		// Default condition
		$strSqlRenewalTransferCondition = ' AND CASE WHEN ' . $strTableAlias . '.unit_space_id IS NOT NULL THEN ' . $strTableAlias . '.unit_space_id = cl.unit_space_id ELSE us.unit_type_id = ' . $strTableAlias . '.unit_type_id END ';

		if( false == self::checkForRenewalTransfer( $arrobjLeases ) ) return $strSqlRenewalTransferCondition;

		$objLease = reset( $arrobjLeases );

		// For student based property
		if( true == $objLease->getIsStudentSemesterSelectionEnabled() && true == valId( $objLease->getPropertyFloorPlanId() ) ) {
			$strSqlRenewalTransferCondition = ' AND CASE WHEN ' . $strTableAlias . '.property_floorplan_id IS NOT NULL THEN ' . $strTableAlias . '.property_floorplan_id = ' . ( int ) $objLease->getPropertyFloorPlanId() . ' ELSE TRUE END ';
		} elseif( false == $objLease->getIsStudentSemesterSelectionEnabled() && true == valId( $objLease->getUnitSpaceId() ) ) {
			$strSqlRenewalTransferCondition = ' AND CASE WHEN ' . $strTableAlias . '.unit_space_id IS NOT NULL THEN ' . $strTableAlias . '.unit_space_id = ' . ( int ) $objLease->getUnitSpaceId() . ' ELSE TRUE END ';
		}

		return $strSqlRenewalTransferCondition;
	}

	public static function checkForRenewalTransfer( $arrobjLeases ) {

		// checking if it is the case of renewal transfer or not
		if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjLeases ) ) return false;

		$objLease = reset( $arrobjLeases );

		// if object fails then return default
		if( false == valObj( $objLease, 'CLease' ) ) return false;

		if( false == $objLease->getIsRenewalTransfer() ) return false;

		return true;
	}

	public static function fetchProspectRatesByArTriggerIdsByLeasesByRenewalLeaseTermIdsByCid( $arrintArTriggerIds, $arrobjLeases, $arrintLeaseTermIds, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		$arrintLeaseIds = array_keys( $arrobjLeases );

		if ( false == valArr( $arrintArTriggerIds ) || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseTermIds ) ) return [];
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = '
					SELECT
						CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
							THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
							THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
							ELSE util_get_translated( \'name\', lt.name, lt.details )
						END AS lease_term_name,
						cl.id AS lease_id,
						pr.cid,
						pr.property_id,
						pr.rate_amount,
						lsw.id AS lease_start_window_id,
						lt.id AS lease_term_id,
						lt.term_month As lease_term_months,
						pr.rate_id,
						pr.id,
						pr.ar_code_id,
						pr.ar_trigger_id
					FROM
						cached_leases cl
						JOIN property_charge_settings pcs ON ( cl.cid = pcs.cid AND cl.property_id = pcs.property_id )
						JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = cl.unit_space_id )
						JOIN prospect_rates pr ON ( pr.cid = cl.cid ' . self::getRenewalTransferCondition( $arrobjLeases, $boolIsRenewal = false ) . ' AND pr.property_id = cl.property_id )
						LEFT JOIN property_preferences pp ON ( pp.cid = cl.cid AND pp.property_id = cl.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' )
						JOIN lease_terms lt ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id
												AND lt.is_unset IS FALSE
												AND lt.is_renewal IS TRUE
												AND lt.is_disabled IS FALSE
												AND lt.deleted_by IS NULL
												AND CASE
													WHEN
														COALESCE(pr.lease_term_id,0) > 0 THEN
															lt.id = pr.lease_term_id
													ELSE
														TRUE
												END
												AND CASE WHEN pr.is_student_property = 1 AND pp.value =\'1\' THEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' ELSE TRUE END ' . $strComercialCondition . ' )
						LEFT JOIN lease_start_windows lsw ON ( 	lsw.cid = pcs.cid
															AND lsw.lease_start_structure_id = pcs.lease_start_structure_id
															AND lt.id = lsw.lease_term_id
															AND lsw.is_active IS TRUE
															AND lsw.deleted_by IS NULL
															AND lsw.deleted_on IS NULL
															AND lsw.property_id = cl.property_id
															AND pr.lease_start_window_id = lsw.id )
					WHERE
						cl.id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ' )
						AND pr.ar_trigger_id IN ( ' . sqlIntImplode( $arrintArTriggerIds ) . ' )
						AND pr.ar_origin_id = ' . ( int ) CArOrigin::BASE . '
						AND cl.cid = ' . ( int ) $intCid . '
						AND lt.id IN ( ' . sqlIntImplode( $arrintLeaseTermIds ) . ' )
					ORDER BY
						pr.rate_amount,lt.term_month';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchRenewalTransferRatesByLeasesByRenewalStartDate( $arrobjLeases, $strRenewalStartDate, $intCid, $objDatabase ) {

		$arrintPropertyIds 			= ( array ) getObjectFieldValuesByFieldName( 'PropertyId', $arrobjLeases );
		$arrintUnitTypeIds 			= ( array ) getObjectFieldValuesByFieldName( 'UnitTypeId', $arrobjLeases );
		$arrintUnitSpaceIds 		= ( array ) getObjectFieldValuesByFieldName( 'UnitSpaceId', $arrobjLeases );
		$arrintPropertyFloorplanIds = ( array ) getObjectFieldValuesByFieldName( 'PropertyFloorPlanId', $arrobjLeases );

		$arrintOriginIds = [ CArOrigin::BASE, CArOrigin::SPECIAL ];

		$arrintArTriggerIds = [
			CArTrigger::PRE_QUALIFICATION,
			CArTrigger::APPLICATION_COMPLETED,
			CArTrigger::APPLICATION_APPROVAL,
			CArTrigger::LEASE_COMPLETED,
			CArTrigger::LEASE_APPROVAL,
			CArTrigger::MOVE_IN,
			CArTrigger::LAST_MONTH_OF_OCCUPANCY,
			CArTrigger::NOTICE,
			CArTrigger::MOVE_OUT,
			CArTrigger::END_OF_LEASE_TERM,
			CArTrigger::END_OF_CALENDAR_YEAR,
			CArTrigger::FINAL_STATEMENT,
			CArTrigger::RENEWAL_LEASE_APPROVAL,
			CArTrigger::RENEWAL_START,
			CArTrigger::MONTHLY,
			CArTrigger::DAILY,
			CArTrigger::WEEKLY,
			CArTrigger::NIGHTLY,
			CArTrigger::EARLY_TERMINATION,
			CArTrigger::RETURN_ITEM_FEE,
			CArTrigger::MONTH_TO_MONTH_BEGIN,
			CArTrigger::PROCESS_TRANSFER,
			CArTrigger::SCHEDULE_TRANSFER,
			CArTrigger::WORK_ORDER_FEES
		];

		if( CLeaseIntervalType::RENEWAL == current( $arrobjLeases )->getLeaseIntervalTypeId() ) {
			array_push( $arrintArTriggerIds, CArTrigger::RENEWAL_OFFER_ACCEPTED );
		}
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setPropertyFloorplanIds( $arrintPropertyFloorplanIds );
		$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
		$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );
		$objRateLogsFilter->setArTriggerIds( $arrintArTriggerIds );
		$objRateLogsFilter->setArOriginIds( $arrintOriginIds );
		$objRateLogsFilter->setPublishedUnitsOnly( false );

		$objLease = current( $arrobjLeases );

		if( true == $objLease->getIsStudentSemesterSelectionEnabled() ) {
			$objRateLogsFilter->setIsStudentProperty( true );
			$objRateLogsFilter->setLeaseStartWindowId( $objLease->getLeaseStartWindowId() );
		}

		$objRateLogsFilter->setMoveInDate( $strRenewalStartDate );

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		return true;
	}

	public static function fetchProspectRatesByRateLogsByMoveInDateByTriggerIdsByCid( $arrobjRateLogs, $strMoveInDate, $arrintOtherIncomeChargeTimings, $arrintAmenityChargeTimings, $arrintAddOnChargeTimings, $intCid, $objDatabase, $arrintPetChargeTimings = NULL, $intLeaseIntervalTypeId = CLeaseIntervalType::RENEWAL, $boolIsRenewalTransfer = false, $arrintActiveLeaseIntervalIds = [], $arrintLeaseUnitSpaceIds = [], $boolIsAddUnitSpaceAmenityChargeUponAssignment = false, $boolIsSemesterSelectionEnabled = false, $boolIsCrossPropertyTransfer = false, $boolCreateDeposit = false, $arrintUnitTypeIdsAndFloorPlanIds = [], $boolIsCreateChargesFromRates = false ) {

		$arrobjProspectRateLogs['other_incomes'] = [];
		$arrobjProspectRateLogs['amenities'] = [];
		$arrobjProspectRateLogs['add_ons'] = [];
		$arrobjProspectRateLogs['pets'] = [];

		if( false == valArr( $arrobjRateLogs ) || false == CValidation::validateDate( $strMoveInDate ) ) {
			return $arrobjProspectRateLogs;
		}

		$arrintOriginIds = [ CArOrigin::BASE, CArOrigin::AMENITY, CArOrigin::PET ];
		$arrintPetArCodeTypeIds = [ CArCodeType::RENT, CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY, CArCodeType::EXPENSE, CArCodeType::DEPOSIT ];

		if( true == $boolCreateDeposit ) {
			array_push( CArCodeType::$c_arrintRentDepositeAndOtherChargesArCodeTypes, CArCodeType::DEPOSIT );
		}

		$arrintLeaseIds = [];
		$arrintPropertyIds = [];
		$arrintUnitTypeIds = [];
		$arrintUnitSpaceIds = [];
		$arrintPropertyFloorplanIds = [];
		$arrintSpaceConfigurationIdsByLeaseIds = [];
		$arrintLeaseStartWindowIdsByLeaseTermIds = [];

		foreach( $arrobjRateLogs as $objRateLog ) {
			$arrintLeaseIds[$objRateLog->getLeaseId()]                                              = $objRateLog->getLeaseId();
			$arrintPropertyIds[$objRateLog->getPropertyId()]                                        = $objRateLog->getPropertyId();
			$arrintLeaseTermIdsByLeaseIds[$objRateLog->getLeaseId()][$objRateLog->getLeaseTermId()] = $objRateLog->getLeaseTermId();

			if( true == valId( $objRateLog->getLeaseStartWindowId() ) ) {
				$arrintLeaseStartWindowIdsByLeaseTermIds[$objRateLog->getLeaseTermId()][$objRateLog->getLeaseStartWindowId()] = $objRateLog->getLeaseStartWindowId();
			}
			if( true == valId( $objRateLog->getUnitSpaceId() ) ) {
				$arrintUnitSpaceIds[$objRateLog->getUnitSpaceId()] = $objRateLog->getUnitSpaceId();
				if( true == valArr( $arrintLeaseUnitSpaceIds ) ) {
					$arrintUnitSpaceIds = $arrintLeaseUnitSpaceIds;
				}
			}
			if( true == valId( $objRateLog->getUnitTypeId() ) ) {
				$arrintUnitTypeIds[$objRateLog->getUnitTypeId()] = $objRateLog->getUnitTypeId();
			}
			if( true == valId( $objRateLog->getPropertyFloorplanId() ) ) {
				$arrintPropertyFloorplanIds[$objRateLog->getPropertyFloorplanId()] = $objRateLog->getPropertyFloorplanId();
			}
			if( true == valId( $objRateLog->getSpaceConfigurationId() ) ) {
				$arrintSpaceConfigurationIdsByLeaseIds[$objRateLog->getLeaseId()][$objRateLog->getSpaceConfigurationId()] = $objRateLog->getSpaceConfigurationId();
			}
		}

		if( true == valArr( $arrintUnitTypeIdsAndFloorPlanIds ) ) {
			$arrintUnitTypeIds				= $arrintUnitTypeIdsAndFloorPlanIds['unit_space_ids'];
			$arrintPropertyFloorplanIds		= $arrintUnitTypeIdsAndFloorPlanIds['property_floor_plan_ids'];
		}

		$arrintArTriggerIds = array_merge( $arrintOtherIncomeChargeTimings, $arrintAmenityChargeTimings, $arrintPetChargeTimings );

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setArOriginIds( $arrintOriginIds );
		$objRateLogsFilter->setArTriggerIds( $arrintArTriggerIds );
		$objRateLogsFilter->setArCodeTypeIds( $arrintArCodeTypeIds );
		$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setPropertyFloorplanIds( $arrintPropertyFloorplanIds );
		$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
		$objRateLogsFilter->setMoveInDate( $strMoveInDate );
		$objRateLogsFilter->setLoadAllLeaseStartWindows( true );
		$objRateLogsFilter->setIsEntrata( true );
		$objRateLogsFilter->setLoadOptionalRates( true );

		if( false === $boolIsRenewalTransfer ) {
			$objRateLogsFilter->setLeaseIntervalIds( $arrintActiveLeaseIntervalIds );
		}

		if( true === $boolIsRenewalTransfer && true == $boolIsSemesterSelectionEnabled && CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId ) {
			$objRateLogsFilter->setUnitSpaceIds( [] );
			$objUnitSpace = \Psi\Eos\Entrata\CUnitSpaces::createService()->fetchUnitSpaceByPropertyFloorplanIdByUnitTypeIdByCid( array_shift( $objRateLogsFilter->getPropertyFloorplanIds() ), array_shift( $objRateLogsFilter->getUnitTypeIds() ),  array_shift( $objRateLogsFilter->getCids() ), $objDatabase );
			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$objRateLogsFilter->setUnitSpaceIds( [ $objUnitSpace->getId() ] );
			}
		}

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSpaceConfigurationCondition = '';
		if( true == valArr( $arrintSpaceConfigurationIdsByLeaseIds ) ) {
			$strSpaceConfigurationCondition = '
				AND CASE
					WHEN pr.space_configuration_id IS NOT NULL AND 0 < pr.space_configuration_id
					THEN ( l.id, pr.space_configuration_id ) = ANY ( ARRAY [ ' . sqlIntMultiImplode( $arrintSpaceConfigurationIdsByLeaseIds ) . '] )
					ELSE TRUE
				END
			';
		}

		$strLeaseTermCondition = '';
		if( true == valArr( $arrintLeaseTermIdsByLeaseIds ) ) {
			$strLeaseTermCondition = '
				AND ( l.id, l.lease_term_id ) = ANY ( ARRAY [ ' . sqlIntMultiImplode( $arrintLeaseTermIdsByLeaseIds ) . '] )
			';
		}

		$strLeaseStartWindowCondition = '';
		if( true == valArr( $arrintLeaseStartWindowIdsByLeaseTermIds ) ) {
			$strLeaseStartWindowCondition = '
				AND CASE
						WHEN l.lease_start_window_id IS NOT NULL AND 0 < l.lease_start_window_id
						THEN ( l.lease_term_id, l.lease_start_window_id ) = ANY ( ARRAY [ ' . sqlIntMultiImplode( $arrintLeaseStartWindowIdsByLeaseTermIds ) . '] )
						ELSE TRUE
					END
			';
		}

		// ORDER BY COMMON part
		$strQueryCommonOrderByPart = '
			ORDER BY
				l.term_month,
				l.start_date
		';

		$strProspectRateColumn = 'pr.*';

		$strAmenityUnitSpaceCondition = $strUnitSpaceCondition = ' AND ( COALESCE( pr.unit_space_id, 0 ) = 0 OR l_unit_space_id = pr.unit_space_id ) ';

		if( true === $boolIsRenewalTransfer && true === valArr( $arrintUnitSpaceIds ) ) {
			$strAmenityUnitSpaceCondition = $strUnitSpaceCondition = ' AND ( COALESCE( pr.unit_space_id, 0 ) = 0 OR pr.unit_space_id IN ( ' . sqlIntImplode( $arrintUnitSpaceIds ) . ' ) ) ';
			if( true == valArr( $arrintLeaseUnitSpaceIds ) && true == $boolIsSemesterSelectionEnabled && CLeaseIntervalType::TRANSFER == $intLeaseIntervalTypeId && true == $boolIsAddUnitSpaceAmenityChargeUponAssignment ) {
				$strUnitSpaceCondition        = ' AND ( COALESCE( pr.unit_space_id, 0 ) = 0 OR ( pr.unit_space_id IN ( ' . $arrintLeaseUnitSpaceIds[0] . ' ) ) ) ';
				$strAmenityUnitSpaceCondition = ' AND ( COALESCE( pr.unit_space_id, 0 ) = 0 OR ( ( pr.ar_cascade_id = ' . CArCascade::SPACE . ' AND pr.unit_space_id IN ( ' . sqlIntImplode( $arrintLeaseUnitSpaceIds ) . ' ) )  
																								  OR ( pr.unit_space_id = ' . ( int ) $arrintLeaseUnitSpaceIds[0] . ' ) ) )';
			}
		}

		if( $boolIsRenewalTransfer && $boolIsSemesterSelectionEnabled && CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId ) {
			$strUnitSpaceCondition = '';
			$strAmenityUnitSpaceCondition = '';
			$strQueryCommonOrderByPart = ' ORDER BY pr.id';
			$strProspectRateColumn = ' DISTINCT ON ( pr.id,l.lease_start_window_id  ) pr.*';
		}

		$strLeaseTermDeletedCondition = ' AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL AND lt.is_disabled IS FALSE ';
		$strLeaseStartWindowDeletedCondition = ' AND lsw.deleted_by IS NULL AND lsw.deleted_on IS NULL AND lsw.is_active IS TRUE ';
		$strLeaseTermRenewalCondition = ' AND lt.is_renewal IS TRUE ';
		$strLeaseTermStructureCondition = ' AND pcs.lease_term_structure_id = lt.lease_term_structure_id ';
		$strLeaseStartStructureCondition = ' AND lsw.lease_start_structure_id = pcs.lease_start_structure_id ';
		$strLeasePropertyCondition		 = ' AND l.property_id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) ';
		$strLeasePropertyJoinCondition   = ' AND l.property_id = pr.property_id ';
		$strPetsWhereCondition			 = ' AND pr.lease_interval_id = l.active_lease_interval_id ';
		if( CLeaseIntervalType::TRANSFER == $intLeaseIntervalTypeId ) {
			$strLeaseTermDeletedCondition        = NULL;
			$strLeaseStartWindowDeletedCondition = NULL;
			$strLeaseTermRenewalCondition        = NULL;
			if( true == $boolIsCrossPropertyTransfer ) {
				$strLeaseTermStructureCondition  = '';
				$strLeaseStartStructureCondition = '';
				$strLeasePropertyCondition       = '';
				$strLeasePropertyJoinCondition   = '';
				$strPetsWhereCondition           = '';
			}
		}

		$strSkipUnitSpaceAmenityRatesCondition = '';
		if( true == $boolIsSemesterSelectionEnabled && CLeaseIntervalType::TRANSFER == $intLeaseIntervalTypeId && false == $boolIsAddUnitSpaceAmenityChargeUponAssignment ) {
			$strSkipUnitSpaceAmenityRatesCondition .= ' AND CASE WHEN l.id IS NOT NULL AND l.is_student_property = TRUE THEN NOT( pr.ar_origin_id = ' . CArOrigin::AMENITY . ' AND pr.ar_cascade_id = ' . CArCascade::SPACE . ' ) ELSE TRUE END ';
		}

		// WITH COMMON part
		$strQueryCommonLeaseTermsFetchWithSql = '
			WITH l As (
			SELECT
				l.*,
				lt.id AS lease_term_id,
				lsw.id AS lease_start_window_id,
				pcs.round_type_id,
				CASE
					WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
					THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
					WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
					THEN util_get_translated( \'name\', lt.name, lt.details ) || \' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
					ELSE util_get_translated( \'name\', lt.name, lt.details )
				END AS lease_term_name,
			    CASE WHEN pp.value = \'1\' THEN TRUE ELSE FALSE END AS is_student_property,
				lt.lease_term_type_id,
				lt.term_month,
				lsw.start_date,
				l.unit_space_id AS l_unit_space_id
			FROM leases l
				JOIN property_charge_settings pcs ON ( l.cid = pcs.cid AND l.property_id = pcs.property_id )
				LEFT JOIN property_preferences pp ON ( l.cid = pp.cid AND l.property_id = pp.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' )
				JOIN lease_terms lt ON (
					l.cid = lt.cid
					' . $strLeaseTermStructureCondition . '
					AND lt.is_unset IS FALSE
					AND CASE WHEN pp.value = \'1\' THEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' ELSE TRUE END
					AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' ' . $strLeaseTermRenewalCondition . $strLeaseTermDeletedCondition . '
					AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
				)
				LEFT JOIN lease_start_windows lsw ON (
					lsw.cid = l.cid
					' . $strLeaseStartStructureCondition . '
					AND lt.id = lsw.lease_term_id
					AND lsw.property_id = l.property_id ' . $strLeaseStartWindowDeletedCondition . '
				)
			WHERE 
				l.cid = ' . ( int ) $intCid . '
				AND l.id IN( ' . sqlIntImplode( $arrintLeaseIds ) . ' )
				' . $strLeasePropertyCondition . '
		)';

		// SELECT COMMON part
		$strQueryCommonSelectPart = '
			SELECT
				' . $strProspectRateColumn . ',
				rounder( pr.rate_amount, COALESCE( ac.round_type_id, l.round_type_id ) ) AS rate_amount,
				l.id AS lease_id,
				l.lease_term_name,
				l.lease_term_id,
				l.lease_start_window_id,
				ac.name AS ar_code_name,
				pr.lease_term_id AS pr_lease_term_id
		';

		// FROM COMMON part
		$strQueryCommonFromPart = '
			FROM
				prospect_rates pr
				JOIN ar_codes ac ON (
					ac.id = pr.ar_code_id
					AND ac.cid = pr.cid
				)
				JOIN l ON ( pr.cid = l.cid ' . $strLeasePropertyJoinCondition . ' )
		';

		// WHERE COMMON part
		$strQueryCommonWherePart = '
			WHERE
				pr.cid = ' . ( int ) $intCid . '
				AND pr.property_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintPropertyIds ) . ' ]::INTEGER [] )
				AND ( COALESCE( pr.lease_term_id, 0 ) = 0 OR l.lease_term_id = pr.lease_term_id )
				AND ( COALESCE( pr.lease_start_window_id, 0 ) = 0 OR ( pr.lease_start_window_id IS NOT NULL AND pr.lease_start_window_id = l.lease_start_window_id ) )
		';

		$strQueryCommonWherePart .= $strLeaseTermCondition;
		$strQueryCommonWherePart .= $strLeaseStartWindowCondition;
		$strQueryCommonWherePart .= $strSpaceConfigurationCondition;

		// Query for other charge section begins
		$strOtherIncomesSql = $strQueryCommonLeaseTermsFetchWithSql;
		$strOtherIncomesSql .= $strQueryCommonSelectPart;
		$strOtherIncomesSql .= $strQueryCommonFromPart;
		$strOtherIncomesSql .= $strQueryCommonWherePart;
		$strOtherIncomesSql .= $strUnitSpaceCondition;

		$arrintOtherIncomeArCodeTypeIds = [ CArCodeType::OTHER_INCOME, CArCodeType::OTHER_LIABILITY, CArCodeType::EXPENSE ];

		if( true == $boolCreateDeposit ) {
			array_push( $arrintOtherIncomeArCodeTypeIds, CArCodeType::DEPOSIT );
		}

		if( false == $boolIsCreateChargesFromRates ) $arrintOtherIncomeChargeTimings = CArTrigger::$c_arrintRenewalOneTimeTriggerIds;

		$strOtherIncomesSql .= '
			AND pr.ar_origin_id = ' . CArOrigin::BASE . '
			AND pr.ar_code_type_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintOtherIncomeArCodeTypeIds ) . ']::INTEGER[] )
			AND pr.ar_trigger_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintOtherIncomeChargeTimings ) . ' ]::INTEGER [] ) ';
		$strOtherIncomesSql .= $strQueryCommonOrderByPart;

		$arrobjProspectRateLogs['other_incomes'] = ( array ) CRateLogs::createService()->fetchRateLogs( $strOtherIncomesSql, $objDatabase, false );

		if( false == $boolIsCreateChargesFromRates ) return $arrobjProspectRateLogs;

		// Query for amenity charge section begins
		$strAmenitiesSql = $strQueryCommonLeaseTermsFetchWithSql;
		$strAmenitiesSql .= $strQueryCommonSelectPart . ', util_get_translated( \'name\', a.name, a.details ) AS memo';
		$strAmenitiesSql .= $strQueryCommonFromPart;
		$strAmenitiesSql .= ' JOIN amenities a ON (
				a.cid = pr.cid
				AND a.id = pr.ar_origin_reference_id
				AND a.deleted_by IS NULL
				AND a.deleted_on IS NULL
		)';
		$strAmenitiesSql .= $strQueryCommonWherePart . $strSkipUnitSpaceAmenityRatesCondition;
		$strAmenitiesSql .= $strAmenityUnitSpaceCondition;
		$strAmenitiesSql .= '
			AND pr.ar_origin_id = ' . CArOrigin::AMENITY . '
			AND pr.ar_code_type_id = ANY ( ARRAY [ ' . sqlIntImplode( CArCodeType::$c_arrintRentDepositeAndOtherChargesArCodeTypes ) . ']::INTEGER[] )
			AND pr.ar_trigger_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintAmenityChargeTimings ) . ']::INTEGER [] )
		';
		$strAmenitiesSql .= $strQueryCommonOrderByPart;

		$arrobjProspectRateLogs['amenities'] = ( array ) CRateLogs::createService()->fetchRateLogs( $strAmenitiesSql, $objDatabase, false );

		// Parameter $arrintUnitTypeIdsFromRP passed from Resident portal If it is valid then load rates will not get call for pets
		if( true === $boolIsRenewalTransfer && false == valArr( $arrintUnitTypeIdsAndFloorPlanIds ) ) {
			$objRateLogsFilter = new CRateLogsFilter();
			$objRateLogsFilter->setCids( [ $intCid ] );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::PET ] );
			$objRateLogsFilter->setArTriggerIds( $arrintArTriggerIds );
			$objRateLogsFilter->setArCodeTypeIds( $arrintPetArCodeTypeIds );
			$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );
			$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
			$objRateLogsFilter->setMoveInDate( $strMoveInDate );
			$objRateLogsFilter->setLoadAllLeaseStartWindows( true );
			$objRateLogsFilter->setIsEntrata( true );
			$objRateLogsFilter->setLoadOptionalRates( true );
			$objRateLogsFilter->setIsRenewal( false );
			$objRateLogsFilter->setLeaseIntervalIds( $arrintActiveLeaseIntervalIds );

			CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		}

		// Query for pet charge section begins
		$strPetsSql = $strQueryCommonLeaseTermsFetchWithSql;
		$strPetsSql .= $strQueryCommonSelectPart . ', l.unit_space_id ';
		$strPetsSql .= $strQueryCommonFromPart;

		$strPetsSql .= $strQueryCommonWherePart;

		$strPetsSql .= $strUnitSpaceCondition;

		$strPetsSql .= '
			AND pr.ar_origin_id = ' . CArOrigin::PET . '
			AND pr.ar_code_type_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintPetArCodeTypeIds ) . ']::INTEGER[] )
			' . $strPetsWhereCondition . '
			AND pr.ar_trigger_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintPetChargeTimings ) . ' ]::INTEGER [] )';
		$strPetsSql .= $strQueryCommonOrderByPart;

		$arrobjProspectRateLogs['pets'] = ( array ) CRateLogs::createService()->fetchRateLogs( $strPetsSql, $objDatabase, false );

		// Query for add-on charge section begins
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::ADD_ONS ] );
		$objRateLogsFilter->setArTriggerIds( $arrintAddOnChargeTimings );
		$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setPropertyFloorplanIds( $arrintPropertyFloorplanIds );
		$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
		$objRateLogsFilter->setMoveInDate( $strMoveInDate );
		$objRateLogsFilter->setLoadAllLeaseStartWindows( true );
		$objRateLogsFilter->setIsEntrata( true );
		$objRateLogsFilter->setLoadOptionalRates( true );

		if( false === $boolIsRenewalTransfer ) {
			$objRateLogsFilter->setLeaseIntervalIds( $arrintActiveLeaseIntervalIds );
		}

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			$objRateLogsFilter->setUnitSpaceIds( [ $objUnitSpace->getId() ] );
		}

		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strAddOnsSql = $strQueryCommonLeaseTermsFetchWithSql;
		$strAddOnsSql .= $strQueryCommonSelectPart . ', a.name AS memo, aog.name AS special_name, aot.name as add_on_type_name';
		$strAddOnsSql .= $strQueryCommonFromPart;

		$strAddOnsConditions	= '';
		$strAddOnJoinType		= ' JOIN ';

		if( true === $boolIsRenewalTransfer && CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId ) {
			$strAddOnJoinType = ' LEFT JOIN ';

			$strAddOnsConditions = ' AND CASE
											WHEN la.id IS NULL THEN pr.unit_space_id = ' . current( $objRateLogsFilter->getUnitSpaceIds() ) . ' 
											ELSE la.ar_origin_reference_id = a.id
										END ';
		}

		$strAddOnsSql .= ' JOIN add_ons a ON (
												a.cid = pr.cid
												AND a.property_id = pr.property_id
												AND a.id = pr.ar_origin_reference_id
												AND a.deleted_by IS NULL
												AND a.deleted_on IS NULL
												AND ( ( a.available_on IS NOT NULL AND a.available_on <= \'' . $strMoveInDate . '\' )' . ' OR ( a.available_on IS NULL ) )
											) '
							. $strAddOnJoinType . ' lease_associations la ON (
															la.cid = pr.cid
															AND la.property_id = pr.property_id
															AND la.lease_interval_id = pr.lease_interval_id
															AND la.id = pr.lease_association_id
															AND ( la.id IS NULL OR la.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) )
															AND la.ar_origin_id =  ' . CArOrigin::ADD_ONS . '
															AND la.deleted_by IS NULL

														)
							JOIN add_on_groups aog ON ( aog.id = a.add_on_group_id AND aog.cid = a.cid AND aog.property_id = a.property_id )
							JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id ) ';

		$strAddOnsSql .= $strQueryCommonWherePart;
		$strAddOnsSql .= $strUnitSpaceCondition;
		$strAddOnsSql .= '
			AND pr.ar_origin_id = ' . CArOrigin::ADD_ONS . '
			AND pr.ar_code_type_id = ANY ( ARRAY [ ' . sqlIntImplode( [ CArCodeType::RENT, CArCodeType::OTHER_INCOME ] ) . ']::INTEGER[] )
			AND pr.ar_trigger_id = ANY ( ARRAY [ ' . sqlIntImplode( $arrintAddOnChargeTimings ) . ']::INTEGER [] )';

		$strAddOnsSql .= $strAddOnsConditions;

		if( false === $boolIsRenewalTransfer ) {
			$strAddOnsSql .= 'AND l.active_lease_interval_id = pr.lease_interval_id';
		}

		$strAddOnsSql .= $strQueryCommonOrderByPart;

		$arrobjProspectRateLogs['add_ons'] = ( array ) CRateLogs::createService()->fetchRateLogs( $strAddOnsSql, $objDatabase, false );

		return $arrobjProspectRateLogs;
	}

	public static function loadEstimatedMTMAmounts( $arrobjLeases, $arrintPropertyIds, $intCid, $intCompanyUserId, $objDatabase, $objTaxCalculationLibrary = NULL ) : array {

		if( false === valArr( $arrobjLeases )
			|| false === valArr( $arrintPropertyIds )
			|| false === valId( $intCid )
			|| false === valId( $intCompanyUserId )
			|| false === valObj( $objDatabase, 'CDatabase' ) ) {
			return [];
		}

		$arrstrLeaseEndDates = extractUniqueFieldValuesFromObjects( 'getLeaseEndDate', $arrobjLeases );
		$strMaxLeaseEndDate = NULL;

		foreach( $arrstrLeaseEndDates as $strLeaseEndDate ) {
				$strRenewalStartDate = date( 'm/d/Y',  strtotime( $strLeaseEndDate . ' +1 DAY' ) );
			if( strtotime( $strMaxLeaseEndDate ) < strtotime( $strLeaseEndDate ) ) {
				$strMaxLeaseEndDate = $strLeaseEndDate;
			}
		}

		$strMaxLeaseEndDate = date( 'm/d/Y',  strtotime( $strMaxLeaseEndDate . ' +1 DAY' ) );

		$arrmixEstimatedMTMAmounts = [];

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::NORMAL, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $intCid, $arrintPropertyIds, $intCompanyUserId );

		$objPostScheduledChargesCriteria->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_SCHEDULED_CHARGES )
			->setIsDryRun( true )
			->setDebugMode( true )
			->setIncludeNonEntrataCoreProperties( true )
			->setLeaseIds( array_keys( $arrobjLeases ) )
			->setCacheableArTriggerIds( CArTrigger::$c_arrintRecurringArTriggers )
			->setPostWindowEndDateOverride( $strMaxLeaseEndDate );

		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );

		$arrintScheduledChargeTypes = [ CScheduledChargeType::MONTH_TO_MONTH, CScheduledChargeType::MONTH_TO_MONTH_FEE ];

		$strSql = ' SELECT
						sc.parent_scheduled_charge_id,
						sc.lease_id,
						sc.charge_amount,
						ac.name AS ar_code_name,
						ac.ar_code_type_id,
						COALESCE( ac.ar_trigger_id, sc.ar_trigger_id ) AS ar_trigger_id,
						ac.id AS ar_code_id,
						sc.ar_formula_id,
						sc.ar_formula_reference_id,
						sc.ar_cascade_reference_id
					FROM
						scheduled_charges sc
						JOIN ar_codes ac ON( ac.cid = sc.cid AND sc.ar_code_id = ac.id )
						JOIN lease_intervals li ON( sc.cid = li.cid AND sc.property_id = li.property_id AND sc.lease_id = li.lease_id AND sc.lease_interval_id = li.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id IN( ' . sqlIntImplode( array_keys( $arrobjLeases ) ) . ' )
						AND li.lease_status_type_id = ' . ( int ) CLeaseStatusType::FUTURE . '
						AND li.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::MONTH_TO_MONTH . '
						AND sc.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND sc.ar_trigger_id IN ( ' . sqlIntImplode( CArTrigger::$c_arrintRecurringArTriggers ) . ' )
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.charge_end_date IS NULL
						AND sc.parent_scheduled_charge_id IS NOT NULL
						AND sc.scheduled_charge_type_id IN ( ' . sqlIntImplode( $arrintScheduledChargeTypes ) . ' ) ';

		$arrmixMTMAmountData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixMTMAmountData ) ) {
			foreach( $arrmixMTMAmountData as $mixMTMData ) {
				$strMixKey                                                      = $mixMTMData['parent_scheduled_charge_id'] . '_' . $mixMTMData['ar_trigger_id'];
				$arrmixEstimatedMTMAmounts[$mixMTMData['lease_id']][$strMixKey] = [
					'parent_scheduled_charge_id' => $mixMTMData['parent_scheduled_charge_id'],
					'mtm_amount'                 => $mixMTMData['charge_amount'],
					'ar_code_name'               => $mixMTMData['ar_code_name'],
					'ar_code_type_id'            => $mixMTMData['ar_code_type_id'],
					'ar_trigger_id'              => $mixMTMData['ar_trigger_id'],
					'ar_code_id'                 => $mixMTMData['ar_code_id'],
					'ar_formula_id'              => $mixMTMData['ar_formula_id'],
					'ar_formula_reference_id'    => $mixMTMData['ar_formula_reference_id'],
					'ar_cascade_reference_id'    => $mixMTMData['ar_cascade_reference_id']
				];
				if( true == valObj( $objTaxCalculationLibrary, \Psi\Core\AccountsReceivables\TaxCalculation\CTaxCalculationLibrary::class ) && true == valId( $mixMTMData['ar_code_id'] ) && true == valStr( $strRenewalStartDate ) ) {
					$fltTotalMtMTaxPercent = $objTaxCalculationLibrary->totalTaxPercentMultiplier( $mixMTMData['ar_code_id'], ( string ) $strRenewalStartDate );
				}
				$arrmixEstimatedMTMAmounts[$mixMTMData['lease_id']][$strMixKey]['tax_percent'] = $fltTotalMtMTaxPercent;
			}
		}

		return $arrmixEstimatedMTMAmounts;
	}

	public static function fetchDepositRatesByUnitSpaceIdsByLeaseTermIdsByPropertyIds( $intPropertyId, $intUnitSpaceId, $intLeaseTermId, $intLeaseStartWindowId, $intSpaceConfigurationId, $strMoveInDate, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ ( int ) $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );
		$objRateLogsFilter->setArTriggerIds( [ CArTrigger::MOVE_OUT, CArTrigger::MOVE_IN ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );
		$objRateLogsFilter->setLoadOptionalRates( true );
		$objRateLogsFilter->setMoveInDate( $strMoveInDate );
		CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT pr.*,
						   atr.name AS ar_trigger_name,
					       ac.name AS ar_code_name,
					       lt.name AS lease_term_name
					FROM prospect_rates pr
							JOIN lease_terms lt ON( pr.cid = lt.cid AND pr.lease_term_id = lt.id )
					        JOIN ar_triggers atr ON( pr.ar_trigger_id = atr.id )
					        JOIN ar_codes ac ON( ac.cid = pr.cid AND ac.id = pr.ar_code_id )
					WHERE
						CASE
							WHEN
								pr.space_configuration_id > 0
							THEN
								pr.space_configuration_id = ' . ( int ) $intSpaceConfigurationId . '
							ELSE
								pr.space_configuration_id = 0
							END
						AND CASE
								WHEN
									pr.lease_term_id > 0
								THEN
									pr.lease_term_id = ' . ( int ) $intLeaseTermId . '
								ELSE
									pr.lease_term_id = 0
								END
						AND CASE
								WHEN
									pr.lease_start_window_id > 0
								THEN
									pr.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
								ELSE
									pr.lease_start_window_id = 0
								END';

		return CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );
	}

	public static function fetchRenewalBaseRentLeaseTerms( $objLease, $intCid, $objDatabase, $boolIsCrossPropertyTransfer = false, $objProperty = NULL ) {

		if( false === valObj( $objLease, 'CLease' ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) return [];

		$strLeaseTermStructureCondition			= ' AND pcs.lease_term_structure_id = lt.lease_term_structure_id ';
		$strLeaseStartStructureCondition		= ' AND lsw.lease_start_structure_id = pcs.lease_start_structure_id ';
		$strNewPropertyCondition				= 'AND paor.property_id = cl.property_id';
		$strPropertyGlSettingsCondition			= 'AND pcs.property_id = pgs.property_id';
		$strPropertyColumn						= 'cl.property_id As property_id';
		if( true === valId( $objLease->getLeaseTermId() ) ) {
			$strScheduledTransferLeaseTermCondition = ' AND lt.id = ' . $objLease->getLeaseTermId() . ' ';
		} else {
			$strScheduledTransferLeaseTermCondition = ' AND lt.term_month IN( ' . CLeaseTerm::LEASE_TERM_STANDARD . ' ) ';
		}
		if( true === valId( $objLease->getLeaseStartWindowId() ) ) {
			$strScheduledTransferLeaseStartWindowCondition = ' AND lsw.id = ' . $objLease->getLeaseStartWindowId() . ' ';
		}
		if( true == $boolIsCrossPropertyTransfer ) {
			$strLeaseTermStructureCondition  = '';
			$strLeaseStartStructureCondition = '';

			if( true == valObj( $objProperty, 'CProperty' ) ) {
				$strNewPropertyCondition			= 'AND paor.property_id =' . $objProperty->getId() . ' ';
				$strPropertyGlSettingsCondition 	= 'AND pgs.property_id =' . $objProperty->getId() . ' ';
				$strPropertyColumn				 	= $objProperty->getId() . ' AS property_id ';
			}
		}
		if( true === valId( $objLease->getUnitSpaceId() ) ) {
			$strPropertyFloorPlanColumn = ' us.property_floorplan_id, ';
			$strUnitTypeColumn = ' us.unit_type_id, ';
			$strUnitSpaceColumn = $objLease->getUnitSpaceId() . ' AS unit_space_id, ';
			$strUnitSpaceJoinCondition = 'JOIN unit_spaces us ON ( cl.cid = us.cid AND us.id = ' . $objLease->getUnitSpaceId() . ' )';
		}

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					WITH ple As ( SELECT
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance,
									round ( count ( pu.id ) * lem.expiration_tolerance / 100 ) pu_count
								FROM
									property_gl_settings pgs
									JOIN lease_expiration_months lem ON ( pgs.cid =lem.cid AND lem.lease_expiration_structure_id = pgs.lease_expiration_structure_id )
									LEFT JOIN property_units pu ON ( pu.cid = lem.cid AND pu.property_id = pgs.property_id )
								WHERE
									pgs.cid = ' . ( int ) $intCid . '
									AND pgs.property_id = ' . ( int ) $objLease->getPropertyId() . '
								GROUP BY
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance
									ORDER BY
									lem.month ASC )
					SELECT
							cl.cid,
							cl.id AS lease_id,
							' . $strPropertyColumn . ',
							' . $strPropertyFloorPlanColumn . '
							' . $strUnitTypeColumn . '
							' . $strUnitSpaceColumn . '
							lt.id AS lease_term_id,
							lsw.id AS lease_start_window_id,
							lt.term_month AS lease_term_months,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL THEN lsw.renewal_start_date
								ELSE lsw.start_date
							END AS window_start_date,
							lsw.end_date AS window_end_date,
							lt.show_on_website,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/ YYYY\')||\')\'
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.renewal_start_date,\'MM/DD/ YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								ELSE util_get_translated( \'name\', lt.name, lt.details )
							END AS lease_term_name,
							--ac.name AS ar_code_name,
							ple.pu_count
					FROM
							cached_leases cl
							JOIN property_charge_settings pcs ON ( pcs.cid = cl.cid AND pcs.property_id = cl.property_id AND cl.id = ' . ( int ) $objLease->getId() . ' )
							JOIN lease_terms lt ON ( lt.cid = pcs.cid ' . $strLeaseTermStructureCondition . ' AND lt.is_unset IS FALSE AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) )
							JOIN properties p ON ( cl.property_id = p.id AND p.cid = cl.cid )
							JOIN property_ar_origin_rules paor ON ( paor.cid = cl.cid ' . $strNewPropertyCondition . ' AND paor.ar_origin_id = ' . CArOrigin::BASE . ' )
						' . $strUnitSpaceJoinCondition . '
							JOIN property_gl_settings pgs ON ( pcs.cid = pgs.cid ' . $strPropertyGlSettingsCondition . ' )
							LEFT JOIN LATERAL (
												SELECT
													ca.*
												FROM
													lease_intervals ili
													JOIN cached_applications ca ON ( ili.cid = ca.cid AND ca.lease_interval_id = ili.id )
												WHERE
													ili.lease_id = cl.id
													AND ili.cid = cl.cid
													AND ili.lease_interval_type_id NOT IN (' . CLeaseIntervalType::MONTH_TO_MONTH . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
													AND ili.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) . ' )
													AND ili.lease_start_date <= cl.lease_start_date
												ORDER BY ili.lease_start_date DESC
												LIMIT 1
											) as ca ON TRUE

							LEFT JOIN lease_start_windows lsw ON (	lsw.cid = pcs.cid
																	' . $strLeaseStartStructureCondition . '
																	AND lt.id = lsw.lease_term_id
																	AND lsw.property_id = cl.property_id )
								LEFT JOIN property_preferences pp ON ( pp.cid = cl.cid AND pp.property_id = cl.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' )
							LEFT JOIN lease_processes lp ON ( lp.cid = cl.cid AND lp.transfer_lease_id =  cl.id AND lp.transferred_on IS NULL )
							LEFT JOIN ple ON ( ple.cid = cl.cid AND ple.property_id = cl.property_id AND ple.month = date_part ( \'month\', CASE
						                                                                                                   WHEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' THEN CASE
						                                                                                                                                                                      WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN CURRENT_DATE
						                                                                                                                                                                      ELSE cl.lease_end_date + 1
						                                                                                                                                        END  + ( lt.term_month || \'MONTHS -1 DAY\' )::INTERVAL
						                                                                                                   ELSE DATE ( date_trunc ( \'month\', lsw.end_date ) )
						                                                                                                 END ) )
							--LEFT JOIN ar_codes ac ON ( ac.cid = cl.cid AND ac.id = rr.ar_code_id )
							WHERE
							cl.cid = ' . ( int ) $intCid . '
							AND CASE
									WHEN lsw.start_date IS NOT NULL
										THEN DATE (lsw.end_date) > cl.lease_start_date
									ELSE TRUE
							END
							AND CASE
									WHEN lsw.start_date IS NOT NULL
										THEN DATE ( lsw.end_date) > CASE
																		WHEN cl.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . '
																			THEN CURRENT_DATE
																		ELSE cl.lease_start_date
																	END
									ELSE TRUE
							END
							AND CASE
									WHEN ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.value = \'1\'
										THEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.id IS NOT NULL
									ELSE lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
								END ' . $strScheduledTransferLeaseTermCondition . $strScheduledTransferLeaseStartWindowCondition . '
								ORDER BY
						    lsw.renewal_start_date,
							lt.term_month ';

		$arrobjBaseRentLeaseTerms = CRateLogs::createService()->fetchRateLogs( $strSql, $objDatabase, false );

		return $arrobjBaseRentLeaseTerms;

	}

}