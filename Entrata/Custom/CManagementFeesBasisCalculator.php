<?php

class CManagementFeesBasisCalculator {

	protected $m_boolCalculateCashReceipts;
	protected $m_boolCalculateAccrualRevenue;
	protected $m_boolCalculateCashRevenue;

	protected $m_fltSecurityDepositsAmount;
	protected $m_fltPaymentsInTransitAmount;
	protected $m_fltPriorPostMonthPaymentsInTransitAmount;

	protected $m_fltReceiptsAmount;
	protected $m_fltPrepaymentRefundsAmount;
	protected $m_fltSecurityDepositAllocationsAmount;

	protected $m_fltCashRevenueAmount;
	protected $m_fltAccrualRevenueAmount;

	public function getSecurityDepositsAmount() {
		return $this->m_fltSecurityDepositsAmount;
	}

	public function getPaymentsInTransitAmount() {
		return $this->m_fltPaymentsInTransitAmount;
	}

	public function getPriorPostMonthPaymentsInTransitAmount() {
		return $this->m_fltPriorPostMonthPaymentsInTransitAmount;
	}

	public function getReceiptsAmount() {
		return $this->m_fltReceiptsAmount;
	}

	public function getPrepaymentRefundsAmount() {
		return $this->m_fltPrepaymentRefundsAmount;
	}

	public function getSecurityDepositAllocationsAmount() {
		return $this->m_fltSecurityDepositAllocationsAmount;
	}

	public function getCashRevenueAmount() {
		return $this->m_fltCashRevenueAmount;
	}

	public function getAccrualRevenueAmount() {
		return $this->m_fltAccrualRevenueAmount;
	}

	public function getCalculateCashReceipts() {
		return $this->m_boolCalculateCashReceipts;
	}

	public function getCalculateAccrualRevenue() {
		return $this->m_boolCalculateAccrualRevenue;
	}

	public function getCalculateCashRevenue() {
		return $this->m_boolCalculateCashRevenue;
	}

	public function setSecurityDepositsAmount( $fltSecurityDepositsAmount ) {
		$this->m_fltSecurityDepositsAmount = $fltSecurityDepositsAmount;
	}

	public function setPaymentsInTransitAmount( $fltPaymentsInTransitAmount ) {
		$this->m_fltPaymentsInTransitAmount = $fltPaymentsInTransitAmount;
	}

	public function setPriorPostMonthPaymentsInTransitAmount( $fltPriorPostMonthPaymentsInTransitAmount ) {
		$this->m_fltPriorPostMonthPaymentsInTransitAmount = $fltPriorPostMonthPaymentsInTransitAmount;
	}

	public function setReceiptsAmount( $fltReceiptsAmount ) {
		$this->m_fltReceiptsAmount = $fltReceiptsAmount;
	}

	public function setPrepaymentRefundsAmount( $fltPrepaymentRefundsAmount ) {
		$this->m_fltPrepaymentRefundsAmount = $fltPrepaymentRefundsAmount;
	}

	public function setSecurityDepositAllocationsAmount( $fltSecurityDepositAllocationsAmount ) {
		$this->m_fltSecurityDepositAllocationsAmount = $fltSecurityDepositAllocationsAmount;
	}

	public function setCashRevenueAmount( $fltCashRevenueAmount ) {
		$this->m_fltCashRevenueAmount = $fltCashRevenueAmount;
	}

	public function setAccrualRevenueAmount( $fltAccrualRevenueAmount ) {
		$this->m_fltAccrualRevenueAmount = $fltAccrualRevenueAmount;
	}

	public function setCalculateCashReceipts( $boolCalculateCashReceipts ) {
		$this->m_boolCalculateCashReceipts = $boolCalculateCashReceipts;
	}

	public function setCalculateAccrualRevenue( $boolCalculateAccrualRevenue ) {
		$this->m_boolCalculateAccrualRevenue = $boolCalculateAccrualRevenue;
	}

	public function setCalculateCashRevenue( $boolCalculateCashRevenue ) {
		$this->m_boolCalculateCashRevenue = $boolCalculateCashRevenue;
	}

	protected function getCashReceiptsSql( $intPropertyId, $strPostMonth, $intCid ) {
		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return '';

		return '
			SELECT
				p.cid,
				p.id AS property_id,

				COALESCE( at_deposits.security_deposit_receipts, 0 ) AS security_deposits_amount,
				COALESCE( at_current_transit.pit_amount, 0 )*-1 AS payments_in_transit_amount,
				COALESCE( at_prior_transit.prior_post_month_payments_in_transit_amount, 0 )*-1 AS prior_post_month_payments_in_transit_amount,

				COALESCE( at_receipts.receipts_amount, 0 ) AS receipts_amount,
				COALESCE( at_refund.pre_payments_refund, 0 ) AS prepayment_refunds_amount,
				COALESCE( at_deposits.security_deposit_allocations, 0 ) AS security_deposit_allocations,

				0 AS cash_revenue_amount,
				0 AS accrual_revenue_amount
			FROM
				properties p
				LEFT JOIN LATERAL (
					SELECT
						at.cid,
						at.property_id,
						SUM ( at.transaction_amount ) * -1 AS receipts_amount
					FROM
						ar_transactions at
						JOIN cached_leases l ON ( l.cid = at.cid AND l.property_id = at.property_id AND l.id = at.lease_id )
					    LEFT JOIN ar_payments ap ON ( ap.cid = at.cid AND ap.property_id = at.property_id AND ap.id = at.ar_payment_id )
					    LEFT JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id )
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.property_id = p.id
						AND at.ar_code_type_id = ' . CArCodeType::PAYMENT . '
						AND NOT at.is_temporary
						AND at.post_month = \'' . $strPostMonth . '\'::DATE
						AND l.occupancy_type_id NOT IN ( ' . COccupancyType::PROPERTY . ' )
					    AND ( pst.id != ' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ' OR pst.id IS NULL )
					GROUP BY
						1, 2
				) at_receipts ON at_receipts.cid = p.cid AND at_receipts.property_id = p.id
				
				LEFT JOIN LATERAL (
					SELECT
						at.cid,
						at.property_id,
						SUM(
							CASE
								WHEN ac_credit.ar_code_type_id = ' . CArCodeType::PAYMENT . ' THEN aa.allocation_amount
								WHEN ac_credit.ar_code_type_id = ' . CArCodeType::REFUND . ' AND at_orig.ar_code_type_id = ' . CArCodeType::PAYMENT . ' THEN aa.allocation_amount	
								ELSE 0
							END * ( CASE WHEN at.transaction_amount < 0 THEN 1 ELSE - 1 END )    
						) AS pre_payments_refund
					FROM
						ar_transactions at
						JOIN ar_allocations aa ON aa.cid = at.cid AND ( aa.charge_ar_transaction_id = at.id OR aa.credit_ar_transaction_id = at.id ) AND aa.post_month = \'' . $strPostMonth . '\'::DATE
						JOIN ar_transactions at_credit ON at_credit.cid = aa.cid AND at_credit.id = aa.credit_ar_transaction_id
						JOIN ar_codes ac_credit ON ac_credit.cid = at_credit.cid AND ac_credit.id = at_credit.ar_code_id
                        JOIN cached_leases cl ON cl.cid = at.cid AND cl.id = at.lease_id
					    LEFT JOIN ar_payments arp ON arp.cid = at_credit.cid AND arp.id = at_credit.ar_payment_id
					    LEFT JOIN ap_headers ah ON ah.cid = at.cid AND ah.refund_ar_transaction_id = at.id AND ah.reversal_ap_header_id IS NULL
						LEFT JOIN ar_transactions at_orig ON at_orig.cid = at_credit.cid AND at_orig.id = at_credit.ar_transaction_id
					WHERE
						at.cid = ' . ( int ) $intCid . '
						AND at.property_id = p.id
						AND at.ar_code_type_id = ' . CArCodeType::REFUND . '
					GROUP BY
						1, 2
				) at_refund ON at_refund.cid = p.cid AND at_refund.property_id = p.id
				
				LEFT JOIN LATERAL (
			         WITH cte_payments_in_transit AS 
					 (
					 SELECT
					     art.cid,
					     art.property_id,
					     art.id,
					     art.lease_id,
					     art.post_month,
					     art.transaction_amount
					 FROM
					     ar_transactions art
					     LEFT JOIN ar_deposit_transactions adt ON adt.cid = art.cid AND adt.ar_transaction_id = art.id
					     LEFT JOIN ar_deposits ad ON ad.cid = adt.cid AND ad.id = adt.ar_deposit_id AND ad.post_month <= \'' . $strPostMonth . '\'::DATE AND ad.is_deleted = 0
					 WHERE
					     art.cid = ' . ( int ) $intCid . '
					     AND p.id = art.property_id
					     AND art.ar_code_type_id = ' . CArCodeType::PAYMENT . '
					     AND art.is_temporary = FALSE
					     AND art.post_month <= \'' . $strPostMonth . '\'::DATE
					 GROUP BY
					     art.cid,
					     art.id
					 HAVING
					     SUM ( CASE
					             WHEN ad.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT . ' THEN art.transaction_amount
					             WHEN ad.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' THEN -1 * transaction_amount
					             ELSE 0
					           END ) = 0)
					SELECT
					   pit.cid,
					   pit.property_id,
					    SUM ( pit.transaction_amount ) AS pit_amount
					 FROM
					     cte_payments_in_transit pit
					     LEFT JOIN cached_lease_logs cll ON cll.cid = pit.cid AND cll.lease_id = pit.lease_id AND cll.reporting_post_month <= \'' . $strPostMonth . '\'::DATE AND cll.apply_through_post_month >= \'' . $strPostMonth . '\'::DATE AND cll.is_post_month_ignored = 0
					 WHERE
					     pit.cid = ' . ( int ) $intCid . '
                         AND cll.occupancy_type_id NOT IN ( ' . COccupancyType::PROPERTY . ' )
					 GROUP BY
					     pit.cid,
					     pit.property_id
				) at_current_transit ON at_current_transit.cid = p.cid AND at_current_transit.property_id = p.id

				LEFT JOIN LATERAL (
			         WITH cte_payments_in_transit AS 
					 (
					 SELECT
					     art.cid,
					     art.property_id,
					     art.id,
					     art.lease_id,
					     art.post_month,
					     art.transaction_amount
					 FROM
					     ar_transactions art
					     LEFT JOIN ar_deposit_transactions adt ON adt.cid = art.cid AND adt.ar_transaction_id = art.id
					     LEFT JOIN ar_deposits ad ON ad.cid = adt.cid AND ad.id = adt.ar_deposit_id AND ad.post_month <= ( \'' . $strPostMonth . '\'::DATE - INTERVAL \'1 MONTH\' ) AND ad.is_deleted = 0
					 WHERE
					     art.cid = ' . ( int ) $intCid . '
					     AND p.id = art.property_id
					     AND art.ar_code_type_id = ' . CArCodeType::PAYMENT . '
					     AND art.is_temporary = FALSE
					     AND art.post_month <= ( \'' . $strPostMonth . '\'::DATE - INTERVAL \'1 MONTH\' )
					 GROUP BY
					     art.cid,
					     art.id
					 HAVING
					     SUM ( CASE
					             WHEN ad.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT . ' THEN art.transaction_amount
					             WHEN ad.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' THEN -1 * transaction_amount
					             ELSE 0
					           END ) = 0)
					SELECT
					   pit.cid,
					   pit.property_id,
					    SUM ( pit.transaction_amount ) AS prior_post_month_payments_in_transit_amount
					 FROM
					     cte_payments_in_transit pit
					     LEFT JOIN cached_lease_logs cll ON cll.cid = pit.cid AND cll.lease_id = pit.lease_id AND cll.reporting_post_month <= ( \'' . $strPostMonth . '\'::DATE - INTERVAL \'1 MONTH\' ) AND cll.apply_through_post_month >= ( \'' . $strPostMonth . '\'::DATE - INTERVAL \'1 MONTH\' ) AND cll.is_post_month_ignored = 0
					 WHERE
					     pit.cid = ' . ( int ) $intCid . '
                         AND cll.occupancy_type_id NOT IN ( ' . COccupancyType::PROPERTY . ' )
					 GROUP BY
					     pit.cid,
					     pit.property_id
				) at_prior_transit ON at_prior_transit.cid = p.cid AND at_prior_transit.property_id = p.id

				/* Security Deposit Receipts & Allocations */

				LEFT JOIN LATERAL (
					SELECT
						art.cid,
						art.property_id,
						( SUM ( 
							CASE WHEN at_offset.ar_code_type_id <> ' . CArCodeType::REFUND . ' AND art.is_deposit_credit = TRUE AND aa.post_month = \'' . $strPostMonth . '\'::DATE
							THEN 
								CASE WHEN art.transaction_amount >= 0 
									THEN COALESCE ( aa.allocation_amount, 0 ) * - 1
						        ELSE COALESCE ( aa.allocation_amount, 0 )
								END
							ELSE 0
					        END 
					    ) * -1 ) AS security_deposit_allocations,
						( SUM (
							CASE WHEN at_offset.ar_code_type_id = ' . CArCodeType::PAYMENT . ' AND art.is_deposit_credit = FALSE AND aa.post_month = \'' . $strPostMonth . '\'::DATE
							THEN
								CASE
									WHEN art.transaction_amount >= 0 THEN COALESCE ( aa.allocation_amount, 0 ) * - 1
								ELSE COALESCE ( aa.allocation_amount, 0 )
								END
						  ELSE 0
						END ) ) AS security_deposit_receipts
					FROM
						ar_transactions art
						JOIN ar_codes ac ON ac.cid = art.cid AND ac.id = art.ar_code_id
							AND ac.ar_code_type_id = ' . CArCodeType::DEPOSIT . ' AND ac.gl_account_type_id = ' . CGlAccountType::LIABILITIES . '
						LEFT JOIN ar_allocations aa ON aa.cid = art.cid AND aa.lease_id = art.lease_id
														AND CASE
															WHEN art.transaction_amount > 0 THEN aa.charge_ar_transaction_id = art.id
															ELSE aa.credit_ar_transaction_id = art.id
														END
						LEFT JOIN ar_transactions at_offset ON at_offset.cid = aa.cid AND at_offset.lease_id = aa.lease_id
														AND CASE
															WHEN art.transaction_amount > 0 THEN aa.credit_ar_transaction_id = at_offset.id
															ELSE aa.charge_ar_transaction_id = at_offset.id
														END
					WHERE
						art.cid = ' . ( int ) $intCid . '
						AND art.property_id = p.id
						AND art.post_month <= \'' . $strPostMonth . '\'::DATE
						AND NOT art.is_temporary
					GROUP BY
						1, 2
				) at_deposits ON at_deposits.cid = p.cid AND at_deposits.property_id = p.id
			WHERE
				p.cid = ' . ( int ) $intCid . '
				AND p.id = ' . ( int ) $intPropertyId;
	}

	protected function getAccrualRevenueSql( $intPropertyId, $strPostMonth, $intCid ) {
		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return '';

		return '
			SELECT
				gd.cid,
				gd.property_id,

				0 AS security_deposits_amount,
				0 AS payments_in_transit_amount,
				0 AS prior_post_month_payments_in_transit_amount,
				0 AS receipts_amount,
				0 AS prepayment_refunds_amount,
				0 AS security_deposit_allocations,

				0 AS cash_revenue_amount,

				( SUM ( gd.amount ) * -1 ) AS accrual_revenue_amount
			FROM
				gl_details gd
				JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ') AND gh.is_template = FALSE )
				JOIN gl_accounts ga ON ( gd.cid = ga.cid AND ga.id = gd.accrual_gl_account_id )
			WHERE
				gd.cid = ' . ( int ) $intCid . '
				AND gd.property_id = ' . ( int ) $intPropertyId . '
				AND gd.post_month = \'' . $strPostMonth . '\'::DATE
				AND gh.post_month = \'' . $strPostMonth . '\'::DATE
				AND gd.accrual_gl_account_id IS NOT NULL
				AND EXISTS (
					SELECT 1
						FROM gl_account_trees gat 
						JOIN gl_groups gg ON ( gat.cid = gg.cid AND gat.gl_group_id = gg.id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
						WHERE 
						ga.cid = gat.cid AND gat.gl_account_id = ga.id
						AND gg.gl_account_type_id = ' . ( int ) CGlAccountType::INCOME . '
						AND gt.gl_tree_type_id = ' . CGlTreeType::STANDARD . '
						AND gt.system_code = \'DEFAULT\' 
						)
			GROUP BY
				1, 2';
	}

	protected function getCashRevenueSql( $intPropertyId, $strPostMonth, $intCid ) {
		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return '';

		return '
			SELECT
				gd.cid,
				gd.property_id,

				0 AS security_deposits_amount,
				0 AS payments_in_transit_amount,
				0 AS prior_post_month_payments_in_transit_amount,
				0 AS receipts_amount,
				0 AS prepayment_refunds_amount,
				0 AS security_deposit_allocations,
	
				( SUM ( gd.amount ) * -1 ) AS cash_revenue_amount,
	
				0 AS accrual_revenue_amount
			FROM
				gl_details gd
				JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ') AND gh.is_template = FALSE )
				JOIN gl_accounts ga ON ( gd.cid = ga.cid AND ga.id = gd.cash_gl_account_id )
			WHERE
				gd.cid = ' . ( int ) $intCid . '
				AND gd.property_id = ' . ( int ) $intPropertyId . '
				AND gd.post_month = \'' . $strPostMonth . '\'::DATE
				AND gh.post_month = \'' . $strPostMonth . '\'::DATE
				AND gd.cash_gl_account_id IS NOT NULL
				AND EXISTS (
					SELECT 1
						FROM gl_account_trees gat 
						JOIN gl_groups gg ON ( gat.cid = gg.cid AND gat.gl_group_id = gg.id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
						WHERE 
						ga.cid = gat.cid AND gat.gl_account_id = ga.id
						AND gg.gl_account_type_id = ' . ( int ) CGlAccountType::INCOME . '
						AND gt.gl_tree_type_id = ' . CGlTreeType::STANDARD . '
						AND gt.system_code = \'DEFAULT\' 
						)
			GROUP BY
				1, 2';
	}

	public function calculateBasisAmounts( $intPropertyId, $strPostMonth, $intCid, CDatabase $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) || ( true != $this->getCalculateAccrualRevenue() && true != $this->getCalculateCashReceipts() && true != $this->getCalculateCashRevenue() ) ) return [];

		$arrmixFetchedData = $this->fetchBasisAmountsByPropertyIdByPostMonth( $intPropertyId,  $strPostMonth, $intCid, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			$this->setValues( $arrmixFetchedData );
		}
	}

	public function fetchBasisAmountsByPropertyIdByPostMonth( $intPropertyId, $strPostMonth, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return [];

		$strOuterSql = '
			SELECT 
				sub.cid,
				sub.property_id,

				SUM( sub.security_deposits_amount ) AS security_deposits_amount,
				SUM( sub.payments_in_transit_amount ) AS payments_in_transit_amount,
				SUM( sub.prior_post_month_payments_in_transit_amount ) AS prior_post_month_payments_in_transit_amount,

				SUM( sub.receipts_amount ) AS receipts_amount,
				SUM( sub.prepayment_refunds_amount ) AS prepayment_refunds_amount,
				SUM( sub.security_deposit_allocations ) AS security_deposit_allocations_amount,

				SUM( sub.cash_revenue_amount ) AS cash_revenue_amount,
				SUM( sub.accrual_revenue_amount ) AS accrual_revenue_amount
            FROM (
            ';

		$strUnionAllSnippet = '
					UNION ALL
		';

		$arrstrSqls = [];

		if( true === $this->m_boolCalculateCashReceipts ) {
			$arrstrSqls[] = $this->getCashReceiptsSql( $intPropertyId, $strPostMonth, $intCid );
		}
		if( true === $this->m_boolCalculateCashRevenue ) {
			$arrstrSqls[] = $this->getCashRevenueSql( $intPropertyId, $strPostMonth, $intCid );
		}
		if( true === $this->m_boolCalculateAccrualRevenue ) {
			$arrstrSqls[] = $this->getAccrualRevenueSql( $intPropertyId, $strPostMonth, $intCid );
		}

		if( false == valArr( $arrstrSqls ) ) {
			return [];
		}

		$strOuterSql .= implode( $strUnionAllSnippet, $arrstrSqls );

		$strOuterSql .= '
                        ) AS sub
                   GROUP BY
                   		1, 2
		';

		$arrmixFetchedData = current( ( array ) fetchData( $strOuterSql, $objDatabase ) );

		$this->setValues( $arrmixFetchedData );

		return $arrmixFetchedData;
	}

	public function setValues( $arrmixValues ) {
		if( true === isset( $arrmixValues['security_deposits_amount'] ) )	$this->setSecurityDepositsAmount( bcadd( $arrmixValues['security_deposits_amount'], 0, 2 ) );
		if( true === isset( $arrmixValues['security_deposit_allocations_amount'] ) ) $this->setSecurityDepositAllocationsAmount( $arrmixValues['security_deposit_allocations_amount'] );
		if( true === isset( $arrmixValues['payments_in_transit_amount'] ) )	    $this->setPaymentsInTransitAmount( $arrmixValues['payments_in_transit_amount'] );
		if( true === isset( $arrmixValues['prior_post_month_payments_in_transit_amount'] ) )	    $this->setPriorPostMonthPaymentsInTransitAmount( $arrmixValues['prior_post_month_payments_in_transit_amount'] );
		if( true === isset( $arrmixValues['receipts_amount'] ) )	    $this->setReceiptsAmount( $arrmixValues['receipts_amount'] );
		if( true === isset( $arrmixValues['prepayment_refunds_amount'] ) )	$this->setPrepaymentRefundsAmount( $arrmixValues['prepayment_refunds_amount'] );

		if( true === isset( $arrmixValues['cash_revenue_amount'] ) ) $this->setCashRevenueAmount( $arrmixValues['cash_revenue_amount'] );
		if( true === isset( $arrmixValues['accrual_revenue_amount'] ) ) $this->setAccrualRevenueAmount( $arrmixValues['accrual_revenue_amount'] );
	}

}
?>
