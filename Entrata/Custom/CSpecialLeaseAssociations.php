<?php

class CSpecialLeaseAssociations extends CLeaseAssociations {

	/**
	 * QUOTE SPECIALS RELATED FETCH QUERIES
	 */

	public static function fetchLeaseAssociationsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) )	return NULL;

		$strSql = '	SELECT
						la.*,
						ra.lease_term_id
					FROM lease_associations la
						JOIN rates ra ON ( la.cid = ra.cid AND la.ar_origin_reference_id = ra.ar_origin_reference_id AND ra.cid = ' . ( int ) $intCid . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' )
					WHERE la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	/**
	 * RENEWAL OFFER SPECIALS RELATED FETCH QUERIES
	 */

	public static function fetchSpecialLeaseAssociationsByQuoteIdBySpecialTypeIdByCid( $intQuoteId, $intSpecialTypeId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						la.*,
						ra.lease_term_id,
						ra.rate_amount
					FROM
						lease_associations la
						JOIN specials sp ON ( la.cid = sp.cid AND la.ar_origin_reference_id = sp.id AND sp.cid = ' . ( int ) $intCid . ' AND sp.special_type_id = ' . ( int ) $intSpecialTypeId . ' )
						JOIN rates ra ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND ra.cid = ' . ( int ) $intCid . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id = ' . ( int ) $intQuoteId;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialLeaseAssociationsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {
	    if( false == valArr( $arrintQuoteIds ) )  return NULL;

	    $strSql = '	SELECT
						la.*,
						sp.company_media_file_id,
						sp.special_type_id,
						ra.id AS rate_id,
						ra.lease_term_id,
						ra.ar_trigger_id,
						ra.ar_code_id,
						ra.rate_amount,
						util_get_translated( \'name\', sp.name, sp.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS special_name,
						util_get_translated( \'description\', sp.description, sp.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS special_description						
					FROM lease_associations la
						JOIN specials sp ON ( la.cid = sp.cid AND la.ar_origin_reference_id = sp.id AND sp.cid = ' . ( int ) $intCid . ' AND sp.special_recipient_id = ' . ( int ) CSpecialRecipient::RENEWALS . ' )
						JOIN rates ra ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND ra.cid = ' . ( int ) $intCid . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' )
					WHERE la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )';

	    return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchActiveSpecialLeaseAssociationsCountByArOriginReferenceIdByPropertyIdIdByCid( $intArOriginReferenceId, $intPropertyId,$intCid, $objDatabase ) {
		if( false == valId( $intArOriginReferenceId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strWhere = 'WHERE ar_origin_id = ' . CArOrigin::SPECIAL . ' AND 
						ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . ' AND 
						property_id = ' . ( int ) $intPropertyId . ' AND 
						cid = ' . ( int ) $intCid . ' AND 
						lease_status_type_id IN (' . CLeaseStatusType::APPLICANT . ', ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ') AND
						deleted_by IS NULL AND
						deleted_on IS NULL';

		return self::fetchLeaseAssociationCount( $strWhere, $objDatabase );
	}

}
?>