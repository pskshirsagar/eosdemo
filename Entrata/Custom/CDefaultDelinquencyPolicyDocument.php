<?php

class CDefaultDelinquencyPolicyDocument extends CBaseDefaultDelinquencyPolicyDocument {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultDelinquencyPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultFileTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelinquencyThresholdTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDocumentName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valThresholdOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResendFrequency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAutoSent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSetCertifiedFundsOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStartEviction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>