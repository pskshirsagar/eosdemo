<?php

class CUnitExclusionReasonScheduledTask extends CScheduledTask {

	public function execute( $intCompanyUserId, $objDatabase, $objApplication = NULL, $objEvent = NULL, $boolVerbose = false ) {

		$objEventLibrary = NULL;
		$boolIsValid = false;
		$intUnitSpaceId = $this->getUnitSpaceId();
		$intPriorScheduledTaskId = $this->getPriorScheduledTaskId();
		$intCid = $this->getCid();
		$intCountScheduledTask = 1;

		if( true == valId( $intPriorScheduledTaskId ) ) {
			$intCountScheduledTask = \Psi\Eos\Entrata\CScheduledTasks::createService()->fetchTotalScheduledTaskByCidByUnitSpaceId( $intCid, $intUnitSpaceId, $objDatabase );
		}

		if( true == valId( $intPriorScheduledTaskId ) && 1 < $intCountScheduledTask ) {
			return true;
		} else {
			if( true == valId( $intPriorScheduledTaskId ) ) {
				$intUnitExclusion = CUnitExclusionReasonType::NOT_EXCLUDED;
			} else {
				$intUnitExclusion = ( int ) $this->getTitle();
			}
			$objUnitSpace     = \Psi\Eos\Entrata\CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $intUnitSpaceId, $intCid, $objDatabase );
			$objPropertyUnit  = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $objUnitSpace->getPropertyUnitId(), $intCid, $objDatabase );
			$objCompanyUser   = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUserByIdByCid( $intCompanyUserId, $intCid, $objDatabase );

			if( $objUnitSpace->getUnitExclusionReasonTypeId() != $intUnitExclusion ) {
				$arrintUnitExclusionReasonEventSubTypesIds = CEventSubType::$c_arrintUnitExclusionReasonEventSubTypesIds;
				$objEventLibrary                           = new CEventLibrary();

				$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
				$objEventLibraryDataObject->setDatabase( $objDatabase );

				$objEvent = $objEventLibrary->createEvent( [ $objPropertyUnit ], CEventType::UNIT_EXCLUSION_REASON_CHANGE, $arrintUnitExclusionReasonEventSubTypesIds[$intUnitExclusion] );
				$objEvent->setCid( $objUnitSpace->getCid() );
				$objEvent->setPropertyId( $objUnitSpace->getPropertyId() );
				$objEvent->setPropertyUnitId( $objUnitSpace->getPropertyUnitId() );
				$objEvent->setUnitExclusionReason( [ 'old_unit_exclusion_reason_type_id' => $objUnitSpace->getUnitExclusionReasonTypeId(), 'new_unit_exclusion_reason_type_id' => $intUnitExclusion ] );
				$objEvent->setUnitSpaceId( $objUnitSpace->getId() );
				$objEvent->setScheduledDatetime( 'NOW()' );
				$objEvent->setEventDatetime( 'NOW()' );
				$objEvent->setCompanyUser( $objCompanyUser );
				$objEventLibrary->buildEventDescription( $objPropertyUnit );

				$objUnitSpace->setUnitExclusionReasonTypeId( $intUnitExclusion );
				$objUnitSpace->setUnitSpaceLogTypeId( CUnitSpaceLogType::MAKE_READY_STATUS );
				$boolIsValid = true;
			}

			switch( NULL ) {
				default:

					if( false == $boolIsValid ) {
						break;
					}

					$objDatabase->begin();
					if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && false == $objUnitSpace->update( $intCompanyUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						$this->addErrorMsgs( $objUnitSpace->getErrorMsgs() );
						break;
					}

					if( true == valObj( $objEventLibrary, 'CEventLibrary' ) && false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
						break;
					}
					$objDatabase->commit();
			}
		}
	}

}
