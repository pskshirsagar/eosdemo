<?php

class CPetRateAssociations extends CRateAssociations {

	public static function fetchPetRateAssociationsByCid( $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:from CClient::fetchPropertyPetTypes()

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSortedPetRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:from CpropertyPetTypes::fetchSortedPropertyPetTypesByPropertyIdByCid()

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSortedPetRateAssociationsForPropertyByPropertyIdWithPetTypeNameByCid( $intPropertyId, $intCid, $objDatabase, $boolFetchData = false, $intPetTypeId = NULL ) {

		// MEGARATES_COMMENTS:taken from CPropertyPetTypes::fetchSortedPropertyPetTypesByPropertyIdWithPetTypeNameByCid( $intPropertyId, $intCid, $objDatabase )
		$strSqlCondition = '';

		if( true == valId( $intPetTypeId ) ) {
			$strSqlCondition = ' AND ra.ar_origin_reference_id = ' . $intPetTypeId;
		}

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name,
						pt.default_pet_type_id,
						pt.is_icon_visible
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						' . $strSqlCondition . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						order_num';

		if( false != $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdByArCascadeIdByArCascadeReferenceIdByPetTypeIdByCidWithPetTypeName( $intPropertyId, $intArCascadeId, $intArCascadeReferenceId, $intPetTypeId, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:taken from CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdByCompanyPetTypeIdWithNullPropertyUnitIdByCid( $ )
		// AND fetchPropertyPetTypesByPropertyIdByCompanyPetTypeIdByPropertyUnitIdByCid

		$strSql = 'SELECT
						ra.*,
						util_get_system_translated( \'name\', dpt.name, dpt.details ) as default_pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( pt.cid = ra.cid AND pt.id = ra.ar_origin_reference_id )
						JOIN default_pet_types dpt ON ( dpt.id = pt.default_pet_type_id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intPetTypeId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdByArCascadeIdByArCascadeReferenceIdByPetTypeIdsByCidWithPetTypeName( $intPropertyId, $intArCascadeId, $intArCascadeReferenceId, $arrintPetTypeIds, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:taken from CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdByCompanyPetTypeIdWithNullPropertyUnitIdByCid( $ )
		// AND fetchPropertyPetTypesByPropertyIdByCompanyPetTypeIdByPropertyUnitIdByCid

		$strSql = 'SELECT
						ra.*,
						util_get_system_translated( \'name\', dpt.name, dpt.details ) as default_pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( pt.cid = ra.cid AND pt.id = ra.ar_origin_reference_id )
						JOIN default_pet_types dpt ON ( dpt.id = pt.default_pet_type_id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintPetTypeIds ) . ' )
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSortedPetRateAssociationsByPropertyIdByUnitSpaceIdsByCidWithPetTypeName( $intPropertyId, $arrintUnitSpaceIds, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS: Taken from CpropetyPetTypes::fetchSortedPropertyPetTypesByPropertyIdByPropertyUnitIdWithPetTypeNameByCid
		// MEGARATES_COMMENTS: Note: AND ra.property_unit_id  = ' . ( int ) $intPropertyUnitId . ' is removed by AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdByUnitSpaceIdsWithPetTypeNameByCid( $intPropertyId, $arrintUnitSpaceIds, $intCid, $objDatabase, $boolIsProperty = false ) {

		// MEGARATES_COMMENTS:logic from CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdByPropertyUnitIdWithPetTypeNameByCid().Note: propertyUnitId is replaced with unitSpcadeIds

		$strSql = 'SELECT
                        ra.ar_origin_reference_id,
                        util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name,
                        ra.ar_origin_id,
                        pt.default_pet_type_id
                    FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE
						pt.cid = ' . ( int ) $intCid . '
						AND ra.is_optional = true
						AND ra.ar_origin_id 	= ' . ( int ) CArOrigin::PET;

                    if( $boolIsProperty == true ) {
			             $strSql .= ' AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
			                          AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId;
                    } else {
			             $strSql .= ' AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
                                      AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';
                    }

                    $strSql .= ' AND ra.property_id = ' . ( int ) $intPropertyId . '
                                 ORDER BY pt.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchDistinctSortedPetRateAssociationsByPropertyIdByUnitSpaceIdsByCidWithPetTypeName( $intPropertyId, $arrintUnitSpaceIds, $intCid, $objDatabase, $boolFetchData = false, $intPetTypeId = NULL ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$strSqlWhereCondition = '';

		if( true == valId( $intPetTypeId ) ) {
			$strSqlWhereCondition = ' AND ra.ar_origin_reference_id = ' . ( int ) $intPetTypeId;
		}

		$strSql = 'SELECT
						DISTINCT ON (ra.ar_origin_reference_id)
						ra.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name,
						pt.is_icon_visible
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						' . $strSqlWhereCondition . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					ORDER BY
						ra.ar_origin_reference_id, ra.order_num, ' . $objDatabase->getCollateSort( 'pt.name' );

		if( false != $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsWithDefaultPetTypeIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS: replacement function for CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdsWithPetTypeIdByCid

		$strSql = 'SELECT
						ra.*,
						pt.default_pet_type_id,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON pt.id = ra.ar_origin_reference_id AND pt.cid = ra.cid
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.is_featured = true
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsWithDefaultPetTypeIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS: replacement function for CProperty::fetchPropertyPetTypesWithPetTypeId AND CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdWithPetTypeIdByCid

		$strSql = 'SELECT
						ra.*,
						pt.default_pet_type_id
					FROM
						rate_associations ra
						JOIN pet_types pt ON pt.id = ra.ar_origin_reference_id AND pt.cid = ra.cid
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.is_featured = true
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationByArCascadeIdByPetTypeIdByPropertyId( $intArCascadeId, $intPetTypeId, $intPropertyId, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:taken from CPropertyPetTypes::fetchPropertyPetTypeByPropertyIdByPetTypeIdWithNullPropertyUniIdByCid ,

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id::int = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_reference_id::int = ' . ( int ) $intPetTypeId;

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPetTypeIdsByPropertyIdByCid( $arrintPetTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:taken from CPropertyPetTypes::fetchPropertyPetTypesByPropertyIdByCompanyPetTypeIdsByCid

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id::int = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintPetTypeIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:replacement function for CPropertyPetTypes::fetchPetTypesByPropertyIdsByCid

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name,
						pt.default_pet_type_id
					FROM
						rate_associations ra
						JOIN pet_types pt ON pt.id = ra.ar_origin_reference_id AND pt.cid = ra.cid
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdByUnitSpaceIds( $intPropertyId, $arrintUnitSpaceIds, $intCid, $objDatabase ) {

		// MEGARATES_COMMENTS:replacement function for CPropertyPetTypes::fetchSortedPropertyPetTypesByPropertyIdByPropertyUnitIdByCid

		$strSql = 'SELECT
						ra.*,
						us.property_unit_id
					FROM
						rate_associations ra
						LEFT JOIN unit_spaces us ON ( us.cid = ra.cid AND us.property_id = ra.property_id AND us.id = ra.unit_space_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPetTypeIdByCid( $intPetTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN pet_types pt ON pt.id = ra.ar_origin_reference_id AND pt.cid = ra.cid
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intPetTypeId . '
						AND ra.is_featured = true';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.id = ' . ( int ) $intId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET;

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRatesByArOriginReferenceIdsByPropertyIdsByCids( $intArOriginRefenceIds, $intPropertyId, $intCid, $objDatabase ) {
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::PET ] );
		$objRateLogsFilter->setArOriginReferenceIds( $intArOriginRefenceIds );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		$arrobjRates = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );

		return $arrobjRates;
	}

	public static function fetchSortedPetRateAssociationsByArCascadeIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intPetTypeId = NULL ) {

		$strSqlCondition = '';

		if( true == valId( $intPetTypeId ) ) {
			$strSqlCondition = ' AND ra.ar_origin_reference_id = ' . $intPetTypeId;
		}

		$strSql = 'SELECT
						ra.*,
						us.property_unit_id
					FROM
						rate_associations ra
						LEFT JOIN unit_spaces us ON ( us.cid = ra.cid AND us.property_id = ra.property_id AND us.id = ra.unit_space_id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id::int = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						' . $strSqlCondition . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByArCascadeIdByArOriginReferenceIdByPetTypeIdByPropertyId( $intArOriginRefenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id::int = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.ar_origin_reference_id::int = ' . ( int ) $intArOriginRefenceId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationByPropertyIdByArCascadeIdByArCascadeReferenceIdByPetTypeIdByCidWithPetTypeName( $intPropertyId, $intArCascadeId, $intArCascadeReferenceId, $intPetTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*,
						util_get_system_translated( \'name\', dpt.name, dpt.details ) as default_pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( pt.cid = ra.cid AND pt.id = ra.ar_origin_reference_id )
						JOIN default_pet_types dpt ON ( dpt.id = pt.default_pet_type_id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intPetTypeId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static  function fetchRateAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ra.property_id, 
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name,
						( CASE WHEN r.property_id IS NOT NULL THEN r.rate_amount ELSE NULL END ) AS rate_amount
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id AND pt.default_pet_type_id  IN ( ' . ( int ) CDefaultPetType::DOG . ', ' . ( int ) CDefaultPetType::CAT . ' ) )
						LEFT JOIN rates r ON ( ra.cid = r.cid AND ra.property_id = r.property_id AND ra.ar_origin_reference_id = r.ar_origin_reference_id AND r.is_allowed = TRUE AND r.deactivation_date > NOW() )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_origin_id = ' . CArOrigin::PET . ' AND ra.unit_type_id IS NULL
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyUnitIdByPropertyIdByCid( $intPropertyUnitId, $intPropertyId, $intCid, $objDatabase, $intPetTypeId = NULL ) {
		if( false == valId( $intPropertyUnitId ) ) return NULL;

		$strSqlCondition = '';

		if( true == valId( $intPetTypeId ) ) {
			$strSqlCondition = ' AND ra.ar_origin_reference_id = ' . $intPetTypeId;
		}

		$strSql = ' SELECT
						 DISTINCT ON (ra.ar_origin_reference_id)
                         ra.*
					FROM
						 rate_associations ra
					     JOIN leases l on ( l.cid = ra.cid and l.unit_space_id = ra.unit_space_id )
					     JOIN unit_spaces us ON ( l.unit_space_id = us.id AND l.cid = us.cid )
					     JOIN property_units pu on ( l.cid = pu.cid and l.property_unit_id = pu.id )
					WHERE
						 ra.cid = ' . ( int ) $intCid . '
						 AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						 AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						 AND ra.property_id = ' . ( int ) $intPropertyId . '
						 ' . $strSqlCondition . '
						 AND pu.id = ' . ( int ) $intPropertyUnitId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetMinMaxRates( $intPropertyId, $intCid, $objDatabase ) {

		$arrintArCodeTypeIds = [ CArCodeType::RENT, CArCodeType::DEPOSIT, CArCodeType::OTHER_INCOME ];
		$strSql = 'SELECT 
			            min( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::RENT . ' OR prr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' ) THEN prr.rate_amount END ) min_pet_rent,
			            min( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::DEPOSIT . ' ) THEN prr.rate_amount END ) min_pet_deposit,
			            min( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::OTHER_INCOME . ' AND prr.ar_trigger_id <> ' . CArTrigger::MONTHLY . ' ) THEN prr.rate_amount END ) min_pet_other,
			            max( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::RENT . ' OR prr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' ) THEN prr.rate_amount END ) max_pet_rent,
			            max( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::DEPOSIT . ' ) THEN prr.rate_amount END ) max_pet_deposit,
			            max( CASE WHEN ( prr.ar_code_type_id = ' . CArCodeType::OTHER_INCOME . ' AND prr.ar_trigger_id <> ' . CArTrigger::MONTHLY . ' ) THEN prr.rate_amount END ) max_pet_other
		           FROM
		                prospect_rates prr
		           WHERE
		                prr.cid = ' . ( int ) $intCid . '
		                AND prr.property_id = ' . ( int ) $intPropertyId . '
			            AND prr.ar_origin_id = ' . CArOrigin::PET . ' AND prr.ar_origin_reference_id IS NOT NULL AND prr.ar_code_type_id IN ( ' . implode( ',', $arrintArCodeTypeIds ) . ' )';

	return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByLeaseIdByPropertyIdByArOriginReferenceIdsByCid( $intLeaseId, $intPropertyId, $arrintArOriginReferenceIds, $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						sub.*
					FROM
						(
						  SELECT
							  ra.*,
							  util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name,
							  rank ( ) OVER ( PARTITION BY ra.cid, ra.ar_origin_reference_id ORDER BY ra.ar_cascade_id DESC )
						  FROM
							  rate_associations ra
							  JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
							  LEFT JOIN leases l ON ( l.cid = ra.cid AND l.property_id = ra.property_id AND l.id = ' . ( int ) $intLeaseId . ' )
                              LEFT JOIN unit_spaces us ON ( us.cid = l.cid AND us.property_id = l.property_id AND us.id = l.unit_space_id )
						  WHERE
							  ra.cid = ' . ( int ) $intCid . '
							  AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
							  AND ra.property_id = ' . ( int ) $intPropertyId . '
							  AND ra.ar_origin_reference_id IN ( ' . sqlIntImplode( $arrintArOriginReferenceIds ) . ' )
							  AND CASE
							  		WHEN ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' THEN ra.ar_cascade_reference_id = l.property_id
							  		WHEN ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' THEN ra.ar_cascade_reference_id = us.unit_type_id
							  		WHEN ra.ar_cascade_id = ' . CArCascade::SPACE . ' THEN ra.ar_cascade_reference_id = l.unit_space_id
							  	END
						) AS sub
					WHERE
						sub.rank = 1';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

 	public static function fetchPetRateAssociationsByPropertyIdByUnitSpaceIdsByPetTypeId( $intPropertyId, $arrintUnitSpaceIds, $intPetTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) || false == valId( $intPropertyId ) || false == valId( $intPetTypeId ) ) return NULL;

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as pet_type_name
					FROM
						rate_associations ra
						JOIN pet_types pt ON ( ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE
						ra.cid::int=' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intPetTypeId . ' 
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					ORDER BY
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetRateAssociationsByPropertyIdsByQuoteIdsByCid( $arrintPropertyIds, $arrintQuoteIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintQuoteIds ) || false == valId( $intCid ) ) return [];

		$strSql = 'SELECT 	
						DISTINCT ON (ra.cid,ra.ar_origin_reference_id,q.id) ra.*,
					    util_get_translated(\'name\', pt.name, pt.details, \'en_US\') as pet_type_name,
					    pt.is_icon_visible,
						q.id AS quote_id,
						pu.max_pets
					FROM 
						quotes q
					    JOIN unit_spaces us ON (q.cid = us.cid AND q.unit_space_id = us.id)
					    JOIN property_units pu ON (us.cid = pu.cid AND us.property_unit_id = pu.id)
					    JOIN unit_spaces usp ON (pu.cid = usp.cid AND pu.id = usp.property_unit_id)
					    JOIN rate_associations ra ON (pu.cid = ra.cid AND ( ra.ar_cascade_reference_id = usp.id OR ra.ar_cascade_reference_id = pu.property_id ) )
					    JOIN pet_types pt ON (ra.cid = pt.cid AND ra.ar_origin_reference_id = pt.id )
					WHERE 
						ra.cid = ' . ( int ) $intCid . ' AND
					    ra.ar_origin_id = ' . CArOrigin::PET . ' AND
					    ra.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) AND
						q.id IN ( ' . sqlIntImplode( $arrintQuoteIds ) . ' ) AND
						ra.is_published = TRUE
					ORDER BY
						ra.cid,
						ra.ar_origin_reference_id,
				        q.id,
				        ra.ar_cascade_id DESC,
				        pt.name
         ';

		return self::fetchRateAssociations( $strSql, $objDatabase, false );

	}

}
?>